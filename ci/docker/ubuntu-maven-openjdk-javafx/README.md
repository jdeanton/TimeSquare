# how to manage the docker images for use by gitlab-ci


cf. https://gitlab.inria.fr/jdeanton/TimeSquare/container_registry

from this directory

(replace latest by a unique tag, for example the current date)

```
docker login registry.gitlab.inria.fr
docker build -t registry.gitlab.inria.fr/jdeanton/timesquare/ubuntu-maven-openjdk-javafx:latest .
docker push registry.gitlab.inria.fr/jdeanton/timesquare/ubuntu-maven-openjdk-javafx:latest
```

TODO automate this action in gitlab-ci (needs to filter on path in order to avoid to build the project itself instead of the docker image)

TIP :  interactive test 
```
docker run -it registry.gitlab.inria.fr/jdeanton/timesquare/ubuntu-maven-openjdk-javafx:latest /bin/bash
```