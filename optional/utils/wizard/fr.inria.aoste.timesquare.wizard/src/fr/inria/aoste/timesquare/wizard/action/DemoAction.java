/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.action;

import java.util.Properties;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;

import fr.inria.aoste.timesquare.wizard.wizards.CCSLLibNewWizard;
import fr.inria.aoste.timesquare.wizard.wizards.ExtendedCCSLNewWizard;
import fr.inria.aoste.timesquare.wizard.wizards.ProjectWizard;

public class DemoAction implements IIntroAction {

	public DemoAction() {
		super();

	}

	@Override
	public void run(IIntroSite site, Properties params) {
		String param = params.getProperty("paramwizard");
		System.out.println("Running ");
		Shell shell = Display.getDefault().getActiveShell();
		IWizard wizard = null;
		if ("prj".equals(param)) {
			wizard = new ProjectWizard();
		}
		if ("ext".equals(param)) {
			wizard = new ExtendedCCSLNewWizard();
		}
		if ("lib".equals(param)) {
			wizard = new CCSLLibNewWizard();
		}

		if (wizard != null) {
			WizardDialog dialog = new WizardDialog(shell, wizard);
			dialog.open();
		}
		System.out.println("End  ");

	}

}
