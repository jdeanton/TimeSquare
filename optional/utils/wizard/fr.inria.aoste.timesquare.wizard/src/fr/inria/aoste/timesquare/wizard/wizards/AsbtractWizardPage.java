/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

public abstract  class AsbtractWizardPage extends WizardNewFileCreationPage {

	protected String extension;
	
	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public AsbtractWizardPage(IStructuredSelection selection, String _extension) {
		super("wizardPage", selection);
		setTitle("File");
		extension = _extension;
		setDescription("This wizard creates a new file with *." + extension
				+ " extension.");
		setFileName("newfile." + extension);
	}

	@Override
	protected boolean validatePage() {
		if (super.validatePage()) {
			String fileExtension = new Path(getFileName()).getFileExtension();
			if (fileExtension == null || ! fileExtension.endsWith(extension)) {
				setErrorMessage("Extension :" + extension + " Expected");
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	protected void initialPopulateContainerNameField() {	
		try
		{
			super.initialPopulateContainerNameField();
		}
		catch (Exception e) {
			
		}
		validatePage();
	}

	@Override
	public void createControl(Composite parent) {		
		super.createControl(parent);
		validatePage();
	}

	
	public IFile getFile() {
		return ResourcesPlugin.getWorkspace().getRoot().getFile(getContainerFullPath().append(getFileName()));
	}
	


}