/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;

/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (extendedCCSL).
 */

public class CCSLLibWizardPage extends AsbtractWizardPage {
	
	public CCSLLibWizardPage(IStructuredSelection selection ) {
		super(selection, "ccslLib");
		
	}
	
	
	
}