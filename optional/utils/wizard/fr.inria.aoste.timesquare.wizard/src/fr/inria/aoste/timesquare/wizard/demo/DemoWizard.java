/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.demo;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;

public class DemoWizard extends BasicNewProjectResourceWizard
// implements INewWizard
{

	public DemoWizard() {
		// super();
	}

	protected WizardNewProjectCreationPage mainPage;

	@Override
	public void addPages() {
		mainPage = new WizardNewProjectCreationPage("CCSL DEMO Project page");
		mainPage.setDescription("Create a new CCSL DEMO project");
		mainPage.setTitle("CCSL DEMO project :");
		mainPage.setMessage("Hello");
		mainPage.setInitialProjectName("DemoCCSL");
		// mainPag
		addPage(mainPage);

	}

	@Override
	public boolean performFinish() {
		try {
			newProject = createProject();
			if (newProject == null)
				return true;
			updatePerspective();
			selectAndReveal(newProject, PluginHelpers.getActiveWorkbench());
			//
		} catch (Throwable ex) {

		}
		return true;
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
	}

	protected IProject newProject;
	protected IProject newProjectHandle = null;

	protected IProject createProject() {
		if (newProject != null)
			return newProject;

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		if (mainPage == null)
			return newProject;
		newProjectHandle = mainPage.getProjectHandle();
		final IProjectDescription description = workspace
				.newProjectDescription(newProjectHandle.getName());
		if (!mainPage.useDefaults())
			description.setLocation(mainPage.getLocationPath());

		WorkspaceModifyOperation operation = new WorkspaceModificationCreateProject(this, description);

		try {
			getContainer().run(true, true, operation);
		} catch (InterruptedException e) {
			return null;
		} catch (InvocationTargetException e) {
			MessageDialog.openError(getShell(),
					"Cannot create a CCSL demo project", e.getTargetException().getMessage());
			return null;
		}

		return newProjectHandle;
	}

	protected void createProject(IProjectDescription description,
			IProject projectHandle, IProgressMonitor monitor)
	/*throws CoreException, OperationCanceledException*/{
		try {
			monitor.beginTask("Create Project", 200);
			projectHandle.create(description, new SubProgressMonitor(monitor, 10));
			// TODO ..
		//	GenerateExampleforDemo ccslfile = new GenerateExampleforDemo(projectHandle);
		//	ccslfile.create();
			// projectHandle.getLocation().append("CCSL")
			// monitor.done();
			if (monitor.isCanceled())
				throw new OperationCanceledException();
			projectHandle.open(new SubProgressMonitor(monitor, 10));

		} catch (Throwable ex) {
			ex.printStackTrace();
			ErrorConsole.printError(ex);
			// ex.printStackTrace();
		} finally {
			monitor.done();
		}
	}

}
