/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class ResourceSelectionWizardPage extends WizardPage implements IWizardPage {

	/**
	 * @wbp.parser.constructor
	 */
	public ResourceSelectionWizardPage(String pageName, String title, String extension, boolean multipleSelect) {
		super(pageName);
		setTitle(title);
		if (multipleSelect) {
			setDescription("Select the resource(s) to import");
		}
		else {
			setDescription("Select a resource to import");
		}
		this.extension = extension;
		this.multipleSelection = multipleSelect;
	}

	private String extension;
	private boolean multipleSelection;
	private List<Object> liblist = new ArrayList<Object>();

	public class ListLabelProvider extends LabelProvider implements ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			if (index == 1) {
				if (obj instanceof IFile)
					return ((IFile) obj).getFullPath().toOSString();
				return obj.toString();
			}
			return null;
		}

		public Image getColumnImage(Object obj, int index) {
			return null;
		}
	}

	public class ListContentProvider implements IStructuredContentProvider {

		public ListContentProvider() {
			super();
		}

		public Object[] getElements(Object element) {
			if (element instanceof List) {
				return ((List<?>) element).toArray();
			}
			return null;
		}

		@Override
		public void dispose() {

		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

		}

	}

	TableViewer tv = null;

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		{

			GridData data = new GridData();
			data.verticalAlignment = GridData.FILL;
			data.grabExcessVerticalSpace = true;
			data.horizontalAlignment = GridData.FILL;
			composite.setLayoutData(data);
		}
		composite.setLayout(new FormLayout());

		Label containerLabel = new Label(composite, SWT.LEFT);
		{
			FormData fd_containerLabel = new FormData();
			fd_containerLabel.right = new FormAttachment(100, -5);
			fd_containerLabel.top = new FormAttachment(0, 5);
			fd_containerLabel.left = new FormAttachment(0, 5);
			containerLabel.setLayoutData(fd_containerLabel);
			if (multipleSelection) {
				containerLabel.setText("Select Resource(s)");
			}
			else {
				containerLabel.setText("Select One Resource");
			}
		}
		Table tb;
		if (multipleSelection) {
			tb = new Table(composite, SWT.PUSH | SWT.CHECK | SWT.MULTI);
		}
		else {
			tb = new Table(composite, SWT.SINGLE);
		}
		tv = new TableViewer( tb );
		{
			tb.setHeaderVisible(true);
			FormData fd_t = new FormData();

			fd_t.top = new FormAttachment(containerLabel, 6);
			fd_t.bottom = new FormAttachment(100, -18);
			fd_t.left = new FormAttachment(0, 5);
			fd_t.right = new FormAttachment(100, -5);
			tb.setLayoutData(fd_t);
			TableViewerColumn tvc = new TableViewerColumn(tv, 0);
			tvc.getColumn().setText(" ");
			tvc.getColumn().setWidth(25);
			TableViewerColumn tvc2 = new TableViewerColumn(tv, 0, 1);
			tvc2.getColumn().setText(" Resource ");
			tvc2.getColumn().setWidth(350);
			tv.setContentProvider(new ListContentProvider());
			tv.setLabelProvider(new ListLabelProvider());
			tv.setInput(liblist);
			for (TableItem ti : tv.getTable().getItems()) {
				if (ti.getData() instanceof String)
					ti.setChecked(true);
			}
		}
		setControl(composite);
	}

	public void buildLst(List<String> input) {
		liblist = new ArrayList<Object>();
		liblist.addAll(input);

		try {
			for (IResource r : ResourcesPlugin.getWorkspace().getRoot().members()) {
				if (r.isAccessible()) { //ajouter ceci évite la stacktrace lorsqu'on a un projet fermé dans son workspace
					buildLst(liblist, r);
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (tv != null) {
			tv.setInput(liblist);
			for (TableItem ti : tv.getTable().getItems()) {
				if (ti.getData() instanceof String)
					ti.setChecked(true);
			}

		}

	}

	private void buildLst(List<Object> lst, IResource r) {

		if (r instanceof IContainer) {
			try {
				for (IResource _r : ((IContainer) r).members()) {
					buildLst(liblist, _r);
				}
			} catch (CoreException e) {

				e.printStackTrace();
			}
			return;
		}
		if (r instanceof IFile) {
			if (extension.equals(r.getFileExtension()))
				liblist.add(r);
		}
	}

	public List<Object> getLiblist() {
		return liblist;
	}

	public TableViewer getTv() {
		return tv;
	}

	public List<Object> getSelection() {
		List<Object> lst = new ArrayList<Object>();
		if (multipleSelection) {
			for (TableItem ti : tv.getTable().getItems()) {
				if (ti.getChecked()) {
					lst.add(ti.getData());
				}
			}
		}
		else {
			for (TableItem ti : tv.getTable().getSelection()) {
				lst.add(ti.getData());
			}
		}
		return lst;
	}

}
