/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.wizards;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;



public class ExtendedCCSLNewWizard extends AbstractNewWizard implements INewWizard {

	public ExtendedCCSLNewWizard() {
		super();
		setTitle("New ExtendedCCSL File");
	}

	@Override
	protected InputStream openContentStream(String fileName, String imports) {
		String contents = readfile("template/template.extendedCCSL", null);
		String rootName = fileName.replace(".extendedCCSL", "");
		return new ByteArrayInputStream(updateTemplate(contents, rootName, imports).getBytes());

	}
	
	@Override
	protected AsbtractWizardPage createNewFileFirstWizardPage(IStructuredSelection selection) {
		return new ExtendedCCSLWizardPage(selection);
	}
	
	@Override
	protected ResourceSelectionWizardPage createImportSelectionWizardPage() {
		ResourceSelectionWizardPage wsl = new ResourceSelectionWizardPage("Library", "Imported CCSL Library", "ccslLib", true);
		List<String> input = new ArrayList<String>();
		input.add("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib");
		input.add("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib");
		wsl.buildLst(input);
		return wsl;
	}

	
	@Override
	protected StringBuffer generateImportStatements(IContainer container, List<Object> selectionlib) {
		StringBuffer imports = new StringBuffer(" // import statements\n");
		if (selectionlib == null) {
			return imports;
		}
		int i = 0;
		for (Object o : selectionlib) {
			if (o instanceof String) {
				imports.append("\t\timport \"" + o + "\" as lib" + i + "; \n");
			}
			if (o instanceof IFile) {
				IFile f = (IFile) o;

				IPath p = f.getFullPath().makeRelativeTo(container.getFullPath());
				String s = p.toString();
				imports.append("\t\timport \"" + s + "\" as lib" + i + "; \n");
			}
			i++;
		}
		return imports;
	}
	
}