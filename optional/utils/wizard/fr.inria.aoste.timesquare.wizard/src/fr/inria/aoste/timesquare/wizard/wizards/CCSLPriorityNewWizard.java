/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.wizards;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.IStructuredSelection;

public class CCSLPriorityNewWizard extends AbstractNewWizard {

	public CCSLPriorityNewWizard() {
		super();
		setTitle("New CCSLPriority File");
	}
	
	@Override
	protected AsbtractWizardPage createNewFileFirstWizardPage(IStructuredSelection selection) {
		return new CCSLPriorityNewWizardPage(selection);
	}
	
	@Override
	protected ResourceSelectionWizardPage createImportSelectionWizardPage() {
		ResourceSelectionWizardPage wsl = new ResourceSelectionWizardPage("CCSL Model", "Imported CCSL Model", "extendedCCSL", false);
		List<String> input = new ArrayList<String>();
		wsl.buildLst(input);
		return wsl;
	}

	@Override
	protected InputStream openContentStream(String fileName, String imports) {
		String contents = readfile("template/template.ccslPriority", null);
		return new ByteArrayInputStream(updateTemplate(contents, "", imports).getBytes());
	}

	@Override
	protected StringBuffer generateImportStatements(IContainer container, List<Object> selectionlib) {
		StringBuffer imports = new StringBuffer();
		for (Object o : selectionlib) {
			if (o instanceof String) {
				imports.append("\timport \"" + o + "\";\n");
			}
			if (o instanceof IFile) {
				IFile f = (IFile) o;
				IPath p = f.getFullPath().makeRelativeTo(container.getFullPath());
				String s = p.toString();
				imports.append("\timport \"" + s + "\";\n");
			}
		}
		return imports;
	}


}
