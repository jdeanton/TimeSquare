/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.content;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.intro.config.IIntroContentProvider;
import org.eclipse.ui.intro.config.IIntroContentProviderSite;
import org.eclipse.ui.intro.config.IIntroURL;
import org.eclipse.ui.intro.config.IntroURLFactory;
import org.osgi.framework.Bundle;

public class TimesquareViewer implements IIntroContentProvider {

	private static final String HREF_BULLET = "bullet"; //$NON-NLS-1$

	private IIntroContentProviderSite site;

	private boolean disposed;

	private String id;

	private List<NewsItem> items;

	private Composite parent;

	private FormToolkit toolkit;

	private FormText formText;

	private Image bulletImage;

	public TimesquareViewer() {
		super();
	}

	public void init(IIntroContentProviderSite site) {
		this.site = site;
		refresh();
	}

	public void createContent(String id, PrintWriter out) {
		if (disposed)
			return;
		this.id = id;

		if (items == null)
			createNewsItems();

		{
			if (items.size() > 0) {
				out.println("<ul id=\"news-feed\" class=\"news-list\">"); //$NON-NLS-1$
				for (int i = 0; i < items.size(); i++) {
					NewsItem item = (NewsItem) items.get(i);
					out.print("<li>"); //$NON-NLS-1$
					out.print("<a class=\"topicList\" href=\""); //$NON-NLS-1$
					out.print((item.url));

					out.print("\">"); //$NON-NLS-1$
					out.print(item.label);
					out.print("</a>"); //$NON-NLS-1$
					out.print("</li>\n"); //$NON-NLS-1$
				}
				out.println("</ul>"); //$NON-NLS-1$
			}

		}
	}

	public void createContent(String id, Composite parent, FormToolkit toolkit) {
		if (disposed)
			return;
		this.id = id;

		if (formText == null) {
			// a one-time pass
			formText = toolkit.createFormText(parent, true);
			formText.addHyperlinkListener(new HyperlinkAdapter() {
				public void linkActivated(HyperlinkEvent e) {
					doNavigate((String) e.getHref());
				}
			});
			bulletImage = createImage(new Path("icons/arrow.gif")); //$NON-NLS-1$
			if (bulletImage != null)
				formText.setImage(HREF_BULLET, bulletImage);
			this.parent = parent;
			this.toolkit = toolkit;
			this.id = id;

		}

		StringBuffer buffer = new StringBuffer();
		buffer.append("<form>"); //$NON-NLS-1$

		if (items == null)
			createNewsItems();

		{
			if (items.size() > 0) {
				for (int i = 0; i < items.size(); i++) {
					NewsItem item = (NewsItem) items.get(i);
					buffer.append("<li style=\"image\" value=\""); //$NON-NLS-1$
					buffer.append(HREF_BULLET);
					buffer.append("\">"); //$NON-NLS-1$
					buffer.append("<a href=\""); //$NON-NLS-1$
					buffer.append((item.url));
					buffer.append("\">"); //$NON-NLS-1$
					buffer.append(item.label);
					buffer.append("</a>"); //$NON-NLS-1$
					buffer.append("</li>"); //$NON-NLS-1$

				}
			} else {

				buffer.append("<p>"); //$NON-NLS-1$			
				buffer.append("</p>"); //$NON-NLS-1$
			}
		}

		buffer.append("</form>"); //$NON-NLS-1$

		String text = buffer.toString();
		text = text.replaceAll("&{1}", "&amp;"); //$NON-NLS-1$ //$NON-NLS-2$
		formText.setText(text, true, false);
	}

	public void dispose() {
		if (bulletImage != null) {
			bulletImage.dispose();
			bulletImage = null;
		}
		disposed = true;
	}

	private void createNewsItems() {
		items = Collections.synchronizedList(new ArrayList<NewsItem>());
		NewsItem item = new NewsItem();
		item.setLabel("New Extended CCSL");
		item.setUrl("http://org.eclipse.ui.intro/runAction?pluginId=fr.inria.aoste.timesquare.wizard&amp;class=fr.inria.aoste.timesquare.wizard.action.DemoAction&amp;paramwizard=ext");
		items.add(item);
		item = new NewsItem();
		item.setLabel("New CCSL lib");
		item.setUrl("http://org.eclipse.ui.intro/runAction?pluginId=fr.inria.aoste.timesquare.wizard&amp;class=fr.inria.aoste.timesquare.wizard.action.DemoAction&amp;paramwizard=lib");
		items.add(item);
	}

	/**
	 * Reflows the page using an UI thread.
	 */
	private void refresh() {
		Thread newsWorker = new Thread(new NewsFeed());
		newsWorker.start();
	}

	private Image createImage(IPath path) {
		Bundle bundle = Platform.getBundle("org.eclipse.ui.intro");
		URL url = FileLocator.find(bundle, path, null);
		try {
			url = FileLocator.toFileURL(url);
			ImageDescriptor desc = ImageDescriptor.createFromURL(url);
			return desc.createImage();
		} catch (IOException e) {
			return null;
		}
	}

	private void doNavigate(final String url) {
		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(), new Runnable() {
			public void run() {
				IIntroURL introUrl = IntroURLFactory.createIntroURL(url);
				if (introUrl != null) {
					// execute the action embedded in the IntroURL
					introUrl.execute();
					return;
				}
				// delegate to the browser support
				openBrowser(url);
			}
		});
	}

	private void openBrowser(String href) {
		try {
			URL url = new URL(href);
			IWorkbenchBrowserSupport support = PlatformUI.getWorkbench().getBrowserSupport();
			support.getExternalBrowser().openURL(url);
		} catch (PartInitException e) {
		} catch (MalformedURLException e) {
		}
	}

	public static class NewsItem {

		public NewsItem(String label, String url) {
			super();
			this.label = label;
			this.url = url;
		}

		public NewsItem() {
			super();
		}

		String label;

		String url;

		void setLabel(String label) {
			this.label = label;
		}

		void setUrl(String url) {
			this.url = url;
		}
	}

	class NewsFeed implements Runnable {
		public void run() {
			// important: don't do the work if the
			// part gets disposed in the process
			if (disposed)
				return;

			PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
				public void run() {
					if (parent != null) {
						// we must recreate the content
						// for SWT because we will use
						// a gentle incremental reflow.
						// HTML reflow will simply reload the page.
						createContent(id, parent, toolkit);
						// reflow(formText);
					}
					site.reflow(TimesquareViewer.this, true);
				}
			});
		}
	}

}
