/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA /I3S
 * 
 * Demo : Create Project
 */
package fr.inria.aoste.timesquare.wizard.demo;

import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

final class WorkspaceModificationCreateProject extends WorkspaceModifyOperation {
	/**
	 * 
	 */
	private final DemoWizard demoWizard;
	private final IProjectDescription description;

	protected WorkspaceModificationCreateProject(DemoWizard demoWizard, IProjectDescription description) {
		this.demoWizard = demoWizard;
		this.description = description;
	}

	@Override
	protected void execute(IProgressMonitor monitor)
			throws CoreException {
		demoWizard.createProject(description, this.demoWizard.newProjectHandle, monitor);
	}
}