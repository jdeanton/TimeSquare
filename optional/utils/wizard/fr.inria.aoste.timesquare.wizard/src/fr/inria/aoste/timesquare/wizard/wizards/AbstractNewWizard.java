/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.wizard.wizards;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.osgi.framework.Bundle;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.wizard.Activator;

/**
 * An abstract class that can be used to build "New File" wizard for various files used in Timesquare.
 * Subclasses must override these methods
 * <ul>
 *   <li>{@link #createNewFileFirstWizardPage(IStructuredSelection)} to create the first page of the wizard
 *   on which the name and parent folder of the file can be specified.
 *   <li>{@link #createImportSelectionWizardPage()} to create an optional second page to help the selection
 *   of imported resources inside the newly created file.
 *   <li>{@link #generateImportStatements(IContainer, List)} to generate the textual import statements
 *   with respect to the expected syntax of the created file.
 * @author bferrero, nchleq
 *
 */
public abstract class AbstractNewWizard extends Wizard implements INewWizard {
	protected AsbtractWizardPage firstPage;
	protected IStructuredSelection selection;
	protected String title;
	protected static DateFormat dateFormat = new SimpleDateFormat("E MMMMM d yyyy  hh:mm:ss z ");

	/**
	 * Constructor for ExtendedCCSLNewWizard.
	 */
	public AbstractNewWizard() {
		super();
		setNeedsProgressMonitor(true);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Adding the page to the wizard.
	 */

	@Override
	public final void addPages() {
		firstPage = createNewFileFirstWizardPage(selection);
		addPage(firstPage);
		importedResourceSelectionWizardPage = createImportSelectionWizardPage();
		if (importedResourceSelectionWizardPage != null) {
			addPage(importedResourceSelectionWizardPage);
		}
	}

	private ResourceSelectionWizardPage importedResourceSelectionWizardPage = null;
	
	protected abstract AsbtractWizardPage createNewFileFirstWizardPage(IStructuredSelection selection);
	
	/**
	 * To be defined in subclasses to return a suitable wizard page to select imports to add
	 * to the content of the new file.
	 * @return
	 */
	protected ResourceSelectionWizardPage createImportSelectionWizardPage() {
		return null;
	}


	/**
	 * This method is called when 'Finish' button is pressed in the wizard. We
	 * will create an operation and run it using wizard as execution context.
	 */
	public boolean performFinish() {
		final String containerName = firstPage.getContainerFullPath().toOSString();
		final String fileName = firstPage.getFileName();
		final List<Object> selectionlib = (importedResourceSelectionWizardPage != null ? importedResourceSelectionWizardPage.getSelection() : null);
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(containerName, fileName, selectionlib, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * The worker method. It will find the container, create the file if missing
	 * or just replace its contents, and open the editor on the newly created
	 * file.
	 */

	private void doFinish(String containerName, String fileName, List<Object> selectionlib, IProgressMonitor monitor)
			throws CoreException {
		// create a sample file
		monitor.beginTask("Creating " + fileName, 2);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(containerName));
		if (!resource.exists() || !(resource instanceof IContainer)) {
			throwCoreException("Container \"" + containerName + "\" does not exist.");
		}
		IContainer container = (IContainer) resource;
		final IFile file = container.getFile(new Path(fileName));
		try {
			StringBuffer imports = generateImportStatements(container, selectionlib);
			InputStream stream = openContentStream(fileName, imports.toString());
			if (file.exists()) {
				file.setContents(stream, true, true, monitor);
			} else {
				file.create(stream, true, monitor);
			}

			stream.close();
		} catch (IOException e) {
		}
		monitor.worked(1);
		monitor.setTaskName("Opening file for editing...");
		getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IDE.openEditor(page, file, true);
				} catch (PartInitException e) {
				}
			}
		});
		monitor.worked(1);
	}

	/**
	 * Generates the textual representation of import statements according to the syntax of the
	 * newly created file.
	 * @param container
	 * @param selectionlib
	 * @return
	 */
	protected abstract StringBuffer generateImportStatements(IContainer container, List<Object> selectionlib);

	/**
	 * We will initialize file contents with a sample text.
	 * @param string 
	 */

	protected abstract InputStream openContentStream(String fileName, String imports);

	public String updateTemplate(String contents, String rootName, String imports) {
		contents = contents.replace("%date%", dateFormat.format(new Date()));
		contents = contents.replace("%author%", System.getProperty("user.name"));
		contents = contents.replace("%rootname%", rootName);
		contents = contents.replace("%import%", imports);
		return contents;
	}

	private void throwCoreException(String message) throws CoreException {
		IStatus status = new Status(IStatus.ERROR, "fr.inria.aoste.timesquare.wizard", IStatus.OK, message, null);
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize
	 * from it.
	 * 
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// this.workbench = workbench;
		this.selection = selection;
		setWindowTitle(title);
		setDefaultPageImageDescriptor(null);
	}

	public IFile getFile() {
		return firstPage.getFile();
	}

	public String readfile(String s, Bundle bundle) {
		String content = "";
		BufferedReader buffer = null;
		try {

			ErrorConsole.println("" + s + " : ");
			if (bundle == null)
				bundle = Activator.getDefault().getBundle();
			URL url = PluginHelpers.getInstallUrl(bundle, s);
			InputStream is = url.openStream();
			InputStreamReader streamReader = new InputStreamReader(is);

			StringWriter writer = new StringWriter();
			buffer = new BufferedReader(streamReader);
			String line = "";
			while (null != (line = buffer.readLine())) {
				writer.write(line);
				writer.write("\n");
			}
			content = writer.toString();

		} catch (Throwable e) {
			ErrorConsole.printError(e, "Fail copy file :" + s);
		} finally {
			if (buffer != null) {
				try {
					buffer.close();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		}
		return content;
	}

}
