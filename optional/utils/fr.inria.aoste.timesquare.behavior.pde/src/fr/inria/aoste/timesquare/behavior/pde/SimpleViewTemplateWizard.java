/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.behavior.pde;

import org.eclipse.pde.ui.IFieldData;
import org.eclipse.pde.ui.templates.ITemplateSection;
import org.eclipse.pde.ui.templates.NewPluginTemplateWizard;


public class SimpleViewTemplateWizard extends NewPluginTemplateWizard {

	protected IFieldData fData;
	
    public void init(IFieldData data) {
        super.init(data);
        fData = data;
        setWindowTitle("Behavior");      
    }

    public ITemplateSection[] createTemplateSections() {
        return new ITemplateSection[] {new SimpleViewTemplateSection()};
    }

}
