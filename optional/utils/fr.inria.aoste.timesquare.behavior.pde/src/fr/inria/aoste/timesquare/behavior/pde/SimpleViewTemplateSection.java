/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.behavior.pde;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.JavaConventions;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.pde.core.plugin.IPluginBase;
import org.eclipse.pde.core.plugin.IPluginElement;
import org.eclipse.pde.core.plugin.IPluginExtension;
import org.eclipse.pde.core.plugin.IPluginModelBase;
import org.eclipse.pde.core.plugin.IPluginModelFactory;
import org.eclipse.pde.core.plugin.IPluginReference;
import org.eclipse.pde.ui.IFieldData;
import org.eclipse.pde.ui.templates.BooleanOption;
import org.eclipse.pde.ui.templates.OptionTemplateSection;
import org.eclipse.pde.ui.templates.PluginReference;
import org.eclipse.pde.ui.templates.StringOption;
import org.eclipse.pde.ui.templates.TemplateOption;

public class SimpleViewTemplateSection extends OptionTemplateSection {

	private static final String KEY_CLASS_NAME = "className";
	private static final String KEY_VIEW_NAME = "viewName";
	// private static final String KEY_MESSAGE_NAME = "message";
	private static final String KEY_REQUML = "requml";
	private static final String KEY_REQASSERT = "reqassert";
	private static final String KEY_REQRELATION = "reqrelation";
	private static final String KEY_UI = "ui";
	
	
	private static final String KEY_USECLOCK = "usesc";
	private static final String KEY_USECLOCK2 = "usescagain";
	private static final String KEY_USEASSERT = "usesa";
	private static final String KEY_USERELATION = "usesr";
	private String packageName = null;

	BooleanOption ui = null;
	
	BooleanOption ua= null;
	BooleanOption uc = null;
	BooleanOption uc1 = null;
	BooleanOption ur = null;
	StringOption pc = null;
	StringOption so = null;

	public SimpleViewTemplateSection() {
		super();
		setPageCount(1);
		createOptions();
	}

	private void createOptions() {
		pc=(StringOption) addOption(KEY_PACKAGE_NAME, "Package Name ", (String)null, 0);
		so = (StringOption) addOption(KEY_CLASS_NAME, "Class Base Name ", "Default", 0);
		addOption(KEY_VIEW_NAME, "Name", "MyLabel", 0);
		// addOption(KEY_MESSAGE_NAME, "Message", "information about ....", 0);
		
		addBlankField(0);

		uc = (BooleanOption) addOption(KEY_USECLOCK, "generate Clock Behavior", true, 0);		
		uc1 = (BooleanOption) addOption(KEY_USECLOCK2, "with  again Clock Behavior", false, 0);		
		ua = (BooleanOption) addOption(KEY_USEASSERT, "generate Assert Behavior", true, 0);
		ur = (BooleanOption) addOption(KEY_USERELATION, "generate relation Behavior", true, 0);

		addBlankField(0);

		addOption(KEY_REQUML, "Activation : Reference is Required", false, 0);
		addOption(KEY_REQRELATION, "Activation :relation is Required", false, 0);
		addOption(KEY_REQASSERT, "Activation : assertion is Required", false, 0);

		addBlankField(0);
		ui = (BooleanOption) addOption(KEY_UI, "Behavoir Dialox Box", false, 0);
		ui.setSelected(false);
		
		uc1.setEnabled(uc.isSelected());

	}

	public void initializeFields(IPluginModelBase model) {
		super.initializeFields(model);
		String pluginId = model.getPluginBase().getId();
		packageName = getFormattedPackageName(pluginId);// +".behavior";
		initializeOption(KEY_PACKAGE_NAME, packageName);
	}

	@Override
	public void addPages(Wizard wizard) {	
		WizardPage page = createPage(0, "org.eclipse.pde.doc.user.template_new_wizard");
		page.setTitle(" Behavior Template");
		page.setDescription("Creates a Behavior");
		wizard.addPage(page);
		markPagesAdded();
	}

	/**
	 * This is the folder relative to your install url and template directory
	 * where the templates are stored.
	 */
	@Override
	public String getSectionId() {
		return "viewtemplate";
	}

	@Override
	protected void initializeFields(IFieldData data) {
		if (data != null) {
			/*String id = data.getId();	
			
			initializeOption(KEY_PACKAGE_NAME, getFormattedPackageName(id));
			this.packageName = getFormattedPackageName(id);*/
		}
	}

	

	/**
	 * Validate your options here!
	 */
	@Override
	public void validateOptions(TemplateOption changed) {
		if( changed==uc)
		{
			uc1.setEnabled(uc.isSelected());
		}
		super.validateOptions(changed);
		if (changed == so) {
			String s = (String) so.getValue();

			boolean start = true;
			boolean validIdentifier = true;

			for (byte b : s.getBytes()) {
				if (start) {
					validIdentifier = validIdentifier && Character.isJavaIdentifierStart(b);
					start = false;
				} else {
					validIdentifier = validIdentifier && Character.isJavaIdentifierPart(b);
				}
			}

			if (!validIdentifier) {
				flagMissingRequiredOption(so);
				return;
			}
		}	
		IStatus val= JavaConventions.validatePackageName(pc.getText(),"1.5","1.5");
		if (val.getSeverity() == IStatus.ERROR) 
		{			
			flagMissingRequiredOption(pc);
			getPage(0).setPageComplete(false);			
			getPage(0).setErrorMessage("Invalid Package");
		} 
		else if (val.getSeverity() == IStatus.WARNING) {
			//flagMissingRequiredOption(pc);
		//	getPage(0).setPageComplete(false);			
			getPage(0).setMessage("Package", IMessageProvider.WARNING);
		}
		else
		{
			getPage(0).setMessage("", IMessageProvider.NONE);
		}
			
	}
	
	

	protected boolean isOkToCreateFolder(File sourceFolder) {
		if (sourceFolder.getName().equals(".svn"))
			return false;
		return super.isOkToCreateFolder(sourceFolder);
		// true;
	}

	@Override
	public String getTemplateDirectory() {
		// TODO Auto-generated method stub
		return "templates";
	}

	@Override
	protected void updateModel(IProgressMonitor monitor) throws CoreException {
		// ErrorConsole.println("start");
		try {
			IPluginBase plugin = model.getPluginBase();
			IPluginModelFactory factory = model.getPluginFactory();

			// org.eclipse.core.runtime.applications

			IPluginExtension extension = createExtension(Activator.BehaviorPEID, true);

			IPluginElement element = factory.createElement(extension);
			element.setName("behaviormanager");
			element.setAttribute("name", getStringOption(KEY_VIEW_NAME));
			String pack = getStringOption(KEY_PACKAGE_NAME);
			if (pack == null)
				pack = plugin.getId();
			String fullClassName = pack + "." + getStringOption(KEY_CLASS_NAME);
			element.setAttribute("BehaviorManager", fullClassName + "BehaviorManager");
			if (ui.isSelected())
				element.setAttribute("BehaviorManagerGUI", fullClassName + "BehaviorManagerGUI");
			extension.add(element);
			boolean b = false;
			for (IPluginExtension ipe : plugin.getExtensions()) {
				if (ipe.getPoint() != null)
					if (ipe.getPoint().equals(Activator.BehaviorPEID))
						b = true;
			}
			if (b == false)
				plugin.add(extension);
			
			JavaCore.create(project);
		
			b = false;

			refreshWorkspace();
			// MyManager.refreshWorkspace(); // TODO Test
		} catch (Throwable ex) {
			// ErrorConsole.printError(ex);
			System.out.println(ex);
		}

	}

	static public IWorkspaceRoot getWorkspaceRoot() {

		return ResourcesPlugin.getWorkspace().getRoot();

	}

	/***
	 * 
	 */
	public static void refreshWorkspace() {
		try {
			getWorkspaceRoot().refreshLocal(IResource.DEPTH_INFINITE, null);
			if (getWorkspaceRoot().getProject() != null)
				getWorkspaceRoot().getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (Throwable e) {
			System.out.println(e);
		}
	}

	/*public void copyfile(  )
	{
		try
		{
			//URL url = MyPluginManager.getInstallUrl(ActivatorTimeParser.getDefault().getBundle(), "/antlr-2.7.7.jar");
			//IPath ph = project.getLocation().append("").append("antlr-2.7.7.jar");			
			//MyPluginManager.copy(MyPluginManager.getIFileStore(url), MyPluginManager.getIFileStore(ph),EFS.OVERWRITE, null);
		} catch (Throwable e)
		{
			MyManager.printError(e, "echec file : antlr-2.7.7.jar");
		}
	}*/

	public String getUsedExtensionPoint() {
		return "fr.inria.ctrte.plugalloc.ConstraintWizard";
	}

	/**
	 * The location of your plugin supplying the template content
	 */
	@Override
	protected URL getInstallURL() {
		try {
			URL url = Activator.getDefault().getBundle().getEntry("/");
			url = FileLocator.resolve(FileLocator.toFileURL(url));
			String ls = url.toString();
			String nls = ls.replaceAll(" ", "%20");
			if (nls.compareTo(ls) != 0) {
				url = new URL(nls);
			}
			return url;
		} catch (Throwable ex) {

		}
		return null;
	}

	@Override
	protected ResourceBundle getPluginResourceBundle() {
		return Platform.getResourceBundle(Activator.getDefault().getBundle());
	}

	/**
	 * You can use this method to add files relative to your section id
	 */
	public String[] getNewFiles() {
		return new String[0];
	}

	@Override
	public IPluginReference[] getDependencies(String schemaVersion) {
		try {
			ArrayList<PluginReference> result = new ArrayList<PluginReference>();

			result.add(new PluginReference("org.eclipse.ui.views", null, 0)); //$NON-NLS-1$
			result.add(new PluginReference("org.eclipse.core.runtime", null, 0)); //$NON-NLS-1$      
			result.add(new PluginReference("org.eclipse.ui", null, 0)); //$NON-NLS-1$

			result.add(new PluginReference("fr.inria.aoste.trace", null, 0)); //$NON-NLS-1$        
			result.add(new PluginReference("fr.inria.aoste.timesquare.trace.util", null, 0)); //$NON-NLS-1$
			result.add(new PluginReference("fr.inria.aoste.timesquare.instantrelation.trace.relation", null, 0)); //$NON-NLS-1$
			result.add(new PluginReference("fr.inria.aoste.timesquare.launcher.core", null, 0)); //$NON-NLS-1$
			result.add(new PluginReference("fr.inria.aoste.timesquare.backend.manager", null, 0)); //$NON-NLS-1$

			return result.toArray(new IPluginReference[result.size()]);
		} catch (Throwable ex) {
			System.err.println("Import Plugin Fail");
		}
		return new IPluginReference[] {};
	}

	@Override
	public boolean isDependentOnParentWizard() {
		return true;
	}

	@Override
	public int getNumberOfWorkUnits() {
		return super.getNumberOfWorkUnits() + 1;
	}

	@Override
	public String getStringOption(String name) {
		/*if (name.equals(KEY_PACKAGE_NAME)) {
			//if (packageName==null)
				//return "noname";
			return packageName;//+".behavior";
		}*/
		return super.getStringOption(name);
	}

	protected String getFormattedPackageName(String id) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < id.length(); i++) {
			char ch = id.charAt(i);
			if (buffer.length() == 0) {
				if (Character.isJavaIdentifierStart(ch))
					buffer.append(Character.toLowerCase(ch));
			} else {
				if (Character.isJavaIdentifierPart(ch) || ch == '.')
					buffer.append(ch);
			}
		}
		String s = buffer.toString().toLowerCase(Locale.ENGLISH);
		// if (s==null)
		// return "test" ;
		return s;
	}

	@Override
	protected boolean isOkToCreateFile(File sourceFile) {
		String name = sourceFile.getName();
		if ("$className$BehaviorManagerGUI.java".equals(name)) {
			return ui.isSelected();
		}
		if (name.startsWith("Clock"))
		{
			return uc.isSelected();
		}
		if (name.startsWith("Assert"))
		{
			return ua.isSelected();
		}
		if (name.startsWith("Relation"))
		{
			return ur.isSelected();
		}
		return super.isOkToCreateFile(sourceFile);
	}

	@Override
	public String getDescription() {
		return "Creates a Behavior";
	}

	@Override
	public String getLabel() {
		return "Behavior";
	}

}
