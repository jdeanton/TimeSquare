/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package $packageName$;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;


public class $className$BehaviorManagerGUI extends BehaviorManagerGUI {

	

	public $className$BehaviorManagerGUI()
	{
		
	}
	
	private Composite container;

	
	
	 /**
	  * @wbp.parser.entryPoint
	  *   wbp.parser.entryPoint ==> "Windows Builder" with "SWT designer"
	  */
	@Override
	public Control createDialogArea(Composite composite) {
		container = new Composite(composite, SWT.RESIZE);
		container.setLayout(new FormLayout());	
		
			
		return container;
	}

	@Override
	public void okPressed() {
		
	}

	@Override
	public void cancelPressed() {
		

	}

	@Override
	protected $className$BehaviorManager getBehaviorManager() {		
		return ( $className$BehaviorManager) super.getBehaviorManager();
	}
	
	

}
