/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package $packageName$;



import org.eclipse.core.runtime.IPath;


%if usesa
import $packageName$.persitentOption.Assert$className$PersistentOptions;
%endif
%if usesc
import $packageName$.persitentOption.Clock$className$PersistentOptions;
%endif
%if usesr
import $packageName$.persitentOption.Relation$className$PersistentOptions;
%endif
%if requml
import org.eclipse.emf.ecore.EcorePackage;
%endif
import $packageName$.persitentOption.$className$PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.AssertBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportError;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;






public class $className$BehaviorManager extends BehaviorManager {

	public $className$BehaviorManager() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getPluginName() {
		
		return "$viewName$";
	}
	

	@Override
	public boolean isActivable(ReportMessage rm, CCSLInfo ccslhelper) {

% if requml
		if (!ccslhelper.haveClockWithReferenceTo(EcorePackage.eINSTANCE.getEObject())) //TODO
		{
			rm.setMessage("0 Clock have reference to ... "); //TODO
			return false;
		}
% endif 

% if reqassert
		if (ccslhelper.getListofAssert().size()==0)
		{
			rm.setMessage("0 assert inthe model ");
			return false;
		}
% endif 

% if reqrelation
		if (ccslhelper.getListofRelation().size()==0)
		{
			rm.setMessage("0 relation inthe model ");
			return false;
		}
% endif 
		
		return true;
	}
	
	
	@Override
	public boolean isValide(ReportError re) {
		
		return true;
	}


	@Override
	public void init(ConfigurationHelper helper) {
		// TODO Auto-generated method stub

	}

	@Override
	public void manageBehavior(ConfigurationHelper helper) {
		// TODO Auto-generated method stub

	}

	@Override
	public ClockBehavior redoClockBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		// TODO Auto-generated method stub
% if usesc	
		if ( persistentOptions instanceof  Clock$className$PersistentOptions) {
			ClockBehavior cb= null;//TODO complet ... 
			return cb;
		}	
% endif
		return null;
	}

	@Override
	public RelationBehavior redoRelationBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		// TODO Auto-generated method stub
%if usesr
		if ( persistentOptions instanceof  Relation$className$PersistentOptions) {
			RelationBehavior cb= null;//TODO complet ...
			return cb;
			
		}
% endif
		return null;
		
	}

	@Override
	public AssertBehavior redoAssertBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		// TODO Auto-generated method stub
%if usesa
		if ( persistentOptions instanceof  Assert$className$PersistentOptions){
			AssertBehavior cb= null;//TODO complet ..... 
			return cb;
			
		}
% endif
		return null;
	}



	@Override
	public void receivePluginOptions(ConfigurationHelper helper, PersistentOptions persistentOptions) throws Throwable {
			if (persistentOptions instanceof $className$PersistentOptions) {
				//TODO complet ...
			}		

	}
	
	
	@Override
	public PersistentOptions getPluginOptions() {
		//TODO complet ...
		return null;
	}

	@Override
	public void beforeExecution(ConfigurationHelper helper, IPath folderin, String namefilein) {
		//TODO complet ...

	}

	@Override
	public void end(ConfigurationHelper helper) {
		//TODO complet ...

	}

	@Override
	public void aNewStep(int step) {
		//TODO complet ...

	}

	@Override
	public $className$Configuration getConfigurator(ConfigurationHelper configurationHelper) {
		return new $className$Configuration(configurationHelper, this);
	}
	
	

}
