/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package $packageName$.persitentOption;

import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;


public class Relation$className$PersistentOptions implements PersistentOptions
{
	// TODO generate serialVersionUID

   
	public Relation$className$PersistentOptions() {
		super();
	}

    
    public String getDescription()
    {
        return "give descprition";
    }

	

    

}
