/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package $packageName$.behavior;


/**
 * @author ${user}$
 */
import fr.inria.aoste.timesquare.backend.manager.visible.AssertBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;

public class Assert$className$Behavior implements AssertBehavior {

	
	public Assert$className$Behavior()
	{
		
	}
	
	@Override
	public String getDescription() {
		
		return "";
	}

	@Override
	public boolean behaviorEquals(Behavior behavior) {
		if( this==behavior)
			return true;
		if( behavior.getClass()==Assert$className$Behavior.class)
		{
			//TODO complet ...
		}
		return false;
	}


	@Override
	public void run(TraceHelper helper) {
	
		
	}
	

}
