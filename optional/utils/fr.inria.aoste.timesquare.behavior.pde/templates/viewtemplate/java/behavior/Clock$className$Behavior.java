/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package $packageName$.behavior;



import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;


% if usescagain
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehaviorAgain;
%else
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
	
%endif
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;

/**
 * @author ${user}$
 */

% if usescagain
public class Clock$className$Behavior  implements	ClockBehaviorAgain
%else
public class Clock$className$Behavior  implements	ClockBehavior	
%endif

{
	

	public Clock$className$Behavior()
	{
		
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public boolean behaviorEquals(Behavior behavior) {
		// TODO Auto-generated method stub
		if( this==behavior)
			return true;
		if( behavior.getClass()==Clock$className$Behavior.class)
		{
			//TODO complet ...
		}
		return false;
	}

	@Override
	public void run(TraceHelper helper) {
		//TODO complet ...
		
	}

	@Override
	public void runWithWrongActivationState(TraceHelper helper) {
		//TODO complet ...
		
	}
	
% if usescagain
	@Override
	public void againRun( TraceHelper helper ) 	{
	//TODO complet ...
	}
	    
	@Override
	public void againRunWithWrongActivationState( TraceHelper helper ) {
		//TODO complet ...
	}
%endif 
	
	

}