/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model2xtext.popup.actions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.xtext.XtextFactory;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.resource.SaveOptions.Builder;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import fr.inria.aoste.timesquare.ccslkernel.parser.xtext.*;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;

public class modelToText implements IObjectActionDelegate {

	private Shell shell;
	private IFile ccslmodel=null;
	/**
	 * Constructor for Action1.
	 */
	public modelToText() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		ExtendedCCSLStandaloneSetup ess= new ExtendedCCSLStandaloneSetup();
		Injector injector = ess.createInjector();
		XtextResourceSet set = injector.getInstance(XtextResourceSet.class);
		
		// instanciate a resource set
	    XtextResourceSet aset = injector.getInstance(XtextResourceSet.class);
		aset.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		EcoreUtil.resolveAll(aset);
		
		ExtendedCCSLStandaloneSetup.doSetup();

		    // create a uri
		    String uristring = ccslmodel.getLocation().toOSString();
		    URI uri = URI.createFileURI(uristring);
		    Resource res = aset.getResource(uri, true);
		   
		    HashMap<Object, Object> saveOptions = new HashMap<Object, Object>();
		    Builder aBuilder = SaveOptions.newBuilder();
		    SaveOptions anOption = aBuilder.getOptions();
		    anOption.addTo(saveOptions);
		    try {
				res.load(saveOptions);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			Resource newRes=null;
		    // res.getContents() contains the model
			if (ccslmodel.getFileExtension().equals("TimeModel") 
					|| ccslmodel.getFileExtension().equals("timemodel"))
			    newRes =  set.createResource(URI.createFileURI(uristring.replace("."+ccslmodel.getFileExtension(), "")+".extendedCCSL"));
			else if (ccslmodel.getFileExtension().equals("ccslLibrary"))
				newRes =  set.createResource(URI.createFileURI(uristring.replace("."+ccslmodel.getFileExtension(), "")+".ccslLib"));	    		else
	    			System.out.println("This file extension is not supported... (Choose an .ccslmodel or a .ccslLibrary file");
			

		    newRes.getContents().addAll(res.getContents());
		    try {
				newRes.save(Collections.emptyMap());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		
		
		
		
		
		/*
		
		    ExtendedCCSLStandaloneSetup.doSetup();
		    ResourceSet resourceSet = new ResourceSetImpl();
		    URI uri = URI.createFileURI(ccslmodel.getLocation().toOSString());
		    Resource resource = resourceSet.getResource(uri, true);
		    ClockConstraintSystem model = (ClockConstraintSystem) resource.getContents().get(0);

		   // ClockConstraintSystem entity = CCSLModelFactory.eINSTANCE.createClockConstraintSystem();
		   // entity.setName("Customer");

		   // model.getElements().add(entity);
		    HashMap saveOptions = new HashMap();
		    Builder aBuilder = SaveOptions.newBuilder();
		    SaveOptions anOption = aBuilder.getOptions();
		    anOption.addTo(saveOptions);
		    XtextResource textResource = new XtextResource();
//		    textResource.attached(model);
		    URI resUri = URI.createFileURI(ccslmodel.getLocation().toOSString()+".extendedCCSL");
		    textResource.setURI(resUri);

		 //   HashMap options = new HashMap();
		    saveOptions.put(XtextResource.OPTION_ENCODING, "UTF-8"); 
		    try {
				//System.out.println(textResource.getSerializer().serialize(model));
				textResource.save(saveOptions);//URI.createFileURI(ccslmodel.getLocation().toOSString())),options);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	//	    textResource.getContents().add(model);
		    try {
				textResource.doSave(new FileOutputStream(ccslmodel.getLocation().toOSString()+".extendedCCSL"), saveOptions);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
*/
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	
	public void selectionChanged(IAction action, ISelection selection) {
		ccslmodel=null;
		if (selection instanceof StructuredSelection) {
			StructuredSelection ss = (StructuredSelection) selection;
			Object o = ss.getFirstElement();

			if (o instanceof IFile) {
				ccslmodel = (IFile) o;
				
			}
		}
		
	}

}
