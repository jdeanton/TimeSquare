/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.checker.ui.popup.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import toools.io.file.RegularFile;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;

public class ConstrainExecutionModelWithTrace extends AbstractHandler {
	

	private IFile ccslFile;
	private String ccslFilePath;
	private IFile traceFile;
	private String traceFilePath;

	
	/**
	 * Constructor for Action1.
	 */
	public ConstrainExecutionModelWithTrace() {
		super();
	}
	private String[] observedClockNames;
	private Map<String,Clock> clockNameToClock;


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selections = HandlerUtil.getCurrentSelection(event);
		if (selections instanceof IStructuredSelection) {
			if (((IStructuredSelection) selections).size() == 2) {
				for(Object selected: ((IStructuredSelection) selections).toList()){
					if (selected instanceof IFile) {
						if (((IFile)selected).getFileExtension().compareTo("extendedCCSL") == 0
							||
							((IFile)selected).getFileExtension().compareTo("timemodel") == 0){
								ccslFile = (IFile) selected;
								ccslFilePath = ccslFile.getLocation().toString();
								ResultsHolder.originalCCSLFilePath = "platform:/resource"+ccslFile.getFullPath();
						}
						if (((IFile)selected).getFileExtension().compareTo("trace") == 0
								||
								((IFile)selected).getFileExtension().compareTo("txt") == 0){
									traceFile = (IFile) selected;
									traceFilePath = traceFile.getLocation().toString();
							}
					}
				}
			}
		}
		Job job = new Job("TimeSquare Trace checker on "+ccslFile.toString()) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {	
				return doIt(monitor);
			}
		};
		job.schedule();
		return null;
	}
//	Job job = new Job("TimeSquare TraceChecker on "+traceFile.toString()) {
//	@Override
//	protected IStatus run(IProgressMonitor monitor) {
	protected IStatus doIt(IProgressMonitor monitor) {
		
		
		
		Resource ccslTraceResource = handleCreationOfCCSLFromTrace();
		monitor.worked(10);
		
		traceFile = null;
		traceFilePath="";
		ccslFile=null;
		ccslFilePath="";
		
		monitor.worked(100);
		return Status.OK_STATUS;
     }
	
	
	

public Resource handleCreationOfCCSLFromTrace() {
	Resource ccslResource = null;
	try {
		ccslResource = ResourceLoader.INSTANCE.loadResource(ccslFile.getFullPath());
	} catch (IOException e) {
		System.err.println("load ccsl file proble on "+ccslFilePath+ "\nexception:");
		e.printStackTrace();
	}
	
	Resource ccslTraceResource = null;
	try {
		ccslTraceResource = createCCSLFromTrace(traceFile,ccslResource);
	} catch (IOException e2) {
		e2.printStackTrace();
	}
			
	
	try {
		traceFile.refreshLocal(1, null);
	} catch (CoreException e2) {
		e2.printStackTrace();
	}
	return ccslTraceResource;
}



//
//	private void printPathTrace(final Path p) {
//		int lastV = p.getSource();
//		int time = 0;
//		int currentV = -1;
//		for(int i = 1; i < p.getLength(); i++){
//			currentV = p.getVertexAt(i);
//			IntSet allConnectingEdges = null;
////			try{
//				allConnectingEdges = stateSpace.getGrph().getEdgesConnecting(lastV, currentV);
////			}catch(Exception e){
////				RegularFile grphFile = new RegularFile(ccslFilePath+".txt");
////				try {
////					grphFile.append(stateSpace.getGrph().toGrphText().getBytes());
////				} catch (IOException ie) {
////					ie.printStackTrace();
////				}
////				System.out.println(p.toString());
////				throw new RuntimeException("Grph().getEdgesConnecting("+lastV+", "+currentV+") returned an empty set !\n"
////						+ "is the path applicale ? "+ p.isApplicable(stateSpace.getGrph())+"\ncause initiale:"+e);
////			}
////			if(allConnectingEdges.size() == 0){
////				RegularFile grphFile = new RegularFile(ccslFilePath+".txt");
////				try {
////					grphFile.append(stateSpace.getGrph().toGrphText().getBytes());
////				} catch (IOException e) {
////					e.printStackTrace();
////				}
////				System.out.println(p.toString());
////				throw new RuntimeException("Grph().getEdgesConnecting("+lastV+", "+currentV+") returned an empty set !\n"
////						+ "is the path applicale ? "+ p.isApplicable(stateSpace.getGrph()));
////			}
//			if(allConnectingEdges.size()>1){
//				System.out.println("(");
//			}
//			int anEdgeWithTime = -1;
//			for(IntCursor ic : allConnectingEdges){
//				if (stateSpace.i2e(ic.value).toString().contains("_ms")){
//					anEdgeWithTime = ic.value;
//				}
//				break;
//			}
//			if(anEdgeWithTime != -1){
//				time++;
//				System.out.println(time+";"+stateSpace.i2e(anEdgeWithTime).toString());
//			}else{
//				System.out.println(time+";"+stateSpace.i2e(allConnectingEdges.toIntArray()[0]).toString());
//			}
//			if(allConnectingEdges.size()>1){
//				System.out.println(")");
//			}
//			lastV = currentV;
//		}
//		System.out.println("---------------------------------\n"+maxPath.toString()+"\n---------------------------------");
//		
//	}
	
	
	
	private Resource createCCSLFromTrace(IFile traceFile, Resource ccslResource) throws IOException {
		ArrayList<String> traceLines = new ArrayList<String>(getTraceLines());
		
		ArrayList<Clock> allCCSLClocks = getAllCCSLClocks(ccslResource);
		
		observedClockNames = getObservedClockNames(traceLines);
		ResultsHolder.observedClockNames = observedClockNames;
				
		clockNameToClock = computeClockMatchings(allCCSLClocks, observedClockNames);
		ResultsHolder.clockNameToClock = clockNameToClock;
		
		String traceSpecificConstraintDefinitionPath="";
		try {
			traceSpecificConstraintDefinitionPath = createConstraintSpecificMoCCMLDefinition(traceLines, observedClockNames);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String resultingCCSLFilePath="";
		try {
			resultingCCSLFilePath = createCCSLFile(clockNameToClock,observedClockNames,traceSpecificConstraintDefinitionPath, allCCSLClocks);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ResourceLoader.INSTANCE.loadResource(resultingCCSLFilePath);
	}

	private String createCCSLFile(Map<String, Clock> clockNameToClock,String[] observedClockNames, String traceSpecificConstraintDefinitionPath, ArrayList<Clock> allCCSLClocks) throws IOException {
		RegularFile traceCCSLFile = new RegularFile(ccslFile.getParent().getLocation().toString()+"/SpecAugmentedWithTrace.extendedCCSL");
		if(traceCCSLFile.exists()){
			traceCCSLFile.delete();
		}
			
		createCCSLHeader(traceCCSLFile, traceSpecificConstraintDefinitionPath);
		
		ResultsHolder.physicalClockName = "";
		for(Clock c : allCCSLClocks){
			if (c.getName().endsWith("_ms")){
				ResultsHolder.physicalClockName = c.getName();
				break;
			}
		}
		createCCSLTraceRelation(traceCCSLFile, clockNameToClock,observedClockNames, ResultsHolder.physicalClockName);
		
		return ccslFile.getParent().getFullPath().toString()+"/SpecAugmentedWithTrace.extendedCCSL";
	}
	private void createCCSLTraceRelation(RegularFile traceCCSLFile, Map<String, Clock> clockNameToClock,String[] observedClockNames, String PhysicalClockName) throws IOException{
		traceCCSLFile.append(("\t Relation theUltimateRelation[TraceSpecificConstraint](  TraceSpecificConstraint_phyClock->"+PhysicalClockName+"\n").getBytes());
		for(String observedClockName : observedClockNames ){
			traceCCSLFile.append(("\t\t ,TraceSpecificConstraint_"+observedClockName+"-> "+clockNameToClock.get(observedClockName).getName()+" \n").getBytes());
		}
		traceCCSLFile.append("\t)\n".getBytes());
		traceCCSLFile.append("\t}\n".getBytes());
		traceCCSLFile.append("}\n".getBytes());
	}
	private void createCCSLHeader(RegularFile traceCCSLFile, String traceSpecificConstraintDefinitionPath) throws IOException {
		traceCCSLFile.append("/*\n".getBytes());
		traceCCSLFile.append("* CCSL specification\n".getBytes());
		traceCCSLFile.append(" * @author:  the trace checker written bu Julien Deantoni\n".getBytes());
		traceCCSLFile.append(" * date :  Thu June 9 2016  10:51:42 CEST \n".getBytes());
		traceCCSLFile.append(" */\n".getBytes());
		traceCCSLFile.append("ClockConstraintSystem traceSpecification {\n".getBytes());
		traceCCSLFile.append("    imports {\n".getBytes());
		traceCCSLFile.append("        // import statements\n".getBytes());
		traceCCSLFile.append("		import \"platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib\" as lib0;\n".getBytes()); 
		traceCCSLFile.append("		import \"platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib\" as lib1; \n".getBytes());
		traceCCSLFile.append(("		import \""+traceSpecificConstraintDefinitionPath+"\" as TSC;\n").getBytes());
		traceCCSLFile.append(("		import \"platform:/resource"+ccslFile.getFullPath().toString()+"\" as theSpec;\n").getBytes());
		traceCCSLFile.append("    }\n".getBytes());
		traceCCSLFile.append("    entryBlock main\n".getBytes());
		traceCCSLFile.append("     \n".getBytes());
		traceCCSLFile.append("        Block main {\n".getBytes());
	}
	private String createConstraintSpecificMoCCMLDefinition (List<String> traceLines, String[] observedClockNames) throws IOException{
		RegularFile constraintDefFile = new RegularFile(ccslFile.getParent().getLocation().toString()+"/TraceSpecificConstraint.moccml");
		
		if(constraintDefFile.exists()){
			constraintDefFile.delete();
		}
	
		createConstraintDefHeader(constraintDefFile);
		createConstraintDefVariables(constraintDefFile, traceLines);
		createConstraintDefStates(constraintDefFile, traceLines);
		createConstraintDefFooter(constraintDefFile, observedClockNames);
	
		constraintDefFile.create();
		return "platform:/resource/"+ccslFile.getParent().getFullPath().toString()+"/TraceSpecificConstraint.moccml";
	}

	private void createConstraintDefFooter(RegularFile constraintDefFile, String[] observedClockNames) throws IOException{
		constraintDefFile.append("\n".getBytes());
		constraintDefFile.append("RelationDeclaration	TraceSpecificConstraint(TraceSpecificConstraint_phyClock:clock".getBytes());
		for(String aClock : observedClockNames){
			constraintDefFile.append((",TraceSpecificConstraint_"+aClock+":clock").getBytes());
		}
		constraintDefFile.append(")\n".getBytes());
		constraintDefFile.append("	           }\n".getBytes());
		constraintDefFile.append("}\n".getBytes());
	}
	
	private void createConstraintDefStates(RegularFile constraintDefFile,List<String> traceLines) throws IOException{
		constraintDefFile.append("	           init:  initialState\n".getBytes());
		constraintDefFile.append("	           State initialState(out: s0s1)\n".getBytes());
		constraintDefFile.append("	             from initialState to s1 : s0s1 -> ()\n".getBytes());
		
		int i = 1;
		for(String line : traceLines){
			constraintDefFile.append(("	           State s"+i+"(\n").getBytes());
			constraintDefFile.append(("	           		in: s"+i+"s"+i+", s"+(i-1)+"s"+i).getBytes());
			if (i!=1){
				constraintDefFile.append((", s"+(i-1)+"s"+i+"bis, s"+(i-1)+"s"+i+"ter\n").getBytes());				
			}else{
				constraintDefFile.append(("\n").getBytes());
			}
			constraintDefFile.append(("					out : s"+i+"s"+i+", s"+i+"s"+(i+1)+", s"+i+"s"+(i+1)+"bis, s"+i+"s"+(i+1)+"ter\n").getBytes());
			constraintDefFile.append(("			   )\n").getBytes());
			constraintDefFile.append(("				from s"+i+" to s"+i+" : s"+i+"s"+i+" -> ( if (currentTime < timeStamp"+i+") when TraceSpecificConstraint_phyClock do currentTime = (currentTime + un))\n").getBytes());
			String[] splittedLine = line.split(";");
			if (splittedLine.length> 2){
				String allClocks = "TraceSpecificConstraint_"+splittedLine[1];
				for (int j = 2; j < splittedLine.length; j++){
					allClocks+=", TraceSpecificConstraint_"+splittedLine[j];
				}
				constraintDefFile.append(("				from s"+i+" to s"+(i+1)+" : s"+i+"s"+(i+1)+" -> ( if (currentTime == timeStamp"+i+") when "+allClocks+")\n").getBytes());
				constraintDefFile.append(("				from s"+i+" to s"+(i+1)+" : s"+i+"s"+(i+1)+"bis -> ( if (currentTime == (timeStamp"+i+" - un)) when "+allClocks+", TraceSpecificConstraint_phyClock do currentTime = (currentTime + un))\n").getBytes());
				constraintDefFile.append(("				from s"+i+" to s"+(i+1)+" : s"+i+"s"+(i+1)+"ter -> ( if (currentTime > timeStamp"+i+") when TraceSpecificConstraint_phyClock do kill(TraceSpecificConstraint_phyClock))\n").getBytes());
			}else{
				constraintDefFile.append(("				from s"+i+" to s"+(i+1)+" : s"+i+"s"+(i+1)+" -> ( if (currentTime == timeStamp"+i+") when TraceSpecificConstraint_"+line.split(";")[1]+")\n").getBytes());
				constraintDefFile.append(("				from s"+i+" to s"+(i+1)+" : s"+i+"s"+(i+1)+"bis -> ( if (currentTime == (timeStamp"+i+" - un)) when TraceSpecificConstraint_"+line.split(";")[1]+" , TraceSpecificConstraint_phyClock do currentTime = (currentTime + un))\n").getBytes());
				constraintDefFile.append(("				from s"+i+" to s"+(i+1)+" : s"+i+"s"+(i+1)+"ter -> ( if (currentTime > timeStamp"+i+") when TraceSpecificConstraint_phyClock do kill(TraceSpecificConstraint_phyClock))\n").getBytes());

			}
			i++;
		}
		constraintDefFile.append(("	           State s"+i+"(\n").getBytes());
		constraintDefFile.append(("	           		in: s"+(i-1)+"s"+i+", s"+(i-1)+"s"+i+"bis,s"+(i-1)+"s"+i+"ter\n").getBytes());
//		constraintDefFile.append(("					out: s"+i+"s"+(i+1)+"\n").getBytes());
		constraintDefFile.append(("			   )\n").getBytes());
//		constraintDefFile.append(("				from s"+i+" to s"+(i+1)+" : s"+i+"s"+(i+1)+" -> ( if (currentTime > un) ").getBytes());
//		for(String aClock : observedClockNames){
//			constraintDefFile.append(("                             do kill(TraceSpecificConstraint_"+aClock+")\n").getBytes());
//		} 	
//		constraintDefFile.append(("						)\n").getBytes());
//
//	             constraintDefFile.append(("	           State s"+(i+1)+"(\n                            in: s"+i+"s"+(i+1)).getBytes());
	     		constraintDefFile.append(("			   \n}\n").getBytes());
		
	}
	private void createConstraintDefVariables(RegularFile constraintDefFile, List<String> traceLines) throws IOException {
		constraintDefFile.append("			variables { \n".getBytes());
		constraintDefFile.append(" 				Integer un = 1\n".getBytes());
		constraintDefFile.append(" 				Integer currentTime = 0\n".getBytes());
		int i = 1;
		for(String line : traceLines){
			constraintDefFile.append((" 				Integer timeStamp"+(i++)+" = "+line.substring(0,line.indexOf(';'))+"\n").getBytes());
		}
		constraintDefFile.append(" 			}\n".getBytes());
	}

	private void createConstraintDefHeader(RegularFile constraintDefFile) throws IOException {
		constraintDefFile.append("AutomataConstraintLibrary temporalConstraints{\n".getBytes());
		constraintDefFile.append("   import 'platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib' as kernel;\n".getBytes());
		constraintDefFile.append("\n".getBytes());
		constraintDefFile.append("   RelationLibrary temporalRelations{\n".getBytes());
		constraintDefFile.append("\n".getBytes());
		constraintDefFile.append("      AutomataRelationDefinition TraceSpecificConstraintDef[TraceSpecificConstraint]{\n".getBytes());
		constraintDefFile.append("\n".getBytes());
	}

	private Map<String,Clock> computeClockMatchings(ArrayList<Clock> allCCSLClocks, String[] observedClockNames) {
		Map<String,Clock> clockNameToClock = new HashMap<String,Clock>(observedClockNames.length); 
		for(String clockName : observedClockNames){
			Clock matchingClock = lookForClockName(clockName,allCCSLClocks);
			if (matchingClock != null){
				clockNameToClock.put(clockName, matchingClock);
			}
		}
		return clockNameToClock;
	}

	private List<String> getTraceLines() {
		RegularFile theTraceFile = new RegularFile(traceFilePath);
		List<String> traceLines =null;
		traceLines = theTraceFile.getLines();
		ResultsHolder.maxTraceTime = new Integer(traceLines.get(traceLines.size()-1).split(";")[0]);
		return traceLines;
	}

	private ArrayList<Clock> getAllCCSLClocks(Resource ccslResource) {
		EList<Element> allCCSLElements = ((ClockConstraintSystem)ccslResource.getContents().get(0)).getSuperBlock().getElements();
		ArrayList<Clock> allCCSLClocks = new ArrayList<Clock>();
		for(Element e : allCCSLElements){
			if (e instanceof Clock){
				allCCSLClocks.add((Clock)e);
			}
		}
		return allCCSLClocks;
	}

	private String[] getObservedClockNames(List<String> traceLines) {
		String observedClocksLine = traceLines.get(0);
		traceLines.remove(0); //removes the observed clock
		traceLines.remove(0); //removes the -------------
		String[] observedClockNames = observedClocksLine.split(";");
		ArrayList<String> tempRes = new ArrayList<String>();
		for(int i = 0 ; i < observedClockNames.length; i++){
			if (observedClockNames[i].compareTo("\r") != 0){
				tempRes.add(observedClockNames[i]);
			}
		}
		String[] res = new String[tempRes.size()];
		int index = 0;
		for (String s : tempRes){
			res[index++] = s;
		}
		return res;
	}

	private Clock lookForClockName(String clockName,ArrayList<Clock> allCCSLClocks) {
		for(Clock c : allCCSLClocks){
			String[] splittedName = clockName.split("_");
			String beginOfName = splittedName[0]; //example Task2
			String endOfName = splittedName[splittedName.length-1]; //example STARTED
			System.out.println(beginOfName+"_"+endOfName);
			
			if(c.getName().contains(beginOfName)
					&&
			   c.getName().contains(endOfName)){
				return c;
			}
		}
		return null;
	}
	
	
//	
//	};
//	return job;
//	}
	
}
