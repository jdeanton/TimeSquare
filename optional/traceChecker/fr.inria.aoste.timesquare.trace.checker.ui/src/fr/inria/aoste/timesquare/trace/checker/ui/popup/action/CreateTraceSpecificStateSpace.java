package fr.inria.aoste.timesquare.trace.checker.ui.popup.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import toools.collections.primitive.IntCursor;
import toools.io.file.RegularFile;

import fr.inria.aoste.timesquare.ccslkernel.explorer.CCSLConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.explorer.CCSLKernelExplorer;
import fr.inria.aoste.timesquare.ccslkernel.explorer.StateSpace;
import fr.inria.aoste.timesquare.ccslkernel.explorer.UniqueString;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import grph.GrphWebNotifications;
import grph.path.Path;
import it.unimi.dsi.fastutil.ints.IntSet;

public class CreateTraceSpecificStateSpace extends AbstractHandler {

	private IFile ccslFile;
	private CCSLKernelExplorer clockExplorer;
	private String ccslFilePath;
	private StateSpace stateSpace = null;
	
	private int maxPathLength = 0;
	/**
	 * Constructor for Action1.
	 */
	public CreateTraceSpecificStateSpace() {
		super();
	}

	

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			if (((IStructuredSelection) selection).size() == 1) {
				Object selected = ((IStructuredSelection) selection).getFirstElement();
				if (selected instanceof IFile) {
					ccslFile = (IFile) selected;
					ccslFilePath = ccslFile.getLocation().toString();
				}
			}
		}
		
		Job job = new Job("TimeSquare Trace Specific State Space Exploration on "+ccslFile.toString()) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
		
				Resource ccslResource = null;
				try {
					ccslResource = ResourceLoader.INSTANCE.loadResource(ccslFile.getFullPath());
				} catch (IOException e) {
					System.err.println("load ccsl file problem on "+ccslFilePath+ "\nexception:");
					e.printStackTrace();
				}
				
				checkStateSpaceOfSpecAugmentedWithTrace(monitor, ccslResource);
				highlightLongerPathInTrace();
				
				createMaxPathCCSLSpecification();
				
				cleanAndSerializeStateSpace();
				
				
				return Status.OK_STATUS;
			}

			private void createMaxPathCCSLSpecification() {
				PathCCSLSpecificationGenerator generator = new PathCCSLSpecificationGenerator(stateSpace, ccslFile);	
				try {
					generator.createCCSLHeader();
					generator.createCCSLTraceRelation();
					
					generator.createConstraintSpecificMoCCMLDefinition();
					
					try {
						ccslFile.refreshLocal(1, null);
					} catch (CoreException e2) {
						e2.printStackTrace();
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			private void cleanAndSerializeStateSpace() {
				for(StringBuffer s : stateSpace.getEdges()){
					if (!isContainingAnObservedClock(s.toString())){
						s.delete(0, s.length());
						stateSpace.getGrph().getEdgeLabelProperty().setValue(stateSpace.e2i(s), "");
					}else{
						//here it contains an observed clock
						StringBuilder newLabel = new StringBuilder();
						for(String clockName : s.toString().split(",")){
							if(isContainingAnObservedClock(clockName)){
								newLabel.append(clockName);
							}
						}
						s.delete(0, s.length());
						s.append(newLabel);
						stateSpace.getGrph().getEdgeLabelProperty().setValue(stateSpace.e2i(s), newLabel.toString());
					}
				}
				
				RegularFile dotRegFile = new RegularFile(ccslFilePath+"_cleanedUp.dot"); 
				dotRegFile.setContentAsASCII(stateSpace.getGrph().toDot());
				
				dotRegFile.create();
				
			}
		};
		
		job.schedule();
		return null;
	}
	
	
	public void checkStateSpaceOfSpecAugmentedWithTrace(IProgressMonitor monitor,
			Resource ccslTraceResource) {
		clockExplorer = new CCSLKernelExplorer(monitor);
		try {
			
			clockExplorer.loadModel(ccslTraceResource);
		} catch (IOException | UnfoldingException | SimulationException e) {
			e.printStackTrace();
		}
		
		try {
			clockExplorer.initSimulation();
		} catch (SimulationException e1) {
			e1.printStackTrace();
		}
			
		
		stateSpace = null;
		
		try {
			stateSpace = clockExplorer.explore(false); //set true to create the dot file at each step (near the eclipse executable)
		} catch (SimulationException e) {
			e.printStackTrace();
		}
		
		clockExplorer = null;
	}
	
	public void highlightLongerPathInTrace() {
		for(Integer v : stateSpace.getGrph().getVertices()){ //changes the state labels
			stateSpace.getGrph().getVertexLabelProperty().setValue(v, String.valueOf(v));
		}
	
		ArrayList<CCSLConstraintState> allPathToHighlight = new ArrayList<CCSLConstraintState>();
		for (StringBuffer label : stateSpace.getEdges()) {
       		if (isContainingAnObservedClock(label.toString())){
       			if(label.toString().contains("DEAD")){
       				stateSpace.changeEdgeColorAndBig(label, 7);
       			}else{
       				stateSpace.changeEdgeColor(label, 12);
       			}
	       		allPathToHighlight.add(stateSpace.getDirectedSimpleEdgeHead(label));
	       		//allPathToHighlight.add(stateSpace.getDirectedSimpleEdgeTail(label));
       		}
		}

		
		
		for(int i = 0; i < allPathToHighlight.size() ; i++){
       			final Path p = stateSpace.getShortestPath(stateSpace.i2v(0), allPathToHighlight.get(i));
       			if (p != null){
       				computeLonger(p);
       			}
   		}
		
		changePathVerticesColor(ResultsHolder.maxPath);

		
		if(ResultsHolder.maxPath != null){
			int maxSimulatedTime = computeTime(ResultsHolder.maxPath);
			if (maxSimulatedTime == ResultsHolder.maxTraceTime){
				System.out.println("#########################################\n\t\t The trace conforms the specification\n \t\t\tsimulated until "+ResultsHolder.maxTraceTime+"\n###################################################\n");
			}else{
				System.out.println("#########################################\n\t\t Trace simulated until time "+(maxSimulatedTime+1)+" on "+ResultsHolder.maxTraceTime+"\n no way to conforms the trace on step that follows time "+(maxSimulatedTime+1)+"###################################################\n");
				ResultsHolder.maxPath.setColor(stateSpace.getGrph(), 3);
			}
			
		}
		RegularFile dotRegFile = new RegularFile(ccslFilePath+".dot"); 
		dotRegFile.setContentAsASCII(stateSpace.getGrph().toDot());
		
		dotRegFile.create();
	}
	
	
//private void changeSinkColor() {
//	for(IntCursor ic : stateSpace.getGrph().getSinks()){
//		stateSpace.changeVertexColor(stateSpace.i2v(ic.value), 2);
//		for (UniqueString label : stateSpace.getIncidentEdges(stateSpace.i2v(ic.value))){
//			for(String anObservedClock : ResultsHolder.observedClockNames){
//	       		if (label.toString().contains(ResultsHolder.clockNameToClock.get(anObservedClock).getName())){
//		       		stateSpace.changeEdgeColor(label, 8);
//	       		}
//	   		}
//		}
//	}
//}
	
	private void changePathVerticesColor(final Path p) {
		if(! p.isApplicable(stateSpace.getGrph())){
			return;
		}
		int currentV = -1;
		for(int i = 0; i <= p.getLength(); i++){
			currentV = p.getVertexAt(i);
			stateSpace.changeVertexColor(stateSpace.i2v(currentV), 10);
		}
		return;
	}
	
	private int computeTime(final Path p) {
		if(! p.isApplicable(stateSpace.getGrph())){
			return -1;
		}
		int lastV = p.getSource();
		int maxSimulatedTime = 0;
		int currentV = -1;
		for(int i = 1; i <= p.getLength(); i++){
			currentV = p.getVertexAt(i);
			IntSet allConnectingEdges = null;
			try{
				allConnectingEdges = stateSpace.getGrph().getEdgesConnecting(lastV, currentV);
			}catch(Exception e){
				RegularFile grphFile = new RegularFile(ccslFilePath+".txt");
				grphFile.append(stateSpace.getGrph().toGrphText().getBytes());
				System.out.println(p.toString());
				throw new RuntimeException("Grph().getEdgesConnecting("+lastV+", "+currentV+") returned an empty set !\n"
						+ "is the path applicale ? "+ p.isApplicable(stateSpace.getGrph())+"\ncause initiale:"+e);
			}
			if(allConnectingEdges.size() == 0){
				RegularFile grphFile = new RegularFile(ccslFilePath+".txt");
				grphFile.append(stateSpace.getGrph().toGrphText().getBytes());
				System.out.println(p.toString());
				throw new RuntimeException("Grph().getEdgesConnecting("+lastV+", "+currentV+") returned an empty set !\n"
						+ "is the path applicale ? "+ p.isApplicable(stateSpace.getGrph()));
			}
			int anEdgeWithTime = -1;
			for(Integer ic : allConnectingEdges){
				if (stateSpace.i2e(ic).toString().contains("_ms")){
					anEdgeWithTime = ic;
				}
				break;
			}
			if(anEdgeWithTime != -1){
				maxSimulatedTime++;
			}
			lastV = currentV;
		}
		return maxSimulatedTime;
	}
	
	
	private void computeLonger(final Path p) {
	//	System.out.println(p.whyNotApplicable(stateSpace.getGrph()));
		if(! p.isApplicable(stateSpace.getGrph())){
//			System.out.println("Path is not applicable: "+p);
			return ;
		}else{
//			System.out.println("--> Path is applicable");
		}
		int lastV = p.getSource();
		int tempMaxPathLength = 0;
		int currentV = -1;
		for(int i = 1; i < p.getLength(); i++){
			currentV = p.getVertexAt(i);
			IntSet allConnectingEdges = stateSpace.getGrph().getEdgesConnecting(lastV, currentV);
			int anEdgeWithObservedClock = -1;
			for(Integer ic : allConnectingEdges){
				if (isContainingAnObservedClock(stateSpace.i2e(ic).toString())){
					anEdgeWithObservedClock = ic;
				}
				break;
			}
			if(anEdgeWithObservedClock != -1){
				tempMaxPathLength++;
			}
			lastV = currentV;
		}
		if (tempMaxPathLength > maxPathLength  ){
			maxPathLength = tempMaxPathLength;
			ResultsHolder.maxPath = p;
		}
	}
	
	
	private boolean isContainingAnObservedClock(String label) {
		for(String ocn : ResultsHolder.observedClockNames){
			if (label.contains(ResultsHolder.clockNameToClock.get(ocn).getName())){
				return true;
			}
		}
		return false;
	}
	
	
	
	
	
	
	
	
	

	private String cleanLabel(UniqueString l1) {
		String sl1 = l1.toString();
		sl1 = sl1.substring(1);
		sl1 = sl1.substring(0, sl1.length()-1);
		sl1 = sl1.replaceAll(",", "");
		sl1 = sl1.trim();
		return sl1;
	}

	
}
