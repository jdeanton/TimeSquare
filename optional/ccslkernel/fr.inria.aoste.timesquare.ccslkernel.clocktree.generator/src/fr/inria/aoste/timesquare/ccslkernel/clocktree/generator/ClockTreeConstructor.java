/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.clocktree.generator;

import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
import grph.oo.ObjectGrph;

public interface ClockTreeConstructor {

	public abstract UnfoldModel getUnfoldModel();

	public abstract void addCoincidence(InstantiatedElement c1,
			InstantiatedElement c2, boolean isCond);

	public abstract void addPrecedence(InstantiatedElement leftc,
			InstantiatedElement rightc, boolean isCond);

	public abstract void addCausality(InstantiatedElement leftc,
			InstantiatedElement rightc, boolean isCond);

	public abstract void addConditional(InstantiatedElement leftc,
			InstantiatedElement rightc);

	public abstract void addExcludes(InstantiatedElement leftc,
			InstantiatedElement rightc, boolean isCond);

	public abstract void addSubClockFree(InstantiatedElement hyperc,
			InstantiatedElement subc, boolean isCond);

	public abstract void addSubClockConstrained(InstantiatedElement hyperc,
			String k_affine, InstantiatedElement subc, boolean isCond);

	public abstract void addSampledOnExpression(
			InstantiatedElement samplingClock,
			InstantiatedElement sampledClock, InstantiatedElement res, boolean isCond);

	public abstract void addNonStrictSampledOnExpression(
			InstantiatedElement samplingClock,
			InstantiatedElement sampledClock, InstantiatedElement res, boolean isCond);

	public abstract void addKiller(InstantiatedElement killer,
			InstantiatedElement res, boolean isCond);

	public abstract void addInfRelation(InstantiatedElement c1,
			InstantiatedElement c2, InstantiatedElement cres, boolean isCond);
	
	public abstract void addSupRelation(InstantiatedElement c1,
			InstantiatedElement c2, InstantiatedElement cres, boolean isCond);

	public abstract void addConcat(InstantiatedElement left,
			InstantiatedElement right, InstantiatedElement concat, boolean isCond);

	public abstract ObjectGrph<CoincidentClocks, Edge> createClockGrphDag(
			String string);

}