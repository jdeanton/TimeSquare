/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.clocktree.generator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;

public class CoincidentClocks extends ArrayList<InstantiatedElement> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8298231586333190464L;
	final private int hashCode = System.identityHashCode(this);
	private VertexKind kind = VertexKind.ClockSet;

	@Override
	public boolean equals(Object o) {
		if (! (o instanceof CoincidentClocks)){
			return false;
		}
		return hashCode == ((CoincidentClocks) o).hashCode;
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}
	@Override
	public boolean add(InstantiatedElement e) {
		if (this.contains(e)){
			return false;
		}
		else return super.add(e);
	}
	
	@Override
	public boolean addAll(Collection<? extends InstantiatedElement> lce) {
		for(InstantiatedElement ce:lce){
			this.add(ce);
		}
		return true;
	}
	public VertexKind getKind() {
		return kind;
	}
	public void setKind(VertexKind kind) {
		this.kind = kind;
	}

	public String getVertexLabel(){
		if (this.kind == VertexKind.ImplicitSuperClock){
			return "ImplicitSuperClock";
		}
		StringBuilder res = new StringBuilder("");
		Iterator<InstantiatedElement> it = this.iterator();
		while (it.hasNext()) {
			InstantiatedElement c = it.next();
			if (c == null){
				continue;
			}
			if (c.isClock()) {
				res.append(c.getInstantiationPath().getLast().getName());
			}
			else if (c.isExpression()) {
				if (c.isTopLevel()) {
					res.append(c.getInstantiationPath().getLast().getName());
				}
				else {
					res.append("IC");
					res.append(c.getId());
				}
			}
			else {
				res.append(c.getQualifiedName("::"));
			}
			if (it.hasNext()) {
				res.append("==");
			}
		}
		return res.toString();
	}
	
	public String getLongVertexLabel(){
		if (this.kind == VertexKind.ImplicitSuperClock){
			return "ImplicitSuperClock";
		}
		StringBuilder res = new StringBuilder("");
		Iterator<InstantiatedElement> it = this.iterator();
		while (it.hasNext()) {
			InstantiatedElement c = it.next();
			if(c == null){
				continue;
			}
			res.append(c.getQualifiedName("::"));
			if (it.hasNext()) {
				res.append("==");
			}
		}
		return res.toString();
	}
	
	public Color getEdgeColor(Display d){
		switch (kind) {
		case ClockSet:
			return new Color(d, 0, 0, 255);
		case ChoiceVertex:
			return new Color(d, 0, 255, 0);
		case ImplicitSuperClock:
			return new Color(d, 255, 0, 255);
		default:
			return new Color(d, 0, 0, 0);
		}
	}
	
	public boolean containsByInstantiationPathName(String instanciationPathName) {
		Iterator<InstantiatedElement> it = this.iterator();
		while (it.hasNext()) {
			InstantiatedElement ie = it.next();
			if (ie.getQualifiedName().compareTo(instanciationPathName)==0)
			{
				return true;
			}
		}
		return false;
	}
//	@Override
//	public String getLabel() {
//		return getVertexLabel();
//	}
	
}
