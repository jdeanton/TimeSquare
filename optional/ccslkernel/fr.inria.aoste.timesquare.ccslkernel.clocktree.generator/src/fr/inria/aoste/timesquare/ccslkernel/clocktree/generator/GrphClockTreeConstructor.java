/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.clocktree.generator;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
import grph.oo.ObjectGrph;


public class GrphClockTreeConstructor implements ClockTreeConstructor {
	
	private UnfoldModel unfoldModel;

	private ObjectGrph<CoincidentClocks, Edge> g = new ObjectGrph<CoincidentClocks, Edge>();

	

	
	public GrphClockTreeConstructor() {
		//to enforce gc eligibility
		System.gc();
		
		try{
			ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);

		}catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
	public UnfoldModel getUnfoldModel(){
		return unfoldModel;
	}
	
	public ObjectGrph<CoincidentClocks, Edge> createClockGrphDag(Resource ccslResource){
		try {
			unfoldModel = UnfoldModel.unfoldModel(ccslResource);
		} 
		catch (Throwable throwable)
		{
			String message= throwable.getMessage();
			if (message==null )
					message=throwable.getClass().getName()+" has throw !!";
			message ="During solver.loadModel(URI) : "+ message ;
			throw new RuntimeException(message, throwable);
		}
		
		//get all the concrete
		List<InstantiatedElement> concretes = unfoldModel.getInstantiationTree().lookupInstances(null);
		//iterate on them
		for(InstantiatedElement concrete : concretes){
			if (concrete.isKernelRelation() && concrete.getDefinition().getName() == "Coincides") {
				TEST();
				addCorrespondingVertexAndEdge(concrete);
				TEST();
			}
		}
		
		for(InstantiatedElement concrete : concretes){
			if (! concrete.isKernelRelation() || concrete.getDefinition().getName() != "Coincides") {
				TEST();
				addCorrespondingVertexAndEdge(concrete);
				TEST();
			}
		}
		
		for(Edge e : g.getEdges()) {
			if (e.ek == EdgeKind.COINCIDES) {
				TEST();
				CoincidentClocks source = g.getDirectedSimpleEdgeTail(e);
				CoincidentClocks target = g.getDirectedSimpleEdgeHead(e);
				
				source.addAll(target);
				target.addAll(source);
				TEST();
			}
		}
		
		
		return g;
	}
	
	public ObjectGrph<CoincidentClocks, Edge> createClockGrphDag(String ccslSpecificationPath){
		try {
			unfoldModel = UnfoldModel.unfoldModel(URI.createURI(ccslSpecificationPath));
		} 
		catch (Throwable throwable)
		{
			String message= throwable.getMessage();
			if (message==null )
					message=throwable.getClass().getName()+" has throw !!";
			message ="During UnfoldModel.unfoldModel(URI) : "+ message ;
			throw new RuntimeException(message, throwable);
		}
		
		List<InstantiatedElement> concretes = unfoldModel.getInstantiationTree().lookupInstances(null);
		//iterate on them
		for (InstantiatedElement concrete : concretes) {
			TEST();
			addCorrespondingVertexAndEdge(concrete);
			TEST();
		}
		
		return g;
	}

	private void addCorrespondingVertexAndEdge(InstantiatedElement concrete) {
		if (concrete.isExpression()) {
			if (concrete.isKernelExpression() /*&& ! concrete.isConditionCase()*/) {
				TEST();
				KernelExpressionDeclaration declaration = (KernelExpressionDeclaration) concrete.getDeclaration();
				KernelExpressionClockTreeBuilder builder = new KernelExpressionClockTreeBuilder(concrete, this);
				builder.doSwitch(declaration);
				TEST();
			}
			else 
			if (concrete.isConditional() && ! concrete.isTailRecursiveCall()) {
				//if ( ! concrete.isRecursiveCall()) {
					for(InstantiatedElement c_ie: concrete.getSons()){
						if (c_ie.isConditionCase()) {
							TEST();
							addConditional(concrete, c_ie);
							TEST();
						}
					}
//				}else{
//					throw new RuntimeException("GrphClockTreeConstructor, recursive conditional relations not supported");
//				}
			}
			else {
			assert(concrete.getDefinition() != null);
			if (concrete.getDefinition() instanceof UserExpressionDefinition && ! concrete.isRecursiveCall() )
				{
					TEST();
					InstantiatedElement rootExprElement = concrete.getRootExpression();
					addCoincidence(rootExprElement, concrete, false);
//					for(InstantiatedElement ie : concrete.getSons()){
//						addCorrespondingVertexAndEdge(ie);
//					}
					TEST();
				}
			}
		}
		else if (concrete.isRelation()) {
			if (concrete.isKernelRelation()) {
				TEST();
				KernelRelationDeclaration declaration = (KernelRelationDeclaration) concrete.getDeclaration();
				KernelRelationClockTreeBuilder builder = new KernelRelationClockTreeBuilder(concrete, this);
				builder.doSwitch(declaration);
				TEST();
			}
			else if (concrete.isConditional() ) {
				//if ( ! concrete.isRecursiveCall()) {
//					for(InstantiatedElement ie:unfoldModel.getInstantiationTree().g(concrete.getInstantiationPath().toString())){
//						//if (ie.getDeclaration()!= concrete.getDeclaration()){
////							addCorrespondingVertexAndEdge(ie);
//						//}
//						addConditional(concrete, ie);
//					}
//				}
			}
		}
	}

	private CoincidentClocks addOrCreateListOf(InstantiatedElement elt) {
		CoincidentClocks l_concrete = getListOf(elt);
		if (l_concrete == null) {
			l_concrete = new CoincidentClocks();
			l_concrete.add(elt);
		}
		return l_concrete;
	}

	private CoincidentClocks getListOf(InstantiatedElement concrete) {
		for (CoincidentClocks l_concrete : g.getVertices()) {
			if (l_concrete.contains(concrete)) {
				return l_concrete;
			}
		}
		return null;
	}
	
	public void addCoincidence(InstantiatedElement c1, InstantiatedElement c2, boolean isCond){
		TEST();
		
		//TODO: how to represent a conditional coincidence ?
		int nbVerticesBefore = g.getVertices().size();
		int nbEdgesBefore = g.getEdges().size();
//		Edge e= new Edge();
//		e.ek=EdgeKind.COINCIDES;
		assert(c1 != null);
		assert(c2 != null);
//		assert(c1 instanceof Clock);
//		assert(c2 instanceof Clock);
		CoincidentClocks l_c1 = addOrCreateListOf(c1);
		CoincidentClocks l_c2 = addOrCreateListOf(c2);
		//if they do not exist in the graph
		if (!g.getVertices().contains(l_c1) && !g.getVertices().contains(l_c2)){
			l_c2.addAll(l_c1); 
			g.addVertex(l_c2);
			return;
		}
		
		if (!g.getVertices().contains(l_c1) && g.getVertices().contains(l_c2)){
			l_c2.addAll(l_c1); 
			return;
		}
		
		if (g.getVertices().contains(l_c1) && !g.getVertices().contains(l_c2)){
			l_c1.addAll(l_c2); 
			return;
		}
		
		TEST();
		
		//if there are no neighbors, we merge
		if ((! g.hasIncidentEdges(l_c1))
				&&
			(! g.hasIncidentEdges(l_c2))){
			//choose the vertex with special decoration

			if (l_c1.getKind() != VertexKind.ClockSet){
				l_c1.addAll(l_c2); 
				g.removeVertex(l_c2);
			}else{
				l_c2.addAll(l_c1);
				g.removeVertex(l_c1);
			}
			
			TEST();		
	
		
			
			int nbEdgesAfter = g.getEdges().size();
			assert(nbEdgesBefore == nbEdgesAfter);
			return;
			
		}
		
//		if ((g.hasIncidentEdges(l_c1))
//				&&
//			(! g.hasIncidentEdges(l_c2))){
//			//choose the vertex with special decoration
//			l_c1.addAll(l_c2); 
//			g.removeVertex(l_c2);
//			int nbEdgesAfter = g.getEdges().size();
//			assert(nbEdgesBefore == nbEdgesAfter);
//			return;
//			
//		}
//		
//		if ((!g.hasIncidentEdges(l_c1))
//				&&
//			(g.hasIncidentEdges(l_c2))){
//			//choose the vertex with special decoration
//			l_c2.addAll(l_c1); 
//			g.removeVertex(l_c1);
//			int nbEdgesAfter = g.getEdges().size();
//			assert(nbEdgesBefore == nbEdgesAfter);
//			return;
//			
//		}
//		
//		//here they both have neighbors
//		
//		if ((l_c1.getKind() != VertexKind.ClockSet)){
//			//we merge the lists and we reconnect the edge of l_c1 to lc2 and 
//			Set<Edge> edges = g.getIncidentEdges(l_c2);
//			for(Edge e : edges){
//					//it is directed
//					CoincidentClocks dest = g.getDirectedSimpleEdgeHead(e);
//					CoincidentClocks source=g.getDirectedSimpleEdgeTail(e);
//					
//					if (l_c2==dest){//the edge is an input edge
//						g.removeEdge(e);
//						g.addSimpleEdge(source, e, l_c1, true);
//					}else{
//						//it is an output edge
//						g.removeEdge(e);
//						g.addSimpleEdge(l_c1, e, dest, true);
//					}
//			}
//			l_c1.addAll(l_c2);
//			g.removeVertex(l_c2);
//
//		}else{
//		
//			//we merge the lists and we reconnect the edge of l_c1 to lc2 and 
//			Set<Edge> edges = g.getIncidentEdges(l_c1);
//			for(Edge e : edges){
//					//it is directed
//					CoincidentClocks dest = g.getDirectedSimpleEdgeHead(e);
//					CoincidentClocks source=g.getDirectedSimpleEdgeTail(e);
//				
//					if (dest == null || l_c1==dest){//the edge is an input edge
//						g.removeEdge(e);
//						g.addSimpleEdge(source, e, l_c2, true);
//					}else{
//						//it is an output edge
//						g.removeEdge(e);
//						g.addSimpleEdge(l_c2, e, dest, true);
//					}
//				
//			}
//			l_c2.addAll(l_c1);
//			g.removeVertex(l_c1);
//			
//			}
		
		TEST();
		Edge e= new Edge(EdgeKind.COINCIDES,isCond);
		
		l_c1.addAll(l_c2);
		l_c2.addAll(l_c1);
				
		if(! g.containsVertex(l_c1)){
			g.addVertex(l_c2);
		}
		if(! g.containsVertex(l_c2)){
			g.addVertex(l_c2);
		}
		TEST();
		g.addSimpleEdge(l_c1, e, l_c2, false);
		TEST();
//		int nbEdgesAfter = g.getEdges().size();
//		assert(nbEdgesBefore == nbEdgesAfter);
//		int nbVerticesAfter = g.getVertices().size();
//		assert(nbVerticesAfter == nbVerticesBefore-1);
		
		return;
	}

	private void TEST() {
		//TEST !! TODO WARNING
		
		for(Edge edge : g.getEdges()) {
			if (edge.isPriorityPropagator())
			{
				CoincidentClocks ccS = g.getDirectedSimpleEdgeTail(edge);
				if (ccS == null) {
					System.out.println("PROBLEM !");
				}
			}
		}
	}
	
//	
//	public void addCoincidence(InstantiatedElement c1, InstantiatedElement c2, boolean isCond){
//
//		
//		Edge e= new Edge(EdgeKind.COINCIDES,isCond);
//		
//		CoincidentClocks l_c1 = addOrCreateListOf(c1);
//		CoincidentClocks l_c2 = addOrCreateListOf(c2);
//		
//		l_c1.add(c2);
//		l_c2.add(c1);
//		
//		
//		
//		if(! g.containsVertex(l_c1)){
//			l_c1.addAll(l_c2);
//		}
//		if(! g.containsVertex(l_c2)){
//			g.addVertex(l_c2);
//		}
//		g.addSimpleEdge(l_c1, e, l_c2, false);
//		return;
//	}
//	


	public void addPrecedence(InstantiatedElement leftc, InstantiatedElement rightc, boolean isCond){
		TEST();
		Edge e= new Edge(EdgeKind.PRECEDES,isCond);
		
		CoincidentClocks l_leftc = addOrCreateListOf(leftc);
		CoincidentClocks l_rightc = addOrCreateListOf(rightc);
		if(! g.containsVertex(l_leftc)){
			g.addVertex(l_leftc);
		}
		if(! g.containsVertex(l_rightc)){
			g.addVertex(l_rightc);
		}
		g.addSimpleEdge(l_leftc, e, l_rightc, true);
		TEST();
		return;
	}
	
public void addCausality(InstantiatedElement leftc, InstantiatedElement rightc, boolean isCond){
	TEST();	
	Edge e= new Edge(EdgeKind.CAUSES,isCond);
		
		CoincidentClocks l_leftc = addOrCreateListOf(leftc);
		CoincidentClocks l_rightc = addOrCreateListOf(rightc);
		if(! g.containsVertex(l_leftc)){
			g.addVertex(l_leftc);
		}
		if(! g.containsVertex(l_rightc)){
			g.addVertex(l_rightc);
		}
		g.addSimpleEdge(l_leftc, e, l_rightc, true);

		TEST();
		return;
	}

public void addExcludes(InstantiatedElement leftc, InstantiatedElement rightc, boolean isCond){
	TEST();
	Edge e= new Edge(EdgeKind.EXCLUDES,isCond);
	
	CoincidentClocks l_leftc = addOrCreateListOf(leftc);
	CoincidentClocks l_rightc = addOrCreateListOf(rightc);
	if(! g.containsVertex(l_leftc)){
		g.addVertex(l_leftc);
	}
	if(! g.containsVertex(l_rightc)){
		g.addVertex(l_rightc);
	}
	g.addSimpleEdge(l_leftc, e, l_rightc, true);
	TEST();
	return;
}

	
	public void addSubClockFree(InstantiatedElement hyperc, InstantiatedElement subc, boolean isCond){
		
		TEST();
		int nbAvant = g.getVertices().size();
		int nbEAvant = g.getEdges().size();
		Edge e= new Edge(EdgeKind.SUBCLOCKFREE,isCond);
		Edge e2 = new Edge(EdgeKind.CAUSES , isCond);

		CoincidentClocks l_hyperc = addOrCreateListOf(hyperc);
		CoincidentClocks l_subc = addOrCreateListOf(subc);
		System.out.println(nbAvant);
		if(! g.containsVertex(l_hyperc)){
			System.out.print(g.addVertex(l_hyperc));
			System.out.println("   add 1");
		}
		if(! g.containsVertex(l_subc)){
			System.out.print(g.addVertex(l_subc));
			System.out.println("   add 2");
		}
		int nbApres = g.getVertices().size();
		int nbEApres = g.getEdges().size();
		System.out.println(nbApres);
		TEST();
		g.addSimpleEdge(l_hyperc, e, l_subc, true);
		g.addSimpleEdge(l_hyperc, e2, l_subc, true);
		TEST();
		return;
	}
	
	public void addSubClockConstrained(InstantiatedElement hyperc, String k_affine, InstantiatedElement subc, boolean isCond){
		TEST();
		Edge e= new Edge(EdgeKind.SUBCLOCKCONSTRAINED ,isCond);
		e.k_affineFunction = k_affine;
		Edge e2 = new Edge(EdgeKind.CAUSES,isCond);
		
		CoincidentClocks l_hyperc = addOrCreateListOf(hyperc);
		CoincidentClocks l_subc = addOrCreateListOf(subc);
		if(! g.containsVertex(l_hyperc)){
			g.addVertex(l_hyperc);
		}
		if(! g.containsVertex(l_subc)){
			g.addVertex(l_subc);
		}
		g.addSimpleEdge(l_hyperc, e2, l_subc, true);
		g.addSimpleEdge(l_hyperc, e, l_subc, true);
		TEST();
		return;
	}
	
	public void addKiller(InstantiatedElement killer, InstantiatedElement res, boolean isCond){
		TEST();
		Edge e= new Edge(EdgeKind.KILL ,isCond);
		
		CoincidentClocks l_killer = addOrCreateListOf(killer);
		CoincidentClocks l_res = addOrCreateListOf(res);
		if(! g.containsVertex(l_killer)){
			g.addVertex(l_killer);
		}
		if(! g.containsVertex(l_res)){
			g.addVertex(l_res);
		}
		g.addSimpleEdge(l_killer, e, l_res, true);
		TEST();
		return;
	}
	
	public void addInfRelation(InstantiatedElement c1, InstantiatedElement c2, InstantiatedElement cres, boolean isCond){
		TEST();
		Edge e1= new Edge(EdgeKind.SUBCLOCKFREE,isCond);
		Edge e2= new Edge(EdgeKind.SUBCLOCKFREE,isCond);
		Edge e3= new Edge(EdgeKind.SUBCLOCKFREE,isCond);
		Edge e1cause= new Edge(EdgeKind.CAUSES,isCond);
		Edge e2cause= new Edge(EdgeKind.CAUSES,isCond);

		
		CoincidentClocks l_c1 = addOrCreateListOf(c1);
		CoincidentClocks l_c2 = addOrCreateListOf(c2);
		CoincidentClocks l_cres = addOrCreateListOf(cres);
		CoincidentClocks virtualVertex = new CoincidentClocks();
		
		if(! g.containsVertex(virtualVertex)){
			g.addVertex(virtualVertex);
		}
		if(! g.containsVertex(l_c1)){
			g.addVertex(l_c1);
		}
		if(! g.containsVertex(l_c2)){
			g.addVertex(l_c2);
		}
		if(! g.containsVertex(l_cres)){
			g.addVertex(l_cres);
		}
		virtualVertex.setKind(VertexKind.ImplicitSuperClock);
		
		g.addSimpleEdge(virtualVertex, e1, l_c1, true);
		g.addSimpleEdge(virtualVertex, e2, l_c2, true);
		g.addSimpleEdge(virtualVertex, e3, l_cres, true);
		g.addSimpleEdge(l_cres, e1cause, l_c1, true);
		g.addSimpleEdge(l_cres, e2cause, l_c2, true);
		TEST();
	}

	
	public void addSupRelation(InstantiatedElement c1, InstantiatedElement c2, InstantiatedElement cres, boolean isCond){
		TEST();
		Edge e1= new Edge(EdgeKind.SUBCLOCKFREE,isCond);
		Edge e2= new Edge(EdgeKind.SUBCLOCKFREE,isCond);
		Edge e3= new Edge(EdgeKind.SUBCLOCKFREE,isCond);
		Edge e1cause= new Edge(EdgeKind.CAUSES,isCond);
		Edge e2cause= new Edge(EdgeKind.CAUSES,isCond);
		
		CoincidentClocks l_c1 = addOrCreateListOf(c1);
		CoincidentClocks l_c2 = addOrCreateListOf(c2);
		CoincidentClocks l_cres = addOrCreateListOf(cres);
		CoincidentClocks virtualVertex = new CoincidentClocks();
		
		if(! g.containsVertex(virtualVertex)){
			g.addVertex(virtualVertex);
		}
		if(! g.containsVertex(l_c1)){
			g.addVertex(l_c1);
		}
		if(! g.containsVertex(l_c2)){
			g.addVertex(l_c2);
		}
		if(! g.containsVertex(l_cres)){
			g.addVertex(l_cres);
		}
		virtualVertex.setKind(VertexKind.ImplicitSuperClock);
		TEST();
		g.addSimpleEdge(virtualVertex, e1, l_c1, true);
		g.addSimpleEdge(virtualVertex, e2, l_c2, true);
		g.addSimpleEdge(virtualVertex, e3, l_cres, true);

		g.addSimpleEdge(l_c1, e1cause, l_cres, true);
		g.addSimpleEdge(l_c2, e2cause, l_cres, true);
		TEST();
	}
	
	public void addConcat(InstantiatedElement c1,
			InstantiatedElement c2,
			InstantiatedElement cres, boolean isCond) {
		TEST();
		//if recursive
		if(c2==cres){
			TEST();
			addCoincidence(c1, c2, isCond);
			TEST();
		}
		else{ //non recursive
			TEST();
			Edge e1= new Edge(EdgeKind.SEQCLOCK,isCond);
			Edge e2= new Edge(EdgeKind.SEQCLOCK,isCond);
			Edge e1cause = new Edge(EdgeKind.CAUSES,isCond);
			Edge e2cause = new Edge(EdgeKind.CAUSES,isCond);
			
			CoincidentClocks l_c1 = addOrCreateListOf(c1);
			CoincidentClocks l_c2 = addOrCreateListOf(c2);
			CoincidentClocks l_cres = addOrCreateListOf(cres);
			
			if(! g.containsVertex(l_c1)){
				g.addVertex(l_c1);
			}
			if(! g.containsVertex(l_c2)){
				g.addVertex(l_c2);
			}
			if(! g.containsVertex(l_cres)){
				g.addVertex(l_cres);
			}
			
			g.addSimpleEdge(l_c1, e1, l_cres, true);
			g.addSimpleEdge(l_c2, e2, l_cres, true);
			
			g.addSimpleEdge(l_cres, e1cause, l_c1, true);
			g.addSimpleEdge(l_cres, e2cause, l_c2, true);
			
			l_cres.setKind(VertexKind.ChoiceVertex);
			TEST();
		}
		
	}

	
	public ObjectGrph<CoincidentClocks, Edge> createClockGrphDag(UnfoldModel um) {
		unfoldModel=um;
		List<InstantiatedElement> concretes = um.getInstantiationTree().lookupInstances(null);
		//iterate on them
		for (InstantiatedElement concrete : concretes) {
			TEST();
			addCorrespondingVertexAndEdge(concrete);
			TEST();
		}
	return g;
	}
	@Override
	public void addConditional(InstantiatedElement leftc,
			InstantiatedElement rightc) {
		TEST();
		Edge e = new Edge(EdgeKind.CONDITIONAL, true);

		CoincidentClocks l_leftc = addOrCreateListOf(leftc);
		CoincidentClocks l_rightc = addOrCreateListOf(rightc);
		if(! g.containsVertex(l_leftc)){
			g.addVertex(l_leftc);
		}
		if(! g.containsVertex(l_rightc)){
			g.addVertex(l_rightc);
		}
		g.addSimpleEdge(l_rightc, e, l_leftc, true);
		TEST();
		
		return;
	}

	@Override
	public void addSampledOnExpression(InstantiatedElement samplingClock,
			InstantiatedElement sampledClock, InstantiatedElement res, boolean isCond) {
		TEST();
		Edge e = new Edge(EdgeKind.SUBCLOCKCONSTRAINED,isCond);
		Edge e2 = new Edge(EdgeKind.PRECEDES,isCond);

		CoincidentClocks l_samplingClock = addOrCreateListOf(samplingClock);
		CoincidentClocks l_sampledClock = addOrCreateListOf(sampledClock);
		CoincidentClocks l_res = addOrCreateListOf(res);
		if(! g.containsVertex(l_samplingClock)){
			g.addVertex(l_samplingClock);
		}
		if(! g.containsVertex(l_sampledClock)){
			g.addVertex(l_sampledClock);
		}
		if(! g.containsVertex(l_res)){
			g.addVertex(l_res);
		}
		TEST();
		g.addSimpleEdge(l_samplingClock, e, l_res, true);
		g.addSimpleEdge(l_sampledClock, e2, l_res, true);
		TEST();
		return;
		
	}

	@Override
	public void addNonStrictSampledOnExpression(
			InstantiatedElement samplingClock,
			InstantiatedElement sampledClock, InstantiatedElement res, boolean isCond) {
		TEST();
		Edge e = new Edge(EdgeKind.SUBCLOCKCONSTRAINED,isCond);
		Edge e2 = new Edge(EdgeKind.CAUSES,isCond);

		CoincidentClocks l_samplingClock = addOrCreateListOf(samplingClock);
		CoincidentClocks l_sampledClock = addOrCreateListOf(sampledClock);
		CoincidentClocks l_res = addOrCreateListOf(res);
		if(! g.containsVertex(l_samplingClock)){
			g.addVertex(l_samplingClock);
		}
		if(! g.containsVertex(l_sampledClock)){
			g.addVertex(l_sampledClock);
		}
		if(! g.containsVertex(l_res)){
			g.addVertex(l_res);
		}
		g.addSimpleEdge(l_samplingClock, e, l_res, true);
		g.addSimpleEdge(l_sampledClock, e2, l_res, true);
		TEST();
		return;
		
	}
	
	
//	String getQualifiedName(EObject eo){
//		StringBuilder sb = new StringBuilder(getName(eo));
//
//		for(EObject ee : EcoreUtil2.getAllContainers(eo)){
//			sb.append(getName(ee));
//		}
//		return sb.toString();
//	}
//	
//	String getName(EObject eo){
//	    String result = null;
//	    EList<EAttribute> eAllAttributes = eo.eClass().getEAllAttributes();
//	    for (EAttribute eAttribute : eAllAttributes) {
//	        if(eAttribute.getName().equals("name")){
//	            result = (String) eo.eGet(eAttribute);
//	        }           
//	    }
//	    return result;
//	}

}