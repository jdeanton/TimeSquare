/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.clocktree.generator;


import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;

public class KernelExpressionClockTreeBuilder extends KernelExpressionSwitch<Boolean> {

	private InstantiatedElement concrete;
	private ClockTreeConstructor builder;
	
	public KernelExpressionClockTreeBuilder(InstantiatedElement concrete, ClockTreeConstructor c) {
		this.concrete = concrete;
		this.builder = c;
	}
	
	@Override
	public Boolean caseConcatenation(Concatenation object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftClock());
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightClock());
		builder.addConcat(left, right, concrete, isCond);
		return true;
	}
	
	@Override
	public Boolean caseDefer(Defer object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}

		InstantiatedElement base = concrete.resolveAbstractEntity(object.getBaseClock());
		InstantiatedElement delayClock = concrete.resolveAbstractEntity(object.getDelayClock());
		
		InstantiatedElement ie = concrete.resolveAbstractEntity(object.getDelayPattern());
		SequenceElement pattern = (SequenceElement) ie.getValue();
		String patternString="";
		for(PrimitiveElement elt : pattern.getFinitePart()){
			int tmp =-1;
			if (elt instanceof IntegerElement){
				tmp= ((IntegerElement)elt).getValue().intValue();
			}else{
				InstantiatedElement intelem = concrete.resolveAbstractEntity(((IntegerVariableRef) elt).getReferencedVar());
				tmp= ((IntegerElement)intelem.getValue()).getValue().intValue();
			}
			patternString+="["+tmp+";"+tmp+"],";
		}
		patternString+="(";
		for(PrimitiveElement elt : pattern.getNonFinitePart()){
			int tmp =-1;
			if (elt instanceof IntegerElement){
				tmp= ((IntegerElement)elt).getValue().intValue();
			}else{
				InstantiatedElement intelem = concrete.resolveAbstractEntity(((IntegerVariableRef) elt).getReferencedVar());
				tmp= 0;//((IntegerElement)intelem.getValue()).getValue().intValue();
			}
			patternString+="["+tmp+";"+tmp+"],";
		}
		patternString+=")w";
		String k_affine=patternString;//+"."+base.getQualifiedName();
		
		builder.addSubClockConstrained(delayClock, k_affine, concrete, isCond);
		builder.addPrecedence(base, concrete, isCond);
		return true;
	}
	
	@Override
	public Boolean caseInf(Inf object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}
		InstantiatedElement c1 = concrete.resolveAbstractEntity(object.getClock1()); 
		InstantiatedElement c2 = concrete.resolveAbstractEntity(object.getClock2());
		builder.addInfRelation(c1, c2, concrete, isCond);
		return true;
	}
	
	@Override
	public Boolean caseIntersection(Intersection object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}
		InstantiatedElement c1 = concrete.resolveAbstractEntity(object.getClock1());
		InstantiatedElement c2 = concrete.resolveAbstractEntity(object.getClock2());
		builder.addSubClockFree(c1, concrete, isCond);
		builder.addSubClockFree(c2, concrete, isCond);
		return true;
	}
	
	@Override
	public Boolean caseNonStrictSampling(NonStrictSampling object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}

		InstantiatedElement samplingClock = concrete.resolveAbstractEntity(object.getSamplingClock());
		InstantiatedElement sampledClock = concrete.resolveAbstractEntity(object.getSampledClock());
		builder.addNonStrictSampledOnExpression(samplingClock, sampledClock, concrete, isCond);
		return true;
	}
	
	@Override
	public Boolean caseStrictSampling(StrictSampling object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}

		InstantiatedElement samplingClock = concrete.resolveAbstractEntity(object.getSamplingClock());
		InstantiatedElement sampledClock = concrete.resolveAbstractEntity(object.getSampledClock());
		builder.addSampledOnExpression(samplingClock, sampledClock, concrete, isCond);
		return true;
	}
	
	@Override
	public Boolean caseSup(Sup object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}

		InstantiatedElement c1 = concrete.resolveAbstractEntity(object.getClock1()); 
		InstantiatedElement c2 = concrete.resolveAbstractEntity(object.getClock2());
		builder.addSupRelation(c1, c2, concrete, isCond);
		return true;
	}
	
	@Override
	public Boolean caseUnion(Union object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent() == null || concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}

		InstantiatedElement c1 = concrete.resolveAbstractEntity(object.getClock1());
		InstantiatedElement c2 = concrete.resolveAbstractEntity(object.getClock2());
		builder.addSubClockFree(concrete, c1, isCond);
		builder.addSubClockFree(concrete, c2, isCond);
		return true;
	}
	
	@Override
	public Boolean caseUpTo(UpTo object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}

		InstantiatedElement toFollow = concrete.resolveAbstractEntity(object.getClockToFollow());
		InstantiatedElement killer = concrete.resolveAbstractEntity(object.getKillerClock());
		
		String k_affine="([1;1])w";//."+toFollow.getQualifiedName();
		
		builder.addSubClockConstrained(toFollow, k_affine, concrete, isCond);
		builder.addKiller(killer, concrete, isCond);
		return true;
	}
	
	@Override
	public Boolean caseWait(Wait object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}

		InstantiatedElement waitingClock = concrete.resolveAbstractEntity(object.getWaitingClock());
		
		InstantiatedElement ie = concrete.resolveAbstractEntity(object.getWaitingValue());
		IntegerElement v = (IntegerElement) ie.getValue();
		Integer waitValue = 0;
		if (v != null){
			waitValue = v.getValue();
		}else{
		}
		
		ie = concrete.resolveAbstractEntity(object.getWaitingClock());
		String waitingClockName = ie.getQualifiedName();
		String k_affine="["+waitValue+";"+waitValue+"]";//."+waitingClockName;
		builder.addSubClockConstrained(waitingClock, k_affine, concrete, isCond);
		return true;
	}
	
}
































