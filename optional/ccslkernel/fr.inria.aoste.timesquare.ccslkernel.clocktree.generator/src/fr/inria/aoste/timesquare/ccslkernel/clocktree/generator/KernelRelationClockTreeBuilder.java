/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.clocktree.generator;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;

public class KernelRelationClockTreeBuilder extends
		KernelRelationSwitch<Boolean> {

	private InstantiatedElement concrete;
	private ClockTreeConstructor builder;

	public KernelRelationClockTreeBuilder(InstantiatedElement concrete, ClockTreeConstructor builder) {
		super();
		this.concrete = concrete;
		this.builder = builder;
	}

	@Override
	public Boolean caseCoincidence(Coincidence object) {
		//TODO: how to represent conditional Concidences ?
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		builder.addCoincidence(left, right, false);
		return true;
	}
	
	@Override
	public Boolean caseExclusion(Exclusion object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent() == null || concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		builder.addExcludes(left, right, isCond);
		builder.addExcludes(right, left, isCond);
		return true;
	}
	
	@Override
	public Boolean caseNonStrictPrecedence(NonStrictPrecedence object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		builder.addCausality(left, right, isCond);
		return true;
	}
	
	@Override
	public Boolean casePrecedence(Precedence object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		builder.addPrecedence(left, right, isCond);
		return true;
	}
	
	@Override
	public Boolean caseSubClock(SubClock object) {
		boolean isCond = false;
		try{
			isCond = concrete.getParent().isConditional();
			
		}catch (NullPointerException e) {
			// not conditional since no parent :)
		}
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		builder.addSubClockFree(right, left,isCond);
		return true;
	}

}
