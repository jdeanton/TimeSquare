/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
//package fr.inria.aoste.timesquare.ccslkernel.clocktree.generator;
//
//import java.util.List;
//import java.util.Set;
//
//import org.eclipse.emf.common.util.URI;
//import org.eclipse.emf.ecore.resource.Resource;
//import org.jgrapht.DirectedGraph;
//import org.jgrapht.graph.DefaultDirectedGraph;
//
//import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
//import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
//import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
//import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
//import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
//import grph.oo.ObjectGrph;
//
//public class CCSLKernelClockTreeConstructor implements ClockTreeConstructor {
//
//	private DirectedGraph<CoincidentClocks, Edge> g = new DefaultDirectedGraph<CoincidentClocks, Edge>(Edge.class);
//	private UnfoldModel unfoldModel;
//
//
//	public CCSLKernelClockTreeConstructor() {
//		// to enforce gc eligibility
//		g = null;
//		System.gc();
//
//		try {
//			// ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
//			g = new DefaultDirectedGraph<CoincidentClocks, Edge>(Edge.class);
//		} catch (Throwable e) {
//			System.out.println(e.toString());
//		}
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#getUnfoldModel()
//	 */
//	@Override
//	public UnfoldModel getUnfoldModel(){
//		return unfoldModel;
//	}
//	
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#getGraph()
//	 */
//	@Override
//	public DirectedGraph<CoincidentClocks, Edge> getGraph(){
//		return g;
//	}
//	
//	
//	
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#createClockDag(org.eclipse.emf.ecore.resource.Resource)
//	 */
//	@Override
//	public  DirectedGraph<CoincidentClocks, Edge> createClockDag(Resource ccslResource){
//		try {
//			unfoldModel = UnfoldModel.unfoldModel(ccslResource);
//		} 
//		catch (Throwable throwable)
//		{
//			String message= throwable.getMessage();
//			if (message==null )
//					message=throwable.getClass().getName()+" has throw !!";
//			message ="During solver.loadModel(URI) : "+ message ;
//			throw new RuntimeException(message, throwable);
//		}
//		
//		List<InstantiatedElement> concretes = unfoldModel.getInstantiationTree().lookupInstances(null);
//		//iterate on them
//		for (InstantiatedElement concrete : concretes) {
//			addCorrespondingVertexAndEdge(concrete);
//		}
//		
//		return g;
//	}
//	
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#createClockDag(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel)
//	 */
//	@Override
//	public  DirectedGraph<CoincidentClocks, Edge> createClockDag(UnfoldModel um){
//		unfoldModel = um;
//		
//		
//		List<InstantiatedElement> concretes = unfoldModel.getInstantiationTree().lookupInstances(null);
//		//iterate on them
//		for (InstantiatedElement concrete : concretes) {
//			addCorrespondingVertexAndEdge(concrete);
//		}
//		
//		return g;
//	}
//	
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#createClockDag(java.lang.String)
//	 */
//	@Override
//	public  DirectedGraph<CoincidentClocks, Edge> createClockDag(String ccslSpecificationPath){
//		UnfoldModel unfoldModel;
//		try {
//			unfoldModel = UnfoldModel.unfoldModel(URI.createURI(ccslSpecificationPath));
//		} 
//		catch (Throwable throwable)
//		{
//			String message= throwable.getMessage();
//			if (message==null )
//					message=throwable.getClass().getName()+" has throw !!";
//			message ="During solver.loadModel(URI) : "+ message ;
//			throw new RuntimeException(message, throwable);
//		}
//		
//		List<InstantiatedElement> concretes = unfoldModel.getInstantiationTree().lookupInstances(null);
//		//iterate on them
//		for (InstantiatedElement concrete : concretes) {
//			addCorrespondingVertexAndEdge(concrete);
//		}
//		
//		return g;
//	}
//
//	private void addCorrespondingVertexAndEdge(InstantiatedElement concrete) {
//		if (concrete.isExpression()) {
//			if (concrete.isKernelExpression() /*&& ! concrete.isConditionCase()*/) {
//				KernelExpressionDeclaration declaration = (KernelExpressionDeclaration) concrete.getDeclaration();
//				KernelExpressionClockTreeBuilder builder = new KernelExpressionClockTreeBuilder(concrete, this);
//				builder.doSwitch(declaration);
//			}
//			else if (concrete.isConditional() ) {
//				if ( ! concrete.isRecursiveCall()) {
//					// TODO deal with conditional
//					for(InstantiatedElement ie:concrete.getConditionalCases()){
//						addCorrespondingVertexAndEdge(ie);
//						addConditional(concrete, ie);
//					}
//				}
//			}
//			else {
//				if ( concrete.getDefinition() != null &&
//						concrete.getDefinition() instanceof UserExpressionDefinition )
//				{
//					InstantiatedElement rootExprElement = concrete.getRootExpression();
//					addCoincidence(rootExprElement, concrete);
//				}
//				
//			}
//		}
//		else if (concrete.isRelation()) {
//			if (concrete.isKernelRelation()) {
//				KernelRelationDeclaration declaration = (KernelRelationDeclaration) concrete.getDeclaration();
//				KernelRelationClockTreeBuilder builder = new KernelRelationClockTreeBuilder(concrete, this);
//				builder.doSwitch(declaration);
//			}
//		}
//	}
//
//	private CoincidentClocks addOrCreateListOf(InstantiatedElement elt) {
//		CoincidentClocks l_concrete = getListOf(elt);
//		if (l_concrete == null) {
//			l_concrete = new CoincidentClocks();
//			l_concrete.add(elt);
//		}
//		return l_concrete;
//	}
//
//	private CoincidentClocks getListOf(InstantiatedElement concrete) {
//		for (CoincidentClocks l_concrete : g.vertexSet()) {
//			if (l_concrete.contains(concrete)) {
//				return l_concrete;
//			}
//		}
//		return null;
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addCoincidence(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addCoincidence(InstantiatedElement c1, InstantiatedElement c2) {
//		//int nb_edgeBefore = g.edgeSet().size();
//		@SuppressWarnings("unused")
//		int nb_vertexBefore = g.vertexSet().size();
//		// Edge e= new Edge();
//		// e.ek=EdgeKind.COINCIDES;
//
//		assert (c1 != null);
//		assert (c2 != null);
////		assert (c1 instanceof Clock);
////		assert (c2 instanceof Clock);
//		CoincidentClocks l_c1 = addOrCreateListOf(c1);
//		CoincidentClocks l_c2 = addOrCreateListOf(c2);
//	//	assert (!l_c2.equals(l_c1));
//
//		// if they do not exist in the graph
//		if (!g.vertexSet().contains(l_c1) && !g.vertexSet().contains(l_c2)) {
//			l_c2.addAll(l_c1);
//			g.addVertex(l_c2);
//			return;
//		}
//
//		if (!g.vertexSet().contains(l_c1) && g.vertexSet().contains(l_c2)) {
//			l_c2.addAll(l_c1);
//			return;
//		}
//
//		if (g.vertexSet().contains(l_c1) && !g.vertexSet().contains(l_c2)) {
//			l_c1.addAll(l_c2);
//			return;
//		}
//
//		// if there are no neighbors, we merge
//		if ((g.edgesOf(l_c1).size() == 0) && (g.edgesOf(l_c2).size() == 0)) {
//			// choose the vertex with special decoration
//			if (l_c1.getKind() != VertexKind.ClockSet) {
//				l_c1.addAll(l_c2);
//				g.removeVertex(l_c2);
//			} else {
//				l_c2.addAll(l_c1);
//				g.removeVertex(l_c1);
//			}
//
//		} else if (!l_c2.equals(l_c1)){
//			if ((l_c1.getKind() != VertexKind.ClockSet)) {
//				// we merge the lists and we reconnect the edge of l_c1 to lc2
//				// and
//				Set<Edge> edges = g.edgesOf(l_c2);
//				for (Edge e : edges) {
//					// it is directed
//					CoincidentClocks dest = g.getEdgeTarget(e);
//					CoincidentClocks source = g.getEdgeSource(e);
//
//					g.removeEdge(e);
//
//					if (l_c2.equals(dest)) {// the edge is an input edge
//						g.addEdge(source, l_c1, e);
//					} else {
//						// it is an output edge
//						assert (!dest.equals(l_c1));
//						g.addEdge(l_c1, dest, e);
//					}
//
//				}
//				l_c1.addAll(l_c2);
//				assert (g.edgesOf(l_c2).size() == 0);
//				g.removeVertex(l_c2);
//
//			} else {
//
//				// we merge the lists and we reconnect the edge of l_c1 to lc2
//				// and
//				Set<Edge> edges = g.edgesOf(l_c1);
//				for (Edge e : edges) {
//					// it is directed
//					CoincidentClocks dest = g.getEdgeTarget(e);
//					CoincidentClocks source = g.getEdgeSource(e);
//
//					g.removeEdge(e);
//
//					if (l_c1.equals(dest)) {// the edge is an input edge
//						g.addEdge(source, l_c2, e);
//					} else {
//						// it is an output edge
//						assert (!dest.equals(l_c2));
//						g.addEdge(l_c2, dest, e);
//					}
//
//				}
//				l_c2.addAll(l_c1);
//				assert (g.edgesOf(l_c1).size() == 0);
//				g.removeVertex(l_c1);
//
//			}
//		}
//
//		//int nb_edgeAfter = g.edgeSet().size();
//		@SuppressWarnings("unused")
//		int nb_vertexAfter = g.vertexSet().size();
//		
////		// remove one vertex
////		assert (nb_vertexAfter + 1 == nb_vertexBefore) : "AddCoincidence(" + c1 + " , " + c2 + "):  Vertex "
////				+ nb_vertexBefore + "=> " + nb_vertexAfter;
//
//		return;
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addPrecedence(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addPrecedence(InstantiatedElement leftc, InstantiatedElement rightc) {
//
//		Edge e = new Edge(EdgeKind.PRECEDES);
//
//		CoincidentClocks l_leftc = addOrCreateListOf(leftc);
//		CoincidentClocks l_rightc = addOrCreateListOf(rightc);
//		g.addVertex(l_leftc);
//		g.addVertex(l_rightc);
//		g.addEdge(l_leftc, l_rightc, e);
//
//		return;
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addCausality(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addCausality(InstantiatedElement leftc, InstantiatedElement rightc) {
//
//		Edge e = new Edge(EdgeKind.CAUSES);
//
//		CoincidentClocks l_leftc = addOrCreateListOf(leftc);
//		CoincidentClocks l_rightc = addOrCreateListOf(rightc);
//		g.addVertex(l_leftc);
//		g.addVertex(l_rightc);
//		g.addEdge(l_leftc, l_rightc, e);
//
//		return;
//	}
//	
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addConditional(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addConditional(InstantiatedElement leftc, InstantiatedElement rightc) {
//
//		Edge e = new Edge(EdgeKind.CONDITIONAL);
//
//		CoincidentClocks l_leftc = addOrCreateListOf(leftc);
//		CoincidentClocks l_rightc = addOrCreateListOf(rightc);
//		g.addVertex(l_leftc);
//		g.addVertex(l_rightc);
//		g.addEdge(l_leftc, l_rightc, e);
//
//		return;
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addExcludes(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addExcludes(InstantiatedElement leftc, InstantiatedElement rightc) {
//
//		Edge e = new Edge(EdgeKind.EXCLUDES);
//
//		CoincidentClocks l_leftc = addOrCreateListOf(leftc);
//		CoincidentClocks l_rightc = addOrCreateListOf(rightc);
//		g.addVertex(l_leftc);
//		g.addVertex(l_rightc);
//		g.addEdge(l_leftc, l_rightc, e);
//
//		return;
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addSubClockFree(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addSubClockFree(InstantiatedElement hyperc, InstantiatedElement subc) {
//
//		Edge e = new Edge(EdgeKind.SUBCLOCKFREE);
//		Edge e2 = new Edge(EdgeKind.CAUSES);
//		
//		CoincidentClocks l_hyperc = addOrCreateListOf(hyperc);
//		CoincidentClocks l_subc = addOrCreateListOf(subc);
//		g.addVertex(l_hyperc);
//		g.addVertex(l_subc);
//		g.addEdge(l_hyperc, l_subc, e);
//		g.addEdge(l_hyperc, l_subc, e2);
//		return;
//	}
//	
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addSubClockConstrained(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addSubClockConstrained(InstantiatedElement hyperc, String k_affine, InstantiatedElement subc) {
//
//		Edge e = new Edge(EdgeKind.SUBCLOCKCONSTRAINED);
//		Edge e2 = new Edge(EdgeKind.CAUSES);
//
//		e.k_affineFunction = k_affine;
//		CoincidentClocks l_hyperc = addOrCreateListOf(hyperc);
//		CoincidentClocks l_subc = addOrCreateListOf(subc);
//		g.addVertex(l_hyperc);
//		g.addVertex(l_subc);
//		g.addEdge(l_hyperc, l_subc, e);
//		g.addEdge(l_hyperc, l_subc, e2);
//
//		return;
//	}
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addSampledOnExpression(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addSampledOnExpression(InstantiatedElement samplingClock, InstantiatedElement sampledClock, InstantiatedElement res) {
//
//		Edge e = new Edge(EdgeKind.SUBCLOCKCONSTRAINED);
//		Edge e2 = new Edge(EdgeKind.PRECEDES);
//
//		CoincidentClocks l_samplingClock = addOrCreateListOf(samplingClock);
//		CoincidentClocks l_sampledClock = addOrCreateListOf(sampledClock);
//		CoincidentClocks l_res = addOrCreateListOf(res);
//		g.addVertex(l_samplingClock);
//		g.addVertex(l_sampledClock);
//		g.addVertex(l_res);
//		g.addEdge(l_samplingClock, l_res, e);
//		g.addEdge(l_sampledClock, l_res, e2);
//
//		return;
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addNonStrictSampledOnExpression(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addNonStrictSampledOnExpression(InstantiatedElement samplingClock, InstantiatedElement sampledClock,
//			InstantiatedElement res) {
//
//		Edge e = new Edge(EdgeKind.SUBCLOCKCONSTRAINED);
//		Edge e2 = new Edge(EdgeKind.CAUSES);
//
//		CoincidentClocks l_samplingClock = addOrCreateListOf(samplingClock);
//		CoincidentClocks l_sampledClock = addOrCreateListOf(sampledClock);
//		CoincidentClocks l_res = addOrCreateListOf(res);
//		g.addVertex(l_samplingClock);
//		g.addVertex(l_sampledClock);
//		g.addVertex(l_res);
//		g.addEdge(l_samplingClock, l_res, e);
//		g.addEdge(l_sampledClock, l_res, e2);
//
//		return;
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addKiller(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addKiller(InstantiatedElement killer, InstantiatedElement res) {
//
//		Edge e = new Edge(EdgeKind.KILL);
//
//		CoincidentClocks l_killer = addOrCreateListOf(killer);
//		CoincidentClocks l_res = addOrCreateListOf(res);
//		g.addVertex(l_killer);
//		g.addVertex(l_res);
//		g.addEdge(l_killer, l_res, e);
//
//		return;
//	}
//
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addInfSupRelation(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addInfRelation(InstantiatedElement c1, InstantiatedElement c2, InstantiatedElement cres) {
//		Edge e1 = new Edge(EdgeKind.SUBCLOCKFREE); //not sure it is so free...
//		Edge e2 = new Edge(EdgeKind.SUBCLOCKFREE);
//		Edge e3 = new Edge(EdgeKind.SUBCLOCKFREE);
//		Edge e1cause= new Edge(EdgeKind.CAUSES);
//		Edge e2cause= new Edge(EdgeKind.CAUSES);
//
//		
//		
//		CoincidentClocks l_c1 = addOrCreateListOf(c1);
//		CoincidentClocks l_c2 = addOrCreateListOf(c2);
//		CoincidentClocks l_cres = addOrCreateListOf(cres);
//		CoincidentClocks virtualVertex = new CoincidentClocks();
//		g.addVertex(virtualVertex);
//		g.addVertex(l_c1);
//		g.addVertex(l_c2);
//		g.addVertex(l_cres);
//		virtualVertex.setKind(VertexKind.ImplicitSuperClock);
//
//		g.addEdge(virtualVertex, l_c1, e1);
//		g.addEdge(virtualVertex, l_c2, e2);
//		g.addEdge(virtualVertex, l_cres, e3);
//		g.addEdge(l_cres, l_c1, e1cause);
//		g.addEdge(l_cres, l_c2, e2cause);
//
//	}
//	
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addInfSupRelation(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addSupRelation(InstantiatedElement c1, InstantiatedElement c2, InstantiatedElement cres) {
//		Edge e1 = new Edge(EdgeKind.SUBCLOCKFREE); //not sure it is so free...
//		Edge e2 = new Edge(EdgeKind.SUBCLOCKFREE);
//		Edge e3 = new Edge(EdgeKind.SUBCLOCKFREE);
//
//		CoincidentClocks l_c1 = addOrCreateListOf(c1);
//		CoincidentClocks l_c2 = addOrCreateListOf(c2);
//		CoincidentClocks l_cres = addOrCreateListOf(cres);
//		CoincidentClocks virtualVertex = new CoincidentClocks();
//		g.addVertex(virtualVertex);
//		g.addVertex(l_c1);
//		g.addVertex(l_c2);
//		g.addVertex(l_cres);
//		virtualVertex.setKind(VertexKind.ImplicitSuperClock);
//
//		g.addEdge(virtualVertex, l_c1, e1);
//		g.addEdge(virtualVertex, l_c2, e2);
//		g.addEdge(virtualVertex, l_cres, e3);
//
//	}
//
////	public void addConcat(ConcreteEntity c1, ConcreteEntity c2, ConcreteEntity cres) {
////
////		// if recursive
////		if (c2 == cres) {
////			addCoincidence(c1, c2);
////		} else {
////			if ((c2 instanceof ImplicitClock) && (((ImplicitClock)c2).getEntity() instanceof RecursiveTailCallJump)){
//////					||
//////			   (cres instanceof ImplicitClock) && (((ImplicitClock)cres).getEntity() instanceof RecursiveTailCallJump)){
//////			//make it cleaner
////				addCoincidence(c1, cres);
////				
////			}else{ // non recursive
////					Edge e1 = new Edge(EdgeKind.SEQCLOCK);
////					Edge e2 = new Edge(EdgeKind.SEQCLOCK);
////		
////					CoincidentClocks l_c1 = addOrCreateListOf(c1);
////					CoincidentClocks l_c2 = addOrCreateListOf(c2);
////					CoincidentClocks l_cres = addOrCreateListOf(cres);
////		
////					g.addVertex(l_c1);
////					g.addVertex(l_c2);
////					g.addVertex(l_cres);
////		
////					g.addEdge(l_c1, l_cres, e1);
////					g.addEdge(l_c2, l_cres, e2);
////		
////					l_cres.setKind(VertexKind.ChoiceVertex);
////			}
////
////		}
////
////	}
//	
//	/* (non-Javadoc)
//	 * @see fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor#addConcat(fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement, fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement)
//	 */
//	@Override
//	public void addConcat(InstantiatedElement left, InstantiatedElement right, InstantiatedElement concat) {
//		if (right.equals(concat)) {
//			// Iterative version of concatenation
//			addCoincidence(left, concat);
//		}
//		else if (right.isRecursiveCall() && right.isTailRecursiveCall()) {
//			// concat in a tail recursive 
//			addCoincidence(left, concat);
//		}
//		else {
//			Edge e1 = new Edge(EdgeKind.SEQCLOCK);
//			Edge e1cause = new Edge(EdgeKind.CAUSES);
//			Edge e2 = new Edge(EdgeKind.SEQCLOCK);
//			Edge e2cause = new Edge(EdgeKind.CAUSES);
//			
//			CoincidentClocks l_c1 = addOrCreateListOf(left);
//			CoincidentClocks l_c2 = addOrCreateListOf(right);
//			CoincidentClocks l_cres = addOrCreateListOf(concat);
//
//			g.addVertex(l_c1);
//			g.addVertex(l_c2);
//			g.addVertex(l_cres);
//
//			g.addEdge(l_c1, l_cres, e1);
//			g.addEdge(l_c2, l_cres, e2);
//			g.addEdge(l_cres, l_c1, e1cause);
//			g.addEdge(l_cres, l_c2, e2cause);
//			
//			l_cres.setKind(VertexKind.ChoiceVertex);
//		}
//	}
//
//	@Override
//	public ObjectGrph<CoincidentClocks, Edge> createClockGrphDag(String string) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//}
