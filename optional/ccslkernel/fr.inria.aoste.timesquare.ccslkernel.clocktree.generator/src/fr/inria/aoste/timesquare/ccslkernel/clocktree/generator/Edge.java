/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.clocktree.generator;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;



public class Edge {
	
	public boolean isConditional = false;
	private static final long serialVersionUID = -2956854405140532449L;
	final private int hashCode = System.identityHashCode(this);
	@Override
	public int hashCode() {
		return hashCode;
	}

	public Edge(EdgeKind ek, boolean isCond) {
		super();
		isConditional = isCond;
		this.ek = ek;
	}
	
	@Override
	public boolean equals(Object obj) {
			if (! (obj instanceof Edge)) {
				return false;
			}
			return hashCode == ((Edge) obj).hashCode;
	}

	public final  EdgeKind ek;
	
	/**
	 * used to specify the k-affine function when the edge is a subclock edge
	 * --> first test, to be improved (should not be a string)
	 */
	public String k_affineFunction = null;
	
	
	public boolean isConstrainer(){
		switch (ek) {
		case CAUSES:
			return false;
		case COINCIDES:
			return true;
		case EXCLUDES:
			return false;
		case PRECEDES:
			return false;
		case SEQCLOCK:
			return true;
		case SUBCLOCKFREE:
			return false;
		case SUBCLOCKCONSTRAINED:
			return true;
		case KILL:
			return true;//????
		case CONDITIONAL:
			return true;//????
	}
	
	return false;
}
	
	public boolean isPriorityPropagator(){
		switch (ek) {
		case CAUSES:
			return true;
		case COINCIDES:
			return true;
		case EXCLUDES:
			return false;
		case PRECEDES:
			return false;
		case SEQCLOCK:
			return true;
		case SUBCLOCKFREE:
			return true;
		case SUBCLOCKCONSTRAINED:
			return true;
		case KILL:
			return false;
		case CONDITIONAL:
			return true;
		
		}
		
		return false;
	}
	
	public boolean isSynchronous(){
		switch (ek) {
		case CAUSES:
			return false;
		case COINCIDES:
			return true;
		case EXCLUDES:
			return true;
		case PRECEDES:
			return false;
		case SEQCLOCK:
			return true;
		case SUBCLOCKFREE:
			return true;
		case SUBCLOCKCONSTRAINED:
			return true;
		case KILL:
			return false;
		case CONDITIONAL:
			return false;
		
		}
		
		return false;
	}
	
	public boolean isAsynchronous(){
		
		switch (ek) {
		case CAUSES:
			return true;
		case COINCIDES:
			return false;
		case EXCLUDES:
			return false;
		case PRECEDES:
			return true;
		case SEQCLOCK:
			return false;
		case SUBCLOCKFREE:
			return false;
		case SUBCLOCKCONSTRAINED:
			return false;
		case KILL:
			return true;//????
		case CONDITIONAL:
			return false;
		}
		
		return false;
	}
	
	public Color getColor(Display d) {
		switch (ek) {
		case CAUSES:
			return new Color(d, 0, 0, 255);
		case COINCIDES:
			return new Color(d, 0, 0, 0);
		case EXCLUDES:
			return new Color(d, 255, 0, 0);
		case PRECEDES:
			return new Color(d, 127, 0, 255);
		case SEQCLOCK:
			return new Color(d, 127, 127, 127);
		case SUBCLOCKFREE:			
			return new Color(d, 127, 255, 127);
		case SUBCLOCKCONSTRAINED:			
			return new Color(d, 127, 255, 127);
		case KILL:
			return new Color(d, 200, 200, 50);
		case CONDITIONAL:
			return new Color(d, 0, 127, 250);
		
		}
		return new Color(d, 255, 255, 255);
	}

	
	public String toString(){
		return getLabel();
	}
	
	public String getLabel() {
		switch (ek) {
		case CAUSES:
			return "<=";
		case COINCIDES:
			return "=";
		case EXCLUDES:
			return "#";
		case PRECEDES:
			return "<";
		case SEQCLOCK:
			return "seq";
		case SUBCLOCKFREE:
			return "sub_f";
		case SUBCLOCKCONSTRAINED:
			return "sub_c";
		case KILL:
			return "kill";
		case CONDITIONAL:	
			return "condition";
		}
	
		return "??";
	}

	public int getSize() {
		switch (ek) {
		case CAUSES:
			return 2;
		case COINCIDES:
			return 10;
		case EXCLUDES:
			return 2;
		case PRECEDES:
			return 2;
		case SEQCLOCK:
			return 5;
		case SUBCLOCKFREE:
			return 2;
		case SUBCLOCKCONSTRAINED:
			return 5;
		case KILL:
			return 1;
		case CONDITIONAL:
			return 3;
		
		}
		
		return 0;
	}

}
