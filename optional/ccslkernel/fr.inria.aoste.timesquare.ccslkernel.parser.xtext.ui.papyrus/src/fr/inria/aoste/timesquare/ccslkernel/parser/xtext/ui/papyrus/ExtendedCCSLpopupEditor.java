/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ui.papyrus;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.Clock;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedProcessing;
import org.eclipse.papyrus.commands.wrappers.GMFtoGEFCommandWrapper;
import org.eclipse.papyrus.extensionpoints.editors.ui.IPopupEditorHelper;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Stereotype;
//import org.eclipse.papyrus.infra.gmfdiag.xtext.glue.edit.part.DefaultXtextSemanticValidator;
//import org.eclipse.papyrus.infra.gmfdiag.xtext.glue.edit.part.IXtextEMFReconciler;
import org.eclipse.xtext.parsetree.reconstr.Serializer;

import com.google.inject.Injector;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ui.internal.ExtendedCCSLActivator;


public class ExtendedCCSLpopupEditor extends org.eclipse.papyrus.extensionpoints.editors.configuration.DefaultDirectEditorConfiguration{
	private static final String CONSTRAINT_FROM_STEREOTYPE_BLOCK_NAME = "otherConstraintsDueToStereotypes";
	private static final String OTHER_CLOCKS_BLOCK_NAME="otherClocksInTheModel";
	//TODO : look here : http://dev.eclipse.org/viewsvn/viewvc.cgi/trunk/plugins/uml/org.eclipse.papyrus.state.editor.xtext.ui/src/org/eclipse/papyrus/state/editor/xtext/ui/contributions/StatePopupEditorConfigurationContribution.java?root=MDT_Papyrus&r1=2305&r2=2304&pathrev=2305
	private static final String OTHER_CONSTRAINTS_BLOCK_NAME = "otherConstraintsInTheModel";
	private static final String COMMENT_ABOVE = "//Do not modify above this comment, it would have no effect";
	private Injector injector;
	private Constraint constraint;
	private String constraintBody="";
	private String headerStart = "ClockConstraintSystem MySpec {\n"+
"	    imports {\n"+
"			import \"platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib\" as lib0;\n"+ 
"			import \"platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib\" as lib1; \n";
private String headerEnd = "	    }\n"+
"	    entryBlock main\n"+
"	     Block main {\n"+COMMENT_ABOVE+"\n";
	public ExtendedCCSLpopupEditor() {
		super();
	}
	
	public IPopupEditorHelper createPopupEditorHelper(Object editPart) {
		// resolves the edit part, and the associated semantic element
		 	if(!(editPart instanceof IGraphicalEditPart))
		 			return null;
		 	
		 	final IGraphicalEditPart graphicalEditPart = (IGraphicalEditPart)editPart;
		 	
		 	if(!(graphicalEditPart.getTopGraphicEditPart().resolveSemanticElement() instanceof Constraint)) {
		 		return null;
		 	}
		 	
		 	constraint = (Constraint)graphicalEditPart.getTopGraphicEditPart().resolveSemanticElement();
		 	constraintBody="";
		 	// retrieves the XText injector
		 	injector = ExtendedCCSLActivator.getInstance().getInjector(ExtendedCCSLActivator.FR_INRIA_AOSTE_TIMESQUARE_CCSLKERNEL_PARSER_XTEXT_EXTENDEDCCSL);
		 	
		 	// builds the text content and extension for a temporary file, to be edited by the xtext editor
		 	String textToEdit = headerStart+getImportModel(constraint)+headerEnd+ this.getTextToEdit(constraint)+getAllOtherConstraintsContent(constraint)+"     }\n}";
		 	String fileExtension = "" + ".extendedCCSL";

		 	
		 	// builds a new IXtextEMFReconciler.
		 	// Its purpose is to extract any relevant information from the textual specification,
		 	// and then merge it in the context UML model if necessary
		 	IXtextEMFReconciler reconciler = new IXtextEMFReconciler() {
			 	
			 	public void reconcile(EObject modelObject, EObject xtextObject) {
			 	// first: retrieves / determines if the xtextObject is a MessageRule object
			 	EObject modifiedObject = xtextObject;
			 	if(!(modelObject instanceof Constraint))
			 		return;
			 	if(modifiedObject == null)
			 	return;
			 	
			 	ClockConstraintSystem CCSLObject = (ClockConstraintSystem)xtextObject;
			 // Creates and executes the update command
				TransactionalEditingDomain dom = graphicalEditPart.getEditingDomain();
			    Serializer serializer = injector.getInstance(Serializer.class);
			    Block superBlock = CCSLObject.getSuperBlock();
			    
			    //remove added Block from the resulting body
			    Block blockToRemove1=null;
			    Block blockToRemove2=null;
			    Block blockToRemove3=null;
			    for(Block b : superBlock.getSubBlock()){
			    	if (b.getName().compareTo(OTHER_CLOCKS_BLOCK_NAME)==0){
			    		blockToRemove1=b;
			    	}
			    	if (b.getName().compareTo(CONSTRAINT_FROM_STEREOTYPE_BLOCK_NAME)==0){
			    		blockToRemove2=b;
			    	}
			    	if (b.getName().compareTo(OTHER_CONSTRAINTS_BLOCK_NAME)==0){
			    		blockToRemove3=b;
			    	}
			    }
			    superBlock.getSubBlock().remove(blockToRemove1);
			    superBlock.getSubBlock().remove(blockToRemove2);
			    superBlock.getSubBlock().remove(blockToRemove3);

			    for(fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element elem : CCSLObject.getSuperBlock().getElements()){
			    	constraintBody+=serializer.serialize(elem)+"\n";
			    }
			    for(Expression exp : CCSLObject.getSuperBlock().getExpressions()){
			    	constraintBody+=serializer.serialize(exp)+"\n";
			    }
			    for(Relation rel : CCSLObject.getSuperBlock().getRelations()){
			    	constraintBody+=serializer.serialize(rel)+"\n";
			    }
			    
			    for(ClassicalExpression classExp : CCSLObject.getSuperBlock().getClassicalExpression()){
			    	constraintBody+=serializer.serialize(classExp)+"\n";
			    }
			    
			    for(Block subBlock : CCSLObject.getSuperBlock().getSubBlock()){
			    	constraintBody+=serializer.serialize(subBlock)+"\n";
			    }
			    
			    constraintBody=constraintBody.replaceAll(COMMENT_ABOVE, "");
				
				UpdateUMLConstraintCommand updateCommand = null;
				try {
					updateCommand = new UpdateUMLConstraintCommand(dom,constraint);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dom.getCommandStack().execute(new GMFtoEMFCommandWrapper(updateCommand));
			 	}
		 	};
		 		

		 	return super.createPopupEditorHelper(graphicalEditPart,
		 			injector,
		 			reconciler,
		 			textToEdit,
		 			fileExtension,
		 			new DefaultXtextSemanticValidator());
			}


	
	private String getImportModel(Constraint originalConstraint) {
		String res="			import \"";
		
		Model model = originalConstraint.getModel();
		res+=model.eResource().getURI();
		return res+"\" as model;";
	}

	private String getAllOtherConstraintsContent(Constraint originalConstraint) {
		String res = "\n\n\n\n\n\n\n //Do not modify under this comment, it would have no effects"; //put it at the end :)
		 res+=getAllClocksFromTheModel(originalConstraint);
		 res += getAllConstraintsFromStereotypes(originalConstraint);
		 res+= getAllConstraintsFromTheModel(originalConstraint);
		return res;
	}
	
	private String getAllConstraintsFromStereotypes(Constraint originalConstraint) {
		String res = "\n	Block "+CONSTRAINT_FROM_STEREOTYPE_BLOCK_NAME+"{\n";
		Model model = originalConstraint.getModel();
		TreeIterator<EObject> it = model.eAllContents();
		while(it.hasNext()){
			EObject eo = it.next();
			if ((eo instanceof Element)){
				for(Stereotype stereotype : ((Element)eo).getAppliedStereotypes()){
					EObject sa = ((Element)eo).getStereotypeApplication(stereotype);
					if (sa instanceof TimedProcessing){
						TimedProcessing tp = (TimedProcessing)sa;
						String qualifiedName = qualifiedNamedToClockName(getStereotypedElementQualifiedName(sa));
						String clockToDelay="";
						for(Stereotype s : ((Element)tp.getStart()).getAppliedStereotypes()){
							EObject saE = ((Element)tp.getStart()).getStereotypeApplication(s);
							clockToDelay = qualifiedNamedToClockName(getStereotypedElementQualifiedName(saE));
						}
						String clockToDefer="";
						for(Stereotype s : ((Element)tp.getFinish()).getAppliedStereotypes()){
							EObject saE = ((Element)tp.getFinish()).getStereotypeApplication(s);
							clockToDefer = qualifiedNamedToClockName(getStereotypedElementQualifiedName(saE));
						}
						//warning restriction to only one clock in the "on" attribute
						String ClockForCounting = qualifiedNamedToClockName(getStereotypedElementQualifiedName(tp.getOn().get(0)));
						res+="		Integer	"+qualifiedName+"delay = "+tp.getDuration().integerValue()+"\n"+
							 "		Expression "+qualifiedName+
								"= DelayFor( DelayForClockToDelay-> "+clockToDelay+", DelayForClockForCounting-> "+ClockForCounting+", DelayForDelay-> "+qualifiedName.replaceAll("::", "_")+"delay)\n"+
							"		Relation "+qualifiedName+"coincides[Coincides](Clock1 ->"+qualifiedName+", Clock2->"+clockToDefer+")\n";
					}
				}
			}
		}
		return res+"	}";
	}
	

	private String getAllClocksFromTheModel(Constraint originalConstraint) {
		String res = "\n	Block "+OTHER_CLOCKS_BLOCK_NAME+"{\n";
		Model model = originalConstraint.getModel();
		TreeIterator<EObject> it = model.eAllContents();
		while(it.hasNext()){
			EObject eo = it.next();
			if ((eo instanceof Element)){
				for(Stereotype stereotype : ((Element)eo).getAppliedStereotypes()){
					EObject sa = ((Element)eo).getStereotypeApplication(stereotype);
					if (sa instanceof Clock){
						Clock c = (Clock)sa;
						String qualifiedName = null;
						if(c.getBase_Property() != null){
							qualifiedName=c.getBase_Property().getQualifiedName();
						}
						if(c.getBase_Event() != null){
							qualifiedName=c.getBase_Event().getQualifiedName();
						}
						if(c.getBase_InstanceSpecification() != null){
							qualifiedName=c.getBase_InstanceSpecification().getQualifiedName();
						}
						res+="		Clock "+qualifiedNamedToClockName(qualifiedName)+"\n";
						//TODO add this when the serializer will be ok
						//"->evt"+qualifiedName+"(\"model->"+c.getBase_Property().getQualifiedName()+"\")\n";
					}
				}
			}
		}
		return res+"}";
	}
	
	private String getAllConstraintsFromTheModel(Constraint originalConstraint) {
		String res = "\n	Block "+OTHER_CONSTRAINTS_BLOCK_NAME+"{\n";
		Model model = originalConstraint.getModel();
		TreeIterator<EObject> it = model.eAllContents();
		while(it.hasNext()){
			EObject eo = it.next();
			if ((eo instanceof Constraint) && !eo.equals(originalConstraint)){
				//for(Stereotype stereotype : ((Element)eo).getAppliedStereotypes()){
				//		EObject sa = ((Element)eo).getStereotypeApplication(stereotype);
				Constraint c = (Constraint)eo;
				//TODO check if it is a TimedConstraint ? language is CCSL ?
				res+="// "+c.getName()+"\n";
				res+=removeFirstSpaceandReturnChar(c.getSpecification().stringValue());
			}
		}
		return res+"}";
	}
	


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.xtext.gmf.glue.PopupEditorConfiguration#getTextToEdit(java.lang.Object)
	 */
	@Override
	public String getTextToEdit(Object editedObject) {
		String textToEdit = "";
		if(editedObject instanceof Constraint) {
			Constraint c= (Constraint)editedObject;
			textToEdit = c.getSpecification().stringValue();
		}
		return textToEdit;
	}
	
	
	//helper
	private String removeFirstSpaceandReturnChar(String stringValue) {
		return stringValue.replaceFirst("^[ \n]*", "");
	}
	
	private String getStereotypedElementQualifiedName(EObject sa) {
		String res="";

			if (sa instanceof Clock){
				Clock c = (Clock)sa;
				if(c.getBase_Property() != null){
					res=c.getBase_Property().getQualifiedName();
				}
				if(c.getBase_Event() != null){
					res=c.getBase_Event().getQualifiedName();
				}
				if(c.getBase_InstanceSpecification() != null){
					res=c.getBase_InstanceSpecification().getQualifiedName();
				}
			}
			if (sa instanceof TimedProcessing){
				TimedProcessing tp = (TimedProcessing)sa;
				if(tp.getBase_Behavior() != null){
					res=tp.getBase_Behavior().getQualifiedName();
				}
				if(tp.getBase_Action() != null){
					res=tp.getBase_Action().getQualifiedName();
				}
				if(tp.getBase_Message() != null){
					res=tp.getBase_Message().getQualifiedName();
				}
			}
						
		return res;
	}

	private String qualifiedNamedToClockName(String qualifiedName){
		return qualifiedName.replaceAll("::", "_");
	}
	

	protected class UpdateUMLConstraintCommand extends org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand {

		/** The message. */
		private Constraint constraint;
		
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand#doExecuteWithResult(org.eclipse.core.runtime.IProgressMonitor
		 * , org.eclipse.core.runtime.IAdaptable)
		 */
		/**
		 * @see org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand#doExecuteWithResult(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.core.runtime.IAdaptable)
		 *
		 * @param arg0
		 * @param arg1
		 * @return
		 * @throws ExecutionException
		 */
		
		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1)  {
			LiteralString spec = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createLiteralString();
			spec.setValue(constraintBody);
			this.constraint.setSpecification(spec);
			return CommandResult.newOKCommandResult(constraint.getSpecification());
		}

		
		/**
		 * Instantiates a new update uml constraint command.
		 *
		 * @param message the message
		 * @throws ServiceException 
		 */
		public UpdateUMLConstraintCommand(TransactionalEditingDomain domain, Constraint newConstraint) throws ServiceException {
			super(domain, "Constraint Update", getWorkspaceFiles(newConstraint));
			this.constraint = newConstraint;
		}

	}
	
	
	
}



