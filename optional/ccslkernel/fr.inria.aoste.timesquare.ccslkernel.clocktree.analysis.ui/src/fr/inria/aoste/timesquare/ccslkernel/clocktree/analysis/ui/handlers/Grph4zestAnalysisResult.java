/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.clocktree.analysis.ui.handlers;

/**
 * This is a way to represent Grph by using ZEST
 * 
 * 
 * @author Julien DeAntoni
 * 
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.eclipse.draw2d.Label;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IGraphContentProvider;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.viewers.IZoomableWorkbenchPart;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;

import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.CoincidentClocks;
import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.Edge;
import grph.oo.ObjectGrph;
import grph.oo.ObjectPath;

public class Grph4zestAnalysisResult implements IZoomableWorkbenchPart {

	
	
	public static class ClockTreeFilter extends ViewerFilter {

		public ClockTreeFilter() {
			super();
		}

		@Override
		public boolean select(Viewer viewer, Object parentElement, Object element) {
			// System.out.println("F+"+element);
			if ( element instanceof Edge)
			{
				
			}
			if ( element instanceof CoincidentClocks)
			{
				
			}
			return true;
		}
	}

	public static class GraphContentProvider implements IGraphContentProvider {// ,INestedContentProvider{

		public GraphContentProvider() {
			super();			
		}

		private ObjectGrph<CoincidentClocks, Edge> _og = null;

		@Override
		public void dispose() {		

		}

		@SuppressWarnings("unchecked")
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			_og = null;
			if (newInput instanceof ObjectGrph) {
				_og = (ObjectGrph<CoincidentClocks, Edge>) newInput;
			}

		}

		@Override
		public Object getSource(Object rel) {
			if (rel instanceof Edge) {
				if (_og != null)
					return _og.getDirectedSimpleEdgeTail((Edge) rel);
			}
			if (rel instanceof CoincidentClocks) {
				return rel;
			}
			return null;
		}

		@Override
		public Object getDestination(Object rel) {
			if (rel instanceof Edge) {
				if (_og != null)
					return _og.getDirectedSimpleEdgeHead((Edge) rel);
			}
			return null;
		}

		@Override
		public Object[] getElements(Object input) {
			if (input instanceof ObjectGrph) {
				Collection<CoincidentClocks> tmp = _og.getVertices();
				Collection<Object> lst = new HashSet<Object>(tmp);
				// add vertex for vertex whithotu edge)
				lst.addAll(_og.getEdges());
				return lst.toArray(new Object[lst.size()]);
			}
			return new Object[] {};
		}
	}

	public static class LabellingProvider implements IBaseLabelProvider, ISelfStyleProvider {

		private ArrayList<ArrayList<ObjectPath>> _setOfTriangles=null;
		private ArrayList<List<Object>> _deadPath = null;
		public LabellingProvider(ArrayList<ArrayList<ObjectPath>> setOfTriangles, ArrayList<List<Object>> deadPath) {
			super();
			 _setOfTriangles = setOfTriangles;
			 _deadPath  = deadPath;
			
		}

		@Override
		public void addListener(ILabelProviderListener listener) {

		}

		@Override
		public void dispose() {

		}

		@Override
		public boolean isLabelProperty(Object element, String property) {

			return true;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {

		}

		@Override
		public void selfStyleConnection(Object element, GraphConnection c) {
			if (element instanceof Edge) {
				boolean inPath=_deadPath.stream().anyMatch(dp -> dp.stream().anyMatch(t -> t.toString().replaceFirst(".*=", "").compareTo(c.getDestination().getData().toString().replaceFirst(".*=","")) == 0))
								&& 
							   _deadPath.stream().anyMatch(dp -> dp.stream().anyMatch(t -> t.toString().replaceFirst(".*=", "").compareTo(c.getSource().getData().toString().replaceFirst(".*=","")) == 0));
				boolean inSubF = false;
				if(_setOfTriangles != null){
					for(ArrayList<ObjectPath> triangle : _setOfTriangles){
						for(int i = 0; i < triangle.size(); i++){
							ObjectPath op = triangle.get(i);
							if(op.getEdges().contains(element)){
								inPath = true;
								if (i == 0){inSubF = true;}
							}
						}
					}
				}
				
				Edge e = (Edge) element;
				Color color = e.getColor(null);
				c.setCurveDepth(0);
				c.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
				c.setText(e.getLabel());
				if(inPath){
					c.setLineWidth(6);
					RGB newRGB = color.getRGB();
					newRGB.red = 255;
				//	newRGB.blue/=2;
					newRGB.green/=2;
					Color redified = new Color(null, newRGB);
					c.setLineColor(redified);
					if (inSubF){
						c.setLineStyle(2);
					}
					
				}else{
					c.setLineWidth(1);
					c.setLineStyle(2);
//					c.setLineColor(color);
				}
				
//				for(List<Object> eList : _deadPath) {
//					if (eList.contains(e)) {
//						c.setLineWidth(8);
//						RGB newRGB = color.getRGB();
//						newRGB.red = 0;
//						newRGB.blue=0;
//						newRGB.green=0;
//						Color redified = new Color(null, newRGB);
//						c.setLineColor(redified);
//					}
//				}
			}

		}

		@Override
		public void selfStyleNode(Object element, GraphNode node) {
			boolean inPath=false;
			if(_setOfTriangles != null){
				for(ArrayList<ObjectPath> triangle :_setOfTriangles){
					for(ObjectPath op : triangle){
						if(op.getVertices().contains(element)){
							inPath = true;
						}
					}
				}
			}
			
			// GraphNode n = new GraphNode(_g, SWT.NONE, v.getVertexLabel());
			if (element instanceof CoincidentClocks) {
				CoincidentClocks v = (CoincidentClocks) element;
				node.setText(v.getVertexLabel());
				Color color = v.getEdgeColor(null);

				if(inPath){
					RGB newRGB = color.getRGB();
					newRGB.red = 255;
					newRGB.blue=0;
					newRGB.green=0;
					Color redified = new Color(null, newRGB);
					node.setBorderColor(redified);
				}else{
					node.setBorderColor(color);
				}

				Label fig = new Label(v.getLongVertexLabel());
				node.setTooltip(fig);
			}
		}

	}

	private ObjectGrph<CoincidentClocks, Edge> _og = new ObjectGrph<CoincidentClocks, Edge>();
	private Graph _g;
	private GraphViewer _gv = null;


	public Grph4zestAnalysisResult(ObjectGrph<CoincidentClocks, Edge> g, Composite parent) {
		this(parent);
		setGrph( g);
	}

	public Grph4zestAnalysisResult(Composite parent) {
		set_gv(new GraphViewer(parent, SWT.NONE));
		_g = get_gv().getGraphControl();
		_gv.setContentProvider(new GraphContentProvider());
		_gv.setLabelProvider(new LabellingProvider(null, null));
		_gv.addFilter(new ClockTreeFilter());
	}

	public void setGrph(ObjectGrph<CoincidentClocks, Edge> g) {
		dispose();
		_og = g;
		get_gv().setInput(g);	
	}

	public ObjectGrph<CoincidentClocks, Edge> getGrph() {
		return _og;
	}

	public Graph getZestGraph() {
		return _g;
	}

	@Override
	public GraphViewer getZoomableViewer() {
		return get_gv();
	}

	public void dispose() {
		try {
			get_gv().setInput(new Object());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public GraphViewer get_gv() {
		return _gv;
	}

	public void set_gv(GraphViewer _gv) {
		this._gv = _gv;
	}

	/*	public void createVertex(){

			for(CoincidentClocks v : _og.vertexSet()){
			//	_gv.addNode(v);
				
				GraphNode n = new GraphNode(_g, SWT.NONE, v.getVertexLabel());
				Color color =  v.getEdgeColor(_d);
				n.setBorderColor(color);
				
				Label fig = new Label(v.getLongVertexLabel());
				n.setTooltip(fig);
				_gNodes.put(v, n);
				_listItem.add(n);
			}
		}
		
		public void createEdges(){
			for(Edge e : _og.edgeSet()){
				
				CoincidentClocks dest= _og.getEdgeTarget(e);
				CoincidentClocks source= _og.getEdgeSource(e);
				//_gv.addRelationship(e, source, dest);
				GraphConnection c = new GraphConnection(_g, SWT.NONE, _gNodes.get(source), _gNodes.get(dest));
				
				Color color = e.getColor(_d);
				c.setLineColor(color);
				c.setConnectionStyle(Graphics.LINE_DASH);
				c.setText(e.getLabel());
				c.setWeight(e.getSize());
				_gEdges.put(e,c);
				_listItem.add(0,c);
			}
		}*/

}