/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.clocktree.analysis.ui.handlers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.ClockTreeConstructor;
import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.CoincidentClocks;
import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.Edge;
import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.EdgeKind;
import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.GrphClockTreeConstructor;
import grph.oo.ObjectGrph;
import grph.oo.ObjectPath;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ClockGraphHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public ClockGraphHandler() {
	}

	private IFile ccslFile;
	private ClockTreeConstructor clockTreegenerator = null;
	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			if (((IStructuredSelection) selection).size() == 1) {
				Object selected = ((IStructuredSelection) selection).getFirstElement();
				if (selected instanceof IFile) {
					ccslFile = (IFile) selected;
					String filename = ccslFile.getFullPath().toString();
					clockTreegenerator = new GrphClockTreeConstructor();//CCSLKernelClockTreeConstructor();
					ObjectGrph<CoincidentClocks, Edge> g = clockTreegenerator.createClockGrphDag("platform:/resource"
							+ filename);

					
					
					Set<String> patternSubFree = new HashSet<String>();
					patternSubFree.add("(<(=)?)*| (((sub_c|sub_f|seq|condition)+)(<(=)?)+)*     (sub_c|seq|condition)*(sub_f)+(sub_c|sub_f|seq|condition)*");

					Set<String> patternPrecedes = new HashSet<String>();
					patternPrecedes.add("<+(<(=)?)*| (((sub_c|sub_f|seq|condition)+)(<(=)?)+)*"); 
					
					Set<String> patternPrecedes2 = new HashSet<String>();
					patternPrecedes2.add("<+(<(=)?)*"); 
					
					Set<String> patternSubC = new HashSet<String>();
					patternSubC.add("(<(=)?)*(sub_c|seq|condition)+");
					
					ArrayList<List<Object>> deadPath = new ArrayList<java.util.List<Object>>();
					Set<ObjectPath> cycles = g.getAllCycles();
					for (ObjectPath p :cycles) {
						for(Object e : p.getEdges()) {
							if (((Edge) e).ek != EdgeKind.CAUSES && ((Edge) e).ek != EdgeKind.PRECEDES) {
								continue;
							}
							deadPath.add(p.getVertices());
						}
					}
					
					Map<String, Set<ObjectPath>> matchedSubF = g.findMatchingPaths(patternSubFree);
					Map<String, Set<ObjectPath>> matchedPrecedes = g.findMatchingPaths(patternPrecedes);
					Map<String, Set<ObjectPath>> matchedPrecedes2 = g.findMatchingPaths(patternPrecedes2);
					Map<String, Set<ObjectPath>> matchedSubC = g.findMatchingPaths(patternSubC);
//					for(Set<ObjectPath> pset : matchedPrecedes2.values()) {
//						for(ObjectPath p : pset){
//							for(Object v : p.getVertices()) {
//								if(p.getVertices().contains(v)) {
//									List tmp = p.getVertices();
//									tmp.addAll(p.getEdges());
//									deadPath.add(tmp);
//								}
//							}
//						}
//					}
					if(deadPath.size()>0) {
						System.out.println("local deadlock found !" + deadPath.get(0));
					}
					
					ArrayList<ArrayList<ObjectPath>> subFSubCCouples = getSubfSubCCouples(matchedSubF, matchedSubC);
					ArrayList<ArrayList<ObjectPath>> setOfTriangles = new ArrayList<ArrayList<ObjectPath>>();
					if (subFSubCCouples.size() > 0){
						setOfTriangles = getSetOfTriangles(matchedPrecedes, subFSubCCouples);
					}
					if (setOfTriangles.size() > 0){
						System.out.println("potential deadlock found !");
						
					}
						ClockGraphAnalysisView analysisView = ClockGraphAnalysisView.getCourant(setOfTriangles, deadPath);
						//analysisView.setSetOfTriangle(setOfTriangles);
						analysisView.display( g, filename);

				}
			}
		}
		return null;
	}
	private ArrayList<ArrayList<ObjectPath>> getSetOfTriangles(Map<String, Set<ObjectPath>> matchedPrecedes, ArrayList<ArrayList<ObjectPath>> subFSubCCouples) {
		ArrayList<ArrayList<ObjectPath>> setOfTriangles =new ArrayList<ArrayList<ObjectPath>>();
		Object inFree = null;
		for(Set<ObjectPath> setOfpathPrecedes : matchedPrecedes.values()){
			for(ObjectPath pathPrecedes : setOfpathPrecedes){
				boolean foundInSubF = false;
				for(Object subCObject : pathPrecedes.getVertices()){
					for(ArrayList<ObjectPath> couple : subFSubCCouples){
						ArrayList<Object> allVerticesButFirst = new ArrayList<Object>(couple.get(0).getVertices());
						allVerticesButFirst.remove(0);
						if (allVerticesButFirst.contains(subCObject)){
							foundInSubF = true;
							inFree =subCObject;
						}
					}
				}
				if(foundInSubF){
					for(Object precedesObject : pathPrecedes.getVertices()){
						for(ArrayList<ObjectPath> couple : subFSubCCouples){
							if (couple.get(1).getVertices().contains(precedesObject)){
								if (pathPrecedes.getVertices().indexOf(precedesObject) > pathPrecedes.getVertices().indexOf(inFree)){
									ArrayList<ObjectPath> triangle = new ArrayList<ObjectPath>(couple);
									triangle.add(pathPrecedes);
									setOfTriangles.add(triangle);
								}
							}
						}
					}
				}
			}
		}
		return setOfTriangles;
	}
	
	
	private ArrayList<ArrayList<ObjectPath>> getSubfSubCCouples(Map<String, Set<ObjectPath>> matchedSubF, Map<String, Set<ObjectPath>> matchedSubC) {
		
	
		ArrayList<ArrayList<ObjectPath>> SubFPrecedesCouple = new ArrayList<ArrayList<ObjectPath>>();
		for (Set<ObjectPath> setOfpathSubF : matchedSubF.values()) {
			for(ObjectPath pathSubF: setOfpathSubF){
				for(Set<ObjectPath> setOfPathSubC : matchedSubC.values()){
					for(ObjectPath pathSubC : setOfPathSubC){
						//check not a subPath
						boolean notSubPath = false;
						for(Object vertex : pathSubC.getVertices()){
							if (! pathSubF.getVertices().contains(vertex)){
								notSubPath = true;
							}
						}
						
						if(notSubPath){
							for(Object vertex : pathSubC.getVertices()){
								//remove the last of subF to avoid "chains of patterns"
								ArrayList<ObjectPath> pathSubFVerticesWithoutLast = new ArrayList<ObjectPath>(pathSubF.getVertices());
								pathSubFVerticesWithoutLast.remove(pathSubFVerticesWithoutLast.get(pathSubFVerticesWithoutLast.size()-1));
								if (pathSubFVerticesWithoutLast.contains(vertex)){
									//here we have a common object between a sub_f and an sub_c
									ArrayList<ObjectPath> couple = new ArrayList<ObjectPath>();
									couple.add(pathSubF);
									couple.add(pathSubC);
									SubFPrecedesCouple.add(couple);
								}
							}
						}
					
					/*if (pathSubF.getVertices().get(0).equals(pathSubC.getVertices().get(0))){
						//here we have a common object between a sub_f and an sub_c
						ArrayList<ObjectPath> couple = new ArrayList<ObjectPath>();
						couple.add(pathSubF);
						couple.add(pathSubC);
						SubFPrecedesCouple.add(couple);
					}
					*/
					}
				}
			}
		}
		return SubFPrecedesCouple;
	}
}
