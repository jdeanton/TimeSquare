/** Vcd.g
   Author: Frederic Mallet
   @SuppressWarnings("unused")
*/
grammar Vcd;

options {
  output = AST;
  ASTLabelType = CommonTree;
}

tokens { RVECTOR; BVECTOR; }

@parser::header {package fr.inria.aoste.timesquare.vcd.antlr;}

@lexer::header {package fr.inria.aoste.timesquare.vcd.antlr;} 

vcd_definitions
	:	vcd_declarations simulation_command*;

vcd_declarations
	:	vcd_declaration_date vcd_declaration_version vcd_declaration_timescale? (declaration_command | vcd_comment)* vcd_declaration_enddefinitions vcd_comment*;
	
simulation_command
	:	//vcd_declaration_comment!
	//|	
	SIMULATION_TIME^ (vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change | vcd_comment)*;

vcd_simulation_dumpall
	:	DUMPALL^ value_change* END;
vcd_simulation_dumpvars
	:	DUMPVARS^ value_change* END;
vcd_simulation_dumpon
	:	DUMPON^ value_change* END;
vcd_simulation_dumpoff
	:	DUMPOFF^ value_change* END;
	
declaration_command
	:	vcd_declaration_scope^ declaration_command* vcd_declaration_upscope!
	//|	vcd_declaration_comment!
	|	vcd_declaration_vars;

vcd_declaration_enddefinitions
	:	ENDDEFINITIONS^ END;

//vcd_declaration_comment
//	:	COMMENT^ ;//.* END;
	
vcd_declaration_date
	:	DATE^ .+ END;

vcd_declaration_scope
	:	SCOPE^ SCOPE_TYPE IDENT END!;

vcd_declaration_timescale
	:	TIMESCALE^ DECIMAL_NUMBER TIME_UNIT END;

vcd_declaration_upscope
	:	UPSCOPE^ END!;

vcd_declaration_vars
	:	VAR^ VAR_TYPE DECIMAL_NUMBER IDENTIFIER_CODE . END;	

vcd_declaration_version
	:	VERSION^ (~END )* END;

value_change
	: real_vector_value_change | bin_vector_value_change | SCALAR_VALUE_CHANGE;

vcd_comment	:
	COMMENT .* END;
	 
real_vector_value_change
	:	REAL_VECTOR IDENTIFIER_CODE -> ^(RVECTOR REAL_VECTOR IDENTIFIER_CODE);

bin_vector_value_change
	:	BINARY_VECTOR IDENTIFIER_CODE -> ^(BVECTOR BINARY_VECTOR IDENTIFIER_CODE);

SIMULATION_TIME
	:	'#' DECIMAL_NUMBER;
	
VAR_TYPE	:	'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1'
		|	'tri'   | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor';
		
SCOPE_TYPE 	:	'begin' | 'fork' | 'function' |	'module' | 'task';
	
TIME_UNIT  	:	's' | 'ms' | 'us' | 'ns' | 'ps'	| 'fs';

REFERENCE
	:	IDENT '[' DECIMAL_NUMBER ']' 
	|	IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']';
	
ENDDEFINITIONS
	:	'$enddefinitions';
	
// status  constraint
DATE	:	'$date';
SCOPE   :	'$scope';
TIMESCALE:	'$timescale';
UPSCOPE	:	'$upscope';
END	:	'$end';
VAR	:	'$var';
VERSION	:	'$version';
DUMPALL	:	'$dumpall';
DUMPON	:	'$dumpon';
DUMPOFF	:	'$dumpoff';
DUMPVARS:	'$dumpvars';
COMMENT :	'$comment';
	
SCALAR_VALUE_CHANGE
	:	(BIT|VALUE) IDENTIFIER_CODE;

DECIMAL_NUMBER
	:	'-'? DIGIT+;
REAL_VECTOR 
	:	('r'|'R') '-'? DIGIT* '.' DIGIT+; 

BINARY_VECTOR	
	:	('b'|'B') (BIT|VALUE)+;
IDENT	:	(LETTER|'_') (LETTER|DIGIT|'_'|'@'|'.'|'-')*;
VERSION_TEXT
	:	(LETTER|DIGIT) (LETTER|DIGIT|IDENT_CHAR|'.')+;
IDENTIFIER_CODE
	:	IDENT_CHAR (DIGIT | LETTER | IDENT_CHAR |'.' |'_')*;

fragment
BIT 	:	 '0' | '1';

fragment
DIGIT   :	'0'..'9';

fragment
VALUE	:	'x' | 'X' | 'z' | 'Z';

fragment
LETTER	:	'a'..'z' | 'A'..'Z';
fragment
IDENT_CHAR  :	 '!'..'-' | '/' | ':'..'@' | '['..'^' | '`' | '{'..'~';
	
WS	:	(' '|'\t'|
		(('\n'| '\r')  { /*newline();*/}   )
		)+ { skip(); };
//STRING : requis pour gere les pragmas DO NOT REMOVE ( pragma alias ,xmi , relation ) ( peut contenir des espaces)
STRING 	: 	'"' .* '"' ;
