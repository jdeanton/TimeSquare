/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
// $ANTLR 3.2 Sep 23, 2009 12:02:23 D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g 2010-06-09 23:42:07
package fr.inria.aoste.timesquare.vcd.antlr;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class VcdLexer extends Lexer {
    public static final int REAL_VECTOR=27;
    public static final int VERSION_TEXT=35;
    public static final int UPSCOPE=20;
    public static final int LETTER=33;
    public static final int IDENT_CHAR=34;
    public static final int VAR_TYPE=22;
    public static final int REFERENCE=29;
    public static final int VERSION=24;
    public static final int VALUE=31;
    public static final int EOF=-1;
    public static final int TIME_UNIT=19;
    public static final int SCALAR_VALUE_CHANGE=25;
    public static final int DUMPOFF=11;
    public static final int ENDDEFINITIONS=12;
    public static final int SIMULATION_TIME=6;
    public static final int DUMPON=10;
    public static final int WS=36;
    public static final int SCOPE=14;
    public static final int BVECTOR=5;
    public static final int DUMPVARS=9;
    public static final int DUMPALL=7;
    public static final int TIMESCALE=17;
    public static final int IDENTIFIER_CODE=23;
    public static final int RVECTOR=4;
    public static final int IDENT=16;
    public static final int BIT=30;
    public static final int VAR=21;
    public static final int DIGIT=32;
    public static final int SCOPE_TYPE=15;
    public static final int DATE=13;
    public static final int END=8;
    public static final int COMMENT=26;
    public static final int BINARY_VECTOR=28;
    public static final int DECIMAL_NUMBER=18;

    // delegates
    // delegators

    public VcdLexer() {;} 
    public VcdLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public VcdLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g"; }

    // $ANTLR start "SIMULATION_TIME"
    public final void mSIMULATION_TIME() throws RecognitionException {
        try {
            int _type = SIMULATION_TIME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:80:2: ( '#' DECIMAL_NUMBER )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:80:4: '#' DECIMAL_NUMBER
            {
            match('#'); 
            mDECIMAL_NUMBER(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SIMULATION_TIME"

    // $ANTLR start "VAR_TYPE"
    public final void mVAR_TYPE() throws RecognitionException {
        try {
            int _type = VAR_TYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:10: ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' )
            int alt1=17;
            alt1 = dfa1.predict(input);
            switch (alt1) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:12: 'event'
                    {
                    match("event"); 


                    }
                    break;
                case 2 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:22: 'integer'
                    {
                    match("integer"); 


                    }
                    break;
                case 3 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:34: 'parameter'
                    {
                    match("parameter"); 


                    }
                    break;
                case 4 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:48: 'real'
                    {
                    match("real"); 


                    }
                    break;
                case 5 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:57: 'realtime'
                    {
                    match("realtime"); 


                    }
                    break;
                case 6 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:70: 'reg'
                    {
                    match("reg"); 


                    }
                    break;
                case 7 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:78: 'supply0'
                    {
                    match("supply0"); 


                    }
                    break;
                case 8 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:82:90: 'supply1'
                    {
                    match("supply1"); 


                    }
                    break;
                case 9 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:5: 'tri'
                    {
                    match("tri"); 


                    }
                    break;
                case 10 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:15: 'triand'
                    {
                    match("triand"); 


                    }
                    break;
                case 11 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:26: 'trior'
                    {
                    match("trior"); 


                    }
                    break;
                case 12 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:36: 'trireg'
                    {
                    match("trireg"); 


                    }
                    break;
                case 13 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:47: 'tri0'
                    {
                    match("tri0"); 


                    }
                    break;
                case 14 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:56: 'tri1'
                    {
                    match("tri1"); 


                    }
                    break;
                case 15 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:65: 'wand'
                    {
                    match("wand"); 


                    }
                    break;
                case 16 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:74: 'wire'
                    {
                    match("wire"); 


                    }
                    break;
                case 17 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:83:83: 'wor'
                    {
                    match("wor"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "VAR_TYPE"

    // $ANTLR start "SCOPE_TYPE"
    public final void mSCOPE_TYPE() throws RecognitionException {
        try {
            int _type = SCOPE_TYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:85:13: ( 'begin' | 'fork' | 'function' | 'module' | 'task' )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 'b':
                {
                alt2=1;
                }
                break;
            case 'f':
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2=='o') ) {
                    alt2=2;
                }
                else if ( (LA2_2=='u') ) {
                    alt2=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;
                }
                }
                break;
            case 'm':
                {
                alt2=4;
                }
                break;
            case 't':
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:85:15: 'begin'
                    {
                    match("begin"); 


                    }
                    break;
                case 2 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:85:25: 'fork'
                    {
                    match("fork"); 


                    }
                    break;
                case 3 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:85:34: 'function'
                    {
                    match("function"); 


                    }
                    break;
                case 4 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:85:47: 'module'
                    {
                    match("module"); 


                    }
                    break;
                case 5 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:85:58: 'task'
                    {
                    match("task"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SCOPE_TYPE"

    // $ANTLR start "TIME_UNIT"
    public final void mTIME_UNIT() throws RecognitionException {
        try {
            int _type = TIME_UNIT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:87:13: ( 's' | 'ms' | 'us' | 'ns' | 'ps' | 'fs' )
            int alt3=6;
            switch ( input.LA(1) ) {
            case 's':
                {
                alt3=1;
                }
                break;
            case 'm':
                {
                alt3=2;
                }
                break;
            case 'u':
                {
                alt3=3;
                }
                break;
            case 'n':
                {
                alt3=4;
                }
                break;
            case 'p':
                {
                alt3=5;
                }
                break;
            case 'f':
                {
                alt3=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:87:15: 's'
                    {
                    match('s'); 

                    }
                    break;
                case 2 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:87:21: 'ms'
                    {
                    match("ms"); 


                    }
                    break;
                case 3 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:87:28: 'us'
                    {
                    match("us"); 


                    }
                    break;
                case 4 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:87:35: 'ns'
                    {
                    match("ns"); 


                    }
                    break;
                case 5 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:87:42: 'ps'
                    {
                    match("ps"); 


                    }
                    break;
                case 6 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:87:49: 'fs'
                    {
                    match("fs"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "TIME_UNIT"

    // $ANTLR start "REFERENCE"
    public final void mREFERENCE() throws RecognitionException {
        try {
            int _type = REFERENCE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:90:2: ( IDENT '[' DECIMAL_NUMBER ']' | IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']' )
            int alt4=2;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:90:4: IDENT '[' DECIMAL_NUMBER ']'
                    {
                    mIDENT(); 
                    match('['); 
                    mDECIMAL_NUMBER(); 
                    match(']'); 

                    }
                    break;
                case 2 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:91:4: IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']'
                    {
                    mIDENT(); 
                    match('['); 
                    mDECIMAL_NUMBER(); 
                    match(':'); 
                    mDECIMAL_NUMBER(); 
                    match(']'); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "REFERENCE"

    // $ANTLR start "ENDDEFINITIONS"
    public final void mENDDEFINITIONS() throws RecognitionException {
        try {
            int _type = ENDDEFINITIONS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:94:2: ( '$enddefinitions' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:94:4: '$enddefinitions'
            {
            match("$enddefinitions"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ENDDEFINITIONS"

    // $ANTLR start "DATE"
    public final void mDATE() throws RecognitionException {
        try {
            int _type = DATE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:97:6: ( '$date' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:97:8: '$date'
            {
            match("$date"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DATE"

    // $ANTLR start "SCOPE"
    public final void mSCOPE() throws RecognitionException {
        try {
            int _type = SCOPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:98:9: ( '$scope' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:98:11: '$scope'
            {
            match("$scope"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SCOPE"

    // $ANTLR start "TIMESCALE"
    public final void mTIMESCALE() throws RecognitionException {
        try {
            int _type = TIMESCALE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:99:10: ( '$timescale' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:99:12: '$timescale'
            {
            match("$timescale"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "TIMESCALE"

    // $ANTLR start "UPSCOPE"
    public final void mUPSCOPE() throws RecognitionException {
        try {
            int _type = UPSCOPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:100:9: ( '$upscope' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:100:11: '$upscope'
            {
            match("$upscope"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "UPSCOPE"

    // $ANTLR start "END"
    public final void mEND() throws RecognitionException {
        try {
            int _type = END;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:101:5: ( '$end' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:101:7: '$end'
            {
            match("$end"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "END"

    // $ANTLR start "VAR"
    public final void mVAR() throws RecognitionException {
        try {
            int _type = VAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:102:5: ( '$var' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:102:7: '$var'
            {
            match("$var"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "VAR"

    // $ANTLR start "VERSION"
    public final void mVERSION() throws RecognitionException {
        try {
            int _type = VERSION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:103:9: ( '$version' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:103:11: '$version'
            {
            match("$version"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "VERSION"

    // $ANTLR start "DUMPALL"
    public final void mDUMPALL() throws RecognitionException {
        try {
            int _type = DUMPALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:104:9: ( '$dumpall' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:104:11: '$dumpall'
            {
            match("$dumpall"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DUMPALL"

    // $ANTLR start "DUMPON"
    public final void mDUMPON() throws RecognitionException {
        try {
            int _type = DUMPON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:105:8: ( '$dumpon' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:105:10: '$dumpon'
            {
            match("$dumpon"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DUMPON"

    // $ANTLR start "DUMPOFF"
    public final void mDUMPOFF() throws RecognitionException {
        try {
            int _type = DUMPOFF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:106:9: ( '$dumpoff' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:106:11: '$dumpoff'
            {
            match("$dumpoff"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DUMPOFF"

    // $ANTLR start "DUMPVARS"
    public final void mDUMPVARS() throws RecognitionException {
        try {
            int _type = DUMPVARS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:107:9: ( '$dumpvars' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:107:11: '$dumpvars'
            {
            match("$dumpvars"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DUMPVARS"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:108:9: ( '$comment' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:108:11: '$comment'
            {
            match("$comment"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "SCALAR_VALUE_CHANGE"
    public final void mSCALAR_VALUE_CHANGE() throws RecognitionException {
        try {
            int _type = SCALAR_VALUE_CHANGE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:111:2: ( ( BIT | VALUE ) IDENTIFIER_CODE )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:111:4: ( BIT | VALUE ) IDENTIFIER_CODE
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='1')||input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            mIDENTIFIER_CODE(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SCALAR_VALUE_CHANGE"

    // $ANTLR start "DECIMAL_NUMBER"
    public final void mDECIMAL_NUMBER() throws RecognitionException {
        try {
            int _type = DECIMAL_NUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:114:2: ( ( '-' )? ( DIGIT )+ )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:114:4: ( '-' )? ( DIGIT )+
            {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:114:4: ( '-' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='-') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:114:4: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:114:9: ( DIGIT )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:114:9: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DECIMAL_NUMBER"

    // $ANTLR start "REAL_VECTOR"
    public final void mREAL_VECTOR() throws RecognitionException {
        try {
            int _type = REAL_VECTOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:116:2: ( ( 'r' | 'R' ) ( '-' )? ( DIGIT )* '.' ( DIGIT )+ )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:116:4: ( 'r' | 'R' ) ( '-' )? ( DIGIT )* '.' ( DIGIT )+
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:116:14: ( '-' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='-') ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:116:14: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:116:19: ( DIGIT )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='0' && LA8_0<='9')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:116:19: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            match('.'); 
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:116:30: ( DIGIT )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='0' && LA9_0<='9')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:116:30: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "REAL_VECTOR"

    // $ANTLR start "BINARY_VECTOR"
    public final void mBINARY_VECTOR() throws RecognitionException {
        try {
            int _type = BINARY_VECTOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:119:2: ( ( 'b' | 'B' ) ( BIT | VALUE )+ )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:119:4: ( 'b' | 'B' ) ( BIT | VALUE )+
            {
            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:119:14: ( BIT | VALUE )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='0' && LA10_0<='1')||LA10_0=='X'||LA10_0=='Z'||LA10_0=='x'||LA10_0=='z') ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='1')||input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "BINARY_VECTOR"

    // $ANTLR start "IDENT"
    public final void mIDENT() throws RecognitionException {
        try {
            int _type = IDENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:120:7: ( ( LETTER | '_' ) ( LETTER | DIGIT | '_' | '@' | '.' | '-' )* )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:120:9: ( LETTER | '_' ) ( LETTER | DIGIT | '_' | '@' | '.' | '-' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:120:22: ( LETTER | DIGIT | '_' | '@' | '.' | '-' )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='-' && LA11_0<='.')||(LA11_0>='0' && LA11_0<='9')||(LA11_0>='@' && LA11_0<='Z')||LA11_0=='_'||(LA11_0>='a' && LA11_0<='z')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='-' && input.LA(1)<='.')||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='@' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IDENT"

    // $ANTLR start "VERSION_TEXT"
    public final void mVERSION_TEXT() throws RecognitionException {
        try {
            int _type = VERSION_TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:122:2: ( ( LETTER | DIGIT ) ( LETTER | DIGIT | IDENT_CHAR | '.' )+ )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:122:4: ( LETTER | DIGIT ) ( LETTER | DIGIT | IDENT_CHAR | '.' )+
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:122:19: ( LETTER | DIGIT | IDENT_CHAR | '.' )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='!' && LA12_0<='^')||(LA12_0>='`' && LA12_0<='~')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='!' && input.LA(1)<='^')||(input.LA(1)>='`' && input.LA(1)<='~') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "VERSION_TEXT"

    // $ANTLR start "IDENTIFIER_CODE"
    public final void mIDENTIFIER_CODE() throws RecognitionException {
        try {
            int _type = IDENTIFIER_CODE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:124:2: ( IDENT_CHAR ( DIGIT | LETTER | IDENT_CHAR | '.' | '_' )* )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:124:4: IDENT_CHAR ( DIGIT | LETTER | IDENT_CHAR | '.' | '_' )*
            {
            mIDENT_CHAR(); 
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:124:15: ( DIGIT | LETTER | IDENT_CHAR | '.' | '_' )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>='!' && LA13_0<='~')) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:
            	    {
            	    if ( (input.LA(1)>='!' && input.LA(1)<='~') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IDENTIFIER_CODE"

    // $ANTLR start "BIT"
    public final void mBIT() throws RecognitionException {
        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:127:6: ( '0' | '1' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='1') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "BIT"

    // $ANTLR start "DIGIT"
    public final void mDIGIT() throws RecognitionException {
        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:130:9: ( '0' .. '9' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:130:11: '0' .. '9'
            {
            matchRange('0','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "DIGIT"

    // $ANTLR start "VALUE"
    public final void mVALUE() throws RecognitionException {
        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:133:7: ( 'x' | 'X' | 'z' | 'Z' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:
            {
            if ( input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "VALUE"

    // $ANTLR start "LETTER"
    public final void mLETTER() throws RecognitionException {
        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:136:8: ( 'a' .. 'z' | 'A' .. 'Z' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "LETTER"

    // $ANTLR start "IDENT_CHAR"
    public final void mIDENT_CHAR() throws RecognitionException {
        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:138:13: ( '!' .. '-' | '/' | ':' .. '@' | '[' .. '^' | '`' | '{' .. '~' )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:
            {
            if ( (input.LA(1)>='!' && input.LA(1)<='-')||input.LA(1)=='/'||(input.LA(1)>=':' && input.LA(1)<='@')||(input.LA(1)>='[' && input.LA(1)<='^')||input.LA(1)=='`'||(input.LA(1)>='{' && input.LA(1)<='~') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "IDENT_CHAR"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:140:4: ( ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+ )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:140:6: ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+
            {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:140:6: ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+
            int cnt14=0;
            loop14:
            do {
                int alt14=4;
                switch ( input.LA(1) ) {
                case ' ':
                    {
                    alt14=1;
                    }
                    break;
                case '\t':
                    {
                    alt14=2;
                    }
                    break;
                case '\n':
                case '\r':
                    {
                    alt14=3;
                    }
                    break;

                }

                switch (alt14) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:140:7: ' '
            	    {
            	    match(' '); 

            	    }
            	    break;
            	case 2 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:140:11: '\\t'
            	    {
            	    match('\t'); 

            	    }
            	    break;
            	case 3 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:141:3: ( ( '\\n' | '\\r' ) )
            	    {
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:141:3: ( ( '\\n' | '\\r' ) )
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:141:4: ( '\\n' | '\\r' )
            	    {
            	    if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}

            	     /*newline();*/

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);

             skip(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:8: ( SIMULATION_TIME | VAR_TYPE | SCOPE_TYPE | TIME_UNIT | REFERENCE | ENDDEFINITIONS | DATE | SCOPE | TIMESCALE | UPSCOPE | END | VAR | VERSION | DUMPALL | DUMPON | DUMPOFF | DUMPVARS | COMMENT | SCALAR_VALUE_CHANGE | DECIMAL_NUMBER | REAL_VECTOR | BINARY_VECTOR | IDENT | VERSION_TEXT | IDENTIFIER_CODE | WS )
        int alt15=26;
        alt15 = dfa15.predict(input);
        switch (alt15) {
            case 1 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:10: SIMULATION_TIME
                {
                mSIMULATION_TIME(); 

                }
                break;
            case 2 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:26: VAR_TYPE
                {
                mVAR_TYPE(); 

                }
                break;
            case 3 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:35: SCOPE_TYPE
                {
                mSCOPE_TYPE(); 

                }
                break;
            case 4 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:46: TIME_UNIT
                {
                mTIME_UNIT(); 

                }
                break;
            case 5 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:56: REFERENCE
                {
                mREFERENCE(); 

                }
                break;
            case 6 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:66: ENDDEFINITIONS
                {
                mENDDEFINITIONS(); 

                }
                break;
            case 7 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:81: DATE
                {
                mDATE(); 

                }
                break;
            case 8 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:86: SCOPE
                {
                mSCOPE(); 

                }
                break;
            case 9 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:92: TIMESCALE
                {
                mTIMESCALE(); 

                }
                break;
            case 10 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:102: UPSCOPE
                {
                mUPSCOPE(); 

                }
                break;
            case 11 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:110: END
                {
                mEND(); 

                }
                break;
            case 12 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:114: VAR
                {
                mVAR(); 

                }
                break;
            case 13 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:118: VERSION
                {
                mVERSION(); 

                }
                break;
            case 14 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:126: DUMPALL
                {
                mDUMPALL(); 

                }
                break;
            case 15 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:134: DUMPON
                {
                mDUMPON(); 

                }
                break;
            case 16 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:141: DUMPOFF
                {
                mDUMPOFF(); 

                }
                break;
            case 17 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:149: DUMPVARS
                {
                mDUMPVARS(); 

                }
                break;
            case 18 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:158: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 19 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:166: SCALAR_VALUE_CHANGE
                {
                mSCALAR_VALUE_CHANGE(); 

                }
                break;
            case 20 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:186: DECIMAL_NUMBER
                {
                mDECIMAL_NUMBER(); 

                }
                break;
            case 21 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:201: REAL_VECTOR
                {
                mREAL_VECTOR(); 

                }
                break;
            case 22 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:213: BINARY_VECTOR
                {
                mBINARY_VECTOR(); 

                }
                break;
            case 23 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:227: IDENT
                {
                mIDENT(); 

                }
                break;
            case 24 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:233: VERSION_TEXT
                {
                mVERSION_TEXT(); 

                }
                break;
            case 25 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:246: IDENTIFIER_CODE
                {
                mIDENTIFIER_CODE(); 

                }
                break;
            case 26 :
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:1:262: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA1 dfa1 = new DFA1(this);
    protected DFA4 dfa4 = new DFA4(this);
    protected DFA15 dfa15 = new DFA15(this);
    static final String DFA1_eotS =
        "\21\uffff\1\31\1\33\15\uffff";
    static final String DFA1_eofS =
        "\40\uffff";
    static final String DFA1_minS =
        "\1\145\3\uffff\1\145\1\165\1\162\2\141\1\160\1\151\3\uffff\1\154"+
        "\1\uffff\1\160\1\60\1\164\1\154\10\uffff\1\171\1\60\2\uffff";
    static final String DFA1_maxS =
        "\1\167\3\uffff\1\145\1\165\1\162\1\157\1\147\1\160\1\151\3\uffff"+
        "\1\154\1\uffff\1\160\1\162\1\164\1\154\10\uffff\1\171\1\61\2\uffff";
    static final String DFA1_acceptS =
        "\1\uffff\1\1\1\2\1\3\7\uffff\1\17\1\20\1\21\1\uffff\1\6\4\uffff"+
        "\1\12\1\13\1\14\1\15\1\16\1\11\1\5\1\4\2\uffff\1\7\1\10";
    static final String DFA1_specialS =
        "\40\uffff}>";
    static final String[] DFA1_transitionS = {
            "\1\1\3\uffff\1\2\6\uffff\1\3\1\uffff\1\4\1\5\1\6\2\uffff\1"+
            "\7",
            "",
            "",
            "",
            "\1\10",
            "\1\11",
            "\1\12",
            "\1\13\7\uffff\1\14\5\uffff\1\15",
            "\1\16\5\uffff\1\17",
            "\1\20",
            "\1\21",
            "",
            "",
            "",
            "\1\22",
            "",
            "\1\23",
            "\1\27\1\30\57\uffff\1\24\15\uffff\1\25\2\uffff\1\26",
            "\1\32",
            "\1\34",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\35",
            "\1\36\1\37",
            "",
            ""
    };

    static final short[] DFA1_eot = DFA.unpackEncodedString(DFA1_eotS);
    static final short[] DFA1_eof = DFA.unpackEncodedString(DFA1_eofS);
    static final char[] DFA1_min = DFA.unpackEncodedStringToUnsignedChars(DFA1_minS);
    static final char[] DFA1_max = DFA.unpackEncodedStringToUnsignedChars(DFA1_maxS);
    static final short[] DFA1_accept = DFA.unpackEncodedString(DFA1_acceptS);
    static final short[] DFA1_special = DFA.unpackEncodedString(DFA1_specialS);
    static final short[][] DFA1_transition;

    static {
        int numStates = DFA1_transitionS.length;
        DFA1_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA1_transition[i] = DFA.unpackEncodedString(DFA1_transitionS[i]);
        }
    }

    class DFA1 extends DFA {

        public DFA1(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = DFA1_eot;
            this.eof = DFA1_eof;
            this.min = DFA1_min;
            this.max = DFA1_max;
            this.accept = DFA1_accept;
            this.special = DFA1_special;
            this.transition = DFA1_transition;
        }
        public String getDescription() {
            return "82:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );";
        }
    }
    static final String DFA4_eotS =
        "\10\uffff";
    static final String DFA4_eofS =
        "\10\uffff";
    static final String DFA4_minS =
        "\1\101\3\55\2\60\2\uffff";
    static final String DFA4_maxS =
        "\3\172\2\71\1\135\2\uffff";
    static final String DFA4_acceptS =
        "\6\uffff\1\1\1\2";
    static final String DFA4_specialS =
        "\10\uffff}>";
    static final String[] DFA4_transitionS = {
            "\32\1\4\uffff\1\1\1\uffff\32\1",
            "\2\2\1\uffff\12\2\6\uffff\33\2\1\3\3\uffff\1\2\1\uffff\32"+
            "\2",
            "\2\2\1\uffff\12\2\6\uffff\33\2\1\3\3\uffff\1\2\1\uffff\32"+
            "\2",
            "\1\4\2\uffff\12\5",
            "\12\5",
            "\12\5\1\7\42\uffff\1\6",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        public String getDescription() {
            return "89:1: REFERENCE : ( IDENT '[' DECIMAL_NUMBER ']' | IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']' );";
        }
    }
    static final String DFA15_eotS =
        "\1\uffff\1\27\4\37\1\51\10\37\1\27\1\37\1\102\1\27\1\102\3\37\2"+
        "\uffff\1\27\1\106\2\37\1\40\1\37\2\uffff\2\37\1\51\5\37\1\uffff"+
        "\6\37\1\126\2\37\1\51\1\37\3\51\3\132\7\27\1\uffff\2\102\2\uffff"+
        "\1\37\2\40\3\37\1\160\1\161\1\37\1\160\3\37\1\160\1\37\1\uffff\3"+
        "\37\1\uffff\6\132\11\27\1\37\1\40\1\105\2\37\1\160\2\uffff\4\37"+
        "\2\160\1\u0095\2\160\1\37\1\u0095\2\37\2\132\1\105\1\u009e\5\27"+
        "\1\u00a4\2\27\1\160\2\40\5\37\1\160\1\37\1\uffff\1\u0095\2\37\4"+
        "\132\1\27\1\uffff\1\u00b4\4\27\1\uffff\2\27\1\105\4\37\2\160\1\37"+
        "\1\u0095\1\132\2\105\1\27\1\uffff\3\27\1\u00ca\4\27\1\160\2\37\2"+
        "\160\1\37\2\132\2\27\1\u00d5\2\27\1\uffff\4\27\1\37\1\160\1\u0095"+
        "\1\105\1\27\1\u00de\1\uffff\1\u00df\2\27\1\u00e2\1\u00e3\1\u00e4"+
        "\1\160\1\27\2\uffff\1\u00e6\1\27\3\uffff\1\27\1\uffff\1\u00e9\1"+
        "\27\1\uffff\3\27\1\u00ee\1\uffff";
    static final String DFA15_eofS =
        "\u00ef\uffff";
    static final String DFA15_minS =
        "\1\11\1\55\15\41\1\143\2\41\1\60\3\41\1\55\2\uffff\1\60\3\41\2"+
        "\55\2\uffff\10\41\1\uffff\21\41\1\156\1\141\1\143\1\151\1\160\1"+
        "\141\1\157\1\uffff\2\41\2\uffff\1\41\2\60\14\41\1\uffff\3\41\1\uffff"+
        "\3\41\1\55\2\41\1\144\1\164\1\155\1\157\1\155\1\163\2\162\1\155"+
        "\1\41\1\55\4\41\2\uffff\15\41\1\55\3\41\1\145\2\160\1\145\1\143"+
        "\1\41\1\163\1\155\1\41\2\60\7\41\1\uffff\3\41\2\60\2\41\1\145\1"+
        "\uffff\1\41\1\141\1\145\1\163\1\157\1\uffff\1\151\1\145\11\41\1"+
        "\55\2\41\1\146\1\uffff\1\154\1\146\1\141\1\41\1\143\1\160\1\157"+
        "\1\156\6\41\2\60\1\151\1\154\1\41\1\146\1\162\1\uffff\1\141\1\145"+
        "\1\156\1\164\4\41\1\156\1\41\1\uffff\1\41\1\163\1\154\4\41\1\151"+
        "\2\uffff\1\41\1\145\3\uffff\1\164\1\uffff\1\41\1\151\1\uffff\1\157"+
        "\1\156\1\163\1\41\1\uffff";
    static final String DFA15_maxS =
        "\1\176\1\71\15\176\1\166\2\176\1\71\3\176\1\172\2\uffff\1\71\3"+
        "\176\1\71\1\172\2\uffff\10\176\1\uffff\21\176\1\156\1\165\1\143"+
        "\1\151\1\160\1\145\1\157\1\uffff\2\176\2\uffff\1\176\1\71\1\135"+
        "\14\176\1\uffff\3\176\1\uffff\3\176\1\172\2\176\1\144\1\164\1\155"+
        "\1\157\1\155\1\163\2\162\1\155\1\176\1\71\4\176\2\uffff\15\176\1"+
        "\71\3\176\1\145\2\160\1\145\1\143\1\176\1\163\1\155\1\176\1\71\1"+
        "\135\7\176\1\uffff\3\176\1\71\1\135\2\176\1\145\1\uffff\1\176\1"+
        "\166\1\145\1\163\1\157\1\uffff\1\151\1\145\11\176\1\71\2\176\1\146"+
        "\1\uffff\1\154\1\156\1\141\1\176\1\143\1\160\1\157\1\156\6\176\1"+
        "\71\1\135\1\151\1\154\1\176\1\146\1\162\1\uffff\1\141\1\145\1\156"+
        "\1\164\4\176\1\156\1\176\1\uffff\1\176\1\163\1\154\4\176\1\151\2"+
        "\uffff\1\176\1\145\3\uffff\1\164\1\uffff\1\176\1\151\1\uffff\1\157"+
        "\1\156\1\163\1\176\1\uffff";
    static final String DFA15_acceptS =
        "\27\uffff\1\31\1\32\6\uffff\1\27\1\30\10\uffff\1\4\30\uffff\1\24"+
        "\2\uffff\1\5\1\1\17\uffff\1\26\3\uffff\1\23\25\uffff\1\2\1\25\43"+
        "\uffff\1\3\10\uffff\1\13\5\uffff\1\14\17\uffff\1\7\25\uffff\1\10"+
        "\12\uffff\1\17\10\uffff\1\16\1\20\2\uffff\1\12\1\15\1\22\1\uffff"+
        "\1\21\2\uffff\1\11\4\uffff\1\6";
    static final String DFA15_specialS =
        "\u00ef\uffff}>";
    static final String[] DFA15_transitionS = {
            "\2\30\2\uffff\1\30\22\uffff\1\30\2\27\1\1\1\17\10\27\1\22\1"+
            "\uffff\1\27\2\21\10\23\7\27\1\25\1\24\17\25\1\20\5\25\1\16\1"+
            "\25\1\16\4\27\1\26\1\27\1\25\1\11\2\25\1\2\1\12\2\25\1\3\3\25"+
            "\1\13\1\15\1\25\1\4\1\25\1\5\1\6\1\7\1\14\1\25\1\10\1\16\1\25"+
            "\1\16\4\27",
            "\1\31\2\uffff\12\32",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\25\34"+
            "\1\33\4\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\15\34"+
            "\1\41\14\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\1\42"+
            "\21\34\1\43\7\34\4\40",
            "\14\40\1\45\1\47\1\40\12\46\6\40\33\34\1\35\3\40\1\36\1\40"+
            "\4\34\1\44\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\24\34"+
            "\1\50\5\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\1\53"+
            "\20\34\1\52\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\1\54"+
            "\7\34\1\55\5\34\1\56\13\34\4\40",
            "\14\40\2\34\1\40\2\60\10\34\6\40\30\34\1\60\1\34\1\60\1\35"+
            "\3\40\1\36\1\40\4\34\1\57\22\34\1\60\1\34\1\60\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\16\34"+
            "\1\61\3\34\1\63\1\34\1\62\5\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\16\34"+
            "\1\64\3\34\1\65\7\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\22\34"+
            "\1\66\7\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\22\34"+
            "\1\67\7\34\4\40",
            "\14\72\1\70\1\34\1\72\12\34\6\72\1\70\32\34\1\71\3\72\1\36"+
            "\1\72\32\34\4\72",
            "\1\101\1\74\1\73\15\uffff\1\75\1\76\1\77\1\100",
            "\14\40\1\45\1\47\1\40\12\46\6\40\33\34\1\35\3\40\1\36\1\40"+
            "\32\34\4\40",
            "\15\72\1\40\1\72\12\103\7\72\32\40\4\72\1\uffff\1\72\32\40"+
            "\4\72",
            "\12\104",
            "\17\40\12\103\45\40\1\uffff\37\40",
            "\14\40\2\34\1\40\2\60\10\34\6\40\30\34\1\60\1\34\1\60\1\35"+
            "\3\40\1\36\1\40\27\34\1\60\1\34\1\60\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\2\36\1\uffff\12\36\6\uffff\33\36\1\105\3\uffff\1\36\1\uffff"+
            "\32\36",
            "",
            "",
            "\12\32",
            "\17\27\12\32\105\27",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\107\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\1\110\2\uffff\12\111",
            "\2\36\1\uffff\12\36\6\uffff\33\36\1\105\3\uffff\1\36\1\uffff"+
            "\32\36",
            "",
            "",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\23\34"+
            "\1\112\6\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\21\34"+
            "\1\113\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\1\114"+
            "\5\34\1\115\23\34\4\40",
            "\14\40\1\34\1\47\1\40\12\46\6\40\33\34\1\35\3\40\1\36\1\40"+
            "\32\34\4\40",
            "\14\40\1\34\1\47\1\40\12\46\6\40\33\34\1\35\3\40\1\36\1\40"+
            "\32\34\4\40",
            "\14\40\2\34\1\40\12\116\6\40\33\34\1\35\3\40\1\36\1\40\32"+
            "\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\17\34"+
            "\1\117\12\34\4\40",
            "",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\10\34"+
            "\1\120\21\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\22\34"+
            "\1\121\7\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\15\34"+
            "\1\122\14\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\21\34"+
            "\1\123\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\21\34"+
            "\1\124\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\6\34"+
            "\1\125\23\34\4\40",
            "\14\40\2\34\1\40\2\60\10\34\6\40\30\34\1\60\1\34\1\60\1\35"+
            "\3\40\1\36\1\40\27\34\1\60\1\34\1\60\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\21\34"+
            "\1\127\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\15\34"+
            "\1\130\14\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\3\34"+
            "\1\131\26\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\135\2\134\1\135\12\134\6\135\33\134\1\133\3\135\1\136"+
            "\1\135\32\134\4\135",
            "\14\135\1\137\2\135\12\140\45\135\1\uffff\37\135",
            "\76\135\1\uffff\37\135",
            "\1\141",
            "\1\142\23\uffff\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\147\3\uffff\1\150",
            "\1\151",
            "",
            "\17\40\12\103\45\40\1\uffff\37\40",
            "\17\27\12\104\105\27",
            "",
            "",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\15\34"+
            "\1\152\14\34\4\40",
            "\12\111",
            "\12\111\1\153\42\uffff\1\154",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\155\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\1\156"+
            "\31\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\13\34"+
            "\1\157\16\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\116\6\40\33\34\1\35\3\40\1\36\1\40\32"+
            "\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\17\34"+
            "\1\162\12\34\4\40",
            "\14\40\2\34\1\40\1\166\1\167\10\34\6\40\33\34\1\35\3\40\1"+
            "\36\1\40\1\163\15\34\1\164\2\34\1\165\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\12\34"+
            "\1\170\17\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\3\34"+
            "\1\171\26\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\172\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\10\34"+
            "\1\173\21\34\4\40",
            "",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\12\34"+
            "\1\174\17\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\2\34"+
            "\1\175\27\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\24\34"+
            "\1\176\5\34\4\40",
            "",
            "\14\135\1\137\2\135\12\140\45\135\1\uffff\37\135",
            "\14\135\2\134\1\135\12\134\6\135\33\134\1\133\3\135\1\136"+
            "\1\135\32\134\4\135",
            "\76\135\1\uffff\37\135",
            "\2\136\1\uffff\12\136\6\uffff\33\136\1\177\3\uffff\1\136\1"+
            "\uffff\32\136",
            "\17\135\12\140\45\135\1\uffff\37\135",
            "\17\135\12\140\1\u0080\42\135\1\u0081\1\135\1\uffff\37\135",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\1\u0087",
            "\1\u0088",
            "\1\u0089",
            "\1\u008a",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\23\34"+
            "\1\u008b\6\34\4\40",
            "\1\u008c\2\uffff\12\u008d",
            "\76\40\1\uffff\37\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\6\34"+
            "\1\u008e\23\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\14\34"+
            "\1\u008f\15\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\23\34"+
            "\1\u0090\6\34\4\40",
            "",
            "",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\13\34"+
            "\1\u0091\16\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\15\34"+
            "\1\u0092\14\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\21\34"+
            "\1\u0093\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\u0094\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\15\34"+
            "\1\u0096\14\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\23\34"+
            "\1\u0097\6\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\13\34"+
            "\1\u0098\16\34\4\40",
            "\1\u0099\2\uffff\12\u009a",
            "\14\135\1\u009b\2\135\12\u009c\45\135\1\uffff\37\135",
            "\76\135\1\132\37\135",
            "\103\27\1\u009d\32\27",
            "\1\u009f",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\136\27",
            "\1\u00a5",
            "\1\u00a6",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\12\u008d",
            "\12\u008d\43\uffff\1\u00a7",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\u00a8\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\u00a9\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\10\34"+
            "\1\u00aa\21\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\30\34"+
            "\1\u00ab\1\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\3\34"+
            "\1\u00ac\26\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\6\34"+
            "\1\u00ad\23\34\4\40",
            "",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\10\34"+
            "\1\u00ae\21\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\u00af\25\34\4\40",
            "\12\u009a",
            "\12\u009a\1\u00b0\42\uffff\1\u00b1",
            "\17\135\12\u009c\45\135\1\uffff\37\135",
            "\17\135\12\u009c\43\135\1\u00b2\1\135\1\uffff\37\135",
            "\1\u00b3",
            "",
            "\136\27",
            "\1\u00b5\15\uffff\1\u00b6\6\uffff\1\u00b7",
            "\1\u00b8",
            "\1\u00b9",
            "\1\u00ba",
            "",
            "\1\u00bb",
            "\1\u00bc",
            "\76\40\1\uffff\37\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\21\34"+
            "\1\u00bd\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\23\34"+
            "\1\u00be\6\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\14\34"+
            "\1\u00bf\15\34\4\40",
            "\14\40\2\34\1\40\1\u00c0\1\u00c1\10\34\6\40\33\34\1\35\3\40"+
            "\1\36\1\40\32\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\16\34"+
            "\1\u00c2\13\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\1\u00c3\2\uffff\12\u00c4",
            "\136\132",
            "\76\135\1\132\37\135",
            "\1\u00c5",
            "",
            "\1\u00c6",
            "\1\u00c8\7\uffff\1\u00c7",
            "\1\u00c9",
            "\136\27",
            "\1\u00cb",
            "\1\u00cc",
            "\1\u00cd",
            "\1\u00ce",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\u00cf\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\4\34"+
            "\1\u00d0\25\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\15\34"+
            "\1\u00d1\14\34\4\40",
            "\12\u00c4",
            "\12\u00c4\43\uffff\1\u00d2",
            "\1\u00d3",
            "\1\u00d4",
            "\136\27",
            "\1\u00d6",
            "\1\u00d7",
            "",
            "\1\u00d8",
            "\1\u00d9",
            "\1\u00da",
            "\1\u00db",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\21\34"+
            "\1\u00dc\10\34\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\136\132",
            "\1\u00dd",
            "\136\27",
            "",
            "\136\27",
            "\1\u00e0",
            "\1\u00e1",
            "\136\27",
            "\136\27",
            "\136\27",
            "\14\40\2\34\1\40\12\34\6\40\33\34\1\35\3\40\1\36\1\40\32\34"+
            "\4\40",
            "\1\u00e5",
            "",
            "",
            "\136\27",
            "\1\u00e7",
            "",
            "",
            "",
            "\1\u00e8",
            "",
            "\136\27",
            "\1\u00ea",
            "",
            "\1\u00eb",
            "\1\u00ec",
            "\1\u00ed",
            "\136\27",
            ""
    };

    static final short[] DFA15_eot = DFA.unpackEncodedString(DFA15_eotS);
    static final short[] DFA15_eof = DFA.unpackEncodedString(DFA15_eofS);
    static final char[] DFA15_min = DFA.unpackEncodedStringToUnsignedChars(DFA15_minS);
    static final char[] DFA15_max = DFA.unpackEncodedStringToUnsignedChars(DFA15_maxS);
    static final short[] DFA15_accept = DFA.unpackEncodedString(DFA15_acceptS);
    static final short[] DFA15_special = DFA.unpackEncodedString(DFA15_specialS);
    static final short[][] DFA15_transition;

    static {
        int numStates = DFA15_transitionS.length;
        DFA15_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA15_transition[i] = DFA.unpackEncodedString(DFA15_transitionS[i]);
        }
    }

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = DFA15_eot;
            this.eof = DFA15_eof;
            this.min = DFA15_min;
            this.max = DFA15_max;
            this.accept = DFA15_accept;
            this.special = DFA15_special;
            this.transition = DFA15_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( SIMULATION_TIME | VAR_TYPE | SCOPE_TYPE | TIME_UNIT | REFERENCE | ENDDEFINITIONS | DATE | SCOPE | TIMESCALE | UPSCOPE | END | VAR | VERSION | DUMPALL | DUMPON | DUMPOFF | DUMPVARS | COMMENT | SCALAR_VALUE_CHANGE | DECIMAL_NUMBER | REAL_VECTOR | BINARY_VECTOR | IDENT | VERSION_TEXT | IDENTIFIER_CODE | WS );";
        }
    }
 

}