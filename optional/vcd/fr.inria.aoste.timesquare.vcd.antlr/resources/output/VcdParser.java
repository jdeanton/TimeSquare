/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
// $ANTLR 3.2 Sep 23, 2009 12:02:23 D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g 2010-06-09 23:42:06
package fr.inria.aoste.timesquare.vcd.antlr;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

/** Vcd.g
   Author: Frederic Mallet
   @SuppressWarnings("unused")
*/
public class VcdParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RVECTOR", "BVECTOR", "SIMULATION_TIME", "DUMPALL", "END", "DUMPVARS", "DUMPON", "DUMPOFF", "ENDDEFINITIONS", "DATE", "SCOPE", "SCOPE_TYPE", "IDENT", "TIMESCALE", "DECIMAL_NUMBER", "TIME_UNIT", "UPSCOPE", "VAR", "VAR_TYPE", "IDENTIFIER_CODE", "VERSION", "SCALAR_VALUE_CHANGE", "COMMENT", "REAL_VECTOR", "BINARY_VECTOR", "REFERENCE", "BIT", "VALUE", "DIGIT", "LETTER", "IDENT_CHAR", "VERSION_TEXT", "WS"
    };
    public static final int REAL_VECTOR=27;
    public static final int VERSION_TEXT=35;
    public static final int LETTER=33;
    public static final int UPSCOPE=20;
    public static final int IDENT_CHAR=34;
    public static final int VAR_TYPE=22;
    public static final int REFERENCE=29;
    public static final int VERSION=24;
    public static final int VALUE=31;
    public static final int EOF=-1;
    public static final int TIME_UNIT=19;
    public static final int SCALAR_VALUE_CHANGE=25;
    public static final int DUMPOFF=11;
    public static final int ENDDEFINITIONS=12;
    public static final int SIMULATION_TIME=6;
    public static final int DUMPON=10;
    public static final int WS=36;
    public static final int SCOPE=14;
    public static final int BVECTOR=5;
    public static final int DUMPALL=7;
    public static final int DUMPVARS=9;
    public static final int TIMESCALE=17;
    public static final int IDENTIFIER_CODE=23;
    public static final int RVECTOR=4;
    public static final int IDENT=16;
    public static final int BIT=30;
    public static final int VAR=21;
    public static final int DIGIT=32;
    public static final int BINARY_VECTOR=28;
    public static final int COMMENT=26;
    public static final int END=8;
    public static final int DATE=13;
    public static final int SCOPE_TYPE=15;
    public static final int DECIMAL_NUMBER=18;

    // delegates
    // delegators


        public VcdParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public VcdParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return VcdParser.tokenNames; }
    public String getGrammarFileName() { return "D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g"; }


    public static class vcd_definitions_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_definitions"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:18:1: vcd_definitions : vcd_declarations ( simulation_command )* ;
    public final VcdParser.vcd_definitions_return vcd_definitions() throws RecognitionException {
        VcdParser.vcd_definitions_return retval = new VcdParser.vcd_definitions_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        VcdParser.vcd_declarations_return vcd_declarations1 = null;

        VcdParser.simulation_command_return simulation_command2 = null;



        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:19:2: ( vcd_declarations ( simulation_command )* )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:19:4: vcd_declarations ( simulation_command )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_vcd_declarations_in_vcd_definitions66);
            vcd_declarations1=vcd_declarations();

            state._fsp--;

            adaptor.addChild(root_0, vcd_declarations1.getTree());
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:19:21: ( simulation_command )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==SIMULATION_TIME) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:19:21: simulation_command
            	    {
            	    pushFollow(FOLLOW_simulation_command_in_vcd_definitions68);
            	    simulation_command2=simulation_command();

            	    state._fsp--;

            	    adaptor.addChild(root_0, simulation_command2.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_definitions"

    public static class vcd_declarations_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_declarations"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:21:1: vcd_declarations : vcd_declaration_date vcd_declaration_version ( vcd_declaration_timescale )? ( declaration_command | vcd_comment )* vcd_declaration_enddefinitions ( vcd_comment )* ;
    public final VcdParser.vcd_declarations_return vcd_declarations() throws RecognitionException {
        VcdParser.vcd_declarations_return retval = new VcdParser.vcd_declarations_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        VcdParser.vcd_declaration_date_return vcd_declaration_date3 = null;

        VcdParser.vcd_declaration_version_return vcd_declaration_version4 = null;

        VcdParser.vcd_declaration_timescale_return vcd_declaration_timescale5 = null;

        VcdParser.declaration_command_return declaration_command6 = null;

        VcdParser.vcd_comment_return vcd_comment7 = null;

        VcdParser.vcd_declaration_enddefinitions_return vcd_declaration_enddefinitions8 = null;

        VcdParser.vcd_comment_return vcd_comment9 = null;



        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:2: ( vcd_declaration_date vcd_declaration_version ( vcd_declaration_timescale )? ( declaration_command | vcd_comment )* vcd_declaration_enddefinitions ( vcd_comment )* )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:4: vcd_declaration_date vcd_declaration_version ( vcd_declaration_timescale )? ( declaration_command | vcd_comment )* vcd_declaration_enddefinitions ( vcd_comment )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_vcd_declaration_date_in_vcd_declarations78);
            vcd_declaration_date3=vcd_declaration_date();

            state._fsp--;

            adaptor.addChild(root_0, vcd_declaration_date3.getTree());
            pushFollow(FOLLOW_vcd_declaration_version_in_vcd_declarations80);
            vcd_declaration_version4=vcd_declaration_version();

            state._fsp--;

            adaptor.addChild(root_0, vcd_declaration_version4.getTree());
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:49: ( vcd_declaration_timescale )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==TIMESCALE) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:49: vcd_declaration_timescale
                    {
                    pushFollow(FOLLOW_vcd_declaration_timescale_in_vcd_declarations82);
                    vcd_declaration_timescale5=vcd_declaration_timescale();

                    state._fsp--;

                    adaptor.addChild(root_0, vcd_declaration_timescale5.getTree());

                    }
                    break;

            }

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:76: ( declaration_command | vcd_comment )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==SCOPE||LA3_0==VAR) ) {
                    alt3=1;
                }
                else if ( (LA3_0==COMMENT) ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:77: declaration_command
            	    {
            	    pushFollow(FOLLOW_declaration_command_in_vcd_declarations86);
            	    declaration_command6=declaration_command();

            	    state._fsp--;

            	    adaptor.addChild(root_0, declaration_command6.getTree());

            	    }
            	    break;
            	case 2 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:99: vcd_comment
            	    {
            	    pushFollow(FOLLOW_vcd_comment_in_vcd_declarations90);
            	    vcd_comment7=vcd_comment();

            	    state._fsp--;

            	    adaptor.addChild(root_0, vcd_comment7.getTree());

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            pushFollow(FOLLOW_vcd_declaration_enddefinitions_in_vcd_declarations94);
            vcd_declaration_enddefinitions8=vcd_declaration_enddefinitions();

            state._fsp--;

            adaptor.addChild(root_0, vcd_declaration_enddefinitions8.getTree());
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:144: ( vcd_comment )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==COMMENT) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:22:144: vcd_comment
            	    {
            	    pushFollow(FOLLOW_vcd_comment_in_vcd_declarations96);
            	    vcd_comment9=vcd_comment();

            	    state._fsp--;

            	    adaptor.addChild(root_0, vcd_comment9.getTree());

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_declarations"

    public static class simulation_command_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "simulation_command"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:24:1: simulation_command : SIMULATION_TIME ( vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change | vcd_comment )* ;
    public final VcdParser.simulation_command_return simulation_command() throws RecognitionException {
        VcdParser.simulation_command_return retval = new VcdParser.simulation_command_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token SIMULATION_TIME10=null;
        VcdParser.vcd_simulation_dumpall_return vcd_simulation_dumpall11 = null;

        VcdParser.vcd_simulation_dumpvars_return vcd_simulation_dumpvars12 = null;

        VcdParser.vcd_simulation_dumpon_return vcd_simulation_dumpon13 = null;

        VcdParser.vcd_simulation_dumpoff_return vcd_simulation_dumpoff14 = null;

        VcdParser.declaration_command_return declaration_command15 = null;

        VcdParser.value_change_return value_change16 = null;

        VcdParser.vcd_comment_return vcd_comment17 = null;


        CommonTree SIMULATION_TIME10_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:25:2: ( SIMULATION_TIME ( vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change | vcd_comment )* )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:2: SIMULATION_TIME ( vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change | vcd_comment )*
            {
            root_0 = (CommonTree)adaptor.nil();

            SIMULATION_TIME10=(Token)match(input,SIMULATION_TIME,FOLLOW_SIMULATION_TIME_in_simulation_command111); 
            SIMULATION_TIME10_tree = (CommonTree)adaptor.create(SIMULATION_TIME10);
            root_0 = (CommonTree)adaptor.becomeRoot(SIMULATION_TIME10_tree, root_0);

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:19: ( vcd_simulation_dumpall | vcd_simulation_dumpvars | vcd_simulation_dumpon | vcd_simulation_dumpoff | declaration_command | value_change | vcd_comment )*
            loop5:
            do {
                int alt5=8;
                switch ( input.LA(1) ) {
                case DUMPALL:
                    {
                    alt5=1;
                    }
                    break;
                case DUMPVARS:
                    {
                    alt5=2;
                    }
                    break;
                case DUMPON:
                    {
                    alt5=3;
                    }
                    break;
                case DUMPOFF:
                    {
                    alt5=4;
                    }
                    break;
                case SCOPE:
                case VAR:
                    {
                    alt5=5;
                    }
                    break;
                case SCALAR_VALUE_CHANGE:
                case REAL_VECTOR:
                case BINARY_VECTOR:
                    {
                    alt5=6;
                    }
                    break;
                case COMMENT:
                    {
                    alt5=7;
                    }
                    break;

                }

                switch (alt5) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:20: vcd_simulation_dumpall
            	    {
            	    pushFollow(FOLLOW_vcd_simulation_dumpall_in_simulation_command115);
            	    vcd_simulation_dumpall11=vcd_simulation_dumpall();

            	    state._fsp--;

            	    adaptor.addChild(root_0, vcd_simulation_dumpall11.getTree());

            	    }
            	    break;
            	case 2 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:45: vcd_simulation_dumpvars
            	    {
            	    pushFollow(FOLLOW_vcd_simulation_dumpvars_in_simulation_command119);
            	    vcd_simulation_dumpvars12=vcd_simulation_dumpvars();

            	    state._fsp--;

            	    adaptor.addChild(root_0, vcd_simulation_dumpvars12.getTree());

            	    }
            	    break;
            	case 3 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:71: vcd_simulation_dumpon
            	    {
            	    pushFollow(FOLLOW_vcd_simulation_dumpon_in_simulation_command123);
            	    vcd_simulation_dumpon13=vcd_simulation_dumpon();

            	    state._fsp--;

            	    adaptor.addChild(root_0, vcd_simulation_dumpon13.getTree());

            	    }
            	    break;
            	case 4 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:95: vcd_simulation_dumpoff
            	    {
            	    pushFollow(FOLLOW_vcd_simulation_dumpoff_in_simulation_command127);
            	    vcd_simulation_dumpoff14=vcd_simulation_dumpoff();

            	    state._fsp--;

            	    adaptor.addChild(root_0, vcd_simulation_dumpoff14.getTree());

            	    }
            	    break;
            	case 5 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:120: declaration_command
            	    {
            	    pushFollow(FOLLOW_declaration_command_in_simulation_command131);
            	    declaration_command15=declaration_command();

            	    state._fsp--;

            	    adaptor.addChild(root_0, declaration_command15.getTree());

            	    }
            	    break;
            	case 6 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:142: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_simulation_command135);
            	    value_change16=value_change();

            	    state._fsp--;

            	    adaptor.addChild(root_0, value_change16.getTree());

            	    }
            	    break;
            	case 7 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:27:157: vcd_comment
            	    {
            	    pushFollow(FOLLOW_vcd_comment_in_simulation_command139);
            	    vcd_comment17=vcd_comment();

            	    state._fsp--;

            	    adaptor.addChild(root_0, vcd_comment17.getTree());

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "simulation_command"

    public static class vcd_simulation_dumpall_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_simulation_dumpall"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:29:1: vcd_simulation_dumpall : DUMPALL ( value_change )* END ;
    public final VcdParser.vcd_simulation_dumpall_return vcd_simulation_dumpall() throws RecognitionException {
        VcdParser.vcd_simulation_dumpall_return retval = new VcdParser.vcd_simulation_dumpall_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token DUMPALL18=null;
        Token END20=null;
        VcdParser.value_change_return value_change19 = null;


        CommonTree DUMPALL18_tree=null;
        CommonTree END20_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:30:2: ( DUMPALL ( value_change )* END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:30:4: DUMPALL ( value_change )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            DUMPALL18=(Token)match(input,DUMPALL,FOLLOW_DUMPALL_in_vcd_simulation_dumpall150); 
            DUMPALL18_tree = (CommonTree)adaptor.create(DUMPALL18);
            root_0 = (CommonTree)adaptor.becomeRoot(DUMPALL18_tree, root_0);

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:30:13: ( value_change )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==SCALAR_VALUE_CHANGE||(LA6_0>=REAL_VECTOR && LA6_0<=BINARY_VECTOR)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:30:13: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_vcd_simulation_dumpall153);
            	    value_change19=value_change();

            	    state._fsp--;

            	    adaptor.addChild(root_0, value_change19.getTree());

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            END20=(Token)match(input,END,FOLLOW_END_in_vcd_simulation_dumpall156); 
            END20_tree = (CommonTree)adaptor.create(END20);
            adaptor.addChild(root_0, END20_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_simulation_dumpall"

    public static class vcd_simulation_dumpvars_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_simulation_dumpvars"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:31:1: vcd_simulation_dumpvars : DUMPVARS ( value_change )* END ;
    public final VcdParser.vcd_simulation_dumpvars_return vcd_simulation_dumpvars() throws RecognitionException {
        VcdParser.vcd_simulation_dumpvars_return retval = new VcdParser.vcd_simulation_dumpvars_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token DUMPVARS21=null;
        Token END23=null;
        VcdParser.value_change_return value_change22 = null;


        CommonTree DUMPVARS21_tree=null;
        CommonTree END23_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:32:2: ( DUMPVARS ( value_change )* END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:32:4: DUMPVARS ( value_change )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            DUMPVARS21=(Token)match(input,DUMPVARS,FOLLOW_DUMPVARS_in_vcd_simulation_dumpvars164); 
            DUMPVARS21_tree = (CommonTree)adaptor.create(DUMPVARS21);
            root_0 = (CommonTree)adaptor.becomeRoot(DUMPVARS21_tree, root_0);

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:32:14: ( value_change )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==SCALAR_VALUE_CHANGE||(LA7_0>=REAL_VECTOR && LA7_0<=BINARY_VECTOR)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:32:14: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_vcd_simulation_dumpvars167);
            	    value_change22=value_change();

            	    state._fsp--;

            	    adaptor.addChild(root_0, value_change22.getTree());

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            END23=(Token)match(input,END,FOLLOW_END_in_vcd_simulation_dumpvars170); 
            END23_tree = (CommonTree)adaptor.create(END23);
            adaptor.addChild(root_0, END23_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_simulation_dumpvars"

    public static class vcd_simulation_dumpon_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_simulation_dumpon"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:33:1: vcd_simulation_dumpon : DUMPON ( value_change )* END ;
    public final VcdParser.vcd_simulation_dumpon_return vcd_simulation_dumpon() throws RecognitionException {
        VcdParser.vcd_simulation_dumpon_return retval = new VcdParser.vcd_simulation_dumpon_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token DUMPON24=null;
        Token END26=null;
        VcdParser.value_change_return value_change25 = null;


        CommonTree DUMPON24_tree=null;
        CommonTree END26_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:34:2: ( DUMPON ( value_change )* END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:34:4: DUMPON ( value_change )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            DUMPON24=(Token)match(input,DUMPON,FOLLOW_DUMPON_in_vcd_simulation_dumpon178); 
            DUMPON24_tree = (CommonTree)adaptor.create(DUMPON24);
            root_0 = (CommonTree)adaptor.becomeRoot(DUMPON24_tree, root_0);

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:34:12: ( value_change )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==SCALAR_VALUE_CHANGE||(LA8_0>=REAL_VECTOR && LA8_0<=BINARY_VECTOR)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:34:12: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_vcd_simulation_dumpon181);
            	    value_change25=value_change();

            	    state._fsp--;

            	    adaptor.addChild(root_0, value_change25.getTree());

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            END26=(Token)match(input,END,FOLLOW_END_in_vcd_simulation_dumpon184); 
            END26_tree = (CommonTree)adaptor.create(END26);
            adaptor.addChild(root_0, END26_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_simulation_dumpon"

    public static class vcd_simulation_dumpoff_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_simulation_dumpoff"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:35:1: vcd_simulation_dumpoff : DUMPOFF ( value_change )* END ;
    public final VcdParser.vcd_simulation_dumpoff_return vcd_simulation_dumpoff() throws RecognitionException {
        VcdParser.vcd_simulation_dumpoff_return retval = new VcdParser.vcd_simulation_dumpoff_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token DUMPOFF27=null;
        Token END29=null;
        VcdParser.value_change_return value_change28 = null;


        CommonTree DUMPOFF27_tree=null;
        CommonTree END29_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:36:2: ( DUMPOFF ( value_change )* END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:36:4: DUMPOFF ( value_change )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            DUMPOFF27=(Token)match(input,DUMPOFF,FOLLOW_DUMPOFF_in_vcd_simulation_dumpoff192); 
            DUMPOFF27_tree = (CommonTree)adaptor.create(DUMPOFF27);
            root_0 = (CommonTree)adaptor.becomeRoot(DUMPOFF27_tree, root_0);

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:36:13: ( value_change )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==SCALAR_VALUE_CHANGE||(LA9_0>=REAL_VECTOR && LA9_0<=BINARY_VECTOR)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:36:13: value_change
            	    {
            	    pushFollow(FOLLOW_value_change_in_vcd_simulation_dumpoff195);
            	    value_change28=value_change();

            	    state._fsp--;

            	    adaptor.addChild(root_0, value_change28.getTree());

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            END29=(Token)match(input,END,FOLLOW_END_in_vcd_simulation_dumpoff198); 
            END29_tree = (CommonTree)adaptor.create(END29);
            adaptor.addChild(root_0, END29_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_simulation_dumpoff"

    public static class declaration_command_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "declaration_command"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:38:1: declaration_command : ( vcd_declaration_scope ( declaration_command )* vcd_declaration_upscope | vcd_declaration_vars );
    public final VcdParser.declaration_command_return declaration_command() throws RecognitionException {
        VcdParser.declaration_command_return retval = new VcdParser.declaration_command_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        VcdParser.vcd_declaration_scope_return vcd_declaration_scope30 = null;

        VcdParser.declaration_command_return declaration_command31 = null;

        VcdParser.vcd_declaration_upscope_return vcd_declaration_upscope32 = null;

        VcdParser.vcd_declaration_vars_return vcd_declaration_vars33 = null;



        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:39:2: ( vcd_declaration_scope ( declaration_command )* vcd_declaration_upscope | vcd_declaration_vars )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==SCOPE) ) {
                alt11=1;
            }
            else if ( (LA11_0==VAR) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:39:4: vcd_declaration_scope ( declaration_command )* vcd_declaration_upscope
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_vcd_declaration_scope_in_declaration_command208);
                    vcd_declaration_scope30=vcd_declaration_scope();

                    state._fsp--;

                    root_0 = (CommonTree)adaptor.becomeRoot(vcd_declaration_scope30.getTree(), root_0);
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:39:27: ( declaration_command )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==SCOPE||LA10_0==VAR) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:39:27: declaration_command
                    	    {
                    	    pushFollow(FOLLOW_declaration_command_in_declaration_command211);
                    	    declaration_command31=declaration_command();

                    	    state._fsp--;

                    	    adaptor.addChild(root_0, declaration_command31.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    pushFollow(FOLLOW_vcd_declaration_upscope_in_declaration_command214);
                    vcd_declaration_upscope32=vcd_declaration_upscope();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:41:4: vcd_declaration_vars
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_vcd_declaration_vars_in_declaration_command222);
                    vcd_declaration_vars33=vcd_declaration_vars();

                    state._fsp--;

                    adaptor.addChild(root_0, vcd_declaration_vars33.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "declaration_command"

    public static class vcd_declaration_enddefinitions_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_declaration_enddefinitions"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:43:1: vcd_declaration_enddefinitions : ENDDEFINITIONS END ;
    public final VcdParser.vcd_declaration_enddefinitions_return vcd_declaration_enddefinitions() throws RecognitionException {
        VcdParser.vcd_declaration_enddefinitions_return retval = new VcdParser.vcd_declaration_enddefinitions_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ENDDEFINITIONS34=null;
        Token END35=null;

        CommonTree ENDDEFINITIONS34_tree=null;
        CommonTree END35_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:44:2: ( ENDDEFINITIONS END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:44:4: ENDDEFINITIONS END
            {
            root_0 = (CommonTree)adaptor.nil();

            ENDDEFINITIONS34=(Token)match(input,ENDDEFINITIONS,FOLLOW_ENDDEFINITIONS_in_vcd_declaration_enddefinitions231); 
            ENDDEFINITIONS34_tree = (CommonTree)adaptor.create(ENDDEFINITIONS34);
            root_0 = (CommonTree)adaptor.becomeRoot(ENDDEFINITIONS34_tree, root_0);

            END35=(Token)match(input,END,FOLLOW_END_in_vcd_declaration_enddefinitions234); 
            END35_tree = (CommonTree)adaptor.create(END35);
            adaptor.addChild(root_0, END35_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_declaration_enddefinitions"

    public static class vcd_declaration_date_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_declaration_date"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:49:1: vcd_declaration_date : DATE ( . )+ END ;
    public final VcdParser.vcd_declaration_date_return vcd_declaration_date() throws RecognitionException {
        VcdParser.vcd_declaration_date_return retval = new VcdParser.vcd_declaration_date_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token DATE36=null;
        Token wildcard37=null;
        Token END38=null;

        CommonTree DATE36_tree=null;
        CommonTree wildcard37_tree=null;
        CommonTree END38_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:50:2: ( DATE ( . )+ END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:50:4: DATE ( . )+ END
            {
            root_0 = (CommonTree)adaptor.nil();

            DATE36=(Token)match(input,DATE,FOLLOW_DATE_in_vcd_declaration_date247); 
            DATE36_tree = (CommonTree)adaptor.create(DATE36);
            root_0 = (CommonTree)adaptor.becomeRoot(DATE36_tree, root_0);

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:50:10: ( . )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==END) ) {
                    alt12=2;
                }
                else if ( ((LA12_0>=RVECTOR && LA12_0<=DUMPALL)||(LA12_0>=DUMPVARS && LA12_0<=WS)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:50:10: .
            	    {
            	    wildcard37=(Token)input.LT(1);
            	    matchAny(input); 
            	    wildcard37_tree = (CommonTree)adaptor.create(wildcard37);
            	    adaptor.addChild(root_0, wildcard37_tree);


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);

            END38=(Token)match(input,END,FOLLOW_END_in_vcd_declaration_date253); 
            END38_tree = (CommonTree)adaptor.create(END38);
            adaptor.addChild(root_0, END38_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_declaration_date"

    public static class vcd_declaration_scope_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_declaration_scope"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:52:1: vcd_declaration_scope : SCOPE SCOPE_TYPE IDENT END ;
    public final VcdParser.vcd_declaration_scope_return vcd_declaration_scope() throws RecognitionException {
        VcdParser.vcd_declaration_scope_return retval = new VcdParser.vcd_declaration_scope_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token SCOPE39=null;
        Token SCOPE_TYPE40=null;
        Token IDENT41=null;
        Token END42=null;

        CommonTree SCOPE39_tree=null;
        CommonTree SCOPE_TYPE40_tree=null;
        CommonTree IDENT41_tree=null;
        CommonTree END42_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:53:2: ( SCOPE SCOPE_TYPE IDENT END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:53:4: SCOPE SCOPE_TYPE IDENT END
            {
            root_0 = (CommonTree)adaptor.nil();

            SCOPE39=(Token)match(input,SCOPE,FOLLOW_SCOPE_in_vcd_declaration_scope262); 
            SCOPE39_tree = (CommonTree)adaptor.create(SCOPE39);
            root_0 = (CommonTree)adaptor.becomeRoot(SCOPE39_tree, root_0);

            SCOPE_TYPE40=(Token)match(input,SCOPE_TYPE,FOLLOW_SCOPE_TYPE_in_vcd_declaration_scope265); 
            SCOPE_TYPE40_tree = (CommonTree)adaptor.create(SCOPE_TYPE40);
            adaptor.addChild(root_0, SCOPE_TYPE40_tree);

            IDENT41=(Token)match(input,IDENT,FOLLOW_IDENT_in_vcd_declaration_scope267); 
            IDENT41_tree = (CommonTree)adaptor.create(IDENT41);
            adaptor.addChild(root_0, IDENT41_tree);

            END42=(Token)match(input,END,FOLLOW_END_in_vcd_declaration_scope269); 

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_declaration_scope"

    public static class vcd_declaration_timescale_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_declaration_timescale"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:55:1: vcd_declaration_timescale : TIMESCALE DECIMAL_NUMBER TIME_UNIT END ;
    public final VcdParser.vcd_declaration_timescale_return vcd_declaration_timescale() throws RecognitionException {
        VcdParser.vcd_declaration_timescale_return retval = new VcdParser.vcd_declaration_timescale_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token TIMESCALE43=null;
        Token DECIMAL_NUMBER44=null;
        Token TIME_UNIT45=null;
        Token END46=null;

        CommonTree TIMESCALE43_tree=null;
        CommonTree DECIMAL_NUMBER44_tree=null;
        CommonTree TIME_UNIT45_tree=null;
        CommonTree END46_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:56:2: ( TIMESCALE DECIMAL_NUMBER TIME_UNIT END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:56:4: TIMESCALE DECIMAL_NUMBER TIME_UNIT END
            {
            root_0 = (CommonTree)adaptor.nil();

            TIMESCALE43=(Token)match(input,TIMESCALE,FOLLOW_TIMESCALE_in_vcd_declaration_timescale279); 
            TIMESCALE43_tree = (CommonTree)adaptor.create(TIMESCALE43);
            root_0 = (CommonTree)adaptor.becomeRoot(TIMESCALE43_tree, root_0);

            DECIMAL_NUMBER44=(Token)match(input,DECIMAL_NUMBER,FOLLOW_DECIMAL_NUMBER_in_vcd_declaration_timescale282); 
            DECIMAL_NUMBER44_tree = (CommonTree)adaptor.create(DECIMAL_NUMBER44);
            adaptor.addChild(root_0, DECIMAL_NUMBER44_tree);

            TIME_UNIT45=(Token)match(input,TIME_UNIT,FOLLOW_TIME_UNIT_in_vcd_declaration_timescale284); 
            TIME_UNIT45_tree = (CommonTree)adaptor.create(TIME_UNIT45);
            adaptor.addChild(root_0, TIME_UNIT45_tree);

            END46=(Token)match(input,END,FOLLOW_END_in_vcd_declaration_timescale286); 
            END46_tree = (CommonTree)adaptor.create(END46);
            adaptor.addChild(root_0, END46_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_declaration_timescale"

    public static class vcd_declaration_upscope_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_declaration_upscope"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:58:1: vcd_declaration_upscope : UPSCOPE END ;
    public final VcdParser.vcd_declaration_upscope_return vcd_declaration_upscope() throws RecognitionException {
        VcdParser.vcd_declaration_upscope_return retval = new VcdParser.vcd_declaration_upscope_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token UPSCOPE47=null;
        Token END48=null;

        CommonTree UPSCOPE47_tree=null;
        CommonTree END48_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:59:2: ( UPSCOPE END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:59:4: UPSCOPE END
            {
            root_0 = (CommonTree)adaptor.nil();

            UPSCOPE47=(Token)match(input,UPSCOPE,FOLLOW_UPSCOPE_in_vcd_declaration_upscope295); 
            UPSCOPE47_tree = (CommonTree)adaptor.create(UPSCOPE47);
            root_0 = (CommonTree)adaptor.becomeRoot(UPSCOPE47_tree, root_0);

            END48=(Token)match(input,END,FOLLOW_END_in_vcd_declaration_upscope298); 

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_declaration_upscope"

    public static class vcd_declaration_vars_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_declaration_vars"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:61:1: vcd_declaration_vars : VAR VAR_TYPE DECIMAL_NUMBER IDENTIFIER_CODE . END ;
    public final VcdParser.vcd_declaration_vars_return vcd_declaration_vars() throws RecognitionException {
        VcdParser.vcd_declaration_vars_return retval = new VcdParser.vcd_declaration_vars_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token VAR49=null;
        Token VAR_TYPE50=null;
        Token DECIMAL_NUMBER51=null;
        Token IDENTIFIER_CODE52=null;
        Token wildcard53=null;
        Token END54=null;

        CommonTree VAR49_tree=null;
        CommonTree VAR_TYPE50_tree=null;
        CommonTree DECIMAL_NUMBER51_tree=null;
        CommonTree IDENTIFIER_CODE52_tree=null;
        CommonTree wildcard53_tree=null;
        CommonTree END54_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:62:2: ( VAR VAR_TYPE DECIMAL_NUMBER IDENTIFIER_CODE . END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:62:4: VAR VAR_TYPE DECIMAL_NUMBER IDENTIFIER_CODE . END
            {
            root_0 = (CommonTree)adaptor.nil();

            VAR49=(Token)match(input,VAR,FOLLOW_VAR_in_vcd_declaration_vars308); 
            VAR49_tree = (CommonTree)adaptor.create(VAR49);
            root_0 = (CommonTree)adaptor.becomeRoot(VAR49_tree, root_0);

            VAR_TYPE50=(Token)match(input,VAR_TYPE,FOLLOW_VAR_TYPE_in_vcd_declaration_vars311); 
            VAR_TYPE50_tree = (CommonTree)adaptor.create(VAR_TYPE50);
            adaptor.addChild(root_0, VAR_TYPE50_tree);

            DECIMAL_NUMBER51=(Token)match(input,DECIMAL_NUMBER,FOLLOW_DECIMAL_NUMBER_in_vcd_declaration_vars313); 
            DECIMAL_NUMBER51_tree = (CommonTree)adaptor.create(DECIMAL_NUMBER51);
            adaptor.addChild(root_0, DECIMAL_NUMBER51_tree);

            IDENTIFIER_CODE52=(Token)match(input,IDENTIFIER_CODE,FOLLOW_IDENTIFIER_CODE_in_vcd_declaration_vars315); 
            IDENTIFIER_CODE52_tree = (CommonTree)adaptor.create(IDENTIFIER_CODE52);
            adaptor.addChild(root_0, IDENTIFIER_CODE52_tree);

            wildcard53=(Token)input.LT(1);
            matchAny(input); 
            wildcard53_tree = (CommonTree)adaptor.create(wildcard53);
            adaptor.addChild(root_0, wildcard53_tree);

            END54=(Token)match(input,END,FOLLOW_END_in_vcd_declaration_vars319); 
            END54_tree = (CommonTree)adaptor.create(END54);
            adaptor.addChild(root_0, END54_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_declaration_vars"

    public static class vcd_declaration_version_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_declaration_version"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:64:1: vcd_declaration_version : VERSION (~ END )* END ;
    public final VcdParser.vcd_declaration_version_return vcd_declaration_version() throws RecognitionException {
        VcdParser.vcd_declaration_version_return retval = new VcdParser.vcd_declaration_version_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token VERSION55=null;
        Token set56=null;
        Token END57=null;

        CommonTree VERSION55_tree=null;
        CommonTree set56_tree=null;
        CommonTree END57_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:65:2: ( VERSION (~ END )* END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:65:4: VERSION (~ END )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            VERSION55=(Token)match(input,VERSION,FOLLOW_VERSION_in_vcd_declaration_version329); 
            VERSION55_tree = (CommonTree)adaptor.create(VERSION55);
            root_0 = (CommonTree)adaptor.becomeRoot(VERSION55_tree, root_0);

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:65:13: (~ END )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>=RVECTOR && LA13_0<=DUMPALL)||(LA13_0>=DUMPVARS && LA13_0<=WS)) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:65:14: ~ END
            	    {
            	    set56=(Token)input.LT(1);
            	    if ( (input.LA(1)>=RVECTOR && input.LA(1)<=DUMPALL)||(input.LA(1)>=DUMPVARS && input.LA(1)<=WS) ) {
            	        input.consume();
            	        adaptor.addChild(root_0, (CommonTree)adaptor.create(set56));
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            END57=(Token)match(input,END,FOLLOW_END_in_vcd_declaration_version339); 
            END57_tree = (CommonTree)adaptor.create(END57);
            adaptor.addChild(root_0, END57_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_declaration_version"

    public static class value_change_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "value_change"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:67:1: value_change : ( real_vector_value_change | bin_vector_value_change | SCALAR_VALUE_CHANGE );
    public final VcdParser.value_change_return value_change() throws RecognitionException {
        VcdParser.value_change_return retval = new VcdParser.value_change_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token SCALAR_VALUE_CHANGE60=null;
        VcdParser.real_vector_value_change_return real_vector_value_change58 = null;

        VcdParser.bin_vector_value_change_return bin_vector_value_change59 = null;


        CommonTree SCALAR_VALUE_CHANGE60_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:68:2: ( real_vector_value_change | bin_vector_value_change | SCALAR_VALUE_CHANGE )
            int alt14=3;
            switch ( input.LA(1) ) {
            case REAL_VECTOR:
                {
                alt14=1;
                }
                break;
            case BINARY_VECTOR:
                {
                alt14=2;
                }
                break;
            case SCALAR_VALUE_CHANGE:
                {
                alt14=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:68:4: real_vector_value_change
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_real_vector_value_change_in_value_change348);
                    real_vector_value_change58=real_vector_value_change();

                    state._fsp--;

                    adaptor.addChild(root_0, real_vector_value_change58.getTree());

                    }
                    break;
                case 2 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:68:31: bin_vector_value_change
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_bin_vector_value_change_in_value_change352);
                    bin_vector_value_change59=bin_vector_value_change();

                    state._fsp--;

                    adaptor.addChild(root_0, bin_vector_value_change59.getTree());

                    }
                    break;
                case 3 :
                    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:68:57: SCALAR_VALUE_CHANGE
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    SCALAR_VALUE_CHANGE60=(Token)match(input,SCALAR_VALUE_CHANGE,FOLLOW_SCALAR_VALUE_CHANGE_in_value_change356); 
                    SCALAR_VALUE_CHANGE60_tree = (CommonTree)adaptor.create(SCALAR_VALUE_CHANGE60);
                    adaptor.addChild(root_0, SCALAR_VALUE_CHANGE60_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "value_change"

    public static class vcd_comment_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vcd_comment"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:70:1: vcd_comment : COMMENT ( . )* END ;
    public final VcdParser.vcd_comment_return vcd_comment() throws RecognitionException {
        VcdParser.vcd_comment_return retval = new VcdParser.vcd_comment_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token COMMENT61=null;
        Token wildcard62=null;
        Token END63=null;

        CommonTree COMMENT61_tree=null;
        CommonTree wildcard62_tree=null;
        CommonTree END63_tree=null;

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:70:13: ( COMMENT ( . )* END )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:71:2: COMMENT ( . )* END
            {
            root_0 = (CommonTree)adaptor.nil();

            COMMENT61=(Token)match(input,COMMENT,FOLLOW_COMMENT_in_vcd_comment365); 
            COMMENT61_tree = (CommonTree)adaptor.create(COMMENT61);
            adaptor.addChild(root_0, COMMENT61_tree);

            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:71:10: ( . )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==END) ) {
                    alt15=2;
                }
                else if ( ((LA15_0>=RVECTOR && LA15_0<=DUMPALL)||(LA15_0>=DUMPVARS && LA15_0<=WS)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:71:10: .
            	    {
            	    wildcard62=(Token)input.LT(1);
            	    matchAny(input); 
            	    wildcard62_tree = (CommonTree)adaptor.create(wildcard62);
            	    adaptor.addChild(root_0, wildcard62_tree);


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            END63=(Token)match(input,END,FOLLOW_END_in_vcd_comment370); 
            END63_tree = (CommonTree)adaptor.create(END63);
            adaptor.addChild(root_0, END63_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vcd_comment"

    public static class real_vector_value_change_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "real_vector_value_change"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:73:1: real_vector_value_change : REAL_VECTOR IDENTIFIER_CODE -> ^( RVECTOR REAL_VECTOR IDENTIFIER_CODE ) ;
    public final VcdParser.real_vector_value_change_return real_vector_value_change() throws RecognitionException {
        VcdParser.real_vector_value_change_return retval = new VcdParser.real_vector_value_change_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token REAL_VECTOR64=null;
        Token IDENTIFIER_CODE65=null;

        CommonTree REAL_VECTOR64_tree=null;
        CommonTree IDENTIFIER_CODE65_tree=null;
        RewriteRuleTokenStream stream_REAL_VECTOR=new RewriteRuleTokenStream(adaptor,"token REAL_VECTOR");
        RewriteRuleTokenStream stream_IDENTIFIER_CODE=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER_CODE");

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:74:2: ( REAL_VECTOR IDENTIFIER_CODE -> ^( RVECTOR REAL_VECTOR IDENTIFIER_CODE ) )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:74:4: REAL_VECTOR IDENTIFIER_CODE
            {
            REAL_VECTOR64=(Token)match(input,REAL_VECTOR,FOLLOW_REAL_VECTOR_in_real_vector_value_change381);  
            stream_REAL_VECTOR.add(REAL_VECTOR64);

            IDENTIFIER_CODE65=(Token)match(input,IDENTIFIER_CODE,FOLLOW_IDENTIFIER_CODE_in_real_vector_value_change383);  
            stream_IDENTIFIER_CODE.add(IDENTIFIER_CODE65);



            // AST REWRITE
            // elements: REAL_VECTOR, IDENTIFIER_CODE
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 74:32: -> ^( RVECTOR REAL_VECTOR IDENTIFIER_CODE )
            {
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:74:35: ^( RVECTOR REAL_VECTOR IDENTIFIER_CODE )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(RVECTOR, "RVECTOR"), root_1);

                adaptor.addChild(root_1, stream_REAL_VECTOR.nextNode());
                adaptor.addChild(root_1, stream_IDENTIFIER_CODE.nextNode());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "real_vector_value_change"

    public static class bin_vector_value_change_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "bin_vector_value_change"
    // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:76:1: bin_vector_value_change : BINARY_VECTOR IDENTIFIER_CODE -> ^( BVECTOR BINARY_VECTOR IDENTIFIER_CODE ) ;
    public final VcdParser.bin_vector_value_change_return bin_vector_value_change() throws RecognitionException {
        VcdParser.bin_vector_value_change_return retval = new VcdParser.bin_vector_value_change_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token BINARY_VECTOR66=null;
        Token IDENTIFIER_CODE67=null;

        CommonTree BINARY_VECTOR66_tree=null;
        CommonTree IDENTIFIER_CODE67_tree=null;
        RewriteRuleTokenStream stream_BINARY_VECTOR=new RewriteRuleTokenStream(adaptor,"token BINARY_VECTOR");
        RewriteRuleTokenStream stream_IDENTIFIER_CODE=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER_CODE");

        try {
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:77:2: ( BINARY_VECTOR IDENTIFIER_CODE -> ^( BVECTOR BINARY_VECTOR IDENTIFIER_CODE ) )
            // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:77:4: BINARY_VECTOR IDENTIFIER_CODE
            {
            BINARY_VECTOR66=(Token)match(input,BINARY_VECTOR,FOLLOW_BINARY_VECTOR_in_bin_vector_value_change402);  
            stream_BINARY_VECTOR.add(BINARY_VECTOR66);

            IDENTIFIER_CODE67=(Token)match(input,IDENTIFIER_CODE,FOLLOW_IDENTIFIER_CODE_in_bin_vector_value_change404);  
            stream_IDENTIFIER_CODE.add(IDENTIFIER_CODE67);



            // AST REWRITE
            // elements: IDENTIFIER_CODE, BINARY_VECTOR
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 77:34: -> ^( BVECTOR BINARY_VECTOR IDENTIFIER_CODE )
            {
                // D:\\workspaces\\TimeSquareDev\\fr.inria.aoste.timesquare.vcd.antlr\\resources\\Vcd.g:77:37: ^( BVECTOR BINARY_VECTOR IDENTIFIER_CODE )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BVECTOR, "BVECTOR"), root_1);

                adaptor.addChild(root_1, stream_BINARY_VECTOR.nextNode());
                adaptor.addChild(root_1, stream_IDENTIFIER_CODE.nextNode());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "bin_vector_value_change"

    // Delegated rules


 

    public static final BitSet FOLLOW_vcd_declarations_in_vcd_definitions66 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_simulation_command_in_vcd_definitions68 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_vcd_declaration_date_in_vcd_declarations78 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_vcd_declaration_version_in_vcd_declarations80 = new BitSet(new long[]{0x0000000004225000L});
    public static final BitSet FOLLOW_vcd_declaration_timescale_in_vcd_declarations82 = new BitSet(new long[]{0x0000000004225000L});
    public static final BitSet FOLLOW_declaration_command_in_vcd_declarations86 = new BitSet(new long[]{0x0000000004225000L});
    public static final BitSet FOLLOW_vcd_comment_in_vcd_declarations90 = new BitSet(new long[]{0x0000000004225000L});
    public static final BitSet FOLLOW_vcd_declaration_enddefinitions_in_vcd_declarations94 = new BitSet(new long[]{0x0000000004204002L});
    public static final BitSet FOLLOW_vcd_comment_in_vcd_declarations96 = new BitSet(new long[]{0x0000000004204002L});
    public static final BitSet FOLLOW_SIMULATION_TIME_in_simulation_command111 = new BitSet(new long[]{0x000000001E204E82L});
    public static final BitSet FOLLOW_vcd_simulation_dumpall_in_simulation_command115 = new BitSet(new long[]{0x000000001E204E82L});
    public static final BitSet FOLLOW_vcd_simulation_dumpvars_in_simulation_command119 = new BitSet(new long[]{0x000000001E204E82L});
    public static final BitSet FOLLOW_vcd_simulation_dumpon_in_simulation_command123 = new BitSet(new long[]{0x000000001E204E82L});
    public static final BitSet FOLLOW_vcd_simulation_dumpoff_in_simulation_command127 = new BitSet(new long[]{0x000000001E204E82L});
    public static final BitSet FOLLOW_declaration_command_in_simulation_command131 = new BitSet(new long[]{0x000000001E204E82L});
    public static final BitSet FOLLOW_value_change_in_simulation_command135 = new BitSet(new long[]{0x000000001E204E82L});
    public static final BitSet FOLLOW_vcd_comment_in_simulation_command139 = new BitSet(new long[]{0x000000001E204E82L});
    public static final BitSet FOLLOW_DUMPALL_in_vcd_simulation_dumpall150 = new BitSet(new long[]{0x000000001A000100L});
    public static final BitSet FOLLOW_value_change_in_vcd_simulation_dumpall153 = new BitSet(new long[]{0x000000001A000100L});
    public static final BitSet FOLLOW_END_in_vcd_simulation_dumpall156 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPVARS_in_vcd_simulation_dumpvars164 = new BitSet(new long[]{0x000000001A000100L});
    public static final BitSet FOLLOW_value_change_in_vcd_simulation_dumpvars167 = new BitSet(new long[]{0x000000001A000100L});
    public static final BitSet FOLLOW_END_in_vcd_simulation_dumpvars170 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPON_in_vcd_simulation_dumpon178 = new BitSet(new long[]{0x000000001A000100L});
    public static final BitSet FOLLOW_value_change_in_vcd_simulation_dumpon181 = new BitSet(new long[]{0x000000001A000100L});
    public static final BitSet FOLLOW_END_in_vcd_simulation_dumpon184 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPOFF_in_vcd_simulation_dumpoff192 = new BitSet(new long[]{0x000000001A000100L});
    public static final BitSet FOLLOW_value_change_in_vcd_simulation_dumpoff195 = new BitSet(new long[]{0x000000001A000100L});
    public static final BitSet FOLLOW_END_in_vcd_simulation_dumpoff198 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_vcd_declaration_scope_in_declaration_command208 = new BitSet(new long[]{0x0000000000304000L});
    public static final BitSet FOLLOW_declaration_command_in_declaration_command211 = new BitSet(new long[]{0x0000000000304000L});
    public static final BitSet FOLLOW_vcd_declaration_upscope_in_declaration_command214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_vcd_declaration_vars_in_declaration_command222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ENDDEFINITIONS_in_vcd_declaration_enddefinitions231 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_enddefinitions234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATE_in_vcd_declaration_date247 = new BitSet(new long[]{0x0000001FFFFFFFF0L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_date253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SCOPE_in_vcd_declaration_scope262 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_SCOPE_TYPE_in_vcd_declaration_scope265 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_IDENT_in_vcd_declaration_scope267 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_scope269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TIMESCALE_in_vcd_declaration_timescale279 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_DECIMAL_NUMBER_in_vcd_declaration_timescale282 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_TIME_UNIT_in_vcd_declaration_timescale284 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_timescale286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_UPSCOPE_in_vcd_declaration_upscope295 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_upscope298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_vcd_declaration_vars308 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_VAR_TYPE_in_vcd_declaration_vars311 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_DECIMAL_NUMBER_in_vcd_declaration_vars313 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_IDENTIFIER_CODE_in_vcd_declaration_vars315 = new BitSet(new long[]{0x0000001FFFFFFFF0L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_vars319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VERSION_in_vcd_declaration_version329 = new BitSet(new long[]{0x0000001FFFFFFFF0L});
    public static final BitSet FOLLOW_set_in_vcd_declaration_version333 = new BitSet(new long[]{0x0000001FFFFFFFF0L});
    public static final BitSet FOLLOW_END_in_vcd_declaration_version339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_real_vector_value_change_in_value_change348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_bin_vector_value_change_in_value_change352 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SCALAR_VALUE_CHANGE_in_value_change356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COMMENT_in_vcd_comment365 = new BitSet(new long[]{0x0000001FFFFFFFF0L});
    public static final BitSet FOLLOW_END_in_vcd_comment370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REAL_VECTOR_in_real_vector_value_change381 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_IDENTIFIER_CODE_in_real_vector_value_change383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BINARY_VECTOR_in_bin_vector_value_change402 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_IDENTIFIER_CODE_in_bin_vector_value_change404 = new BitSet(new long[]{0x0000000000000002L});

}