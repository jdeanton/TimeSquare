/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.antlr.editors;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
import org.eclipse.ui.part.MultiPageEditorPart;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.VcdZoom;
import fr.inria.aoste.timesquare.vcd.antlr.view.ConstraintView;
import fr.inria.aoste.timesquare.vcd.antlr.view.VcdDiagramReader;
import fr.inria.aoste.timesquare.vcd.draw.FigureCanvasBase;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.menu.Mode;
import fr.inria.aoste.timesquare.vcd.menu.VcdMenu;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.model.IDeclarationCommand;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.command.ScopeCommand;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
import fr.inria.aoste.timesquare.vcd.model.keyword.ScopeType;
import fr.inria.aoste.timesquare.vcd.model.visitor.Collector;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.ConstraintsFactory;
import fr.inria.aoste.timesquare.vcd.view.IVcdMultiPageEditor;
import fr.inria.aoste.timesquare.vcd.view.Iupdate;
import fr.inria.aoste.timesquare.vcd.view.MarkerFactory;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;
import fr.inria.aoste.timesquare.vcd.view.figure.CountFigure;

/**
 * <ul>
 * <li>page 0 vcd diagram
 * <li>page 1 vcd text
 * </ul>
 */
public class VcdMultiPageEditor extends MultiPageEditorPart implements
		IResourceChangeListener, IVcdMultiPageEditor, IVcdDiagram, Iupdate {

	private static VCDDefinitions vcdFromBackend = null;
	private static IFile file = null;
	
	private static final IFile getFile() {
		return file;
	}

	private static final void setFile(IFile file) {
		VcdMultiPageEditor.file = file;
	}

	private ConstraintsFactory _constraintsFactory;
	private MarkerFactory _markerFactory;
	private boolean _ctrlKey = false;
	private VCDTextEditor _vcdTextEditor = null;
	private VcdFactory _vcdFactory = null;
	private Mode _ghostMode = null;
	private ListConnections _listConnections;
	private double _markerZoomValue = 2f;
	private Shell _shell;
	private boolean _isSimulation = false;
	private TraceCollector _traceCollector;
	private double _traceCollectorZoomValue = 2.0;
	private VCDDefinitions _vcdModel;
	private double _zoomValue = 1;
	protected  VcdMultiPageEditorContributor _vcdMultiPageEditorContributor = null;
	private VcdZoom _vcdZoom = null;
	private FigureCanvasBase _figureCanvasBase = null;
	private VcdColorPreferences _colorSettings;
	private VcdMenu _vcdMenu;
	private static VcdColorPreferences _colorSettingsFromBackend = null;
	private boolean isdisposed = false;
	/**
	 * Creates a multi-page editor example.
	 */
	public VcdMultiPageEditor() {
		super();
		CountFigure.disp();
		/** When the editor is created by request from the backend 
		 * (see {@link #createNewVcdMultiPageEditor(VCDDefinitions, IFile, VcdColorPreferences)}) the color preferences
		 * are provided by the backend and stored in an intermediate variable _colorSettingsFromBackend.
		 * The test below is used in this case, otherwise a new {@link VcdColorPreferences} object is created. 
		 */
		if (_colorSettingsFromBackend != null){
			_colorSettings = _colorSettingsFromBackend;
		}else{
			_colorSettings = VcdColorPreferences.createColor();
		}
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
		_listConnections = createList();
		_vcdZoom = new VcdZoom(_listConnections, this);
		_figureCanvasBase = new FigureCanvasBase(this, _colorSettings);
		_vcdMenu = new VcdMenu(this);
	}

	
	public VcdColorPreferences getColorPreferences() {
		return _colorSettings;
	}

	public VcdMenu getVcdmenu() {
		return _vcdMenu;
	}

	
	public void setFocus() {
		try {
			super.setFocus();
			if (_vcdMenu==null){
				_vcdMenu=new VcdMenu(this);
			}
			IEditorActionBarContributor contributor = getEditorSite().getActionBarContributor();
			if (contributor instanceof MultiPageEditorActionBarContributor) {
				_vcdMultiPageEditorContributor = (VcdMultiPageEditorContributor) contributor ;	
				((MultiPageEditorActionBarContributor) contributor).setActivePage(this);				
			}
			if (_vcdZoom!=null) {
				_vcdZoom.setIczoom(ToolBarAPi.getDef().getCombozoom());
			}
			ToolBarAPi.getDef().setCurrentVcd(this);	
			if (_vcdModel!=null){
				ConstraintView.getCurrentConstraintView().setConstraint(_vcdModel.getConstraintList());
			}
			ConstraintView.getCurrentConstraintView().setFocus();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void setActiveVCDPage() {
		if (getPageCount() != 0)
			setActivePage(0);
	}

	@Override
	protected void setActivePage(int pageIndex) {
		try {
			if (_vcdMenu!=null){
				_vcdMenu.getFindAction();
				_vcdMenu.getOrderAction();
			}
			super.setActivePage(pageIndex);
			setFocus();
		} catch (Exception e) {
			ErrorConsole.printError(e);
		}
	}

	public void create() {
		_shell = getContainer().getShell();
		_figureCanvasBase.createListener(_listConnections, this);
		setPartName(getEditorInput().getName());
	}

	public void syncModel2Text() {
		try {
			if (_vcdModel==null){
				return ;
			}
			String s = _vcdModel.getVCDString();
			if (_vcdTextEditor != null){
				_vcdTextEditor.updateFull(s, true);
			}
		} catch (Exception ex) {
			ErrorConsole.printError(ex);
		}
	}

	public int getSizeFile(IFile f) {
		try {
			if (!f.exists()){
				return 0;
			}
			f.refreshLocal(0, null);
			int n = f.getContents().available();
			return n;
		} catch (Exception ex) {
			ErrorConsole.printError(ex);
			return 0;
		}
	}

	public boolean isNewFile(IFile f) {
		return f.equals(getFile());
	}

	public void setSimulationProgress(int step) {
		if (_figureCanvasBase!=null){
			_figureCanvasBase.setSimulationProgress(step);
		}
	}

	public ListConnections createList() {
		return new ListConnections(
				new ArrayList<Description>(),
				new ArrayList<ConstraintsConnection>(),
				new HashMap<ConstraintCommentCommand, ArrayList<ConstraintsConnection>>());
	}

	/**
	 * Used both at the beginning and at the end of simulation
	 * 
	 * 3 times
	 * 
	 * Creates page 0 of the multi-page editor, which shows VCD in graphical form.
	 * 
	 * This function handles several cases:
	 * <ul>
	 * 	 <li>when called from the backend manager, an {@link IFile} is given by the backend and this IFile actually does not
	 *       exists. This IFile is only used to help the IU framework to create the right editor for .vcd files. In this
	 *       case either the file is new or is empty. This will set the {@link VcdMultiPageEditor} in simulation mode.
	 *   <li>when called from the IU framework, by choosing Open on a .vcd file, if no editor is currently running for
	 *       this file, a new instance of {@link VcdMultiPageEditor} is created, then {@link #setInput(IEditorInput)}
	 *       if called on this instance.  
	 * </ul>
	 * In both cases, the {@link #getEditorInput()} function will return an {@link IEditorInput} that specifies the 
	 * {@link IFile}
	 */
	void createPage0() {
		try {
			_isSimulation = false;
			IFile ifloc = ((FileEditorInput) getEditorInput()).getFile();
			
			if (!isNewFile(ifloc) && getSizeFile(ifloc) != 0) {
				
				ErrorConsole.printOKln("VCD : Read " + ifloc.getName());
				String editorName = getEditorInput().getName();
				FileEditorInput fileEditorInput = ((FileEditorInput) getEditorInput());
				
				//
				new VcdDiagramReader().read(this, fileEditorInput.getStorage().getContents(), editorName, _colorSettings);
				if (_vcdModel == null){	
					return ;
				}
				
			} else {
				/* In simulation mode (called from the backend) the VCD model stored in the ScoreBoard has been
				 * temporarily stored in the vcdFromBackend field (see the createNewVcdMultiPageEditor() function)
				 * We get this instance and store it in the _vcdModel field which will hold a reference to the
				 * actual model to use.  */
				_isSimulation = true;
				ErrorConsole.printOKln("VCD : Simulation ");
				_vcdModel = vcdFromBackend;
				// In simulation mode, the following test should never be true !!
				if (_vcdModel == null) {
					_vcdModel = new VCDDefinitions();
					_vcdModel.setScope(new ScopeCommand(ScopeType.module, "default"));
				}
				//
				Collector.drawInEditor(this, _vcdModel, _colorSettings);
				_vcdModel.setFactory(_vcdFactory);
			}
			
			if (_vcdModel != null){
				//create a parent folder for the new generated VCD file
				_vcdModel.setVcdfolder(ifloc.getParent());
			}
		} catch (Throwable ex) {
			ErrorConsole.printError(ex, "---> Read/Init Error");
			if (ex instanceof RuntimeException)
				throw (RuntimeException) ex;
			throw new RuntimeException(ex);
		}
		
		if (_vcdModel.getFactory() == null) {
			_vcdModel.setFactory(getVcdFactory());
		}
		
		getVcdFactory().setVcdZoom(_vcdZoom);
		getVcdFactory().setVcdDef(_vcdModel);
		
		_constraintsFactory = new ConstraintsFactory(_listConnections, this);
		
		for (IConstraintData constraintData : _vcdModel.getConstraintList()) {
			if (constraintData.getConstraints() != null) {
				constraintData.getConstraints().setVcdFactory(_vcdModel.getFactory());
				constraintData.getConstraints().setList(_listConnections);
				constraintData.getConstraints().setIcc(_constraintsFactory);
			}
		}
		
		_markerFactory = new MarkerFactory(_listConnections, this);

		_figureCanvasBase.makeVcd(getContainer(), _isSimulation);
		
		if (_figureCanvasBase.getComposite() != null) {
			_figureCanvasBase.getComposite().getVerticalBar().setVisible(true);
			_figureCanvasBase.getComposite().getHorizontalBar().setVisible(true);
		}
		
		addPage(0, _figureCanvasBase.getComposite());
		
		//Add a title for this page
		setPageText(0, "Vcd Viewer");
		
		//Create all listeners for this VcdMultiPageEditor
		create();
		
		//To ensure the scroll bar still works even if there is no zoom action
		_figureCanvasBase.scrollUpdate();

		_figureCanvasBase.layoutall();
		
		setGhostMode(_ghostMode);
	}

	/**
	 * Creates page 1 of the multi-page editor, which contains a text editor.
	 */
	void createPage1() {
		try {
			FileEditorInput iep = (FileEditorInput) getEditorInput();
			IFile ifloc = iep.getFile();
			System.out.println(ifloc.getName() + " " + ifloc.getParent());
			ifloc.getParent().refreshLocal(IResource.DEPTH_ONE, null);
			if (!ifloc.exists()){
				return;
			}
			_vcdTextEditor = new VCDTextEditor(getVcdmenu(), !_isSimulation);
			int index = addPage(_vcdTextEditor, iep);
			setPageText(index, _vcdTextEditor.getTitle());
		} catch (Exception ex) {
			ErrorDialog.openError(getSite().getShell(),
					"Error creating nested text editor", ex.getMessage(), null);
			ErrorConsole.printError(ex);
		}

	}

	/**
	 * Called by the framework to create all the pages of a {@link MultiPageEditorPart}.
	 * Calls {@link #createPage0()} to create the graphical VCD view, and {@link #createPage1()} to create the text
	 * editor that contains the VCD in textual form. 
	 */
	@Override
	protected void createPages() {
		createPage0();
		createPage1();
		pageChange(0);
		setActivePage(0);
		setFocus();
	}

	public void print() {
		_figureCanvasBase.printer(getTitle());
	}
	
	/**
	 * The <code>MultiPageEditorPart</code> implementation of this
	 * <code>IWorkbenchPart</code> method disposes all nested editors.
	 * Subclasses may extend.
	 */
	public void vcdMultiPageEditorRefresh() {
		try {
				if ( isdisposed){
					return ;
				}
				ErrorConsole.println("Reload " + getTitle());
				CountFigure.disp();
				if (_colorSettings!=null){
					_colorSettings.updatecolor();
				}
				//remove all existed listeners
				ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
				if (getMyShell().isDisposed()){
					return;
				}
				
				if (_vcdZoom!=null){
					_vcdZoom.unSetVdt();
				}
				
				setFile( null);
				IFile ifloc = ((FileEditorInput) getEditorInput()).getFile();
				ifloc.getParent().refreshLocal(0, null);
				if (getPageCount() > 0){
					removePage(0);
				}
				if (getPageCount() == 1){
					removePage(0);
				}
				ToolBarAPi.getDef().unsetCurrentVcd(this);
				if (_vcdModel!=null ){
					if ( ConstraintView.exist()){
						ConstraintView.getCurrentConstraintView().unsetConstraint(_vcdModel.getConstraintList());
					}
				}
				
				IEditorActionBarContributor contributor = getEditorSite().getActionBarContributor();
				if (contributor != null && contributor instanceof VcdMultiPageEditorContributor) {
					((VcdMultiPageEditorContributor) contributor).unsetEditor(null);
				}
				if (_listConnections!=null){
					_listConnections.clearAll();
				}
				if( _traceCollector!=null){
					_traceCollector.clear();
				}
				if (_vcdFactory!=null){
					_vcdFactory.clear();
				}
				if (_vcdModel!=null){
					_vcdModel.getArraylistSimulationCommand().clear();
					_vcdModel.getArraylistDeclarationCommand().clear();
					_vcdModel.getConstraintCommentCommand().clear();
					_vcdModel.setScope(null);
				}
				if (_vcdTextEditor!=null){
					_vcdTextEditor.removemenu();
					_vcdTextEditor.dispose();
				}
				if (_vcdMultiPageEditorContributor!=null){
					_vcdMultiPageEditorContributor.dispose();
				}
				if (_figureCanvasBase!=null){
					_figureCanvasBase.dispose();
				}
				_figureCanvasBase = null;			
				_vcdMultiPageEditorContributor =null;
				_vcdModel = null;			
				_vcdFactory=null;
				_traceCollector= null;
				_vcdMenu = new VcdMenu(this);
				_listConnections = createList();
				_vcdZoom = new VcdZoom(_listConnections, this);
				_figureCanvasBase = new FigureCanvasBase(this, _colorSettings);
				createPage0();
				createPage1();
				_figureCanvasBase.getComposite().setFocus();
				_vcdZoom.setIczoom(ToolBarAPi.getDef().getCombozoom());
				_vcdZoom.applyScrollZoom();
				_vcdTextEditor=null;
				ToolBarAPi.getDef().setCurrentVcd(this);
				ConstraintView.getCurrentConstraintView().setFocus();
				ConstraintView.getCurrentConstraintView().resetConstraint(_vcdModel.getConstraintList());	
				ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
			} catch (Throwable ex) {
			ErrorConsole.printError(ex);
		}
	}


	/**
	 * The <code>MultiPageEditorPart</code> implementation of this
	 * <code>IWorkbenchPart</code> method disposes all nested editors.
	 * Subclasses may extend.
	 */
	@Override
	public void dispose() {
		try {
			CountFigure.disp();
			isdisposed = true;
			ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
			ToolBarAPi.getDef().unsetCurrentVcd(this);
			if (_vcdModel != null) {
				if (ConstraintView.exist()){
					ConstraintView.getCurrentConstraintView().unsetConstraint(_vcdModel.getConstraintList());
				}
				_vcdModel.getConstraintList().clear();
			}			
			_figureCanvasBase.dispose();
			_vcdZoom.unSetVdt();
			IEditorActionBarContributor contributor = getEditorSite().getActionBarContributor();
			if (contributor != null && contributor instanceof VcdMultiPageEditorContributor) {
				((VcdMultiPageEditorContributor) contributor).unsetEditor(null);
			}
			_listConnections.clearAll();
			_traceCollector.clear();
			_vcdFactory.clear();
			if (_vcdModel != null) {
				_vcdModel.getArraylistSimulationCommand().clear();
				for (IDeclarationCommand idc : _vcdModel.getArraylistDeclarationCommand()) {
					if (idc != null){
						idc.clear();
					}
				}
				_vcdModel.getArraylistDeclarationCommand().clear();
				_vcdModel.getConstraintCommentCommand().clear();
				_vcdModel.setScope(null);
			}
			if (_vcdTextEditor != null) {
				_vcdTextEditor.removemenu();
				_vcdTextEditor.dispose();
			}
			if (_vcdMultiPageEditorContributor != null){
				_vcdMultiPageEditorContributor.dispose();
			}
			if (_vcdMenu != null){
				_vcdMenu.clear();
			}
			_vcdMultiPageEditorContributor = null;
			_vcdModel = null;
			_colorSettings = null;
			_vcdFactory = null;
			_constraintsFactory = null;
			_listConnections = null;
			_markerFactory = null;
			_figureCanvasBase = null;
			_vcdZoom = null;
			_vcdMenu = null;
			_traceCollector = null;
			_vcdTextEditor = null;
			CountFigure.disp();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		super.dispose();
	}

	@Override
	protected void finalize() throws Throwable {
		
		ConstraintView.refreshCourant();
		System.out.println("Delete VCDEditor");
		CountFigure.disp();
		ErrorConsole.printOKln("Delete VCDEditor");
		super.finalize();
	}

	/**
	 * Saves the multi-page editor's document.
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		if (getPageCount() > 2)
			if (getEditor(1) != null)
				getEditor(1).doSave(monitor);
		if (_vcdTextEditor != null) {
			_vcdTextEditor.doSave(monitor);
			if (!isdisposed){
				setPageText(1, _vcdTextEditor.getTitle());
			}
			setInput(_vcdTextEditor.getEditorInput());
		}
		if (isdisposed){
			return;
		}
		removePage(0);
		createPage0();
	}

	/**
	 * Saves the multi-page editor's document as another file. Also updates the
	 * text for page 0's tab, and updates this multi-page editor's input to
	 * correspond to the nested editor's.
	 */
	@Override
	public void doSaveAs() {
		if (getEditor(0) != null)
			getEditor(0).doSaveAs();
		if (_vcdTextEditor != null) {
			_vcdTextEditor.doSaveAs();
			setPageText(1, _vcdTextEditor.getTitle());
			setInput(_vcdTextEditor.getEditorInput());
		}
	}

	public FigureCanvas getCanvas() {
		return _figureCanvasBase.getClockCanvas();
	}

	public ConstraintsFactory getConstraintsFactory() {
		return _constraintsFactory;
	}

	public MarkerFactory getMarkerFactory() {
		return _markerFactory;
	}

	public VcdFactory getVcdFactory() {
		return _vcdFactory;
	}

	public boolean isContainGhost() {
		if (getTraceCollector()==null)
			return false;
		return getTraceCollector().isContainGhost();
	}

	public ListConnections getListConnections() {
		return _listConnections;
	}

	public FigureCanvasBase getfcb() {
		return _figureCanvasBase;
	}

	public double getMarkerZoom() {
		return _markerZoomValue;
	}

	public Shell getMyShell() {
		return _shell;
	}

	public FigureCanvas getNames() {
		return _figureCanvasBase.getNames();
	}

	public FigureCanvas getScaleCanvas() {
		return _figureCanvasBase.getScaleCanvas();
	}

	public ToolBar getToolbar() {
		return _figureCanvasBase.getToolbar();
	}

	/**
	 * Method declared on IEditorPart
	 */
	public void gotoMarker(IMarker marker) {
		setActivePage(0);
		IDE.gotoMarker(getEditor(0), marker);
	}

	/**
	 * The <code>MultiPageEditorExample</code> implementation of this method
	 * checks that the input is an instance of <code>IFileEditorInput</code>.
	 */
	@Override
	public void init(IEditorSite site, IEditorInput editorInput)
			throws PartInitException {
		if (!(editorInput instanceof IFileEditorInput)){
			throw new PartInitException(
					"Invalid Input: Must be IFileEditorInput");
		}
		super.init(site, editorInput);
	}

	public boolean isCtrlKey() {
		return _ctrlKey;
	}

	public Mode isGhostMode() {
		return _ghostMode;
	}

	/**
	 * Method declared on IEditorPart.
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	public boolean isSimulation() {
		return _isSimulation;
	}

	int _activePageNumber = 0;

	/**
	 * @return the activePage
	 */
	public final int getActivePageID() {
		return _activePageNumber;
	}

	public final VCDTextEditor getTextEditor() {
		return _vcdTextEditor;
	}

	/**
	 * Calculates the contents of page 2 when the it is activated.
	 */
	@Override
	protected void pageChange(int newPageIndex) {
		_activePageNumber = newPageIndex;
		IEditorActionBarContributor contributor = getEditorSite()
				.getActionBarContributor();
		if (contributor != null && contributor instanceof MultiPageEditorActionBarContributor) {
			((MultiPageEditorActionBarContributor) contributor).setActivePage(this);
		}
		if (getEditor(newPageIndex) != null)
			super.pageChange(newPageIndex);
	}

	/**
	 * Closes all project files on project close.
	 */
	public void resourceChanged(final IResourceChangeEvent event) {

		if (event.getType() == IResourceChangeEvent.PRE_CLOSE) {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					IWorkbenchPage[] pages = getSite().getWorkbenchWindow().getPages();
					for (int i = 0; i < pages.length; i++) {
						if (((FileEditorInput) _vcdTextEditor.getEditorInput()).getFile().getProject().equals(event.getResource())) {
							IEditorPart editorPart = pages[i].findEditor(_vcdTextEditor.getEditorInput());
							pages[i].closeEditor(editorPart, true);
						}
					}
				}
			});
		}
	}

	public void setCtrlKey(boolean ctrlKey) {
		this._ctrlKey = ctrlKey;
	}

	public void setVcdFactory(VcdFactory _vcdFactory) {
		this._vcdFactory = _vcdFactory;
	}

	public void setGhostMode(Mode _ghostMode) {
		if (_ghostMode == null){
			return;
		}
		if (_ghostMode.isShow()) {
			if (getConstraintsFactory() != null)
				getConstraintsFactory().showAllGhost();
			if (_vcdMenu != null)
				_vcdMenu.setGhostSelected(Mode.show);
			getTraceCollector().ghostMode(true);
		}
		if (_ghostMode.isHide()) {
			if (getConstraintsFactory() != null)
				getConstraintsFactory().hideAllGhost();
			if (_vcdMenu != null)
				_vcdMenu.setGhostSelected(Mode.hide);
			getTraceCollector().ghostMode(false);
		}
		this._ghostMode = _ghostMode;
	}

	public void setMarkerZoom(double markerZoom) {
		this._markerZoomValue = markerZoom;
	}

	public void setSimulation(boolean simulation) {
		this._isSimulation = simulation;
	}

	public void setTc(TraceCollector _traceCollector) {
		this._traceCollector = _traceCollector;
	}

	public TraceCollector getTraceCollector() {
		return _traceCollector;
	}

	public void setTcZoom(double _traceCollectorZoomValue) {
		this._traceCollectorZoomValue = _traceCollectorZoomValue;
	}

	public double getTraceZoomValue() {
		return _traceCollectorZoomValue;
	}

	public void setVcdModel(VCDDefinitions _vcdDefinitions) {
		this._vcdModel = _vcdDefinitions;
	}

	public VCDDefinitions getVcdModel() {
		return _vcdModel;
	}

	public void setZoomValue(double zoom) {
		this._zoomValue = zoom;
	}

	public double getZoomValue() {
		return _zoomValue;
	}

	public VcdZoom getVcdzoom() {
		return _vcdZoom;
	}

	@Override
	public int update(int n1, int n2, boolean b) {
		return 0;
	}
	
	@Override
	public int update2(int n1, int n2, boolean b) {
		if (_figureCanvasBase!=null)
			return _figureCanvasBase.update2(n1, n2, b);
		return 0;
	}

	/**
	 * Creates a new VCD viewer for the associated VCD model, intended to be saved in textual form in the file f.
	 * 
	 * This function is called from the VCD generation backend manager. Based on the given IFile, its effect is
	 * either to create a new editor or to bring to front an existing one based on the same IFile. Function
	 * {@link IDE#openEditor(IWorkbenchPage, IFile)} is used for this purpose : the f.i.a.t.vcd.antlr plugin
	 * register the {@link VcdMultiPageEditor} as an editor for .vcd files.
	 * 
	 * If the call to this function leads to the creation of a new editor, the {@link #createPages()} function
	 * is eventually called by the UI framework.
	 *  
	 * @param vcdModel
	 * @param f
	 * @param colorPrefs
	 * @return
	 */
	public static VcdMultiPageEditor createNewVcdMultiPageEditor(VCDDefinitions vcdModel, IFile f, VcdColorPreferences colorPrefs) {
		try {
			vcdFromBackend = vcdModel;
			_colorSettingsFromBackend = colorPrefs;
			setFile(f);
			IWorkbench _iWorkbench = PlatformUI.getWorkbench();
			
			if (_iWorkbench.getActiveWorkbenchWindow() == null) {}
			
			IEditorPart editorInput = IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), f);
			editorInput.getEditorSite().getPart().setFocus();
			VcdMultiPageEditor _vcdMultiPageEditor = (VcdMultiPageEditor) editorInput;
			_vcdMultiPageEditor.setGhostMode(Mode.show);
			/* After the editor has been built and shown, we do not need any more to store the VCD model and the
			 * color settings in the variables dedicated to hold them when the editor is built from the backend */
			vcdFromBackend = null;
			setFile(null);
			_colorSettingsFromBackend = null;
			return _vcdMultiPageEditor;
		} catch (PartInitException ex) {
			ErrorConsole.printError(ex, "PartInitException");
		} catch (Throwable ex) {
			ErrorConsole.printError(ex);
		}
		vcdFromBackend = null;
		setFile(null);
		_colorSettingsFromBackend = null;
		return null;
	}

}