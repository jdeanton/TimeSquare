/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.antlr.editors;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.dialogs.HelpDialog;
import fr.inria.aoste.timesquare.vcd.menu.VcdMenu;

/**
 * Manages the installation/deinstallation of global actions for multi-page
 * editors. Responsible for the redirection of global actions to the active
 * editor. Multi-page contributor replaces the contributors for the individual
 * editors in the multi-page editor.
 */
public class VcdMultiPageEditorContributor extends MultiPageEditorActionBarContributor {
	private VcdMultiPageEditor _vcdMultiPageEditor = null;
	private IMenuManager _vcdMenuManager=null;
	private IEditorPart _globalEditorPart=null;
	private IMenuManager _syncMenu=null;
	private IMenuManager _findMenu=null;
	private Action _orderAction=null;
	private Action _showGhostAction=null;
	private Action _hideGhostAction=null;
	private Action _partialGhostAction=null;
	private Action _findAction=null;
	private IMenuManager _clockMenu=null;
	private IMenuManager _optionsMenu=null;
	private HelpDialog _helpDialog = null;

	/**
	 * Creates a multi-page contributor.
	 */
	public VcdMultiPageEditorContributor() {
		super();
	}
	
	/**
	 * Returns the action registed with the given text editor.
	 * 
	 * @return IAction or null if editor is null.
	 */
	protected IAction getAction(ITextEditor editor, String actionID) {
		return (editor == null ? null : editor.getAction(actionID));
	}

	@Override
	public void setActivePage(IEditorPart _editorPart) {

		if (_globalEditorPart == _editorPart) {
			createActions();
		}
		_globalEditorPart = _editorPart;
		IActionBars _actionBars = getActionBars();
		if (_actionBars == null) {
			if (_editorPart != null)
				if (_editorPart.getEditorSite() != null)
					_actionBars = _editorPart.getEditorSite().getActionBars();
		}
		if ((_actionBars != null)) {

			ITextEditor editor = (_editorPart instanceof ITextEditor) ? (ITextEditor) _editorPart : null;

			_actionBars.setGlobalActionHandler(ActionFactory.DELETE.getId(),
					getAction(editor, ITextEditorActionConstants.DELETE));
			_actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(),
					getAction(editor, ITextEditorActionConstants.UNDO));
			_actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(),
					getAction(editor, ITextEditorActionConstants.REDO));
			_actionBars.setGlobalActionHandler(ActionFactory.CUT.getId(),
					getAction(editor, ITextEditorActionConstants.CUT));
			_actionBars.setGlobalActionHandler(ActionFactory.COPY.getId(),
					getAction(editor, ITextEditorActionConstants.COPY));
			_actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(),
					getAction(editor, ITextEditorActionConstants.PASTE));
			_actionBars.setGlobalActionHandler(ActionFactory.SELECT_ALL.getId(),
					getAction(editor, ITextEditorActionConstants.SELECT_ALL));

			if (_editorPart instanceof VcdMultiPageEditor) {
				VcdMenu vcdmenu = ((VcdMultiPageEditor) _editorPart).getVcdmenu();
				if (vcdmenu!=null){
					_actionBars.setGlobalActionHandler(	ActionFactory.REFRESH.getId(), vcdmenu.getRefreshAction());
					Action printer=vcdmenu.getPrinterAction();
					if (printer!=null){
						_actionBars.setGlobalActionHandler(ActionFactory.PRINT.getId(),printer);
					}	
					_actionBars.setGlobalActionHandler(ActionFactory.FIND.getId(),vcdmenu.getFindAction());
				}
			}
			_actionBars.updateActionBars();
		}

	}

	public void unsetEditor(IEditorPart _editorPart) {
		if (_editorPart == _globalEditorPart){
			setActivePage(null);
		}
	}

	
	@Override
	public void setActiveEditor(IEditorPart part) {
		setActivePage(part);
	}

	private void createActions() {
		try {
			_vcdMenuManager.removeAll();
			updateMenu();
			IEditorPart editorPart = null;
			IWorkbenchWindow iww = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow();
			if (iww != null){
				if (iww.getActivePage() != null) {
					editorPart = (iww.getActivePage().getActiveEditor());
				}
			}
			if (editorPart instanceof VcdMultiPageEditor) {
				_vcdMultiPageEditor = (VcdMultiPageEditor) editorPart;
				ToolBarAPi.getDef().setCurrentVcd(_vcdMultiPageEditor);
			}
			if (_vcdMultiPageEditor == null){
				return;
			}
			VcdMenu _vcdMenu = _vcdMultiPageEditor.getVcdmenu();
			if (_vcdMenu==null){
				return ;
			}
			_vcdMultiPageEditor._vcdMultiPageEditorContributor = this;
			_orderAction = _vcdMenu.getOrderAction();
			_clockMenu.removeAll();
			_clockMenu.add(_orderAction);

			// Add "Show all ghost" to "Option" menu.
			_optionsMenu.removeAll();
			_optionsMenu.add(new Separator("ghost"));
			_optionsMenu.add(new Separator("sync"));
			_showGhostAction = _vcdMenu.getShowGhostAction();
			_optionsMenu.appendToGroup("ghost", _showGhostAction);
			

			_hideGhostAction = _vcdMenu.getHideGhostAction();
			_optionsMenu.appendToGroup("ghost",
					new ActionContributionItem(
					_hideGhostAction) {
				@Override
				public boolean isVisible() {
					return getAction().isEnabled();
				}
			});

			// Add "Partial" to "Option" menu.
			_partialGhostAction = _vcdMenu.getPartialGhostAction();
			_optionsMenu.appendToGroup("ghost", new ActionContributionItem(
					_partialGhostAction) {
				@Override
				public boolean isVisible() {
					return getAction().isEnabled();
				}
			});

			_syncMenu.removeAll();
			for (Action a : _vcdMenu.getSyncAction()) {
				_syncMenu.add(a);
			}
			_optionsMenu.appendToGroup("sync", _syncMenu);

			// Add "Find" to "Find" menu.
			_findAction = _vcdMenu.getFindAction();
			_findMenu.removeAll();
			_findMenu.add(_findAction);
			Action help = new Action("About") {
					@Override
					public boolean isEnabled() {
						return true;
					}

					@Override
					public void run() {
						if (_helpDialog == null)
							_helpDialog = new HelpDialog() {
								@Override
								public void close() {
									_helpDialog = null;
									super.close();
									_vcdMultiPageEditor.setFocus();
								}
							};
					}
				};			
			
			_vcdMenuManager.add(_vcdMenu.getRefreshAction());
			_vcdMenuManager.add(_vcdMenu.getVCDOption());
			_vcdMenuManager.add(_vcdMenu.getPrinterAction());
			_vcdMenuManager.add(help);
		} catch (Throwable e) {
			ErrorConsole.printError(e, " Ex");
			return;
		}
	}

	// Add "Vcd Editor" to tolls bar
	@Override
	public void contributeToMenu(IMenuManager _menuManager) {
		_vcdMenuManager = new MenuManager("&Vcd Editor") {
			@Override
			public boolean isDynamic() {
				return true;
			}

			@Override
			public boolean isDirty() {
				return true;
			}
		};
		_menuManager.prependToGroup(IWorkbenchActionConstants.MB_ADDITIONS,	_vcdMenuManager);
		updateMenu();
	}

	public void updateMenu() {
		createClockMenu(_vcdMenuManager);
		createOptionsMenu(_vcdMenuManager);
		createFindMenu(_vcdMenuManager);
	}

	public void createClockMenu(IMenuManager _menuManager) {
		// Add "Clock" to "VCD Editor" menu.
		_clockMenu = new MenuManager("&Clock");
		_menuManager.add(_clockMenu);
	}

	public void createOptionsMenu(IMenuManager _menuManager) {
		// Add "Option" to "VCD Editor" menu.
		_optionsMenu = new MenuManager("&Options");
		_menuManager.add(_optionsMenu);
		// Add "Synchronized" to "Option" menu.
		_syncMenu = new MenuManager("&Synchronized");
		_optionsMenu.add(_syncMenu);
	}

	public void createFindMenu(IMenuManager _menuManager) {
		// Add "Find" to "VCD Editor" menu.
		_findMenu = new MenuManager("&Find");
		_menuManager.add(_findMenu);
	}

	@Override
	public void contributeToToolBar(IToolBarManager _toolBarManager) {
		for (IContributionItem a : ToolBarAPi.getDef().getZoomApi()) {
			_toolBarManager.add(a);
		}
		_toolBarManager.update(true);
	}

}
