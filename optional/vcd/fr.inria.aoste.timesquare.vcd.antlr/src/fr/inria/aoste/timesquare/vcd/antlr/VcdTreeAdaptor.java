/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.antlr;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.Tree;

import fr.inria.aoste.timesquare.vcd.model.Comment;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.Uposition.Enumblocktype;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.command.DateCommand;
import fr.inria.aoste.timesquare.vcd.model.command.ScopeCommand;
import fr.inria.aoste.timesquare.vcd.model.command.TimeScaleCommand;
import fr.inria.aoste.timesquare.vcd.model.command.VersionCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.ScopeType;

public class VcdTreeAdaptor extends CommonTreeAdaptor {
	private int state = 0;
	// private boolean commentState = false;
	private VCDDefinitions vcd = new VCDDefinitions();
	private int level;
	private Tree tree;
	private int time = -1;
	private int pulse = -1;

	public int convertSimulationDate(Token arg0) {
		try {
			return Integer.valueOf(arg0.getText().substring(1));
		} catch (Exception ex) {
			return 0;
		}
	}

	private Uposition up = null;
	private void processPosition(Token token) {
		int state = token.getType();
		switch(state) {
		case VcdLexer.SIMULATION_TIME:
			String time = token.getText().substring(1);
			up = new Uposition(Integer.parseInt(time), Enumblocktype.SimulationTime);
			break;
		case VcdLexer.DATE:
			up = new Uposition(0, Enumblocktype.Date);
			break;
		case VcdLexer.ENDDEFINITIONS:
			up=new Uposition(0, Enumblocktype.EndDef)  ;
			break;
		case VcdLexer.SCOPE:
			up=new Uposition(0, Enumblocktype.Scope);
			break;
		case VcdLexer.TIMESCALE:
		  	up=new Uposition(0, Enumblocktype.TimeScale);
		  	break;
		case VcdLexer.VAR:
			up=new Uposition(0, Enumblocktype.Var);
			break;
		case VcdLexer.VERSION:
			up=new Uposition(0, Enumblocktype.Version);
			break;
		case VcdLexer.DUMPALL:
			up=new Uposition(0, Enumblocktype.DumpAll);
			break;
		case VcdLexer.DUMPOFF:
			up=new Uposition(0, Enumblocktype.DumpOff);
			break;
		case VcdLexer.DUMPON:
			up=new Uposition(0, Enumblocktype.DumpOn);
			break;
		case VcdLexer.DUMPVARS:
			up=new Uposition(0, Enumblocktype.DumpVars);
			break;
		}
	}
	
	private void processEndPosition() {
		state = 0;
		if (up!=null) up.setTheEnd(true);
	}

	private boolean inComment = false;
	@Override
	public Tree create(Token arg0) {
		if (arg0 == null)
			return (Tree) super.create(null);
		switch (state) {
		case 0:
			state = arg0.getType();
			processPosition(arg0);
			if (state == VcdLexer.SIMULATION_TIME) {
				time = convertSimulationDate(arg0);
				vcd.setOffSet(time);
				ValueChangeTree vct = new ValueChangeTree(arg0);
				vcd.addSimulation(vct.getCommand());
				return vct;
			}
			if (state == VcdLexer.SCOPE) {
				level = 1;
				return tree = new ScopeTree(arg0);
			}
			if (state == VcdLexer.VAR)
				return tree = new VarTree(arg0);
			return tree = (Tree) super.create(arg0);
		case VcdLexer.DATE:
			if (arg0.getType() == VcdLexer.END) {
				StringBuffer buf = new StringBuffer();
				for (int i = 0; i < tree.getChildCount(); i++) {
					if (i > 0)
						buf.append(' ');
					buf.append(tree.getChild(i).getText());
				}
				vcd.addDefinition(new DateCommand(buf.toString()));
				processEndPosition();
			}
			break;
		case VcdLexer.VERSION:
			if (arg0.getType() == VcdLexer.END) {
				vcd
						.addDefinition(new VersionCommand(tree.getChild(0)
								.getText(), tree.getChild(1).getText()));
				processEndPosition();
			}
			break;
		case VcdLexer.TIMESCALE:
			if (arg0.getType() == VcdLexer.END) {
				vcd.addDefinition(new TimeScaleCommand(tree.getChild(0)
						.getText(), tree.getChild(1).getText()));
				processEndPosition();
			}
			break;
		case VcdLexer.SCOPE:
			if (arg0.getType() == VcdLexer.VAR)
				return new VarTree(arg0);
			if (arg0.getType() == VcdLexer.SCOPE) {
				level++;
				return new ScopeTree(arg0);
			}
			if (arg0.getType() == VcdLexer.UPSCOPE) {
				level--;
				if (level == 0) {
					state = 0;
					vcd.setScope(((ScopeTree) tree).getScope());
				}
		    	up=new Uposition( 0, Enumblocktype.UpScope);				
			}
			break;
		case VcdLexer.SIMULATION_TIME:
			if(inComment) {
				if(arg0.getType() == VcdLexer.END) {
					System.out.println("Comment: is it used ?"+comment);
					comment = null;
					inComment = false;
				} else {
					if (comment == null) 
						comment = new Comment(arg0.getText(), up);
					else
						comment.addPart(arg0.getText());
				}
				break;
			}
			if (arg0.getType() == VcdLexer.SIMULATION_TIME) {
				processPosition(arg0);
				int newtime = convertSimulationDate(arg0);
				if (time != -1) {
					if (pulse == -1)
						pulse = newtime - time;
					else if (newtime - time < pulse)
						pulse = newtime - time;
				}
				// vcd.setPulse(pulse);
				time = newtime;
				ValueChangeTree vct = new ValueChangeTree(arg0);
				vcd.addSimulation(vct.getCommand());
				return vct;
			}
			if (arg0.getType() == VcdLexer.DUMPVARS
					|| arg0.getType() == VcdLexer.DUMPON
					|| arg0.getType() == VcdLexer.DUMPOFF
					|| arg0.getType() == VcdLexer.DUMPALL) {
				processPosition(arg0);
				state = arg0.getType();
				// ValueChangeTree vct = new ValueChangeTree(arg0);
				// vcd.setPulse(pulse);
				// vcd.addSimulation(vct.getCommand());
				return new ValueChangeTree(arg0);
			} else if (arg0.getType() == VcdLexer.COMMENT) {
				inComment = true;
			}
			/*
			 * if(arg0.getType() == VcdLexer.COMMENT){ state = arg0.getType();
			 * //commentState = true; return tree = (Tree) super.create(arg0); }
			 */
			break;
		case VcdLexer.DUMPALL:
		case VcdLexer.DUMPOFF:
		case VcdLexer.DUMPON:
		case VcdLexer.DUMPVARS:
			if (arg0.getType() == VcdLexer.END) {
				this.state = VcdLexer.SIMULATION_TIME;
			}
			break;
		case VcdLexer.VAR:
			if (arg0.getType() == VcdLexer.END) {
				processEndPosition();
				if (scope == null) {
					scope = new ScopeCommand(ScopeType.module, null);
					vcd.setScope(scope);
				}
				scope.addChild(((VarTree) tree).getVar());
			}
			return (Tree) super.create(arg0);
		case VcdLexer.COMMENT:
			if (arg0.getType() == VcdLexer.END) {
				vcd.addDefinition(this.comment.convert());
				this.comment = null;
				processEndPosition();
				break;
			}
			String s = arg0.getText();
			if (this.comment == null) this.comment = new Comment(s, up);
			else this.comment.addPart(s);
			break;
		default:
			if (arg0.getType() == VcdLexer.END) {
				processEndPosition();
			}
		}
		return (Tree) super.create(arg0);
	}
	private Comment comment = null;
	
	private ScopeCommand scope = null; // only used when no scope is defined

	public VCDDefinitions getVcd() {
		return vcd;
	}
}
