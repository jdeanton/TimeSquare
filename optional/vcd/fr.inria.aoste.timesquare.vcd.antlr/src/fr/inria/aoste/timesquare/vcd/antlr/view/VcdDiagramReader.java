/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.antlr.view;

import java.io.IOException;
import java.io.InputStream;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.antlr.VcdLexer;
import fr.inria.aoste.timesquare.vcd.antlr.VcdParser;
import fr.inria.aoste.timesquare.vcd.antlr.VcdTreeAdaptor;
import fr.inria.aoste.timesquare.vcd.model.visitor.Collector;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class VcdDiagramReader {

	/**
	 * Reads a .vcd file using an ANTLR parser and draws the diagram .
	 * 
	 * @param input
	 * @throws RecognitionException
	 */
	public void read(ANTLRStringStream input) throws RecognitionException {
		VcdLexer lexer = new VcdLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		VcdParser parser = new VcdParser(tokens);
		VcdTreeAdaptor adaptor = new VcdTreeAdaptor();
		parser.setTreeAdaptor(adaptor);
		parser.vcd_definitions();
		Collector.drawInWindow(adaptor.getVcd());
	}

	/**
	 * Reads a .vcd file using an ANTLR parser and draws the diagram in the editor.
	 * 
	 * @param editor
	 * @param file
	 * @param name
	 * @param mca
	 * @throws IOException
	 * @throws RecognitionException
	 */
	public void read(IVcdDiagram editor, InputStream file, String name, VcdColorPreferences mca)
			throws IOException, RecognitionException {
		ANTLRInputStream input = new ANTLRInputStream(file); // this version does not take the file name but the charSet encoding ???
		VcdLexer lexer = new VcdLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		VcdParser parser = new VcdParser(tokens);
		VcdTreeAdaptor adaptor = new VcdTreeAdaptor();
		parser.setTreeAdaptor(adaptor);
		parser.vcd_definitions();
		Collector.drawInEditor(editor, adaptor.getVcd(), mca);
	}

}
