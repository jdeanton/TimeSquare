/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.antlr.view;

import java.util.ArrayList;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.ViewPart;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;

/**
 * This class/view allowed to list, display or hide the constraints in the
 * selected VCD
 * 
 * @author benoit Ferrero
 */

public class ConstraintView extends ViewPart {
	static private ConstraintView courant;
	// static public ArrayList<IConstraintData> constraint = null;
	private static ArrayList<IConstraintData> localConstraint = null;
	private Composite parents = null;
	private Table table = null;
	private boolean b = true;
	private int size = 0;

	public ConstraintView() {
		setCourant(this);
		// System.out.println("Build View");
		// localConstraint = constraint;
	}

	/*private static final ArrayList<IConstraintData> getLocalConstraint() {
		return localConstraint;
	}*/

	private synchronized static final void setCourant(ConstraintView courant) {
		ConstraintView.courant = courant;
	}

	private synchronized static final void unsetAll(ConstraintView _courant) {
		if (ConstraintView.courant == _courant) {
			ConstraintView.courant = null;
			// ConstraintView.localConstraint=null;
		} else {
			System.err.println("Warning View" + (_courant != null) + " " + (ConstraintView.courant != null));
		}
	}

	private static final void setLocalConstraint(ArrayList<IConstraintData> localConstraint) {
		ConstraintView.localConstraint = localConstraint;
	}

	@Override
	public void createPartControl(Composite parent) {
		try {
			setCourant(this);
			parents = parent;
			// create a Table to store and show data
			table = new Table(parent, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION | SWT.CHECK);
			// GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
			// table.setLayoutData(data);
			table.setHeaderVisible(true);
			TableColumn column = null;
			// add column with check box
			column = new TableColumn(table, SWT.CENTER, 0);
			column.setText("");
			column.setMoveable(false);
			column.setWidth(25);
			column.setResizable(false);
			// add column with Constraint Name
			column = new TableColumn(table, SWT.LEFT, 1);
			column.setText("Constraint Name");
			column.setMoveable(false);
			column.setWidth(200);
			table.addMouseListener(new MouseListenerCV(table));
			draw();
			contributeToActionBars();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(new SelectAll("Select All"));
		manager.add(new DeSelectAll("DeSelect All"));
	}

	public final class DeSelectAll extends Action {
		public DeSelectAll(String text) {
			super(text);
		}

		@Override
		public void run() {
			for (int i = 0; i < table.getItemCount(); i++) {
				TableItem tableItem = table.getItem(i);
				IConstraintData data = (IConstraintData) tableItem.getData();
				if (data != null)
					if (data.getConstraints() != null)
					{
						if ( data.getConstraints().getIsConstraintVisible()==true)
							data.getConstraints().drawTableItemConstraints();
						data.getConstraints().setIsConstraintVisible(false);
						
					}
			}
			setFocus();
		}

	}

	public final class SelectAll extends Action {
		public SelectAll(String text) {
			super(text);
		}

		@Override
		public void run() {

			for (int i = 0; i < table.getItemCount(); i++) {
				TableItem tableItem = table.getItem(i);
				IConstraintData data = (IConstraintData) tableItem.getData();
				if (data != null)
					if (data.getConstraints() != null)
					{
						if ( data.getConstraints().getIsConstraintVisible()==false)
							data.getConstraints().drawTableItemConstraints();
						data.getConstraints().setIsConstraintVisible(true);
					}
			}
			setFocus();
		}

	}

	private  class MouseListenerCV implements MouseListener {
		private Table table;

		public MouseListenerCV(Table table) {
			this.table = table;
		}

		public void mouseDown(MouseEvent e) {
			// if it's a left click
			if (e.button == 1) {
				IConstraintData data = null;
				TableItem tableItem = table.getItem(new Point(e.x, e.y));
				if (tableItem == null)
					return;
				data = (IConstraintData) tableItem.getData();
				if (data == null) {
					ErrorConsole.printlnError("ERROR DATA NULL");
					return;
				}
				if (data.getConstraints() == null) {
					ErrorConsole.printlnError("--> IC are  NULL :  ");
					return;
				}
				if (tableItem.getChecked() == true) {
					// tableItem.setChecked(true);
					if (data.getConstraints().getIsConstraintVisible() == false) {
						data.getConstraints().setIsConstraintVisible(true);
						int ret = data.getConstraints().drawTableItemConstraints();
						// tableItem.setChecked(false);
						if (ret == -1) {
							tableItem.setChecked(true);
							ErrorConsole.printlnError("WARNING : at least one clock is hidden");
						}
					}
				} 
				else //if (tableItem.getChecked() == false) 
				{
					// tableItem.setChecked(false);
				
					// tableItem.setChecked(true);
					if (data.getConstraints().getIsConstraintVisible()) {
						// data.getIc().drawConstraint();
						data.getConstraints().setIsConstraintVisible(false);
						data.getConstraints().drawTableItemConstraints();
					}
				}
			}

		}

		public void mouseUp(MouseEvent e) {
			
		}

		public void mouseDoubleClick(MouseEvent e) {
		}
	}

	@Override
	public void setFocus() {
		if (table == null)
			return;
		for (int i = 0; i < table.getItemCount(); i++) {
			TableItem tableItem = table.getItem(i);
			IConstraintData data = (IConstraintData) tableItem.getData();
			if (data != null)
				if (data.getConstraints() != null)
					tableItem.setChecked(data.getConstraints().getIsConstraintVisible());
		}
	}

	private int draw() {
		try {
			if (parents == null)
				return -1;
			if (table == null)
				return -1;
			for (TableItem ti : table.getItems()) {
				ti.setData(null);
			}
			table.removeAll();
			if (localConstraint == null)
				return -1;
			for (IConstraintData var : localConstraint) {
				// if (var instanceof IConstraintData)
				{
					table.getColumn(1).pack();
					TableItem tableItem = new TableItem(table, SWT.LEFT_TO_RIGHT);
					tableItem.setChecked(false);
					if (var.toString() != null) {
						tableItem.setText(1, var.toString());
					} else {
						tableItem.setText(1, "...");
					}
					tableItem.setData(var);
				}
			}
			table.getColumn(1).pack();
			table.redraw();
		} catch (Throwable t) {
			ErrorConsole.printError(t);
			t.printStackTrace();
		}
		return 0;
	}

	@Override
	public void dispose() {
		unsetAll(this);
		// setLocalConstraint(null);
		// System.gc();
		super.dispose();
	}

	public static boolean exist() {
		return courant != null;
	}

	/**
	 * This method return the current view
	 * 
	 * @return ConstraintView
	 */
	public synchronized static ConstraintView getCurrentConstraintView() {
		if (courant == null) {
			try {
				PluginHelpers.getShowView("fr.inria.aoste.timesquare.vcd.antlr.constraintview");
				// courant = (ConstraintView)
				if (courant == null) {
					PluginHelpers.getCreateView("fr.inria.aoste.timesquare.vcd.antlr.constraintview");
				}
				// MyPluginManager.getShowView("fr.inria.aoste.timesquare.vcd.antlr.constraintview");
				if (courant != null) {
					courant.setFocus();
				}

			} catch (Throwable ex) {
				ErrorConsole.printError(ex, "Constraint View");
			}
		}
		return courant;
	}

	static synchronized public void refreshCourant() {
		if (courant != null) {
			courant.constraintViewRefresh();
		}
	}

	/**
	 * This method return the table
	 * 
	 * @return Table
	 */
	public Table getTable() {
		return table;
	}

	/**
	 * This method return the constraintText
	 * 
	 * @return String
	 */
	public ArrayList<IConstraintData> getConstraint() {
		return localConstraint;
	}

	/**
	 * This method save all the constraints
	 * 
	 */
	public void setConstraint(ArrayList<IConstraintData> constraint) {

		if (constraint == null) {
			size = 0;
			setLocalConstraint(null);
			draw();
			return;
		}
		int newSize = constraint.size();
		if (localConstraint != constraint || newSize != size || b) {
			setLocalConstraint(constraint);
			int r = draw();
			if (r == -1) {
				// localConstraint = null;
				size = 0;
				return;
			}

			size = newSize;
		}
	}

	public int constraintViewRefresh() {
		if (localConstraint != null) {
			if (size != localConstraint.size() || b) {
				size = localConstraint.size();
				Display.getDefault().syncExec(new Runnable() {

					public void run() {
						try {
							draw();
						} catch (Exception e) {
							System.err.println(e);
						}

					}
				});

			}
		}
		return 0;
	}

	public void unsetConstraint(ArrayList<IConstraintData> constraint) {
		if (localConstraint == constraint && constraint != null) {
			if (table != null)
				for (TableItem ti : table.getItems()) {
					ti.setData(null);
				}
			setConstraint(null);
			// localConstraint.clear();
		}
	}

	public void resetConstraint(ArrayList<IConstraintData> constraint) {

		if (constraint == null) {
			size = 0;
			setLocalConstraint(null);
			draw();
			return;
		}
		int newSize = constraint.size();

		setLocalConstraint(constraint);
		int r = draw();
		if (r == -1) {
			setLocalConstraint(null);
			size = 0;
			return;
		}

		size = newSize;

	}
}
