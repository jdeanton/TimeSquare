/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.antlr;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

import fr.inria.aoste.timesquare.vcd.model.command.ScopeCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.ScopeType;

public class ScopeTree extends CommonTree {
	private ScopeCommand scope = null;
	private ScopeType type = null;

	public ScopeTree(Token arg0) {
		super(arg0);
	}

	public ScopeCommand getScope() {
		return this.scope;
	}

	public void addChild(Tree arg0) {
		if (arg0.isNil())
			return;
		if (arg0 instanceof ScopeTree) {
			if (scope != null)
				scope.addChild(((ScopeTree) arg0).getScope());
		} else if (arg0 instanceof VarTree) {
			scope.addChild(((VarTree) arg0).getVar());
		} else {
			if (type == null)
				type = ScopeType.valueOf(arg0.getText());
			else {
				scope = new ScopeCommand(type, arg0.getText());
			}
		}
	}
}
