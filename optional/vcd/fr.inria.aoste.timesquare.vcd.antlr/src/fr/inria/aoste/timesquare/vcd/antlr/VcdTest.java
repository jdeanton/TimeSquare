/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.antlr;

import java.io.IOException;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;

public class VcdTest {
	/**
	 * @param args
	 * @throws IOException
	 * @throws IOException
	 * @throws RecognitionException
	 */
	public static void main(String[] args) throws IOException,
			RecognitionException {		
		ANTLRFileStream input = new ANTLRFileStream(
				"D:/Dev/CCSL/Charly/VHDL/CCSL-fm/re.vcd");
		VcdLexer lexer = new VcdLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		VcdParser parser = new VcdParser(tokens);
		VcdTreeAdaptor adaptor = new VcdTreeAdaptor();
		parser.setTreeAdaptor(adaptor);
		parser.vcd_definitions().getTree();
		@SuppressWarnings("unused")
		VCDDefinitions vcd = adaptor.getVcd();
//		System.out.println(vcd.visit(new StringVisitor()));
//		VarCollector vc = new VarCollector(vcd.getDecls());
//		vcd.visit(vc);
//		System.out.println(vcd.getConstraintCommentCommand());
		
//		RuleBuilder rb = new RuleBuilder();
//		vcd.visit((IVcdVisitor)rb);
		/*
		 * for (IVar var : vc) { System.out.println(var); }
		 */
//		ConstraintCommentVisitor cv = new ConstraintCommentVisitor();
//		vcd.visit(cv);
//		System.out.println("Clock : " + cv.getClock());
//		System.out.println("Function : " + cv.getFunction());
//		for (int i = 0; i < cv.getReferenceClocks().size() - 1; i++)
//			System.out.println("Reference clocks : "
//					+ cv.getReferenceClocks().get(i));
//		System.out.println("\n\n");
//		System.out.println(vcd.getConstraintCommentCommand());
		/*
		 * System.out.println("\n\nTree:"+tree.toStringTree()); for(int i = 0;
		 * i<tree.getChildCount(); i++)
		 * System.out.println("\t"+tree.getChild(i).toStringTree());
		 */
	}
}
