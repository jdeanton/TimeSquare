/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.antlr.editors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IAction;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.vcd.IComboZoom;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.antlr.ActivatorVcdAntlr;
import fr.inria.aoste.timesquare.vcd.antlr.IToolBarAPI;
import fr.inria.aoste.timesquare.vcd.antlr.action.ComboContributionItem;
import fr.inria.aoste.timesquare.vcd.antlr.action.Zoomaction;

public class ToolBarAPi implements IToolBarAPI {

	/**
	 * @return the combozoom
	 */
	public final IComboZoom getCombozoom() {
		return combozoom;
	}

	static private ToolBarAPi def = null;


	public synchronized static ToolBarAPi getDef() {
		if (def == null) {
			def = new ToolBarAPi();
		}
		return def;
	}

	public ToolBarAPi() {
		super();
	}

	private IVcdDiagram _currentVcd = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.inria.aoste.timesquare.vcd.antlr.editors.IToolBarAPI#getCourantvcd()
	 */
	public IVcdDiagram getCurrentVcd() {
		return _currentVcd;
	}

	public void setCurrentVcd(IVcdDiagram currentVcd) {
		this._currentVcd = currentVcd;
		combozoom.updateFocus();
	}

	public void unsetCurrentVcd(IVcdDiagram remove) {
		if (_currentVcd==remove){
			_currentVcd = null;	
			combozoom.updateFocus();
			zoomaction=null;
		}
	}
	
	private IAction[] zoomaction = null;
	private Collection<ContributionItem> zoomaci = null;
	private IComboZoom combozoom = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.inria.aoste.timesquare.vcd.antlr.editors.IToolBarAPI#getZoomApi()
	 */
	public Collection<ContributionItem> getZoomApi() {
		try {
			if (zoomaction == null) {
				zoomaction = new IAction[3];
				zoomaci = new ArrayList<ContributionItem>();

				zoomaction[0] = new Zoomaction(this, PluginHelpers
						.getImageDescriptor(ActivatorVcdAntlr.PLUGIN_ID,
								"icons/loupemoins.PNG"), "Zoom -", 0, this);
				zoomaction[1] = new Zoomaction(this, PluginHelpers
						.getImageDescriptor(ActivatorVcdAntlr.PLUGIN_ID,
								"icons/loupe1.png"), "Zoom 0", 1, this);
				zoomaction[2] = new Zoomaction(this, PluginHelpers
						.getImageDescriptor(ActivatorVcdAntlr.PLUGIN_ID,
								"icons/loupeplus.PNG"), "Zoom +", 2, this);

				for (int i = 0; i < 3; i++) {
					zoomaci.add( new ActionContributionItem(zoomaction[i]));

				}

				ComboContributionItem c = new ComboContributionItem(this);
				zoomaci.add(c);
				zoomaci = Collections.unmodifiableCollection(zoomaci);
				combozoom = c;
			}
		} catch (Throwable e) {
			ErrorConsole.printError(e);
		}
		return zoomaci;
	}

}
