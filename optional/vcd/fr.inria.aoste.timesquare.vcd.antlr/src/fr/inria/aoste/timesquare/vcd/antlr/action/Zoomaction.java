/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.antlr.action;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.antlr.IToolBarAPI;

public class Zoomaction extends Action {

	int type;
	IToolBarAPI tba;

	public Zoomaction(IToolBarAPI toolBarAPi, ImageDescriptor imagedesc,
			String tooltilps, int type, IToolBarAPI tba) {
		super();
		setImageDescriptor(imagedesc);
		setToolTipText(tooltilps);
		this.type = type;
		this.tba = tba;

	}

	/**
	 * @author mzc
	 */
	@Override
	public void run() {
		try {
			if (tba.getCurrentVcd() == null)
				return;
			setEnabled(false);
			double d = tba.getCurrentVcd().getTraceZoomValue();
			int counter = tba.getCurrentVcd().getVcdzoom().getZoomCounter();
			tba.getCurrentVcd().setZoomValue(1);
			if (type == 1) {
				if(counter>0){
					tba.getCurrentVcd().setZoomValue(0.5);
					tba.getCurrentVcd().getVcdzoom().setZoomCounter(counter - 1);
				}else if(counter<0){
					tba.getCurrentVcd().setZoomValue(2);
					tba.getCurrentVcd().getVcdzoom().setZoomCounter(counter + 1);
				}
				d=1.0;
			}else if (type == 0 && counter>-1) {
				tba.getCurrentVcd().getVcdzoom().setZoomCounter(counter - 1);
				tba.getCurrentVcd().setZoomValue(0.5);
				d=d*0.5;
			}else if (type == 2 && counter < 4) {
				tba.getCurrentVcd().setZoomValue(2);
				tba.getCurrentVcd().getVcdzoom().setZoomCounter(counter + 1);
				d=d*2;
			}
			tba.getCurrentVcd().setTcZoom(d);
			tba.getCurrentVcd().getVcdzoom().applyClickZoom();
			setEnabled(true);

		}

		catch (Exception ex) {
			ErrorConsole.printError(ex);
		}

	}
}