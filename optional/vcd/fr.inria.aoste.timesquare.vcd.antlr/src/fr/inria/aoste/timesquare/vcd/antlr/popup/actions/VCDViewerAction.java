/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.antlr.popup.actions;

import org.antlr.runtime.ANTLRInputStream;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.antlr.view.VcdDiagramReader;

public class VCDViewerAction implements IObjectActionDelegate {

	/**
	 * Constructor for Action1.
	 */
	public VCDViewerAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		Shell shell = new Shell();
		try {
			ANTLRInputStream input = new ANTLRInputStream(file.getContents(), file.getName());
			new VcdDiagramReader().read(input);
		} catch (Throwable ex) {
			ErrorConsole.printError(ex);
			MessageDialog
					.openError(shell, "Error in VCD viewer", ex.toString());
		}
	}

	private IFile file;

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */

	public void selectionChanged(IAction action, ISelection selection) {
		if (!(selection instanceof TreeSelection))
			return;
		TreeSelection ts = (TreeSelection) selection;
		myLoad(ts.getFirstElement());
	}

	private void myLoad(Object o) {
		if (!(o instanceof IFile))
			return;
		this.file = (IFile) o;
	}
}
