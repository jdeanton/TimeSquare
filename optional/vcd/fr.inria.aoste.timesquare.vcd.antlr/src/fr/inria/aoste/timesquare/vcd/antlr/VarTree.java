/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.antlr;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.VarType;

public class VarTree extends CommonTree {
	private VarCommand var = null;
	private VarType type = null;
	private int size = -1;
	private String ident = null;

	public VarTree() {
		super();
	}

	public VarTree(CommonTree arg0) {
		super(arg0);
	}

	public VarTree(Token arg0) {
		super(arg0);
	}

	public VarCommand getVar() {
		return this.var;
	}

	@Override
	public void addChild(Tree arg0) {
		if (arg0.isNil())
			return;
		if (arg0.getType() == VcdLexer.END)
			return;
		if (type == null)
			type = VarType.valueOf(arg0.getText());
		else if (size == -1)
			size = Integer.parseInt(arg0.getText());
		else if (ident == null)
			ident = arg0.getText();
		else
			var = new VarCommand(type, size, ident, arg0.getText());
	}
}
