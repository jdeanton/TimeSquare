/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.antlr;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

import fr.inria.aoste.timesquare.vcd.model.BinaryVectorValueChange;
import fr.inria.aoste.timesquare.vcd.model.RealVectorValueChange;
import fr.inria.aoste.timesquare.vcd.model.ScalarValueChange;
import fr.inria.aoste.timesquare.vcd.model.ValueChange;
import fr.inria.aoste.timesquare.vcd.model.command.SimulationCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.SimulationKeyword;

public class ValueChangeTree extends CommonTree {
	private SimulationCommand command;

	public ValueChangeTree(Token arg0) {
		super(arg0);
		String text = arg0.getText().substring(1);
		if (arg0.getType() == VcdLexer.SIMULATION_TIME) {
			int time = Integer.parseInt(text);
			command = new SimulationCommand(time, null);
		} else {
			SimulationKeyword keyword = SimulationKeyword.valueOf(text);
			command = new SimulationCommand(keyword);
		}

	}

	public SimulationCommand getCommand() {
		return this.command;
	}

	@Override
	public void addChild(Tree arg0) {
		if (arg0.isNil())
			return;
		String text;
		ValueChange vc = null;
		switch (arg0.getType()) {
		case VcdLexer.SCALAR_VALUE_CHANGE:
			text = arg0.getText();
			vc = new ScalarValueChange(text.charAt(0), text.substring(1));
			break;
		case VcdLexer.RVECTOR:
			assert (arg0.getChild(0).getType() == VcdLexer.REAL_VECTOR);
			text = arg0.getChild(0).getText();
			vc = new RealVectorValueChange(text.substring(1), arg0.getChild(1)
					.getText());
			break;
		case VcdLexer.BVECTOR:
			assert (arg0.getChild(0).getType() == VcdLexer.BINARY_VECTOR);
			text = arg0.getChild(0).getText();
			vc = new BinaryVectorValueChange(text.substring(1), arg0
					.getChild(1).getText());
			break;
		case VcdLexer.DUMPALL:
		case VcdLexer.DUMPOFF:
		case VcdLexer.DUMPON:
		case VcdLexer.DUMPVARS:
			assert (arg0 instanceof ValueChangeTree);
			ValueChangeTree child = (ValueChangeTree) arg0;

			command.merge(child.command);

			break;
		default:
			return;
		}
		command.addValueChange(vc);
	}
}
