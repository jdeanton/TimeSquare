/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
// $ANTLR 3.1.2 /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g 2009-06-09 11:20:50
package fr.inria.aoste.timesquare.vcd.antlr;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.IntStream;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;

public class VcdLexer extends Lexer {
    public static final int REAL_VECTOR=27;
    public static final int VERSION_TEXT=35;
    public static final int UPSCOPE=20;
    public static final int LETTER=33;
    public static final int IDENT_CHAR=34;
    public static final int VAR_TYPE=22;
    public static final int REFERENCE=29;
    public static final int VERSION=24;
    public static final int VALUE=31;
    public static final int EOF=-1;
    public static final int TIME_UNIT=19;
    public static final int SCALAR_VALUE_CHANGE=25;
    public static final int DUMPOFF=11;
    public static final int ENDDEFINITIONS=12;
    public static final int SIMULATION_TIME=6;
    public static final int DUMPON=10;
    public static final int WS=36;
    public static final int SCOPE=14;
    public static final int BVECTOR=5;
    public static final int DUMPVARS=9;
    public static final int DUMPALL=7;
    public static final int TIMESCALE=17;
    public static final int IDENTIFIER_CODE=23;
    public static final int RVECTOR=4;
    public static final int IDENT=16;
    public static final int BIT=30;
    public static final int VAR=21;
    public static final int DIGIT=32;
    public static final int SCOPE_TYPE=15;
    public static final int DATE=13;
    public static final int END=8;
    public static final int COMMENT=26;
    public static final int BINARY_VECTOR=28;
    public static final int STRING=37;
    public static final int DECIMAL_NUMBER=18;

    // delegates
    // delegators

    public VcdLexer() {} 
    public VcdLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public VcdLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "/local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g"; }

    // $ANTLR start "SIMULATION_TIME"
    public final void mSIMULATION_TIME() throws RecognitionException {
        try {
            int _type = SIMULATION_TIME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:79:2: ( '#' DECIMAL_NUMBER )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:79:4: '#' DECIMAL_NUMBER
            {
            match('#'); 
            mDECIMAL_NUMBER(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SIMULATION_TIME"

    // $ANTLR start "VAR_TYPE"
    public final void mVAR_TYPE() throws RecognitionException {
        try {
            int _type = VAR_TYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:10: ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' )
            int alt1=17;
            alt1 = dfa1.predict(input);
            switch (alt1) {
                case 1 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:12: 'event'
                    {
                    match("event"); 


                    }
                    break;
                case 2 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:22: 'integer'
                    {
                    match("integer"); 


                    }
                    break;
                case 3 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:34: 'parameter'
                    {
                    match("parameter"); 


                    }
                    break;
                case 4 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:48: 'real'
                    {
                    match("real"); 


                    }
                    break;
                case 5 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:57: 'realtime'
                    {
                    match("realtime"); 


                    }
                    break;
                case 6 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:70: 'reg'
                    {
                    match("reg"); 


                    }
                    break;
                case 7 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:78: 'supply0'
                    {
                    match("supply0"); 


                    }
                    break;
                case 8 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:81:90: 'supply1'
                    {
                    match("supply1"); 


                    }
                    break;
                case 9 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:5: 'tri'
                    {
                    match("tri"); 


                    }
                    break;
                case 10 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:15: 'triand'
                    {
                    match("triand"); 


                    }
                    break;
                case 11 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:26: 'trior'
                    {
                    match("trior"); 


                    }
                    break;
                case 12 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:36: 'trireg'
                    {
                    match("trireg"); 


                    }
                    break;
                case 13 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:47: 'tri0'
                    {
                    match("tri0"); 


                    }
                    break;
                case 14 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:56: 'tri1'
                    {
                    match("tri1"); 


                    }
                    break;
                case 15 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:65: 'wand'
                    {
                    match("wand"); 


                    }
                    break;
                case 16 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:74: 'wire'
                    {
                    match("wire"); 


                    }
                    break;
                case 17 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:82:83: 'wor'
                    {
                    match("wor"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "VAR_TYPE"

    // $ANTLR start "SCOPE_TYPE"
    public final void mSCOPE_TYPE() throws RecognitionException {
        try {
            int _type = SCOPE_TYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:84:13: ( 'begin' | 'fork' | 'function' | 'module' | 'task' )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 'b':
                {
                alt2=1;
                }
                break;
            case 'f':
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2=='o') ) {
                    alt2=2;
                }
                else if ( (LA2_2=='u') ) {
                    alt2=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;
                }
                }
                break;
            case 'm':
                {
                alt2=4;
                }
                break;
            case 't':
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:84:15: 'begin'
                    {
                    match("begin"); 


                    }
                    break;
                case 2 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:84:25: 'fork'
                    {
                    match("fork"); 


                    }
                    break;
                case 3 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:84:34: 'function'
                    {
                    match("function"); 


                    }
                    break;
                case 4 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:84:47: 'module'
                    {
                    match("module"); 


                    }
                    break;
                case 5 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:84:58: 'task'
                    {
                    match("task"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SCOPE_TYPE"

    // $ANTLR start "TIME_UNIT"
    public final void mTIME_UNIT() throws RecognitionException {
        try {
            int _type = TIME_UNIT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:86:13: ( 's' | 'ms' | 'us' | 'ns' | 'ps' | 'fs' )
            int alt3=6;
            switch ( input.LA(1) ) {
            case 's':
                {
                alt3=1;
                }
                break;
            case 'm':
                {
                alt3=2;
                }
                break;
            case 'u':
                {
                alt3=3;
                }
                break;
            case 'n':
                {
                alt3=4;
                }
                break;
            case 'p':
                {
                alt3=5;
                }
                break;
            case 'f':
                {
                alt3=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:86:15: 's'
                    {
                    match('s'); 

                    }
                    break;
                case 2 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:86:21: 'ms'
                    {
                    match("ms"); 


                    }
                    break;
                case 3 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:86:28: 'us'
                    {
                    match("us"); 


                    }
                    break;
                case 4 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:86:35: 'ns'
                    {
                    match("ns"); 


                    }
                    break;
                case 5 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:86:42: 'ps'
                    {
                    match("ps"); 


                    }
                    break;
                case 6 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:86:49: 'fs'
                    {
                    match("fs"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "TIME_UNIT"

    // $ANTLR start "REFERENCE"
    public final void mREFERENCE() throws RecognitionException {
        try {
            int _type = REFERENCE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:89:2: ( IDENT '[' DECIMAL_NUMBER ']' | IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']' )
            int alt4=2;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:89:4: IDENT '[' DECIMAL_NUMBER ']'
                    {
                    mIDENT(); 
                    match('['); 
                    mDECIMAL_NUMBER(); 
                    match(']'); 

                    }
                    break;
                case 2 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:90:4: IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']'
                    {
                    mIDENT(); 
                    match('['); 
                    mDECIMAL_NUMBER(); 
                    match(':'); 
                    mDECIMAL_NUMBER(); 
                    match(']'); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "REFERENCE"

    // $ANTLR start "ENDDEFINITIONS"
    public final void mENDDEFINITIONS() throws RecognitionException {
        try {
            int _type = ENDDEFINITIONS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:93:2: ( '$enddefinitions' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:93:4: '$enddefinitions'
            {
            match("$enddefinitions"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ENDDEFINITIONS"

    // $ANTLR start "DATE"
    public final void mDATE() throws RecognitionException {
        try {
            int _type = DATE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:96:6: ( '$date' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:96:8: '$date'
            {
            match("$date"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DATE"

    // $ANTLR start "SCOPE"
    public final void mSCOPE() throws RecognitionException {
        try {
            int _type = SCOPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:97:9: ( '$scope' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:97:11: '$scope'
            {
            match("$scope"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SCOPE"

    // $ANTLR start "TIMESCALE"
    public final void mTIMESCALE() throws RecognitionException {
        try {
            int _type = TIMESCALE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:98:10: ( '$timescale' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:98:12: '$timescale'
            {
            match("$timescale"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "TIMESCALE"

    // $ANTLR start "UPSCOPE"
    public final void mUPSCOPE() throws RecognitionException {
        try {
            int _type = UPSCOPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:99:9: ( '$upscope' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:99:11: '$upscope'
            {
            match("$upscope"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "UPSCOPE"

    // $ANTLR start "END"
    public final void mEND() throws RecognitionException {
        try {
            int _type = END;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:100:5: ( '$end' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:100:7: '$end'
            {
            match("$end"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "END"

    // $ANTLR start "VAR"
    public final void mVAR() throws RecognitionException {
        try {
            int _type = VAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:101:5: ( '$var' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:101:7: '$var'
            {
            match("$var"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "VAR"

    // $ANTLR start "VERSION"
    public final void mVERSION() throws RecognitionException {
        try {
            int _type = VERSION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:102:9: ( '$version' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:102:11: '$version'
            {
            match("$version"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "VERSION"

    // $ANTLR start "DUMPALL"
    public final void mDUMPALL() throws RecognitionException {
        try {
            int _type = DUMPALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:103:9: ( '$dumpall' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:103:11: '$dumpall'
            {
            match("$dumpall"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DUMPALL"

    // $ANTLR start "DUMPON"
    public final void mDUMPON() throws RecognitionException {
        try {
            int _type = DUMPON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:104:8: ( '$dumpon' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:104:10: '$dumpon'
            {
            match("$dumpon"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DUMPON"

    // $ANTLR start "DUMPOFF"
    public final void mDUMPOFF() throws RecognitionException {
        try {
            int _type = DUMPOFF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:105:9: ( '$dumpoff' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:105:11: '$dumpoff'
            {
            match("$dumpoff"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DUMPOFF"

    // $ANTLR start "DUMPVARS"
    public final void mDUMPVARS() throws RecognitionException {
        try {
            int _type = DUMPVARS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:106:9: ( '$dumpvars' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:106:11: '$dumpvars'
            {
            match("$dumpvars"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DUMPVARS"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:107:9: ( '$comment' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:107:11: '$comment'
            {
            match("$comment"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "SCALAR_VALUE_CHANGE"
    public final void mSCALAR_VALUE_CHANGE() throws RecognitionException {
        try {
            int _type = SCALAR_VALUE_CHANGE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:110:2: ( ( BIT | VALUE ) IDENTIFIER_CODE )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:110:4: ( BIT | VALUE ) IDENTIFIER_CODE
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='1')||input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            mIDENTIFIER_CODE(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SCALAR_VALUE_CHANGE"

    // $ANTLR start "DECIMAL_NUMBER"
    public final void mDECIMAL_NUMBER() throws RecognitionException {
        try {
            int _type = DECIMAL_NUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:113:2: ( ( '-' )? ( DIGIT )+ )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:113:4: ( '-' )? ( DIGIT )+
            {
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:113:4: ( '-' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='-') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:113:4: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:113:9: ( DIGIT )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:113:9: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "DECIMAL_NUMBER"

    // $ANTLR start "REAL_VECTOR"
    public final void mREAL_VECTOR() throws RecognitionException {
        try {
            int _type = REAL_VECTOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:115:2: ( ( 'r' | 'R' ) ( '-' )? ( DIGIT )* '.' ( DIGIT )+ )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:115:4: ( 'r' | 'R' ) ( '-' )? ( DIGIT )* '.' ( DIGIT )+
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:115:14: ( '-' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='-') ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:115:14: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:115:19: ( DIGIT )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='0' && LA8_0<='9')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:115:19: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            match('.'); 
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:115:30: ( DIGIT )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='0' && LA9_0<='9')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:115:30: DIGIT
            	    {
            	    mDIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "REAL_VECTOR"

    // $ANTLR start "BINARY_VECTOR"
    public final void mBINARY_VECTOR() throws RecognitionException {
        try {
            int _type = BINARY_VECTOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:118:2: ( ( 'b' | 'B' ) ( BIT | VALUE )+ )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:118:4: ( 'b' | 'B' ) ( BIT | VALUE )+
            {
            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:118:14: ( BIT | VALUE )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='0' && LA10_0<='1')||LA10_0=='X'||LA10_0=='Z'||LA10_0=='x'||LA10_0=='z') ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='1')||input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "BINARY_VECTOR"

    // $ANTLR start "IDENT"
    public final void mIDENT() throws RecognitionException {
        try {
            int _type = IDENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:119:7: ( ( LETTER | '_' ) ( LETTER | DIGIT | '_' | '@' | '.' | '-' )* )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:119:9: ( LETTER | '_' ) ( LETTER | DIGIT | '_' | '@' | '.' | '-' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:119:22: ( LETTER | DIGIT | '_' | '@' | '.' | '-' )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='-' && LA11_0<='.')||(LA11_0>='0' && LA11_0<='9')||(LA11_0>='@' && LA11_0<='Z')||LA11_0=='_'||(LA11_0>='a' && LA11_0<='z')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:
            	    {
            	    if ( (input.LA(1)>='-' && input.LA(1)<='.')||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='@' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IDENT"

    // $ANTLR start "VERSION_TEXT"
    public final void mVERSION_TEXT() throws RecognitionException {
        try {
            int _type = VERSION_TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:121:2: ( ( LETTER | DIGIT ) ( LETTER | DIGIT | IDENT_CHAR | '.' )+ )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:121:4: ( LETTER | DIGIT ) ( LETTER | DIGIT | IDENT_CHAR | '.' )+
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:121:19: ( LETTER | DIGIT | IDENT_CHAR | '.' )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='!' && LA12_0<='^')||(LA12_0>='`' && LA12_0<='~')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:
            	    {
            	    if ( (input.LA(1)>='!' && input.LA(1)<='^')||(input.LA(1)>='`' && input.LA(1)<='~') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "VERSION_TEXT"

    // $ANTLR start "IDENTIFIER_CODE"
    public final void mIDENTIFIER_CODE() throws RecognitionException {
        try {
            int _type = IDENTIFIER_CODE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:123:2: ( IDENT_CHAR ( DIGIT | LETTER | IDENT_CHAR | '.' | '_' )* )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:123:4: IDENT_CHAR ( DIGIT | LETTER | IDENT_CHAR | '.' | '_' )*
            {
            mIDENT_CHAR(); 
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:123:15: ( DIGIT | LETTER | IDENT_CHAR | '.' | '_' )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>='!' && LA13_0<='~')) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:
            	    {
            	    if ( (input.LA(1)>='!' && input.LA(1)<='~') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IDENTIFIER_CODE"

    // $ANTLR start "BIT"
    public final void mBIT() throws RecognitionException {
        try {
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:126:6: ( '0' | '1' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='1') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "BIT"

    // $ANTLR start "DIGIT"
    public final void mDIGIT() throws RecognitionException {
        try {
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:129:9: ( '0' .. '9' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:129:11: '0' .. '9'
            {
            matchRange('0','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "DIGIT"

    // $ANTLR start "VALUE"
    public final void mVALUE() throws RecognitionException {
        try {
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:132:7: ( 'x' | 'X' | 'z' | 'Z' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:
            {
            if ( input.LA(1)=='X'||input.LA(1)=='Z'||input.LA(1)=='x'||input.LA(1)=='z' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "VALUE"

    // $ANTLR start "LETTER"
    public final void mLETTER() throws RecognitionException {
        try {
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:135:8: ( 'a' .. 'z' | 'A' .. 'Z' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "LETTER"

    // $ANTLR start "IDENT_CHAR"
    public final void mIDENT_CHAR() throws RecognitionException {
        try {
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:137:13: ( '!' .. '-' | '/' | ':' .. '@' | '[' .. '^' | '`' | '{' .. '~' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:
            {
            if ( (input.LA(1)>='!' && input.LA(1)<='-')||input.LA(1)=='/'||(input.LA(1)>=':' && input.LA(1)<='@')||(input.LA(1)>='[' && input.LA(1)<='^')||input.LA(1)=='`'||(input.LA(1)>='{' && input.LA(1)<='~') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "IDENT_CHAR"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:139:4: ( ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+ )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:139:6: ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+
            {
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:139:6: ( ' ' | '\\t' | ( ( '\\n' | '\\r' ) ) )+
            int cnt14=0;
            loop14:
            do {
                int alt14=4;
                switch ( input.LA(1) ) {
                case ' ':
                    {
                    alt14=1;
                    }
                    break;
                case '\t':
                    {
                    alt14=2;
                    }
                    break;
                case '\n':
                case '\r':
                    {
                    alt14=3;
                    }
                    break;

                }

                switch (alt14) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:139:7: ' '
            	    {
            	    match(' '); 

            	    }
            	    break;
            	case 2 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:139:11: '\\t'
            	    {
            	    match('\t'); 

            	    }
            	    break;
            	case 3 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:140:3: ( ( '\\n' | '\\r' ) )
            	    {
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:140:3: ( ( '\\n' | '\\r' ) )
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:140:4: ( '\\n' | '\\r' )
            	    {
            	    if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}

            	     /*newline();*/

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);

             skip(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:142:9: ( '\"' ( . )* '\"' )
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:142:12: '\"' ( . )* '\"'
            {
            match('\"'); 
            // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:142:16: ( . )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0=='\"') ) {
                    alt15=2;
                }
                else if ( ((LA15_0>='\u0000' && LA15_0<='!')||(LA15_0>='#' && LA15_0<='\uFFFF')) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:142:16: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "STRING"

    public void mTokens() throws RecognitionException {
        // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:8: ( SIMULATION_TIME | VAR_TYPE | SCOPE_TYPE | TIME_UNIT | REFERENCE | ENDDEFINITIONS | DATE | SCOPE | TIMESCALE | UPSCOPE | END | VAR | VERSION | DUMPALL | DUMPON | DUMPOFF | DUMPVARS | COMMENT | SCALAR_VALUE_CHANGE | DECIMAL_NUMBER | REAL_VECTOR | BINARY_VECTOR | IDENT | VERSION_TEXT | IDENTIFIER_CODE | WS | STRING )
        int alt16=27;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:10: SIMULATION_TIME
                {
                mSIMULATION_TIME(); 

                }
                break;
            case 2 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:26: VAR_TYPE
                {
                mVAR_TYPE(); 

                }
                break;
            case 3 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:35: SCOPE_TYPE
                {
                mSCOPE_TYPE(); 

                }
                break;
            case 4 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:46: TIME_UNIT
                {
                mTIME_UNIT(); 

                }
                break;
            case 5 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:56: REFERENCE
                {
                mREFERENCE(); 

                }
                break;
            case 6 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:66: ENDDEFINITIONS
                {
                mENDDEFINITIONS(); 

                }
                break;
            case 7 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:81: DATE
                {
                mDATE(); 

                }
                break;
            case 8 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:86: SCOPE
                {
                mSCOPE(); 

                }
                break;
            case 9 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:92: TIMESCALE
                {
                mTIMESCALE(); 

                }
                break;
            case 10 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:102: UPSCOPE
                {
                mUPSCOPE(); 

                }
                break;
            case 11 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:110: END
                {
                mEND(); 

                }
                break;
            case 12 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:114: VAR
                {
                mVAR(); 

                }
                break;
            case 13 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:118: VERSION
                {
                mVERSION(); 

                }
                break;
            case 14 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:126: DUMPALL
                {
                mDUMPALL(); 

                }
                break;
            case 15 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:134: DUMPON
                {
                mDUMPON(); 

                }
                break;
            case 16 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:141: DUMPOFF
                {
                mDUMPOFF(); 

                }
                break;
            case 17 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:149: DUMPVARS
                {
                mDUMPVARS(); 

                }
                break;
            case 18 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:158: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 19 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:166: SCALAR_VALUE_CHANGE
                {
                mSCALAR_VALUE_CHANGE(); 

                }
                break;
            case 20 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:186: DECIMAL_NUMBER
                {
                mDECIMAL_NUMBER(); 

                }
                break;
            case 21 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:201: REAL_VECTOR
                {
                mREAL_VECTOR(); 

                }
                break;
            case 22 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:213: BINARY_VECTOR
                {
                mBINARY_VECTOR(); 

                }
                break;
            case 23 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:227: IDENT
                {
                mIDENT(); 

                }
                break;
            case 24 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:233: VERSION_TEXT
                {
                mVERSION_TEXT(); 

                }
                break;
            case 25 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:246: IDENTIFIER_CODE
                {
                mIDENTIFIER_CODE(); 

                }
                break;
            case 26 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:262: WS
                {
                mWS(); 

                }
                break;
            case 27 :
                // /local/home/bferrero/workspace/open2/fr.inria.aoste.timesquare.vcd.antlr/resources/Vcd.g:1:265: STRING
                {
                mSTRING(); 

                }
                break;

        }

    }


    protected DFA1 dfa1 = new DFA1(this);
    protected DFA4 dfa4 = new DFA4(this);
    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA1_eotS =
        "\21\uffff\1\31\1\33\15\uffff";
    static final String DFA1_eofS =
        "\40\uffff";
    static final String DFA1_minS =
        "\1\145\3\uffff\1\145\1\165\1\162\2\141\1\160\1\151\3\uffff\1\154"+
        "\1\uffff\1\160\1\60\1\164\1\154\10\uffff\1\171\1\60\2\uffff";
    static final String DFA1_maxS =
        "\1\167\3\uffff\1\145\1\165\1\162\1\157\1\147\1\160\1\151\3\uffff"+
        "\1\154\1\uffff\1\160\1\162\1\164\1\154\10\uffff\1\171\1\61\2\uffff";
    static final String DFA1_acceptS =
        "\1\uffff\1\1\1\2\1\3\7\uffff\1\17\1\20\1\21\1\uffff\1\6\4\uffff"+
        "\1\12\1\13\1\14\1\15\1\16\1\11\1\5\1\4\2\uffff\1\7\1\10";
    static final String DFA1_specialS =
        "\40\uffff}>";
    static final String[] DFA1_transitionS = {
            "\1\1\3\uffff\1\2\6\uffff\1\3\1\uffff\1\4\1\5\1\6\2\uffff\1\7",
            "",
            "",
            "",
            "\1\10",
            "\1\11",
            "\1\12",
            "\1\13\7\uffff\1\14\5\uffff\1\15",
            "\1\16\5\uffff\1\17",
            "\1\20",
            "\1\21",
            "",
            "",
            "",
            "\1\22",
            "",
            "\1\23",
            "\1\27\1\30\57\uffff\1\24\15\uffff\1\25\2\uffff\1\26",
            "\1\32",
            "\1\34",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\35",
            "\1\36\1\37",
            "",
            ""
    };

    static final short[] DFA1_eot = DFA.unpackEncodedString(DFA1_eotS);
    static final short[] DFA1_eof = DFA.unpackEncodedString(DFA1_eofS);
    static final char[] DFA1_min = DFA.unpackEncodedStringToUnsignedChars(DFA1_minS);
    static final char[] DFA1_max = DFA.unpackEncodedStringToUnsignedChars(DFA1_maxS);
    static final short[] DFA1_accept = DFA.unpackEncodedString(DFA1_acceptS);
    static final short[] DFA1_special = DFA.unpackEncodedString(DFA1_specialS);
    static final short[][] DFA1_transition;

    static {
        int numStates = DFA1_transitionS.length;
        DFA1_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA1_transition[i] = DFA.unpackEncodedString(DFA1_transitionS[i]);
        }
    }

    class DFA1 extends DFA {

        public DFA1(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = DFA1_eot;
            this.eof = DFA1_eof;
            this.min = DFA1_min;
            this.max = DFA1_max;
            this.accept = DFA1_accept;
            this.special = DFA1_special;
            this.transition = DFA1_transition;
        }
        public String getDescription() {
            return "81:1: VAR_TYPE : ( 'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1' | 'tri' | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor' );";
        }
    }
    static final String DFA4_eotS =
        "\10\uffff";
    static final String DFA4_eofS =
        "\10\uffff";
    static final String DFA4_minS =
        "\1\101\3\55\2\60\2\uffff";
    static final String DFA4_maxS =
        "\3\172\2\71\1\135\2\uffff";
    static final String DFA4_acceptS =
        "\6\uffff\1\1\1\2";
    static final String DFA4_specialS =
        "\10\uffff}>";
    static final String[] DFA4_transitionS = {
            "\32\1\4\uffff\1\1\1\uffff\32\1",
            "\2\2\1\uffff\12\2\6\uffff\33\2\1\3\3\uffff\1\2\1\uffff\32\2",
            "\2\2\1\uffff\12\2\6\uffff\33\2\1\3\3\uffff\1\2\1\uffff\32\2",
            "\1\4\2\uffff\12\5",
            "\12\5",
            "\12\5\1\7\42\uffff\1\6",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        public String getDescription() {
            return "88:1: REFERENCE : ( IDENT '[' DECIMAL_NUMBER ']' | IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']' );";
        }
    }
    static final String DFA16_eotS =
        "\1\uffff\1\31\4\36\1\52\10\36\1\31\1\36\1\103\1\31\1\103\3\36\1"+
        "\31\2\uffff\1\31\1\112\2\36\1\uffff\1\41\1\36\1\uffff\2\36\1\52"+
        "\5\36\1\uffff\6\36\1\132\2\36\1\52\1\36\3\52\3\136\7\31\1\uffff"+
        "\2\103\1\uffff\2\31\2\uffff\1\36\2\41\3\36\1\164\1\165\1\36\1\164"+
        "\3\36\1\164\1\36\1\uffff\3\36\1\uffff\6\136\11\31\1\36\1\41\1\106"+
        "\2\36\1\164\2\uffff\4\36\2\164\1\u0099\2\164\1\36\1\u0099\2\36\2"+
        "\136\1\106\1\u00a2\5\31\1\u00a8\2\31\1\164\2\41\5\36\1\164\1\36"+
        "\1\uffff\1\u0099\2\36\4\136\1\31\1\uffff\1\u00b8\4\31\1\uffff\2"+
        "\31\1\106\4\36\2\164\1\36\1\u0099\1\136\2\106\1\31\1\uffff\3\31"+
        "\1\u00ce\4\31\1\164\2\36\2\164\1\36\2\136\2\31\1\u00d9\2\31\1\uffff"+
        "\4\31\1\36\1\164\1\u0099\1\106\1\31\1\u00e2\1\uffff\1\u00e3\2\31"+
        "\1\u00e6\1\u00e7\1\u00e8\1\164\1\31\2\uffff\1\u00ea\1\31\3\uffff"+
        "\1\31\1\uffff\1\u00ed\1\31\1\uffff\3\31\1\u00f2\1\uffff";
    static final String DFA16_eofS =
        "\u00f3\uffff";
    static final String DFA16_minS =
        "\1\11\1\55\15\41\1\143\2\41\1\60\3\41\1\55\1\0\2\uffff\1\60\3\41"+
        "\1\uffff\2\55\1\uffff\10\41\1\uffff\21\41\1\156\1\141\1\143\1\151"+
        "\1\160\1\141\1\157\1\uffff\2\41\1\uffff\2\0\2\uffff\1\41\2\60\14"+
        "\41\1\uffff\3\41\1\uffff\3\41\1\55\2\41\1\144\1\164\1\155\1\157"+
        "\1\155\1\163\2\162\1\155\1\41\1\55\4\41\2\uffff\15\41\1\55\3\41"+
        "\1\145\2\160\1\145\1\143\1\41\1\163\1\155\1\41\2\60\7\41\1\uffff"+
        "\3\41\2\60\2\41\1\145\1\uffff\1\41\1\141\1\145\1\163\1\157\1\uffff"+
        "\1\151\1\145\11\41\1\55\2\41\1\146\1\uffff\1\154\1\146\1\141\1\41"+
        "\1\143\1\160\1\157\1\156\6\41\2\60\1\151\1\154\1\41\1\146\1\162"+
        "\1\uffff\1\141\1\145\1\156\1\164\4\41\1\156\1\41\1\uffff\1\41\1"+
        "\163\1\154\4\41\1\151\2\uffff\1\41\1\145\3\uffff\1\164\1\uffff\1"+
        "\41\1\151\1\uffff\1\157\1\156\1\163\1\41\1\uffff";
    static final String DFA16_maxS =
        "\1\176\1\71\15\176\1\166\2\176\1\71\3\176\1\172\1\uffff\2\uffff"+
        "\1\71\3\176\1\uffff\1\71\1\172\1\uffff\10\176\1\uffff\21\176\1\156"+
        "\1\165\1\143\1\151\1\160\1\145\1\157\1\uffff\2\176\1\uffff\2\uffff"+
        "\2\uffff\1\176\1\71\1\135\14\176\1\uffff\3\176\1\uffff\3\176\1\172"+
        "\2\176\1\144\1\164\1\155\1\157\1\155\1\163\2\162\1\155\1\176\1\71"+
        "\4\176\2\uffff\15\176\1\71\3\176\1\145\2\160\1\145\1\143\1\176\1"+
        "\163\1\155\1\176\1\71\1\135\7\176\1\uffff\3\176\1\71\1\135\2\176"+
        "\1\145\1\uffff\1\176\1\166\1\145\1\163\1\157\1\uffff\1\151\1\145"+
        "\11\176\1\71\2\176\1\146\1\uffff\1\154\1\156\1\141\1\176\1\143\1"+
        "\160\1\157\1\156\6\176\1\71\1\135\1\151\1\154\1\176\1\146\1\162"+
        "\1\uffff\1\141\1\145\1\156\1\164\4\176\1\156\1\176\1\uffff\1\176"+
        "\1\163\1\154\4\176\1\151\2\uffff\1\176\1\145\3\uffff\1\164\1\uffff"+
        "\1\176\1\151\1\uffff\1\157\1\156\1\163\1\176\1\uffff";
    static final String DFA16_acceptS =
        "\30\uffff\1\32\1\31\4\uffff\1\27\2\uffff\1\30\10\uffff\1\4\30\uffff"+
        "\1\24\2\uffff\1\5\2\uffff\1\33\1\1\17\uffff\1\26\3\uffff\1\23\25"+
        "\uffff\1\2\1\25\43\uffff\1\3\10\uffff\1\13\5\uffff\1\14\17\uffff"+
        "\1\7\25\uffff\1\10\12\uffff\1\17\10\uffff\1\16\1\20\2\uffff\1\12"+
        "\1\15\1\22\1\uffff\1\21\2\uffff\1\11\4\uffff\1\6";
    static final String DFA16_specialS =
        "\27\uffff\1\0\57\uffff\1\2\1\1\u00aa\uffff}>";
    static final String[] DFA16_transitionS = {
            "\2\30\2\uffff\1\30\22\uffff\1\30\1\31\1\27\1\1\1\17\10\31\1"+
            "\22\1\uffff\1\31\2\21\10\23\7\31\1\25\1\24\17\25\1\20\5\25\1"+
            "\16\1\25\1\16\4\31\1\26\1\31\1\25\1\11\2\25\1\2\1\12\2\25\1"+
            "\3\3\25\1\13\1\15\1\25\1\4\1\25\1\5\1\6\1\7\1\14\1\25\1\10\1"+
            "\16\1\25\1\16\4\31",
            "\1\32\2\uffff\12\33",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\25\35"+
            "\1\34\4\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\15\35"+
            "\1\42\14\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\1\43"+
            "\21\35\1\44\7\35\4\41",
            "\14\41\1\46\1\50\1\41\12\47\6\41\33\35\1\37\3\41\1\40\1\41"+
            "\4\35\1\45\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\24\35"+
            "\1\51\5\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\1\54"+
            "\20\35\1\53\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\1\55"+
            "\7\35\1\56\5\35\1\57\13\35\4\41",
            "\14\41\2\35\1\41\2\61\10\35\6\41\30\35\1\61\1\35\1\61\1\37"+
            "\3\41\1\40\1\41\4\35\1\60\22\35\1\61\1\35\1\61\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\16\35"+
            "\1\62\3\35\1\64\1\35\1\63\5\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\16\35"+
            "\1\65\3\35\1\66\7\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\22\35"+
            "\1\67\7\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\22\35"+
            "\1\70\7\35\4\41",
            "\14\73\1\71\1\35\1\73\12\35\6\73\1\71\32\35\1\72\3\73\1\40"+
            "\1\73\32\35\4\73",
            "\1\102\1\75\1\74\15\uffff\1\76\1\77\1\100\1\101",
            "\14\41\1\46\1\50\1\41\12\47\6\41\33\35\1\37\3\41\1\40\1\41"+
            "\32\35\4\41",
            "\15\73\1\41\1\73\12\104\7\73\32\41\4\73\1\uffff\1\73\32\41"+
            "\4\73",
            "\12\105",
            "\17\41\12\104\45\41\1\uffff\37\41",
            "\14\41\2\35\1\41\2\61\10\35\6\41\30\35\1\61\1\35\1\61\1\37"+
            "\3\41\1\40\1\41\27\35\1\61\1\35\1\61\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\2\40\1\uffff\12\40\6\uffff\33\40\1\106\3\uffff\1\40\1\uffff"+
            "\32\40",
            "\41\111\1\110\1\107\134\110\uff81\111",
            "",
            "",
            "\12\33",
            "\17\31\12\33\105\31",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\113\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "",
            "\1\114\2\uffff\12\115",
            "\2\40\1\uffff\12\40\6\uffff\33\40\1\106\3\uffff\1\40\1\uffff"+
            "\32\40",
            "",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\23\35"+
            "\1\116\6\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\21\35"+
            "\1\117\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\1\120"+
            "\5\35\1\121\23\35\4\41",
            "\14\41\1\35\1\50\1\41\12\47\6\41\33\35\1\37\3\41\1\40\1\41"+
            "\32\35\4\41",
            "\14\41\1\35\1\50\1\41\12\47\6\41\33\35\1\37\3\41\1\40\1\41"+
            "\32\35\4\41",
            "\14\41\2\35\1\41\12\122\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\17\35"+
            "\1\123\12\35\4\41",
            "",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\10\35"+
            "\1\124\21\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\22\35"+
            "\1\125\7\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\15\35"+
            "\1\126\14\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\21\35"+
            "\1\127\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\21\35"+
            "\1\130\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\6\35"+
            "\1\131\23\35\4\41",
            "\14\41\2\35\1\41\2\61\10\35\6\41\30\35\1\61\1\35\1\61\1\37"+
            "\3\41\1\40\1\41\27\35\1\61\1\35\1\61\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\21\35"+
            "\1\133\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\15\35"+
            "\1\134\14\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\3\35"+
            "\1\135\26\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\141\2\140\1\141\12\140\6\141\33\140\1\137\3\141\1\142\1"+
            "\141\32\140\4\141",
            "\14\141\1\143\2\141\12\144\45\141\1\uffff\37\141",
            "\76\141\1\uffff\37\141",
            "\1\145",
            "\1\146\23\uffff\1\147",
            "\1\150",
            "\1\151",
            "\1\152",
            "\1\153\3\uffff\1\154",
            "\1\155",
            "",
            "\17\41\12\104\45\41\1\uffff\37\41",
            "\17\31\12\105\105\31",
            "",
            "\41\111\1\110\1\107\134\110\uff81\111",
            "\41\111\1\110\1\107\134\110\uff81\111",
            "",
            "",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\15\35"+
            "\1\156\14\35\4\41",
            "\12\115",
            "\12\115\1\157\42\uffff\1\160",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\161\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\1\162"+
            "\31\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\13\35"+
            "\1\163\16\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\122\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\17\35"+
            "\1\166\12\35\4\41",
            "\14\41\2\35\1\41\1\172\1\173\10\35\6\41\33\35\1\37\3\41\1\40"+
            "\1\41\1\167\15\35\1\170\2\35\1\171\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\12\35"+
            "\1\174\17\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\3\35"+
            "\1\175\26\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\176\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\10\35"+
            "\1\177\21\35\4\41",
            "",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\12\35"+
            "\1\u0080\17\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\2\35"+
            "\1\u0081\27\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\24\35"+
            "\1\u0082\5\35\4\41",
            "",
            "\14\141\1\143\2\141\12\144\45\141\1\uffff\37\141",
            "\14\141\2\140\1\141\12\140\6\141\33\140\1\137\3\141\1\142\1"+
            "\141\32\140\4\141",
            "\76\141\1\uffff\37\141",
            "\2\142\1\uffff\12\142\6\uffff\33\142\1\u0083\3\uffff\1\142"+
            "\1\uffff\32\142",
            "\17\141\12\144\45\141\1\uffff\37\141",
            "\17\141\12\144\1\u0084\42\141\1\u0085\1\141\1\uffff\37\141",
            "\1\u0086",
            "\1\u0087",
            "\1\u0088",
            "\1\u0089",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "\1\u008d",
            "\1\u008e",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\23\35"+
            "\1\u008f\6\35\4\41",
            "\1\u0090\2\uffff\12\u0091",
            "\76\41\1\uffff\37\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\6\35"+
            "\1\u0092\23\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\14\35"+
            "\1\u0093\15\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\23\35"+
            "\1\u0094\6\35\4\41",
            "",
            "",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\13\35"+
            "\1\u0095\16\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\15\35"+
            "\1\u0096\14\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\21\35"+
            "\1\u0097\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\u0098\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\15\35"+
            "\1\u009a\14\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\23\35"+
            "\1\u009b\6\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\13\35"+
            "\1\u009c\16\35\4\41",
            "\1\u009d\2\uffff\12\u009e",
            "\14\141\1\u009f\2\141\12\u00a0\45\141\1\uffff\37\141",
            "\76\141\1\136\37\141",
            "\103\31\1\u00a1\32\31",
            "\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\136\31",
            "\1\u00a9",
            "\1\u00aa",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\12\u0091",
            "\12\u0091\43\uffff\1\u00ab",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\u00ac\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\u00ad\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\10\35"+
            "\1\u00ae\21\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\30\35"+
            "\1\u00af\1\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\3\35"+
            "\1\u00b0\26\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\6\35"+
            "\1\u00b1\23\35\4\41",
            "",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\10\35"+
            "\1\u00b2\21\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\u00b3\25\35\4\41",
            "\12\u009e",
            "\12\u009e\1\u00b4\42\uffff\1\u00b5",
            "\17\141\12\u00a0\45\141\1\uffff\37\141",
            "\17\141\12\u00a0\43\141\1\u00b6\1\141\1\uffff\37\141",
            "\1\u00b7",
            "",
            "\136\31",
            "\1\u00b9\15\uffff\1\u00ba\6\uffff\1\u00bb",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00be",
            "",
            "\1\u00bf",
            "\1\u00c0",
            "\76\41\1\uffff\37\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\21\35"+
            "\1\u00c1\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\23\35"+
            "\1\u00c2\6\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\14\35"+
            "\1\u00c3\15\35\4\41",
            "\14\41\2\35\1\41\1\u00c4\1\u00c5\10\35\6\41\33\35\1\37\3\41"+
            "\1\40\1\41\32\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\16\35"+
            "\1\u00c6\13\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\1\u00c7\2\uffff\12\u00c8",
            "\136\136",
            "\76\141\1\136\37\141",
            "\1\u00c9",
            "",
            "\1\u00ca",
            "\1\u00cc\7\uffff\1\u00cb",
            "\1\u00cd",
            "\136\31",
            "\1\u00cf",
            "\1\u00d0",
            "\1\u00d1",
            "\1\u00d2",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\u00d3\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\4\35"+
            "\1\u00d4\25\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\15\35"+
            "\1\u00d5\14\35\4\41",
            "\12\u00c8",
            "\12\u00c8\43\uffff\1\u00d6",
            "\1\u00d7",
            "\1\u00d8",
            "\136\31",
            "\1\u00da",
            "\1\u00db",
            "",
            "\1\u00dc",
            "\1\u00dd",
            "\1\u00de",
            "\1\u00df",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\21\35"+
            "\1\u00e0\10\35\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\136\136",
            "\1\u00e1",
            "\136\31",
            "",
            "\136\31",
            "\1\u00e4",
            "\1\u00e5",
            "\136\31",
            "\136\31",
            "\136\31",
            "\14\41\2\35\1\41\12\35\6\41\33\35\1\37\3\41\1\40\1\41\32\35"+
            "\4\41",
            "\1\u00e9",
            "",
            "",
            "\136\31",
            "\1\u00eb",
            "",
            "",
            "",
            "\1\u00ec",
            "",
            "\136\31",
            "\1\u00ee",
            "",
            "\1\u00ef",
            "\1\u00f0",
            "\1\u00f1",
            "\136\31",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( SIMULATION_TIME | VAR_TYPE | SCOPE_TYPE | TIME_UNIT | REFERENCE | ENDDEFINITIONS | DATE | SCOPE | TIMESCALE | UPSCOPE | END | VAR | VERSION | DUMPALL | DUMPON | DUMPOFF | DUMPVARS | COMMENT | SCALAR_VALUE_CHANGE | DECIMAL_NUMBER | REAL_VECTOR | BINARY_VECTOR | IDENT | VERSION_TEXT | IDENTIFIER_CODE | WS | STRING );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA16_23 = input.LA(1);

                        s = -1;
                        if ( (LA16_23=='\"') ) {s = 71;}

                        else if ( (LA16_23=='!'||(LA16_23>='#' && LA16_23<='~')) ) {s = 72;}

                        else if ( ((LA16_23>='\u0000' && LA16_23<=' ')||(LA16_23>='\u007F' && LA16_23<='\uFFFF')) ) {s = 73;}

                        else s = 25;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA16_72 = input.LA(1);

                        s = -1;
                        if ( (LA16_72=='\"') ) {s = 71;}

                        else if ( (LA16_72=='!'||(LA16_72>='#' && LA16_72<='~')) ) {s = 72;}

                        else if ( ((LA16_72>='\u0000' && LA16_72<=' ')||(LA16_72>='\u007F' && LA16_72<='\uFFFF')) ) {s = 73;}

                        else s = 25;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA16_71 = input.LA(1);

                        s = -1;
                        if ( (LA16_71=='\"') ) {s = 71;}

                        else if ( (LA16_71=='!'||(LA16_71>='#' && LA16_71<='~')) ) {s = 72;}

                        else if ( ((LA16_71>='\u0000' && LA16_71<=' ')||(LA16_71>='\u007F' && LA16_71<='\uFFFF')) ) {s = 73;}

                        else s = 25;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 16, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}