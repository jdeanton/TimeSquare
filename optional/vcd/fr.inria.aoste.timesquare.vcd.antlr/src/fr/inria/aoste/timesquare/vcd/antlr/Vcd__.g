lexer grammar Vcd;
@members {
int time=-1;
ListCommentary  acm=new ListCommentary();
Uposition up=null;
       
public int getTime()
{
	return time;  
}

public ListCommentary getAcm() 
{
	return acm;
}
public int stringtime(String s)
{
	time=Integer.parseInt(s);
	up = new Uposition(time,Uposition.Enumblocktype.SimulationTime );
	return time;
}

public	int addcomment(String s)
{
	//System.out.println(time+   " " +s );
	acm.add(new Commentary(s,up));
	return 0;
}
	
}
@header {package fr.inria.aoste.timesquare.vcd.antlr;
import fr.inria.aoste.timesquare.vcd.model.Commentary;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.ListCommentary;
}

// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 110
SIMULATION_TIME
	:	'#' DECIMAL_NUMBER
		{ String s=getText();
		 stringtime(new String (s.substring(1))); }
		; //TODO time
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 116
VAR_TYPE	:	'event' | 'integer' | 'parameter' | 'real' | 'realtime' | 'reg' | 'supply0' | 'supply1'
		|	'tri'   | 'triand' | 'trior' | 'trireg' | 'tri0' | 'tri1' | 'wand' | 'wire' | 'wor';
		
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 119
SCOPE_TYPE 	:	'begin' | 'fork' | 'function' |	'module' | 'task';
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 121
TIME_UNIT  	:	's' | 'ms' | 'us' | 'ns' | 'ps'	| 'fs';

// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 123
REFERENCE
	:	IDENT '[' DECIMAL_NUMBER ']' 
	|	IDENT '[' DECIMAL_NUMBER ':' DECIMAL_NUMBER ']';
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 127
ENDDEFINITIONS
	:	'$enddefinitions' {  up=new Uposition( 0,Uposition.Enumblocktype.EndDef  )  ;  };
	
// status  constraint
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 131
COMMENT	:
	'$comment' .* '$end' 
	 {  
	 String s=getText();	
	 addcomment(new String ( s.substring(8, s.length()-4)));
	 }
	 { skip();
	 };
	 


	 
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 143
DATE	:	'$date'  	{  up=new Uposition( 0,Uposition.Enumblocktype.Date  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 144
SCOPE   :	'$scope' 	{  up=new Uposition( 0,Uposition.Enumblocktype.Scope  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 145
TIMESCALE:	'$timescale'  	{  up=new Uposition( 0,Uposition.Enumblocktype.TimeScale  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 146
UPSCOPE	:	'$upscope'    	{  up=new Uposition( 0,Uposition.Enumblocktype.UpScope  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 147
END	:	'$end'		{ if (up!=null) up.setTheend(true);  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 148
VAR	:	'$var'		{  up=new Uposition( 0,Uposition.Enumblocktype.Var  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 149
VERSION	:	'$version'	{  up=new Uposition( 0,Uposition.Enumblocktype.Version  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 150
DUMPALL	:	'$dumpall'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpAll  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 151
DUMPON	:	'$dumpon'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpOn  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 152
DUMPOFF	:	'$dumpoff'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpOff  )  ;  };
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 153
DUMPVARS:	'$dumpvars'	{  up=new Uposition( 0,Uposition.Enumblocktype.DumpVars  )  ;  };
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 155
SCALAR_VALUE_CHANGE
	:	(BIT|VALUE) IDENTIFIER_CODE;

// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 158
DECIMAL_NUMBER
	:	'-'? (BIT|DIGIT)+;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 160
REAL_VECTOR 
	:	('r'|'R') '-'? (BIT|DIGIT)* '.' (BIT|DIGIT)+; 

// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 163
BINARY_VECTOR	
	:	('b'|'B') (BIT|VALUE)+;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 165
IDENT	:	(LETTER|'_') (LETTER|DIGIT|BIT|'_'|'@'|'.'|'-')*;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 166
VERSION_TEXT
	:	(LETTER|DIGIT|BIT) (LETTER|DIGIT|BIT|IDENT_CHAR|'.')+;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 168
IDENTIFIER_CODE
	:	IDENT_CHAR (DIGIT | BIT | LETTER | IDENT_CHAR |'.' |'_')*;
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 170
DIGIT   :	'2'..'9';
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 171
VALUE	:	'x' | 'X' | 'z' | 'Z';
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 172
BIT 	:	 '0' | '1';
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 173
LETTER	:	'a'..'z' | 'A'..'Z';
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 174
fragment
IDENT_CHAR  :	 '!'..'-' | '/' | ':'..'@' | '['..'^' | '`' | '{'..'~';
	
// $ANTLR src "D:\SHELLEY\Antlr\Vcd.g" 177
WS	:	(' '|'\t'|
		(('\n'| '\r')  { /*newline();*/}   )
		)+ { skip(); };

