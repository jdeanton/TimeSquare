/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.antlr.action;

import java.util.ArrayList;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IComboZoom;
import fr.inria.aoste.timesquare.vcd.antlr.IToolBarAPI;

public class ComboContributionItem extends ContributionItem implements IComboZoom {

	ArrayList<Double> _arraylistDouble = new ArrayList<Double>();
	Combo _combo = null;
	IToolBarAPI _toolBarAPI = null;
	private ToolItem toolitem;

	static public final class RenameFocusListener implements FocusListener {
		public void focusGained(FocusEvent e) {}
		public void focusLost(FocusEvent e) {}
	}

	public  final class ComboSelectionListener implements SelectionListener {
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}
		public void widgetSelected(SelectionEvent e) {
			try {
				if (_toolBarAPI.getCurrentVcd() == null){
					return;
				}
				Double old = _toolBarAPI.getCurrentVcd().getTraceZoomValue();
				String s = ((Combo) e.getSource()).getText();
				if (s == null){
					return;
				}
				if (s.length() == 0){
					return;
				}
				//Mettre la valeur de d en pourcent
				Double d = Double.parseDouble(s) / 100;
				if ((d < 0.001) || (d > 5000)) {
					ErrorConsole.println("Zoom : Out of order");
					return;
				}
				_toolBarAPI.getCurrentVcd().setZoomValue(old / d);
				_toolBarAPI.getCurrentVcd().setTcZoom(d);
				_toolBarAPI.getCurrentVcd().getVcdzoom().applyClickZoom();
			} catch (Exception ex) {
				ErrorConsole.printError(ex);
			}
		}
	}

	public ComboContributionItem(IToolBarAPI tba) {
		this._toolBarAPI = tba;
		_arraylistDouble.add(6.25);
		_arraylistDouble.add(12.5);
		_arraylistDouble.add(25.0);
		_arraylistDouble.add(50.0);
		_arraylistDouble.add(100.0);
		_arraylistDouble.add(200.0);
		_arraylistDouble.add(300.0);
		_arraylistDouble.add(400.0);

	}

	protected Control createControl(Composite parent) {
		_combo = new Combo(parent, SWT.DROP_DOWN);
		_combo.addSelectionListener(new ComboSelectionListener());
		_combo.addFocusListener(new RenameFocusListener());
		updateitem();
		select(100.0);
		if (toolitem != null)
			toolitem.setWidth(80);
		return _combo;
	}

	@Override
	public final void fill(Composite parent) {
		createControl(parent);
	}

	@Override
	public void fill(ToolBar parent, int index) {
		toolitem = new ToolItem(parent, SWT.SEPARATOR, index);
		Control control = createControl(parent);
		toolitem.setControl(control);
	}

	@Override
	public boolean isEnabled() {
		if (_toolBarAPI.getCurrentVcd() == null)
			return false;
		return true;
	}

	public int select(Double d) {
		int n = 0;
		n = _arraylistDouble.indexOf(d);
		if (n != -1) {
			_combo.select(n);
			_combo.setToolTipText("Zoom in " + d + " % for VCD");
			return 1;
		}
		n = 0;
		for (double _doubleElement : _arraylistDouble) {
			if (_doubleElement > d)
				break;
			n++;
		}
		_arraylistDouble.add(n, d);
		updateitem();
		n = _arraylistDouble.indexOf(d);
		_combo.select(n);
		_combo.setToolTipText("Zoom in " + d + " % for VCD");
		return 0;
	}

	public int updateitem() {
		_combo.removeAll();
		ArrayList<String> _arraylistString = new ArrayList<String>();
		for (Double _doubleElement : _arraylistDouble) {
			_arraylistString.add(_doubleElement.toString());
		}
		_combo.setItems(_arraylistString.toArray(new String[_arraylistString.size()]));
		return 0;
	}

	public int updateFocus() {
		if (_combo == null)
			return -1;
		if (_toolBarAPI == null)
			return -1;
		if (_toolBarAPI.getCurrentVcd() == null)
			return -1;
		if (_combo.isDisposed())
			return -1;
		_combo.removeAll();
		_combo.setItems(_toolBarAPI.getCurrentVcd().getVcdzoom().getZoomlist().arrayOf());
		_combo.select(_toolBarAPI.getCurrentVcd().getVcdzoom().getZoomlist().getIndice());
		return 0;
	}
}