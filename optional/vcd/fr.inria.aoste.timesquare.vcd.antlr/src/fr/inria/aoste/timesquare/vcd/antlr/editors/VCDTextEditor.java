/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.antlr.editors;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;

import fr.inria.aoste.timesquare.vcd.menu.VcdMenu;

final class VCDTextEditor extends TextEditor {

	VcdMenu vcdmenu;

	boolean mode = false;

	public VCDTextEditor(VcdMenu vcdin) {
		super();
		vcdmenu = vcdin;
	}

	public VCDTextEditor(VcdMenu vcdin, boolean modein) {
		super();
		vcdmenu = vcdin;
		mode = modein;
	}

	@Override
	protected void createActions() {
		super.createActions();
		IAction acprint = getAction(ITextEditorActionConstants.PRINT);
		IAction acfind = getAction(ITextEditorActionConstants.FIND);
		if (vcdmenu!=null)
		{
		vcdmenu.getFindAction().setActionText(acfind);
		vcdmenu.getPrinterAction().setActionText(acprint);
		setAction(ITextEditorActionConstants.PRINT, null);
		setAction(ITextEditorActionConstants.FIND, null);
		setAction(ITextEditorActionConstants.PRINT, vcdmenu.getPrinterAction());
		setAction(ITextEditorActionConstants.FIND, vcdmenu.getFindAction());
		}
	}

	public int updateFull(String s, boolean b) {
		if (getDocumentProvider() == null)
			return -1;
		IDocument id = getDocumentProvider().getDocument(getEditorInput());
		id.set(s);
		if (b)
			doSave(null);
		return 0;
	}

	@Override
	public boolean isEditable() {
		return mode;
	}

	@Override
	protected boolean isLineNumberRulerVisible() {	
		return true ;
	}
	
	protected void removemenu()
	{
		vcdmenu=null;		
	}

}