/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import fr.inria.aoste.timesquare.vcd.label.TickTimeLabelling;
import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.UpdateCommand;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

public class ScaleCommand implements ICommentCommand {

	double  _rulerBase=10.0; //
	String _unit="";
	private TickTimeLabelling _tickTimeLabelling=new TickTimeLabelling();
	
	public ScaleCommand() {
		super();	
	}
	
	
	
	public ScaleCommand(double rulerBase) {
		super();
		this._rulerBase = rulerBase;		
	}	

	public ScaleCommand(double rulerBase, String unit) {
		super();
		this._rulerBase = rulerBase;
		this._unit = unit;
		_tickTimeLabelling.setRate(rulerBase);
		_tickTimeLabelling.setUnit(unit);
	}

	@Override
	public void visit(IDeclarationVisitor visitor) {
		visitor.visitConstraintComment("scale "+_rulerBase +" "+_unit  );
	}

	@Override
	public int clear() {
		return 0;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public void setActive(boolean active) {}

	@Override
	public void setString(int n, String s) {		
		if (n==1){
			try{
				double d= Double.parseDouble(s);
				setBase(d);			
			}catch (Exception e) {}
		}
		if (n==2){
			setId(s);
		}
	}

	@Override
	public void setClockLink(String name) {}

	@Override
	public int update(String newcommentvalue, IComment updatevalue) {return 0;}

	@Override
	public Uposition getUp() {
		return null;
	}

	@Override
	public void setUp(Uposition up) {}

	@Override
	public String getType() {
		return "scale";
	}

	@Override
	public int validate(VCDDefinitions vcdDefinitions) {
		vcdDefinitions.setTimeLabelling(_tickTimeLabelling);
		return 0;
	}

	@Override
	public int setClockListener(UpdateCommand clklst) {
		return 0;
	}

	@Override
	public void validate() {}

	@Override
	public void newData(Object o) {}

	public double getBase() {
		return _rulerBase;
	}

	public void setBase(double base) {
		this._rulerBase = base;
		_tickTimeLabelling.setRate(base);
	}

	public String getId() {
		return _unit;
	}

	public void setId(String id) {
		this._unit = id;
		_tickTimeLabelling.setUnit(id);
	}

}
