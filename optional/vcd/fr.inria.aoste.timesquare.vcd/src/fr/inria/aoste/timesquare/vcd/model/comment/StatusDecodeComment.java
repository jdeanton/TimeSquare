/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import fr.inria.aoste.timesquare.vcd.model.Comment;

public class StatusDecodeComment implements IDecodeComment {

	public StatusDecodeComment() {
		super();
	}

	public StatusCommentCommand create(Comment comment) {
		if (comment.nbParts() < 3){
			return null;
		}
		return new StatusCommentCommand(comment.get(1), comment.get(2), comment.getTime()); 
	}
	
	public StatusCommentCommand createEmpty() {
		return new StatusCommentCommand();
	}

}
