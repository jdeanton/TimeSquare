/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

public enum Value {
	_0, _1, _x, _z;

	@Override
	public String toString() {
		return super.toString().substring(1);
	}

	public static Value fromChar(char c) {
		c = Character.toLowerCase(c);
		for (Value v : values()) {
			if (v.toString().charAt(0) == c)
				return v;
		}
		throw new IllegalArgumentException("Not a Value " + c);
	}

	public static Value fromString(String s) {
		return valueOf("_" + s.toLowerCase());
	}
}
