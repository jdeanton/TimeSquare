/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.preferences;

import java.util.ArrayList;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.vcd.VcdActivator;

final public class ColorSelection {

	private ArrayList<ColorData> _arrayColor = new ArrayList<ColorData>();
	private String _colorName;
	private IPreferenceStore _store = null;

	public ColorSelection(String colorName) {
		super();
		this._colorName = colorName;
		String top = "";
		if (colorName != null && colorName.length() != 0) {
			top = colorName + ".";
		}
		if (VcdActivator.getDefault() != null) {
			_store = VcdActivator.getDefault().getPreferenceStore();		
		}
		_arrayColor.add(0, new ColorData(top + "background", "Background", 0, 0, 0));
		_arrayColor.add(1, new ColorData(top + "text", "Text", 255, 255, 255));
		_arrayColor.add(2, new ColorData(top + "clock", "Clocks", 0, 255, 0));
		_arrayColor.add(3, new ColorData(top + "highImpedence", "High Impedance", 255, 212, 0));
		_arrayColor.add(4, new ColorData(top + "Timer", "Timer", 0, 255, 0));
		_arrayColor.add(5, new ColorData(top + "TimerLabel", "Timer Label", 0, 127, 255));
		_arrayColor.add(6, new ColorData(top + "timerZ", "Timer Inactif", 255, 100, 0));
		_arrayColor.add(7, new ColorData(top + "ghost", "Ghosts", 0, 127, 0));
		_arrayColor.add(8, new ColorData(top + "time", "Time Scale", 255, 0, 0));
		_arrayColor.add(9, new ColorData(top + "whiteArrow", "Precedence", 255, 255, 255));
		_arrayColor.add(10, new ColorData(top + "coincidence", "Coincidence", 255, 0, 0));
		_arrayColor.add(11, new ColorData(top + "weaklyPrecedenceCoincidence", "Weakly Coincidence", 255, 220, 220));
		_arrayColor.add(12, new ColorData(top + "tickMarker", "TickMarker", 255, 255, 0));

	}

	public String getColorName() {
		return _colorName;
	}

	public void init() {
		if (_store != null) {
			for (ColorData cd : _arrayColor) {
				_store.setDefault(cd.name, cd.getValue());
			}
			_store.setDefault(PreferenceConstants.P_namesize, 150);
		}
	}

	public void defaultColor() {
		if (_store != null) {
			for (ColorData cd : _arrayColor) {
				_store.setDefault(cd.name, cd.getValue());
			}
		}
	}

	public int getWidthName() {
		if (_store == null){
			return 120;
		}
		return _store.getInt(PreferenceConstants.P_namesize);
	}

	protected Color convertColor(int n) {
		if (n >= _arrayColor.size()){
			return new Color(null, 0, 0, 0);
			}
		return _arrayColor.get(n).getColor();
	}

	protected ArrayList<ColorData> getArrayColor() {
		return _arrayColor;
	}
	
	public class ColorData {
		
		String name;
		String title;
		int r, g, b;

		public ColorData(String name, String title, int r, int g, int b) {
			super();
			this.name = name;
			this.title = title;
			this.r = r;
			this.g = g;
			this.b = b;
		}

		public Color getColor() {
			if (_store != null){
				return new Color(null, PreferenceConverter.getColor(_store, name));
			}
			return new Color(null, r, g, b);
		}

		String getValue() {
			return r + "," + g + "," + b;
		}

	}
	
}
