/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.figure;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;

public interface ExtendFigure extends IFigure {
	public enum Level {
		z("z", 0), u("u", 1), x("x", 2), l1("l1", 3), l0("l0", 4), time("time",
				5), lnull("lnull", 6);
		int n;
		String s;
		private Level(String arg0, int arg1) {
			s = arg0;
			n = arg1;
		}
	}
	public void setLength(int length);
	public int incLength(int l);
	public int getLength();
	public Point getTr();
	public void setTr(Point tr);
	public boolean isGhost();
	public int mycompute();
	public void setoldValue(int n);
	public Level getLevel();
	public Level getPrecedeLevel();
	public Level getFuturLevel();
	public void setPrecedeLevel(Level l);
	public void setFuturLevel(Level l);
	
}