/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.dialogs;

import org.eclipse.core.runtime.Platform;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Panel;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.vcd.VcdActivator;

public class HelpDialog {
	private Shell _sShell = null; // @jve:decl-index=0:visual-constraint="0,0"
	private FigureCanvas _canvas = null;
	private Label _title = null;
	private Label _author = null;
	private Label _author2 = null;
	private Label _version = null;
	private Composite _composite = null;
	private Label _help = null;
	private Button _button1 = null;
	private boolean _resize = false;
	private Label _helpContents = null;
	private Label _explains = null;
	private Label _inria = null;
	private Label _date = null;
	private static int _size = 511;
	private Image _logo = null;
	private Button _close = null;
	private Button _helpbutton2 = null;

	/**
	 * This method initializes canvas
	 */
	private void createCanvas() {
		_canvas = new FigureCanvas(_sShell, SWT.NONE);
		_canvas.setBackground(ColorConstants.white);
		_logo = PluginHelpers.getImage(VcdActivator.PLUGIN_ID,
				"icons/logo.jpg");
		_logo.type = SWT.BITMAP;
		
		Panel p = new Panel() {
			@Override
			public void paint(Graphics graphics) {
				graphics.drawImage(_logo, new org.eclipse.draw2d.geometry.Point(
						0, 0));
			}
		};

		p.setPreferredSize(_logo.getBounds().width, _logo.getBounds().height);
		_canvas.setContents(p);
		_canvas.setBounds(new Rectangle(3, 31, 252, 153));
	}

	/**
	 * This method initializes composite
	 * 
	 */
	private void createComposite() {
		_composite = new Composite(_sShell, SWT.NONE);
		_composite.setBackground(ColorConstants.white);
		_composite.setLayout(null);
		_composite.setBounds(new Rectangle(0, 290, 253, 80));
		_help = new Label(_composite, SWT.NONE);
		_help.setBackground(ColorConstants.white);
		_help.setBounds(new Rectangle(6, 46, 130, 19));
		_help.setText("	        Help contents");
		_button1 = new Button(_composite, SWT.ARROW | SWT.RIGHT);
		_button1.setBounds(new Rectangle(161, 46, 26, 16));
		_helpbutton2 = new Button(_composite, SWT.ARROW | SWT.LEFT);
		_helpbutton2.setBounds(new Rectangle(161, 46, 26, 16));
		_helpbutton2.setVisible(false);
		_helpbutton2
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {

						_sShell.setSize(255, _sShell.getSize().y);
						_close.setBounds(new Rectangle(105, 370, 38, 23));
						_button1.setVisible(true);
						_helpbutton2.setVisible(false);
						_resize = false;
					}
				});
		
		_button1
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						if (!_resize) {
							_sShell.setSize(_size, _sShell.getSize().y);
							_close.setBounds(new Rectangle(230, 370, 38, 23));
							_button1.setVisible(false);
							_helpbutton2.setVisible(true);
							_resize = true;
						}
					}
				});
		
	}

	public void close() {
		_sShell.close();
	}

	public HelpDialog() {
		createSShell();
		_sShell.open();
	}

	/**
	 * This method initializes sShell
	 */
	private void createSShell() {
		_sShell = new Shell(SWT.NO_REDRAW_RESIZE | SWT.APPLICATION_MODAL
				| SWT.CENTER);
		_sShell.setBackground(ColorConstants.white);
		_sShell.setText("About vcd viewer editor");
		createCanvas();
		_sShell.setLayout(null);
		_sShell.setSize(new Point(256, 410));
		Rectangle r = _sShell.getDisplay().getClientArea();
		Point p = new Point((r.width - 256) / 2, (r.height - 410) / 2);
		_sShell.setLocation(p);
		_title = new Label(_sShell, SWT.NONE);
		_title.setBackground(ColorConstants.white);
		FontData myFontData = new FontData("VcdFont", 12, SWT.BOLD);
		Font myFont = new Font(_sShell.getDisplay(), myFontData);
		_title.setFont(myFont);
		_title.setText("         VCD VIEWER EDITOR");
		_title.setBounds(new Rectangle(1, 9, 251, 27));
		_inria = new Label(_sShell, SWT.NONE);
		_inria.setBackground(ColorConstants.white);
		_inria.setText("                            Kairos INRIA/I3S");
		_inria.setBounds(new Rectangle(0, 185, 252, 26));
		_author = new Label(_sShell, SWT.NONE);
		_author.setBackground(ColorConstants.white);
		_author.setText("                      Author:");
		_author.setBounds(new Rectangle(0, 215, 140, 46));

		_author2 = new Label(_sShell, SWT.NONE);
		_author2.setBackground(ColorConstants.white);
		_author2.setText("Anthony Gaudino\nBenoit Ferrero");
		_author2.setBounds(new Rectangle(140, 215, 100, 46));

		_date = new Label(_sShell, SWT.NONE);
		_date.setBackground(ColorConstants.white);
		_date.setText("                           Date: 30/10/2008");
		_date.setBounds(new Rectangle(0, 275, 252, 26));

		_version = new Label(_sShell, SWT.NONE);
		_version.setBackground(ColorConstants.white);
		_version.setText("                            Version: "
				+ Platform.getBundle("fr.inria.aoste.timesquare.vcd").getHeaders().get(
						"Bundle-Version"));
		_version.setBounds(new Rectangle(0, 303, 252, 26));
		createComposite();
		_helpContents = new Label(_sShell, SWT.NONE);
		_helpContents.setBackground(ColorConstants.white);
		_helpContents.setBounds(new Rectangle(316, 9, 291, 25));
		_helpContents.setFont(myFont);
		_helpContents.setText("  HELP CONTENTS");
		_explains = new Label(_sShell, SWT.NONE);
		_explains.setBackground(ColorConstants.white);
		_explains.setBounds(new Rectangle(269, 75, 303, 77));
		_explains
				.setText("Left click: Allow to show clocks constraints\n\n     Right click: Allow to show time bar\n\n     Crtl + Wheelbutton: Allow to zoom ");

		_close = new Button(_sShell, SWT.NONE);
		_close.setBounds(new Rectangle(100, 370, 38, 23));
		_close.setText("Close");
		_close
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {

						close();
					}
				});
	}

}
