/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

public interface ICommentCommand extends IDeclarationCommand {

	public boolean isActive();

	public void setActive(boolean active);
	
	public void setString(int n, String s);
	
	public void setClockLink(String name); // old setClock
	
	public int update( String newcommentvalue, IComment updatevalue);	

	public  Uposition getUp() ;

	public  void setUp(Uposition up) ;
	
	public String getType();	
	
	public int validate(VCDDefinitions vcddef);
	
	public int setClockListener(UpdateCommand clklst);

	public void validate();
	
	public void newData(Object  o);
}
