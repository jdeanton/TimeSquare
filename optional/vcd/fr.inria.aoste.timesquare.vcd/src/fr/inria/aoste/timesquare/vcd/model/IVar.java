/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;
import fr.inria.aoste.timesquare.vcd.view.VcdValueFactory;

public interface IVar {
	
	public String getName();

	public String getQualifiedName();

	public int getSize();

	public String getIdentiferCode();

	public VcdValueFactory getValueFactory();

	public void setValueFactory(VcdValueFactory v);

	public boolean isVisibleByDefault();

	public void setVisibleByDefault(boolean f);
	
	public int clear();
	
	public VarCommand getVarCommand();
	
	public Object getData(String s);
	
	public Object setData(String s, Object o);
	
	public String getAliasName();
}
