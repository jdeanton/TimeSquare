/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.listener;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.action.ActionSync;
import fr.inria.aoste.timesquare.vcd.dialogs.SelectClocksDialog;
import fr.inria.aoste.timesquare.vcd.menu.Mode;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.aoste.timesquare.vcd.view.EventPanel;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class MouseDraggedListener implements MouseListener, MouseMoveListener {

	

	// private ListConnections list;
	private IVcdDiagram _vcdDiagram;
	private IFigure _fig;
	private boolean _state = false;
	private int _selectedIndex = -1;
	private int _indexWave = -1;
	private int _startY = 0;
	private int _namesInitialPositionY = 0;

	public MouseDraggedListener(ListConnections list, IVcdDiagram vdt) {
		this._vcdDiagram = vdt;
	}

	public void mouseDoubleClick(MouseEvent e) {}

	public void mouseDown(MouseEvent e) {
		// offset of vertical scrollBar
		int dy = _vcdDiagram.getNames().getVerticalBar().getSelection();
		// select name clock
		_fig = _vcdDiagram.getNames().getContents().findFigureAt(e.x, e.y + dy);

		// if it's a left click
		if (e.button == 1) {
			if (_fig instanceof Label) {
				IFigure rect = _fig.getParent();
				_state = true;
				_selectedIndex = _vcdDiagram.getNames().getContents().getChildren()
						.indexOf(rect);
				_startY = e.y;
				_namesInitialPositionY = ((Label) _fig).getLocation().y;
			}
		}
		// if it's a right click
		else if (e.button == 3) {
			if (_fig instanceof Label) {
				// create contextMenu
				Menu contextMenu = new Menu(_vcdDiagram.getMyShell(), SWT.POP_UP);
				// add "Ghost" to contextMenu
				MenuItem ghostItem = new MenuItem(contextMenu, SWT.CASCADE);
				ghostItem.setText("Ghost");
				// add "Hide" to contextMenu
				MenuItem hideItem = new MenuItem(contextMenu, SWT.CASCADE);
				hideItem.setText("Hide Clock");
				// set "Ghost" like subMenu
				Menu subMenuGhost = new Menu(contextMenu);
				ghostItem.setMenu(subMenuGhost);
				// add "Show Ghost" to "Ghost"
				MenuItem showGhostItem = new MenuItem(subMenuGhost, SWT.CASCADE);
				showGhostItem.setText("Show Ghost");
				// add "Hide Ghost" to "Ghost"
				MenuItem hideGhostItem = new MenuItem(subMenuGhost, SWT.CASCADE);
				hideGhostItem.setText("Hide Ghost");
				// find and save IVar and index of the name
				IFigure rect = _fig.getParent();
				_selectedIndex = _vcdDiagram.getNames().getContents().getChildren()
						.indexOf(rect);
				IVar var = _vcdDiagram.getTraceCollector().getSelectedClocks().get(_selectedIndex);
				// add listener to "Hide"
				hideItem.addSelectionListener(new SelectedHide(var, _vcdDiagram));
				// if selected clock have ghost
				if (var.getValueFactory().haveGhostinclock() == true) {
					// "Ghost" is enable
					ghostItem.setEnabled(true);
					// select clock wave
					IFigure fig2 = _vcdDiagram.getCanvas().getContents().findFigureAt(
							e.x + 130, e.y + dy);
					_indexWave = _vcdDiagram.getCanvas().getContents().getChildren()
							.indexOf(fig2);
					if (_indexWave == -1) {
						_indexWave = _vcdDiagram.getCanvas().getContents().getChildren()
								.indexOf(fig2.getParent());
					}
					// add listener to "Show Ghost"
					showGhostItem.addSelectionListener(new SelectedShowGhost(
							_indexWave, _vcdDiagram));
					// add listener to "Hide Ghost"
					hideGhostItem.addSelectionListener(new SelectedHideGhost(
							_indexWave, _vcdDiagram));
				} else{
					// "Ghost" is not enable
					ghostItem.setEnabled(false);
				}
				// add contextMenu to the shell
				_vcdDiagram.getMyShell().setMenu(contextMenu);
				// set contextMenu is visible
				contextMenu.setVisible(true);
			}
		}
	}

	public void mouseMove(MouseEvent e) {
//		int dy = _vcdDiagram.getNames().getVerticalBar().getSelection();
		if (_state) {
			int distance = e.y - _startY;
			_fig.getParent().setLocation(
					new Point(_fig.getParent().getBounds().x,distance + _namesInitialPositionY));
		}
	}

	public void mouseUp(MouseEvent es) {
		try
		{
			if (es.button == 1) {
				if (_fig==null)
					return ;
				if (_selectedIndex==-1)
					return ;
				int newIndex = -1;
				int verticalmargin = 5;
				_state = false;
				newIndex = (_fig.getParent().getBounds().y - verticalmargin)
						/ (_fig.getParent().getBounds().height + verticalmargin);
				if (newIndex > _vcdDiagram.getNames().getContents().getChildren().size() - 1)
				newIndex = _vcdDiagram.getNames().getContents().getChildren().size() - 1;
				if (newIndex < 0)
					newIndex = 0;
				RectangleFigure rf1 = (RectangleFigure) _vcdDiagram.getNames().getContents().getChildren().get(_selectedIndex);
				IVar var = _vcdDiagram.getTraceCollector().getSelectedClocks().get(_selectedIndex);
				_vcdDiagram.getNames().getContents().remove(rf1);
				_vcdDiagram.getTraceCollector().getSelectedClocks().remove(var);
				_vcdDiagram.getNames().getContents().add(rf1, newIndex);
				_vcdDiagram.getTraceCollector().getSelectedClocks().add(newIndex, var);
				_vcdDiagram.getNames().getContents().getLayoutManager().layout(
						_vcdDiagram.getNames().getContents());

				alternateWaves(_selectedIndex, newIndex);

				_vcdDiagram.getNames().getContents().getLayoutManager().layout(
						_vcdDiagram.getNames().getContents());
				_fig = null;
				_selectedIndex = -1;
				if (_vcdDiagram.getVcdmenu().getSyncAction() != null){
					for (ActionSync menuitem : _vcdDiagram.getVcdmenu().getSyncAction()) {
						if (menuitem.isChecked()) {
							ConstraintCommentCommand cc = menuitem.getCc();
							Color color = menuitem.getColor(); 
							_vcdDiagram.getConstraintsFactory().redrawSyncInterval(cc,
									color);
						}
					}
				}
				_vcdDiagram.getCanvas().redraw();
			}
		}
		catch (Throwable e) {
			System.err.println(e);
		}
	}

	/**
	 * 
	 * @param selectedIndex
	 * @param newIndex
	 * @author mzc
	 */
	public void alternateWaves(int selectedIndex, int newIndex) {
		EventPanel p1 = (EventPanel) _vcdDiagram.getCanvas().getContents().getChildren().get(selectedIndex);
		_vcdDiagram.getCanvas().getContents().remove(p1);
		_vcdDiagram.getCanvas().getContents().add(p1, newIndex);
		_vcdDiagram.getCanvas().getContents().getLayoutManager().layout(
				_vcdDiagram.getCanvas().getContents());
		int n = _vcdDiagram.getNames().getContents().getChildren().size();
		for (int i = 0; i < n; i++) {
			EventPanel p2 = (EventPanel) _vcdDiagram.getCanvas().getContents().getChildren().get(i);
			p2.getEventline().setEventlineLocation(p2.getLocation());
			if(p2.isDurationModel()){
				for (Object o : p2.getChildren()) {
					IFigure f = (IFigure) o;
					if (f instanceof ExtendFigure) {
						Point pt=((ExtendFigure) f).getTr();
						pt.y=i * 40 + 25;
						((ExtendFigure) f).setTr(pt);
						((ExtendFigure) f).mycompute();
					}
				}
			}
		}

	}
	
	//Old version
	/*
	 public void alternateWaves(int selectedIndex, int newIndex) {
		Panel p1 = (Panel) _vcdDiagram.getCanvas().getContents().getChildren().get(selectedIndex);
		_vcdDiagram.getCanvas().getContents().remove(p1);
		_vcdDiagram.getCanvas().getContents().add(p1, newIndex);
		_vcdDiagram.getCanvas().getContents().getLayoutManager().layout(
				_vcdDiagram.getCanvas().getContents());
		int n = _vcdDiagram.getNames().getContents().getChildren().size();
		for (int i = 0; i < n; i++) {
			Panel p2 = (Panel) _vcdDiagram.getCanvas().getContents().getChildren().get(i);
			for (Object o : p2.getChildren()) {
				IFigure f = (IFigure) o;
				if (f instanceof ExtendFigure) {
					Point pt=((ExtendFigure) f).getTr();
					pt.y=i * 40 + 25;
					((ExtendFigure) f).setTr(pt);
					((ExtendFigure) f).mycompute();
				}
			}
		}
	}
	 */
	
	
	
	// listener of "Hide" in the contextMenu
		public static class SelectedHide implements SelectionListener {
			
			private IVar variable;
			private IVcdDiagram vdt;

			public SelectedHide(IVar var, IVcdDiagram vdt) {
				variable = var;
				this.vdt = vdt;
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			public void widgetSelected(SelectionEvent e) {
				// obtain hide clock and show clock
				ArrayList<IVar> hideClock = vdt.getTraceCollector().getAllnames();
				ArrayList<IVar> showClock = vdt.getTraceCollector().getSelectedClocks();
				hideClock.add(variable);
				showClock.remove(variable);
				// call apply on SelectClocksDialog
				SelectClocksDialog scd = new SelectClocksDialog(vdt.getMyShell(),
						hideClock, showClock, "", "", "");
				scd.apply(vdt);
			}
		}

		// listener of "Show Ghost" in the contextMenu
		public static class SelectedShowGhost implements SelectionListener {
			
			private int index;
			private IVcdDiagram vdt;

			public SelectedShowGhost(int index, IVcdDiagram vdt) {
				this.index = index;
				this.vdt = vdt;
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			public void widgetSelected(SelectionEvent e) {
				vdt.getConstraintsFactory().showGhost(index);
				if (isAllShow())
					vdt.getVcdmenu().setGhostSelected(Mode.show);
				else
					vdt.getVcdmenu().setGhostSelected(Mode.partial);
			}

			private boolean isAllShow() {
				for (IVar var : vdt.getTraceCollector().getSelectedClocks()) {
					if (var.getValueFactory().haveGhostinclock()) {
						if (!var.getValueFactory().getIsGhostVisible())
							return false;
					}
				}
				return true;
			}
		}

		// listener of "Hide Ghost" in the contextMenu
		public static class SelectedHideGhost implements SelectionListener {
			
			private int index;
			private IVcdDiagram vdt;

			public SelectedHideGhost(int index, IVcdDiagram vdt) {
				this.index = index;
				this.vdt = vdt;
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			public void widgetSelected(SelectionEvent e) {
				vdt.getConstraintsFactory().hideGhost(index);
				if (isAllHide())
					vdt.getVcdmenu().setGhostSelected(Mode.hide);
				else
					vdt.getVcdmenu().setGhostSelected(Mode.partial);
			}

			private boolean isAllHide() {
				for (IVar var : vdt.getTraceCollector().getSelectedClocks()) {
					if (var.getValueFactory().haveGhostinclock()) {
						if (var.getValueFactory().getIsGhostVisible())
							return false;
					}
				}
				return true;
			}
		}
	
}
