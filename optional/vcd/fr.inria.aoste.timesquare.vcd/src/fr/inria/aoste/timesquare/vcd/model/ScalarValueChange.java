/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

final public class ScalarValueChange extends ValueChange {
	
	private Value _value;

	public ScalarValueChange(char v, String ident) {
		this(Value.fromChar(v), ident);
	}

	public ScalarValueChange(String v, String ident) {
		this(Value.fromString(v), ident);
	}

	public ScalarValueChange(Value v, String ident) {
		super(ident);
		this._value = v;
	}

	@Override
	public Object getValue() {
		return _value;
	}

	@Override
	public String toString() {
		return _value + super.toString();
	}

	@Override
	public void setValue(Object value) {
		this._value = (Value) value;
	}
}
