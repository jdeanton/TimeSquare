/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.draw.print;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PrintOperation;
import org.eclipse.draw2d.PrinterGraphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import fr.inria.aoste.timesquare.vcd.draw.FigureCanvasBase;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class FigurePrinter extends PrintOperation {

	private FigureCanvasBase _figureCanvasBase = null;
	private VcdColorPreferences _mca = null;
	
	public FigurePrinter() {
		super();
	}

	public FigurePrinter(Printer p) {
		super(p);
	}

	public void setFigureCanvasBase(FigureCanvasBase fcbin) {
		this._figureCanvasBase = fcbin;
		_mca= _figureCanvasBase.getMca();
	}

	public int clear(){
		cleanup();
		_figureCanvasBase=null;
		_mca=null;
		return 0;
	}
	
	@Override
	protected void printPages() {
		PrinterGraphics graphics = getFreshPrinterGraphics();
		int size = _mca.getWidthName();
		int dy = _figureCanvasBase.getScaleCanvas().getContents().getSize().height;
		setupPrinterGraphicsFor(graphics, _figureCanvasBase.getClockCanvas().getContents(), size,	dy);
		graphics.pushState();
		getPrinter().startPage();

		/********************************************/
		Dimension d = new Dimension(_figureCanvasBase.getClockCanvas().getContents().getSize()).expand(size, dy);
		graphics.clipRect(new Rectangle(new Point(0, 0), d));
		printfigure(graphics, _figureCanvasBase.getNames().getContents(), 0, dy);
		printfigure(graphics, _figureCanvasBase.getClockCanvas().getContents(), size, 0); //size ,dy 		
		printfigure(graphics, _figureCanvasBase.getScaleCanvas().getContents(), 0, -dy); /* size , 0 */
		/********************************************/
		getPrinter().endPage();
		graphics.popState();
	}

	private int printfigure(PrinterGraphics g, IFigure figure, int x, int y) {
		g.translate(new Point(x, y));
		figure.paint(g);
		return 0;
	}

	protected void setupPrinterGraphicsFor(Graphics graphics, IFigure figure,int x, int y) {
		double dpiScale = (double) getPrinter().getDPI().x / Display.getCurrent().getDPI().x;
		Rectangle printRegion = getPrintRegion();
		Rectangle bounds = figure.getBounds();
		bounds.width += x;
		bounds.height += y;
		double xScale =  printRegion.width / (bounds.width *1.05 * dpiScale);
		double yScale =  printRegion.height / (bounds.height *1.05 *dpiScale);

		graphics.scale(Math.min(xScale, yScale) * dpiScale);
		graphics.setForegroundColor(_mca.colorBlack());
		graphics.setBackgroundColor(_mca.colorBlack());
		graphics.setFont(figure.getFont());
	}

	@Override
	public void run(String jobName) {
		if (MessageDialog.openQuestion(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell(), "Question...",
				"Really print " + getNumofPages() + " pages on "
						+ getPrinter().getPrinterData().name
						+ " ( Beta Fonction )" + "?"))
			super.run(jobName);
	}

	private int getNumofPages() {
		return 1;
	}

}
