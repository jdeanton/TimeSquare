/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.command;

import java.util.Date;

import fr.inria.aoste.timesquare.vcd.model.IDeclarationCommand;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

final public class DateCommand implements IDeclarationCommand {
	
	private String _date;

	public DateCommand(String date) {
		this._date = date;
	}

	public DateCommand() {
		this(new Date().toString());
	}

	public void visit(IDeclarationVisitor visitor) {
		visitor.visitDate(_date);
	}

	
	public int clear() {
		return 0;
	}
}
