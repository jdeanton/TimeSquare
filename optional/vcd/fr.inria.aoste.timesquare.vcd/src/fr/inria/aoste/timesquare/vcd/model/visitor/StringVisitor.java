/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.visitor;

import fr.inria.aoste.timesquare.vcd.model.Value;
import fr.inria.aoste.timesquare.vcd.model.ValueChange;
import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.ScopeType;
import fr.inria.aoste.timesquare.vcd.model.keyword.SimulationKeyword;
import fr.inria.aoste.timesquare.vcd.model.keyword.TimeUnit;
import fr.inria.aoste.timesquare.vcd.model.keyword.VarType;

/**
 * A visitor of a VCD model that can translate the VCD model into text following the 
 * .vcd file specification.
 * 
 * @author Benoit Ferrero
 *
 */
final public class StringVisitor implements IStringVisitor {
	
	private StringBuffer _strBuffer = new StringBuffer();

	@Override
	public String toString() {
		return _strBuffer.toString();
	}

	public StringVisitor() {
		super();
	}

	public IDeclarationVisitor getDeclarationVisitor() {
		return new MyDeclarationVisitor();
	}

	public IScopeVisitor getScopeVisitor() {
		return new MyScopeVisitor();
	}

	public ISimulationCollector getSimulationVisitor() {
		return new VCDTextFileSimulationVisitor();
	}
	
	class MyScopeVisitor implements IScopeVisitor {

		public void visitScope(ScopeType type, String name) {
			_strBuffer.append("$scope ").append(type).append(' ').append(name).append(" $end\n");
		}

		public void visitVar(VarCommand command, VarType type, String ident) {
			_strBuffer.append("$var ").append(type).append(' ').append(
					command.getSize()).append(' ').append(ident).append(' ')
					.append(command.getName()).append(" $end\n");
		}

		public void visitUpscope() {
			_strBuffer.append("$upscope $end\n");
		}
	}
	
	class MyDeclarationVisitor implements IDeclarationVisitor {

		public void visitDate(String date) {
			_strBuffer.append("$date \n").append(date).append("\n$end\n\n");
		}

		public void visitEndDefinitions() {
			_strBuffer.append("$enddefinitions $end\n\n");
		}

		public void visitVersion(String text, String task) {
			_strBuffer.append("$version \n").append(text).append(' ').append(task).append("\n$end\n\n");
		}

		public void visitConstraintComment(String comment) {
			if (comment == null){
				return;
			}
			_strBuffer.append("$comment ").append(comment);
			_strBuffer.append(" $end\n");
		}

		public void visitStatusComment(String func, String ident, int time) {
		}

		public void visitTimeScale(int number, TimeUnit unit) {
			_strBuffer.append("$timescale \n").append(number).append(' ').append(unit).append(" $end\n\n");
		}
	}
	
	private class VCDTextFileSimulationVisitor implements ISimulationCollector {

		public void visitTimeLine(int time) {
			_strBuffer.append('#').append(time).append('\n');
		}

		public void visitKeyword(SimulationKeyword keyword) {
			if (keyword != null) {
				_strBuffer.append('$').append(keyword).append('\n');
			}
		}

		public void visitValueChange(ValueChange value, int time) {
			if(value == null){
				return;
			}
			if(value.getValue() instanceof Value){
				if((Value)value.getValue() != Value._0){
					_strBuffer.append(value).append('\n');
				}
			}else{
				_strBuffer.append(value).append('\n');
			}	
		}

		public void visitEnd(SimulationKeyword keyword) {
			if (keyword != null){
				_strBuffer.append("$end\n");
			}
		}

		public void endSimulation() {}
		
	}
	

	
}
