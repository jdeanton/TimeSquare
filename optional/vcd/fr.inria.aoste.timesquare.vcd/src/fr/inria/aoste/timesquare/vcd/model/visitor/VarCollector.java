/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.visitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import fr.inria.aoste.timesquare.vcd.model.IDeclarationCommand;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.command.HierVar;
import fr.inria.aoste.timesquare.vcd.model.command.Var;
import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.ScopeType;
import fr.inria.aoste.timesquare.vcd.model.keyword.VarType;

public class VarCollector implements IScopeVisitor, Iterable<IVar> {
	
	private ArrayList<IVar> _vars = new ArrayList<IVar>();
	private LinkedList<String> _names = new LinkedList<String>();
	private String _parent = null;

	public VarCollector(ArrayList<IDeclarationCommand> list) {
		super();
	}

	public void visitScope(ScopeType type, String name) {
		if (_parent != null)
			_names.add(_parent);
		_parent = name;
	}

	public void visitUpscope() {
		if (_names.isEmpty())
			this._parent = null;
		else
			this._parent = _names.removeLast();
	}

	public void visitVar(VarCommand command, VarType type, String ref) {
		IVar var = new Var(command, _parent);
		if (!_names.isEmpty()) {
			ListIterator<String> it = _names.listIterator(_names.size() - 1);
			//We will never pass into this loop, because the vcd files which had been created with the vcd backend have only one scopeCommand.
			while (it.hasPrevious()){
				var.clear();
				var=null;
				var = new HierVar(var, it.previous());
			}
		}
		//If 'hide' is true, set the var invisible
		Object obj=var.getData("hide");
		if ((obj instanceof Boolean) && ((Boolean)obj==true)){
			var.setVisibleByDefault(false);
		}
		obj=var.getData("alias");
		if ((obj instanceof String) ){
			var.getVarCommand().setData("alias", obj);
		}
		_vars.add(var);
	}

	public Iterator<IVar> iterator() {
		return _vars.iterator();
	}

	@Override
	public String toString() {
		StringBuffer strBuffer = new StringBuffer();
		for (IVar var : this) {
			strBuffer.append(var.toString() + "\n");
		}
		return strBuffer.toString();
	}
}
