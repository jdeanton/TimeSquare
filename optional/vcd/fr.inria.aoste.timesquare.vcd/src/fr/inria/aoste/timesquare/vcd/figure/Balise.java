/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.figure;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;

public class Balise extends RectangleFigure {

	private IFigure _anchor = null;
	private Linkto _linkTo;
	
	public enum Linkto {
		tl("tl", 0), tr("tr", 1), bl("bl", 2), br("br", 3);
		String s;
		int n;

		private Linkto(String s, int n) {
			this.n = n;
			this.s = s;
		}

	}

	public IFigure getAnchor() {
		return _anchor;
	}

	public void setAnchor(IFigure anchor) {
		this._anchor = anchor;
	}


	public final Linkto getLinkTo() {
		return _linkTo;
	}

	public final void setLinkTo(Linkto linkTo) {
		this._linkTo = linkTo;
	}

	public int update() {
		IFigure anchor = getAnchor();
		if (_linkTo == null)
			return 0;
		switch (_linkTo) {
		case tl: {
			getBounds().x = anchor.getBounds().getTopLeft().x - 2;
			getBounds().y = anchor.getBounds().getTopLeft().y - 4;
		}
			break;
		case tr: {
			getBounds().x = anchor.getBounds().getTopRight().x + 2;
			getBounds().y = anchor.getBounds().getTopRight().y - 4;
		}
			break;
		case bl:

		{
			getBounds().x = anchor.getBounds().getBottomLeft().x - 2;
			getBounds().y = anchor.getBounds().getBottomLeft().y + 5;
		}
			break;
		case br:

		{
			getBounds().x = anchor.getBounds().getBottomRight().x + 2;
			getBounds().y = anchor.getBounds().getBottomRight().y + 5;
		}
			break;
		}
		return 0;
	}

}
