/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.UpdateCommand;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

public class AliasCommand implements ICommentCommand {

	String _s1="";
	String _s2="" ;
	Uposition _up;
	UpdateCommand _clklst;
	
	public boolean isActive() {	
		return true;
	}

	public AliasCommand() {
		super();
	}

	public AliasCommand(String s1, String s2) {
		super();
		this._s1=s1;
		this._s2=  s2.substring(1, s2.length()-1);
	
	}
	
	public void setActive(boolean active) {}

	
	public void visit(IDeclarationVisitor visitor) {
		visitor.visitConstraintComment("alias " + _s1+ " \""+_s2 +"\"");
	}

	
	public int clear() {
		return 0;
	}

	public void setString(int n, String s) {
		if (n==1){
			_s1=s;
		}
		if (n==2) {
			_s2=s.substring(1, s.length()-1); 
		}
		if (_s1!=null && _s2!=null){
			if (_clklst!=null){
				this._clklst.putDataonClock(_s1, "alias", _s2);
			}
		}
	}
	
	public void setClockLink(String name) {
		_s1=name;
		if (_s1!=null && _s2!=null){
			if (_clklst!=null){
				this._clklst.putDataonClock(_s1, "alias", _s2);
			}
		}
		
	}


	public int update( String newcommentvalue, IComment updatevalue) {
		return 0;
	}
	

	public final Uposition getUp() {
		return _up;
	}

	public final void setUp(Uposition up) {
		this._up = up;
	}
	
	public String getType() {
		return "alias";
	}

	
	public int validate(VCDDefinitions vcddef) {
		if (_clklst!=null){
			this._clklst.putDataonClock(_s1, "alias", _s2);
		}
		return 0;
	}

	
	public int setClockListener(UpdateCommand clklst) {
		this._clklst=clklst;
		this._clklst.putDataonClock(_s1, "alias", _s2);
		return 0;
	}

	
	public void validate() {
		if (_clklst!=null)
			_clklst.putDataonClock(_s1, "alias", _s2);
		
	}

	public void newData(Object o) {}

}
