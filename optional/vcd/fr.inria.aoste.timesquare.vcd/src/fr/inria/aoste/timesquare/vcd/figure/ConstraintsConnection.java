/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.figure;

import java.util.ArrayList;

import org.eclipse.draw2d.PolylineConnection;

import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
import fr.inria.aoste.timesquare.vcd.relation.extensionpoint.Instant;

public class ConstraintsConnection extends PolylineConnection {
	private IConstraintData _comment;
	private Boolean _global = null;
	private ArrayList<Instant> _instants = new ArrayList<Instant>();
	private Instant _instantCaller = null;
	private ArrayList<Instant> _instantsCaller = new ArrayList<Instant>();
	private int indice=0;
	
	public IConstraintData getComment() {
		return _comment;
	}

	public void setComment(IConstraintData comment) {
		this._comment = comment;
	}

	public Boolean isGlobal() {
		return _global;
	}

	public void setGlobal(boolean global) {
		this._global = Boolean.valueOf( global);
	}

	public int redraw() {
		return 0;
	}

	public void setInstants(ArrayList<Instant> instants) {
		this._instants = instants;
	}

	public ArrayList<Instant> getInstants() {
		return _instants;
	}

	@Override
	public int hashCode() {
		return 0;
	}

	@Override
	public boolean equals(Object o)
	{
		if (o instanceof ConstraintsConnection)
		{ 
			return equals((ConstraintsConnection) o );
		}
		return false;
	}
	
	public boolean equals(ConstraintsConnection obj) {
		int n=0;
		ConstraintsConnection cc =  obj;
		
		if (_instants.size() != cc.getInstants().size())
			return false;
		for (n=0; n<_instants.size(); n++){
			if (!_instants.get(n).equals(cc.getInstants().get(n))){
				return false;
			}
		}
		if ((!_instantsCaller.isEmpty()) && (!cc.getInstantsCaller().isEmpty())) {
			if (_instantsCaller.size() != cc.getInstantsCaller().size()){
				return false;
			}
			if (!_instantsCaller.containsAll(cc.getInstantsCaller())) {
				return false;
			}
		}

		return true;
	}

	public Instant getInstantCaller() {
		return _instantCaller;
	}

	public void setInstantCaller(Instant instantCaller) {
		this._instantCaller = instantCaller;
	}

	public ArrayList<Instant> getInstantsCaller() {
		return _instantsCaller;
	}
	
	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}
	
}
