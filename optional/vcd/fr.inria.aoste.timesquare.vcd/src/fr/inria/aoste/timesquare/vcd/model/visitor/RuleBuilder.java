/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.visitor;

import fr.inria.aoste.timesquare.vcd.model.ValueChange;
import fr.inria.aoste.timesquare.vcd.model.keyword.SimulationKeyword;
import fr.inria.aoste.timesquare.vcd.model.keyword.TimeUnit;

public class RuleBuilder implements IVcdVisitor, IDeclarationVisitor, ISimulationVisitor {

	int scale(int time) { // if dMin == 0 => invalid trace
		return time/dMin;
	}
	
	
	public void visitConstraintComment(String comment) {}

	
	public void visitDate(String date) {}

	
	public void visitEndDefinitions() {}

	
	public void visitStatusComment(String func, String ident, int time) {}

	private int timeScale = 1;
	private TimeUnit unit = TimeUnit.fs;
	
	public void visitTimeScale(int number, TimeUnit unit) {
		this.timeScale = number;
		this.unit = unit;
	}

	
	public void visitVersion(String text, String task) { }

	
	public void endSimulation() { 
		System.out.println(timeScale+":"+unit+" "+dMin);
	}

	
	public void visitEnd(SimulationKeyword keyword) {}

	
	public void visitKeyword(SimulationKeyword keyword) {}

	private int dMin = 0, previous = 0;
	
	public void visitTime(int time) {
		int d = time-previous;
		if (dMin==0) dMin = d;
		else if (d>0) dMin = Math.min(dMin, d);
		previous = time;
	}

	
	public void visitValueChange(ValueChange value) {}

	
	public IDeclarationVisitor getDeclarationVisitor() {
		return this;
	}

	
	public IScopeVisitor getScopeVisitor() {
		return null;
	}

	
	public ISimulationVisitor getSimulationVisitor() {
		return this;
	}

}
