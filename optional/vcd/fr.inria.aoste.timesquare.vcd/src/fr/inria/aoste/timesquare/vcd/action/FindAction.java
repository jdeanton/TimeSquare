/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.action;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Panel;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.dialogs.FindDialog;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class FindAction extends Action implements IActionTextVCD {

	private final IVcdDiagram ivcddiagram;
	VcdColorPreferences mca;
	IAction actionText = null;
	
	public FindAction(IVcdDiagram ivcd) {
		super();
		this.ivcddiagram = ivcd;
		mca= ivcd.getColorPreferences() ;
	}

	public IAction getActionText() {
		return actionText;
	}

	public void setActionText(IAction ac) {
		actionText = ac;
	}

	@Override
	public void run() {
		if (ivcddiagram.getActivePageID() == 0){
			find();
		}else if (actionText != null) {
			actionText.run();
		}
	}

	private void find() {
		FindDialog inputDialog = new FindDialog(Display.getCurrent()
				.getActiveShell(), "Find", "Clock name", "Instance", "",
				ivcddiagram);
		int reponse;
		reponse = inputDialog.open();
		if (reponse == Window.OK) {
			Description d = new Description(inputDialog.getClock(),null, inputDialog
					.getInstance(), -1);
			for (Object obj : ivcddiagram.getCanvas().getContents()
					.getChildren()) {
				Panel panel = null;
				if (obj instanceof Panel)
					panel = (Panel) obj;
				else
					continue;
				for (Object f : panel.getChildren()) {
					IFigure poly = (IFigure) f;
					Description descr = ivcddiagram.getVcdFactory()
							.getFigureForDescription(poly);
					if (descr == null) // || d == null)
						continue;
					if (d.equals(descr)) {
						int x;
						x = poly.getBounds().x;
						poly.setForegroundColor(mca.colorRedFind());
						ivcddiagram.getCanvas().scrollToX(
								x
										- (this.ivcddiagram.getCanvas()
												.getSize().x / 2));
						ivcddiagram.getScaleCanvas().scrollToX(
								x
										- (this.ivcddiagram.getScaleCanvas()
												.getSize().x / 2));
						ivcddiagram.getMarkerFactory().hideMarkerFireable();
						ivcddiagram.getListConnections().getListFind().add(poly);
						ivcddiagram.setFocus();
						return;
					}
				}
			}
			MessageBox messageBox = new MessageBox(Display.getCurrent()
					.getActiveShell(), SWT.ICON_ERROR | SWT.OK);
			messageBox.setText("Warning");
			messageBox.setMessage("Figure introuvable !");
			messageBox.open();
		}
		ivcddiagram.setFocus();
	}
}