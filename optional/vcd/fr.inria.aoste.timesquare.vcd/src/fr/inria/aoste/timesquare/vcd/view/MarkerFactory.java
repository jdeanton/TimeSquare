/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Polygon;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.comment.StatusCommentCommand;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class MarkerFactory {

	private Polyline _marker = null;
	private Polyline _marker2 = null;
	private Polygon _timepolygone = null;
	private Label _time = new Label();
	private IVcdDiagram _vdt;
	private VcdColorPreferences _mca;
	private int _oldfirable = -1;
	private int _oldmarker = -1;

	public MarkerFactory(ListConnections list, IVcdDiagram vdt) {
		super();
		this._vdt = vdt;
		_mca = vdt.getColorPreferences();
		vdt.getVcdFactory().setMarkerFactory(this);
	}

	public void hideMarkerFireable() {
		if (_oldfirable != -1){
			showFireable(_oldfirable);
		}
		if (_oldmarker != -1){
			showMarker(_oldmarker, 0);
		}
	}

	public void showFireable(int x) {
		int height = _vdt.getCanvas().getContents().getSize().height;
		for (Object obj : _vdt.getNames().getContents().getChildren()) {
			if (obj instanceof RectangleFigure) {
				RectangleFigure rf = (RectangleFigure) obj;
				Label l = (Label) rf.getChildren().get(0);
				l.setForegroundColor(_mca.colorWhiteText());
			}
		}
		if (x == _oldfirable) {
			_oldfirable = -1;
			return;
		}
		_oldfirable = x;
		IFigure oldfig = null;
		for (int y = 0; y < height; y++) {
			IFigure fig = _vdt.getCanvas().getContents().findFigureAt(x, y);
			if (oldfig != null) {
				if (oldfig.equals(fig)){
					continue;
				}
			}
			Description descr = _vdt.getVcdFactory().getFigureForDescription(fig);
			String clockName = "" ;
			if (descr != null) {
				for (StatusCommentCommand cc : _vdt.getVcdModel().getStatusCommentCommand()) {
					if (cc.getTime() == descr.getTime()
							- _vdt.getVcdModel().getPulse()) {
						for (IVar var : _vdt.getTraceCollector().getSelectedClocks()) {
							if (var.getIdentiferCode().equals(cc.getIdent())) {
								clockName = var.getName();
								break;
							}
						}
						for (Object obj : _vdt.getNames().getContents()
								.getChildren()) {
							if (obj instanceof RectangleFigure) {
								RectangleFigure rf = (RectangleFigure) obj;
								Label l = (Label) rf.getChildren().get(0);
								if (l.getText().equals(clockName))
									l.setForegroundColor(_mca
											.colorDarkGreenGhostFireable());
								else
									continue;
							}
						}
						clockName = "";
						continue;
					}
				}
				if (clockName.equals("")) {
					clockName = descr.getName();
					for (Object obj : _vdt.getNames().getContents()
							.getChildren()) {
						if (obj instanceof RectangleFigure) {
							RectangleFigure rf = (RectangleFigure) obj;
							Label l = (Label) rf.getChildren().get(0);
							if (l.getText().equals(clockName)){
								l.setForegroundColor(_mca.colorLightGreenFirable());
							}else{
								continue;
							}
						}
					}
				}
				oldfig = fig;
			}
		}
	}

	public void showMarker(int x, int xscroll) {
		int factor = 1;
		
		double zoom = _vdt.getMarkerZoom();
		
		double value = 1.0;
		
		value = ((x - 5) + xscroll) / zoom * factor;
		value = Math.round(value/10)*10;
		if (_vdt.getVcdModel().getTimeScaleCommand() != null) {
			factor = _vdt.getVcdModel().getTimeScaleCommand().getNumber();
		}
		if (_marker != null) {
			remove(_vdt.getVcdFactory().getBackPanel(), _marker);
			remove(_vdt.getVcdFactory().getScalePanel(), _marker2);
			remove(_vdt.getVcdFactory().getScalePanel(), _timepolygone);
			if ((int)value == _oldmarker) {
				_oldmarker = -1;
				_marker = null;
				_marker2 = null;
				_timepolygone = null;
				return;
			}
		}
		_oldmarker = (int)value;
		
		double exate_x = Math.round(value/10);
		exate_x = exate_x*10*zoom + 5 - xscroll;
		_time.setText(_vdt.getVcdModel().getTimeLabelling().getTimeMarker(value));
		_time.setForegroundColor(_mca.colorRedTime());
		_time.setOpaque(true);
		_time.setBounds(new Rectangle((int)exate_x, 0, 200, 20));
		_marker = VerticalLine(_mca.colorRedTimeXor(), new Point((int)exate_x+ xscroll, 0),new Point((int)exate_x+ xscroll, 1000));
		_marker.setOutlineXOR(true);
		_marker.setLineWidth(1);
		_timepolygone = new Polygon();
		_timepolygone.setOpaque(false);
		_timepolygone.setLayoutManager(new BorderLayout());
		_timepolygone.setForegroundColor(_mca.colorBlack());
		_timepolygone.setPoints(new PointList());
		_timepolygone.addPoint(new Point((int)exate_x + xscroll + 2, 30));
		_timepolygone.addPoint(new Point((int)exate_x + xscroll + 150, 30));
		_timepolygone.addPoint(new Point((int)exate_x + xscroll + 150, 50));
		_timepolygone.addPoint(new Point((int)exate_x + xscroll + 2, 50));
		_timepolygone.add(_time, BorderLayout.LEFT);
		_marker2 = VerticalLine(_mca.colorRedTime(), new Point((int)exate_x+ xscroll, 30),
				new Point((int)exate_x+ xscroll, 50));
		remove(_vdt.getScaleCanvas().getContents(), _time);
		_vdt.getCanvas().layout();
		_vdt.getVcdFactory().getBackPanel().add(_marker);
		_vdt.getVcdFactory().getScalePanel().add(_marker2);
		_vdt.getScaleCanvas().getContents().add(_timepolygone);
	}

	private void remove(IFigure content, IFigure remove) {
		if (content.getChildren().contains(remove)){
			content.remove(remove);
		}
	}
	
	private Polyline VerticalLine(Color color, Point p1, Point p2){
		Polyline pl = new Polyline();
		pl.addPoint(p1);
		pl.addPoint(p2);
		pl.setForegroundColor(color);
		return pl;
	}

}
