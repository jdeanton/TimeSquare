/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.listener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.MenuItem;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;

public class VcdIntervalButtonListener implements SelectionListener {
	private ListConnections _listConnections;
	private IVcdDiagram _vcdDiagram;
	private ConstraintCommentCommand _constraintCommentCommand;

	public VcdIntervalButtonListener(ListConnections list, IVcdDiagram vdt,
			ConstraintCommentCommand cc) {
		super();
		this._listConnections = list;
		this._vcdDiagram = vdt;
		this._constraintCommentCommand = cc;
	}

	public void widgetDefaultSelected(SelectionEvent e) {}

	public void widgetSelected(SelectionEvent e) {
		MenuItem menuitem = (MenuItem) e.getSource();
		if (menuitem.getSelection()) {
			_vcdDiagram.getConstraintsFactory().drawSyncInterval(_constraintCommentCommand,
					_listConnections.menuForColorGet(menuitem));
		}
		if (!menuitem.getSelection()) {
			for (IFigure f : _listConnections.getListInterval().get(_constraintCommentCommand)) {
				_vcdDiagram.getCanvas().getContents().remove(f);
			}
			_listConnections.getListInterval().get(_constraintCommentCommand).clear();
		}
	}
}
