/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.MenuItem;

import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;

public class ListConnections {
	
	private ArrayList<Description> _listDescriptions = new ArrayList<Description>();
	private ArrayList<ConstraintsConnection> _listConstraints = new ArrayList<ConstraintsConnection>();
	private HashMap<ConstraintCommentCommand, ArrayList<ConstraintsConnection>> _listInterval = new HashMap<ConstraintCommentCommand, ArrayList<ConstraintsConnection>>();
	private ArrayList<IFigure> _listFind = new ArrayList<IFigure>();
	private HashMap<ConstraintCommentCommand, ArrayList<IFigure>> _firstclock1 = new HashMap<ConstraintCommentCommand, ArrayList<IFigure>>();
	private HashMap<ConstraintCommentCommand, ArrayList<IFigure>> _firstclock2 = new HashMap<ConstraintCommentCommand, ArrayList<IFigure>>();
	private HashMap<ConstraintCommentCommand, ArrayList<IFigure>> _lastclock1 = new HashMap<ConstraintCommentCommand, ArrayList<IFigure>>();
	private HashMap<ConstraintCommentCommand, ArrayList<IFigure>> _lastclock2 = new HashMap<ConstraintCommentCommand, ArrayList<IFigure>>();
	private HashMap<MenuItem, ConstraintCommentCommand> _menuforcomment = new HashMap<MenuItem, ConstraintCommentCommand>();
	private HashMap<MenuItem, Color> _menuforcolor = new HashMap<MenuItem, Color>();
	private ArrayList<IFigure> _listcoincidence = new ArrayList<IFigure>();
	private ArrayList<IFigure> _listGhost = new ArrayList<IFigure>();
	private HashMap<Action, ConstraintCommentCommand> _actionforcomment = new HashMap<Action, ConstraintCommentCommand>();
	private HashMap<Action, Color> _actionforcolor = new HashMap<Action, Color>();

	public ListConnections(
			ArrayList<Description> listDesciptions,
			ArrayList<ConstraintsConnection> listSync,
			HashMap<ConstraintCommentCommand, ArrayList<ConstraintsConnection>> listInterval) {
		super();
		this._listDescriptions= listDesciptions;
		this._listConstraints = listSync;
		this._listInterval = listInterval;
	}

	public void clearAll() {
		_listDescriptions.clear();
		for (IFigure ifi: _listFind){
			ifi.erase();
		}
		_listFind.clear();
		for (IFigure ifi: _listConstraints){
			ifi.erase();
		}
		_listConstraints.clear();
		for (IFigure ifi: _listcoincidence){
			ifi.erase();
		}
		_listcoincidence.clear();
		for (ArrayList<IFigure> lifi: _firstclock1.values()){
			for (IFigure ifi: lifi){
				ifi.erase();
			}
			lifi.clear();
		}
		for (ArrayList<IFigure> lifi: _firstclock2.values()){
			for (IFigure ifi: lifi){
				ifi.erase();
			}
			lifi.clear();
		}
		for (ArrayList<IFigure> lifi: _lastclock1.values()){
			for (IFigure ifi: lifi){
				ifi.erase();
			}
			lifi.clear();
		}
		for (ArrayList<IFigure> lifi: _lastclock2.values()){
			for (IFigure ifi: lifi){
				ifi.erase();
			}
			lifi.clear();
		}
		_firstclock1.clear();
		_firstclock2.clear();
		_lastclock1.clear();
		_lastclock2.clear();
		
	}

	public void clear() {
		for (IFigure ifi:_listConstraints){
			ifi.erase();
		}
		_listConstraints.clear();
	}

	public void clearInterval(ConstraintCommentCommand cc) {
		if (_listInterval.get(cc) != null)
			_listInterval.get(cc).clear();
		if (_lastclock1.get(cc) != null)
			_lastclock1.get(cc).clear();
		if (_firstclock1.get(cc) != null)
			_firstclock1.get(cc).clear();
		if (_lastclock2.get(cc) != null)
			_lastclock2.get(cc).clear();
		if (_firstclock2.get(cc) != null)
			_firstclock2.get(cc).clear();
	}

	public Color actionColorGet(Action ac) {
		return _actionforcolor.get(ac);
	}

	public void actionColorPut(Action ac, Color c) {
		_actionforcolor.put(ac, c);
	}

	public ConstraintCommentCommand actionForCommentGet(Action ac) {
		return _actionforcomment.get(ac);
	}

	public void actionForCommentPut(Action ac, ConstraintCommentCommand cc) {
		_actionforcomment.put(ac, cc);
	}

	public ArrayList<IFigure> firstClock1Get(ConstraintCommentCommand cc) {
		return _firstclock1.get(cc);
	}

	public void firstClock1Put(ConstraintCommentCommand cc , ArrayList<IFigure> list) {
		_firstclock1.put(cc, list);
	}

	public ArrayList<IFigure> firstClock2Get(ConstraintCommentCommand cc) {
		return _firstclock2.get(cc);
	}

	public void firstClock2Put(ConstraintCommentCommand cc , ArrayList<IFigure> list) {
		_firstclock2.put(cc, list);
	}

	public ArrayList<IFigure> lastClock1Get(ConstraintCommentCommand cc) {
		return _lastclock1.get(cc);
	}

	public void lastClock1Put(ConstraintCommentCommand cc , ArrayList<IFigure> list) {
		_lastclock1.put(cc, list);
	}

	public ArrayList<IFigure> lastClock2Get(ConstraintCommentCommand cc) {
		return _lastclock2.get(cc);
	}

	public void lastClock2Put(ConstraintCommentCommand cc , ArrayList<IFigure> list) {
		_lastclock2.put(cc, list);
	}

	public ArrayList<IFigure> getListCoincidence() {
		return _listcoincidence;
	}

	public void listGhostAdd(IFigure e) {
		_listGhost.add(e);
	}

	public ArrayList<IFigure> getListFind() {
		return _listFind;
	}

	public void menuForColorPut(MenuItem mi, Color c) {
		_menuforcolor.put(mi, c);
	}

	public Color menuForColorGet(MenuItem mi) {
		return _menuforcolor.get(mi);
	}

	public HashMap<MenuItem, ConstraintCommentCommand> getMenuForComment() {
		return _menuforcomment;
	}

	public HashMap<ConstraintCommentCommand, ArrayList<ConstraintsConnection>> getListInterval() {
		return _listInterval;
	}

	public ArrayList<ConstraintsConnection> getListConstraints() {
		return _listConstraints;
	}

	public ArrayList<Description> getListDescription() {
		return _listDescriptions;
	}
}
