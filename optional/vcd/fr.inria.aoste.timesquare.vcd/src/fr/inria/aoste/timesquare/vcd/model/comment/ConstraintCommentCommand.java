/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.vcd.model.Function;
import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.UpdateCommand;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;
import fr.inria.aoste.timesquare.vcd.view.constraint.IConstraint;

final public class ConstraintCommentCommand implements ICommentCommand ,IConstraintData {

	Uposition _up;
	private String _clock;
	private Function _function;
	private ArrayList<String> _referenceClocks;
	private Moderator _moderator;
	public final Moderator getModerator() {
		return _moderator;
	}

	private IConstraint _iConstraint = null;
	private volatile String _comment;
	
	
	

	public ConstraintCommentCommand() {
		super();
	}

	public ConstraintCommentCommand(String comment, String clock,
			Function function, ArrayList<String> referenceClocks, Moderator moderator) {
		this._clock = clock;
		this._function = function;
		this._referenceClocks = referenceClocks;
		this._comment = comment;
		this._moderator = moderator;
	}

	protected final void setFunction(Function function) {
		this._function = function;
	}

	protected final void setReferenceClocks(ArrayList<String> referenceClocks) {
		this._referenceClocks = referenceClocks;
	}

	protected final void setModerator(Moderator moderator) {
		this._moderator = moderator;
	}

	protected final void setComment(String comment) {
		this._comment = comment;
	}

	public void visit(IDeclarationVisitor visitor) {
		visitor.visitConstraintComment("constraint " +_comment);
	}

	@Override
	public String toString() {
		return _comment;
	}

	public String getClock() {
		return _clock.trim();
	}

	public ArrayList<String> getReferenceClocks() {
		return _referenceClocks;
	}

	public Function getFunction() {
		return _function;
	}

	public final IConstraint getConstraints() {
		return _iConstraint;
	}

	public final void setIc(IConstraint ic) {
		this._iConstraint = ic;
	}

	public boolean isActive() {
		return true;
	}

	public void setActive(boolean active) {}

	
	public int clear() {
		if (_iConstraint!=null){
			if (_iConstraint.getList()!=null){
				_iConstraint.getList().clearAll();
			}
		}
		return 0;
	}
	
	public void setString(int n, String s) {}

	public void setClock(String name) {
		_clock=name;
	}
	
	public final Uposition getUp() {
		return _up;
	}

	public final void setUp(Uposition up) {
		this._up = up;
	}
	
	public int update( String newcommentvalue, IComment updatevalue) {		
		ConstraintDecodeComment.createConstraintCommentCommand(newcommentvalue, updatevalue, this);
		if (_comment ==null)
			_comment=newcommentvalue;
		return 0;
	}


	
	public String getType() {
		return "constraint";
	}

	
	public int validate(VCDDefinitions vcddef) {
		vcddef.addContraint(this);
		if (getConstraints() != null) {			
			if (vcddef.getFactory() != null){
				getConstraints().setVcdFactory(vcddef.getFactory());
			}
		}
		return 0;
	}

	
	public void setClockLink(String name) {
	}

	
	public int setClockListener(UpdateCommand clklst) {
		return 0;
	}

	
	public void validate() {}

	public boolean clockisConstraint(String name){
		return true;
	}

	public String getLabeling() {		
		return _comment;
	}

	public void newData(Object o) {}
	
}