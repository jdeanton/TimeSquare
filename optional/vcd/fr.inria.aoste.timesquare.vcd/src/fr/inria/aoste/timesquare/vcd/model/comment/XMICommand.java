/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.UpdateCommand;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

public class XMICommand implements ICommentCommand{
	
	String _ClockID;
	String _xmi;
	
	public XMICommand() {
		super();
	}

	public XMICommand(String idclock, String xmiin) {
		super();
		this._ClockID = idclock;
		this._xmi = cleanning(xmiin);
	}

	public String cleanning(String s){
		if (s==null){
			return null;
		}
		if (s.startsWith("\"")){
			s=s.substring(1);
		}
		if (s.endsWith("\"")){
			s=s.substring(0, s.length()-1);
		}
		return s;
	}
	
	public String getType() {		
		return "xmi";
	}
	
	Uposition _up;
	
	public Uposition getUp() {
		return _up;
	}
	
	public boolean isActive() {
		return true;
	}
	
	public void setActive(boolean active) {}

	public void setClockLink(String name) {
		_ClockID=name;
	}
	
	public void setString(int n, String s) {
		if (n==1){
			_ClockID =s;
		}
		if (n==2){
			_xmi= cleanning(s);
		}
		if (_ClockID!=null && _xmi!=null){
			if (clklst!=null){
				this.clklst.putDataonClock(_ClockID, "xmi", _xmi);
			}
		}
		
	}

	public void setUp(Uposition up) {
		this._up=up;
	}

	public int update(String newcommentvalue, IComment updatevalue) {
		return 0;
	}
	
	public int clear() {
		return 0;
	}


	
	public void visit(IDeclarationVisitor visitor) {
		visitor.visitConstraintComment("xmi  " + _ClockID + " \""+_xmi+"\"" ); //TODO 
	}

	public int validate(VCDDefinitions vcddef) {
		if (clklst!=null){
			clklst.putDataonClock(_ClockID, "xmi", _xmi);
		}
		return 0;
	}

	UpdateCommand clklst;
	
	public int setClockListener(UpdateCommand _clklst) {
		this.clklst=_clklst;
		this.clklst.putDataonClock(_ClockID, "xmi", _xmi);
		return 0;
	}

	
	public void validate() {
		if (clklst!=null)
		clklst.putDataonClock(_ClockID, "xmi", _xmi);
	}

	
	public void newData(Object o) {
		// TODO Auto-generated method stub
		
	}

	
}
