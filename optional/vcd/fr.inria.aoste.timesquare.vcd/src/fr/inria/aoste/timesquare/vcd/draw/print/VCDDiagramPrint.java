/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.draw.print;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.swt.widgets.ToolBar;

import fr.inria.aoste.timesquare.vcd.IVcd;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.draw.FigureCanvasBase;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.menu.Mode;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.ConstraintsFactory;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public class VCDDiagramPrint implements IVcd {

	private FigureCanvasBase _figureCanvasBase ;
	private VcdFactory _vcdFactory;
	private ListConnections _listConnections;
	private TraceCollector _traceCollector;
	private VCDDefinitions _vcdDef;
	private ConstraintsFactory _ConstraintsFactory;
	private VcdColorPreferences _mca;
	
	public VCDDiagramPrint(VCDDefinitions def,VcdColorPreferences mca, VcdFactory vcdFactory) {
		super();
		this._mca=mca;
		this._figureCanvasBase = new FigureCanvasBase (this, mca);		
		this._vcdFactory = vcdFactory;
		_listConnections = createList();
		_vcdDef=def;
		_ConstraintsFactory = new ConstraintsFactory(_listConnections, this);
	}
	
	public VcdColorPreferences getColorPreferences() {
		return _mca;
	}
	
	public ToolBar getToolbar() {
		return null;
	}

	public FigureCanvasBase getFcb() {
		return _figureCanvasBase;
	}

	public void setTraceCollector(TraceCollector tc) {
		this._traceCollector = tc;
	}

	public FigureCanvas getCanvas() {
		return _figureCanvasBase.getClockCanvas();
	}

	public ConstraintsFactory getConstraintsFactory() {
		return _ConstraintsFactory;
	}

	public VcdFactory getVcdFactory() {
		return _vcdFactory;
	}

	public ListConnections getListConnections() {
		return _listConnections;
	}
	
	public TraceCollector getTraceCollector() {
		return _traceCollector;
	}

	public double getTraceZoomValue() {
		return 1.0;
	}

	public VCDDefinitions getVcdModel() {
		return _vcdDef;
	}
	
	protected ListConnections createList() {
		return new ListConnections(
				new ArrayList<Description>(),
				new ArrayList<ConstraintsConnection>(),
				new HashMap<ConstraintCommentCommand, ArrayList<ConstraintsConnection>>());
	}

	public Mode isGhostMode() {
		return Mode.hide;
	}
	
	public int clear(){
		_figureCanvasBase.dispose();
		_listConnections.clear();
		_vcdFactory.clear();
		_vcdFactory.clear2();
		if (_traceCollector!=null)
			_traceCollector.clear();
		return 0;
	}

	@Override
	public boolean isCtrlKey() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setCtrlKey(boolean ctrlKey) {
		// TODO Auto-generated method stub
		
	}

}
