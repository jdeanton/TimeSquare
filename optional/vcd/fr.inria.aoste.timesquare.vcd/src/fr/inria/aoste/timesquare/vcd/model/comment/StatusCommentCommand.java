/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.UpdateCommand;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

final public class StatusCommentCommand implements ICommentCommand {
	
	private String _func;
	private String _ident;
	private int _time;

	public StatusCommentCommand() {
		super();
	}

	public StatusCommentCommand(String func, String ident, int time) {
		this._func = func;
		this._ident = ident;
		this._time = time;
	}

	public String getFunc() {
		return _func;
	}

	public String getIdent() {
		return _ident;
	}

	public int getTime() {
		return _time;
	}

	public void visit(IDeclarationVisitor visitor) {
		visitor.visitStatusComment(_func, _ident, _time);
	}

	@Override
	public String toString() {
		return _func + " " + _ident + " " + _time;
	}

	public boolean isActive() {
		return true;
	}

	public void setActive(boolean active) {}

	public int clear() {
		return 0;
	}
	
	public void setString(int n, String s) {}

	public void setClockLink(String name) {}

	Uposition up;

	public final Uposition getUp() {
		return up;
	}

	public final void setUp(Uposition up) {
		this.up = up;
	}
	
	public int update( String newcommentvalue, IComment updatevalue) {
		return 0;
	}
	
	public String getType() {
		return "status";
	}

	public int validate(VCDDefinitions vcddef) {
		return 0;
	}
	
	public int setClockListener(UpdateCommand clklst) {
		return 0;
	}

	public void validate() {}

	public void newData(Object o) {}

}
