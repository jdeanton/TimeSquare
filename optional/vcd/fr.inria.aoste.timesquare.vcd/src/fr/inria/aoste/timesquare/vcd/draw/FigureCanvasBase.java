/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.draw;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.ImageFigure;
import org.eclipse.draw2d.ScrollPaneSolver;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;

import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.vcd.IVcd;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.VcdActivator;
import fr.inria.aoste.timesquare.vcd.draw.print.FigurePrinter;
import fr.inria.aoste.timesquare.vcd.draw.print.VCDDiagramPrint;
import fr.inria.aoste.timesquare.vcd.listener.MouseClickListener;
import fr.inria.aoste.timesquare.vcd.listener.MouseDraggedListener;
import fr.inria.aoste.timesquare.vcd.listener.MouseMoveListenerVCD;
import fr.inria.aoste.timesquare.vcd.listener.MouseTactileDraggedListener;
import fr.inria.aoste.timesquare.vcd.listener.VcdKeyListener;
import fr.inria.aoste.timesquare.vcd.listener.WheelListener;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public final class FigureCanvasBase {

	/**
	 * _me means the VcdMultiPageEditor
	 */
	private IVcd _vcdMultiPageEditor = null;
	/**
	 * This FigureCanvas represents all the clocklines area
	 */
	private FigureCanvas _clockCanvas = null;
	private FigureCanvas _names = null;
	/**
	 * The scale for clock lines which located in the top of the window 
	 */
	private FigureCanvas _scale = null;

	private FigureCanvas _picture = null; 
	private ToolBar _toolbar = null; 
	private Composite _compositeParent = null; 
	/**
	 * The progressing picture located in the top left of the window
	 */
	private ImageFigure _progressRatePicture = null;
	private VcdColorPreferences _mca;
	private MouseMoveListener _mouseMoveListener;
	private KeyListener _keyListener;
	private MouseListener _mouseListener;
	private MouseDraggedListener _mouseDraggedListenerName;
	private WheelListener _wheelListener;
	private MouseTactileDraggedListener _MouseTactileDraggedListener;

	public FigureCanvasBase(IVcd me, VcdColorPreferences mca) {
		this._vcdMultiPageEditor = me;
		this._mca=mca;
	}

	public void makeVcd(Composite p, boolean simulation) {
		if (p == null){
			return;
		}
		if (p.isDisposed()){
			return;
		}
		init(p, simulation);
		positionpicture();
		positionscale();
		positioncanvas();
		positionnames();
		positiontoolbar();
	}

	private void init(Composite p, boolean simulation) {
		_compositeParent = new Composite(p, SWT.FILL | SWT.V_SCROLL | SWT.H_SCROLL);
		_clockCanvas = new FigureCanvas(_compositeParent, SWT.NONE);
		_names = new FigureCanvas(_compositeParent, SWT.NONE);
		_scale = new FigureCanvas(_compositeParent, SWT.NONE);
		_picture = new FigureCanvas(_compositeParent, SWT.NONE);
		_toolbar = new ToolBar(_compositeParent, SWT.NONE);
		_compositeParent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		_compositeParent.setBackground(_mca.colorMenuBackground());
		_compositeParent.setLayout(new FormLayout());
		_progressRatePicture = new ImageFigure(PluginHelpers.getImage(
				VcdActivator.PLUGIN_ID, "icons/sampleInira.gif"));
		_vcdMultiPageEditor.getVcdFactory().getPicture().add(_progressRatePicture);
		_progressRatePicture.setVisible(simulation);
	}

	private void positionpicture() {
		FormData pictureData = new FormData();
		pictureData.left = new FormAttachment(0, 0);
		pictureData.top = new FormAttachment(0, 0);
		pictureData.height = 50;
		pictureData.width = _mca.getWidthName();
		_picture.setLayoutData(pictureData);
		_picture.setBackground(_mca.colorBlack());
		_picture.setScrollBarVisibility(FigureCanvas.NEVER);
		_picture.setContents(_vcdMultiPageEditor.getVcdFactory().getPicture());
	}

	private void positionscale() {
		FormData scaleData = new FormData();
		scaleData.left = new FormAttachment(_picture);
		scaleData.right = new FormAttachment(100, 0);
		scaleData.top = new FormAttachment(0, 0);
		scaleData.height = 50;
		_scale.setLayoutData(scaleData);
		_scale.setBackground(_mca.colorBlack());
		_scale.setScrollBarVisibility(FigureCanvas.NEVER);
		_scale.setContents(_vcdMultiPageEditor.getVcdFactory().getScalePanel());
		_vcdMultiPageEditor.getVcdFactory().getScalePanel().setSize(-1, 35);
		_scale.setSize(-1, 50);
	}

	private void positioncanvas() {
		FormData canvasData = new FormData();
		canvasData.left = new FormAttachment(_names);
		canvasData.right = new FormAttachment(100, 0);
		canvasData.bottom = new FormAttachment(_toolbar);
		canvasData.top = new FormAttachment(_picture);
		_clockCanvas.setBackground(_mca.colorBlack());
		_clockCanvas.setScrollBarVisibility(FigureCanvas.NEVER);
		_clockCanvas.setLayoutData(canvasData);
		_clockCanvas.setContents(_vcdMultiPageEditor.getVcdFactory().getBackPanel());

	}


	private void positionnames() {
		FormData namesData = new FormData();
		namesData.left = new FormAttachment(0, 0);
		namesData.bottom = new FormAttachment(_toolbar);
		namesData.top = new FormAttachment(_picture);
		namesData.width = _mca.getWidthName();
		_names.setBackground(_mca.colorBlack());
		_names.setLayoutData(namesData);
		_names.setContents(_vcdMultiPageEditor.getVcdFactory().getNames());
		_names.setScrollBarVisibility(FigureCanvas.NEVER);
	}

	private void positiontoolbar() {
		FormData toolbarData = new FormData();
		toolbarData.left = new FormAttachment(0, 0);
		toolbarData.right = new FormAttachment(100, 0);
		toolbarData.bottom = new FormAttachment(100, 0);
		toolbarData.height = 25;
		_toolbar.setLayoutData(toolbarData);
	}

	public void layoutall() {
		_clockCanvas.layout();
		_names.layout();
		_scale.layout();
		_picture.layout();
		_toolbar.layout();
		_compositeParent.layout();
	}

	public FigureCanvas getClockCanvas() {
		return _clockCanvas;
	}

	public FigureCanvas getNames() {
		return _names;
	}

	public FigureCanvas getScaleCanvas() {
		return _scale;
	}

	public FigureCanvas getPicture() {
		return _picture;
	}

	public ToolBar getToolbar() {
		return _toolbar;
	}

	public Composite getComposite() {
		return _compositeParent;
	}

	public void setSimulationProgress(int etape) {
		if (_progressRatePicture == null){
			return;
		}
		String s = VcdActivator.PLUGIN_ID;
		switch (etape) {
		case 0:
			_progressRatePicture.setImage(PluginHelpers.getImage(s,
					"icons/simulation.png"));
			break;
		case 1:
			_progressRatePicture.setImage(PluginHelpers.getImage(s,
					"icons/simulation2.png"));
			break;
		case 2:
			_progressRatePicture.setImage(PluginHelpers.getImage(s,
					"icons/simulation3.png"));
			break;
		case 3:
			_progressRatePicture.setImage(PluginHelpers.getImage(s,
					"icons/simulation4.png"));
			break;
		case 4:
			_progressRatePicture.setImage(PluginHelpers.getImage(s,
					"icons/simulation5.png"));
			break;
		case 5:
			_progressRatePicture.setImage(PluginHelpers.getImage(s,
					"icons/simulation6.png"));
			break;
		case 6:
			_progressRatePicture.setImage(PluginHelpers.getImage(s,
					"icons/simulation7.png"));
			break;
		default :
		}
	}

	public void scrollUpdate() {	
		ScrollBar sbrscr = getClockCanvas().getHorizontalBar();
		ScrollBar sbrdst = getComposite().getHorizontalBar();
		sbrdst.setIncrement(sbrscr.getIncrement());
		sbrdst.setMaximum(sbrscr.getMaximum());
		sbrdst.setMinimum(sbrscr.getMinimum());
		sbrdst.setSelection(sbrscr.getSelection());
		sbrdst.setPageIncrement(sbrscr.getPageIncrement());
		sbrdst.setThumb(sbrscr.getThumb());

		sbrscr = getClockCanvas().getVerticalBar();
		sbrdst = getComposite().getVerticalBar();
		sbrdst.setIncrement(sbrscr.getIncrement());
		sbrdst.setMaximum(sbrscr.getMaximum());
		sbrdst.setMinimum(sbrscr.getMinimum());
		sbrdst.setSelection(sbrscr.getSelection());
		sbrdst.setPageIncrement(sbrscr.getPageIncrement());
		sbrdst.setThumb(sbrscr.getThumb());
		ScrollPaneSolver.Result result;
		FigureCanvas fc = getClockCanvas();
		result = ScrollPaneSolver.solve(new Rectangle(fc.getBounds())
				.setLocation(0, 0), fc.getViewport(), FigureCanvas.AUTOMATIC,
				FigureCanvas.AUTOMATIC, 0, 0);

		if (getComposite().getHorizontalBar().getVisible() != result.showH)
			getComposite().getHorizontalBar().setVisible(result.showH);
		if (getComposite().getVerticalBar().getVisible() != result.showV)
			getComposite().getVerticalBar().setVisible(result.showV);
	}


	public void scroll2End() {
		ScrollBar scbCC = getClockCanvas().getHorizontalBar();
		ScrollBar scbSC = getScaleCanvas().getHorizontalBar();
		scbCC.setSelection(scbCC.getMaximum());
		scbSC.setSelection(scbSC.getMaximum());
		_clockCanvas.scrollToX(scbCC.getMaximum());
		_scale.scrollToX(scbSC.getMaximum());
	}


	public int update2(int n1, int n2, boolean b) { //n1::last size,  n2::current size,  b::false--continue, true--end simulation 
		if (_vcdMultiPageEditor == null){
			return 0;
		}
		if (_vcdMultiPageEditor.getCanvas() == null){
			return 0;
		}
		if (_vcdMultiPageEditor.getCanvas().isDisposed()){
			return 0;
		}
		scrollUpdate();
		scroll2End();
		//		_vcdMultiPageEditor.getTraceCollector().setZoom(_vcdMultiPageEditor.getTraceZoomValue());
		_vcdMultiPageEditor.getVcdModel().visit(_vcdMultiPageEditor.getTraceCollector(), n1, n2, b); //this command will only execute for once
		_clockCanvas.setContents(_vcdMultiPageEditor.getVcdFactory().getBackPanel());

		this.getScaleCanvas().layout();
		_vcdMultiPageEditor.getCanvas().layout();
		return 0;
	}

	public int printer(String name) {
		PrintDialog printDialog = new PrintDialog(new Shell(), SWT.NONE);
		printDialog.setText("Print "+ name);
		PrinterData printerData = printDialog.open();
		if (!(printerData == null)) {
			Printer printer = new Printer(printerData);
			VcdColorPreferences mca= VcdColorPreferences.createColor();
			mca.setMode(false);
			VcdFactory vcdFactory = new VcdFactory(mca);
			VCDDefinitions 	vcdDefinitions=_vcdMultiPageEditor.getVcdModel().cloneWithNewFactory(vcdFactory);
			TraceCollector traceCollector= new TraceCollector(_vcdMultiPageEditor.getTraceCollector().getSelectedClocks() , vcdFactory, true);
			vcdDefinitions.visit(traceCollector);
			VCDDiagramPrint vcdDiagramPrint= new VCDDiagramPrint( vcdDefinitions ,mca, vcdFactory);
			vcdDiagramPrint.setTraceCollector(traceCollector);
			FigureCanvasBase figureCanvasBase = vcdDiagramPrint.getFcb();
			figureCanvasBase.makeVcd(new Composite(new Shell(), SWT.PRINT_SCREEN), false);
			figureCanvasBase.getNames().setContents(vcdFactory.getNames());
			figureCanvasBase.getClockCanvas().setContents(vcdFactory.getBackPanel());

			figureCanvasBase.getNames().getParent().layout();
			figureCanvasBase.getClockCanvas().getContents().validate();
			figureCanvasBase.getNames().getContents().validate();
			figureCanvasBase.getClockCanvas().getParent().layout();
			figureCanvasBase.scrollUpdate();			
			FigurePrinter figurePrinter=new FigurePrinter(printer);			
			figurePrinter.setFigureCanvasBase(figureCanvasBase);
			figurePrinter.run(name);
			vcdDiagramPrint.clear();
			traceCollector.clear();
			figurePrinter.clear();
		}
		return 0;
	}

	public int createListener(ListConnections list, IVcdDiagram vdt){
		getClockCanvas().addMouseMoveListener(_mouseMoveListener =new MouseMoveListenerVCD(vdt));
		getClockCanvas().addMouseListener(_mouseListener=new MouseClickListener(list, vdt));
		_mouseDraggedListenerName = new MouseDraggedListener(list, vdt);
		getNames().addMouseListener(_mouseDraggedListenerName);
		getNames().addMouseMoveListener(_mouseDraggedListenerName);
		_MouseTactileDraggedListener = new MouseTactileDraggedListener(vdt);
		getClockCanvas().addMouseListener(_MouseTactileDraggedListener);
		getClockCanvas().addMouseMoveListener(_MouseTactileDraggedListener);
		getClockCanvas().addMouseWheelListener(_wheelListener=new WheelListener(vdt));
		getClockCanvas().addKeyListener(_keyListener=new VcdKeyListener(vdt));		
		addScrollListener();
		return 0;
	}

	public int dispose(){
		if (!_clockCanvas.isDisposed()){
			for (Control c:  _clockCanvas.getChildren()){
				c.dispose();
			}
			if (_mouseMoveListener!=null){
				_clockCanvas.removeMouseMoveListener(_mouseMoveListener);
			}
			if (_keyListener!=null){
				_clockCanvas.removeKeyListener(_keyListener);
			}
			if (_mouseListener!=null){
				_clockCanvas.removeMouseListener(_mouseListener);
			}
			if (_wheelListener!=null){
				_clockCanvas.removeMouseWheelListener(_wheelListener);
			}
			if (_MouseTactileDraggedListener != null){
				_clockCanvas.removeMouseListener(_MouseTactileDraggedListener);
				_clockCanvas.removeMouseMoveListener(_MouseTactileDraggedListener);
			}
			_clockCanvas.dispose();
		}
		if (!_names.isDisposed()){
			for (Control c:  _names.getChildren()){
				c.dispose();
			}
			if( _mouseDraggedListenerName!=null){
				_names.removeMouseListener(_mouseDraggedListenerName);
				_names.removeMouseMoveListener(_mouseDraggedListenerName);
			}
			_names.dispose();
		}
		if (!_scale.isDisposed()){
			for (Control c:  _scale.getChildren()){
				c.dispose();
			}
		}
		if (!_picture.isDisposed()){
			for (Control c:  _picture.getChildren()){
				c.dispose();
			}
		}
		if (!_toolbar.isDisposed()){
			for (Control c:  _toolbar.getChildren()){
				c.dispose();
			}
		}
		_scale.dispose();
		_picture.dispose();
		_toolbar.dispose(); 
		_compositeParent.dispose();
		_mca=null;
		_vcdMultiPageEditor=null;
		return 0;
	}

	public VcdColorPreferences getMca() {
		return _mca;
	}

	public int addScrollListener() {

		getClockCanvas().getVerticalBar().addSelectionListener(
				new CanvasVerticalBarListener());
		/** Mouse Scrool name -> canvas */
		getNames().getVerticalBar().addSelectionListener(
				new NamesVerticalBarListener());
		getComposite().getVerticalBar().addSelectionListener(
				new CompositesVerticalBarListener());



		//		getClockCanvas().getHorizontalBar().addSelectionListener(
		//				new CanvasHorizontalBarListener());

		getComposite().addListener(SWT.Resize, new ResizeListener());

		getClockCanvas().addListener(SWT.Resize, new ResizeListener());

		getScaleCanvas().addListener(SWT.Resize, new ResizeListener());


		getComposite().getHorizontalBar().addSelectionListener(
				new CompositeHorizontalBarListener());

		return 0;
	}

	private final class CompositeHorizontalBarListener implements
	SelectionListener {
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}
		public void widgetSelected(SelectionEvent e) {
			int x = getComposite().getHorizontalBar()
					.getSelection();
			getClockCanvas().scrollToX(x);
			getScaleCanvas().scrollToX(x);
			scrollUpdate();
		}
	}

	private final class CompositesVerticalBarListener implements
	SelectionListener{

		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}

		public void widgetSelected(SelectionEvent e) {
			if(_vcdMultiPageEditor.isCtrlKey()){
				if((FigureCanvas)e.getSource() == _clockCanvas){
					int y = _names.getVerticalBar().getSelection();
					getClockCanvas().scrollToY(y);
					getComposite().getVerticalBar().setSelection(y);
				}
				if((FigureCanvas)e.getSource() == _names){
					int y = _clockCanvas.getVerticalBar().getSelection();
					getNames().scrollToY(y);
					getComposite().getVerticalBar().setSelection(y);
				}
				return;
			}
			int y = getComposite().getVerticalBar().getSelection();
			getClockCanvas().scrollToY(y);
			getNames().scrollToY(y);
			scrollUpdate();
		}

	}

	//	private final class CanvasHorizontalBarListener implements
	//		SelectionListener {
	//		public void widgetDefaultSelected(SelectionEvent e) {
	//			widgetSelected(e);
	//		}
	//		public void widgetSelected(SelectionEvent e) {
	//			
	//			getScaleCanvas().scrollToX(
	//					getClockCanvas().getHorizontalBar().getSelection());
	//			scrollUpdate();
	//		}
	//	}

	private final class CanvasVerticalBarListener implements SelectionListener{
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}

		public void widgetSelected(SelectionEvent e) {
			if(_vcdMultiPageEditor.isCtrlKey()){
				int y = getNames().getVerticalBar().getSelection();
				getClockCanvas().scrollToY(y);
				return;
			}
			int y = getClockCanvas().getVerticalBar().getSelection();
			getNames().scrollToY(y);
		}
	}

	private final class NamesVerticalBarListener implements SelectionListener{
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}

		public void widgetSelected(SelectionEvent e) {
			int y = getNames().getVerticalBar().getSelection();
			getClockCanvas().scrollToY(y);
		}
	}

	public final class ResizeListener implements Listener {
		public void handleEvent(Event event) {
			scrollUpdate();
		}
	}

}