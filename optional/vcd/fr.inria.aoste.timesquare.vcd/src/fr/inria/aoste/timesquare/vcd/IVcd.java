/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.swt.widgets.ToolBar;

import fr.inria.aoste.timesquare.vcd.menu.Mode;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.ConstraintsFactory;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public interface IVcd {
	
	public abstract VcdColorPreferences getColorPreferences();
	public abstract FigureCanvas getCanvas();
	public abstract ToolBar getToolbar();
	public abstract ConstraintsFactory getConstraintsFactory();
	public abstract VcdFactory getVcdFactory();
	public abstract TraceCollector getTraceCollector();
	public abstract double getTraceZoomValue();
	public abstract VCDDefinitions getVcdModel();
	public abstract ListConnections getListConnections();
	public abstract Mode isGhostMode();
	public abstract boolean isCtrlKey();
	public abstract void setCtrlKey(boolean ctrlKey);
	
}
