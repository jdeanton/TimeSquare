/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.dialogs;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.utils.ui.idialog.BasedDialog;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.menu.Mode;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;

//DialogBox open by the action of "Partial..."
public class PartialDialog extends BasedDialog<IVar> {
	private IVcdDiagram ivcd;

	public PartialDialog(Shell parentShell, String possibleText,
			String selectedText, IVcdDiagram ivcd, String title) {
		super(parentShell, possibleText, selectedText, title);
		this.ivcd = ivcd;
		setUpdown(false);
		TraceCollector tc = ivcd.getTraceCollector();
		labelProvider = new MyLabelProvider();
		// for each hide clock
		for (IVar var : tc.getAllnames()) {
			if (var.getValueFactory() != null) {
				if (var.getValueFactory().haveGhostinclock()) {
					if (var.getValueFactory().getIsGhostVisible() == true) {
						elselect.addElement(var);
					} else {
						elsource.addElement(var);
					}
				}
			}
		}
		// for each show clock
		for (IVar var : tc.getSelectedClocks()) {
			if (var.getValueFactory().haveGhostinclock()) {
				if (var.getValueFactory().getIsGhostVisible() == true) {
					elselect.addElement(var);
				} else {
					elsource.addElement(var);
				}
			}
		}
	}

	protected static class MyLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		@Override
		public Image getImage(Object element) {
			return null;
		}

		@Override
		public String getText(Object o) {
			if ((o instanceof IVar))
				return ((IVar) o).getAliasName();
			// TODO nbr ghost
			return o.toString();
		}

		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		public String getColumnText(Object element, int columnIndex) {
			if (columnIndex == 0) {
				return getText(element);
			} else if (columnIndex == 1) {
				return "toto";
			}
			return "not the right column index";
		}
	}

	// for apply the change
	public int apply() {
		// if all clock are show
		if (elsource.isEmpty()) {
			ivcd.setGhostMode(Mode.show);
			return 0;
		}
		// if all clock are hide
		if (elselect.isEmpty()) {
			ivcd.setGhostMode(Mode.hide);
			return 1;
		}
		// Default usage
		for (IVar var : elselect.getElements()) {
			var.getValueFactory().setGhostVisible(true);
		}
		for (IVar var : elsource.getElements()) {
			var.getValueFactory().setGhostVisible(false);
		}
		ivcd.getVcdmenu().setGhostSelected(Mode.partial);
		return 2;
	}
}