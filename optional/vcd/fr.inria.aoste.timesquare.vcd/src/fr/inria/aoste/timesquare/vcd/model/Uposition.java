/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.model;

public class Uposition {

	public enum Enumblocktype {

		SimulationTime(0), DumpAll(1), DumpVars(2), DumpOn(3), DumpOff(4), Date(
				5), Scope(6), TimeScale(7), UpScope(8), Var(9), Version(10), EndDef(
				11);
		private Enumblocktype(int n) {
			this.n = n;
		}
		String s;
		int n;
		
	}

	int _value;
	Enumblocktype _block;
	boolean _theEnd = false;

	public boolean isTheEnd() {
		return _theEnd;
	}

	public void setTheEnd(boolean theEnd) {
		this._theEnd = theEnd;
	}

	public Uposition(int value, Enumblocktype block) {
		super();
		this._value = value;
		this._block = block;
	}

	public int getValue() {
		return _value;
	}

	public Enumblocktype getBlock() {
		return _block;
	}

	public void setValue(int value) {
		this._value = value;
	}

	public void setBlock_Value(Enumblocktype block, int value) {
		this._block = block;
		this._value = value;
	}

	@Override
	public String toString() {
		if (_theEnd)
			return "< After : " + _block + " >";
		return "<" + _block + " " + _value + ">";
	}

}
