/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.figure;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Polygon;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public class DrawTimer extends Polygon implements ExtendFigure {

	private String labelvalue = "Active";
	private int length;
	protected int oldvalue = 0;
	private Point tr = new Point(0, 0);
	private VcdFactory _vcdFactory;
	private int x;
	private int y;
	Label label = null;
	private Level precedeLevel;
	private Level futurLevel;
	
	public DrawTimer(VcdFactory vcd, int x, int y, int length, String label) {
		super();
		this._vcdFactory = vcd;
		this.x = x;
		this.y = y;
		this.length = length;
		labelvalue = label;
		CountFigure.inc();
	}

	public int incLength(int l) {
		length += l;
		return length;
	}
	public String getLabelvalue() {
		return labelvalue;
	}

	public int getLength() {
		return length;
	}

	public Point getTr() {
		return tr;
	}

	public boolean isGhost() {
		return false;
	}

	public int mycompute() {		
		double zoom = 1.0;
		int x2 = x + length;
		int mx = x;
		int mx2 = x2;
		if (_vcdFactory.getVcdZoom() != null) {
			mx = _vcdFactory.getVcdZoom().computeposition(x);
			mx2 = _vcdFactory.getVcdZoom().computeposition(x2);
		}
		setPoints(new PointList());
		setLayoutManager(new BorderLayout());
		setOpaque(false);
		setForegroundColor(_vcdFactory.getMca().colorTimer());
		addPoint(new Point(mx + 2, y + _vcdFactory.getHeight()).translate(tr));
		addPoint(new Point(mx, y + _vcdFactory.getHalfHeight()).translate(tr));
		addPoint(new Point(mx + 2, y).translate(tr));
		addPoint(new Point(mx2 - 2, y).translate(tr));
		addPoint(new Point(mx2, y + _vcdFactory.getHalfHeight()).translate(tr));
		addPoint(new Point(mx2 - 2, y + _vcdFactory.getHeight()).translate(tr));
		if (label == null) {
			label = new Label("Active");
		}
		add(label, BorderLayout.CENTER);
		label.setForegroundColor(_vcdFactory.getMca().colorTimerText());
		label.setBounds(new Rectangle(new Point(mx, y).translate(tr),new Dimension((int) (length * zoom), _vcdFactory.getHeight())));
		return 0;
	}

	public void setLabelvalue(String labelvalue) {
		this.labelvalue = labelvalue;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void setoldValue(int n) {
		oldvalue = n;
	}

	public void setTr(Point tr) {
//		tr.x = tr.x - 40;
		this.tr = tr;
	}

	public Level getLevel() {
		return Level.time;
	}

	public final Level getPrecedeLevel() {
		return precedeLevel;
	}

	public final void setPrecedeLevel(Level precedeLevel) {
		this.precedeLevel = precedeLevel;
	}

	public final Level getFuturLevel() {
		return futurLevel;
	}

	public final void setFuturLevel(Level futurLevel) {
		this.futurLevel = futurLevel;
	}

	@Override
	protected void finalize() throws Throwable {
		CountFigure.dec();
		super.finalize();
	}

}
