/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.preferences;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;


final public class VcdColorPreferences {
	static private VcdColorPreferences DEFAULT;

	private ColorSelection selection = null;

	private ColorSelection selectionprint = null;

	protected ColorSelection getSelectionprint() {
		return selectionprint;
	}

	protected ColorSelection getSelection() {
		return selection;
	}

	private static class ListColor {

		private ColorSelection sel;

		public ListColor(ColorSelection selc) {
			super();
			sel = selc;
			if (sel != null)
				setcolor();
		}

		private	Color colorWhiteRule;
		private	Color colorWhiteText;
		private	Color colorRedTime;
		private	Color colorBack;
		private	Color colorGhostClock;
		private	Color colorYellowZ;
		private	Color colorTimer;
		private	Color colorTimertext;
		private	Color colorWhiteArrow;
		private	Color colorClock;
		private	Color colorRedCoincidence;
		private	Color colorweaklyPrecedes;
		private	Color colorTimerZ;
		private	Color colorpacket ; 
		private	int width;

		public int setcolor() {
			colorBack = sel.convertColor(0);
			colorWhiteRule = sel.convertColor(1);
			colorWhiteText = sel.convertColor(1);
			colorClock = sel.convertColor(2);
			colorYellowZ = sel.convertColor(3);
			colorTimer = sel.convertColor(4);
			colorTimertext = sel.convertColor(5);
			colorTimerZ= sel.convertColor(6);
			colorGhostClock = sel.convertColor(7);
			colorRedTime = sel.convertColor(8);
			colorWhiteArrow = sel.convertColor(9);
			colorRedCoincidence = sel.convertColor(10);
			colorweaklyPrecedes= sel.convertColor(11);
			colorpacket= sel.convertColor(12);
			width = sel.getWidthName();
			return 0;
		}

	}

	public int updatecolor() {
		return ls.setcolor();
	}

	private VcdColorPreferences create() {

		return new VcdColorPreferences(selection, selectionprint);

	}

	synchronized public static VcdColorPreferences createColor() {
		return getDefault().create();

	}

	synchronized protected static VcdColorPreferences getDefault() {
		if (DEFAULT == null) {
			DEFAULT = new VcdColorPreferences();
		}
		return DEFAULT;
	}

	private ListColor ls1;
	private ListColor ls2;
	private ListColor ls;
	boolean mode = true;

	public boolean isMode() {
		return mode;
	}

	public void setMode(boolean mode) {
		if (mode)
			ls = ls1;
		else
			ls = ls2;
		this.mode = mode;
	}

	private VcdColorPreferences(ColorSelection col, ColorSelection colprint) {
		selection = col;
		selectionprint = colprint;
		ls1 = new ListColor(selection);
		ls2 = new ListColor(selectionprint);
		setMode(true);
	}

	private VcdColorPreferences() {
		selection = new ColorSelection("vcd");
		ls1 = new ListColor(selection);
		selectionprint = new ColorSelection("print");
		ls2 = new ListColor(selectionprint);
		getSelection().init();
		getSelectionprint().init();
		setMode(true);
	}

	public Color colorMenuBackground() {
		return ColorConstants.menuBackground;
	}

	public Color colorBlack() {
		return ls.colorBack;
	}

	public Color colorTextToolTip() {
		return ColorConstants.white;
	}

	public Color colorYellowTooltip() {
		return ColorConstants.yellow;
	}

	public Color colorWhiteArrow() {
		return ls.colorWhiteArrow;
	}

	public Color colorWhiteRule() {
		return ls.colorWhiteRule;
	}

	public Color colorWhiteText() {
		return ls.colorWhiteText;
	}

	public Color colorRedRule() {
		return ColorConstants.red;
	}

	public Color colorRedFind() {
		return ColorConstants.red;
	}

	public Color colorRedTime() {
		return ls.colorRedTime;
	}

	public Color colorRedTimeXor() {
		RGB rbg = ls.colorRedTime.getRGB();
		RGB rbg2 = ls.colorBack.getRGB();
		return new Color(null, rbg.red ^ rbg2.red, rbg.green ^ rbg2.green,	rbg.blue ^ rbg2.blue);
	}

	public Color colorWeakyCoincidence() {
		return ls.colorweaklyPrecedes ;
	}
	
	public Color colorRedCoincidence() {
		return ls.colorRedCoincidence;
	}

	public Color colorGhostClock() {
		return ls.colorGhostClock;
	}

	public Color colorClock() {
		return ls.colorClock; 
	}

	public Color colorTimer() {
		return ls.colorTimer;
	}

	public Color colorTimerText() {
		return ls.colorTimertext;
	}

	public Color colorLightGrayClockNameContour() {
		return ColorConstants.lightGray;
	}

	public Color colorLightGraySync() {
		return ColorConstants.lightGray;
	}

	public Color colorLightBlueSync() {
		return ColorConstants.lightBlue;
	}

	public Color colorRedSync() {
		return ColorConstants.red;
	}

	public Color colorYellowZ() {
		return ls.colorYellowZ;
	}
	public Color colorTimerZ() {
		return ls.colorTimerZ;
	}
	
	public Color colorTickMarker() {
		return ls.colorpacket;
	}

	public Color colorBlueSustain() {
		return ls.colorpacket;
	}

	public Color colorDarkGreenGhostFireable() {
		return ColorConstants.darkGreen;
	}

	public Color colorLightGreenFirable() {
		return ColorConstants.lightGreen;
	}

	public int getWidthName() {
		return ls.width;
	}

	@Override
	protected void finalize() throws Throwable {
		System.out.println("Delete MYcolorAPI");
		ErrorConsole.printOKln("Delete MYcolorAPI");
		super.finalize();

	}
}
