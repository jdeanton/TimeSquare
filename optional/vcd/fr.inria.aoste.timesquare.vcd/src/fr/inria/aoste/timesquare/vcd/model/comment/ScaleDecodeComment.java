/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import fr.inria.aoste.timesquare.vcd.model.Comment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;

public class ScaleDecodeComment implements IDecodeComment {

	public ScaleDecodeComment() {}

	@Override
	public ICommentCommand create(Comment comment) {
		
		if (comment.nbParts() <=1)			
			return new ScaleCommand();
		Double d=10.0d;
		try{
			d= Double.parseDouble(comment.get(1));
		}
		catch (Exception e) {
			return new ScaleCommand(1,comment.get(1));
		}
		if (comment.nbParts() <3){
			return new ScaleCommand(d);
		}
		return new ScaleCommand(d, comment.get(2));
	}

	@Override
	public ICommentCommand createEmpty() {
		return new ScaleCommand();
	}

}
