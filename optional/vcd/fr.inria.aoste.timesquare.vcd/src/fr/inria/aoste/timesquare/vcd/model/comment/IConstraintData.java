/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.vcd.view.constraint.IConstraint;

public interface IConstraintData {

	public ArrayList<String> getReferenceClocks() ;

	public String getClock();
	
	public Moderator getModerator();
	
	public String getLabeling();
	
	public  IConstraint getConstraints() ;
	
}
