/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.vcd.model.command.ScopeCommand;
import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;

public class ClockListener implements UpdateCommand {

	private ArrayList<ICommentCommand> _listCommentCommand =new ArrayList<ICommentCommand>();
	private ArrayList<ScopeCommand> _listScopeCommand =new ArrayList<ScopeCommand>();
	
	public void add(ScopeCommand sc){
		if ( sc==null){
			return ;
		}
		if (_listScopeCommand.contains(sc)){
			return ;
		}
		_listScopeCommand.add(sc);
		for (ICommentCommand icc : _listCommentCommand){
			for(IScopeCommand child : sc.getChilds()){
				if (child instanceof VarCommand){
					((VarCommand) child).newCommentCommand(icc);
				}
			}
			sc.listenerAddingCommand(icc);
		}
	}
	
	public void add(ICommentCommand icc){
		if ( icc==null){
			return ;
		}
		if (_listCommentCommand.contains(icc)){
			return ;
		}
		icc.setClockListener(this);
		_listCommentCommand.add(icc);
		for( ScopeCommand sc:_listScopeCommand){
			sc.listenerAddingCommand(icc);
		}
	}

	public void newvar(IScopeCommand child) {
		for (ICommentCommand icc : _listCommentCommand){
			if (child instanceof VarCommand ){
				((VarCommand) child).newCommentCommand(icc);
			}
		}
	}
	
	public Object getDataonClock(String idclock, String key) {
		
		for( ScopeCommand sc:_listScopeCommand){
			for(IScopeCommand child :sc.getChilds()){
				if (child instanceof VarCommand ){
					try{		
						if (((VarCommand) child).getName().compareTo(idclock)==0){
							return child.getData(key);
						}
						if (((VarCommand) child).getIdentifierCode().compareTo(idclock)==0){
							return child.getData(key);
						}	
					}
					catch (Throwable e) {}
				}
			}
		}
		return null;
	}

	
	public Object putDataonClock(String idclock, String key, Object value) {
	
		for( ScopeCommand sc:_listScopeCommand){
			for(IScopeCommand child :sc.getChilds()){
				try{						
						if (((VarCommand) child).getName().compareTo(idclock)==0){
							return child.setData(key,value);
						}
						if (((VarCommand) child).getIdentifierCode().compareTo(idclock)==0){
							return child.setData(key,value);
						}
					}catch (Throwable e) {}
			}
		}
		return null;
	}
	
	public Object getDataonTickClock(String idclock,int tick,  String key) {
		for( ScopeCommand sc:_listScopeCommand){
			for(IScopeCommand child :sc.getChilds()){
				if (child instanceof VarCommand ){
					try{	
						if (((VarCommand) child).getName().compareTo(idclock)==0){
							return ((VarCommand) child).getTickData(tick,key);
						}
						if (((VarCommand) child).getIdentifierCode().compareTo(idclock)==0){
							return ((VarCommand) child).getTickData(tick,key);
						}
					
					}
					catch (Throwable e) {}
				}
			}
		}
		return null;
	}

	
	public Object putDataonTickClock(String idclock,int tick,String key, Object value) {
	
		for( ScopeCommand sc:_listScopeCommand){
			for(IScopeCommand child :sc.getChilds()){
				try{	
						if (((VarCommand) child).getName().compareTo(idclock)==0){
							return ((VarCommand) child).setTickData(tick,key,value);
						}
						if (((VarCommand) child).getIdentifierCode().compareTo(idclock)==0){
							return ((VarCommand) child).setTickData(tick,key,value);
						}
						
						}catch (Throwable e) {}
			}
		}
		return null;
	}
	
}
