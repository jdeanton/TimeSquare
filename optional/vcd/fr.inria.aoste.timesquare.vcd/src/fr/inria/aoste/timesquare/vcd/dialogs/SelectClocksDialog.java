/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.dialogs;

import java.util.ArrayList;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.utils.ui.idialog.BasedDialog;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;

public class SelectClocksDialog extends BasedDialog<IVar> {

	protected boolean f = true;

	public SelectClocksDialog(Shell parentShell, ArrayList<IVar> a,
			ArrayList<IVar> b, String st1, String st2, String title) {
		super(parentShell, st1, st2, title);
		labelProvider = new MyLabelProvider();

		if (a.size() == 0) {
			f = false;
		}
		for (IVar var : b) {
			elselect.addElement(var);
		}

		for (IVar var : a) {
			elsource.addElement(var);
		}
	}

	//

	/*
	 * @Override protected boolean isSelectableElement(String text) { // iterate
	 * through all possibilities and return true if text corresponds
	 * Iterator<IVar> it = possibleElementList.getElements().iterator(); while
	 * (it.hasNext()) { IVar element = (IVar) it.next(); if
	 * (text.equalsIgnoreCase(element.getName()) ||
	 * text.equalsIgnoreCase(element.getQualifiedName())) { return true; } }
	 * return false; }
	 */

	protected static class MyLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		@Override
		public Image getImage(Object element) {
			return null;
		}

		@Override
		public String getText(Object o) {
			if ((o instanceof IVar)) {
				return ((IVar) o).getAliasName();
			}

			/*
			 * if (o.toString().equals("clk")) return null;
			 */
			return o.toString();
		}

		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		public String getColumnText(Object element, int columnIndex) {

			if (columnIndex == 0) {
				return getText(element);
			} else if (columnIndex == 1) {
				return "toto";
			}
			return "not the right column index";
		}
	}

	public ArrayList<IVar> getSrc() {
		return elsource.getElements();
	}

	public ArrayList<IVar> added() {
		ArrayList<IVar> l = getSelectedElements();
		ArrayList<IVar> la = new ArrayList<IVar>();
		for (IVar o : l)
			la.add(o);
		return la;
	}

	public int apply(IVcdDiagram ivcd) {
		ivcd.getCanvas().getContents().getChildren().clear();
		ivcd.getNames().getContents().getChildren().clear();
		ListConnections list = ivcd.getListConnections();
		list.clear();
		ivcd.getTraceCollector().setAllClocks(getSrc());
		ivcd.getVcdFactory().clear2();
		ivcd.getTraceCollector().constructClock(ivcd.getVcdFactory(), added(),
				ivcd.getTraceZoomValue());
		ivcd.getVcdFactory().getNameforfigures().clear();
		ivcd.getTraceCollector().setZoom(ivcd.getTraceZoomValue());
		ivcd.getVcdModel().visit(ivcd.getTraceCollector());
		ivcd.getNames().setContents(ivcd.getVcdFactory().getNames());
		ivcd.getCanvas().setContents(ivcd.getVcdFactory().getBackPanel());
//		ivcd.getNames().getParent().layout();
//		ivcd.getCanvas().getContents().validate();
//		ivcd.getNames().getContents().validate();
//		ivcd.getCanvas().getParent().layout();
		for (MenuItem menuitem : list.getMenuForComment().keySet()) {
			if (menuitem.getSelection()) {
				ConstraintCommentCommand cc = list.getMenuForComment().get(
						menuitem);
				Color color = list.menuForColorGet(menuitem);
				ivcd.getConstraintsFactory().drawSyncInterval(cc, color);
			}
		}

		int y = ivcd.getVcdFactory().getHeigth();
		int x = ivcd.getVcdFactory().getWindowsBoundsLength();
		ivcd.getCanvas().getContents().setPreferredSize(new Dimension(x, y));
		ivcd.getNames().getContents().setPreferredSize(new Dimension(120, y));
		ivcd.getCanvas().redraw();
		ivcd.getNames().redraw();
		ivcd.getfcb().scrollUpdate();
		ivcd.getVcdzoom().applyScrollZoom();
		if (ivcd.isGhostMode()!=null)
			if (ivcd.isGhostMode().isHide())
				ivcd.getConstraintsFactory().hideAllGhost();
		return 0;
	}

}