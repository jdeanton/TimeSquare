/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.listener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;

public class VcdKeyListener implements KeyListener {
	private IVcdDiagram _vcdDiagram;

	public VcdKeyListener(IVcdDiagram vcdDiagram) {
		this._vcdDiagram = vcdDiagram;
	}

	public void keyPressed(KeyEvent e) {

		if (e.keyCode == SWT.CONTROL)
			_vcdDiagram.setCtrlKey(true);
	}

	public void keyReleased(KeyEvent e) {

		if (e.keyCode == SWT.CONTROL)
			_vcdDiagram.setCtrlKey(false);
	}

}
