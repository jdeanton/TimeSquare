/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;

public class ConstraintSync extends AbsConstraint implements IConstraint {

	public ConstraintSync() {
		super();
		testn = 1;
	}

	public int draw(Draw1 currentfig, String currentclock) {
		String clock0 = cc.getClock();
		String clock1 = cc.getReferenceClocks().get(0);
		int v1 = Integer.valueOf(cc.getReferenceClocks().get(1));
		int v2 = Integer.valueOf(cc.getReferenceClocks().get(2));
		String clockdest = (clock0.equals(currentclock)) ? clock1 : clock0;
		int occurencesclock1;
		int occurencesclock2;
		if (clock0.equals(currentclock)) {
			occurencesclock1 = v1;
			occurencesclock2 = v2;
		} else {
			occurencesclock1 = v2;
			occurencesclock2 = v1;
		}
		ArrayList<IFigure> firstclock1 = new ArrayList<IFigure>();
		ArrayList<IFigure> firstclock2 = new ArrayList<IFigure>();
		ArrayList<IFigure> lastclock1 = new ArrayList<IFigure>();
		ArrayList<IFigure> lastclock2 = new ArrayList<IFigure>();
		for (int i = 0; i < vcdFactory.getNameforfigures().get(currentclock)
				.size(); i++) {
			if ((i + 1) % occurencesclock1 == 0) {
				lastclock1.add(vcdFactory.getNameforfigures().get(currentclock)
						.get(i));
				firstclock1.add(vcdFactory.getNameforfigures()
						.get(currentclock).get(i - (occurencesclock1 - 1)));
			}
		}
		for (int i = 0; i < vcdFactory.getNameforfigures().get(clockdest)
				.size(); i++) {
			if ((i + 1) % occurencesclock2 == 0) {
				lastclock2.add(vcdFactory.getNameforfigures().get(clockdest)
						.get(i));
				firstclock2.add(vcdFactory.getNameforfigures().get(clockdest)
						.get(i - (occurencesclock2 - 1)));
			}
		}
		if (lastclock1.contains(currentfig)) {
			int period = lastclock1.indexOf(currentfig) + 1;
			PolylineConnection dest;
			if (period < firstclock2.size()) {
				dest = (PolylineConnection) firstclock2.get(period); // TODO
																		// sync1
				ConstraintsConnection poly = icc.constructDashConnection(
						mca.colorWhiteArrow(), currentfig,(Draw1) dest, false);
				poly.setComment(cc);
				poly.setGlobal(isGlobal);
				icc.addToList(list.getListConstraints(), poly);
				return 0;
			}
		}
		if (firstclock1.contains(currentfig)) {
			int period = firstclock1.indexOf(currentfig) - 1;
			if (period == -1)
				return 0;
			if (period < lastclock2.size()) {
				PolylineConnection dest = (PolylineConnection) lastclock2
						.get(period); // TODO sync2
				ConstraintsConnection poly = icc.constructDashConnection(
						mca.colorWhiteArrow(), (Draw1)dest, currentfig, false);
				poly.setComment(cc);
				poly.setGlobal(isGlobal);
				icc.addToList(list.getListConstraints(), poly);
				return 0;
			}
		}
		return 0;
	}

	public int drawTableItemConstraints() {
		try {
			isGlobal = true;
			String nameClock = cc.getClock();
			if (haveAllClockVisible()) {
				for (IFigure fig : vcdFactory.getNameforfigures()
						.get(nameClock)) {
					draw((Draw1)fig, nameClock);
				}
			} else
				return -1;
			IVcdDiagram vdt = vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints()) {
				Dimension dim = vdt.getCanvas().getContents()
						.getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width,
						bounds.height, true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible = true;
			isGlobal = false;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		} catch (Throwable t) {
			ErrorConsole.printError(t);
			isGlobal = false;
			return -1;
		}
	}

	public void drawSyncInterval(Color color) {}

	@Override
	public int drawOneTick(int ticknum, String clockId) {
		// TODO Auto-generated method stub
		return 0;
	}
}
