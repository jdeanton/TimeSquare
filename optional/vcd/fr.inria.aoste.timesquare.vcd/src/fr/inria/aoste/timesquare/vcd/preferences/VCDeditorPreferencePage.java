/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.preferences;



import java.util.ArrayList;

import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import fr.inria.aoste.timesquare.vcd.VcdActivator;
import fr.inria.aoste.timesquare.vcd.preferences.ColorSelection.ColorData;
import fr.inria.aoste.timesquare.vcd.view.figure.CountFigure;

/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog. By subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the
 * preference store that belongs to the main plug-in class. That way,
 * preferences can be accessed directly via the preference store.
 */

public class VCDeditorPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	public static final class RestoreColor implements SelectionListener {
		public void widgetDefaultSelected(SelectionEvent e) {
			sel.defaultColor();
		}
		ColorSelection sel;
		ArrayList<FieldEditor> listfd;
		public RestoreColor(ColorSelection in,ArrayList<FieldEditor> list) {
			super();
			sel=in;
			listfd=list;
		}

		public void widgetSelected(SelectionEvent e) {
			sel.defaultColor();
			for(FieldEditor fe:listfd)
				fe.loadDefault();
		}
	}

	public VCDeditorPreferencePage() {
		super(FLAT);
		setPreferenceStore(VcdActivator.getDefault().getPreferenceStore());
		setDescription("VCD editor : ");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */

	@Override
	protected Control createContents(Composite parent) {
		Composite c = new Composite(parent, SWT.NONE);
		c.setLayout(new GridLayout(1, false));
		c.setFont(parent.getFont());
		GridData gd = new GridData(GridData.FILL);
		gd.horizontalSpan = 1;
		c.setLayoutData(gd);
		// Composite c=getFieldEditorParent();
		CreateVCDColor(c,"",VcdColorPreferences.getDefault().getSelection() );
		CreateVCDColor(c,"printing",VcdColorPreferences.getDefault().getSelectionprint() );
		Group length = new Group(c, SWT.FILL);
		length.setLayout(new GridLayout(1, false));
		length.setFont(c.getFont());
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 1;
		length.setLayoutData(gd);
		length.setText("VCD length");
		IntegerFieldEditor ife = new IntegerFieldEditor(PreferenceConstants.P_namesize, "Width of the column name",length);
		ife.setValidRange(115, 300);
		addField(ife);
		new Text(parent, SWT.FILL).setText("Count Figure :"+CountFigure.countvalue());
		initialize();
		checkState();
		return c;
	}

	private int CreateVCDColor(Composite parent, String option, ColorSelection colsel) {
		GridData gd;
		ArrayList<FieldEditor> listfd= new ArrayList<FieldEditor>();
		Group vcd = new Group(parent, SWT.FILL);
		vcd.setLayout(new GridLayout(1, true));			
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan =5;
		vcd.setLayoutData(gd);
		vcd.setText("VCD Color "+option);
		Composite list = new Composite(vcd, SWT.NONE);
		GridLayout gde=new GridLayout(3, true);
		gde.horizontalSpacing =10;
		list.setLayout(gde);	
		gd = new GridData(GridData.BEGINNING);
		gd.horizontalSpan =0;
		gd.verticalSpan =0;
		gd.verticalAlignment =SWT.BEGINNING;
		gd.grabExcessVerticalSpace=true;
		list.setLayoutData(gd);
		Composite local1 = new Composite(list, SWT.FILL);
		RowLayout rl= new RowLayout(SWT.VERTICAL);
		rl.marginBottom=2;
		rl.center=false;
		local1.setLayout(rl);
		local1.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		Composite local2 = new Composite(list, SWT.FILL);
		rl= new RowLayout(SWT.VERTICAL);
		rl.marginBottom=2;
		rl.center=false;
		local2.setLayout(rl);
		local2.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		Composite local3 = new Composite(list, SWT.FILL);
		rl= new RowLayout(SWT.VERTICAL);
		rl.marginBottom=2;
		rl.center=false;
		local3.setLayout(rl);
		local3.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		int n= 0;
		for (ColorData cd :colsel.getArrayColor()) {
			Composite tmp=null;
			if (n==0) tmp=local1;
			if (n==1) tmp=local2;
			if (n==2) tmp=local3;
			FieldEditor fd= new ColorFieldEditor(cd.name, cd.title, tmp);
			addField(fd);
			listfd.add(fd);
			n++;
			if (n==3){
				n=0;
			}
		}
		Button but= new Button(vcd, SWT.NONE);
		but.setText("Restore Default Color");
		but.addSelectionListener(new RestoreColor(colsel,listfd)); 
		local1.pack();
		local2.pack();
		local1.layout();
		local2.layout();
		vcd.pack();
		return 0;
	}
	
	public void init(IWorkbench workbench) {
	}

	@Override
	protected void createFieldEditors() {}

}