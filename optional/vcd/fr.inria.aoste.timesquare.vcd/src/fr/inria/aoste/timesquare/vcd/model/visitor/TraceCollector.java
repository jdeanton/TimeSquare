/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.visitor;

import java.util.ArrayList;
import java.util.HashMap;

import fr.inria.aoste.timesquare.vcd.dialogs.SelectClocksDialog;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.ValueChange;
import fr.inria.aoste.timesquare.vcd.model.keyword.SimulationKeyword;
import fr.inria.aoste.timesquare.vcd.model.keyword.VarType;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;
import fr.inria.aoste.timesquare.vcd.view.VcdValueFactory;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure.Level;

public class TraceCollector implements ISimulationCollector {
	
	private int _time = -1;
	private boolean _isPartial = false;
	private HashMap<String, TraceData> _traceDataMap = new HashMap<String, TraceData>();
	private ArrayList<IVar> _selectedClocks = new ArrayList<IVar>();
	private ArrayList<IVar> _invisibleClocks = new ArrayList<IVar>();
	private double _zoomValue = 1;
	private VcdFactory _vcdFactory = null;
	
	public TraceCollector(Iterable<IVar> vars, VcdFactory vcdFactory) {
		this(vars, vcdFactory, false);
	}

	public TraceCollector(Iterable<IVar> vars, VcdFactory vcdFactory, boolean allVarsVisible) {
		super();
		this._vcdFactory = vcdFactory;		
		_traceDataMap.clear();
		for (IVar var : vars) {
			if (var.isVisibleByDefault() || allVarsVisible) {
				_selectedClocks.add(var);
				VcdValueFactory _vcdValueFactory = vcdFactory.initVcdValueFactory(var,  _zoomValue);
				Object initVarValue = null;
				if ( var.getVarCommand().getType()==VarType.tri ){
					initVarValue = Level.z;
				}
				TraceData td = new TraceData(_vcdValueFactory, initVarValue);
				_traceDataMap.put(var.getIdentiferCode(), td);
			} else {
				_invisibleClocks.add(var);
			}
		}
		for (IVar var : _selectedClocks) {
			getAllnames().remove(var);
		}
	}

	/**
	 * This method is called when we decide which clocks will be visible or not using the clock selection
	 * dialog ({@link SelectClocksDialog}.
	 * For every clock in the listvar list of {@link IVar}, its associated {@link VcdValueFactory} is recreated
	 * whether or not the IVar already had one. Also a new {@link TraceData} is created.
	 * 
	 * @param factory
	 * @param listvar This list contains all the clocks to show. Because the dialog allows both to add new clocks
	 * to show and to remove visible clocks, this list will be the new _selectedClocks list.
	 * @param zoomValue
	 * @see SelectClocksDialog#apply(fr.inria.aoste.timesquare.vcd.IVcdDiagram)
	 */
	public void constructClock(VcdFactory factory, ArrayList<IVar> listvar, double zoomValue) {
		if (_traceDataMap != null) {
			for (TraceData traceData : _traceDataMap.values()) {
				traceData._value = null;
				if (traceData._extendFigure != null){
					traceData._extendFigure.erase();
				}
				traceData._extendFigure = null;
				traceData._vcdValueFactory = null;
			}
			_traceDataMap.clear();
		}
		_selectedClocks.clear();
		_selectedClocks = listvar;
		for (IVar var : _selectedClocks) {
			_traceDataMap.put(var.getIdentiferCode(), new TraceData(factory.initVcdValueFactory(var, 2)));
			getAllnames().remove(var);
		}
	}

	public void visitTimeLine(int time) {
		this._time = time;
		_vcdFactory.modifyTimeLineSize(time);
	}

	public void visitKeyword(SimulationKeyword keyword) {}

	public void visitValueChange(ValueChange valueChange,int time) {
		TraceData traceData = _traceDataMap.get(valueChange.getIdentifierCode());
		if (traceData == null){
			return;
		}
		Object value = valueChange.getValue();
		if (traceData._value != null) {
			traceData._vcdValueFactory.build(traceData._value, time, traceData._previousTime);

		}
		traceData._value = value;
		traceData._previousTime = time;
	}

	public void endSimulation() {
			for (TraceData td : _traceDataMap.values()) {
				if (td._value != null){
					td._vcdValueFactory.buildEnd(td._value, td._previousTime, _time);
				}
			}
	}
	
	public ArrayList<IVar> getSelectedClocks() {
		return _selectedClocks;
	}

	public ArrayList<IVar> getAllnames() {
		return _invisibleClocks;
	}

	public void setAllClocks(ArrayList<IVar> allClocks) {
		this._invisibleClocks = allClocks;
	}

	public void setZoom(double zoomValue) {
		this._zoomValue = zoomValue;
	}

	public boolean isPartial() {
		return _isPartial;
	}

	public void setPartial(boolean partial) {
		this._isPartial = partial;
	}

	public VcdValueFactory getDataFactory(String index) {
		TraceData _traceData = _traceDataMap.get(index);
		if (_traceData == null){
			return null;
		}
		return _traceData._vcdValueFactory;
	}

	public boolean isContainGhost() {
		ArrayList<IVar> listVar = new ArrayList<IVar>();
		listVar.addAll(getSelectedClocks());
		listVar.addAll(getAllnames());
		for (IVar var : listVar) {
			if (var.getValueFactory() != null) {
				if (var.getValueFactory().haveGhostinclock() == true){
					return true;
				}
			}
		}
		return false;
	}

	public void ghostMode(boolean _ghostMode) {
		for (IVar vr : getSelectedClocks()) {
			if (vr.getValueFactory() != null) {
				vr.getValueFactory().setIsGhostVisible(_ghostMode);
			}
		}
	}

	public int clear() {
		for (TraceData td : _traceDataMap.values()) {
			if (td != null){
				td.clear();
			}
		}
		_selectedClocks.clear();
		for (IVar iv : _invisibleClocks) {
			iv.clear();
		}
		_invisibleClocks.clear();
		_traceDataMap.clear();
		_vcdFactory = null;
		return 0;
	}

	@Override
	public void visitEnd(SimulationKeyword keyword) {
	}
	
}

class TraceData {
	protected Object _value;
	protected VcdValueFactory _vcdValueFactory;
	protected int _previousTime = 0;
	protected ExtendFigure _extendFigure = null;

	TraceData(VcdValueFactory _vcdValueFactory, Object _value) {
		this._vcdValueFactory = _vcdValueFactory;
		_previousTime = 0;
		this._value = _value;
	}
	TraceData(VcdValueFactory _vcdValueFactory) {
		this(_vcdValueFactory, null);
	}
	
	@Override
	public String toString() {
		return _previousTime + "," + _value;
	}

	public int clear() {
		if (_vcdValueFactory != null){
			_vcdValueFactory.clear();
		}
		_vcdValueFactory = null;
		if (_extendFigure != null){
			_extendFigure.erase();
		}
		_extendFigure = null;
		return 0;
	}

	@Override
	protected void finalize() throws Throwable {
		if (_vcdValueFactory != null){
			_vcdValueFactory.clear();
		}
		super.finalize();
	}

}
