/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.label;



import java.text.DecimalFormat;

import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;

public class TickTimeLabelling implements ITimeLabelling {
	private VCDDefinitions _vcdDefinitions = null;
	private String _unit = "";
	private double _rate = 0.1d;
	private DecimalFormat _decimalFormat = new DecimalFormat("#.####E0");
	public String getUnit() {
		return _unit;
	}

	public void setUnit(String unit) {
		if (unit == null || unit.length() == 0){
			this._unit="";
			return ;
		}
		this._unit = unit;
	}

	public double getRate() {
		return _rate;
	}

	public void setRate(double rate) {
		if ( rate!=0.0d){
			this._rate = rate;
		}
	}

	public TickTimeLabelling() {
		super();
	}

	@Override
	public String getTimeMarker(double x) {
		if (_unit.length() == 0) {
			return new StringBuilder("Tick: ").append(Math.round(x * _rate)).toString();
		}
		return new StringBuilder("").append(_decimalFormat.format((x * _rate))).append(" ").append(_unit).toString();
	}

	@Override
	public String getRulerTimeIndice(int x) {
		if (_unit.length() == 0) {
			if (x % 10 == 0){
				return  Integer.toString(x / 10);
			}
			return "";
		}
		return _decimalFormat.format(x*_rate);
	}

	public VCDDefinitions getVcdDefinitions() {
		return _vcdDefinitions;
	}

	public void setVcdDefinitions(VCDDefinitions vcdDefinitions) {
		this._vcdDefinitions = vcdDefinitions;
	}

}
