/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;

import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.relation.extensionpoint.Instant;

public class Eventline extends Polyline {

	private String eventName;

	private PointList pointListRef = new PointList();

	private VcdColorPreferences colorPreferences;

	/**
	 * lastEvent: -1 ---> start
	 * lastEvent: 2 ---> drawZ
	 * lastEvent: 1 ---> drawTick
	 * lastEvent: 0 ---> drawNoTick
	 */
	private int lastEvent = -1;

	Boolean drawNoTickCalled = false;

	/**
	 * If drawZCalledTwice >= 2, then this clock will be uniformly displayed.
	 */
	private int drawZCalledTwice = 0;

	private int _startPointX, _startPointY, _endPointX, _endPointY, _unitStepLength,
			_unitStepLengthRef, _endPointXRef, _countInstant = 0;

	private ArrayList<Integer> tickInstants;

	private HashMap<Integer, Boolean> ticksShow;

	private HashMap<Integer, Instant> instants;

	private static int height = 20;

	public Eventline(Point startPoint, int unitStepLength, String eventName, VcdColorPreferences mca) {
		super();
		this.colorPreferences = mca;
		this.setOpaque(false);
		this.eventName = eventName;
		_unitStepLength = unitStepLength * 2;
		_startPointX = startPoint.x;
		_startPointY = startPoint.y + height + 5;
		_endPointX = _startPointX;
		_endPointY = _startPointY;
		_unitStepLengthRef = _unitStepLength;
		_endPointXRef = _endPointX;
		this.setLineWidth(1);
		this.ticksShow = new HashMap<Integer, Boolean>();
		this.tickInstants = new ArrayList<Integer>();
		this.instants = new HashMap<Integer, Instant>();
		this.setForegroundColor(colorPreferences.colorClock());
	}

	public String getEventName() {
		return this.eventName;
	}

	public ArrayList<Integer> getTickInstants() {
		return this.tickInstants;
	}

	public HashMap<Integer, Instant> getIntants() {
		return this.instants;
	}

	public HashMap<Integer, Boolean> getTicksTime() {
		return this.ticksShow;
	}

	public int getPointsSize() {
		return this.getPoints().size();
	}

	public Point getPoint(int index) {
		return this.getPoints().getPoint(index);
	}

	public int getHeight() {
		return Eventline.height;
	}

	public int getStartPointX() {
		return this._startPointX;
	}

	public void setStartPointX(int x) {
		if (x < 0)
			throw new IllegalArgumentException("the x starting point must be positive: " + x);
		_startPointX = x;
	}

	public int getStartPointY() {
		return this._startPointY;
	}

	public void setStartPointY(int y) {
		if (y < 0)
			throw new IllegalArgumentException("the y starting point must be positive: " + y);
		_startPointY = y;
	}

	public int getEndPointX() {
		return this._endPointX;
	}

	public void setEndPointX(int x) {
		if (x < 0)
			throw new IllegalArgumentException("the x starting point must be positive: " + x);
		_endPointX = x;
	}

	public int getEndPointY() {
		return this._endPointY;
	}

	public void setEndPointY(int y) {
		if (y < 0)
			throw new IllegalArgumentException("the x starting point must be positive: " + y);
		_endPointY = y;
	}

	public void setStartPoint(Point sp) {
		this._startPointX = sp.x;
		this._startPointY = sp.y;
	}

	public void setEndPoint(Point p) {
		this._endPointX = p.x;
		this._endPointY = p.y;
	}

	public int getUnitStepLength() {
		return _unitStepLength;
	}

	public void setUnitStepLength(int usl) {
		_unitStepLength = usl;
	}

	public void drawNoTick(int time) {
		_endPointX = (time) * _unitStepLength / 10 + 5;
		switch (lastEvent) {
		case -1:
			this.addPoint(new Point(_startPointX, _endPointY));
			this.addPoint(new Point(_endPointX, _endPointY));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY));
			break;
		case 0:
			this.addPoint(new Point(_endPointX, _endPointY));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY));
			break;
		case 1:
			this.addPoint(new Point(_endPointX, _endPointY));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY));
			break;
		case 2:
			this.addPoint(new Point(_endPointX, _endPointY - height / 2));
			this.addPoint(new Point(_endPointX, _endPointY));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY));
			break;
		default:
			break;
		}
		lastEvent = 0;
		EventPanel eventlinePanel = (EventPanel) getParent();
		eventlinePanel.setPreferredSize(_endPointX + _unitStepLength,
				eventlinePanel.getPreferredSize().height);
	}

	public void drawTick(int time) {
		_endPointX = (time) * _unitStepLength / 10 + 5;
		switch (lastEvent) {
		case -1:
			this.addPoint(new Point(_startPointX, _endPointY));
			this.addPoint(new Point(_endPointX, _endPointY));
			this.addPoint(new Point(_endPointX, _endPointY - height));
			this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY - height));
			this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY));
			break;
		case 0:
			// if(drawZCalledTwice >= 2){
			// this.addPoint(new Point(_endPointX , _endPointY - height/2));
			// this.addPoint(new Point(_endPointX, _endPointY - height));
			// this.addPoint(new Point(_endPointX + 2*_unitStepLength/5,
			// _endPointY - height));
			// this.addPoint(new Point(_endPointX + 2*_unitStepLength/5,
			// _endPointY - height/2));
			// this.addPoint(new Point(_endPointX + _unitStepLength , _endPointY
			// - height/2));
			// }else{
			this.addPoint(new Point(_endPointX, _endPointY));
			this.addPoint(new Point(_endPointX, _endPointY - height));
			this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY - height));
			this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY));
			// }
			break;
		case 1:
			if (drawZCalledTwice >= 2) {
				this.addPoint(new Point(_endPointX, _endPointY - height / 2));
				this.addPoint(new Point(_endPointX, _endPointY - height));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY - height));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY - height
						/ 2));
				this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY - height / 2));
			} else {
				this.addPoint(new Point(_endPointX, _endPointY));
				this.addPoint(new Point(_endPointX, _endPointY - height));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY - height));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY));
				this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY));
			}
			break;
		case 2:
			if (drawZCalledTwice >= 2) {
				this.addPoint(new Point(_endPointX, _endPointY - height / 2));
				this.addPoint(new Point(_endPointX, _endPointY - height));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY - height));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY - height
						/ 2));
				this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY - height / 2));
			} else {
				this.addPoint(new Point(_endPointX, _endPointY - height / 2));
				this.addPoint(new Point(_endPointX, _endPointY - height));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY - height));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength / 5, _endPointY));
				this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY));
			}
			break;
		default:
			break;
		}
		lastEvent = 1;
		EventPanel eventlinePanel = (EventPanel) getParent();
		eventlinePanel.setPreferredSize(_endPointX + _unitStepLength,
				eventlinePanel.getPreferredSize().height);
		int t = time / 10;
		ticksShow.put(t, false);
		tickInstants.add(t);
		_countInstant++;
		Instant in = new Instant(eventName, _countInstant);
		instants.put(t, in);

	}

	public void drawEnd(int time) {
		_endPointX = (time) * _unitStepLength / 10 + 5;
		switch (lastEvent) {
		case -1:
			this.addPoint(new Point(_startPointX, _endPointY - height / 2));
			this.addPoint(new Point(_startPointX + 2 * _unitStepLength, _endPointY - height / 2));
			break;
		case 0:
			this.addPoint(new Point(_endPointX, _endPointY));
			this.addPoint(new Point(_endPointX, _endPointY - height / 2));
			this.addPoint(new Point(_endPointX + 2 * _unitStepLength, _endPointY - height / 2));
			break;
		case 1:

			if (drawZCalledTwice >= 2) {
				this.removePoint(this.getPoints().size() - 1);
				this.removePoint(this.getPoints().size() - 1);
				Point lastPoint = this.getPoints().getLastPoint();
				this.addPoint(new Point(lastPoint.x, _endPointY - height / 2));
				this.addPoint(new Point(_endPointX, _endPointY - height / 2));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength, _endPointY - height / 2));
			} else {
				this.addPoint(new Point(_endPointX, _endPointY));
				this.addPoint(new Point(_endPointX, _endPointY - height / 2));
				this.addPoint(new Point(_endPointX + 2 * _unitStepLength, _endPointY - height / 2));
			}
			break;
		case 2:
			this.addPoint(new Point(_endPointX, _endPointY - height / 2));
			this.addPoint(new Point(_endPointX + 2 * _unitStepLength, _endPointY - height / 2));
			break;
		default:
			break;
		}

		if (drawZCalledTwice >= 2 && !drawNoTickCalled) {
			PointList pl = new PointList();
			pl.addAll(this.getPoints());
			int sizeofpl = pl.size();
			Point p = null;
			for (int i = 0; i < sizeofpl; i++) {
				p = pl.getPoint(i);
				if (p.y == _endPointY) {
					p.setY(_endPointY - height / 2);
				}
				pl.setPoint(p, i);
			}
			this.removeAllPoints();
			this.setPoints(pl);
			this.setForegroundColor(colorPreferences.colorYellowZ());
		}
		lastEvent = 2;
		EventPanel clocklinePanel = (EventPanel) getParent();
		clocklinePanel.setPreferredSize(_endPointX + _unitStepLength * 5,
				clocklinePanel.getPreferredSize().height);

		pointListRef.addAll(this.getPoints());

	}

	public void drawZ(int time) {
		_endPointX = (time) * _unitStepLength / 10 + 5;
		switch (lastEvent) {
		case -1:
			this.addPoint(new Point(_startPointX, _endPointY - height / 2));
			this.addPoint(new Point(_startPointX + _unitStepLength, _endPointY - height / 2));
			break;
		case 0:
			this.addPoint(new Point(_endPointX, _endPointY));
			this.addPoint(new Point(_endPointX, _endPointY - height / 2));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY - height / 2));
			break;
		case 1:
			this.removePoint(this.getPoints().size() - 1);
			this.removePoint(this.getPoints().size() - 1);
			Point lastPoint = this.getPoints().getLastPoint();
			this.addPoint(new Point(lastPoint.x, _endPointY - height / 2));
			this.addPoint(new Point(_endPointX, _endPointY - height / 2));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY - height / 2));
			break;
		case 2:
			this.addPoint(new Point(_endPointX, _endPointY - height / 2));
			this.addPoint(new Point(_endPointX + _unitStepLength, _endPointY - height / 2));
			break;
		default:
			break;
		}

		if (lastEvent == 1) {
			drawZCalledTwice++;
		}
		lastEvent = 2;
		EventPanel clocklinePanel = (EventPanel) getParent();
		clocklinePanel.setPreferredSize(_endPointX + _unitStepLength,
				clocklinePanel.getPreferredSize().height);
	}

	public void eventLineZoom(double value) {
		PointList pl = new PointList();
		pl.addAll(pointListRef);
		int sizeofpl = pl.size();
		Point p = null;
		for (int i = 0; i < sizeofpl; i++) {
			p = pl.getPoint(i);
			p.setX((int) ((p.x - _startPointX) * value + _startPointX));
			pl.setPoint(p, i);
		}
		this.removeAllPoints();
		this.setPoints(pl);
		_unitStepLength = (int) (_unitStepLengthRef * value);
		if (sizeofpl < 2) {
			return;
		}
		_endPointX = pl.getPoint(sizeofpl - 2).x;
	}

	public void resetDefaultSize() {
		PointList pl = new PointList();
		pl.addAll(pointListRef);
		this.removeAllPoints();
		this.setPoints(pl);
		_unitStepLength = _unitStepLengthRef;
		_endPointX = _endPointXRef;
	}

	public void eventLineZoomIn() {
		PointList pl = new PointList();
		pl.addAll(this.getPoints());
		int sizeofpl = pl.size();
		Point p = null;
		for (int i = 0; i < sizeofpl; i++) {
			p = pl.getPoint(i);
			p.setX((p.x - _startPointX) * 2 + _startPointX);
			pl.setPoint(p, i);
		}
		this.removeAllPoints();
		this.setPoints(pl);
		_unitStepLength = _unitStepLength * 2;
		_endPointX = pl.getPoint(sizeofpl - 2).x;
	}

	public void eventLineZoomOut() {
		PointList pl = new PointList();
		pl.addAll(this.getPoints());
		int sizeofpl = pl.size();
		Point p = null;
		for (int i = 0; i < sizeofpl; i++) {
			p = pl.getPoint(i);
			p.setX((p.x - _startPointX) / 2 + _startPointX);
			pl.setPoint(p, i);
		}
		this.removeAllPoints();
		this.setPoints(pl);
		_unitStepLength = _unitStepLength / 2;
		_endPointX = pl.getPoint(sizeofpl - 2).x;
	}

	public void setEventlineLocation(Point pt) {
		PointList pl = this.getPoints();
		int sizeofpl = pl.size();
		Point p = null;
		for (int i = 0; i < sizeofpl; i++) {
			p = pl.getPoint(i);
			p.setY(p.y + pt.y + height + 5 - _startPointY);
			pl.setPoint(p, i);

			p = pointListRef.getPoint(i);
			p.setY(p.y + pt.y + height + 5 - _startPointY);
			pointListRef.setPoint(p, i);
		}
		this.setPoints(pl);
		_startPointY = pt.y + height + 5;
		_endPointY = pt.y + height + 5;

	}

	public void clear() {
		tickInstants.clear();
		ticksShow.clear();
		instants.clear();
		tickInstants = null;
		ticksShow = null;
		instants = null;
		this.pointListRef = null;
	}

}
