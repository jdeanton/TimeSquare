/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.listener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.Label;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class MouseClickListener implements MouseListener {
	private IVcdDiagram _vcdDiagram;
	private int _xScroll;
	private int _yScroll;
	VcdColorPreferences _mca;
	private ListConnections _listConnections;
	public MouseClickListener(ListConnections listConnections, IVcdDiagram vcdDiagram
			) {
		this._listConnections=listConnections;
		this._vcdDiagram = vcdDiagram;
		_mca = vcdDiagram.getColorPreferences();
	}

	private void buttonDown1(MouseEvent e) {
		try {
			buttonDownold1(e);
		} catch (Throwable ex) {
			ErrorConsole.printError(ex);
		}
	}

	private void buttonDown2(MouseEvent e) {}

	private void buttonDown3(MouseEvent e) {
		try {
			_vcdDiagram.getMarkerFactory().showMarker(e.x, _xScroll);
			_vcdDiagram.getMarkerFactory().showFireable(e.x + _xScroll);
		} catch (Throwable ex) {
			ErrorConsole.printError(ex);
		}
	}

	@Override
	public void mouseDown(MouseEvent e) {
		_xScroll = _vcdDiagram.getCanvas().getHorizontalBar().getSelection();
		_yScroll = _vcdDiagram.getCanvas().getVerticalBar().getSelection();
		switch (e.button) {
		case 1:
			buttonDown1(e); // left click
			break;
		case 2:
			buttonDown2(e); // middle click
			break;
		case 3:
			buttonDown3(e); // right click
			break;
		default:
			break;
		}

	}

	@Override
	public void mouseDoubleClick(MouseEvent e) {}

	@Override
	public void mouseUp(MouseEvent e) {}
	
	private void buttonDownold1(MouseEvent e) {
		try {
			IFigure fig = _vcdDiagram.getCanvas().getContents().findFigureAt(
					e.x + _xScroll, e.y + _yScroll);
			Description descr;
			if (fig == null)
				return;
			if (fig instanceof Label)
				descr = _vcdDiagram.getVcdFactory().getFigureForDescription(fig.getParent());
			else
				descr = _vcdDiagram.getVcdFactory().getFigureForDescription(fig);

			if (descr == null) {
				return;
			}
			if (!_listConnections.getListFind().isEmpty()) {
				if (_listConnections.getListFind().contains(fig)) {
					_listConnections.getListFind().remove(fig);
					fig.setForegroundColor(_mca.colorLightGreenFirable());
				}
			}
			if (_listConnections.getListDescription().isEmpty()) {
				if (fig instanceof Label)
					_listConnections.getListDescription().add(
							_vcdDiagram.getVcdFactory().getFigureForDescription(fig.getParent()));
				else
					_listConnections.getListDescription()
							.add(
									_vcdDiagram.getVcdFactory().getFigureForDescription(fig));
			} else {
				if (!_listConnections.getListDescription().contains(descr))
					_listConnections.getListDescription()
							.add(
									_vcdDiagram.getVcdFactory().getFigureForDescription(fig));
				else
					_listConnections.getListDescription().remove(descr);
			}
		} catch (Throwable ex) {
			ErrorConsole.printError(ex);
		}
	}
}
