/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.command;

import java.util.HashMap;

import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.view.VcdValueFactory;

final public class HierVar implements IVar {
	
	private IVar _child;
	private String _parentStr;
	private HashMap<String , Object> _hm= new HashMap<String, Object>();
	
	public HierVar(IVar child, String parent) {
		super();
		this._child = child;
		this._parentStr = parent;
	}

	public String getName() {
		return _child.getName();
	}

	public String getQualifiedName() {
		return _parentStr + ":" + _child.getQualifiedName();
	}

	public String getAliasName()
	{
		return _child.getAliasName();
	}
	public int getSize() {
		return _child.getSize();
	}

	@Override
	public String toString() {
		return "var " + getSize() + " " + getQualifiedName();
	}

	public String getIdentiferCode() {
		return this._child.getIdentiferCode();
	}

	public VcdValueFactory getValueFactory() {
		return null;
	}

	public void setValueFactory(VcdValueFactory v) {

	}

	public boolean isVisibleByDefault() {

		return true;
	}

	public void setVisibleByDefault(boolean f) {}

	public int clear() {
		_child.clear();
		_hm.clear();
		return 0;
	}

	public Object getData(String s) {
		return _hm.get(s);
	}

	public Object setData(String s, Object o) {
		return _hm.put(s, o);
	}

	public VarCommand getVarCommand() {
		return null;
	}

}
