/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.command;

import java.util.HashMap;

import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.IScopeCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.VarType;
import fr.inria.aoste.timesquare.vcd.model.visitor.IScopeVisitor;

final public class VarCommand implements IScopeCommand {
	
	private VarType _type;
	private int _size;
	private String _ident, _ref, _alias;
	private HashMap<String , Object> _hm= new HashMap<String, Object>();
	private HashMap<Integer , HashMap<String, Object>> _ticvlist= new HashMap<Integer, HashMap<String, Object>>();

	public VarCommand(VarType type, int size, String ident, String ref) {
		this._type = type;
		this._size = size;
		this._ident = ident;
		this._ref = ref;
		this._alias=ref;
	}

	public String getName() {
		return this._ref;
	}

	public String getAlias() {
		return this._alias;
	}

	public int getSize() {
		return this._size;
	}

	public void visit(IScopeVisitor visitor) {
		visitor.visitVar(this, _type, _ident);
	}

	public String getIdentifierCode() {
		return this._ident;
	}
	
	public Object getData(String s) {
		return _hm.get(s);
	}

	
	public Object setData(String s, Object o) {
		if ( s.compareTo("alias")==0){
			if (o!=null){
				_alias = (String) o;
			}
		}
		return _hm.put(s, o);
	}

	public Object getTickData(int tck,String s) {
		HashMap<String, Object> tmp=_ticvlist.get(tck);
		if (tmp==null){
			return null;
		}
		return tmp.get(s);
	}

	
	public Object setTickData(int tck, String s, Object o) {
		HashMap<String, Object> tmp=_ticvlist.get(tck);
		if (tmp==null){
			tmp= new HashMap<String, Object>();
			_ticvlist.put(tck,tmp);	
		}
		return tmp.put(s, o);
	}	

	public void newCommentCommand(ICommentCommand icc) {
		if (icc!=null){
			icc.validate();
		}	
		
	}

	public VarType getType() {
		return this._type;
	}
}
