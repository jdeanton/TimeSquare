/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class EventPanel extends Panel {
	
	private VcdColorPreferences _mca;
	
	private VcdFactory _vcdFactory;
	
	private Eventline _eventline;
	
	private String _eventlineName = null;
	
	private String _aliasName;
	
	private Boolean _isDurationModel = false;
	
	private int 
		_eventStartPointX,
		_eventStartPointY,
		_eventHeight,
		_eventUnitLength;
	private Rectangle _clocklineArea;
	
	private HashMap<Integer, Boolean> _ticks;
	
	public HashMap<Integer, Boolean> getTicks(){
		return this._ticks;
	}
	
	public ArrayList<Integer> _tickInstants;
	
	private HashMap<Integer, RoundedRectangle> _markers;
	
	private HashMap<Integer, Point> _tickMarkersPositions = new HashMap<Integer,Point>();
	
	public HashMap<Integer, RoundedRectangle> getMarkers(){
		return this._markers;
	}
	
	public String getAliasName(){
		return _aliasName;
	}
	
	public String getClockName(){
		return _eventlineName;
	}
	
	public Boolean isDurationModel(){
		return _isDurationModel;
	}
	
	public void setDurationModel(){
		_isDurationModel = true;
	}
	
	public EventPanel(ConstraintsFactory cf, VcdFactory vf, String eventlineName,String aliasName, VcdColorPreferences mca){
		super();
		this._mca = mca;
		this._vcdFactory = vf;
		this._eventlineName = eventlineName;
		this._aliasName = aliasName;
		_markers = new HashMap<Integer, RoundedRectangle>();
		setOpaque(true);
	}
	
	public EventPanel(ConstraintsFactory cf, VcdFactory vf, String eventlineXmiName,String eventlineName,String aliasName, VcdColorPreferences mca){
		super();
		this._mca = mca;
		this._vcdFactory = vf;
		this._eventlineName = eventlineName;
		this._aliasName = aliasName;
		_markers = new HashMap<Integer, RoundedRectangle>();
		setOpaque(true);
	}
	
	
	public void initEventPanel(Eventline cl) {
		this._eventline = cl;
		this.add(cl);
		addMouseListener(new MouseListener(){
			@Override
			public void mousePressed(MouseEvent me) {
				if(me.button==1){
					_eventStartPointX = _eventline.getStartPointX();
					_eventline.getEndPointX();
					_eventStartPointY = _eventline.getStartPointY();
					_eventHeight = _eventline.getHeight();
					_eventUnitLength = _eventline.getUnitStepLength();
					if(_eventUnitLength <= 0){
						return;
					}
					_ticks = _eventline.getTicksTime();
					_clocklineArea = _eventline.getBounds();
					if(_clocklineArea.contains(new Point(me.x,me.y))){
						if((me.x - _eventStartPointX)%(_eventUnitLength)<=_eventUnitLength*2/5){
							int tickInstant = (me.x - _eventStartPointX)/(_eventUnitLength);
							if(_ticks.containsKey(tickInstant)){
								if(_ticks.get(tickInstant) == false){
									RoundedRectangle rr = new RoundedRectangle();
									Rectangle rec = null;
									if(_clocklineArea.height == _eventHeight+1){
										rec = new Rectangle(0, 0, (_eventUnitLength+1)*2/5,20);
										rr.setBounds(rec);
										rr.setBackgroundColor(_mca.colorTickMarker());
										rr.setLocation(new Point(_eventStartPointX + _eventUnitLength*tickInstant +1 ,_eventStartPointY-_eventHeight +1));			
									}else{
										rec = new Rectangle(0, 0, (_eventUnitLength+1)*2/5,10);
										rr.setBounds(rec);
										rr.setBackgroundColor(_mca.colorTickMarker());
										rr.setLocation(new Point(_eventStartPointX + _eventUnitLength*tickInstant +1 ,_eventStartPointY-_eventHeight +1));
									}
									add(rr);
									_markers.put(tickInstant, rr);
									_tickMarkersPositions.put(tickInstant, new Point(5 + 20*tickInstant +1 ,getLocation().y + 5 +1));
									_ticks.remove(tickInstant);
									_ticks.put(tickInstant, true);
									_vcdFactory.getConstraintsFactory().drawOneTickConstraints(tickInstant, _eventlineName);
								}else{
									remove(_markers.get(tickInstant));
									_tickMarkersPositions.remove(tickInstant);
									_markers.get(tickInstant).erase();
									_markers.remove(tickInstant);
									_ticks.remove(tickInstant);
									_ticks.put(tickInstant, false);
									_vcdFactory.getConstraintsFactory().drawOneTickConstraints(tickInstant, _eventlineName);
								}
							}
						}
					}		
				}
			}
			@Override
			public void mouseReleased(MouseEvent me) {}
			@Override
			public void mouseDoubleClicked(MouseEvent me) {}
		});
		
		addMouseMotionListener(new MouseMotionListener() {
			
			private Label label = new Label();
		
			private RoundedRectangle rr = new RoundedRectangle();
			
			@Override
			public void mouseMoved(MouseEvent me) {
				rr.setVisible(false);
				label.setVisible(false);
			}
			
			@Override
			public void mouseHover(MouseEvent me) {
				_clocklineArea = _eventline.getBounds();
				if(_clocklineArea.contains(new Point(me.x,me.y))){
					_eventStartPointX = _eventline.getStartPointX();
					_eventline.getEndPointX();
					_eventStartPointY = _eventline.getStartPointY();
					_eventHeight = _eventline.getHeight();
					_eventUnitLength = _eventline.getUnitStepLength();
					if(_eventUnitLength <= 0){
						return;
					}
					_ticks = _eventline.getTicksTime();
					_tickInstants = _eventline.getTickInstants();
					if((me.x - _eventStartPointX)%(_eventUnitLength)<=_eventUnitLength*2/5){
						int ticknum = (me.x - _eventStartPointX)/(_eventUnitLength);
						if(_tickInstants.contains(ticknum)){
							rr.setVisible(true);
							label.setVisible(true);
							int tickNumShow = _tickInstants.indexOf(ticknum)+1;
							int n = _aliasName.length() + 5;
							rr.setBounds(new Rectangle(0, 0, 8 * n, 28));
							label.setBounds(new Rectangle(0, 0, 8 * n, 23));
							label.setText(_aliasName+"["+tickNumShow+"]");
							Point loc = new Point(me.x,me.y+18);
							rr.setLocation(loc);
							label.setLocation(loc);
							getParent().getParent().add(rr);
						}
					}
				}
			}
			
			@Override
			public void mouseExited(MouseEvent me) {
				rr.setVisible(false);
				label.setVisible(false);
			}
			
			@Override
			public void mouseEntered(MouseEvent me) {
				label.setOpaque(false);
				label.setBounds(new Rectangle(0, 0, 70, 20));
				label.setLabelAlignment(PositionConstants.CENTER);
				label.setForegroundColor(ColorConstants.black);
				rr.setBounds(new Rectangle(0,0,70,20));
				rr.setOpaque(false);
				rr.setBackgroundColor(ColorConstants.lightGray);
				rr.setForegroundColor(ColorConstants.gray);
				rr.add(label);
				
			}
			
			@Override
			public void mouseDragged(MouseEvent me) {
				
			}
			
		});
		
		
	}
	
	public void markersZoom(double value){
		_eventStartPointX = _eventline.getStartPointX();
		for(int ticknum: _markers.keySet()){
			Point p = new Point(_tickMarkersPositions.get(ticknum));
			p.x = (int) ((p.x - _eventStartPointX - 1) * value+ _eventStartPointX + 1);
			Dimension dim = _markers.get(ticknum).getSize();
			dim.width = (int) ( (40*value/5) );
			_markers.get(ticknum).setSize(dim);
			_markers.get(ticknum).setLocation(p);
		}
	}
	
	public void markersZoomIn(){
		_eventStartPointX = _eventline.getStartPointX();
		_eventline.getEndPointX();
		_eventStartPointY = _eventline.getStartPointY();
		_eventHeight = _eventline.getHeight();
		_eventUnitLength = _eventline.getUnitStepLength();
		
		for(RoundedRectangle recf: _markers.values()){
			Point p = recf.getLocation();
			p.x = (p.x - _eventStartPointX - 1) * 2 + _eventStartPointX + 1;
			Dimension dim = recf.getSize();
			dim.width *=2;
			recf.setSize(dim);
			recf.setLocation(p);
		}
	}
	
	public void markersZoomOut(){
		_eventStartPointX = _eventline.getStartPointX();
		_eventline.getEndPointX();
		_eventStartPointY = _eventline.getStartPointY();
		_eventHeight = _eventline.getHeight();
		_eventUnitLength = _eventline.getUnitStepLength();
		
		for(RoundedRectangle recf: _markers.values()){
			Point p = recf.getLocation();
			p.x = (p.x - _eventStartPointX -1 ) / 2 + _eventStartPointX + 1;
			recf.setLocation(p);
			Dimension dim = recf.getSize();
			dim.width /=2;
			recf.setSize(dim);
		}
		
	}
	
	public void setEventline(Eventline cl){
		this._eventline = cl;
	}
	
	public Eventline getEventline(){
		return this._eventline;
	}
}
