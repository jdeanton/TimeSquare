/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.command;

import fr.inria.aoste.timesquare.vcd.model.IDeclarationCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.TimeUnit;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

final public class TimeScaleCommand implements IDeclarationCommand {
	
	private int _number=1;
	private TimeUnit _unit;

	public TimeScaleCommand() {
		super();
		 set(1, TimeUnit.tick);
	}

	public TimeScaleCommand(String number, String unit) {
		setNumber(number);
		setUnit(unit);	
	}

	public void visit(IDeclarationVisitor visitor) {
		if (_unit != null && _unit!=TimeUnit.tick)
			visitor.visitTimeScale(_number, _unit);
	}

	public int getNumber() {
		return _number;
	}

	public TimeUnit getUnit() {
		return _unit;
	}

	
	public int clear() {		
		return 0;
	}

	public final void setNumber(String number) {
		try {
			this._number = Integer.parseInt(number);
		} catch (Throwable e) {}
	}

	public final void setNumber(int number) {
		this._number = number;
	}

	public final void setUnit(TimeUnit unit) {
		this._unit = unit;
	}

	public final void setUnit(String unit) {
		try {
			this._unit = TimeUnit.valueOf(unit);
		} catch (Throwable e) {}
	}

	public int set(int n, TimeUnit tu) {
		setNumber(n);
		setUnit(tu);
		return 0;
	}

	public int set(int n) {
		switch (n) {
		case 0:
			return set(1, TimeUnit.s);
		case 1:
			return set(100, TimeUnit.ms);
		case 2:
			return set(10, TimeUnit.ms);
		case 3:
			return set(1, TimeUnit.ms);
		case 4:
			return set(100, TimeUnit.us);
		case 5:
			return set(10, TimeUnit.us);
		case 6:
			return set(1, TimeUnit.us);
		case 7:
			return set(100, TimeUnit.ns);
		case 8:
			return set(10, TimeUnit.ns);
		case 9:
			return set(1, TimeUnit.ns);
		case 10:
			return set(100, TimeUnit.ps);
		case 11:
			return set(10, TimeUnit.ps);
		case 12:
			return set(1, TimeUnit.ps);
		case 13:
			return set(100, TimeUnit.fs);
		case 14:
			return set(10, TimeUnit.fs);
		case 15:
			return set(1, TimeUnit.fs);

		default:
			break;
		}
		return 0;
	}
}
