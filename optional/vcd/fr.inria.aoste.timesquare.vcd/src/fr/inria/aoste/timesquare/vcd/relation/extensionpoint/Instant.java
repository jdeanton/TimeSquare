/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.relation.extensionpoint;

import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;

public class Instant {
	
	private String xmiId = "";
	private int instantNumber = -1;
	private VarCommand vr;
	
	public Instant(String xmiId, int instantNumber) {
		super();
		this.xmiId = xmiId;
		this.instantNumber = instantNumber;
	}

	public final VarCommand getVr() {
		return vr;
	}

	public final void setVr(VarCommand vr) {
		this.vr = vr;
	}

	public String getXmiId() {
		return xmiId;
	}

	public int getInstantNumber() {
		return instantNumber;
	}	
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Instant))
			return false;		
		
		if (!xmiId.equals(((Instant) obj).getXmiId()))
			return false;

		if (instantNumber != ((Instant) obj).getInstantNumber())
			return false;

		return true;
	}
	
	@Override
	public int hashCode() {
		
		return xmiId.hashCode()+ instantNumber ;
	}

	public Object getData(String id){
		return vr.getTickData(instantNumber, id);
	}
	
	public Object putData(String id, Object value){
		return vr.setTickData(instantNumber, id,value);
	}

}
