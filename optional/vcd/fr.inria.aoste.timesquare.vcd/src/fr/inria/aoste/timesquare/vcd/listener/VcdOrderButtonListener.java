/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.listener;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.dialogs.SelectClocksDialog;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;

public class VcdOrderButtonListener implements SelectionListener {
	private ListConnections _listConnections;
	private IVcdDiagram _vcdDiagram;

	public VcdOrderButtonListener(ListConnections listConnections, IVcdDiagram vcdDiagram) {
		this._listConnections = listConnections;
		this._vcdDiagram = vcdDiagram;
	}

	public void widgetDefaultSelected(SelectionEvent e) {}

	public void widgetSelected(SelectionEvent e) {
		Shell shell = new Shell();
		SelectClocksDialog selectClockDialog = new SelectClocksDialog(shell, _vcdDiagram.getTraceCollector()
				.getAllnames(), _vcdDiagram.getTraceCollector().getSelectedClocks(), "All clocks",
				"Selected clocks", "Selected clocks dialog");
		selectClockDialog.create();
		int ret = selectClockDialog.open();
		if (ret == Window.OK) {
			_vcdDiagram.getCanvas().getContents().getChildren().clear();
			_vcdDiagram.getNames().getContents().getChildren().clear();
			_listConnections.clear();
			_vcdDiagram.getTraceCollector().setAllClocks(selectClockDialog.getSrc());
			_vcdDiagram.getTraceCollector().constructClock(_vcdDiagram.getVcdFactory(), selectClockDialog.added(),
					1);
			_vcdDiagram.getVcdFactory().getNameforfigures().clear();
			_vcdDiagram.getVcdFactory().getClocklinesXmiMap().clear();
			_vcdDiagram.getVcdFactory().getTimeline().removeAllPoints();
			_vcdDiagram.getVcdFactory().getTimeline().removeAll();
			_vcdDiagram.getVcdFactory().getScalePanel().removeAll();

			_vcdDiagram.getVcdModel().visit(_vcdDiagram.getTraceCollector());
//			_vcdDiagram.getNames().setContents(_vcdDiagram.getVcdFactory().getNames());
//			_vcdDiagram.getCanvas().setContents(_vcdDiagram.getVcdFactory().getBackPanel());

//			_vcdDiagram.getNames().getParent().layout();
//			_vcdDiagram.getCanvas().getContents().validate();
			for (MenuItem menuitem : _listConnections.getMenuForComment().keySet()) {
				if (menuitem.getSelection()) {
					ConstraintCommentCommand cc = _listConnections.getMenuForComment().get(
							menuitem);
					Color color = _listConnections.menuForColorGet(menuitem);
					_vcdDiagram.getConstraintsFactory().drawSyncInterval(cc, color);
				}
			}
			if (selectClockDialog.added().size() <= 1) {
				_vcdDiagram.getMyShell().setSize(_vcdDiagram.getMyShell().getSize().x, 200);
			} else {
				_vcdDiagram.getMyShell().setSize(_vcdDiagram.getMyShell().getSize().x,
						selectClockDialog.added().size() * 45 + 200);
			}
			if (_vcdDiagram.isGhostMode().isHide())
				_vcdDiagram.getConstraintsFactory().hideAllGhost();
		}
	}

}
