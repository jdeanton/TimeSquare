/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.model.comment.Moderator;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class ConstraintAlternates extends AbsConstraint implements IConstraint {

	public ConstraintAlternates() {
		super();
		testn = 1;
	}

	/**    
	 *      Draw A alternates with B
	 */
	public int draw(Draw1 currentfig, String currentclock) {
		String getAclk= cc.getClock();		
		String getBclk= cc.getReferenceClocks().get(0);
		int v1=1;
		int v2=1;
		if (cc.getReferenceClocks().size()==3){
			v1 = Integer.valueOf(cc.getReferenceClocks().get(1));
			v2 = Integer.valueOf(cc.getReferenceClocks().get(2));
		}
		Moderator md = cc.getModerator();
		if (md==null){
			md= Moderator.weakly;
		}
		
		int index = getIndice(currentclock,currentfig);
		
		if ( getAclk.equals(currentclock)) {			
			int pack= index/ v1;
			int a1= pack*v1;
			int an= (pack+1) *v1 -1;
			int bn= (pack*v2)-1;
			int b1= (pack*v2);
			connect(getTick(getAclk, an), getTick(getBclk, b1), md.is_rightWeakly());
			connect(getTick(getBclk, bn), getTick(getAclk, a1), md.is_leftWeakly());
			drawPacket(getTick(getAclk, a1),getTick(getAclk, an));
			if (flagdraw){ // DO NOT use in fullmode 
				drawPacket(getTick(getBclk, bn-v2+1),getTick(getBclk, bn));
			}
			drawPacket(getTick(getBclk, b1),getTick(getBclk, b1+v2-1));
		} else {
			int pack= index/ v2;
			int a1= (pack+1)*v1;
			int an= (pack+1) *v1 -1;
			int bn= ((pack+1)*v2)-1;
			int b1= (pack*v2);				
			connect(getTick(getBclk, bn), getTick(getAclk, a1), md.is_leftWeakly());
			connect(getTick(getAclk, an), getTick(getBclk, b1), md.is_rightWeakly());
			drawPacket(getTick(getBclk, b1),getTick(getBclk, bn));
			drawPacket(getTick(getAclk, an-v1+1),getTick(getAclk, an));
			drawPacket(getTick(getAclk, a1),getTick(getAclk, a1+v1-1));
		}
		return 0;
	}		

	private void drawPacket(IFigure f1, IFigure f2)	{
		if (f1==null){
			return ;
		}
		if (f2==null){
			return ;
		}
		ArrayList<ConstraintsConnection> packet = icc.constructPacket(mca.colorBlueSustain(), (Draw1)f1, (Draw1)f2);		
		for (ConstraintsConnection c : packet) {
			c.setGlobal(isGlobal);
			c.setComment(cc);
			icc.addToList(list.getListConstraints(), c);
		}
		
	}
	
	
	private void connect(ExtendFigure source, ExtendFigure target, boolean empty) {
		if(source == null || target == null){  // Out of the scope
			return;
		}
		Color color = mca.colorWhiteArrow();
		if(source.getBounds().x == target.getBounds().x){
			color = mca.colorWeakyCoincidence();
		}
		ConstraintsConnection poly = icc.constructDashConnection(color, (Draw1)source,(Draw1) target, empty);
		poly.setComment(cc);
		poly.setGlobal(isGlobal);
		icc.addToList(list.getListConstraints(), poly);
	}
	
	boolean flagdraw= true;

	public int drawTableItemConstraints() {
		try {
			isGlobal = true;
			String nameClock = cc.getClock();
			int v1=1;
			flagdraw =false;
			if (cc.getReferenceClocks().size()==3){
				v1 = Integer.valueOf(cc.getReferenceClocks().get(1));
			}
			if (haveAllClockVisible()) {
				ArrayList<ExtendFigure> lfig=vcdFactory.getNameforfigures().get(nameClock);
				for (int i=0;i< lfig.size(); i+=v1){
					IFigure fig =lfig.get(i);
					draw((Draw1) fig, nameClock);
				}
			}else{
				return -1;
			}
			IVcdDiagram vdt = vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints()) {
				Dimension dim = vdt.getCanvas().getContents().getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width,bounds.height, true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible = true;
			isGlobal = false;
			flagdraw =true;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		} catch (Throwable t) {
			isGlobal = false;
			ErrorConsole.printError(t);
			flagdraw =true;
			return -1;
		}
	}

	@Override
	public int drawOneTick(int ticknum, String clockId) {
		// TODO Auto-generated method stub
		return 0;
	}

}
