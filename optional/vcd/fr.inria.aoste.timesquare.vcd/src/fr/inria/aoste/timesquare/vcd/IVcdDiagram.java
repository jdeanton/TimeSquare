/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.vcd.draw.FigureCanvasBase;
import fr.inria.aoste.timesquare.vcd.menu.Mode;
import fr.inria.aoste.timesquare.vcd.menu.VcdMenu;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;
import fr.inria.aoste.timesquare.vcd.view.MarkerFactory;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public interface IVcdDiagram extends IVcd {

	public abstract MarkerFactory getMarkerFactory();

	public abstract double getMarkerZoom();

	public abstract Shell getMyShell();

	public abstract FigureCanvas getNames();

	public abstract FigureCanvas getScaleCanvas();

	public abstract double getZoomValue();

	public abstract boolean isCtrlKey();

	public abstract boolean isSimulation();

	public abstract void setCtrlKey(boolean ctrlKey);

	public abstract void setGhostMode(Mode ghostMode);

	public abstract void setMarkerZoom(double markerZoom);

	public abstract void setSimulation(boolean simulation);

	public abstract void setSimulationProgress(int etape);

	public abstract void setTcZoom(double tcZoom);

	public abstract void setZoomValue(double zoom);

	public abstract VcdZoom getVcdzoom();

	public abstract void setFocus();

	public abstract FigureCanvasBase getfcb();

	public abstract void setTc(TraceCollector tc);

	public abstract void setVcdModel(VCDDefinitions vcd);

	public abstract void setVcdFactory(VcdFactory factory);

	public abstract void vcdMultiPageEditorRefresh();

	public abstract void print();

	public abstract boolean isContainGhost();

	public abstract int getActivePageID();

	public abstract VcdMenu getVcdmenu();

}