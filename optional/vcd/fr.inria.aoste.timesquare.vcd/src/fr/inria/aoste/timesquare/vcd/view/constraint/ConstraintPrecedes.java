/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class ConstraintPrecedes extends AbsConstraint implements IConstraint {
	
	public ConstraintPrecedes() {
		super();
		testn= 1;
	}

	public int draw(Draw1 fig, String currentclock) {
		try
		{
			precedesFunction(fig, currentclock, cc, mca.colorTickMarker());
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public int drawTableItemConstraints() {
		try {
			isGlobal = true;
			String nameClock = cc.getClock();
			int coeff =1;			
			try
			{
			 coeff = Integer.valueOf(cc.getReferenceClocks().get(1));			
			}
			catch (Throwable e) {
				// TODO: handle exception
			}
			if (haveAllClockVisible()) 
			{
				ArrayList<ExtendFigure> alf= vcdFactory.getNameforfigures().get(nameClock);
				for (int n=0; n<alf.size();n+=coeff)
				{
						IFigure fig =alf.get(n);
					draw((Draw1) fig, nameClock);
						
				}
			} else
				return -1;
			IVcdDiagram vdt = vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints()) {
				Dimension dim = vdt.getCanvas().getContents()
						.getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width,
						bounds.height, true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible = true;
			isGlobal = false;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		} catch (Throwable t) {
			isGlobal = false;
			ErrorConsole.printError(t);
			return -1;
		}
	}
	
	
	private void precedesFunction(IFigure currentfig, String currentclock,IConstraintData cc, Color color) {
		boolean state= true;
		if (cc.getClock().compareTo(currentclock)!=0)
			state =false;
		
		int startindex = -1;
		int stopindex = -1;
		int startindex2 = -1;
		int stopindex2 = -1;
		IVcdDiagram vdt = vcdFactory.getVcddia();
		ArrayList<ExtendFigure> clk1 = vdt.getVcdFactory().getNameforfigures().get(	cc.getClock());
		ArrayList<ExtendFigure> clk2 = vdt.getVcdFactory().getNameforfigures().get(	cc.getReferenceClocks().get(0));
		int index = 0;
		int coeff =1;
		int coeff2 =1;
		try
		{
			coeff = Integer.valueOf(cc.getReferenceClocks().get(1));
			coeff2 = Integer.valueOf(cc.getReferenceClocks().get(2));
		}
		catch (Throwable e) {
			
		}
		int group = 0;

		if (state) {
			index = clk1.indexOf(currentfig) + 1;
			group = (index - 1) / coeff;

		} else {
			index = clk2.indexOf(currentfig) + 1;
			group = (index - 1) / coeff2;
		}

		startindex = group * coeff;
		stopindex = (group + 1) * coeff - 1;
		startindex2 = group * coeff2;
		stopindex2 = (group + 1) * coeff2 - 1;

		if (clk2.size()<= startindex2){
			return ;
		}
	
		if (clk1.size()<= stopindex){
			return ;
		}
		
		IFigure start = clk1.get(startindex);
		IFigure stop = clk1.get(stopindex);
		IFigure start2 = clk2.get(startindex2);
		
		ConstraintsConnection poly2 = icc.constructDashConnection(mca.colorWhiteArrow(), (Draw1) stop, (Draw1)start2, false);
		poly2.setGlobal(isGlobal);
		poly2.setComment(cc);
		icc.addToList(list.getListConstraints(), poly2);
		
		if (clk2.size()<= stopindex2){
			return ;
		}
		
		IFigure stop2 = clk2.get(stopindex2);

		ArrayList<ConstraintsConnection> conn = icc.constructPacket(color, (Draw1)start,(Draw1)stop);
		ArrayList<ConstraintsConnection> conn2 = icc.constructPacket(color,(Draw1) start2,(Draw1)stop2);

		for (ConstraintsConnection c : conn) {
			c.setGlobal(isGlobal);
			c.setComment(cc);
			icc.addToList(list.getListConstraints(), c);
		}
		for (ConstraintsConnection c : conn2) {
			c.setGlobal(isGlobal);
			c.setComment(cc);
			icc.addToList(list.getListConstraints(), c);
		}
	}

	@Override
	public int drawOneTick(int ticknum, String clockId) {
		// TODO Auto-generated method stub
		return 0;
	}

}
