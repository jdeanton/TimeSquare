/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.vcd.model.Comment;
import fr.inria.aoste.timesquare.vcd.model.Function;
import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.view.constraint.ConstraintAlternates;
import fr.inria.aoste.timesquare.vcd.view.constraint.ConstraintFilteredBy;
import fr.inria.aoste.timesquare.vcd.view.constraint.ConstraintPrecedes;
import fr.inria.aoste.timesquare.vcd.view.constraint.ConstraintSampledOn;
import fr.inria.aoste.timesquare.vcd.view.constraint.ConstraintSustains;
import fr.inria.aoste.timesquare.vcd.view.constraint.ConstraintSync;
import fr.inria.aoste.timesquare.vcd.view.constraint.IConstraint;

public class ConstraintDecodeComment implements IDecodeComment {

	public ConstraintCommentCommand create(Comment st) {
		try {
			return createConstraintCommentCommand(st.getComment().substring(11), st, null);
		} catch (Throwable ex) {}
		return null;
	}

	public static ConstraintCommentCommand filteredBy(String superc,String sub, String bw) {
		IConstraint ic = null;
		String clock = null;// new String();
		ArrayList<String> clocks = new ArrayList<String>();
		String comment = sub + " = " + superc + " filteredBy " + bw;
		clock = sub;
		clocks.add(superc);
		ic = new ConstraintFilteredBy();
		ConstraintCommentCommand cc = new ConstraintCommentCommand(comment,clock, Function._filteredby, clocks, null);
		cc.setIc(ic);
		ic.setCc(cc);
		return cc;
	}

	public static ConstraintCommentCommand synchronize(String left,String right, int a, int b) {
		IConstraint ic = null;
		String clock = null;
		ArrayList<String> clocks = new ArrayList<String>();
		String comment = left + " by " + a + " synchronizeswith " + right + " by " + b;
		clock = left;
		clocks.add(right);
		clocks.add("" + a);
		clocks.add("" + b);
		ic = new ConstraintSync();
		ConstraintCommentCommand cc = new ConstraintCommentCommand(comment , clock, Function._synchronizeswith, clocks, null);
		cc.setIc(ic);
		ic.setCc(cc);
		return cc;
	}

	public static ConstraintCommentCommand isperiodicOn(String left , String right) {
		IConstraint ic = null;
		String clock =null;
		ArrayList<String> clocks = new ArrayList<String>();
		String comment = left + " isPeriodicOn " + right;
		clock = left;
		clocks.add(right);
		ic = new ConstraintFilteredBy();
		ConstraintCommentCommand cc = new ConstraintCommentCommand(comment , clock, Function._isperiodicon, clocks, null);
		cc.setIc(ic);
		ic.setCc(cc);
		return cc;
	}

	public static ConstraintCommentCommand sampledOn(String superc, String sub,String trig, boolean weakly){
		ConstraintSampledOn ic = null;
		String clock = null;
		ArrayList<String> clocks = new ArrayList<String>();
		String mod = weakly ? " weakly " : " strictly ";
		Moderator md = weakly ? Moderator.weakly : Moderator.strictly;
		String comment = sub + " = " + superc + mod + " sampledOn " + trig;
		clock = sub;
		clocks.add(superc);
		ic = new ConstraintSampledOn();
		ic.setWeakly(weakly);
		ConstraintCommentCommand cc = new ConstraintCommentCommand(comment,clock, Function._filteredby, clocks, md);
		cc.setIc(ic);
		ic.setCc(cc);
		return cc;
	}

	
	public ICommentCommand createEmpty() {
		return new ConstraintCommentCommand();
	}

	public static ConstraintCommentCommand createConstraintCommentCommand(String comment, IComment st, ConstraintCommentCommand old) {
		try {
			IConstraint ic = null;
			String clock = "";
			Function function = null;
			ArrayList<String> clocks = new ArrayList<String>();
			Moderator md = null;
			if (st.nbParts() <= 2) {
				return null;
			}

			if (st.get(2).toLowerCase().equals(Function._alternateswith.toString())) {
				function = Function._alternateswith;
				clock = st.get(1);
				clocks.add(st.get(3));
				if (st.nbParts() == 6) {
					clocks.add(st.get(4));
					clocks.add(st.get(5));
				}
				md = Moderator.strictly;
				ic = new ConstraintAlternates();
			}
			if (st.get(2).toLowerCase().equals(Function._isperiodicon.toString())
					|| (st.get(2).toLowerCase().equals("periodicon"))) {
				function = Function._isperiodicon;
				clock = st.get(1);
				clocks.add(st.get(3));
				ic = new ConstraintFilteredBy();
			}
			if (st.nbParts()>3){
				if (st.get(3).toLowerCase().equals(Function._alternateswith.toString())) {
					function = Function._alternateswith;
					clock = st.get(1);
					clocks.add(st.get(4));
					if (st.nbParts() == 7) {
						clocks.add(st.get(5));
						clocks.add(st.get(6));
					}
					md = Moderator.getModerator(st.get(2), Moderator.leftWeakly);
					ic = new ConstraintAlternates();
				}
			}
			if (st.nbParts()>3){
				if (st.get(3).toLowerCase().equals(Function._sustains.toString())) {
					function = Function._sustains;
					clock = st.get(1);
					clocks.add(st.get(4));
					clocks.add(st.get(6));
					ic = new ConstraintSustains();
				}
			}
			if (st.get(2).equals("<")) {
				function = Function._precedes;
				clock = st.get(1);
				clocks.add(st.get(3));
				ic = new ConstraintPrecedes();
			}
			if (st.nbParts()>3){
				if (st.get(3).equals("<")) {
					function = Function._precedes;
					clock = st.get(1);
					clocks.add(st.get(4));
					ic = new ConstraintPrecedes();
				}
			}
			if (st.get(2).toLowerCase().equals(Function._precedes.toString())) {
				function = Function._precedes;
				clock = st.get(1);
				clocks.add(st.get(3));
				if (st.nbParts() == 6) {
					clocks.add(st.get(4));
					clocks.add(st.get(5));
				} else {
					clocks.add(st.get(5));
					clocks.add(st.get(6));
				}
				ic = new ConstraintPrecedes();
			}
			if (st.nbParts()>3){
				if (st.get(3).toLowerCase().equals(Function._precedes.toString())) {
					function = Function._precedes;
					clock = st.get(1);
					clocks.add(st.get(4));
					clocks.add(st.get(6));
					clocks.add(st.get(7));
					ic = new ConstraintPrecedes();
				}
			}

			if (st.nbParts() >= 5) {
				if (st.get(4).toLowerCase().equals(Function._sampledon.toString())) {
					function = Function._sampledon;
					clock = st.get(1);
					clocks.add(st.get(3));
					clocks.add(st.get(5));
					ic = new ConstraintSampledOn();
				}
				if (st.get(3).toLowerCase().equals(Function._oneshoton.toString())) {
					function = Function._oneshoton;
					clock = st.get(1);
					clocks.add(st.get(4));
					clocks.add(st.get(5));
					clocks.add(st.get(6));
					clocks.add(st.get(7));
				}
				if (st.get(4).toLowerCase().equals(Function._restrictedto.toString())) {
					function = Function._restrictedto;
					clock = st.get(1);
					clocks.add(st.get(3));
				}
				if (st.get(4).toLowerCase().equals(Function._filteredby.toString())) {
					function = Function._filteredby;
					clock = st.get(1);
					clocks.add(st.get(3));
					ic = new ConstraintFilteredBy();
				}
				if (st.get(4).toLowerCase().equals(Function._delayedfor.toString())) {
					function = Function._delayedfor;
					clock = st.get(1);
					clocks.add(st.get(3));
					clocks.add(st.get(7));
				}
				if (st.get(4).toLowerCase().equals(Function._synchronizeswith.toString())) {
					function = Function._synchronizeswith;
					clock = st.get(1);
					clocks.add(st.get(5));
					clocks.add(st.get(3));
					clocks.add(st.get(7));
					ic = new ConstraintSync();
				}

			}
			if (st.nbParts() >= 6) {
				if (st.get(5).toLowerCase().equals(Function._sampledon.toString())) {
					function = Function._sampledon;
					clock = st.get(1);
					clocks.add(st.get(3));
					clocks.add(st.get(6));
					ic = new ConstraintSampledOn();
					((ConstraintSampledOn) ic).setWeakly(!st.get(4).toLowerCase()
							.equals("strictly"));
				}
				if (st.get(5).toLowerCase().equals(Function._synchronizeswith.toString())) {
					function = Function._synchronizeswith;
					clock = st.get(1);
					clocks.add(st.get(6));
					clocks.add(st.get(3));
					clocks.add(st.get(8));
					ic = new ConstraintSync();
				}
			}
			if (function != null) {
				ConstraintCommentCommand cc=null;
				if ( old==null ){
					cc = new ConstraintCommentCommand(	comment, clock, function, clocks, md);
				}
				else{
					cc= old;
					cc.setComment(comment);
					cc.setClock(clock);			
					cc.setFunction(function);
					cc.setReferenceClocks(clocks);
					cc.setModerator(md);
				}
				cc.setIc(ic);
				if (ic != null) {
					ic.setCc(cc);
				}
				return cc;
			}
			return  null;
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return  null;
	}

}
