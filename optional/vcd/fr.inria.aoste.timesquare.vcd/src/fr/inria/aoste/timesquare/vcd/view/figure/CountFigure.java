/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.figure;


public class CountFigure {
	static int n=0;
	static int dec=0;
	static int inc=0;
	static public void inc(){
		n++;
		if (n %1000== 0){}
		inc++;
		if (inc %1000== 0){}
	}
	
	static public void dec(){
		n--;
		if (n %1000== 0 ){}
		dec++;
		if (dec %1000== 0){}
	}
	
	static public void  disp(){}
	
	static public int countvalue(){
		return n;
	}
}
