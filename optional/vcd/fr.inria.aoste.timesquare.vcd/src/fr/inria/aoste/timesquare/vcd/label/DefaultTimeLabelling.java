/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.label;

import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.keyword.TimeUnit;

public class DefaultTimeLabelling implements ITimeLabelling {

	VCDDefinitions _vcdDefinitions=null;
	
	@Override
	public String getTimeMarker(double x) {
		TimeUnit tu=null;
		if (_vcdDefinitions.getTimeScaleCommand() != null) {
			tu = _vcdDefinitions.getTimeScaleCommand().getUnit();
		}
		return TimeUnit.getTime(x, tu);
	}

	@Override
	public String getRulerTimeIndice(int x) {
		return Integer.toString(x);
	}

	public VCDDefinitions getVcdDefinitions() {
		return _vcdDefinitions;
	}

	public void setVcdDefinitions(VCDDefinitions vcdDefinitions) {
		this._vcdDefinitions = vcdDefinitions;
	}

}
