/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

public enum Function {
	_filteredby, _alternateswith, _synchronizeswith, _sampledon, _oneshoton, _sustains, _delayedfor, _precedes, _union, _isperiodicon, _restrictedto;

	@Override
	public String toString() {
		return super.toString().substring(1);
	}

	public static Function fromString(String s) {
		if (s.compareTo("periodicon") == 0){
			return _isperiodicon;
		}
		return valueOf("_" + s.toLowerCase());
	}
}
