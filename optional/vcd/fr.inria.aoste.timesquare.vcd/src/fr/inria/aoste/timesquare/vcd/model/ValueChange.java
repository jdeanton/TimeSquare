/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

/**
 * Abstract base class of all the classes used to represent values of VCD variables at a single
 * instant.
 * @author bferrero
 * @author mzc
 *
 */
public abstract class ValueChange {
	
	private String identifierCode;

	protected ValueChange(String ident) {
		this.identifierCode = ident;
	}

	public abstract Object getValue();

	public abstract void setValue(Object value);

	@Override
	public String toString() {
		return identifierCode;
	}
	
	@Override
	public int hashCode() {		
		return identifierCode.hashCode();
	}

	@Override
	 public boolean equals(Object o) {
		if (!(o instanceof ValueChange)){
			return false;
		}
		if (this == o){
			return true;
		}
		if (identifierCode!=null){
			if (identifierCode.compareTo(((ValueChange) o).identifierCode)==0){
				return true;
			}
		}else{
			if (((ValueChange) o).identifierCode==null){
				return true ;
			}
		}		
		return false;
	}

	static public ValueChange fromString(String v) {
		v = v.trim();
		int iSpace = v.indexOf(' ');
		if (iSpace == -1) {
			return new ScalarValueChange(Value.fromChar(v.charAt(0)), v
					.substring(1));
		}
		String value = v.substring(0, iSpace);
		String ident = v.substring(iSpace + 1);
		return create(value, ident);
	}

	static private ValueChange create(String value, String code) {
		char c = value.charAt(0);
		value = value.substring(1);
		switch (c) {
		case 'b':
		case 'B':
			return new BinaryVectorValueChange(value.substring(1), code);
		case 'r':
		case 'R':
			return new RealVectorValueChange(value.substring(1), code);
		default:
			throw new IllegalArgumentException("<"+value + ";" + code+">");
		}
	}

	final public String getIdentifierCode() {
		return identifierCode;
	}
}
