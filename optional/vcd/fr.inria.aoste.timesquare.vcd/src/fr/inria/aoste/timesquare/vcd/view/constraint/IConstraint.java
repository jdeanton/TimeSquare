/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.constraint;

import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.IConstructContraint;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;

public interface IConstraint {

	/**
	 * @return the vcdFactory
	 */
	VcdFactory getVcdFactory();

	/**
	 * @param vcdFactory
	 *            the vcdFactory to set
	 */
	void setVcdFactory(VcdFactory vcdFactory);
	 
	

	/**
	 * @return the cc
	 */ 
	public IConstraintData getCc();

	/**
	 * @param cc
	 *            the cc to set
	 */
	public void setCc(IConstraintData cc);

	/**
	 * @return the list
	 */
	public ListConnections getList();

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(ListConnections list);

	int draw(Draw1 currentfig, String currentclock);
	
	int drawOneTick(int ticknum, String clockId);

	/**
	 * @return the number of tick of the clock
	 */
	int drawTableItemConstraints();

	boolean getIsConstraintVisible();

	void setIsConstraintVisible(boolean b);

	/**
	 * @return the icc
	 */
	public IConstructContraint getIcc();

	/**
	 * @param icc
	 *            the icc to set
	 */
	public void setIcc(IConstructContraint icc);

	public boolean haveAllClockVisible();
	
	public VcdColorPreferences getMca() ;

	public void setMca(VcdColorPreferences mca) ;

}