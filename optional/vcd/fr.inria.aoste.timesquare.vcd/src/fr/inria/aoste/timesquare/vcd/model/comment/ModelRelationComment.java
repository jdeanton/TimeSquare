/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import org.eclipse.core.runtime.IPath;

import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.UpdateCommand;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

public class ModelRelationComment implements ICommentCommand {
	
	private IPath _path;

	public void setPath(IPath path) {
		this._path = path;
	}

	public IPath getPath() {
		return _path;
	}

	public String getType() {
		return null;
	}

	public Uposition getUp() {
		return null;
	}

	public boolean isActive() {
		return false;
	}

	public void setActive(boolean active) {}

	public void setClockLink(String name) {}

	public int setClockListener(UpdateCommand clklst) {
		return 0;
	}

	public void setString(int n, String s) {}

	public void setUp(Uposition up) {}

	public int update(String newcommentvalue, IComment updatevalue) {
		return 0;
	}

	public int validate(VCDDefinitions vcddef) {
		return 0;
	}

	public void validate() {}

	public int clear() {
		return 0;
	}

	public void visit(IDeclarationVisitor visitor) {}

	public void newData(Object o) {}

}
