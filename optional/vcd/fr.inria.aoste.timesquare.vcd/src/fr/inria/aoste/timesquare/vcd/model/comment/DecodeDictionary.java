/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.runtime.IConfigurationElement;

import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;

public class DecodeDictionary implements IExtensionManager {

	private static DecodeDictionary _decode;

	public synchronized static final DecodeDictionary getDefault() {
		if (_decode == null)
			_decode = new DecodeDictionary();
		return _decode;
	}

	private DecodeDictionary() {
		ExtensionPointManager.findAllExtensions(this);
		disp();
	}

	@Override
	final public String getExtensionPointName() {
		return "fr.inria.aoste.timesquare.vcd.vcdPE";
	}

	@Override
	final public String getPluginName() {
		return "fr.inria.aoste.timesquare.vcd";
	}

	public enum LinkComment{
		clock, all, tick , global , time ;
	}

	private static class InfoComment{
		
		private String name;
		private IDecodeComment idc;
		ArrayList<LinkComment> lslc = new ArrayList<LinkComment>();
		
		InfoComment(String name, IDecodeComment idc) {
			super();
			this.name = name;
			this.idc = idc;
		}
		
		@Override		
		public String toString() {
			StringBuilder sb= new StringBuilder(name);
			try
			{
				sb.append(" ").append(idc.getClass().getName());
				String separator = " [";
				for (LinkComment lk:lslc){
					sb.append(separator);
					sb.append(lk);
					separator = ", ";
				}
				sb.append("]");
			}
			catch (Throwable e) {
				sb.append("!!");
			}
			return sb.toString();
		}


	}

	private HashMap<String, InfoComment> _hmsdc = new HashMap<String, InfoComment>();
	public IDecodeComment getDecoder(String s) {
		try
		{
			if (s==null)
				return null;
			if (_hmsdc==null)
				return null;
			InfoComment ic= _hmsdc.get(s.toLowerCase());
			if (ic==null)
				return null;
			return ic.idc;
		}
		catch (Throwable e) {
			return null;
		}
	}
	
	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable{
		Class<? extends IDecodeComment> icdclass = ExtensionPointManager.getPointExtensionClass(ice, "class", IDecodeComment.class);
		String pragma = ice.getAttribute("pragma");
		InfoComment ifc= new InfoComment(pragma, icdclass.newInstance());
		for (IConfigurationElement ice2: ice.getChildren()){
			attribute(ice2,ifc.lslc);
		}
		_hmsdc.put(pragma, ifc);			
	}

	private void attribute(IConfigurationElement ice , ArrayList<LinkComment> allkc ){
		String s=ice.getName();
		LinkComment lk= LinkComment.valueOf(s);
		if (lk!=null)
			allkc.add(lk);
	}

	public boolean isClockExtension(String id){
		InfoComment ifc= _hmsdc.get(id.toLowerCase());
		return ifc.lslc.contains(LinkComment.clock);
	}

	public boolean isTimeExtension(String id){
		InfoComment ifc= _hmsdc.get(id.toLowerCase());
		return ifc.lslc.contains(LinkComment.time);
	}


	public int addPragma(String s, IDecodeComment _idc, LinkComment alk[]){
		InfoComment ifc= new InfoComment(s, _idc);
		for (LinkComment lk:alk){
			ifc.lslc.add(lk);
		}		
		_hmsdc.put(s, ifc);
		return 0;
	}

	private void disp(){
		System.out.println("VCD comment " + Integer.toHexString(hashCode()));
		for (String s : _hmsdc.keySet()){
			System.out.println("\t" +s +": " +_hmsdc.get(s).toString());
		}
	}
}
