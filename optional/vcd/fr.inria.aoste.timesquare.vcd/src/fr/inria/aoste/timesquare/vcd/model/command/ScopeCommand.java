/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.command;

import java.util.ArrayList;
import java.util.HashMap;

import fr.inria.aoste.timesquare.vcd.model.ClockListener;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.IScopeCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.ScopeType;
import fr.inria.aoste.timesquare.vcd.model.visitor.IScopeVisitor;

public class ScopeCommand implements IScopeCommand {
	
	private ScopeType _scopeType;
	private String _name;
	private ArrayList<IScopeCommand> _children;
	private HashMap<String , Object> _hm= new HashMap<String, Object>();
	ClockListener _clockListener;
	
	public ScopeCommand(ScopeType type, String name) {
		super();
		this._scopeType = type;
		this._name = name;
		this._children = new ArrayList<IScopeCommand>();
	}

	public void addChild(IScopeCommand child) {
		this._children.add(child);
		if (_clockListener!=null){
			if (child instanceof ScopeCommand){
				_clockListener.add((ScopeCommand) child);
			}else{			
				_clockListener.newvar(child);
			}
		}
			
	}

	public void visit(IScopeVisitor visitor) {
		visitor.visitScope(_scopeType, _name);
		for (IScopeCommand sc : _children){
			sc.visit(visitor);
		}
		visitor.visitUpscope();
	}
	
	 public void setClockListener(ClockListener clockListener){
		 _clockListener=clockListener;
		 for (IScopeCommand child : _children){
			 if (child instanceof ScopeCommand){
				 _clockListener.add((ScopeCommand) child);
			 }else{
				 _clockListener.newvar(child);
			 }
		 }
	 }
	
	 public int listenerAddingCommand(ICommentCommand icc){
		 for (IScopeCommand child : _children){
			 if (child instanceof VarCommand){
				 ((VarCommand)	child).newCommentCommand(icc);
				 }
		 }
		 return 0;
	 }
	 
	public Object getData(String s) {
		return _hm.get(s);
	}

	public Object setData(String s, Object o) {
		if ( s.compareTo("alias")==0){
			System.out.println("alias");
		}
		return _hm.put(s, o);
	}

	public ArrayList<IScopeCommand> getChilds() {
		return _children;
	}
}
