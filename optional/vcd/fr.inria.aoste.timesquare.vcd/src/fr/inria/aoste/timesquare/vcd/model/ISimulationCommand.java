/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

import fr.inria.aoste.timesquare.vcd.model.visitor.ISimulationCollector;

/**
 * Represents a single time instant of the VCD. Used to store all the ValueChange objects that give
 * the actual value of the variables of the VCD.
 * 
 * @author bferrero
 * @author mzc
 *
 */
public interface ISimulationCommand extends Iterable<ValueChange> {
	
	public void visit(ISimulationCollector visitor);

	public int getTime();
	
	public ISimulationCommand cloneCommand();
}
