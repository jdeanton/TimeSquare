/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

public final class RealVectorValueChange extends VectorValueChange {
	
	private double _value;

	public RealVectorValueChange(String value, String ident) {
		this(Double.valueOf(value), ident);
	}

	public RealVectorValueChange(double value, String ident) {
		super('r', ident);
		this._value = value;
	}

	@Override
	public Object getValue() {
		return this._value;
	}

	@Override
	public void setValue(Object value) {
	}
}
