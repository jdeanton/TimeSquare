/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.visitor;

import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.ScopeType;
import fr.inria.aoste.timesquare.vcd.model.keyword.VarType;

/**
 * An interface for visitors 
 * @author Benoit Ferrero, Anthony Gaudino
 */
public interface IScopeVisitor {
	
	void visitVar(VarCommand command, VarType type, String ref);

	void visitScope(ScopeType type, String name);

	void visitUpscope();
}
