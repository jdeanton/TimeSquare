/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.action;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;

public final class ActionSync extends Action {
	
	private IVcdDiagram vcdMultiPageEditor;
	ConstraintCommentCommand cc;
	Color color;
	
	public ConstraintCommentCommand getCc() {
		return cc;
	}

	public ActionSync(IVcdDiagram vcdMultiPageEditor,ConstraintCommentCommand cc, int style) {
		super(cc.toString(), style);
		this.vcdMultiPageEditor = vcdMultiPageEditor;
		this.cc = cc;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void run() {
		try {
			if (isChecked()) {
				vcdMultiPageEditor.getConstraintsFactory().drawSyncInterval(
						vcdMultiPageEditor.getListConnections().actionForCommentGet(this),
						vcdMultiPageEditor.getListConnections().actionColorGet(this));
			}
			if (!isChecked()) {
				for (IFigure f : vcdMultiPageEditor.getListConnections().getListInterval()
						.get(vcdMultiPageEditor.getListConnections().actionForCommentGet(this))) {
					if (f != null)
						if (vcdMultiPageEditor.getCanvas().getContents() == f
								.getParent())
							vcdMultiPageEditor.getCanvas().getContents()
									.remove(f);
				}
				vcdMultiPageEditor.getListConnections().getListInterval().get(
						vcdMultiPageEditor.getListConnections().actionForCommentGet(this))
						.clear();
			}
		} catch (Throwable t) {
			ErrorConsole.printError(t, "Erreur Sync ... ");
		}
	}
}