/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.menu;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.PreferencesUtil;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.action.ActionSync;
import fr.inria.aoste.timesquare.vcd.action.FindAction;
import fr.inria.aoste.timesquare.vcd.action.OrderAction;
import fr.inria.aoste.timesquare.vcd.action.PrinterAction;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.model.Function;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class VcdMenu {
	private IVcdDiagram _vcdDiagram;
	private Action _refreshAction;
	private GhostAction _showGhostAction, _hideGhostAction, _partialGhostAction;
	private Action _orderAction;
	private Action _vcdOption;
	private FindAction _findAction;
	private PrinterAction _printAction;
	private ArrayList<ActionSync> _syncAction = new ArrayList<ActionSync>();
	ListConnections _listConnections;

	public VcdMenu(IVcdDiagram vcdDiagram) {
		super();
		this._vcdDiagram = vcdDiagram;
		_listConnections = vcdDiagram.getListConnections();
		_showGhostAction = new GhostAction(vcdDiagram, "&Show all ghost",
				IAction.AS_RADIO_BUTTON, 0);
		_showGhostAction.setToolTipText("&Use to show ghost instants");
		_hideGhostAction = new GhostAction(vcdDiagram, "&Hide all ghost",
				IAction.AS_RADIO_BUTTON, 1);
		_hideGhostAction.setToolTipText("&Use to hide ghost instants");
		_partialGhostAction = new GhostAction(vcdDiagram, "&Partial ...",
				IAction.AS_RADIO_BUTTON, 2);
		_partialGhostAction
				.setToolTipText("&Use to show or hide ghost instants");
		_findAction = new FindAction(vcdDiagram);
		_findAction.setText("&Find");
		_printAction = new PrinterAction(vcdDiagram);
		_printAction.setText("&Print");
		setGhostSelected(Mode.hide);
		_orderAction = new OrderAction(vcdDiagram);
		_orderAction.setText("&Ordering ...");
		_orderAction.setToolTipText("&Use to hide show and move clock");
		_vcdOption = getVCDOption();
	}

	public FindAction getFindAction() {
		_findAction.setText("&Find");
		return _findAction;
	}

	public Action getOrderAction() {
		return _orderAction;
	}

	public PrinterAction getPrinterAction() {
		_printAction.setText("&Print");
		return _printAction;
	}

	public Action getVCDOption() {
		if (_vcdOption == null) {
			
			_vcdOption = new Action() {
				@Override
				public void run() {
					try {
						PreferencesUtil
								.createPreferenceDialogOn(
										new Shell(),
										"fr.inria.aoste.timesquare.vcd.preferences.VCDeditorPreferencePage",
										null, null).open();

					} catch (Throwable e) {
						e.printStackTrace();
					}
				}
			};

		}
		_vcdOption.setText("&Vcd Option");
		return _vcdOption;
	}

	public Action getRefreshAction() {		 
		if (_refreshAction == null) {
			
			_refreshAction = new Action() {
				@Override
				public void run() {
					try {
						_vcdDiagram.vcdMultiPageEditorRefresh();
					} catch (Throwable e) {
						e.printStackTrace();
					}
				}
			};
			
		}
		_refreshAction.setText("&Refresh");
		return _refreshAction;
	}

	public Action getShowGhostAction() {
		_showGhostAction.setEnabled(_vcdDiagram.isContainGhost());
		return _showGhostAction;
	}

	public Action getHideGhostAction() {
		_hideGhostAction.setEnabled(_vcdDiagram.isContainGhost());
		return _hideGhostAction;
	}

	public Action getPartialGhostAction() {
		_partialGhostAction.setEnabled(_vcdDiagram.isContainGhost());
		return _partialGhostAction;
	}

	public void setGhostSelected(Mode n) {
		if (n == null){
			return;
		}
		_showGhostAction.setChecked(n.isShow());
		_hideGhostAction.setChecked(n.isHide());
		_partialGhostAction.setChecked(n.isPartial());
	}

	public Mode getGhostSelected() {
		if (_showGhostAction.isChecked()){
			return Mode.show;
		}
		if (_hideGhostAction.isChecked()){
			return Mode.hide;
		}
		if (_partialGhostAction.isChecked()){
			return Mode.partial;
		}
		return null;
	}

	public ArrayList<ActionSync> getSyncAction() {
		VcdColorPreferences mca = _vcdDiagram.getColorPreferences();
		if (mca==null){
			return _syncAction ;
		}
		if (_syncAction.isEmpty()) {
			int i = 0;
			ArrayList<String> clocksNames = new ArrayList<String>();
			Color color = mca.colorLightGraySync();
			for (ConstraintCommentCommand constraintCC : _vcdDiagram.getVcdModel()
					.getConstraintCommentCommand()) {
				if (constraintCC.getFunction() != Function._synchronizeswith){
					continue;
				}
				if ((i % 3) == 0){
					color = mca.colorLightGraySync();
				}
				if ((i % 3) == 1){
					color = mca.colorLightBlueSync();
				}
				if ((i % 3) == 2){
					color = mca.colorRedSync();
				}

				clocksNames.add(constraintCC.getClock());
				_listConnections.getListInterval().put(constraintCC,
						new ArrayList<ConstraintsConnection>());
				_listConnections.firstClock1Put(constraintCC, new ArrayList<IFigure>());
				_listConnections.firstClock2Put(constraintCC, new ArrayList<IFigure>());
				_listConnections.lastClock1Put(constraintCC, new ArrayList<IFigure>());
				_listConnections.lastClock2Put(constraintCC, new ArrayList<IFigure>());
				ActionSync as = new ActionSync(_vcdDiagram, constraintCC, IAction.AS_CHECK_BOX);
				as.setColor(color);
				_syncAction.add(as);
				_listConnections.actionForCommentPut(_syncAction.get(i), constraintCC);
				_listConnections.actionColorPut(_syncAction.get(i), color);
				_syncAction.get(i).setToolTipText(
						"&Use to hide or show synchronized interval");
				i++;
			}
		}
		return _syncAction;
	}

	public int clear() {
		_listConnections = null;
		_vcdDiagram = null;
		_refreshAction = null;
		_showGhostAction = null;
		_hideGhostAction = null;
		_partialGhostAction = null;
		_orderAction = null;
		if (_findAction!=null)
			_findAction.setAccelerator(-1 );
		_findAction = null;
		if (_printAction!=null)
			_printAction.setAccelerator(-1 );
		_printAction = null;
		return 0;
	}
}