/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class ConstraintSustains extends AbsConstraint implements IConstraint {

	public ConstraintSustains() {
		super();
		testn = 2;
	}

	private String nameSustains = null;
	private String nameStart = null;
	private String nameStop = null;
	private ArrayList<ExtendFigure> clockSustain = null;
	private ArrayList<ExtendFigure> clockStart = null;
	private ArrayList<ExtendFigure> clockStop = null;

	public int draw(Draw1 currentFig, String currentclock) {
		int startID = -1, stopID = -1, sustainFirstID = -1, sustainLastID = -1;
		int cas = 0;
		int currentX = currentFig.getBounds().x;
		nameSustains = cc.getClock();
		nameStart = cc.getReferenceClocks().get(0);
		nameStop = cc.getReferenceClocks().get(1);
		clockSustain = vcdFactory.getNameforfigures().get(nameSustains);
		clockStart = vcdFactory.getNameforfigures().get(nameStart);
		clockStop = vcdFactory.getNameforfigures().get(nameStop);
		if (currentclock.equals(nameSustains))
			cas = 1;
		if (currentclock.equals(nameStart))
			cas = 2;
		if (currentclock.equals(nameStop))
			cas = 3;
		switch (cas) {
		case 1: {
			// case of the users click on sustains clock
			startID = icc.lastBeforeweak(nameStart, currentX);
			int startX = clockStart.get(startID).getBounds().x;
			sustainFirstID = icc.firstAfter(nameSustains, startX);
			stopID = icc.firstAfterStrict(nameStop, startX);
			if (stopID == -1) {
				sustainLastID = clockSustain.size() - 1;
				break;
			}
			int stopX = clockStop.get(stopID).getBounds().x;
			sustainLastID = icc.lastBefore(nameSustains, stopX);
			break;
		}
		case 2: {
			// case of the users click on start clock
			startID = clockStart.indexOf(currentFig);
			sustainFirstID = icc.firstAfter(nameSustains, currentX);
			stopID = icc.firstAfterStrict(nameStop, currentX);
			if (stopID == -1) {
				sustainLastID = clockSustain.size() - 1;
				break;
			}
			int stopX = clockStop.get(stopID).getBounds().x;
			sustainLastID = icc.lastBefore(nameSustains, stopX);
			break;
		}
		case 3: {
			// case of the users click on stop clock
			stopID = clockStop.indexOf(currentFig);
			sustainLastID = icc.lastBefore(nameSustains, currentX);
			startID = icc.lastBefore(nameStart, currentX);
			int startX = clockStart.get(startID).getBounds().x;
			sustainFirstID = icc.firstAfter(nameSustains, startX);
			break;
		}
		default:
			break;
		}
		int ret = display(startID, stopID, sustainFirstID, sustainLastID);
		if (ret == -1)
			return -1;
		return 0;
	}

	private int display(int startID, int stopID, int sustainFirstID,
			int sustainLastID) {
		if (startID == -1 || sustainFirstID == -1 || sustainLastID == -1)
			return -1;
		IFigure startFig = clockStart.get(startID);
		IFigure sustainFirstFig = clockSustain.get(sustainFirstID);
		IFigure sustainLastFig = clockSustain.get(sustainLastID);
		Color colorSustain = mca.colorBlueSustain();
		Color colorArraw = mca.colorWhiteArrow();
		ArrayList<ConstraintsConnection> packet = icc.constructPacket(
				colorSustain, (Draw1 )sustainFirstFig,  (Draw1 )sustainLastFig);
		ConstraintsConnection arrowStart = icc.constructDashConnection(
				colorArraw,  (Draw1 ) startFig,  (Draw1 )packet.get(0).getSourceAnchor()
						.getOwner(), false);
		arrowStart.setComment(cc);
		for (ConstraintsConnection c : packet) {
			c.setGlobal(isGlobal);		
			c.setComment(cc);
			icc.addToList(list.getListConstraints(), c);
		}
		arrowStart.setGlobal(isGlobal);
		icc.addToList(list.getListConstraints(), arrowStart);
		if (stopID != -1) {
			IFigure stopFig = clockStop.get(startID);
			ConstraintsConnection arrowStop = icc.constructDashConnection(
					colorArraw,  (Draw1 )packet.get(2).getSourceAnchor().getOwner(),
					 (Draw1 )stopFig, false);
			arrowStop.setComment(cc);
			arrowStop.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), arrowStop);
		}
		return 0;
	}

	public int drawTableItemConstraints() {
		try {
			isGlobal = true;
			String nameClock = cc.getReferenceClocks().get(0);
			if (haveAllClockVisible()) {
				for (IFigure fig : vcdFactory.getNameforfigures()
						.get(nameClock)) {
					draw( (Draw1 )fig, nameClock);
				}
			} else
				return -1;
			IVcdDiagram vdt = vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints()) {
				Dimension dim = vdt.getCanvas().getContents()
						.getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width,
						bounds.height, true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible = true;
			isGlobal = false;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		} catch (Throwable t) {
			ErrorConsole.printError(t);
			isGlobal = false;
			return -1;
		}
	}

	@Override
	public int drawOneTick(int ticknum, String clockId) {
		// TODO Auto-generated method stub
		return 0;
	}
}