/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.listener;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.VcdZoom;

public class WheelListener implements MouseWheelListener {
	
	private IVcdDiagram _vcdDiagram;
	
	public WheelListener(IVcdDiagram vcdDiagram) {
		this._vcdDiagram = vcdDiagram;
	}

	public void mouseScrolled(MouseEvent e) {
		if (_vcdDiagram.isSimulation()){
			return;
		}
		if (!_vcdDiagram.isCtrlKey()){
			return;
		}
		VcdZoom vcdzoom = _vcdDiagram.getVcdzoom();
			
		if (vcdzoom==null){
			return ;
		}
		if (vcdzoom.isLock()){
			return;
		}
		int counter = vcdzoom.getZoomCounter();
		_vcdDiagram.setZoomValue(1);
		int sel = _vcdDiagram.getCanvas().getHorizontalBar().getSelection() - 5;

		if (e.count > 0 && counter < 4) {
			_vcdDiagram.setZoomValue(2);
			_vcdDiagram.setTcZoom(_vcdDiagram.getTraceZoomValue() * 2);
			counter++;
			vcdzoom.setZoomCounter(counter);
			vcdzoom.applyScrollZoom();
//			_vcdDiagram.getfcb().getComposite().getVerticalBar().setSelection(sel*2 + 5);
			_vcdDiagram.getScaleCanvas().getHorizontalBar().setSelection(sel*2 + 5);
			_vcdDiagram.getCanvas().getHorizontalBar().setSelection(sel*2 + 5);
			_vcdDiagram.getScaleCanvas().scrollToX(sel*2 + 5);
			_vcdDiagram.getCanvas().scrollToX(sel*2 + 5);
		} else if (e.count < 0 &&counter > -5){
			_vcdDiagram.setZoomValue(0.5);
			_vcdDiagram.setTcZoom(_vcdDiagram.getTraceZoomValue() * 0.5);
			counter--;
			vcdzoom.setZoomCounter(counter);
			vcdzoom.applyScrollZoom();
//			_vcdDiagram.getfcb().getComposite().getVerticalBar().setSelection(sel/2 + 5);
			_vcdDiagram.getCanvas().getHorizontalBar().setSelection(sel/2 + 5);
			_vcdDiagram.getScaleCanvas().scrollToX(sel/2 + 5);
			_vcdDiagram.getScaleCanvas().scrollToX(sel/2 + 5);
			_vcdDiagram.getCanvas().scrollToX(sel/2 + 5);
		}		
	}

}
