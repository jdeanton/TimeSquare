/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class ConstraintSampledOn extends AbsConstraint implements IConstraint {

	public ConstraintSampledOn() {
		super();
		testn = 2;
	}

	boolean weakly = false;

	int c1 = -1, c2a = -1, c2b = -1, c3 = 1;

	String clk1 = null;
	String clk2 = null;
	String clk3 = null;
	ArrayList<ExtendFigure> figures1 = null;
	ArrayList<ExtendFigure> figures2 = null;
	ArrayList<ExtendFigure> figures3 = null;

	public final boolean isWeakly() {
		return weakly;
	}

	public final void setWeakly(boolean weakly) {
		this.weakly = weakly;
	}

	public int draw(Draw1 currentfig, String currentclock) {
		try {
			c1 = c2a = c2b = c3 = -1;
			int cas = 0;
			clk1 = cc.getClock();
			clk2 = cc.getReferenceClocks().get(0);
			clk3 = cc.getReferenceClocks().get(1);
			figures1 = vcdFactory.getNameforfigures().get(clk1);
			figures2 = vcdFactory.getNameforfigures().get(clk2);
			figures3 = vcdFactory.getNameforfigures().get(clk3);
			if (currentclock.equals(clk1))
				cas = 1;
			if (currentclock.equals(clk2))
				cas = 2;
			if (currentclock.equals(clk3))
				cas = 3;

			int x = ((PolylineConnection) currentfig).getLocation().x;
			if (weakly) {
				switch (cas) {
				case 1:
					c1 = vcdFactory.getNameforfigures().get(clk1).indexOf(
							currentfig);
					c2a = icc.lastBefore(clk2, x);
					c2b = icc.synchrone(clk2, x);
					c3 = icc.lastBefore(clk3, x);
					break;
				case 2:
					c2b = vcdFactory.getNameforfigures().get(clk2).indexOf(
							currentfig);
					c2a = c2b - 1;
					c1 = icc.synchrone(clk1, x);
					c3 = icc.lastBefore(clk3, x);
					break;
				case 3:
					c3 = vcdFactory.getNameforfigures().get(clk3).indexOf(
							currentfig);
					c2a = icc.lastBeforeweak(clk2, x);
					c2b = icc.firstAfter(clk2, x);
					c1 = icc.firstAfter(clk1, x);
					break;
				default:
					break;
				}
			} else {
				switch (cas) {
				case 1:
					c1 = vcdFactory.getNameforfigures().get(clk1).indexOf(
							currentfig);
					c2a = icc.lastBefore(clk2, x);
					c2b = icc.synchrone(clk2, x);
					c3 = icc.lastBefore(clk3, x);
					break;
				case 2:
					c2b = vcdFactory.getNameforfigures().get(clk2).indexOf(
							currentfig);
					c2a = c2b - 1;
					c1 = icc.synchrone(clk1, x);
					c3 = icc.lastBefore(clk3, x);
					break;
				case 3:
					c3 = vcdFactory.getNameforfigures().get(clk3).indexOf(
							currentfig);
					c2a = icc.lastBefore(clk2, x);
					c2b = icc.firstAfterStrict(clk2, x);
					c1 = icc.firstAfter(clk1, x);
					break;
				default:
					break;
				}
			}
			displaySync();
			return 0;
		} catch (Throwable e) {
			return -1;
		}
	}

	private int displaySync() {

		if (c1 != -1 && c2b != -1) {
			ConstraintsConnection poly = icc
					.constructCoincidenceConnection(mca.colorRedCoincidence(),
							(Draw1)figures1.get(c1),(Draw1)figures2.get(c2b));
			poly.setComment(cc);
			poly.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly);
		}
		if (c3 != -1 && c2a != -1 && c1 != -1) {

			ConstraintsConnection poly2 = icc.constructDashConnection(
					mca.colorWhiteArrow(),(Draw1) figures2.get(c2a), (Draw1)figures3
							.get(c3), false);
			poly2.setComment(cc);
			poly2.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly2);
		}
		if (c3 != -1 && c2b != -1 && c1 != -1) {

			ConstraintsConnection poly2 = icc.constructDashConnection(
					mca.colorWhiteArrow(), (Draw1)figures3.get(c3), (Draw1)figures2
							.get(c2b), false);
			poly2.setComment(cc);
			poly2.setGlobal(isGlobal);
			icc.addToList(list.getListConstraints(), poly2);
		}
		return 0;
	}

	public int drawTableItemConstraints() {
		try {
			isGlobal = true;
			String nameClock = cc.getClock();
			if (haveAllClockVisible()) {
				for (IFigure fig : vcdFactory.getNameforfigures()
						.get(nameClock)) {
					draw((Draw1)fig, nameClock);
				}
			} else
				return -1;
			IVcdDiagram vdt = vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints()) {
				Dimension dim = vdt.getCanvas().getContents()
						.getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width,
						bounds.height, true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible = true;
			isGlobal = false;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		} catch (Throwable t) {
			ErrorConsole.printError(t);
			isGlobal = false;
			return -1;
		}
	}

	@Override
	public int drawOneTick(int ticknum, String clockId) {
		// TODO Auto-generated method stub
		return 0;
	}

}
