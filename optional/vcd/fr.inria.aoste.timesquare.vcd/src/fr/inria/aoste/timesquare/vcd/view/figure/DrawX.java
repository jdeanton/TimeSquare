/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.figure;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.Polygon;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;

import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public class DrawX extends Polygon implements ExtendFigure {

	private boolean ghostvisible;
	private int length;
	protected int oldvalue = 0;
	private Point tr = new Point(0, 0);
	private VcdFactory vcd;
	private int x;
	private int y;
	Polyline polyline = null;
	private Level precedeLevel;
	private Level futurLevel;
	
	public DrawX(VcdFactory vcd, int x, int y, int length) {
		super();
		this.vcd = vcd;
		this.y = y;
		this.x = x;
		this.length = length;
		ghostvisible = true;
		CountFigure.inc();
	}

	public int incLength(int l) {
		length += l;
		return length;
	}
	
	public int getLength() {
		return length;
	}

	public Point getTr() {
		return tr;
	}

	public boolean isGhost() {
		return true;
	}

	public boolean isGhostvisible() {
		return ghostvisible;
	}

	public int mycompute() {
		if (ghostvisible) {
			setForegroundColor(vcd.getMca().colorGhostClock());
			setBackgroundColor(vcd.getMca().colorGhostClock());
		} else {
			setForegroundColor(vcd.getMca().colorClock());
			setBackgroundColor(vcd.getMca().colorBlack());
		}
		setLayoutManager(new BorderLayout());
		setOpaque(false);
		setPoints(new PointList());
		int x2 = x + length;
		int mx = x;
		int mx2 = x2;
		if (vcd.getVcdZoom() != null) {
			mx = vcd.getVcdZoom().computeposition(x);
			mx2 = vcd.getVcdZoom().computeposition(x2);
		}
		
		if (ghostvisible) {
			addPoint(new Point(mx, y + vcd.getHeight()).translate(tr));
			addPoint(new Point(mx+1, y + vcd.getHeight()).translate(tr));
			addPoint(new Point(mx+1, y).translate(tr));
			addPoint(new Point(mx2, y).translate(tr));
			addPoint(new Point(mx2, y + vcd.getHeight()).translate(tr));
			addPoint(new Point(mx2+1, y + vcd.getHeight()).translate(tr));
		}else{
			addPoint(new Point(mx, y + vcd.getHeight()).translate(tr));
			addPoint(new Point(mx2, y + vcd.getHeight()).translate(tr));
		}
		if (futurLevel == Level.z) {
			addPoint(new Point(mx2, y + vcd.getHalfHeight()).translate(tr));
			addPoint(new Point(mx2, y + vcd.getHeight()).translate(tr));
		}
		setFill(true);
		if (ghostvisible) {
			if (polyline != null){
				remove(polyline);
			}
			polyline = new Polyline();
			polyline.setPoints(new PointList());
			polyline.setForegroundColor(vcd.getMca().colorClock());
			polyline.addPoint(new Point(mx, y + vcd.getHeight()).translate(tr));
			polyline.addPoint(new Point(mx2, y + vcd.getHeight()).translate(tr));
			polyline.setOpaque(false);
			add(polyline);
		}
		return 0;
	}

	public void setGhostvisible(boolean ghostvisible) {
		this.ghostvisible = ghostvisible;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void setoldValue(int n) {
		oldvalue = n;
	}

	public void setTr(Point tr) {
//		tr.x = tr.x - 40;
		this.tr = tr;
	}

	public Level getLevel() {
		return Level.x;
	}

	public final Level getPrecedeLevel() {
		return precedeLevel;
	}

	public final void setPrecedeLevel(Level precedeLevel) {
		this.precedeLevel = precedeLevel;
	}

	public final Level getFuturLevel() {
		return futurLevel;
	}

	public final void setFuturLevel(Level futurLevel) {
		this.futurLevel = futurLevel;
	}
	
	@Override
	protected void finalize() throws Throwable {
		CountFigure.dec();
		super.finalize();
	}
}
