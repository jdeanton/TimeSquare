/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.command;

import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.view.VcdValueFactory;

final public class Var implements IVar {
	
	private VarCommand _varC;
	private String _parent;
	private VcdValueFactory _vcdValueFactory;
	private boolean visibleByDefault = true;
	
	public Var(VarCommand varC, String parent) {
		super();
		this._varC = varC;
		this._parent = parent;
	}

	public String getName() {
		return _varC.getName();
	}
	
	public String getAliasName(){
		return _varC.getAlias();
	}

	public String getQualifiedName() {
		return _parent + ":" + this.getName();
	}

	public int getSize() {
		return _varC.getSize();
	}

	@Override
	public String toString() {
		return "var " + getSize() + " " + getQualifiedName();
	}

	public String getIdentiferCode() {
		return _varC.getIdentifierCode();
	}


	public VcdValueFactory getValueFactory() {
		return _vcdValueFactory;
	}

	public void setValueFactory(VcdValueFactory vin) {
		_vcdValueFactory = vin;
	}


	public boolean isVisibleByDefault() {

		return visibleByDefault;
	}

	public void setVisibleByDefault(boolean bool) {
		visibleByDefault = bool;
	}

	public int clear() {
		if (_vcdValueFactory != null) {
			_vcdValueFactory.clear();
		}
		_varC = null;				// NOPMD.NullAssignment
		_vcdValueFactory = null;	// NOPMD.NullAssignment
		return 0;
	}

	
	public Object getData(String s) {
		return _varC.getData(s);
	}

	
	public Object setData(String s, Object o) {	
		return _varC.setData(s, o);
	}

	
	public VarCommand getVarCommand() {
		return _varC;
	}
}
