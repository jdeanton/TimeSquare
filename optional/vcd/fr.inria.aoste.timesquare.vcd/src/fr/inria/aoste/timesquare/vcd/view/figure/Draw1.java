/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.figure;

import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;

import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;
import fr.inria.aoste.timesquare.vcd.relation.extensionpoint.Instant;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public class Draw1 extends PolylineConnection implements ExtendFigure {

	private int length;
	protected int oldvalue = 0;
	private Point tr = new Point(0, 0);
	private  VcdFactory vcd;
	private final int x;
	private final int y;
	Instant instant;
	private Level precedeLevel;
	private Level futurLevel;

	public Draw1(VcdFactory vcd, int x, int y, int length) {
		super();
		this.vcd = vcd;
		this.x = x;
		this.y = y;
		this.length = length;
		CountFigure.inc();
	}

	public int incLength(int l) {
		length += l;
		return length;
	}

	public int getLength() {
		return length;
	}

	public Point getTr() {
		return tr;
	}

	public boolean isGhost() {
		return false;
	}

	public int mycompute() {
		double zoom = 1.0;
		int begin = 0;
		setPoints(new PointList());
		removeAllPoints();
		setOpaque(false);
		setForegroundColor(vcd.getMca().colorClock());
		int mx = (int) (x * zoom);
		if (vcd.getVcdZoom() != null) {
			mx = vcd.getVcdZoom().computeposition(x);
		}
		if ((precedeLevel == Level.l0) || (precedeLevel == Level.x)){
			addPoint(new Point(mx, begin + y + vcd.getHeight()).translate(tr));
		}
		if (precedeLevel == Level.z){
			addPoint(new Point(mx, y + vcd.getHalfHeight()).translate(tr));
		}
		addPoint(new Point(mx, begin + y).translate(tr));
		int x2 = x + length;
		int mx2 = (int) (x2 * zoom);
		if (vcd.getVcdZoom() != null) {
			mx2 = vcd.getVcdZoom().computeposition(x2);
		}

		addPoint(new Point(mx2, begin + y).translate(tr));
		if (futurLevel == Level.z){
			addPoint(new Point(mx2, y + vcd.getHalfHeight()).translate(tr));
		}else{
			addPoint(new Point(mx2, begin + y + vcd.getHeight()).translate(tr));
		}
		return 0;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void setoldValue(int n) {
		oldvalue = n;
	}

	public void setTr(Point tr) {
//		tr.x = tr.x - 40;
		this.tr = tr;
	}

	public Level getLevel() {
		return Level.l1;
	}

	public final Level getPrecedeLevel() {
		return precedeLevel;
	}

	public final void setPrecedeLevel(Level precedeLevel) {
		this.precedeLevel = precedeLevel;
	}

	public final Level getFuturLevel() {
		return futurLevel;
	}

	public final void setFuturLevel(Level futurLevel) {
		this.futurLevel = futurLevel;
	}

	@Override
	protected void finalize() throws Throwable {
		CountFigure.dec();	
		super.finalize();
	}
	
	public void linktoTick(VarCommand _vc, int _tick)
	{
		String clockXmiId = "";
		Object atick =_vc.getTickData(_tick , "tick");
		if (atick instanceof Instant){
			this.instant= (Instant) atick ;
		}else{
			Object obj=_vc.getData("xmi");
			if (obj instanceof String){
				clockXmiId = ((String)obj).substring(((String)obj).indexOf("#")+1);
			}
			this.instant= new Instant(clockXmiId, _tick);
			this.instant.setVr(_vc);
		}
	}

	public Instant getInstant() {
		return instant;
	}
	 
}
