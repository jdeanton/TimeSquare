/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.listener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.widgets.ToolItem;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.model.Description;

public class MouseMoveListenerVCD implements MouseMoveListener {

	private IVcdDiagram vcd = null;

	public MouseMoveListenerVCD(IVcdDiagram vcd) {
		super();
		this.vcd = vcd;
	}

	public void mouseMove(MouseEvent e) {
		try
		{			
			int xscroll = vcd.getfcb().getClockCanvas().getHorizontalBar().getSelection();
			int yscroll = vcd.getfcb().getClockCanvas().getVerticalBar().getSelection();
			IFigure fig = vcd.getfcb().getClockCanvas().getContents().findFigureAt(
					e.x + xscroll, e.y + yscroll);
			if (fig != null) {
				if (fig instanceof PolylineConnection || fig instanceof Label) {
					vcd.getConstraintsFactory().showConstraint(fig);
					if (fig instanceof PolylineConnection) {
						Description descr = vcd.getVcdFactory()
								.getFigureForDescription(fig);
						if (descr != null){
							if (descr.getDescription() != null) {
								if (descr.getIndex() >= 0) {
									vcd.getfcb().getClockCanvas().setToolTipText(
											descr.getAlias() + "[" 
													+ (descr.getIndex() +1 )+ "]");
								}
							}
						}
					}
					if (fig instanceof ConstraintsConnection) {
						ConstraintsConnection poly = (ConstraintsConnection) fig;
						if (poly.getComment() != null) {
							ToolItem item;
							if (0 >= vcd.getToolbar().getItems().length) {
								new ToolItem(this.vcd.getToolbar(), SWT.PUSH);
							}
							item = vcd.getToolbar().getItems()[0];
							item.setText(poly.getComment().toString());
						}
					}
				} else {
					vcd.getfcb().getClockCanvas().setToolTipText(null);
					vcd.getConstraintsFactory().hideConstraints();
					vcd.getConstraintsFactory().hideCoincidence();
				}
			}
		}
		catch (Throwable e2) {}
		
	}
}