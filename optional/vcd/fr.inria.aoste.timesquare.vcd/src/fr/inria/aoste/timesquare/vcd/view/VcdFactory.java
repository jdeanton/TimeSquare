/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.Polygon;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Font;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.VcdZoom;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.Timeline;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

final public class VcdFactory {
	
	static final private int _halfHeight = 10, _height = 20, dY = 5, nameWidth = 0;
	static final private int y = 5;
	private int _endTime = -1;
	private VcdZoom _vcdZoom = null;
	private IVcdDiagram _vcdDiagram = null;
	private Timeline _timeline ;

	public Timeline getTimeline() {
		return this._timeline;
	}
	
	public void setTimeline(Timeline tl) {
		this._timeline = tl;
	}
	
	public final int getHalfHeight() {
		return _halfHeight;
	}

	public final int getNameWidth() {
		return nameWidth;
	}

	public final int getHeight() {
		return _height;
	}
	
	private Panel _timelinePanel;
	private Panel _backPanel;
	private Panel namesP;
	private Panel _picture;
	private Rectangle _backPanelBounds = new Rectangle();
	private HashMap<ExtendFigure, Description> figurefordescription = new HashMap<ExtendFigure, Description>();
	private HashMap<String, ArrayList<ExtendFigure>> nameforfigures = new HashMap<String, ArrayList<ExtendFigure>>();
	private ArrayList<RectangleFigure> ghostList = new ArrayList<RectangleFigure>();
	private ConstraintsFactory _constraintsFactory;
	private MarkerFactory _markerFactory;
	private VcdColorPreferences _mca = null;
	
	/**
	 * @author mzc
	 */
	private HashMap<String, EventPanel> _clockPanelsXmiMap = new HashMap<String, EventPanel>();
	
	private HashMap<String, Eventline> _clocklinesXmiMap = new HashMap<String, Eventline>();
	
	private HashMap<String, EventPanel> _clockPanelsMap = new HashMap<String, EventPanel>();
	
	private HashMap<String, Eventline> _clocklinesMap = new HashMap<String, Eventline>();
	
	public VcdColorPreferences getMca() {
		return _mca;
	}

	
	public HashMap<String, Eventline> getClocklinesXmiMap(){
		return _clocklinesXmiMap;
	}
	
	public HashMap<String, EventPanel> getClockPanelsXmiMap(){
		return _clockPanelsXmiMap;
	}
	
	public HashMap<String, Eventline> getClocklinesMap(){
		return _clocklinesMap;
	}
	
	public HashMap<String, EventPanel> getClockPanelsMap(){
		return _clockPanelsMap;
	}
	
	//TODO
	/**
	 * @param time
	 * @param panelName
	 * @author mzc
	 */
	public void drawTick(int time,String panelName){
		Eventline el = _clockPanelsXmiMap.get(panelName).getEventline();
		el.drawTick(time);
	}
	
	//TODO
	/** 
	 * @param time
	 * @param name
	 * @author mzc
	 */
	public void drawNoTick(int time,String panelName){
		Eventline el = _clockPanelsXmiMap.get(panelName).getEventline();
		el.drawNoTick(time);
	}
	
	//TODO
	/**
	 * @param time
	 * @param name
	 * @author mzc
	 */
	public void drawEnd(int time,String panelName) {
		Eventline el = _clockPanelsXmiMap.get(panelName).getEventline();
		el.drawEnd(time);
	}
	
	public void drawZ(int time,String panelName){
		Eventline el = _clockPanelsXmiMap.get(panelName).getEventline();
		el.drawZ(time);
	}
	
	public VcdFactory(VcdColorPreferences mcain) {
		_mca = mcain;
		if (_mca == null) {
			_mca = VcdColorPreferences.createColor();
		}
		_timeline = new Timeline(new Point(5, 0),10,_mca);
		GridLayout layout = null;
		layout = new GridLayout(1, true);
		layout.marginHeight = 25;
		namesP = new Panel();
		namesP.setBackgroundColor(_mca.colorBlack());
		namesP.setOpaque(true);
		namesP.setLayoutManager(layout);
		_backPanel = new Panel();
		_backPanel.setBackgroundColor(_mca.colorBlack());
		_backPanel.setOpaque(false);
		layout = new GridLayout(1, true);
		layout.marginHeight = 25;
		_backPanel.setLayoutManager(layout);
		layout = new GridLayout(1, true);
		layout.marginHeight = 1;
		layout.verticalSpacing = 0;
		layout.horizontalSpacing = 0;
		_timelinePanel = new Panel();
		layout = new GridLayout(1, true);
		layout.marginWidth = 0;
		layout.horizontalSpacing = 0;
		_timelinePanel.setBackgroundColor(_mca.colorBlack());
//		_timelinePanel.setBackgroundColor(ColorConstants.blue);
		_timelinePanel.setOpaque(false);
		_timelinePanel.setLayoutManager(layout);
		_timelinePanel.setSize(-1, 35);
		_timelinePanel.add(_timeline);

		_picture = new Panel();
		_picture.setBackgroundColor(_mca.colorBlack());
		_picture.setOpaque(false);
		_picture.setLayoutManager(layout);
		_picture.setSize(120, 35);
	}	
	/**
	 * This method create
	 * <li>a new instance of {@link VcdValueFactory} for one clock</li>
	 * <li>a new panel to store the {@link Eventline}
	 * <li>a new instance of {@link Eventline}
	 * <li>a associated controlling widget located at the right side.</li>
	 * 
	 * @param var
	 * @param zoom
	 * @return VcdValueFactory
	 */
	public VcdValueFactory initVcdValueFactory(IVar var, double zoom) {
		Object obj = var.getData("xmi");
		String clockXmiId = null;
		if(obj instanceof String){
			clockXmiId = ((String)obj).substring(((String)obj).indexOf("#")+1);
		}
		String name = var.getName();
		String text = var.getAliasName();
		if(clockXmiId==null){
			EventPanel ep = new EventPanel(_constraintsFactory,this,name,text,_mca);
			ep.setBackgroundColor(_mca.colorBlack());
//			ep.setBackgroundColor(ColorConstants.gray);
			ep.setPreferredSize(new Dimension(2000, _height + dY + 10));
			_backPanel.add(ep);
			_backPanelBounds.setBounds(ep.getBounds());
			_backPanel.getLayoutManager().layout(_backPanel);
			Eventline cl = new Eventline(ep.getLocation(),10,name,_mca);
			ep.initEventPanel(cl);
			ep.setBounds(cl.getBounds());
			_clockPanelsXmiMap.put(name, ep);
			_clocklinesXmiMap.put(name, cl);
		}else{	
			EventPanel ep = new EventPanel(_constraintsFactory,this,clockXmiId,name,text,_mca);
			ep.setBackgroundColor(_mca.colorBlack());
//			ep.setBackgroundColor(ColorConstants.gray);
			ep.setPreferredSize(new Dimension(2000, _height + dY + 10));
			_backPanel.add(ep);
			_backPanelBounds.setBounds(ep.getBounds());
			_backPanel.getLayoutManager().layout(_backPanel);
			Eventline cl = new Eventline(ep.getLocation(),10,clockXmiId, _mca);
			ep.initEventPanel(cl);
			ep.setBounds(cl.getBounds());		
			_clockPanelsXmiMap.put(clockXmiId, ep);
			_clocklinesXmiMap.put(clockXmiId, cl);	
			_clockPanelsMap.put(name, ep);
			_clocklinesMap.put(name, cl);
			
		}
		Label l = new Label(text);
		RectangleFigure rectangle = new RectangleFigure();
		int size = _mca.getWidthName() - 10; // 90;
		rectangle.setOpaque(false);
		rectangle.setLineStyle(Graphics.LINE_DOT);
		rectangle.setForegroundColor(_mca.colorLightGrayClockNameContour());
		rectangle.setBounds(new Rectangle(0, 0, size, _height + dY + 10));
		l.setBounds(new Rectangle(1, 1, size - 4, _height + dY + 5));
		l.setForegroundColor(_mca.colorWhiteText());
		l.setOpaque(true);
		Font font = new Font(null, "Arial", 10, 1);
		l.setFont(font);
		rectangle.add(l);
		namesP.add(rectangle);
		RectangleFigure tiprectangle = new ToolTipRectangle(var);
		rectangle.setToolTip(tiprectangle);
		if (var.getValueFactory() != null) {
			var.getValueFactory().clear();
		}
		VcdValueFactory out = new VcdValueFactory(var.getVarCommand(), this, y, zoom);
		var.setValueFactory(out);
		return out;
	}

	/**
	 * new
	 * @return
	 */
	public Panel getNames() {
		return namesP;
	}
	
	/**
	 * getScale()
	 * @return
	 */
	public Panel getScalePanel() {
		return _timelinePanel;
	}

	/**
	 * new
	 * @return
	 */
	public Panel getBackPanel() {
		return _backPanel;
	}
	
	/**
	 * getLength
	 * @return
	 */
	public int getWindowsBoundsLength() {
		return _backPanelBounds.getSize().width;
	}

	public int getHeigth() {
		int n = 0;
		for (Object p : _backPanel.getChildren()) {
			if (p instanceof Panel) {
				n++;
			}
		}
		return (n + 1) * 40 + 40;
	}

	public Panel getPicture() {
		return _picture;
	}

	public Description getFigureForDescription(IFigure ef) {
		return figurefordescription.get(ef);
	}

	public HashMap<String, ArrayList<ExtendFigure>> getNameforfigures() {
		return nameforfigures;
	}

	public ConstraintsFactory getConstraintsFactory() {
		return _constraintsFactory;
	}

	public void setConstraintsFactory(ConstraintsFactory constraintsFactory) {
		this._constraintsFactory = constraintsFactory;
	}

	public MarkerFactory getMarkerFactory() {
		return _markerFactory;
	}

	public void setMarkerFactory(MarkerFactory markerFactory) {
		this._markerFactory = markerFactory;
	}

	private VCDDefinitions _vcdDef = null;

	public VCDDefinitions getVcdDefinitions() {
		return _vcdDef;
	}

	public void setVcdDef(VCDDefinitions vcd) {
		this._vcdDef = vcd;
	}

	int _scaleRange = 100;

	public int getScaleRange() {
		return _scaleRange;
	}

	public void setScaleRange(int _scaleRange) {
		if (_scaleRange != 0)
			this._scaleRange = _scaleRange;
	}

	/**
	 * new
	 * @author mzc
	 */
	public int modifyTimeLineSize(int currentTime) {
		
		if(_endTime > currentTime){
			return 0;
		}
		if(_endTime == currentTime){
			return 0;
		}
		if(currentTime%100 == 0){
			_timeline.extendTimeLine(_vcdDef.getTimeLabelling().getRulerTimeIndice(currentTime));
		}
		_endTime = currentTime;
		return 0;
	}

	public VcdZoom getVcdZoom() {
		return _vcdZoom;
	}

	public void setVcdZoom(VcdZoom _vcdZoom2) {
		this._vcdZoom = _vcdZoom2;
	}

	public IVcdDiagram getVcddia() {
		return _vcdDiagram;
	}

	public void setVcddia(IVcdDiagram vcddia) {
		this._vcdDiagram = vcddia;
	}

	public void clear2() {
		ghostList.clear();
		for (Panel panel : _clockPanelsXmiMap.values()) {
			panel.removeAll();
			panel.erase();
			panel.repaint();
		}
		_clockPanelsXmiMap.clear();
		for (ArrayList<ExtendFigure> lif : nameforfigures.values()) {
			for (IFigure ifi : lif) {
				if (ifi != null)
					ifi.erase();
			}
			lif.clear();
		}
		nameforfigures.clear();
	}

	public void clear() {
		figurefordescription.clear();
		ghostList.clear();
		
		for(Eventline cl: _clocklinesMap.values()){
			cl.getIntants().clear();
			cl.getTickInstants().clear();
			cl.getTicksTime().clear();
			cl.erase();
		}
		for (EventPanel eventPanel : _clockPanelsMap.values()) {
			eventPanel.removeAll();
			eventPanel.erase();
			eventPanel.repaint();
		}
		_clockPanelsMap.clear();
		_clockPanelsMap.clear();
//		for (ArrayList<ExtendFigure> lif : nameforfigures.values()) {
//			for (IFigure ifi : lif) {
//				ifi.erase();
//			}
//			lif.clear();
//		}
//		nameforfigures.clear();
//		for (TimeLineElement te : _arraylistTimeElement) {
//			te.clear();
//		}
		_clocklinesMap.clear();
//		_arraylistTimeElement.clear();
		_timeline.getLabels().clear();
		_timeline.removeAll();
		_timeline.removeAllPoints();
		_timelinePanel.removeAll();
		_backPanel.removeAll();
		namesP.removeAll();
		_picture.removeAll();
		_timelinePanel.erase();
		_backPanel.erase();
		namesP.erase();
		_picture.erase();
		_timeline = null;
		_clocklinesXmiMap = null;
		_constraintsFactory = null;
		_markerFactory = null;
		_mca = null;
		_vcdDiagram = null;
		_vcdZoom = null;
		nameforfigures = null;

	}
	

	private final class ToolTipRectangle extends RectangleFigure {
		private IVar _variable = null;
		private Label _label = null;
		public ToolTipRectangle(IVar _variable) {
			super();
			int n = _variable.getName().length() + 8;
			this._variable = _variable;
			this.setOpaque(true);
			this.setOutline(false);
			this.setForegroundColor(ColorConstants.gray);
			this.setBackgroundColor(ColorConstants.lightGray);
			this.setBounds(new Rectangle(0, 0, 8 * n + 10, 58));
			_label = new Label();
			_label.setText("default ");
			_label.setBounds(new Rectangle(5, 3, 8 * n, 53));
			_label.setForegroundColor(ColorConstants.black);
			_label.setBackgroundColor(ColorConstants.lightGray);
			_label.setOpaque(true);
			this.add(_label);
		}
		@Override
		public boolean isVisible() {
			return super.isVisible();
		}
		@Override
		public void setBounds(Rectangle rect) {
			super.setBounds(rect);
		}
		@Override
		public void repaint() {
			if (_label != null) {
				if (_variable != null) {
					String _str = null;
					boolean _bool = (_variable.getName().compareTo(_variable.getAliasName()) != 0);
					_str = "Name:\t" + _variable.getAliasName() + "\n\t";
					if (_bool) {
						_str +=  "or\t"+_variable.getName() + "\n";
					}
					if (_variable.getValueFactory() != null) {
						_str += "Nb of Tc: " + '\t' + _variable.getValueFactory().getTickNum() + "\n";
						_str += "Ghost: "+'\t' + _variable.getValueFactory().haveGhostinclock();
						if (_variable.getValueFactory().haveGhostinclock())
							_str += "\nNb of Ghosts: " + '\t' + _variable.getValueFactory().getNbGhost();
					}
					_label.setText(_str);
					_label.setSize(_label.getPreferredSize());
					this.setSize(_label.getPreferredSize().expand(10, 6));
					_label.repaint();
				} else {
					_label.setText("nothing");
				}
			}
			super.repaint();
		}

		@Override
		public void setVisible(boolean visible) {
			super.setVisible(visible);
		}
	}
	
	void add(ExtendFigure f, Description description, boolean b) {
		if (f == null)
			return;
		_backPanelBounds = _backPanelBounds.getUnion(f.getBounds());
		_backPanel.getLayoutManager().layout(_backPanel);
		Point location = null;
		if(_clockPanelsXmiMap.get(description.getName()) != null){
			EventPanel ep = _clockPanelsXmiMap.get(description.getName());
			location = ep.getLocation();
			ep.getClientArea().getUnion(f.getBounds());
			if (f instanceof PolylineConnection) {
				PolylineConnection fig = (PolylineConnection) f;
				fig.getPoints().translate(location);
				(f).setTr(location);
				(f).mycompute();
			}
	
			if (f instanceof Polygon) {
				Polygon poly = (Polygon) f;
				poly.getPoints().translate(location);
				for (Object l : poly.getChildren()) {
					if (l instanceof Label)
						((Label) l).setBounds(((Label) l).getBounds().translate(location));
				}
				poly.setBounds(poly.getBounds().translate(location));		
				(f).setTr(location);
				(f).mycompute();
			}
			ep.add(f);
			ep.setDurationModel();
			ep.setPreferredSize(f.getBounds().x+f.getLength(), ep.getBounds().height);
		}
	}
	
}
