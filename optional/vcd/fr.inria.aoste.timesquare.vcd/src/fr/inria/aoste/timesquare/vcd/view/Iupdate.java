/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;

public interface Iupdate {

	public abstract int update(int nl, int n, boolean b);
	
	public abstract int update2(int n1, int n2, boolean b);

	public abstract boolean isSimulation();

	public abstract void setSimulation(boolean simulation);

	public abstract void setSimulationProgress(int etape);

	public abstract TraceCollector getTraceCollector();

	public abstract Shell getMyShell();

	public abstract VCDDefinitions getVcdModel();

	public abstract void vcdMultiPageEditorRefresh();

}