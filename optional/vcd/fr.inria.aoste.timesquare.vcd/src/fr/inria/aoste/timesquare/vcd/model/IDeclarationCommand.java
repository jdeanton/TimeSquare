/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

/**
 * An interface for classes that implements the various commands found in a VCD file to
 * specify either the date and version of the file, the timescale, and all the comments used
 * by Timesquare to specify information used for the drawing of the diagram in the VCD view.
 * 
 * @author Benoit Ferrero
 *
 */
public interface IDeclarationCommand {
	void visit(IDeclarationVisitor visitor);
	int clear();
}
