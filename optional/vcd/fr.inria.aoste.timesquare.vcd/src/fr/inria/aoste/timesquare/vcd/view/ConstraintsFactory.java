/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYAnchor;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.ToolItem;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcd;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.figure.Balise;
import fr.inria.aoste.timesquare.vcd.figure.Balise.Linkto;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.VcdChopboxAnchor;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class ConstraintsFactory implements IConstructContraint {

	private ListConnections _listConnnections;
	private IVcd _vdt;
	private VcdColorPreferences _mca;

	public ConstraintsFactory(ListConnections list, IVcd vdt) {
		super();
		this._listConnnections = list;
		this._vdt = vdt;
		_mca = vdt.getColorPreferences();
		vdt.getVcdFactory().setConstraintsFactory(this);

	}

	public VcdColorPreferences getMca() {
		return _mca;
	}

	public void addToList(ArrayList<ConstraintsConnection> listSync,
			ConstraintsConnection poly) {
		ConstraintsConnection polyc = poly;
		if (polyc.isGlobal() == null) {
			ErrorConsole.println("Warning :  " + poly.getComment()
					+ " must be set Global Flag ");
			System.out.println("Warning :  " + poly.getComment()
					+ " must be set Global Flag ");
		}
		if (polyc.getComment() == null) {
			ErrorConsole.println("Warning :  must be set  Comment Flag  ");
			System.out.println("Warning :  must be set  Comment Flag   ");
		}
		Iterator<ConstraintsConnection> it = listSync.iterator();
		boolean state = true;
		IFigure startAnchor1 = null;
		IFigure startAnchor2 = null;
		IFigure endAnchor1 = null;
		IFigure endAnchor2 = null;
		while (it.hasNext()) {
			ConstraintsConnection pc = it.next();
			Boolean b1= (pc.isGlobal());
			Boolean b2= (poly.isGlobal());
			if (b1==null && b2!=null){
				continue ;
			}
			if (b1!=null && b2==null){
				continue ;
			}
			if (b1!=null && b2!=null){
				if (b1.booleanValue() != b2.booleanValue()){
					continue;
				}
			}
			if (pc.getComment() != poly.getComment()){
				continue;
			}
			if (pc.getTargetAnchor() != null && polyc.getSourceAnchor() != null
					&& polyc.getTargetAnchor() != null
					&& pc.getSourceAnchor() != null) {
				startAnchor1 = pc.getSourceAnchor().getOwner();
				startAnchor2 = polyc.getSourceAnchor().getOwner();
				endAnchor1 = pc.getTargetAnchor().getOwner();
				endAnchor2 = polyc.getTargetAnchor().getOwner();
				if (startAnchor1 instanceof PolylineConnection
						&& startAnchor2 instanceof PolylineConnection
						&& endAnchor1 instanceof PolylineConnection
						&& endAnchor2 instanceof PolylineConnection) {
					if (((PolylineConnection) startAnchor1).getPoints()
							.getBounds().equals(
							((PolylineConnection) startAnchor2)
							.getPoints().getBounds())) {
						if (((PolylineConnection) endAnchor1).getPoints()
								.getBounds().equals(
								((PolylineConnection) endAnchor2)
								.getPoints().getBounds())) {
							state = false;
							listSync.remove(pc);
							if (_vdt.getCanvas().getContents().getChildren()
									.contains(pc))
								_vdt.getCanvas().getContents().remove(pc);
							Rectangle bounds = pc.getBounds();
							_vdt.getCanvas().redraw(bounds.x, bounds.y,
									bounds.width, bounds.height, true);
							break;
						}
					}
				}
				if (startAnchor1 instanceof RectangleFigure
						&& startAnchor2 instanceof RectangleFigure
						&& endAnchor1 instanceof RectangleFigure
						&& endAnchor2 instanceof RectangleFigure) {
					Rectangle rect1 = new Rectangle(
							((RectangleFigure) startAnchor1).getBounds());
					Rectangle rect2 = new Rectangle(
							((RectangleFigure) endAnchor1).getBounds());
					if (rect1.expand(5, 5).intersects(
							((RectangleFigure) startAnchor2).getBounds())) {
						if (rect2.expand(5, 5).intersects(
								((RectangleFigure) endAnchor2).getBounds())) {
							state = false;
							listSync.remove(pc);
							if (_vdt.getCanvas().getContents().getChildren()
									.contains(pc))
								_vdt.getCanvas().getContents().remove(pc);
							Rectangle bounds = pc.getBounds();
							_vdt.getCanvas().redraw(bounds.x, bounds.y,
									bounds.width, bounds.height, true);
							break;
						}
					}
				}
				if (startAnchor1 instanceof PolylineConnection
						&& startAnchor2 instanceof PolylineConnection
						&& endAnchor1 instanceof RectangleFigure
						&& endAnchor2 instanceof RectangleFigure) {
					if (((PolylineConnection) startAnchor1).getPoints()
							.getBounds().equals(
							((PolylineConnection) startAnchor2)
							.getPoints().getBounds())) {
						Rectangle rect = new Rectangle(
								((RectangleFigure) endAnchor1).getBounds());
						if (rect.expand(5, 5).intersects(
								((RectangleFigure) endAnchor2).getBounds())) {
							state = false;
							listSync.remove(pc);
							if (_vdt.getCanvas().getContents().getChildren()
									.contains(pc))
								_vdt.getCanvas().getContents().remove(pc);
							Rectangle bounds = pc.getBounds();
							_vdt.getCanvas().redraw(bounds.x, bounds.y,
									bounds.width, bounds.height, true);
							break;
						}
					}
				}
				if (startAnchor1 instanceof RectangleFigure
						&& startAnchor2 instanceof RectangleFigure
						&& endAnchor1 instanceof PolylineConnection
						&& endAnchor2 instanceof PolylineConnection) {
					Rectangle rect = new Rectangle(
							((RectangleFigure) startAnchor2).getBounds());
					if (rect.expand(5, 5).intersects(
							((RectangleFigure) startAnchor1).getBounds())) {
						if (((PolylineConnection) endAnchor1).getPoints()
								.getBounds().equals(
								((PolylineConnection) endAnchor2)
								.getPoints().getBounds())) {
							state = false;
							listSync.remove(pc);
							if (_vdt.getCanvas().getContents().getChildren()
									.contains(pc))
								_vdt.getCanvas().getContents().remove(pc);
							Rectangle bounds = pc.getBounds();
							_vdt.getCanvas().redraw(bounds.x, bounds.y,
									bounds.width, bounds.height, true);
							break;
						}
					}
				}
			} else {
				if (polyc.getPoints().size() > 2) {
					for (int i = 0; i < pc.getPoints().size(); i++) {
						if (i != pc.getPoints().size() - 1) {
							if (!pc.getPoints().getPoint(i).equals(
									polyc.getPoints().getPoint(i)))
								break;

							continue;
						}
						state = false;
						listSync.remove(pc);
						_vdt.getCanvas().getContents().remove(pc);
						Rectangle bounds = pc.getBounds();
						_vdt.getCanvas().redraw(bounds.x, bounds.y,
								bounds.width, bounds.height, true);
						break;
					}
				}
			}
		}
		if (state)
			listSync.add(poly);
	}

	public ConstraintsConnection constructCoincidenceConnection(Color color, Draw1 f1,
			Draw1 f2) {
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineWidth(2);
		c.setOpaque(true);
		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
		ChopboxAnchor targetAnchor = new ChopboxAnchor(f2);
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		PolygonDecoration decoration = new PolygonDecoration();
		PointList decorationPointList = new PointList();
		decorationPointList.addPoint(-2, 0);
		decorationPointList.addPoint(-1, 1);
		decorationPointList.addPoint(0, 0);
		decorationPointList.addPoint(-1, -1);
		decoration.setTemplate(decorationPointList);
		c.setTargetDecoration(decoration);
		c.setSourceDecoration(decoration);
		c.getInstants().add(f1.getInstant());
		c.getInstants().add(f2.getInstant());
		return c;
	}

	/**
	 * @author mzc
	 */
	public ConstraintsConnection constructCoincidenceConnection(Color color, Eventline c1, int c1_tickNumber,Eventline c2, int c2_tickNumber) {
		int c1TickInstant = c1.getTickInstants().get(c1_tickNumber-1);
		int c2TickInstant = c2.getTickInstants().get(c2_tickNumber-1);
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineWidth(2);
		c.setOpaque(true);
		//***************************************************
		
		VcdChopboxAnchor sourceAnchor = new VcdChopboxAnchor(c1,c1TickInstant);
		VcdChopboxAnchor targetAnchor = new VcdChopboxAnchor(c2,c2TickInstant);

		//***************************************************
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		PolygonDecoration decoration = new PolygonDecoration();
//		PointList decorationPointList = new PointList();
//		decorationPointList.addPoint(-1, 1);
//		decorationPointList.addPoint(1, 1);
//		decorationPointList.addPoint(0, 0);
//		decorationPointList.addPoint(-1, -1);
//		decoration.setTemplate(decorationPointList);
		decoration.setTemplate(PolygonDecoration.TRIANGLE_TIP);
		c.setTargetDecoration(decoration);
		c.setSourceDecoration(decoration);
		return c;
	}
	
	/**
	 * @author mzc
	 */
	@Override
	public ConstraintsConnection constructDashConnection(Color color,Eventline c1, int c1_tickNumber, Eventline c2, int c2_tickNumber,boolean empty) {

		int c1TickInstant = c1.getTickInstants().get(c1_tickNumber-1);
		int c2TickInstant = c2.getTickInstants().get(c2_tickNumber-1);
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineStyle(Graphics.LINE_DASH);
		c.setLineWidth(2);
		c.setOpaque(true);
		VcdChopboxAnchor sourceAnchor = new VcdChopboxAnchor(c1,c1TickInstant);
		VcdChopboxAnchor targetAnchor = new VcdChopboxAnchor(c2,c2TickInstant);
		
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		PolygonDecoration decoration = new PolygonDecoration();
		decoration.setTemplate(PolygonDecoration.TRIANGLE_TIP);
		if (empty) {
			decoration.setForegroundColor(color);
			decoration.setBackgroundColor(_mca.colorBlack());
		}
		c.setTargetDecoration(decoration);
		return c;
	}

	
	/**
	 * @see fr.inria.aoste.timesquare.vcd.view.IConstructContraint#constructDashConnection(org.eclipse.swt.graphics.Color, org.eclipse.draw2d.IFigure,
	 *      org.eclipse.draw2d.IFigure)
	 */
	public ConstraintsConnection constructDashConnection(Color color, Draw1 f1,
			Draw1 f2, boolean empty) {
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineStyle(Graphics.LINE_DASH);
		c.setLineWidth(2);
		c.setOpaque(true);
		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
		ChopboxAnchor targetAnchor = new ChopboxAnchor(f2);
		
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		PolygonDecoration decoration = new PolygonDecoration();
		decoration.setTemplate(PolygonDecoration.TRIANGLE_TIP);
		if (empty) {
			decoration.setForegroundColor(color);
			decoration.setBackgroundColor(_mca.colorBlack());
		}
		c.setTargetDecoration(decoration);
		c.getInstants().add(f1.getInstant());
		c.getInstants().add(f2.getInstant());
		return c;
	}
	
	public ConstraintsConnection constructConnection(Color color, IFigure f1,Point f2) {
		ConstraintsConnection c = new ConstraintsConnection();
		c.setForegroundColor(color);
		c.setLineWidth(2);
		c.setOpaque(true);
		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
		XYAnchor targetAnchor = new XYAnchor(f2);
		c.setSourceAnchor(sourceAnchor);
		c.setTargetAnchor(targetAnchor);
		return c;
	}



	public ConstraintsConnection constructPolylineConnection(Color color,
			Point pt1, Point pt2) {
		ConstraintsConnection poly = new ConstraintsConnection();
		poly.setPoints(new PointList());
		poly.setForegroundColor(color);
		poly.setOutlineXOR(true);
		poly.setLineWidth(2);
		poly.setOpaque(true);
		poly.addPoint(pt1);
		poly.addPoint(pt2);
		return poly;
	}

	/***
	 * 
	 * @return index == date of (clk define by name)
	 * 
	 */

	public int synchrone(String name, int date) {

		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
				name);
		int n = 0;
		for (IFigure figs : figures) {
			if (((PolylineConnection) figs).getLocation().x == date) {
				return n;
			}
			n++;
		}
		return -1;
	}

	/***
	 * 
	 * @return premier index >= of (clk define by name) /
	 * 
	 */

	public int firstAfter(String name, int date) {
		
		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
				name);
		int n = 0;
		for (IFigure figs : figures) {
			if (((PolylineConnection) figs).getLocation().x >= date) {
				return n;
			}
			n++;
		}
		return -1;
	}


	/***
	 * 
	 * @return premier index > of (clk define by name) /
	 * 
	 */

	public int firstAfterStrict(String name, int date) {
		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
				name);
		int n = 0;
		for (IFigure figs : figures) {
			if (((PolylineConnection) figs).getLocation().x > date) {
				return n;
			}
			n++;
		}
		return -1;
	}

	/***
	 * 
	 * @return dernier index < of (clk define by name) /
	 * 
	 */

	public int lastBefore(String name, int px) {
		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
				name);
		int n = 0;
		int k = -1;
		for (IFigure figs : figures) {
			if (((PolylineConnection) figs).getLocation().x < px) {
				k = n;
			}
			n++;
		}
		return k;
	}

	public int lastBeforeweak(String name, int px) {
		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
				name);
		int n = 0;
		int k = -1;
		for (IFigure figs : figures) {
			if (((PolylineConnection) figs).getLocation().x <= px) {
				k = n;
			}
			n++;
		}
		return k;
	}

	public void drawOneTickConstraints2(IFigure fig, ConstraintCommentCommand cc, String currentclock) {
		if (cc.getClock().equals(currentclock)) {
			switch (cc.getFunction()) {
			case _filteredby:
			case _synchronizeswith:
			case _sampledon:
			case _sustains:
			case _alternateswith:
			case _isperiodicon:
			case _precedes:
				if (cc.getConstraints() != null) {
					// cc.getIc().setCc(cc);
					// cc.getIc().setList(listed);
					// cc.getIc().setVcdFactory(vdt.getFactory());
					// cc.getIc().setIcc(this);
					if (cc.getConstraints().haveAllClockVisible())
						if (fig instanceof Draw1)
							cc.getConstraints().draw((Draw1) fig, currentclock);
				}
				break;
			/*
			  case _periodicon: 
			 filterByFunction(fig, currentclock, cc, VcdColorPreferences .colorRedCoincidence(), true); break;
			 */
			case _restrictedto:
				// filterByFunction(fig, currentclock, cc, mca.colorRedCoincidence(), true);
				break;
			/*
			  case _alternateswith: 
			  alternateFunction(fig, currentclock, cc, null, true); break;
			 */
			case _oneshoton:
				// oneShotConstraintFunction(fig, currentclock, cc, null, true);
				break;
			case _union:
				// unionFunction(fig, currentclock, cc, null, true);
				break;
			// case _precedes:
			// precedesFunction(fig, currentclock, cc, VcdColorPreferences.DEFAULT.colorBluePrecedes(), true);
			// break;
			default:
			}
			for (IFigure f : _listConnnections.getListConstraints()) {
				Dimension dim = _vdt.getCanvas().getContents()
						.getPreferredSize();
				_vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				_vdt.getCanvas().redraw(bounds.x, bounds.y,
						bounds.width, bounds.height, true);
				_vdt.getCanvas().getContents().setPreferredSize(dim);
			}
		}
		if (cc.getReferenceClocks().contains(currentclock)) {
			switch (cc.getFunction()) {
			case _filteredby:
			case _synchronizeswith:
			case _sampledon:
			case _sustains:
			case _alternateswith:
			case _isperiodicon:
			case _precedes:
				if (cc.getConstraints() != null) {
					// cc.getIc().setCc(cc);
					// cc.getIc().setList(listed);
					// cc.getIc().setVcdFactory(vdt.getFactory());
					// cc.getIc().setIcc(this);
					if (cc.getConstraints().haveAllClockVisible())
						if (fig instanceof Draw1)
							cc.getConstraints().draw((Draw1) fig, currentclock);
				}

				break;
			/*
			 * case _periodicon: filterByFunction(fig, currentclock,
			 * cc, VcdColorPreferences .colorRedCoincidence(), false); break;
			 */
			case _restrictedto:
				// filterByFunction(fig, currentclock, cc, mca.colorRedCoincidence(), false);
				break;
			/*
			 * case _alternateswith: alternateFunction(fig,
			 * currentclock, cc, null, false); break;
			 */
			case _union:
				// unionFunction(fig, currentclock, cc, null, false);
				break;
			// case _precedes:
			// precedesFunction(fig, currentclock, cc, VcdColorPreferences.DEFAULT
			// .colorBluePrecedes(), false);
			// break;
			default:
				break;
			}
			for (IFigure f : _listConnnections.getListConstraints()) {
				Dimension dim = _vdt.getCanvas().getContents()
						.getPreferredSize();
				_vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				_vdt.getCanvas().redraw(bounds.x, bounds.y,
						bounds.width, bounds.height, true);
				_vdt.getCanvas().getContents().setPreferredSize(dim);
			}
		}

	}

	public boolean drawOneTickConstraints1(IFigure fig) {
		if (fig != null) {
			Description descr;
			if (fig instanceof Label)
				descr = _vdt.getVcdFactory().getFigureForDescription(fig.getParent());
			else
				descr = _vdt.getVcdFactory().getFigureForDescription(fig);
			if (descr == null)
				return false;
			String currentclock = descr.getName();
			if (fig instanceof PolylineConnection || fig instanceof Label) {
				for ( IConstraintData cc : _vdt.getVcdModel().getConstraintList()) {
					if (cc instanceof ConstraintCommentCommand )
					{
						drawOneTickConstraints2(fig, (ConstraintCommentCommand) cc, currentclock);
					}
					else{
						if (cc.getConstraints().haveAllClockVisible()){
							if (fig instanceof Draw1){
								cc.getConstraints().draw((Draw1) fig, currentclock);
							}
						}
					}
				}
				if (fig.getForegroundColor().equals(
						_mca.colorLightGreenFirable())) {
					// fig.setForegroundColor(VcdColorPreferences.colorred());
				} else {
					fig.setForegroundColor(_mca.colorLightGreenFirable());
				}
			}

		}
		return true;
	}

	
	//TODO
	public boolean drawOneTickConstraints(int tickInstant, String clockName) {
		for ( IConstraintData cc : _vdt.getVcdModel().getConstraintList()) {
			if (cc instanceof ConstraintCommentCommand )
			{
				//TODO this method is of no use
	//				drawOneTickConstraints2Test(fig, (ConstraintCommentCommand) cc, currentclock);
			}
			else{
				if (cc.getConstraints().haveAllClockVisible()){
					cc.getConstraints().drawOneTick(tickInstant, clockName);
				}
			}
		}
		return true;
	}
	//TODO
	public void drawOneTickConstraints2Test(IFigure fig, ConstraintCommentCommand cc, String currentclock) {
		if (cc.getClock().equals(currentclock)) {
			switch (cc.getFunction()) {
			case _filteredby:
			case _synchronizeswith:
			case _sampledon:
			case _sustains:
			case _alternateswith:
			case _isperiodicon:
			case _precedes:
				if (cc.getConstraints() != null) {
					// cc.getIc().setCc(cc);
					// cc.getIc().setList(listed);
					// cc.getIc().setVcdFactory(vdt.getFactory());
					// cc.getIc().setIcc(this);
					if (cc.getConstraints().haveAllClockVisible()){
							//cc.getConstraints().drawTest((Draw1) fig, currentclock);
					}
				}
				break;
			/*
			  case _periodicon: 
			 filterByFunction(fig, currentclock, cc, VcdColorPreferences .colorRedCoincidence(), true); break;
			 */
			case _restrictedto:
				// filterByFunction(fig, currentclock, cc, mca.colorRedCoincidence(), true);
				break;
			/*
			  case _alternateswith: 
			  alternateFunction(fig, currentclock, cc, null, true); break;
			 */
			case _oneshoton:
				// oneShotConstraintFunction(fig, currentclock, cc, null, true);
				break;
			case _union:
				// unionFunction(fig, currentclock, cc, null, true);
				break;
			// case _precedes:
			// precedesFunction(fig, currentclock, cc, VcdColorPreferences.DEFAULT.colorBluePrecedes(), true);
			// break;
			default:
			}
			for (IFigure f : _listConnnections.getListConstraints()) {
				Dimension dim = _vdt.getCanvas().getContents()
						.getPreferredSize();
				_vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				_vdt.getCanvas().redraw(bounds.x, bounds.y,
						bounds.width, bounds.height, true);
				_vdt.getCanvas().getContents().setPreferredSize(dim);
			}
		}
		if (cc.getReferenceClocks().contains(currentclock)) {
			switch (cc.getFunction()) {
			case _filteredby:
			case _synchronizeswith:
			case _sampledon:
			case _sustains:
			case _alternateswith:
			case _isperiodicon:
			case _precedes:
				if (cc.getConstraints() != null) {
					// cc.getIc().setCc(cc);
					// cc.getIc().setList(listed);
					// cc.getIc().setVcdFactory(vdt.getFactory());
					// cc.getIc().setIcc(this);
					if (cc.getConstraints().haveAllClockVisible())
						if (fig instanceof Draw1)
							cc.getConstraints().draw((Draw1) fig, currentclock);
				}

				break;
			/*
			 * case _periodicon: filterByFunction(fig, currentclock,
			 * cc, VcdColorPreferences .colorRedCoincidence(), false); break;
			 */
			case _restrictedto:
				// filterByFunction(fig, currentclock, cc, mca.colorRedCoincidence(), false);
				break;
			/*
			 * case _alternateswith: alternateFunction(fig,
			 * currentclock, cc, null, false); break;
			 */
			case _union:
				// unionFunction(fig, currentclock, cc, null, false);
				break;
			// case _precedes:
			// precedesFunction(fig, currentclock, cc, VcdColorPreferences.DEFAULT
			// .colorBluePrecedes(), false);
			// break;
			default:
				break;
			}
			for (IFigure f : _listConnnections.getListConstraints()) {
				Dimension dim = _vdt.getCanvas().getContents()
						.getPreferredSize();
				_vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				_vdt.getCanvas().redraw(bounds.x, bounds.y,
						bounds.width, bounds.height, true);
				_vdt.getCanvas().getContents().setPreferredSize(dim);
			}
		}

	}
	
	
	
	public void addAndRedraw(IFigure figure) {
		Dimension dim = _vdt.getCanvas().getContents().getPreferredSize();
		_vdt.getCanvas().getContents().add(figure);
		Rectangle bounds = figure.getBounds();
		_vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,	true);
		_vdt.getCanvas().getContents().setPreferredSize(dim);
	}

	public void redraw(IFigure figure) {
		Dimension dim = _vdt.getCanvas().getContents().getPreferredSize();
		Rectangle bounds = figure.getBounds();
		_vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
				true);
		_vdt.getCanvas().getContents().setPreferredSize(dim);
	}

	public void redrawSyncInterval(ConstraintCommentCommand cc, Color color) {
		if (cc == null)
			return;
		for (IFigure f : _listConnnections.getListInterval().get(cc)) {
			_vdt.getCanvas().getContents().remove(f);
		}
		_listConnnections.getListInterval().get(cc).clear();
		Iterator<IFigure> it = _listConnnections.lastClock1Get(cc).iterator();
		Iterator<IFigure> ot = _listConnnections.lastClock2Get(cc).iterator();
		while (it.hasNext() && ot.hasNext()) {
			Point pt1 = ((PolylineConnection) it.next()).getPoints()
					.getPoint(3);
			Point pt2 = ((PolylineConnection) ot.next()).getPoints()
					.getPoint(3);
			ConstraintsConnection poly = null;
			if (pt1.x > pt2.x) {
				if (pt1.y > pt2.y) {
					poly = constructPolylineConnection(color, new Point(
							pt1.x + 5, pt1.y + 15), new Point(pt1.x + 5,
							pt2.y - 15));
				} else {
					poly = constructPolylineConnection(color, new Point(
							pt1.x + 5, pt1.y - 15), new Point(pt1.x + 5,
							pt2.y + 15));
				}
				_listConnnections.getListInterval().get(cc).add(poly);
				_vdt.getCanvas().getContents().add(poly);
			} else {
				if (pt2.y > pt1.y) {
					poly = constructPolylineConnection(color, new Point(
							pt2.x + 5, pt2.y + 15), new Point(pt2.x + 5,
							pt1.y - 15));
				} else {
					poly = constructPolylineConnection(color, new Point(
							pt2.x + 5, pt2.y - 15), new Point(pt2.x + 5,
							pt1.y + 15));
				}
				_listConnnections.getListInterval().get(cc).add(poly);
				_vdt.getCanvas().getContents().add(poly);
			}
		}
	}

	public void drawSyncInterval(ConstraintCommentCommand cc, Color color) {
		for (IFigure f : _listConnnections.getListInterval().get(cc)) {
			if (f != null)
				if (_vdt.getCanvas().getContents() == f.getParent())
					_vdt.getCanvas().getContents().remove(f);
		}
		Dimension dim = _vdt.getCanvas().getContents().getPreferredSize();
		_listConnnections.clearInterval(cc);
		String clock1 = null;
		String clock2 = null;
		clock1 = cc.getClock();
		clock2 = cc.getReferenceClocks().get(0);
		int occurencesclock1 = Integer.valueOf(cc.getReferenceClocks().get(1));
		int occurencesclock2 = Integer.valueOf(cc.getReferenceClocks().get(2));
		if (_vdt.getVcdFactory().getNameforfigures().get(clock1) == null)
			return;
		if (_vdt.getVcdFactory().getNameforfigures().get(clock2) == null)
			return;
		_listConnnections.lastClock1Get(cc).clear();
		_listConnnections.lastClock2Get(cc).clear();
		for (int i = 0; i < _vdt.getVcdFactory().getNameforfigures().get(clock1)
				.size(); i++) {
			if ((i + 1) % occurencesclock1 == 0) {
				_listConnnections.lastClock1Get(cc)
						.add(
						_vdt.getVcdFactory().getNameforfigures()
						.get(clock1).get(i));
				_listConnnections.firstClock1Get(cc).add(
						_vdt.getVcdFactory().getNameforfigures().get(clock1).get(
						i - (occurencesclock1 - 1)));
			}
		}
		for (int i = 0; i < _vdt.getVcdFactory().getNameforfigures().get(clock2)
				.size(); i++) {
			if ((i + 1) % occurencesclock2 == 0) {
				_listConnnections.lastClock2Get(cc)
						.add(
						_vdt.getVcdFactory().getNameforfigures()
						.get(clock2).get(i));
				_listConnnections.firstClock2Get(cc).add(
						_vdt.getVcdFactory().getNameforfigures().get(clock2).get(
						i - (occurencesclock2 - 1)));
			}
		}
		Iterator<IFigure> it = _listConnnections.lastClock1Get(cc).iterator();
		Iterator<IFigure> ot = _listConnnections.lastClock2Get(cc).iterator();
		while (it.hasNext() && ot.hasNext()) {
			Point pt1 = ((PolylineConnection) it.next()).getPoints()
					.getPoint(3);
			Point pt2 = ((PolylineConnection) ot.next()).getPoints()
					.getPoint(3);
			ConstraintsConnection poly = null;
			if (pt1.x > pt2.x) {
				if (pt1.y > pt2.y) {
					poly = constructPolylineConnection(color, new Point(
							pt1.x + 5, pt1.y + 15), new Point(pt1.x + 5,
							pt2.y - 15));
				} else {
					poly = constructPolylineConnection(color, new Point(
							pt1.x + 5, pt1.y - 15), new Point(pt1.x + 5,
							pt2.y + 15));
				}

			} else {
				if (pt2.y > pt1.y) {
					poly = constructPolylineConnection(color, new Point(
							pt2.x + 5, pt2.y + 15), new Point(pt2.x + 5,
							pt1.y - 15));
				} else {
					poly = constructPolylineConnection(color, new Point(
							pt2.x + 5, pt2.y - 15), new Point(pt2.x + 5,
							pt1.y + 15));
				}

			}
			_listConnnections.getListInterval().get(cc).add(poly);
			if (poly != null)
				_vdt.getCanvas().getContents().add(poly);

		}

		_vdt.getCanvas().getContents().setPreferredSize(dim);
	}

	public IVcd getVdt() {
		return _vdt;
	}

	public void hideCoincidence() {
		for (IFigure figs : _listConnnections.getListCoincidence()) {
			figs.setForegroundColor(_mca.colorLightGreenFirable());
		}
		_listConnnections.getListCoincidence().clear();
	}

	public void hideConstraints() {
		if (_vdt.getToolbar().getItems().length == 0)
			return;
		for (ToolItem item : _vdt.getToolbar().getItems()) {
			item.setText("");
		}
	}

	public void addhiddenGhost(RectangleFigure rect) {
		RectangleFigure temp = new RectangleFigure();
		PolylineConnection poly = new PolylineConnection();
		PointList pl = new PointList();
		poly.setPoints(pl);
		temp.setOpaque(false);
		temp.setFill(true);
		poly.setOpaque(false);
		poly.setFill(true);
		Rectangle bounds = rect.getBounds();
		temp.setForegroundColor(_mca.colorBlack());
		temp.setBackgroundColor(_mca.colorBlack());
		poly.setForegroundColor(_mca.colorLightGreenFirable());
		poly.setBackgroundColor(_mca.colorLightGreenFirable());
		temp.setBounds(bounds);
		pl.addPoint(new Point(bounds.x, bounds.y + bounds.height - 1));
		pl.addPoint(new Point(bounds.x + bounds.width, bounds.y + bounds.height
				- 1));
		poly.setPoints(pl);
		if (rect.getParent() != null) {
			rect.getParent().add(temp);
			rect.getParent().add(poly);
		}
		_listConnnections.listGhostAdd(temp);
		_listConnnections.listGhostAdd(poly);

	}

	public void hideGhost(int index) {
		try {
			IVar var = _vdt.getTraceCollector().getSelectedClocks().get(index);
			var.getValueFactory().setGhostVisible(false);
		} catch (Throwable e) {
		}

	}

	public void hideAllGhost() {
		ArrayList<IVar> listVar = _vdt.getTraceCollector().getAllnames();
		ArrayList<IVar> listVar2 = _vdt.getTraceCollector().getSelectedClocks();
		for (IVar var : listVar) {
			if (var.getValueFactory() != null)
				var.getValueFactory().setGhostVisible(false);
		}
		for (IVar var : listVar2) {
			if (var.getValueFactory() != null)
				var.getValueFactory().setGhostVisible(false);
		}
	}

	public void showConstraint(IFigure fig) {
		if (fig != null) {
			if (fig instanceof PolylineConnection || fig instanceof Label) {
				Description descr = null;
				if (fig instanceof PolylineConnection)
					descr = _vdt.getVcdFactory().getFigureForDescription(fig);
				if (fig instanceof Label)
					descr = _vdt.getVcdFactory().getFigureForDescription(fig.getParent());
				if (descr == null)
					return;
				String currentclock = descr.getName();
				int i = 0;
				for (ConstraintCommentCommand cc : _vdt.getVcdModel()
						.getConstraintCommentCommand()) {
					if (cc.getClock().equals(currentclock)
							|| cc.getReferenceClocks().contains(currentclock)) {
						if (_vdt.getToolbar() == null)
							return;
						for (ToolItem item : _vdt.getToolbar().getItems()) {
							if (item.getText().equals(cc.toString()))
								return;
						}
						String fonction = "";
						switch (cc.getFunction()) {
						default:
							fonction = cc.toString();
						}
						ToolItem item;
						if (i >= _vdt.getToolbar().getItems().length) {
							item = new ToolItem(_vdt.getToolbar(), SWT.PUSH);
						}
						item = _vdt.getToolbar().getItems()[i];
						item.setText(fonction);
						i++;
					}
				}
			}
		}
	}

	public void showGhost(int index) {
		try {
			IVar var = _vdt.getTraceCollector().getSelectedClocks().get(index);
			var.getValueFactory().setGhostVisible(true);
		} catch (Throwable e) {

		}
	}

	public void showAllGhost() {
		ArrayList<IVar> listVar = _vdt.getTraceCollector().getAllnames();
		ArrayList<IVar> listVar2 = _vdt.getTraceCollector().getSelectedClocks();
		for (IVar var : listVar) {
			if (var.getValueFactory() != null)
				var.getValueFactory().setGhostVisible(true);
		}
		for (IVar var : listVar2) {
			if (var.getValueFactory() != null)
				var.getValueFactory().setGhostVisible(true);
		}
	}

	private Balise createBalise(int x, int y) {
		Balise rect = new Balise();
		rect.setOpaque(false);
		rect.setFill(true);
		rect.setForegroundColor(_mca.colorBlack());
		rect.setBackgroundColor(_mca.colorBlack());
		rect.setBounds(new Rectangle(x, y, 1, 1));
		return rect;
	}

	public ArrayList<ConstraintsConnection> constructPacket(Color color,
			Draw1 f1, Draw1 f2) {
		ArrayList<ConstraintsConnection> res = new ArrayList<ConstraintsConnection>();
		Balise[] tab = new Balise[4];
		tab[0] = createBalise(f1.getBounds().getTopLeft().x - 2, f1.getBounds()
				.getTopLeft().y - 4);
		tab[0].setAnchor(f1);
		tab[0].setLinkTo(Linkto.tl);
		tab[1] = createBalise(f2.getBounds().getTopRight().x + 2, f2
				.getBounds().getTopRight().y - 4);
		tab[1].setAnchor(f2);
		tab[1].setLinkTo(Linkto.tr);
		tab[2] = createBalise(f2.getBounds().getBottomRight().x + 2, f2
				.getBounds().getBottomRight().y + 5);
		tab[2].setAnchor(f2);
		tab[2].setLinkTo(Linkto.br);
		tab[3] = createBalise(f1.getBounds().getBottomLeft().x - 2, f1
				.getBounds().getBottomLeft().y + 5);
		tab[3].setAnchor(f1);
		tab[3].setLinkTo(Linkto.bl);
		for (int i = 0; i < 4; i++) {
			for (Object obj : f1.getParent().getChildren()) {
				if (obj instanceof Balise) {
					Balise rect = (Balise) obj;
					if (tab[i].getBounds().equals(rect.getBounds())) {
						tab[i] = rect;
					}
				}
			}
		}
		f1.getParent().add(tab[0]);
		f1.getParent().add(tab[1]);
		f1.getParent().add(tab[2]);
		f1.getParent().add(tab[3]);
		ChopboxAnchor targetAnchor;
		for (int j = 0; j < 4; j++) {
			ConstraintsConnection poly = new ConstraintsConnection();
			poly.setOpaque(false);
			poly.setLineWidth(2);
			poly.setForegroundColor(color);
			ChopboxAnchor sourceAnchor = new ChopboxAnchor(tab[j]);
			if (j == 3) {
				targetAnchor = new ChopboxAnchor(tab[0]);
			} else
				targetAnchor = new ChopboxAnchor(tab[j + 1]);
			poly.setSourceAnchor(sourceAnchor);
			poly.setTargetAnchor(targetAnchor);
			poly.getInstants().add(f1.getInstant());
			poly.getInstants().add(f2.getInstant());
			res.add(poly);
		}

		return res;
	}

	/* Arrow Motor */
	public int getDateOf(String name, int n) {
		return 0;
	}

}











//package fr.inria.aoste.timesquare.vcd.view;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//
//import org.eclipse.draw2d.ChopboxAnchor;
//import org.eclipse.draw2d.Graphics;
//import org.eclipse.draw2d.IFigure;
//import org.eclipse.draw2d.Label;
//import org.eclipse.draw2d.PolygonDecoration;
//import org.eclipse.draw2d.PolylineConnection;
//import org.eclipse.draw2d.RectangleFigure;
//import org.eclipse.draw2d.XYAnchor;
//import org.eclipse.draw2d.geometry.Dimension;
//import org.eclipse.draw2d.geometry.Point;
//import org.eclipse.draw2d.geometry.PointList;
//import org.eclipse.draw2d.geometry.Rectangle;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.graphics.Color;
//import org.eclipse.swt.widgets.ToolItem;
//
//import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
//import fr.inria.aoste.timesquare.vcd.IVcd;
//import fr.inria.aoste.timesquare.vcd.ListConnections;
//import fr.inria.aoste.timesquare.vcd.figure.Balise;
//import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
//import fr.inria.aoste.timesquare.vcd.figure.Balise.Linkto;
//import fr.inria.aoste.timesquare.vcd.model.Description;
//import fr.inria.aoste.timesquare.vcd.model.IVar;
//import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
//import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
//import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
//import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
//import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;
//
//public class ConstraintsFactory implements IConstructContraint {
//
//	private ListConnections _listConnnections;
//	private IVcd _vdt;
//	private VcdColorPreferences _mca;
//
//	public ConstraintsFactory(ListConnections list, IVcd vdt) {
//		super();
//		this._listConnnections = list;
//		this._vdt = vdt;
//		_mca = vdt.getMca();
//		vdt.getVcdFactory().setConstraintsFactory(this);
//
//	}
//
//	public VcdColorPreferences getMca() {
//		return _mca;
//	}
//
//	public void addToList(ArrayList<ConstraintsConnection> listSync,
//			ConstraintsConnection poly) {
//		ConstraintsConnection polyc = poly;
//		if (polyc.isGlobal() == null) {
//			ErrorConsole.println("Warning :  " + poly.getComment()
//					+ " must be set Global Flag ");
//			System.out.println("Warning :  " + poly.getComment()
//					+ " must be set Global Flag ");
//		}
//		if (polyc.getComment() == null) {
//			ErrorConsole.println("Warning :  must be set  Comment Flag  ");
//			System.out.println("Warning :  must be set  Comment Flag   ");
//		}
//		Iterator<ConstraintsConnection> it = listSync.iterator();
//		boolean state = true;
//		IFigure startAnchor1 = null;
//		IFigure startAnchor2 = null;
//		IFigure endAnchor1 = null;
//		IFigure endAnchor2 = null;
//		while (it.hasNext()) {
//			ConstraintsConnection pc = it.next();
//			Boolean b1= (pc.isGlobal());
//			Boolean b2= (poly.isGlobal());
//			if (b1==null && b2!=null){
//				continue ;
//			}
//			if (b1!=null && b2==null){
//				continue ;
//			}
//			if (b1!=null && b2!=null){
//				if (b1.booleanValue() != b2.booleanValue()){
//					continue;
//				}
//			}
//			if (pc.getComment() != poly.getComment()){
//				continue;
//			}
//			if (pc.getTargetAnchor() != null && polyc.getSourceAnchor() != null
//					&& polyc.getTargetAnchor() != null
//					&& pc.getSourceAnchor() != null) {
//				startAnchor1 = pc.getSourceAnchor().getOwner();
//				startAnchor2 = polyc.getSourceAnchor().getOwner();
//				endAnchor1 = pc.getTargetAnchor().getOwner();
//				endAnchor2 = polyc.getTargetAnchor().getOwner();
//				if (startAnchor1 instanceof PolylineConnection
//						&& startAnchor2 instanceof PolylineConnection
//						&& endAnchor1 instanceof PolylineConnection
//						&& endAnchor2 instanceof PolylineConnection) {
//					if (((PolylineConnection) startAnchor1).getPoints()
//							.getBounds().equals(
//							((PolylineConnection) startAnchor2)
//							.getPoints().getBounds())) {
//						if (((PolylineConnection) endAnchor1).getPoints()
//								.getBounds().equals(
//								((PolylineConnection) endAnchor2)
//								.getPoints().getBounds())) {
//							state = false;
//							listSync.remove(pc);
//							if (_vdt.getCanvas().getContents().getChildren()
//									.contains(pc))
//								_vdt.getCanvas().getContents().remove(pc);
//							Rectangle bounds = pc.getBounds();
//							_vdt.getCanvas().redraw(bounds.x, bounds.y,
//									bounds.width, bounds.height, true);
//							break;
//						}
//					}
//				}
//				if (startAnchor1 instanceof RectangleFigure
//						&& startAnchor2 instanceof RectangleFigure
//						&& endAnchor1 instanceof RectangleFigure
//						&& endAnchor2 instanceof RectangleFigure) {
//					Rectangle rect1 = new Rectangle(
//							((RectangleFigure) startAnchor1).getBounds());
//					Rectangle rect2 = new Rectangle(
//							((RectangleFigure) endAnchor1).getBounds());
//					if (rect1.expand(5, 5).intersects(
//							((RectangleFigure) startAnchor2).getBounds())) {
//						if (rect2.expand(5, 5).intersects(
//								((RectangleFigure) endAnchor2).getBounds())) {
//							state = false;
//							listSync.remove(pc);
//							if (_vdt.getCanvas().getContents().getChildren()
//									.contains(pc))
//								_vdt.getCanvas().getContents().remove(pc);
//							Rectangle bounds = pc.getBounds();
//							_vdt.getCanvas().redraw(bounds.x, bounds.y,
//									bounds.width, bounds.height, true);
//							break;
//						}
//					}
//				}
//				if (startAnchor1 instanceof PolylineConnection
//						&& startAnchor2 instanceof PolylineConnection
//						&& endAnchor1 instanceof RectangleFigure
//						&& endAnchor2 instanceof RectangleFigure) {
//					if (((PolylineConnection) startAnchor1).getPoints()
//							.getBounds().equals(
//							((PolylineConnection) startAnchor2)
//							.getPoints().getBounds())) {
//						Rectangle rect = new Rectangle(
//								((RectangleFigure) endAnchor1).getBounds());
//						if (rect.expand(5, 5).intersects(
//								((RectangleFigure) endAnchor2).getBounds())) {
//							state = false;
//							listSync.remove(pc);
//							if (_vdt.getCanvas().getContents().getChildren()
//									.contains(pc))
//								_vdt.getCanvas().getContents().remove(pc);
//							Rectangle bounds = pc.getBounds();
//							_vdt.getCanvas().redraw(bounds.x, bounds.y,
//									bounds.width, bounds.height, true);
//							break;
//						}
//					}
//				}
//				if (startAnchor1 instanceof RectangleFigure
//						&& startAnchor2 instanceof RectangleFigure
//						&& endAnchor1 instanceof PolylineConnection
//						&& endAnchor2 instanceof PolylineConnection) {
//					Rectangle rect = new Rectangle(
//							((RectangleFigure) startAnchor2).getBounds());
//					if (rect.expand(5, 5).intersects(
//							((RectangleFigure) startAnchor1).getBounds())) {
//						if (((PolylineConnection) endAnchor1).getPoints()
//								.getBounds().equals(
//								((PolylineConnection) endAnchor2)
//								.getPoints().getBounds())) {
//							state = false;
//							listSync.remove(pc);
//							if (_vdt.getCanvas().getContents().getChildren()
//									.contains(pc))
//								_vdt.getCanvas().getContents().remove(pc);
//							Rectangle bounds = pc.getBounds();
//							_vdt.getCanvas().redraw(bounds.x, bounds.y,
//									bounds.width, bounds.height, true);
//							break;
//						}
//					}
//				}
//			} else {
//				if (polyc.getPoints().size() > 2) {
//					for (int i = 0; i < pc.getPoints().size(); i++) {
//						if (i != pc.getPoints().size() - 1) {
//							if (!pc.getPoints().getPoint(i).equals(
//									polyc.getPoints().getPoint(i)))
//								break;
//
//							continue;
//						}
//						state = false;
//						listSync.remove(pc);
//						_vdt.getCanvas().getContents().remove(pc);
//						Rectangle bounds = pc.getBounds();
//						_vdt.getCanvas().redraw(bounds.x, bounds.y,
//								bounds.width, bounds.height, true);
//						break;
//					}
//				}
//			}
//		}
//		if (state)
//			listSync.add(poly);
//	}
//
//	public ConstraintsConnection constructConnection(Color color, Draw1 f1,
//			Draw1 f2) {
//		ConstraintsConnection c = new ConstraintsConnection();
//		c.setForegroundColor(color);
//		c.setLineWidth(2);
//		c.setOpaque(true);
//		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
//		ChopboxAnchor targetAnchor = new ChopboxAnchor(f2);
//		c.setSourceAnchor(sourceAnchor);
//		c.setTargetAnchor(targetAnchor);
//		PolygonDecoration decoration = new PolygonDecoration();
//		PointList decorationPointList = new PointList();
//		decorationPointList.addPoint(-2, 0);
//		decorationPointList.addPoint(-1, 1);
//		decorationPointList.addPoint(0, 0);
//		decorationPointList.addPoint(-1, -1);
//		decoration.setTemplate(decorationPointList);
//		c.setTargetDecoration(decoration);
//		c.setSourceDecoration(decoration);
//		c.getInstants().add(f1.getInstant());
//		c.getInstants().add(f2.getInstant());
//		return c;
//	}
//
//	public ConstraintsConnection constructConnection(Color color, IFigure f1,
//			Point f2) {
//		ConstraintsConnection c = new ConstraintsConnection();
//		c.setForegroundColor(color);
//		c.setLineWidth(2);
//		c.setOpaque(true);
//		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
//		XYAnchor targetAnchor = new XYAnchor(f2);
//		c.setSourceAnchor(sourceAnchor);
//		c.setTargetAnchor(targetAnchor);
//		return c;
//	}
//
//	/**
//	 * @see fr.inria.aoste.timesquare.vcd.view.IConstructContraint#constructDashConnection(org.eclipse.swt.graphics.Color, org.eclipse.draw2d.IFigure,
//	 *      org.eclipse.draw2d.IFigure)
//	 */
//	public ConstraintsConnection constructDashConnection(Color color, Draw1 f1,
//			Draw1 f2, boolean empty) {
//		ConstraintsConnection c = new ConstraintsConnection();
//		c.setForegroundColor(color);
//		c.setLineStyle(Graphics.LINE_DASH);
//		c.setLineWidth(2);
//		c.setOpaque(true);
//		ChopboxAnchor sourceAnchor = new ChopboxAnchor(f1);
//		ChopboxAnchor targetAnchor = new ChopboxAnchor(f2);
//		
//		c.setSourceAnchor(sourceAnchor);
//		c.setTargetAnchor(targetAnchor);
//		PolygonDecoration decoration = new PolygonDecoration();
//		decoration.setTemplate(PolygonDecoration.TRIANGLE_TIP);
//		if (empty) {
//			decoration.setForegroundColor(color);
//			decoration.setBackgroundColor(_mca.colorBlack());
//		}
//		c.setTargetDecoration(decoration);
//		c.getInstants().add(f1.getInstant());
//		c.getInstants().add(f2.getInstant());
//		return c;
//	}
//
//	public ConstraintsConnection constructPolylineConnection(Color color,
//			Point pt1, Point pt2) {
//		ConstraintsConnection poly = new ConstraintsConnection();
//		poly.setPoints(new PointList());
//		poly.setForegroundColor(color);
//		poly.setOutlineXOR(true);
//		poly.setLineWidth(2);
//		poly.setOpaque(true);
//		poly.addPoint(pt1);
//		poly.addPoint(pt2);
//		return poly;
//	}
//
//	/***
//	 * 
//	 * @return index == date of (clk define by name)
//	 * 
//	 */
//
//	public int synchrone(String name, int date) {
//
//		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
//				name);
//		int n = 0;
//		for (IFigure figs : figures) {
//			if (((PolylineConnection) figs).getLocation().x == date) {
//				return n;
//			}
//			n++;
//		}
//		return -1;
//	}
//
//	/***
//	 * 
//	 * @return premier index >= of (clk define by name) /
//	 * 
//	 */
//
//	public int firstAfter(String name, int date) {
//		
//		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
//				name);
//		int n = 0;
//		for (IFigure figs : figures) {
//			if (((PolylineConnection) figs).getLocation().x >= date) {
//				return n;
//			}
//			n++;
//		}
//		return -1;
//	}
//
//
//	/***
//	 * 
//	 * @return premier index > of (clk define by name) /
//	 * 
//	 */
//
//	public int firstAfterStrict(String name, int date) {
//		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
//				name);
//		int n = 0;
//		for (IFigure figs : figures) {
//			if (((PolylineConnection) figs).getLocation().x > date) {
//				return n;
//			}
//			n++;
//		}
//		return -1;
//	}
//
//	/***
//	 * 
//	 * @return dernier index < of (clk define by name) /
//	 * 
//	 */
//
//	public int lastBefore(String name, int px) {
//		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
//				name);
//		int n = 0;
//		int k = -1;
//		for (IFigure figs : figures) {
//			if (((PolylineConnection) figs).getLocation().x < px) {
//				k = n;
//			}
//			n++;
//		}
//		return k;
//	}
//
//	public int lastBeforeweak(String name, int px) {
//		ArrayList<ExtendFigure> figures = _vdt.getVcdFactory().getNameforfigures().get(
//				name);
//		int n = 0;
//		int k = -1;
//		for (IFigure figs : figures) {
//			if (((PolylineConnection) figs).getLocation().x <= px) {
//				k = n;
//			}
//			n++;
//		}
//		return k;
//	}
//
//	public void drawConstraints(IFigure fig, ConstraintCommentCommand cc, String currentclock) {
//		if (cc.getClock().equals(currentclock)) {
//			switch (cc.getFunction()) {
//			case _filteredby:
//			case _synchronizeswith:
//			case _sampledon:
//			case _sustains:
//			case _alternateswith:
//			case _isperiodicon:
//			case _precedes:
//				if (cc.getConstraints() != null) {
//					// cc.getIc().setCc(cc);
//					// cc.getIc().setList(listed);
//					// cc.getIc().setVcdFactory(vdt.getFactory());
//					// cc.getIc().setIcc(this);
//					if (cc.getConstraints().haveAllClockVisible())
//						if (fig instanceof Draw1)
//							cc.getConstraints().draw((Draw1) fig, currentclock);
//				}
//				break;
//			/*
//			  case _periodicon: 
//			 filterByFunction(fig, currentclock, cc, VcdColorPreferences .colorRedCoincidence(), true); break;
//			 */
//			case _restrictedto:
//				// filterByFunction(fig, currentclock, cc, mca.colorRedCoincidence(), true);
//				break;
//			/*
//			  case _alternateswith: 
//			  alternateFunction(fig, currentclock, cc, null, true); break;
//			 */
//			case _oneshoton:
//				// oneShotConstraintFunction(fig, currentclock, cc, null, true);
//				break;
//			case _union:
//				// unionFunction(fig, currentclock, cc, null, true);
//				break;
//			// case _precedes:
//			// precedesFunction(fig, currentclock, cc, VcdColorPreferences.DEFAULT.colorBluePrecedes(), true);
//			// break;
//			default:
//			}
//			for (IFigure f : _listConnnections.getListConstraints()) {
//				Dimension dim = _vdt.getCanvas().getContents()
//						.getPreferredSize();
//				_vdt.getCanvas().getContents().add(f);
//				Rectangle bounds = f.getBounds();
//				_vdt.getCanvas().redraw(bounds.x, bounds.y,
//						bounds.width, bounds.height, true);
//				_vdt.getCanvas().getContents().setPreferredSize(dim);
//			}
//		}
//		if (cc.getReferenceClocks().contains(currentclock)) {
//			switch (cc.getFunction()) {
//			case _filteredby:
//			case _synchronizeswith:
//			case _sampledon:
//			case _sustains:
//			case _alternateswith:
//			case _isperiodicon:
//			case _precedes:
//				if (cc.getConstraints() != null) {
//					// cc.getIc().setCc(cc);
//					// cc.getIc().setList(listed);
//					// cc.getIc().setVcdFactory(vdt.getFactory());
//					// cc.getIc().setIcc(this);
//					if (cc.getConstraints().haveAllClockVisible())
//						if (fig instanceof Draw1)
//							cc.getConstraints().draw((Draw1) fig, currentclock);
//				}
//
//				break;
//			/*
//			 * case _periodicon: filterByFunction(fig, currentclock,
//			 * cc, VcdColorPreferences .colorRedCoincidence(), false); break;
//			 */
//			case _restrictedto:
//				// filterByFunction(fig, currentclock, cc, mca.colorRedCoincidence(), false);
//				break;
//			/*
//			 * case _alternateswith: alternateFunction(fig,
//			 * currentclock, cc, null, false); break;
//			 */
//			case _union:
//				// unionFunction(fig, currentclock, cc, null, false);
//				break;
//			// case _precedes:
//			// precedesFunction(fig, currentclock, cc, VcdColorPreferences.DEFAULT
//			// .colorBluePrecedes(), false);
//			// break;
//			default:
//				break;
//			}
//			for (IFigure f : _listConnnections.getListConstraints()) {
//				Dimension dim = _vdt.getCanvas().getContents()
//						.getPreferredSize();
//				_vdt.getCanvas().getContents().add(f);
//				Rectangle bounds = f.getBounds();
//				_vdt.getCanvas().redraw(bounds.x, bounds.y,
//						bounds.width, bounds.height, true);
//				_vdt.getCanvas().getContents().setPreferredSize(dim);
//			}
//		}
//
//	}
//
//	public boolean drawCheckBoxConstraints(IFigure fig) {
//		System.out.println("size of constraintList: "+ _vdt.getVcdDefinitions().getConstraintList().size());
//		if (fig != null) {
//			Description descr;
//			if (fig instanceof Label)
//				descr = _vdt.getVcdFactory().getFigureForDescription(fig.getParent());
//			else
//				descr = _vdt.getVcdFactory().getFigureForDescription(fig);
//			if (descr == null)
//				return false;
//			String currentclock = descr.getName();
//			if (fig instanceof PolylineConnection || fig instanceof Label) {
//				for ( IConstraintData cc : _vdt.getVcdDefinitions().getConstraintList()) {
//					if (cc instanceof ConstraintCommentCommand )
//					{
//						drawConstraints(fig, (ConstraintCommentCommand) cc, currentclock);
//					}
//					else{
//						if (cc.getConstraints().haveAllClockVisible()){
//							if (fig instanceof Draw1){
//								cc.getConstraints().draw((Draw1) fig, currentclock);
//							}
//						}
//					}
//				}
//				if (fig.getForegroundColor().equals(
//						_mca.colorLightGreenFirable())) {
//					// fig.setForegroundColor(VcdColorPreferences.colorred());
//				} else {
//					fig.setForegroundColor(_mca.colorLightGreenFirable());
//				}
//			}
//
//		}
//		return true;
//	}
//
//	public void addAndRedraw(IFigure figure) {
//		Dimension dim = _vdt.getCanvas().getContents().getPreferredSize();
//		_vdt.getCanvas().getContents().add(figure);
//		Rectangle bounds = figure.getBounds();
//		_vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,	true);
//		_vdt.getCanvas().getContents().setPreferredSize(dim);
//	}
//
//	public void redraw(IFigure figure) {
//		Dimension dim = _vdt.getCanvas().getContents().getPreferredSize();
//		Rectangle bounds = figure.getBounds();
//		_vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width, bounds.height,
//				true);
//		_vdt.getCanvas().getContents().setPreferredSize(dim);
//	}
//
//	public void redrawSyncInterval(ConstraintCommentCommand cc, Color color) {
//		if (cc == null)
//			return;
//		for (IFigure f : _listConnnections.getListInterval().get(cc)) {
//			_vdt.getCanvas().getContents().remove(f);
//		}
//		_listConnnections.getListInterval().get(cc).clear();
//		Iterator<IFigure> it = _listConnnections.lastClock1Get(cc).iterator();
//		Iterator<IFigure> ot = _listConnnections.lastClock2Get(cc).iterator();
//		while (it.hasNext() && ot.hasNext()) {
//			Point pt1 = ((PolylineConnection) it.next()).getPoints()
//					.getPoint(3);
//			Point pt2 = ((PolylineConnection) ot.next()).getPoints()
//					.getPoint(3);
//			ConstraintsConnection poly = null;
//			if (pt1.x > pt2.x) {
//				if (pt1.y > pt2.y) {
//					poly = constructPolylineConnection(color, new Point(
//							pt1.x + 5, pt1.y + 15), new Point(pt1.x + 5,
//							pt2.y - 15));
//				} else {
//					poly = constructPolylineConnection(color, new Point(
//							pt1.x + 5, pt1.y - 15), new Point(pt1.x + 5,
//							pt2.y + 15));
//				}
//				_listConnnections.getListInterval().get(cc).add(poly);
//				_vdt.getCanvas().getContents().add(poly);
//			} else {
//				if (pt2.y > pt1.y) {
//					poly = constructPolylineConnection(color, new Point(
//							pt2.x + 5, pt2.y + 15), new Point(pt2.x + 5,
//							pt1.y - 15));
//				} else {
//					poly = constructPolylineConnection(color, new Point(
//							pt2.x + 5, pt2.y - 15), new Point(pt2.x + 5,
//							pt1.y + 15));
//				}
//				_listConnnections.getListInterval().get(cc).add(poly);
//				_vdt.getCanvas().getContents().add(poly);
//			}
//		}
//	}
//
//	public void drawSyncInterval(ConstraintCommentCommand cc, Color color) {
//		for (IFigure f : _listConnnections.getListInterval().get(cc)) {
//			if (f != null)
//				if (_vdt.getCanvas().getContents() == f.getParent())
//					_vdt.getCanvas().getContents().remove(f);
//		}
//		Dimension dim = _vdt.getCanvas().getContents().getPreferredSize();
//		_listConnnections.clearInterval(cc);
//		String clock1 = null;
//		String clock2 = null;
//		clock1 = cc.getClock();
//		clock2 = cc.getReferenceClocks().get(0);
//		int occurencesclock1 = Integer.valueOf(cc.getReferenceClocks().get(1));
//		int occurencesclock2 = Integer.valueOf(cc.getReferenceClocks().get(2));
//		if (_vdt.getVcdFactory().getNameforfigures().get(clock1) == null)
//			return;
//		if (_vdt.getVcdFactory().getNameforfigures().get(clock2) == null)
//			return;
//		_listConnnections.lastClock1Get(cc).clear();
//		_listConnnections.lastClock2Get(cc).clear();
//		for (int i = 0; i < _vdt.getVcdFactory().getNameforfigures().get(clock1)
//				.size(); i++) {
//			if ((i + 1) % occurencesclock1 == 0) {
//				_listConnnections.lastClock1Get(cc)
//						.add(
//						_vdt.getVcdFactory().getNameforfigures()
//						.get(clock1).get(i));
//				_listConnnections.firstClock1Get(cc).add(
//						_vdt.getVcdFactory().getNameforfigures().get(clock1).get(
//						i - (occurencesclock1 - 1)));
//			}
//		}
//		for (int i = 0; i < _vdt.getVcdFactory().getNameforfigures().get(clock2)
//				.size(); i++) {
//			if ((i + 1) % occurencesclock2 == 0) {
//				_listConnnections.lastClock2Get(cc)
//						.add(
//						_vdt.getVcdFactory().getNameforfigures()
//						.get(clock2).get(i));
//				_listConnnections.firstClock2Get(cc).add(
//						_vdt.getVcdFactory().getNameforfigures().get(clock2).get(
//						i - (occurencesclock2 - 1)));
//			}
//		}
//		Iterator<IFigure> it = _listConnnections.lastClock1Get(cc).iterator();
//		Iterator<IFigure> ot = _listConnnections.lastClock2Get(cc).iterator();
//		while (it.hasNext() && ot.hasNext()) {
//			Point pt1 = ((PolylineConnection) it.next()).getPoints()
//					.getPoint(3);
//			Point pt2 = ((PolylineConnection) ot.next()).getPoints()
//					.getPoint(3);
//			ConstraintsConnection poly = null;
//			if (pt1.x > pt2.x) {
//				if (pt1.y > pt2.y) {
//					poly = constructPolylineConnection(color, new Point(
//							pt1.x + 5, pt1.y + 15), new Point(pt1.x + 5,
//							pt2.y - 15));
//				} else {
//					poly = constructPolylineConnection(color, new Point(
//							pt1.x + 5, pt1.y - 15), new Point(pt1.x + 5,
//							pt2.y + 15));
//				}
//
//			} else {
//				if (pt2.y > pt1.y) {
//					poly = constructPolylineConnection(color, new Point(
//							pt2.x + 5, pt2.y + 15), new Point(pt2.x + 5,
//							pt1.y - 15));
//				} else {
//					poly = constructPolylineConnection(color, new Point(
//							pt2.x + 5, pt2.y - 15), new Point(pt2.x + 5,
//							pt1.y + 15));
//				}
//
//			}
//			_listConnnections.getListInterval().get(cc).add(poly);
//			if (poly != null)
//				_vdt.getCanvas().getContents().add(poly);
//
//		}
//
//		_vdt.getCanvas().getContents().setPreferredSize(dim);
//	}
//
//	public IVcd getVdt() {
//		return _vdt;
//	}
//
//	public void hideCoincidence() {
//		for (IFigure figs : _listConnnections.getListCoincidence()) {
//			figs.setForegroundColor(_mca.colorLightGreenFirable());
//		}
//		_listConnnections.getListCoincidence().clear();
//	}
//
//	public void hideConstraints() {
//		if (_vdt.getToolbar().getItems().length == 0)
//			return;
//		for (ToolItem item : _vdt.getToolbar().getItems()) {
//			item.setText("");
//		}
//	}
//
//	public void addhiddenGhost(RectangleFigure rect) {
//		RectangleFigure temp = new RectangleFigure();
//		PolylineConnection poly = new PolylineConnection();
//		PointList pl = new PointList();
//		poly.setPoints(pl);
//		temp.setOpaque(false);
//		temp.setFill(true);
//		poly.setOpaque(false);
//		poly.setFill(true);
//		Rectangle bounds = rect.getBounds();
//		temp.setForegroundColor(_mca.colorBlack());
//		temp.setBackgroundColor(_mca.colorBlack());
//		poly.setForegroundColor(_mca.colorLightGreenFirable());
//		poly.setBackgroundColor(_mca.colorLightGreenFirable());
//		temp.setBounds(bounds);
//		pl.addPoint(new Point(bounds.x, bounds.y + bounds.height - 1));
//		pl.addPoint(new Point(bounds.x + bounds.width, bounds.y + bounds.height
//				- 1));
//		poly.setPoints(pl);
//		if (rect.getParent() != null) {
//			rect.getParent().add(temp);
//			rect.getParent().add(poly);
//		}
//		_listConnnections.listGhostAdd(temp);
//		_listConnnections.listGhostAdd(poly);
//
//	}
//
//	public void hideGhost(int index) {
//		try {
//			IVar var = _vdt.getTraceCollector().getSelectedClocks().get(index);
//			var.getValueFactory().setGhostVisible(false);
//		} catch (Throwable e) {
//		}
//
//	}
//
//	public void hideAllGhost() {
//		ArrayList<IVar> listVar = _vdt.getTraceCollector().getAllnames();
//		ArrayList<IVar> listVar2 = _vdt.getTraceCollector().getSelectedClocks();
//		for (IVar var : listVar) {
//			if (var.getValueFactory() != null)
//				var.getValueFactory().setGhostVisible(false);
//		}
//		for (IVar var : listVar2) {
//			if (var.getValueFactory() != null)
//				var.getValueFactory().setGhostVisible(false);
//		}
//	}
//
//	public void showConstraint(IFigure fig) {
//		if (fig != null) {
//			if (fig instanceof PolylineConnection || fig instanceof Label) {
//				Description descr = null;
//				if (fig instanceof PolylineConnection)
//					descr = _vdt.getVcdFactory().getFigureForDescription(fig);
//				if (fig instanceof Label)
//					descr = _vdt.getVcdFactory().getFigureForDescription(fig.getParent());
//				if (descr == null)
//					return;
//				String currentclock = descr.getName();
//				int i = 0;
//				for (ConstraintCommentCommand cc : _vdt.getVcdDefinitions()
//						.getConstraintCommentCommand()) {
//					if (cc.getClock().equals(currentclock)
//							|| cc.getReferenceClocks().contains(currentclock)) {
//						if (_vdt.getToolbar() == null)
//							return;
//						for (ToolItem item : _vdt.getToolbar().getItems()) {
//							if (item.getText().equals(cc.toString()))
//								return;
//						}
//						String fonction = "";
//						switch (cc.getFunction()) {
//						default:
//							fonction = cc.toString();
//						}
//						ToolItem item;
//						if (i >= _vdt.getToolbar().getItems().length) {
//							item = new ToolItem(_vdt.getToolbar(), SWT.PUSH);
//						}
//						item = _vdt.getToolbar().getItems()[i];
//						item.setText(fonction);
//						i++;
//					}
//				}
//			}
//		}
//	}
//
//	public void showGhost(int index) {
//		try {
//			IVar var = _vdt.getTraceCollector().getSelectedClocks().get(index);
//			var.getValueFactory().setGhostVisible(true);
//		} catch (Throwable e) {
//
//		}
//	}
//
//	public void showAllGhost() {
//		ArrayList<IVar> listVar = _vdt.getTraceCollector().getAllnames();
//		ArrayList<IVar> listVar2 = _vdt.getTraceCollector().getSelectedClocks();
//		for (IVar var : listVar) {
//			if (var.getValueFactory() != null)
//				var.getValueFactory().setGhostVisible(true);
//		}
//		for (IVar var : listVar2) {
//			if (var.getValueFactory() != null)
//				var.getValueFactory().setGhostVisible(true);
//		}
//	}
//
//	private Balise createBalise(int x, int y) {
//		Balise rect = new Balise();
//		rect.setOpaque(false);
//		rect.setFill(true);
//		rect.setForegroundColor(_mca.colorBlack());
//		rect.setBackgroundColor(_mca.colorBlack());
//		rect.setBounds(new Rectangle(x, y, 1, 1));
//		return rect;
//	}
//
//	public ArrayList<ConstraintsConnection> constructPacket(Color color,
//			Draw1 f1, Draw1 f2) {
//		ArrayList<ConstraintsConnection> res = new ArrayList<ConstraintsConnection>();
//		Balise[] tab = new Balise[4];
//		tab[0] = createBalise(f1.getBounds().getTopLeft().x - 2, f1.getBounds()
//				.getTopLeft().y - 4);
//		tab[0].setAnchor(f1);
//		tab[0].setLinkTo(Linkto.tl);
//		tab[1] = createBalise(f2.getBounds().getTopRight().x + 2, f2
//				.getBounds().getTopRight().y - 4);
//		tab[1].setAnchor(f2);
//		tab[1].setLinkTo(Linkto.tr);
//		tab[2] = createBalise(f2.getBounds().getBottomRight().x + 2, f2
//				.getBounds().getBottomRight().y + 5);
//		tab[2].setAnchor(f2);
//		tab[2].setLinkTo(Linkto.br);
//		tab[3] = createBalise(f1.getBounds().getBottomLeft().x - 2, f1
//				.getBounds().getBottomLeft().y + 5);
//		tab[3].setAnchor(f1);
//		tab[3].setLinkTo(Linkto.bl);
//		for (int i = 0; i < 4; i++) {
//			for (Object obj : f1.getParent().getChildren()) {
//				if (obj instanceof Balise) {
//					Balise rect = (Balise) obj;
//					if (tab[i].getBounds().equals(rect.getBounds())) {
//						tab[i] = rect;
//					}
//				}
//			}
//		}
//		f1.getParent().add(tab[0]);
//		f1.getParent().add(tab[1]);
//		f1.getParent().add(tab[2]);
//		f1.getParent().add(tab[3]);
//		ChopboxAnchor targetAnchor;
//		for (int j = 0; j < 4; j++) {
//			ConstraintsConnection poly = new ConstraintsConnection();
//			poly.setOpaque(false);
//			poly.setLineWidth(2);
//			poly.setForegroundColor(color);
//			ChopboxAnchor sourceAnchor = new ChopboxAnchor(tab[j]);
//			if (j == 3) {
//				targetAnchor = new ChopboxAnchor(tab[0]);
//			} else
//				targetAnchor = new ChopboxAnchor(tab[j + 1]);
//			poly.setSourceAnchor(sourceAnchor);
//			poly.setTargetAnchor(targetAnchor);
//			poly.getInstants().add(f1.getInstant());
//			poly.getInstants().add(f2.getInstant());
//			res.add(poly);
//		}
//
//		return res;
//	}
//
//	/* Arrow Motor */
//	public int getDateOf(String name, int n) {
//		return 0;
//	}
//
//}