/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.command;

import java.util.ArrayList;
import java.util.Iterator;
import fr.inria.aoste.timesquare.vcd.model.ISimulationCommand;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.ValueChange;
import fr.inria.aoste.timesquare.vcd.model.keyword.SimulationKeyword;
import fr.inria.aoste.timesquare.vcd.model.visitor.ISimulationCollector;

/**
 * Represents a single time instant of the VCD. Used to store all the ValueChange objects that give
 * the actual value of the variables of the VCD.
 * 
 * @author bferrero
 * @author mzc
 *
 */
public class SimulationCommand implements ISimulationCommand {
	
	/**
	 * Represents respectively the value for each clock at a given time (_time)
	 * For example, if we generate five clocks, each time when we create an instance of SimulationCommand,
	 * the size of _changes will be set to be five.
	 */
	private ArrayList<ValueChange> _changes = new ArrayList<ValueChange>();
	private int _time;
	private SimulationKeyword _keyword; // null if normal dump
	private VCDDefinitions _vcdModel;

	public SimulationCommand(int time, VCDDefinitions vcdModel) {
		this._time = time;
		this._keyword = null;
		this._vcdModel = vcdModel;
	}

	public SimulationCommand(SimulationKeyword keyword) {
		this._time = 0;
		this._keyword = keyword;
	}
	
	public  ISimulationCommand cloneCommand(){
		SimulationCommand sc=new SimulationCommand(_time, _vcdModel);
		sc._keyword= this._keyword;
		sc._changes= new ArrayList<ValueChange>(_changes);
		return sc;
	}

	public void merge(SimulationCommand other) {
		this._keyword = other._keyword;
		this._changes = other._changes;
	}

	/**
	 * Change the value of one clock at a given time
	 * 
	 * @param vc Used as ScalerValueChange(value,indent)
	 */
	public void addValueChange(ValueChange vc) {
		_changes.add(vc);
//		if (_vcdModel != null) {
//			_vcdModel.addClockLineData(vc, _time);
//		}
	}

	public Iterator<ValueChange> iterator() {
		return _changes.iterator();
	}

	
	/**
	 * <li>create the Timeline with an associated _time</li>
	 * <li>create clock pieces at the given time (_time)
	 */
	public void visit(ISimulationCollector visitor) {
		
		visitor.visitTimeLine(_time);

		visitor.visitKeyword(_keyword);
		for (ValueChange vc : _changes){
			visitor.visitValueChange(vc,_time);
		}
		
		visitor.visitEnd(_keyword);
	}
	
	//Old version
/*	public void visit(ISimulationCollector visitor) {
		

		visitor.visitTimeLine(_time);

		visitor.visitKeyword(_keyword);

		for (ValueChange vc : _changes){
			visitor.visitValueChange(vc);
		}

		visitor.visitEnd(_keyword);
	} */

	public int getTime() {
		return _time;
	}

	public final void setTime(int time) {
		this._time = time;
	}
	
	public VCDDefinitions getVcdDefinitions(){
		return _vcdModel;
	}
	
	public void setVcdDefinitions(VCDDefinitions vcdDef){
		this._vcdModel = vcdDef;
	}
	
}
