/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class Description {
	
	private String _name;
	private String _aliasStr;
	private int _index = -1;
	private String _description;
	private ArrayList<ExtendFigure> _figures = new ArrayList<ExtendFigure>();
	private int _time = -1;

	public Description(String name, String alias, int index, ArrayList<ExtendFigure> figures,int time) {
		super();
		this._name = name;
		if (alias==null){
			_aliasStr=name;
		}
		else{
			_aliasStr=alias;
		}
		this._index = index;
		this._figures = figures;
		this._time = time;
	}

	public Description(String name,String _alias, int index, String description,
			ArrayList<ExtendFigure> figures, int time) {
		this._name = name;
		if (_alias==null){
			_aliasStr=name;
		}else{
			_aliasStr=_alias;
		}
		this._index = index;
		this._description = description;
		this._figures = figures;
		this._time = time;
	}

	public Description(String name,String _alias, String description, int time) {
		this._name = name;
		if (_alias==null){
			_aliasStr=name;
		}else{
			_aliasStr=_alias;
		}
		this._description = description;
		this._time = time;
	}

	public Description(String name,String _alias, int time) {
		super();
		this._name = name;
		if (_alias==null){
			_aliasStr=name;
		}else{
			_aliasStr=_alias;
		}
		this._time = time;
	}

	@Override
	public int hashCode() {
		int n=_name!=null ? _name.hashCode() : 0;
		int d=_description!=null ? _description.hashCode() : 0;
		return n+d;
	}

	@Override
	public boolean equals(Object obj) {
		if (!( obj instanceof Description))
			return false;
		Description descr = (Description) obj;
		try{
			if (descr._name.equals(_name) && descr._description.equals(_description)){
				return true;
			}
		}
		catch (Throwable e) {}
		return false;
	}

	public String getName() {
		return _name;
	}

	public String getAlias() {
		return _aliasStr;
	}

	public String getDescription() {
		return _description;
	}

	@Override
	public String toString() {
		return _name + " " + _description;
	}

	public ArrayList<ExtendFigure> getFigures() {
		return _figures;
	}

	public int getIndex() {
		return _index;
	}

	public int getTime() {
		return _time;
	}
}
