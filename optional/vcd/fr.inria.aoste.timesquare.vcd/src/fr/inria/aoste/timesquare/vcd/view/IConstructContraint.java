/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;

public interface IConstructContraint {

	public ConstraintsConnection constructDashConnection(Color color,
			Draw1 f1, Draw1 f2, boolean empty);

	
	
	public void addToList(ArrayList<ConstraintsConnection> listSync,
			ConstraintsConnection poly);

	public ConstraintsConnection constructCoincidenceConnection(Color color, Draw1 f1,
			Draw1 f2);

	public ConstraintsConnection constructPolylineConnection(Color color,
			Point pt1, Point pt2);

	public ConstraintsConnection constructConnection(Color color, IFigure f1,
			Point f2);

	public ArrayList<ConstraintsConnection> constructPacket(Color color,
			Draw1 f1, Draw1 f2);

	public int synchrone(String name, int px);

	public int firstAfter(String name, int px);

	public int firstAfterStrict(String name, int px);

	public int lastBefore(String name, int px);

	public int lastBeforeweak(String name, int px);
	
	public void addAndRedraw(IFigure figure) ;

	public void redraw(IFigure figure);
	
	public ConstraintsConnection constructCoincidenceConnection(Color color, Eventline c1, int c1_tickNumber,Eventline c2, int c2_tickNumber);
	
	public ConstraintsConnection constructDashConnection(Color color, Eventline c1, int c1_tickNumber,Eventline c2, int c2_tickNumber, boolean empty);

}