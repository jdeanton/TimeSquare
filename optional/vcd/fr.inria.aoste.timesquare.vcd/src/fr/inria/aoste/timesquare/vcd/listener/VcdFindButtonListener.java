/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.listener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Panel;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.dialogs.FindDialog;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class VcdFindButtonListener implements SelectionListener {
	
	private ListConnections _listConnections;
	private IVcdDiagram _vcdDiagram;
	private IFigure _poly;
	int _reponse;
	int _x;
	VcdColorPreferences _mca;
	
	public VcdFindButtonListener(ListConnections list, IVcdDiagram vdt) {
		this._listConnections = list;
		this._vcdDiagram = vdt;
		_mca=vdt.getColorPreferences();
	}

	public void widgetDefaultSelected(SelectionEvent e) {}

	public void widgetSelected(SelectionEvent e) {
		FindDialog inputDialog = new FindDialog(Display.getCurrent()
				.getActiveShell(), "Find", "Clock name", "Instance", "", _vcdDiagram);
		_reponse = inputDialog.open();
		if (_reponse == Window.OK) {
			Description d = new Description(inputDialog.getClock(),null, inputDialog
					.getInstance(), -1);
			for (Object obj : _vcdDiagram.getCanvas().getContents().getChildren()) {
				Panel panel = null;
				if (obj instanceof Panel){
					panel = (Panel) obj;
				}else{
					continue;
				}
				for (Object f : panel.getChildren()) {
					_poly = (IFigure) f;
					Description descr = _vcdDiagram.getVcdFactory()
							.getFigureForDescription(_poly);
					if (descr == null)
						continue;
					if (d.equals(descr)) {
						_x = _poly.getBounds().x;
						_poly.setForegroundColor(_mca.colorRedFind());
						_vcdDiagram.getCanvas().scrollTo(
								_x - (_vcdDiagram.getCanvas().getSize().x / 2), 0);
						_listConnections.getListFind().add(_poly);
						return;
					}
				}
			}
			MessageBox messageBox = new MessageBox(Display.getCurrent()
					.getActiveShell(), SWT.ICON_ERROR | SWT.OK);
			messageBox.setText("Warning");
			messageBox.setMessage("Figure introuvable !");
			messageBox.open();
		}
	}
}
