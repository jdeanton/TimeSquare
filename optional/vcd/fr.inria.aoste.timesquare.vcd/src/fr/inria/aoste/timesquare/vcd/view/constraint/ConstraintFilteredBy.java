/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class ConstraintFilteredBy extends AbsConstraint implements IConstraint {

	public ConstraintFilteredBy() {
		super();
		testn = 1;

	}

	public int draw(Draw1 currentfig, String currentclock) {
		boolean state = true;
		if (cc.getReferenceClocks().get(0).equals(currentclock))
			state = false;
		ArrayList<ExtendFigure> figures = null;
		if (state)
			figures = vcdFactory.getNameforfigures().get(
					cc.getReferenceClocks().get(0));
		else
			figures = vcdFactory.getNameforfigures().get(cc.getClock());
		Draw1 dest = null;
		boolean state2 = false;
		for (IFigure figs : figures) {
			if (((PolylineConnection) figs).getLocation().x == ((PolylineConnection) currentfig)
					.getLocation().x) {
				if (figs instanceof Draw1) {
					dest = (Draw1) figs;
					state2 = true;
					break;
				}
			}
		}
		if (!state2)
			return 0;
		ConstraintsConnection poly = null;
		if (state)
			poly = icc.constructCoincidenceConnection(mca.colorRedCoincidence(), dest,
					currentfig);
		else
			poly = icc.constructCoincidenceConnection(mca.colorRedCoincidence(),
					currentfig, dest);
		poly.setComment(cc);
		poly.setGlobal(isGlobal);
		icc.addToList(list.getListConstraints(), poly);

		return 0;
	}

	public int drawTableItemConstraints() {
		try {
			isGlobal = true;
			String nameClock = cc.getClock();
			if (haveAllClockVisible()) {
				for (IFigure fig : vcdFactory.getNameforfigures()
						.get(nameClock)) {
					if (fig instanceof Draw1) {
						draw((Draw1)fig, nameClock);
					}
				}
			} else
				return -1;
			IVcdDiagram vdt = vcdFactory.getVcddia();
			for (IFigure f : list.getListConstraints()) {
				Dimension dim = vdt.getCanvas().getContents()
						.getPreferredSize();
				vdt.getCanvas().getContents().add(f);
				Rectangle bounds = f.getBounds();
				vdt.getCanvas().redraw(bounds.x, bounds.y, bounds.width,
						bounds.height, true);
				vdt.getCanvas().getContents().setPreferredSize(dim);
			}
			isConstraintVisible = true;
			isGlobal = false;
			return vcdFactory.getNameforfigures().get(nameClock).size();
		} catch (Throwable t) {
			ErrorConsole.printError(t);
			isGlobal = false;
			return -1;
		}
	}

	@Override
	public int drawOneTick(int ticknum, String clockId) {
		// TODO Auto-generated method stub
		return 0;
	}

}
