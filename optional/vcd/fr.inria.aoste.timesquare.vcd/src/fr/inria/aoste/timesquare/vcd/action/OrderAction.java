/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.action;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.dialogs.SelectClocksDialog;

public class OrderAction extends Action {

	private final IVcdDiagram ivcddiagram;

	public OrderAction(IVcdDiagram ivcd) {
		this.ivcddiagram = ivcd;
	}

	@Override
	public void run() {
		try {

			Shell shell = new Shell();
			SelectClocksDialog d = new SelectClocksDialog(shell, ivcddiagram
					.getTraceCollector().getAllnames(), ivcddiagram.getTraceCollector()
					.getSelectedClocks(), "Others clocks", "Displayed clocks",
					"Selected clocks dialog");
			d.create();
			int ret = d.open();
			if (ret == Window.OK) {

				d.apply(ivcddiagram);
				ivcddiagram.setFocus();
			}
		} catch (Throwable ex) {
			ErrorConsole.printError(ex);
		}

	}
}