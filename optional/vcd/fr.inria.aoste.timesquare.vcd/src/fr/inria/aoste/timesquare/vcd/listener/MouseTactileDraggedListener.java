/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.listener;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;

public class MouseTactileDraggedListener implements MouseListener, MouseMoveListener{
	private IVcdDiagram _vcdDiagram;
	
	private int _startX = 0;
	
	private int _currentX = 0;
	
	private FigureCanvas _eventCanvas = null;
	
	private Boolean _triggered = false;
	
	public MouseTactileDraggedListener(IVcdDiagram vcdDiagram) {
		this._vcdDiagram = vcdDiagram;
	}

	@Override
	public void mouseMove(MouseEvent e) {
		// TODO Auto-generated method stub

		if(_triggered){
			int distanceX = _startX - e.x;
			int x = distanceX+_currentX;
			if( x>_vcdDiagram.getCanvas().getHorizontalBar().getMaximum())  {
				return;
			}
			_eventCanvas.scrollToX(x);
			_eventCanvas.getHorizontalBar().setSelection(x);
			_vcdDiagram.getScaleCanvas().scrollToX(_eventCanvas.getHorizontalBar().getSelection());
			_vcdDiagram.getScaleCanvas().getHorizontalBar().setSelection(_eventCanvas.getHorizontalBar().getSelection());
			_vcdDiagram.getfcb().scrollUpdate();
		}
	}

	@Override
	public void mouseDoubleClick(MouseEvent e) {
	}

	@Override
	public void mouseDown(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.button == 1){
			_triggered = true;
			_eventCanvas = _vcdDiagram.getCanvas();
			_startX = e.x;
			_currentX = _eventCanvas.getHorizontalBar().getSelection();
		}
	}

	@Override
	public void mouseUp(MouseEvent e) {
		_triggered = false;
		_startX = 0;
	}


}
