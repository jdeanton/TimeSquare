/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.menu;

public enum Mode {
	show(0), hide(1), partial(2);
	final int n;

	private Mode(int n) {
		this.n = n;
	}

	public boolean isHide() {
		return this == hide;
	}

	public boolean isShow() {
		return this == show;
	}

	public boolean isPartial() {
		return this == partial;
	}
	
	public static  Mode bool2Mode( boolean md)
	{
		if (md){
			return Mode.show;
		}else{
			return Mode.hide;
		}
	}
}
