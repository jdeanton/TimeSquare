/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.figure;

import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;

import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public class DrawZ extends PolylineConnection implements ExtendFigure {

	private int length;
	protected int oldvalue = 0;
	private Point tr = new Point(0, 0);
	private VcdFactory vcd;
	private boolean bool =false;
	private int x;
	private int y;
	private Level precedeLevel;
	private Level futurLevel;

	public DrawZ(VcdFactory vcd, int x, int y, int length, boolean b) {
		super();
		this.vcd = vcd;
		this.x = x;
		this.y = y;
		this.length = length;
		bool=b;
		CountFigure.inc();
	}

	
	public int incLength(int l) {
		length += l;
		return length;
	}
	
	public int getLength() {
		return length;
	}

	public Point getTr() {
		return tr;
	}

	public boolean isGhost() {
		return false;
	}

	public int mycompute() {
		double zoom = 1.0;
		setPoints(new PointList());
		if (bool){
			setForegroundColor(vcd.getMca().colorYellowZ());
		}else{
			setForegroundColor(vcd.getMca().colorTimerZ());
		}
		setOpaque(false);
		int mx = (int) (x * zoom);
		if (vcd.getVcdZoom() != null){
			mx = vcd.getVcdZoom().computeposition(x);
		}
		addPoint(new Point(mx, y + vcd.getHalfHeight()).translate(tr));
		int x2 = x + length;
		mx = (int) (x2 * zoom);
		if (vcd.getVcdZoom() != null){
			mx = vcd.getVcdZoom().computeposition(x2);
		}
		addPoint(new Point(mx, y + vcd.getHalfHeight()).translate(tr));
		return 0;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void setoldValue(int n) {
		oldvalue = n;
	}

	public void setTr(Point tr) {
//		tr.x = tr.x - 40;
		this.tr = tr;
	}

	public Level getLevel() {
		return Level.z;
	}

	public final Level getPrecedeLevel() {
		return precedeLevel;
	}

	public final void setPrecedeLevel(Level precedeLevel) {
		this.precedeLevel = precedeLevel;
	}

	public final Level getFuturLevel() {
		return futurLevel;
	}

	public final void setFuturLevel(Level futurLevel) {
		this.futurLevel = futurLevel;
	}

	@Override
	protected void finalize() throws Throwable {
		CountFigure.dec();
		super.finalize();
	}
}
