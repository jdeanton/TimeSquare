/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

import java.util.ArrayList;

import org.eclipse.core.resources.IContainer;

import fr.inria.aoste.timesquare.vcd.label.DefaultTimeLabelling;
import fr.inria.aoste.timesquare.vcd.label.ITimeLabelling;
import fr.inria.aoste.timesquare.vcd.model.command.ScopeCommand;
import fr.inria.aoste.timesquare.vcd.model.command.TimeScaleCommand;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.aoste.timesquare.vcd.model.comment.DecodeDictionary;
import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
import fr.inria.aoste.timesquare.vcd.model.comment.IDecodeComment;
import fr.inria.aoste.timesquare.vcd.model.comment.ModelRelationComment;
import fr.inria.aoste.timesquare.vcd.model.comment.StatusCommentCommand;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;
import fr.inria.aoste.timesquare.vcd.model.visitor.IScopeVisitor;
import fr.inria.aoste.timesquare.vcd.model.visitor.ISimulationCollector;
import fr.inria.aoste.timesquare.vcd.model.visitor.StringVisitor;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

/**
 * This class represents the root of a model of one VCD diagram.
 * @author Benoit Ferrero
 *
 */
final public class VCDDefinitions {

	private ClockListener _clocklistener = new ClockListener();
	/**
	 * Root of the ScopeCommand tree.
	 */
	private ScopeCommand _scopeCommand = null;
	/**
	 * The list of all {@link IDeclarationCommand} objects of this VCD diagram.
	 */
	private ArrayList<IDeclarationCommand> _arraylistDeclarationCommand = new ArrayList<IDeclarationCommand>();
	private int _pulse = -1;
	/**
	 * An arrayList which contains all the information for graphical drawings. Each {@link ISimulationCommand}
	 * object represents a single instant of the VCD and contains all the values for the VCD variables (ie.
	 * clocks in the CCSL simulation). The values are represented with instances of a subclass {@link ValueChange}. 
	 * @see #addSimulation(ISimulationCommand)
	 */
	private ArrayList<ISimulationCommand> _arraylistSimulationCommand = new ArrayList<ISimulationCommand>();
	private int _offSet = -1;
	private VcdFactory _vcdFactory = null;
	
//	private HashMap<String, IntArrayList> _clockLinesData = new HashMap<String, IntArrayList>();
//	
//	public HashMap<String, IntArrayList> getClockLinesData() {
//		return _clockLinesData;
//	}

	/**
	 * The time label for the scale
	 */
	ITimeLabelling _timeLabelling= new DefaultTimeLabelling();
	
	ArrayList<IConstraintData>  _constraintList= new ArrayList<IConstraintData>();
	
	IContainer _vcdFolder;
	
	public VCDDefinitions() {
		super();
		_timeLabelling.setVcdDefinitions(this);
	}


	public final VcdFactory getFactory() {
		return _vcdFactory;
	}

	public void setScope(ScopeCommand scopeCommand) {
		this._scopeCommand = scopeCommand;
		if (scopeCommand!=null){
			_clocklistener.add(scopeCommand);
			scopeCommand.setClockListener(_clocklistener);
		}
	}

	public void addDefinition(IDeclarationCommand declarationCommand) {
		if (declarationCommand != null) {
			_arraylistDeclarationCommand.add(declarationCommand);
			if (declarationCommand instanceof ICommentCommand) {
				_clocklistener.add((ICommentCommand) declarationCommand);
				((ICommentCommand) declarationCommand).validate(this);
			}
		}
	}

	public final void setFactory(VcdFactory vcdFactory) {
		this._vcdFactory = vcdFactory;
		if (vcdFactory!=null && _constraintList!=null){
			for (IConstraintData _constraintData : _constraintList) {				
				if (_constraintData.getConstraints()!=null )
				{
					if (_constraintData.getConstraints().getIcc()==null){			
						_constraintData.getConstraints().setIcc(vcdFactory.getConstraintsFactory());
					}
					if (_constraintData.getConstraints().getList()==null){
						_constraintData.getConstraints().setList(vcdFactory.getVcddia().getListConnections());
					}
				}
			}
		}
	}

	public void addSimulation(ISimulationCommand simulationCommand) {
		this._arraylistSimulationCommand.add(simulationCommand);
	}
	
	/**
	 * 
	 * @param vc
	 * @param _time
	 * @author mzc
	 */
//	public void addClockLineData(ValueChange vc, int _time) {
//		//TODO
//		String clockIdentifier = vc.getIdentifierCode();
//		IntArrayList line = _clockLinesData.get(clockIdentifier);		
//		if (line == null) {
//			line = new IntArrayList();
//			_clockLinesData.put(clockIdentifier, line);
//		}
//		if (vc instanceof ScalarValueChange) {
//			Value v = (Value) vc.getValue();
//			if (v == Value._1) {
//				line.add(_time);
//			}
//		}
//	}

	public Object visit(StringVisitor stringVisitor) {
		IDeclarationVisitor declarationVisitor = stringVisitor.getDeclarationVisitor();
		if (declarationVisitor != null){
			visit(declarationVisitor);
		}
		IScopeVisitor scopeVisitor = stringVisitor.getScopeVisitor();
		if (scopeVisitor != null){
			visit(scopeVisitor);
		}
		if (declarationVisitor != null){
			declarationVisitor.visitEndDefinitions();
		}
		ISimulationCollector simulationVisitor = (ISimulationCollector) stringVisitor.getSimulationVisitor();
		if (simulationVisitor != null){
			visit(simulationVisitor);
		}
		return stringVisitor;
	}

	public void visit(IDeclarationVisitor visitor) {
		if (visitor == null){
			return;
		}
		for (IDeclarationCommand dc : _arraylistDeclarationCommand){
			dc.visit(visitor);
		}
	}

	public void visit(IScopeVisitor visitor) {
		if (_scopeCommand == null){
			throw new RuntimeException("VCD without scope");
		}
		_scopeCommand.visit(visitor);
	}

	
	/**
	 * @param simulationCollector
	 * @author mzc
	 */
	public void visit(ISimulationCollector simulationCollector) {
		for (ISimulationCommand sc : _arraylistSimulationCommand) {
			sc.visit(simulationCollector);
		}
		simulationCollector.endSimulation();
	}
	
	//Old version
/*	public void visit(ISimulationCollector simulationCollector) {
		int time=1;		
		for (ISimulationCommand simulationCommand : _arraylistSimulationCommand) {
			System.out.println("Simulation Time: "+simulationCommand.getTime());
			if ( time < simulationCommand.getTime()){
				time = simulationCommand.getTime();
			}
		} 		
		double log=Math.log10((time / 100.0)); 
		double ceil= Math.ceil(log);
		int scaleRange=(int) Math.pow(10, Math.max(1,ceil));
		_vcdFactory.setScaleRange(scaleRange);

		for (ISimulationCommand sc : _arraylistSimulationCommand) {
			sc.visit(simulationCollector);
		}
		simulationCollector.endSimulation();
	} */

	public void visit(ISimulationCollector collector, int n1, int n2, boolean b) {
		int n = _arraylistSimulationCommand.size();
		n2 = n2 < n ? n2 : n;
		for (int i = n1; i < n2; i++) {
			_arraylistSimulationCommand.get(i).visit(collector);
		}
		if (b){
			collector.endSimulation();
		}
	}

	//TODO Old version
	/*
	 	public void visit(ISimulationCollector collector, int n1, int n2, boolean b) {
		int n = _arraylistSimulationCommand.size();
		n2 = n2 < n ? n2 : n;
		System.out.println("n:"+n + " n2:"+n2 + " n1:"+n1);
		for (int i = n1; i < n2; i++) {
			_arraylistSimulationCommand.get(i).visit(collector);
		}
		if (b){
			collector.endSimulation();
		}
	}
	 */

	public ArrayList<StatusCommentCommand> getStatusCommentCommand() {
		ArrayList<StatusCommentCommand> _arraylistStatusCommentCommand = new ArrayList<StatusCommentCommand>();
		for (IDeclarationCommand _dc : _arraylistDeclarationCommand) {
			if (_dc instanceof StatusCommentCommand){
				_arraylistStatusCommentCommand.add((StatusCommentCommand) _dc);
			}
		}
		return _arraylistStatusCommentCommand;
	}

	public ArrayList<ConstraintCommentCommand> getConstraintCommentCommand() {
		ArrayList<ConstraintCommentCommand> commands = new ArrayList<ConstraintCommentCommand>();
		for (IDeclarationCommand _dc : _arraylistDeclarationCommand) {
			if (_dc instanceof ConstraintCommentCommand){
				commands.add( (ConstraintCommentCommand) _dc );
			}
		}
		return commands;
	}

	public TimeScaleCommand getTimeScaleCommand() {
		for (IDeclarationCommand _dc : _arraylistDeclarationCommand) {
			if (_dc instanceof TimeScaleCommand){
				return (TimeScaleCommand) _dc;
			}
		}
		return null;
	}

	public int getPulse() {
		return _pulse;
	}

	public void setPulse(int pulse) {
		this._pulse = pulse;
	}

	public int getOffSet() {
		return _offSet;
	}

	public void setOffSet(int offset) {
		this._offSet = offset;
	}

	public ArrayList<IDeclarationCommand> getArraylistDeclarationCommand() {
		return _arraylistDeclarationCommand;
	}

	public ArrayList<ISimulationCommand> getArraylistSimulationCommand() {
		return _arraylistSimulationCommand;
	}

	public String getVCDString() {
		return visit(new StringVisitor()).toString();
	}

	public VCDDefinitions cloneWithNewFactory(VcdFactory vcdFactory) {
		VCDDefinitions vcdnew = new VCDDefinitions();
		vcdnew.setFactory(vcdFactory);
		vcdnew.setOffSet(_offSet);
		vcdnew.setPulse(_pulse);
		vcdnew.setScope(_scopeCommand);
		vcdnew._arraylistDeclarationCommand = new ArrayList<IDeclarationCommand>();
		for ( IDeclarationCommand _dc:this._arraylistDeclarationCommand)
			vcdnew._arraylistDeclarationCommand.add(_dc);
		vcdnew._arraylistSimulationCommand = new ArrayList<ISimulationCommand>();
		for ( ISimulationCommand _sc:this._arraylistSimulationCommand)
			vcdnew._arraylistSimulationCommand.add(_sc.cloneCommand());
		return vcdnew;
	}

	public ICommentCommand createComment(String commentID) {
		IDecodeComment _decodeComment = DecodeDictionary.getDefault().getDecoder(commentID);
		if (_decodeComment == null) {
			System.err.println("comment unknow");
			return null;
		}
		ICommentCommand _commentCommand = _decodeComment.createEmpty();
		addDefinition(_commentCommand);
		return _commentCommand;
	}

	public ICommentCommand createComment(String commentID, String value) {
		IDecodeComment _decodeComment = DecodeDictionary.getDefault().getDecoder(commentID);
		if (_decodeComment == null) {
			System.err.println("comment unknow");
			return null;
		}
		Comment _comment = new Comment(commentID + " " + value, null);
		ICommentCommand _commentCommand = _decodeComment.create(_comment);
		addDefinition(_commentCommand);
		return _commentCommand;
	}

	public ICommentCommand createCommentClock(String commentID, String clockName) {
		IDecodeComment _decodeComment = DecodeDictionary.getDefault().getDecoder(commentID);
		if (_decodeComment == null) {
			System.err.println("comment unknow");
			return null;
		}
		ICommentCommand commentCommand = _decodeComment.createEmpty();
		commentCommand.setClockLink(clockName);
		addDefinition(commentCommand);
		return commentCommand;
	}

	public ICommentCommand createCommentTick(String commentID, Object tick) {
		IDecodeComment decodeComment = DecodeDictionary.getDefault().getDecoder(commentID);
		if (decodeComment == null) {
			System.err.println("comment unknow");
			return null;
		}
		ICommentCommand commentCommand = decodeComment.createEmpty();
		return commentCommand;
	}

	public int setCommentCommand(ICommentCommand commentCommand, String s) {
		if (commentCommand == null){
			return 0;
		}
		CommentArrayList commentArraylist = array(s, "");
		int i = 1;
		for (String _str : commentArraylist) {
			commentCommand.setString(i++, _str);
		}
		commentCommand.update(s, commentArraylist);
		return 0;
	}

	private CommentArrayList array(String s, String init) {
		CommentArrayList parts = new CommentArrayList();
		if (init != null) {
			parts.add(init);
		}
		if (s == null){
			return parts;
		}
		if (s.length() == 0){
			return parts;
		}
		String cs = s.replaceAll("\n", " ").replaceAll("\r", " ").replaceAll("\t", " ");
		String st[];
		String r = null;
		while (!cs.equals(r)) {
			r = cs;
			cs = cs.replaceAll("  ", " ");
		}
		if (cs.length() == 0){
			return parts;
		}
		while (cs.charAt(0) == ' ') {
			cs = cs.substring(1);
		}
		st = cs.split(" ");
		for (String _s : st) {
			if (_s != null){
				parts.add(_s);
			}
		}
		return parts;
	}

	public final ClockListener getClocklistener() {
		return _clocklistener;
	}

	public ModelRelationComment getModelRelationComment() {
		for (IDeclarationCommand mrc : _arraylistDeclarationCommand) {
			if (mrc instanceof ModelRelationComment){
				return (ModelRelationComment) mrc;
			}
		}
		return null;
	}

	public int addContraint(IConstraintData icd){
		if (_constraintList!=null){
			if (!_constraintList.contains(icd)){
				_constraintList.add(icd);
				if (icd.getConstraints()!=null && _vcdFactory!=null){
					if (icd.getConstraints().getIcc()==null){
						icd.getConstraints().setIcc(_vcdFactory.getConstraintsFactory());
					}
					if ((icd.getConstraints().getList()==null) && (_vcdFactory.getVcddia()!=null)){
						icd.getConstraints().setList(_vcdFactory.getVcddia().getListConnections());	
					}
				}
			}
		}
		return 0;
	}
	public final ArrayList<IConstraintData> getConstraintList() {
		return _constraintList;
	}

	public final IContainer getVcdfolder() {
		return _vcdFolder;
	}

	public final void setVcdfolder(IContainer vcdfolder) {
		this._vcdFolder = vcdfolder;
		for (IDeclarationCommand _decl: _arraylistDeclarationCommand){
			if (_decl instanceof ICommentCommand) {
				((ICommentCommand) _decl).validate(this);
			}
		}
	}
	
	public void setTimeLabelling(ITimeLabelling timeLabelling) {
		this._timeLabelling = timeLabelling;
	}

	public ITimeLabelling getTimeLabelling() {
		return _timeLabelling;
	}
	
}
