/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.vcd.model;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.model.comment.DecodeDictionary;
import fr.inria.aoste.timesquare.vcd.model.comment.IDecodeComment;

public class Comment implements IComment {
	
	private StringBuffer _complete;
	private Uposition _up = null;
	private ArrayList<String> _parts = new ArrayList<String>();
	
	public int getTime() {
		return 0;
	}


	public void addPart(String part) {
		_parts.add(part);
		this._complete.append(' ').append(part);
	}

	private void array(String s) {
		String cs = s.replaceAll("\n", " ").replaceAll("\r", " ").replaceAll("\t", " ");
		String st[];
		String r = null;
		while (!cs.equals(r)) {
			r = cs;
			cs = cs.replaceAll("  ", " ");
		}
		while (cs.charAt(0) == ' ') {
			cs = cs.substring(1);
		}
		st = cs.split(" ");
		for (String _s : st) {
			if (_s != null)
				_parts.add(_s);
		}
	}

	public Comment(String start, Uposition up) {
		super();
		this._complete = new StringBuffer(start);
		if (start.contains(" ")) {
			array(start);
		} else {
			_parts.add(start);
		}
		this._up = up;
		if (this._up == null)
			this._up = new Uposition(0, null);
	}

	public int nbParts() {
		return _parts.size();
	}

	public String get(int pos) {
		return _parts.get(pos);
	}

	public ICommentCommand convert() {
		try {
				IDecodeComment idc = DecodeDictionary.getDefault().getDecoder(_parts.get(0));
				if (idc != null) {
					ICommentCommand x = idc.create(this);
					if (x!=null)
						x.validate();
					return x;
				}
				return null;
		} catch (Throwable ex) {
			ErrorConsole.printError(  ex," :" + _parts.get(0));
			return null;
		}
	}

	@Override
	public String toString() {
		return ("Other : " + this._complete + " @ " + _up);
	}

	public String getComment() {
		return this._complete.toString();
	}

}
