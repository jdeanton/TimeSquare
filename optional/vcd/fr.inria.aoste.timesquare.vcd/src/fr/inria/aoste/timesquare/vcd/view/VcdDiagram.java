/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PrintFigureOperation;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.VcdZoom;
import fr.inria.aoste.timesquare.vcd.draw.FigureCanvasBase;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.listener.VcdFindButtonListener;
import fr.inria.aoste.timesquare.vcd.listener.VcdIntervalButtonListener;
import fr.inria.aoste.timesquare.vcd.listener.VcdOrderButtonListener;
import fr.inria.aoste.timesquare.vcd.menu.Mode;
import fr.inria.aoste.timesquare.vcd.menu.VcdMenu;
import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.model.Function;
import fr.inria.aoste.timesquare.vcd.model.IVar;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class VcdDiagram extends ApplicationWindow implements IVcdDiagram,Iupdate {

	protected FigureCanvas _figureCanvas;
	protected Composite _composite;
	protected ConstraintsFactory _constraintsFactory;
	private MarkerFactory _markerFactory;
	protected boolean _ctrlKey = false;
	protected VcdFactory _vcdFactory =new VcdFactory(VcdColorPreferences.createColor());
	protected Mode _ghostMode = Mode.hide ;
	protected ArrayList<MenuItem> _intervalItem;
	protected ListConnections _listConnections;
	protected double _markerZoomVaule = 1;
	private FigureCanvas _names;
	private Rectangle _oldrc = null;
	protected Shell _shell;
	protected boolean _simulation = false;
	protected TraceCollector _traceCollector;
	protected double _traceCollectorZoomValue = 1;
	protected String _title;
	protected ToolBar _toolbar;
	protected VCDDefinitions _vcdDef;
	protected double _zoomValue = 1;
	protected VcdColorPreferences _mca;
	protected VcdMenu _vcdMenu;
	FigureCanvasBase _fcb=null;
	
	public VcdColorPreferences getColorPreferences() {
		return _mca;
	}

	public int getActivePageID() {
		return 0;
	}

	public FigureCanvasBase getfcb() {
		return _fcb;
	}

	public void print() {
		Shell s = new Shell();
		PrintDialog printDialog = new PrintDialog(s, SWT.NONE);
		printDialog.setText("Print");
		PrinterData printerData = printDialog.open();
		if (!(printerData == null)) {
			Printer p = new Printer(printerData);
			PrintFigureOperation pfo = new PrintFigureOperation(p, _figureCanvas.getContents());
			pfo.setPrintMode(PrintFigureOperation.FIT_PAGE);
			pfo.run("test");

		}
	}

	
	public VcdDiagram(Shell parentShell, String title, VcdFactory factory) {
		super(parentShell);
		this._listConnections = new ListConnections(
				new ArrayList<Description>(),
				new ArrayList<ConstraintsConnection>(),
				new HashMap<ConstraintCommentCommand, ArrayList<ConstraintsConnection>>());
		addToolBar(SWT.NONE);
		this._title = title;
		this._vcdFactory = factory;
		_mca= factory.getMca();
		if (_mca==null)
			_mca = VcdColorPreferences.createColor();	
		this._constraintsFactory = new ConstraintsFactory(_listConnections, this);
		this._markerFactory = new MarkerFactory(_listConnections, this);
		_vcdMenu= new VcdMenu(this);
		vcdzoom= new VcdZoom(_listConnections, this);
		parentShell.setImage(PluginHelpers.getImage("fr.inria.aoste.timesquare.vcd.antlr", "icons/sampleInria.gif"));
	}

	public void vcdMultiPageEditorRefresh() {}

	@Override
	public boolean close() {

		getCanvas().setContents(null);
		getNames().setContents(null);
		getCanvas().dispose();
		getNames().dispose();
		ErrorConsole.println("Closed");
		_composite.dispose();
		_listConnections.getListConstraints().clear();
		Runtime.getRuntime().runFinalization();
		return super.close();
	}

	@Override
	public void create() {
		super.create();
		_shell = this.getShell();
		_shell.setSize(Toolkit.getDefaultToolkit().getScreenSize().width,
				getCanvas().getSize().y + 140);
		createMenuBar();
	}

	public void createClockMenu(Menu menuBar) {

		MenuItem clockMenu;
		clockMenu = new MenuItem(menuBar, SWT.CASCADE);
		clockMenu.setText("Clock");
		Menu menu = new Menu(this.getShell(), SWT.DROP_DOWN);
		clockMenu.setMenu(menu);

		MenuItem subItem = new MenuItem(menu, SWT.NULL);
		subItem.setText("Order");
		subItem.addSelectionListener(new VcdOrderButtonListener(_listConnections, this));
	}

	@Override
	protected Control createContents(Composite parent) {
		
		getShell().setText(_title);
		_fcb = new FigureCanvasBase(this, _mca);
		_fcb.makeVcd(parent,false);
		_composite = _fcb.getComposite();
		_names = _fcb.getNames();
		_figureCanvas=_fcb.getClockCanvas();
		_toolbar = new ToolBar(_composite, SWT.NONE);
		_fcb.getComposite().getVerticalBar().setVisible(true);
		_fcb.getComposite().getHorizontalBar().setVisible(true);
		_fcb.scrollUpdate();
		_fcb.layoutall();
		_fcb.createListener(_listConnections, this);
		_fcb.addScrollListener();		
		return _composite;
	}

	public FigureCanvas getScaleCanvas() {
		return _fcb.getScaleCanvas();
	}

	public void createFileMenu(Menu menuBar) {
		MenuItem fileMenu;
		fileMenu = new MenuItem(menuBar, SWT.CASCADE);
		fileMenu.setText("File");
		Menu menu = new Menu(this.getShell(), SWT.DROP_DOWN);
		fileMenu.setMenu(menu);
		MenuItem exitItem = new MenuItem(menu, SWT.NULL);
		exitItem.setText("Exit");
		class ExitListener implements SelectionListener {
			private VcdDiagram vdt;

			public ExitListener(VcdDiagram vdt) {
				super();
				this.vdt = vdt;
			}
			
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);

			}

			public void widgetSelected(SelectionEvent e) {
				vdt.close();
			}
		}
		exitItem.addSelectionListener(new ExitListener(this));
	}

	public void createFindMenu(Menu menuBar) {
		MenuItem findMenu;
		findMenu = new MenuItem(menuBar, SWT.CASCADE);
		findMenu.setText("Find");
		Menu menu = new Menu(this.getShell(), SWT.DROP_DOWN);
		findMenu.setMenu(menu);		
		MenuItem subItem = new MenuItem(menu, SWT.NULL);
		subItem.setText("Find");
		subItem.addSelectionListener(new VcdFindButtonListener(_listConnections, this));
	}

	public void createMenuBar() {
		Menu menuBar = new Menu(this.getShell(), SWT.BAR);
		this.getShell().setMenuBar(menuBar);
		// create each header and subMenu for the menuBar
		createFileMenu(menuBar);
		createClockMenu(menuBar);
		createOptionsMenu(menuBar);
		createFindMenu(menuBar);
	}

	public void createOptionsMenu(Menu menuBar) {
		// Options menu.
		MenuItem optionMenu;
		optionMenu = new MenuItem(menuBar, SWT.CASCADE);
		optionMenu.setText("Options");
		Menu menu = new Menu(this.getShell(), SWT.DROP_DOWN);
		optionMenu.setMenu(menu);
		MenuItem syncItem = new MenuItem(menu, SWT.CASCADE);
		syncItem.setText("Synchronized");
		Menu syncMenu = new Menu(menu);
		syncItem.setMenu(syncMenu);
		syncItem.setEnabled(false);
		for (ConstraintCommentCommand cc : _vcdDef.getConstraintCommentCommand()) {
			if (cc.getFunction() == Function._synchronizeswith) {
				syncItem.setEnabled(true);
				break;
			}
		}
		if (syncItem.isEnabled()) {
			_intervalItem = new ArrayList<MenuItem>();
			int i = 0;
			ArrayList<String> clocksnames = new ArrayList<String>();
			Color color = _mca.colorLightGraySync();
			for (ConstraintCommentCommand cc : _vcdDef.getConstraintCommentCommand()) {
				if (i == 0)
					color = _mca.colorLightGraySync();
				if (i == 1)
					color = _mca.colorLightBlueSync();
				if (i == 2)
					color = _mca.colorRedSync();
				if (i == 3)
					i = 0;
				if (cc.getFunction() != Function._synchronizeswith)
					continue;
				if (clocksnames.contains(cc.getReferenceClocks().get(0)))
					continue;
				clocksnames.add(cc.getClock());
				_listConnections.getListInterval().put(cc,new ArrayList<ConstraintsConnection>());
				_listConnections.firstClock1Put(cc, new ArrayList<IFigure>());
				_listConnections.firstClock2Put(cc, new ArrayList<IFigure>());
				_listConnections.lastClock1Put(cc, new ArrayList<IFigure>());
				_listConnections.lastClock2Put(cc, new ArrayList<IFigure>());
				MenuItem item = new MenuItem(syncMenu, SWT.CHECK);
				item.setText(cc.toString());
				item.addSelectionListener(new VcdIntervalButtonListener(_listConnections,this, cc));
				_listConnections.getMenuForComment().put(item, cc);
				_listConnections.menuForColorPut(item, color);
				_intervalItem.add(item);
				i++;
			}
		}
	}

	public FigureCanvas getCanvas() {
		return _figureCanvas;
	}

	public ConstraintsFactory getConstraintsFactory() {
		return _constraintsFactory;
	}

	public MarkerFactory getMarkerFactory() {
		return _markerFactory;
	}

	public VcdFactory getVcdFactory() {
		return _vcdFactory;
	}

	public final void setVcdFactory(VcdFactory factory) {
		this._vcdFactory = factory;
	}

	public ArrayList<MenuItem> getIntervalItem() {
		return _intervalItem;
	}

	public ListConnections getListConnections() {
		return _listConnections;
	}

	public double getMarkerZoom() {
		return _markerZoomVaule;
	}

	public Shell getMyShell() {
		return _shell;
	}

	public FigureCanvas getNames() {
		return _names;
	}

	public TraceCollector getTraceCollector() {
		return _traceCollector;
	}

	public double getTraceZoomValue() {
		return _traceCollectorZoomValue;
	}

	public ToolBar getToolbar() {
		return _toolbar;
	}

	public VCDDefinitions getVcdModel() {
		return _vcdDef;
	}

	public double getZoomValue() {
		return _zoomValue;
	}

	public boolean isCtrlKey() {
		return _ctrlKey;
	}

	public Mode isGhostMode() {
		return _ghostMode;
	}

	public boolean isSimulation() {
		return _simulation;
	}

	public void setCtrlKey(boolean ctrlKey) {
		this._ctrlKey = ctrlKey;
	}

	public void setGhostMode(Mode ghostMode) {
		this._ghostMode = ghostMode;
	}

	public void setMarkerZoom(double markerZoom) {
		this._markerZoomVaule = markerZoom;
	}

	public void setSimulation(boolean simulation) {
		this._simulation = simulation;
	}

	public void setTc(TraceCollector tc) {
		this._traceCollector = tc;
	}

	public void setTcZoom(double tcZoom) {
		this._traceCollectorZoomValue = tcZoom;
	}

	public void setVcdModel(VCDDefinitions vcd) {
		this._vcdDef = vcd;
	}

	public void setZoomValue(double zoom) {
		this._zoomValue = zoom;
	}

	public void setSimulationProgress(int etape) {}
	
	public int update2(int n1, int n2, boolean b){
		return update(n1, n2, b);	
	}
	
	public int update(int n1, int n2, boolean b) {

		ArrayList<IVar> listvar = new ArrayList<IVar>();
		listvar.addAll(getTraceCollector().getSelectedClocks());
		getTraceCollector().setZoom(getTraceZoomValue());
		getVcdModel().visit(getTraceCollector(), n1, n2, b);
		getCanvas().setContents(getVcdFactory().getBackPanel());

		int x1 = -1, x2 = -1, y1 = -1, y2 = -1;

		Rectangle rc = null;
		for (Object obj : getCanvas().getContents().getChildren()) {
			Panel panel = null;
			if (obj instanceof Panel)
				panel = (Panel) obj;
			else
				continue;
			Rectangle r = null;
			for (Object f : panel.getChildren()) {
				if (f instanceof IFigure) {
					IFigure poly = (IFigure) f;
					r = poly.getBounds();
					if (rc == null)
						rc = r;
					else
						rc = rc.getUnion(r);

				}

			}

		}
		if (rc==null){
			return 0;
		}
		
		Rectangle rc2 = null;
		
		if (_oldrc == null) {
			rc2 = rc;
		} else{
			rc2 = new org.eclipse.draw2d.geometry.Rectangle(_oldrc.getTopRight(), rc.getBottomRight());
		}
		Rectangle rc3 = null;

		_oldrc = rc;

		for (Object obj : getCanvas().getContents().getChildren()) {
			Panel panel = null;
			if (obj instanceof Panel)
				panel = (Panel) obj;
			else
				continue;
			Rectangle r = null;
			for (Object f : panel.getChildren()) {
				if (f instanceof IFigure) {
					IFigure poly = (IFigure) f;
					r = poly.getBounds();
					if (r.intersects(rc2)){
						if (rc3 == null){
							rc3 = r;
						}else{
							rc3 = rc3.getUnion(r);
						}
					}
				}

			}

		}
		if (rc3 != null) {
			x1 = rc3.x;
			x2 = rc3.width;
			y1 = rc3.y;
			y2 = rc3.height;
			getCanvas().layout();
			for (MenuItem menuitem : _listConnections.getMenuForComment().keySet()) {
				if (menuitem.getSelection()) {
					ConstraintCommentCommand cc = _listConnections.getMenuForComment().get(menuitem);
					Color color = _listConnections.menuForColorGet(menuitem);
					getConstraintsFactory().drawSyncInterval(cc, color);
				}
			}
			getCanvas().redraw(x1, y1, x2, y2, false);
		}
		return 0;
	}

	private VcdZoom vcdzoom = null;

	public VcdZoom getVcdzoom() {		
		return vcdzoom;
	}

	public void setFocus() {
		if (_shell != null){
			if (!_shell.isDisposed()){
				_shell.setFocus();
			}
		}
	}

	public int replace(int pos, String src, String newtext) {
		return 0;
	}

	public boolean isContainGhost() {
		return true;
	}

	public VcdMenu getVcdmenu() {
		return _vcdMenu;
	}

}