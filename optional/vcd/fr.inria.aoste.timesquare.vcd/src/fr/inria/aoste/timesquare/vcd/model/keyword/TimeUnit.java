/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.keyword;

public enum TimeUnit {
	
	s(0), ms(1), us(2), ns(3), ps(4), fs(5), tick(-1);
	
	final int n;
	
	private TimeUnit(int n) {
		this.n = n;
	}

	public static String getTime(double d, TimeUnit t) {
		if( t==null || t==tick){
			return "" +d;
		}
		if ( d==0.0){
			return "0.0"+ t.toString() ;
		}
		int k=(int ) Math.log10(d)  /3 ;
		int k2 = t.n-k ;
		if (k2<0) k2=0;
		if (k2>5) k2=5;
		TimeUnit tx= getTimeUnit(k2);
		double d2 = d * t.getConvertionRatio(tx);
		return ""+((float)d2)+ " "+ tx.toString();
		
	}
	
	public double getConvertionRatio(TimeUnit convert){
		if ((this.n==-1) || ( convert.n==-1)){		
			return 1.0;
		}
		int delta =  (convert.n - this.n ) *3;
		return Math.pow(10 , delta);
	}
	
	public static TimeUnit getTimeUnit (int x){
		for( TimeUnit tu : TimeUnit.values()){
			if (tu.n==x){
				return tu;
			}
		}
		return null;
	}
	
	

	
}
