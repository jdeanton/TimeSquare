/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.visitor;

import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.Iupdate;
import fr.inria.aoste.timesquare.vcd.view.VcdDiagram;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;

public class Collector {

	private TraceCollector _traceCollector;
	private VCDDefinitions _vcdDefinitions;
	private Iupdate _vcdDiagram = null;
	static private boolean _block = true;

	/**
	 * Builds a {@link VcdFactory}, then collects all the definitions of variables in the VCD model
	 * @param vcdDef
	 * @param mca
	 * @return
	 */
	public VcdFactory collect(VCDDefinitions vcdDef, VcdColorPreferences mca) {
		VcdFactory factory = new VcdFactory(mca);
		
		VarCollector varC = new VarCollector(vcdDef.getArraylistDeclarationCommand());
		
		vcdDef.visit(varC);
		factory.setVcdDef(vcdDef);
		_traceCollector = new TraceCollector(varC, factory);
		vcdDef.setFactory(factory);
		vcdDef.visit(_traceCollector);
		this._vcdDefinitions = vcdDef;

		return factory;
	}
	
	//Old version 
//	public static void drawInWindow(VCDDefinitions vcdDef, VcdColorPreferences mca) {
//		drawInWindow(vcdDef, "VCD viewer",mca);
//	} // not used

	
	/**
	 * This method is only called at the end of simulation
	 * 
	 * @param vcdDef
	 * @return
	 */
	public static VcdDiagram drawInWindow(VCDDefinitions vcdDef) {
		Shell shell = new Shell();
		VcdColorPreferences mca = VcdColorPreferences.createColor();
		vcdDef.setFactory(new VcdFactory(mca));
		VcdDiagram window = new VcdDiagram(shell, "VCD viewer", vcdDef.getFactory());		
		drawInEditor(window, vcdDef, mca);
		
		window.setBlockOnOpen(_block);
		window.create();
		window.getShell().setActive();
		window.open();
		return window;
	}
	
	//Old version
//	public static void drawInWindow(VCDDefinitions vcdDef, String name, VcdColorPreferences mca) {
//		Collector co = new Collector();
//		drawInWindow(co.collect(vcdDef,mca), name, co);
//	}// not used
	//Old version
//	public static void drawInWindow(VcdFactory vcdFactory, String name,Collector collector) {
//		Shell shell = new Shell();
//		VcdDiagram window = new VcdDiagram(shell, name, vcdFactory);
//		window.setTc(collector._traceCollector);
//		window.setVcdDefinitions(collector._vcdDefinitions);
//		vcdFactory.setVcddia(window);
//		window.setVcdFactory(vcdFactory);
//		window.setBlockOnOpen(_block);
//		collector._vcdDiagram = window;
//		window.create();
//		window.getShell().setActive();
//		window.open();
//	}
	
	/**
	 * This method is called at the beginning of simulation
	 * @param editor
	 * @param vcdDef
	 * @param mca
	 */
	public static void drawInEditor(IVcdDiagram editor, VCDDefinitions vcdDef, VcdColorPreferences mca) {
		drawInEditor(editor, vcdDef, "VCD viewer",mca);
	}

	/**
	 * 
	 * By calling the method collect() in Collector, all figures for clocks are created and will be 
	 * stored in co.
	 * 
	 * @param editor
	 * @param vcdDef
	 * @param name
	 * @param mca
	 */
	public static void drawInEditor(IVcdDiagram editor, VCDDefinitions vcdDef, String name, VcdColorPreferences mca) {
		Collector co = new Collector();
		
		VcdFactory factory = co.collect(vcdDef,mca);
		
		drawInEditor(editor, factory, name, co);
	}

	
	public static void drawInEditor(IVcdDiagram editor, VcdFactory vcdFactory,
			String name, Collector collector) {
		editor.setTc(collector._traceCollector);
		editor.setVcdModel(collector._vcdDefinitions);
		editor.setVcdFactory(vcdFactory);
		vcdFactory.setVcddia(editor);
		collector._vcdDiagram = (Iupdate) editor;
	}

	public final Iupdate getVcdDiagram() {
		return _vcdDiagram;
	}
	
	public static final void setBlock(boolean block) {
		Collector._block = block;
	}

	public static final boolean isBlock() {
		return _block;
	}	
	
}
