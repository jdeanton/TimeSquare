/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.menu;

import org.eclipse.jface.action.Action;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.dialogs.PartialDialog;

//action of "Show Ghost","Hide Ghost" and "Partial..."
public class GhostAction extends Action {
	
	private final IVcdDiagram _vcdMultiPageEditor;
	private final int _n;
	private final String _labelActive;
	private boolean _checked;

	public GhostAction(IVcdDiagram vcdMultiPageEditor, String text, int style,int n) {
		super(text, style);
		_labelActive = text;
		this._vcdMultiPageEditor = vcdMultiPageEditor;
		this._n = n;
		_checked = false;
		super.setChecked(_checked);
	}

	@Override
	public void run() {
		try {
			if (!isChecked()){ 
				return ;
			}
			VcdMenu vcdMenu = _vcdMultiPageEditor.getVcdmenu();
			switch (_n) {
			case 0:
				// case of show ghost
				_vcdMultiPageEditor.setGhostMode(Mode.show);
				vcdMenu.setGhostSelected(Mode.show);
				break;
			case 1:
				// case of hide ghost
				_vcdMultiPageEditor.setGhostMode(Mode.hide);
				vcdMenu.setGhostSelected(Mode.hide);
				break;
			case 2:
				// case of partial
			
				Mode oldsel = vcdMenu.getGhostSelected();
				if (vcdMenu.getPartialGhostAction().isChecked()) {
					// create a dialog box to choice which clock with ghost to
					// show or hide
					PartialDialog partialDialog = new PartialDialog(
							_vcdMultiPageEditor.getMyShell(),
							"Clocks with hidden ghosts ", "Clocks with show ghosts",
							_vcdMultiPageEditor, ".");
					// show dialog box
					int ret = partialDialog.open();
					// if "OK"
					if (ret == org.eclipse.jface.window.Window.OK) {
						// call method apply of partialDialog
						partialDialog.apply();
					}
					// if "CANCEL"
					if (ret == org.eclipse.jface.window.Window.CANCEL) {
						vcdMenu.setGhostSelected(oldsel);
					}
					else{
						vcdMenu.setGhostSelected(Mode.partial);
					}
				}
				break;
			default:				
				break;
			}
		} catch (Throwable e) {
			ErrorConsole.printError(e, "Ghost run");
		}
		_vcdMultiPageEditor.setFocus();
	}

	@Override
	public void setChecked(boolean checked) {
		this._checked = checked;
		if ( isEnabled())
			super.setChecked(checked);
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (enabled) {
			setText(_labelActive);
			super.setChecked(_checked);
		} else {
			setText("No ghost on VcdDiagram");
			super.setChecked(false);
		}
		super.setEnabled(enabled);
	}
}