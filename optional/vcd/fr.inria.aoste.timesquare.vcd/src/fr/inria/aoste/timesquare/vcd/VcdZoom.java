/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.action.ActionSync;
import fr.inria.aoste.timesquare.vcd.model.comment.ConstraintCommentCommand;
import fr.inria.aoste.timesquare.vcd.view.EventPanel;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public class VcdZoom {

	private double size = 1.0;
	private int delta = 0;
	private IVcdDiagram vdt;
	private ListConnections list;
	private IComboZoom iczoom;
	private int counter;
	private int lastpoint = 0;
	private IFigure fig=null;
	private Dimension dim=null;
	private boolean lock = false;
	public ZoomList zoomlist=null;
	
	public VcdZoom(ListConnections listin, IVcdDiagram vdtin) {
		vdt = vdtin;
		list = listin;
		zoomlist = new ZoomList();
	}

	public int getDelta() {
		return delta;
	}

	public ListConnections getList() {
		return list;
	}

	public void setList(ListConnections list) {
		this.list = list;
	}

	public void setDelta(int delta) {
		this.delta = delta;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double sizein) {
		if (sizein <= 0.0){
			return;
		}
		this.size = sizein;
	}

	public void scale(int n) {
		if (n == 0) {
			return;
		}
		if (n > 0) {
			size = size * (1 << n);
			return;
		}
		size = size / (1 << (-n));

	}

	public void scaleration(double f) {
		size = size * f;
	}


	public final boolean isLock() {
		return lock;
	}

	/**
	 * @author mzc
	 */
	public void applyScrollZoom() {
		try {
			fig = vdt.getCanvas().getContents();
			int n = zoomlist.selectValue(vdt.getTraceZoomValue() * 100);
			if (n == -1) {
				lock = false;
				return;
			}
			applyZoomMore();
			vdt.setMarkerZoom(vdt.getTraceZoomValue());
			vdt.getMarkerFactory().hideMarkerFireable();
			vdt.getCanvas().layout();
			vdt.getCanvas().update();
			vdt.getfcb().getComposite().layout();
			vdt.getfcb().getComposite().update();
			
			zoomlist.selectValue(vdt.getTraceZoomValue() * 100);
			if (iczoom != null){
				iczoom.select(vdt.getTraceZoomValue() * 100);
			}
			vdt.getfcb().scrollUpdate();
		} catch (Throwable e) {
			ErrorConsole.printError(e, " VCDZOOM ");
		}
		lock = false;
	}

	public void applyClickZoom() {
		try {
			int selX = vdt.getfcb().getClockCanvas().getHorizontalBar().getSelection();

			fig = vdt.getCanvas().getContents();
			double delta = 1.0f;
			delta = vdt.getZoomValue();
			int n = zoomlist.selectValue(vdt.getTraceZoomValue() * 100);
			if (n == -1) {
				lock = false;
				return;
			}
			applyZoomMore();
			vdt.setMarkerZoom(vdt.getTraceZoomValue());
			vdt.getMarkerFactory().hideMarkerFireable();
			vdt.getCanvas().layout();
			vdt.getCanvas().update();
			vdt.getfcb().getComposite().layout();
			vdt.getfcb().getComposite().update();
			
			zoomlist.selectValue(vdt.getTraceZoomValue() * 100);
			if (iczoom != null){
				iczoom.select(vdt.getTraceZoomValue() * 100);
			}
			vdt.getCanvas().scrollToX((int) (selX * delta));
			vdt.getScaleCanvas().scrollToX((int) (selX * delta));


			vdt.getfcb().scrollUpdate();
		} catch (Throwable e) {
			ErrorConsole.printError(e, " VCDZOOM ");
		}
		lock = false;
	}
	
	
	public int computeposition(int t) {
		int r = (int) ((t) * vdt.getTraceZoomValue());
		return r;
	}

	/**
	 * @author mzc
	 */
	private void applyZoomMore() {
		lastpoint = 0;	
		for (Object obj : vdt.getCanvas().getContents().getChildren()) {
			EventPanel ep = null;
			if (obj instanceof EventPanel) {
				ep = (EventPanel) obj;
			} else{
				continue;
			}
			ep.setValid(false);
			ep.getEventline().eventLineZoom(vdt.getTraceZoomValue()/2);
			ep.markersZoom(vdt.getTraceZoomValue()/2);
			ep.setValid(true);
			if(ep.isDurationModel()){
				for (Object f : ep.getChildren()) {
					if (f instanceof IFigure) {
						if (f instanceof ExtendFigure) {
							((ExtendFigure) f).mycompute();
						}
					}
				}
			}
		}
		vdt.getVcdFactory().getTimeline().timelineZoom(vdt.getTraceZoomValue()/2);
		lastpoint = vdt.getVcdFactory().getScalePanel().getPreferredSize().width;
		
		
		
		fig.setPreferredSize(new Dimension(lastpoint,
				fig.getPreferredSize().height));
		
		for (Object obj : vdt.getCanvas().getContents().getChildren()) {
			EventPanel ep = null;
			if (obj instanceof EventPanel) {
				ep = (EventPanel) obj;
			} else{
				continue;
			}
			ep.setValid(false);
			ep.setPreferredSize(fig.getPreferredSize().width, ep
					.getPreferredSize().height);
			ep.setValid(true);
		}
		dim = vdt.getCanvas().getContents().getPreferredSize();
		vdt.getCanvas().getContents().setPreferredSize(
				new Dimension(
						vdt.getCanvas().getContents().getPreferredSize().width,
						dim.height));
		
		vdt.getVcdFactory().getScalePanel().repaint();
		vdt.getScaleCanvas().redraw();
		if (vdt.getVcdmenu().getSyncAction() != null)
			for (ActionSync menuitem : vdt.getVcdmenu().getSyncAction()) {
				if (menuitem.isChecked()) {
					ConstraintCommentCommand cc = menuitem.getCc();

					Color color = menuitem.getColor();
					vdt.getConstraintsFactory().drawSyncInterval(cc, color);
				}
			}
		vdt.getCanvas().redraw();
	}
	
	public void setZoomCounter(int comp) {
		this.counter = comp;
	}

	public int getZoomCounter() {
		return counter;
	}

	public final IComboZoom getIczoom() {
		return iczoom;
	}

	public final void setIczoom(IComboZoom iczoom) {
		this.iczoom = iczoom;
	}

	public static class ZoomList {
		private ZoomList() {
			super();
			ardb.add(6.25);
			ardb.add(12.5);
			ardb.add(25.0);
			ardb.add(50.0);
			ardb.add(100.0);
			ardb.add(200.0);
			ardb.add(400.0);
			indice = ardb.indexOf(100.0);
		}

		private int indice = 0;

		public final int getIndice() {
			return indice;
		}

		private ArrayList<Double> ardb = new ArrayList<Double>();

		public int selectValue(Double d) {
			if ((d < 0.001) || (d > 5000)) {
				return -1;
			}
			int n = 0;
			n = ardb.indexOf(d);
			if (n != -1) {
				indice = n;
				return n;
			}
			n = 0;
			for (double dl : ardb) {
				if (dl > d)
					break;
				n++;
			}
			ardb.add(n, d);
			n = ardb.indexOf(d);
			indice = n;
			return n;
		}

		public String[] arrayOf() {
			ArrayList<String> lst = new ArrayList<String>();
			for (Double d : ardb) {
				lst.add(d.toString());
			}
			return lst.toArray(new String[] {});
		}
	}

	public final ZoomList getZoomlist() {
		return zoomlist;
	}

	public IVcdDiagram getVdt() {
		return vdt;
	}

	public void unSetVdt() {
		this.vdt = null;
	}
	
	
}
