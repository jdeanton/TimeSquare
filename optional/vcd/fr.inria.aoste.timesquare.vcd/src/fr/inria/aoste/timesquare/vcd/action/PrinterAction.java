/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.vcd.action;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;

public class PrinterAction extends Action implements IActionTextVCD {

	IAction _actionText = null;
	private final IVcdDiagram _ivcddiagram;
	
	public IAction getActionText() {
		return _actionText;
	}

	public void setActionText(IAction ac) {
		_actionText = ac;
	}

	public PrinterAction(IVcdDiagram ivcd) {
		super();
		this._ivcddiagram = ivcd;
	}

	@Override
	public void run() {
		if (_ivcddiagram.getActivePageID() == 0)
			_ivcddiagram.print();
		else if (_actionText != null)
			_actionText.run();
	}

}