/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model.comment;

import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.UpdateCommand;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;

public final class HideCommand implements ICommentCommand {

	private String _name;
	private IComment _commentary;

	public HideCommand() {
		super();
	}

	public IComment getComment() {
		return _commentary;
	}

	public void setComment(IComment commentary) {
		this._commentary = commentary;
	}

	public String getName() {
		return _name;
	}

	public HideCommand(String name) {
		super();
		this._name = name;
	}

	public void visit(IDeclarationVisitor visitor) {
		visitor.visitConstraintComment("hide " +_name ); 
	}

	
	@Override
	public String toString() {
		return "Hide Clock :" + _name;
	}

	public boolean isActive() {

		return true;
	}

	public void setActive(boolean active) {

	}

	
	public int clear() {
		return 0;
	}
	
	
	public void setString(int n, String s) {}

	
	public void setClockLink(String name) {	
		this._name=name;
	}
	
	
	Uposition _up;

	public final Uposition getUp() {
		return _up;
	}

	public final void setUp(Uposition up) {
		this._up = up;
	}
	
	public int update(String newcommentvalue, IComment updatevalue) {
		return 0;
	}
	
	
	
	public String getType() {
		return "hide";
	}

	
	public int validate(VCDDefinitions vcddef) {
		if (_clklst!=null){
			_clklst.putDataonClock(_name, "hide", true);
		}
		return 0;
	}

	UpdateCommand _clklst;
	
	public int setClockListener(UpdateCommand _clklst) {
		this._clklst=_clklst;
		this._clklst.putDataonClock(_name, "hide", true);
		return 0;
	}

	
	public void validate() {
		if (_clklst!=null)
		_clklst.putDataonClock(_name, "hide", true);
	}

	public void newData(Object o) {}
	
}
