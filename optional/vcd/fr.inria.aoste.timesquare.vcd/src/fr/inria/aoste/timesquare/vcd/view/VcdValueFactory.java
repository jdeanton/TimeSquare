/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure;

import fr.inria.aoste.timesquare.vcd.model.Description;
import fr.inria.aoste.timesquare.vcd.model.Value;
import fr.inria.aoste.timesquare.vcd.model.command.VarCommand;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw0;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
import fr.inria.aoste.timesquare.vcd.view.figure.DrawTimer;
import fr.inria.aoste.timesquare.vcd.view.figure.DrawX;
import fr.inria.aoste.timesquare.vcd.view.figure.DrawZ;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure.Level;

final public class VcdValueFactory {

	private VcdFactory _vcdFactory;
	private int _x, _y, _counter = 0 ;
	private String _clockXmiId;
	private String _clockName;
	private String _aliasName;
	private ArrayList<ExtendFigure> _tickFigures = new ArrayList<ExtendFigure>();
	private ArrayList<DrawX> _ghostFigures = new ArrayList<DrawX>();
	private boolean _isGhostVisible;
	private int _time = 0;
	private int _edge = 0;
	private boolean _isLastvalueX = false;
	private boolean _ghostinclock = false;
	private int _nbGhost = 0;
	private Value _oldValue = null;
	private Value _currentValue = null;
	private ExtendFigure _precedent;
	private VarCommand _varCommand;
	private int _tickNum=0;
	
	VcdValueFactory(VarCommand vcin,  VcdFactory vcdFactory, int y, double zoom) {
		super();
		this._varCommand=vcin;
		Object obj = vcin.getData("xmi");
		if(obj instanceof String){
			this._clockXmiId = ((String)obj).substring(((String)obj).indexOf("#")+1);
		}else{
			_clockXmiId = null;
		}
		
		this._vcdFactory = vcdFactory;
		this._y = y;
		this._x = vcdFactory.getNameWidth();
		_clockName = vcin.getName();
		_aliasName=vcin.getAlias();
	}

	public String getClockName() {
		return _clockName;
	}
	
	public VcdFactory getVcdFactory(){
		if(_vcdFactory==null)
			return null;
		return _vcdFactory;
	}
	
	public boolean getIsGhostVisible() {
		return _isGhostVisible;
	}

	public void setIsGhostVisible(boolean b) {
		_isGhostVisible = b;
	}

	public int getNbGhost() {
		return _nbGhost;
	}

	public boolean haveGhostinclock() {
		return _ghostinclock;
	}

	public int getTickNum(){
		return this._tickNum;
	}
	
/**
 * This method is used just at the end of simulation
 * @param value
 * @param length
 */
	public void buildEnd(Object value, int previousTime, int time) {
		if (value instanceof Value) {
			_vcdFactory.modifyTimeLineSize(_time);
			String clockId = null;
			if(_clockXmiId == null){
				clockId = _clockName;
			}else{
				clockId = _clockXmiId;
			}
			if ( (Value)value == Value._z ){
				_vcdFactory.drawEnd(previousTime, clockId);
			}
			_isLastvalueX = ((Value) value == Value._x);
		} 
		else {
			int length = time + 20 - previousTime;
			Boolean b = ghostundisp(value.toString());
			Description d = getDescription(value.toString(), length);
			ExtendFigure f = get(value.toString(), length);
			_isLastvalueX = value.toString().equals("x");
			if (f!=null){
				_vcdFactory.add(f, d, b);
			}
		}
	
	}
	
	/**
	 * 
	 * @param v
	 * @param t
	 * @param previoustime
	 */
	public void build(Object v, int t, int previousTime) {
		this._time = t;
		if (v instanceof Value) {
			String clockId = null;
			if(_clockXmiId == null){
				clockId = _clockName;
			}else{
				clockId = _clockXmiId;
			}
			_isLastvalueX = ((Value) v == Value._x);
			if( (Value)v == Value._0 ){
				_vcdFactory.drawNoTick(previousTime, clockId);
			}else if( (Value)v == Value._1 ) {
				_tickNum++;
				_vcdFactory.drawTick(previousTime,clockId);
			}else if ( (Value)v == Value._z ){
				_vcdFactory.drawZ(previousTime, clockId);
			}
			
		} else {
			if (v == null){
				return ;
			}
			Boolean b = ghostundisp(v.toString());
			Description d = getDescription(v.toString(), _time);
			ExtendFigure f = get(v.toString(), _time - previousTime);
			_isLastvalueX = v.toString().equals("x");
			if (f!=null){
				this._vcdFactory.add(f, d, b);
			}
		}
	}

	
	public boolean ghostundisp(Value value) {
		switch (value) {
		case _0:
		case _1:
		case _z:
			return false;
		case _x:
			return true;
		}
		return false;
	}

	public boolean ghostundisp(String value) {
		if (value.equals("x"))
			return true;
		return false;
	}

	public int getEdge() {
		return _edge;
	}

	@SuppressWarnings("unused")
	private Description getDescription(Value value, int time) {
		switch (value) {
		case _1:
			return new Description(_clockName, _aliasName, _counter, String
					.valueOf(_counter), _tickFigures, time);
		case _0:
			return new Description(_clockName, _aliasName, -1, "-1", _tickFigures, time);
		case _x:
		case _z:
			return new Description(_clockName,_aliasName,  time);
		}
		return null;
	}

	private Description getDescription(String value, int time) {
		return new Description(_clockName,_aliasName,  -1, _tickFigures, time);
	}

	@SuppressWarnings("unused")
	private ExtendFigure get(Value value, int length) {
		Value tmp = _currentValue;
		_currentValue = value;
		switch (value) {
		case _0:
			_oldValue = tmp;
			
			return getZero(length);
		case _1:
			_counter++;
			ExtendFigure fig = getOne(length);
			_tickFigures.add(fig);
			_oldValue = tmp;
			return fig;
		case _x:
			ExtendFigure figx = getX(length);
			return figx;
		case _z:
			_oldValue = tmp;
			return getZ(length,true);
		}
		return null;
	}

	// return a new value or null
	private ExtendFigure get(String value, int length) {
		if (value.equals("z")){
			return getZ(length,false);
		}
		if (_precedent instanceof DrawTimer){
			_precedent.incLength(length);
			_precedent.mycompute();
			_x += length;
			return null;
		}
		DrawTimer drawtime = new DrawTimer(_vcdFactory, _x, _y, length, "Active");
		if (_precedent != null){
			drawtime.setPrecedeLevel(_precedent.getLevel());
		}
		drawtime.mycompute();
		_x += length;
		_precedent = drawtime;
		return drawtime;
	}

	// return a new value or null
	private Draw1 getOne(int length) {
		if (_precedent instanceof Draw1){
			_precedent.incLength(length);
			_precedent.mycompute();
			_x += length ;
			return null;
		}
		Draw1 dr1 = new Draw1(_vcdFactory, _x, _y, length);
		dr1.linktoTick(_varCommand, _counter);
		if (_precedent != null)
			dr1.setPrecedeLevel(_precedent.getLevel());
		dr1.setoldValue(_isLastvalueX ? 1 : 0);
		dr1.mycompute();
		_x += length;
		_edge++;
		_precedent = dr1; 
		return dr1;
	}

	// return a new value or null
	private Draw0 getZero(int length) {
		if (_precedent instanceof Draw0){
			_precedent.incLength(length);
			_precedent.mycompute();
			_x += length ;
			return null;
		}
		Draw0 dr0 = new Draw0(_vcdFactory, _x, _y, length);
		if (_precedent != null){
			dr0.setPrecedeLevel(_precedent.getLevel());
		}
		dr0.setoldValue(_oldValue == Value._z ? 1 : 0);
		dr0.mycompute();
		_precedent = dr0;
		_x += length;
		return dr0;
	}

	// return a new value or null
	private DrawZ getZ(int length, boolean b) {
		if (_precedent instanceof DrawZ){
			_precedent.incLength(length);
			_precedent.mycompute();
			_x += length ;
			return null;
		}
		if (_precedent != null) {
			_precedent.setFuturLevel(Level.z);
			_precedent.mycompute();
			_precedent.repaint();
		}
		DrawZ drz = new DrawZ(_vcdFactory, _x, _y, length,b);
		if (_precedent != null){
			drz.setPrecedeLevel(_precedent.getLevel());
		}
		drz.setoldValue(_oldValue == Value._z ? 1 : 0);
		drz.mycompute();
		_x += length;
		_precedent = drz;
		return drz;
	}

	// return a new value or null
	private ExtendFigure getX(int length) {
		if (_precedent instanceof DrawX){
			_precedent.incLength(length);
			_precedent.mycompute();
			_x += length ;
			return null;
		}
		DrawX drawX = new DrawX(_vcdFactory, _x, _y, length);
		if (_precedent != null){
			drawX.setPrecedeLevel(_precedent.getLevel());
		}
		_x += length;
		drawX.setGhostvisible(_isGhostVisible); // TODO
		_ghostinclock = true;
		_nbGhost++;
		_ghostFigures.add(drawX);
		_precedent = drawX;
		return drawX;
	}

	public void setGhostVisible(boolean b) {
		for (DrawX f : _ghostFigures) {
			f.setGhostvisible(b);
			f.mycompute();
			_isGhostVisible = b;
		}
	}
	
	public void clear(){
		if (_tickFigures!=null){
			for (IFigure ifi: _tickFigures)
			{
				if (ifi!=null)
					ifi.erase();
			}
			_tickFigures.clear();
		}
		if (_ghostFigures!=null){
			for (IFigure ifi: _ghostFigures)
			{
				if (ifi!=null){
					ifi.erase();
				}
			}
			_ghostFigures.clear();
		}
		_ghostFigures=null;
		_precedent=null;
		_tickFigures=null;
		_vcdFactory=null;
		_currentValue=null;
	}
}
