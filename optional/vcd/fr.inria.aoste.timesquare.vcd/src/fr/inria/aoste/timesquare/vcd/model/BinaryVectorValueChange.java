/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.model;

public final class BinaryVectorValueChange extends VectorValueChange {
	
	private String _value;
	private Value[] _values;

	public BinaryVectorValueChange(String value, String ident) {
		super('b', ident);
		check(value.toLowerCase());
	}

	@Override
	public void setValue(Object value) {
	}

	private void check(String value) {
		char[] buf = value.toCharArray();
		this._values = new Value[buf.length];
		for (int i = 0; i < buf.length; i++)
			this._values[i] = Value.fromChar(buf[i]);
		this._value = value;
	}

	@Override
	public Object getValue() {
		return this._value;
	}

}
