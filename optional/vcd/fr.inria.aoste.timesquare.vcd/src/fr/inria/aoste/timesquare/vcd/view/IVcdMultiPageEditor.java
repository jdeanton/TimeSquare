/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view;

import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.TraceCollector;

public interface IVcdMultiPageEditor {

	public abstract void setTc(TraceCollector tc);

	public abstract void setVcdModel(VCDDefinitions vcd);

	public abstract void setVcdFactory(VcdFactory factory);

	public abstract void syncModel2Text();
	
	public abstract void setActiveVCDPage();
}