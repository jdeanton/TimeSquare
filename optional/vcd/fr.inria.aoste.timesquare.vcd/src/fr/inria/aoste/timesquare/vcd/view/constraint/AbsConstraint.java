/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.view.constraint;

import java.util.ArrayList;

import org.eclipse.jface.action.Action;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.vcd.ListConnections;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.timesquare.vcd.view.IConstructContraint;
import fr.inria.aoste.timesquare.vcd.view.VcdFactory;
import fr.inria.aoste.timesquare.vcd.view.figure.ExtendFigure;

public abstract class AbsConstraint implements IConstraint {

	protected VcdFactory vcdFactory = null;
	protected VCDDefinitions vcddef = null;
	protected IConstraintData cc = null;
	protected ListConnections list;
	protected IConstructContraint icc;
	protected boolean isConstraintVisible = false;
	protected boolean isGlobal = false;
	protected VcdColorPreferences mca;

	public VcdColorPreferences getMca() {
		return mca;
	}

	public void setMca(VcdColorPreferences mca) {
		this.mca = mca;
	}

	public final VcdFactory getVcdFactory() {
		return vcdFactory;
	}

	public final void setVcdFactory(VcdFactory vcdFactory) {
		this.vcdFactory = vcdFactory;
		if (vcdFactory != null) {
			mca = vcdFactory.getMca();
			vcddef = vcdFactory.getVcdDefinitions();
			if (vcddef == null){
				ErrorConsole.println("Warning VCDDef is null");
			}
		}
		return;
	}

	public IConstraintData getCc() {
		return cc;
	}

	public final void setCc(IConstraintData cc) {
		this.cc = cc;
	}

	public final ListConnections getList() {
		return list;
	}

	public final void setList(ListConnections list) {
		this.list = list;
	}

	public final IConstructContraint getIcc() {
		return icc;
	}

	public final void setIcc(IConstructContraint icc) {
		this.icc = icc;
	}

	public final boolean getIsConstraintVisible() {
		return isConstraintVisible;
	}

	public final void setIsConstraintVisible(boolean b) {
		isConstraintVisible = b;
	}

	protected int testn = 1;

	public final boolean haveAllClockVisible() {
		if (cc.getClock() == null){
			return true;
		}
		if (vcdFactory.getNameforfigures().get(cc.getClock()) == null){
			return false;
		}
		if ((cc.getReferenceClocks() == null) || (cc.getReferenceClocks().size() == 0)){
			return true;
		}
		if (vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(0)) == null){
			return false;
		}
		if (testn >= 2){
			if (vcdFactory.getNameforfigures().get(cc.getReferenceClocks().get(1)) == null){
				return false;
			}
		}
		return true;
	}

	protected final ExtendFigure getTick(String clock, int num) {
		if (num < 0){
			return null;
		}
		ArrayList<ExtendFigure> ticks = vcdFactory.getNameforfigures().get(clock);
		if (num < ticks.size()){
			return ticks.get(num);
		}
		return null;
	}

	protected final int getIndice(String clock, ExtendFigure figure) {
		ArrayList<ExtendFigure> ticks = vcdFactory.getNameforfigures().get(clock);
		return ticks.indexOf(figure);
	}

	@Override
	protected void finalize() throws Throwable {

		if (list != null) {
			list.clearAll();
			list.clear();
		}
		super.finalize();
	}

	public Action[] getAction() {
		return new Action[] {};
	}

}
