/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 * @author Zhichao MAI
 */

package fr.inria.aoste.timesquare.vcd.model;

import java.util.ArrayList;

import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;

import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;

public class Timeline extends Polyline{
	
	private VcdColorPreferences _mca;
	
	private PointList _pointListRef = new PointList();
	
	private int 
		_startPointX ,
		_startPointY,
		_endPointX,
		_endPointY;
	
	private static int 
		_unitTimeLength,
		_unitTimeLengthRef,
	 	_height = 20,
	 	_halfHeight = 12,
	 	_tHeight = 5;
		
	
	private ArrayList<Label> _labels = new ArrayList<Label>();
	
	private ArrayList<Integer> _labelsPositionXRef = new ArrayList<Integer>();
	
	public ArrayList<Label> getLabels(){
		return this._labels;
	}
	
	public Timeline(Point startPoint, int unitTimeLength, VcdColorPreferences mca) {
		super();
		this.setOpaque(false);
		this._mca = mca;
		_unitTimeLength = unitTimeLength * 2;
		_unitTimeLengthRef = _unitTimeLength;
		_startPointX = startPoint.x;
		_startPointY = startPoint.y  + 10;
		_endPointX = _startPointX;
		_endPointY = _startPointY;
		PointList pl = new PointList();
		pl.addPoint(_startPointX, _startPointY + _height);
		_pointListRef.addPoint(_startPointX, _startPointY + _height);
		this.setPoints(pl);
		this.setForegroundColor(_mca.colorWhiteRule());
	}
	
	public void extendTimeLine(String labelText) {
		PointList pl = this.getPoints();
		pl.addPoint(_endPointX, _endPointY);
		_pointListRef.addPoint(_endPointX, _endPointY);
		Label label = new Label(labelText);
		label.setBounds(new Rectangle(0, 0, 60, 30));
		Point p = new Point(pl.getLastPoint());
		p.x += 2;
		label.setLocation(p);
		_labelsPositionXRef.add(p.x);
		label.setForegroundColor(_mca.colorRedRule());
		label.setLabelAlignment(PositionConstants.LEFT);
		_labels.add(label);
		this.add(label);
		int i = 0;
		for(i = 1; i<5; i++){
			pl.addPoint(_endPointX + _unitTimeLength*i, _endPointY);
			pl.addPoint(_endPointX + _unitTimeLength*i, _endPointY + _tHeight);
			pl.addPoint(_endPointX + _unitTimeLength*i, _endPointY);
			_pointListRef.addPoint(_endPointX + _unitTimeLength*i, _endPointY);
			_pointListRef.addPoint(_endPointX + _unitTimeLength*i, _endPointY + _tHeight);
			_pointListRef.addPoint(_endPointX + _unitTimeLength*i, _endPointY);
		}
		pl.addPoint(_endPointX + _unitTimeLength*5, _endPointY);
		pl.addPoint(_endPointX + _unitTimeLength*5, _endPointY + _halfHeight);
		pl.addPoint(_endPointX + _unitTimeLength*5, _endPointY);
		_pointListRef.addPoint(_endPointX + _unitTimeLength*5, _endPointY);
		_pointListRef.addPoint(_endPointX + _unitTimeLength*5, _endPointY + _halfHeight);
		_pointListRef.addPoint(_endPointX + _unitTimeLength*5, _endPointY);
		for(i = 6; i<10; i++) {
			pl.addPoint(_endPointX + _unitTimeLength*i, _endPointY);
			pl.addPoint(_endPointX + _unitTimeLength*i, _endPointY + _tHeight);
			pl.addPoint(_endPointX + _unitTimeLength*i, _endPointY);
			_pointListRef.addPoint(_endPointX + _unitTimeLength*i, _endPointY);
			_pointListRef.addPoint(_endPointX + _unitTimeLength*i, _endPointY + _tHeight);
			_pointListRef.addPoint(_endPointX + _unitTimeLength*i, _endPointY);
		}
		pl.addPoint(_endPointX + 10*_unitTimeLength, _endPointY);
		pl.addPoint(_endPointX + 10*_unitTimeLength, _endPointY + _height);
		_pointListRef.addPoint(_endPointX + 10*_unitTimeLength, _endPointY);
		_pointListRef.addPoint(_endPointX + 10*_unitTimeLength, _endPointY + _height);
		_endPointX = _endPointX + 10*_unitTimeLength;
		Panel timelinePanel = (Panel)getParent();
		timelinePanel.setPreferredSize(_endPointX, timelinePanel.getPreferredSize().height);
		
	}
	
	public void timeLineEnd() {
		this.getPoints().addPoint(_endPointX, _endPointY);
		_pointListRef.addPoint(_endPointX, _endPointY);
	}
	
	public int getStartPointX(){
		return this._startPointX;
	}

	public void setStartPointX(int x) {
		if (x < 0)
			throw new IllegalArgumentException("the x starting point must be positive: "+x);
		_startPointX=x;
	}

	public int getStartPointY() {
		return this._startPointY;
	}
	
	public void setStartPointY(int y) {
		if (y < 0)
			throw new IllegalArgumentException("the y starting point must be positive: "+y);
		_startPointY=y;
	}
	
	public int getEndPointX(){
		return this._endPointX;
	}
	
	public void setEndPointX(int x) {
		if (x < 0)
			throw new IllegalArgumentException("the x starting point must be positive: "+x);
		_endPointX=x;
	}
	
	public int getEndPointY(){
		return this._endPointY;
	}
	
	public void setEndPointY(int y) {
		if (y < 0)
			throw new IllegalArgumentException("the x starting point must be positive: "+y);
		_endPointY=y;
	}
	
	public void setStartPoint(Point sp) {
		this._startPointX = sp.x;
		this._startPointY = sp.y;
	}
	
	public void setEndPoint(Point sp) {
		this._endPointX = sp.x;
		this._endPointY = sp.y;
	}
	
	public int getUnitTimeLength() {
		return Timeline._unitTimeLength;
	}
	
	public void setUnitTimeLength(int utl) {
		Timeline._unitTimeLength = utl;
	}

	public void timelineZoom(double value){
		PointList pl = new PointList();
		pl.addAll(_pointListRef);
		int sizeofpl = pl.size();
		Point p = null;
		for(int i = 0; i < sizeofpl; i++){
			p = pl.getPoint(i);
			p.setX( (int) ((p.x - _startPointX)*value + _startPointX) );
			pl.setPoint(p, i);
		}
		int index = 0;
		int labelPositionX;
		for(Label label: _labels) {
			p = label.getLocation();
			labelPositionX = _labelsPositionXRef.get(index);
			p.x = (int) ((labelPositionX - _startPointX -2) * value+ _startPointX + 2);
			label.setLocation(p);
			index++;
		}
		this.removeAllPoints();
		this.setPoints(pl);
		_unitTimeLength = (int) (_unitTimeLengthRef * value);
		_endPointX = pl.getLastPoint().x;
		Panel timelinePanel = (Panel)getParent();
		timelinePanel.setPreferredSize(this.getSize().width, timelinePanel.getSize().height);
	}
	
	
	public void timeLineZoomIn() {
		PointList pl = new PointList();
		pl.addAll(this.getPoints());
		int sizeofpl = pl.size();
		Point p = null;
		for(int i=0; i<sizeofpl; i++){
			p = pl.getPoint(i);
			p.setX( (p.x - _startPointX) * 2 + _startPointX);
			pl.setPoint(p, i);
		}
		for(Label label: _labels) {
			p = label.getLocation();
			p.x = (p.x - _startPointX -2) * 2 + _startPointX + 2;
			label.setLocation(p);
		}
		this.removeAllPoints();
		this.setPoints(pl);
		_unitTimeLength = _unitTimeLength * 2;
		_endPointX = pl.getLastPoint().x;
		Panel timelinePanel = (Panel)getParent();
		timelinePanel.setPreferredSize(this.getSize().width, timelinePanel.getSize().height);
		
	}
	
	public void timeLineZoomOut() {
		PointList pl = new PointList();
		pl.addAll(this.getPoints());
		int sizeofpl = pl.size();
		Point p = null;
		for(int i=0; i<sizeofpl; i++){
			p = pl.getPoint(i);
			p.setX( (p.x - _startPointX ) / 2 + _startPointX);
			pl.setPoint(p, i);
		}
		for(Label label: _labels) {
			p = label.getLocation();
			p.x = (p.x - _startPointX -2 ) / 2 + _startPointX + 2;
			label.setLocation(p);
		}
		this.removeAllPoints();
		this.setPoints(pl);
		_unitTimeLength = _unitTimeLength / 2;
		_endPointX = pl.getLastPoint().x;
		Panel timelinePanel = (Panel)getParent();
		timelinePanel.setPreferredSize(this.getSize().width, timelinePanel.getSize().height);
	}

}
