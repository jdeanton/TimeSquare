/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.dialogs;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.inria.aoste.timesquare.vcd.IVcdDiagram;
import fr.inria.aoste.timesquare.vcd.model.IVar;

public class FindDialog extends Dialog {
	private IVcdDiagram _vcdDiagram;
	private String _title;
	private String _message;
	private String _value = "";
	private String _value2 = "";
	private Button _okButton;
	private CCombo _combo;
	private Text _text2;
	private Text _errorMessageText;
	private String _errorMessage;
	private String _message2;
	private int[] _clockedge = null;
	private int _max = 0;
	String[] _clocknames=null;
	

	protected void validateInput() {

		if (_combo.getSelectionIndex() != -1) {

			int sel = _combo.getSelectionIndex();
			if ((sel != -1) && (_clockedge != null) && (sel < _clockedge.length)) {
				_max = _clockedge[sel];

				setErrorMessage("Clock  " + _combo.getItem(sel) + " have  "
						+ _max + "  edge(s) ", ColorConstants.black);
			} else {
				_max = 0;
			}
			String s = _text2.getText();
			if (s == null || s.equals("")) {
				_okButton.setEnabled(false);
			} else {
				int n = Integer.parseInt(s);
				if ((n < _max + 1) && (n != 0)) {
					_okButton.setEnabled(true);
					setErrorMessage("Clock  " + _combo.getItem(sel) + " have  "
							+ _max + "  edge(s) ", ColorConstants.black);
				} else {
					_okButton.setEnabled(false);
					setErrorMessage("ERROR : Clock  " + _combo.getItem(sel)
							+ " have  " + _max + "  edge(s) ",
							ColorConstants.red);
				}
			}

		} else {
			setErrorMessage("Warning : no clock selected ",
					ColorConstants.darkBlue);
			_okButton.setEnabled(false);
		}

	}

	protected Button getOkButton() {
		return _okButton;
	}
	

	@Override
	protected void buttonPressed(int buttonId) {
		try
		{
			if (buttonId == IDialogConstants.OK_ID) {
				int n= _combo.getSelectionIndex();		
				if (n!=-1 && _clocknames!=null && n< _clocknames.length)
					_value= _clocknames[n];		
				_value2 = _text2.getText();
				System.out.println("--->"+_value+"<--"+n);
			} else {
				_value = null;
				_value2 = null;
			}
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
		super.buttonPressed(buttonId);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (_title != null) {
			shell.setText(_title);
		}
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		_okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		_okButton.setEnabled(false);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		GridData data = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.GRAB_VERTICAL | GridData.HORIZONTAL_ALIGN_FILL
				| GridData.VERTICAL_ALIGN_CENTER);
		data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
		if (_message != null) {
			Label label = new Label(composite, SWT.WRAP);
			label.setText(_message);
			label.setLayoutData(data);
			label.setFont(parent.getFont());
		}
		_combo = new CCombo(composite, SWT.NONE | SWT.READ_ONLY);
		_combo.setBackground(new Color(null, 255, 255, 255));
		int size = _vcdDiagram.getTraceCollector().getSelectedClocks().size();
		String[] clockalias = new String[size];
		 _clocknames = new String[size];
		_clockedge = new int[size];
		int i = 0;
		for (IVar var : _vcdDiagram.getTraceCollector().getSelectedClocks()) {
			int n = 0;
			if (var.getValueFactory() != null){
				n = var.getValueFactory().getEdge();
			}
			clockalias[i] = var.getAliasName() ; //Name();
			_clocknames[i]= var.getName();
			_clockedge[i] = n;
			i++;
		}
		_combo.setItems(clockalias);
		_combo.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		
		_combo.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
			public void widgetSelected(SelectionEvent e) {
				validateInput();
			}
		});
		
		if (_message2 != null) {
			Label label = new Label(composite, SWT.WRAP);
			label.setText(_message2);
			data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
			label.setLayoutData(data);
			label.setFont(parent.getFont());
		}
		_text2 = new Text(composite, SWT.SINGLE | SWT.BORDER);
		_text2.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		
		_text2.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				validateInput();
			}
		});
		
		_text2.addListener(SWT.Verify, new ListernerInt());
		_errorMessageText = new Text(composite, SWT.READ_ONLY | SWT.WRAP);
		_errorMessageText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		_errorMessageText.setBackground(_errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		setErrorMessage(_errorMessage, ColorConstants.black);
		applyDialogFont(composite);
		return composite;
	}
	
	public static class ListernerInt implements Listener {

		public ListernerInt() {
			super();
		}

		public void handleEvent(Event e) {
			String string = e.text;
			char[] chars = new char[string.length()];
			string.getChars(0, chars.length, chars, 0);
			for (int i = 0; i < chars.length; i++) {
				if (!('0' <= chars[i] && chars[i] <= '9')) {
					e.doit = false;
					return;
				}
			}
		}
	}

	public FindDialog(Shell parentShell, String dialogTitle,
			String dialogMessage, String dialogMessage2, String initialValue,
			IVcdDiagram vdt) {
		super(parentShell);
		this._title = dialogTitle;
		_message = dialogMessage;
		_message2 = dialogMessage2;
		if (initialValue == null) {
			_value = "";
		} else {
			_value = initialValue;
		}

		this._vcdDiagram = vdt;
	}

	public String getClock() {
		return _value;
	}

	public String getInstance() {
		try
		{
			int n = Integer.parseInt(_value2);
		return ""+(n-1);
		}
		catch (Throwable e) {
		 return null;
		}
	}

	public String getMessage2() {
		return _message2;
	}

	public void setMessage2(String message2) {
		this._message2 = message2;
	}
	
	public void setErrorMessage(String errorMessage, Color c) {
		this._errorMessage = errorMessage;
		if (_errorMessageText != null && !_errorMessageText.isDisposed()) {
			_errorMessageText.setText(errorMessage == null ? " \n "
					: errorMessage);
			boolean hasError = errorMessage != null
					&& (StringConverter.removeWhiteSpaces(errorMessage))
							.length() > 0;
			_errorMessageText.setEnabled(hasError);
			_errorMessageText.setVisible(hasError);
			_errorMessageText.setForeground(c);
			_errorMessageText.getParent().update();
		}
	}
	

}
