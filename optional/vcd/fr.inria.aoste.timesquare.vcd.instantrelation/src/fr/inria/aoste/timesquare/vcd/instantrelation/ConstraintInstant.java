/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.instantrelation;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.vcd.relation.extensionpoint.Instant;

public class ConstraintInstant extends Instant {
	EObject eo;

	public ConstraintInstant( int instantNumber, EObject eo) {
		super("", instantNumber);
		this.eo = eo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((eo == null) ? 0 : eo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!( obj instanceof ConstraintInstant))
			return false;
		ConstraintInstant other = (ConstraintInstant) obj;
		if (eo == null) {
			if (other.eo != null)
				return false;
		} else if (!eo.equals(other.eo))
			return false;
		return true;
	}

	
	
}
