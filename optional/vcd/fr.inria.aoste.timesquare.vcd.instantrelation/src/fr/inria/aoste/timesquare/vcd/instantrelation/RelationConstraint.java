/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.instantrelation;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.trace.util.SearchEventOccurence;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.vcd.model.comment.IConstraintData;
import fr.inria.aoste.timesquare.vcd.model.comment.Moderator;
import fr.inria.aoste.timesquare.vcd.view.constraint.IConstraint;
import fr.inria.aoste.trace.Trace;

public class RelationConstraint implements IConstraintData {

	
	
	public RelationConstraint() {
		super();
		
	}

	public Trace getTrace()
	{
		try
		{ 
			if (_ccslConstraintRef==null){
				return null;
			}
			Resource rs= _ccslConstraintRef.eResource();
			if (rs==null){
				return null;
			}
			ResourceSet rset=rs.getResourceSet();
			if (rset==null){
				return null;
			}
			for (Resource rc: rset.getResources())
			{
				if (rc.getContents()!=null)
					if (rc.getContents().size()!=0)
					{
						EObject eo=rc.getContents().get(0);
						if (eo instanceof Trace){
							return (Trace) eo ;
						}
					}
			}
		}
		catch (Throwable e) {
			return null;
		}
		return null;
	}
	
	
	public SearchEventOccurence getSearchClockState()
	{
		Trace ccsltrace=getTrace();
		if (ccsltrace!=null)
		{
			return new SearchEventOccurence(ccsltrace); 
			//ccsltrace.getSearchClockState();
		}
		return null;
	}
	/**
	 * 
	 * @return
	 * public for tets
	 */
	public final CCSLConstraintRef getCc() {
		return _ccslConstraintRef;
	}

	protected final void setCc(CCSLConstraintRef cc) {
		this._ccslConstraintRef = cc;
	}

	public RelationConstraint(CCSLConstraintRef cc) {
		super();
		this._ccslConstraintRef = cc;
	}

	CCSLConstraintRef _ccslConstraintRef;
	
	
	public String getClock() {
		return null;
	}

	
	public IConstraint getConstraints() {
		// TODO Auto-generated method stub
		return ic;
	}

	
	public String getLabeling() {
		
		return null;
	}

	
	public Moderator getModerator() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ArrayList<String> getReferenceClocks() {
		// TODO Auto-generated method stub
		return null;
	}

	IConstraint ic=null;

	protected final void setIc(IConstraint ic) {
		this.ic = ic;
	}

	
	public String toString() {
		if ( _ccslConstraintRef.getCcslConstraint()!=null)
		{	
			String s= AdapterRegistry.getAdapter(_ccslConstraintRef.getCcslConstraint()).getReferenceName(_ccslConstraintRef.getCcslConstraint());  
				//cc.getCcslConstraint().toString();
			if ( s!=null && s.length()!=0){
				return s;
			}
		
		}
		return _ccslConstraintRef.toString();
	}
	
}
