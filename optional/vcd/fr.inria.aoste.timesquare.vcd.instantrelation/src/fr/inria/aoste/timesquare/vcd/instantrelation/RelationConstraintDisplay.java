/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.instantrelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.impl.XMLHelperImpl;
import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence;
import fr.inria.aoste.timesquare.trace.util.SearchEventOccurence;
import fr.inria.aoste.timesquare.vcd.figure.ConstraintsConnection;
import fr.inria.aoste.timesquare.vcd.model.VcdChopboxAnchor;
import fr.inria.aoste.timesquare.vcd.relation.extensionpoint.Instant;
import fr.inria.aoste.timesquare.vcd.view.Eventline;
import fr.inria.aoste.timesquare.vcd.view.constraint.AbsConstraint;
import fr.inria.aoste.timesquare.vcd.view.figure.Draw1;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.Reference;

public class RelationConstraintDisplay extends AbsConstraint/* implements IConstraint*/ {

	public RelationConstraintDisplay() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int draw(Draw1 currentfig, String currentclock) {
		RelationConstraint icd = getCc();

		int index = getIndice(currentclock, currentfig);
		if (icd !=null) {
			RelationConstraint rc = (RelationConstraint) icd;
			try {
				
				Object o = vcddef.getClocklistener().getDataonTickClock(currentclock, index, "clockstate");
				if (!(o instanceof EventOccurrence)) {
					o = null;
					EObject eo = getRefenceClock(rc, currentclock);
					if (eo == null) {
						return -1;
					}
					SearchEventOccurence scs = rc.getSearchClockState();
					if (scs != null) {
						o = scs.findClockState(eo, index);
					}
				}
				EventOccurrence cs = null;
				if (o instanceof EventOccurrence) {
					cs = (EventOccurrence) o;
				}
				vcddef.getClocklistener().putDataonTickClock(currentclock, index, "clockstate", cs);
				drawforfig(currentfig, cs);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
	public int drawOneTick(int tickInstant, String currentclock) {
		RelationConstraint icd = getCc();
		Eventline cl = vcdFactory.getClocklinesMap().get(currentclock);
		int index = cl.getTickInstants().indexOf(tickInstant); //Index of tick
		if (icd !=null) {
			RelationConstraint rc = (RelationConstraint) icd;
			try {

				Object o = vcddef.getClocklistener().getDataonTickClock(currentclock, index, "clockstate");
				if (!(o instanceof EventOccurrence)) {
					o = null;
					EObject eo = getRefenceClock(rc, currentclock);
					if (eo == null) {
						return -1;
					}
					SearchEventOccurence scs = rc.getSearchClockState();
					if (scs != null) {
						o = scs.findClockState(eo, index);
					}
				}
				EventOccurrence cs = null;
				if (o instanceof EventOccurrence) {
					cs = (EventOccurrence) o;
				}
				vcddef.getClocklistener().putDataonTickClock(currentclock, index, "clockstate", cs);
				Instant instant = cl.getIntants().get(tickInstant);
				drawOneTickMore(instant, cs);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	

	protected EObject getRefenceClock(RelationConstraint icd, String currentclock) {
		try {
			EObject eo = null;

			Object o = vcddef.getClocklistener().getDataonClock(currentclock, "eobjetxmi");
			if (o instanceof EObject) {
				return (EObject) o;
			}
			String xmi = (String) vcddef.getClocklistener().getDataonClock(currentclock, "xmi");

			// URIConverter uric = ((RelationConstraint) icd).cc.eResource().getResourceSet().getURIConverter();
			if (icd._ccslConstraintRef.eResource() == null)
				return null;
			URI uri1 = icd._ccslConstraintRef.eResource().getURI();
			URI uri = URI.createURI(xmi);
			if (uri.isPlatform()) {
				uri1 = uri;
			} else {
				XMLHelperImpl xmlimp = new XMLHelperImpl();
				uri1 = xmlimp.resolve(uri, uri1);
			}
			eo = icd._ccslConstraintRef.eResource().getResourceSet().getEObject(uri1, false);
			//System.out.println("Find Clock : " +  AdapterRegistry.getAdapter(eo).getReferenceName( eo));
			vcddef.getClocklistener().putDataonClock(currentclock, "eobjetxmi", eo);
			return eo;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	protected void drawforfig(Draw1 fig, EventOccurrence csfig) {

		OccurrenceRelation[] relations = getRelationOf(fig.getInstant(), csfig);

		Set<OccurrenceRelation> set = new HashSet<OccurrenceRelation>();
		set.addAll(Arrays.asList(relations));
		ArrayList<OccurrenceRelation> distinctList = new ArrayList<OccurrenceRelation>(set);
		ArrayList<Instant> instantsCaller = new ArrayList<Instant>();
		mainLoop: for (OccurrenceRelation r : distinctList) {
			if (r instanceof Packet) {
				for (EventOccurrence c : ((Packet) r).getOccurrences()) {
					if (Compare.compare(fig.getInstant(), c)) {
						for (EventOccurrence cs : ((Packet) r)
								.getOccurrences()) {
							instantsCaller.add(clockStateToInstant(cs));

						}
						break mainLoop;
					}
				}
			}

			if (instantsCaller.isEmpty()) {
				instantsCaller.add(fig.getInstant());
			}
		}
		for (OccurrenceRelation r : distinctList) {
			try {
				drawTickConstraints(r,((CCSLConstraintRef) r.eContainer()).getRelatedOccurrenceRelations().indexOf(r) ,instantsCaller);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	protected void drawOneTickMore(Instant instant, EventOccurrence csfig) {
		//Instant instant = vcdFactory.getClocklinesMap().get(clockId).getIntants().get(tickInstant);
		
		OccurrenceRelation[] relations = getRelationOf(instant, csfig);
		Set<OccurrenceRelation> set = new HashSet<OccurrenceRelation>();
		set.addAll(Arrays.asList(relations));
		ArrayList<OccurrenceRelation> distinctList = new ArrayList<OccurrenceRelation>(set);
		ArrayList<Instant> instantsCaller = new ArrayList<Instant>();
		mainLoop: for (OccurrenceRelation r : distinctList) {
			if (r instanceof Packet) {
				for (EventOccurrence c : ((Packet) r).getOccurrences()) {
					if (Compare.compare(instant, c)) {
						for (EventOccurrence cs : ((Packet) r)
								.getOccurrences()) {
							instantsCaller.add(clockStateToInstant(cs));

						}
						break mainLoop;
					}
				}
			}
			if (instantsCaller.isEmpty()) {
				instantsCaller.add(instant);
			}
		}
		for (OccurrenceRelation r : distinctList) {
			try {
				drawTickConstraints(r,((CCSLConstraintRef) r.eContainer()).getRelatedOccurrenceRelations().indexOf(r) ,instantsCaller);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	
	public OccurrenceRelation[] getRelationOf(Instant instant, EventOccurrence csin) {

		ArrayList<OccurrenceRelation> relations = new ArrayList<OccurrenceRelation>();
		CCSLConstraintRef cc = ((RelationConstraint) getCc()).getCc();

		for (OccurrenceRelation r : cc.getRelatedOccurrenceRelations()) {
			if (r instanceof Coincidence) {
				for (EventOccurrence c : ((Coincidence) r).getCoincidentOccurrences())
					if (Compare.compare(instant, c)) {
						relations.add(r);
					}
			}
			if (r instanceof Precedence) {
				if (Compare.compare(instant, ((Precedence) r).getSource())) {
					relations.add(r);
				}
				if (Compare.compare(instant, ((Precedence) r).getTarget())) {
					relations.add(r);
				}
			}
			if (r instanceof Packet) {
				EList<EventOccurrence> listInstants = ((Packet) r).getOccurrences();
				for (EventOccurrence c : listInstants)
					if (Compare.compare(instant, c)) {
						relations.add(r);
						ArrayList<OccurrenceRelation> relationsOfFirstInstant = getRelationOf(
								clockStateToInstant(listInstants.get(0)),
								cc);
						Packet p = null;
						for (OccurrenceRelation rel : relationsOfFirstInstant) {
							if (rel instanceof Precedence) {
								Precedence rp = (Precedence) rel;
								Instant i = clockStateToInstant(rp
										.getSource());
								relations.addAll(getRelationOf(i, cc));
							}
							if (rel instanceof Packet) {
								p = (Packet) rel;
							}
						}
						if (p != null)
							relationsOfFirstInstant.remove(p);
						p = null;
						if (listInstants.size() > 1) {
							ArrayList<OccurrenceRelation> relationsOfLastInstant = getRelationOf(
									clockStateToInstant(listInstants
									.get(listInstants.size() - 1)),
									cc);

							for (OccurrenceRelation rel : relationsOfLastInstant) {
								if (rel instanceof Precedence) {
									Precedence rp = (Precedence) rel;
									Instant i = clockStateToInstant(rp.getTarget());
									relations.addAll(getRelationOf(i, cc));
								}
								if (rel instanceof Packet) {
									p = (Packet) rel;
								}
							}
							if (p != null)
								relationsOfLastInstant.remove(p);
							relations.addAll(relationsOfLastInstant);
						}

						relations.addAll(relationsOfFirstInstant);

					}
			}
		}

		return relations.toArray(new OccurrenceRelation[relations.size()] );

	}

	public int drawTableItemConstraints() {

		CCSLConstraintRef cc = ((RelationConstraint) getCc())._ccslConstraintRef;
		for (OccurrenceRelation r : cc.getRelatedOccurrenceRelations()) {
			try {

				ArrayList<Instant> instantsCaller = new ArrayList<Instant>();
				instantsCaller.add(new ConstraintInstant( -1,cc));

				drawTickConstraints(r,cc.getRelatedOccurrenceRelations().indexOf(r), instantsCaller);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		return 0;
	}

	
	
	
	@Override
	public RelationConstraint getCc() {
		return (RelationConstraint) super.getCc();
	}

	/**
	 * 
	 * @param r
	 * @param indice: the selected number of checkBox
	 * @param instantsCaller
	 */
	private void drawTickConstraints(OccurrenceRelation r,int indice ,ArrayList<Instant> instantsCaller) {
		
		
		ConstraintsConnection constraint = null;
		if(r instanceof Coincidence){
			Instant i1 = clockStateToInstant(((Coincidence) r).getCoincidentOccurrences().get(0));
			Instant i2 = clockStateToInstant(((Coincidence) r).getCoincidentOccurrences().get(1));
			if(vcdFactory.getClocklinesXmiMap().containsKey(i1.getXmiId()) &&
					vcdFactory.getClocklinesXmiMap().containsKey(i2.getXmiId())){
				Eventline c1 = vcdFactory.getClocklinesXmiMap().get(i1.getXmiId());
				Eventline c2 = vcdFactory.getClocklinesXmiMap().get(i2.getXmiId());
				if(c1 !=null && c2!= null){
					constraint = icc.constructCoincidenceConnection(mca.colorRedCoincidence(), c1, i1.getInstantNumber(), c2, i2.getInstantNumber());
					constraint.getInstants().add(i1);
					constraint.getInstants().add(i2);
					constraint.getInstantsCaller().addAll(instantsCaller);
					
					for (ConstraintsConnection cc : list.getListConstraints()) {
						if( ((VcdChopboxAnchor)(cc.getTargetAnchor())).equals( ((VcdChopboxAnchor)(constraint.getTargetAnchor())) ) 
								&& ( (VcdChopboxAnchor)(cc.getSourceAnchor()) ).equals( ((VcdChopboxAnchor)(constraint.getSourceAnchor())) )){
							if(cc.equals(constraint)){
								cc.setVisible(!cc.isVisible());								
								icc.redraw(cc);
								constraint.erase();
//								list.getListConstraints().remove(cc);
//								cc.erase();
								return;
							}
						}
					}
					constraint.setComment(getCc());
					constraint.setIndice(indice);
					list.getListConstraints().add(constraint);
				}
			}
			if (constraint != null){
				icc.addAndRedraw(constraint);
			}
		}
		if (r instanceof Precedence) {
			Instant i1 = clockStateToInstant(((Precedence) r).getSource());
			Instant i2 = clockStateToInstant(((Precedence) r).getTarget());
			if(vcdFactory.getClocklinesXmiMap().containsKey(i1.getXmiId()) &&
					vcdFactory.getClocklinesXmiMap().containsKey(i2.getXmiId())){
				Eventline c1 = vcdFactory.getClocklinesXmiMap().get(i1.getXmiId());
				Eventline c2 = vcdFactory.getClocklinesXmiMap().get(i2.getXmiId());
				if(c1 !=null && c2!=null){
					Color color = mca.colorWhiteArrow();
					constraint = icc.constructDashConnection(color, c1, i1.getInstantNumber(), c2,i2.getInstantNumber(),((Precedence) r).isIsStrict());
					constraint.getInstants().add(i1);
					constraint.getInstants().add(i2);
					constraint.getInstantsCaller().addAll(instantsCaller);
					for (ConstraintsConnection cc : list.getListConstraints()) {
						if( ((VcdChopboxAnchor)(cc.getTargetAnchor())).equals( ((VcdChopboxAnchor)(constraint.getTargetAnchor())) ) 
								&& ( (VcdChopboxAnchor)(cc.getSourceAnchor()) ).equals( ((VcdChopboxAnchor)(constraint.getSourceAnchor())) )){
							if(cc.equals(constraint)){
								cc.setVisible(!cc.isVisible());								
								icc.redraw(cc);
								constraint.erase();
								cc.erase();
								cc = null;
								return;
							}
						}
					}
					constraint.setComment(getCc());
					constraint.setIndice(indice);
					list.getListConstraints().add(constraint);
				}
			}
			if (constraint != null){
				icc.addAndRedraw(constraint);
			}
		}
	}

	private ArrayList<OccurrenceRelation> getRelationOf(Instant instant,
			CCSLConstraintRef cc) {

		ArrayList<OccurrenceRelation> relations = new ArrayList<OccurrenceRelation>();
		for (OccurrenceRelation r : cc.getRelatedOccurrenceRelations()) {
			if (r instanceof Coincidence) {
				for (EventOccurrence c : ((Coincidence) r).getCoincidentOccurrences())
					if (Compare.compare(instant, c)) {
						relations.add(r);
					}
			}
			if (r instanceof Precedence) {
				if (Compare.compare(instant, ((Precedence) r).getSource())) {
					relations.add(r);
				}
				if (Compare.compare(instant, ((Precedence) r).getTarget())) {
					relations.add(r);
				}
			}
			if (r instanceof Packet) {
				EList<EventOccurrence> listInstants = ((Packet) r)
						.getOccurrences();
				for (EventOccurrence c : listInstants)
					if (Compare.compare(instant, c)) {
						relations.add(r);
					}
			}
		}

		return relations;
	}

	private Instant clockStateToInstant(EventOccurrence cs) {

		if (cs.getReferedElement() !=null) {
			Reference clockstate = (cs.getReferedElement());
			String xmiId = null;
			try {
				xmiId = clockstate.eResource().getURIFragment(clockstate);			
				if (xmiId == null) {
					xmiId = "";
				}

			} catch (Throwable e) {			
					xmiId = "";
			}
			int counter = cs.getCounter();
			return new Instant(xmiId, counter);
		}
		return null;
	}
	

}
