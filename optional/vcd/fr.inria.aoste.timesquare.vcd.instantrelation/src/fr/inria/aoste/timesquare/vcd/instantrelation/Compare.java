/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.instantrelation;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.vcd.relation.extensionpoint.Instant;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.Reference;

public class Compare {

	static public boolean compare(Instant i, EventOccurrence cs) {
		try {
			if (cs.getReferedElement() !=null) {
				EObject clockstate = ((Reference) cs.getReferedElement());
				String xmiId = null;
				try {
					xmiId = clockstate.eResource().getURIFragment(clockstate);
				} catch (Throwable e) {
					System.err.println(e);
				}
				if (i.getXmiId() == null)
					return false;
				if (!i.getXmiId().equals(xmiId))
					return false;
				int counter = cs.getCounter();
				if (i.getInstantNumber() != counter)
					return false;
				return true;
			}
			return false;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}
	}
}

