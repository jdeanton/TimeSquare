/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.vcd.instantrelation;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelationModel;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.provider.CCSLRelationModelItemProviderAdapterFactory;
import fr.inria.aoste.timesquare.vcd.model.IComment;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.UpdateCommand;
import fr.inria.aoste.timesquare.vcd.model.Uposition;
import fr.inria.aoste.timesquare.vcd.model.VCDDefinitions;
import fr.inria.aoste.timesquare.vcd.model.visitor.IDeclarationVisitor;


public class ModelRelationComment implements ICommentCommand{
	
	String filename;
	
	public ModelRelationComment(String _filename) {
		super();
		System.out.println("Create Relation model rule :" +_filename);
		this.filename= cleanning(_filename);	
	}

	
	
	public String cleanning(String s){
		if (s==null)
			return null;
		if (s.startsWith("\""))
			s=s.substring(1);
		if (s.endsWith("\""))
			s=s.substring(0, s.length()-1);
		return s;
		
	}
	
	public ModelRelationComment() {
		super();
		System.out.println("Create Relation model rule");
	}

	private OccurrenceRelationModel cCSLRelation=null;
	
	private IPath path=null;

	public void setPath(IPath path) {
		this.path = path;
	}

	public IPath getPath() {
		return path;
	}

	
	public String getType() {
		return "relation";
	}

	
	public Uposition getUp() {
		return null;
	}

	
	public boolean isActive() {
		return true;
	}

	
	public void setActive(boolean active) {}

	
	public void setClockLink(String name) {}

	
	public int setClockListener(UpdateCommand clklst) {
		return 0;
	}

	
	public void setString(int n, String s) {		
		if (n==1){
			filename= cleanning(s);
		}
	}

	
	public void setUp(Uposition up) {}

	
	public int update(String newcommentvalue, IComment updatevalue) {
		return 0;
	}

	private VCDDefinitions vcd;
	
	public int validate(VCDDefinitions vcddef) {
		vcd=vcddef;		
		validate();		
		return 0;
	}

	
	public void validate() {
		if (vcd==null)
			return ;
		try
		{
		if (cCSLRelation!=null)
			return ; 
		IContainer ic=	vcd.getVcdfolder();
		if (ic==null)
			return ;
		if (filename==null)
			return ;
		path = ic.getLocation().append(filename);
		loadCCSLRelation();
		}
		catch (Throwable e) {
			System.err.println("Error  :" +e.getMessage());
			e.printStackTrace();
		}
		
	}

	
	public int clear() {
	
		return 0;
	}

	
	public void visit(IDeclarationVisitor visitor) {
		visitor.visitConstraintComment("relation   \""+filename+"\"" );  
		
	}
	
	
	private void loadCCSLRelation() {
		if (path == null)
			return;		
		System.out.println("FILE_PATH : " + path);
		// Requied for open vcd when Eclipse starting
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
		adapterFactory.addAdapterFactory(new CCSLRelationModelItemProviderAdapterFactory());
		//adapterFactory.addAdapterFactory(new RelationItemProviderAdapterFactory());
		//adapterFactory.addAdapterFactory(new ClockrelationItemProviderAdapterFactory());
		//adapterFactory.addAdapterFactory(new ClockexpressionItemProviderAdapterFactory());		adapterFactory.addAdapterFactory(new TraceItemProviderAdapterFactory());
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());

		
		URI uri = URI.createFileURI(path.toOSString());

		ResourceSet resourceSet= new ResourceSetImpl();		
		Resource resource = resourceSet.getResource(uri, true);	
		Object o= resource.getContents().get(0);	 	
		//System.out.println("file reading " + o);
		if (o instanceof OccurrenceRelationModel) {

			cCSLRelation = (OccurrenceRelationModel) o;
			EcoreUtil.resolveAll(cCSLRelation);
			for ( CCSLConstraintRef cc:((OccurrenceRelationModel) o).getRefsToCCSLConstraints())
			{
				addClockConstraint(cc);
			}
				
		}
		else
		{
			System.err.println("isnot instance of CCSLRelation");
		}
	}
	
//	private HashMap<CCSLConstraintRef, RelationConstraint> map= new HashMap<CCSLConstraintRef, RelationConstraint>();
	
	private void addClockConstraint(CCSLConstraintRef cc)
	{
		RelationConstraint rc= new RelationConstraint(cc);
		
		RelationConstraintDisplay rcd= new RelationConstraintDisplay();
		rc.setIc(rcd);
		rcd.setCc(rc);
		rcd.setVcdFactory(vcd.getFactory());
		vcd.addContraint(rc);
//		for (OccurrenceRelation r:cc.getRelatedOccurrenceRelations())
//		{
//			addRelation(rc ,r);
//		}
//		map.put(cc, rc);
	}
	
//	private void addRelation(RelationConstraint rc, OccurrenceRelation r)
//	{
//		
//	}



	
	public void newData(Object o) {
		// TODO Auto-generated method stub
		 if (o instanceof CCSLConstraintRef)
		 {
			 addClockConstraint((CCSLConstraintRef) o);
		 }
		if (o instanceof OccurrenceRelation)
		{
			//EObject eo= ((Relation) o).eContainer();
			
			
		}
	}
	
}
