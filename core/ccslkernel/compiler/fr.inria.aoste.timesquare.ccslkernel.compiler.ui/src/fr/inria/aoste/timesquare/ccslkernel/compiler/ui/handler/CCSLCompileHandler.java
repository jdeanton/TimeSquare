/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.ui.handler;

import java.io.IOException;
import java.util.Vector;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import fr.inria.aoste.timesquare.ccslkernel.compiler.Compiler;
import fr.inria.aoste.timesquare.ccslkernel.compiler.CompilerParameters;
import fr.inria.aoste.timesquare.ccslkernel.compiler.exceptions.CompilerException;
import fr.inria.aoste.timesquare.ccslkernel.compiler.ui.Activator;
import fr.inria.aoste.timesquare.ccslkernel.compiler.ui.dialog.CCSLCompilerProjectSelectionDialog;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;

public class CCSLCompileHandler extends AbstractHandler {

	public CCSLCompileHandler() {
	}

	private IFile ccslFile;
	private ResourceLoader loader = ResourceLoader.INSTANCE;
	private Compiler compiler;
	private CompilerParameters parameters;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			if (((IStructuredSelection) selection).size() == 1) {
				Object selected = ((IStructuredSelection) selection).getFirstElement();
				if (selected instanceof IFile) {
					ccslFile = (IFile) selected;
					String filename = ccslFile.getFullPath().toString();
					Resource resource = null;
					Exception exception = null;
					try {
						resource = loader.loadResource(filename);
					} catch (IOException e) {
						exception = e;
					}
					Shell parentShell = HandlerUtil.getActiveShell(event);
					if (exception != null) {
						IStatus status = null;
						if (exception instanceof CoreException) {
							status = ((CoreException) exception).getStatus();
						}
						else {
							status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
									exception.getLocalizedMessage(), exception);
						}
						ErrorDialog.openError(parentShell, null,
								"Resource " + filename + " IO load error: " + exception.getLocalizedMessage(),
								status);
						return null;
					}
					if ( ! resource.getErrors().isEmpty()) {
						// Resource has load errors
						Vector<IStatus> statusObjects = new Vector<IStatus>();
						for (Diagnostic diagnostic : resource.getErrors()) {
							statusObjects.add(new Status(IStatus.ERROR, Activator.PLUGIN_ID, diagnostic.getMessage()));
						}				
						IStatus[] allStatus = new IStatus[statusObjects.size()];
						for(int i = 0; i < statusObjects.size(); i++){
							IStatus s = statusObjects.get(i);
							allStatus[i] = s;
						}
						MultiStatus errors = new MultiStatus(Activator.PLUGIN_ID, IStatus.ERROR,
								allStatus, "Load errors for resource " + filename,
								null);
						ErrorDialog.openError(parentShell, null,
								"Resource " + filename + " load errors", errors);
						return null;
					}
					
					CCSLCompilerProjectSelectionDialog dialog = new CCSLCompilerProjectSelectionDialog(parentShell);
					
					if (dialog.open() != Window.OK) {
						return null;
					}
					parameters = new CompilerParameters();

					String newProjectName = dialog.getNewProjectName();
					if (newProjectName == null || newProjectName.isEmpty()) {
						Object result = dialog.getFirstResult();
						if (result == null) {
							return null;
						}
						else if ( !(result instanceof IProject)) {
							return null;
						}
						else {
							parameters.outputProjectName = ((IProject) result).getName();						
						}
					}
					else {
						parameters.outputProjectName = newProjectName;
					}
					parameters.outputProjectNamePrefix = null;
					parameters.packagePrefix = parameters.outputProjectName;
					parameters.modelsAreSingleton = dialog.getModelsAreSingleton();
					parameters.generateTests = dialog.getGenerateTest();
					parameters.generateMain = dialog.getGenerateMain();
					parameters.useRuntimeFilterBy = dialog.getUseRuntimeFilterBy();
					
					compiler = new Compiler(parameters);
					try {
						compiler.compile(resource);
					} catch (CompilerException e) {
						exception = e;
					}
					if (exception != null) {
						IStatus status = null;
						if (exception instanceof CoreException) {
							status = ((CoreException) exception).getStatus();
						}
						else {
							status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
										exception.getLocalizedMessage(), exception);
						}
						ErrorDialog.openError(parentShell, null,
								"Resource " + filename + " compilation error: " + exception.getLocalizedMessage(),
								status);
					}
				}
			}
		}
		return null;
	}

}
