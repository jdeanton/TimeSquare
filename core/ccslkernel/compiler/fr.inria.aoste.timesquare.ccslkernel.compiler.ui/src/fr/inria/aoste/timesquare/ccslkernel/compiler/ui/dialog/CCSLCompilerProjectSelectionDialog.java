/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.ui.dialog;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class CCSLCompilerProjectSelectionDialog extends ElementListSelectionDialog {

	private IWorkspaceRoot workspaceRoot;
	
	public CCSLCompilerProjectSelectionDialog(Shell parent) {
		super(parent, WorkbenchLabelProvider.getDecoratingWorkbenchLabelProvider());
		init();
	}
	
	public CCSLCompilerProjectSelectionDialog(Shell parent, ILabelProvider renderer) {
		super(parent, renderer);
		init();
	}
	
	private void init() {
		setMultipleSelection(false);
		workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		setElements(workspaceRoot.getProjects());
		setMessage("Select an existing project or enter the name of a new project");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		createNewProjectNameText(composite);
		
		createCompilationOptionsCheckboxes(parent);
		
		return composite;
	}
	
	private String newProjectName = null;
	
    protected Text createNewProjectNameText(Composite parent) {
    	
		Label label = new Label(parent, SWT.NONE);
		label.setText("Enter the name of a new project:");
		label.setFont(parent.getFont());
		
        GridData data1 = new GridData();
        data1.grabExcessVerticalSpace = false;
        data1.grabExcessHorizontalSpace = true;
        data1.horizontalAlignment = GridData.FILL;
        data1.verticalAlignment = GridData.BEGINNING;
        label.setLayoutData(data1);

        final Text text = new Text(parent, SWT.BORDER);

        GridData data2 = new GridData();
        data2.grabExcessVerticalSpace = false;
        data2.grabExcessHorizontalSpace = true;
        data2.horizontalAlignment = GridData.FILL;
        data2.verticalAlignment = GridData.BEGINNING;
        text.setLayoutData(data2);
        text.setFont(parent.getFont());

        Listener listener = new Listener() {
            public void handleEvent(Event e) {
            	setNewProjectName(text.getText());
            }
        };
        text.addListener(SWT.Modify, listener);
        
        return text;
    }

	public String getNewProjectName() {
		return newProjectName;
	}

	private void setNewProjectName(String newProjectName) {
		this.newProjectName = newProjectName;
	}
	
	private void createCompilationOptionsCheckboxes(Composite parent) {
		final Button singletonModelsButton = new Button(parent, SWT.CHECK);
		singletonModelsButton.setText("Generate model classes as Singleton");
		singletonModelsButton.setToolTipText("If the import graph of the models is not a tree it may be necessary to activate this option so that each model has a unique instance.");
		singletonModelsButton.setSelection(modelsAreSingleton);
		singletonModelsButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (singletonModelsButton.getSelection()) {
					modelsAreSingleton = true;
				}
				else {
					modelsAreSingleton = false;
				}
			}
		});
		
		final Button generateTestButton = new Button(parent, SWT.CHECK);
		generateTestButton.setText("Generate test class (junit) skeleton");
		generateTestButton.setSelection(generateTest);
		generateTestButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (generateTestButton.getSelection()) {
					generateTest = true;
				}
				else {
					generateTest = false;
				}
			}
		});
		
		final Button generateMainButton = new Button(parent, SWT.CHECK);
		generateMainButton.setText("Generate class with main() function");
		generateMainButton.setSelection(generateMain);
		generateMainButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (generateMainButton.getSelection()) {
					generateMain = true;
				}
				else {
					generateMain = false;
				}
			}
		});

		final Button useRuntimeFilterbyButton = new Button(parent, SWT.CHECK);
		useRuntimeFilterbyButton.setText("Use FilterBy from runtime");
		useRuntimeFilterbyButton.setToolTipText("If the CCSL specification model contains some instance of the FilterBy expression, select this option to have these instances use the FilterBy implementation from the CCSL runtime rather than have it generated from the CCSL expression definition");
		useRuntimeFilterbyButton.setSelection(useRuntimeFilterBy);
		useRuntimeFilterbyButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				useRuntimeFilterBy = useRuntimeFilterbyButton.getSelection();
			}
		});
	}
	
	private boolean modelsAreSingleton = false;
	
	private boolean generateTest = true;
	
	private boolean generateMain = false;

	private boolean useRuntimeFilterBy = false;
	
	public boolean getModelsAreSingleton() {
		return modelsAreSingleton;
	}

	public boolean getGenerateTest() {
		return generateTest;
	}
	
	public boolean getGenerateMain() {
		return generateMain;
	}

	public boolean getUseRuntimeFilterBy() {
		return useRuntimeFilterBy;
	}
	
}
