/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.compiler.Compiler;
import fr.inria.aoste.timesquare.ccslkernel.compiler.CompilerParameters;
import fr.inria.aoste.timesquare.ccslkernel.compiler.exceptions.CompilerException;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;

public class CompilerTest {

	private Compiler compiler;
	private CompilerParameters parameters;

	private void loadCompileAndBuild(String uri, boolean deleteAfter) {
		ResourceLoader loader = ResourceLoader.INSTANCE;
		Resource resource;
		try {
			resource = loader.loadPluginResource(uri);
		} catch (IOException e) {
			e.printStackTrace();
			throw new AssertionError("Exception caught while loading model: " + uri + " : " + e.getMessage(), e);
		}
		assertNotNull("Error with model " + uri + " resource is null", resource);
		assertNotNull(resource.getContents().get(0));
		assertTrue(resource.getContents().get(0) instanceof ClockConstraintSystem);
		IProject project = null;
		compiler = new Compiler(parameters);
		try {
			project = compiler.compile(resource);
		} catch (CompilerException e) {
			e.printStackTrace();
			throw new AssertionError("Exception caught while compiling model: " + uri + " : " + e.getMessage(), e);
		}
		assertNotNull("Error with model " + uri + " project is null", project);
		try {
			project.build(IncrementalProjectBuilder.FULL_BUILD, new NullProgressMonitor());
		} catch (CoreException e) {
			e.printStackTrace();
			throw new AssertionError("Exception caught while building project: " + uri + " : " + e.getMessage(), e);
		}
		if (deleteAfter) {
			try {
				project.delete(true, new NullProgressMonitor());
			} catch (CoreException e) {
				e.printStackTrace();
				throw new AssertionError("Exception caught while deleting project: " + uri + " : " + e.getMessage(), e);
			}
		}
	}

	@Before
	public void setUp() throws Exception {
		parameters = new CompilerParameters();
		parameters.packagePrefix = "fr.inria.aoste.timesquare.ccslkernel.compiler.tests.output";
		parameters.outputProjectNamePrefix = "fr.inria.aoste.timesquare.ccslkernel.compiler.tests.output.";
		compiler = new Compiler(parameters);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test1() {
		loadCompileAndBuild("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/PrecedesU.extendedCCSL", false);
	}

	@Test
	public void test2() {
		loadCompileAndBuild("/fr.inria.aoste.timesquare.ccslkernel.compiler.tests/data/BlockHierarchy.extendedCCSL", false);
	}

	@Test
	public void test3() {
		loadCompileAndBuild("/fr.inria.aoste.timesquare.ccslkernel.compiler.tests/data/Alternates.extendedCCSL", false);
	}

	@Test
	public void test4() {
		loadCompileAndBuild("/fr.inria.aoste.timesquare.ccslkernel.compiler.tests/data/Minus.extendedCCSL", false);
	}

	@Test
	public void test5() {
		loadCompileAndBuild("/fr.inria.aoste.timesquare.ccslkernel.compiler.tests/data/Defer.extendedCCSL", false);
	}

	@Test
	public void test6() {
		loadCompileAndBuild("/fr.inria.aoste.timesquare.ccslkernel.compiler.tests/data/FilteredBy.extendedCCSL", false);
	}

	@Test
	public void test7() {
		loadCompileAndBuild("/fr.inria.aoste.timesquare.ccslkernel.compiler.tests/data/Periodic.extendedCCSL", false);
	}

	private String solverTestModelsUris[] = {
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates2.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates3.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion2.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion3.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion4.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion5.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12655.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12744.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12982-1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12982-2.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12982-3.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug13110.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat2.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat3.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat3StartSemantics.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat4StartSemantics.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/DeferByOne.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Defer.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/FilterBy1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/FilterBy2.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Inf.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Periodic1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/PrecedesU3.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/PrecedesU.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Sampling1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Sampling2.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/SimplePrecedes.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/StrictSampling1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/StrictSampling2.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Sup.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Union.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/UpTo1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/UpTo2.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/UpTo3.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Wait1.extendedCCSL",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Watchdog.extendedCCSL", };

	@Test
	public void testSolverExamples() {
		for (String uri : solverTestModelsUris) {
			System.out.println("Compiling and building model " + uri);
			loadCompileAndBuild(uri, true);
		}
	}

}
