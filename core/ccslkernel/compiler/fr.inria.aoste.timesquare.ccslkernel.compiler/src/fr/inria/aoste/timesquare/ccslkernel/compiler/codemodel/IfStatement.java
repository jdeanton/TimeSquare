/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.compiler.CompilerParameters;

public class IfStatement extends AbstractStatement {

	public IfStatement() {
	}

	public IfStatement(String testCode) {
		this.testCode = testCode;
	}

	private String testCode;
	private List<AbstractStatement> thenStatements;
	private List<AbstractStatement> elseStatements;

	public void setTestCode(String code) {
		testCode = code;
	}

	public IfStatement addThenStatement(AbstractStatement statement) {
		if (thenStatements == null) {
			thenStatements = new ArrayList<AbstractStatement>();
		}
		thenStatements.add(statement);
		return this;
	}

	public IfStatement addThenStatement(String code) {
		return addThenStatement(new SimpleStatement(code));
	}

	public IfStatement addElseStatement(AbstractStatement statement) {
		if (elseStatements == null) {
			elseStatements = new ArrayList<AbstractStatement>();
		}
		elseStatements.add(statement);
		return this;
	}

	public IfStatement addElseStatement(String code) {
		return addElseStatement(new SimpleStatement(code));
	}

	@Override
	public void generate(StringBuilder sb, int indent) {
		indent(sb, indent);
		sb.append("if ( " + testCode + " ) {\n");
		if (thenStatements != null) {
			for (AbstractStatement st : thenStatements) {
				st.generate(sb, indent + CompilerParameters.indentStep);
			}
		}
		indent(sb, indent);
		sb.append("}\n");
		if (elseStatements != null && !elseStatements.isEmpty()) {
			indent(sb, indent);
			if (elseStatements.size() == 1 && elseStatements.get(0) instanceof IfStatement) {
				sb.append("else ");
				elseStatements.get(0).generate(sb, indent);
			} else {
				sb.append("else {\n");
				for (AbstractStatement st : elseStatements) {
					st.generate(sb, indent + CompilerParameters.indentStep);
				}
				indent(sb, indent);
				sb.append("}\n");
			}
		}
	}

}
