/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import fr.inria.aoste.timesquare.ccslkernel.compiler.CompilerParameters;
import fr.inria.aoste.timesquare.ccslkernel.compiler.helpers.PluginProjectHelper;

public class ClassDefinition {

	private String className;
	private String packageName;
	private String fileName;
	private IProject project;
	private String parentClassName;
	private List<String> interfaces = new ArrayList<String>();
	private Set<String> importedPackages = new HashSet<String>();

	private List<String> modifiers = new ArrayList<String>();
	private List<ClassDefinition> innerClasses = new ArrayList<ClassDefinition>();
	private List<MemberVariable> memberVariables = new ArrayList<MemberVariable>();
	private List<MemberFunction> memberFunctions = new ArrayList<MemberFunction>();
	
	
	public ClassDefinition(String className) {
		this.className = className;
		this.fileName = className + ".java";
	}
	
	public ClassDefinition(String className, String packageName) {
		this.className = className;
		this.packageName = packageName;
		this.fileName = className + ".java";
	}
	
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	public void setParentClassName(String name) {
		parentClassName = name;
	}
	
	public void addInterface(String name) {
		interfaces.add(name);
	}
	
	public void addImportedPackage(String packageName) {
		if ( packageName.compareTo("java.lang") != 0) {
			importedPackages.add(packageName);
		}
	}
	
	public void addModifier(String modifier) {
		modifiers.add(modifier);
	}
	
	public void setProject(IProject project) {
		this.project = project;
	}
	
	public void addInnerClass(ClassDefinition classDef) {
		innerClasses.add(classDef);
	}
	
	public void addMemberFunction(MemberFunction function) {
		memberFunctions.add(function);
	}
	
	public void addMemberVariable(MemberVariable variable) {
		memberVariables.add(variable);
	}
	
	public MemberFunction getMemberFunction(String functionName) {
		for (MemberFunction function : memberFunctions) {
			if (function.getName().compareTo(functionName) == 0)
				return function;
		}
		return null;
	}
	
	public MemberVariable getMemberVariable(String variableName) {
		for (MemberVariable var : memberVariables) {
			if (var.getName().compareTo(variableName) == 0) 
				return var;
		}
		return null;
	}
	
	private void generateFileHeader(StringBuilder fileContent) {
		fileContent.append("package " + packageName + ";\n\n");
		for (String name : importedPackages) {
			if ( ! name.startsWith("java.lang.") ) {
				fileContent.append("import " + name + ";\n");
			}
		}
		fileContent.append("\n");
	}
	
	private void generateClassDefinition(StringBuilder fileContent, int indentation) {
		addIndentation(indentation, fileContent);
		if (modifiers.isEmpty()) {
			fileContent.append("public class " + className);
		}
		else {
			for (String modifier : modifiers) {
				fileContent.append(modifier + " ");
			}
			fileContent.append("class " + className);
		}
		if (parentClassName != null &&  ! parentClassName.isEmpty()) {
			fileContent.append(" extends " + parentClassName);
		}
		if ( ! interfaces.isEmpty() ) {
			fileContent.append(" implements ");
			for (Iterator<String> iter = interfaces.iterator(); iter.hasNext(); ) {
				fileContent.append(iter.next());
				if (iter.hasNext())
					fileContent.append(", ");
			}
		}
		fileContent.append(" {\n\n");

		for (ClassDefinition innerClass : innerClasses) {
			innerClass.generateClassDefinition(fileContent, indentation + CompilerParameters.indentStep);
			fileContent.append("\n");
		}
		for (MemberVariable variable : memberVariables) {
			variable.generate(fileContent, indentation + CompilerParameters.indentStep);
			fileContent.append("\n");
		}
		if ( ! memberFunctions.isEmpty()) {
			fileContent.append("\n");
		}
		for (MemberFunction function : memberFunctions) {
			function.generate(fileContent, indentation + CompilerParameters.indentStep);
			fileContent.append("\n");
		}
		// End the class definition
		addIndentation(indentation, fileContent);
		fileContent.append("}\n");
	}
	
	public void generate(PluginProjectHelper pluginHelper) throws CoreException {
		String folderName = packageName.replace(".", File.separator);
		String filePath = CompilerParameters.genSourceFolder + File.separator + folderName + File.separator + fileName;
		pluginHelper.createPackage(project, packageName);
		StringBuilder fileContent = new StringBuilder();
		generateFileHeader(fileContent);
		generateClassDefinition(fileContent, 0);
		pluginHelper.createFile(project, filePath, fileContent.toString());
	}
	
	private void addIndentation(int indent, StringBuilder content) {
		for (int i = 0; i < indent; i++) {
			content.append(' ');
		}
	}

}
