/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler;

import fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel.ClassDefinition;
import fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel.MemberFunction;
import fr.inria.aoste.timesquare.ccslkernel.compiler.helpers.PluginProjectHelper;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntDivide;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util.ClassicalExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;

public class ClassicalExpressionCompiler {

	@SuppressWarnings("unused")
	private CompilerParameters parameters;
	@SuppressWarnings("unused")
	private PluginProjectHelper pluginHelper;
	private NameProvider nameProvider;
	
	private NamedElement context;
	private String functionName;
	private Class<?> returnType;
	private String returnTypeName;

	public ClassicalExpressionCompiler(CompilerParameters params, PluginProjectHelper pluginHelper, NameProvider nameProvider) {
		this.parameters = params;
		this.pluginHelper = pluginHelper;
		this.nameProvider = nameProvider;
	}
		
	public String generateClassicalExpression(ClassicalExpression expression, NamedElement context,
			ClassDefinition enclosingClass) {
		this.context = context;
		functionName = nameProvider.getFunctionName(expression);
		MemberFunction function = new MemberFunction(functionName);
		function.addModifier("private");
		returnType = nameProvider.getJavaClass(expression);
		returnTypeName = nameProvider.getClassName(expression);
		function.setReturnType(returnTypeName);
		enclosingClass.addImportedPackage(returnType.getName());
		String varName = generateCode(expression, function, enclosingClass);
		function.addStatement("return " + varName);
		enclosingClass.addMemberFunction(function);
		return functionName;
	}
	
	public String getFunctionName() {
		return functionName;
	}
	
	public Class<?> getReturnType() {
		return returnType;
	}
	public String getReturnTypeName() {
		return returnTypeName;
	}
	
	@SuppressWarnings("rawtypes")
	private class codeGenerator extends ClassicalExpressionSwitch<String> {
		private MemberFunction function;
		private ClassDefinition enclosingClass;
		public codeGenerator(MemberFunction function, ClassDefinition enclosingClass) {
			this.function = function;
			this.enclosingClass = enclosingClass;
		}
		private void addPackage(String packageName) {
			if ( ! packageName.startsWith("java.lang."))
				enclosingClass.addImportedPackage(packageName);
		}
		@Override
		public String caseSeqGetHead(SeqGetHead object) {
			String operandVarname = doSwitch(object.getOperand());
			String varName = nameProvider.genSym();
			Class type = nameProvider.getJavaClass(object);
			String typeName = nameProvider.getClassName(object);
			addPackage(type.getName());
			function.addStatement(typeName + " " + varName + " = "
						+ "(" + typeName + ")" + operandVarname + ".getHead()");
			return varName;
		}
		@Override
		public String caseSeqGetTail(SeqGetTail object) {
			String operandVarname = doSwitch(object.getOperand());
			String varName = nameProvider.genSym();
			Class type = nameProvider.getJavaClass(object);
			String typeName = nameProvider.getClassName(object);
			addPackage(type.getName());
			function.addStatement(typeName + " " + varName + " = " + operandVarname + ".getTail()");
			return varName;
		}
		@Override
		public String caseSeqIsEmpty(SeqIsEmpty object) {
			String operandVarname = doSwitch(object.getOperand());
			String varName = nameProvider.genSym();
			Class type = nameProvider.getJavaClass(object);
			String typeName = nameProvider.getClassName(object);
			addPackage(type.getName());
			function.addStatement(typeName + " " + varName + " = " + operandVarname + ".isEmpty()");
			return varName;			
		}
		@Override
		public String caseNumberSeqVariableRef(NumberSeqVariableRef object) {
			AbstractEntity refVar = object.getReferencedVar();
			String varName = nameProvider.getVariableName(refVar);
			return varName;
		}
		@Override
		public String caseAnd(And object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Boolean " + varName + " = (" + leftVarname + " && " + rightVarname + ")");
			return varName;
		}
		@Override
		public String caseOr(Or object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Boolean " + varName + " = (" + leftVarname + " || " + rightVarname + ")");
			return varName;
		}
		@Override
		public String caseNot(Not object) {
			String operandVarname = doSwitch(object.getOperand());
			String varName = nameProvider.genSym();
			function.addStatement("Boolean " + varName + " = ( ! " + operandVarname + ")");
			return varName;
		}
		@Override
		public String caseBooleanVariableRef(BooleanVariableRef object) {
			return nameProvider.getVariableName( object.getReferencedVar() );
		}
		@Override
		public String caseBooleanRef(BooleanRef object) {
			return (object.getReferencedBool().getValue() ? "true" : "false");
		}
		@Override
		public String caseIntInf(IntInf object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Boolean " + varName + " = " + leftVarname + " < " + rightVarname);
			return varName;
		}
		@Override
		public String caseIntSup(IntSup object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Boolean " + varName + " = " + leftVarname + " > " + rightVarname);
			return varName;
		}
		@Override
		public String caseUnaryIntMinus(UnaryIntMinus object) {
			String operandVarname = doSwitch(object.getOperand());
			String varName = nameProvider.genSym();
			function.addStatement("Integer " + varName + " = - (" + operandVarname + ")"); 
			return varName;
		}
		@Override
		public String caseUnaryIntPlus(UnaryIntPlus object) {
			String operandVarname = doSwitch(object.getOperand());
			String varName = nameProvider.genSym();
			function.addStatement("Integer " + varName + " = + (" + operandVarname + ")"); 
			return varName;
		}
		@Override
		public String caseIntPlus(IntPlus object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Integer " + varName + " = " + leftVarname + " + " + rightVarname);
			return varName;
		}
		@Override
		public String caseIntMinus(IntMinus object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Integer " + varName + " = " + leftVarname + " - " + rightVarname);
			return varName;
		}
		@Override
		public String caseIntMultiply(IntMultiply object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Integer " + varName + " = " + leftVarname + " * " + rightVarname);
			return varName;
		}
		@Override
		public String caseIntDivide(IntDivide object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Integer " + varName + " = " + leftVarname + " / " + rightVarname);
			return varName;
		}
		@Override
		public String caseIntEqual(IntEqual object) {
			String leftVarname = doSwitch(object.getLeftValue());
			String rightVarname = doSwitch(object.getRightValue());
			String varName = nameProvider.genSym();
			function.addStatement("Boolean " + varName + " = (" + leftVarname + " == " + rightVarname + ")");
			return varName;
		}
		@Override
		public String caseIntegerVariableRef(IntegerVariableRef object) {
			return nameProvider.getVariableName( object.getReferencedVar() );
		}
		@Override
		public String caseIntegerRef(IntegerRef object) {
			IntegerElement referencedInt = object.getIntegerElem();
			if (referencedInt.eContainer() == context) {
				return nameProvider.getVariableName( referencedInt );
			}
			else {
				return object.getIntegerElem().getValue().toString();
			}
		}
	}
	
	private String generateCode(ClassicalExpression expression, MemberFunction function, ClassDefinition enclosingClass) {
		return new codeGenerator(function, enclosingClass).doSwitch(expression);
	}
}
