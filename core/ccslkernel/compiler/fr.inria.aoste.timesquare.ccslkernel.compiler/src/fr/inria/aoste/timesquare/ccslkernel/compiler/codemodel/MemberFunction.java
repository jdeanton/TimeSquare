/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.compiler.CompilerParameters;

public class MemberFunction {

	public MemberFunction(String name) {
		this.name = name;
	}

	public MemberFunction(String name, String returnType) {
		this.name = name;
		this.returnType = returnType;
	}

	private List<String> annotations = new ArrayList<String>();
	private boolean overrides = false;
	private boolean constructor = false;
	private List<String> modifiers = new ArrayList<String>();
	private String name;
	private String returnType;
	private List<String> exceptions = new ArrayList<String>();

	private class Parameter {
		public final String type;
		public final String name;

		public Parameter(String type, String name) {
			this.type = type;
			this.name = name;
		}
	};

	private List<Parameter> parameters = new ArrayList<Parameter>();

//	private StringBuilder body = null;

	private List<AbstractStatement> statements = null;
	
	public String getName() {
		return name;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public void addAnnotation(String annotation) {
		annotations.add(annotation);
	}

	public void setOverrides(boolean ov) {
		overrides = ov;
	}

	public void setConstructor(boolean v) {
		constructor = v;
	}

	public void addModifier(String modifier) {
		modifiers.add(modifier);
	}

	public void addParameter(String type, String name) {
		Parameter parameter = new Parameter(type, name);
		parameters.add(parameter);
	}

	public void addException(String type) {
		exceptions.add(type);
	}
	
	public void addStatement(AbstractStatement statement) {
		if (statements == null) {
			statements = new ArrayList<AbstractStatement>();
		}
		statements.add(statement);
	}
	
	public void addStatement(String code) {
		addStatement(new SimpleStatement(code));
	}
	
	public AbstractStatement getLastStatement() {
		if (statements == null)
			return null;
		return statements.get(statements.size() - 1);
	}
	
	private void addIndentation(int indent, StringBuilder content) {
		for (int i = 0; i < indent; i++) {
			content.append(' ');
		}
	}

	public void generate(StringBuilder stringBuilder, int indentation) {
		for (String annotation : annotations) {
			addIndentation(indentation, stringBuilder);
			stringBuilder.append("@" + annotation + "\n");
		}
		if (overrides) {
			addIndentation(indentation, stringBuilder);
			stringBuilder.append("@" + Override.class.getSimpleName() + "\n");
		}
		addIndentation(indentation, stringBuilder);
		for (String modifier : modifiers) {
			stringBuilder.append(modifier + " ");
		}
		if (constructor) {
			stringBuilder.append(name + "(");
		} else {
			if (returnType == null) {
				stringBuilder.append("void " + name + "(");
			} else {
				stringBuilder.append(returnType + " " + name + "(");
			}
		}
		for (Iterator<Parameter> iter = parameters.iterator(); iter.hasNext();) {
			Parameter parameter = iter.next();
			stringBuilder.append(parameter.type + " " + parameter.name);
			if (iter.hasNext()) {
				stringBuilder.append(", ");
			}
		}
		stringBuilder.append(")");
		if ( ! exceptions.isEmpty() ) {
			stringBuilder.append("\n");
			addIndentation(indentation * 4, stringBuilder);
			stringBuilder.append("throws ");
			for (Iterator<String> iter = exceptions.iterator(); iter.hasNext(); ) {
				stringBuilder.append(iter.next());
				if (iter.hasNext())
					stringBuilder.append(", ");
			}
		}
		stringBuilder.append(" {\n");
		if (statements != null && ! statements.isEmpty()) {
			for (AbstractStatement statement : statements) {
				statement.generate(stringBuilder, indentation + CompilerParameters.indentStep);
			}
		}
		addIndentation(indentation, stringBuilder);
		stringBuilder.append("}\n");
	}

}
