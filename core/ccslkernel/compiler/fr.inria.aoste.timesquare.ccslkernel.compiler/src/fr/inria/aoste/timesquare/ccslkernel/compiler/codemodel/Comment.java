/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel;

public class Comment extends AbstractStatement {

	String commentText;
	
	public Comment(String text) {
		this.commentText = text;
	}

	@Override
	public void generate(StringBuilder sb, int indent) {
		indent(sb, indent);
		sb.append("// " + commentText + "\n");
	}

}
