/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel.ClassDefinition;
import fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel.Comment;
import fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel.IfStatement;
import fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel.MemberFunction;
import fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel.MemberVariable;
import fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel.SimpleStatement;
import fr.inria.aoste.timesquare.ccslkernel.compiler.exceptions.CompilerException;
import fr.inria.aoste.timesquare.ccslkernel.compiler.helpers.PluginProjectHelper;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.util.BasicTypeSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Death;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.EqualitySolver;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.RecursiveDefinitionChecker;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.AbstractRuntimeBlock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.AbstractRuntimeModel;
import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.ValueProvider;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.AbstractRuntimeExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeConcatenation;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeConcatenation.ConcatState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.BDDHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.AbstractCCSLSimulationEngine;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.AbstractStepExecutionEngine;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.CCSLSimulationEngine;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.CCSLStepExecutionEngine;
import fr.inria.aoste.timesquare.trace.util.QualifiedNameBuilder;

/**
 * Main class for the CCSL compiler. Usage is to instantiate this class together with an instance 
 * of {@link CompilerParameters} and call the {@link #compile(Resource)} with an EMF resource
 * containing a {@link ClockConstraintSystem} object.
 * 
 * <p>The resource to compile needs to be in a resource set together with the imported models and
 * libraries. The resource is not checked for errors, it is the responsibility of the caller to
 * perform these tests.
 * 
 * <p>The compilation scheme is to generate for each object such as ClockConstraintSystem, Block,
 * user-defined relation and expression, a dedicated class to instantiate in order to obtain a
 * representation of the original CCSL model. Objects that correspond to ClockConstraintSystem
 * can be used with the simulation API implemented by the classes {@link AbstractCCSLSimulationEngine}
 * and {@link AbstractStepExecutionEngine}.
 * 
 * <p>ClockConstraintSystem are translated to a class that extends the {@link AbstractRuntimeModel}
 * from the runtime plugin fr.inria.aoste.timesquare.ccslkernel.runtime. Instantiation of these
 * class entails the instantiation of all the internal objects corresponding to the CCSL model.
 * 
 * <p>Each Block object is translated to a class that extends {@link AbstractRuntimeBlock}.
 * 
 * @author nchleq
 * @see CompilerParameters
 * @see AbstractCCSLSimulationEngine#AbstractCCSLSimulationEngine(AbstractRuntimeModel)
 * @see AbstractCCSLSimulationEngine#initSimulation()
 * @see AbstractCCSLSimulationEngine#endSimulation()
 * @see AbstractStepExecutionEngine#executeStep()
 */
public class Compiler {

	private CompilerParameters parameters;
	private PluginProjectHelper pluginHelper;
	private NameProvider nameProvider;
	private UnfoldModel unfoldModel;
	
	public Compiler(CompilerParameters params) {
		this.parameters = params;
		this.pluginHelper = new PluginProjectHelper();
	}

	public IProject compile(Resource model) throws CompilerException {
		try {
			unfoldModel = UnfoldModel.unfoldModel(model);
		} catch (IOException | UnfoldingException e) {
			e.printStackTrace();
		}
		if (unfoldModel != null) {
			IProject project = createDestinationProject();
			compile(unfoldModel, project);
			if (parameters.generateTests) {
				generateTestClass(unfoldModel, project);
			}
			if (parameters.generateMain) {
				generateMainClass(unfoldModel, project);
			}
			return project;
		}
		return null;
	}

	private IProject createDestinationProject() throws CompilerException {
		String projectName;
		if (parameters.outputProjectName != null) {
			projectName = parameters.outputProjectName;
		}
		else {
			projectName = parameters.outputProjectNamePrefix + unfoldModel.getModel().getName().toLowerCase();

		}
		IProject project = null;
		try {
			project = pluginHelper.createPluginProject(projectName);
		} catch (CoreException e) {
			throw new CompilerException("Caught CoreException while creating output project: ", e);
		}
		for (String bundle : parameters.requiredDependencies) {
			try {
				pluginHelper.addRequiredBundle(project, bundle);
			} catch (CoreException e) {
				throw new CompilerException("Caught CoreException while adding dependency: ", e);
			}
		}
		if (parameters.generateTests) {
			try {
				pluginHelper.addRequiredBundle(project, "org.junit");
			} catch (CoreException e) {
				throw new CompilerException("Caught CoreException while adding dependency: ", e);
			}
		}
		return project;
	}
	
	Set<RelationDefinition> usedRelationDefinitions = new HashSet<RelationDefinition>();
	Set<ExpressionDefinition> usedExpressionDefinitions = new HashSet<ExpressionDefinition>();
	
	private void compile(UnfoldModel unfoldModel, IProject project) throws CompilerException {
		try {
			// Generate code for the model itself.
			generateModelCode(unfoldModel, project);
			while ( ! usedExpressionDefinitions.isEmpty() || ! usedRelationDefinitions.isEmpty()) {
				// Copy the list and clear them, the compilation of definitions may discover other definitions
				// that need to be compiled.
				List<ExpressionDefinition> edefs = new ArrayList<ExpressionDefinition>(usedExpressionDefinitions);
				List<RelationDefinition> rdefs = new ArrayList<RelationDefinition>(usedRelationDefinitions);
				usedExpressionDefinitions.clear();
				usedRelationDefinitions.clear();
				for (ExpressionDefinition definition : edefs) {
					if (definition instanceof ConditionalExpressionDefinition) {
						compileConditionalExpressionDefinition((ConditionalExpressionDefinition) definition, project);
					} else if (definition instanceof UserExpressionDefinition) {
						compileUserExpressionDefinition((UserExpressionDefinition) definition, project);
					}
				}
				for (RelationDefinition definition : rdefs) {
					if (definition instanceof ConditionalRelationDefinition) {
						compileConditionalRelationDefinition((ConditionalRelationDefinition)definition, project);
					}
					else {
						compileUserRelationDefinition((UserRelationDefinition) definition, project);
					}
				}
			}
		} catch (CoreException e) {
			throw new CompilerException("Caught CoreException when compiling model " + project.getName() + ": ", e);
		}
	}

	/**
	 * Generates a class to represent the model. This class will instantiate all the necessary blocks
	 * using for each a private member variable initialized with a new instance of the corresponding
	 * class for the block. 
	 * The class will also have a getEntryBlock() function to access the root of the block
	 * hierarchy.
	 *  
	 * @param unfoldModel
	 * @param project
	 * @throws CoreException
	 * @throws CompilerException 
	 */
	private void generateModelCode(UnfoldModel unfoldModel, IProject project) throws CoreException, CompilerException {

		this.nameProvider = new NameProvider(parameters, unfoldModel);

		ClockConstraintSystem model = unfoldModel.getModel();
		String packageName = nameProvider.getPackageName(model);
		String className = nameProvider.getClassName(model);
		
		ClassDefinition classDef = new ClassDefinition(className, packageName);
		classDef.setProject(project);
		
		classDef.addImportedPackage(AbstractRuntimeModel.class.getName());
		classDef.addImportedPackage(AbstractRuntimeBlock.class.getName());
		classDef.setParentClassName(AbstractRuntimeModel.class.getSimpleName());
		
		MemberFunction constructor1 = new MemberFunction(className);
		constructor1.setConstructor(true);
		if (parameters.modelsAreSingleton) {
			constructor1.addModifier("private");
		}
		else {
			constructor1.addModifier("public");
		}
		constructor1.addStatement("super(\"" + model.getName() + "\")");
		constructor1.addStatement("init()");
		classDef.addMemberFunction(constructor1);
		
		MemberFunction constructor2 = new MemberFunction(className);
		constructor2.setConstructor(true);
		if (parameters.modelsAreSingleton) {
			constructor2.addModifier("private");
		}
		else {
			constructor2.addModifier("public");
		}
		constructor2.addParameter(IRuntimeContainer.class.getSimpleName(), "parent");
		constructor2.addStatement("super(\"" + model.getName() + "\", parent)");
		constructor2.addStatement("init()");
		classDef.addMemberFunction(constructor2);
		
		classDef.addImportedPackage(IRuntimeContainer.class.getName());
		
		if (parameters.modelsAreSingleton) {
			// private static class SingletonHolder { ... }
			ClassDefinition holderClass = new ClassDefinition("SingletonHolder");
			holderClass.addModifier("private");
			holderClass.addModifier("static");
			// 	private final static XXXXXModel INSTANCE = new XXXXXModel();
			MemberVariable instance = new MemberVariable("INSTANCE", className);
			instance.setInitializer("new " + className + "()");
			instance.addModifier("private");
			instance.addModifier("final");
			instance.addModifier("static");
			holderClass.addMemberVariable(instance);
			classDef.addInnerClass(holderClass);
			// 	public static XXXXXModel getInstance()
			MemberFunction getInstanceFunction = new MemberFunction("getInstance", className);
			getInstanceFunction.addModifier("public");
			getInstanceFunction.addModifier("static");
			getInstanceFunction.addStatement("return SingletonHolder.INSTANCE");
			classDef.addMemberFunction(getInstanceFunction);
		}
		
		MemberFunction initFunction = new MemberFunction("init", "void");
		initFunction.addModifier("protected");
		classDef.addMemberFunction(initFunction);
		
		List<String> importedModelNames = new ArrayList<String>();
		
		/*
		 * Add a variable for each imported model. These variables are initialized by null, and their
		 * getter function initialize them if not already done. The same guarded initialization code
		 * also appear in the init() function of the model.
		 */
		for (UnfoldModel imported : unfoldModel.getImportedModels()) {
			ClockConstraintSystem importedModel = imported.getModel();
			String importedPackage = nameProvider.getPackageName(importedModel);
			String importedClassName = nameProvider.getClassName(importedModel);
			String variableName = nameProvider.getVariableName(importedModel);
			MemberVariable variable = new MemberVariable(variableName, importedClassName);
			variable.addModifier("private");
			variable.setInitializer("null");
			classDef.addMemberVariable(variable);
			classDef.addImportedPackage(importedPackage + "." + importedClassName);
			String getterName = nameProvider.getVariableGetterName(importedModel);
			MemberFunction getter = new MemberFunction(getterName, importedClassName);
			getter.addModifier("public");
			if (parameters.modelsAreSingleton) {
				initFunction.addStatement(variableName + " = " + importedClassName + ".getInstance()");
				// initFunction.addStatement(variableName + ".setParent(this)"); ???
			}
			else {
				IfStatement ifNotExists = new IfStatement(variableName + " == null");
				ifNotExists.addThenStatement(variableName + " = new " + importedClassName + "(this)");
				getter.addStatement(ifNotExists);
				initFunction.addStatement(ifNotExists);
			}
			getter.addStatement("return " + variableName);
			classDef.addMemberFunction(getter);
			importedModelNames.add(variableName);
		}
		/*
		 * Generate a function that returns an array of the imported models
		 */
		MemberFunction getImportedFunction = new MemberFunction("getImportedModels",
					AbstractRuntimeModel.class.getSimpleName() + "[]");
		getImportedFunction.setOverrides(true);
		getImportedFunction.addModifier("public");
		{
			String initializer = new String();
			for (Iterator<String> it = importedModelNames.iterator(); it.hasNext();) {
				initializer += it.next();
				if (it.hasNext()) {
					initializer += ", ";
				}
			}
			getImportedFunction.addStatement(AbstractRuntimeModel.class.getSimpleName() + "[] subs = { " + initializer + " }");
			getImportedFunction.addStatement("return subs");
		}
		classDef.addMemberFunction(getImportedFunction);
		
		/*
		 * Do the same for the blocks. Imported models are generated before the blocks because
		 * expressions and relations inside the blocks can use elements from imported models.
		 */
		List<String> names = new ArrayList<String>();
		for (Block block : model.getSubBlock()) {
			String variableName = nameProvider.getVariableName(block);
			String blockTypeName = nameProvider.getClassName(block);
			String blockPackageName = nameProvider.getPackageName(block);
			MemberVariable blockVariable = new MemberVariable(variableName, blockTypeName);
			blockVariable.addModifier("private");
			blockVariable.setInitializer("null");
			classDef.addMemberVariable(blockVariable);
			classDef.addImportedPackage(blockPackageName + "." + blockTypeName);
			names.add(variableName);
			String getterName = nameProvider.getVariableGetterName(block);
			MemberFunction getter = new MemberFunction(getterName, blockTypeName);
			getter.addModifier("public");
			IfStatement ifNotExists = new IfStatement(variableName + " == null");
			ifNotExists.addThenStatement(variableName + " = new " + blockTypeName + "(this, \"" + block.getName() + "\")");
			getter.addStatement(ifNotExists);
			getter.addStatement("return " + variableName);
			classDef.addMemberFunction(getter);
			initFunction.addStatement(ifNotExists);
		}
		
		MemberFunction subBlocksFunction = new MemberFunction("getSubBlocks",
							AbstractRuntimeBlock.class.getSimpleName() + "[]");
		subBlocksFunction.addModifier("public");
		subBlocksFunction.setOverrides(true);
		String initializer = new String();
		for (Iterator<String> it = names.iterator(); it.hasNext();) {
			initializer += it.next();
			if (it.hasNext()) {
				initializer += ", ";
			}
		}
		subBlocksFunction.addStatement(AbstractRuntimeBlock.class.getSimpleName() + "[] subs = { " + initializer + " }");
		subBlocksFunction.addStatement("return subs");
		classDef.addMemberFunction(subBlocksFunction);
				
		String entryBlockVariable = nameProvider.getVariableName(model.getSuperBlock());

		MemberFunction entryBlockFunction = new MemberFunction("getEntryBlock",
				AbstractRuntimeBlock.class.getSimpleName());
		entryBlockFunction.addModifier("public");
		entryBlockFunction.setOverrides(true);
		entryBlockFunction.addStatement("return " + entryBlockVariable);
		classDef.addMemberFunction(entryBlockFunction);
		
		classDef.generate(pluginHelper);
		
		// Iterate over all sub-blocks and generate their code
		for (Block block : model.getSubBlock()) {
			generateBlockCode(block, project);
		}
		// Generate code for all imported models
		for (UnfoldModel imported : unfoldModel.getImportedModels()) {
			UnfoldModel saved = this.unfoldModel;
			this.unfoldModel = imported;
			generateModelCode(imported, project);
			this.unfoldModel = saved;
		}
	}
	
	private void generateMainClass(UnfoldModel unfoldModel, IProject project) throws CompilerException {
		ClockConstraintSystem model = unfoldModel.getModel();
		String packageName = nameProvider.getMainPackageName(model);
		String className = nameProvider.getMainClassName(model);
		
		String modelClassName = nameProvider.getClassName(model);
		String modelPackageName = nameProvider.getPackageName(model);

		ClassDefinition classDef = new ClassDefinition(className, packageName);
		classDef.setProject(project);

		classDef.addImportedPackage("java.util.Set");
		classDef.addImportedPackage(modelPackageName + "." + modelClassName);
		classDef.addImportedPackage(RuntimeClock.class.getName());
		classDef.addImportedPackage(CCSLSimulationEngine.class.getName());
		classDef.addImportedPackage(CCSLStepExecutionEngine.class.getName());
		classDef.addImportedPackage(SimulationException.class.getName());

		MemberFunction mainFunction = new MemberFunction("main", "void");
		mainFunction.addModifier("public");
		mainFunction.addModifier("static");
		mainFunction.addParameter("String[]", "args");
		mainFunction.addException(SimulationException.class.getSimpleName());
		
		if (parameters.modelsAreSingleton) {
			mainFunction.addStatement(modelClassName + " model = " + modelClassName + ".getInstance()");
		}
		else {
			mainFunction.addStatement(modelClassName + " model = new " + modelClassName + "()");
		}
		mainFunction.addStatement("CCSLSimulationEngine engine = new CCSLSimulationEngine(model)");
		mainFunction.addStatement("engine.initSimulation()");
		mainFunction.addStatement("CCSLStepExecutionEngine stepEngine = new CCSLStepExecutionEngine(engine)");
		mainFunction.addStatement(new Comment("First simulation step."));
		mainFunction.addStatement("stepEngine.executeStep()");
		mainFunction.addStatement("Set<RuntimeClock> firedClocks = stepEngine.getFiredClocks()");
		mainFunction.addStatement("System.out.println(firedClocks)");
		mainFunction.addStatement(new Comment("Second simulation step. The CCSLStepExecutionEngine object can be reused."));
		mainFunction.addStatement("stepEngine.executeStep()");
		mainFunction.addStatement(new Comment("Third simulation step."));
		mainFunction.addStatement("stepEngine.executeStep()");
		mainFunction.addStatement("engine.endSimulation()");

		classDef.addMemberFunction(mainFunction);

		try {
			classDef.generate(pluginHelper);
		} catch (CoreException e) {
			throw new CompilerException("Caught CoreException when generating tests for model " + project.getName() + ": ", e);
		}
	}
	
	private void generateTestClass(UnfoldModel unfoldModel, IProject project) throws CompilerException {
		ClockConstraintSystem model = unfoldModel.getModel();
		String packageName = nameProvider.getTestPackageName(model);
		String className = nameProvider.getTestClassName(model);
		
		String modelClassName = nameProvider.getClassName(model);
		String modelPackageName = nameProvider.getPackageName(model);

		ClassDefinition classDef = new ClassDefinition(className, packageName);
		classDef.setProject(project);
		
		classDef.addImportedPackage("java.util.Set");
		classDef.addImportedPackage("org.junit.Test");
		classDef.addImportedPackage("static org.junit.Assert.*");
		classDef.addImportedPackage(modelPackageName + "." + modelClassName);
		classDef.addImportedPackage(RuntimeClock.class.getName());
		classDef.addImportedPackage(CCSLSimulationEngine.class.getName());
		classDef.addImportedPackage(CCSLStepExecutionEngine.class.getName());
		classDef.addImportedPackage(SimulationException.class.getName());
		
		MemberFunction testFunction = new MemberFunction("test", "void");
		testFunction.addModifier("public");
		testFunction.addAnnotation(org.junit.Test.class.getSimpleName());
		testFunction.addException(SimulationException.class.getSimpleName());
		if (parameters.modelsAreSingleton) {
			testFunction.addStatement(modelClassName + " model = " + modelClassName + ".getInstance()");
		}
		else {
			testFunction.addStatement(modelClassName + " model = new " + modelClassName + "()");
		}
		testFunction.addStatement("assertNotNull(model)");
		testFunction.addStatement("assertNotNull(model.getEntryBlock())");
		testFunction.addStatement("CCSLSimulationEngine engine = new CCSLSimulationEngine(model)");
		testFunction.addStatement("engine.initSimulation()");
		testFunction.addStatement("CCSLStepExecutionEngine stepEngine = new CCSLStepExecutionEngine(engine)");
		testFunction.addStatement(new Comment("First simulation step."));
		testFunction.addStatement("stepEngine.executeStep()");
		testFunction.addStatement("Set<RuntimeClock> firedClocks = stepEngine.getFiredClocks()");
		testFunction.addStatement(new Comment("Second simulation step. The CCSLStepExecutionEngine object can be reused."));
		testFunction.addStatement("stepEngine.executeStep()");
		testFunction.addStatement(new Comment("Third simulation step."));
		testFunction.addStatement("stepEngine.executeStep()");
		testFunction.addStatement("engine.endSimulation()");
		
		classDef.addMemberFunction(testFunction);
		
		try {
			classDef.generate(pluginHelper);
		} catch (CoreException e) {
			throw new CompilerException("Caught CoreException when generating tests for model " + project.getName() + ": ", e);
		}
	}

	/**
	 * Performs an analysis of the Coincidence relations present in the block to identify clocks
	 * that are statically "equal". The goal of this analysis is that such clocks are further given
	 * the same BuDDyBDD variable.
	 * 
	 * @param block the block the content of which is to be analyzed.
	 * @return an instance of {@link EqualitySolver} or null if there are no Coincidence relation in
	 * the block.
	 */
	private EqualitySolver<String> blockClockEqualityAnalysis(Block block) {
		boolean foundCoincidence = false;
		EqualitySolver<String> equalityRegister = new EqualitySolver<String>();
		for (Relation relation : block.getRelations()) {
			if ( ( ! relation.getIsAnAssertion()) && relation.getType() instanceof Coincidence) {
				foundCoincidence = true;
				AbstractEntity left = ((Coincidence) relation.getType()).getLeftEntity();
				AbstractEntity right = ((Coincidence) relation.getType()).getRightEntity();
				BindableEntity leftValue = resolveAbstract(left, relation.getBindings());
				BindableEntity rightValue = resolveAbstract(right, relation.getBindings());
				String leftName = null;
				if (leftValue.eContainer() != block)
					leftName = generateAccessCode(block, relation, leftValue, null);
				else if (leftValue instanceof Clock)
					leftName = nameProvider.getVariableName(leftValue);
				else if (leftValue instanceof Expression)
					leftName = nameProvider.getClockName((Expression) leftValue);
				String rightName = null;
				if (rightValue instanceof Clock)
					rightName = nameProvider.getVariableName(rightValue);
				else if (rightValue instanceof Expression)
					rightName = nameProvider.getClockName((Expression) rightValue);
				if (leftName != null && rightName != null) {
					equalityRegister.registerEquality(leftName, rightName);
				}
			}
		}
		if ( ! foundCoincidence) {
			return null;
		}
		else {
			return equalityRegister;
		}
	}
	
	/**
	 * Generates a class to represent the given block.
	 * 
	 * Each element, expression and relation defined in the block will be represented by some
	 * member variables. Clocks, elements and relations are represented by one variable, and
	 * expressions are represented by two variables: one for the expression itself, and one for the
	 * clock associated to the expression.
	 * 
	 * All these variables are initialized in an init() function called from the constructor
	 * of the generated class. The code of this function is generated.
	 * 
	 * 
	 * @param block
	 * @param project
	 * @throws CoreException
	 * @throws CompilerException 
	 */
	private void generateBlockCode(Block block, IProject project)
			throws CoreException, CompilerException {

		String packageName = nameProvider.getPackageName(block);
		String className = nameProvider.getClassName(block);
		
		ClassDefinition classDef = new ClassDefinition(className, packageName);
		classDef.setParentClassName(AbstractRuntimeBlock.class.getSimpleName());
		classDef.addImportedPackage(AbstractRuntimeBlock.class.getName());
		classDef.addImportedPackage(IRuntimeContainer.class.getName());
		classDef.setProject(project);

		MemberFunction constructor = new MemberFunction(className);
		constructor.setConstructor(true);
		constructor.addModifier("public");
		constructor.addParameter(IRuntimeContainer.class.getSimpleName(), "parent");
		constructor.addParameter("String", "name");
		constructor.addStatement("super(parent, name)");
		constructor.addStatement("init()");
		classDef.addMemberFunction(constructor);

		MemberFunction initFunction = new MemberFunction("init", "void");
		initFunction.addModifier("protected");
		classDef.addMemberFunction(initFunction);
		
		List<String> elementNames = new ArrayList<String>();
		
		EqualitySolver<String> equalityRegister = blockClockEqualityAnalysis(block);
		
		for (Element element : block.getElements()) {
			if (element instanceof Clock) {
				String name = generateClockCode((Clock) element, "private", classDef, initFunction,
						elementNames, equalityRegister);
				if (elementNames.contains(name)) {
					throw new CompilerException("Duplicate clock name \"" + element.getName()
							+ "\" in block " + block.getName());
				}
				elementNames.add(name);
			}
			else {
				String name = generateElementCode(element, "public", classDef, true);
				if (elementNames.contains(name)) {
					throw new CompilerException("Duplicate element name \"" + element.getName()
							+ "\" in block " + block.getName());
				}
				elementNames.add(name);
			}
		}

		List<String> constraintNames = new ArrayList<String>();
		List<String> relationNames = new ArrayList<String>();
		List<String> expressionNames = new ArrayList<String>();

		ArrayList<ConcreteEntity> expressionsAndRelations = new ArrayList<ConcreteEntity>(block.getExpressions());
		expressionsAndRelations.addAll(block.getRelations());
		List<ConcreteEntity> buildOrder = getBuildOrder(expressionsAndRelations);
		for (ConcreteEntity entity : buildOrder) {
			if (entity instanceof Expression) {
				String clockVariableName = nameProvider.getClockName((Expression) entity);
				String clockName = entity.getName();				
				generateDiscreteClockCode(clockVariableName, clockName,
						nameProvider.implicitClockClassName, nameProvider.implicitClockPackage,
						"public", classDef, initFunction, elementNames, equalityRegister, null);
				elementNames.add(clockVariableName);

				String varName = generateExpressionCode(block, (Expression) entity, false,
						project, classDef);
				if (constraintNames.contains(varName) || expressionNames.contains(varName)) {
					throw new CompilerException("Duplicate expression name \"" + entity.getName()
							+ "\" in block " + block.getName());
				}
				constraintNames.add(varName);
				expressionNames.add(varName);
			}
			else if (entity instanceof Relation) {
				String varName = generateRelationCode(block, (Relation) entity, classDef, project);
				if (constraintNames.contains(varName) || expressionNames.contains(varName)) {
					throw new CompilerException("Duplicate relation name \"" + entity.getName()
							+ "\" in block " + block.getName());
				}
				constraintNames.add(varName);
				relationNames.add(varName);
			}
		}
		
		List<String> subBlockNames = new ArrayList<String>();
		for (Block subBlock : block.getSubBlock()) {
			// Generate code for sub blocks in their own class and add here some
			// code to create instances of these classes.
			generateBlockCode(subBlock, project);
			String subVariableName = nameProvider.getVariableName(subBlock);
			String subclassName = nameProvider.getClassName(subBlock);
			MemberVariable variable = new MemberVariable(subVariableName, subclassName);
			variable.addModifier("public");
			classDef.addMemberVariable(variable);
			initFunction.addStatement(subVariableName + " = new " + subclassName + "(this, \"" + subBlock.getName() + "\")");
			subBlockNames.add(subVariableName);
			classDef.addImportedPackage(nameProvider.getPackageName(subBlock) + "." + subclassName);
		}

		String tempSymbol = nameProvider.genSym();
		MemberFunction getConstraintsFunction = new MemberFunction("getConstraints",
				ICCSLConstraint.class.getSimpleName() + "[]");
		getConstraintsFunction.setOverrides(true);
		getConstraintsFunction.addModifier("protected");
		String initializer = new String();
		for (Iterator<String> iter = constraintNames.iterator(); iter.hasNext();) {
			initializer += iter.next();
			if (iter.hasNext())
				initializer += ", ";
		}
		getConstraintsFunction.addStatement(ICCSLConstraint.class.getSimpleName() + "[] " + tempSymbol 
				+ " = { " + initializer + " }");
		getConstraintsFunction.addStatement("return " + tempSymbol);
		classDef.addMemberFunction(getConstraintsFunction);
		classDef.addImportedPackage(ICCSLConstraint.class.getName());
		
		MemberFunction subblocksFunction = new MemberFunction("getSubBlocks",
				AbstractRuntimeBlock.class.getSimpleName() + "[]");
		subblocksFunction.setOverrides(true);
		subblocksFunction.addModifier("public");
		initializer = new String();
		for (Iterator<String> it = subBlockNames.iterator(); it.hasNext();) {
			initializer += it.next();
			if (it.hasNext())
				initializer += ", ";
		}
		subblocksFunction.addStatement(AbstractRuntimeBlock.class.getSimpleName() + "[] " + tempSymbol 
				+ " = { " + initializer + " }");
		subblocksFunction.addStatement("return " + tempSymbol);
		classDef.addMemberFunction(subblocksFunction);
		
		classDef.generate(pluginHelper);
	}

	private String generateRelationCode(NamedElement context, Relation relation,
			ClassDefinition enclosingClass, IProject project) throws CoreException {

		String variableName = nameProvider.getVariableName(relation);
		String relationClassName = nameProvider.getClassName(relation);
		String relationPackageName = nameProvider.getPackageName(relation);
		// Get arguments to generate correct construction code
		RelationDeclaration declaration = relation.getType();
		if ( ! (declaration instanceof KernelRelationDeclaration)) {
			RelationDefinition definition = unfoldModel.getUsedRelationDefinition(declaration);
			if (definition != null) {
				usedRelationDefinitions.add(definition);
			}
		}
		List<String> arguments = new ArrayList<String>();
		List<AbstractEntity> parameters;
		if (declaration instanceof KernelRelationDeclaration) {
			parameters = new ArrayList<AbstractEntity>();
			// Kernel relation have an empty parameters list, but they have
			// a left and a right parameter.
			KernelRelationDeclaration kdecl = (KernelRelationDeclaration) declaration;
			parameters.add(kdecl.getLeftEntity());
			parameters.add(kdecl.getRightEntity());
		} else {
			parameters = declaration.getParameters();
		}
		for (AbstractEntity parameter : parameters) {
			if (context instanceof UserRelationDefinition || context instanceof UserExpressionDefinition) {
				BindableEntity value = resolveAbstract(parameter, relation.getBindings());
				if (value instanceof Expression) {
					arguments.add(nameProvider.getClockName((Expression) value));
				} else if (value instanceof ClassicalExpression) {
					arguments.add(nameProvider.getProviderName(value));
					
				} else {
					arguments.add(nameProvider.getVariableName(value));
				}
			} else {
				NamedElement modelObject = resolveAbstract(parameter, relation.getBindings());
				arguments.add(generateAccessCode(context, relation, modelObject, enclosingClass));
			}
		}
		MemberVariable variable = new MemberVariable(variableName, relationClassName);
		variable.addModifier("public");
		enclosingClass.addMemberVariable(variable);

		MemberFunction initFunction = enclosingClass.getMemberFunction("init");		
		String initializer = variableName + " = new " + relationClassName + "(";
		for (Iterator<String> iter = arguments.iterator(); iter.hasNext();) {
			initializer += iter.next();
			if (iter.hasNext())
				initializer += ", ";
		}
		initializer += ")";
		initFunction.addStatement(initializer);
		initFunction.addStatement(variableName + ".setParent(this)");
		initFunction.addStatement(variableName + ".setName(\"" + relation.getName() + "\")");
		enclosingClass.addImportedPackage(relationPackageName + "." + relationClassName);
		
		if (relation.getIsAnAssertion()) {
			initFunction.addStatement(variableName + ".setAssertion(true)");
		}
		else {
			initFunction.addStatement(variableName + ".setAssertion(false)");
		}
		
		return variableName;
	}

	private BindableEntity resolveAbstract(AbstractEntity parameter, List<Binding> bindings) {
		for (Binding binding : bindings) {
			if (parameter == binding.getAbstract()) {
				BindableEntity target = binding.getBindable();
				return target;
			}
		}
		return null;
	}

	private EqualitySolver<String> definitionClockEqualityAnalysis(List<Relation> relations) {
		boolean foundCoincidence = false;
		EqualitySolver<String> equalityRegister = new EqualitySolver<String>();
		for (Relation relation : relations) {
			if ( ( ! relation.getIsAnAssertion()) && relation.getType() instanceof Coincidence) {
				foundCoincidence = true;
				AbstractEntity left = ((Coincidence) relation.getType()).getLeftEntity();
				AbstractEntity right = ((Coincidence) relation.getType()).getRightEntity();
				BindableEntity leftValue = resolveAbstract(left, relation.getBindings());
				BindableEntity rightValue = resolveAbstract(right, relation.getBindings());
				String leftName = null;
				if (leftValue instanceof Clock)
					leftName = nameProvider.getVariableName(leftValue);
				else if (leftValue instanceof Expression)
					leftName = nameProvider.getClockName((Expression) leftValue);
				String rightName = null;
				if (rightValue instanceof Clock)
					rightName = nameProvider.getVariableName(rightValue);
				else if (rightValue instanceof Expression)
					rightName = nameProvider.getClockName((Expression) rightValue);
				if (leftName != null && rightName != null) {
					equalityRegister.registerEquality(leftName, rightName);
				}
			}
		}
		if ( ! foundCoincidence) {
			return null;
		}
		else {
			return equalityRegister;
		}
	}

	private void compileUserRelationDefinition(UserRelationDefinition definition,
			IProject project) throws CoreException {

		String definitionClassName = nameProvider.getClassName(definition);
		String definitionClassPackage = nameProvider.getPackageName(definition);
		ClassDefinition classDef = new ClassDefinition(definitionClassName, definitionClassPackage);
		classDef.setParentClassName(AbstractRuntimeRelation.class.getSimpleName());
		classDef.addImportedPackage(AbstractRuntimeRelation.class.getName());
		classDef.setProject(project);
		
		List<Relation> allRelations = new ArrayList<Relation>();
		for (ConcreteEntity entity : definition.getConcreteEntities()) {
			if (entity instanceof Relation)
				allRelations.add((Relation) entity);
		}
		EqualitySolver<String> equalityRegister = definitionClockEqualityAnalysis(allRelations);
		allRelations.clear();
		
		List<String> elementNames = new ArrayList<String>();
		List<String> constraintNames = new ArrayList<String>();
		List<String> relationNames = new ArrayList<String>();
		List<String> expressionNames = new ArrayList<String>();
		List<String> clockNames = new ArrayList<String>();
		
		List<AbstractEntity> nonClockParameters = getNonClockParameters(definition);

		MemberFunction initFunction = new MemberFunction("init", "void");
		initFunction.addModifier("protected");
		classDef.addMemberFunction(initFunction);
		
		// Generate private variables for each parameter of the associated
		// declaration
		generateArgumentsPrivateMembers(definition, classDef, nonClockParameters);

		// Generate a constructor taking as arguments the parameters of the
		// declaration
		generateRelationDefinitionConstructor(definition, definitionClassName, classDef, nonClockParameters);
		
		for (ClassicalExpression expression : definition.getClassicalExpressions()) {
			generateClassicalExpression(expression, definition, classDef);
		}

		for (ConcreteEntity entity : definition.getConcreteEntities()) {
			if (entity instanceof Relation) {
				String relationVarName = generateRelationCode(definition, (Relation) entity,
						classDef, project);
				constraintNames.add(relationVarName);
				relationNames.add(relationVarName);
			} else if (entity instanceof Expression) {
				
				String clockVariableName = nameProvider.getClockName((Expression) entity);
				String clockName = entity.getName();
				clockNames.add(clockVariableName);
				
				generateDiscreteClockCode(clockVariableName, clockName,
						nameProvider.implicitClockClassName, nameProvider.implicitClockPackage,
						"public", classDef, initFunction, clockNames, equalityRegister, null);
				elementNames.add(clockVariableName);				
				
				String expressionVarName = generateExpressionCode(definition, (Expression) entity,
						false, project, classDef);
				constraintNames.add(expressionVarName);
				expressionNames.add(expressionVarName);
			}
			else if (entity instanceof Clock) {
				String clockName = entity.getName();
				String variableName = nameProvider.getVariableName(entity);
				String clockClassName = nameProvider.clockClassName;
				String clockPackageName = nameProvider.clockClassPackage;
				
				generateDiscreteClockCode(variableName, clockName,
						clockClassName, clockPackageName, "private", classDef, initFunction,
						clockNames, equalityRegister, null);
				generateDiscreteClockGetter(nameProvider.getVariableGetterName(entity), variableName, clockName, clockClassName, clockPackageName,
						"public", classDef, initFunction);
				
				clockNames.add(variableName);
			}
			else if (entity instanceof Element) {
				String elementVarName = generateElementCode(entity, "private", classDef, true);
				elementNames.add(elementVarName);
			}
		}

		// protected ICCSLConstraint[] getConstraints() { ... }
		MemberFunction getConstraintsFunction = new MemberFunction("getConstraints",
				ICCSLConstraint.class.getSimpleName() + "[]");
		getConstraintsFunction.setOverrides(true);
		getConstraintsFunction.addModifier("protected");
		{
			String initializer = new String();
			for (Iterator<String> iter = constraintNames.iterator(); iter.hasNext();) {
				initializer += iter.next();
				if (iter.hasNext())
					initializer += ", ";
			}
			getConstraintsFunction.addStatement(ICCSLConstraint.class.getSimpleName() + "[] res = { " +
					initializer + " }");
		}
		getConstraintsFunction.addStatement("return res");
		classDef.addMemberFunction(getConstraintsFunction);
		
		// public void setAssertion(boolean assertion) { ... }
		MemberFunction setAssertionFunction = new MemberFunction("setAssertion");
		setAssertionFunction.setOverrides(true);
		setAssertionFunction.addModifier("public");
		setAssertionFunction.addParameter("boolean", "assertion");
		setAssertionFunction.addStatement("super.setAssertion(assertion)");
		for (String subRelationName : relationNames) {
			setAssertionFunction.addStatement(subRelationName + ".setAssertion(assertion)");
		}
		classDef.addMemberFunction(setAssertionFunction);
		
		// public void start(AbstractSemanticHelper helper) throws SimulationException { ... }
		MemberFunction startFunction = new MemberFunction("start");
		startFunction.setOverrides(true);
		startFunction.addModifier("public");
		startFunction.addException("SimulationException");
		startFunction.addParameter("AbstractSemanticHelper", "helper");
		{
			IfStatement st = new IfStatement("! canCallStart()");
			st.addThenStatement(new SimpleStatement("return"));
			startFunction.addStatement(st);
		}
		startFunction.addStatement("super.start(helper)");
		for (AbstractEntity parameter : nonClockParameters) {
			IfStatement providerTest = new IfStatement();
			providerTest.setTestCode(nameProvider.getProviderName(parameter) + " != null");
			providerTest.addThenStatement(nameProvider.getVariableName(parameter) + " = "
					+ nameProvider.getProviderName(parameter) + ".getValue()");
			startFunction.addStatement(providerTest);
		}		
		for (String relationName : relationNames) {
			startFunction.addStatement(relationName + ".start(helper)");
		}
		for (String expressionName : expressionNames) {
			startFunction.addStatement(expressionName + ".start(helper)");
		}
		classDef.addMemberFunction(startFunction);
		classDef.addImportedPackage(SimulationException.class.getName());
		
		// public void semantic(AbstractSemanticHelper helper)  throws SimulationException { ... }
		MemberFunction semanticFunction = new MemberFunction("semantic");
		semanticFunction.setOverrides(true);
		semanticFunction.addModifier("public");
		semanticFunction.addException("SimulationException");
		semanticFunction.addParameter("AbstractSemanticHelper", "helper");
		{
			IfStatement st = new IfStatement("! canCallSemantic()");
			st.addThenStatement(new SimpleStatement("return"));
			semanticFunction.addStatement(st);
		}
		semanticFunction.addStatement("super.semantic(helper)");
		for (String relationName : relationNames) {
			semanticFunction.addStatement(relationName + ".semantic(helper)");
		}
		for (String expressionName : expressionNames) {
			semanticFunction.addStatement(expressionName + ".semantic(helper)");
		}
		classDef.addMemberFunction(semanticFunction);
		
		// public void assertionSemantic(AbstractSemanticHelper helper) { ... }
		MemberFunction assertionSemFunction = new MemberFunction("assertionSemantic");
		assertionSemFunction.setOverrides(true);
		assertionSemFunction.addModifier("public");
		assertionSemFunction.addParameter("AbstractSemanticHelper", "helper");
		for (String subRelationName : relationNames) {
			assertionSemFunction.addStatement(subRelationName + ".assertionSemantic(helper)");
		}
		assertionSemFunction.addStatement("BuDDyBDD andTerm = helper.createOne()");
		for (String subRelationName : relationNames) {
			String bddVar = nameProvider.genSym("bddVar");
			assertionSemFunction.addStatement("BuDDyBDD " + bddVar + " = helper.getAssertionVariable(" + subRelationName + ")");
			assertionSemFunction.addStatement("andTerm = helper.createAnd(andTerm, " + bddVar + ")");
		}
		assertionSemFunction.addStatement("helper.assertionSemantic(this, andTerm)");
		classDef.addMemberFunction(assertionSemFunction);
		
		// public void update(AbstractUpdateHelper helper)  throws SimulationException { ... }
		MemberFunction updateFunction = new MemberFunction("update");
		updateFunction.setOverrides(true);
		updateFunction.addModifier("public");
		updateFunction.addParameter("AbstractUpdateHelper", "helper");
		updateFunction.addException("SimulationException");
		{
			IfStatement st = new IfStatement("! canCallUpdate()");
			st.addThenStatement(new SimpleStatement("return"));
			updateFunction.addStatement(st);
		}
		updateFunction.addStatement("super.update(helper)");
		for (String relationName : relationNames) {
			updateFunction.addStatement(relationName + ".update(helper)");
		}
		for (String expressionName : expressionNames) {
			updateFunction.addStatement(expressionName + ".update(helper)");
		}
		classDef.addMemberFunction(updateFunction);
		
		// public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException { .. }
		
		// public void terminate(AbstractUpdateHelper helper) throws SimulationException { ... }
		
		classDef.addImportedPackage(ICCSLConstraint.class.getName());
		classDef.addImportedPackage(AbstractSemanticHelper.class.getName());
		classDef.addImportedPackage(AbstractUpdateHelper.class.getName());

		classDef.generate(pluginHelper);
	}

	
	private void compileConditionalRelationDefinition(ConditionalRelationDefinition definition, IProject project) throws CoreException, CompilerException {
		String definitionClassName = nameProvider.getClassName(definition);
		String definitionClassPackage = nameProvider.getPackageName(definition);
		ClassDefinition classDef = new ClassDefinition(definitionClassName, definitionClassPackage);
		classDef.setParentClassName(AbstractRuntimeRelation.class.getSimpleName());
		classDef.addImportedPackage(AbstractRuntimeRelation.class.getName());
		classDef.setProject(project);

		MemberFunction initFunction = new MemberFunction("init", "void");
		initFunction.addModifier("protected");
		classDef.addMemberFunction(initFunction);

		classDef.addImportedPackage(ICCSLConstraint.class.getName());
		classDef.addImportedPackage(AbstractSemanticHelper.class.getName());
		classDef.addImportedPackage(AbstractUpdateHelper.class.getName());
		
		List<AbstractEntity> nonClockParameters = getNonClockParameters(definition);

		generateArgumentsPrivateMembers(definition, classDef, nonClockParameters);
		
		generateRelationDefinitionConstructor(definition, definitionClassName, classDef, nonClockParameters);
		
		classDef.addImportedPackage(nameProvider.clockClassPackage + "." + nameProvider.clockClassName);

		HashMap<ClassicalExpression, String> classicalExprNames = new HashMap<ClassicalExpression, String>();
		HashMap<ClassicalExpression, String> classicalExprProviderNames = new HashMap<ClassicalExpression, String>();
		HashMap<Relation, String> relationNames = new HashMap<Relation, String>();
		HashMap<Expression, String> expressionNames = new HashMap<Expression, String>();
		List<String> constraintNames = new ArrayList<String>();

		for (ClassicalExpression expression : definition.getClassicalExpressions()) {
			ClassicalExpressionCompiler generator = new ClassicalExpressionCompiler(parameters, pluginHelper, nameProvider);
			String fctName = generator.generateClassicalExpression(expression, definition, classDef);
			classicalExprNames.put(expression, fctName);
			String providerTypeName = ValueProvider.class.getSimpleName() + "<" + generator.getReturnTypeName() + ">";
			String providerName = nameProvider.getProviderName(expression);
			MemberVariable providerVariable = new MemberVariable(providerName, providerTypeName);
			providerVariable.addModifier("private");

			String initializer = " new " + providerTypeName + "() {\n";
			initializer += "      public " + generator.getReturnTypeName() + " getValue() {\n";
			initializer += "        return " + fctName + "();\n";
			initializer += "      }\n" + "    }";
			providerVariable.setInitializer(initializer);
			
			classDef.addMemberVariable(providerVariable);
			classDef.addImportedPackage(ValueProvider.class.getName());
			classicalExprProviderNames.put(expression, providerName);
		}

		for (ConcreteEntity entity : definition.getConcreteEntities()) {
			if (entity instanceof Relation) {
				String relationVarName = generateRelationCode(definition, (Relation) entity,
						classDef, project);
				constraintNames.add(relationVarName);
				relationNames.put((Relation) entity, relationVarName);
			} else if (entity instanceof Expression) {
				String clockVariableName = nameProvider.getClockName((Expression) entity);
				String clockName = entity.getName();
				
				generateDiscreteClockCode(clockVariableName, clockName,
						nameProvider.implicitClockClassName, nameProvider.implicitClockPackage,
						"public", classDef, initFunction, null, null, null);

				String expressionVarName;
				expressionVarName = generateExpressionCode(definition, (Expression) entity,
						false, project, classDef);
				constraintNames.add(expressionVarName);
				expressionNames.put((Expression) entity, expressionVarName);
								
			} else if (entity instanceof Element) {
				@SuppressWarnings("unused")
				String elementVarName = generateElementCode(entity, "private", classDef, true);
			}
		}
		
		HashMap<RelCase, String> switchTestNames = new HashMap<RelCase, String>();
		// Each branch of the conditional has an index starting at 1. 0 is used for the default case.
		HashMap<RelCase, Integer> switchTestIndices = new HashMap<RelCase, Integer>();
		int switchTestCounter = 0; // use to generate indices.
		
		/*
		 * the following variable is used to represent the last selected branch of the conditional. Each
		 * branch has an index (stored in the map switchTestIndices) and this variable is set to the corresponding
		 * value when conditional tests are evaluated. This allows to check when a different branch w.r.t to the
		 * previous execution is selected : associated relations and expressions are started, ie. call start() on them.  
		 */
		MemberVariable selectionVar = new MemberVariable("selectedCase", "int");
		selectionVar.addModifier("private");
		selectionVar.setInitializer("-1");  // conditional cases have index starting from 1. 
		classDef.addMemberVariable(selectionVar);
		
		for (RelCase swCase : definition.getRelCases()) {
			BooleanExpression test = swCase.getCondition();
			String testFunctionName = generateClassicalExpression(test, definition, classDef);
			switchTestNames.put(swCase, testFunctionName);
			List<Relation> relations = swCase.getRelation();
			for (Relation relation : relations) {
				String relationVarName = generateRelationCode(definition, relation, classDef, project);
				if (relationNames.get(relationVarName) != null) {
					throw new CompilerException("Duplicate relation name \"" + relation.getName()
							+ "\" in conditional relation definition " + definition.getName());
				}
				relationNames.put(relation, relationVarName);
				constraintNames.add(relationVarName);
			}
			switchTestCounter += 1;
			switchTestIndices.put(swCase, switchTestCounter);
		}
		if ( definition.getDefaultRelation() != null) {
			EList<Relation> defaultRelations = definition.getDefaultRelation();
			for (Relation relation : defaultRelations) {
				String relationVarName = generateRelationCode(definition, relation, classDef, project);
				if (relationNames.get(relationVarName) != null) {
					throw new CompilerException("Duplicate relation name \"" + relation.getName()
							+ "\" in conditional relation definition " + definition.getName());
				}
				relationNames.put(relation, relationVarName);
				constraintNames.add(relationVarName);
			}
		}

		// protected ICCSLConstraint[] getConstraints() { ... }
		MemberFunction getConstraintsFunction = new MemberFunction("getConstraints",
				ICCSLConstraint.class.getSimpleName() + "[]");
		getConstraintsFunction.setOverrides(true);
		getConstraintsFunction.addModifier("protected");
		{
			String initializer = new String();
			for (Iterator<String> iter = constraintNames.iterator(); iter.hasNext();) {
				initializer += iter.next();
				if (iter.hasNext())
					initializer += ", ";
			}
			getConstraintsFunction.addStatement(ICCSLConstraint.class.getSimpleName() + "[] res = { " +
					initializer + " }");
		}
		getConstraintsFunction.addStatement("return res");
		classDef.addMemberFunction(getConstraintsFunction);

		// public void start(AbstractSemanticHelper helper) throws SimulationException
		generateConditionalRelationStartFunction(definition, classDef, switchTestNames, switchTestIndices,
				relationNames, expressionNames, nonClockParameters);

		// public void semantic(AbstractSemanticHelper helper) throws SimulationException;
		generateConditionalRelationSemanticFunction(definition, classDef, switchTestNames, switchTestIndices,
				relationNames, expressionNames, nonClockParameters);
		
		// public void setAssertion(boolean assertion) { ... }
		MemberFunction setAssertionFunction = new MemberFunction("setAssertion");
		setAssertionFunction.setOverrides(true);
		setAssertionFunction.addModifier("public");
		setAssertionFunction.addParameter("boolean", "assertion");
		setAssertionFunction.addStatement("super.setAssertion(assertion)");
		for (String subRelationName : relationNames.values()) {
			setAssertionFunction.addStatement(subRelationName + ".setAssertion(assertion)");
		}
		classDef.addMemberFunction(setAssertionFunction);

		// public void assertionSemantic(AbstractSemanticHelper helper) { ... }
		generateConditionalRelationAssertionSemanticFunction(definition, classDef, switchTestNames,
				relationNames, expressionNames, nonClockParameters);
		
		// public void update(AbstractUpdateHelper helper) throws SimulationException;
		generateConditionalRelationUpdateFunction(definition, classDef, switchTestNames,
				relationNames, expressionNames, nonClockParameters);
		
		// public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException;
		generateConditionalRelationDeathSemanticFunction(definition, classDef, switchTestNames,
				relationNames, expressionNames, nonClockParameters);

		classDef.generate(pluginHelper);		
	}

	private void generateRelationDefinitionConstructor(RelationDefinition definition, String definitionClassName,
			ClassDefinition classDefinition, List<AbstractEntity> nonClockParameters) {
		List<AbstractEntity> parameters = definition.getDeclaration().getParameters();
		MemberFunction constructor = new MemberFunction(definitionClassName);
		constructor.setConstructor(true);
		constructor.addModifier("public");

		for (Iterator<AbstractEntity> iter = parameters.iterator(); iter.hasNext();) {
			AbstractEntity parameter = iter.next();
			Type parameterType = parameter.getType();
			@SuppressWarnings("rawtypes")
			Class parameterClass = nameProvider.getJavaClass(parameterType);
			String paramClassName = nameProvider.getClassName(parameter);
			
			constructor.addParameter(paramClassName, nameProvider.getVariableName(parameter));
			classDefinition.addImportedPackage(parameterClass.getName());
		}
		constructor.addStatement("super()");
		for (AbstractEntity parameter : parameters) {
			constructor.addStatement("this."+ nameProvider.getVariableName(parameter) + " = "
					+ nameProvider.getVariableName(parameter));
		}
		constructor.addStatement("init()");
		classDefinition.addMemberFunction(constructor);

		if ( ! nonClockParameters.isEmpty()) {
			constructor = new MemberFunction(definitionClassName);
			constructor.setConstructor(true);
			constructor.addModifier("public");
			
			for (Iterator<AbstractEntity> iter = parameters.iterator(); iter.hasNext();) {
				AbstractEntity parameter = iter.next();
				Type parameterType = parameter.getType();
				@SuppressWarnings("rawtypes")
				Class parameterClass = nameProvider.getJavaClass(parameterType);
				String paramClassName = nameProvider.getClassName(parameter);
				if (nonClockParameters.contains(parameter)) {
					constructor.addParameter(ValueProvider.class.getSimpleName() + "<" + paramClassName + ">",
							nameProvider.getVariableName(parameter));
				}
				else {
					constructor.addParameter(paramClassName, nameProvider.getVariableName(parameter));
				}
				classDefinition.addImportedPackage(parameterClass.getName());
				classDefinition.addImportedPackage(ValueProvider.class.getName());
			}
			for (AbstractEntity parameter : parameters) {
				String var;
				if (nonClockParameters.contains(parameter)) {
					var = nameProvider.getProviderName(parameter);
				}
				else {
					var = nameProvider.getVariableName(parameter);
				}
				constructor.addStatement("this." + var + " = " + nameProvider.getVariableName(parameter));
			}
			constructor.addStatement("init()");
			classDefinition.addMemberFunction(constructor);
		}
	}
	
	private void generateConditionalRelationStartFunction(ConditionalRelationDefinition definition,
			ClassDefinition classDefinition,
			HashMap<RelCase, String> switchTestNames,
			HashMap<RelCase, Integer> switchTestIndices,
			HashMap<Relation, String> relationNames, HashMap<Expression, String> expressionNames,
			List<AbstractEntity> nonClockParameters) {
		/*
		 * start() function generation for a Conditional Relation Definition.
		 */
		MemberFunction startFunction = new MemberFunction("start", "void");
		startFunction.setOverrides(true);
		startFunction.addModifier("public");
		startFunction.addException(SimulationException.class.getSimpleName());
		startFunction.addParameter(AbstractSemanticHelper.class.getSimpleName(), "helper");
		classDefinition.addMemberFunction(startFunction);
		classDefinition.addImportedPackage(SimulationException.class.getName());
		{
			IfStatement st = new IfStatement("! canCallStart()");
			st.addThenStatement(new SimpleStatement("return"));
			startFunction.addStatement(st);
		}
		startFunction.addStatement("super.start(helper)");

		// Initialize private variables that depends possibly on the evaluation of a classical
		// expression to get their value.
		for (AbstractEntity parameter : nonClockParameters) {
			IfStatement providerTest = new IfStatement();
			providerTest.setTestCode(nameProvider.getProviderName(parameter) + " != null");
			providerTest.addThenStatement(nameProvider.getVariableName(parameter) + " = "
					+ nameProvider.getProviderName(parameter) + ".getValue()");
			startFunction.addStatement(providerTest);
		}
		// Iterate over all cases, and generate code using a series of if-then-else(if) statements, the
		// default case being the else branch of the last if-statement.
		// Create an instance of if-statement, and replace it by a new one while there are still a case
		// to handle in the definition.
		IfStatement caseIfStatement = new IfStatement();
		startFunction.addStatement(caseIfStatement);
		for (Iterator<RelCase> iter = definition.getRelCases().iterator(); iter.hasNext();) {
			RelCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			caseIfStatement.addThenStatement("selectedCase = " + switchTestIndices.get(swCase));
			for (Relation relation : swCase.getRelation()) {
				String relationVarName = relationNames.get(relation);
				caseIfStatement.addThenStatement(relationVarName + ".start(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".start(helper)");
				}
			}
			if (iter.hasNext()) {
				// Create a new if-statement, attach it to the current if-statement as the (unique) else clause
				// and make it the current if-statement.
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if (definition.getDefaultRelation() != null && ! definition.getDefaultRelation().isEmpty()) {
			caseIfStatement.addElseStatement("selectedCase = 0");
			for (Relation relation : definition.getDefaultRelation()) {
				caseIfStatement.addElseStatement(relationNames.get(relation) + ".start(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".start(helper)");
				}
			}
		}
	}
	
	private void generateConditionalRelationSemanticFunction(ConditionalRelationDefinition definition,
			ClassDefinition classDefinition,
			HashMap<RelCase, String> switchTestNames, HashMap<RelCase, Integer> switchTestIndices,
			HashMap<Relation, String> relationNames, HashMap<Expression, String> expressionNames,
			List<AbstractEntity> nonClockParameters) {
		/*
		 * semantic() function generation for a Conditional Relation Definition.
		 */
		MemberFunction semanticFunction = new MemberFunction("semantic", "void");
		semanticFunction.setOverrides(true);
		semanticFunction.addModifier("public");
		semanticFunction.addException(SimulationException.class.getSimpleName());
		semanticFunction.addParameter(AbstractSemanticHelper.class.getSimpleName(), "helper");
		classDefinition.addMemberFunction(semanticFunction);
		classDefinition.addImportedPackage(SimulationException.class.getName());
		{
			IfStatement st = new IfStatement("! canCallSemantic()");
			st.addThenStatement(new SimpleStatement("return"));
			semanticFunction.addStatement(st);
		}
		semanticFunction.addStatement("super.semantic(helper)");

		// Iterate over all cases, and generate code using a series of if-then-else(if) statements, the
		// default case being the else branch of the last if-statement.
		// Create an instance of if-statement, and replace it by a new one while there are still a case
		// to handle in the definition.
		IfStatement caseIfStatement = new IfStatement();
		semanticFunction.addStatement(caseIfStatement);
		for (Iterator<RelCase> iter = definition.getRelCases().iterator(); iter.hasNext();) {
			RelCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			IfStatement selectionChangedIf = new IfStatement("selectedCase != " + switchTestIndices.get(swCase));
			for (Relation relation : swCase.getRelation()) {
				String relationVarName = relationNames.get(relation);
				selectionChangedIf.addThenStatement(relationVarName + ".start(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					selectionChangedIf.addThenStatement(exprName + ".start(helper)");
				}
			}
			caseIfStatement.addThenStatement(selectionChangedIf);
			caseIfStatement.addThenStatement("selectedCase = " + switchTestIndices.get(swCase));
			for (Relation relation : swCase.getRelation()) {
				String relationVarName = relationNames.get(relation);
				caseIfStatement.addThenStatement(relationVarName + ".semantic(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".semantic(helper)");
				}
			}
			if (iter.hasNext()) {
				// Create a new if-statement, attach it to the current if-statement as the (unique) else clause
				// and make it the current if-statement.
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if (definition.getDefaultRelation() != null && ! definition.getDefaultRelation().isEmpty()) {
			IfStatement selectionChangedIf = new IfStatement("selectedCase != 0");
			for (Relation relation : definition.getDefaultRelation()) {
				String relationVarName = relationNames.get(relation);
				selectionChangedIf.addThenStatement(relationVarName + ".start(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					selectionChangedIf.addThenStatement(exprName + ".start(helper)");
				}
			}
			caseIfStatement.addElseStatement(selectionChangedIf);
			caseIfStatement.addElseStatement("selectedCase = 0");
			for (Relation relation : definition.getDefaultRelation()) {
				caseIfStatement.addElseStatement(relationNames.get(relation) + ".semantic(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".semantic(helper)");
				}
			}
		}
	}
	
	private void generateConditionalRelationAssertionSemanticFunction(ConditionalRelationDefinition definition,
			ClassDefinition classDef, HashMap<RelCase, String> switchTestNames,
			HashMap<Relation, String> relationNames, HashMap<Expression, String> expressionNames,
			List<AbstractEntity> nonClockParameters) {
		/*
		 * assertionSemantic() function generation for a Conditional Relation Definition.
		 */
		MemberFunction assertSemanticFunction = new MemberFunction("assertionSemantic", "void");
		assertSemanticFunction.setOverrides(true);
		assertSemanticFunction.addModifier("public");
		assertSemanticFunction.addParameter(AbstractSemanticHelper.class.getSimpleName(), "helper");
		classDef.addMemberFunction(assertSemanticFunction);

		assertSemanticFunction.addStatement("BuDDyBDD andTerm = helper.createOne()");
		// Iterate over all cases, and generate code using a series of if-then-else(if) statements, the
		// default case being the else branch of the last if-statement.
		// Create an instance of if-statement, and replace it by a new one while there are still a case
		// to handle in the definition.
		IfStatement caseIfStatement = new IfStatement();
		assertSemanticFunction.addStatement(caseIfStatement);
		for (Iterator<RelCase> iter = definition.getRelCases().iterator(); iter.hasNext();) {
			RelCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			for (Relation relation : swCase.getRelation()) {
				String relationVarName = relationNames.get(relation);
				caseIfStatement.addThenStatement(relationVarName + ".assertionSemantic(helper)");
				String bddVar = nameProvider.genSym("bddVar");
				caseIfStatement.addThenStatement("BuDDyBDD " + bddVar + " = helper.getAssertionVariable(" + relationVarName + ")");
				caseIfStatement.addThenStatement("andTerm = helper.createAnd(andTerm, " + bddVar + ")");
			}
			if (iter.hasNext()) {
				// Create a new if-statement, attach it to the current if-statement as the (unique) else clause
				// and make it the current if-statement.
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if (definition.getDefaultRelation() != null && ! definition.getDefaultRelation().isEmpty()) {
			for (Relation relation : definition.getDefaultRelation()) {
				String relationVarName = relationNames.get(relation);
				caseIfStatement.addElseStatement(relationVarName + ".assertionSemantic(helper)");
				String bddVar = nameProvider.genSym("bddVar");
				caseIfStatement.addThenStatement("BuDDyBDD " + bddVar + " = helper.getAssertionVariable(" + relationVarName + ")");
				caseIfStatement.addThenStatement("andTerm = helper.createAnd(andTerm, " + bddVar + ")");
			}
		}
		assertSemanticFunction.addStatement("helper.assertionSemantic(this, andTerm)");
	}


	private void generateConditionalRelationUpdateFunction(ConditionalRelationDefinition definition,
			ClassDefinition classDefinition,
			HashMap<RelCase, String> switchTestNames,
			HashMap<Relation, String> relationNames, HashMap<Expression, String> expressionNames,
			List<AbstractEntity> nonClockParameters) {
		/*
		 * update() function generation for a Conditional Relation Definition.
		 */
		MemberFunction updateFunction = new MemberFunction("update", "void");
		updateFunction.setOverrides(true);
		updateFunction.addModifier("public");
		updateFunction.addException(SimulationException.class.getSimpleName());
		updateFunction.addParameter(AbstractUpdateHelper.class.getSimpleName(), "helper");
		classDefinition.addMemberFunction(updateFunction);
		classDefinition.addImportedPackage(SimulationException.class.getName());
		{
			IfStatement st = new IfStatement("! canCallUpdate()");
			st.addThenStatement(new SimpleStatement("return"));
			updateFunction.addStatement(st);
		}
		updateFunction.addStatement("super.update(helper)");

		// Iterate over all cases, and generate code using a series of if-then-else(if) statements, the
		// default case being the else branch of the last if-statement.
		// Create an instance of if-statement, and replace it by a new one while there are still a case
		// to handle in the definition.
		IfStatement caseIfStatement = new IfStatement();
		updateFunction.addStatement(caseIfStatement);
		for (Iterator<RelCase> iter = definition.getRelCases().iterator(); iter.hasNext();) {
			RelCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			for (Relation relation : swCase.getRelation()) {
				String relationVarName = relationNames.get(relation);
				caseIfStatement.addThenStatement(relationVarName + ".update(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".update(helper)");
				}
			}
			if (iter.hasNext()) {
				// Create a new if-statement, attach it to the current if-statement as the (unique) else clause
				// and make it the current if-statement.
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if (definition.getDefaultRelation() != null && ! definition.getDefaultRelation().isEmpty()) {
			for (Relation relation : definition.getDefaultRelation()) {
				caseIfStatement.addElseStatement(relationNames.get(relation) + ".update(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".update(helper)");
				}
			}
		}
	}
	
	private void generateConditionalRelationDeathSemanticFunction(ConditionalRelationDefinition definition,
			ClassDefinition classDefinition,
			HashMap<RelCase, String> switchTestNames,
			HashMap<Relation, String> relationNames, HashMap<Expression, String> expressionNames,
			List<AbstractEntity> nonClockParameters) {
		/*
		 * semantic() function generation for a Conditional Relation Definition.
		 */
		MemberFunction deathSemanticFunction = new MemberFunction("deathSemantic", "void");
		deathSemanticFunction.setOverrides(true);
		deathSemanticFunction.addModifier("public");
		deathSemanticFunction.addException(SimulationException.class.getSimpleName());
		deathSemanticFunction.addParameter(AbstractSemanticHelper.class.getSimpleName(), "helper");
		classDefinition.addMemberFunction(deathSemanticFunction);
		classDefinition.addImportedPackage(SimulationException.class.getName());
		deathSemanticFunction.addStatement("super.deathSemantic(helper)");

		// Iterate over all cases, and generate code using a series of if-then-else(if) statements, the
		// default case being the else branch of the last if-statement.
		// Create an instance of if-statement, and replace it by a new one while there are still a case
		// to handle in the definition.
		IfStatement caseIfStatement = new IfStatement();
		deathSemanticFunction.addStatement(caseIfStatement);
		for (Iterator<RelCase> iter = definition.getRelCases().iterator(); iter.hasNext();) {
			RelCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			for (Relation relation : swCase.getRelation()) {
				String relationVarName = relationNames.get(relation);
				caseIfStatement.addThenStatement(relationVarName + ".deathSemantic(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".deathSemantic(helper)");
				}
			}
			if (iter.hasNext()) {
				// Create a new if-statement, attach it to the current if-statement as the (unique) else clause
				// and make it the current if-statement.
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if (definition.getDefaultRelation() != null && ! definition.getDefaultRelation().isEmpty()) {
			for (Relation relation : definition.getDefaultRelation()) {
				caseIfStatement.addElseStatement(relationNames.get(relation) + ".deathSemantic(helper)");
				List<Expression> subExpressions = getExpressionTree(relation, true, false);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".deathSemantic(helper)");
				}
			}
		}
	}


	
	private class ElementCodeGenerator extends BasicTypeSwitch<String> {
		private ClassDefinition enclosingClass;
		private String modifiers;
		public ElementCodeGenerator(ClassDefinition enclosingClass, String modifiers) {
			this.enclosingClass = enclosingClass;
			this.modifiers = modifiers;
		}
		@Override
		public String caseSequenceElement(SequenceElement object) {
			String variableName = nameProvider.getVariableName(object);
			String sequenceClassName = nameProvider.getJavaClass(object).getSimpleName();
			SequenceType seqType = (SequenceType) object.getType();
			String elementClassName;
			if (seqType == null) {
				elementClassName = "Integer";
			}
			else {
				elementClassName = nameProvider.getClassName(seqType.getElementType());
			}
			String fullTypeName = sequenceClassName + "<" + elementClassName + ">";
			MemberVariable variable = new MemberVariable(variableName, fullTypeName);
			variable.addModifier(modifiers);
			variable.setInitializer("new " + fullTypeName + "()");
			
			MemberFunction initFunction = enclosingClass.getMemberFunction("init");
			if (object.getFinitePart() != null && ! object.getFinitePart().isEmpty()) {
				String initializerName = nameProvider.genSym();
				StringBuilder initializer = new StringBuilder();
				initializer.append(elementClassName + " " + initializerName + "[]");
				initializer.append(" = { ");
				for (Iterator<PrimitiveElement> iter = object.getFinitePart().iterator(); iter.hasNext(); ) {
					PrimitiveElement elt = iter.next();
					initializer.append(getRepresentation(elt));
					if (iter.hasNext())
						initializer.append(", ");
				}
				initializer.append(" }");
				initFunction.addStatement(initializer.toString());
				
				initFunction.addStatement(variableName + ".setFinitePart(" + initializerName + ")");
			}
			if (object.getNonFinitePart() != null && ! object.getNonFinitePart().isEmpty()) {
				String initializerName = nameProvider.genSym();
				StringBuilder initializer = new StringBuilder();
				initializer.append(elementClassName + " " + initializerName + "[]");
				initializer.append(" = { ");
				for (Iterator<PrimitiveElement> iter = object.getNonFinitePart().iterator(); iter.hasNext(); ) {
					PrimitiveElement elt = iter.next();
					initializer.append(getRepresentation(elt));
					if (iter.hasNext())
						initializer.append(", ");
				}
				initializer.append(" }");
				initFunction.addStatement(initializer.toString());
				initFunction.addStatement(variableName + ".setInfinitePart(" + initializerName + ")");
			}

			enclosingClass.addMemberVariable(variable);
			enclosingClass.addImportedPackage(nameProvider.getJavaClass(object).getName());
			return variableName;
		}
		@Override
		public String caseIntegerElement(IntegerElement object) {
			String variableName = nameProvider.getVariableName(object);
			MemberVariable variable = new MemberVariable(variableName, "Integer");
			variable.addModifier(modifiers);
			variable.setInitializer(object.getValue().toString());
			enclosingClass.addMemberVariable(variable);
			return variableName;
		}
		@Override
		public String caseRealElement(RealElement object) {
			String variableName = nameProvider.getVariableName(object);
			MemberVariable variable = new MemberVariable(variableName, "Float");
			variable.addModifier(modifiers);
			variable.setInitializer(object.getValue().toString());
			enclosingClass.addMemberVariable(variable);
			return variableName;
		}

	}
	
	private String generateClockCode(Clock clock, String modifiers, ClassDefinition enclosingClass,
			MemberFunction initFunction, List<String> otherGeneratedClocks,
			EqualitySolver<String> equalityRegister) {
		String variableName = nameProvider.getVariableName(clock);
		String clockClassName = nameProvider.getClassName(clock);
		String clockPackageName = nameProvider.getPackageName(clock);
		Type type = clock.getType();
		if (type != null && type instanceof DenseClockType) {
			MemberVariable variable = new MemberVariable(variableName, clockClassName);
			variable.addModifier(modifiers);
			variable.setInitializer("new " + clockClassName + "(\"" + clock.getName() + "\")");
			enclosingClass.addMemberVariable(variable);
			enclosingClass.addImportedPackage(clockPackageName + "." + clockClassName);
			return variableName;
		}
		else {
			generateDiscreteClockGetter(nameProvider.getVariableGetterName(clock), variableName,
						clock.getName(), clockClassName, clockPackageName, "public",
						enclosingClass, initFunction);
			if (clock.getTickingEvent() == null){
			return generateDiscreteClockCode(variableName, clock.getName(),
						clockClassName, clockPackageName, modifiers, enclosingClass,
						initFunction, otherGeneratedClocks, equalityRegister, null);
			}else{
				return generateDiscreteClockCode(variableName, clock.getName(),
						clockClassName, clockPackageName, modifiers, enclosingClass,
						initFunction, otherGeneratedClocks, equalityRegister, clock.getTickingEvent().getReferencedObjectRefs());
			}
		}
	}

	private String generateDiscreteClockCode(String variableName, String clockName, String clockClassName, String clockPackageName,
			String modifiers, ClassDefinition enclosingClass,
			MemberFunction initFunction, List<String> otherGeneratedClocks,
			EqualitySolver<String> equalityRegister, EList<EObject> referencedObjects) {
		MemberVariable variable = new MemberVariable(variableName, clockClassName);
		variable.addModifier(modifiers);
		variable.setInitializer("null");
		enclosingClass.addMemberVariable(variable);
		enclosingClass.addImportedPackage(clockPackageName + "." + clockClassName);

		IfStatement ifNotExists = new IfStatement(variableName + " == null");
		ifNotExists.addThenStatement(variableName + " = new " + clockClassName + "(\"" + clockName + "\")");
		initFunction.addStatement(ifNotExists);
		initFunction.addStatement(variableName + ".setDense(false)");
		initFunction.addStatement(variableName + ".setParent(this)");
		
		
		if (referencedObjects != null){
			for (EObject eObject : referencedObjects) {
				if (! eObject.eIsProxy()){
					initFunction.addStatement(variableName + ".linkedElements.add(\""+QualifiedNameBuilder.buildQualifiedName(eObject, "::")
					+ "\")");
				}
			}
		}
		
		if (equalityRegister == null || equalityRegister.getEqualityClass(variableName) == null) {
			initFunction.addStatement(variableName + ".bddVariableNumber = "
					+ "BDDHelper.newBDDVariableNumber()");
		}
		else {
			Set<String> equiClass = equalityRegister.getEqualityClass(variableName);
			boolean foundEquivalentClock = false;
			for (String other : equiClass) {
				if (other.compareTo(variableName) != 0 && otherGeneratedClocks.contains(other)) {
					initFunction.addStatement(variableName + ".bddVariableNumber = "
							+ other + ".bddVariableNumber");
					foundEquivalentClock = true;
					break;
				}
			}
			if ( ! foundEquivalentClock ) {
				initFunction.addStatement(variableName + ".bddVariableNumber = "
						+ "BDDHelper.newBDDVariableNumber()");
			}
		}
		enclosingClass.addImportedPackage(BDDHelper.class.getName());

		return variableName;
	}
	
	private String generateDiscreteClockGetter(String getterName, String variableName, String clockName, String clockClassName, String clockPackageName,
			String modifiers, ClassDefinition enclosingClass,
			MemberFunction initFunction) {
		IfStatement ifNotExists = new IfStatement(variableName + " == null");
		ifNotExists.addThenStatement(variableName + " = new " + clockClassName + "(\"" + clockName + "\")");
		MemberFunction getterFunction = new MemberFunction(getterName, clockClassName);
		getterFunction.addModifier("public");
		getterFunction.addStatement(ifNotExists);
		getterFunction.addStatement("return " + variableName);
		enclosingClass.addMemberFunction(getterFunction);

		return getterName;
	}

	/**
	 * Generates in the enclosing class the code that declares the element as a member field of the
	 * class, and if the initialize flag is true also appends to the init() function of the enclosing
	 * class the code that initializes the field.
	 * @param element
	 * @param modifiers a string with space separated modifiers such as public, private, etc.
	 * @param enclosingClass
	 * @param initialize
	 * @param parentPath 
	 * @return
	 */
	private String generateElementCode(NamedElement element, String modifiers, ClassDefinition enclosingClass,
			boolean initialize) {
		if (element.eClass().getEPackage() == BasicTypePackage.eINSTANCE) {
			return new ElementCodeGenerator(enclosingClass, modifiers).doSwitch(element);
		}
		else {
			String variableName = nameProvider.getVariableName(element);
			String elementClassName = nameProvider.getClassName(element);
			String elementPackageName = nameProvider.getPackageName(element);
			MemberVariable variable = new MemberVariable(variableName, elementClassName);
			variable.addModifier(modifiers);
			variable.setInitializer("new " + elementClassName + "(\"" + element.getName() + "\")");
			enclosingClass.addMemberVariable(variable);
			enclosingClass.addImportedPackage(elementPackageName + "." + elementClassName);
			if (initialize) {
				MemberFunction initFunction = enclosingClass.getMemberFunction("init");
				if (initFunction != null) {
					if (element instanceof Clock) {
						Type type = ((Clock)element).getType();
						if (type != null && type instanceof DenseClockType) {
							initFunction.addStatement(variableName + ".setDense(true)");
						}
						else {
							initFunction.addStatement(variableName + ".setDense(false)");
						}
					}
					initFunction.addStatement(variableName + ".setParent(this)");
				}
			}
			return variableName;
		}
	}
		
	private String getRepresentation(PrimitiveElement element) {
		if (element instanceof IntegerElement) {
			return ((IntegerElement) element).getValue().toString();
		}
		else if (element instanceof BooleanElement) {
			return ((BooleanElement) element).getValue().toString();
		}
		else if (element instanceof RealElement) {
			return ((RealElement) element).getValue().toString();
		}
		else if (element instanceof StringElement) {
			return "\"" + ((StringElement) element).getValue() + "\"";
		}
		else if (element instanceof IntegerVariableRef) {
			return nameProvider.getVariableName( ((IntegerVariableRef) element).getReferencedVar());
		}
		else if (element instanceof IntegerRef) {
			return ((IntegerRef) element).getIntegerElem().getValue().toString();
		}
		else if (element instanceof BooleanVariableRef) {
			return nameProvider.getVariableName( ((BooleanVariableRef) element).getReferencedVar() );
		}
		else if (element instanceof BooleanRef) {
			return ((BooleanRef) element).getReferencedBool().getValue().toString();
		}
		else if (element instanceof NumberSeqVariableRef) {
			return nameProvider.getVariableName( ((NumberSeqVariableRef) element).getReferencedVar() );
		}
		else
			return element.toString();
	}

	private String generateClassicalExpression(ClassicalExpression expression, NamedElement context,
			ClassDefinition enclosingClass) {
		ClassicalExpressionCompiler generator = new ClassicalExpressionCompiler(parameters, pluginHelper, nameProvider);
		String fctName = generator.generateClassicalExpression(expression, context, enclosingClass);
		return fctName;
	}
	
	private String generateCrossReferenceAccessCode(NamedElement context, NamedElement source,
				NamedElement target, ClockConstraintSystem targetRoot,
				ClassDefinition enclosingClass) {
		String code = "this";
		NamedElement current = context;
		// ascend in the block hierarchy until the root model object.
		while (current.eContainer() != null) {
			NamedElement container = (NamedElement) current.eContainer();
			String typeName = null;
			if (container instanceof Block && ! (container instanceof ClockConstraintSystem)) {
				typeName = nameProvider.getPackageName(container) + "." + nameProvider.getClassName(container);
			}
			else {
				typeName = nameProvider.getClassName(container);
				enclosingClass.addImportedPackage(nameProvider.getPackageName(container) + "." + typeName);
			}
			code = "((" + typeName + ")(" + code + ").getParent())";
			current = container;
		}
		// then descend to the target element.
		String code2 = "";
		current = target;
		while (current != null) {
			code2 = "." + nameProvider.getVariableGetterName(current) + "()" + code2;
			current = (NamedElement) current.eContainer();
		}
		code = code + code2;
		return code;
	}
	
	private String generateAccessCode(NamedElement context, NamedElement source, NamedElement target, ClassDefinition enclosingClass) {
		if (context == null) {
			return null;
		}
		else if (source.eResource() != target.eResource()) {
			// Source and target are in two different resources. These resources can contain either
			// CCSL models or CCSL libraries. Libraries are not generated per-se.
			EObject targetRoot = EcoreUtil.getRootContainer(target, true);
			if (targetRoot instanceof Library) {
				if (target instanceof PrimitiveElement) {
					return generateElementCode(target, "private", enclosingClass, true);
				}
				else {
					// TODO SequenceElement and RecordElement.
				}
			}
			else if (targetRoot instanceof ClockConstraintSystem) {
				return generateCrossReferenceAccessCode(context, source, target, (ClockConstraintSystem) targetRoot, enclosingClass);
			}
		}
		else if (context instanceof Block) {
			if (target.eContainer() == context) {
				if (target instanceof Expression) {
					return nameProvider.getClockName((Expression) target);
				}
				return nameProvider.getVariableName(target);
			} else {
				// Blocks are linked with the 'parent' reference. Cast the object returned by the
				// getParent() function with the right block type.
				Block upperBlock = (Block) context.eContainer();
				String upperBlockType = nameProvider.getClassName(upperBlock);
				return "((" + upperBlockType + ")getParent())." + generateAccessCode((NamedElement) context.eContainer(), source, target, enclosingClass);
			}
		}
		else if (context instanceof UserRelationDefinition || context instanceof UserExpressionDefinition) {
			if (target instanceof Expression) {
				return nameProvider.getClockName((Expression) target);
			}
			return nameProvider.getVariableGetterName(target) + "()";
		}
		return null;
	}

	private String generateExpressionCode(NamedElement context, Expression expression,
			boolean useParentExpressionClock,
			IProject project, ClassDefinition enclosingClass) throws CoreException {

		String variableName = nameProvider.getVariableName(expression);
		String clockName;
		String exprClassName = nameProvider.getClassName(expression);
		String exprPackageName = nameProvider.getPackageName(expression);

		ExpressionDeclaration declaration = expression.getType();
		if ( ! (declaration instanceof KernelExpressionDeclaration)) {
			if (parameters.useRuntimeFilterBy
					&& declaration.getName().compareTo(CompilerParameters.FILTERBYNAME) == 0) {
				// DO nothing: do not schedule the definition for compilation.
			}
			else {
				ExpressionDefinition definition = unfoldModel.getUsedExpressionDefinition(declaration);
				if (definition != null) {
					usedExpressionDefinitions.add(definition);
				}
			}
		}
		List<String> arguments = new ArrayList<String>();
		if (useParentExpressionClock) {
			clockName = "getExpressionClock()";
		}
		else {
			clockName = nameProvider.getClockName(expression);
		}
		arguments.add(clockName);
		List<AbstractEntity> parameters;
		if (declaration instanceof KernelExpressionDeclaration) {
			parameters = getKernelExpressionParameters((KernelExpressionDeclaration) declaration);
		} else {
			parameters = declaration.getParameters();
		}
		for (AbstractEntity parameter : parameters) {
			if (context instanceof UserRelationDefinition || context instanceof UserExpressionDefinition) {
				BindableEntity value = resolveAbstract(parameter, expression.getBindings());
				if (value instanceof Expression) {
					if (value == expression) {
						arguments.add(clockName);
					}
					else {
						arguments.add(nameProvider.getClockName((Expression) value));
					}
				}
				else if (value instanceof ClassicalExpression) {
					arguments.add(nameProvider.getProviderName((ClassicalExpression) value));
				}
				else {
					arguments.add(nameProvider.getVariableName(value));
				}
			}
			else {
				NamedElement modelObject = resolveAbstract(parameter, expression.getBindings());
				arguments.add(generateAccessCode(context, expression, modelObject, enclosingClass));
			}
		}

		MemberVariable exprVariable = new MemberVariable(variableName, exprClassName);
		exprVariable.addModifier("public");
		enclosingClass.addMemberVariable(exprVariable);
		
		MemberFunction initFunction = enclosingClass.getMemberFunction("init");
		
		String initializer = variableName + " = new " + exprClassName + "(";
		for (Iterator<String> iter = arguments.iterator(); iter.hasNext();) {
			initializer += iter.next();
			if (iter.hasNext())
				initializer += ", ";
		}
		initializer += ")";
		initFunction.addStatement(initializer);
		
		initFunction.addStatement(variableName + ".setName(\"" + expression.getName() + "\")");
		initFunction.addStatement(variableName + ".setParent(this)");

		enclosingClass.addImportedPackage(exprPackageName + "." + exprClassName);
		return variableName;
	}
	
	private class InlineConcatenationGenerator {
		private Expression expression;
		private ClassDefinition classDefinition;
		private MemberFunction initFunction;
		
		private String concatStateVariable;
		private String concatClockName;
		
		public InlineConcatenationGenerator(Expression expression, ClassDefinition classDefinition, MemberFunction initFunction) {
			this.expression = expression;
			this.classDefinition = classDefinition;
			this.initFunction = initFunction;
		}

		public String generate() {
			classDefinition.addImportedPackage(RuntimeConcatenation.class.getName());
			classDefinition.addImportedPackage(RuntimeConcatenation.class.getName() + ".ConcatState");
			
			concatStateVariable = nameProvider.getVariableName(expression) + "ConcatState";
			String concatStateVariableType = ConcatState.class.getSimpleName();
			
			MemberVariable variable = new MemberVariable(concatStateVariable, concatStateVariableType);
			variable.addModifier("public");
			classDefinition.addMemberVariable(variable);
			
			classDefinition.addImportedPackage(RuntimeClock.class.getName());
			concatClockName = nameProvider.getClockName(expression);
			MemberVariable clockVar = new MemberVariable(concatClockName, RuntimeClock.class.getSimpleName());
			clockVar.addModifier("public");
			classDefinition.addMemberVariable(clockVar);
			clockVar.setInitializer("new " + RuntimeClock.class.getSimpleName() + "(\"" + concatClockName + "\")");
			initFunction.addStatement(concatClockName + ".setDense(false)");
			initFunction.addStatement(concatClockName + ".setParent(this)");
			initFunction.addStatement(concatClockName + ".bddVariableNumber = BDDHelper.newBDDVariableNumber()");
			return concatStateVariable;
		}
	}
		
	@SuppressWarnings("unused")
	private List<NamedElement> computeStartOrder(NamedElement root, NamedElement context) {
		if (root instanceof Clock) {
			return Collections.emptyList();
		}
		if (root instanceof Expression) {
			
		}
		return Collections.emptyList();
	}
	
	private void generateArgumentsPrivateMembers(RelationDefinition definition, ClassDefinition enclosingClass, List<AbstractEntity> nonClockParameters) {
		List<AbstractEntity> parameters = definition.getDeclaration().getParameters();
		generateArgumentsPrivateMembers(parameters, enclosingClass, nonClockParameters);
	}


	private void generateArgumentsPrivateMembers(ExpressionDefinition definition, ClassDefinition enclosingClass, List<AbstractEntity> nonClockParameters) {
		List<AbstractEntity> parameters = definition.getDeclaration().getParameters();
		generateArgumentsPrivateMembers(parameters, enclosingClass, nonClockParameters);
	}
	
	private void generateArgumentsPrivateMembers(List<AbstractEntity> parameters, ClassDefinition enclosingClass,
												 List<AbstractEntity> nonClockParameters) {
		for (AbstractEntity parameter : parameters) {
			Type parameterType = parameter.getType();
			@SuppressWarnings("rawtypes")
			Class parameterClass = nameProvider.getJavaClass(parameterType);
			String paramClassName = nameProvider.getClassName(parameter);
			MemberVariable variable = new MemberVariable(nameProvider.getVariableName(parameter),
							paramClassName);
			variable.addModifier("private");
			enclosingClass.addImportedPackage(parameterClass.getName());
			enclosingClass.addMemberVariable(variable);
			if (nonClockParameters.contains(parameter)) {
				String providerVar = nameProvider.getProviderName(parameter);
				MemberVariable pvar = new MemberVariable(providerVar,
						ValueProvider.class.getSimpleName() + "<" + paramClassName + ">");
				pvar.addModifier("private");
				enclosingClass.addImportedPackage(ValueProvider.class.getName());
				enclosingClass.addMemberVariable(pvar);
			}
		}
	}
	
	private void generateExpressionDefinitionConstructor(ExpressionDefinition definition, String definitionClassName,
				ClassDefinition classDefinition, List<AbstractEntity> nonClockParameters) {
		List<AbstractEntity> parameters = definition.getDeclaration().getParameters();
		MemberFunction constructor = new MemberFunction(definitionClassName);
		constructor.setConstructor(true);
		constructor.addModifier("public");
		
		constructor.addParameter(nameProvider.implicitClockClassName, "implicitClock");
		for (Iterator<AbstractEntity> iter = parameters.iterator(); iter.hasNext();) {
			AbstractEntity parameter = iter.next();
			Type parameterType = parameter.getType();
			@SuppressWarnings("rawtypes")
			Class parameterClass = nameProvider.getJavaClass(parameterType);
			String paramClassName = nameProvider.getClassName(parameter);
			
			constructor.addParameter(paramClassName, nameProvider.getVariableName(parameter));
			classDefinition.addImportedPackage(parameterClass.getName());
		}
		constructor.addStatement("super(implicitClock)");
		for (AbstractEntity parameter : parameters) {
			constructor.addStatement("this."+ nameProvider.getVariableName(parameter) + " = "
					+ nameProvider.getVariableName(parameter));
		}
		constructor.addStatement("init()");
		classDefinition.addMemberFunction(constructor);
		
		if ( ! nonClockParameters.isEmpty()) {
			constructor = new MemberFunction(definitionClassName);
			constructor.setConstructor(true);
			constructor.addModifier("public");
			
			constructor.addParameter(nameProvider.implicitClockClassName, "implicitClock");
			for (Iterator<AbstractEntity> iter = parameters.iterator(); iter.hasNext();) {
				AbstractEntity parameter = iter.next();
				Type parameterType = parameter.getType();
				@SuppressWarnings("rawtypes")
				Class parameterClass = nameProvider.getJavaClass(parameterType);
				String paramClassName = nameProvider.getClassName(parameter);
				if (nonClockParameters.contains(parameter)) {
					constructor.addParameter(ValueProvider.class.getSimpleName() + "<" + paramClassName + ">",
							nameProvider.getVariableName(parameter));
				}
				else {
					constructor.addParameter(paramClassName, nameProvider.getVariableName(parameter));
				}
				classDefinition.addImportedPackage(parameterClass.getName());
				classDefinition.addImportedPackage(ValueProvider.class.getName());
			}
			constructor.addStatement("super(implicitClock)");
			for (AbstractEntity parameter : parameters) {
				String var;
				if (nonClockParameters.contains(parameter)) {
					var = nameProvider.getProviderName(parameter);
				}
				else {
					var = nameProvider.getVariableName(parameter);
				}
				constructor.addStatement("this." + var + " = " + nameProvider.getVariableName(parameter));
			}
			constructor.addStatement("init()");
			classDefinition.addMemberFunction(constructor);
		}
	}
	
	
	private List<ConcreteEntity> getBuildOrder(List<ConcreteEntity> entities) {
		final HashMap<ConcreteEntity, Integer> ranks = new HashMap<ConcreteEntity, Integer>();
		for (ConcreteEntity entity : entities) {
			ranks.put(entity, 0);
		}
		for (ConcreteEntity entity : entities) {
			if (entity instanceof Expression || entity instanceof Relation) {
				setDepths(entity, ranks);
			}
		}
		ArrayList<ConcreteEntity> result = new ArrayList<>(entities);
		Collections.sort(result, new Comparator<ConcreteEntity>() {
			@Override
			public int compare(ConcreteEntity o1, ConcreteEntity o2) {
				return ranks.get(o2) - ranks.get(o1);
			}
		});
		return result;
	}
	
	private void setDepths(ConcreteEntity root, HashMap<ConcreteEntity, Integer> depths) {
		int parentDepth = depths.get(root);
		List<AbstractEntity> parameters = Collections.emptyList();
		List<Binding> bindings = Collections.emptyList();
		if (root instanceof Expression) {
			ExpressionDeclaration declaration = ((Expression) root).getType();
			if (declaration instanceof KernelExpressionDeclaration) {
				parameters = getKernelExpressionParameters((KernelExpressionDeclaration) declaration);
			} else {
				parameters = declaration.getParameters();
			}
			bindings = ((Expression) root).getBindings();
		}
		else if (root instanceof Relation) {
			RelationDeclaration declaration = ((Relation) root).getType();
			if (declaration instanceof KernelRelationDeclaration) {
				parameters = new ArrayList<AbstractEntity>();
				parameters.add( ((KernelRelationDeclaration) declaration).getLeftEntity() );
				parameters.add( ((KernelRelationDeclaration) declaration).getRightEntity() );
			}
			else {
				parameters = declaration.getParameters();
			}
			bindings = ((Relation) root).getBindings();
		}
		for (AbstractEntity parameter : parameters) {
			BindableEntity value = resolveAbstract(parameter, bindings);
			if (value instanceof Expression) {
				int currentDepth = depths.get(value);
				int newDepth = (parentDepth + 1 > currentDepth ? parentDepth + 1 : currentDepth);
				depths.put((ConcreteEntity) value, newDepth);
			}
		}
	}

	private List<Expression> getExpressionTree(Relation root, boolean includeConcatLeft, boolean includeConcatRight) {
		List<Expression> result = new ArrayList<Expression>();
		List<AbstractEntity> parameters = new ArrayList<AbstractEntity>();
		RelationDeclaration declaration = root.getType();
		if (declaration instanceof KernelRelationDeclaration) {
			parameters.add(((KernelRelationDeclaration) declaration).getLeftEntity());
			parameters.add(((KernelRelationDeclaration) declaration).getRightEntity());
		}
		else {
			parameters.addAll(declaration.getParameters());
		}
		for (AbstractEntity parameter : parameters) {
			BindableEntity value = resolveAbstract(parameter, root.getBindings());
			if (value instanceof Expression) {
				result.add((Expression) value);
				result.addAll(getExpressionTree((Expression) value, includeConcatLeft, includeConcatRight));
			}
		}
		return result;
	}
	
	private List<Expression> getExpressionTree(Expression root,
			boolean includeConcatLeft, boolean includeConcatRight) {
		List<Expression> result = new ArrayList<Expression>();
		ExpressionDeclaration declaration = root.getType();
		List<AbstractEntity> parameters;
		if (declaration instanceof Concatenation) {
			parameters = new ArrayList<AbstractEntity>();
			if (includeConcatLeft)
				parameters.add(((Concatenation) declaration).getLeftClock());
			if (includeConcatRight)
				parameters.add(((Concatenation) declaration).getRightClock());
		}
		else if (declaration instanceof KernelExpressionDeclaration) {
			parameters = getKernelExpressionParameters((KernelExpressionDeclaration) declaration);
		}
		else {
			parameters = declaration.getParameters();
		}
		for (AbstractEntity parameter : parameters) {
			BindableEntity value = resolveAbstract(parameter, root.getBindings());
			if (value instanceof Expression) {
				result.add((Expression) value);
				result.addAll(getExpressionTree((Expression) value, includeConcatLeft, includeConcatRight));
			}
		}
		return result;
	}
	
	private String compileConditionalExpressionDefinition(ConditionalExpressionDefinition definition, IProject project)
			throws CoreException, CompilerException {

		RecursiveDefinitionChecker checker = new RecursiveDefinitionChecker(definition);
		// Inline Concatenation if the definition is recursive and the "recursion path" contains a Concatenation
		Expression recursiveCall = null;
		if (checker.isTailRecursive()) {
			recursiveCall = checker.getRecursiveCall();
		}
		String definitionClassName = nameProvider.getClassName(definition);
		String definitionClassPackage = nameProvider.getPackageName(definition);
		
		ClassDefinition classDefinition = new ClassDefinition(definitionClassName, definitionClassPackage);
		classDefinition.setProject(project);
		classDefinition.setParentClassName(AbstractRuntimeExpression.class.getSimpleName());
		classDefinition.addInterface(IRuntimeContainer.class.getSimpleName());
		classDefinition.addImportedPackage(IRuntimeContainer.class.getName());

		List<String> constraintNames = new ArrayList<String>();

		MemberFunction initFunction = new MemberFunction("init", "void");
		initFunction.addModifier("protected");
		classDefinition.addMemberFunction(initFunction);

		classDefinition.addImportedPackage(AbstractRuntimeExpression.class.getName());
		
		List<AbstractEntity> nonClockParameters = getNonClockParameters(definition);
		
		// generate private members for each argument
		generateArgumentsPrivateMembers(definition, classDefinition, nonClockParameters);
		// Generate a constructor taking as arguments the parameters of the
		// declaration, plus (at the beginning) the implicit clock. 
		generateExpressionDefinitionConstructor(definition, definitionClassName, classDefinition, nonClockParameters);
		classDefinition.addImportedPackage(nameProvider.clockClassPackage + "." + nameProvider.clockClassName);

		HashMap<ClassicalExpression, String> classicalExprNames = new HashMap<>();
		HashMap<ClassicalExpression, String> classicalExprProviderNames = new HashMap<>();
		HashMap<Expression, String> expressionNames = new HashMap<>();
		
		for (ClassicalExpression expression : definition.getClassicalExpressions()) {
			ClassicalExpressionCompiler generator = new ClassicalExpressionCompiler(parameters, pluginHelper, nameProvider);
			String fctName = generator.generateClassicalExpression(expression, definition, classDefinition);
			if ( checker.isTailRecursive() && checker.getRecursionArguments().contains(expression) ) {
				// Add a variable 
				MemberVariable variable = new MemberVariable(nameProvider.getVariableName(expression),
									generator.getReturnTypeName());
				variable.addModifier("private");
				classDefinition.addMemberVariable(variable);
			}
			classicalExprNames.put(expression, fctName);
			String providerTypeName = ValueProvider.class.getSimpleName() + "<" + generator.getReturnTypeName() + ">";
			String providerName = nameProvider.getProviderName(expression);
			MemberVariable providerVariable = new MemberVariable(providerName, providerTypeName);
			providerVariable.addModifier("private");

			String initializer = " new " + providerTypeName + "() {\n";
			initializer += "      public " + generator.getReturnTypeName() + " getValue() {\n";
			initializer += "        return " + fctName + "();\n";
			initializer += "      }\n" + "    }";
			providerVariable.setInitializer(initializer);
			
			classDefinition.addMemberVariable(providerVariable);
			classDefinition.addImportedPackage(ValueProvider.class.getName());
			classicalExprProviderNames.put(expression, providerName);
		}

		for (ConcreteEntity entity : definition.getConcreteEntities()) {
			if (entity instanceof Relation) {
				String relationVarName = generateRelationCode(definition, (Relation) entity,
						classDefinition, project);
				constraintNames.add(relationVarName);
			} else if (entity instanceof Expression &&
					( (recursiveCall == null) || (recursiveCall != null && entity != recursiveCall) )) {
				String clockVariableName = nameProvider.getClockName((Expression) entity);
				String clockName = entity.getName();
				
				generateDiscreteClockCode(clockVariableName, clockName,
						nameProvider.implicitClockClassName, nameProvider.implicitClockPackage,
						"public", classDefinition, initFunction, null, null, null);

				String expressionVarName;
				expressionVarName = generateExpressionCode(definition, (Expression) entity,
						false, project, classDefinition);
				constraintNames.add(expressionVarName);
				if (expressionNames.get(expressionVarName) != null) {
					throw new CompilerException("Duplicate expression name \"" + entity.getName() + "\" in Conditional Expression Definition " + definition.getName());
				}
				expressionNames.put((Expression) entity, expressionVarName);
								
			} else if (entity instanceof Element) {
				@SuppressWarnings("unused")
				String elementVarName = generateElementCode(entity, "private", classDefinition, true);
			}
		}
		// Dummy instance to use in the maps for the default case of the switch.
		ExprCase defaultCase = ClockExpressionAndRelationFactory.eINSTANCE.createExprCase();
		
		HashMap<ExprCase, String> switchTestNames = new HashMap<ExprCase, String>();
		HashMap<ExprCase, String> switchExprNames = new HashMap<ExprCase, String>();
		// Each branch of the conditional has an index starting at 1. 0 is used for the default case.
		HashMap<ExprCase, Integer> switchTestIndices = new HashMap<ExprCase, Integer>();
		int switchTestCounter = 0; // use to generate indices.

		/*
		 * the following variable is used to represent the last selected branch of the conditional. Each
		 * branch has an index (stored in the map switchTestIndices) and this variable is set to the corresponding
		 * value when conditional tests are evaluated. This allows to check when a different branch w.r.t to the
		 * previous execution is selected : associated relations and expressions are started, ie. call start() on them.  
		 */
		MemberVariable selectionVar = new MemberVariable("selectedCase", "int");
		selectionVar.addModifier("private");
		selectionVar.setInitializer("-1");  // conditional cases have index starting from 1. 
		classDefinition.addMemberVariable(selectionVar);

		for (ExprCase swCase : definition.getExprCases()) {
			BooleanExpression test = swCase.getCondition();
			Expression expression = swCase.getExpression();
			String testFunctionName = generateClassicalExpression(test, definition, classDefinition);
			switchTestNames.put(swCase, testFunctionName);
			switchTestCounter += 1;
			switchTestIndices.put(swCase, switchTestCounter);
			if (checker.isRecursive() && checker.getRecursionPath().contains(expression)
					&& expression.getType() instanceof Concatenation) {
				InlineConcatenationGenerator generator = new InlineConcatenationGenerator(expression, classDefinition, initFunction);
				String stateName = generator.generate();
				switchExprNames.put(swCase, stateName);
			}
			else if ((recursiveCall == null) || (recursiveCall != null && expression != recursiveCall)) {
				String exprFunctionName = generateExpressionCode(definition, expression, false,
								project, classDefinition);
				switchExprNames.put(swCase, exprFunctionName);
				expressionNames.put(expression, exprFunctionName);
				
				String clockVariableName = nameProvider.getClockName(expression);
				String clockName = expression.getName();
				
				generateDiscreteClockCode(clockVariableName, clockName,
						nameProvider.implicitClockClassName, nameProvider.implicitClockPackage,
						"public", classDefinition, initFunction, null, null, null);
				
			}
		}
		if ( definition.getDefaultExpression() != null) {
			Expression expression = definition.getDefaultExpression();
			if (checker.isRecursive() && checker.getRecursionPath().contains(expression)
					&& expression.getType() instanceof Concatenation) {
				InlineConcatenationGenerator generator = new InlineConcatenationGenerator(expression, classDefinition, initFunction);
				String stateName = generator.generate();
				switchExprNames.put(defaultCase, stateName);
			}
			else if ((recursiveCall == null) ||
						(recursiveCall != null && recursiveCall != definition.getDefaultExpression())) {
				String exprFunctionName = generateExpressionCode(definition, definition.getDefaultExpression(),
						false, project, classDefinition);
				switchExprNames.put(defaultCase, exprFunctionName);
				expressionNames.put(definition.getDefaultExpression(), exprFunctionName);
				
				String clockVariableName = nameProvider.getClockName(expression);
				String clockName = expression.getName();
				
				generateDiscreteClockCode(clockVariableName, clockName,
						nameProvider.implicitClockClassName, nameProvider.implicitClockPackage,
						"public", classDefinition, initFunction, null, null, null);

			}
		}

		generateConditionalStartFunction(definition, classDefinition, checker, switchTestNames, switchExprNames,
				switchTestIndices, defaultCase, expressionNames, nonClockParameters);
		
		generateConditionalSemanticFunction(definition, classDefinition, checker,
				switchTestNames, switchExprNames, switchTestIndices, defaultCase, expressionNames);

		// The following, if uncommented, breaks the generated FilterBy !?!
//		generateConditionalDeathSemanticFunction(definition, classDefinition, checker,
//				switchTestNames, switchExprNames, defaultCase, expressionNames, classicalExprNames);
		
		generateConditionalUpdateFunction(definition, classDefinition, checker,
				switchTestNames, switchExprNames, defaultCase, expressionNames, classicalExprNames);
		
		classDefinition.generate(pluginHelper);
		return "";
	}

	private void generateConditionalStartFunction(ConditionalExpressionDefinition definition, ClassDefinition classDefinition,
			RecursiveDefinitionChecker checker, HashMap<ExprCase, String> switchTestNames,
			HashMap<ExprCase, String> switchExprNames, HashMap<ExprCase, Integer> switchTestIndices, ExprCase defaultCase,
			HashMap<Expression, String> expressionNames, List<AbstractEntity> nonClockParameters) {
		/*
		 * start() function generation for a Conditional Expression Definition.
		 */
		MemberFunction startFunction = new MemberFunction("start", "void");
		startFunction.setOverrides(true);
		startFunction.addModifier("public");
		startFunction.addException("SimulationException");
		startFunction.addParameter("AbstractSemanticHelper", "helper");
		classDefinition.addMemberFunction(startFunction);
		{
			IfStatement st = new IfStatement("! canCallStart()");
			st.addThenStatement(new SimpleStatement("return"));
			startFunction.addStatement(st);
		}
		startFunction.addStatement("super.start(helper)");
		// Initialize private variables that depends possibly on the evaluation of a classical
		// expression to get their value.
		for (AbstractEntity parameter : nonClockParameters) {
			IfStatement providerTest = new IfStatement();
			providerTest.setTestCode(nameProvider.getProviderName(parameter) + " != null");
			providerTest.addThenStatement(nameProvider.getVariableName(parameter) + " = "
					+ nameProvider.getProviderName(parameter) + ".getValue()");
			startFunction.addStatement(providerTest);
		}
		// Iterate over all cases, and generate code using a series of if-then-else(if) statements, the
		// default case being the else branch of the last if-statement.
		// Create an instance of if-statement, and replace it by a new one while there are still a case
		// to handle in the definition.
		IfStatement caseIfStatement = new IfStatement();
		startFunction.addStatement(caseIfStatement);
		for (Iterator<ExprCase> iter = definition.getExprCases().iterator(); iter.hasNext();) {
			ExprCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			String exprFunctionName = switchExprNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			caseIfStatement.addThenStatement("selectedCase = " + switchTestIndices.get(swCase));
			if (swCase.getExpression().getType() instanceof Concatenation && checker.isTailRecursive()
					&& checker.getRecursionPath().contains(swCase.getExpression())) {
				caseIfStatement.addThenStatement(exprFunctionName + " = " +
						ConcatState.class.getSimpleName() + "." + ConcatState.FOLLOWLEFT.name());
			} else {
				caseIfStatement.addThenStatement(exprFunctionName + ".start(helper)");
			}
			List<Expression> subExpressions = getExpressionTree(swCase.getExpression(), true, false);
			for (Expression expr : subExpressions) {
				String exprName = expressionNames.get(expr);
				caseIfStatement.addThenStatement(exprName + ".start(helper)");
			}
			if (iter.hasNext()) {
				// Create a new if-statement, attach it to the current if-statement as the (unique) else clause
				// and make it the current if-statement.
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if (definition.getDefaultExpression() != null) {
			caseIfStatement.addElseStatement("selectedCase = 0");
			if (definition.getDefaultExpression().getType() instanceof Concatenation && checker.isTailRecursive()
					&& checker.getRecursionPath().contains(definition.getDefaultExpression())) {
				caseIfStatement.addElseStatement(switchExprNames.get(defaultCase) + " = " +
						ConcatState.class.getSimpleName() + "." + ConcatState.FOLLOWLEFT.name());
			} else {
				caseIfStatement.addElseStatement(switchExprNames.get(defaultCase) + ".start(helper)");
			}
			List<Expression> subExpressions = getExpressionTree(definition.getDefaultExpression(), true, false);
			for (Expression expr : subExpressions) {
				String exprName = expressionNames.get(expr);
				caseIfStatement.addElseStatement(exprName + ".start(helper)");
			}
		}
	}
	
	
	private void generateConditionalSemanticFunction(ConditionalExpressionDefinition definition, ClassDefinition classDefinition,
			RecursiveDefinitionChecker checker, HashMap<ExprCase, String> switchTestNames,
			HashMap<ExprCase, String> switchExprNames, HashMap<ExprCase, Integer> switchTestIndices, ExprCase defaultCase,
			HashMap<Expression, String> expressionNames) {
		
		MemberFunction semanticFunction = new MemberFunction("semantic", "void");
		semanticFunction.setOverrides(true);
		semanticFunction.addModifier("public");
		semanticFunction.addParameter(AbstractSemanticHelper.class.getSimpleName(), "helper");
		semanticFunction.addException(SimulationException.class.getSimpleName());
		classDefinition.addMemberFunction(semanticFunction);
		classDefinition.addImportedPackage(AbstractSemanticHelper.class.getName());
		classDefinition.addImportedPackage(SimulationException.class.getName());
		{
			IfStatement ifStatement = new IfStatement("! canCallSemantic()");
			ifStatement.addThenStatement("return");
			semanticFunction.addStatement(ifStatement);
		}
		semanticFunction.addStatement("super.semantic(helper)");
		IfStatement caseIfStatement = new IfStatement();
		semanticFunction.addStatement(caseIfStatement);

		for (Iterator<ExprCase> iter = definition.getExprCases().iterator(); iter.hasNext(); ) {
			ExprCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			String exprFunctionName = switchExprNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			IfStatement selectionChangedIf = new IfStatement("selectedCase != " + switchTestIndices.get(swCase));
			caseIfStatement.addThenStatement(selectionChangedIf);
			caseIfStatement.addThenStatement("selectedCase = " + switchTestIndices.get(swCase));
			caseIfStatement.addThenStatement("helper.registerClockEquality(" + nameProvider.getClockName(swCase.getExpression()) + ", getExpressionClock()" + ")");
			caseIfStatement.addThenStatement("helper.registerClockUse(" + nameProvider.getClockName(swCase.getExpression()) + ")");
			caseIfStatement.addThenStatement("helper.registerClockUse( getExpressionClock() )");
			if (swCase.getExpression().getType() instanceof Concatenation 
					&& checker.isTailRecursive()
					&& checker.getRecursionPath().contains(swCase.getExpression())) {
				// Equivalent of a start() of the Concatenation
				selectionChangedIf.addThenStatement(exprFunctionName + " = " +
							ConcatState.class.getSimpleName() + "." + ConcatState.FOLLOWLEFT.name());
				Expression concatExp = swCase.getExpression();
				Concatenation concatDecl = (Concatenation) concatExp.getType();
				IfStatement concatLeftIf = new IfStatement();
				concatLeftIf.setTestCode(exprFunctionName + " == " +
						ConcatState.class.getSimpleName() + "." + ConcatState.FOLLOWLEFT.name());
				caseIfStatement.addThenStatement(concatLeftIf);
				BindableEntity leftValue = resolveAbstract(concatDecl.getLeftClock(), concatExp.getBindings());
				if (leftValue instanceof Expression) {
					if (expressionNames.get(leftValue) != null) {
						selectionChangedIf.addThenStatement(expressionNames.get(leftValue) + ".start(helper)");
						concatLeftIf.addThenStatement(expressionNames.get(leftValue) + ".semantic(helper)");
					}
					concatLeftIf.addThenStatement("helper.registerClockEquality(" + nameProvider.getClockName((Expression) leftValue)
							+ ", " + nameProvider.getClockName(concatExp) + ")");		
					concatLeftIf.addThenStatement("helper.registerClockUse(" + nameProvider.getClockName((Expression) leftValue) + ")");
					concatLeftIf.addThenStatement("helper.registerClockUse(" + nameProvider.getClockName(concatExp) + ")");
				}
				else if (leftValue instanceof Clock) {
					concatLeftIf.addThenStatement("helper.registerClockEquality(" + nameProvider.getVariableName(leftValue)
							+ ", " + nameProvider.getClockName(concatExp) + ")");
					concatLeftIf.addThenStatement("helper.registerClockUse(" + nameProvider.getVariableName(leftValue) + ")");
					concatLeftIf.addThenStatement("helper.registerClockUse(" + nameProvider.getClockName(concatExp) + ")");
				}
				IfStatement concatRightIf = new IfStatement();
				concatRightIf.setTestCode(exprFunctionName + " == " + 
						ConcatState.class.getSimpleName() + "." + ConcatState.FOLLOWRIGHT.name());
				concatLeftIf.addElseStatement(concatRightIf);
				BindableEntity rightValue = resolveAbstract(concatDecl.getRightClock(), concatExp.getBindings());
				if (rightValue instanceof Expression && rightValue != checker.getRecursiveCall()) {
					if (expressionNames.get(rightValue) != null) {
						selectionChangedIf.addThenStatement(expressionNames.get(rightValue) + ".start(helper)");
						concatRightIf.addThenStatement(expressionNames.get(rightValue) + ".semantic(helper)");
					}
					concatRightIf.addThenStatement("helper.registerClockEquality(" + nameProvider.getClockName((Expression) rightValue)
								+ ", " + nameProvider.getClockName(concatExp) +")");
					concatRightIf.addThenStatement("helper.registerClockUse(" + nameProvider.getClockName((Expression) rightValue) + ")");
					concatRightIf.addThenStatement("helper.registerClockUse(" + nameProvider.getClockName(concatExp) +")");
				}
				else if (rightValue instanceof Clock) {
					concatRightIf.addThenStatement("helper.registerClockEquality(" + nameProvider.getVariableName(rightValue)
							+ ", " + nameProvider.getClockName(concatExp) + ")");
					concatRightIf.addThenStatement("helper.registerClockUse(" + nameProvider.getVariableName(rightValue) + ")");
					concatRightIf.addThenStatement("helper.registerClockUse(" + nameProvider.getClockName(concatExp) + ")");
				}
			}
			else if ( checker.isRecursive() && checker.getRecursiveCall() == swCase.getExpression()) {
				// TODO ??
			}
			else {
				selectionChangedIf.addThenStatement(exprFunctionName + ".start(helper)");
				caseIfStatement.addThenStatement(exprFunctionName + ".semantic(helper)");
				List<Expression> subExpressions = getExpressionTree(swCase.getExpression(), true, true);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					selectionChangedIf.addThenStatement(exprName + ".start(helper)");
					caseIfStatement.addThenStatement(exprName + ".semantic(helper)");
				}
			}
			if (iter.hasNext()) {
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if ( definition.getDefaultExpression() != null ) {
			IfStatement selectionChangedIf = new IfStatement("selectedCase != 0");
			selectionChangedIf.addThenStatement(switchExprNames.get(defaultCase) + ".start(helper)");
			caseIfStatement.addElseStatement(selectionChangedIf);
			caseIfStatement.addElseStatement("selectedCase = 0");
			caseIfStatement.addElseStatement("helper.registerClockEquality(" + nameProvider.getClockName(definition.getDefaultExpression())
					+ ", getExpressionClock()" + ")");
			caseIfStatement.addElseStatement("helper.registerClockUse(" + nameProvider.getClockName(definition.getDefaultExpression()) + ")");
			caseIfStatement.addElseStatement("helper.registerClockUse( getExpressionClock() )");
			caseIfStatement.addElseStatement(switchExprNames.get(defaultCase) + ".semantic(helper)");
			List<Expression> subExpressions = getExpressionTree(definition.getDefaultExpression(), true, false);
			for (Expression expr : subExpressions) {
				String exprName = expressionNames.get(expr);
				selectionChangedIf.addThenStatement(exprName + ".start(helper)");
				caseIfStatement.addElseStatement(exprName + ".semantic(helper)");
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void generateConditionalDeathSemanticFunction(ConditionalExpressionDefinition definition,
			ClassDefinition classDefinition, RecursiveDefinitionChecker checker,
			HashMap<ExprCase, String> switchTestNames, HashMap<ExprCase, String> switchExprNames, ExprCase defaultCase,
			HashMap<Expression, String> expressionNames, HashMap<ClassicalExpression, String> classicalExprNames) {
		MemberFunction deathSemFunction = new MemberFunction("deathSemantic", "void");
		deathSemFunction.addModifier("public");
		deathSemFunction.addParameter(AbstractSemanticHelper.class.getSimpleName(), "helper");
		deathSemFunction.addException(SimulationException.class.getSimpleName());
		classDefinition.addMemberFunction(deathSemFunction);
		classDefinition.addImportedPackage(AbstractSemanticHelper.class.getName());
		classDefinition.addImportedPackage(SimulationException.class.getName());
		
		deathSemFunction.addStatement("super.deathSemantic(helper)");
		IfStatement caseIfStatement = new IfStatement();
		deathSemFunction.addStatement(caseIfStatement);
		for (Iterator<ExprCase> iter = definition.getExprCases().iterator(); iter.hasNext(); ) {
			ExprCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			String exprFunctionName = switchExprNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			caseIfStatement.addThenStatement("helper.registerDeathEquality(" + nameProvider.getClockName(swCase.getExpression())
					+ ", getExpressionClock()" + ")");
			if (swCase.getExpression().getType() instanceof Concatenation 
					&& checker.isTailRecursive()
					&& checker.getRecursionPath().contains(swCase.getExpression())) {
				Expression concatExp = swCase.getExpression();
				Concatenation concatDecl = (Concatenation) concatExp.getType();
				IfStatement concatLeftIf = new IfStatement();
				concatLeftIf.setTestCode(exprFunctionName + " == " +
						ConcatState.class.getSimpleName() + "." + ConcatState.FOLLOWLEFT.name());
				caseIfStatement.addThenStatement(concatLeftIf);
				BindableEntity leftValue = resolveAbstract(concatDecl.getLeftClock(), concatExp.getBindings());
				if (leftValue instanceof Expression) {
					if (expressionNames.get(leftValue) != null) {
						concatLeftIf.addThenStatement(expressionNames.get(leftValue) + ".deathSemantic(helper)");
					}
					concatLeftIf.addThenStatement("helper.registerDeathEquality(" + nameProvider.getClockName((Expression) leftValue)
							+ ", " + nameProvider.getClockName(concatExp) + ")");		
				}
				else if (leftValue instanceof Clock) {
					concatLeftIf.addThenStatement("helper.registerDeathEquality(" + nameProvider.getVariableName(leftValue)
							+ ", " + nameProvider.getClockName(concatExp) + ")");
				}
				IfStatement concatRightIf = new IfStatement();
				concatRightIf.setTestCode(exprFunctionName + " == " + 
						ConcatState.class.getSimpleName() + "." + ConcatState.FOLLOWRIGHT.name());
				concatLeftIf.addElseStatement(concatRightIf);
				BindableEntity rightValue = resolveAbstract(concatDecl.getRightClock(), concatExp.getBindings());
				if (rightValue instanceof Expression && rightValue != checker.getRecursiveCall()) {
					if (expressionNames.get(rightValue) != null) {
						concatRightIf.addThenStatement(expressionNames.get(rightValue) + ".deathSemantic(helper)");
					}
					concatRightIf.addThenStatement("helper.registerDeathEquality(" + nameProvider.getClockName((Expression) rightValue)
								+ ", " + nameProvider.getClockName(concatExp) +")");
				}
				else if (rightValue instanceof Clock) {
					concatRightIf.addThenStatement("helper.registerDeathEquality(" + nameProvider.getVariableName(rightValue)
							+ ", " + nameProvider.getClockName(concatExp) + ")");
				}
			}
			else if ( checker.isRecursive() && checker.getRecursiveCall() == swCase.getExpression()) {
				
			}
			else {
				caseIfStatement.addThenStatement(exprFunctionName + ".deathSemantic(helper)");
				List<Expression> subExpressions = getExpressionTree(swCase.getExpression(), true, true);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".deathSemantic(helper)");
				}
			}
			if (iter.hasNext()) {
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if ( definition.getDefaultExpression() != null ) {
			caseIfStatement.addElseStatement("helper.registerDeathEquality(" + nameProvider.getClockName(definition.getDefaultExpression())
					+ ", getExpressionClock()" + ")");
			caseIfStatement.addElseStatement(switchExprNames.get(defaultCase) + ".deathSemantic(helper)");
			List<Expression> subExpressions = getExpressionTree(definition.getDefaultExpression(), true, false);
			for (Expression expr : subExpressions) {
				String exprName = expressionNames.get(expr);
				caseIfStatement.addElseStatement(exprName + ".deathSemantic(helper)");
			}
		}
	}
	
	private void generateConditionalUpdateFunction(ConditionalExpressionDefinition definition, ClassDefinition classDefinition,
			RecursiveDefinitionChecker checker, HashMap<ExprCase, String> switchTestNames,
			HashMap<ExprCase, String> switchExprNames, ExprCase defaultCase, HashMap<Expression,
			String> expressionNames, HashMap<ClassicalExpression, String> classicalExprNames) {

		MemberFunction updateFunction = new MemberFunction("update", "void");
		updateFunction.addModifier("public");
		updateFunction.addParameter(AbstractUpdateHelper.class.getSimpleName(), "helper");
		updateFunction.addException(SimulationException.class.getSimpleName());
		updateFunction.setOverrides(true);
		classDefinition.addMemberFunction(updateFunction);
		classDefinition.addImportedPackage(AbstractUpdateHelper.class.getName());
		classDefinition.addImportedPackage(SimulationException.class.getName());
		{
			IfStatement ifSt = new IfStatement("! canCallUpdate()");
			ifSt.addThenStatement("return");
			updateFunction.addStatement(ifSt);
		}
		updateFunction.addStatement("super.update(helper)");
		IfStatement caseIfStatement = new IfStatement();
		updateFunction.addStatement(caseIfStatement);
		for (Iterator<ExprCase> iter = definition.getExprCases().iterator(); iter.hasNext(); ) {
			ExprCase swCase = iter.next();
			String testFunctionName = switchTestNames.get(swCase);
			String exprFunctionName = switchExprNames.get(swCase);
			caseIfStatement.setTestCode(testFunctionName + "()");
			if (swCase.getExpression().getType() instanceof Concatenation 
					&& checker.isTailRecursive()
					&& checker.getRecursionPath().contains(swCase.getExpression())) {
				Expression concatExp = swCase.getExpression();
				Concatenation concatDecl = (Concatenation) concatExp.getType();
				IfStatement concatIfLeft = new IfStatement(exprFunctionName + " == ConcatState.FOLLOWLEFT");
				caseIfStatement.addThenStatement(concatIfLeft);
				BindableEntity leftValue = resolveAbstract(concatDecl.getLeftClock(), concatExp.getBindings());
				BindableEntity rightValue = resolveAbstract(concatDecl.getRightClock(), concatExp.getBindings());
				if (leftValue instanceof Expression) {
					if (expressionNames.get(leftValue) != null) {
						concatIfLeft.addThenStatement(expressionNames.get(leftValue) + ".update(helper)");
						IfStatement deadTest = new IfStatement(expressionNames.get(leftValue) + ".isDead()");
						concatIfLeft.addThenStatement(deadTest);
						if (rightValue instanceof Expression && rightValue != checker.getRecursiveCall()) {
							deadTest.addThenStatement(exprFunctionName + " = ConcatState.FOLLOWRIGHT");							
							deadTest.addThenStatement("helper.registerClockToStart(" + nameProvider.getClockName((Expression) rightValue) + ")");
//							deadTest.addThenStatement(expressionNames.get(rightValue) + ".start()");
						}
						else if (rightValue instanceof Expression && rightValue == checker.getRecursiveCall()) {
							deadTest.addThenStatement(new Comment("Recursive call here."));
							for (Binding binding : ((Expression) rightValue).getBindings()) {
								AbstractEntity param = binding.getAbstract();
								BindableEntity val = resolveAbstract(param, ((Expression) rightValue).getBindings());
								if (param != val) {
									if (val instanceof ClassicalExpression) {
										deadTest.addThenStatement(nameProvider.getVariableName(param) + " = "
												+ classicalExprNames.get(val) + "()");
									}
									else {
										// ???
									}
								}
							}
							// The following is useless because we are in the ConcatState.FOLLOWLEFT branch
							deadTest.addThenStatement(exprFunctionName + " = ConcatState.FOLLOWLEFT");
							if (leftValue instanceof Expression && leftValue != checker.getRecursiveCall()) {
								deadTest.addThenStatement("helper.registerClockToStart(" + nameProvider.getClockName((Expression) leftValue) + ")");
//								deadTest.addThenStatement(expressionNames.get(leftValue) + ".start()");
							}
						}
					}
				}
				else if (leftValue instanceof Clock) {
					// TODO
					
				}
				IfStatement concatIfRight = new IfStatement(exprFunctionName + " == ConcatState.FOLLOWRIGHT");
				concatIfLeft.addElseStatement(concatIfRight);
				if (rightValue instanceof Expression) {
					if (rightValue == checker.getRecursiveCall()) {
						// generate code to update the values according to the recursive call and
						// restart the concatenation (ie. make it FOLLOWLEFT and start its left arg.
						concatIfRight.addThenStatement(new Comment("Recursive call. Code is here but will never run because of previous test."));
						for (Binding binding : ((Expression) rightValue).getBindings()) {
							AbstractEntity param = binding.getAbstract();
							BindableEntity val = resolveAbstract(param, ((Expression) rightValue).getBindings());
							if (param != val) {
								if (val instanceof ClassicalExpression) {
									concatIfRight.addThenStatement(nameProvider.getVariableName(param) + " = "
											+ classicalExprNames.get(val) + "()");
								}
								else {
									// ???
									
								}
							}
						}
						concatIfRight.addThenStatement(exprFunctionName + " = ConcatState.FOLLOWLEFT");
						if (leftValue instanceof Expression && leftValue != checker.getRecursiveCall()) {
							concatIfRight.addThenStatement("helper.registerClockToStart(" + nameProvider.getClockName((Expression) leftValue) + ")");
//							concatIfRight.addThenStatement(expressionNames.get(leftValue) + ".start()");
						}
					}
					else if (expressionNames.get(rightValue) != null) {
						concatIfRight.addThenStatement(expressionNames.get(rightValue) + ".update(helper)");
					}
				}
				else if (rightValue instanceof Clock) {
					// TODO
				}
			}
			else if ( checker.getRecursiveCall() != null && checker.getRecursiveCall() == swCase.getExpression()) {
				// TODO
				
			}
			else {
				List<Expression> subExpressions = getExpressionTree(swCase.getExpression(), true, true);
				for (Expression expr : subExpressions) {
					String exprName = expressionNames.get(expr);
					caseIfStatement.addThenStatement(exprName + ".update(helper)");
				}
				caseIfStatement.addThenStatement(exprFunctionName + ".update(helper)");
				IfStatement deadTest = new IfStatement(exprFunctionName + ".isDead()");
				caseIfStatement.addThenStatement(deadTest);
				deadTest.addThenStatement("terminate(helper)");
			}
			if (iter.hasNext()) {
				IfStatement elseifStatement = new IfStatement();
				caseIfStatement.addElseStatement(elseifStatement);
				caseIfStatement = elseifStatement;
			}
		}
		if ( definition.getDefaultExpression() != null ) {
			Expression expression = definition.getDefaultExpression();
			List<Expression> subExpressions = getExpressionTree(definition.getDefaultExpression(), true, true);
			for (Expression expr : subExpressions) {
				String exprName = expressionNames.get(expr);
				caseIfStatement.addElseStatement(exprName + ".update(helper)");
			}
			caseIfStatement.addElseStatement(expressionNames.get(expression) + ".update(helper)");
			IfStatement deadTest = new IfStatement(expressionNames.get(expression) + ".isDead()");
			caseIfStatement.addElseStatement(deadTest);
			deadTest.addThenStatement("terminate(helper)");
		}
	}
	
	
	private void compileUserExpressionDefinition(UserExpressionDefinition definition, IProject project)
			throws CoreException {

		@SuppressWarnings("unused")
		RecursiveDefinitionChecker checker = new RecursiveDefinitionChecker(definition);
		String definitionClassName = nameProvider.getClassName(definition);
		String definitionClassPackage = nameProvider.getPackageName(definition);

		ClassDefinition classDef = new ClassDefinition(definitionClassName, definitionClassPackage);
		classDef.setProject(project);
		classDef.setParentClassName(AbstractRuntimeExpression.class.getSimpleName());
		classDef.addImportedPackage(AbstractRuntimeExpression.class.getName());
		
		List<Relation> allRelations = new ArrayList<Relation>();
		for (ConcreteEntity entity : definition.getConcreteEntities()) {
			if (entity instanceof Relation)
				allRelations.add((Relation) entity);
		}
		EqualitySolver<String> equalityRegister = definitionClockEqualityAnalysis(allRelations);
		allRelations.clear();
		
		List<String> elementNames = new ArrayList<String>();
		List<String> constraintNames = new ArrayList<String>();
		List<String> relationNames = new ArrayList<String>();
		List<String> expressionNames = new ArrayList<String>();

		MemberFunction initFunction = new MemberFunction("init", "void");
		initFunction.addModifier("protected");
		classDef.addMemberFunction(initFunction);
		
		List<AbstractEntity> nonClockParameters = getNonClockParameters(definition);
		// generate private members for each argument clock
		generateArgumentsPrivateMembers(definition, classDef, nonClockParameters);
		// Generate a constructor taking as arguments the parameters of the
		// declaration, plus (at the beginning) the implicit clock. 
		generateExpressionDefinitionConstructor(definition, definitionClassName, classDef, nonClockParameters);
		classDef.addImportedPackage(nameProvider.clockClassPackage + "." + nameProvider.clockClassName);
		classDef.addImportedPackage(nameProvider.implicitClockPackage + "." + nameProvider.implicitClockClassName);

		for (ClassicalExpression expression : definition.getClassicalExpressions()) {
			ClassicalExpressionCompiler generator = new ClassicalExpressionCompiler(parameters, pluginHelper, nameProvider);
			String fctName = generator.generateClassicalExpression(expression, definition, classDef);

			String providerTypeName = ValueProvider.class.getSimpleName() + "<" + generator.getReturnTypeName() + ">";
			String providerName = nameProvider.getProviderName(expression);
			MemberVariable providerVariable = new MemberVariable(providerName, providerTypeName);
			providerVariable.addModifier("private");
			String initializer = "new " + providerTypeName + "() {";
			initializer += " public " + generator.getReturnTypeName() + " getValue() { ";
			initializer += "return " + fctName + "();";
			initializer += " } }";
			providerVariable.setInitializer(initializer);
			
			classDef.addMemberVariable(providerVariable);
		}
		String rootExpressionVariable = null;
		// Generate code that build inner entities in a logical order : leaves of the expression tree
		// before upper nodes. This is not strictly necessary as the links between elements are done
		// using only the clocks, which are initialized first, but it helps to make the generated code
		// more readable.
		ArrayList<ConcreteEntity> relationsAndExpressions = new ArrayList<ConcreteEntity>();
		ArrayList<ConcreteEntity> otherEntities	= new ArrayList<ConcreteEntity>();
		for (ConcreteEntity entity : definition.getConcreteEntities()) {
			if (entity instanceof Relation || entity instanceof Expression) {
				relationsAndExpressions.add(entity);
			}
			else {
				otherEntities.add(entity);
			}
		}
		for (ConcreteEntity entity : otherEntities) {
			if (entity instanceof Element) {
				@SuppressWarnings("unused")
				String elementVarName = generateElementCode(entity, "private", classDef, true);
			}
		}
		List<ConcreteEntity> entitiesBuildOrder = getBuildOrder(relationsAndExpressions);
		for (ConcreteEntity entity : entitiesBuildOrder) {
			if (entity instanceof Relation) {
				String relationVarName = generateRelationCode(definition, (Relation) entity,
						classDef, project);
				constraintNames.add(relationVarName);
				relationNames.add(relationVarName);
				initFunction.addStatement(relationVarName + ".setParent(this)");
			} else if (entity instanceof Expression) {
				String expressionVarName;
				if (entity == definition.getRootExpression()) {
					expressionVarName = generateExpressionCode(definition, (Expression) entity,
							true, project, classDef);
					rootExpressionVariable = expressionVarName;
				}
				else {
					String clockVariableName = nameProvider.getClockName((Expression) entity);
					String clockName = entity.getName();
					
					generateDiscreteClockCode(clockVariableName, clockName,
							nameProvider.implicitClockClassName, nameProvider.implicitClockPackage,
							"public", classDef, initFunction, elementNames, equalityRegister, null);
					elementNames.add(clockVariableName);

					expressionVarName = generateExpressionCode(definition, (Expression) entity,
							false, project, classDef);
					
				}
				constraintNames.add(expressionVarName);
				expressionNames.add(expressionVarName);
				initFunction.addStatement(expressionVarName + ".setParent(this)");
			}
		}
		
		if (rootExpressionVariable != null) {
			MemberFunction getRootFunction = new MemberFunction("getRootExpression");
			getRootFunction.addModifier("public");
			getRootFunction.setReturnType(AbstractRuntimeExpression.class.getSimpleName());
			getRootFunction.addStatement("return " + rootExpressionVariable);
			classDef.addMemberFunction(getRootFunction);
		}
		
		MemberFunction startFunction = new MemberFunction("start", "void");
		startFunction.setOverrides(true);
		startFunction.addModifier("public");
		startFunction.addException(SimulationException.class.getSimpleName());
		startFunction.addParameter(AbstractSemanticHelper.class.getSimpleName(), "helper");
		startFunction.addStatement(new IfStatement(" ! canCallStart()").addThenStatement("return"));
		startFunction.addStatement("super.start(helper)");
		for (AbstractEntity parameter : nonClockParameters) {
			String providerVar = nameProvider.getProviderName(parameter);
			String parameterVar = nameProvider.getVariableName(parameter);
			startFunction.addStatement(new IfStatement(providerVar + " != null").addThenStatement(parameterVar + " = " + providerVar + ".getValue()"));
		}
		startFunction.addStatement("getRootExpression().start(helper)");
		for (String relationName : relationNames) {
			startFunction.addStatement(relationName + ".start(helper)");
		}
		classDef.addMemberFunction(startFunction);
		classDef.addImportedPackage(AbstractSemanticHelper.class.getName());
		classDef.addImportedPackage(SimulationException.class.getName());
		
		MemberFunction semanticFunction = new MemberFunction("semantic", "void");
		semanticFunction.setOverrides(true);
		semanticFunction.addModifier("public");
		semanticFunction.addException(SimulationException.class.getSimpleName());
		semanticFunction.addParameter(AbstractSemanticHelper.class.getSimpleName(), "helper");
		semanticFunction.addStatement(new IfStatement(" ! canCallSemantic()").addThenStatement("return"));
		semanticFunction.addStatement("super.semantic(helper)");
		semanticFunction.addStatement("getRootExpression().semantic(helper)");
		for (String relationName : relationNames) {
			semanticFunction.addStatement(relationName + ".semantic(helper)");
		}
		classDef.addMemberFunction(semanticFunction);
		classDef.addImportedPackage(AbstractSemanticHelper.class.getName());
		
		MemberFunction updateFunction = new MemberFunction("update", "void");
		updateFunction.setOverrides(true);
		updateFunction.addModifier("public");
		updateFunction.addException(SimulationException.class.getSimpleName());
		updateFunction.addParameter(AbstractUpdateHelper.class.getSimpleName(), "helper");
		updateFunction.addStatement(new IfStatement(" ! canCallUpdate()").addThenStatement("return"));
		updateFunction.addStatement("super.update(helper)");
		updateFunction.addStatement("getRootExpression().update(helper)");
		for (String relationName : relationNames) {
			updateFunction.addStatement(relationName + ".update(helper)");
		}
		classDef.addMemberFunction(updateFunction);
		classDef.addImportedPackage(AbstractUpdateHelper.class.getName());

		classDef.generate(pluginHelper);
	}

	private List<AbstractEntity> getNonClockParameters(UserRelationDefinition definition) {
		RelationDeclaration declaration = definition.getDeclaration();
		return getNonClockParameters(declaration.getParameters());
	}
	
	private List<AbstractEntity> getNonClockParameters(UserExpressionDefinition definition) {
		ExpressionDeclaration declaration = definition.getDeclaration();
		return getNonClockParameters(declaration.getParameters());
	}
	
	private List<AbstractEntity> getNonClockParameters(List<AbstractEntity> parameters) {
		List<AbstractEntity> res = new ArrayList<AbstractEntity>();
		for (AbstractEntity parameter : parameters) {
			Type type = parameter.getType();
			if ( ! (type instanceof DiscreteClockType)) {
				res.add(parameter);
			}
		}
		return res;		
	}

	private final class ExpressionParametersBuilder extends KernelExpressionSwitch<List<AbstractEntity>> {
		@Override
		public List<AbstractEntity> caseUnion(Union object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getClock1());
			params.add(object.getClock2());
			return params;
		}

		@Override
		public List<AbstractEntity> caseDefer(Defer object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getBaseClock());
			params.add(object.getDelayClock());
			params.add(object.getDelayPattern());
			return params;
		}
		
		@Override
		public List<AbstractEntity> caseInf(Inf object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getClock1());
			params.add(object.getClock2());
			return params;
		}
		
		@Override
		public List<AbstractEntity> caseSup(Sup object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getClock1());
			params.add(object.getClock2());
			return params;
		}

		@Override
		public List<AbstractEntity> caseIntersection(Intersection object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getClock1());
			params.add(object.getClock2());
			return params;
		}

		@Override
		public List<AbstractEntity> caseUpTo(UpTo object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getClockToFollow());
			params.add(object.getKillerClock());
			return params;
		}

		@Override
		public List<AbstractEntity> caseConcatenation(Concatenation object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getLeftClock());
			params.add(object.getRightClock());
			return params;
		}

		@Override
		public List<AbstractEntity> caseWait(Wait object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getWaitingClock());
			params.add(object.getWaitingValue());
			return params;
		}
		
		@Override
		public List<AbstractEntity> caseNonStrictSampling(NonStrictSampling object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getSampledClock());
			params.add(object.getSamplingClock());
			return params;
		}
		
		@Override
		public List<AbstractEntity> caseStrictSampling(StrictSampling object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getSampledClock());
			params.add(object.getSamplingClock());
			return params;
		}
		
		@Override
		public List<AbstractEntity> caseDiscretization(Discretization object) {
			List<AbstractEntity> params = new ArrayList<AbstractEntity>();
			params.add(object.getDenseClock());
			params.add(object.getDiscretizationFactor());
			return params;
		}
		
		@Override
		public List<AbstractEntity> caseDeath(Death object) {
			return new ArrayList<AbstractEntity>();
		}
	}

	private List<AbstractEntity> getKernelExpressionParameters(KernelExpressionDeclaration declaration) {
		KernelExpressionSwitch<List<AbstractEntity>> selector = new ExpressionParametersBuilder();
		return selector.doSwitch(declaration);
	}

}