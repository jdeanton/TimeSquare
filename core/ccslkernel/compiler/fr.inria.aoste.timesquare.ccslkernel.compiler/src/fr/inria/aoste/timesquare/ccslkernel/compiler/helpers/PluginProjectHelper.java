/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.helpers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.jdt.core.IClasspathAttribute;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.pde.core.project.IBundleClasspathEntry;
import org.eclipse.pde.core.project.IBundleProjectDescription;
import org.eclipse.pde.core.project.IBundleProjectService;
import org.eclipse.pde.core.project.IPackageExportDescription;
import org.eclipse.pde.core.project.IRequiredBundleDescription;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import fr.inria.aoste.timesquare.ccslkernel.compiler.Activator;
import fr.inria.aoste.timesquare.ccslkernel.compiler.CompilerParameters;

public class PluginProjectHelper {

	public PluginProjectHelper() {
		workspace = ResourcesPlugin.getWorkspace();
		root = workspace.getRoot();
	}
	
	public IProject createPluginProject(String projectName) throws CoreException {

		BundleContext context = Activator.getContext();
		ServiceReference<IBundleProjectService> ref = context.getServiceReference(IBundleProjectService.class);
		IBundleProjectService service = context.getService(ref);

		IProject project = getExistingProject(projectName);
		IBundleProjectDescription bundleDesc;

		if (project != null) {
			fixExistingProject(project);
			return project;
		}
		
		project = createProject(projectName);

		bundleDesc = service.getDescription(project);
		service.setBundleRoot(project, null);

		if (bundleDesc.getSymbolicName() == null || bundleDesc.getSymbolicName().compareTo(projectName) != 0)
			bundleDesc.setSymbolicName(projectName);
		if (bundleDesc.getBundleName() == null || bundleDesc.getBundleName().compareTo(projectName) != 0)
			bundleDesc.setBundleName(projectName);
		bundleDesc.setLocationURI(project.getLocationURI());

		bundleDesc.setNatureIds(new String[] { org.eclipse.jdt.core.JavaCore.NATURE_ID,
				IBundleProjectDescription.PLUGIN_NATURE });
		bundleDesc.setExecutionEnvironments(new String[] { "JavaSE-1.7", "JavaSE-1.6" });
		bundleDesc.setRequiredBundles(new IRequiredBundleDescription[0]);

		IPath[] iPathTab = {new Path(".")};
		bundleDesc.setBinIncludes(iPathTab);
		
		bundleDesc.apply(new NullProgressMonitor());
		
		initializePluginProjectBuilders(project);

		setPluginProjectClassPath(project);

//		IBundleClasspathEntry cpe1 = service.newBundleClasspathEntry(new Path(CompilerParameters.sourceFolder),
//				new Path("bin"), null);
		IBundleClasspathEntry cpe2 = service.newBundleClasspathEntry(new Path(CompilerParameters.genSourceFolder),
				null, null);
		bundleDesc.setBundleClasspath(new IBundleClasspathEntry[] { /* cpe1, */ cpe2 });

		bundleDesc.apply(new NullProgressMonitor());

		context.ungetService(ref);
				
		return project;
	}
	
	private void fixExistingProject(IProject project) throws CoreException {
		BundleContext context = Activator.getContext();
		ServiceReference<IBundleProjectService> ref = context.getServiceReference(IBundleProjectService.class);
		IBundleProjectService service = context.getService(ref);

		IBundleProjectDescription bundleDesc = service.getDescription(project);;

		if ( ! isJavaProject(project)) {
			String newNatures[] = Arrays.copyOf(bundleDesc.getNatureIds(),
					bundleDesc.getNatureIds().length + 1);
			newNatures[bundleDesc.getNatureIds().length] = org.eclipse.jdt.core.JavaCore.NATURE_ID;
			bundleDesc.setNatureIds(newNatures);
			bundleDesc.apply(new NullProgressMonitor());
		}
		if ( ! isPluginProject(project)) {
			String newNatures[] = Arrays.copyOf(bundleDesc.getNatureIds(),
					bundleDesc.getNatureIds().length + 1);
			newNatures[bundleDesc.getNatureIds().length] = IBundleProjectDescription.PLUGIN_NATURE;
			bundleDesc.setNatureIds(newNatures);
			bundleDesc.apply(new NullProgressMonitor());
		}
		// Look for at least "JavaSE-1.7" or "JavaSE-1.6" as a possible execution environment.
		String execEnv[] = bundleDesc.getExecutionEnvironments();
		boolean envJava6Found = false;
		boolean envJava7Found = false;
		if (execEnv != null) {
			for (String env : execEnv) {
				if (env.compareTo("JavaSE-1.7") == 0)
					envJava7Found = true;
				else if (env.compareTo("JavaSE-1.6") == 0)
					envJava6Found = true;
			}
		}
		if ( ! envJava6Found) {
			addExecutionEnvironment(bundleDesc, "JavaSE-1.6");
		}
		if ( ! envJava7Found) {
			addExecutionEnvironment(bundleDesc, "JavaSE-1.7");
		}
		
		fixPluginProjectBuilders(project);

		fixPluginProjectClassPath(project);
		
//		IBundleClasspathEntry classpath[] = bundleDesc.getBundleClasspath();
//		if (classpath == null) {
//			IBundleClasspathEntry cpe1 = service.newBundleClasspathEntry(new Path(CompilerParameters.sourceFolder),
//					null, null);
//			IBundleClasspathEntry cpe2 = service.newBundleClasspathEntry(new Path(CompilerParameters.genSourceFolder),
//					null, null);
//			bundleDesc.setBundleClasspath(new IBundleClasspathEntry[] { cpe1, cpe2 });
//			bundleDesc.apply(new NullProgressMonitor());
//		}
//		else {
//			boolean srcgenEntryFound = false;
//			for (IBundleClasspathEntry entry : classpath) {
//				if (entry.getSourcePath() != null && entry.getSourcePath().toString().compareTo(CompilerParameters.genSourceFolder) == 0)
//					srcgenEntryFound = true;
//			}
//			if ( ! srcgenEntryFound ) {
//				IBundleClasspathEntry cpe = service.newBundleClasspathEntry(new Path(CompilerParameters.genSourceFolder),
//						null, null);
//				IBundleClasspathEntry[] newEntries = Arrays.copyOf(classpath, classpath.length + 1);
//				newEntries[classpath.length] = cpe;
//				bundleDesc.setBundleClasspath(newEntries);
//				bundleDesc.apply(new NullProgressMonitor());
//			}
//		}
		context.ungetService(ref);
	}
	
	
	
	private boolean isJavaProject(IProject project) throws CoreException {
		BundleContext context = Activator.getContext();
		ServiceReference<IBundleProjectService> ref = context.getServiceReference(IBundleProjectService.class);
		IBundleProjectService service = context.getService(ref);
		IBundleProjectDescription bundleDesc = service.getDescription(project);
		boolean isJavaProject = false;
		for (String nature : bundleDesc.getNatureIds()) {
			if (nature.compareTo(org.eclipse.jdt.core.JavaCore.NATURE_ID) == 0)
				isJavaProject = true;
		}
		context.ungetService(ref);
		return isJavaProject;
	}

	@SuppressWarnings("restriction")
	private boolean isPluginProject(IProject project) throws CoreException {
		BundleContext context = Activator.getContext();
		ServiceReference<IBundleProjectService> ref = context.getServiceReference(IBundleProjectService.class);
		IBundleProjectService service = context.getService(ref);
		IBundleProjectDescription bundleDesc = service.getDescription(project);
		boolean isPluginProject = false;
		for (String nature : bundleDesc.getNatureIds()) {
			if ((nature.compareTo(org.eclipse.pde.internal.core.natures.PDE.PLUGIN_NATURE) == 0)
					|| (nature.compareTo(IBundleProjectDescription.PLUGIN_NATURE) == 0))
				isPluginProject = true;
		}
		context.ungetService(ref);
		return isPluginProject;
	}
	
	private void addExecutionEnvironment(IBundleProjectDescription bundleDesc, String env) throws CoreException {
		String[] currentEnvs = bundleDesc.getExecutionEnvironments();
		String[] newEnvs;
		if (currentEnvs != null) {
			newEnvs = Arrays.copyOf(currentEnvs, currentEnvs.length + 1);
			newEnvs[currentEnvs.length] = env;
		}
		else {
			newEnvs = new String[] { env };
		}
		bundleDesc.setExecutionEnvironments(newEnvs);
		bundleDesc.apply(new NullProgressMonitor());
	}
	
	protected IWorkspace workspace;
	protected IWorkspaceRoot root;

	private IProject createProject(final String projectName) throws CoreException {
		final IProject[] result = new IProject[1];
		IWorkspaceRunnable operation = new IWorkspaceRunnable() {
			public void run(IProgressMonitor monitor) throws CoreException {
				final IProject project = root.getProject(projectName);
				if (project.exists()) {
					// project.delete(true, new NullProgressMonitor());
					project.open(new NullProgressMonitor());
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
					result[0] = project;
					return;
				}
				project.create(new NullProgressMonitor());
				project.open(new NullProgressMonitor());
				project.refreshLocal(IResource.DEPTH_INFINITE, null);

				result[0] = project;
			}
		};
		workspace.run(operation, new NullProgressMonitor());
		return result[0];
	}
	
	private IProject getExistingProject(final String projectName) throws CoreException {
		final IProject[] result = new IProject[1];
		IWorkspaceRunnable operation = new IWorkspaceRunnable() {
			public void run(IProgressMonitor monitor) throws CoreException {
				final IProject project = root.getProject(projectName);
				if (project.exists()) {
					// project.delete(true, new NullProgressMonitor());
					project.open(new NullProgressMonitor());
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
					result[0] = project;
					return;
				}
				else {
					result[0] = null;
				}
			}
		};
		workspace.run(operation, new NullProgressMonitor());
		return result[0];
	}

	/**
	 * Initialize the classpath of a plugin project to contain both the JRE and
	 * the required plugins (as a container). The actual required plugins can be
	 * added later using the {@link #addRequiredBundle(IProject, String)}
	 * method. This function also creates src/ and src-gen/ folders in the plugin project.
	 * 
	 * @param project
	 * @throws CoreException
	 */
	private void setPluginProjectClassPath(IProject project) throws CoreException {
		IJavaProject javaProject = org.eclipse.jdt.core.JavaCore.create(project);

		IFolder srcFolder = project.getFolder(new Path(CompilerParameters.sourceFolder));
		if ( ! srcFolder.exists() )
			srcFolder.create(false, true, new NullProgressMonitor());
		IFolder genSrcFolder = project.getFolder(new Path(CompilerParameters.genSourceFolder));
		if ( ! genSrcFolder.exists() )
			genSrcFolder.create(false, true, new NullProgressMonitor());
		
		IClasspathEntry src = JavaCore.newSourceEntry(srcFolder.getFullPath());
		IClasspathEntry genSrc = JavaCore.newSourceEntry(genSrcFolder.getFullPath());

		IClasspathEntry jre = JavaCore.newContainerEntry(new Path(org.eclipse.jdt.launching.JavaRuntime.JRE_CONTAINER),
				new IAccessRule[0], new IClasspathAttribute[] {}, false);
		IClasspathEntry reqPlugins = JavaCore.newContainerEntry(new Path("org.eclipse.pde.core.requiredPlugins"),
				new IAccessRule[0], new IClasspathAttribute[] {}, false);
		IClasspathEntry[] entries = new IClasspathEntry[] { src, genSrc, jre, reqPlugins };
		javaProject.setRawClasspath(entries, project.getFullPath().append("bin"), new NullProgressMonitor());
		javaProject.save(new NullProgressMonitor(), true);
	}

	private void fixPluginProjectClassPath(IProject project) throws CoreException {
		IJavaProject javaProject = org.eclipse.jdt.core.JavaCore.create(project);

		IFolder srcFolder = project.getFolder(new Path(CompilerParameters.sourceFolder));
		if ( ! srcFolder.exists() )
			srcFolder.create(false, true, new NullProgressMonitor());
		IFolder genSrcFolder = project.getFolder(new Path(CompilerParameters.genSourceFolder));
		if ( ! genSrcFolder.exists() )
			genSrcFolder.create(false, true, new NullProgressMonitor());

		IClasspathEntry[] currentClasspath = javaProject.getRawClasspath();
//		boolean srcEntryFound = false;
		boolean srcgenEntryFound = false;
		boolean jreEntryFound = false;
		boolean reqPluginsEntryFound = false;

		for (IClasspathEntry entry : currentClasspath) {
//			if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE &&
//					entry.getPath().equals(srcFolder.getFullPath())) {
//				srcEntryFound = true;
//			}
			if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE &&
					entry.getPath().equals(genSrcFolder.getFullPath())) {
				srcgenEntryFound = true;
			}
			if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER &&
					entry.getPath().toString().equals(org.eclipse.jdt.launching.JavaRuntime.JRE_CONTAINER)) {
				jreEntryFound = true;
			}
			if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER &&
					entry.getPath().toString().equals("org.eclipse.pde.core.requiredPlugins")) {
				reqPluginsEntryFound = true;
			}
		}
//		if ( ! srcEntryFound) {
//			IClasspathEntry entry = JavaCore.newSourceEntry(srcFolder.getFullPath(), new IPath[] {}, new IPath[] {}, null);
//			addClasspathEntry(javaProject, entry);
//		}
		if ( ! srcgenEntryFound) {
			IClasspathEntry entry = JavaCore.newSourceEntry(genSrcFolder.getFullPath());
			addClasspathEntry(javaProject, entry);
		}
		if ( ! jreEntryFound) {
			IClasspathEntry entry = JavaCore.newContainerEntry(new Path(org.eclipse.jdt.launching.JavaRuntime.JRE_CONTAINER));
			addClasspathEntry(javaProject, entry);
		}
		if ( ! reqPluginsEntryFound) {
			IClasspathEntry entry = JavaCore.newContainerEntry(new Path("org.eclipse.pde.core.requiredPlugins"));
			addClasspathEntry(javaProject, entry);
		}
	}
	
	@SuppressWarnings("unused")
	private void addClasspathEntry(IProject project, IClasspathEntry entry) throws JavaModelException {
		IJavaProject javaProject = JavaCore.create(project);
		if (javaProject != null)	
			addClasspathEntry(javaProject, entry);
	}
	
	private void addClasspathEntry(IJavaProject javaProject, IClasspathEntry entry) throws JavaModelException {
		IClasspathEntry[] currentClasspath = javaProject.getRawClasspath();
		IClasspathEntry[] newClasspath = Arrays.copyOf(currentClasspath, currentClasspath.length + 1);
		newClasspath[currentClasspath.length] = entry;
		javaProject.setRawClasspath(newClasspath, new NullProgressMonitor());
		javaProject.save(new NullProgressMonitor(), true);
	}
	
	private void fixPluginProjectBuilders(IProject project) throws CoreException {
		IProjectDescription description = project.getDescription();
		ICommand[] commands = description.getBuildSpec();
		if (commands == null || commands.length == 0) {
			initializePluginProjectBuilders(project);
			return;
		}
		//must found 3 builders in the correct order
		boolean javaBuilderFound = false;
		boolean manifestBuilderFound = false;
		boolean schemaBuilderFound = false;
		ArrayList<ICommand> otherBuilders = new ArrayList<ICommand>();
		for (ICommand cmd : commands) {
			if (cmd.getBuilderName().compareTo(org.eclipse.jdt.core.JavaCore.BUILDER_ID) == 0) {
				javaBuilderFound = ( ! manifestBuilderFound  && ! schemaBuilderFound );
			}
			else if (cmd.getBuilderName().compareTo("org.eclipse.pde.ManifestBuilder") == 0) {
				manifestBuilderFound = ( javaBuilderFound && ! schemaBuilderFound );
			}
			else if (cmd.getBuilderName().compareTo("org.eclipse.pde.SchemaBuilder") == 0) {
				schemaBuilderFound = ( javaBuilderFound && schemaBuilderFound );
			}
			else {
				otherBuilders.add(cmd);
			}
		}
		if ( ! javaBuilderFound || ! manifestBuilderFound || ! schemaBuilderFound) {
			ICommand[] newCmds = new ICommand[otherBuilders.size() + 3];
			newCmds[0] = description.newCommand();
			newCmds[1] = description.newCommand();
			newCmds[2] = description.newCommand();
			newCmds[0].setBuilderName(org.eclipse.jdt.core.JavaCore.BUILDER_ID);
			newCmds[1].setBuilderName("org.eclipse.pde.ManifestBuilder");
			newCmds[2].setBuilderName("org.eclipse.pde.SchemaBuilder");
			int i = 3;
			for (ICommand cmd : otherBuilders) {
				newCmds[i] = cmd;
				i = i + 1;
			}
			description.setBuildSpec(newCmds);
			project.setDescription(description, IProject.FORCE, new NullProgressMonitor());
		}
	}
	
	private void initializePluginProjectBuilders(IProject project) throws CoreException {
		IProjectDescription description = project.getDescription();
		ICommand[] commands = new ICommand[] { description.newCommand(),
				description.newCommand(), description.newCommand() };
		commands[0].setBuilderName(org.eclipse.jdt.core.JavaCore.BUILDER_ID);
		commands[1].setBuilderName("org.eclipse.pde.ManifestBuilder");
		commands[2].setBuilderName("org.eclipse.pde.SchemaBuilder");
		description.setBuildSpec(commands);
		project.setDescription(description, IProject.FORCE, new NullProgressMonitor());
	}

	public void addRequiredBundle(IProject project, String bundleName) throws CoreException {
		BundleContext context = Activator.getContext();
		ServiceReference<IBundleProjectService> ref = context.getServiceReference(IBundleProjectService.class);
		IBundleProjectService service = context.getService(ref);
		IBundleProjectDescription bundleDesc = service.getDescription(project);

		IRequiredBundleDescription[] required = bundleDesc.getRequiredBundles();
		int newLength = required != null ? required.length + 1 : 1;
		IRequiredBundleDescription[] newRequired = new IRequiredBundleDescription[newLength];
		if (required != null) {
			System.arraycopy(required, 0, newRequired, 0, required.length);
		}
		IRequiredBundleDescription reqBundle = service.newRequiredBundle(bundleName, null, false, false);
		newRequired[newLength - 1] = reqBundle;
		bundleDesc.setRequiredBundles(newRequired);

		bundleDesc.apply(new NullProgressMonitor());
		context.ungetService(ref);
	}

	public void addExportedPackage(IProject project, String packageName) throws CoreException {
		BundleContext context = Activator.getContext();
		ServiceReference<IBundleProjectService> ref = context.getServiceReference(IBundleProjectService.class);
		IBundleProjectService service = context.getService(ref);
		IBundleProjectDescription bundleDesc = service.getDescription(project);

		IPackageExportDescription[] exports = bundleDesc.getPackageExports();
		int newLength = (exports != null ? exports.length + 1 : 1);
		IPackageExportDescription[] newExports = new IPackageExportDescription[newLength];
		if (exports != null) {
			System.arraycopy(exports, 0, newExports, 0, exports.length);
		}
		newExports[newLength - 1] = service.newPackageExport(packageName, null, true, null);
		bundleDesc.setPackageExports(newExports);

		bundleDesc.apply(new NullProgressMonitor());
		context.ungetService(ref);
	}

	private void createFolder(IFolder folder) throws CoreException {
		IContainer parent = folder.getParent();
		if (parent instanceof IFolder) {
			createFolder((IFolder) parent);
		}
		if (!folder.exists()) {
			folder.create(false, true, new NullProgressMonitor());
		}
	}

	private void createFolder(IProject project, IPath path) throws CoreException {
		IFolder newFolder = project.getFolder(path);
		createFolder(newFolder);
	}

	public void createFolder(IProject project, String path) throws CoreException {
		createFolder(project, new Path(path));
	}

	public void createPackage(IProject project, String packageName) throws CoreException {
		String packageFolder = packageName.replace('.', File.separatorChar);
		createFolder(project, CompilerParameters.genSourceFolder + File.separator + packageFolder);
		addExportedPackage(project, packageName);
	}

	public IFile createFile(IProject project, String path, String content) throws CoreException {
		IFile file = project.getFile(path);
		if (file.exists()) {
			file.delete(true, null);
		}
		file.create(new ByteArrayInputStream(content.getBytes()), true, null);
		return file;
	}

}
