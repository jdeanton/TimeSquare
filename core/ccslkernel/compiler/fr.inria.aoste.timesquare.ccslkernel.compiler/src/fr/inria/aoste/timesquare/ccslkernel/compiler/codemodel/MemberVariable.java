/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public class MemberVariable {

	private String name;
	private List<Annotation> annotations = new ArrayList<>();
	private List<String> modifiers = new ArrayList<String>();
	private String typeName;
	private String initializer;
	
	public MemberVariable(String name, String typeName) {
		this.name = name;
		this.typeName = typeName;
	}
	
	public String getName() {
		return name;
	}

	public void addModifier(String modifier) {
		modifiers.add(modifier);
	}
	
	public void setInitializer(String init) {
		initializer = init;
	}
	
	public void generate(StringBuilder fileContent, int indentation) {
		for (Annotation annotation : annotations) {
			addIndentation(indentation, fileContent);
			fileContent.append(annotation.toString() + "\n");
		}
		addIndentation(indentation, fileContent);
		for (String modifier : modifiers) {
			fileContent.append(modifier + " ");
		}
		fileContent.append(typeName + " " + name);
		if (initializer != null &&   ! initializer.isEmpty()) {
			fileContent.append(" = " + initializer);
		}
		fileContent.append(";\n");
	}
	
	private void addIndentation(int indent, StringBuilder content) {
		for (int i = 0; i < indent; i++) {
			content.append(' ');
		}
	}

}
