/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler.codemodel;

public class SimpleStatement extends AbstractStatement {

	private String code;
	
	public SimpleStatement(String code) {
		this.code = code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	@Override
	public void generate(StringBuilder sb, int indent) {
		indent(sb, indent);
		sb.append(code);
		sb.append(";\n");
	}

}
