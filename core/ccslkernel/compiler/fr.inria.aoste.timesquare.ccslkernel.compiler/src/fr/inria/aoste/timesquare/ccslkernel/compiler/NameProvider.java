/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.compiler;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Char;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Real;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.util.BasicTypeSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util.ClassicalExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Death;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.util.TimeModelSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
import fr.inria.aoste.timesquare.ccslkernel.runtime.Activator;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeSequence;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeConcatenation;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeDeath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeDefer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeDiscretization;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeFilterBy;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeInf;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeIntersection;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeSampling;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeSup;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeUnion;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeUpTo;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeWait;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimeCoincides;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimeExclusion;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimeNonStrictPrecedes;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimePrecedes;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimeSubClock;

public class NameProvider {

	@SuppressWarnings("unused")
	private final static String RUNTIME_PREFIX = "Runtime";
	private final static String RUNTIME_PACKAGE = Activator.class.getPackage().getName();
	
	public NameProvider(CompilerParameters parameters, UnfoldModel unfoldModel) {
		this.parameters = parameters;
		this.unfoldModel = unfoldModel;
	}

	private CompilerParameters parameters;
	private UnfoldModel unfoldModel;
	
	private static class BasicType2JavaClassMapper extends BasicTypeSwitch<Class<?>> {
		@Override
		public Class<RuntimeClock> caseDiscreteClockType(DiscreteClockType object) {
			return RuntimeClock.class;
		}
		@Override
		public Class<RuntimeClock> caseDenseClockType(DenseClockType object) {
			return RuntimeClock.class;
		}
		@SuppressWarnings("rawtypes")
		@Override
		public Class<RuntimeSequence> caseSequenceType(SequenceType object) {
			return RuntimeSequence.class;
		}
		@SuppressWarnings("rawtypes")
		@Override
		public Class<RuntimeSequence> caseSequenceElement(SequenceElement object) {
			return RuntimeSequence.class;
		}
		@Override
		public Class<java.lang.Integer> caseInteger(Integer object) {
			return java.lang.Integer.class;
		}
		@Override
		public Class<java.lang.Integer> caseIntegerElement(IntegerElement object) {
			return java.lang.Integer.class;
		}
		@Override
		public Class<Float> caseReal(Real object) {
			return java.lang.Float.class;
		}
		@Override
		public Class<Float> caseRealElement(RealElement object) {
			return java.lang.Float.class;
		}
		@Override
		public Class<String> caseString(fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String object) {
			return java.lang.String.class;
		}
		@Override
		public Class<String> caseStringElement(StringElement object) {
			return java.lang.String.class;
		}
		@Override
		public Class<Character> caseChar(Char object) {
			return java.lang.Character.class;
		}
		@Override
		public Class<Character> caseCharElement(CharElement object) {
			return java.lang.Character.class;
		}
		@Override
		public Class<?> caseRecord(Record object) {
			return null;
		}
		@Override
		public Class<java.lang.Boolean> caseBoolean(Boolean object) {
			return java.lang.Boolean.class;
		}
		@Override
		public Class<java.lang.Boolean> caseBooleanElement(BooleanElement object) {
			return java.lang.Boolean.class;
		}
	}
	
	static private BasicType2JavaClassMapper basicTypeClassMapper = new BasicType2JavaClassMapper();
	
	private static class TimeModel2JavaClassMapper extends TimeModelSwitch<Class<?>> {
		@Override
		public Class<RuntimeClock> caseClock(Clock object) {
			return RuntimeClock.class;
		}
	}

	static private TimeModel2JavaClassMapper timeModelClassMapper = new TimeModel2JavaClassMapper();

	private static class ClassicalExpr2JavaClassMapper extends ClassicalExpressionSwitch<Class<?>> {
		@Override
		public Class<java.lang.Boolean> caseBooleanExpression(BooleanExpression object) {
			return java.lang.Boolean.class;
		}
		@Override
		public Class<java.lang.Integer> caseIntegerExpression(IntegerExpression object) {
			return java.lang.Integer.class;
		}
		@Override
		public Class<java.lang.Float> caseRealExpression(RealExpression object) {
			return java.lang.Float.class;
		}
		@SuppressWarnings("rawtypes")
		@Override
		public Class<RuntimeSequence> caseSeqExpression(SeqExpression object) {
			return RuntimeSequence.class;
		}
	}
	
	static private ClassicalExpr2JavaClassMapper classicalExprClassMapper =  new ClassicalExpr2JavaClassMapper();

	public Class<?> getJavaClass(NamedElement modelObject) {
		if (modelObject.eClass().getEPackage() == BasicTypePackage.eINSTANCE) {
			return basicTypeClassMapper.doSwitch(modelObject);
		}
		else if (modelObject.eClass().getEPackage() == TimeModelPackage.eINSTANCE) {
			return timeModelClassMapper.doSwitch(modelObject);
		}
		else if (modelObject.eClass().getEPackage() == ClassicalExpressionPackage.eINSTANCE) {
			return classicalExprClassMapper.doSwitch(modelObject);
		}
		else if (modelObject.eClass().getEPackage() == KernelRelationPackage.eINSTANCE) {
			return kernelRelation2JavaClassMapper.doSwitch(modelObject);
		}
		else if (modelObject.eClass().getEPackage() == KernelExpressionPackage.eINSTANCE) {
			return kernelExpression2JavaClassMapper.doSwitch(modelObject);
		}
		else
			return null;
	}

	private String inferSequenceTypeName(NamedElement modelObject) {
		SequenceType seqType;
		if (modelObject instanceof SequenceElement) {
			seqType = (SequenceType) ((SequenceElement) modelObject).getType();
		}
		else if (modelObject instanceof SeqExpression) {
			// Actual SeqExpression objects are not always complete, the getType() function
			// can return null.
			seqType = (SequenceType) ((SeqExpression)modelObject).getType();
		}
		else if (modelObject instanceof SequenceType) {
			seqType = (SequenceType) modelObject;
		}
		else {
			seqType = null;
		}
		if (seqType == null || seqType.getElementType() == null) {
			return RuntimeSequence.class.getSimpleName() + "<Integer>";
		}
		else {
			PrimitiveType eltType = seqType.getElementType();
			return RuntimeSequence.class.getSimpleName() + "<" + getClassName(eltType) + ">";
		}
	}
	
	public String getClassName(NamedElement modelObject) {
		if (modelObject instanceof ClockConstraintSystem) {
			return capitalizeFirstLetter(modelObject.getName() + "Model");
		}
		else if (modelObject instanceof Block) {
			return capitalizeFirstLetter(modelObject.getName() + "Block");
		}
		else if (modelObject instanceof Clock) {
			return clockClassName;
		}
		else if (modelObject instanceof AbstractEntity && ((AbstractEntity)modelObject).getType() != null) {
			return getClassName(((AbstractEntity)modelObject).getType());
		}
		else if (modelObject instanceof Relation) {
			if (((Relation) modelObject).getType() instanceof KernelRelationDeclaration) {
				KernelRelationDeclaration decl = (KernelRelationDeclaration) ((Relation) modelObject).getType();
				return kernelRelation2JavaClassMapper.doSwitch(decl).getSimpleName();
			}
			else {
				NamedElement definition = unfoldModel.getUsedRelationDefinition(((Relation) modelObject).getType());
				return capitalizeFirstLetter(definition.getName());
			}
		}
		else if (modelObject instanceof Expression) {
			if (((Expression) modelObject).getType() instanceof KernelExpressionDeclaration) {
				KernelExpressionDeclaration decl = (KernelExpressionDeclaration) ((Expression) modelObject).getType();
				return kernelExpression2JavaClassMapper.doSwitch(decl).getSimpleName();
			}
			else {
				ExpressionDeclaration decl = ((Expression) modelObject).getType();
				if (parameters.useRuntimeFilterBy && decl.getName().compareTo(CompilerParameters.FILTERBYNAME) == 0) {
					return RuntimeFilterBy.class.getSimpleName();
				}
				else {
					NamedElement definition = unfoldModel.getUsedExpressionDefinition(decl);
					return capitalizeFirstLetter(definition.getName());
				}
			}
		}
		else if (modelObject.eClass().getEPackage() == BasicTypePackage.eINSTANCE) {
			// Handle the RuntimeSequence case to add type arguments. HACK : we add <Integer> by default as
			// it is the most frequent kind of Sequence used in CCSL specifications.
			Class<?> type = basicTypeClassMapper.doSwitch(modelObject);
			if (type == RuntimeSequence.class) {  // Comes from a SequenceElement or SequenceType
				return inferSequenceTypeName(modelObject);
			}
			else
				return type.getSimpleName();
		}
		else if (modelObject.eClass().getEPackage() == TimeModelPackage.eINSTANCE) {
			return timeModelClassMapper.doSwitch(modelObject).getSimpleName();
		}
		else if (modelObject.eClass().getEPackage() == ClassicalExpressionPackage.eINSTANCE) {
			Class<?> type = classicalExprClassMapper.doSwitch(modelObject);
			if (type == RuntimeSequence.class) {
				return inferSequenceTypeName(modelObject);
			}
			else
				return type.getSimpleName();
		}
		return capitalizeFirstLetter(modelObject.getName());
	}

	private static class KernelRelation2JavaClassMapper extends KernelRelationSwitch<Class<?>> {
		@Override
		public Class<?> caseCoincidence(Coincidence object) {
			return RuntimeCoincides.class;
		}
		@Override
		public Class<?> caseExclusion(Exclusion object) {
			return RuntimeExclusion.class;
		}
		@Override
		public Class<?> caseNonStrictPrecedence(NonStrictPrecedence object) {
			return RuntimeNonStrictPrecedes.class;
		}
		@Override
		public Class<?> casePrecedence(Precedence object) {
			return RuntimePrecedes.class;
		}
		@Override
		public Class<?> caseSubClock(SubClock object) {
			return RuntimeSubClock.class;
		}
	}
	
	static private KernelRelation2JavaClassMapper kernelRelation2JavaClassMapper = new KernelRelation2JavaClassMapper();
	
	private static class KernelExpression2JavaClassMapper extends KernelExpressionSwitch<Class<?>> {
		@Override
		public Class<?> caseConcatenation(Concatenation object) {
			return RuntimeConcatenation.class;
		}
		@Override
		public Class<?> caseDeath(Death object) {
			return RuntimeDeath.class;
		}
		@Override
		public Class<?> caseDefer(Defer object) {
			return RuntimeDefer.class;
		}
		@Override
		public Class<?> caseDiscretization(Discretization object) {
			return RuntimeDiscretization.class;
		}
		@Override
		public Class<?> caseInf(Inf object) {
			return RuntimeInf.class;
		}
		@Override
		public Class<?> caseIntersection(Intersection object) {
			return RuntimeIntersection.class;
		}
		@Override
		public Class<?> caseNonStrictSampling(NonStrictSampling object) {
			return RuntimeSampling.class;
		}
		@Override
		public Class<?> caseStrictSampling(StrictSampling object) {
			return RuntimeStrictSampling.class;
		}
		@Override
		public Class<?> caseSup(Sup object) {
			return RuntimeSup.class;
		}
		@Override
		public Class<?> caseUnion(Union object) {
			return RuntimeUnion.class;
		}
		@Override
		public Class<?> caseUpTo(UpTo object) {
			return RuntimeUpTo.class;
		}
		@Override
		public Class<?> caseWait(Wait object) {
			return RuntimeWait.class;
		}
	}
	
	static private KernelExpression2JavaClassMapper kernelExpression2JavaClassMapper = new KernelExpression2JavaClassMapper();
	
	public String getPackageName(NamedElement modelObject) {
		String packageName;
		if (modelObject instanceof ClockConstraintSystem) {
			packageName = "models";
		}
		else if (modelObject instanceof Block) {
			packageName = unfoldModel.getModel().getName() + ".blocks";
		}
		else if (modelObject instanceof Clock) {
			return clockClassPackage;
		}
		else if (modelObject instanceof Relation) {
			if (((Relation) modelObject).getType() instanceof KernelRelationDeclaration) {
				return RUNTIME_PACKAGE + ".relations";
			}
			else {
				packageName = "relations";
			}
		}
		else if (modelObject instanceof Expression) {
			if (((Expression) modelObject).getType() instanceof KernelExpressionDeclaration) {
				return RUNTIME_PACKAGE + ".expressions";
			}
			else {
				ExpressionDeclaration decl = ((Expression) modelObject).getType();
				if (parameters.useRuntimeFilterBy && decl.getName().compareTo(CompilerParameters.FILTERBYNAME) == 0) {
					return RuntimeFilterBy.class.getPackage().getName();
				}
				else {
					packageName = "expressions";
				}
			}
		}
		else if (modelObject instanceof UserRelationDefinition) {
			packageName = "relations";
		}
		else if (modelObject instanceof UserExpressionDefinition) {
			packageName = "expressions";
		}
		else if (modelObject.eClass().getEPackage() == BasicTypePackage.eINSTANCE) {
			return basicTypeClassMapper.doSwitch(modelObject).getPackage().getName();
		}
		else {
			packageName = modelObject.getClass().getSimpleName().toLowerCase();
		}
		return parameters.packagePrefix + "." + packageName;
	}
	
	public String getVariableName(NamedElement modelObject) {
		return modelObject.getName().toLowerCase();
	}
	
	public String getVariableGetterName(NamedElement modelObject) {
		return "get" + capitalizeFirstLetter(modelObject.getName());
	}
	
	public String getClockName(Expression expression) {
		return expression.getName().toLowerCase() + "_clock";
	}
	
	final public String clockClassName = RuntimeClock.class.getSimpleName();
	final public String clockClassPackage = RuntimeClock.class.getPackage().getName();

	final public String implicitClockClassName = RuntimeClock.class.getSimpleName();
	final public String implicitClockPackage = RuntimeClock.class.getPackage().getName();
	
	private String capitalizeFirstLetter(String name) {
		if (Character.isUpperCase( name.charAt(0) ))
			return name;
		else {
			char replacement = Character.toUpperCase( name.charAt(0) );
			StringBuilder newName = new StringBuilder();
			newName.append(replacement);
			newName.append( name.substring(1) );
			return newName.toString();
		}
	}

	private String lowercaseFirstLetter(String name) {
		if (Character.isLowerCase( name.charAt(0) ))
			return name;
		else {
			char replacement = Character.toLowerCase( name.charAt(0) );
			StringBuilder newName = new StringBuilder();
			newName.append(replacement);
			newName.append( name.substring(1) );
			return newName.toString();
		}
	}

	public String getFunctionName(ClassicalExpression expression) {
		if (expression.getName().isEmpty()) {
			return "get" + capitalizeFirstLetter(genSym("expression"));
		}
		else
			return "get" + capitalizeFirstLetter(expression.getName());
	}

	public String getProviderName(NamedElement expression) {
		return lowercaseFirstLetter(expression.getName() + "Provider");
	}

	final private String symbolPrefix = "sym";
	private static int symbolCount = 0;

	public synchronized String genSym() {
		symbolCount++;
		return symbolPrefix + symbolCount;
	}

	public synchronized String genSym(String prefix) {
		symbolCount++;
		return prefix + symbolCount;
	}

	public String getTestPackageName(ClockConstraintSystem model) {
		return parameters.packagePrefix + ".tests";
	}

	public String getTestClassName(ClockConstraintSystem model) {
		return capitalizeFirstLetter(model.getName() + "Tests");
	}

	public String getMainPackageName(ClockConstraintSystem model) {
		return parameters.packagePrefix + ".main";
	}

	public String getMainClassName(ClockConstraintSystem model) {
		return capitalizeFirstLetter(model.getName() + "Main");
	}

}
