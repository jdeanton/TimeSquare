/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.ccslkernel.model.editor.TimeModel.presentation;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.ui.action.CreateChildAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ClockImpl;


public final class CreateChildCCSLModelAction extends CreateChildAction {
	public CreateChildCCSLModelAction(IEditorPart editorPart, ISelection selection, Object descriptor) {
		super(editorPart, selection, descriptor);
		changetext();
	}

	private void changetext() {
		String s= getText();
		if ( descriptor instanceof CommandParameter)
		{			
			CommandParameter cp=((CommandParameter) descriptor);
			Class<?> clazz=cp.getEStructuralFeature().getContainerClass();
			EClass c=cp.getEStructuralFeature().getEContainingClass();
			//EObject eo= cp.getEValue();
			String featurename="";
			EStructuralFeature esf= cp.getEStructuralFeature();
			if (esf!=null)
			{			
				
				if ( c.getEAllStructuralFeatures().size()==1)
				{
					return ;
				}
				if ( clazz.isAssignableFrom(Block.class))
				{
					EObject eo=cp.getEValue() ;				
					if ( eo instanceof Relation) return ;
					if ( eo instanceof Expression) return ;
					if (eo.getClass() == ClockImpl.class) return ; 
					if (eo.eClass() == CCSLModelPackage.eINSTANCE.getBlock()) return ;
					
				}
				
				if ( clazz.isAssignableFrom(Relation.class))
				{
					return ;
				}
				if ( clazz.isAssignableFrom(Expression.class))
				{
					return ;
				}
				if ( clazz.isAssignableFrom(SequenceElement.class))
				{
					return ;
				}
				featurename =esf.getName();
				setText(featurename+"|"+s);			
			}
		}
		
	}

	

	
	
	
	
}