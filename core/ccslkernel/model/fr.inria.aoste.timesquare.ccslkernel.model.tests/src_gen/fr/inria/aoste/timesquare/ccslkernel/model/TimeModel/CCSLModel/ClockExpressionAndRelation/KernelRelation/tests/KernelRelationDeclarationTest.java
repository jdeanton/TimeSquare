/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.tests;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.tests.RelationDeclarationTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Declaration</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class KernelRelationDeclarationTest extends RelationDeclarationTest {

	/**
	 * Constructs a new Declaration test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KernelRelationDeclarationTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Declaration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected KernelRelationDeclaration getFixture() {
		return (KernelRelationDeclaration)fixture;
	}

} //KernelRelationDeclarationTest
