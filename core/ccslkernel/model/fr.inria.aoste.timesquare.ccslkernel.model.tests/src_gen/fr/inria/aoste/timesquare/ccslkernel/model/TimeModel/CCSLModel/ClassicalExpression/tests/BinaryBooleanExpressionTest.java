/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.tests;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Binary Boolean Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class BinaryBooleanExpressionTest extends BooleanExpressionTest {

	/**
	 * Constructs a new Binary Boolean Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryBooleanExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Binary Boolean Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BinaryBooleanExpression getFixture() {
		return (BinaryBooleanExpression)fixture;
	}

} //BinaryBooleanExpressionTest
