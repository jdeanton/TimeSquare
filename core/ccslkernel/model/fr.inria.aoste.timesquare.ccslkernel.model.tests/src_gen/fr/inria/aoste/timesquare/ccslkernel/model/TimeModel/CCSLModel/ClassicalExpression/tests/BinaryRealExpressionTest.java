/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.tests;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Binary Real Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class BinaryRealExpressionTest extends RealExpressionTest {

	/**
	 * Constructs a new Binary Real Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryRealExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Binary Real Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BinaryRealExpression getFixture() {
		return (BinaryRealExpression)fixture;
	}

} //BinaryRealExpressionTest
