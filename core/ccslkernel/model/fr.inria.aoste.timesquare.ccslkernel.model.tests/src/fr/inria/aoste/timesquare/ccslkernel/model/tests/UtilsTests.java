/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;


public class UtilsTests {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Resource res;
		try {
			res = ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.model.tests/data/Defer.extendedCCSL");
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		}
		assertFalse(res.getContents().isEmpty());
		assertTrue(res.getContents().get(0) instanceof ClockConstraintSystem);
		ClockConstraintSystem spec = (ClockConstraintSystem) res.getContents().get(0);
		// The main block
		Block mainBlock = spec.getSuperBlock();
		for (Element element : mainBlock.getElements()) {
			String qualifiedName = CCSLKernelUtils.getQualifiedName(element);
			assertNotNull(qualifiedName);
			EObject object = CCSLKernelUtils.findObjectFromQualifiedName(qualifiedName, spec);
			assertEquals(element, object);
		}
		for (Expression expr : mainBlock.getExpressions()) {
			String qualifiedName = CCSLKernelUtils.getQualifiedName(expr);
			assertNotNull(qualifiedName);
			EObject object = CCSLKernelUtils.findObjectFromQualifiedName(qualifiedName, spec);
			assertEquals(expr, object);
		}
		for (Relation relation : mainBlock.getRelations()) {
			String qualifiedName = CCSLKernelUtils.getQualifiedName(relation);
			assertNotNull(qualifiedName);
			EObject object = CCSLKernelUtils.findObjectFromQualifiedName(qualifiedName, spec);
			assertEquals(relation, object);			
		}
		for (ClassicalExpression cexpr : mainBlock.getClassicalExpression()) {
			String qualifiedName = CCSLKernelUtils.getQualifiedName(cexpr);
			assertNotNull(qualifiedName);
			EObject object = CCSLKernelUtils.findObjectFromQualifiedName(qualifiedName, spec);
			assertEquals(cexpr, object);
		}
	}

}
