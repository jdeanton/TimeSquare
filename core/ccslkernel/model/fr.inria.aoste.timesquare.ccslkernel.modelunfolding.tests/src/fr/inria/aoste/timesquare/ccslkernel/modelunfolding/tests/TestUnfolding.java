/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding.tests;

import static org.junit.Assert.*;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;

public class TestUnfolding {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSimplePrecedes() {
		UnfoldModel umodel = null;
		try {
			umodel = UnfoldModel.unfoldModel(URI.createURI("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/SimplePrecedes.extendedCCSL"));
		} catch (Throwable e) {
			AssertionError ae = new AssertionError("Exception");
			ae.initCause(e);
			throw ae;
		}
		assertNotNull(umodel);
		int nClocks = 0;
		int nRelations = 0;
		int nExpressions = 0;
		InstantiatedElement theRelation = null;
		for (InstantiatedElement instance : umodel.getInstantiationTree().lookupInstances(null)) {
			instance.describe(System.out);
			if (instance.isClock())
				nClocks++;
			if (instance.isRelation()) {
				nRelations++;
				theRelation = instance;
			}
			if (instance.isExpression())
				nExpressions++;
		}
		assertTrue(nClocks == 2);
		assertTrue(nRelations == 1);
		assertTrue(nExpressions == 0);
		assertNotNull(theRelation);
		assertNotNull(theRelation.getAbstractMapping().resolveAbstractEntity("LeftClock"));
		assertNotNull(theRelation.getAbstractMapping().resolveAbstractEntity("RightClock"));
		assertNotNull(theRelation.getDeclaration());
		assertTrue(theRelation.getDeclaration() instanceof KernelRelationDeclaration);
		AbstractEntity left = ((KernelRelationDeclaration)(theRelation.getDeclaration())).getLeftEntity();
		InstantiatedElement leftClock = theRelation.getAbstractMapping().resolveAbstractEntity(left);
		assertNotNull(leftClock);
		AbstractEntity right = ((KernelRelationDeclaration)(theRelation.getDeclaration())).getRightEntity();
		InstantiatedElement rightClock = theRelation.getAbstractMapping().resolveAbstractEntity(right); 
		assertNotNull(rightClock);
		assertNotSame(leftClock, rightClock);
	}

	@Test
	public void testBlockScope() {
		UnfoldModel umodel = null;
		try {
			umodel = UnfoldModel.unfoldModel(URI.createURI("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.modelunfolding.tests/data/blocktest.extendedCCSL"));
		} catch (Throwable e) {
			AssertionError ae = new AssertionError("Exception");
			ae.initCause(e);
			throw ae;
		}
		assertNotNull(umodel);
		int nClocks = 0;
		int nRelations = 0;
		int nExpressions = 0;
		InstantiatedElement subClockRelation = null;
		InstantiatedElement precedesRelation = null;
		for (InstantiatedElement instance : umodel.getInstantiationTree().lookupInstances(null)) {
			instance.describe(System.out);
			if (instance.isClock())
				nClocks++;
			if (instance.isRelation()) {
				nRelations++;
				if (instance.getDeclaration().getName().equals("SubClock"))
					subClockRelation = instance;
				else if (instance.getDeclaration().getName().equals("Precedes"))
					precedesRelation = instance;
			}
			if (instance.isExpression())
				nExpressions++;
		}
		assertTrue(nClocks == 3);
		assertTrue(nRelations == 2);
		assertTrue(nExpressions == 0);
		assertNotNull(precedesRelation);
		assertNotNull(precedesRelation.getAbstractMapping().resolveAbstractEntity("LeftClock"));
		assertNotNull(precedesRelation.getAbstractMapping().resolveAbstractEntity("RightClock"));

		assertNotNull(subClockRelation);
		assertNotNull(subClockRelation.getAbstractMapping().resolveAbstractEntity("LeftClock"));
		assertNotNull(subClockRelation.getAbstractMapping().resolveAbstractEntity("RightClock"));

		List<InstantiatedElement> l1 = umodel.getInstantiationTree().lookupInstances(null, 2);
		assertEquals(2, l1.size()); // l1 contains "main::c1" and "main::c2"
		assertTrue(l1.get(0).isClock());
		assertTrue(l1.get(1).isClock());
		
		InstantiatedElement elt = umodel.getInstantiationTree().lookupInstance("main::sub1::r1");
		assertNotNull(elt);
		assertTrue(elt.isKernelRelation());
		assertTrue(elt.isLeaf());
		
		elt = umodel.getInstantiationTree().lookupInstance("main::sub1::c3");
		assertTrue(elt.isClock());
	}
	
	@Test
	public void test2() {
		UnfoldModel umodel = null;
		try {
			umodel = UnfoldModel.unfoldModel(URI.createURI("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates.extendedCCSL"));
		} catch (Throwable e) {
			AssertionError ae = new AssertionError("Exception");
			ae.initCause(e);
			throw ae;
		}
		assertNotNull(umodel);
		List<InstantiatedElement> elements = umodel.getInstantiationTree().lookupInstances(null);
		for (InstantiatedElement instance : elements) {
			instance.describe(System.out);
		}
		assertEquals(7, elements.size());
		
		InstantiatedElement elt = umodel.getInstantiationTree().lookupInstance("main::c1");
		assertNotNull(elt);
		assertTrue(elt.isClock());
		
		elt = umodel.getInstantiationTree().lookupInstance("main::c2");
		assertNotNull(elt);
		assertTrue(elt.isClock());
		
		elt = umodel.getInstantiationTree().lookupInstance("main::r1");
		assertNotNull(elt);
		assertTrue(elt.isRelation());
		assertFalse(elt.isLeaf());
		assertFalse(elt.isKernelRelation());

		elt = umodel.getInstantiationTree().lookupInstance("main::r1::AlternatesDef::Alt_c1DelayedByOne");
		assertNotNull(elt);
		assertTrue(elt.isKernelExpression());
		assertTrue(elt.isLeaf());
		
		elt = umodel.getInstantiationTree().lookupInstance("main::r1::AlternatesDef::seqOneInfinite");
		assertNotNull(elt);
		assertTrue(elt.isElement());
		assertTrue(elt.isLeaf());

		elt = umodel.getInstantiationTree().lookupInstance("main::r1::AlternatesDef::Alt_c1PrecedesC2");
		assertNotNull(elt);
		assertTrue(elt.isKernelRelation());
		
		elt = umodel.getInstantiationTree().lookupInstance("main::r1::AlternatesDef::Alt_c2precedesC1DelayedByOne");
		assertNotNull(elt);
		assertTrue(elt.isKernelRelation());
		
	}
	
	@Test
	public void test3() {
		UnfoldModel umodel = null;
		try {
			umodel = UnfoldModel.unfoldModel(URI.createURI("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates2.extendedCCSL"));
		} catch (Throwable e) {
			AssertionError ae = new AssertionError("Exception");
			ae.initCause(e);
			throw ae;
		}
		assertNotNull(umodel);
		for (InstantiatedElement instance : umodel.getInstantiationTree().lookupInstances(null)) {
			instance.describe(System.out);
		}

	}

	@Test
	public void test4() {
		UnfoldModel umodel = null;
		try {
			umodel = UnfoldModel.unfoldModel(URI.createURI("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates3.extendedCCSL"));
		} catch (Throwable e) {
			AssertionError ae = new AssertionError("Exception");
			ae.initCause(e);
			throw ae;
		}
		assertNotNull(umodel);
		for (InstantiatedElement instance : umodel.getInstantiationTree().lookupInstances(null)) {
			instance.describe(System.out);
		}

	}

	@Test
	public void test5() {
		UnfoldModel umodel = null;
		try {
			umodel = UnfoldModel.unfoldModel(URI.createURI("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/FilterBy1.extendedCCSL"));
		} catch (Throwable e) {
			AssertionError ae = new AssertionError("Exception");
			ae.initCause(e);
			throw ae;
		}
		assertNotNull(umodel);
		List<InstantiatedElement> instances = umodel.getInstantiationTree().lookupInstances(null);
		for (InstantiatedElement instance : instances) {
			instance.describe(System.out);
		}
		assertNotNull(umodel);
	}

	@Test
	public void test6() {
		UnfoldModel umodel = null;
		try {
			umodel = UnfoldModel.unfoldModel(URI.createURI("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/FilterBy2.extendedCCSL"));
		} catch (Throwable e) {
			AssertionError ae = new AssertionError("Exception");
			ae.initCause(e);
			throw ae;
		}
		assertNotNull(umodel);
		for (InstantiatedElement instance : umodel.getInstantiationTree().lookupInstances(null)) {
			instance.describe(System.out);
		}

	}

	
	
}
