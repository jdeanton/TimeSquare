/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.eclipse.emf.ecore.resource.Resource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.RecursiveDefinitionChecker;

public class RecursionTest {

	private String libUris[] = {
//			"/fr.inria.aoste.timesquare.example.cea2012/SDF_MoCC/SDF.ccslLib",
//			"/fr.inria.aoste.timesquare.example.cea2012/lib4RT/CCSL4RT.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/relationsInExpressionDef/tesDeath.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/recursiveLibrary/recursiveConcatenation.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/Timmo2use/EAST-ADL.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/supelec/Test.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/recursiveStuff/recursions.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/TestLib/Test.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/SwitchLib/switchOnOff.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/lib4RT/CCSL4RT.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/complexForPerfTests/lib4RT.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.examples/complexForPerfTests/TFSMMoC.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Watchdog.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/UpToInDefs.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.modelunfolding.tests/data/recursiveLibrary/recursiveConcatenation.ccslLib",
			"/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib"
	};
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		ResourceLoader loader = ResourceLoader.INSTANCE;
		Resource resource = null;
		for (String uri : libUris) {
			try {
				resource = loader.loadPluginResource(uri);
			} catch (IOException e) {
				AssertionError ae = new AssertionError("Exception");
				ae.initCause(e);
				throw ae;
			}
			if (resource.getContents().get(0) instanceof Library) {
				Library library = (Library) resource.getContents().get(0);
				for (ExpressionLibrary eLib : library.getExpressionLibraries()) {
					for (ExpressionDefinition definition : eLib.getExpressionDefinitions()) {
						RecursiveDefinitionChecker checker = new RecursiveDefinitionChecker(definition);
						if (checker.isRecursive()) {
							System.out.println("definition " + definition.getName() + " in lib " + uri + " is recursive.");
						}
					}
				}
			}
		}
	}

}
