/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding.tests;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.URI;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;

public class Bug14889 {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		UnfoldModel umodel = null;
		try {
			umodel = UnfoldModel.unfoldModel(URI.createURI("platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.modelunfolding.tests/data/recursiveLibrary/recursiveConcatTest1.extendedCCSL"));
		} catch (NullPointerException e) {
			AssertionError ae = new AssertionError("Null Pointer Exception: " + e.getMessage());
			ae.initCause(e);
			throw ae;			
		} catch (Throwable e) {
			AssertionError ae = new AssertionError("Exception: " + e.getMessage());
			ae.initCause(e);
			throw ae;
		}
		assertNotNull(umodel);
	
	
	
	
	}
}
