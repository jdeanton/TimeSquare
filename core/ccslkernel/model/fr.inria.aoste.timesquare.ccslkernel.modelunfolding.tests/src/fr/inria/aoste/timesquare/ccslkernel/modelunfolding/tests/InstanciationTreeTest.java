/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding.tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationTree;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.InstantiationTreeMergeConflict;

/**
 * @author nchleq
 *
 */
public class InstanciationTreeTest {

	private class MyKey extends NamedElementImpl {
		
	}
	
	private class MyInstance{

		private String name;

		public MyInstance(String name) {
			this.name = name;
		}

		@SuppressWarnings("unused")
		public String getName() {
			return name;
		}
		
		@Override
		public String toString() {
			return this.name;
		}
	}
	
	private InstantiationTree<MyInstance> tree;
	
	private MyKey key1;
	private MyKey key2;
	private MyKey key3;
	private MyKey key4;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		tree = new InstantiationTree<MyInstance>();
		key1 = new MyKey();
		key1.setName("k1");
		key2 = new MyKey();
		key2.setName("k2");
		key3 = new MyKey();
		key3.setName("k3");
		key4 = new MyKey();
		key4.setName("k4");
	}

	/**
	 * Test method for {@link fr.inria.aoste.timesquare.ccslkernel.solver.InstantiationTree#storeInstance(java.util.List, fr.inria.aoste.timesquare.ccslkernel.solver.runobject.ISolverRunObject)}.
	 */
	@Test
	public void testStoreInstance1() {
		InstantiationPath path = new InstantiationPath();
		path.add(key1);
		MyInstance inst = new MyInstance("o1");
		MyInstance v1 = (MyInstance) tree.lookupInstance(path);
		assertNull(v1);
		tree.storeInstance(path, inst);
		v1 = (MyInstance) tree.lookupInstance(path);
		assertNotNull(v1);
		assertEquals(inst, v1);
		
		String qualifiedPathName = path.getQualifiedName();
		v1 = tree.lookupInstance(qualifiedPathName);
		assertNotNull(v1);
		assertEquals(inst, v1);
		
	}

	@Test
	public void testStoreInstance2() {
		InstantiationPath path = new InstantiationPath();
		path.add(key1);
		path.add(key2);
		MyInstance inst = new MyInstance("o1");
		MyInstance v1 = (MyInstance) tree.lookupInstance(path);
		assertNull(v1);
		tree.storeInstance(path, inst);
		v1 = (MyInstance) tree.lookupInstance(path);
		assertNotNull(v1);
		assertEquals(inst, v1);
	}

	
	@Test
	public void testStoreInstance3() {
		InstantiationPath path1 = new InstantiationPath();
		path1.add(key1);
		path1.add(key2);
		InstantiationPath path2 = new InstantiationPath();
		path2.add(key2);
		path2.add(key3);
		path2.add(key4);
		
		MyInstance inst1 = new MyInstance("o1");
		MyInstance inst2 = new MyInstance("o2");
		MyInstance v1 = (MyInstance) tree.lookupInstance(path1);
		assertNull(v1);
		MyInstance v2 = (MyInstance) tree.lookupInstance(path2);
		assertNull(v2);
		tree.storeInstance(path1, inst1);
		tree.storeInstance(path2, inst2);
		v1 = (MyInstance) tree.lookupInstance(path1);
		v2 = (MyInstance) tree.lookupInstance(path2);
		assertNotNull(v1);
		assertEquals(inst1, v1);
		assertNotNull(v2);
		assertEquals(inst2, v2);
	}

	@Test
	public void testlookupInstances1() {
		InstantiationPath path1 = new InstantiationPath();
		path1.add(key1);
		path1.add(key2);
		InstantiationPath path2 = new InstantiationPath();
		path2.add(key1);
		path2.add(key2);
		path2.add(key3);
		InstantiationPath path3 = new InstantiationPath();
		path3.add(key1);
		path3.add(key2);
		path3.add(key4);
		
		
		MyInstance inst1 = new MyInstance("o1");
		MyInstance inst2 = new MyInstance("o2");
		MyInstance inst3 = new MyInstance("o3");
		
		MyInstance v1 = (MyInstance) tree.lookupInstance(path1);
		assertNull(v1);
		MyInstance v2 = (MyInstance) tree.lookupInstance(path2);
		assertNull(v2);
		MyInstance v3 = (MyInstance) tree.lookupInstance(path3);
		assertNull(v3);
		
		tree.storeInstance(path1, inst1);
		tree.storeInstance(path2, inst2);
		tree.storeInstance(path3, inst3);
		
		v1 = (MyInstance) tree.lookupInstance(path1);
		v2 = (MyInstance) tree.lookupInstance(path2);
		v3 = (MyInstance) tree.lookupInstance(path3);
		assertNotNull(v1);
		assertEquals(inst1, v1);
		assertNotNull(v2);
		assertEquals(inst2, v2);
		assertNotNull(v3);
		assertEquals(inst3, v3);
		
		List<MyInstance> lst1 = tree.lookupInstances(path3);
		assertEquals(1, lst1.size());
		assertTrue(lst1.contains(inst3));
		List<MyInstance> lst2 = tree.lookupInstances(path1);
		assertEquals(3, lst2.size());
		assertTrue(lst2.contains(inst1));
		assertTrue(lst2.contains(inst2));
		assertTrue(lst2.contains(inst3));
		
		InstantiationPath path4 = new InstantiationPath();
		assertTrue(path4.isEmpty());
		List<MyInstance> lst3 = tree.lookupInstances(path4);
		assertEquals(3, lst3.size());
		assertTrue(lst3.contains(inst1));
		assertTrue(lst3.contains(inst2));
		assertTrue(lst3.contains(inst3));
		
	}

	@Test
	public void testlookupInstances2() {
		InstantiationPath path1 = new InstantiationPath();
		path1.add(key1);
		path1.add(key2);
		InstantiationPath path2 = new InstantiationPath();
		path2.add(key1);
		path2.add(key2);
		path2.add(key3);
		InstantiationPath path3 = new InstantiationPath();
		path3.add(key1);
		path3.add(key2);
		path3.add(key4);


		MyInstance inst1 = new MyInstance("o1");
		MyInstance inst2 = new MyInstance("o2");
		MyInstance inst3 = new MyInstance("o3");

		tree.storeInstance(path1, inst1);
		tree.storeInstance(path2, inst2);
		tree.storeInstance(path3, inst3);
		
		List<MyInstance> lst4 = tree.getParentList(inst1);
		assertEquals(0, lst4.size());
		assertFalse(lst4.contains(inst1));

		List<MyInstance> lst5 = tree.getParentList(inst2);
		assertEquals(1, lst5.size());
		assertTrue(lst5.contains(inst1));
		assertFalse(lst5.contains(inst2));
		
		List<MyInstance> lst6 = tree.getParentList(inst3);
		assertEquals(1, lst6.size());
		assertTrue(lst6.contains(inst1));
		assertFalse(lst6.contains(inst2));
		assertFalse(lst6.contains(inst3));

	}

	@Test
	public void testlookupInstances3() {
		InstantiationPath path1 = new InstantiationPath();
		path1.add(key1);
		path1.add(key2);
		InstantiationPath path2 = new InstantiationPath();
		path2.add(key1);
		path2.add(key2);
		path2.add(key3);
		InstantiationPath path3 = new InstantiationPath();
		path3.add(key1);
		path3.add(key2);
		path3.add(key4);
		InstantiationPath path4 = new InstantiationPath();
		path4.add(key1);
		path4.add(key3);
		InstantiationPath path5 = new InstantiationPath();
		path5.add(key1);
		path5.add(key3);
		path5.add(key4);
		path5.add(key1);


		MyInstance inst1 = new MyInstance("o1");
		MyInstance inst2 = new MyInstance("o2");
		MyInstance inst3 = new MyInstance("o3");
		MyInstance inst4 = new MyInstance("o4");
		MyInstance inst5 = new MyInstance("o5");

		tree.storeInstance(path1, inst1);
		tree.storeInstance(path2, inst2);
		tree.storeInstance(path3, inst3);
		tree.storeInstance(path4, inst4);
		tree.storeInstance(path5, inst5);
		
		/*
		 *                 root
		 *                   |
		 *                   | k1
		 *                   |
		 *                   o
		 *                   |\       
		 *                k2 | \k3
		 *                   |  \
		 *                o1 o   o-o4
		 *                  / \      \
		 *              k3 /   \k4    \k4
		 *                /     \      \
		 *            o2 o       o o3   o
		 *                              |
		 *                              | k1
		 *                              |
		 *                              o o5
		 */
		
		List<MyInstance> lst1 = tree.getParentList(inst1);
		assertEquals(0, lst1.size());
		assertFalse(lst1.contains(inst1));

		List<MyInstance> lst2 = tree.getParentList(inst2);
		assertEquals(1, lst2.size());
		assertTrue(lst2.contains(inst1));
		assertFalse(lst2.contains(inst2));
		
		List<MyInstance> lst3 = tree.getParentList(inst3);
		assertEquals(1, lst3.size());
		assertTrue(lst3.contains(inst1));
		assertFalse(lst3.contains(inst2));
		assertFalse(lst3.contains(inst3));
		
		List<MyInstance> lst4 = tree.getParentList(inst5);
		assertEquals(1, lst4.size());
		assertTrue(lst4.contains(inst4));
		assertFalse(lst4.contains(inst5));
	
		List<MyInstance> lst5 = tree.lookupInstances(null, 0);
		assertTrue(lst5.isEmpty());
		
		List<MyInstance> lst6 = tree.lookupInstances(null, 1);
		assertTrue(lst6.isEmpty());
		
		List<MyInstance> lst7 = tree.lookupInstances(null, 2);
		assertEquals(2, lst7.size());
		assertTrue(lst7.contains(inst1));
		assertTrue(lst7.contains(inst4));
		
		List<MyInstance> lst8 = tree.lookupInstances(null, 3);
		assertEquals(4, lst8.size());
		assertTrue(lst8.contains(inst1));
		assertTrue(lst8.contains(inst2));
		assertTrue(lst8.contains(inst3));
		assertTrue(lst8.contains(inst4));

		InstantiationPath path6 = new InstantiationPath();
		path6.add(key1);
		path6.add(key2);
		List<MyInstance> lst9 = tree.lookupInstances(path6, 0);
		assertEquals(1, lst9.size());
		assertTrue(lst9.contains(inst1));		
		List<MyInstance> lst10 = tree.lookupInstances(path6, 1);
		assertEquals(3, lst10.size());
		assertTrue(lst10.contains(inst1));
		assertTrue(lst10.contains(inst2));
		assertTrue(lst10.contains(inst3));

		InstantiationPath path7 = new InstantiationPath();
		path7.add(key1);
		path7.add(key3);
		List<MyInstance> lst11 = tree.lookupInstances(path7, 0);
		assertEquals(1, lst11.size());
		assertTrue(lst11.contains(inst4));
		path7.add(key4);
		List<MyInstance> lst12 = tree.lookupInstances(path7, 0);
		assertTrue(lst12.isEmpty());
		List<MyInstance> lst13 = tree.lookupInstances(path7, 1);
		assertEquals(1, lst13.size());
		assertTrue(lst13.contains(inst5));
	}
	
	@Test
	public void testMerge1() {
		/*
		 *      tree1
		 *        |
		 *        | k1
		 *        |
		 *        o1
		 *
		 * merged with :
		 * 
		 *      tree2
		 *        |
		 *        | k2
		 *        |
		 *        o2
		 *        
		 * should give :
		 * 
		 *      tree1
		 *        /\
		 *    k1 /  \ k2
		 *      /    \
		 *     o1    o2
		 */
		MyInstance inst1 = new MyInstance("o1");
		MyInstance inst2 = new MyInstance("o2");

		InstantiationTree<MyInstance> tree1 = new InstantiationTree<InstanciationTreeTest.MyInstance>();
		InstantiationPath path1 = new InstantiationPath();
		path1.add(key1);
		tree1.storeInstance(path1, inst1);
		
		InstantiationTree<MyInstance> tree2 = new InstantiationTree<InstanciationTreeTest.MyInstance>();
		InstantiationPath path2 = new InstantiationPath();
		path2.add(key2);
		tree2.storeInstance(path2, inst2);
		
		try {
			tree1.mergeWith(tree2);
		} catch (InstantiationTreeMergeConflict e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		MyInstance o1 = tree1.lookupInstance(path1);
		assertNotNull(o1);
		assertEquals(inst1, o1);
		MyInstance o2 = tree1.lookupInstance(path2);
		assertNotNull(o2);
		assertEquals(inst2, o2);
	}
	
	@Test
	public void testMerge2() {
		/*
		 * merges   tree1    with     tree2
		 *            |                 |
		 *            | k1              | k1
		 *            |                 |
		 *            +                 +
		 *            |                 |
		 *            | k2              | k3
		 *            |                 |
		 *           o1                o2
		 *           
		 * should produce :
		 * 
		 *          tree1
		 *            |
		 *            | k1
		 *            |
		 *            +
		 *           / \
		 *       k2 /   \ k3
		 *         /     \
		 *        o1     o2
		 */
		MyInstance inst1 = new MyInstance("o1");
		MyInstance inst2 = new MyInstance("o2");

		InstantiationTree<MyInstance> tree1 = new InstantiationTree<InstanciationTreeTest.MyInstance>();
		InstantiationPath path1 = new InstantiationPath();
		path1.add(key1);
		path1.add(key2);
		tree1.storeInstance(path1, inst1);
		
		InstantiationTree<MyInstance> tree2 = new InstantiationTree<InstanciationTreeTest.MyInstance>();
		InstantiationPath path2 = new InstantiationPath();
		path2.add(key1);
		path2.add(key3);
		tree2.storeInstance(path2, inst2);
		
		try {
			tree1.mergeWith(tree2);
		} catch (InstantiationTreeMergeConflict e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		MyInstance o1 = tree1.lookupInstance(path1);
		assertNotNull(o1);
		assertEquals(inst1, o1);
		MyInstance o2 = tree1.lookupInstance(path2);
		assertNotNull(o2);
		assertEquals(inst2, o2);
		
		InstantiationPath path3 = new InstantiationPath();
		path3.add(key1);
		assertNull(tree1.lookupInstance(path3));
	}
	
	@Test
	public void testMerge3() {
		MyInstance inst1 = new MyInstance("o1");
		MyInstance inst2 = new MyInstance("o2");
		MyInstance inst3 = new MyInstance("o3");

		InstantiationTree<MyInstance> tree1 = new InstantiationTree<InstanciationTreeTest.MyInstance>();
		InstantiationPath path1 = new InstantiationPath();
		path1.add(key1);
		path1.add(key2);
		tree1.storeInstance(path1, inst1);
		InstantiationPath path2 = new InstantiationPath();
		path2.add(key1);
		path2.add(key3);
		tree1.storeInstance(path2, inst2);
		
		InstantiationTree<MyInstance> tree2 = new InstantiationTree<InstanciationTreeTest.MyInstance>();
		tree2.storeInstance(path2, inst3);
		
		try {
			tree1.mergeWith(tree2);
		} catch (InstantiationTreeMergeConflict e) {
			// success
			return;
		}
		fail("Failed to get InstantiationTreeMergeConflict exception");
	}
	
	
	@Test
	public void testMerge4() {
		/* builds two trees so that when merging them we get the same tree as 
		 * the testlookupInstances3() test.
		 */
		InstantiationPath path1 = new InstantiationPath();
		path1.add(key1);
		path1.add(key2);
		InstantiationPath path2 = new InstantiationPath();
		path2.add(key1);
		path2.add(key2);
		path2.add(key3);
		InstantiationPath path3 = new InstantiationPath();
		path3.add(key1);
		path3.add(key2);
		path3.add(key4);


		MyInstance inst1 = new MyInstance("o1");
		MyInstance inst2 = new MyInstance("o2");
		MyInstance inst3 = new MyInstance("o3");
	
		tree.storeInstance(path1, inst1);
		tree.storeInstance(path2, inst2);
		tree.storeInstance(path3, inst3);

		InstantiationTree<MyInstance> tree2 = new InstantiationTree<InstanciationTreeTest.MyInstance>();
		
		InstantiationPath path4 = new InstantiationPath();
		path4.add(key1);
		path4.add(key3);
		InstantiationPath path5 = new InstantiationPath();
		path5.add(key1);
		path5.add(key3);
		path5.add(key4);
		path5.add(key1);

		MyInstance inst4 = new MyInstance("o4");
		MyInstance inst5 = new MyInstance("o5");
		
		tree2.storeInstance(path4, inst4);
		tree2.storeInstance(path5, inst5);
		
		try {
			tree.mergeWith(tree2);
		} catch (InstantiationTreeMergeConflict e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		List<MyInstance> lst1 = tree.getParentList(inst1);
		assertEquals(0, lst1.size());
		assertFalse(lst1.contains(inst1));

		List<MyInstance> lst2 = tree.getParentList(inst2);
		assertEquals(1, lst2.size());
		assertTrue(lst2.contains(inst1));
		assertFalse(lst2.contains(inst2));
		
		List<MyInstance> lst3 = tree.getParentList(inst3);
		assertEquals(1, lst3.size());
		assertTrue(lst3.contains(inst1));
		assertFalse(lst3.contains(inst2));
		assertFalse(lst3.contains(inst3));
		
		List<MyInstance> lst4 = tree.getParentList(inst5);
		assertEquals(1, lst4.size());
		assertTrue(lst4.contains(inst4));
		assertFalse(lst4.contains(inst5));
	
		List<MyInstance> lst5 = tree.lookupInstances(null, 0);
		assertTrue(lst5.isEmpty());
		
		List<MyInstance> lst6 = tree.lookupInstances(null, 1);
		assertTrue(lst6.isEmpty());
		
		List<MyInstance> lst7 = tree.lookupInstances(null, 2);
		assertEquals(2, lst7.size());
		assertTrue(lst7.contains(inst1));
		assertTrue(lst7.contains(inst4));
		
		List<MyInstance> lst8 = tree.lookupInstances(null, 3);
		assertEquals(4, lst8.size());
		assertTrue(lst8.contains(inst1));
		assertTrue(lst8.contains(inst2));
		assertTrue(lst8.contains(inst3));
		assertTrue(lst8.contains(inst4));

		InstantiationPath path6 = new InstantiationPath();
		path6.add(key1);
		path6.add(key2);
		List<MyInstance> lst9 = tree.lookupInstances(path6, 0);
		assertEquals(1, lst9.size());
		assertTrue(lst9.contains(inst1));		
		List<MyInstance> lst10 = tree.lookupInstances(path6, 1);
		assertEquals(3, lst10.size());
		assertTrue(lst10.contains(inst1));
		assertTrue(lst10.contains(inst2));
		assertTrue(lst10.contains(inst3));

		InstantiationPath path7 = new InstantiationPath();
		path7.add(key1);
		path7.add(key3);
		List<MyInstance> lst11 = tree.lookupInstances(path7, 0);
		assertEquals(1, lst11.size());
		assertTrue(lst11.contains(inst4));
		path7.add(key4);
		List<MyInstance> lst12 = tree.lookupInstances(path7, 0);
		assertTrue(lst12.isEmpty());
		List<MyInstance> lst13 = tree.lookupInstances(path7, 1);
		assertEquals(1, lst13.size());
		assertTrue(lst13.contains(inst5));


	}

}
