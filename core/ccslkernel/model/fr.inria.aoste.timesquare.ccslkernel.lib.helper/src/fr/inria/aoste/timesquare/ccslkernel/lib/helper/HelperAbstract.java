/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.lib.helper;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.xtext.util.BindingValidation;

public abstract class HelperAbstract {

	public EObject findDeclaration(Resource r, String name) 
	{
		for (EObject eo2 : org.eclipse.xtext.EcoreUtil2.eAllContentsAsList(r)) 
		{
			// System.out.println(eo2);
			if ((eo2 instanceof RelationDeclaration) || (eo2 instanceof ExpressionDeclaration)) {				 
				if( name.equals(((NamedElement) eo2).getName()))
						return eo2;
				continue;
			}
			if (eo2 instanceof Element)
					{
					if (eo2.eContainer() instanceof ExpressionLibrary) 
					{				 
						if( name.equals(((NamedElement) eo2).getName()))
							return eo2;
						continue;
					}
					System.out.println("eo2" +eo2.eContainer().eClass().getName());
					}
			if (eo2 instanceof Type)
			{							 
				if( name.equals(((NamedElement) eo2).getName()))
					return eo2;	
			
			}

		}
		throw new RuntimeException("not found name:"+name);
		
	}
	
	public EObject findParametre(EObject eo, String name) 
	{
		if( eo==null)
			return null;
		for (EObject b : eo.eContents()) {

			if (b instanceof AbstractEntity) {
				if( name.equals(((NamedElement) b).getName()))
					return b;
			}

		}		
		throw new RuntimeException("not found name"+name);
		
	}
	
	public Binding getBinding(Relation eo , AbstractEntity abstractEntity)
	{
		for( Binding b:eo.getBindings())
		{
			if( b.getAbstract()==abstractEntity)
				return b;
		}
		return null;
	}
	
	public Binding getBinding(Expression eo , AbstractEntity abstractEntity)
	{
		for( Binding b:eo.getBindings())
		{
			if( b.getAbstract()==abstractEntity)
				return b;
		}
		return null;
	}
	
	public void setBinding(Relation eo , AbstractEntity abstractEntity, BindableEntity bindableEntity)
	{
		if (eo.getType()!=abstractEntity.eContainer())
		{
			return;
		}
		if ( BindingValidation.validateBinding(abstractEntity, bindableEntity).getSeverity()==IStatus.ERROR)
		{
			System.err.println("Error binding");
			throw new RuntimeException("binding error");
		}
		Binding b=getBinding(eo,abstractEntity);
		if( b==null)
		{
			b= ClockExpressionAndRelationFactory.eINSTANCE.createBinding();
			b.setAbstract(abstractEntity);
			eo.getBindings().add(b);
				
		}
		b.setBindable(bindableEntity);
	}

	public void setBinding(Expression eo , AbstractEntity abstractEntity, BindableEntity bindableEntity)
	{
		if (eo.getType()!=abstractEntity.eContainer())
		{
			return;
		}
		if ( BindingValidation.validateBinding(abstractEntity, bindableEntity).getSeverity()==IStatus.ERROR)
		{
			System.err.println("Error binding");
			throw new RuntimeException("binding error");
		}
		Binding b=getBinding(eo,abstractEntity);
		if( b==null)
		{
			b= ClockExpressionAndRelationFactory.eINSTANCE.createBinding();
			b.setAbstract(abstractEntity);
			eo.getBindings().add(b);
				
		}
		b.setBindable(bindableEntity);
	}
	
	
	public BindableEntity getBindableEntity(Relation eo , AbstractEntity abstractEntity)
	{
		if (eo.getType()!=abstractEntity.eContainer())
			return null;
		Binding b=getBinding(eo,abstractEntity);
		if( b!=null)
			return b.getBindable();
		return null;
	}
	
	public BindableEntity getBindableEntity(Expression eo , AbstractEntity abstractEntity)
	{
		
		if (eo.getType()!=abstractEntity.eContainer())
			return null;
		Binding b=getBinding(eo,abstractEntity);
		if( b!=null)
			return b.getBindable();
		return null;
	}
	
	
	public abstract boolean knownRelation(Relation relation);
	public abstract boolean knownExpression(Expression expression);
	
	public abstract  Enum<?> getRelationEnum(Relation relation);

	public abstract  Enum<?> getExpressionEnum(Expression expression);
}
