/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.lib.helper;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

public class CCSLUtil {

	
	final static EStructuralFeature esf=TimeModelPackage.eINSTANCE.getNamedElement_Name();
	
	
	/***
	 *  @param eObject
	 *  give a unique name for all NamedElement  contains in the  eObject
	 */
	public static void updateName(EObject eObject )
	{
		List<NamedElement> lne= new ArrayList<NamedElement>();
		TreeIterator<EObject> iterator = eObject.eAllContents();
		while (iterator.hasNext()) {
			EObject eo= iterator.next();
			if (eo instanceof NamedElement)
				lne.add((NamedElement) eo);
		}
		
		List<NamedElement> lne2= new ArrayList<NamedElement>();
		List<String> lname= new ArrayList<String>();
		for (NamedElement ne:lne)
		{
			String s= ne.getName();
			if( s==null || (s.length()==0))
			{
				lne2.add(ne);
			}
			else
			{
				if( !lname.contains(s)  && ne.eIsSet(esf))
				{
					lname.add(s);
				}
				else
				{
					lne2.add(ne);
				}
			}
		}
		for (NamedElement ne:lne2)
		{
			String base= ne.getName();
			if (base==null || (base.length()==0 )|| !ne.eIsSet(esf))
				base= ne.eClass().getName();
			String newname=generateName(lname,base);
			ne.setName(newname);
			lname.add(newname);
		}
		
	}
	
	private static String generateName(List<String> lst, String base)
	{
		String tmp=null;
		long i=0;
		while (true)
		{
			i++;
			tmp= base +"_" +i;
			if( !lst.contains(tmp))
				return tmp;
		}
		
	}
}
