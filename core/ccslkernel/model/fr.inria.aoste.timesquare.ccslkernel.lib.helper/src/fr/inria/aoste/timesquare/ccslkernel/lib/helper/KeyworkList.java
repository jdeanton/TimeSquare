/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.lib.helper;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.AbstractRule;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.Keyword;

import com.google.inject.Injector;

import fr.inria.aoste.timesquare.ccslkernel.library.xtext.CCSLLibraryStandaloneSetup;
import fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSLStandaloneSetup;



public class KeyworkList {

	
	
		
	public static final Collection<String> LIBRARY_KEYWORD= computeLibrary();

	public static final Collection<String> CCSL_KEYWORD= computeCCSL();

	private static Collection<String> computeLibrary() {
		
		CCSLLibraryStandaloneSetup.doSetup();
		Injector injector = new CCSLLibraryStandaloneSetup().createInjector();
		return compute(injector);
	}


	private static Collection<String> computeCCSL() {
	
		ExtendedCCSLStandaloneSetup.doSetup();		
		Injector injector =  new ExtendedCCSLStandaloneSetup().createInjector();
		return compute(injector);
	}


	private static Collection<String> compute (Injector injector)
	{
	//	System.out.println("in");
		Set<String> list=new HashSet<String>();
		for(AbstractRule rules: injector.getInstance(IGrammarAccess.class).getGrammar().getRules())
		{
		//	System.out.println(rules);
			TreeIterator<EObject> i = rules.eAllContents();
			while (i.hasNext()) {
				EObject o = i.next();
				if (o instanceof Keyword) {
					Keyword k = (Keyword) o;
					if (!k.isPredicated())
					{					
						list.add(k.getValue());
					}
				}
			}
		}
		//System.out.println("out");
		return Collections.unmodifiableSet(list);
	}
	
	
	
}
