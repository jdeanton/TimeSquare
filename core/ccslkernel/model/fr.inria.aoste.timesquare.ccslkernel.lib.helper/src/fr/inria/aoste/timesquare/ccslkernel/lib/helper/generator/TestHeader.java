/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.lib.helper.generator;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.junit.Assert;
import org.junit.Test;

import com.google.inject.Injector;

import fr.inria.aoste.timesquare.ccslkernel.lib.helper.CCSLUtil;
import fr.inria.aoste.timesquare.ccslkernel.lib.helper.KernelHelper;
import fr.inria.aoste.timesquare.ccslkernel.lib.helper.KeyworkList;
import fr.inria.aoste.timesquare.ccslkernel.lib.helper.LibraryHelper;
import fr.inria.aoste.timesquare.ccslkernel.library.xtext.CCSLLibraryStandaloneSetup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSLStandaloneSetup;

public class TestHeader {

	@Test
	public void testLib() throws Throwable {
		LibraryHelper lh = new LibraryHelper();
		checkClass(lh);
	}

	@Test
	public void testKernel() throws Throwable {
		KernelHelper kh = new KernelHelper();
		checkClass(kh);
	}

	private Binding createBinding(AbstractEntity abstractEntity, BindableEntity bindableEntity) {
		Binding b = ClockExpressionAndRelationFactory.eINSTANCE.createBinding();
		b.setAbstract(abstractEntity);
		b.setBindable(bindableEntity);
		return b;
	}

	@Test
	public void example() throws IllegalArgumentException, IllegalAccessException, IOException {
		{

			CCSLLibraryStandaloneSetup.doSetup();
			ExtendedCCSLStandaloneSetup.doSetup();
			ExtendedCCSLStandaloneSetup ess = new ExtendedCCSLStandaloneSetup();
			Injector injector = ess.createInjector();
			XtextResourceSet rs = injector.getInstance(XtextResourceSet.class);			
			rs.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);

			Resource r = rs.createResource(URI.createFileURI("out/test.extendedCCSL"));
			ClockConstraintSystem cs = CCSLModelFactory.eINSTANCE.createClockConstraintSystem();
			r.getContents().add(cs);

			ImportStatement e = TimeModelFactory.eINSTANCE.createImportStatement();
			e.setAlias("kernel");
			e.setImportURI(KernelHelper.resourceURI);
			cs.getImports().add(e);
			e = TimeModelFactory.eINSTANCE.createImportStatement();
			e.setAlias("lib");
			e.setImportURI(LibraryHelper.resourceURI);
			cs.getImports().add(e);
			
			EcoreUtil.resolveAll(rs);
			KernelHelper kh = new KernelHelper(rs);
			LibraryHelper lh = new LibraryHelper(rs);

			Block b = CCSLModelFactory.eINSTANCE.createBlock();
			cs.setName("system");
			b.setName("test");

			cs.getSubBlock().add(b);
			cs.setSuperBlock(b);
			Clock c1 = TimeModelFactory.eINSTANCE.createClock();
		//	c1.setName("c1");
			
			Clock c2 = TimeModelFactory.eINSTANCE.createClock();
		//	c2.setName("c2");
			Clock c3 = TimeModelFactory.eINSTANCE.createClock();
		//	c3.setName("c3");
			c3.setType(kh.DenseClockType_Dense);
			b.getElements().add(c1);
			b.getElements().add(c2);
			b.getElements().add(c3);
			Expression expression = null;
			expression = ClockExpressionAndRelationFactory.eINSTANCE.createExpression();
			expression.setName("exp");
			expression.setType(kh.NonStrictSample);
			expression.getBindings().add(createBinding(kh.Sample_SampledClock, c1));
			expression.getBindings().add(createBinding(kh.Sample_SamplingClock, c2));
			b.getExpressions().add(expression);
			/*if (r instanceof XtextResource) {
				 ((XtextResource) r).
			}*/
			
			IntegerElement ie= BasicTypeFactory.eINSTANCE.createIntegerElement();
			//ie.setName("number");
			ie.setValue(Integer.valueOf(12));
			b.getElements().add(ie);
			Relation relation = ClockExpressionAndRelationFactory.eINSTANCE.createRelation();
			relation.setName("r");
			relation.setType(lh.Alternates);
			relation.getBindings().add(createBinding(lh.Alternates_AlternatesLeftClock, c1));
			relation.getBindings().add(createBinding(lh.Alternates_AlternatesRightClock, c2));
			b.getRelations().add(kh.createCoincides("testConcidence",c1, c2));
					
			b.getRelations().add(relation);
			 expression = ClockExpressionAndRelationFactory.eINSTANCE.createExpression();
			expression.setName("exp");
			expression.setType(kh.Defer);
			expression.getBindings().add(createBinding(kh.Defer_DelayClock, c1));
			expression.getBindings().add(createBinding(kh.Defer_BaseClock, c2));
			expression.getBindings().add(createBinding(kh.Defer_DelayPatternExpression, ie));
			b.getExpressions().add(expression);
			CCSLUtil.updateName(cs);
			r.save(Collections.emptyMap());	
			}
		{

			ExtendedCCSLStandaloneSetup ess = new ExtendedCCSLStandaloneSetup();
			Injector injector = ess.createInjector();
			XtextResourceSet rs2 = injector.getInstance(XtextResourceSet.class);
			Resource r2 = rs2.getResource(URI.createURI("out/test.extendedCCSL"), true);
			EObject eo = r2.getContents().get(0);
			EcoreUtil.resolveAll(rs2);
			Diagnostic d = Diagnostician.INSTANCE.validate(eo);
			int actualerror = d.getChildren().size();
			assertEquals("Error Validation ", 0, actualerror);

		}
	}
	
	
	@Test
	public void example2() throws IllegalArgumentException, IllegalAccessException, IOException 
		{
			ExtendedCCSLStandaloneSetup ess = new ExtendedCCSLStandaloneSetup();
			Injector injector = ess.createInjector();
			XtextResourceSet rs = injector.getInstance(XtextResourceSet.class);
			Resource r = rs.getResource(URI.createURI("in/basic.extendedCCSL"), true);
			EObject eo = r.getContents().get(0);
			EcoreUtil.resolveAll(rs);
			URI uri2=URI.createURI("out/basic.extendedCCSL");
			r.setURI(uri2);
			KernelHelper kh = new KernelHelper(rs);
			Block b= (( ClockConstraintSystem) eo).getSuperBlock();
			int cptprecedes=0;
			int cptexclusion=0;
			for (Relation rel :b.getRelations())
			{
				if( rel.getType() ==kh.Precedes)
					cptprecedes++;
				if( rel.getType() ==kh.Exclusion)
					cptexclusion++;
			}
			assertEquals("Precedes ", 1, cptprecedes);
			assertEquals("Exclusion ", 1, cptexclusion);
			
			cptprecedes=0;
		    cptexclusion=0;
				for (Relation rel :b.getRelations())
				{
					if( rel.getType() ==kh.Precedes)
						cptprecedes++;
					if( rel.getType() ==kh.Exclusion)
						cptexclusion++;
				}
				assertEquals("Precedes ", 1, cptprecedes);
				assertEquals("Exclusion ", 1, cptexclusion);
			for( Expression expr: b.getExpressions())
			{
				if (expr.getType()==kh.Union)
				{
					expr.setType(kh.Intersection);
					for( Binding bin : expr.getBindings())
					{
						if( bin.getAbstract()==kh.Union_Clock1)
							bin.setAbstract(kh.Intersection_Clock1);
						if( bin.getAbstract()==kh.Union_Clock2)
							bin.setAbstract(kh.Intersection_Clock2);
					}
					
				}
			}
			Clock c1 = TimeModelFactory.eINSTANCE.createClock();
			c1.setName("test1");			
			Clock c2 = TimeModelFactory.eINSTANCE.createClock();
			c2.setName("test2");			
			b.getElements().add(c1);
			b.getElements().add(c2);
			
			
			RealElement re=BasicTypeFactory.eINSTANCE.createRealElement();
			re.setName("areal");			 
			re.setValue(Float.valueOf(0.000005f));
			b.getElements().add(re);
			re=BasicTypeFactory.eINSTANCE.createRealElement();
			re.setName("areal2");			 
			re.setValue(Float.valueOf(0.000000005f));
			b.getElements().add(re);
			
			Expression expression = null;
			expression = ClockExpressionAndRelationFactory.eINSTANCE.createExpression();
			expression.setName("sampled");
			expression.setType(kh.Intersection);			
					
			
			b.getExpressions().add(expression);
			
			
			expression.getBindings().add(createBinding(kh.Intersection_Clock1, c1));
			expression.getBindings().add(createBinding(kh.Intersection_Clock2, c2));
		
			Diagnostic d = Diagnostician.INSTANCE.validate(eo);
			int actualerror = d.getChildren().size();
			assertEquals("Error Validation ", 0, actualerror);
			
			r.save(Collections.emptyMap());	
		}		
	
	

	private void checkClass(Object h) throws IllegalArgumentException, IllegalAccessException {
		HashMap<String, EObject> map = new HashMap<String, EObject>();
		for (Field f : h.getClass().getDeclaredFields()) {
			System.out.println(f.getName() + " " + f.get(h));
			if (EObject.class.isAssignableFrom(f.getType())) {
				map.put(f.getName(), (EObject) f.get(h));
			}
		}
		System.out.println(map.size());
		for (Map.Entry<String, EObject> entry : map.entrySet()) {
			int i = entry.getKey().indexOf("_");
			if (i != -1) {
				if (entry.getValue() instanceof AbstractEntity) {
					String base = entry.getKey().substring(0, i);
					EObject eo = map.get(base);
					Assert.assertEquals(eo, entry.getValue().eContainer());
				}
			}
		}
	}
	@Test
	public void listKeyword()
	{
		System.out.println("...");
		for (String s: KeyworkList.CCSL_KEYWORD)
			System.out.println(s);
		System.out.println("...");
		for (String s: KeyworkList.LIBRARY_KEYWORD)
			System.out.println(s);
	}

}
