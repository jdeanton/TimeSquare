/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.visitor;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Char;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Real;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.util.BasicTypeSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntDivide;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqDecr;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnarySeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Xor;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util.ClassicalExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Death;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.util.ClockExpressionAndRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.util.CCSLModelSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.util.TimeModelSwitch;

public abstract  class VisitorVoid {
	
	protected void visitNamedElement(NamedElement e)
	{
	}

	protected void visitClock(Clock c)
	{
	}

	protected void visitEvent(Event e)
	{
	}

	protected void visitImportStatement(ImportStatement st)
	{
	}

	protected void visitClockConstraintSystem(ClockConstraintSystem o)
	{
	}

	protected void visitBlock(Block object)
	{
	}

	protected void visitType(Type ty)
	{
	}

	protected void visitElement(Element el)
	{
	}

	protected void  visitPrimitiveType(PrimitiveType ty)
	{
	}

	protected void visitPrimitiveElement(PrimitiveElement el)
	{
	}

	protected void visitExpression(Expression ex)
	{
	}

	protected void visitUserExpressionDefinition(UserExpressionDefinition d)
	{
	}
	protected void visitExpressionLibrary(ExpressionLibrary l)
	{
	}

	protected void visitBinding(Binding b)
	{
	}
	
	protected void visitConditionalExpressionDefinition(ConditionalExpressionDefinition cd)
	{
	}
	
	
	private final TimeModelSwitch<Boolean> timeModelVisitor = new TimeModelSwitch<Boolean>()
	{
		@Override
		public Boolean caseNamedElement(NamedElement object) {
			visitNamedElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseClock(Clock c) {
			visitClock(c);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseEvent(Event e) {
			visitEvent(e);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseImportStatement(ImportStatement st) {
			visitImportStatement(st);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBindableEntity(BindableEntity object) {
			visitBindableEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseConcreteEntity(ConcreteEntity object) {
			visitConcreteEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseElement(Element object) {
			visitElement(object);
			return Boolean.TRUE;
		}
	};
	
	private final CCSLModelSwitch<Boolean> ccslModelSwitch = new CCSLModelSwitch<Boolean>()
	{
		@Override
		public Boolean caseClockConstraintSystem(ClockConstraintSystem object) {
			visitClockConstraintSystem(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBlock(Block object) {
			visitBlock(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNamedElement(NamedElement object) {
			visitNamedElement(object);
			return Boolean.TRUE;
		}
	};
	
	private final BasicTypeSwitch<Boolean> basicTypeSwitch = new BasicTypeSwitch<Boolean>()
	{
		@Override
		public Boolean caseType(Type ty) {
			visitType(ty);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseElement(Element el) {
			visitElement(el);
			return Boolean.TRUE;
		}
		@Override
		public Boolean casePrimitiveType(PrimitiveType ty) {
			visitPrimitiveType(ty);
			return Boolean.TRUE;
		}
		@Override
		public Boolean casePrimitiveElement(PrimitiveElement el) {
			visitPrimitiveElement(el);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBindableEntity(BindableEntity object) {
			visitBindableEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBoolean(
				fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean object) {
			visitBoolean(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBooleanElement(BooleanElement object) {
			visitBooleanElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBox(Box object) {
			visitBox(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseChar(Char object) {
			visitChar(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseCharElement(CharElement object) {
			visitCharElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseConcreteEntity(ConcreteEntity object) {
			visitConcreteEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseDenseClockType(DenseClockType object) {
			visitDenseClockType(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseDiscreteClockType(DiscreteClockType object) {
			visitDiscreteClockType(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseEnumerationType(EnumerationType object) {
			visitEnumerationType(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseField(Field object) {
			visitField(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseInteger(Integer object) {
			visitInteger(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntegerElement(IntegerElement object) {
			visitIntegerElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNamedElement(NamedElement object) {
			visitNamedElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseReal(Real object) {
			visitReal(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealElement(RealElement object) {
			visitRealElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRecord(Record object) {
			visitRecord(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRecordElement(RecordElement object) {
			visitRecordElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSequenceElement(SequenceElement object) {
			visitSequenceElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSequenceType(SequenceType object) {
			visitSequenceType(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseString(String object) {
			visitString(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseStringElement(StringElement object) {
			visitStringElement(object);
			return Boolean.TRUE;
		}
	};
	
	private final ClockExpressionAndRelationSwitch<Boolean> clockExpressionAndRelationSwitch
		= new ClockExpressionAndRelationSwitch<Boolean>()
	{
		@Override
		public Boolean caseExpression(Expression ex) {
			visitExpression(ex);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUserExpressionDefinition(UserExpressionDefinition df) {
			visitUserExpressionDefinition(df);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseExpressionLibrary(ExpressionLibrary l) {
			visitExpressionLibrary(l);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBinding(Binding b) {
			visitBinding(b);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseConditionalExpressionDefinition(ConditionalExpressionDefinition cd) {
			visitConditionalExpressionDefinition(cd);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRelCase(RelCase object) {
			visitRelCase(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRelationDefinition(RelationDefinition object) {
			visitRelationDefinition(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUserRelationDefinition(UserRelationDefinition object) {
			visitUserRelationDefinition(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRelation(Relation object) {
			visitRelation(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseConditionalRelationDefinition(
				ConditionalRelationDefinition object) {
			visitConditionalRelationDefinition(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseAbstractEntity(AbstractEntity object) {
			visitAbstractEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBindableEntity(BindableEntity object) {
			visitBindableEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseConcreteEntity(ConcreteEntity object) {
			visitConcreteEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseExprCase(ExprCase object) {
			visitExprCase(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseExpressionDeclaration(ExpressionDeclaration object) {
			visitExpressionDeclaration(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseExpressionDefinition(ExpressionDefinition object) {
			visitExpressionDefinition(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseLibrary(Library object) {
			visitLibrary(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRelationLibrary(RelationLibrary object) {
			visitRelationLibrary(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNamedElement(NamedElement object) {
			visitNamedElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRelationDeclaration(RelationDeclaration object) {
			visitRelationDeclaration(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseExternalExpressionDefinition(
				ExternalExpressionDefinition object) {
			visitExternalExpressionDefinition(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseExternalRelationDefinition(ExternalRelationDefinition object) {
			visitExternalRelationDefinition(object);
			return Boolean.TRUE;
		}
	};
	
	private final ClassicalExpressionSwitch<Boolean> classicalExpressionSwitch
		= new ClassicalExpressionSwitch<Boolean>()
	{
		@Override
		public Boolean caseAnd(And object) {
			visitAnd(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBinaryBooleanExpression(BinaryBooleanExpression object) {
			visitBinaryBooleanExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBinaryIntegerExpression(BinaryIntegerExpression object) {
			visitBinaryIntegerExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBinaryRealExpression(BinaryRealExpression object) {
			visitBinaryRealExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBindableEntity(BindableEntity object) {
			visitBindableEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBooleanExpression(BooleanExpression object) {
			visitBooleanExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBooleanRef(BooleanRef object) {
			visitBooleanRef(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseBooleanVariableRef(BooleanVariableRef object) {
			visitBooleanVariableRef(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseClassicalExpression(ClassicalExpression object) {
			visitClassicalExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseConcreteEntity(ConcreteEntity object) {
			visitConcreteEntity(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseElement(Element object) {
			visitElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntDivide(IntDivide object) {
			visitIntDivide(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntegerExpression(IntegerExpression object) {
			visitIntegerExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntegerRef(IntegerRef object) {
			visitIntegerRef(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntegerVariableRef(IntegerVariableRef object) {
			visitIntegerVariableRef(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntEqual(IntEqual object) {
			visitIntEqual(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntInf(IntInf object) {
			visitIntInf(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntMinus(IntMinus object) {
			visitIntMinus(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntMultiply(IntMultiply object) {
			visitIntMultiply(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntPlus(IntPlus object) {
			visitIntPlus(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntSup(IntSup object) {
			visitIntSup(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNamedElement(NamedElement object) {
			visitNamedElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNot(Not object) {
			visitNot(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNumberSeqRef(NumberSeqRef object) {
			visitNumberSeqRef(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNumberSeqVariableRef(NumberSeqVariableRef object) {
			visitNumberSeqVariableRef(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseOr(Or object) {
			visitOr(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean casePrimitiveElement(PrimitiveElement object) {
			visitPrimitiveElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealEqual(RealEqual object) {
			visitRealEqual(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealExpression(RealExpression object) {
			visitRealExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealInf(RealInf object) {
			visitRealInf(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealMinus(RealMinus object) {
			visitRealMinus(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealMultiply(RealMultiply object) {
			visitRealMultiply(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealPlus(RealPlus object) {
			visitRealPlus(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealRef(RealRef object) {
			visitRealRef(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealSup(RealSup object) {
			visitRealSup(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRealVariableRef(RealVariableRef object) {
			visitRealVariableRef(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSeqDecr(SeqDecr object) {
			visitSeqDecr(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSeqExpression(SeqExpression object) {
			visitSeqExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSeqGetHead(SeqGetHead object) {
			visitSeqGetHead(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSeqGetTail(SeqGetTail object) {
			visitSeqGetTail(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSeqIsEmpty(SeqIsEmpty object) {
			visitSeqIsEmpty(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSeqSched(SeqSched object) {
			visitSeqSched(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnaryBooleanExpression(UnaryBooleanExpression object) {
			visitUnaryBooleanExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnaryIntegerExpression(UnaryIntegerExpression object) {
			visitUnaryIntegerExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnaryIntMinus(UnaryIntMinus object) {
			visitUnaryIntMinus(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnaryIntPlus(UnaryIntPlus object) {
			visitUnaryIntPlus(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnaryRealExpression(UnaryRealExpression object) {
			visitUnaryRealExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnaryRealMinus(UnaryRealMinus object) {
			visitUnaryRealMinus(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnaryRealPlus(UnaryRealPlus object) {
			visitUnaryRealPlus(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnarySeqExpression(UnarySeqExpression object) {
			visitUnarySeqExpression(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseXor(Xor object) {
			visitXor(object);
			return Boolean.TRUE;
		}
	};
	
	private KernelExpressionSwitch<Boolean> kernelExpressionSwitch = new KernelExpressionSwitch<Boolean>()
	{
		@Override
		public Boolean caseConcatenation(Concatenation object) {
			visitConcatenation(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseDeath(Death object) {
			visitDeath(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseDefer(Defer object) {
			visitDefer(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseDiscretization(Discretization object) {
			visitDiscretization(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseExpressionDeclaration(ExpressionDeclaration object) {
			visitExpressionDeclaration(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseInf(Inf object) {
			visitInf(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseIntersection(Intersection object) {
			visitIntersection(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseKernelExpressionDeclaration(
				KernelExpressionDeclaration object) {
			visitKernelExpressionDeclaration(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNamedElement(NamedElement object) {
			visitNamedElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNonStrictSampling(NonStrictSampling object) {
			visitNonStrictSampling(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseStrictSampling(StrictSampling object) {
			visitStrictSampling(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSup(Sup object) {
			visitSup(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUnion(Union object) {
			visitUnion(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseUpTo(UpTo object) {
			visitUpTo(object);
			return Boolean.TRUE;
		}
	};
	
	private KernelRelationSwitch<Boolean> kernelRelationSwitch = new KernelRelationSwitch<Boolean>() 
	{
		@Override
		public Boolean caseCoincidence(Coincidence object) {
			visitCoincidence(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseExclusion(Exclusion object) {
			visitExclusion(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseKernelRelationDeclaration(
				KernelRelationDeclaration object) {
			visitKernelRelationDeclaration(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNamedElement(NamedElement object) {
			visitNamedElement(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseNonStrictPrecedence(NonStrictPrecedence object) {
			visitNonStrictPrecedence(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean casePrecedence(Precedence object) {
			visitPrecedence(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseRelationDeclaration(RelationDeclaration object) {
			visitRelationDeclaration(object);
			return Boolean.TRUE;
		}
		@Override
		public Boolean caseSubClock(SubClock object) {
			visitSubClock(object);
			return Boolean.TRUE;
		}
	};
	
	public void visit(EObject o) {
		if (o.eClass().getEPackage() == TimeModelPackage.eINSTANCE) {
			timeModelVisitor.doSwitch(o);
		}
		else if (o.eClass().getEPackage() == CCSLModelPackage.eINSTANCE) {
			ccslModelSwitch.doSwitch(o);
		}
		else if (o.eClass().getEPackage() == BasicTypePackage.eINSTANCE) {
			basicTypeSwitch.doSwitch(o);
		}
		else if (o.eClass().getEPackage() == ClockExpressionAndRelationPackage.eINSTANCE) {
			clockExpressionAndRelationSwitch.doSwitch(o);
		}
		else if (o.eClass().getEPackage() == ClassicalExpressionPackage.eINSTANCE) {
			classicalExpressionSwitch.doSwitch(o);
		}
		else if (o.eClass().getEPackage() == KernelExpressionPackage.eINSTANCE) {
			kernelExpressionSwitch.doSwitch(o);
		}
		else if (o.eClass().getEPackage() == KernelRelationPackage.eINSTANCE) {
			kernelRelationSwitch.doSwitch(o);
		}
		else {
			return;
		}
	}

	protected void visitXor(Xor object) {
	}

	protected void visitRelationLibrary(RelationLibrary object) {
	}

	protected void visitSubClock(SubClock object) {
	}

	protected void visitRelationDeclaration(RelationDeclaration object) {
	}

	protected void visitPrecedence(Precedence object) {
	}

	protected void visitNonStrictPrecedence(NonStrictPrecedence object) {
	}

	protected void visitKernelRelationDeclaration(
			KernelRelationDeclaration object) {
	}

	protected void visitExclusion(Exclusion object) {
	}

	protected void visitCoincidence(Coincidence object) {
	}

	protected void visitUpTo(UpTo object) {
	}

	protected void visitUnion(Union object) {
	}

	protected void visitSup(Sup object) {
	}

	protected void visitStrictSampling(StrictSampling object) {
	}

	protected void visitNonStrictSampling(NonStrictSampling object) {
	}

	protected void visitKernelExpressionDeclaration(
			KernelExpressionDeclaration object) {
	}

	protected void visitIntersection(Intersection object) {
	}

	protected void visitInf(Inf object) {
	}

	protected void visitDiscretization(Discretization object) {
	}

	protected void visitDefer(Defer object) {
	}

	protected void visitDeath(Death object) {
	}

	protected void visitConcatenation(Concatenation object) {
	}

	protected void visitUnarySeqExpression(UnarySeqExpression object) {
	}

	protected void visitUnaryRealPlus(UnaryRealPlus object) {
	}

	protected void visitUnaryRealMinus(UnaryRealMinus object) {
	}

	protected void visitUnaryRealExpression(UnaryRealExpression object) {
	}

	protected void visitUnaryIntPlus(UnaryIntPlus object) {
	}

	protected void visitUnaryIntMinus(UnaryIntMinus object) {
	}

	protected void visitUnaryIntegerExpression(UnaryIntegerExpression object) {
	}

	protected void visitUnaryBooleanExpression(UnaryBooleanExpression object) {
	}

	protected void visitSeqSched(SeqSched object) {		
	}

	protected void visitSeqIsEmpty(SeqIsEmpty object) {		
	}

	protected void visitSeqGetTail(SeqGetTail object) {		
	}

	protected void visitSeqGetHead(SeqGetHead object) {		
	}

	protected void visitSeqExpression(SeqExpression object) {		
	}

	protected void visitSeqDecr(SeqDecr object) {		
	}

	protected void visitRealVariableRef(RealVariableRef object) {		
	}

	protected void visitRealSup(RealSup object) {		
	}

	protected void visitRealRef(RealRef object) {		
	}

	protected void visitRealPlus(RealPlus object) {		
	}

	protected void visitRealMultiply(RealMultiply object) {		
	}

	protected void visitRealMinus(RealMinus object) {		
	}

	protected void visitRealInf(RealInf object) {		
	}

	protected void visitRealExpression(RealExpression object) {		
	}

	protected void visitRealEqual(RealEqual object) {		
	}

	protected void visitOr(Or object) {		
	}

	protected void visitNumberSeqVariableRef(NumberSeqVariableRef object) {		
	}

	protected void visitNumberSeqRef(NumberSeqRef object) {		
	}

	protected void visitNot(Not object) {		
	}

	protected void visitIntSup(IntSup object) {		
	}

	protected void visitIntPlus(IntPlus object) {		
	}

	protected void visitIntMultiply(IntMultiply object) {		
	}

	protected void visitIntMinus(IntMinus object) {		
	}

	protected void visitIntInf(IntInf object) {		
	}

	protected void visitIntEqual(IntEqual object) {		
	}

	protected void visitIntegerVariableRef(IntegerVariableRef object) {		
	}

	protected void visitIntegerRef(IntegerRef object) {		
	}

	protected void visitIntegerExpression(IntegerExpression object) {		
	}

	protected void visitIntDivide(IntDivide object) {		
	}

	protected void visitClassicalExpression(ClassicalExpression object) {		
	}

	protected void visitBooleanVariableRef(BooleanVariableRef object) {		
	}

	protected void visitBooleanRef(BooleanRef object) {		
	}

	protected void visitBooleanExpression(BooleanExpression object) {		
	}

	protected void visitBinaryRealExpression(BinaryRealExpression object) {		
	}

	protected void visitBinaryIntegerExpression(BinaryIntegerExpression object) {		
	}

	protected void visitBinaryBooleanExpression(BinaryBooleanExpression object) {		
	}

	protected void visitAnd(And object) {		
	}

	protected void visitBoolean(
			fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean object) {
	}

	protected void visitBooleanElement(BooleanElement object) {
	}

	protected void visitBox(Box object) {
	}

	protected void visitChar(Char object) {
	}

	protected void visitCharElement(CharElement object) {
		return;
	}

	protected void visitDenseClockType(DenseClockType object) {
		return;
	}

	protected void visitDiscreteClockType(DiscreteClockType object) {
		return;
	}

	protected void visitEnumerationType(EnumerationType object) {
		return;
	}

	protected void visitField(Field object) {
		return;
	}

	protected void visitInteger(Integer object) {
		return;
	}

	protected void visitIntegerElement(IntegerElement object) {
		return;
	}

	protected void visitReal(Real object) {
		return;
	}

	protected void visitRealElement(RealElement object) {
		return;
	}

	protected void visitRecord(Record object) {
		return;
	}

	protected void visitRecordElement(RecordElement object) {
		return;
	}

	protected void visitSequenceElement(SequenceElement object) {
		return;
	}

	protected void visitSequenceType(SequenceType object) {

		return;
	}

	protected void visitStringElement(StringElement object) {
		return;
	}

	protected void visitString(String object) {
		return;
	}

	protected void visitLibrary(Library object) {
		return;
	}

	protected void visitExpressionDefinition(ExpressionDefinition object) {
		return;
	}
	
	protected void visitExternalExpressionDefinition(ExternalExpressionDefinition object) {
		return;
	}

	protected void visitExpressionDeclaration(ExpressionDeclaration object) {
		return;
	}

	protected void visitExprCase(ExprCase object) {
		return;
	}

	protected void visitConcreteEntity(ConcreteEntity object) {
		return;
	}

	protected void visitBindableEntity(BindableEntity object) {
		return;
	}

	protected void visitAbstractEntity(AbstractEntity object) {
		return;
	}

	protected void visitConditionalRelationDefinition(
			ConditionalRelationDefinition object) {
		return;
	}

	protected void visitRelation(Relation object) {
	}

	protected void visitUserRelationDefinition(UserRelationDefinition object) {
	}

	protected void visitRelationDefinition(RelationDefinition object) {
	}
	
	protected void visitExternalRelationDefinition(ExternalRelationDefinition object) {		
	}

	protected void visitRelCase(RelCase object) {
	}

	
}
