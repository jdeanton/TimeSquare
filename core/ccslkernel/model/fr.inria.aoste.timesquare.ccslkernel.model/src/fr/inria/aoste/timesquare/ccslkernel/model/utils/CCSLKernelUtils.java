/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;

public class CCSLKernelUtils {

	/**
	 * The default separator used between elements of a qualified name for CCSLKernel objects
	 */
	public static final String defaultSeparator = "::";

	/**
	 * Returns the name of the object using the {@link #getSimpleName(EObject)} function.
	 * @param object
	 * @return
	 * @see #getSimpleName(EObject)
	 */
	public static String getName(EObject object) {
		return getSimpleName(object);
	}
	
	/**
	 * Returns the name of the object. Only instances of {@link NamedElement} or one of its subclasses
	 * have a name. For other objects the function returns <code>null</code>. 
	 * @param object
	 * @return
	 * @see NamedElement#getName()
	 */
	public static String getSimpleName(EObject object) {
		if (object instanceof NamedElement) {
			return ((NamedElement) object).getName();
		} else {
			return null;
		}
	}

	/**
	 * Returns the fully qualified name of the object, with elements separated by the default separator
	 * ({@value #defaultSeparator})
	 * @param object
	 * @return
	 * @see #getQualifiedName(EObject, String)
	 */
	public static String getQualifiedName(EObject object) {
		return getQualifiedName(object, defaultSeparator);
	}

	/**
	 * Returns the fully qualified name of the object, with elements separated by the given separator argument.
	 * The fully qualified name is obtained by concatenating all the simple names of objects encountered in the
	 * containment hierarchy starting at the given object, up to and <em>excluding</em> the topmost
	 * {@link ClockConstraintSystem} object at the root of the model. Intermediate objects that have no name
	 * are ignored. The names of the objects are computed using the {@link #getSimpleName(EObject)} function.
	 * @param object
	 * @param separator
	 * @return
	 * @see #getSimpleName(EObject)
	 */
	public static String getQualifiedName(EObject object, String separator) {
		if (object == null) {
			return null;
		}
		String name = getSimpleName(object);
		String parentQN = getQualifiedName(object.eContainer(), separator);
		if (parentQN != null) {
			return ( name != null ? parentQN + separator + name : parentQN );
		}
		else
			return name;
	}
	
	/**
	 * To be used with {@link org.eclipse.xtext.naming.QualifiedName#create(List<String>)}
	 * @param object
	 * @return
	 */
	public static List<String> getQualifiedNameSegments(EObject object) {
		List<String> result;
		if (object.eContainer() == null) {
			result = new ArrayList<String>();
		}
		else {
			result = getQualifiedNameSegments(object.eContainer());
		}
		String name = getSimpleName(object);
		result.add(name);
		return result;
	}
	
	/**
	 * @see #findObjectFromQualifiedName(String, String, ClockConstraintSystem)
	 * @param qualifiedName
	 * @param model
	 * @return
	 */
	public static EObject findObjectFromQualifiedName(String qualifiedName, ClockConstraintSystem model) {
		return findObjectFromQualifiedName(qualifiedName, defaultSeparator, model);
	}
	
	/**
	 * Returns, if it exists, the object the qualified name of which is the same as the one given in
	 * the qualifiedName argument. If no such object exists, returns null. This function allows to retrieve an
	 * object based on its qualified name.
	 * If the qualified name of an object is computed using either {@link #getQualifiedName(EObject)} or
	 * {@link #getQualifiedName(EObject, String)} using the same separator argument, then the function
	 * {@link #findObjectFromQualifiedName(String, String, ClockConstraintSystem)} will return the same object.
	 * @param qualifiedName
	 * @param separator
	 * @param model
	 * @return
	 * @see #findObjectFromQualifiedName(String, ClockConstraintSystem)
	 */
	public static EObject findObjectFromQualifiedName(String qualifiedName, String separator, ClockConstraintSystem model) {
		String[] pathElementNames = qualifiedName.split(separator);
		return findObjectFromQualifiedName(pathElementNames, 0, model);
	}

	/*
	 * Private static functions that implements the search of an object based on a qualified name.
	 * The search is conducted after the qualified name has been split in parts using the given separator : this
	 * gives an array of String
	 * The index argument is used to mark the position to consider in this array of String. If the current
	 * EObject (in the modelObject argument) is a NamedElement and has the expected name (the one which is a the
	 * index position in the String array), then it is either on the path to the expected object if the index is
	 * not at the end of the String array, or else (if the index points to the last name) it is the object we
	 * are looking for. 
	 */
	private static EObject findObjectFromQualifiedName(String[] pathElementNames, int index, EObject modelObject) {
		if (index >= pathElementNames.length) {
			return null;
		}
		String elementName = pathElementNames[index];
		if (modelObject instanceof NamedElement) {
			if (((NamedElement) modelObject).getName().compareTo(elementName) == 0) {
				if (index == pathElementNames.length - 1) {
					return modelObject;
				}
				else {
					if (modelObject instanceof Block) {
						return findObjectInBlock(pathElementNames, index, (Block) modelObject);
					}
					// Do we need to perform the search in other elements : expressions, relations, etc. ?
					// Currently none of them contained other named elements, as soon as we do not
					// consider the unfolding process of the model.
				}
			}
			else {
				return null;
			}
		}
		return null;
	}
	
	
	private static EObject findObjectInBlock(String[] pathElementNames, int index, Block modelObject) {
		for (Block subBlock : ((Block) modelObject).getSubBlock()) {
			EObject found = findObjectFromQualifiedName(pathElementNames, index + 1, subBlock);
			if (found != null) {
				return found;
			}
		}
		for (Element element : ((Block) modelObject).getElements()) {
			EObject found = findObjectFromQualifiedName(pathElementNames, index + 1, element);
			if (found != null) {
				return found;
			}
		}
		for (Expression expr : ((Block) modelObject).getExpressions()) {
			EObject found = findObjectFromQualifiedName(pathElementNames, index + 1, expr);
			if (found != null) {
				return found;
			}
		}
		for (Relation relation : ((Block) modelObject).getRelations()) {
			EObject found = findObjectFromQualifiedName(pathElementNames, index + 1, relation);
			if (found != null) {
				return found;
			}
		}
		for (ClassicalExpression cexpr : modelObject.getClassicalExpression()) {
			EObject found = findObjectFromQualifiedName(pathElementNames, index + 1, cexpr);
			if (found != null) {
				return found;
			}
		}
		return null;
	}
	
	public static String getImportAlias(ClockConstraintSystem mainModel, Resource importedResource) {
		ResourceSet resourceSet = mainModel.eResource().getResourceSet();
		if (resourceSet != importedResource.getResourceSet())
			return null;
		for (ImportStatement impStatement : mainModel.getImports()) {
			String importedURI = impStatement.getImportURI();
			URI uri = resourceSet.getURIConverter().normalize( URI.createURI(importedURI) );
			if (uri.isRelative()) {
				uri = uri.resolve(mainModel.eResource().getURI());
			}
			Resource resource = resourceSet.getResource(uri, false);
			if (resource == importedResource) {
				String alias = impStatement.getAlias();
				return alias;
			}
		}
		return null;
	}
	
	public static String getImportAlias(Resource importingResource, Resource importedResource) {
		if (importingResource == importedResource)
			return "";
		ClockConstraintSystem ccslModelRoot = null;
		if (importingResource.getContents().get(0) instanceof ClockConstraintSystem) {
			ccslModelRoot = (ClockConstraintSystem) importingResource.getContents().get(0);
		}
		else
			return null;
		return getImportAlias(ccslModelRoot, importedResource);
	}
	
	public static String getImportAlias(EObject context, EObject target) {
		EObject root = EcoreUtil.getRootContainer(context);
		if (root instanceof ClockConstraintSystem)
			return getImportAlias((ClockConstraintSystem) root, target.eResource());
		else
			return null;
	}

	
}
