/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.visitor;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Char;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Real;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.util.BasicTypeSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntDivide;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqDecr;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnarySeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Xor;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util.ClassicalExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Death;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.util.ClockExpressionAndRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.util.CCSLModelSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.util.TimeModelSwitch;

public abstract class Visitor<T> {

	public Visitor() {
	}

	public T visit(EObject o) {
		if (o.eClass().getEPackage() == TimeModelPackage.eINSTANCE) {
			return timeModelVisitor.doSwitch(o);
		} else if (o.eClass().getEPackage() == CCSLModelPackage.eINSTANCE) {
			return ccslModelSwitch.doSwitch(o);
		} else if (o.eClass().getEPackage() == BasicTypePackage.eINSTANCE) {
			return basicTypeSwitch.doSwitch(o);
		} else if (o.eClass().getEPackage() == ClockExpressionAndRelationPackage.eINSTANCE) {
			return clockExpressionAndRelationSwitch.doSwitch(o);
		} else if (o.eClass().getEPackage() == ClassicalExpressionPackage.eINSTANCE) {
			return classicalExpressionSwitch.doSwitch(o);
		} else if (o.eClass().getEPackage() == KernelExpressionPackage.eINSTANCE) {
			return kernelExpressionSwitch.doSwitch(o);
		} else if (o.eClass().getEPackage() == KernelRelationPackage.eINSTANCE) {
			return kernelRelationSwitch.doSwitch(o);
		} else {
			return null;
		}
	}

	private final TimeModelSwitch<T> timeModelVisitor = new TimeModelSwitch<T>() {
		@Override
		protected T doSwitch(int classifierID, EObject theEObject) {
			return super.doSwitch(classifierID, theEObject);
		}
		@Override
		public T caseNamedElement(NamedElement object) {
			return visitNamedElement(object);
		}

		@Override
		public T caseClock(Clock c) {
			return visitClock(c);
		}

		@Override
		public T caseEvent(Event e) {
			return visitEvent(e);
		}

		@Override
		public T caseImportStatement(ImportStatement st) {
			return visitImportStatement(st);
		}

		@Override
		public T caseBindableEntity(BindableEntity object) {
			return visitBindableEntity(object);
		}

		@Override
		public T caseConcreteEntity(ConcreteEntity object) {
			return visitConcreteEntity(object);
		}

		@Override
		public T caseElement(Element object) {
			return visitElement(object);
		}
	};

	private final CCSLModelSwitch<T> ccslModelSwitch = new CCSLModelSwitch<T>() {
		@Override
		public T caseClockConstraintSystem(ClockConstraintSystem object) {
			return visitClockConstraintSystem(object);
		}

		@Override
		public T caseBlock(Block object) {
			return visitBlock(object);
		}

		@Override
		public T caseNamedElement(NamedElement object) {
			return visitNamedElement(object);
		}
	};

	private final BasicTypeSwitch<T> basicTypeSwitch = new BasicTypeSwitch<T>() {
		@Override
		public T caseType(Type ty) {
			return visitType(ty);

		}

		@Override
		public T caseElement(Element el) {
			return visitElement(el);

		}

		@Override
		public T casePrimitiveType(PrimitiveType ty) {
			return visitPrimitiveType(ty);

		}

		@Override
		public T casePrimitiveElement(PrimitiveElement el) {
			return visitPrimitiveElement(el);

		}

		@Override
		public T caseBindableEntity(BindableEntity object) {
			return visitBindableEntity(object);

		}

		@Override
		public T caseBoolean(
				fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean object) {
			return visitBoolean(object);

		}

		@Override
		public T caseBooleanElement(BooleanElement object) {
			return visitBooleanElement(object);

		}

		@Override
		public T caseBox(Box object) {
			return visitBox(object);

		}

		@Override
		public T caseChar(Char object) {
			return visitChar(object);

		}

		@Override
		public T caseCharElement(CharElement object) {
			return visitCharElement(object);

		}

		@Override
		public T caseConcreteEntity(ConcreteEntity object) {
			return visitConcreteEntity(object);

		}

		@Override
		public T caseDenseClockType(DenseClockType object) {
			return visitDenseClockType(object);

		}

		@Override
		public T caseDiscreteClockType(DiscreteClockType object) {
			return visitDiscreteClockType(object);

		}

		@Override
		public T caseEnumerationType(EnumerationType object) {
			return visitEnumerationType(object);

		}

		@Override
		public T caseField(Field object) {
			return visitField(object);

		}

		@Override
		public T caseInteger(Integer object) {
			return visitInteger(object);

		}

		@Override
		public T caseIntegerElement(IntegerElement object) {
			return visitIntegerElement(object);

		}

		@Override
		public T caseNamedElement(NamedElement object) {
			return visitNamedElement(object);

		}

		@Override
		public T caseReal(Real object) {
			return visitReal(object);

		}

		@Override
		public T caseRealElement(RealElement object) {
			return visitRealElement(object);

		}

		@Override
		public T caseRecord(Record object) {
			return visitRecord(object);

		}

		@Override
		public T caseRecordElement(RecordElement object) {
			return visitRecordElement(object);

		}

		@Override
		public T caseSequenceElement(SequenceElement object) {
			return visitSequenceElement(object);

		}

		@Override
		public T caseSequenceType(SequenceType object) {
			return visitSequenceType(object);

		}

		@Override
		public T caseString(String object) {
			return visitString(object);

		}

		@Override
		public T caseStringElement(StringElement object) {
			return visitStringElement(object);

		}
	};

	private final ClockExpressionAndRelationSwitch<T> clockExpressionAndRelationSwitch = new ClockExpressionAndRelationSwitch<T>() {
		@Override
		public T caseExpression(Expression ex) {
			return visitExpression(ex);

		}

		@Override
		public T caseUserExpressionDefinition(UserExpressionDefinition df) {
			return visitUserExpressionDefinition(df);

		}

		@Override
		public T caseExpressionLibrary(ExpressionLibrary l) {
			return visitExpressionLibrary(l);

		}

		@Override
		public T caseBinding(Binding b) {
			return visitBinding(b);

		}

		@Override
		public T caseConditionalExpressionDefinition(
				ConditionalExpressionDefinition cd) {
			return visitConditionalExpressionDefinition(cd);

		}

		@Override
		public T caseRelCase(RelCase object) {
			return visitRelCase(object);

		}

		@Override
		public T caseRelationDefinition(RelationDefinition object) {
			return visitRelationDefinition(object);

		}

		@Override
		public T caseUserRelationDefinition(UserRelationDefinition object) {
			return visitUserRelationDefinition(object);

		}

		@Override
		public T caseRelation(Relation object) {
			return visitRelation(object);

		}

		@Override
		public T caseConditionalRelationDefinition(
				ConditionalRelationDefinition object) {
			return visitConditionalRelationDefinition(object);

		}

		@Override
		public T caseAbstractEntity(AbstractEntity object) {
			return visitAbstractEntity(object);

		}

		@Override
		public T caseBindableEntity(BindableEntity object) {
			return visitBindableEntity(object);

		}

		@Override
		public T caseConcreteEntity(ConcreteEntity object) {
			return visitConcreteEntity(object);

		}

		@Override
		public T caseExprCase(ExprCase object) {
			return visitExprCase(object);

		}

		@Override
		public T caseExpressionDeclaration(ExpressionDeclaration object) {
			return visitExpressionDeclaration(object);

		}

		@Override
		public T caseExpressionDefinition(ExpressionDefinition object) {
			return visitExpressionDefinition(object);

		}

		@Override
		public T caseLibrary(Library object) {
			return visitLibrary(object);

		}

		@Override
		public T caseRelationLibrary(RelationLibrary object) {
			return visitRelationLibrary(object);

		}

		@Override
		public T caseNamedElement(NamedElement object) {
			return visitNamedElement(object);

		}

		@Override
		public T caseRelationDeclaration(RelationDeclaration object) {
			return visitRelationDeclaration(object);

		}
	};

	private final ClassicalExpressionSwitch<T> classicalExpressionSwitch = new ClassicalExpressionSwitch<T>() {
		@Override
		public T caseAnd(And object) {
			return visitAnd(object);

		}

		@Override
		public T caseBinaryBooleanExpression(BinaryBooleanExpression object) {
			return visitBinaryBooleanExpression(object);

		}

		@Override
		public T caseBinaryIntegerExpression(BinaryIntegerExpression object) {
			return visitBinaryIntegerExpression(object);

		}

		@Override
		public T caseBinaryRealExpression(BinaryRealExpression object) {
			return visitBinaryRealExpression(object);

		}

		@Override
		public T caseBindableEntity(BindableEntity object) {
			return visitBindableEntity(object);

		}

		@Override
		public T caseBooleanExpression(BooleanExpression object) {
			return visitBooleanExpression(object);

		}

		@Override
		public T caseBooleanRef(BooleanRef object) {
			return visitBooleanRef(object);

		}

		@Override
		public T caseBooleanVariableRef(BooleanVariableRef object) {
			return visitBooleanVariableRef(object);

		}

		@Override
		public T caseClassicalExpression(ClassicalExpression object) {
			return visitClassicalExpression(object);

		}

		@Override
		public T caseConcreteEntity(ConcreteEntity object) {
			return visitConcreteEntity(object);

		}

		@Override
		public T caseElement(Element object) {
			return visitElement(object);

		}

		@Override
		public T caseIntDivide(IntDivide object) {
			return visitIntDivide(object);

		}

		@Override
		public T caseIntegerExpression(IntegerExpression object) {
			return visitIntegerExpression(object);

		}

		@Override
		public T caseIntegerRef(IntegerRef object) {
			return visitIntegerRef(object);

		}

		@Override
		public T caseIntegerVariableRef(IntegerVariableRef object) {
			return visitIntegerVariableRef(object);

		}

		@Override
		public T caseIntEqual(IntEqual object) {
			return visitIntEqual(object);

		}

		@Override
		public T caseIntInf(IntInf object) {
			return visitIntInf(object);

		}

		@Override
		public T caseIntMinus(IntMinus object) {
			return visitIntMinus(object);

		}

		@Override
		public T caseIntMultiply(IntMultiply object) {
			return visitIntMultiply(object);

		}

		@Override
		public T caseIntPlus(IntPlus object) {
			return visitIntPlus(object);

		}

		@Override
		public T caseIntSup(IntSup object) {
			return visitIntSup(object);

		}

		@Override
		public T caseNamedElement(NamedElement object) {
			return visitNamedElement(object);

		}

		@Override
		public T caseNot(Not object) {
			return visitNot(object);

		}

		@Override
		public T caseNumberSeqRef(NumberSeqRef object) {
			return visitNumberSeqRef(object);

		}

		@Override
		public T caseNumberSeqVariableRef(NumberSeqVariableRef object) {
			return visitNumberSeqVariableRef(object);

		}

		@Override
		public T caseOr(Or object) {
			return visitOr(object);

		}

		@Override
		public T casePrimitiveElement(PrimitiveElement object) {
			return visitPrimitiveElement(object);

		}

		@Override
		public T caseRealEqual(RealEqual object) {
			return visitRealEqual(object);

		}

		@Override
		public T caseRealExpression(RealExpression object) {
			return visitRealExpression(object);

		}

		@Override
		public T caseRealInf(RealInf object) {
			return visitRealInf(object);

		}

		@Override
		public T caseRealMinus(RealMinus object) {
			return visitRealMinus(object);

		}

		@Override
		public T caseRealMultiply(RealMultiply object) {
			return visitRealMultiply(object);

		}

		@Override
		public T caseRealPlus(RealPlus object) {
			return visitRealPlus(object);

		}

		@Override
		public T caseRealRef(RealRef object) {
			return visitRealRef(object);

		}

		@Override
		public T caseRealSup(RealSup object) {
			return visitRealSup(object);

		}

		@Override
		public T caseRealVariableRef(RealVariableRef object) {
			return visitRealVariableRef(object);

		}

		@Override
		public T caseSeqDecr(SeqDecr object) {
			return visitSeqDecr(object);

		}

		@Override
		public T caseSeqExpression(SeqExpression object) {
			return visitSeqExpression(object);

		}

		@Override
		public T caseSeqGetHead(SeqGetHead object) {
			return visitSeqGetHead(object);

		}

		@Override
		public T caseSeqGetTail(SeqGetTail object) {
			return visitSeqGetTail(object);

		}

		@Override
		public T caseSeqIsEmpty(SeqIsEmpty object) {
			return visitSeqIsEmpty(object);

		}

		@Override
		public T caseSeqSched(SeqSched object) {
			return visitSeqSched(object);

		}

		@Override
		public T caseUnaryBooleanExpression(UnaryBooleanExpression object) {
			return visitUnaryBooleanExpression(object);

		}

		@Override
		public T caseUnaryIntegerExpression(UnaryIntegerExpression object) {
			return visitUnaryIntegerExpression(object);

		}

		@Override
		public T caseUnaryIntMinus(UnaryIntMinus object) {
			return visitUnaryIntMinus(object);

		}

		@Override
		public T caseUnaryIntPlus(UnaryIntPlus object) {
			return visitUnaryIntPlus(object);

		}

		@Override
		public T caseUnaryRealExpression(UnaryRealExpression object) {
			return visitUnaryRealExpression(object);

		}

		@Override
		public T caseUnaryRealMinus(UnaryRealMinus object) {
			return visitUnaryRealMinus(object);

		}

		@Override
		public T caseUnaryRealPlus(UnaryRealPlus object) {
			return visitUnaryRealPlus(object);

		}

		@Override
		public T caseUnarySeqExpression(UnarySeqExpression object) {
			return visitUnarySeqExpression(object);

		}

		@Override
		public T caseXor(Xor object) {
			return visitXor(object);
		}
	};

	private KernelExpressionSwitch<T> kernelExpressionSwitch = new KernelExpressionSwitch<T>() {
		@Override
		public T caseConcatenation(Concatenation object) {
			return visitConcatenation(object);
		}

		@Override
		public T caseDeath(Death object) {
			return visitDeath(object);
		}

		@Override
		public T caseDefer(Defer object) {
			return visitDefer(object);
		}

		@Override
		public T caseDiscretization(Discretization object) {
			return visitDiscretization(object);
		}

		@Override
		public T caseExpressionDeclaration(ExpressionDeclaration object) {
			return visitExpressionDeclaration(object);
		}

		@Override
		public T caseInf(Inf object) {
			return visitInf(object);
		}

		@Override
		public T caseIntersection(Intersection object) {
			return visitIntersection(object);
		}

		@Override
		public T caseKernelExpressionDeclaration(
				KernelExpressionDeclaration object) {
			return visitKernelExpressionDeclaration(object);
		}

		@Override
		public T caseNamedElement(NamedElement object) {
			return visitNamedElement(object);
		}

		@Override
		public T caseNonStrictSampling(NonStrictSampling object) {
			return visitNonStrictSampling(object);
		}

		@Override
		public T caseStrictSampling(StrictSampling object) {
			return visitStrictSampling(object);
		}

		@Override
		public T caseSup(Sup object) {
			return visitSup(object);
		}

		@Override
		public T caseUnion(Union object) {
			return visitUnion(object);
		}

		@Override
		public T caseUpTo(UpTo object) {
			return visitUpTo(object);
		}
		
		@Override
		public T caseWait(Wait object) {
			return visitWait(object);
		}
	};

	private KernelRelationSwitch<T> kernelRelationSwitch = new KernelRelationSwitch<T>() {
		@Override
		public T caseCoincidence(Coincidence object) {
			return visitCoincidence(object);

		}

		@Override
		public T caseExclusion(Exclusion object) {
			return visitExclusion(object);

		}

		@Override
		public T caseKernelRelationDeclaration(KernelRelationDeclaration object) {
			return visitKernelRelationDeclaration(object);

		}

		@Override
		public T caseNamedElement(NamedElement object) {
			return visitNamedElement(object);

		}

		@Override
		public T caseNonStrictPrecedence(NonStrictPrecedence object) {
			return visitNonStrictPrecedence(object);

		}

		@Override
		public T casePrecedence(Precedence object) {
			return visitPrecedence(object);

		}

		@Override
		public T caseRelationDeclaration(RelationDeclaration object) {
			return visitRelationDeclaration(object);

		}

		@Override
		public T caseSubClock(SubClock object) {
			return visitSubClock(object);

		}
	};

	protected T visitNamedElement(NamedElement e) {
		return null;
	}

	protected T visitClock(Clock c) {
		return null;
	}

	protected T visitEvent(Event e) {
		return null;
	}

	protected T visitImportStatement(ImportStatement st) {
		return null;
	}

	protected T visitClockConstraintSystem(ClockConstraintSystem o) {
		return null;
	}

	protected T visitBlock(Block object) {
		return null;
	}

	protected T visitType(Type ty) {
		return null;
	}

	protected T visitElement(Element el) {
		return null;
	}

	protected T visitPrimitiveType(PrimitiveType ty) {
		return null;
	}

	protected T visitPrimitiveElement(PrimitiveElement el) {
		return null;
	}

	protected T visitExpression(Expression ex) {
		return null;
	}

	protected T visitUserExpressionDefinition(UserExpressionDefinition d) {
		return null;
	}

	protected T visitExpressionLibrary(ExpressionLibrary l) {
		return null;
	}

	protected T visitBinding(Binding b) {
		return null;
	}

	protected T visitConditionalExpressionDefinition(
			ConditionalExpressionDefinition cd) {
		return null;
	}

	protected T visitXor(Xor object) {
		return null;
	}

	protected T visitRelationLibrary(RelationLibrary object) {
		return null;
	}

	protected T visitSubClock(SubClock object) {
		return null;
	}

	protected T visitRelationDeclaration(RelationDeclaration object) {
		return null;
	}

	protected T visitPrecedence(Precedence object) {
		return null;
	}

	protected T visitNonStrictPrecedence(NonStrictPrecedence object) {
		return null;
	}

	protected T visitKernelRelationDeclaration(KernelRelationDeclaration object) {
		return null;
	}

	protected T visitExclusion(Exclusion object) {
		return null;
	}

	protected T visitCoincidence(Coincidence object) {
		return null;
	}

	protected T visitUpTo(UpTo object) {
		return null;
	}

	protected T visitUnion(Union object) {
		return null;
	}
	
	protected T visitWait(Wait object) {
		return null;
	}

	protected T visitSup(Sup object) {
		return null;
	}

	protected T visitStrictSampling(StrictSampling object) {
		return null;
	}

	protected T visitNonStrictSampling(NonStrictSampling object) {
		return null;
	}

	protected T visitKernelExpressionDeclaration(
			KernelExpressionDeclaration object) {
		return null;
	}

	protected T visitIntersection(Intersection object) {
		return null;
	}

	protected T visitInf(Inf object) {
		return null;
	}

	protected T visitDiscretization(Discretization object) {
		return null;
	}

	protected T visitDefer(Defer object) {
		return null;
	}

	protected T visitDeath(Death object) {
		return null;
	}

	protected T visitConcatenation(Concatenation object) {
		return null;
	}

	protected T visitUnarySeqExpression(UnarySeqExpression object) {
		return null;
	}

	protected T visitUnaryRealPlus(UnaryRealPlus object) {
		return null;
	}

	protected T visitUnaryRealMinus(UnaryRealMinus object) {
		return null;
	}

	protected T visitUnaryRealExpression(UnaryRealExpression object) {
		return null;
	}

	protected T visitUnaryIntPlus(UnaryIntPlus object) {
		return null;
	}

	protected T visitUnaryIntMinus(UnaryIntMinus object) {
		return null;
	}

	protected T visitUnaryIntegerExpression(UnaryIntegerExpression object) {
		return null;
	}

	protected T visitUnaryBooleanExpression(UnaryBooleanExpression object) {
		return null;
	}

	protected T visitSeqSched(SeqSched object) {
		return null;
	}

	protected T visitSeqIsEmpty(SeqIsEmpty object) {
		return null;
	}

	protected T visitSeqGetTail(SeqGetTail object) {
		return null;
	}

	protected T visitSeqGetHead(SeqGetHead object) {
		return null;
	}

	protected T visitSeqExpression(SeqExpression object) {
		return null;
	}

	protected T visitSeqDecr(SeqDecr object) {
		return null;
	}

	protected T visitRealVariableRef(RealVariableRef object) {
		return null;
	}

	protected T visitRealSup(RealSup object) {
		return null;
	}

	protected T visitRealRef(RealRef object) {
		return null;
	}

	protected T visitRealPlus(RealPlus object) {
		return null;
	}

	protected T visitRealMultiply(RealMultiply object) {
		return null;
	}

	protected T visitRealMinus(RealMinus object) {
		return null;
	}

	protected T visitRealInf(RealInf object) {
		return null;
	}

	protected T visitRealExpression(RealExpression object) {
		return null;
	}

	protected T visitRealEqual(RealEqual object) {
		return null;
	}

	protected T visitOr(Or object) {
		return null;
	}

	protected T visitNumberSeqVariableRef(NumberSeqVariableRef object) {
		return null;
	}

	protected T visitNumberSeqRef(NumberSeqRef object) {
		return null;
	}

	protected T visitNot(Not object) {
		return null;
	}

	protected T visitIntSup(IntSup object) {
		return null;
	}

	protected T visitIntPlus(IntPlus object) {
		return null;
	}

	protected T visitIntMultiply(IntMultiply object) {
		return null;
	}

	protected T visitIntMinus(IntMinus object) {
		return null;
	}

	protected T visitIntInf(IntInf object) {
		return null;
	}

	protected T visitIntEqual(IntEqual object) {
		return null;
	}

	protected T visitIntegerVariableRef(IntegerVariableRef object) {
		return null;
	}

	protected T visitIntegerRef(IntegerRef object) {
		return null;
	}

	protected T visitIntegerExpression(IntegerExpression object) {
		return null;
	}

	protected T visitIntDivide(IntDivide object) {
		return null;
	}

	protected T visitClassicalExpression(ClassicalExpression object) {
		return null;
	}

	protected T visitBooleanVariableRef(BooleanVariableRef object) {
		return null;
	}

	protected T visitBooleanRef(BooleanRef object) {
		return null;
	}

	protected T visitBooleanExpression(BooleanExpression object) {
		return null;
	}

	protected T visitBinaryRealExpression(BinaryRealExpression object) {
		return null;
	}

	protected T visitBinaryIntegerExpression(BinaryIntegerExpression object) {
		return null;
	}

	protected T visitBinaryBooleanExpression(BinaryBooleanExpression object) {
		return null;
	}

	protected T visitAnd(And object) {
		return null;
	}

	protected T visitBoolean(
			fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean object) {
		return null;
	}

	protected T visitBooleanElement(BooleanElement object) {
		return null;
	}

	protected T visitBox(Box object) {
		return null;
	}

	protected T visitChar(Char object) {
		return null;
	}

	protected T visitCharElement(CharElement object) {
		return null;
	}

	protected T visitDenseClockType(DenseClockType object) {
		return null;
	}

	protected T visitDiscreteClockType(DiscreteClockType object) {
		return null;
	}

	protected T visitEnumerationType(EnumerationType object) {
		return null;
	}

	protected T visitField(Field object) {
		return null;
	}

	protected T visitInteger(Integer object) {
		return null;
	}

	protected T visitIntegerElement(IntegerElement object) {
		return null;
	}

	protected T visitReal(Real object) {
		return null;
	}

	protected T visitRealElement(RealElement object) {
		return null;
	}

	protected T visitRecord(Record object) {
		return null;
	}

	protected T visitRecordElement(RecordElement object) {
		return null;
	}

	protected T visitSequenceElement(SequenceElement object) {
		return null;
	}

	protected T visitSequenceType(SequenceType object) {

		return null;
	}

	protected T visitStringElement(StringElement object) {
		return null;
	}

	protected T visitString(String object) {
		return null;
	}

	protected T visitLibrary(Library object) {
		return null;
	}

	protected T visitExpressionDefinition(ExpressionDefinition object) {
		return null;
	}

	protected T visitExpressionDeclaration(ExpressionDeclaration object) {
		return null;
	}

	protected T visitExprCase(ExprCase object) {
		return null;
	}

	protected T visitConcreteEntity(ConcreteEntity object) {
		return null;
	}

	protected T visitBindableEntity(BindableEntity object) {
		return null;
	}

	protected T visitAbstractEntity(AbstractEntity object) {
		return null;
	}

	protected T visitConditionalRelationDefinition(
			ConditionalRelationDefinition object) {
		return null;
	}

	protected T visitRelation(Relation object) {
		return null;
	}

	protected T visitUserRelationDefinition(UserRelationDefinition object) {
		return null;
	}

	protected T visitRelationDefinition(RelationDefinition object) {
		return null;
	}

	protected T visitRelCase(RelCase object) {
		return null;
	}

}
