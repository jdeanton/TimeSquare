/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.utils;

import java.io.IOException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/* not sure where this class should be located : model or solver 
 * - model: seems to be better (less dependencies) but requires to add 
 * an explicit dependency from solver to model, which is now implicit 
 * through modelunfolding
 * - solver: do not require to modify the dependencies of solver but Users of ResourceLoader
 * have to drag more dependencies
 * 
 * Could also be in another plugin but solver needs it anyway.
 */

public class ResourceLoader {
	public static ResourceLoader INSTANCE = new ResourceLoader();

	private ResourceLoader() {
		/* SINGLETON */
	}

	public Resource loadPluginResource(String fileName) throws IOException {
		return loadResource(URI.createPlatformPluginURI(fileName, false));
	}
	public Resource loadResource(String fileName) throws IOException {
		URI uri = URI.createPlatformResourceURI(fileName, false);
		return loadResource(uri);
	}

	public Resource loadResource(IPath fullPath) throws IOException {
		return loadResource(fullPath.toOSString());
	}

	public Resource loadResource(URI uri) throws IOException {
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(uri, true);
		//resourceSet.createResource(uri);
		resource.load(null);
		EcoreUtil.resolveAll(resourceSet);
		return resource;
	}
}
