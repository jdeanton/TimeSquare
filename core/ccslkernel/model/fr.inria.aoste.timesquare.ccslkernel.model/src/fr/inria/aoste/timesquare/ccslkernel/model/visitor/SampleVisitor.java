/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.visitor;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;

public class SampleVisitor extends VisitorVoid {

	@Override
	public void visitNamedElement(NamedElement e) {
		return;
	}

	@Override
	public void visitClock(Clock c) {
		return;
	}

	@Override
	public void visitClockConstraintSystem(ClockConstraintSystem s) {
		for (Type t : s.getDataTypes()) {
			visit(t);
		}
		return;
	}

	@Override
	public void visitBlock(Block b) {
		for (Expression e : b.getExpressions()) {
			visit(e);
		}
		return;
	}

	@Override
	public void visitEvent(Event e) {
		return;
	}

	@Override
	public void visitImportStatement(ImportStatement st) {
		return;
	}

	@Override
	public void visitType(Type ty) {
		return;
	}

	@Override
	public void visitElement(Element el) {
		return;
	}

	@Override
	public void visitPrimitiveType(PrimitiveType ty) {
		return;
	}

	@Override
	public void visitPrimitiveElement(PrimitiveElement el) {
		return;
	}

	@Override
	public void visitExpression(Expression ex) {
		return;
	}

	@Override
	public void visitUserExpressionDefinition(UserExpressionDefinition d) {
		return;
	}

}
