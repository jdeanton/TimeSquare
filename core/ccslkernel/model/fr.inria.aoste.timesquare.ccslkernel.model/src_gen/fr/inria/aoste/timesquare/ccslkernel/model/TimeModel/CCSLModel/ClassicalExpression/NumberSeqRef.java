/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Seq Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef#getReferencedNumberSeq <em>Referenced Number Seq</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#getNumberSeqRef()
 * @model
 * @generated
 */
public interface NumberSeqRef extends SeqExpression
{
  /**
	 * Returns the value of the '<em><b>Referenced Number Seq</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Referenced Number Seq</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Number Seq</em>' reference.
	 * @see #setReferencedNumberSeq(SequenceElement)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#getNumberSeqRef_ReferencedNumberSeq()
	 * @model required="true"
	 * @generated
	 */
  SequenceElement getReferencedNumberSeq();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef#getReferencedNumberSeq <em>Referenced Number Seq</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Number Seq</em>' reference.
	 * @see #getReferencedNumberSeq()
	 * @generated
	 */
  void setReferencedNumberSeq(SequenceElement value);

} // NumberSeqRef
