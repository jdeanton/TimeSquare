/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.util;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Char;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Real;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage
 * @generated
 */
public class BasicTypeSwitch<T>
{
  /**
	 * The cached model package
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected static BasicTypePackage modelPackage;

  /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public BasicTypeSwitch()
  {
		if (modelPackage == null) {
			modelPackage = BasicTypePackage.eINSTANCE;
		}
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  public T doSwitch(EObject theEObject)
  {
		return doSwitch(theEObject.eClass(), theEObject);
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  protected T doSwitch(EClass theEClass, EObject theEObject)
  {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  protected T doSwitch(int classifierID, EObject theEObject)
  {
		switch (classifierID) {
			case BasicTypePackage.TYPE: {
				Type type = (Type)theEObject;
				T result = caseType(type);
				if (result == null) result = caseNamedElement(type);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.ELEMENT: {
				Element element = (Element)theEObject;
				T result = caseElement(element);
				if (result == null) result = caseConcreteEntity(element);
				if (result == null) result = caseBindableEntity(element);
				if (result == null) result = caseNamedElement(element);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.PRIMITIVE_TYPE: {
				PrimitiveType primitiveType = (PrimitiveType)theEObject;
				T result = casePrimitiveType(primitiveType);
				if (result == null) result = caseType(primitiveType);
				if (result == null) result = caseNamedElement(primitiveType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.PRIMITIVE_ELEMENT: {
				PrimitiveElement primitiveElement = (PrimitiveElement)theEObject;
				T result = casePrimitiveElement(primitiveElement);
				if (result == null) result = caseElement(primitiveElement);
				if (result == null) result = caseConcreteEntity(primitiveElement);
				if (result == null) result = caseBindableEntity(primitiveElement);
				if (result == null) result = caseNamedElement(primitiveElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.STRING: {
				fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String string = (fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String)theEObject;
				T result = caseString(string);
				if (result == null) result = casePrimitiveType(string);
				if (result == null) result = caseType(string);
				if (result == null) result = caseNamedElement(string);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.BOOLEAN: {
				fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean boolean_ = (fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean)theEObject;
				T result = caseBoolean(boolean_);
				if (result == null) result = casePrimitiveType(boolean_);
				if (result == null) result = caseType(boolean_);
				if (result == null) result = caseNamedElement(boolean_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.INTEGER: {
				fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer integer = (fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer)theEObject;
				T result = caseInteger(integer);
				if (result == null) result = casePrimitiveType(integer);
				if (result == null) result = caseType(integer);
				if (result == null) result = caseNamedElement(integer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.REAL: {
				Real real = (Real)theEObject;
				T result = caseReal(real);
				if (result == null) result = casePrimitiveType(real);
				if (result == null) result = caseType(real);
				if (result == null) result = caseNamedElement(real);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.CHAR: {
				Char char_ = (Char)theEObject;
				T result = caseChar(char_);
				if (result == null) result = casePrimitiveType(char_);
				if (result == null) result = caseType(char_);
				if (result == null) result = caseNamedElement(char_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.STRING_ELEMENT: {
				StringElement stringElement = (StringElement)theEObject;
				T result = caseStringElement(stringElement);
				if (result == null) result = casePrimitiveElement(stringElement);
				if (result == null) result = caseElement(stringElement);
				if (result == null) result = caseConcreteEntity(stringElement);
				if (result == null) result = caseBindableEntity(stringElement);
				if (result == null) result = caseNamedElement(stringElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.BOOLEAN_ELEMENT: {
				BooleanElement booleanElement = (BooleanElement)theEObject;
				T result = caseBooleanElement(booleanElement);
				if (result == null) result = casePrimitiveElement(booleanElement);
				if (result == null) result = caseElement(booleanElement);
				if (result == null) result = caseConcreteEntity(booleanElement);
				if (result == null) result = caseBindableEntity(booleanElement);
				if (result == null) result = caseNamedElement(booleanElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.INTEGER_ELEMENT: {
				IntegerElement integerElement = (IntegerElement)theEObject;
				T result = caseIntegerElement(integerElement);
				if (result == null) result = casePrimitiveElement(integerElement);
				if (result == null) result = caseElement(integerElement);
				if (result == null) result = caseConcreteEntity(integerElement);
				if (result == null) result = caseBindableEntity(integerElement);
				if (result == null) result = caseNamedElement(integerElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.REAL_ELEMENT: {
				RealElement realElement = (RealElement)theEObject;
				T result = caseRealElement(realElement);
				if (result == null) result = casePrimitiveElement(realElement);
				if (result == null) result = caseElement(realElement);
				if (result == null) result = caseConcreteEntity(realElement);
				if (result == null) result = caseBindableEntity(realElement);
				if (result == null) result = caseNamedElement(realElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.CHAR_ELEMENT: {
				CharElement charElement = (CharElement)theEObject;
				T result = caseCharElement(charElement);
				if (result == null) result = casePrimitiveElement(charElement);
				if (result == null) result = caseElement(charElement);
				if (result == null) result = caseConcreteEntity(charElement);
				if (result == null) result = caseBindableEntity(charElement);
				if (result == null) result = caseNamedElement(charElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.RECORD: {
				fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record record = (fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record)theEObject;
				T result = caseRecord(record);
				if (result == null) result = caseType(record);
				if (result == null) result = caseNamedElement(record);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.SEQUENCE_TYPE: {
				SequenceType sequenceType = (SequenceType)theEObject;
				T result = caseSequenceType(sequenceType);
				if (result == null) result = caseType(sequenceType);
				if (result == null) result = caseNamedElement(sequenceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.FIELD: {
				Field field = (Field)theEObject;
				T result = caseField(field);
				if (result == null) result = caseNamedElement(field);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.RECORD_ELEMENT: {
				RecordElement recordElement = (RecordElement)theEObject;
				T result = caseRecordElement(recordElement);
				if (result == null) result = caseElement(recordElement);
				if (result == null) result = caseConcreteEntity(recordElement);
				if (result == null) result = caseBindableEntity(recordElement);
				if (result == null) result = caseNamedElement(recordElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.BOX: {
				Box box = (Box)theEObject;
				T result = caseBox(box);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.SEQUENCE_ELEMENT: {
				SequenceElement sequenceElement = (SequenceElement)theEObject;
				T result = caseSequenceElement(sequenceElement);
				if (result == null) result = caseElement(sequenceElement);
				if (result == null) result = caseConcreteEntity(sequenceElement);
				if (result == null) result = caseBindableEntity(sequenceElement);
				if (result == null) result = caseNamedElement(sequenceElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.DISCRETE_CLOCK_TYPE: {
				DiscreteClockType discreteClockType = (DiscreteClockType)theEObject;
				T result = caseDiscreteClockType(discreteClockType);
				if (result == null) result = caseType(discreteClockType);
				if (result == null) result = caseNamedElement(discreteClockType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.DENSE_CLOCK_TYPE: {
				DenseClockType denseClockType = (DenseClockType)theEObject;
				T result = caseDenseClockType(denseClockType);
				if (result == null) result = caseType(denseClockType);
				if (result == null) result = caseNamedElement(denseClockType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BasicTypePackage.ENUMERATION_TYPE: {
				EnumerationType enumerationType = (EnumerationType)theEObject;
				T result = caseEnumerationType(enumerationType);
				if (result == null) result = casePrimitiveType(enumerationType);
				if (result == null) result = caseType(enumerationType);
				if (result == null) result = caseNamedElement(enumerationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseType(Type object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseElement(Element object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Type</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T casePrimitiveType(PrimitiveType object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T casePrimitiveElement(PrimitiveElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>String</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseString(fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBoolean(fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Integer</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseInteger(fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseReal(Real object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Char</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Char</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseChar(Char object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>String Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseStringElement(StringElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBooleanElement(BooleanElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntegerElement(IntegerElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealElement(RealElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Char Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Char Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseCharElement(CharElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Record</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecord(fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequence Type</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequence Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSequenceType(SequenceType object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Field</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseField(Field object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Record Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRecordElement(RecordElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Box</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Box</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBox(Box object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Sequence Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequence Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSequenceElement(SequenceElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Clock Type</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Clock Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseDiscreteClockType(DiscreteClockType object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Dense Clock Type</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dense Clock Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseDenseClockType(DenseClockType object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Type</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseEnumerationType(EnumerationType object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNamedElement(NamedElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Bindable Entity</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bindable Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBindableEntity(BindableEntity object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Concrete Entity</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concrete Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseConcreteEntity(ConcreteEntity object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
  public T defaultCase(EObject object)
  {
		return null;
	}

} //BasicTypeSwitch
