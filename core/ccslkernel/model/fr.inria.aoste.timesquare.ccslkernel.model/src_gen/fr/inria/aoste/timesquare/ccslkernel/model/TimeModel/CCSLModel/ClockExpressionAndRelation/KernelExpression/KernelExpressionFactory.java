/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage
 * @generated
 */
public interface KernelExpressionFactory extends EFactory
{
  /**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  KernelExpressionFactory eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionFactoryImpl.init();

  /**
	 * Returns a new object of class '<em>Up To</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Up To</em>'.
	 * @generated
	 */
  UpTo createUpTo();

  /**
	 * Returns a new object of class '<em>Defer</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defer</em>'.
	 * @generated
	 */
  Defer createDefer();

  /**
	 * Returns a new object of class '<em>Strict Sampling</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Strict Sampling</em>'.
	 * @generated
	 */
  StrictSampling createStrictSampling();

  /**
	 * Returns a new object of class '<em>Concatenation</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concatenation</em>'.
	 * @generated
	 */
  Concatenation createConcatenation();

  /**
	 * Returns a new object of class '<em>Union</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Union</em>'.
	 * @generated
	 */
  Union createUnion();

  /**
	 * Returns a new object of class '<em>Intersection</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Intersection</em>'.
	 * @generated
	 */
  Intersection createIntersection();

  /**
	 * Returns a new object of class '<em>Sup</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sup</em>'.
	 * @generated
	 */
  Sup createSup();

  /**
	 * Returns a new object of class '<em>Inf</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inf</em>'.
	 * @generated
	 */
  Inf createInf();

  /**
	 * Returns a new object of class '<em>Non Strict Sampling</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Strict Sampling</em>'.
	 * @generated
	 */
  NonStrictSampling createNonStrictSampling();

  /**
	 * Returns a new object of class '<em>Discretization</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discretization</em>'.
	 * @generated
	 */
  Discretization createDiscretization();

  /**
	 * Returns a new object of class '<em>Death</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Death</em>'.
	 * @generated
	 */
  Death createDeath();

  /**
	 * Returns a new object of class '<em>Wait</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wait</em>'.
	 * @generated
	 */
	Wait createWait();

		/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
  KernelExpressionPackage getKernelExpressionPackage();

} //KernelExpressionFactory
