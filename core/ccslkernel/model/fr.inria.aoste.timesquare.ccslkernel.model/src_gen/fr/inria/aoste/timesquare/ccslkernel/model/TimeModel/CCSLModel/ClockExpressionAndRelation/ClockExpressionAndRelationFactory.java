/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage
 * @generated
 */
public interface ClockExpressionAndRelationFactory extends EFactory
{
  /**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  ClockExpressionAndRelationFactory eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationFactoryImpl.init();

  /**
	 * Returns a new object of class '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression</em>'.
	 * @generated
	 */
  Expression createExpression();

  /**
	 * Returns a new object of class '<em>User Expression Definition</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Expression Definition</em>'.
	 * @generated
	 */
  UserExpressionDefinition createUserExpressionDefinition();

  /**
	 * Returns a new object of class '<em>Expression Library</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression Library</em>'.
	 * @generated
	 */
  ExpressionLibrary createExpressionLibrary();

  /**
	 * Returns a new object of class '<em>Binding</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Binding</em>'.
	 * @generated
	 */
  Binding createBinding();

  /**
	 * Returns a new object of class '<em>Conditional Expression Definition</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conditional Expression Definition</em>'.
	 * @generated
	 */
  ConditionalExpressionDefinition createConditionalExpressionDefinition();

  /**
	 * Returns a new object of class '<em>Expr Case</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expr Case</em>'.
	 * @generated
	 */
  ExprCase createExprCase();

  /**
	 * Returns a new object of class '<em>User Relation Definition</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Relation Definition</em>'.
	 * @generated
	 */
  UserRelationDefinition createUserRelationDefinition();

  /**
	 * Returns a new object of class '<em>Relation</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relation</em>'.
	 * @generated
	 */
  Relation createRelation();

  /**
	 * Returns a new object of class '<em>Conditional Relation Definition</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conditional Relation Definition</em>'.
	 * @generated
	 */
  ConditionalRelationDefinition createConditionalRelationDefinition();

  /**
	 * Returns a new object of class '<em>Relation Library</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relation Library</em>'.
	 * @generated
	 */
  RelationLibrary createRelationLibrary();

  /**
	 * Returns a new object of class '<em>Rel Case</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rel Case</em>'.
	 * @generated
	 */
  RelCase createRelCase();

  /**
	 * Returns a new object of class '<em>Library</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Library</em>'.
	 * @generated
	 */
  Library createLibrary();

  /**
	 * Returns a new object of class '<em>Abstract Entity</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Entity</em>'.
	 * @generated
	 */
  AbstractEntity createAbstractEntity();

  /**
	 * Returns a new object of class '<em>Concrete Entity</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concrete Entity</em>'.
	 * @generated
	 */
  ConcreteEntity createConcreteEntity();

  /**
	 * Returns a new object of class '<em>Relation Declaration</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relation Declaration</em>'.
	 * @generated
	 */
  RelationDeclaration createRelationDeclaration();

  /**
	 * Returns a new object of class '<em>Expression Declaration</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression Declaration</em>'.
	 * @generated
	 */
  ExpressionDeclaration createExpressionDeclaration();

  /**
	 * Returns a new object of class '<em>External Relation Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>External Relation Definition</em>'.
	 * @generated
	 */
	ExternalRelationDefinition createExternalRelationDefinition();

		/**
	 * Returns a new object of class '<em>External Expression Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>External Expression Definition</em>'.
	 * @generated
	 */
	ExternalExpressionDefinition createExternalExpressionDefinition();

		/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
  ClockExpressionAndRelationPackage getClockExpressionAndRelationPackage();

} //ClockExpressionAndRelationFactory
