/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class KernelRelationFactoryImpl extends EFactoryImpl implements KernelRelationFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public static KernelRelationFactory init()
  {
		try {
			KernelRelationFactory theKernelRelationFactory = (KernelRelationFactory)EPackage.Registry.INSTANCE.getEFactory(KernelRelationPackage.eNS_URI);
			if (theKernelRelationFactory != null) {
				return theKernelRelationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new KernelRelationFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public KernelRelationFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case KernelRelationPackage.SUB_CLOCK: return createSubClock();
			case KernelRelationPackage.COINCIDENCE: return createCoincidence();
			case KernelRelationPackage.EXCLUSION: return createExclusion();
			case KernelRelationPackage.PRECEDENCE: return createPrecedence();
			case KernelRelationPackage.NON_STRICT_PRECEDENCE: return createNonStrictPrecedence();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SubClock createSubClock()
  {
		SubClockImpl subClock = new SubClockImpl();
		return subClock;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Coincidence createCoincidence()
  {
		CoincidenceImpl coincidence = new CoincidenceImpl();
		return coincidence;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Exclusion createExclusion()
  {
		ExclusionImpl exclusion = new ExclusionImpl();
		return exclusion;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Precedence createPrecedence()
  {
		PrecedenceImpl precedence = new PrecedenceImpl();
		return precedence;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public NonStrictPrecedence createNonStrictPrecedence()
  {
		NonStrictPrecedenceImpl nonStrictPrecedence = new NonStrictPrecedenceImpl();
		return nonStrictPrecedence;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public KernelRelationPackage getKernelRelationPackage()
  {
		return (KernelRelationPackage)getEPackage();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static KernelRelationPackage getPackage()
  {
		return KernelRelationPackage.eINSTANCE;
	}

} //KernelRelationFactoryImpl
