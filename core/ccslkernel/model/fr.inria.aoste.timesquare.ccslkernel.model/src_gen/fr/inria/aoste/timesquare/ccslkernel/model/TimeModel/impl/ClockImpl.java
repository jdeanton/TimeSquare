/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.ElementImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Clock</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ClockImpl#getTickingEvent <em>Ticking Event</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ClockImpl#getDefiningEvent <em>Defining Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClockImpl extends ElementImpl implements Clock
{
  /**
	 * The cached value of the '{@link #getTickingEvent() <em>Ticking Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getTickingEvent()
	 * @generated
	 * @ordered
	 */
  protected Event tickingEvent;

  /**
	 * The cached value of the '{@link #getDefiningEvent() <em>Defining Event</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getDefiningEvent()
	 * @generated
	 * @ordered
	 */
  protected Event definingEvent;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected ClockImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return TimeModelPackage.Literals.CLOCK;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Event getTickingEvent()
  {
		return tickingEvent;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetTickingEvent(Event newTickingEvent, NotificationChain msgs)
  {
		Event oldTickingEvent = tickingEvent;
		tickingEvent = newTickingEvent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimeModelPackage.CLOCK__TICKING_EVENT, oldTickingEvent, newTickingEvent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setTickingEvent(Event newTickingEvent)
  {
		if (newTickingEvent != tickingEvent) {
			NotificationChain msgs = null;
			if (tickingEvent != null)
				msgs = ((InternalEObject)tickingEvent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimeModelPackage.CLOCK__TICKING_EVENT, null, msgs);
			if (newTickingEvent != null)
				msgs = ((InternalEObject)newTickingEvent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimeModelPackage.CLOCK__TICKING_EVENT, null, msgs);
			msgs = basicSetTickingEvent(newTickingEvent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimeModelPackage.CLOCK__TICKING_EVENT, newTickingEvent, newTickingEvent));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Event getDefiningEvent()
  {
		if (definingEvent != null && definingEvent.eIsProxy()) {
			InternalEObject oldDefiningEvent = (InternalEObject)definingEvent;
			definingEvent = (Event)eResolveProxy(oldDefiningEvent);
			if (definingEvent != oldDefiningEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TimeModelPackage.CLOCK__DEFINING_EVENT, oldDefiningEvent, definingEvent));
			}
		}
		return definingEvent;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public Event basicGetDefiningEvent()
  {
		return definingEvent;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setDefiningEvent(Event newDefiningEvent)
  {
		Event oldDefiningEvent = definingEvent;
		definingEvent = newDefiningEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TimeModelPackage.CLOCK__DEFINING_EVENT, oldDefiningEvent, definingEvent));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case TimeModelPackage.CLOCK__TICKING_EVENT:
				return basicSetTickingEvent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case TimeModelPackage.CLOCK__TICKING_EVENT:
				return getTickingEvent();
			case TimeModelPackage.CLOCK__DEFINING_EVENT:
				if (resolve) return getDefiningEvent();
				return basicGetDefiningEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case TimeModelPackage.CLOCK__TICKING_EVENT:
				setTickingEvent((Event)newValue);
				return;
			case TimeModelPackage.CLOCK__DEFINING_EVENT:
				setDefiningEvent((Event)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case TimeModelPackage.CLOCK__TICKING_EVENT:
				setTickingEvent((Event)null);
				return;
			case TimeModelPackage.CLOCK__DEFINING_EVENT:
				setDefiningEvent((Event)null);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case TimeModelPackage.CLOCK__TICKING_EVENT:
				return tickingEvent != null;
			case TimeModelPackage.CLOCK__DEFINING_EVENT:
				return definingEvent != null;
		}
		return super.eIsSet(featureID);
	}

} //ClockImpl
