/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationFactory
 * @model kind="package"
 * @generated
 */
public interface ClockExpressionAndRelationPackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNAME = "ClockExpressionAndRelation";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_URI = "http://fr.inria.aoste.timemodel.ccslmodel.clockexpressionandrelation";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_PREFIX = "fr.inria.aoste.timemodel.ccslmodel.clockexpressionandrelation";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  ClockExpressionAndRelationPackage eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl.init();

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.BindableEntityImpl <em>Bindable Entity</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.BindableEntityImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getBindableEntity()
	 * @generated
	 */
  int BINDABLE_ENTITY = 15;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINDABLE_ENTITY__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The number of structural features of the '<em>Bindable Entity</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINDABLE_ENTITY_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConcreteEntityImpl <em>Concrete Entity</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConcreteEntityImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getConcreteEntity()
	 * @generated
	 */
  int CONCRETE_ENTITY = 16;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONCRETE_ENTITY__NAME = BINDABLE_ENTITY__NAME;

  /**
	 * The number of structural features of the '<em>Concrete Entity</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONCRETE_ENTITY_FEATURE_COUNT = BINDABLE_ENTITY_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExpression()
	 * @generated
	 */
  int EXPRESSION = 0;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION__NAME = CONCRETE_ENTITY__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION__TYPE = CONCRETE_ENTITY_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION__BINDINGS = CONCRETE_ENTITY_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_FEATURE_COUNT = CONCRETE_ENTITY_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionDefinitionImpl <em>Expression Definition</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionDefinitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExpressionDefinition()
	 * @generated
	 */
  int EXPRESSION_DEFINITION = 14;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_DEFINITION__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_DEFINITION__DECLARATION = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Expression Definition</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_DEFINITION_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserExpressionDefinitionImpl <em>User Expression Definition</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserExpressionDefinitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getUserExpressionDefinition()
	 * @generated
	 */
  int USER_EXPRESSION_DEFINITION = 1;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_EXPRESSION_DEFINITION__NAME = EXPRESSION_DEFINITION__NAME;

  /**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_EXPRESSION_DEFINITION__DECLARATION = EXPRESSION_DEFINITION__DECLARATION;

  /**
	 * The feature id for the '<em><b>Concrete Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES = EXPRESSION_DEFINITION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Root Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION = EXPRESSION_DEFINITION_FEATURE_COUNT + 1;

  /**
	 * The feature id for the '<em><b>Classical Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS = EXPRESSION_DEFINITION_FEATURE_COUNT + 2;

  /**
	 * The number of structural features of the '<em>User Expression Definition</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_EXPRESSION_DEFINITION_FEATURE_COUNT = EXPRESSION_DEFINITION_FEATURE_COUNT + 3;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionLibraryImpl <em>Expression Library</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionLibraryImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExpressionLibrary()
	 * @generated
	 */
  int EXPRESSION_LIBRARY = 2;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_LIBRARY__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Expression Definitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_LIBRARY__ELEMENTS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The feature id for the '<em><b>Expression Declarations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

  /**
	 * The number of structural features of the '<em>Expression Library</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_LIBRARY_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.BindingImpl <em>Binding</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.BindingImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getBinding()
	 * @generated
	 */
  int BINDING = 3;

  /**
	 * The feature id for the '<em><b>Abstract</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINDING__ABSTRACT = 0;

  /**
	 * The feature id for the '<em><b>Bindable</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINDING__BINDABLE = 1;

  /**
	 * The number of structural features of the '<em>Binding</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINDING_FEATURE_COUNT = 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalExpressionDefinitionImpl <em>Conditional Expression Definition</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalExpressionDefinitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getConditionalExpressionDefinition()
	 * @generated
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION = 4;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION__NAME = USER_EXPRESSION_DEFINITION__NAME;

  /**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION__DECLARATION = USER_EXPRESSION_DEFINITION__DECLARATION;

  /**
	 * The feature id for the '<em><b>Concrete Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION__CONCRETE_ENTITIES = USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES;

  /**
	 * The feature id for the '<em><b>Root Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION__ROOT_EXPRESSION = USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION;

  /**
	 * The feature id for the '<em><b>Classical Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS = USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS;

  /**
	 * The feature id for the '<em><b>Expr Cases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES = USER_EXPRESSION_DEFINITION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Default Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION = USER_EXPRESSION_DEFINITION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Conditional Expression Definition</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_EXPRESSION_DEFINITION_FEATURE_COUNT = USER_EXPRESSION_DEFINITION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExprCaseImpl <em>Expr Case</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExprCaseImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExprCase()
	 * @generated
	 */
  int EXPR_CASE = 5;

  /**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPR_CASE__CONDITION = 0;

  /**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPR_CASE__EXPRESSION = 1;

  /**
	 * The number of structural features of the '<em>Expr Case</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPR_CASE_FEATURE_COUNT = 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDefinitionImpl <em>Relation Definition</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDefinitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelationDefinition()
	 * @generated
	 */
  int RELATION_DEFINITION = 6;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_DEFINITION__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_DEFINITION__DECLARATION = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Relation Definition</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_DEFINITION_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserRelationDefinitionImpl <em>User Relation Definition</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserRelationDefinitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getUserRelationDefinition()
	 * @generated
	 */
  int USER_RELATION_DEFINITION = 7;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_RELATION_DEFINITION__NAME = RELATION_DEFINITION__NAME;

  /**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_RELATION_DEFINITION__DECLARATION = RELATION_DEFINITION__DECLARATION;

  /**
	 * The feature id for the '<em><b>Concrete Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_RELATION_DEFINITION__CONCRETE_ENTITIES = RELATION_DEFINITION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Classical Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_RELATION_DEFINITION__CLASSICAL_EXPRESSIONS = RELATION_DEFINITION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>User Relation Definition</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int USER_RELATION_DEFINITION_FEATURE_COUNT = RELATION_DEFINITION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationImpl <em>Relation</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelation()
	 * @generated
	 */
  int RELATION = 8;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION__NAME = CONCRETE_ENTITY__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION__TYPE = CONCRETE_ENTITY_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION__BINDINGS = CONCRETE_ENTITY_FEATURE_COUNT + 1;

  /**
	 * The feature id for the '<em><b>Is An Assertion</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION__IS_AN_ASSERTION = CONCRETE_ENTITY_FEATURE_COUNT + 2;

  /**
	 * The number of structural features of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_FEATURE_COUNT = CONCRETE_ENTITY_FEATURE_COUNT + 3;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalRelationDefinitionImpl <em>Conditional Relation Definition</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalRelationDefinitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getConditionalRelationDefinition()
	 * @generated
	 */
  int CONDITIONAL_RELATION_DEFINITION = 9;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_RELATION_DEFINITION__NAME = USER_RELATION_DEFINITION__NAME;

  /**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_RELATION_DEFINITION__DECLARATION = USER_RELATION_DEFINITION__DECLARATION;

  /**
	 * The feature id for the '<em><b>Concrete Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_RELATION_DEFINITION__CONCRETE_ENTITIES = USER_RELATION_DEFINITION__CONCRETE_ENTITIES;

  /**
	 * The feature id for the '<em><b>Classical Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_RELATION_DEFINITION__CLASSICAL_EXPRESSIONS = USER_RELATION_DEFINITION__CLASSICAL_EXPRESSIONS;

  /**
	 * The feature id for the '<em><b>Rel Cases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_RELATION_DEFINITION__REL_CASES = USER_RELATION_DEFINITION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Default Relation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION = USER_RELATION_DEFINITION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Conditional Relation Definition</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONDITIONAL_RELATION_DEFINITION_FEATURE_COUNT = USER_RELATION_DEFINITION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationLibraryImpl <em>Relation Library</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationLibraryImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelationLibrary()
	 * @generated
	 */
  int RELATION_LIBRARY = 10;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_LIBRARY__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Relation Definitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_LIBRARY__RELATION_DEFINITIONS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_LIBRARY__ELEMENTS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The feature id for the '<em><b>Relation Declarations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_LIBRARY__RELATION_DECLARATIONS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

  /**
	 * The number of structural features of the '<em>Relation Library</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_LIBRARY_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelCaseImpl <em>Rel Case</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelCaseImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelCase()
	 * @generated
	 */
  int REL_CASE = 11;

  /**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REL_CASE__CONDITION = 0;

  /**
	 * The feature id for the '<em><b>Relation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REL_CASE__RELATION = 1;

  /**
	 * The number of structural features of the '<em>Rel Case</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REL_CASE_FEATURE_COUNT = 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.LibraryImpl <em>Library</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.LibraryImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getLibrary()
	 * @generated
	 */
  int LIBRARY = 12;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LIBRARY__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Expression Libraries</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LIBRARY__EXPRESSION_LIBRARIES = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Relation Libraries</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LIBRARY__RELATION_LIBRARIES = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The feature id for the '<em><b>Predefined Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LIBRARY__PREDEFINED_TYPES = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

  /**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LIBRARY__IMPORTS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

  /**
	 * The number of structural features of the '<em>Library</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int LIBRARY_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.AbstractEntityImpl <em>Abstract Entity</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.AbstractEntityImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getAbstractEntity()
	 * @generated
	 */
  int ABSTRACT_ENTITY = 13;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ABSTRACT_ENTITY__NAME = BINDABLE_ENTITY__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ABSTRACT_ENTITY__TYPE = BINDABLE_ENTITY_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Desired Event Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ENTITY__DESIRED_EVENT_KIND = BINDABLE_ENTITY_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Abstract Entity</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ABSTRACT_ENTITY_FEATURE_COUNT = BINDABLE_ENTITY_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDeclarationImpl <em>Relation Declaration</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDeclarationImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelationDeclaration()
	 * @generated
	 */
  int RELATION_DECLARATION = 17;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_DECLARATION__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_DECLARATION__PARAMETERS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Relation Declaration</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RELATION_DECLARATION_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionDeclarationImpl <em>Expression Declaration</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionDeclarationImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExpressionDeclaration()
	 * @generated
	 */
  int EXPRESSION_DECLARATION = 18;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_DECLARATION__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_DECLARATION__PARAMETERS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_DECLARATION__RETURN_TYPE = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Expression Declaration</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EXPRESSION_DECLARATION_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;


  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExternalRelationDefinitionImpl <em>External Relation Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExternalRelationDefinitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExternalRelationDefinition()
	 * @generated
	 */
	int EXTERNAL_RELATION_DEFINITION = 19;

		/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_RELATION_DEFINITION__NAME = RELATION_DEFINITION__NAME;

		/**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_RELATION_DEFINITION__DECLARATION = RELATION_DEFINITION__DECLARATION;

		/**
	 * The number of structural features of the '<em>External Relation Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_RELATION_DEFINITION_FEATURE_COUNT = RELATION_DEFINITION_FEATURE_COUNT + 0;

		/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExternalExpressionDefinitionImpl <em>External Expression Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExternalExpressionDefinitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExternalExpressionDefinition()
	 * @generated
	 */
	int EXTERNAL_EXPRESSION_DEFINITION = 20;

		/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EXPRESSION_DEFINITION__NAME = EXPRESSION_DEFINITION__NAME;

		/**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EXPRESSION_DEFINITION__DECLARATION = EXPRESSION_DEFINITION__DECLARATION;

		/**
	 * The number of structural features of the '<em>External Expression Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EXPRESSION_DEFINITION_FEATURE_COUNT = EXPRESSION_DEFINITION_FEATURE_COUNT + 0;


		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression
	 * @generated
	 */
  EClass getExpression();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression#getType()
	 * @see #getExpression()
	 * @generated
	 */
  EReference getExpression_Type();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bindings</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression#getBindings()
	 * @see #getExpression()
	 * @generated
	 */
  EReference getExpression_Bindings();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition <em>User Expression Definition</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Expression Definition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition
	 * @generated
	 */
  EClass getUserExpressionDefinition();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition#getConcreteEntities <em>Concrete Entities</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Concrete Entities</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition#getConcreteEntities()
	 * @see #getUserExpressionDefinition()
	 * @generated
	 */
  EReference getUserExpressionDefinition_ConcreteEntities();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition#getRootExpression <em>Root Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition#getRootExpression()
	 * @see #getUserExpressionDefinition()
	 * @generated
	 */
  EReference getUserExpressionDefinition_RootExpression();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition#getClassicalExpressions <em>Classical Expressions</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classical Expressions</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition#getClassicalExpressions()
	 * @see #getUserExpressionDefinition()
	 * @generated
	 */
  EReference getUserExpressionDefinition_ClassicalExpressions();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary <em>Expression Library</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Library</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary
	 * @generated
	 */
  EClass getExpressionLibrary();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary#getExpressionDefinitions <em>Expression Definitions</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expression Definitions</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary#getExpressionDefinitions()
	 * @see #getExpressionLibrary()
	 * @generated
	 */
  EReference getExpressionLibrary_ExpressionDefinitions();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary#getElements()
	 * @see #getExpressionLibrary()
	 * @generated
	 */
  EReference getExpressionLibrary_Elements();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary#getExpressionDeclarations <em>Expression Declarations</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expression Declarations</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary#getExpressionDeclarations()
	 * @see #getExpressionLibrary()
	 * @generated
	 */
  EReference getExpressionLibrary_ExpressionDeclarations();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binding</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding
	 * @generated
	 */
  EClass getBinding();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding#getAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Abstract</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding#getAbstract()
	 * @see #getBinding()
	 * @generated
	 */
  EReference getBinding_Abstract();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding#getBindable <em>Bindable</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bindable</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding#getBindable()
	 * @see #getBinding()
	 * @generated
	 */
  EReference getBinding_Bindable();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition <em>Conditional Expression Definition</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Expression Definition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition
	 * @generated
	 */
  EClass getConditionalExpressionDefinition();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition#getExprCases <em>Expr Cases</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expr Cases</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition#getExprCases()
	 * @see #getConditionalExpressionDefinition()
	 * @generated
	 */
  EReference getConditionalExpressionDefinition_ExprCases();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition#getDefaultExpression <em>Default Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition#getDefaultExpression()
	 * @see #getConditionalExpressionDefinition()
	 * @generated
	 */
  EReference getConditionalExpressionDefinition_DefaultExpression();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase <em>Expr Case</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expr Case</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase
	 * @generated
	 */
  EClass getExprCase();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase#getCondition()
	 * @see #getExprCase()
	 * @generated
	 */
  EReference getExprCase_Condition();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase#getExpression()
	 * @see #getExprCase()
	 * @generated
	 */
  EReference getExprCase_Expression();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition <em>Relation Definition</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation Definition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition
	 * @generated
	 */
  EClass getRelationDefinition();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Declaration</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition#getDeclaration()
	 * @see #getRelationDefinition()
	 * @generated
	 */
  EReference getRelationDefinition_Declaration();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition <em>User Relation Definition</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Relation Definition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition
	 * @generated
	 */
  EClass getUserRelationDefinition();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition#getConcreteEntities <em>Concrete Entities</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Concrete Entities</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition#getConcreteEntities()
	 * @see #getUserRelationDefinition()
	 * @generated
	 */
  EReference getUserRelationDefinition_ConcreteEntities();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition#getClassicalExpressions <em>Classical Expressions</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classical Expressions</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition#getClassicalExpressions()
	 * @see #getUserRelationDefinition()
	 * @generated
	 */
  EReference getUserRelationDefinition_ClassicalExpressions();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation
	 * @generated
	 */
  EClass getRelation();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation#getType()
	 * @see #getRelation()
	 * @generated
	 */
  EReference getRelation_Type();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bindings</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation#getBindings()
	 * @see #getRelation()
	 * @generated
	 */
  EReference getRelation_Bindings();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation#getIsAnAssertion <em>Is An Assertion</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is An Assertion</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation#getIsAnAssertion()
	 * @see #getRelation()
	 * @generated
	 */
  EAttribute getRelation_IsAnAssertion();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition <em>Conditional Relation Definition</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Relation Definition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition
	 * @generated
	 */
  EClass getConditionalRelationDefinition();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition#getRelCases <em>Rel Cases</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rel Cases</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition#getRelCases()
	 * @see #getConditionalRelationDefinition()
	 * @generated
	 */
  EReference getConditionalRelationDefinition_RelCases();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition#getDefaultRelation <em>Default Relation</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Default Relation</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition#getDefaultRelation()
	 * @see #getConditionalRelationDefinition()
	 * @generated
	 */
  EReference getConditionalRelationDefinition_DefaultRelation();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary <em>Relation Library</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation Library</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary
	 * @generated
	 */
  EClass getRelationLibrary();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary#getRelationDefinitions <em>Relation Definitions</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relation Definitions</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary#getRelationDefinitions()
	 * @see #getRelationLibrary()
	 * @generated
	 */
  EReference getRelationLibrary_RelationDefinitions();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary#getElements()
	 * @see #getRelationLibrary()
	 * @generated
	 */
  EReference getRelationLibrary_Elements();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary#getRelationDeclarations <em>Relation Declarations</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relation Declarations</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary#getRelationDeclarations()
	 * @see #getRelationLibrary()
	 * @generated
	 */
  EReference getRelationLibrary_RelationDeclarations();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase <em>Rel Case</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rel Case</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase
	 * @generated
	 */
  EClass getRelCase();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase#getCondition()
	 * @see #getRelCase()
	 * @generated
	 */
  EReference getRelCase_Condition();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relation</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase#getRelation()
	 * @see #getRelCase()
	 * @generated
	 */
  EReference getRelCase_Relation();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library <em>Library</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Library</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library
	 * @generated
	 */
  EClass getLibrary();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library#getExpressionLibraries <em>Expression Libraries</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expression Libraries</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library#getExpressionLibraries()
	 * @see #getLibrary()
	 * @generated
	 */
  EReference getLibrary_ExpressionLibraries();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library#getRelationLibraries <em>Relation Libraries</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relation Libraries</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library#getRelationLibraries()
	 * @see #getLibrary()
	 * @generated
	 */
  EReference getLibrary_RelationLibraries();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library#getPredefinedTypes <em>Predefined Types</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Predefined Types</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library#getPredefinedTypes()
	 * @see #getLibrary()
	 * @generated
	 */
  EReference getLibrary_PredefinedTypes();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library#getImports()
	 * @see #getLibrary()
	 * @generated
	 */
  EReference getLibrary_Imports();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity <em>Abstract Entity</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Entity</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity
	 * @generated
	 */
  EClass getAbstractEntity();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity#getType()
	 * @see #getAbstractEntity()
	 * @generated
	 */
  EReference getAbstractEntity_Type();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity#getDesiredEventKind <em>Desired Event Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Desired Event Kind</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity#getDesiredEventKind()
	 * @see #getAbstractEntity()
	 * @generated
	 */
	EAttribute getAbstractEntity_DesiredEventKind();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition <em>Expression Definition</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Definition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition
	 * @generated
	 */
  EClass getExpressionDefinition();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Declaration</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition#getDeclaration()
	 * @see #getExpressionDefinition()
	 * @generated
	 */
  EReference getExpressionDefinition_Declaration();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity <em>Bindable Entity</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bindable Entity</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity
	 * @generated
	 */
  EClass getBindableEntity();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity <em>Concrete Entity</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concrete Entity</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity
	 * @generated
	 */
  EClass getConcreteEntity();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration <em>Relation Declaration</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation Declaration</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration
	 * @generated
	 */
  EClass getRelationDeclaration();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration#getParameters()
	 * @see #getRelationDeclaration()
	 * @generated
	 */
  EReference getRelationDeclaration_Parameters();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration <em>Expression Declaration</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Declaration</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration
	 * @generated
	 */
  EClass getExpressionDeclaration();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration#getParameters()
	 * @see #getExpressionDeclaration()
	 * @generated
	 */
  EReference getExpressionDeclaration_Parameters();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Return Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration#getReturnType()
	 * @see #getExpressionDeclaration()
	 * @generated
	 */
  EReference getExpressionDeclaration_ReturnType();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalRelationDefinition <em>External Relation Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Relation Definition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalRelationDefinition
	 * @generated
	 */
	EClass getExternalRelationDefinition();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition <em>External Expression Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Expression Definition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition
	 * @generated
	 */
	EClass getExternalExpressionDefinition();

		/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
  ClockExpressionAndRelationFactory getClockExpressionAndRelationFactory();

  /**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExpression()
		 * @generated
		 */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPRESSION__TYPE = eINSTANCE.getExpression_Type();

    /**
		 * The meta object literal for the '<em><b>Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPRESSION__BINDINGS = eINSTANCE.getExpression_Bindings();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserExpressionDefinitionImpl <em>User Expression Definition</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserExpressionDefinitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getUserExpressionDefinition()
		 * @generated
		 */
    EClass USER_EXPRESSION_DEFINITION = eINSTANCE.getUserExpressionDefinition();

    /**
		 * The meta object literal for the '<em><b>Concrete Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES = eINSTANCE.getUserExpressionDefinition_ConcreteEntities();

    /**
		 * The meta object literal for the '<em><b>Root Expression</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION = eINSTANCE.getUserExpressionDefinition_RootExpression();

    /**
		 * The meta object literal for the '<em><b>Classical Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS = eINSTANCE.getUserExpressionDefinition_ClassicalExpressions();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionLibraryImpl <em>Expression Library</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionLibraryImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExpressionLibrary()
		 * @generated
		 */
    EClass EXPRESSION_LIBRARY = eINSTANCE.getExpressionLibrary();

    /**
		 * The meta object literal for the '<em><b>Expression Definitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS = eINSTANCE.getExpressionLibrary_ExpressionDefinitions();

    /**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPRESSION_LIBRARY__ELEMENTS = eINSTANCE.getExpressionLibrary_Elements();

    /**
		 * The meta object literal for the '<em><b>Expression Declarations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS = eINSTANCE.getExpressionLibrary_ExpressionDeclarations();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.BindingImpl <em>Binding</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.BindingImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getBinding()
		 * @generated
		 */
    EClass BINDING = eINSTANCE.getBinding();

    /**
		 * The meta object literal for the '<em><b>Abstract</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BINDING__ABSTRACT = eINSTANCE.getBinding_Abstract();

    /**
		 * The meta object literal for the '<em><b>Bindable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BINDING__BINDABLE = eINSTANCE.getBinding_Bindable();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalExpressionDefinitionImpl <em>Conditional Expression Definition</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalExpressionDefinitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getConditionalExpressionDefinition()
		 * @generated
		 */
    EClass CONDITIONAL_EXPRESSION_DEFINITION = eINSTANCE.getConditionalExpressionDefinition();

    /**
		 * The meta object literal for the '<em><b>Expr Cases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES = eINSTANCE.getConditionalExpressionDefinition_ExprCases();

    /**
		 * The meta object literal for the '<em><b>Default Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION = eINSTANCE.getConditionalExpressionDefinition_DefaultExpression();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExprCaseImpl <em>Expr Case</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExprCaseImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExprCase()
		 * @generated
		 */
    EClass EXPR_CASE = eINSTANCE.getExprCase();

    /**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPR_CASE__CONDITION = eINSTANCE.getExprCase_Condition();

    /**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPR_CASE__EXPRESSION = eINSTANCE.getExprCase_Expression();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDefinitionImpl <em>Relation Definition</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDefinitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelationDefinition()
		 * @generated
		 */
    EClass RELATION_DEFINITION = eINSTANCE.getRelationDefinition();

    /**
		 * The meta object literal for the '<em><b>Declaration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RELATION_DEFINITION__DECLARATION = eINSTANCE.getRelationDefinition_Declaration();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserRelationDefinitionImpl <em>User Relation Definition</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserRelationDefinitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getUserRelationDefinition()
		 * @generated
		 */
    EClass USER_RELATION_DEFINITION = eINSTANCE.getUserRelationDefinition();

    /**
		 * The meta object literal for the '<em><b>Concrete Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference USER_RELATION_DEFINITION__CONCRETE_ENTITIES = eINSTANCE.getUserRelationDefinition_ConcreteEntities();

    /**
		 * The meta object literal for the '<em><b>Classical Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference USER_RELATION_DEFINITION__CLASSICAL_EXPRESSIONS = eINSTANCE.getUserRelationDefinition_ClassicalExpressions();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationImpl <em>Relation</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelation()
		 * @generated
		 */
    EClass RELATION = eINSTANCE.getRelation();

    /**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RELATION__TYPE = eINSTANCE.getRelation_Type();

    /**
		 * The meta object literal for the '<em><b>Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RELATION__BINDINGS = eINSTANCE.getRelation_Bindings();

    /**
		 * The meta object literal for the '<em><b>Is An Assertion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute RELATION__IS_AN_ASSERTION = eINSTANCE.getRelation_IsAnAssertion();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalRelationDefinitionImpl <em>Conditional Relation Definition</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalRelationDefinitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getConditionalRelationDefinition()
		 * @generated
		 */
    EClass CONDITIONAL_RELATION_DEFINITION = eINSTANCE.getConditionalRelationDefinition();

    /**
		 * The meta object literal for the '<em><b>Rel Cases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CONDITIONAL_RELATION_DEFINITION__REL_CASES = eINSTANCE.getConditionalRelationDefinition_RelCases();

    /**
		 * The meta object literal for the '<em><b>Default Relation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION = eINSTANCE.getConditionalRelationDefinition_DefaultRelation();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationLibraryImpl <em>Relation Library</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationLibraryImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelationLibrary()
		 * @generated
		 */
    EClass RELATION_LIBRARY = eINSTANCE.getRelationLibrary();

    /**
		 * The meta object literal for the '<em><b>Relation Definitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RELATION_LIBRARY__RELATION_DEFINITIONS = eINSTANCE.getRelationLibrary_RelationDefinitions();

    /**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RELATION_LIBRARY__ELEMENTS = eINSTANCE.getRelationLibrary_Elements();

    /**
		 * The meta object literal for the '<em><b>Relation Declarations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RELATION_LIBRARY__RELATION_DECLARATIONS = eINSTANCE.getRelationLibrary_RelationDeclarations();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelCaseImpl <em>Rel Case</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelCaseImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelCase()
		 * @generated
		 */
    EClass REL_CASE = eINSTANCE.getRelCase();

    /**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REL_CASE__CONDITION = eINSTANCE.getRelCase_Condition();

    /**
		 * The meta object literal for the '<em><b>Relation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REL_CASE__RELATION = eINSTANCE.getRelCase_Relation();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.LibraryImpl <em>Library</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.LibraryImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getLibrary()
		 * @generated
		 */
    EClass LIBRARY = eINSTANCE.getLibrary();

    /**
		 * The meta object literal for the '<em><b>Expression Libraries</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference LIBRARY__EXPRESSION_LIBRARIES = eINSTANCE.getLibrary_ExpressionLibraries();

    /**
		 * The meta object literal for the '<em><b>Relation Libraries</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference LIBRARY__RELATION_LIBRARIES = eINSTANCE.getLibrary_RelationLibraries();

    /**
		 * The meta object literal for the '<em><b>Predefined Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference LIBRARY__PREDEFINED_TYPES = eINSTANCE.getLibrary_PredefinedTypes();

    /**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference LIBRARY__IMPORTS = eINSTANCE.getLibrary_Imports();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.AbstractEntityImpl <em>Abstract Entity</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.AbstractEntityImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getAbstractEntity()
		 * @generated
		 */
    EClass ABSTRACT_ENTITY = eINSTANCE.getAbstractEntity();

    /**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference ABSTRACT_ENTITY__TYPE = eINSTANCE.getAbstractEntity_Type();

    /**
		 * The meta object literal for the '<em><b>Desired Event Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_ENTITY__DESIRED_EVENT_KIND = eINSTANCE.getAbstractEntity_DesiredEventKind();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionDefinitionImpl <em>Expression Definition</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionDefinitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExpressionDefinition()
		 * @generated
		 */
    EClass EXPRESSION_DEFINITION = eINSTANCE.getExpressionDefinition();

    /**
		 * The meta object literal for the '<em><b>Declaration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPRESSION_DEFINITION__DECLARATION = eINSTANCE.getExpressionDefinition_Declaration();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.BindableEntityImpl <em>Bindable Entity</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.BindableEntityImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getBindableEntity()
		 * @generated
		 */
    EClass BINDABLE_ENTITY = eINSTANCE.getBindableEntity();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConcreteEntityImpl <em>Concrete Entity</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConcreteEntityImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getConcreteEntity()
		 * @generated
		 */
    EClass CONCRETE_ENTITY = eINSTANCE.getConcreteEntity();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDeclarationImpl <em>Relation Declaration</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDeclarationImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getRelationDeclaration()
		 * @generated
		 */
    EClass RELATION_DECLARATION = eINSTANCE.getRelationDeclaration();

    /**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RELATION_DECLARATION__PARAMETERS = eINSTANCE.getRelationDeclaration_Parameters();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionDeclarationImpl <em>Expression Declaration</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionDeclarationImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExpressionDeclaration()
		 * @generated
		 */
    EClass EXPRESSION_DECLARATION = eINSTANCE.getExpressionDeclaration();

    /**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPRESSION_DECLARATION__PARAMETERS = eINSTANCE.getExpressionDeclaration_Parameters();

    /**
		 * The meta object literal for the '<em><b>Return Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EXPRESSION_DECLARATION__RETURN_TYPE = eINSTANCE.getExpressionDeclaration_ReturnType();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExternalRelationDefinitionImpl <em>External Relation Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExternalRelationDefinitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExternalRelationDefinition()
		 * @generated
		 */
		EClass EXTERNAL_RELATION_DEFINITION = eINSTANCE.getExternalRelationDefinition();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExternalExpressionDefinitionImpl <em>External Expression Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExternalExpressionDefinitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl#getExternalExpressionDefinition()
		 * @generated
		 */
		EClass EXTERNAL_EXPRESSION_DEFINITION = eINSTANCE.getExternalExpressionDefinition();

  }

} //ClockExpressionAndRelationPackage
