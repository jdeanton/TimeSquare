/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class KernelExpressionFactoryImpl extends EFactoryImpl implements KernelExpressionFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public static KernelExpressionFactory init()
  {
		try {
			KernelExpressionFactory theKernelExpressionFactory = (KernelExpressionFactory)EPackage.Registry.INSTANCE.getEFactory(KernelExpressionPackage.eNS_URI);
			if (theKernelExpressionFactory != null) {
				return theKernelExpressionFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new KernelExpressionFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public KernelExpressionFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case KernelExpressionPackage.UP_TO: return createUpTo();
			case KernelExpressionPackage.DEFER: return createDefer();
			case KernelExpressionPackage.STRICT_SAMPLING: return createStrictSampling();
			case KernelExpressionPackage.CONCATENATION: return createConcatenation();
			case KernelExpressionPackage.UNION: return createUnion();
			case KernelExpressionPackage.INTERSECTION: return createIntersection();
			case KernelExpressionPackage.SUP: return createSup();
			case KernelExpressionPackage.INF: return createInf();
			case KernelExpressionPackage.NON_STRICT_SAMPLING: return createNonStrictSampling();
			case KernelExpressionPackage.DISCRETIZATION: return createDiscretization();
			case KernelExpressionPackage.DEATH: return createDeath();
			case KernelExpressionPackage.WAIT: return createWait();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public UpTo createUpTo()
  {
		UpToImpl upTo = new UpToImpl();
		return upTo;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Defer createDefer()
  {
		DeferImpl defer = new DeferImpl();
		return defer;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public StrictSampling createStrictSampling()
  {
		StrictSamplingImpl strictSampling = new StrictSamplingImpl();
		return strictSampling;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Concatenation createConcatenation()
  {
		ConcatenationImpl concatenation = new ConcatenationImpl();
		return concatenation;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Union createUnion()
  {
		UnionImpl union = new UnionImpl();
		return union;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Intersection createIntersection()
  {
		IntersectionImpl intersection = new IntersectionImpl();
		return intersection;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Sup createSup()
  {
		SupImpl sup = new SupImpl();
		return sup;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Inf createInf()
  {
		InfImpl inf = new InfImpl();
		return inf;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public NonStrictSampling createNonStrictSampling()
  {
		NonStrictSamplingImpl nonStrictSampling = new NonStrictSamplingImpl();
		return nonStrictSampling;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Discretization createDiscretization()
  {
		DiscretizationImpl discretization = new DiscretizationImpl();
		return discretization;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Death createDeath()
  {
		DeathImpl death = new DeathImpl();
		return death;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Wait createWait() {
		WaitImpl wait = new WaitImpl();
		return wait;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public KernelExpressionPackage getKernelExpressionPackage()
  {
		return (KernelExpressionPackage)getEPackage();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static KernelExpressionPackage getPackage()
  {
		return KernelExpressionPackage.eINSTANCE;
	}

} //KernelExpressionFactoryImpl
