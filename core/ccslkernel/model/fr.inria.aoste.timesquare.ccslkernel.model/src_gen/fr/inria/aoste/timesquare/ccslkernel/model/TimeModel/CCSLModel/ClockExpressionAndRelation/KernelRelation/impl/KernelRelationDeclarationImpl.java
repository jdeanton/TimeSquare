/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationDeclarationImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl.KernelRelationDeclarationImpl#getRightEntity <em>Right Entity</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl.KernelRelationDeclarationImpl#getLeftEntity <em>Left Entity</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class KernelRelationDeclarationImpl extends RelationDeclarationImpl implements KernelRelationDeclaration
{
  /**
	 * The cached value of the '{@link #getRightEntity() <em>Right Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getRightEntity()
	 * @generated
	 * @ordered
	 */
  protected AbstractEntity rightEntity;

  /**
	 * The cached value of the '{@link #getLeftEntity() <em>Left Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getLeftEntity()
	 * @generated
	 * @ordered
	 */
  protected AbstractEntity leftEntity;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected KernelRelationDeclarationImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return KernelRelationPackage.Literals.KERNEL_RELATION_DECLARATION;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public AbstractEntity getRightEntity()
  {
		return rightEntity;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetRightEntity(AbstractEntity newRightEntity, NotificationChain msgs)
  {
		AbstractEntity oldRightEntity = rightEntity;
		rightEntity = newRightEntity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY, oldRightEntity, newRightEntity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setRightEntity(AbstractEntity newRightEntity)
  {
		if (newRightEntity != rightEntity) {
			NotificationChain msgs = null;
			if (rightEntity != null)
				msgs = ((InternalEObject)rightEntity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY, null, msgs);
			if (newRightEntity != null)
				msgs = ((InternalEObject)newRightEntity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY, null, msgs);
			msgs = basicSetRightEntity(newRightEntity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY, newRightEntity, newRightEntity));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public AbstractEntity getLeftEntity()
  {
		return leftEntity;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetLeftEntity(AbstractEntity newLeftEntity, NotificationChain msgs)
  {
		AbstractEntity oldLeftEntity = leftEntity;
		leftEntity = newLeftEntity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY, oldLeftEntity, newLeftEntity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setLeftEntity(AbstractEntity newLeftEntity)
  {
		if (newLeftEntity != leftEntity) {
			NotificationChain msgs = null;
			if (leftEntity != null)
				msgs = ((InternalEObject)leftEntity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY, null, msgs);
			if (newLeftEntity != null)
				msgs = ((InternalEObject)newLeftEntity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY, null, msgs);
			msgs = basicSetLeftEntity(newLeftEntity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY, newLeftEntity, newLeftEntity));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY:
				return basicSetRightEntity(null, msgs);
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY:
				return basicSetLeftEntity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY:
				return getRightEntity();
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY:
				return getLeftEntity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY:
				setRightEntity((AbstractEntity)newValue);
				return;
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY:
				setLeftEntity((AbstractEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY:
				setRightEntity((AbstractEntity)null);
				return;
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY:
				setLeftEntity((AbstractEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__RIGHT_ENTITY:
				return rightEntity != null;
			case KernelRelationPackage.KERNEL_RELATION_DECLARATION__LEFT_ENTITY:
				return leftEntity != null;
		}
		return super.eIsSet(featureID);
	}

} //KernelRelationDeclarationImpl
