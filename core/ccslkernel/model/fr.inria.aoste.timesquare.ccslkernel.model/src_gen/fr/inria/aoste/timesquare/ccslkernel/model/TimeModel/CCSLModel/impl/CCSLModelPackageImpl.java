/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl.KernelRelationPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.PrimitivesTypeValuePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CCSLModelPackageImpl extends EPackageImpl implements CCSLModelPackage
{
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass clockConstraintSystemEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass blockEClass = null;

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blockTransitionEClass = null;

		/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private CCSLModelPackageImpl()
  {
		super(eNS_URI, CCSLModelFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link CCSLModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static CCSLModelPackage init()
  {
		if (isInited) return (CCSLModelPackage)EPackage.Registry.INSTANCE.getEPackage(CCSLModelPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredCCSLModelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		CCSLModelPackageImpl theCCSLModelPackage = registeredCCSLModelPackage instanceof CCSLModelPackageImpl ? (CCSLModelPackageImpl)registeredCCSLModelPackage : new CCSLModelPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		TimeModelPackageImpl theTimeModelPackage = (TimeModelPackageImpl)(registeredPackage instanceof TimeModelPackageImpl ? registeredPackage : TimeModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassicalExpressionPackage.eNS_URI);
		ClassicalExpressionPackageImpl theClassicalExpressionPackage = (ClassicalExpressionPackageImpl)(registeredPackage instanceof ClassicalExpressionPackageImpl ? registeredPackage : ClassicalExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);
		ClockExpressionAndRelationPackageImpl theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackageImpl)(registeredPackage instanceof ClockExpressionAndRelationPackageImpl ? registeredPackage : ClockExpressionAndRelationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelExpressionPackage.eNS_URI);
		KernelExpressionPackageImpl theKernelExpressionPackage = (KernelExpressionPackageImpl)(registeredPackage instanceof KernelExpressionPackageImpl ? registeredPackage : KernelExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelRelationPackage.eNS_URI);
		KernelRelationPackageImpl theKernelRelationPackage = (KernelRelationPackageImpl)(registeredPackage instanceof KernelRelationPackageImpl ? registeredPackage : KernelRelationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BasicTypePackage.eNS_URI);
		BasicTypePackageImpl theBasicTypePackage = (BasicTypePackageImpl)(registeredPackage instanceof BasicTypePackageImpl ? registeredPackage : BasicTypePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PrimitivesTypeValuePackage.eNS_URI);
		PrimitivesTypeValuePackageImpl thePrimitivesTypeValuePackage = (PrimitivesTypeValuePackageImpl)(registeredPackage instanceof PrimitivesTypeValuePackageImpl ? registeredPackage : PrimitivesTypeValuePackage.eINSTANCE);

		// Create package meta-data objects
		theCCSLModelPackage.createPackageContents();
		theTimeModelPackage.createPackageContents();
		theClassicalExpressionPackage.createPackageContents();
		theClockExpressionAndRelationPackage.createPackageContents();
		theKernelExpressionPackage.createPackageContents();
		theKernelRelationPackage.createPackageContents();
		theBasicTypePackage.createPackageContents();
		thePrimitivesTypeValuePackage.createPackageContents();

		// Initialize created meta-data
		theCCSLModelPackage.initializePackageContents();
		theTimeModelPackage.initializePackageContents();
		theClassicalExpressionPackage.initializePackageContents();
		theClockExpressionAndRelationPackage.initializePackageContents();
		theKernelExpressionPackage.initializePackageContents();
		theKernelRelationPackage.initializePackageContents();
		theBasicTypePackage.initializePackageContents();
		thePrimitivesTypeValuePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCCSLModelPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CCSLModelPackage.eNS_URI, theCCSLModelPackage);
		return theCCSLModelPackage;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getClockConstraintSystem()
  {
		return clockConstraintSystemEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getClockConstraintSystem_SuperBlock()
  {
		return (EReference)clockConstraintSystemEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getClockConstraintSystem_DataTypes()
  {
		return (EReference)clockConstraintSystemEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getClockConstraintSystem_Imports()
  {
		return (EReference)clockConstraintSystemEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBlock()
  {
		return blockEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBlock_SubBlock()
  {
		return (EReference)blockEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBlock_Container()
  {
		return (EReference)blockEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBlock_Elements()
  {
		return (EReference)blockEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBlock_Relations()
  {
		return (EReference)blockEClass.getEStructuralFeatures().get(3);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBlock_Expressions()
  {
		return (EReference)blockEClass.getEStructuralFeatures().get(4);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBlock_ClassicalExpression()
  {
		return (EReference)blockEClass.getEStructuralFeatures().get(5);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlock_InitialBlock() {
		return (EReference)blockEClass.getEStructuralFeatures().get(6);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlock_BlockTransitions() {
		return (EReference)blockEClass.getEStructuralFeatures().get(7);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBlockTransition() {
		return blockTransitionEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlockTransition_Source() {
		return (EReference)blockTransitionEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlockTransition_Target() {
		return (EReference)blockTransitionEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlockTransition_On() {
		return (EReference)blockTransitionEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public CCSLModelFactory getCCSLModelFactory()
  {
		return (CCSLModelFactory)getEFactoryInstance();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		clockConstraintSystemEClass = createEClass(CLOCK_CONSTRAINT_SYSTEM);
		createEReference(clockConstraintSystemEClass, CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK);
		createEReference(clockConstraintSystemEClass, CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES);
		createEReference(clockConstraintSystemEClass, CLOCK_CONSTRAINT_SYSTEM__IMPORTS);

		blockEClass = createEClass(BLOCK);
		createEReference(blockEClass, BLOCK__SUB_BLOCK);
		createEReference(blockEClass, BLOCK__CONTAINER);
		createEReference(blockEClass, BLOCK__ELEMENTS);
		createEReference(blockEClass, BLOCK__RELATIONS);
		createEReference(blockEClass, BLOCK__EXPRESSIONS);
		createEReference(blockEClass, BLOCK__CLASSICAL_EXPRESSION);
		createEReference(blockEClass, BLOCK__INITIAL_BLOCK);
		createEReference(blockEClass, BLOCK__BLOCK_TRANSITIONS);

		blockTransitionEClass = createEClass(BLOCK_TRANSITION);
		createEReference(blockTransitionEClass, BLOCK_TRANSITION__SOURCE);
		createEReference(blockTransitionEClass, BLOCK_TRANSITION__TARGET);
		createEReference(blockTransitionEClass, BLOCK_TRANSITION__ON);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ClassicalExpressionPackage theClassicalExpressionPackage = (ClassicalExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(ClassicalExpressionPackage.eNS_URI);
		ClockExpressionAndRelationPackage theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackage)EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);
		BasicTypePackage theBasicTypePackage = (BasicTypePackage)EPackage.Registry.INSTANCE.getEPackage(BasicTypePackage.eNS_URI);
		TimeModelPackage theTimeModelPackage = (TimeModelPackage)EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theClassicalExpressionPackage);
		getESubpackages().add(theClockExpressionAndRelationPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		clockConstraintSystemEClass.getESuperTypes().add(this.getBlock());
		blockEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		blockTransitionEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());

		// Initialize classes and features; add operations and parameters
		initEClass(clockConstraintSystemEClass, ClockConstraintSystem.class, "ClockConstraintSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClockConstraintSystem_SuperBlock(), this.getBlock(), null, "superBlock", null, 1, 1, ClockConstraintSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClockConstraintSystem_DataTypes(), theBasicTypePackage.getType(), null, "dataTypes", null, 0, -1, ClockConstraintSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClockConstraintSystem_Imports(), theTimeModelPackage.getImportStatement(), null, "imports", null, 0, -1, ClockConstraintSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blockEClass, Block.class, "Block", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlock_SubBlock(), this.getBlock(), this.getBlock_Container(), "subBlock", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_Container(), this.getBlock(), this.getBlock_SubBlock(), "container", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_Elements(), theBasicTypePackage.getElement(), null, "elements", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_Relations(), theClockExpressionAndRelationPackage.getRelation(), null, "relations", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_Expressions(), theClockExpressionAndRelationPackage.getExpression(), null, "expressions", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_ClassicalExpression(), theClassicalExpressionPackage.getClassicalExpression(), null, "classicalExpression", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_InitialBlock(), this.getBlock(), null, "initialBlock", null, 0, 1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlock_BlockTransitions(), this.getBlockTransition(), null, "blockTransitions", null, 0, -1, Block.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blockTransitionEClass, BlockTransition.class, "BlockTransition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlockTransition_Source(), this.getBlock(), null, "source", null, 1, 1, BlockTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlockTransition_Target(), this.getBlock(), null, "target", null, 1, 1, BlockTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlockTransition_On(), theTimeModelPackage.getClock(), null, "on", null, 1, 1, BlockTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //CCSLModelPackageImpl
