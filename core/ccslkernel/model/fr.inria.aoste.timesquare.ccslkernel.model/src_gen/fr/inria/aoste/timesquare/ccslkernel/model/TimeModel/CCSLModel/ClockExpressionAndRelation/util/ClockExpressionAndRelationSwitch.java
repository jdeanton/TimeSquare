/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.util;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.*;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage
 * @generated
 */
public class ClockExpressionAndRelationSwitch<T>
{
  /**
	 * The cached model package
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected static ClockExpressionAndRelationPackage modelPackage;

  /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public ClockExpressionAndRelationSwitch()
  {
		if (modelPackage == null) {
			modelPackage = ClockExpressionAndRelationPackage.eINSTANCE;
		}
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  public T doSwitch(EObject theEObject)
  {
		return doSwitch(theEObject.eClass(), theEObject);
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  protected T doSwitch(EClass theEClass, EObject theEObject)
  {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  protected T doSwitch(int classifierID, EObject theEObject)
  {
		switch (classifierID) {
			case ClockExpressionAndRelationPackage.EXPRESSION: {
				Expression expression = (Expression)theEObject;
				T result = caseExpression(expression);
				if (result == null) result = caseConcreteEntity(expression);
				if (result == null) result = caseBindableEntity(expression);
				if (result == null) result = caseNamedElement(expression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION: {
				UserExpressionDefinition userExpressionDefinition = (UserExpressionDefinition)theEObject;
				T result = caseUserExpressionDefinition(userExpressionDefinition);
				if (result == null) result = caseExpressionDefinition(userExpressionDefinition);
				if (result == null) result = caseNamedElement(userExpressionDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY: {
				ExpressionLibrary expressionLibrary = (ExpressionLibrary)theEObject;
				T result = caseExpressionLibrary(expressionLibrary);
				if (result == null) result = caseNamedElement(expressionLibrary);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.BINDING: {
				Binding binding = (Binding)theEObject;
				T result = caseBinding(binding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION: {
				ConditionalExpressionDefinition conditionalExpressionDefinition = (ConditionalExpressionDefinition)theEObject;
				T result = caseConditionalExpressionDefinition(conditionalExpressionDefinition);
				if (result == null) result = caseUserExpressionDefinition(conditionalExpressionDefinition);
				if (result == null) result = caseExpressionDefinition(conditionalExpressionDefinition);
				if (result == null) result = caseNamedElement(conditionalExpressionDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.EXPR_CASE: {
				ExprCase exprCase = (ExprCase)theEObject;
				T result = caseExprCase(exprCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.RELATION_DEFINITION: {
				RelationDefinition relationDefinition = (RelationDefinition)theEObject;
				T result = caseRelationDefinition(relationDefinition);
				if (result == null) result = caseNamedElement(relationDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.USER_RELATION_DEFINITION: {
				UserRelationDefinition userRelationDefinition = (UserRelationDefinition)theEObject;
				T result = caseUserRelationDefinition(userRelationDefinition);
				if (result == null) result = caseRelationDefinition(userRelationDefinition);
				if (result == null) result = caseNamedElement(userRelationDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.RELATION: {
				Relation relation = (Relation)theEObject;
				T result = caseRelation(relation);
				if (result == null) result = caseConcreteEntity(relation);
				if (result == null) result = caseBindableEntity(relation);
				if (result == null) result = caseNamedElement(relation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION: {
				ConditionalRelationDefinition conditionalRelationDefinition = (ConditionalRelationDefinition)theEObject;
				T result = caseConditionalRelationDefinition(conditionalRelationDefinition);
				if (result == null) result = caseUserRelationDefinition(conditionalRelationDefinition);
				if (result == null) result = caseRelationDefinition(conditionalRelationDefinition);
				if (result == null) result = caseNamedElement(conditionalRelationDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY: {
				RelationLibrary relationLibrary = (RelationLibrary)theEObject;
				T result = caseRelationLibrary(relationLibrary);
				if (result == null) result = caseNamedElement(relationLibrary);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.REL_CASE: {
				RelCase relCase = (RelCase)theEObject;
				T result = caseRelCase(relCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.LIBRARY: {
				Library library = (Library)theEObject;
				T result = caseLibrary(library);
				if (result == null) result = caseNamedElement(library);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY: {
				AbstractEntity abstractEntity = (AbstractEntity)theEObject;
				T result = caseAbstractEntity(abstractEntity);
				if (result == null) result = caseBindableEntity(abstractEntity);
				if (result == null) result = caseNamedElement(abstractEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.EXPRESSION_DEFINITION: {
				ExpressionDefinition expressionDefinition = (ExpressionDefinition)theEObject;
				T result = caseExpressionDefinition(expressionDefinition);
				if (result == null) result = caseNamedElement(expressionDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.BINDABLE_ENTITY: {
				BindableEntity bindableEntity = (BindableEntity)theEObject;
				T result = caseBindableEntity(bindableEntity);
				if (result == null) result = caseNamedElement(bindableEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.CONCRETE_ENTITY: {
				ConcreteEntity concreteEntity = (ConcreteEntity)theEObject;
				T result = caseConcreteEntity(concreteEntity);
				if (result == null) result = caseBindableEntity(concreteEntity);
				if (result == null) result = caseNamedElement(concreteEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.RELATION_DECLARATION: {
				RelationDeclaration relationDeclaration = (RelationDeclaration)theEObject;
				T result = caseRelationDeclaration(relationDeclaration);
				if (result == null) result = caseNamedElement(relationDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.EXPRESSION_DECLARATION: {
				ExpressionDeclaration expressionDeclaration = (ExpressionDeclaration)theEObject;
				T result = caseExpressionDeclaration(expressionDeclaration);
				if (result == null) result = caseNamedElement(expressionDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.EXTERNAL_RELATION_DEFINITION: {
				ExternalRelationDefinition externalRelationDefinition = (ExternalRelationDefinition)theEObject;
				T result = caseExternalRelationDefinition(externalRelationDefinition);
				if (result == null) result = caseRelationDefinition(externalRelationDefinition);
				if (result == null) result = caseNamedElement(externalRelationDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClockExpressionAndRelationPackage.EXTERNAL_EXPRESSION_DEFINITION: {
				ExternalExpressionDefinition externalExpressionDefinition = (ExternalExpressionDefinition)theEObject;
				T result = caseExternalExpressionDefinition(externalExpressionDefinition);
				if (result == null) result = caseExpressionDefinition(externalExpressionDefinition);
				if (result == null) result = caseNamedElement(externalExpressionDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseExpression(Expression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>User Expression Definition</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Expression Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUserExpressionDefinition(UserExpressionDefinition object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Library</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Library</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseExpressionLibrary(ExpressionLibrary object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Binding</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBinding(Binding object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Conditional Expression Definition</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditional Expression Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseConditionalExpressionDefinition(ConditionalExpressionDefinition object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Expr Case</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expr Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseExprCase(ExprCase object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Relation Definition</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRelationDefinition(RelationDefinition object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>User Relation Definition</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Relation Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUserRelationDefinition(UserRelationDefinition object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRelation(Relation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Conditional Relation Definition</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditional Relation Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseConditionalRelationDefinition(ConditionalRelationDefinition object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Relation Library</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation Library</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRelationLibrary(RelationLibrary object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Rel Case</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rel Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRelCase(RelCase object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Library</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Library</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseLibrary(Library object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Entity</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseAbstractEntity(AbstractEntity object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Definition</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseExpressionDefinition(ExpressionDefinition object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Bindable Entity</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bindable Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBindableEntity(BindableEntity object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Concrete Entity</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concrete Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseConcreteEntity(ConcreteEntity object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Relation Declaration</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRelationDeclaration(RelationDeclaration object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Declaration</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseExpressionDeclaration(ExpressionDeclaration object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>External Relation Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Relation Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalRelationDefinition(ExternalRelationDefinition object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>External Expression Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Expression Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalExpressionDefinition(ExternalExpressionDefinition object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNamedElement(NamedElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
  public T defaultCase(EObject object)
  {
		return null;
	}

} //ClockExpressionAndRelationSwitch
