/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Clock Constraint System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.ClockConstraintSystemImpl#getSuperBlock <em>Super Block</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.ClockConstraintSystemImpl#getDataTypes <em>Data Types</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.ClockConstraintSystemImpl#getImports <em>Imports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClockConstraintSystemImpl extends BlockImpl implements ClockConstraintSystem
{
  /**
	 * The cached value of the '{@link #getSuperBlock() <em>Super Block</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getSuperBlock()
	 * @generated
	 * @ordered
	 */
  protected Block superBlock;

  /**
	 * The cached value of the '{@link #getDataTypes() <em>Data Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getDataTypes()
	 * @generated
	 * @ordered
	 */
  protected EList<Type> dataTypes;

  /**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
  protected EList<ImportStatement> imports;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected ClockConstraintSystemImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Block getSuperBlock()
  {
		if (superBlock != null && superBlock.eIsProxy()) {
			InternalEObject oldSuperBlock = (InternalEObject)superBlock;
			superBlock = (Block)eResolveProxy(oldSuperBlock);
			if (superBlock != oldSuperBlock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK, oldSuperBlock, superBlock));
			}
		}
		return superBlock;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public Block basicGetSuperBlock()
  {
		return superBlock;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setSuperBlock(Block newSuperBlock)
  {
		Block oldSuperBlock = superBlock;
		superBlock = newSuperBlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK, oldSuperBlock, superBlock));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Type> getDataTypes()
  {
		if (dataTypes == null) {
			dataTypes = new EObjectContainmentEList<Type>(Type.class, this, CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES);
		}
		return dataTypes;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ImportStatement> getImports()
  {
		if (imports == null) {
			imports = new EObjectContainmentEList<ImportStatement>(ImportStatement.class, this, CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__IMPORTS);
		}
		return imports;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES:
				return ((InternalEList<?>)getDataTypes()).basicRemove(otherEnd, msgs);
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__IMPORTS:
				return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK:
				if (resolve) return getSuperBlock();
				return basicGetSuperBlock();
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES:
				return getDataTypes();
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__IMPORTS:
				return getImports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK:
				setSuperBlock((Block)newValue);
				return;
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES:
				getDataTypes().clear();
				getDataTypes().addAll((Collection<? extends Type>)newValue);
				return;
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends ImportStatement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK:
				setSuperBlock((Block)null);
				return;
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES:
				getDataTypes().clear();
				return;
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__IMPORTS:
				getImports().clear();
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK:
				return superBlock != null;
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES:
				return dataTypes != null && !dataTypes.isEmpty();
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__IMPORTS:
				return imports != null && !imports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ClockConstraintSystemImpl
