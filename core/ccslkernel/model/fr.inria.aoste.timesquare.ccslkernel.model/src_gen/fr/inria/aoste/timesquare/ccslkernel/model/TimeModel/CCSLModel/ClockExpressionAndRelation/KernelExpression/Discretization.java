/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discretization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization#getDenseClock <em>Dense Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization#getDiscretizationFactor <em>Discretization Factor</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage#getDiscretization()
 * @model
 * @generated
 */
public interface Discretization extends KernelExpressionDeclaration
{
  /**
	 * Returns the value of the '<em><b>Dense Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dense Clock</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Dense Clock</em>' containment reference.
	 * @see #setDenseClock(AbstractEntity)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage#getDiscretization_DenseClock()
	 * @model containment="true" required="true"
	 * @generated
	 */
  AbstractEntity getDenseClock();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization#getDenseClock <em>Dense Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dense Clock</em>' containment reference.
	 * @see #getDenseClock()
	 * @generated
	 */
  void setDenseClock(AbstractEntity value);

  /**
	 * Returns the value of the '<em><b>Discretization Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Discretization Factor</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Discretization Factor</em>' containment reference.
	 * @see #setDiscretizationFactor(AbstractEntity)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage#getDiscretization_DiscretizationFactor()
	 * @model containment="true" required="true"
	 * @generated
	 */
  AbstractEntity getDiscretizationFactor();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization#getDiscretizationFactor <em>Discretization Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discretization Factor</em>' containment reference.
	 * @see #getDiscretizationFactor()
	 * @generated
	 */
  void setDiscretizationFactor(AbstractEntity value);

} // Discretization
