/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClockExpressionAndRelationFactoryImpl extends EFactoryImpl implements ClockExpressionAndRelationFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public static ClockExpressionAndRelationFactory init()
  {
		try {
			ClockExpressionAndRelationFactory theClockExpressionAndRelationFactory = (ClockExpressionAndRelationFactory)EPackage.Registry.INSTANCE.getEFactory(ClockExpressionAndRelationPackage.eNS_URI);
			if (theClockExpressionAndRelationFactory != null) {
				return theClockExpressionAndRelationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ClockExpressionAndRelationFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public ClockExpressionAndRelationFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case ClockExpressionAndRelationPackage.EXPRESSION: return createExpression();
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION: return createUserExpressionDefinition();
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY: return createExpressionLibrary();
			case ClockExpressionAndRelationPackage.BINDING: return createBinding();
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION: return createConditionalExpressionDefinition();
			case ClockExpressionAndRelationPackage.EXPR_CASE: return createExprCase();
			case ClockExpressionAndRelationPackage.USER_RELATION_DEFINITION: return createUserRelationDefinition();
			case ClockExpressionAndRelationPackage.RELATION: return createRelation();
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION: return createConditionalRelationDefinition();
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY: return createRelationLibrary();
			case ClockExpressionAndRelationPackage.REL_CASE: return createRelCase();
			case ClockExpressionAndRelationPackage.LIBRARY: return createLibrary();
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY: return createAbstractEntity();
			case ClockExpressionAndRelationPackage.CONCRETE_ENTITY: return createConcreteEntity();
			case ClockExpressionAndRelationPackage.RELATION_DECLARATION: return createRelationDeclaration();
			case ClockExpressionAndRelationPackage.EXPRESSION_DECLARATION: return createExpressionDeclaration();
			case ClockExpressionAndRelationPackage.EXTERNAL_RELATION_DEFINITION: return createExternalRelationDefinition();
			case ClockExpressionAndRelationPackage.EXTERNAL_EXPRESSION_DEFINITION: return createExternalExpressionDefinition();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Expression createExpression()
  {
		ExpressionImpl expression = new ExpressionImpl();
		return expression;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public UserExpressionDefinition createUserExpressionDefinition()
  {
		UserExpressionDefinitionImpl userExpressionDefinition = new UserExpressionDefinitionImpl();
		return userExpressionDefinition;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ExpressionLibrary createExpressionLibrary()
  {
		ExpressionLibraryImpl expressionLibrary = new ExpressionLibraryImpl();
		return expressionLibrary;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Binding createBinding()
  {
		BindingImpl binding = new BindingImpl();
		return binding;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ConditionalExpressionDefinition createConditionalExpressionDefinition()
  {
		ConditionalExpressionDefinitionImpl conditionalExpressionDefinition = new ConditionalExpressionDefinitionImpl();
		return conditionalExpressionDefinition;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ExprCase createExprCase()
  {
		ExprCaseImpl exprCase = new ExprCaseImpl();
		return exprCase;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public UserRelationDefinition createUserRelationDefinition()
  {
		UserRelationDefinitionImpl userRelationDefinition = new UserRelationDefinitionImpl();
		return userRelationDefinition;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Relation createRelation()
  {
		RelationImpl relation = new RelationImpl();
		return relation;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ConditionalRelationDefinition createConditionalRelationDefinition()
  {
		ConditionalRelationDefinitionImpl conditionalRelationDefinition = new ConditionalRelationDefinitionImpl();
		return conditionalRelationDefinition;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RelationLibrary createRelationLibrary()
  {
		RelationLibraryImpl relationLibrary = new RelationLibraryImpl();
		return relationLibrary;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RelCase createRelCase()
  {
		RelCaseImpl relCase = new RelCaseImpl();
		return relCase;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Library createLibrary()
  {
		LibraryImpl library = new LibraryImpl();
		return library;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public AbstractEntity createAbstractEntity()
  {
		AbstractEntityImpl abstractEntity = new AbstractEntityImpl();
		return abstractEntity;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ConcreteEntity createConcreteEntity()
  {
		ConcreteEntityImpl concreteEntity = new ConcreteEntityImpl();
		return concreteEntity;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RelationDeclaration createRelationDeclaration()
  {
		RelationDeclarationImpl relationDeclaration = new RelationDeclarationImpl();
		return relationDeclaration;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ExpressionDeclaration createExpressionDeclaration()
  {
		ExpressionDeclarationImpl expressionDeclaration = new ExpressionDeclarationImpl();
		return expressionDeclaration;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ExternalRelationDefinition createExternalRelationDefinition() {
		ExternalRelationDefinitionImpl externalRelationDefinition = new ExternalRelationDefinitionImpl();
		return externalRelationDefinition;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ExternalExpressionDefinition createExternalExpressionDefinition() {
		ExternalExpressionDefinitionImpl externalExpressionDefinition = new ExternalExpressionDefinitionImpl();
		return externalExpressionDefinition;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ClockExpressionAndRelationPackage getClockExpressionAndRelationPackage()
  {
		return (ClockExpressionAndRelationPackage)getEPackage();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static ClockExpressionAndRelationPackage getPackage()
  {
		return ClockExpressionAndRelationPackage.eINSTANCE;
	}

} //ClockExpressionAndRelationFactoryImpl
