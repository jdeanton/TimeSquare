/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl#getSubBlock <em>Sub Block</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl#getContainer <em>Container</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl#getRelations <em>Relations</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl#getExpressions <em>Expressions</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl#getClassicalExpression <em>Classical Expression</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl#getInitialBlock <em>Initial Block</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl#getBlockTransitions <em>Block Transitions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BlockImpl extends NamedElementImpl implements Block
{
  /**
	 * The cached value of the '{@link #getSubBlock() <em>Sub Block</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getSubBlock()
	 * @generated
	 * @ordered
	 */
  protected EList<Block> subBlock;

  /**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
  protected EList<Element> elements;

  /**
	 * The cached value of the '{@link #getRelations() <em>Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getRelations()
	 * @generated
	 * @ordered
	 */
  protected EList<Relation> relations;

  /**
	 * The cached value of the '{@link #getExpressions() <em>Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getExpressions()
	 * @generated
	 * @ordered
	 */
  protected EList<Expression> expressions;

  /**
	 * The cached value of the '{@link #getClassicalExpression() <em>Classical Expression</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getClassicalExpression()
	 * @generated
	 * @ordered
	 */
  protected EList<ClassicalExpression> classicalExpression;

  /**
	 * The cached value of the '{@link #getInitialBlock() <em>Initial Block</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialBlock()
	 * @generated
	 * @ordered
	 */
	protected Block initialBlock;

		/**
	 * The cached value of the '{@link #getBlockTransitions() <em>Block Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<BlockTransition> blockTransitions;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected BlockImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return CCSLModelPackage.Literals.BLOCK;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Block> getSubBlock()
  {
		if (subBlock == null) {
			subBlock = new EObjectContainmentWithInverseEList<Block>(Block.class, this, CCSLModelPackage.BLOCK__SUB_BLOCK, CCSLModelPackage.BLOCK__CONTAINER);
		}
		return subBlock;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Block getContainer()
  {
		if (eContainerFeatureID() != CCSLModelPackage.BLOCK__CONTAINER) return null;
		return (Block)eInternalContainer();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetContainer(Block newContainer, NotificationChain msgs)
  {
		msgs = eBasicSetContainer((InternalEObject)newContainer, CCSLModelPackage.BLOCK__CONTAINER, msgs);
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setContainer(Block newContainer)
  {
		if (newContainer != eInternalContainer() || (eContainerFeatureID() != CCSLModelPackage.BLOCK__CONTAINER && newContainer != null)) {
			if (EcoreUtil.isAncestor(this, newContainer))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainer != null)
				msgs = ((InternalEObject)newContainer).eInverseAdd(this, CCSLModelPackage.BLOCK__SUB_BLOCK, Block.class, msgs);
			msgs = basicSetContainer(newContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CCSLModelPackage.BLOCK__CONTAINER, newContainer, newContainer));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Element> getElements()
  {
		if (elements == null) {
			elements = new EObjectContainmentEList<Element>(Element.class, this, CCSLModelPackage.BLOCK__ELEMENTS);
		}
		return elements;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Relation> getRelations()
  {
		if (relations == null) {
			relations = new EObjectContainmentEList<Relation>(Relation.class, this, CCSLModelPackage.BLOCK__RELATIONS);
		}
		return relations;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Expression> getExpressions()
  {
		if (expressions == null) {
			expressions = new EObjectContainmentEList<Expression>(Expression.class, this, CCSLModelPackage.BLOCK__EXPRESSIONS);
		}
		return expressions;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ClassicalExpression> getClassicalExpression()
  {
		if (classicalExpression == null) {
			classicalExpression = new EObjectContainmentEList<ClassicalExpression>(ClassicalExpression.class, this, CCSLModelPackage.BLOCK__CLASSICAL_EXPRESSION);
		}
		return classicalExpression;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Block getInitialBlock() {
		if (initialBlock != null && initialBlock.eIsProxy()) {
			InternalEObject oldInitialBlock = (InternalEObject)initialBlock;
			initialBlock = (Block)eResolveProxy(oldInitialBlock);
			if (initialBlock != oldInitialBlock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CCSLModelPackage.BLOCK__INITIAL_BLOCK, oldInitialBlock, initialBlock));
			}
		}
		return initialBlock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block basicGetInitialBlock() {
		return initialBlock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInitialBlock(Block newInitialBlock) {
		Block oldInitialBlock = initialBlock;
		initialBlock = newInitialBlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CCSLModelPackage.BLOCK__INITIAL_BLOCK, oldInitialBlock, initialBlock));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BlockTransition> getBlockTransitions() {
		if (blockTransitions == null) {
			blockTransitions = new EObjectContainmentEList<BlockTransition>(BlockTransition.class, this, CCSLModelPackage.BLOCK__BLOCK_TRANSITIONS);
		}
		return blockTransitions;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case CCSLModelPackage.BLOCK__SUB_BLOCK:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubBlock()).basicAdd(otherEnd, msgs);
			case CCSLModelPackage.BLOCK__CONTAINER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainer((Block)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case CCSLModelPackage.BLOCK__SUB_BLOCK:
				return ((InternalEList<?>)getSubBlock()).basicRemove(otherEnd, msgs);
			case CCSLModelPackage.BLOCK__CONTAINER:
				return basicSetContainer(null, msgs);
			case CCSLModelPackage.BLOCK__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
			case CCSLModelPackage.BLOCK__RELATIONS:
				return ((InternalEList<?>)getRelations()).basicRemove(otherEnd, msgs);
			case CCSLModelPackage.BLOCK__EXPRESSIONS:
				return ((InternalEList<?>)getExpressions()).basicRemove(otherEnd, msgs);
			case CCSLModelPackage.BLOCK__CLASSICAL_EXPRESSION:
				return ((InternalEList<?>)getClassicalExpression()).basicRemove(otherEnd, msgs);
			case CCSLModelPackage.BLOCK__BLOCK_TRANSITIONS:
				return ((InternalEList<?>)getBlockTransitions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
  {
		switch (eContainerFeatureID()) {
			case CCSLModelPackage.BLOCK__CONTAINER:
				return eInternalContainer().eInverseRemove(this, CCSLModelPackage.BLOCK__SUB_BLOCK, Block.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case CCSLModelPackage.BLOCK__SUB_BLOCK:
				return getSubBlock();
			case CCSLModelPackage.BLOCK__CONTAINER:
				return getContainer();
			case CCSLModelPackage.BLOCK__ELEMENTS:
				return getElements();
			case CCSLModelPackage.BLOCK__RELATIONS:
				return getRelations();
			case CCSLModelPackage.BLOCK__EXPRESSIONS:
				return getExpressions();
			case CCSLModelPackage.BLOCK__CLASSICAL_EXPRESSION:
				return getClassicalExpression();
			case CCSLModelPackage.BLOCK__INITIAL_BLOCK:
				if (resolve) return getInitialBlock();
				return basicGetInitialBlock();
			case CCSLModelPackage.BLOCK__BLOCK_TRANSITIONS:
				return getBlockTransitions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case CCSLModelPackage.BLOCK__SUB_BLOCK:
				getSubBlock().clear();
				getSubBlock().addAll((Collection<? extends Block>)newValue);
				return;
			case CCSLModelPackage.BLOCK__CONTAINER:
				setContainer((Block)newValue);
				return;
			case CCSLModelPackage.BLOCK__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends Element>)newValue);
				return;
			case CCSLModelPackage.BLOCK__RELATIONS:
				getRelations().clear();
				getRelations().addAll((Collection<? extends Relation>)newValue);
				return;
			case CCSLModelPackage.BLOCK__EXPRESSIONS:
				getExpressions().clear();
				getExpressions().addAll((Collection<? extends Expression>)newValue);
				return;
			case CCSLModelPackage.BLOCK__CLASSICAL_EXPRESSION:
				getClassicalExpression().clear();
				getClassicalExpression().addAll((Collection<? extends ClassicalExpression>)newValue);
				return;
			case CCSLModelPackage.BLOCK__INITIAL_BLOCK:
				setInitialBlock((Block)newValue);
				return;
			case CCSLModelPackage.BLOCK__BLOCK_TRANSITIONS:
				getBlockTransitions().clear();
				getBlockTransitions().addAll((Collection<? extends BlockTransition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case CCSLModelPackage.BLOCK__SUB_BLOCK:
				getSubBlock().clear();
				return;
			case CCSLModelPackage.BLOCK__CONTAINER:
				setContainer((Block)null);
				return;
			case CCSLModelPackage.BLOCK__ELEMENTS:
				getElements().clear();
				return;
			case CCSLModelPackage.BLOCK__RELATIONS:
				getRelations().clear();
				return;
			case CCSLModelPackage.BLOCK__EXPRESSIONS:
				getExpressions().clear();
				return;
			case CCSLModelPackage.BLOCK__CLASSICAL_EXPRESSION:
				getClassicalExpression().clear();
				return;
			case CCSLModelPackage.BLOCK__INITIAL_BLOCK:
				setInitialBlock((Block)null);
				return;
			case CCSLModelPackage.BLOCK__BLOCK_TRANSITIONS:
				getBlockTransitions().clear();
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case CCSLModelPackage.BLOCK__SUB_BLOCK:
				return subBlock != null && !subBlock.isEmpty();
			case CCSLModelPackage.BLOCK__CONTAINER:
				return getContainer() != null;
			case CCSLModelPackage.BLOCK__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case CCSLModelPackage.BLOCK__RELATIONS:
				return relations != null && !relations.isEmpty();
			case CCSLModelPackage.BLOCK__EXPRESSIONS:
				return expressions != null && !expressions.isEmpty();
			case CCSLModelPackage.BLOCK__CLASSICAL_EXPRESSION:
				return classicalExpression != null && !classicalExpression.isEmpty();
			case CCSLModelPackage.BLOCK__INITIAL_BLOCK:
				return initialBlock != null;
			case CCSLModelPackage.BLOCK__BLOCK_TRANSITIONS:
				return blockTransitions != null && !blockTransitions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BlockImpl
