/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionFactory
 * @model kind="package"
 * @generated
 */
public interface ClassicalExpressionPackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNAME = "ClassicalExpression";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_URI = "http://fr.inria.aoste.timemodel.classicalexpression";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_PREFIX = "fr.inria.aoste.timemodel.classicalexpression";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  ClassicalExpressionPackage eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl.init();

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionImpl <em>Classical Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getClassicalExpression()
	 * @generated
	 */
  int CLASSICAL_EXPRESSION = 0;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLASSICAL_EXPRESSION__NAME = BasicTypePackage.PRIMITIVE_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLASSICAL_EXPRESSION__TYPE = BasicTypePackage.PRIMITIVE_ELEMENT__TYPE;

  /**
	 * The number of structural features of the '<em>Classical Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLASSICAL_EXPRESSION_FEATURE_COUNT = BasicTypePackage.PRIMITIVE_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanExpressionImpl <em>Boolean Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBooleanExpression()
	 * @generated
	 */
  int BOOLEAN_EXPRESSION = 1;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_EXPRESSION__NAME = CLASSICAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_EXPRESSION__TYPE = CLASSICAL_EXPRESSION__TYPE;

  /**
	 * The number of structural features of the '<em>Boolean Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_EXPRESSION_FEATURE_COUNT = CLASSICAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealExpressionImpl <em>Real Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealExpression()
	 * @generated
	 */
  int REAL_EXPRESSION = 2;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_EXPRESSION__NAME = CLASSICAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_EXPRESSION__TYPE = CLASSICAL_EXPRESSION__TYPE;

  /**
	 * The number of structural features of the '<em>Real Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_EXPRESSION_FEATURE_COUNT = CLASSICAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerExpressionImpl <em>Integer Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntegerExpression()
	 * @generated
	 */
  int INTEGER_EXPRESSION = 3;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_EXPRESSION__NAME = CLASSICAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_EXPRESSION__TYPE = CLASSICAL_EXPRESSION__TYPE;

  /**
	 * The number of structural features of the '<em>Integer Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_EXPRESSION_FEATURE_COUNT = CLASSICAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryBooleanExpressionImpl <em>Binary Boolean Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryBooleanExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBinaryBooleanExpression()
	 * @generated
	 */
  int BINARY_BOOLEAN_EXPRESSION = 4;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_BOOLEAN_EXPRESSION__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_BOOLEAN_EXPRESSION__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Binary Boolean Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_BOOLEAN_EXPRESSION_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryBooleanExpressionImpl <em>Unary Boolean Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryBooleanExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryBooleanExpression()
	 * @generated
	 */
  int UNARY_BOOLEAN_EXPRESSION = 5;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_BOOLEAN_EXPRESSION__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_BOOLEAN_EXPRESSION__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_BOOLEAN_EXPRESSION__OPERAND = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Unary Boolean Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_BOOLEAN_EXPRESSION_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryRealExpressionImpl <em>Binary Real Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryRealExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBinaryRealExpression()
	 * @generated
	 */
  int BINARY_REAL_EXPRESSION = 6;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_REAL_EXPRESSION__NAME = REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_REAL_EXPRESSION__TYPE = REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_REAL_EXPRESSION__LEFT_VALUE = REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_REAL_EXPRESSION__RIGHT_VALUE = REAL_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Binary Real Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_REAL_EXPRESSION_FEATURE_COUNT = REAL_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealExpressionImpl <em>Unary Real Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryRealExpression()
	 * @generated
	 */
  int UNARY_REAL_EXPRESSION = 7;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_EXPRESSION__NAME = REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_EXPRESSION__TYPE = REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_EXPRESSION__VALUE = REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_EXPRESSION__OPERAND = REAL_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Unary Real Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_EXPRESSION_FEATURE_COUNT = REAL_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryIntegerExpressionImpl <em>Binary Integer Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryIntegerExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBinaryIntegerExpression()
	 * @generated
	 */
  int BINARY_INTEGER_EXPRESSION = 8;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_INTEGER_EXPRESSION__NAME = INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_INTEGER_EXPRESSION__TYPE = INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_INTEGER_EXPRESSION__LEFT_VALUE = INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_INTEGER_EXPRESSION__RIGHT_VALUE = INTEGER_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Binary Integer Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BINARY_INTEGER_EXPRESSION_FEATURE_COUNT = INTEGER_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntegerExpressionImpl <em>Unary Integer Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntegerExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryIntegerExpression()
	 * @generated
	 */
  int UNARY_INTEGER_EXPRESSION = 9;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INTEGER_EXPRESSION__NAME = INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INTEGER_EXPRESSION__TYPE = INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INTEGER_EXPRESSION__VALUE = INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INTEGER_EXPRESSION__OPERAND = INTEGER_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Unary Integer Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INTEGER_EXPRESSION_FEATURE_COUNT = INTEGER_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanRefImpl <em>Boolean Ref</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanRefImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBooleanRef()
	 * @generated
	 */
  int BOOLEAN_REF = 10;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_REF__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_REF__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Referenced Bool</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_REF__REFERENCED_BOOL = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Boolean Ref</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_REF_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealRefImpl <em>Real Ref</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealRefImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealRef()
	 * @generated
	 */
  int REAL_REF = 11;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_REF__NAME = REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_REF__TYPE = REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Real Elem</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_REF__REAL_ELEM = REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Real Ref</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_REF_FEATURE_COUNT = REAL_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerRefImpl <em>Integer Ref</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerRefImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntegerRef()
	 * @generated
	 */
  int INTEGER_REF = 12;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_REF__NAME = INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_REF__TYPE = INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Integer Elem</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_REF__INTEGER_ELEM = INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Integer Ref</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_REF_FEATURE_COUNT = INTEGER_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealPlusImpl <em>Unary Real Plus</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealPlusImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryRealPlus()
	 * @generated
	 */
  int UNARY_REAL_PLUS = 13;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_PLUS__NAME = UNARY_REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_PLUS__TYPE = UNARY_REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_PLUS__VALUE = UNARY_REAL_EXPRESSION__VALUE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_PLUS__OPERAND = UNARY_REAL_EXPRESSION__OPERAND;

  /**
	 * The number of structural features of the '<em>Unary Real Plus</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_PLUS_FEATURE_COUNT = UNARY_REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealMinusImpl <em>Unary Real Minus</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealMinusImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryRealMinus()
	 * @generated
	 */
  int UNARY_REAL_MINUS = 14;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_MINUS__NAME = UNARY_REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_MINUS__TYPE = UNARY_REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_MINUS__VALUE = UNARY_REAL_EXPRESSION__VALUE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_MINUS__OPERAND = UNARY_REAL_EXPRESSION__OPERAND;

  /**
	 * The number of structural features of the '<em>Unary Real Minus</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_REAL_MINUS_FEATURE_COUNT = UNARY_REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealPlusImpl <em>Real Plus</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealPlusImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealPlus()
	 * @generated
	 */
  int REAL_PLUS = 15;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_PLUS__NAME = BINARY_REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_PLUS__TYPE = BINARY_REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_PLUS__LEFT_VALUE = BINARY_REAL_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_PLUS__RIGHT_VALUE = BINARY_REAL_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Real Plus</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_PLUS_FEATURE_COUNT = BINARY_REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealMinusImpl <em>Real Minus</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealMinusImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealMinus()
	 * @generated
	 */
  int REAL_MINUS = 16;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MINUS__NAME = BINARY_REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MINUS__TYPE = BINARY_REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MINUS__LEFT_VALUE = BINARY_REAL_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MINUS__RIGHT_VALUE = BINARY_REAL_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Real Minus</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MINUS_FEATURE_COUNT = BINARY_REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealMultiplyImpl <em>Real Multiply</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealMultiplyImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealMultiply()
	 * @generated
	 */
  int REAL_MULTIPLY = 17;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MULTIPLY__NAME = BINARY_REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MULTIPLY__TYPE = BINARY_REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MULTIPLY__LEFT_VALUE = BINARY_REAL_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MULTIPLY__RIGHT_VALUE = BINARY_REAL_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Real Multiply</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_MULTIPLY_FEATURE_COUNT = BINARY_REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntPlusImpl <em>Unary Int Plus</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntPlusImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryIntPlus()
	 * @generated
	 */
  int UNARY_INT_PLUS = 18;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_PLUS__NAME = UNARY_INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_PLUS__TYPE = UNARY_INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_PLUS__VALUE = UNARY_INTEGER_EXPRESSION__VALUE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_PLUS__OPERAND = UNARY_INTEGER_EXPRESSION__OPERAND;

  /**
	 * The number of structural features of the '<em>Unary Int Plus</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_PLUS_FEATURE_COUNT = UNARY_INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntMinusImpl <em>Unary Int Minus</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntMinusImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryIntMinus()
	 * @generated
	 */
  int UNARY_INT_MINUS = 19;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_MINUS__NAME = UNARY_INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_MINUS__TYPE = UNARY_INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_MINUS__VALUE = UNARY_INTEGER_EXPRESSION__VALUE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_MINUS__OPERAND = UNARY_INTEGER_EXPRESSION__OPERAND;

  /**
	 * The number of structural features of the '<em>Unary Int Minus</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_INT_MINUS_FEATURE_COUNT = UNARY_INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntPlusImpl <em>Int Plus</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntPlusImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntPlus()
	 * @generated
	 */
  int INT_PLUS = 20;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_PLUS__NAME = BINARY_INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_PLUS__TYPE = BINARY_INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_PLUS__LEFT_VALUE = BINARY_INTEGER_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_PLUS__RIGHT_VALUE = BINARY_INTEGER_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Int Plus</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_PLUS_FEATURE_COUNT = BINARY_INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntMinusImpl <em>Int Minus</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntMinusImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntMinus()
	 * @generated
	 */
  int INT_MINUS = 21;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MINUS__NAME = BINARY_INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MINUS__TYPE = BINARY_INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MINUS__LEFT_VALUE = BINARY_INTEGER_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MINUS__RIGHT_VALUE = BINARY_INTEGER_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Int Minus</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MINUS_FEATURE_COUNT = BINARY_INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntMultiplyImpl <em>Int Multiply</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntMultiplyImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntMultiply()
	 * @generated
	 */
  int INT_MULTIPLY = 22;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MULTIPLY__NAME = BINARY_INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MULTIPLY__TYPE = BINARY_INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MULTIPLY__LEFT_VALUE = BINARY_INTEGER_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MULTIPLY__RIGHT_VALUE = BINARY_INTEGER_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Int Multiply</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_MULTIPLY_FEATURE_COUNT = BINARY_INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntDivideImpl <em>Int Divide</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntDivideImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntDivide()
	 * @generated
	 */
  int INT_DIVIDE = 23;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_DIVIDE__NAME = BINARY_INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_DIVIDE__TYPE = BINARY_INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_DIVIDE__LEFT_VALUE = BINARY_INTEGER_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_DIVIDE__RIGHT_VALUE = BINARY_INTEGER_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Int Divide</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_DIVIDE_FEATURE_COUNT = BINARY_INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NotImpl <em>Not</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NotImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getNot()
	 * @generated
	 */
  int NOT = 24;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NOT__NAME = UNARY_BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NOT__TYPE = UNARY_BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NOT__OPERAND = UNARY_BOOLEAN_EXPRESSION__OPERAND;

  /**
	 * The number of structural features of the '<em>Not</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NOT_FEATURE_COUNT = UNARY_BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.AndImpl <em>And</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.AndImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getAnd()
	 * @generated
	 */
  int AND = 25;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int AND__NAME = BINARY_BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int AND__TYPE = BINARY_BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int AND__LEFT_VALUE = BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int AND__RIGHT_VALUE = BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>And</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int AND_FEATURE_COUNT = BINARY_BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.OrImpl <em>Or</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.OrImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getOr()
	 * @generated
	 */
  int OR = 26;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int OR__NAME = BINARY_BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int OR__TYPE = BINARY_BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int OR__LEFT_VALUE = BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int OR__RIGHT_VALUE = BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Or</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int OR_FEATURE_COUNT = BINARY_BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.XorImpl <em>Xor</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.XorImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getXor()
	 * @generated
	 */
  int XOR = 27;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int XOR__NAME = BINARY_BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int XOR__TYPE = BINARY_BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int XOR__LEFT_VALUE = BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int XOR__RIGHT_VALUE = BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE;

  /**
	 * The number of structural features of the '<em>Xor</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int XOR_FEATURE_COUNT = BINARY_BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealEqualImpl <em>Real Equal</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealEqualImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealEqual()
	 * @generated
	 */
  int REAL_EQUAL = 28;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_EQUAL__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_EQUAL__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_EQUAL__LEFT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_EQUAL__RIGHT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Real Equal</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_EQUAL_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealInfImpl <em>Real Inf</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealInfImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealInf()
	 * @generated
	 */
  int REAL_INF = 29;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_INF__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_INF__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_INF__LEFT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_INF__RIGHT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Real Inf</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_INF_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealSupImpl <em>Real Sup</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealSupImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealSup()
	 * @generated
	 */
  int REAL_SUP = 30;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_SUP__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_SUP__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_SUP__LEFT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_SUP__RIGHT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Real Sup</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_SUP_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntEqualImpl <em>Int Equal</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntEqualImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntEqual()
	 * @generated
	 */
  int INT_EQUAL = 31;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_EQUAL__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_EQUAL__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_EQUAL__LEFT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_EQUAL__RIGHT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Int Equal</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_EQUAL_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntInfImpl <em>Int Inf</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntInfImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntInf()
	 * @generated
	 */
  int INT_INF = 32;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_INF__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_INF__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_INF__LEFT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_INF__RIGHT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Int Inf</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_INF_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntSupImpl <em>Int Sup</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntSupImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntSup()
	 * @generated
	 */
  int INT_SUP = 33;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_SUP__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_SUP__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Left Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_SUP__LEFT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Right Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_SUP__RIGHT_VALUE = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Int Sup</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INT_SUP_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqIsEmptyImpl <em>Seq Is Empty</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqIsEmptyImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqIsEmpty()
	 * @generated
	 */
  int SEQ_IS_EMPTY = 34;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_IS_EMPTY__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_IS_EMPTY__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_IS_EMPTY__OPERAND = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Seq Is Empty</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_IS_EMPTY_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqExpressionImpl <em>Seq Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqExpression()
	 * @generated
	 */
  int SEQ_EXPRESSION = 45;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_EXPRESSION__NAME = CLASSICAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_EXPRESSION__TYPE = CLASSICAL_EXPRESSION__TYPE;

  /**
	 * The number of structural features of the '<em>Seq Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_EXPRESSION_FEATURE_COUNT = CLASSICAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnarySeqExpressionImpl <em>Unary Seq Expression</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnarySeqExpressionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnarySeqExpression()
	 * @generated
	 */
  int UNARY_SEQ_EXPRESSION = 35;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_SEQ_EXPRESSION__NAME = SEQ_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_SEQ_EXPRESSION__TYPE = SEQ_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_SEQ_EXPRESSION__OPERAND = SEQ_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Unary Seq Expression</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNARY_SEQ_EXPRESSION_FEATURE_COUNT = SEQ_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqGetTailImpl <em>Seq Get Tail</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqGetTailImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqGetTail()
	 * @generated
	 */
  int SEQ_GET_TAIL = 36;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_GET_TAIL__NAME = UNARY_SEQ_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_GET_TAIL__TYPE = UNARY_SEQ_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_GET_TAIL__OPERAND = UNARY_SEQ_EXPRESSION__OPERAND;

  /**
	 * The number of structural features of the '<em>Seq Get Tail</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_GET_TAIL_FEATURE_COUNT = UNARY_SEQ_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqGetHeadImpl <em>Seq Get Head</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqGetHeadImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqGetHead()
	 * @generated
	 */
  int SEQ_GET_HEAD = 37;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_GET_HEAD__NAME = INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_GET_HEAD__TYPE = INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_GET_HEAD__OPERAND = INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Seq Get Head</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_GET_HEAD_FEATURE_COUNT = INTEGER_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqDecrImpl <em>Seq Decr</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqDecrImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqDecr()
	 * @generated
	 */
  int SEQ_DECR = 38;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_DECR__NAME = UNARY_SEQ_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_DECR__TYPE = UNARY_SEQ_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_DECR__OPERAND = UNARY_SEQ_EXPRESSION__OPERAND;

  /**
	 * The number of structural features of the '<em>Seq Decr</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_DECR_FEATURE_COUNT = UNARY_SEQ_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqSchedImpl <em>Seq Sched</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqSchedImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqSched()
	 * @generated
	 */
  int SEQ_SCHED = 39;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_SCHED__NAME = UNARY_SEQ_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_SCHED__TYPE = UNARY_SEQ_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_SCHED__OPERAND = UNARY_SEQ_EXPRESSION__OPERAND;

  /**
	 * The feature id for the '<em><b>Integer Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_SCHED__INTEGER_EXPR = UNARY_SEQ_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Seq Sched</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQ_SCHED_FEATURE_COUNT = UNARY_SEQ_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanVariableRefImpl <em>Boolean Variable Ref</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanVariableRefImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBooleanVariableRef()
	 * @generated
	 */
  int BOOLEAN_VARIABLE_REF = 40;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_VARIABLE_REF__NAME = BOOLEAN_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_VARIABLE_REF__TYPE = BOOLEAN_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Referenced Var</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_VARIABLE_REF__REFERENCED_VAR = BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Boolean Variable Ref</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_VARIABLE_REF_FEATURE_COUNT = BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerVariableRefImpl <em>Integer Variable Ref</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerVariableRefImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntegerVariableRef()
	 * @generated
	 */
  int INTEGER_VARIABLE_REF = 41;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_VARIABLE_REF__NAME = INTEGER_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_VARIABLE_REF__TYPE = INTEGER_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Referenced Var</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_VARIABLE_REF__REFERENCED_VAR = INTEGER_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Integer Variable Ref</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_VARIABLE_REF_FEATURE_COUNT = INTEGER_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealVariableRefImpl <em>Real Variable Ref</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealVariableRefImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealVariableRef()
	 * @generated
	 */
  int REAL_VARIABLE_REF = 42;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_VARIABLE_REF__NAME = REAL_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_VARIABLE_REF__TYPE = REAL_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Referenced Var</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_VARIABLE_REF__REFERENCED_VAR = REAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Real Variable Ref</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_VARIABLE_REF_FEATURE_COUNT = REAL_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqRefImpl <em>Number Seq Ref</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqRefImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getNumberSeqRef()
	 * @generated
	 */
  int NUMBER_SEQ_REF = 43;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NUMBER_SEQ_REF__NAME = SEQ_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NUMBER_SEQ_REF__TYPE = SEQ_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Referenced Number Seq</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ = SEQ_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Number Seq Ref</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NUMBER_SEQ_REF_FEATURE_COUNT = SEQ_EXPRESSION_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqVariableRefImpl <em>Number Seq Variable Ref</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqVariableRefImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getNumberSeqVariableRef()
	 * @generated
	 */
  int NUMBER_SEQ_VARIABLE_REF = 44;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NUMBER_SEQ_VARIABLE_REF__NAME = SEQ_EXPRESSION__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NUMBER_SEQ_VARIABLE_REF__TYPE = SEQ_EXPRESSION__TYPE;

  /**
	 * The feature id for the '<em><b>Referenced Var</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NUMBER_SEQ_VARIABLE_REF__REFERENCED_VAR = SEQ_EXPRESSION_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Number Seq Variable Ref</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NUMBER_SEQ_VARIABLE_REF_FEATURE_COUNT = SEQ_EXPRESSION_FEATURE_COUNT + 1;


  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression <em>Classical Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Classical Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression
	 * @generated
	 */
  EClass getClassicalExpression();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression <em>Boolean Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression
	 * @generated
	 */
  EClass getBooleanExpression();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealExpression <em>Real Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealExpression
	 * @generated
	 */
  EClass getRealExpression();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerExpression <em>Integer Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerExpression
	 * @generated
	 */
  EClass getIntegerExpression();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression <em>Binary Boolean Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Boolean Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression
	 * @generated
	 */
  EClass getBinaryBooleanExpression();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression#getLeftValue()
	 * @see #getBinaryBooleanExpression()
	 * @generated
	 */
  EReference getBinaryBooleanExpression_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression#getRightValue()
	 * @see #getBinaryBooleanExpression()
	 * @generated
	 */
  EReference getBinaryBooleanExpression_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryBooleanExpression <em>Unary Boolean Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Boolean Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryBooleanExpression
	 * @generated
	 */
  EClass getUnaryBooleanExpression();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryBooleanExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryBooleanExpression#getOperand()
	 * @see #getUnaryBooleanExpression()
	 * @generated
	 */
  EReference getUnaryBooleanExpression_Operand();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression <em>Binary Real Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Real Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression
	 * @generated
	 */
  EClass getBinaryRealExpression();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression#getLeftValue()
	 * @see #getBinaryRealExpression()
	 * @generated
	 */
  EReference getBinaryRealExpression_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression#getRightValue()
	 * @see #getBinaryRealExpression()
	 * @generated
	 */
  EReference getBinaryRealExpression_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression <em>Unary Real Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Real Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression
	 * @generated
	 */
  EClass getUnaryRealExpression();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression#getValue()
	 * @see #getUnaryRealExpression()
	 * @generated
	 */
  EReference getUnaryRealExpression_Value();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression#getOperand()
	 * @see #getUnaryRealExpression()
	 * @generated
	 */
  EReference getUnaryRealExpression_Operand();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression <em>Binary Integer Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Integer Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression
	 * @generated
	 */
  EClass getBinaryIntegerExpression();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression#getLeftValue()
	 * @see #getBinaryIntegerExpression()
	 * @generated
	 */
  EReference getBinaryIntegerExpression_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression#getRightValue()
	 * @see #getBinaryIntegerExpression()
	 * @generated
	 */
  EReference getBinaryIntegerExpression_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression <em>Unary Integer Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Integer Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression
	 * @generated
	 */
  EClass getUnaryIntegerExpression();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression#getValue()
	 * @see #getUnaryIntegerExpression()
	 * @generated
	 */
  EReference getUnaryIntegerExpression_Value();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression#getOperand()
	 * @see #getUnaryIntegerExpression()
	 * @generated
	 */
  EReference getUnaryIntegerExpression_Operand();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef <em>Boolean Ref</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Ref</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef
	 * @generated
	 */
  EClass getBooleanRef();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef#getReferencedBool <em>Referenced Bool</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Bool</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef#getReferencedBool()
	 * @see #getBooleanRef()
	 * @generated
	 */
  EReference getBooleanRef_ReferencedBool();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef <em>Real Ref</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Ref</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef
	 * @generated
	 */
  EClass getRealRef();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef#getRealElem <em>Real Elem</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Real Elem</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef#getRealElem()
	 * @see #getRealRef()
	 * @generated
	 */
  EReference getRealRef_RealElem();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef <em>Integer Ref</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Ref</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef
	 * @generated
	 */
  EClass getIntegerRef();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef#getIntegerElem <em>Integer Elem</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Integer Elem</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef#getIntegerElem()
	 * @see #getIntegerRef()
	 * @generated
	 */
  EReference getIntegerRef_IntegerElem();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealPlus <em>Unary Real Plus</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Real Plus</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealPlus
	 * @generated
	 */
  EClass getUnaryRealPlus();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealMinus <em>Unary Real Minus</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Real Minus</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealMinus
	 * @generated
	 */
  EClass getUnaryRealMinus();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealPlus <em>Real Plus</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Plus</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealPlus
	 * @generated
	 */
  EClass getRealPlus();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMinus <em>Real Minus</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Minus</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMinus
	 * @generated
	 */
  EClass getRealMinus();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMultiply <em>Real Multiply</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Multiply</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMultiply
	 * @generated
	 */
  EClass getRealMultiply();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntPlus <em>Unary Int Plus</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Int Plus</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntPlus
	 * @generated
	 */
  EClass getUnaryIntPlus();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntMinus <em>Unary Int Minus</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Int Minus</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntMinus
	 * @generated
	 */
  EClass getUnaryIntMinus();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus <em>Int Plus</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Plus</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus
	 * @generated
	 */
  EClass getIntPlus();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus <em>Int Minus</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Minus</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus
	 * @generated
	 */
  EClass getIntMinus();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMultiply <em>Int Multiply</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Multiply</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMultiply
	 * @generated
	 */
  EClass getIntMultiply();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntDivide <em>Int Divide</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Divide</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntDivide
	 * @generated
	 */
  EClass getIntDivide();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not <em>Not</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not
	 * @generated
	 */
  EClass getNot();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And <em>And</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And
	 * @generated
	 */
  EClass getAnd();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or <em>Or</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or
	 * @generated
	 */
  EClass getOr();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Xor <em>Xor</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Xor</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Xor
	 * @generated
	 */
  EClass getXor();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual <em>Real Equal</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Equal</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual
	 * @generated
	 */
  EClass getRealEqual();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual#getLeftValue()
	 * @see #getRealEqual()
	 * @generated
	 */
  EReference getRealEqual_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual#getRightValue()
	 * @see #getRealEqual()
	 * @generated
	 */
  EReference getRealEqual_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf <em>Real Inf</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Inf</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf
	 * @generated
	 */
  EClass getRealInf();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf#getLeftValue()
	 * @see #getRealInf()
	 * @generated
	 */
  EReference getRealInf_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf#getRightValue()
	 * @see #getRealInf()
	 * @generated
	 */
  EReference getRealInf_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup <em>Real Sup</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Sup</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup
	 * @generated
	 */
  EClass getRealSup();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup#getLeftValue()
	 * @see #getRealSup()
	 * @generated
	 */
  EReference getRealSup_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup#getRightValue()
	 * @see #getRealSup()
	 * @generated
	 */
  EReference getRealSup_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual <em>Int Equal</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Equal</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual
	 * @generated
	 */
  EClass getIntEqual();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual#getLeftValue()
	 * @see #getIntEqual()
	 * @generated
	 */
  EReference getIntEqual_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual#getRightValue()
	 * @see #getIntEqual()
	 * @generated
	 */
  EReference getIntEqual_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf <em>Int Inf</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Inf</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf
	 * @generated
	 */
  EClass getIntInf();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf#getLeftValue()
	 * @see #getIntInf()
	 * @generated
	 */
  EReference getIntInf_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf#getRightValue()
	 * @see #getIntInf()
	 * @generated
	 */
  EReference getIntInf_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup <em>Int Sup</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Sup</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup
	 * @generated
	 */
  EClass getIntSup();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup#getLeftValue <em>Left Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup#getLeftValue()
	 * @see #getIntSup()
	 * @generated
	 */
  EReference getIntSup_LeftValue();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup#getRightValue <em>Right Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup#getRightValue()
	 * @see #getIntSup()
	 * @generated
	 */
  EReference getIntSup_RightValue();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty <em>Seq Is Empty</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Seq Is Empty</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty
	 * @generated
	 */
  EClass getSeqIsEmpty();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty#getOperand()
	 * @see #getSeqIsEmpty()
	 * @generated
	 */
  EReference getSeqIsEmpty_Operand();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnarySeqExpression <em>Unary Seq Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Seq Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnarySeqExpression
	 * @generated
	 */
  EClass getUnarySeqExpression();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnarySeqExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnarySeqExpression#getOperand()
	 * @see #getUnarySeqExpression()
	 * @generated
	 */
  EReference getUnarySeqExpression_Operand();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail <em>Seq Get Tail</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Seq Get Tail</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail
	 * @generated
	 */
  EClass getSeqGetTail();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead <em>Seq Get Head</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Seq Get Head</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead
	 * @generated
	 */
  EClass getSeqGetHead();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead#getOperand()
	 * @see #getSeqGetHead()
	 * @generated
	 */
  EReference getSeqGetHead_Operand();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqDecr <em>Seq Decr</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Seq Decr</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqDecr
	 * @generated
	 */
  EClass getSeqDecr();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched <em>Seq Sched</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Seq Sched</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched
	 * @generated
	 */
  EClass getSeqSched();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched#getIntegerExpr <em>Integer Expr</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Integer Expr</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched#getIntegerExpr()
	 * @see #getSeqSched()
	 * @generated
	 */
  EReference getSeqSched_IntegerExpr();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef <em>Boolean Variable Ref</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Variable Ref</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef
	 * @generated
	 */
  EClass getBooleanVariableRef();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef#getReferencedVar <em>Referenced Var</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Var</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef#getReferencedVar()
	 * @see #getBooleanVariableRef()
	 * @generated
	 */
  EReference getBooleanVariableRef_ReferencedVar();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef <em>Integer Variable Ref</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Variable Ref</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef
	 * @generated
	 */
  EClass getIntegerVariableRef();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef#getReferencedVar <em>Referenced Var</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Var</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef#getReferencedVar()
	 * @see #getIntegerVariableRef()
	 * @generated
	 */
  EReference getIntegerVariableRef_ReferencedVar();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef <em>Real Variable Ref</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Variable Ref</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef
	 * @generated
	 */
  EClass getRealVariableRef();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef#getReferencedVar <em>Referenced Var</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Var</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef#getReferencedVar()
	 * @see #getRealVariableRef()
	 * @generated
	 */
  EReference getRealVariableRef_ReferencedVar();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef <em>Number Seq Ref</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Seq Ref</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef
	 * @generated
	 */
  EClass getNumberSeqRef();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef#getReferencedNumberSeq <em>Referenced Number Seq</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Number Seq</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef#getReferencedNumberSeq()
	 * @see #getNumberSeqRef()
	 * @generated
	 */
  EReference getNumberSeqRef_ReferencedNumberSeq();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef <em>Number Seq Variable Ref</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Seq Variable Ref</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef
	 * @generated
	 */
  EClass getNumberSeqVariableRef();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef#getReferencedVar <em>Referenced Var</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Var</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef#getReferencedVar()
	 * @see #getNumberSeqVariableRef()
	 * @generated
	 */
  EReference getNumberSeqVariableRef_ReferencedVar();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression <em>Seq Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Seq Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression
	 * @generated
	 */
  EClass getSeqExpression();

  /**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
  ClassicalExpressionFactory getClassicalExpressionFactory();

  /**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionImpl <em>Classical Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getClassicalExpression()
		 * @generated
		 */
    EClass CLASSICAL_EXPRESSION = eINSTANCE.getClassicalExpression();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanExpressionImpl <em>Boolean Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBooleanExpression()
		 * @generated
		 */
    EClass BOOLEAN_EXPRESSION = eINSTANCE.getBooleanExpression();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealExpressionImpl <em>Real Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealExpression()
		 * @generated
		 */
    EClass REAL_EXPRESSION = eINSTANCE.getRealExpression();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerExpressionImpl <em>Integer Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntegerExpression()
		 * @generated
		 */
    EClass INTEGER_EXPRESSION = eINSTANCE.getIntegerExpression();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryBooleanExpressionImpl <em>Binary Boolean Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryBooleanExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBinaryBooleanExpression()
		 * @generated
		 */
    EClass BINARY_BOOLEAN_EXPRESSION = eINSTANCE.getBinaryBooleanExpression();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE = eINSTANCE.getBinaryBooleanExpression_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE = eINSTANCE.getBinaryBooleanExpression_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryBooleanExpressionImpl <em>Unary Boolean Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryBooleanExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryBooleanExpression()
		 * @generated
		 */
    EClass UNARY_BOOLEAN_EXPRESSION = eINSTANCE.getUnaryBooleanExpression();

    /**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference UNARY_BOOLEAN_EXPRESSION__OPERAND = eINSTANCE.getUnaryBooleanExpression_Operand();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryRealExpressionImpl <em>Binary Real Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryRealExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBinaryRealExpression()
		 * @generated
		 */
    EClass BINARY_REAL_EXPRESSION = eINSTANCE.getBinaryRealExpression();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BINARY_REAL_EXPRESSION__LEFT_VALUE = eINSTANCE.getBinaryRealExpression_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BINARY_REAL_EXPRESSION__RIGHT_VALUE = eINSTANCE.getBinaryRealExpression_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealExpressionImpl <em>Unary Real Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryRealExpression()
		 * @generated
		 */
    EClass UNARY_REAL_EXPRESSION = eINSTANCE.getUnaryRealExpression();

    /**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference UNARY_REAL_EXPRESSION__VALUE = eINSTANCE.getUnaryRealExpression_Value();

    /**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference UNARY_REAL_EXPRESSION__OPERAND = eINSTANCE.getUnaryRealExpression_Operand();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryIntegerExpressionImpl <em>Binary Integer Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryIntegerExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBinaryIntegerExpression()
		 * @generated
		 */
    EClass BINARY_INTEGER_EXPRESSION = eINSTANCE.getBinaryIntegerExpression();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BINARY_INTEGER_EXPRESSION__LEFT_VALUE = eINSTANCE.getBinaryIntegerExpression_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BINARY_INTEGER_EXPRESSION__RIGHT_VALUE = eINSTANCE.getBinaryIntegerExpression_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntegerExpressionImpl <em>Unary Integer Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntegerExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryIntegerExpression()
		 * @generated
		 */
    EClass UNARY_INTEGER_EXPRESSION = eINSTANCE.getUnaryIntegerExpression();

    /**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference UNARY_INTEGER_EXPRESSION__VALUE = eINSTANCE.getUnaryIntegerExpression_Value();

    /**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference UNARY_INTEGER_EXPRESSION__OPERAND = eINSTANCE.getUnaryIntegerExpression_Operand();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanRefImpl <em>Boolean Ref</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanRefImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBooleanRef()
		 * @generated
		 */
    EClass BOOLEAN_REF = eINSTANCE.getBooleanRef();

    /**
		 * The meta object literal for the '<em><b>Referenced Bool</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BOOLEAN_REF__REFERENCED_BOOL = eINSTANCE.getBooleanRef_ReferencedBool();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealRefImpl <em>Real Ref</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealRefImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealRef()
		 * @generated
		 */
    EClass REAL_REF = eINSTANCE.getRealRef();

    /**
		 * The meta object literal for the '<em><b>Real Elem</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REAL_REF__REAL_ELEM = eINSTANCE.getRealRef_RealElem();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerRefImpl <em>Integer Ref</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerRefImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntegerRef()
		 * @generated
		 */
    EClass INTEGER_REF = eINSTANCE.getIntegerRef();

    /**
		 * The meta object literal for the '<em><b>Integer Elem</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference INTEGER_REF__INTEGER_ELEM = eINSTANCE.getIntegerRef_IntegerElem();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealPlusImpl <em>Unary Real Plus</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealPlusImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryRealPlus()
		 * @generated
		 */
    EClass UNARY_REAL_PLUS = eINSTANCE.getUnaryRealPlus();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealMinusImpl <em>Unary Real Minus</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryRealMinusImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryRealMinus()
		 * @generated
		 */
    EClass UNARY_REAL_MINUS = eINSTANCE.getUnaryRealMinus();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealPlusImpl <em>Real Plus</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealPlusImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealPlus()
		 * @generated
		 */
    EClass REAL_PLUS = eINSTANCE.getRealPlus();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealMinusImpl <em>Real Minus</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealMinusImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealMinus()
		 * @generated
		 */
    EClass REAL_MINUS = eINSTANCE.getRealMinus();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealMultiplyImpl <em>Real Multiply</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealMultiplyImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealMultiply()
		 * @generated
		 */
    EClass REAL_MULTIPLY = eINSTANCE.getRealMultiply();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntPlusImpl <em>Unary Int Plus</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntPlusImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryIntPlus()
		 * @generated
		 */
    EClass UNARY_INT_PLUS = eINSTANCE.getUnaryIntPlus();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntMinusImpl <em>Unary Int Minus</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnaryIntMinusImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnaryIntMinus()
		 * @generated
		 */
    EClass UNARY_INT_MINUS = eINSTANCE.getUnaryIntMinus();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntPlusImpl <em>Int Plus</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntPlusImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntPlus()
		 * @generated
		 */
    EClass INT_PLUS = eINSTANCE.getIntPlus();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntMinusImpl <em>Int Minus</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntMinusImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntMinus()
		 * @generated
		 */
    EClass INT_MINUS = eINSTANCE.getIntMinus();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntMultiplyImpl <em>Int Multiply</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntMultiplyImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntMultiply()
		 * @generated
		 */
    EClass INT_MULTIPLY = eINSTANCE.getIntMultiply();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntDivideImpl <em>Int Divide</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntDivideImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntDivide()
		 * @generated
		 */
    EClass INT_DIVIDE = eINSTANCE.getIntDivide();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NotImpl <em>Not</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NotImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getNot()
		 * @generated
		 */
    EClass NOT = eINSTANCE.getNot();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.AndImpl <em>And</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.AndImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getAnd()
		 * @generated
		 */
    EClass AND = eINSTANCE.getAnd();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.OrImpl <em>Or</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.OrImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getOr()
		 * @generated
		 */
    EClass OR = eINSTANCE.getOr();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.XorImpl <em>Xor</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.XorImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getXor()
		 * @generated
		 */
    EClass XOR = eINSTANCE.getXor();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealEqualImpl <em>Real Equal</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealEqualImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealEqual()
		 * @generated
		 */
    EClass REAL_EQUAL = eINSTANCE.getRealEqual();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REAL_EQUAL__LEFT_VALUE = eINSTANCE.getRealEqual_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REAL_EQUAL__RIGHT_VALUE = eINSTANCE.getRealEqual_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealInfImpl <em>Real Inf</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealInfImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealInf()
		 * @generated
		 */
    EClass REAL_INF = eINSTANCE.getRealInf();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REAL_INF__LEFT_VALUE = eINSTANCE.getRealInf_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REAL_INF__RIGHT_VALUE = eINSTANCE.getRealInf_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealSupImpl <em>Real Sup</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealSupImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealSup()
		 * @generated
		 */
    EClass REAL_SUP = eINSTANCE.getRealSup();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REAL_SUP__LEFT_VALUE = eINSTANCE.getRealSup_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REAL_SUP__RIGHT_VALUE = eINSTANCE.getRealSup_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntEqualImpl <em>Int Equal</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntEqualImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntEqual()
		 * @generated
		 */
    EClass INT_EQUAL = eINSTANCE.getIntEqual();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference INT_EQUAL__LEFT_VALUE = eINSTANCE.getIntEqual_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference INT_EQUAL__RIGHT_VALUE = eINSTANCE.getIntEqual_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntInfImpl <em>Int Inf</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntInfImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntInf()
		 * @generated
		 */
    EClass INT_INF = eINSTANCE.getIntInf();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference INT_INF__LEFT_VALUE = eINSTANCE.getIntInf_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference INT_INF__RIGHT_VALUE = eINSTANCE.getIntInf_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntSupImpl <em>Int Sup</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntSupImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntSup()
		 * @generated
		 */
    EClass INT_SUP = eINSTANCE.getIntSup();

    /**
		 * The meta object literal for the '<em><b>Left Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference INT_SUP__LEFT_VALUE = eINSTANCE.getIntSup_LeftValue();

    /**
		 * The meta object literal for the '<em><b>Right Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference INT_SUP__RIGHT_VALUE = eINSTANCE.getIntSup_RightValue();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqIsEmptyImpl <em>Seq Is Empty</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqIsEmptyImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqIsEmpty()
		 * @generated
		 */
    EClass SEQ_IS_EMPTY = eINSTANCE.getSeqIsEmpty();

    /**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference SEQ_IS_EMPTY__OPERAND = eINSTANCE.getSeqIsEmpty_Operand();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnarySeqExpressionImpl <em>Unary Seq Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.UnarySeqExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getUnarySeqExpression()
		 * @generated
		 */
    EClass UNARY_SEQ_EXPRESSION = eINSTANCE.getUnarySeqExpression();

    /**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference UNARY_SEQ_EXPRESSION__OPERAND = eINSTANCE.getUnarySeqExpression_Operand();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqGetTailImpl <em>Seq Get Tail</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqGetTailImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqGetTail()
		 * @generated
		 */
    EClass SEQ_GET_TAIL = eINSTANCE.getSeqGetTail();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqGetHeadImpl <em>Seq Get Head</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqGetHeadImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqGetHead()
		 * @generated
		 */
    EClass SEQ_GET_HEAD = eINSTANCE.getSeqGetHead();

    /**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference SEQ_GET_HEAD__OPERAND = eINSTANCE.getSeqGetHead_Operand();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqDecrImpl <em>Seq Decr</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqDecrImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqDecr()
		 * @generated
		 */
    EClass SEQ_DECR = eINSTANCE.getSeqDecr();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqSchedImpl <em>Seq Sched</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqSchedImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqSched()
		 * @generated
		 */
    EClass SEQ_SCHED = eINSTANCE.getSeqSched();

    /**
		 * The meta object literal for the '<em><b>Integer Expr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference SEQ_SCHED__INTEGER_EXPR = eINSTANCE.getSeqSched_IntegerExpr();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanVariableRefImpl <em>Boolean Variable Ref</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanVariableRefImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getBooleanVariableRef()
		 * @generated
		 */
    EClass BOOLEAN_VARIABLE_REF = eINSTANCE.getBooleanVariableRef();

    /**
		 * The meta object literal for the '<em><b>Referenced Var</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BOOLEAN_VARIABLE_REF__REFERENCED_VAR = eINSTANCE.getBooleanVariableRef_ReferencedVar();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerVariableRefImpl <em>Integer Variable Ref</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.IntegerVariableRefImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getIntegerVariableRef()
		 * @generated
		 */
    EClass INTEGER_VARIABLE_REF = eINSTANCE.getIntegerVariableRef();

    /**
		 * The meta object literal for the '<em><b>Referenced Var</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference INTEGER_VARIABLE_REF__REFERENCED_VAR = eINSTANCE.getIntegerVariableRef_ReferencedVar();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealVariableRefImpl <em>Real Variable Ref</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.RealVariableRefImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getRealVariableRef()
		 * @generated
		 */
    EClass REAL_VARIABLE_REF = eINSTANCE.getRealVariableRef();

    /**
		 * The meta object literal for the '<em><b>Referenced Var</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference REAL_VARIABLE_REF__REFERENCED_VAR = eINSTANCE.getRealVariableRef_ReferencedVar();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqRefImpl <em>Number Seq Ref</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqRefImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getNumberSeqRef()
		 * @generated
		 */
    EClass NUMBER_SEQ_REF = eINSTANCE.getNumberSeqRef();

    /**
		 * The meta object literal for the '<em><b>Referenced Number Seq</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ = eINSTANCE.getNumberSeqRef_ReferencedNumberSeq();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqVariableRefImpl <em>Number Seq Variable Ref</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqVariableRefImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getNumberSeqVariableRef()
		 * @generated
		 */
    EClass NUMBER_SEQ_VARIABLE_REF = eINSTANCE.getNumberSeqVariableRef();

    /**
		 * The meta object literal for the '<em><b>Referenced Var</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference NUMBER_SEQ_VARIABLE_REF__REFERENCED_VAR = eINSTANCE.getNumberSeqVariableRef_ReferencedVar();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqExpressionImpl <em>Seq Expression</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqExpressionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl#getSeqExpression()
		 * @generated
		 */
    EClass SEQ_EXPRESSION = eINSTANCE.getSeqExpression();

  }

} //ClassicalExpressionPackage
