/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.PrimitivesTypeValuePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class KernelRelationPackageImpl extends EPackageImpl implements KernelRelationPackage
{
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass kernelRelationDeclarationEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass subClockEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass coincidenceEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass exclusionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass precedenceEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass nonStrictPrecedenceEClass = null;

  /**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private KernelRelationPackageImpl()
  {
		super(eNS_URI, KernelRelationFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link KernelRelationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static KernelRelationPackage init()
  {
		if (isInited) return (KernelRelationPackage)EPackage.Registry.INSTANCE.getEPackage(KernelRelationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredKernelRelationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		KernelRelationPackageImpl theKernelRelationPackage = registeredKernelRelationPackage instanceof KernelRelationPackageImpl ? (KernelRelationPackageImpl)registeredKernelRelationPackage : new KernelRelationPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		TimeModelPackageImpl theTimeModelPackage = (TimeModelPackageImpl)(registeredPackage instanceof TimeModelPackageImpl ? registeredPackage : TimeModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CCSLModelPackage.eNS_URI);
		CCSLModelPackageImpl theCCSLModelPackage = (CCSLModelPackageImpl)(registeredPackage instanceof CCSLModelPackageImpl ? registeredPackage : CCSLModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassicalExpressionPackage.eNS_URI);
		ClassicalExpressionPackageImpl theClassicalExpressionPackage = (ClassicalExpressionPackageImpl)(registeredPackage instanceof ClassicalExpressionPackageImpl ? registeredPackage : ClassicalExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);
		ClockExpressionAndRelationPackageImpl theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackageImpl)(registeredPackage instanceof ClockExpressionAndRelationPackageImpl ? registeredPackage : ClockExpressionAndRelationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelExpressionPackage.eNS_URI);
		KernelExpressionPackageImpl theKernelExpressionPackage = (KernelExpressionPackageImpl)(registeredPackage instanceof KernelExpressionPackageImpl ? registeredPackage : KernelExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BasicTypePackage.eNS_URI);
		BasicTypePackageImpl theBasicTypePackage = (BasicTypePackageImpl)(registeredPackage instanceof BasicTypePackageImpl ? registeredPackage : BasicTypePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PrimitivesTypeValuePackage.eNS_URI);
		PrimitivesTypeValuePackageImpl thePrimitivesTypeValuePackage = (PrimitivesTypeValuePackageImpl)(registeredPackage instanceof PrimitivesTypeValuePackageImpl ? registeredPackage : PrimitivesTypeValuePackage.eINSTANCE);

		// Create package meta-data objects
		theKernelRelationPackage.createPackageContents();
		theTimeModelPackage.createPackageContents();
		theCCSLModelPackage.createPackageContents();
		theClassicalExpressionPackage.createPackageContents();
		theClockExpressionAndRelationPackage.createPackageContents();
		theKernelExpressionPackage.createPackageContents();
		theBasicTypePackage.createPackageContents();
		thePrimitivesTypeValuePackage.createPackageContents();

		// Initialize created meta-data
		theKernelRelationPackage.initializePackageContents();
		theTimeModelPackage.initializePackageContents();
		theCCSLModelPackage.initializePackageContents();
		theClassicalExpressionPackage.initializePackageContents();
		theClockExpressionAndRelationPackage.initializePackageContents();
		theKernelExpressionPackage.initializePackageContents();
		theBasicTypePackage.initializePackageContents();
		thePrimitivesTypeValuePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theKernelRelationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(KernelRelationPackage.eNS_URI, theKernelRelationPackage);
		return theKernelRelationPackage;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getKernelRelationDeclaration()
  {
		return kernelRelationDeclarationEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getKernelRelationDeclaration_RightEntity()
  {
		return (EReference)kernelRelationDeclarationEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getKernelRelationDeclaration_LeftEntity()
  {
		return (EReference)kernelRelationDeclarationEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSubClock()
  {
		return subClockEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getCoincidence()
  {
		return coincidenceEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getExclusion()
  {
		return exclusionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getPrecedence()
  {
		return precedenceEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getNonStrictPrecedence()
  {
		return nonStrictPrecedenceEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public KernelRelationFactory getKernelRelationFactory()
  {
		return (KernelRelationFactory)getEFactoryInstance();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		kernelRelationDeclarationEClass = createEClass(KERNEL_RELATION_DECLARATION);
		createEReference(kernelRelationDeclarationEClass, KERNEL_RELATION_DECLARATION__RIGHT_ENTITY);
		createEReference(kernelRelationDeclarationEClass, KERNEL_RELATION_DECLARATION__LEFT_ENTITY);

		subClockEClass = createEClass(SUB_CLOCK);

		coincidenceEClass = createEClass(COINCIDENCE);

		exclusionEClass = createEClass(EXCLUSION);

		precedenceEClass = createEClass(PRECEDENCE);

		nonStrictPrecedenceEClass = createEClass(NON_STRICT_PRECEDENCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ClockExpressionAndRelationPackage theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackage)EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		kernelRelationDeclarationEClass.getESuperTypes().add(theClockExpressionAndRelationPackage.getRelationDeclaration());
		subClockEClass.getESuperTypes().add(this.getKernelRelationDeclaration());
		coincidenceEClass.getESuperTypes().add(this.getKernelRelationDeclaration());
		exclusionEClass.getESuperTypes().add(this.getKernelRelationDeclaration());
		precedenceEClass.getESuperTypes().add(this.getKernelRelationDeclaration());
		nonStrictPrecedenceEClass.getESuperTypes().add(this.getKernelRelationDeclaration());

		// Initialize classes and features; add operations and parameters
		initEClass(kernelRelationDeclarationEClass, KernelRelationDeclaration.class, "KernelRelationDeclaration", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getKernelRelationDeclaration_RightEntity(), theClockExpressionAndRelationPackage.getAbstractEntity(), null, "rightEntity", null, 1, 1, KernelRelationDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getKernelRelationDeclaration_LeftEntity(), theClockExpressionAndRelationPackage.getAbstractEntity(), null, "leftEntity", null, 1, 1, KernelRelationDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(subClockEClass, SubClock.class, "SubClock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(coincidenceEClass, Coincidence.class, "Coincidence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(exclusionEClass, Exclusion.class, "Exclusion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(precedenceEClass, Precedence.class, "Precedence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nonStrictPrecedenceEClass, NonStrictPrecedence.class, "NonStrictPrecedence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //KernelRelationPackageImpl
