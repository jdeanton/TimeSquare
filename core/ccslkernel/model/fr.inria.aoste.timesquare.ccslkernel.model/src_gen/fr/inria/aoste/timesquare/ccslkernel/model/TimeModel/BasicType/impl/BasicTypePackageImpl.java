/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Char;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Real;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl.KernelRelationPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.PrimitivesTypeValuePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BasicTypePackageImpl extends EPackageImpl implements BasicTypePackage
{
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass typeEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass elementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass primitiveTypeEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass primitiveElementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass stringEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass booleanEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass integerEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass charEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass stringElementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass booleanElementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass integerElementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realElementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass charElementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass recordEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass sequenceTypeEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass fieldEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass recordElementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass boxEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass sequenceElementEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass discreteClockTypeEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass denseClockTypeEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass enumerationTypeEClass = null;

  /**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private BasicTypePackageImpl()
  {
		super(eNS_URI, BasicTypeFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link BasicTypePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static BasicTypePackage init()
  {
		if (isInited) return (BasicTypePackage)EPackage.Registry.INSTANCE.getEPackage(BasicTypePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredBasicTypePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		BasicTypePackageImpl theBasicTypePackage = registeredBasicTypePackage instanceof BasicTypePackageImpl ? (BasicTypePackageImpl)registeredBasicTypePackage : new BasicTypePackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		TimeModelPackageImpl theTimeModelPackage = (TimeModelPackageImpl)(registeredPackage instanceof TimeModelPackageImpl ? registeredPackage : TimeModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CCSLModelPackage.eNS_URI);
		CCSLModelPackageImpl theCCSLModelPackage = (CCSLModelPackageImpl)(registeredPackage instanceof CCSLModelPackageImpl ? registeredPackage : CCSLModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassicalExpressionPackage.eNS_URI);
		ClassicalExpressionPackageImpl theClassicalExpressionPackage = (ClassicalExpressionPackageImpl)(registeredPackage instanceof ClassicalExpressionPackageImpl ? registeredPackage : ClassicalExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);
		ClockExpressionAndRelationPackageImpl theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackageImpl)(registeredPackage instanceof ClockExpressionAndRelationPackageImpl ? registeredPackage : ClockExpressionAndRelationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelExpressionPackage.eNS_URI);
		KernelExpressionPackageImpl theKernelExpressionPackage = (KernelExpressionPackageImpl)(registeredPackage instanceof KernelExpressionPackageImpl ? registeredPackage : KernelExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelRelationPackage.eNS_URI);
		KernelRelationPackageImpl theKernelRelationPackage = (KernelRelationPackageImpl)(registeredPackage instanceof KernelRelationPackageImpl ? registeredPackage : KernelRelationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PrimitivesTypeValuePackage.eNS_URI);
		PrimitivesTypeValuePackageImpl thePrimitivesTypeValuePackage = (PrimitivesTypeValuePackageImpl)(registeredPackage instanceof PrimitivesTypeValuePackageImpl ? registeredPackage : PrimitivesTypeValuePackage.eINSTANCE);

		// Create package meta-data objects
		theBasicTypePackage.createPackageContents();
		theTimeModelPackage.createPackageContents();
		theCCSLModelPackage.createPackageContents();
		theClassicalExpressionPackage.createPackageContents();
		theClockExpressionAndRelationPackage.createPackageContents();
		theKernelExpressionPackage.createPackageContents();
		theKernelRelationPackage.createPackageContents();
		thePrimitivesTypeValuePackage.createPackageContents();

		// Initialize created meta-data
		theBasicTypePackage.initializePackageContents();
		theTimeModelPackage.initializePackageContents();
		theCCSLModelPackage.initializePackageContents();
		theClassicalExpressionPackage.initializePackageContents();
		theClockExpressionAndRelationPackage.initializePackageContents();
		theKernelExpressionPackage.initializePackageContents();
		theKernelRelationPackage.initializePackageContents();
		thePrimitivesTypeValuePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBasicTypePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BasicTypePackage.eNS_URI, theBasicTypePackage);
		return theBasicTypePackage;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getType()
  {
		return typeEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getElement()
  {
		return elementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getElement_Type()
  {
		return (EReference)elementEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getPrimitiveType()
  {
		return primitiveTypeEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getPrimitiveElement()
  {
		return primitiveElementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getString()
  {
		return stringEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBoolean()
  {
		return booleanEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getInteger()
  {
		return integerEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getReal()
  {
		return realEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getChar()
  {
		return charEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getStringElement()
  {
		return stringElementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getStringElement_Value()
  {
		return (EAttribute)stringElementEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBooleanElement()
  {
		return booleanElementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getBooleanElement_Value()
  {
		return (EAttribute)booleanElementEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntegerElement()
  {
		return integerElementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getIntegerElement_Value()
  {
		return (EAttribute)integerElementEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealElement()
  {
		return realElementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getRealElement_Value()
  {
		return (EAttribute)realElementEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getCharElement()
  {
		return charElementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getCharElement_Value()
  {
		return (EAttribute)charElementEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRecord()
  {
		return recordEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRecord_Field()
  {
		return (EReference)recordEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSequenceType()
  {
		return sequenceTypeEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getSequenceType_ElementType()
  {
		return (EReference)sequenceTypeEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getField()
  {
		return fieldEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getField_Type()
  {
		return (EReference)fieldEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRecordElement()
  {
		return recordElementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRecordElement_Box()
  {
		return (EReference)recordElementEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBox()
  {
		return boxEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBox_Containment()
  {
		return (EReference)boxEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSequenceElement()
  {
		return sequenceElementEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getSequenceElement_FinitePart()
  {
		return (EReference)sequenceElementEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getSequenceElement_NonFinitePart()
  {
		return (EReference)sequenceElementEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getDiscreteClockType()
  {
		return discreteClockTypeEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getDenseClockType()
  {
		return denseClockTypeEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getDenseClockType_BaseUnit()
  {
		return (EAttribute)denseClockTypeEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getDenseClockType_PhysicalMagnitude()
  {
		return (EAttribute)denseClockTypeEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getEnumerationType()
  {
		return enumerationTypeEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getEnumerationType_EnumLiteral()
  {
		return (EAttribute)enumerationTypeEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public BasicTypeFactory getBasicTypeFactory()
  {
		return (BasicTypeFactory)getEFactoryInstance();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		typeEClass = createEClass(TYPE);

		elementEClass = createEClass(ELEMENT);
		createEReference(elementEClass, ELEMENT__TYPE);

		primitiveTypeEClass = createEClass(PRIMITIVE_TYPE);

		primitiveElementEClass = createEClass(PRIMITIVE_ELEMENT);

		stringEClass = createEClass(STRING);

		booleanEClass = createEClass(BOOLEAN);

		integerEClass = createEClass(INTEGER);

		realEClass = createEClass(REAL);

		charEClass = createEClass(CHAR);

		stringElementEClass = createEClass(STRING_ELEMENT);
		createEAttribute(stringElementEClass, STRING_ELEMENT__VALUE);

		booleanElementEClass = createEClass(BOOLEAN_ELEMENT);
		createEAttribute(booleanElementEClass, BOOLEAN_ELEMENT__VALUE);

		integerElementEClass = createEClass(INTEGER_ELEMENT);
		createEAttribute(integerElementEClass, INTEGER_ELEMENT__VALUE);

		realElementEClass = createEClass(REAL_ELEMENT);
		createEAttribute(realElementEClass, REAL_ELEMENT__VALUE);

		charElementEClass = createEClass(CHAR_ELEMENT);
		createEAttribute(charElementEClass, CHAR_ELEMENT__VALUE);

		recordEClass = createEClass(RECORD);
		createEReference(recordEClass, RECORD__FIELD);

		sequenceTypeEClass = createEClass(SEQUENCE_TYPE);
		createEReference(sequenceTypeEClass, SEQUENCE_TYPE__ELEMENT_TYPE);

		fieldEClass = createEClass(FIELD);
		createEReference(fieldEClass, FIELD__TYPE);

		recordElementEClass = createEClass(RECORD_ELEMENT);
		createEReference(recordElementEClass, RECORD_ELEMENT__BOX);

		boxEClass = createEClass(BOX);
		createEReference(boxEClass, BOX__CONTAINMENT);

		sequenceElementEClass = createEClass(SEQUENCE_ELEMENT);
		createEReference(sequenceElementEClass, SEQUENCE_ELEMENT__FINITE_PART);
		createEReference(sequenceElementEClass, SEQUENCE_ELEMENT__NON_FINITE_PART);

		discreteClockTypeEClass = createEClass(DISCRETE_CLOCK_TYPE);

		denseClockTypeEClass = createEClass(DENSE_CLOCK_TYPE);
		createEAttribute(denseClockTypeEClass, DENSE_CLOCK_TYPE__BASE_UNIT);
		createEAttribute(denseClockTypeEClass, DENSE_CLOCK_TYPE__PHYSICAL_MAGNITUDE);

		enumerationTypeEClass = createEClass(ENUMERATION_TYPE);
		createEAttribute(enumerationTypeEClass, ENUMERATION_TYPE__ENUM_LITERAL);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TimeModelPackage theTimeModelPackage = (TimeModelPackage)EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		ClockExpressionAndRelationPackage theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackage)EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);
		PrimitivesTypeValuePackage thePrimitivesTypeValuePackage = (PrimitivesTypeValuePackage)EPackage.Registry.INSTANCE.getEPackage(PrimitivesTypeValuePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		typeEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		elementEClass.getESuperTypes().add(theClockExpressionAndRelationPackage.getConcreteEntity());
		primitiveTypeEClass.getESuperTypes().add(this.getType());
		primitiveElementEClass.getESuperTypes().add(this.getElement());
		stringEClass.getESuperTypes().add(this.getPrimitiveType());
		booleanEClass.getESuperTypes().add(this.getPrimitiveType());
		integerEClass.getESuperTypes().add(this.getPrimitiveType());
		realEClass.getESuperTypes().add(this.getPrimitiveType());
		charEClass.getESuperTypes().add(this.getPrimitiveType());
		stringElementEClass.getESuperTypes().add(this.getPrimitiveElement());
		booleanElementEClass.getESuperTypes().add(this.getPrimitiveElement());
		integerElementEClass.getESuperTypes().add(this.getPrimitiveElement());
		realElementEClass.getESuperTypes().add(this.getPrimitiveElement());
		charElementEClass.getESuperTypes().add(this.getPrimitiveElement());
		recordEClass.getESuperTypes().add(this.getType());
		sequenceTypeEClass.getESuperTypes().add(this.getType());
		fieldEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		recordElementEClass.getESuperTypes().add(this.getElement());
		sequenceElementEClass.getESuperTypes().add(this.getElement());
		discreteClockTypeEClass.getESuperTypes().add(this.getType());
		denseClockTypeEClass.getESuperTypes().add(this.getType());
		enumerationTypeEClass.getESuperTypes().add(this.getPrimitiveType());

		// Initialize classes and features; add operations and parameters
		initEClass(typeEClass, Type.class, "Type", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(elementEClass, Element.class, "Element", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getElement_Type(), this.getType(), null, "type", null, 0, 1, Element.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(primitiveTypeEClass, PrimitiveType.class, "PrimitiveType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(primitiveElementEClass, PrimitiveElement.class, "PrimitiveElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stringEClass, fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String.class, "String", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(booleanEClass, fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean.class, "Boolean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(integerEClass, fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer.class, "Integer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(realEClass, Real.class, "Real", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(charEClass, Char.class, "Char", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stringElementEClass, StringElement.class, "StringElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringElement_Value(), thePrimitivesTypeValuePackage.getString(), "value", null, 1, 1, StringElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanElementEClass, BooleanElement.class, "BooleanElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanElement_Value(), thePrimitivesTypeValuePackage.getBoolean(), "value", null, 1, 1, BooleanElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerElementEClass, IntegerElement.class, "IntegerElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerElement_Value(), thePrimitivesTypeValuePackage.getInteger(), "value", null, 1, 1, IntegerElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(realElementEClass, RealElement.class, "RealElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRealElement_Value(), thePrimitivesTypeValuePackage.getReal(), "value", null, 1, 1, RealElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(charElementEClass, CharElement.class, "CharElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCharElement_Value(), thePrimitivesTypeValuePackage.getChar(), "value", null, 1, 1, CharElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(recordEClass, fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record.class, "Record", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRecord_Field(), this.getField(), null, "field", null, 1, -1, fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sequenceTypeEClass, SequenceType.class, "SequenceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSequenceType_ElementType(), this.getPrimitiveType(), null, "elementType", null, 1, 1, SequenceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fieldEClass, Field.class, "Field", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getField_Type(), this.getType(), null, "type", null, 1, 1, Field.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(recordElementEClass, RecordElement.class, "RecordElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRecordElement_Box(), this.getBox(), null, "box", null, 1, -1, RecordElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(boxEClass, Box.class, "Box", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBox_Containment(), this.getElement(), null, "containment", null, 1, 1, Box.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sequenceElementEClass, SequenceElement.class, "SequenceElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSequenceElement_FinitePart(), this.getPrimitiveElement(), null, "finitePart", null, 0, -1, SequenceElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSequenceElement_NonFinitePart(), this.getPrimitiveElement(), null, "nonFinitePart", null, 0, -1, SequenceElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(discreteClockTypeEClass, DiscreteClockType.class, "DiscreteClockType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(denseClockTypeEClass, DenseClockType.class, "DenseClockType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDenseClockType_BaseUnit(), thePrimitivesTypeValuePackage.getString(), "baseUnit", null, 0, 1, DenseClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDenseClockType_PhysicalMagnitude(), thePrimitivesTypeValuePackage.getString(), "physicalMagnitude", null, 0, 1, DenseClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(enumerationTypeEClass, EnumerationType.class, "EnumerationType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEnumerationType_EnumLiteral(), thePrimitivesTypeValuePackage.getString(), "enumLiteral", null, 1, -1, EnumerationType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //BasicTypePackageImpl
