/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.*;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage
 * @generated
 */
public class ClassicalExpressionSwitch<T>
{
  /**
	 * The cached model package
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected static ClassicalExpressionPackage modelPackage;

  /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public ClassicalExpressionSwitch()
  {
		if (modelPackage == null) {
			modelPackage = ClassicalExpressionPackage.eINSTANCE;
		}
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  public T doSwitch(EObject theEObject)
  {
		return doSwitch(theEObject.eClass(), theEObject);
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  protected T doSwitch(EClass theEClass, EObject theEObject)
  {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  protected T doSwitch(int classifierID, EObject theEObject)
  {
		switch (classifierID) {
			case ClassicalExpressionPackage.CLASSICAL_EXPRESSION: {
				ClassicalExpression classicalExpression = (ClassicalExpression)theEObject;
				T result = caseClassicalExpression(classicalExpression);
				if (result == null) result = casePrimitiveElement(classicalExpression);
				if (result == null) result = caseElement(classicalExpression);
				if (result == null) result = caseConcreteEntity(classicalExpression);
				if (result == null) result = caseBindableEntity(classicalExpression);
				if (result == null) result = caseNamedElement(classicalExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.BOOLEAN_EXPRESSION: {
				BooleanExpression booleanExpression = (BooleanExpression)theEObject;
				T result = caseBooleanExpression(booleanExpression);
				if (result == null) result = caseClassicalExpression(booleanExpression);
				if (result == null) result = casePrimitiveElement(booleanExpression);
				if (result == null) result = caseElement(booleanExpression);
				if (result == null) result = caseConcreteEntity(booleanExpression);
				if (result == null) result = caseBindableEntity(booleanExpression);
				if (result == null) result = caseNamedElement(booleanExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_EXPRESSION: {
				RealExpression realExpression = (RealExpression)theEObject;
				T result = caseRealExpression(realExpression);
				if (result == null) result = caseClassicalExpression(realExpression);
				if (result == null) result = casePrimitiveElement(realExpression);
				if (result == null) result = caseElement(realExpression);
				if (result == null) result = caseConcreteEntity(realExpression);
				if (result == null) result = caseBindableEntity(realExpression);
				if (result == null) result = caseNamedElement(realExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INTEGER_EXPRESSION: {
				IntegerExpression integerExpression = (IntegerExpression)theEObject;
				T result = caseIntegerExpression(integerExpression);
				if (result == null) result = caseClassicalExpression(integerExpression);
				if (result == null) result = casePrimitiveElement(integerExpression);
				if (result == null) result = caseElement(integerExpression);
				if (result == null) result = caseConcreteEntity(integerExpression);
				if (result == null) result = caseBindableEntity(integerExpression);
				if (result == null) result = caseNamedElement(integerExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION: {
				BinaryBooleanExpression binaryBooleanExpression = (BinaryBooleanExpression)theEObject;
				T result = caseBinaryBooleanExpression(binaryBooleanExpression);
				if (result == null) result = caseBooleanExpression(binaryBooleanExpression);
				if (result == null) result = caseClassicalExpression(binaryBooleanExpression);
				if (result == null) result = casePrimitiveElement(binaryBooleanExpression);
				if (result == null) result = caseElement(binaryBooleanExpression);
				if (result == null) result = caseConcreteEntity(binaryBooleanExpression);
				if (result == null) result = caseBindableEntity(binaryBooleanExpression);
				if (result == null) result = caseNamedElement(binaryBooleanExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.UNARY_BOOLEAN_EXPRESSION: {
				UnaryBooleanExpression unaryBooleanExpression = (UnaryBooleanExpression)theEObject;
				T result = caseUnaryBooleanExpression(unaryBooleanExpression);
				if (result == null) result = caseBooleanExpression(unaryBooleanExpression);
				if (result == null) result = caseClassicalExpression(unaryBooleanExpression);
				if (result == null) result = casePrimitiveElement(unaryBooleanExpression);
				if (result == null) result = caseElement(unaryBooleanExpression);
				if (result == null) result = caseConcreteEntity(unaryBooleanExpression);
				if (result == null) result = caseBindableEntity(unaryBooleanExpression);
				if (result == null) result = caseNamedElement(unaryBooleanExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.BINARY_REAL_EXPRESSION: {
				BinaryRealExpression binaryRealExpression = (BinaryRealExpression)theEObject;
				T result = caseBinaryRealExpression(binaryRealExpression);
				if (result == null) result = caseRealExpression(binaryRealExpression);
				if (result == null) result = caseClassicalExpression(binaryRealExpression);
				if (result == null) result = casePrimitiveElement(binaryRealExpression);
				if (result == null) result = caseElement(binaryRealExpression);
				if (result == null) result = caseConcreteEntity(binaryRealExpression);
				if (result == null) result = caseBindableEntity(binaryRealExpression);
				if (result == null) result = caseNamedElement(binaryRealExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.UNARY_REAL_EXPRESSION: {
				UnaryRealExpression unaryRealExpression = (UnaryRealExpression)theEObject;
				T result = caseUnaryRealExpression(unaryRealExpression);
				if (result == null) result = caseRealExpression(unaryRealExpression);
				if (result == null) result = caseClassicalExpression(unaryRealExpression);
				if (result == null) result = casePrimitiveElement(unaryRealExpression);
				if (result == null) result = caseElement(unaryRealExpression);
				if (result == null) result = caseConcreteEntity(unaryRealExpression);
				if (result == null) result = caseBindableEntity(unaryRealExpression);
				if (result == null) result = caseNamedElement(unaryRealExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.BINARY_INTEGER_EXPRESSION: {
				BinaryIntegerExpression binaryIntegerExpression = (BinaryIntegerExpression)theEObject;
				T result = caseBinaryIntegerExpression(binaryIntegerExpression);
				if (result == null) result = caseIntegerExpression(binaryIntegerExpression);
				if (result == null) result = caseClassicalExpression(binaryIntegerExpression);
				if (result == null) result = casePrimitiveElement(binaryIntegerExpression);
				if (result == null) result = caseElement(binaryIntegerExpression);
				if (result == null) result = caseConcreteEntity(binaryIntegerExpression);
				if (result == null) result = caseBindableEntity(binaryIntegerExpression);
				if (result == null) result = caseNamedElement(binaryIntegerExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.UNARY_INTEGER_EXPRESSION: {
				UnaryIntegerExpression unaryIntegerExpression = (UnaryIntegerExpression)theEObject;
				T result = caseUnaryIntegerExpression(unaryIntegerExpression);
				if (result == null) result = caseIntegerExpression(unaryIntegerExpression);
				if (result == null) result = caseClassicalExpression(unaryIntegerExpression);
				if (result == null) result = casePrimitiveElement(unaryIntegerExpression);
				if (result == null) result = caseElement(unaryIntegerExpression);
				if (result == null) result = caseConcreteEntity(unaryIntegerExpression);
				if (result == null) result = caseBindableEntity(unaryIntegerExpression);
				if (result == null) result = caseNamedElement(unaryIntegerExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.BOOLEAN_REF: {
				BooleanRef booleanRef = (BooleanRef)theEObject;
				T result = caseBooleanRef(booleanRef);
				if (result == null) result = caseBooleanExpression(booleanRef);
				if (result == null) result = caseClassicalExpression(booleanRef);
				if (result == null) result = casePrimitiveElement(booleanRef);
				if (result == null) result = caseElement(booleanRef);
				if (result == null) result = caseConcreteEntity(booleanRef);
				if (result == null) result = caseBindableEntity(booleanRef);
				if (result == null) result = caseNamedElement(booleanRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_REF: {
				RealRef realRef = (RealRef)theEObject;
				T result = caseRealRef(realRef);
				if (result == null) result = caseRealExpression(realRef);
				if (result == null) result = caseClassicalExpression(realRef);
				if (result == null) result = casePrimitiveElement(realRef);
				if (result == null) result = caseElement(realRef);
				if (result == null) result = caseConcreteEntity(realRef);
				if (result == null) result = caseBindableEntity(realRef);
				if (result == null) result = caseNamedElement(realRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INTEGER_REF: {
				IntegerRef integerRef = (IntegerRef)theEObject;
				T result = caseIntegerRef(integerRef);
				if (result == null) result = caseIntegerExpression(integerRef);
				if (result == null) result = caseClassicalExpression(integerRef);
				if (result == null) result = casePrimitiveElement(integerRef);
				if (result == null) result = caseElement(integerRef);
				if (result == null) result = caseConcreteEntity(integerRef);
				if (result == null) result = caseBindableEntity(integerRef);
				if (result == null) result = caseNamedElement(integerRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.UNARY_REAL_PLUS: {
				UnaryRealPlus unaryRealPlus = (UnaryRealPlus)theEObject;
				T result = caseUnaryRealPlus(unaryRealPlus);
				if (result == null) result = caseUnaryRealExpression(unaryRealPlus);
				if (result == null) result = caseRealExpression(unaryRealPlus);
				if (result == null) result = caseClassicalExpression(unaryRealPlus);
				if (result == null) result = casePrimitiveElement(unaryRealPlus);
				if (result == null) result = caseElement(unaryRealPlus);
				if (result == null) result = caseConcreteEntity(unaryRealPlus);
				if (result == null) result = caseBindableEntity(unaryRealPlus);
				if (result == null) result = caseNamedElement(unaryRealPlus);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.UNARY_REAL_MINUS: {
				UnaryRealMinus unaryRealMinus = (UnaryRealMinus)theEObject;
				T result = caseUnaryRealMinus(unaryRealMinus);
				if (result == null) result = caseUnaryRealExpression(unaryRealMinus);
				if (result == null) result = caseRealExpression(unaryRealMinus);
				if (result == null) result = caseClassicalExpression(unaryRealMinus);
				if (result == null) result = casePrimitiveElement(unaryRealMinus);
				if (result == null) result = caseElement(unaryRealMinus);
				if (result == null) result = caseConcreteEntity(unaryRealMinus);
				if (result == null) result = caseBindableEntity(unaryRealMinus);
				if (result == null) result = caseNamedElement(unaryRealMinus);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_PLUS: {
				RealPlus realPlus = (RealPlus)theEObject;
				T result = caseRealPlus(realPlus);
				if (result == null) result = caseBinaryRealExpression(realPlus);
				if (result == null) result = caseRealExpression(realPlus);
				if (result == null) result = caseClassicalExpression(realPlus);
				if (result == null) result = casePrimitiveElement(realPlus);
				if (result == null) result = caseElement(realPlus);
				if (result == null) result = caseConcreteEntity(realPlus);
				if (result == null) result = caseBindableEntity(realPlus);
				if (result == null) result = caseNamedElement(realPlus);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_MINUS: {
				RealMinus realMinus = (RealMinus)theEObject;
				T result = caseRealMinus(realMinus);
				if (result == null) result = caseBinaryRealExpression(realMinus);
				if (result == null) result = caseRealExpression(realMinus);
				if (result == null) result = caseClassicalExpression(realMinus);
				if (result == null) result = casePrimitiveElement(realMinus);
				if (result == null) result = caseElement(realMinus);
				if (result == null) result = caseConcreteEntity(realMinus);
				if (result == null) result = caseBindableEntity(realMinus);
				if (result == null) result = caseNamedElement(realMinus);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_MULTIPLY: {
				RealMultiply realMultiply = (RealMultiply)theEObject;
				T result = caseRealMultiply(realMultiply);
				if (result == null) result = caseBinaryRealExpression(realMultiply);
				if (result == null) result = caseRealExpression(realMultiply);
				if (result == null) result = caseClassicalExpression(realMultiply);
				if (result == null) result = casePrimitiveElement(realMultiply);
				if (result == null) result = caseElement(realMultiply);
				if (result == null) result = caseConcreteEntity(realMultiply);
				if (result == null) result = caseBindableEntity(realMultiply);
				if (result == null) result = caseNamedElement(realMultiply);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.UNARY_INT_PLUS: {
				UnaryIntPlus unaryIntPlus = (UnaryIntPlus)theEObject;
				T result = caseUnaryIntPlus(unaryIntPlus);
				if (result == null) result = caseUnaryIntegerExpression(unaryIntPlus);
				if (result == null) result = caseIntegerExpression(unaryIntPlus);
				if (result == null) result = caseClassicalExpression(unaryIntPlus);
				if (result == null) result = casePrimitiveElement(unaryIntPlus);
				if (result == null) result = caseElement(unaryIntPlus);
				if (result == null) result = caseConcreteEntity(unaryIntPlus);
				if (result == null) result = caseBindableEntity(unaryIntPlus);
				if (result == null) result = caseNamedElement(unaryIntPlus);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.UNARY_INT_MINUS: {
				UnaryIntMinus unaryIntMinus = (UnaryIntMinus)theEObject;
				T result = caseUnaryIntMinus(unaryIntMinus);
				if (result == null) result = caseUnaryIntegerExpression(unaryIntMinus);
				if (result == null) result = caseIntegerExpression(unaryIntMinus);
				if (result == null) result = caseClassicalExpression(unaryIntMinus);
				if (result == null) result = casePrimitiveElement(unaryIntMinus);
				if (result == null) result = caseElement(unaryIntMinus);
				if (result == null) result = caseConcreteEntity(unaryIntMinus);
				if (result == null) result = caseBindableEntity(unaryIntMinus);
				if (result == null) result = caseNamedElement(unaryIntMinus);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INT_PLUS: {
				IntPlus intPlus = (IntPlus)theEObject;
				T result = caseIntPlus(intPlus);
				if (result == null) result = caseBinaryIntegerExpression(intPlus);
				if (result == null) result = caseIntegerExpression(intPlus);
				if (result == null) result = caseClassicalExpression(intPlus);
				if (result == null) result = casePrimitiveElement(intPlus);
				if (result == null) result = caseElement(intPlus);
				if (result == null) result = caseConcreteEntity(intPlus);
				if (result == null) result = caseBindableEntity(intPlus);
				if (result == null) result = caseNamedElement(intPlus);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INT_MINUS: {
				IntMinus intMinus = (IntMinus)theEObject;
				T result = caseIntMinus(intMinus);
				if (result == null) result = caseBinaryIntegerExpression(intMinus);
				if (result == null) result = caseIntegerExpression(intMinus);
				if (result == null) result = caseClassicalExpression(intMinus);
				if (result == null) result = casePrimitiveElement(intMinus);
				if (result == null) result = caseElement(intMinus);
				if (result == null) result = caseConcreteEntity(intMinus);
				if (result == null) result = caseBindableEntity(intMinus);
				if (result == null) result = caseNamedElement(intMinus);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INT_MULTIPLY: {
				IntMultiply intMultiply = (IntMultiply)theEObject;
				T result = caseIntMultiply(intMultiply);
				if (result == null) result = caseBinaryIntegerExpression(intMultiply);
				if (result == null) result = caseIntegerExpression(intMultiply);
				if (result == null) result = caseClassicalExpression(intMultiply);
				if (result == null) result = casePrimitiveElement(intMultiply);
				if (result == null) result = caseElement(intMultiply);
				if (result == null) result = caseConcreteEntity(intMultiply);
				if (result == null) result = caseBindableEntity(intMultiply);
				if (result == null) result = caseNamedElement(intMultiply);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INT_DIVIDE: {
				IntDivide intDivide = (IntDivide)theEObject;
				T result = caseIntDivide(intDivide);
				if (result == null) result = caseBinaryIntegerExpression(intDivide);
				if (result == null) result = caseIntegerExpression(intDivide);
				if (result == null) result = caseClassicalExpression(intDivide);
				if (result == null) result = casePrimitiveElement(intDivide);
				if (result == null) result = caseElement(intDivide);
				if (result == null) result = caseConcreteEntity(intDivide);
				if (result == null) result = caseBindableEntity(intDivide);
				if (result == null) result = caseNamedElement(intDivide);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.NOT: {
				Not not = (Not)theEObject;
				T result = caseNot(not);
				if (result == null) result = caseUnaryBooleanExpression(not);
				if (result == null) result = caseBooleanExpression(not);
				if (result == null) result = caseClassicalExpression(not);
				if (result == null) result = casePrimitiveElement(not);
				if (result == null) result = caseElement(not);
				if (result == null) result = caseConcreteEntity(not);
				if (result == null) result = caseBindableEntity(not);
				if (result == null) result = caseNamedElement(not);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.AND: {
				And and = (And)theEObject;
				T result = caseAnd(and);
				if (result == null) result = caseBinaryBooleanExpression(and);
				if (result == null) result = caseBooleanExpression(and);
				if (result == null) result = caseClassicalExpression(and);
				if (result == null) result = casePrimitiveElement(and);
				if (result == null) result = caseElement(and);
				if (result == null) result = caseConcreteEntity(and);
				if (result == null) result = caseBindableEntity(and);
				if (result == null) result = caseNamedElement(and);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.OR: {
				Or or = (Or)theEObject;
				T result = caseOr(or);
				if (result == null) result = caseBinaryBooleanExpression(or);
				if (result == null) result = caseBooleanExpression(or);
				if (result == null) result = caseClassicalExpression(or);
				if (result == null) result = casePrimitiveElement(or);
				if (result == null) result = caseElement(or);
				if (result == null) result = caseConcreteEntity(or);
				if (result == null) result = caseBindableEntity(or);
				if (result == null) result = caseNamedElement(or);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.XOR: {
				Xor xor = (Xor)theEObject;
				T result = caseXor(xor);
				if (result == null) result = caseBinaryBooleanExpression(xor);
				if (result == null) result = caseBooleanExpression(xor);
				if (result == null) result = caseClassicalExpression(xor);
				if (result == null) result = casePrimitiveElement(xor);
				if (result == null) result = caseElement(xor);
				if (result == null) result = caseConcreteEntity(xor);
				if (result == null) result = caseBindableEntity(xor);
				if (result == null) result = caseNamedElement(xor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_EQUAL: {
				RealEqual realEqual = (RealEqual)theEObject;
				T result = caseRealEqual(realEqual);
				if (result == null) result = caseBooleanExpression(realEqual);
				if (result == null) result = caseClassicalExpression(realEqual);
				if (result == null) result = casePrimitiveElement(realEqual);
				if (result == null) result = caseElement(realEqual);
				if (result == null) result = caseConcreteEntity(realEqual);
				if (result == null) result = caseBindableEntity(realEqual);
				if (result == null) result = caseNamedElement(realEqual);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_INF: {
				RealInf realInf = (RealInf)theEObject;
				T result = caseRealInf(realInf);
				if (result == null) result = caseBooleanExpression(realInf);
				if (result == null) result = caseClassicalExpression(realInf);
				if (result == null) result = casePrimitiveElement(realInf);
				if (result == null) result = caseElement(realInf);
				if (result == null) result = caseConcreteEntity(realInf);
				if (result == null) result = caseBindableEntity(realInf);
				if (result == null) result = caseNamedElement(realInf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_SUP: {
				RealSup realSup = (RealSup)theEObject;
				T result = caseRealSup(realSup);
				if (result == null) result = caseBooleanExpression(realSup);
				if (result == null) result = caseClassicalExpression(realSup);
				if (result == null) result = casePrimitiveElement(realSup);
				if (result == null) result = caseElement(realSup);
				if (result == null) result = caseConcreteEntity(realSup);
				if (result == null) result = caseBindableEntity(realSup);
				if (result == null) result = caseNamedElement(realSup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INT_EQUAL: {
				IntEqual intEqual = (IntEqual)theEObject;
				T result = caseIntEqual(intEqual);
				if (result == null) result = caseBooleanExpression(intEqual);
				if (result == null) result = caseClassicalExpression(intEqual);
				if (result == null) result = casePrimitiveElement(intEqual);
				if (result == null) result = caseElement(intEqual);
				if (result == null) result = caseConcreteEntity(intEqual);
				if (result == null) result = caseBindableEntity(intEqual);
				if (result == null) result = caseNamedElement(intEqual);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INT_INF: {
				IntInf intInf = (IntInf)theEObject;
				T result = caseIntInf(intInf);
				if (result == null) result = caseBooleanExpression(intInf);
				if (result == null) result = caseClassicalExpression(intInf);
				if (result == null) result = casePrimitiveElement(intInf);
				if (result == null) result = caseElement(intInf);
				if (result == null) result = caseConcreteEntity(intInf);
				if (result == null) result = caseBindableEntity(intInf);
				if (result == null) result = caseNamedElement(intInf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INT_SUP: {
				IntSup intSup = (IntSup)theEObject;
				T result = caseIntSup(intSup);
				if (result == null) result = caseBooleanExpression(intSup);
				if (result == null) result = caseClassicalExpression(intSup);
				if (result == null) result = casePrimitiveElement(intSup);
				if (result == null) result = caseElement(intSup);
				if (result == null) result = caseConcreteEntity(intSup);
				if (result == null) result = caseBindableEntity(intSup);
				if (result == null) result = caseNamedElement(intSup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.SEQ_IS_EMPTY: {
				SeqIsEmpty seqIsEmpty = (SeqIsEmpty)theEObject;
				T result = caseSeqIsEmpty(seqIsEmpty);
				if (result == null) result = caseBooleanExpression(seqIsEmpty);
				if (result == null) result = caseClassicalExpression(seqIsEmpty);
				if (result == null) result = casePrimitiveElement(seqIsEmpty);
				if (result == null) result = caseElement(seqIsEmpty);
				if (result == null) result = caseConcreteEntity(seqIsEmpty);
				if (result == null) result = caseBindableEntity(seqIsEmpty);
				if (result == null) result = caseNamedElement(seqIsEmpty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.UNARY_SEQ_EXPRESSION: {
				UnarySeqExpression unarySeqExpression = (UnarySeqExpression)theEObject;
				T result = caseUnarySeqExpression(unarySeqExpression);
				if (result == null) result = caseSeqExpression(unarySeqExpression);
				if (result == null) result = caseClassicalExpression(unarySeqExpression);
				if (result == null) result = casePrimitiveElement(unarySeqExpression);
				if (result == null) result = caseElement(unarySeqExpression);
				if (result == null) result = caseConcreteEntity(unarySeqExpression);
				if (result == null) result = caseBindableEntity(unarySeqExpression);
				if (result == null) result = caseNamedElement(unarySeqExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.SEQ_GET_TAIL: {
				SeqGetTail seqGetTail = (SeqGetTail)theEObject;
				T result = caseSeqGetTail(seqGetTail);
				if (result == null) result = caseUnarySeqExpression(seqGetTail);
				if (result == null) result = caseSeqExpression(seqGetTail);
				if (result == null) result = caseClassicalExpression(seqGetTail);
				if (result == null) result = casePrimitiveElement(seqGetTail);
				if (result == null) result = caseElement(seqGetTail);
				if (result == null) result = caseConcreteEntity(seqGetTail);
				if (result == null) result = caseBindableEntity(seqGetTail);
				if (result == null) result = caseNamedElement(seqGetTail);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.SEQ_GET_HEAD: {
				SeqGetHead seqGetHead = (SeqGetHead)theEObject;
				T result = caseSeqGetHead(seqGetHead);
				if (result == null) result = caseIntegerExpression(seqGetHead);
				if (result == null) result = caseClassicalExpression(seqGetHead);
				if (result == null) result = casePrimitiveElement(seqGetHead);
				if (result == null) result = caseElement(seqGetHead);
				if (result == null) result = caseConcreteEntity(seqGetHead);
				if (result == null) result = caseBindableEntity(seqGetHead);
				if (result == null) result = caseNamedElement(seqGetHead);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.SEQ_DECR: {
				SeqDecr seqDecr = (SeqDecr)theEObject;
				T result = caseSeqDecr(seqDecr);
				if (result == null) result = caseUnarySeqExpression(seqDecr);
				if (result == null) result = caseSeqExpression(seqDecr);
				if (result == null) result = caseClassicalExpression(seqDecr);
				if (result == null) result = casePrimitiveElement(seqDecr);
				if (result == null) result = caseElement(seqDecr);
				if (result == null) result = caseConcreteEntity(seqDecr);
				if (result == null) result = caseBindableEntity(seqDecr);
				if (result == null) result = caseNamedElement(seqDecr);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.SEQ_SCHED: {
				SeqSched seqSched = (SeqSched)theEObject;
				T result = caseSeqSched(seqSched);
				if (result == null) result = caseUnarySeqExpression(seqSched);
				if (result == null) result = caseSeqExpression(seqSched);
				if (result == null) result = caseClassicalExpression(seqSched);
				if (result == null) result = casePrimitiveElement(seqSched);
				if (result == null) result = caseElement(seqSched);
				if (result == null) result = caseConcreteEntity(seqSched);
				if (result == null) result = caseBindableEntity(seqSched);
				if (result == null) result = caseNamedElement(seqSched);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.BOOLEAN_VARIABLE_REF: {
				BooleanVariableRef booleanVariableRef = (BooleanVariableRef)theEObject;
				T result = caseBooleanVariableRef(booleanVariableRef);
				if (result == null) result = caseBooleanExpression(booleanVariableRef);
				if (result == null) result = caseClassicalExpression(booleanVariableRef);
				if (result == null) result = casePrimitiveElement(booleanVariableRef);
				if (result == null) result = caseElement(booleanVariableRef);
				if (result == null) result = caseConcreteEntity(booleanVariableRef);
				if (result == null) result = caseBindableEntity(booleanVariableRef);
				if (result == null) result = caseNamedElement(booleanVariableRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.INTEGER_VARIABLE_REF: {
				IntegerVariableRef integerVariableRef = (IntegerVariableRef)theEObject;
				T result = caseIntegerVariableRef(integerVariableRef);
				if (result == null) result = caseIntegerExpression(integerVariableRef);
				if (result == null) result = caseClassicalExpression(integerVariableRef);
				if (result == null) result = casePrimitiveElement(integerVariableRef);
				if (result == null) result = caseElement(integerVariableRef);
				if (result == null) result = caseConcreteEntity(integerVariableRef);
				if (result == null) result = caseBindableEntity(integerVariableRef);
				if (result == null) result = caseNamedElement(integerVariableRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.REAL_VARIABLE_REF: {
				RealVariableRef realVariableRef = (RealVariableRef)theEObject;
				T result = caseRealVariableRef(realVariableRef);
				if (result == null) result = caseRealExpression(realVariableRef);
				if (result == null) result = caseClassicalExpression(realVariableRef);
				if (result == null) result = casePrimitiveElement(realVariableRef);
				if (result == null) result = caseElement(realVariableRef);
				if (result == null) result = caseConcreteEntity(realVariableRef);
				if (result == null) result = caseBindableEntity(realVariableRef);
				if (result == null) result = caseNamedElement(realVariableRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.NUMBER_SEQ_REF: {
				NumberSeqRef numberSeqRef = (NumberSeqRef)theEObject;
				T result = caseNumberSeqRef(numberSeqRef);
				if (result == null) result = caseSeqExpression(numberSeqRef);
				if (result == null) result = caseClassicalExpression(numberSeqRef);
				if (result == null) result = casePrimitiveElement(numberSeqRef);
				if (result == null) result = caseElement(numberSeqRef);
				if (result == null) result = caseConcreteEntity(numberSeqRef);
				if (result == null) result = caseBindableEntity(numberSeqRef);
				if (result == null) result = caseNamedElement(numberSeqRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.NUMBER_SEQ_VARIABLE_REF: {
				NumberSeqVariableRef numberSeqVariableRef = (NumberSeqVariableRef)theEObject;
				T result = caseNumberSeqVariableRef(numberSeqVariableRef);
				if (result == null) result = caseSeqExpression(numberSeqVariableRef);
				if (result == null) result = caseClassicalExpression(numberSeqVariableRef);
				if (result == null) result = casePrimitiveElement(numberSeqVariableRef);
				if (result == null) result = caseElement(numberSeqVariableRef);
				if (result == null) result = caseConcreteEntity(numberSeqVariableRef);
				if (result == null) result = caseBindableEntity(numberSeqVariableRef);
				if (result == null) result = caseNamedElement(numberSeqVariableRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassicalExpressionPackage.SEQ_EXPRESSION: {
				SeqExpression seqExpression = (SeqExpression)theEObject;
				T result = caseSeqExpression(seqExpression);
				if (result == null) result = caseClassicalExpression(seqExpression);
				if (result == null) result = casePrimitiveElement(seqExpression);
				if (result == null) result = caseElement(seqExpression);
				if (result == null) result = caseConcreteEntity(seqExpression);
				if (result == null) result = caseBindableEntity(seqExpression);
				if (result == null) result = caseNamedElement(seqExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Classical Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Classical Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseClassicalExpression(ClassicalExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBooleanExpression(BooleanExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealExpression(RealExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntegerExpression(IntegerExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Boolean Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Boolean Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBinaryBooleanExpression(BinaryBooleanExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Boolean Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Boolean Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnaryBooleanExpression(UnaryBooleanExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Real Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Real Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBinaryRealExpression(BinaryRealExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Real Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Real Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnaryRealExpression(UnaryRealExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Integer Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Integer Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBinaryIntegerExpression(BinaryIntegerExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Integer Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Integer Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnaryIntegerExpression(UnaryIntegerExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Ref</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBooleanRef(BooleanRef object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Ref</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealRef(RealRef object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Ref</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntegerRef(IntegerRef object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Real Plus</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Real Plus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnaryRealPlus(UnaryRealPlus object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Real Minus</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Real Minus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnaryRealMinus(UnaryRealMinus object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Plus</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Plus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealPlus(RealPlus object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Minus</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Minus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealMinus(RealMinus object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Multiply</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Multiply</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealMultiply(RealMultiply object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Int Plus</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Int Plus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnaryIntPlus(UnaryIntPlus object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Int Minus</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Int Minus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnaryIntMinus(UnaryIntMinus object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Int Plus</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Plus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntPlus(IntPlus object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Int Minus</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Minus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntMinus(IntMinus object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Int Multiply</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Multiply</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntMultiply(IntMultiply object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Int Divide</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Divide</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntDivide(IntDivide object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Not</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNot(Not object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>And</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseAnd(And object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Or</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseOr(Or object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Xor</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Xor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseXor(Xor object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Equal</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Equal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealEqual(RealEqual object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Inf</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Inf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealInf(RealInf object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Sup</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Sup</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealSup(RealSup object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Int Equal</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Equal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntEqual(IntEqual object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Int Inf</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Inf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntInf(IntInf object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Int Sup</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Sup</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntSup(IntSup object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Seq Is Empty</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Seq Is Empty</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSeqIsEmpty(SeqIsEmpty object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Seq Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Seq Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnarySeqExpression(UnarySeqExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Seq Get Tail</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Seq Get Tail</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSeqGetTail(SeqGetTail object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Seq Get Head</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Seq Get Head</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSeqGetHead(SeqGetHead object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Seq Decr</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Seq Decr</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSeqDecr(SeqDecr object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Seq Sched</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Seq Sched</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSeqSched(SeqSched object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Variable Ref</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Variable Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBooleanVariableRef(BooleanVariableRef object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Variable Ref</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Variable Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntegerVariableRef(IntegerVariableRef object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Real Variable Ref</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Variable Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseRealVariableRef(RealVariableRef object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Number Seq Ref</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Seq Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNumberSeqRef(NumberSeqRef object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Number Seq Variable Ref</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Seq Variable Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNumberSeqVariableRef(NumberSeqVariableRef object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Seq Expression</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Seq Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSeqExpression(SeqExpression object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNamedElement(NamedElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Bindable Entity</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bindable Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseBindableEntity(BindableEntity object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Concrete Entity</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concrete Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseConcreteEntity(ConcreteEntity object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseElement(Element object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T casePrimitiveElement(PrimitiveElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
  public T defaultCase(EObject object)
  {
		return null;
	}

} //ClassicalExpressionSwitch
