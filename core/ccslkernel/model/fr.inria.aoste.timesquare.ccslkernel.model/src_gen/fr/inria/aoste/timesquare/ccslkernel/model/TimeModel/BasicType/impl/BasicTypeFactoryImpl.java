/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Char;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Real;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BasicTypeFactoryImpl extends EFactoryImpl implements BasicTypeFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public static BasicTypeFactory init()
  {
		try {
			BasicTypeFactory theBasicTypeFactory = (BasicTypeFactory)EPackage.Registry.INSTANCE.getEFactory(BasicTypePackage.eNS_URI);
			if (theBasicTypeFactory != null) {
				return theBasicTypeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BasicTypeFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public BasicTypeFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case BasicTypePackage.STRING: return createString();
			case BasicTypePackage.BOOLEAN: return createBoolean();
			case BasicTypePackage.INTEGER: return createInteger();
			case BasicTypePackage.REAL: return createReal();
			case BasicTypePackage.CHAR: return createChar();
			case BasicTypePackage.STRING_ELEMENT: return createStringElement();
			case BasicTypePackage.BOOLEAN_ELEMENT: return createBooleanElement();
			case BasicTypePackage.INTEGER_ELEMENT: return createIntegerElement();
			case BasicTypePackage.REAL_ELEMENT: return createRealElement();
			case BasicTypePackage.CHAR_ELEMENT: return createCharElement();
			case BasicTypePackage.RECORD: return createRecord();
			case BasicTypePackage.SEQUENCE_TYPE: return createSequenceType();
			case BasicTypePackage.FIELD: return createField();
			case BasicTypePackage.RECORD_ELEMENT: return createRecordElement();
			case BasicTypePackage.BOX: return createBox();
			case BasicTypePackage.SEQUENCE_ELEMENT: return createSequenceElement();
			case BasicTypePackage.DISCRETE_CLOCK_TYPE: return createDiscreteClockType();
			case BasicTypePackage.DENSE_CLOCK_TYPE: return createDenseClockType();
			case BasicTypePackage.ENUMERATION_TYPE: return createEnumerationType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String createString()
  {
		StringImpl string = new StringImpl();
		return string;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean createBoolean()
  {
		BooleanImpl boolean_ = new BooleanImpl();
		return boolean_;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer createInteger()
  {
		IntegerImpl integer = new IntegerImpl();
		return integer;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Real createReal()
  {
		RealImpl real = new RealImpl();
		return real;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Char createChar()
  {
		CharImpl char_ = new CharImpl();
		return char_;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public StringElement createStringElement()
  {
		StringElementImpl stringElement = new StringElementImpl();
		return stringElement;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public BooleanElement createBooleanElement()
  {
		BooleanElementImpl booleanElement = new BooleanElementImpl();
		return booleanElement;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntegerElement createIntegerElement()
  {
		IntegerElementImpl integerElement = new IntegerElementImpl();
		return integerElement;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealElement createRealElement()
  {
		RealElementImpl realElement = new RealElementImpl();
		return realElement;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public CharElement createCharElement()
  {
		CharElementImpl charElement = new CharElementImpl();
		return charElement;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record createRecord()
  {
		RecordImpl record = new RecordImpl();
		return record;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SequenceType createSequenceType()
  {
		SequenceTypeImpl sequenceType = new SequenceTypeImpl();
		return sequenceType;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Field createField()
  {
		FieldImpl field = new FieldImpl();
		return field;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RecordElement createRecordElement()
  {
		RecordElementImpl recordElement = new RecordElementImpl();
		return recordElement;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Box createBox()
  {
		BoxImpl box = new BoxImpl();
		return box;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SequenceElement createSequenceElement()
  {
		SequenceElementImpl sequenceElement = new SequenceElementImpl();
		return sequenceElement;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public DiscreteClockType createDiscreteClockType()
  {
		DiscreteClockTypeImpl discreteClockType = new DiscreteClockTypeImpl();
		return discreteClockType;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public DenseClockType createDenseClockType()
  {
		DenseClockTypeImpl denseClockType = new DenseClockTypeImpl();
		return denseClockType;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EnumerationType createEnumerationType()
  {
		EnumerationTypeImpl enumerationType = new EnumerationTypeImpl();
		return enumerationType;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public BasicTypePackage getBasicTypePackage()
  {
		return (BasicTypePackage)getEPackage();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static BasicTypePackage getPackage()
  {
		return BasicTypePackage.eINSTANCE;
	}

} //BasicTypeFactoryImpl
