/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity#getType <em>Type</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity#getDesiredEventKind <em>Desired Event Kind</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage#getAbstractEntity()
 * @model
 * @generated
 */
public interface AbstractEntity extends BindableEntity
{
  /**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage#getAbstractEntity_Type()
	 * @model required="true"
	 * @generated
	 */
  Type getType();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
  void setType(Type value);

		/**
	 * Returns the value of the '<em><b>Desired Event Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Desired Event Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Desired Event Kind</em>' attribute.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind
	 * @see #setDesiredEventKind(EventKind)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage#getAbstractEntity_DesiredEventKind()
	 * @model
	 * @generated
	 */
	EventKind getDesiredEventKind();

		/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity#getDesiredEventKind <em>Desired Event Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Desired Event Kind</em>' attribute.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind
	 * @see #getDesiredEventKind()
	 * @generated
	 */
	void setDesiredEventKind(EventKind value);

} // AbstractEntity
