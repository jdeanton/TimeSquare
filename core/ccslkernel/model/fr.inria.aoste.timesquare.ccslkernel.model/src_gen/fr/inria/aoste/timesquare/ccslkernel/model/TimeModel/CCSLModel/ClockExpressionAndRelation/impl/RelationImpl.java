/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationImpl#getType <em>Type</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationImpl#getBindings <em>Bindings</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationImpl#getIsAnAssertion <em>Is An Assertion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RelationImpl extends ConcreteEntityImpl implements Relation
{
  /**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
  protected RelationDeclaration type;

  /**
	 * The cached value of the '{@link #getBindings() <em>Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getBindings()
	 * @generated
	 * @ordered
	 */
  protected EList<Binding> bindings;

  /**
	 * The default value of the '{@link #getIsAnAssertion() <em>Is An Assertion</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getIsAnAssertion()
	 * @generated
	 * @ordered
	 */
  protected static final Boolean IS_AN_ASSERTION_EDEFAULT = Boolean.FALSE;

  /**
	 * The cached value of the '{@link #getIsAnAssertion() <em>Is An Assertion</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getIsAnAssertion()
	 * @generated
	 * @ordered
	 */
  protected Boolean isAnAssertion = IS_AN_ASSERTION_EDEFAULT;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected RelationImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClockExpressionAndRelationPackage.Literals.RELATION;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RelationDeclaration getType()
  {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (RelationDeclaration)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClockExpressionAndRelationPackage.RELATION__TYPE, oldType, type));
			}
		}
		return type;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public RelationDeclaration basicGetType()
  {
		return type;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setType(RelationDeclaration newType)
  {
		RelationDeclaration oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClockExpressionAndRelationPackage.RELATION__TYPE, oldType, type));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Binding> getBindings()
  {
		if (bindings == null) {
			bindings = new EObjectContainmentEList<Binding>(Binding.class, this, ClockExpressionAndRelationPackage.RELATION__BINDINGS);
		}
		return bindings;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Boolean getIsAnAssertion()
  {
		return isAnAssertion;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setIsAnAssertion(Boolean newIsAnAssertion)
  {
		Boolean oldIsAnAssertion = isAnAssertion;
		isAnAssertion = newIsAnAssertion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClockExpressionAndRelationPackage.RELATION__IS_AN_ASSERTION, oldIsAnAssertion, isAnAssertion));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION__BINDINGS:
				return ((InternalEList<?>)getBindings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case ClockExpressionAndRelationPackage.RELATION__BINDINGS:
				return getBindings();
			case ClockExpressionAndRelationPackage.RELATION__IS_AN_ASSERTION:
				return getIsAnAssertion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION__TYPE:
				setType((RelationDeclaration)newValue);
				return;
			case ClockExpressionAndRelationPackage.RELATION__BINDINGS:
				getBindings().clear();
				getBindings().addAll((Collection<? extends Binding>)newValue);
				return;
			case ClockExpressionAndRelationPackage.RELATION__IS_AN_ASSERTION:
				setIsAnAssertion((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION__TYPE:
				setType((RelationDeclaration)null);
				return;
			case ClockExpressionAndRelationPackage.RELATION__BINDINGS:
				getBindings().clear();
				return;
			case ClockExpressionAndRelationPackage.RELATION__IS_AN_ASSERTION:
				setIsAnAssertion(IS_AN_ASSERTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION__TYPE:
				return type != null;
			case ClockExpressionAndRelationPackage.RELATION__BINDINGS:
				return bindings != null && !bindings.isEmpty();
			case ClockExpressionAndRelationPackage.RELATION__IS_AN_ASSERTION:
				return IS_AN_ASSERTION_EDEFAULT == null ? isAnAssertion != null : !IS_AN_ASSERTION_EDEFAULT.equals(isAnAssertion);
		}
		return super.eIsSet(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public String toString()
  {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (isAnAssertion: ");
		result.append(isAnAssertion);
		result.append(')');
		return result.toString();
	}

} //RelationImpl
