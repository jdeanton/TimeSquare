/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Up To</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UpToImpl#getClockToFollow <em>Clock To Follow</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UpToImpl#getKillerClock <em>Killer Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UpToImpl#isIsPreemptive <em>Is Preemptive</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UpToImpl extends KernelExpressionDeclarationImpl implements UpTo
{
  /**
	 * The cached value of the '{@link #getClockToFollow() <em>Clock To Follow</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockToFollow()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity clockToFollow;
	/**
	 * The cached value of the '{@link #getKillerClock() <em>Killer Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKillerClock()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity killerClock;

		/**
	 * The default value of the '{@link #isIsPreemptive() <em>Is Preemptive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsPreemptive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_PREEMPTIVE_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isIsPreemptive() <em>Is Preemptive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsPreemptive()
	 * @generated
	 * @ordered
	 */
	protected boolean isPreemptive = IS_PREEMPTIVE_EDEFAULT;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected UpToImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return KernelExpressionPackage.Literals.UP_TO;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getClockToFollow() {
		return clockToFollow;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClockToFollow(AbstractEntity newClockToFollow, NotificationChain msgs) {
		AbstractEntity oldClockToFollow = clockToFollow;
		clockToFollow = newClockToFollow;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW, oldClockToFollow, newClockToFollow);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setClockToFollow(AbstractEntity newClockToFollow) {
		if (newClockToFollow != clockToFollow) {
			NotificationChain msgs = null;
			if (clockToFollow != null)
				msgs = ((InternalEObject)clockToFollow).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW, null, msgs);
			if (newClockToFollow != null)
				msgs = ((InternalEObject)newClockToFollow).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW, null, msgs);
			msgs = basicSetClockToFollow(newClockToFollow, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW, newClockToFollow, newClockToFollow));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getKillerClock() {
		return killerClock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKillerClock(AbstractEntity newKillerClock, NotificationChain msgs) {
		AbstractEntity oldKillerClock = killerClock;
		killerClock = newKillerClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UP_TO__KILLER_CLOCK, oldKillerClock, newKillerClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKillerClock(AbstractEntity newKillerClock) {
		if (newKillerClock != killerClock) {
			NotificationChain msgs = null;
			if (killerClock != null)
				msgs = ((InternalEObject)killerClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.UP_TO__KILLER_CLOCK, null, msgs);
			if (newKillerClock != null)
				msgs = ((InternalEObject)newKillerClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.UP_TO__KILLER_CLOCK, null, msgs);
			msgs = basicSetKillerClock(newKillerClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UP_TO__KILLER_CLOCK, newKillerClock, newKillerClock));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isIsPreemptive() {
		return isPreemptive;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIsPreemptive(boolean newIsPreemptive) {
		boolean oldIsPreemptive = isPreemptive;
		isPreemptive = newIsPreemptive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UP_TO__IS_PREEMPTIVE, oldIsPreemptive, isPreemptive));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW:
				return basicSetClockToFollow(null, msgs);
			case KernelExpressionPackage.UP_TO__KILLER_CLOCK:
				return basicSetKillerClock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW:
				return getClockToFollow();
			case KernelExpressionPackage.UP_TO__KILLER_CLOCK:
				return getKillerClock();
			case KernelExpressionPackage.UP_TO__IS_PREEMPTIVE:
				return isIsPreemptive();
		}
		return super.eGet(featureID, resolve, coreType);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW:
				setClockToFollow((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.UP_TO__KILLER_CLOCK:
				setKillerClock((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.UP_TO__IS_PREEMPTIVE:
				setIsPreemptive((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW:
				setClockToFollow((AbstractEntity)null);
				return;
			case KernelExpressionPackage.UP_TO__KILLER_CLOCK:
				setKillerClock((AbstractEntity)null);
				return;
			case KernelExpressionPackage.UP_TO__IS_PREEMPTIVE:
				setIsPreemptive(IS_PREEMPTIVE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.UP_TO__CLOCK_TO_FOLLOW:
				return clockToFollow != null;
			case KernelExpressionPackage.UP_TO__KILLER_CLOCK:
				return killerClock != null;
			case KernelExpressionPackage.UP_TO__IS_PREEMPTIVE:
				return isPreemptive != IS_PREEMPTIVE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (isPreemptive: ");
		result.append(isPreemptive);
		result.append(')');
		return result.toString();
	}

} //UpToImpl
