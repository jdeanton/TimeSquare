/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import java.lang.String;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory
 * @model kind="package"
 * @generated
 */
public interface BasicTypePackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNAME = "BasicType";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_URI = "http://fr.inria.aoste.timemodel.basicTypes";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_PREFIX = "fr.inria.aoste.timemodel.basicTypes";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  BasicTypePackage eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl.init();

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.TypeImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getType()
	 * @generated
	 */
  int TYPE = 0;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int TYPE__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int TYPE_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.ElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.ElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getElement()
	 * @generated
	 */
  int ELEMENT = 1;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ELEMENT__NAME = ClockExpressionAndRelationPackage.CONCRETE_ENTITY__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ELEMENT__TYPE = ClockExpressionAndRelationPackage.CONCRETE_ENTITY_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ELEMENT_FEATURE_COUNT = ClockExpressionAndRelationPackage.CONCRETE_ENTITY_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.PrimitiveTypeImpl <em>Primitive Type</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.PrimitiveTypeImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getPrimitiveType()
	 * @generated
	 */
  int PRIMITIVE_TYPE = 2;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PRIMITIVE_TYPE__NAME = TYPE__NAME;

  /**
	 * The number of structural features of the '<em>Primitive Type</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PRIMITIVE_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.PrimitiveElementImpl <em>Primitive Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.PrimitiveElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getPrimitiveElement()
	 * @generated
	 */
  int PRIMITIVE_ELEMENT = 3;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PRIMITIVE_ELEMENT__NAME = ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PRIMITIVE_ELEMENT__TYPE = ELEMENT__TYPE;

  /**
	 * The number of structural features of the '<em>Primitive Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int PRIMITIVE_ELEMENT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.StringImpl <em>String</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.StringImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getString()
	 * @generated
	 */
  int STRING = 4;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRING__NAME = PRIMITIVE_TYPE__NAME;

  /**
	 * The number of structural features of the '<em>String</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRING_FEATURE_COUNT = PRIMITIVE_TYPE_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BooleanImpl <em>Boolean</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BooleanImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getBoolean()
	 * @generated
	 */
  int BOOLEAN = 5;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN__NAME = PRIMITIVE_TYPE__NAME;

  /**
	 * The number of structural features of the '<em>Boolean</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_FEATURE_COUNT = PRIMITIVE_TYPE_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.IntegerImpl <em>Integer</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.IntegerImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getInteger()
	 * @generated
	 */
  int INTEGER = 6;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER__NAME = PRIMITIVE_TYPE__NAME;

  /**
	 * The number of structural features of the '<em>Integer</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_FEATURE_COUNT = PRIMITIVE_TYPE_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RealImpl <em>Real</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RealImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getReal()
	 * @generated
	 */
  int REAL = 7;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL__NAME = PRIMITIVE_TYPE__NAME;

  /**
	 * The number of structural features of the '<em>Real</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_FEATURE_COUNT = PRIMITIVE_TYPE_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.CharImpl <em>Char</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.CharImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getChar()
	 * @generated
	 */
  int CHAR = 8;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CHAR__NAME = PRIMITIVE_TYPE__NAME;

  /**
	 * The number of structural features of the '<em>Char</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CHAR_FEATURE_COUNT = PRIMITIVE_TYPE_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.StringElementImpl <em>String Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.StringElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getStringElement()
	 * @generated
	 */
  int STRING_ELEMENT = 9;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRING_ELEMENT__NAME = PRIMITIVE_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRING_ELEMENT__TYPE = PRIMITIVE_ELEMENT__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRING_ELEMENT__VALUE = PRIMITIVE_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>String Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRING_ELEMENT_FEATURE_COUNT = PRIMITIVE_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BooleanElementImpl <em>Boolean Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BooleanElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getBooleanElement()
	 * @generated
	 */
  int BOOLEAN_ELEMENT = 10;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_ELEMENT__NAME = PRIMITIVE_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_ELEMENT__TYPE = PRIMITIVE_ELEMENT__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_ELEMENT__VALUE = PRIMITIVE_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Boolean Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOOLEAN_ELEMENT_FEATURE_COUNT = PRIMITIVE_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.IntegerElementImpl <em>Integer Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.IntegerElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getIntegerElement()
	 * @generated
	 */
  int INTEGER_ELEMENT = 11;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_ELEMENT__NAME = PRIMITIVE_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_ELEMENT__TYPE = PRIMITIVE_ELEMENT__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_ELEMENT__VALUE = PRIMITIVE_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Integer Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTEGER_ELEMENT_FEATURE_COUNT = PRIMITIVE_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RealElementImpl <em>Real Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RealElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getRealElement()
	 * @generated
	 */
  int REAL_ELEMENT = 12;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_ELEMENT__NAME = PRIMITIVE_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_ELEMENT__TYPE = PRIMITIVE_ELEMENT__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_ELEMENT__VALUE = PRIMITIVE_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Real Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int REAL_ELEMENT_FEATURE_COUNT = PRIMITIVE_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.CharElementImpl <em>Char Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.CharElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getCharElement()
	 * @generated
	 */
  int CHAR_ELEMENT = 13;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CHAR_ELEMENT__NAME = PRIMITIVE_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CHAR_ELEMENT__TYPE = PRIMITIVE_ELEMENT__TYPE;

  /**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CHAR_ELEMENT__VALUE = PRIMITIVE_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Char Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CHAR_ELEMENT_FEATURE_COUNT = PRIMITIVE_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RecordImpl <em>Record</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RecordImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getRecord()
	 * @generated
	 */
  int RECORD = 14;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RECORD__NAME = TYPE__NAME;

  /**
	 * The feature id for the '<em><b>Field</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RECORD__FIELD = TYPE_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Record</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RECORD_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceTypeImpl <em>Sequence Type</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceTypeImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getSequenceType()
	 * @generated
	 */
  int SEQUENCE_TYPE = 15;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQUENCE_TYPE__NAME = TYPE__NAME;

  /**
	 * The feature id for the '<em><b>Element Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQUENCE_TYPE__ELEMENT_TYPE = TYPE_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Sequence Type</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQUENCE_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.FieldImpl <em>Field</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.FieldImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getField()
	 * @generated
	 */
  int FIELD = 16;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int FIELD__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int FIELD__TYPE = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int FIELD_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RecordElementImpl <em>Record Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RecordElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getRecordElement()
	 * @generated
	 */
  int RECORD_ELEMENT = 17;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RECORD_ELEMENT__NAME = ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RECORD_ELEMENT__TYPE = ELEMENT__TYPE;

  /**
	 * The feature id for the '<em><b>Box</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RECORD_ELEMENT__BOX = ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Record Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int RECORD_ELEMENT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BoxImpl <em>Box</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BoxImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getBox()
	 * @generated
	 */
  int BOX = 18;

  /**
	 * The feature id for the '<em><b>Containment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOX__CONTAINMENT = 0;

  /**
	 * The number of structural features of the '<em>Box</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BOX_FEATURE_COUNT = 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceElementImpl <em>Sequence Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getSequenceElement()
	 * @generated
	 */
  int SEQUENCE_ELEMENT = 19;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQUENCE_ELEMENT__NAME = ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQUENCE_ELEMENT__TYPE = ELEMENT__TYPE;

  /**
	 * The feature id for the '<em><b>Finite Part</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQUENCE_ELEMENT__FINITE_PART = ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Non Finite Part</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQUENCE_ELEMENT__NON_FINITE_PART = ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Sequence Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SEQUENCE_ELEMENT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DiscreteClockTypeImpl <em>Discrete Clock Type</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DiscreteClockTypeImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getDiscreteClockType()
	 * @generated
	 */
  int DISCRETE_CLOCK_TYPE = 20;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DISCRETE_CLOCK_TYPE__NAME = TYPE__NAME;

  /**
	 * The number of structural features of the '<em>Discrete Clock Type</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DISCRETE_CLOCK_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DenseClockTypeImpl <em>Dense Clock Type</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DenseClockTypeImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getDenseClockType()
	 * @generated
	 */
  int DENSE_CLOCK_TYPE = 21;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DENSE_CLOCK_TYPE__NAME = TYPE__NAME;

  /**
	 * The feature id for the '<em><b>Base Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DENSE_CLOCK_TYPE__BASE_UNIT = TYPE_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Physical Magnitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DENSE_CLOCK_TYPE__PHYSICAL_MAGNITUDE = TYPE_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Dense Clock Type</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DENSE_CLOCK_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.EnumerationTypeImpl <em>Enumeration Type</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.EnumerationTypeImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getEnumerationType()
	 * @generated
	 */
  int ENUMERATION_TYPE = 22;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ENUMERATION_TYPE__NAME = PRIMITIVE_TYPE__NAME;

  /**
	 * The feature id for the '<em><b>Enum Literal</b></em>' attribute list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ENUMERATION_TYPE__ENUM_LITERAL = PRIMITIVE_TYPE_FEATURE_COUNT + 0;

  /**
	 * The number of structural features of the '<em>Enumeration Type</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int ENUMERATION_TYPE_FEATURE_COUNT = PRIMITIVE_TYPE_FEATURE_COUNT + 1;


  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type
	 * @generated
	 */
  EClass getType();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element
	 * @generated
	 */
  EClass getElement();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element#getType()
	 * @see #getElement()
	 * @generated
	 */
  EReference getElement_Type();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveType <em>Primitive Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveType
	 * @generated
	 */
  EClass getPrimitiveType();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement <em>Primitive Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement
	 * @generated
	 */
  EClass getPrimitiveElement();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.String
	 * @generated
	 */
  EClass getString();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean <em>Boolean</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Boolean
	 * @generated
	 */
  EClass getBoolean();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer <em>Integer</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer
	 * @generated
	 */
  EClass getInteger();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Real <em>Real</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Real
	 * @generated
	 */
  EClass getReal();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Char <em>Char</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Char</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Char
	 * @generated
	 */
  EClass getChar();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement <em>String Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement
	 * @generated
	 */
  EClass getStringElement();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement#getValue()
	 * @see #getStringElement()
	 * @generated
	 */
  EAttribute getStringElement_Value();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement <em>Boolean Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement
	 * @generated
	 */
  EClass getBooleanElement();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement#getValue()
	 * @see #getBooleanElement()
	 * @generated
	 */
  EAttribute getBooleanElement_Value();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement <em>Integer Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement
	 * @generated
	 */
  EClass getIntegerElement();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement#getValue()
	 * @see #getIntegerElement()
	 * @generated
	 */
  EAttribute getIntegerElement_Value();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement <em>Real Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement
	 * @generated
	 */
  EClass getRealElement();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement#getValue()
	 * @see #getRealElement()
	 * @generated
	 */
  EAttribute getRealElement_Value();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement <em>Char Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Char Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement
	 * @generated
	 */
  EClass getCharElement();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement#getValue()
	 * @see #getCharElement()
	 * @generated
	 */
  EAttribute getCharElement_Value();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record <em>Record</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record
	 * @generated
	 */
  EClass getRecord();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record#getField <em>Field</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Field</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Record#getField()
	 * @see #getRecord()
	 * @generated
	 */
  EReference getRecord_Field();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType <em>Sequence Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType
	 * @generated
	 */
  EClass getSequenceType();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType#getElementType <em>Element Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Element Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType#getElementType()
	 * @see #getSequenceType()
	 * @generated
	 */
  EReference getSequenceType_ElementType();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field <em>Field</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field
	 * @generated
	 */
  EClass getField();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Field#getType()
	 * @see #getField()
	 * @generated
	 */
  EReference getField_Type();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement <em>Record Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement
	 * @generated
	 */
  EClass getRecordElement();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement#getBox <em>Box</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Box</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement#getBox()
	 * @see #getRecordElement()
	 * @generated
	 */
  EReference getRecordElement_Box();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box <em>Box</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Box</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box
	 * @generated
	 */
  EClass getBox();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box#getContainment <em>Containment</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Containment</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Box#getContainment()
	 * @see #getBox()
	 * @generated
	 */
  EReference getBox_Containment();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement <em>Sequence Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement
	 * @generated
	 */
  EClass getSequenceElement();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement#getFinitePart <em>Finite Part</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Finite Part</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement#getFinitePart()
	 * @see #getSequenceElement()
	 * @generated
	 */
  EReference getSequenceElement_FinitePart();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement#getNonFinitePart <em>Non Finite Part</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Non Finite Part</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement#getNonFinitePart()
	 * @see #getSequenceElement()
	 * @generated
	 */
  EReference getSequenceElement_NonFinitePart();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType <em>Discrete Clock Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Discrete Clock Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType
	 * @generated
	 */
  EClass getDiscreteClockType();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType <em>Dense Clock Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dense Clock Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType
	 * @generated
	 */
  EClass getDenseClockType();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType#getBaseUnit <em>Base Unit</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Unit</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType#getBaseUnit()
	 * @see #getDenseClockType()
	 * @generated
	 */
  EAttribute getDenseClockType_BaseUnit();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType#getPhysicalMagnitude <em>Physical Magnitude</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical Magnitude</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType#getPhysicalMagnitude()
	 * @see #getDenseClockType()
	 * @generated
	 */
  EAttribute getDenseClockType_PhysicalMagnitude();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType <em>Enumeration Type</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Type</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType
	 * @generated
	 */
  EClass getEnumerationType();

  /**
	 * Returns the meta object for the attribute list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType#getEnumLiteral <em>Enum Literal</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Enum Literal</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType#getEnumLiteral()
	 * @see #getEnumerationType()
	 * @generated
	 */
  EAttribute getEnumerationType_EnumLiteral();

  /**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
  BasicTypeFactory getBasicTypeFactory();

  /**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.TypeImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getType()
		 * @generated
		 */
    EClass TYPE = eINSTANCE.getType();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.ElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.ElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getElement()
		 * @generated
		 */
    EClass ELEMENT = eINSTANCE.getElement();

    /**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference ELEMENT__TYPE = eINSTANCE.getElement_Type();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.PrimitiveTypeImpl <em>Primitive Type</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.PrimitiveTypeImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getPrimitiveType()
		 * @generated
		 */
    EClass PRIMITIVE_TYPE = eINSTANCE.getPrimitiveType();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.PrimitiveElementImpl <em>Primitive Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.PrimitiveElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getPrimitiveElement()
		 * @generated
		 */
    EClass PRIMITIVE_ELEMENT = eINSTANCE.getPrimitiveElement();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.StringImpl <em>String</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.StringImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getString()
		 * @generated
		 */
    EClass STRING = eINSTANCE.getString();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BooleanImpl <em>Boolean</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BooleanImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getBoolean()
		 * @generated
		 */
    EClass BOOLEAN = eINSTANCE.getBoolean();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.IntegerImpl <em>Integer</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.IntegerImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getInteger()
		 * @generated
		 */
    EClass INTEGER = eINSTANCE.getInteger();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RealImpl <em>Real</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RealImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getReal()
		 * @generated
		 */
    EClass REAL = eINSTANCE.getReal();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.CharImpl <em>Char</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.CharImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getChar()
		 * @generated
		 */
    EClass CHAR = eINSTANCE.getChar();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.StringElementImpl <em>String Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.StringElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getStringElement()
		 * @generated
		 */
    EClass STRING_ELEMENT = eINSTANCE.getStringElement();

    /**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute STRING_ELEMENT__VALUE = eINSTANCE.getStringElement_Value();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BooleanElementImpl <em>Boolean Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BooleanElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getBooleanElement()
		 * @generated
		 */
    EClass BOOLEAN_ELEMENT = eINSTANCE.getBooleanElement();

    /**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute BOOLEAN_ELEMENT__VALUE = eINSTANCE.getBooleanElement_Value();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.IntegerElementImpl <em>Integer Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.IntegerElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getIntegerElement()
		 * @generated
		 */
    EClass INTEGER_ELEMENT = eINSTANCE.getIntegerElement();

    /**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute INTEGER_ELEMENT__VALUE = eINSTANCE.getIntegerElement_Value();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RealElementImpl <em>Real Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RealElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getRealElement()
		 * @generated
		 */
    EClass REAL_ELEMENT = eINSTANCE.getRealElement();

    /**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute REAL_ELEMENT__VALUE = eINSTANCE.getRealElement_Value();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.CharElementImpl <em>Char Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.CharElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getCharElement()
		 * @generated
		 */
    EClass CHAR_ELEMENT = eINSTANCE.getCharElement();

    /**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute CHAR_ELEMENT__VALUE = eINSTANCE.getCharElement_Value();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RecordImpl <em>Record</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RecordImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getRecord()
		 * @generated
		 */
    EClass RECORD = eINSTANCE.getRecord();

    /**
		 * The meta object literal for the '<em><b>Field</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RECORD__FIELD = eINSTANCE.getRecord_Field();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceTypeImpl <em>Sequence Type</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceTypeImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getSequenceType()
		 * @generated
		 */
    EClass SEQUENCE_TYPE = eINSTANCE.getSequenceType();

    /**
		 * The meta object literal for the '<em><b>Element Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference SEQUENCE_TYPE__ELEMENT_TYPE = eINSTANCE.getSequenceType_ElementType();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.FieldImpl <em>Field</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.FieldImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getField()
		 * @generated
		 */
    EClass FIELD = eINSTANCE.getField();

    /**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference FIELD__TYPE = eINSTANCE.getField_Type();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RecordElementImpl <em>Record Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.RecordElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getRecordElement()
		 * @generated
		 */
    EClass RECORD_ELEMENT = eINSTANCE.getRecordElement();

    /**
		 * The meta object literal for the '<em><b>Box</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference RECORD_ELEMENT__BOX = eINSTANCE.getRecordElement_Box();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BoxImpl <em>Box</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BoxImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getBox()
		 * @generated
		 */
    EClass BOX = eINSTANCE.getBox();

    /**
		 * The meta object literal for the '<em><b>Containment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BOX__CONTAINMENT = eINSTANCE.getBox_Containment();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceElementImpl <em>Sequence Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getSequenceElement()
		 * @generated
		 */
    EClass SEQUENCE_ELEMENT = eINSTANCE.getSequenceElement();

    /**
		 * The meta object literal for the '<em><b>Finite Part</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference SEQUENCE_ELEMENT__FINITE_PART = eINSTANCE.getSequenceElement_FinitePart();

    /**
		 * The meta object literal for the '<em><b>Non Finite Part</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference SEQUENCE_ELEMENT__NON_FINITE_PART = eINSTANCE.getSequenceElement_NonFinitePart();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DiscreteClockTypeImpl <em>Discrete Clock Type</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DiscreteClockTypeImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getDiscreteClockType()
		 * @generated
		 */
    EClass DISCRETE_CLOCK_TYPE = eINSTANCE.getDiscreteClockType();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DenseClockTypeImpl <em>Dense Clock Type</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DenseClockTypeImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getDenseClockType()
		 * @generated
		 */
    EClass DENSE_CLOCK_TYPE = eINSTANCE.getDenseClockType();

    /**
		 * The meta object literal for the '<em><b>Base Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute DENSE_CLOCK_TYPE__BASE_UNIT = eINSTANCE.getDenseClockType_BaseUnit();

    /**
		 * The meta object literal for the '<em><b>Physical Magnitude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute DENSE_CLOCK_TYPE__PHYSICAL_MAGNITUDE = eINSTANCE.getDenseClockType_PhysicalMagnitude();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.EnumerationTypeImpl <em>Enumeration Type</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.EnumerationTypeImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl#getEnumerationType()
		 * @generated
		 */
    EClass ENUMERATION_TYPE = eINSTANCE.getEnumerationType();

    /**
		 * The meta object literal for the '<em><b>Enum Literal</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute ENUMERATION_TYPE__ENUM_LITERAL = eINSTANCE.getEnumerationType_EnumLiteral();

  }

} //BasicTypePackage
