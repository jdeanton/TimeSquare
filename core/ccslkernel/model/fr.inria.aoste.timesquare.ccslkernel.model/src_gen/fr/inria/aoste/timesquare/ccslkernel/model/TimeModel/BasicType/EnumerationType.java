/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType;

import java.lang.String;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.EnumerationType#getEnumLiteral <em>Enum Literal</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage#getEnumerationType()
 * @model
 * @generated
 */
public interface EnumerationType extends PrimitiveType
{
  /**
	 * Returns the value of the '<em><b>Enum Literal</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Enum Literal</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum Literal</em>' attribute list.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage#getEnumerationType_EnumLiteral()
	 * @model dataType="fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.String" required="true"
	 * @generated
	 */
  EList<String> getEnumLiteral();

} // EnumerationType
