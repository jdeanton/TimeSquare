/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Seq Sched</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.SeqSchedImpl#getIntegerExpr <em>Integer Expr</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SeqSchedImpl extends UnarySeqExpressionImpl implements SeqSched
{
  /**
	 * The cached value of the '{@link #getIntegerExpr() <em>Integer Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getIntegerExpr()
	 * @generated
	 * @ordered
	 */
  protected IntegerExpression integerExpr;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected SeqSchedImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClassicalExpressionPackage.Literals.SEQ_SCHED;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntegerExpression getIntegerExpr()
  {
		return integerExpr;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetIntegerExpr(IntegerExpression newIntegerExpr, NotificationChain msgs)
  {
		IntegerExpression oldIntegerExpr = integerExpr;
		integerExpr = newIntegerExpr;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR, oldIntegerExpr, newIntegerExpr);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setIntegerExpr(IntegerExpression newIntegerExpr)
  {
		if (newIntegerExpr != integerExpr) {
			NotificationChain msgs = null;
			if (integerExpr != null)
				msgs = ((InternalEObject)integerExpr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR, null, msgs);
			if (newIntegerExpr != null)
				msgs = ((InternalEObject)newIntegerExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR, null, msgs);
			msgs = basicSetIntegerExpr(newIntegerExpr, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR, newIntegerExpr, newIntegerExpr));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR:
				return basicSetIntegerExpr(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR:
				return getIntegerExpr();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR:
				setIntegerExpr((IntegerExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR:
				setIntegerExpr((IntegerExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.SEQ_SCHED__INTEGER_EXPR:
				return integerExpr != null;
		}
		return super.eIsSet(featureID);
	}

} //SeqSchedImpl
