/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event#getReferencedObjectRefs <em>Referenced Object Refs</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends NamedElement
{
  /**
	 * Returns the value of the '<em><b>Referenced Object Refs</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Referenced Object Refs</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Object Refs</em>' reference list.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage#getEvent_ReferencedObjectRefs()
	 * @model
	 * @generated
	 */
  EList<EObject> getReferencedObjectRefs();

  /**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind}.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Kind</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind
	 * @see #setKind(EventKind)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage#getEvent_Kind()
	 * @model
	 * @generated
	 */
  EventKind getKind();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind
	 * @see #getKind()
	 * @generated
	 */
  void setKind(EventKind value);

} // Event
