/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionFactory
 * @model kind="package"
 * @generated
 */
public interface KernelExpressionPackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNAME = "KernelExpression";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_URI = "http://fr.inria.aoste.timemodel.ccslmodel.clockexpressionandrelation.kernelexpression";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_PREFIX = "fr.inria.aoste.timemodel.ccslmodel.clockexpressionandrelation.kernelexpression";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  KernelExpressionPackage eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl.init();

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionDeclarationImpl <em>Declaration</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionDeclarationImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getKernelExpressionDeclaration()
	 * @generated
	 */
  int KERNEL_EXPRESSION_DECLARATION = 0;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int KERNEL_EXPRESSION_DECLARATION__NAME = ClockExpressionAndRelationPackage.EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int KERNEL_EXPRESSION_DECLARATION__PARAMETERS = ClockExpressionAndRelationPackage.EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE = ClockExpressionAndRelationPackage.EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The number of structural features of the '<em>Declaration</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT = ClockExpressionAndRelationPackage.EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UpToImpl <em>Up To</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UpToImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getUpTo()
	 * @generated
	 */
  int UP_TO = 1;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UP_TO__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UP_TO__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UP_TO__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Clock To Follow</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UP_TO__CLOCK_TO_FOLLOW = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Killer Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UP_TO__KILLER_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The feature id for the '<em><b>Is Preemptive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UP_TO__IS_PREEMPTIVE = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

		/**
	 * The number of structural features of the '<em>Up To</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UP_TO_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 3;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeferImpl <em>Defer</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeferImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getDefer()
	 * @generated
	 */
  int DEFER = 2;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DEFER__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DEFER__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DEFER__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Base Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFER__BASE_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Delay Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFER__DELAY_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The feature id for the '<em><b>Delay Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFER__DELAY_PATTERN = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

		/**
	 * The number of structural features of the '<em>Defer</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DEFER_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 3;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.StrictSamplingImpl <em>Strict Sampling</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.StrictSamplingImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getStrictSampling()
	 * @generated
	 */
  int STRICT_SAMPLING = 3;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRICT_SAMPLING__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRICT_SAMPLING__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRICT_SAMPLING__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Sampled Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRICT_SAMPLING__SAMPLED_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Sampling Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRICT_SAMPLING__SAMPLING_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Strict Sampling</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int STRICT_SAMPLING_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.ConcatenationImpl <em>Concatenation</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.ConcatenationImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getConcatenation()
	 * @generated
	 */
  int CONCATENATION = 4;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONCATENATION__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONCATENATION__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONCATENATION__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Left Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCATENATION__LEFT_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Right Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCATENATION__RIGHT_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Concatenation</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CONCATENATION_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UnionImpl <em>Union</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UnionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getUnion()
	 * @generated
	 */
  int UNION = 5;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNION__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNION__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNION__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Clock1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNION__CLOCK1 = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Clock2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNION__CLOCK2 = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Union</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int UNION_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.IntersectionImpl <em>Intersection</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.IntersectionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getIntersection()
	 * @generated
	 */
  int INTERSECTION = 6;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTERSECTION__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTERSECTION__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTERSECTION__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Clock1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERSECTION__CLOCK1 = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Clock2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERSECTION__CLOCK2 = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Intersection</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INTERSECTION_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.SupImpl <em>Sup</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.SupImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getSup()
	 * @generated
	 */
  int SUP = 7;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SUP__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SUP__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SUP__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Clock1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUP__CLOCK1 = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Clock2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUP__CLOCK2 = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Sup</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int SUP_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.InfImpl <em>Inf</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.InfImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getInf()
	 * @generated
	 */
  int INF = 8;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INF__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INF__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INF__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Clock1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INF__CLOCK1 = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Clock2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INF__CLOCK2 = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Inf</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int INF_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.NonStrictSamplingImpl <em>Non Strict Sampling</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.NonStrictSamplingImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getNonStrictSampling()
	 * @generated
	 */
  int NON_STRICT_SAMPLING = 9;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NON_STRICT_SAMPLING__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NON_STRICT_SAMPLING__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NON_STRICT_SAMPLING__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Sampled Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_STRICT_SAMPLING__SAMPLED_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Sampling Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_STRICT_SAMPLING__SAMPLING_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Non Strict Sampling</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NON_STRICT_SAMPLING_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DiscretizationImpl <em>Discretization</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DiscretizationImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getDiscretization()
	 * @generated
	 */
  int DISCRETIZATION = 10;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DISCRETIZATION__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DISCRETIZATION__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DISCRETIZATION__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The feature id for the '<em><b>Dense Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DISCRETIZATION__DENSE_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Discretization Factor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DISCRETIZATION__DISCRETIZATION_FACTOR = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Discretization</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DISCRETIZATION_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeathImpl <em>Death</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeathImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getDeath()
	 * @generated
	 */
  int DEATH = 11;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DEATH__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

  /**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DEATH__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

  /**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DEATH__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

  /**
	 * The number of structural features of the '<em>Death</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int DEATH_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;


  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.WaitImpl <em>Wait</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.WaitImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getWait()
	 * @generated
	 */
	int WAIT = 12;

		/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT__NAME = KERNEL_EXPRESSION_DECLARATION__NAME;

		/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT__PARAMETERS = KERNEL_EXPRESSION_DECLARATION__PARAMETERS;

		/**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT__RETURN_TYPE = KERNEL_EXPRESSION_DECLARATION__RETURN_TYPE;

		/**
	 * The feature id for the '<em><b>Waiting Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT__WAITING_CLOCK = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Waiting Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT__WAITING_VALUE = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_FEATURE_COUNT = KERNEL_EXPRESSION_DECLARATION_FEATURE_COUNT + 2;


		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Declaration</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration
	 * @generated
	 */
  EClass getKernelExpressionDeclaration();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo <em>Up To</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Up To</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo
	 * @generated
	 */
  EClass getUpTo();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo#getClockToFollow <em>Clock To Follow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock To Follow</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo#getClockToFollow()
	 * @see #getUpTo()
	 * @generated
	 */
	EReference getUpTo_ClockToFollow();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo#getKillerClock <em>Killer Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Killer Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo#getKillerClock()
	 * @see #getUpTo()
	 * @generated
	 */
	EReference getUpTo_KillerClock();

		/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo#isIsPreemptive <em>Is Preemptive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Preemptive</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo#isIsPreemptive()
	 * @see #getUpTo()
	 * @generated
	 */
	EAttribute getUpTo_IsPreemptive();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer <em>Defer</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Defer</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer
	 * @generated
	 */
  EClass getDefer();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer#getBaseClock <em>Base Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Base Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer#getBaseClock()
	 * @see #getDefer()
	 * @generated
	 */
	EReference getDefer_BaseClock();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer#getDelayClock <em>Delay Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delay Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer#getDelayClock()
	 * @see #getDefer()
	 * @generated
	 */
	EReference getDefer_DelayClock();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer#getDelayPattern <em>Delay Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delay Pattern</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer#getDelayPattern()
	 * @see #getDefer()
	 * @generated
	 */
	EReference getDefer_DelayPattern();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling <em>Strict Sampling</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Strict Sampling</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling
	 * @generated
	 */
  EClass getStrictSampling();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling#getSampledClock <em>Sampled Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sampled Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling#getSampledClock()
	 * @see #getStrictSampling()
	 * @generated
	 */
	EReference getStrictSampling_SampledClock();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling#getSamplingClock <em>Sampling Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sampling Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling#getSamplingClock()
	 * @see #getStrictSampling()
	 * @generated
	 */
	EReference getStrictSampling_SamplingClock();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation <em>Concatenation</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concatenation</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation
	 * @generated
	 */
  EClass getConcatenation();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation#getLeftClock <em>Left Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation#getLeftClock()
	 * @see #getConcatenation()
	 * @generated
	 */
	EReference getConcatenation_LeftClock();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation#getRightClock <em>Right Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation#getRightClock()
	 * @see #getConcatenation()
	 * @generated
	 */
	EReference getConcatenation_RightClock();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union <em>Union</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Union</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union
	 * @generated
	 */
  EClass getUnion();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union#getClock1 <em>Clock1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock1</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union#getClock1()
	 * @see #getUnion()
	 * @generated
	 */
	EReference getUnion_Clock1();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union#getClock2 <em>Clock2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock2</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union#getClock2()
	 * @see #getUnion()
	 * @generated
	 */
	EReference getUnion_Clock2();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection <em>Intersection</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Intersection</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection
	 * @generated
	 */
  EClass getIntersection();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection#getClock1 <em>Clock1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock1</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection#getClock1()
	 * @see #getIntersection()
	 * @generated
	 */
	EReference getIntersection_Clock1();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection#getClock2 <em>Clock2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock2</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection#getClock2()
	 * @see #getIntersection()
	 * @generated
	 */
	EReference getIntersection_Clock2();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup <em>Sup</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sup</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup
	 * @generated
	 */
  EClass getSup();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup#getClock1 <em>Clock1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock1</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup#getClock1()
	 * @see #getSup()
	 * @generated
	 */
	EReference getSup_Clock1();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup#getClock2 <em>Clock2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock2</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup#getClock2()
	 * @see #getSup()
	 * @generated
	 */
	EReference getSup_Clock2();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf <em>Inf</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inf</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf
	 * @generated
	 */
  EClass getInf();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf#getClock1 <em>Clock1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock1</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf#getClock1()
	 * @see #getInf()
	 * @generated
	 */
	EReference getInf_Clock1();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf#getClock2 <em>Clock2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Clock2</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf#getClock2()
	 * @see #getInf()
	 * @generated
	 */
	EReference getInf_Clock2();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling <em>Non Strict Sampling</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Strict Sampling</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling
	 * @generated
	 */
  EClass getNonStrictSampling();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling#getSampledClock <em>Sampled Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sampled Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling#getSampledClock()
	 * @see #getNonStrictSampling()
	 * @generated
	 */
	EReference getNonStrictSampling_SampledClock();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling#getSamplingClock <em>Sampling Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sampling Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling#getSamplingClock()
	 * @see #getNonStrictSampling()
	 * @generated
	 */
	EReference getNonStrictSampling_SamplingClock();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization <em>Discretization</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Discretization</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization
	 * @generated
	 */
  EClass getDiscretization();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization#getDenseClock <em>Dense Clock</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dense Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization#getDenseClock()
	 * @see #getDiscretization()
	 * @generated
	 */
  EReference getDiscretization_DenseClock();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization#getDiscretizationFactor <em>Discretization Factor</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Discretization Factor</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization#getDiscretizationFactor()
	 * @see #getDiscretization()
	 * @generated
	 */
  EReference getDiscretization_DiscretizationFactor();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Death <em>Death</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Death</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Death
	 * @generated
	 */
  EClass getDeath();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait
	 * @generated
	 */
	EClass getWait();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait#getWaitingClock <em>Waiting Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Waiting Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait#getWaitingClock()
	 * @see #getWait()
	 * @generated
	 */
	EReference getWait_WaitingClock();

		/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait#getWaitingValue <em>Waiting Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Waiting Value</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait#getWaitingValue()
	 * @see #getWait()
	 * @generated
	 */
	EReference getWait_WaitingValue();

		/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
  KernelExpressionFactory getKernelExpressionFactory();

  /**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionDeclarationImpl <em>Declaration</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionDeclarationImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getKernelExpressionDeclaration()
		 * @generated
		 */
    EClass KERNEL_EXPRESSION_DECLARATION = eINSTANCE.getKernelExpressionDeclaration();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UpToImpl <em>Up To</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UpToImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getUpTo()
		 * @generated
		 */
    EClass UP_TO = eINSTANCE.getUpTo();

    /**
		 * The meta object literal for the '<em><b>Clock To Follow</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UP_TO__CLOCK_TO_FOLLOW = eINSTANCE.getUpTo_ClockToFollow();

				/**
		 * The meta object literal for the '<em><b>Killer Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UP_TO__KILLER_CLOCK = eINSTANCE.getUpTo_KillerClock();

				/**
		 * The meta object literal for the '<em><b>Is Preemptive</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UP_TO__IS_PREEMPTIVE = eINSTANCE.getUpTo_IsPreemptive();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeferImpl <em>Defer</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeferImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getDefer()
		 * @generated
		 */
    EClass DEFER = eINSTANCE.getDefer();

    /**
		 * The meta object literal for the '<em><b>Base Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFER__BASE_CLOCK = eINSTANCE.getDefer_BaseClock();

				/**
		 * The meta object literal for the '<em><b>Delay Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFER__DELAY_CLOCK = eINSTANCE.getDefer_DelayClock();

				/**
		 * The meta object literal for the '<em><b>Delay Pattern</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFER__DELAY_PATTERN = eINSTANCE.getDefer_DelayPattern();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.StrictSamplingImpl <em>Strict Sampling</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.StrictSamplingImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getStrictSampling()
		 * @generated
		 */
    EClass STRICT_SAMPLING = eINSTANCE.getStrictSampling();

    /**
		 * The meta object literal for the '<em><b>Sampled Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRICT_SAMPLING__SAMPLED_CLOCK = eINSTANCE.getStrictSampling_SampledClock();

				/**
		 * The meta object literal for the '<em><b>Sampling Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRICT_SAMPLING__SAMPLING_CLOCK = eINSTANCE.getStrictSampling_SamplingClock();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.ConcatenationImpl <em>Concatenation</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.ConcatenationImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getConcatenation()
		 * @generated
		 */
    EClass CONCATENATION = eINSTANCE.getConcatenation();

    /**
		 * The meta object literal for the '<em><b>Left Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCATENATION__LEFT_CLOCK = eINSTANCE.getConcatenation_LeftClock();

				/**
		 * The meta object literal for the '<em><b>Right Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCATENATION__RIGHT_CLOCK = eINSTANCE.getConcatenation_RightClock();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UnionImpl <em>Union</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UnionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getUnion()
		 * @generated
		 */
    EClass UNION = eINSTANCE.getUnion();

    /**
		 * The meta object literal for the '<em><b>Clock1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNION__CLOCK1 = eINSTANCE.getUnion_Clock1();

				/**
		 * The meta object literal for the '<em><b>Clock2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNION__CLOCK2 = eINSTANCE.getUnion_Clock2();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.IntersectionImpl <em>Intersection</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.IntersectionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getIntersection()
		 * @generated
		 */
    EClass INTERSECTION = eINSTANCE.getIntersection();

    /**
		 * The meta object literal for the '<em><b>Clock1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERSECTION__CLOCK1 = eINSTANCE.getIntersection_Clock1();

				/**
		 * The meta object literal for the '<em><b>Clock2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERSECTION__CLOCK2 = eINSTANCE.getIntersection_Clock2();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.SupImpl <em>Sup</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.SupImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getSup()
		 * @generated
		 */
    EClass SUP = eINSTANCE.getSup();

    /**
		 * The meta object literal for the '<em><b>Clock1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUP__CLOCK1 = eINSTANCE.getSup_Clock1();

				/**
		 * The meta object literal for the '<em><b>Clock2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUP__CLOCK2 = eINSTANCE.getSup_Clock2();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.InfImpl <em>Inf</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.InfImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getInf()
		 * @generated
		 */
    EClass INF = eINSTANCE.getInf();

    /**
		 * The meta object literal for the '<em><b>Clock1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INF__CLOCK1 = eINSTANCE.getInf_Clock1();

				/**
		 * The meta object literal for the '<em><b>Clock2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INF__CLOCK2 = eINSTANCE.getInf_Clock2();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.NonStrictSamplingImpl <em>Non Strict Sampling</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.NonStrictSamplingImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getNonStrictSampling()
		 * @generated
		 */
    EClass NON_STRICT_SAMPLING = eINSTANCE.getNonStrictSampling();

    /**
		 * The meta object literal for the '<em><b>Sampled Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NON_STRICT_SAMPLING__SAMPLED_CLOCK = eINSTANCE.getNonStrictSampling_SampledClock();

				/**
		 * The meta object literal for the '<em><b>Sampling Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NON_STRICT_SAMPLING__SAMPLING_CLOCK = eINSTANCE.getNonStrictSampling_SamplingClock();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DiscretizationImpl <em>Discretization</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DiscretizationImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getDiscretization()
		 * @generated
		 */
    EClass DISCRETIZATION = eINSTANCE.getDiscretization();

    /**
		 * The meta object literal for the '<em><b>Dense Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference DISCRETIZATION__DENSE_CLOCK = eINSTANCE.getDiscretization_DenseClock();

    /**
		 * The meta object literal for the '<em><b>Discretization Factor</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference DISCRETIZATION__DISCRETIZATION_FACTOR = eINSTANCE.getDiscretization_DiscretizationFactor();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeathImpl <em>Death</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeathImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getDeath()
		 * @generated
		 */
    EClass DEATH = eINSTANCE.getDeath();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.WaitImpl <em>Wait</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.WaitImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl#getWait()
		 * @generated
		 */
		EClass WAIT = eINSTANCE.getWait();

				/**
		 * The meta object literal for the '<em><b>Waiting Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAIT__WAITING_CLOCK = eINSTANCE.getWait_WaitingClock();

				/**
		 * The meta object literal for the '<em><b>Waiting Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAIT__WAITING_VALUE = eINSTANCE.getWait_WaitingValue();

  }

} //KernelExpressionPackage
