/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Variable Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BooleanVariableRefImpl#getReferencedVar <em>Referenced Var</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BooleanVariableRefImpl extends BooleanExpressionImpl implements BooleanVariableRef
{
  /**
	 * The cached value of the '{@link #getReferencedVar() <em>Referenced Var</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getReferencedVar()
	 * @generated
	 * @ordered
	 */
  protected AbstractEntity referencedVar;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected BooleanVariableRefImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClassicalExpressionPackage.Literals.BOOLEAN_VARIABLE_REF;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public AbstractEntity getReferencedVar()
  {
		if (referencedVar != null && referencedVar.eIsProxy()) {
			InternalEObject oldReferencedVar = (InternalEObject)referencedVar;
			referencedVar = (AbstractEntity)eResolveProxy(oldReferencedVar);
			if (referencedVar != oldReferencedVar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClassicalExpressionPackage.BOOLEAN_VARIABLE_REF__REFERENCED_VAR, oldReferencedVar, referencedVar));
			}
		}
		return referencedVar;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public AbstractEntity basicGetReferencedVar()
  {
		return referencedVar;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setReferencedVar(AbstractEntity newReferencedVar)
  {
		AbstractEntity oldReferencedVar = referencedVar;
		referencedVar = newReferencedVar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassicalExpressionPackage.BOOLEAN_VARIABLE_REF__REFERENCED_VAR, oldReferencedVar, referencedVar));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BOOLEAN_VARIABLE_REF__REFERENCED_VAR:
				if (resolve) return getReferencedVar();
				return basicGetReferencedVar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BOOLEAN_VARIABLE_REF__REFERENCED_VAR:
				setReferencedVar((AbstractEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BOOLEAN_VARIABLE_REF__REFERENCED_VAR:
				setReferencedVar((AbstractEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BOOLEAN_VARIABLE_REF__REFERENCED_VAR:
				return referencedVar != null;
		}
		return super.eIsSet(featureID);
	}

} //BooleanVariableRefImpl
