/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sequence Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceElementImpl#getFinitePart <em>Finite Part</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.SequenceElementImpl#getNonFinitePart <em>Non Finite Part</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SequenceElementImpl extends ElementImpl implements SequenceElement
{
  /**
	 * The cached value of the '{@link #getFinitePart() <em>Finite Part</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getFinitePart()
	 * @generated
	 * @ordered
	 */
  protected EList<PrimitiveElement> finitePart;

  /**
	 * The cached value of the '{@link #getNonFinitePart() <em>Non Finite Part</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getNonFinitePart()
	 * @generated
	 * @ordered
	 */
  protected EList<PrimitiveElement> nonFinitePart;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected SequenceElementImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return BasicTypePackage.Literals.SEQUENCE_ELEMENT;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<PrimitiveElement> getFinitePart()
  {
		if (finitePart == null) {
			finitePart = new EObjectContainmentEList<PrimitiveElement>(PrimitiveElement.class, this, BasicTypePackage.SEQUENCE_ELEMENT__FINITE_PART);
		}
		return finitePart;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<PrimitiveElement> getNonFinitePart()
  {
		if (nonFinitePart == null) {
			nonFinitePart = new EObjectContainmentEList<PrimitiveElement>(PrimitiveElement.class, this, BasicTypePackage.SEQUENCE_ELEMENT__NON_FINITE_PART);
		}
		return nonFinitePart;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case BasicTypePackage.SEQUENCE_ELEMENT__FINITE_PART:
				return ((InternalEList<?>)getFinitePart()).basicRemove(otherEnd, msgs);
			case BasicTypePackage.SEQUENCE_ELEMENT__NON_FINITE_PART:
				return ((InternalEList<?>)getNonFinitePart()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case BasicTypePackage.SEQUENCE_ELEMENT__FINITE_PART:
				return getFinitePart();
			case BasicTypePackage.SEQUENCE_ELEMENT__NON_FINITE_PART:
				return getNonFinitePart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case BasicTypePackage.SEQUENCE_ELEMENT__FINITE_PART:
				getFinitePart().clear();
				getFinitePart().addAll((Collection<? extends PrimitiveElement>)newValue);
				return;
			case BasicTypePackage.SEQUENCE_ELEMENT__NON_FINITE_PART:
				getNonFinitePart().clear();
				getNonFinitePart().addAll((Collection<? extends PrimitiveElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case BasicTypePackage.SEQUENCE_ELEMENT__FINITE_PART:
				getFinitePart().clear();
				return;
			case BasicTypePackage.SEQUENCE_ELEMENT__NON_FINITE_PART:
				getNonFinitePart().clear();
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case BasicTypePackage.SEQUENCE_ELEMENT__FINITE_PART:
				return finitePart != null && !finitePart.isEmpty();
			case BasicTypePackage.SEQUENCE_ELEMENT__NON_FINITE_PART:
				return nonFinitePart != null && !nonFinitePart.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SequenceElementImpl
