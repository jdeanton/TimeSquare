/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditional Expression Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalExpressionDefinitionImpl#getExprCases <em>Expr Cases</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalExpressionDefinitionImpl#getDefaultExpression <em>Default Expression</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionalExpressionDefinitionImpl extends UserExpressionDefinitionImpl implements ConditionalExpressionDefinition
{
  /**
	 * The cached value of the '{@link #getExprCases() <em>Expr Cases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getExprCases()
	 * @generated
	 * @ordered
	 */
  protected EList<ExprCase> exprCases;

  /**
	 * The cached value of the '{@link #getDefaultExpression() <em>Default Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getDefaultExpression()
	 * @generated
	 * @ordered
	 */
  protected Expression defaultExpression;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected ConditionalExpressionDefinitionImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClockExpressionAndRelationPackage.Literals.CONDITIONAL_EXPRESSION_DEFINITION;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ExprCase> getExprCases()
  {
		if (exprCases == null) {
			exprCases = new EObjectContainmentEList<ExprCase>(ExprCase.class, this, ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES);
		}
		return exprCases;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Expression getDefaultExpression()
  {
		return defaultExpression;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultExpression(Expression newDefaultExpression, NotificationChain msgs) {
		Expression oldDefaultExpression = defaultExpression;
		defaultExpression = newDefaultExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION, oldDefaultExpression, newDefaultExpression);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDefaultExpression(Expression newDefaultExpression) {
		if (newDefaultExpression != defaultExpression) {
			NotificationChain msgs = null;
			if (defaultExpression != null)
				msgs = ((InternalEObject)defaultExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION, null, msgs);
			if (newDefaultExpression != null)
				msgs = ((InternalEObject)newDefaultExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION, null, msgs);
			msgs = basicSetDefaultExpression(newDefaultExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION, newDefaultExpression, newDefaultExpression));
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES:
				return ((InternalEList<?>)getExprCases()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION:
				return basicSetDefaultExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES:
				return getExprCases();
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION:
				return getDefaultExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES:
				getExprCases().clear();
				getExprCases().addAll((Collection<? extends ExprCase>)newValue);
				return;
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION:
				setDefaultExpression((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES:
				getExprCases().clear();
				return;
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION:
				setDefaultExpression((Expression)null);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES:
				return exprCases != null && !exprCases.isEmpty();
			case ClockExpressionAndRelationPackage.CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION:
				return defaultExpression != null;
		}
		return super.eIsSet(featureID);
	}

} //ConditionalExpressionDefinitionImpl
