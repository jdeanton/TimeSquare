/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.AbstractEntityImpl#getType <em>Type</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.AbstractEntityImpl#getDesiredEventKind <em>Desired Event Kind</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AbstractEntityImpl extends BindableEntityImpl implements AbstractEntity
{
  /**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
  protected Type type;

  /**
	 * The default value of the '{@link #getDesiredEventKind() <em>Desired Event Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesiredEventKind()
	 * @generated
	 * @ordered
	 */
	protected static final EventKind DESIRED_EVENT_KIND_EDEFAULT = EventKind.UNDEFINED;
		/**
	 * The cached value of the '{@link #getDesiredEventKind() <em>Desired Event Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesiredEventKind()
	 * @generated
	 * @ordered
	 */
	protected EventKind desiredEventKind = DESIRED_EVENT_KIND_EDEFAULT;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected AbstractEntityImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClockExpressionAndRelationPackage.Literals.ABSTRACT_ENTITY;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Type getType()
  {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (Type)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__TYPE, oldType, type));
			}
		}
		return type;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public Type basicGetType()
  {
		return type;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setType(Type newType)
  {
		Type oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__TYPE, oldType, type));
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EventKind getDesiredEventKind() {
		return desiredEventKind;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDesiredEventKind(EventKind newDesiredEventKind) {
		EventKind oldDesiredEventKind = desiredEventKind;
		desiredEventKind = newDesiredEventKind == null ? DESIRED_EVENT_KIND_EDEFAULT : newDesiredEventKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__DESIRED_EVENT_KIND, oldDesiredEventKind, desiredEventKind));
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__DESIRED_EVENT_KIND:
				return getDesiredEventKind();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__TYPE:
				setType((Type)newValue);
				return;
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__DESIRED_EVENT_KIND:
				setDesiredEventKind((EventKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__TYPE:
				setType((Type)null);
				return;
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__DESIRED_EVENT_KIND:
				setDesiredEventKind(DESIRED_EVENT_KIND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__TYPE:
				return type != null;
			case ClockExpressionAndRelationPackage.ABSTRACT_ENTITY__DESIRED_EVENT_KIND:
				return desiredEventKind != DESIRED_EVENT_KIND_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (desiredEventKind: ");
		result.append(desiredEventKind);
		result.append(')');
		return result.toString();
	}

} //AbstractEntityImpl
