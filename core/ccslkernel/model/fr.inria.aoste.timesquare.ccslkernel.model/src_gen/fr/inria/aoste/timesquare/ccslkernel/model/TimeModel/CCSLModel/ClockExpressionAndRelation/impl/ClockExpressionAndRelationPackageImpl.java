/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl.KernelRelationPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.PrimitivesTypeValuePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClockExpressionAndRelationPackageImpl extends EPackageImpl implements ClockExpressionAndRelationPackage
{
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass expressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass userExpressionDefinitionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass expressionLibraryEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass bindingEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass conditionalExpressionDefinitionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass exprCaseEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass relationDefinitionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass userRelationDefinitionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass relationEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass conditionalRelationDefinitionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass relationLibraryEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass relCaseEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass libraryEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass abstractEntityEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass expressionDefinitionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass bindableEntityEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass concreteEntityEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass relationDeclarationEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass expressionDeclarationEClass = null;

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalRelationDefinitionEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalExpressionDefinitionEClass = null;

		/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private ClockExpressionAndRelationPackageImpl()
  {
		super(eNS_URI, ClockExpressionAndRelationFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ClockExpressionAndRelationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static ClockExpressionAndRelationPackage init()
  {
		if (isInited) return (ClockExpressionAndRelationPackage)EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredClockExpressionAndRelationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ClockExpressionAndRelationPackageImpl theClockExpressionAndRelationPackage = registeredClockExpressionAndRelationPackage instanceof ClockExpressionAndRelationPackageImpl ? (ClockExpressionAndRelationPackageImpl)registeredClockExpressionAndRelationPackage : new ClockExpressionAndRelationPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		TimeModelPackageImpl theTimeModelPackage = (TimeModelPackageImpl)(registeredPackage instanceof TimeModelPackageImpl ? registeredPackage : TimeModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CCSLModelPackage.eNS_URI);
		CCSLModelPackageImpl theCCSLModelPackage = (CCSLModelPackageImpl)(registeredPackage instanceof CCSLModelPackageImpl ? registeredPackage : CCSLModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassicalExpressionPackage.eNS_URI);
		ClassicalExpressionPackageImpl theClassicalExpressionPackage = (ClassicalExpressionPackageImpl)(registeredPackage instanceof ClassicalExpressionPackageImpl ? registeredPackage : ClassicalExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelExpressionPackage.eNS_URI);
		KernelExpressionPackageImpl theKernelExpressionPackage = (KernelExpressionPackageImpl)(registeredPackage instanceof KernelExpressionPackageImpl ? registeredPackage : KernelExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelRelationPackage.eNS_URI);
		KernelRelationPackageImpl theKernelRelationPackage = (KernelRelationPackageImpl)(registeredPackage instanceof KernelRelationPackageImpl ? registeredPackage : KernelRelationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BasicTypePackage.eNS_URI);
		BasicTypePackageImpl theBasicTypePackage = (BasicTypePackageImpl)(registeredPackage instanceof BasicTypePackageImpl ? registeredPackage : BasicTypePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PrimitivesTypeValuePackage.eNS_URI);
		PrimitivesTypeValuePackageImpl thePrimitivesTypeValuePackage = (PrimitivesTypeValuePackageImpl)(registeredPackage instanceof PrimitivesTypeValuePackageImpl ? registeredPackage : PrimitivesTypeValuePackage.eINSTANCE);

		// Create package meta-data objects
		theClockExpressionAndRelationPackage.createPackageContents();
		theTimeModelPackage.createPackageContents();
		theCCSLModelPackage.createPackageContents();
		theClassicalExpressionPackage.createPackageContents();
		theKernelExpressionPackage.createPackageContents();
		theKernelRelationPackage.createPackageContents();
		theBasicTypePackage.createPackageContents();
		thePrimitivesTypeValuePackage.createPackageContents();

		// Initialize created meta-data
		theClockExpressionAndRelationPackage.initializePackageContents();
		theTimeModelPackage.initializePackageContents();
		theCCSLModelPackage.initializePackageContents();
		theClassicalExpressionPackage.initializePackageContents();
		theKernelExpressionPackage.initializePackageContents();
		theKernelRelationPackage.initializePackageContents();
		theBasicTypePackage.initializePackageContents();
		thePrimitivesTypeValuePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClockExpressionAndRelationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClockExpressionAndRelationPackage.eNS_URI, theClockExpressionAndRelationPackage);
		return theClockExpressionAndRelationPackage;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getExpression()
  {
		return expressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExpression_Type()
  {
		return (EReference)expressionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExpression_Bindings()
  {
		return (EReference)expressionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUserExpressionDefinition()
  {
		return userExpressionDefinitionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUserExpressionDefinition_ConcreteEntities()
  {
		return (EReference)userExpressionDefinitionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUserExpressionDefinition_RootExpression()
  {
		return (EReference)userExpressionDefinitionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUserExpressionDefinition_ClassicalExpressions()
  {
		return (EReference)userExpressionDefinitionEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getExpressionLibrary()
  {
		return expressionLibraryEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExpressionLibrary_ExpressionDefinitions()
  {
		return (EReference)expressionLibraryEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExpressionLibrary_Elements()
  {
		return (EReference)expressionLibraryEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExpressionLibrary_ExpressionDeclarations()
  {
		return (EReference)expressionLibraryEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBinding()
  {
		return bindingEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBinding_Abstract()
  {
		return (EReference)bindingEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBinding_Bindable()
  {
		return (EReference)bindingEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getConditionalExpressionDefinition()
  {
		return conditionalExpressionDefinitionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getConditionalExpressionDefinition_ExprCases()
  {
		return (EReference)conditionalExpressionDefinitionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getConditionalExpressionDefinition_DefaultExpression()
  {
		return (EReference)conditionalExpressionDefinitionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getExprCase()
  {
		return exprCaseEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExprCase_Condition()
  {
		return (EReference)exprCaseEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExprCase_Expression()
  {
		return (EReference)exprCaseEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRelationDefinition()
  {
		return relationDefinitionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelationDefinition_Declaration()
  {
		return (EReference)relationDefinitionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUserRelationDefinition()
  {
		return userRelationDefinitionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUserRelationDefinition_ConcreteEntities()
  {
		return (EReference)userRelationDefinitionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUserRelationDefinition_ClassicalExpressions()
  {
		return (EReference)userRelationDefinitionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRelation()
  {
		return relationEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelation_Type()
  {
		return (EReference)relationEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelation_Bindings()
  {
		return (EReference)relationEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EAttribute getRelation_IsAnAssertion()
  {
		return (EAttribute)relationEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getConditionalRelationDefinition()
  {
		return conditionalRelationDefinitionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getConditionalRelationDefinition_RelCases()
  {
		return (EReference)conditionalRelationDefinitionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getConditionalRelationDefinition_DefaultRelation()
  {
		return (EReference)conditionalRelationDefinitionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRelationLibrary()
  {
		return relationLibraryEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelationLibrary_RelationDefinitions()
  {
		return (EReference)relationLibraryEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelationLibrary_Elements()
  {
		return (EReference)relationLibraryEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelationLibrary_RelationDeclarations()
  {
		return (EReference)relationLibraryEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRelCase()
  {
		return relCaseEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelCase_Condition()
  {
		return (EReference)relCaseEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelCase_Relation()
  {
		return (EReference)relCaseEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getLibrary()
  {
		return libraryEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getLibrary_ExpressionLibraries()
  {
		return (EReference)libraryEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getLibrary_RelationLibraries()
  {
		return (EReference)libraryEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getLibrary_PredefinedTypes()
  {
		return (EReference)libraryEClass.getEStructuralFeatures().get(2);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getLibrary_Imports()
  {
		return (EReference)libraryEClass.getEStructuralFeatures().get(3);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getAbstractEntity()
  {
		return abstractEntityEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getAbstractEntity_Type()
  {
		return (EReference)abstractEntityEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractEntity_DesiredEventKind() {
		return (EAttribute)abstractEntityEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getExpressionDefinition()
  {
		return expressionDefinitionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExpressionDefinition_Declaration()
  {
		return (EReference)expressionDefinitionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBindableEntity()
  {
		return bindableEntityEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getConcreteEntity()
  {
		return concreteEntityEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRelationDeclaration()
  {
		return relationDeclarationEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRelationDeclaration_Parameters()
  {
		return (EReference)relationDeclarationEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getExpressionDeclaration()
  {
		return expressionDeclarationEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExpressionDeclaration_Parameters()
  {
		return (EReference)expressionDeclarationEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getExpressionDeclaration_ReturnType()
  {
		return (EReference)expressionDeclarationEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExternalRelationDefinition() {
		return externalRelationDefinitionEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExternalExpressionDefinition() {
		return externalExpressionDefinitionEClass;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ClockExpressionAndRelationFactory getClockExpressionAndRelationFactory()
  {
		return (ClockExpressionAndRelationFactory)getEFactoryInstance();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		expressionEClass = createEClass(EXPRESSION);
		createEReference(expressionEClass, EXPRESSION__TYPE);
		createEReference(expressionEClass, EXPRESSION__BINDINGS);

		userExpressionDefinitionEClass = createEClass(USER_EXPRESSION_DEFINITION);
		createEReference(userExpressionDefinitionEClass, USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES);
		createEReference(userExpressionDefinitionEClass, USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION);
		createEReference(userExpressionDefinitionEClass, USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS);

		expressionLibraryEClass = createEClass(EXPRESSION_LIBRARY);
		createEReference(expressionLibraryEClass, EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS);
		createEReference(expressionLibraryEClass, EXPRESSION_LIBRARY__ELEMENTS);
		createEReference(expressionLibraryEClass, EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS);

		bindingEClass = createEClass(BINDING);
		createEReference(bindingEClass, BINDING__ABSTRACT);
		createEReference(bindingEClass, BINDING__BINDABLE);

		conditionalExpressionDefinitionEClass = createEClass(CONDITIONAL_EXPRESSION_DEFINITION);
		createEReference(conditionalExpressionDefinitionEClass, CONDITIONAL_EXPRESSION_DEFINITION__EXPR_CASES);
		createEReference(conditionalExpressionDefinitionEClass, CONDITIONAL_EXPRESSION_DEFINITION__DEFAULT_EXPRESSION);

		exprCaseEClass = createEClass(EXPR_CASE);
		createEReference(exprCaseEClass, EXPR_CASE__CONDITION);
		createEReference(exprCaseEClass, EXPR_CASE__EXPRESSION);

		relationDefinitionEClass = createEClass(RELATION_DEFINITION);
		createEReference(relationDefinitionEClass, RELATION_DEFINITION__DECLARATION);

		userRelationDefinitionEClass = createEClass(USER_RELATION_DEFINITION);
		createEReference(userRelationDefinitionEClass, USER_RELATION_DEFINITION__CONCRETE_ENTITIES);
		createEReference(userRelationDefinitionEClass, USER_RELATION_DEFINITION__CLASSICAL_EXPRESSIONS);

		relationEClass = createEClass(RELATION);
		createEReference(relationEClass, RELATION__TYPE);
		createEReference(relationEClass, RELATION__BINDINGS);
		createEAttribute(relationEClass, RELATION__IS_AN_ASSERTION);

		conditionalRelationDefinitionEClass = createEClass(CONDITIONAL_RELATION_DEFINITION);
		createEReference(conditionalRelationDefinitionEClass, CONDITIONAL_RELATION_DEFINITION__REL_CASES);
		createEReference(conditionalRelationDefinitionEClass, CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION);

		relationLibraryEClass = createEClass(RELATION_LIBRARY);
		createEReference(relationLibraryEClass, RELATION_LIBRARY__RELATION_DEFINITIONS);
		createEReference(relationLibraryEClass, RELATION_LIBRARY__ELEMENTS);
		createEReference(relationLibraryEClass, RELATION_LIBRARY__RELATION_DECLARATIONS);

		relCaseEClass = createEClass(REL_CASE);
		createEReference(relCaseEClass, REL_CASE__CONDITION);
		createEReference(relCaseEClass, REL_CASE__RELATION);

		libraryEClass = createEClass(LIBRARY);
		createEReference(libraryEClass, LIBRARY__EXPRESSION_LIBRARIES);
		createEReference(libraryEClass, LIBRARY__RELATION_LIBRARIES);
		createEReference(libraryEClass, LIBRARY__PREDEFINED_TYPES);
		createEReference(libraryEClass, LIBRARY__IMPORTS);

		abstractEntityEClass = createEClass(ABSTRACT_ENTITY);
		createEReference(abstractEntityEClass, ABSTRACT_ENTITY__TYPE);
		createEAttribute(abstractEntityEClass, ABSTRACT_ENTITY__DESIRED_EVENT_KIND);

		expressionDefinitionEClass = createEClass(EXPRESSION_DEFINITION);
		createEReference(expressionDefinitionEClass, EXPRESSION_DEFINITION__DECLARATION);

		bindableEntityEClass = createEClass(BINDABLE_ENTITY);

		concreteEntityEClass = createEClass(CONCRETE_ENTITY);

		relationDeclarationEClass = createEClass(RELATION_DECLARATION);
		createEReference(relationDeclarationEClass, RELATION_DECLARATION__PARAMETERS);

		expressionDeclarationEClass = createEClass(EXPRESSION_DECLARATION);
		createEReference(expressionDeclarationEClass, EXPRESSION_DECLARATION__PARAMETERS);
		createEReference(expressionDeclarationEClass, EXPRESSION_DECLARATION__RETURN_TYPE);

		externalRelationDefinitionEClass = createEClass(EXTERNAL_RELATION_DEFINITION);

		externalExpressionDefinitionEClass = createEClass(EXTERNAL_EXPRESSION_DEFINITION);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		KernelExpressionPackage theKernelExpressionPackage = (KernelExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(KernelExpressionPackage.eNS_URI);
		KernelRelationPackage theKernelRelationPackage = (KernelRelationPackage)EPackage.Registry.INSTANCE.getEPackage(KernelRelationPackage.eNS_URI);
		ClassicalExpressionPackage theClassicalExpressionPackage = (ClassicalExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(ClassicalExpressionPackage.eNS_URI);
		TimeModelPackage theTimeModelPackage = (TimeModelPackage)EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		BasicTypePackage theBasicTypePackage = (BasicTypePackage)EPackage.Registry.INSTANCE.getEPackage(BasicTypePackage.eNS_URI);
		PrimitivesTypeValuePackage thePrimitivesTypeValuePackage = (PrimitivesTypeValuePackage)EPackage.Registry.INSTANCE.getEPackage(PrimitivesTypeValuePackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theKernelExpressionPackage);
		getESubpackages().add(theKernelRelationPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		expressionEClass.getESuperTypes().add(this.getConcreteEntity());
		userExpressionDefinitionEClass.getESuperTypes().add(this.getExpressionDefinition());
		expressionLibraryEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		conditionalExpressionDefinitionEClass.getESuperTypes().add(this.getUserExpressionDefinition());
		relationDefinitionEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		userRelationDefinitionEClass.getESuperTypes().add(this.getRelationDefinition());
		relationEClass.getESuperTypes().add(this.getConcreteEntity());
		conditionalRelationDefinitionEClass.getESuperTypes().add(this.getUserRelationDefinition());
		relationLibraryEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		libraryEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		abstractEntityEClass.getESuperTypes().add(this.getBindableEntity());
		expressionDefinitionEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		bindableEntityEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		concreteEntityEClass.getESuperTypes().add(this.getBindableEntity());
		relationDeclarationEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		expressionDeclarationEClass.getESuperTypes().add(theTimeModelPackage.getNamedElement());
		externalRelationDefinitionEClass.getESuperTypes().add(this.getRelationDefinition());
		externalExpressionDefinitionEClass.getESuperTypes().add(this.getExpressionDefinition());

		// Initialize classes and features; add operations and parameters
		initEClass(expressionEClass, Expression.class, "Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExpression_Type(), this.getExpressionDeclaration(), null, "type", null, 1, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExpression_Bindings(), this.getBinding(), null, "bindings", null, 0, -1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(userExpressionDefinitionEClass, UserExpressionDefinition.class, "UserExpressionDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUserExpressionDefinition_ConcreteEntities(), this.getConcreteEntity(), null, "concreteEntities", null, 0, -1, UserExpressionDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUserExpressionDefinition_RootExpression(), this.getExpression(), null, "rootExpression", null, 0, 1, UserExpressionDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUserExpressionDefinition_ClassicalExpressions(), theClassicalExpressionPackage.getClassicalExpression(), null, "classicalExpressions", null, 0, -1, UserExpressionDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expressionLibraryEClass, ExpressionLibrary.class, "ExpressionLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExpressionLibrary_ExpressionDefinitions(), this.getExpressionDefinition(), null, "expressionDefinitions", null, 0, -1, ExpressionLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExpressionLibrary_Elements(), theBasicTypePackage.getElement(), null, "elements", null, 0, -1, ExpressionLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExpressionLibrary_ExpressionDeclarations(), this.getExpressionDeclaration(), null, "expressionDeclarations", null, 0, -1, ExpressionLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bindingEClass, Binding.class, "Binding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinding_Abstract(), this.getAbstractEntity(), null, "abstract", null, 1, 1, Binding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinding_Bindable(), this.getBindableEntity(), null, "bindable", null, 1, 1, Binding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionalExpressionDefinitionEClass, ConditionalExpressionDefinition.class, "ConditionalExpressionDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConditionalExpressionDefinition_ExprCases(), this.getExprCase(), null, "exprCases", null, 1, -1, ConditionalExpressionDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConditionalExpressionDefinition_DefaultExpression(), this.getExpression(), null, "defaultExpression", null, 0, 1, ConditionalExpressionDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(exprCaseEClass, ExprCase.class, "ExprCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExprCase_Condition(), theClassicalExpressionPackage.getBooleanExpression(), null, "condition", null, 1, 1, ExprCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExprCase_Expression(), this.getExpression(), null, "expression", null, 1, 1, ExprCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relationDefinitionEClass, RelationDefinition.class, "RelationDefinition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelationDefinition_Declaration(), this.getRelationDeclaration(), null, "declaration", null, 1, 1, RelationDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(userRelationDefinitionEClass, UserRelationDefinition.class, "UserRelationDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUserRelationDefinition_ConcreteEntities(), this.getConcreteEntity(), null, "concreteEntities", null, 0, -1, UserRelationDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUserRelationDefinition_ClassicalExpressions(), theClassicalExpressionPackage.getClassicalExpression(), null, "classicalExpressions", null, 0, -1, UserRelationDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relationEClass, Relation.class, "Relation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelation_Type(), this.getRelationDeclaration(), null, "type", null, 1, 1, Relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRelation_Bindings(), this.getBinding(), null, "bindings", null, 1, -1, Relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRelation_IsAnAssertion(), thePrimitivesTypeValuePackage.getBoolean(), "isAnAssertion", "false", 0, 1, Relation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionalRelationDefinitionEClass, ConditionalRelationDefinition.class, "ConditionalRelationDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConditionalRelationDefinition_RelCases(), this.getRelCase(), null, "relCases", null, 1, -1, ConditionalRelationDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConditionalRelationDefinition_DefaultRelation(), this.getRelation(), null, "defaultRelation", null, 0, -1, ConditionalRelationDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relationLibraryEClass, RelationLibrary.class, "RelationLibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelationLibrary_RelationDefinitions(), this.getRelationDefinition(), null, "relationDefinitions", null, 0, -1, RelationLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRelationLibrary_Elements(), theBasicTypePackage.getElement(), null, "elements", null, 0, -1, RelationLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRelationLibrary_RelationDeclarations(), this.getRelationDeclaration(), null, "relationDeclarations", null, 0, -1, RelationLibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relCaseEClass, RelCase.class, "RelCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelCase_Condition(), theClassicalExpressionPackage.getBooleanExpression(), null, "condition", null, 1, 1, RelCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRelCase_Relation(), this.getRelation(), null, "relation", null, 1, -1, RelCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(libraryEClass, Library.class, "Library", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLibrary_ExpressionLibraries(), this.getExpressionLibrary(), null, "expressionLibraries", null, 0, -1, Library.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLibrary_RelationLibraries(), this.getRelationLibrary(), null, "relationLibraries", null, 0, -1, Library.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLibrary_PredefinedTypes(), theBasicTypePackage.getType(), null, "predefinedTypes", null, 0, -1, Library.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLibrary_Imports(), theTimeModelPackage.getImportStatement(), null, "imports", null, 0, -1, Library.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractEntityEClass, AbstractEntity.class, "AbstractEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractEntity_Type(), theBasicTypePackage.getType(), null, "type", null, 1, 1, AbstractEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractEntity_DesiredEventKind(), theTimeModelPackage.getEventKind(), "desiredEventKind", null, 0, 1, AbstractEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expressionDefinitionEClass, ExpressionDefinition.class, "ExpressionDefinition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExpressionDefinition_Declaration(), this.getExpressionDeclaration(), null, "declaration", null, 1, 1, ExpressionDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bindableEntityEClass, BindableEntity.class, "BindableEntity", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(concreteEntityEClass, ConcreteEntity.class, "ConcreteEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(relationDeclarationEClass, RelationDeclaration.class, "RelationDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelationDeclaration_Parameters(), this.getAbstractEntity(), null, "parameters", null, 0, -1, RelationDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expressionDeclarationEClass, ExpressionDeclaration.class, "ExpressionDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExpressionDeclaration_Parameters(), this.getAbstractEntity(), null, "parameters", null, 0, -1, ExpressionDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExpressionDeclaration_ReturnType(), theBasicTypePackage.getType(), null, "returnType", null, 1, 1, ExpressionDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(externalRelationDefinitionEClass, ExternalRelationDefinition.class, "ExternalRelationDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(externalExpressionDefinitionEClass, ExternalExpressionDefinition.class, "ExternalExpressionDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //ClockExpressionAndRelationPackageImpl
