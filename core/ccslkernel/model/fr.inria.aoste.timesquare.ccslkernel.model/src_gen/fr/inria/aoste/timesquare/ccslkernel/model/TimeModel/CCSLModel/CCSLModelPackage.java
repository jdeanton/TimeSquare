/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelFactory
 * @model kind="package"
 * @generated
 */
public interface CCSLModelPackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNAME = "CCSLModel";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_URI = "http://fr.inria.aoste.timemodel.ccslmodel";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_PREFIX = "fr.inria.aoste.timemodel.ccslmodel";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  CCSLModelPackage eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl.init();

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl#getBlock()
	 * @generated
	 */
  int BLOCK = 1;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BLOCK__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Sub Block</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BLOCK__SUB_BLOCK = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BLOCK__CONTAINER = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BLOCK__ELEMENTS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

  /**
	 * The feature id for the '<em><b>Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BLOCK__RELATIONS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

  /**
	 * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BLOCK__EXPRESSIONS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

  /**
	 * The feature id for the '<em><b>Classical Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BLOCK__CLASSICAL_EXPRESSION = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 5;

  /**
	 * The feature id for the '<em><b>Initial Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__INITIAL_BLOCK = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 6;

		/**
	 * The feature id for the '<em><b>Block Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__BLOCK_TRANSITIONS = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 7;

		/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int BLOCK_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 8;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.ClockConstraintSystemImpl <em>Clock Constraint System</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.ClockConstraintSystemImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl#getClockConstraintSystem()
	 * @generated
	 */
  int CLOCK_CONSTRAINT_SYSTEM = 0;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__NAME = BLOCK__NAME;

  /**
	 * The feature id for the '<em><b>Sub Block</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__SUB_BLOCK = BLOCK__SUB_BLOCK;

  /**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__CONTAINER = BLOCK__CONTAINER;

  /**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__ELEMENTS = BLOCK__ELEMENTS;

  /**
	 * The feature id for the '<em><b>Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__RELATIONS = BLOCK__RELATIONS;

  /**
	 * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__EXPRESSIONS = BLOCK__EXPRESSIONS;

  /**
	 * The feature id for the '<em><b>Classical Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__CLASSICAL_EXPRESSION = BLOCK__CLASSICAL_EXPRESSION;

  /**
	 * The feature id for the '<em><b>Initial Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOCK_CONSTRAINT_SYSTEM__INITIAL_BLOCK = BLOCK__INITIAL_BLOCK;

		/**
	 * The feature id for the '<em><b>Block Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOCK_CONSTRAINT_SYSTEM__BLOCK_TRANSITIONS = BLOCK__BLOCK_TRANSITIONS;

		/**
	 * The feature id for the '<em><b>Super Block</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK = BLOCK_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Data Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES = BLOCK_FEATURE_COUNT + 1;

		/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM__IMPORTS = BLOCK_FEATURE_COUNT + 2;

		/**
	 * The number of structural features of the '<em>Clock Constraint System</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_CONSTRAINT_SYSTEM_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 3;


  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockTransitionImpl <em>Block Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockTransitionImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl#getBlockTransition()
	 * @generated
	 */
	int BLOCK_TRANSITION = 2;

		/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TRANSITION__NAME = TimeModelPackage.NAMED_ELEMENT__NAME;

		/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TRANSITION__SOURCE = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TRANSITION__TARGET = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

		/**
	 * The feature id for the '<em><b>On</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TRANSITION__ON = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

		/**
	 * The number of structural features of the '<em>Block Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TRANSITION_FEATURE_COUNT = TimeModelPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;


		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem <em>Clock Constraint System</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clock Constraint System</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem
	 * @generated
	 */
  EClass getClockConstraintSystem();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem#getSuperBlock <em>Super Block</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Block</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem#getSuperBlock()
	 * @see #getClockConstraintSystem()
	 * @generated
	 */
  EReference getClockConstraintSystem_SuperBlock();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem#getDataTypes <em>Data Types</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Types</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem#getDataTypes()
	 * @see #getClockConstraintSystem()
	 * @generated
	 */
  EReference getClockConstraintSystem_DataTypes();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem#getImports()
	 * @see #getClockConstraintSystem()
	 * @generated
	 */
  EReference getClockConstraintSystem_Imports();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block
	 * @generated
	 */
  EClass getBlock();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getSubBlock <em>Sub Block</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Block</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getSubBlock()
	 * @see #getBlock()
	 * @generated
	 */
  EReference getBlock_SubBlock();

  /**
	 * Returns the meta object for the container reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getContainer()
	 * @see #getBlock()
	 * @generated
	 */
  EReference getBlock_Container();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getElements()
	 * @see #getBlock()
	 * @generated
	 */
  EReference getBlock_Elements();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getRelations <em>Relations</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getRelations()
	 * @see #getBlock()
	 * @generated
	 */
  EReference getBlock_Relations();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expressions</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getExpressions()
	 * @see #getBlock()
	 * @generated
	 */
  EReference getBlock_Expressions();

  /**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getClassicalExpression <em>Classical Expression</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classical Expression</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getClassicalExpression()
	 * @see #getBlock()
	 * @generated
	 */
  EReference getBlock_ClassicalExpression();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getInitialBlock <em>Initial Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial Block</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getInitialBlock()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_InitialBlock();

		/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getBlockTransitions <em>Block Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Block Transitions</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block#getBlockTransitions()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_BlockTransitions();

		/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition <em>Block Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Transition</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition
	 * @generated
	 */
	EClass getBlockTransition();

		/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition#getSource()
	 * @see #getBlockTransition()
	 * @generated
	 */
	EReference getBlockTransition_Source();

		/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition#getTarget()
	 * @see #getBlockTransition()
	 * @generated
	 */
	EReference getBlockTransition_Target();

		/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition#getOn <em>On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition#getOn()
	 * @see #getBlockTransition()
	 * @generated
	 */
	EReference getBlockTransition_On();

		/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
  CCSLModelFactory getCCSLModelFactory();

  /**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.ClockConstraintSystemImpl <em>Clock Constraint System</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.ClockConstraintSystemImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl#getClockConstraintSystem()
		 * @generated
		 */
    EClass CLOCK_CONSTRAINT_SYSTEM = eINSTANCE.getClockConstraintSystem();

    /**
		 * The meta object literal for the '<em><b>Super Block</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK = eINSTANCE.getClockConstraintSystem_SuperBlock();

    /**
		 * The meta object literal for the '<em><b>Data Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES = eINSTANCE.getClockConstraintSystem_DataTypes();

    /**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CLOCK_CONSTRAINT_SYSTEM__IMPORTS = eINSTANCE.getClockConstraintSystem_Imports();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl#getBlock()
		 * @generated
		 */
    EClass BLOCK = eINSTANCE.getBlock();

    /**
		 * The meta object literal for the '<em><b>Sub Block</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BLOCK__SUB_BLOCK = eINSTANCE.getBlock_SubBlock();

    /**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BLOCK__CONTAINER = eINSTANCE.getBlock_Container();

    /**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BLOCK__ELEMENTS = eINSTANCE.getBlock_Elements();

    /**
		 * The meta object literal for the '<em><b>Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BLOCK__RELATIONS = eINSTANCE.getBlock_Relations();

    /**
		 * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BLOCK__EXPRESSIONS = eINSTANCE.getBlock_Expressions();

    /**
		 * The meta object literal for the '<em><b>Classical Expression</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference BLOCK__CLASSICAL_EXPRESSION = eINSTANCE.getBlock_ClassicalExpression();

				/**
		 * The meta object literal for the '<em><b>Initial Block</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__INITIAL_BLOCK = eINSTANCE.getBlock_InitialBlock();

				/**
		 * The meta object literal for the '<em><b>Block Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__BLOCK_TRANSITIONS = eINSTANCE.getBlock_BlockTransitions();

				/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockTransitionImpl <em>Block Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.BlockTransitionImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl#getBlockTransition()
		 * @generated
		 */
		EClass BLOCK_TRANSITION = eINSTANCE.getBlockTransition();

				/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TRANSITION__SOURCE = eINSTANCE.getBlockTransition_Source();

				/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TRANSITION__TARGET = eINSTANCE.getBlockTransition_Target();

				/**
		 * The meta object literal for the '<em><b>On</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TRANSITION__ON = eINSTANCE.getBlockTransition_On();

  }

} //CCSLModelPackage
