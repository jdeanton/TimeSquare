/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Discretization</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DiscretizationImpl#getDenseClock <em>Dense Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DiscretizationImpl#getDiscretizationFactor <em>Discretization Factor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DiscretizationImpl extends KernelExpressionDeclarationImpl implements Discretization
{
  /**
	 * The cached value of the '{@link #getDenseClock() <em>Dense Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getDenseClock()
	 * @generated
	 * @ordered
	 */
  protected AbstractEntity denseClock;

  /**
	 * The cached value of the '{@link #getDiscretizationFactor() <em>Discretization Factor</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getDiscretizationFactor()
	 * @generated
	 * @ordered
	 */
  protected AbstractEntity discretizationFactor;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected DiscretizationImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return KernelExpressionPackage.Literals.DISCRETIZATION;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public AbstractEntity getDenseClock()
  {
		return denseClock;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetDenseClock(AbstractEntity newDenseClock, NotificationChain msgs)
  {
		AbstractEntity oldDenseClock = denseClock;
		denseClock = newDenseClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK, oldDenseClock, newDenseClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setDenseClock(AbstractEntity newDenseClock)
  {
		if (newDenseClock != denseClock) {
			NotificationChain msgs = null;
			if (denseClock != null)
				msgs = ((InternalEObject)denseClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK, null, msgs);
			if (newDenseClock != null)
				msgs = ((InternalEObject)newDenseClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK, null, msgs);
			msgs = basicSetDenseClock(newDenseClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK, newDenseClock, newDenseClock));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public AbstractEntity getDiscretizationFactor()
  {
		return discretizationFactor;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetDiscretizationFactor(AbstractEntity newDiscretizationFactor, NotificationChain msgs)
  {
		AbstractEntity oldDiscretizationFactor = discretizationFactor;
		discretizationFactor = newDiscretizationFactor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR, oldDiscretizationFactor, newDiscretizationFactor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setDiscretizationFactor(AbstractEntity newDiscretizationFactor)
  {
		if (newDiscretizationFactor != discretizationFactor) {
			NotificationChain msgs = null;
			if (discretizationFactor != null)
				msgs = ((InternalEObject)discretizationFactor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR, null, msgs);
			if (newDiscretizationFactor != null)
				msgs = ((InternalEObject)newDiscretizationFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR, null, msgs);
			msgs = basicSetDiscretizationFactor(newDiscretizationFactor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR, newDiscretizationFactor, newDiscretizationFactor));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK:
				return basicSetDenseClock(null, msgs);
			case KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR:
				return basicSetDiscretizationFactor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK:
				return getDenseClock();
			case KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR:
				return getDiscretizationFactor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK:
				setDenseClock((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR:
				setDiscretizationFactor((AbstractEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK:
				setDenseClock((AbstractEntity)null);
				return;
			case KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR:
				setDiscretizationFactor((AbstractEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case KernelExpressionPackage.DISCRETIZATION__DENSE_CLOCK:
				return denseClock != null;
			case KernelExpressionPackage.DISCRETIZATION__DISCRETIZATION_FACTOR:
				return discretizationFactor != null;
		}
		return super.eIsSet(featureID);
	}

} //DiscretizationImpl
