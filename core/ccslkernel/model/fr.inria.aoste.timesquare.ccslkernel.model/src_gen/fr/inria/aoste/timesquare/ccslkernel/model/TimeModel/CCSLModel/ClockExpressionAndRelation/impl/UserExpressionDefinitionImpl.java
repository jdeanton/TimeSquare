/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Expression Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserExpressionDefinitionImpl#getConcreteEntities <em>Concrete Entities</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserExpressionDefinitionImpl#getRootExpression <em>Root Expression</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.UserExpressionDefinitionImpl#getClassicalExpressions <em>Classical Expressions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UserExpressionDefinitionImpl extends ExpressionDefinitionImpl implements UserExpressionDefinition
{
  /**
	 * The cached value of the '{@link #getConcreteEntities() <em>Concrete Entities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getConcreteEntities()
	 * @generated
	 * @ordered
	 */
  protected EList<ConcreteEntity> concreteEntities;

  /**
	 * The cached value of the '{@link #getRootExpression() <em>Root Expression</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getRootExpression()
	 * @generated
	 * @ordered
	 */
  protected Expression rootExpression;

  /**
	 * The cached value of the '{@link #getClassicalExpressions() <em>Classical Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getClassicalExpressions()
	 * @generated
	 * @ordered
	 */
  protected EList<ClassicalExpression> classicalExpressions;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected UserExpressionDefinitionImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ConcreteEntity> getConcreteEntities()
  {
		if (concreteEntities == null) {
			concreteEntities = new EObjectContainmentEList<ConcreteEntity>(ConcreteEntity.class, this, ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES);
		}
		return concreteEntities;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Expression getRootExpression()
  {
		if (rootExpression != null && rootExpression.eIsProxy()) {
			InternalEObject oldRootExpression = (InternalEObject)rootExpression;
			rootExpression = (Expression)eResolveProxy(oldRootExpression);
			if (rootExpression != oldRootExpression) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION, oldRootExpression, rootExpression));
			}
		}
		return rootExpression;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public Expression basicGetRootExpression()
  {
		return rootExpression;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setRootExpression(Expression newRootExpression)
  {
		Expression oldRootExpression = rootExpression;
		rootExpression = newRootExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION, oldRootExpression, rootExpression));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ClassicalExpression> getClassicalExpressions()
  {
		if (classicalExpressions == null) {
			classicalExpressions = new EObjectContainmentEList<ClassicalExpression>(ClassicalExpression.class, this, ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS);
		}
		return classicalExpressions;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES:
				return ((InternalEList<?>)getConcreteEntities()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS:
				return ((InternalEList<?>)getClassicalExpressions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES:
				return getConcreteEntities();
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION:
				if (resolve) return getRootExpression();
				return basicGetRootExpression();
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS:
				return getClassicalExpressions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES:
				getConcreteEntities().clear();
				getConcreteEntities().addAll((Collection<? extends ConcreteEntity>)newValue);
				return;
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION:
				setRootExpression((Expression)newValue);
				return;
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS:
				getClassicalExpressions().clear();
				getClassicalExpressions().addAll((Collection<? extends ClassicalExpression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES:
				getConcreteEntities().clear();
				return;
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION:
				setRootExpression((Expression)null);
				return;
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS:
				getClassicalExpressions().clear();
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES:
				return concreteEntities != null && !concreteEntities.isEmpty();
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION:
				return rootExpression != null;
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS:
				return classicalExpressions != null && !classicalExpressions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //UserExpressionDefinitionImpl
