/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clock</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock#getTickingEvent <em>Ticking Event</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock#getDefiningEvent <em>Defining Event</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage#getClock()
 * @model
 * @generated
 */
public interface Clock extends Element
{
  /**
	 * Returns the value of the '<em><b>Ticking Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ticking Event</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Ticking Event</em>' containment reference.
	 * @see #setTickingEvent(Event)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage#getClock_TickingEvent()
	 * @model containment="true"
	 * @generated
	 */
  Event getTickingEvent();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock#getTickingEvent <em>Ticking Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ticking Event</em>' containment reference.
	 * @see #getTickingEvent()
	 * @generated
	 */
  void setTickingEvent(Event value);

  /**
	 * Returns the value of the '<em><b>Defining Event</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Defining Event</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Event</em>' reference.
	 * @see #setDefiningEvent(Event)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage#getClock_DefiningEvent()
	 * @model
	 * @generated
	 */
  Event getDefiningEvent();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock#getDefiningEvent <em>Defining Event</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Event</em>' reference.
	 * @see #getDefiningEvent()
	 * @generated
	 */
  void setDefiningEvent(Event value);

} // Clock
