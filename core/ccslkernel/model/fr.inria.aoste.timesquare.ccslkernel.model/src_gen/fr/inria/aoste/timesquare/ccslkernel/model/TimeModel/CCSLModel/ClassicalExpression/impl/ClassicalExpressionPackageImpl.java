/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntDivide;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqDecr;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnarySeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Xor;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.KernelExpressionPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.impl.KernelRelationPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ClockExpressionAndRelationPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl.CCSLModelPackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.PrimitivesTypeValuePackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassicalExpressionPackageImpl extends EPackageImpl implements ClassicalExpressionPackage
{
  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass classicalExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass booleanExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass integerExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass binaryBooleanExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass unaryBooleanExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass binaryRealExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass unaryRealExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass binaryIntegerExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass unaryIntegerExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass booleanRefEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realRefEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass integerRefEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass unaryRealPlusEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass unaryRealMinusEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realPlusEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realMinusEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realMultiplyEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass unaryIntPlusEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass unaryIntMinusEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass intPlusEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass intMinusEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass intMultiplyEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass intDivideEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass notEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass andEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass orEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass xorEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realEqualEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realInfEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realSupEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass intEqualEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass intInfEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass intSupEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass seqIsEmptyEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass unarySeqExpressionEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass seqGetTailEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass seqGetHeadEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass seqDecrEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass seqSchedEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass booleanVariableRefEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass integerVariableRefEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass realVariableRefEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass numberSeqRefEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass numberSeqVariableRefEClass = null;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private EClass seqExpressionEClass = null;

  /**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private ClassicalExpressionPackageImpl()
  {
		super(eNS_URI, ClassicalExpressionFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ClassicalExpressionPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static ClassicalExpressionPackage init()
  {
		if (isInited) return (ClassicalExpressionPackage)EPackage.Registry.INSTANCE.getEPackage(ClassicalExpressionPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredClassicalExpressionPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ClassicalExpressionPackageImpl theClassicalExpressionPackage = registeredClassicalExpressionPackage instanceof ClassicalExpressionPackageImpl ? (ClassicalExpressionPackageImpl)registeredClassicalExpressionPackage : new ClassicalExpressionPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		TimeModelPackageImpl theTimeModelPackage = (TimeModelPackageImpl)(registeredPackage instanceof TimeModelPackageImpl ? registeredPackage : TimeModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CCSLModelPackage.eNS_URI);
		CCSLModelPackageImpl theCCSLModelPackage = (CCSLModelPackageImpl)(registeredPackage instanceof CCSLModelPackageImpl ? registeredPackage : CCSLModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);
		ClockExpressionAndRelationPackageImpl theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackageImpl)(registeredPackage instanceof ClockExpressionAndRelationPackageImpl ? registeredPackage : ClockExpressionAndRelationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelExpressionPackage.eNS_URI);
		KernelExpressionPackageImpl theKernelExpressionPackage = (KernelExpressionPackageImpl)(registeredPackage instanceof KernelExpressionPackageImpl ? registeredPackage : KernelExpressionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(KernelRelationPackage.eNS_URI);
		KernelRelationPackageImpl theKernelRelationPackage = (KernelRelationPackageImpl)(registeredPackage instanceof KernelRelationPackageImpl ? registeredPackage : KernelRelationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BasicTypePackage.eNS_URI);
		BasicTypePackageImpl theBasicTypePackage = (BasicTypePackageImpl)(registeredPackage instanceof BasicTypePackageImpl ? registeredPackage : BasicTypePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PrimitivesTypeValuePackage.eNS_URI);
		PrimitivesTypeValuePackageImpl thePrimitivesTypeValuePackage = (PrimitivesTypeValuePackageImpl)(registeredPackage instanceof PrimitivesTypeValuePackageImpl ? registeredPackage : PrimitivesTypeValuePackage.eINSTANCE);

		// Create package meta-data objects
		theClassicalExpressionPackage.createPackageContents();
		theTimeModelPackage.createPackageContents();
		theCCSLModelPackage.createPackageContents();
		theClockExpressionAndRelationPackage.createPackageContents();
		theKernelExpressionPackage.createPackageContents();
		theKernelRelationPackage.createPackageContents();
		theBasicTypePackage.createPackageContents();
		thePrimitivesTypeValuePackage.createPackageContents();

		// Initialize created meta-data
		theClassicalExpressionPackage.initializePackageContents();
		theTimeModelPackage.initializePackageContents();
		theCCSLModelPackage.initializePackageContents();
		theClockExpressionAndRelationPackage.initializePackageContents();
		theKernelExpressionPackage.initializePackageContents();
		theKernelRelationPackage.initializePackageContents();
		theBasicTypePackage.initializePackageContents();
		thePrimitivesTypeValuePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClassicalExpressionPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClassicalExpressionPackage.eNS_URI, theClassicalExpressionPackage);
		return theClassicalExpressionPackage;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getClassicalExpression()
  {
		return classicalExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBooleanExpression()
  {
		return booleanExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealExpression()
  {
		return realExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntegerExpression()
  {
		return integerExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBinaryBooleanExpression()
  {
		return binaryBooleanExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBinaryBooleanExpression_LeftValue()
  {
		return (EReference)binaryBooleanExpressionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBinaryBooleanExpression_RightValue()
  {
		return (EReference)binaryBooleanExpressionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUnaryBooleanExpression()
  {
		return unaryBooleanExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUnaryBooleanExpression_Operand()
  {
		return (EReference)unaryBooleanExpressionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBinaryRealExpression()
  {
		return binaryRealExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBinaryRealExpression_LeftValue()
  {
		return (EReference)binaryRealExpressionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBinaryRealExpression_RightValue()
  {
		return (EReference)binaryRealExpressionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUnaryRealExpression()
  {
		return unaryRealExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUnaryRealExpression_Value()
  {
		return (EReference)unaryRealExpressionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUnaryRealExpression_Operand()
  {
		return (EReference)unaryRealExpressionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBinaryIntegerExpression()
  {
		return binaryIntegerExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBinaryIntegerExpression_LeftValue()
  {
		return (EReference)binaryIntegerExpressionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBinaryIntegerExpression_RightValue()
  {
		return (EReference)binaryIntegerExpressionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUnaryIntegerExpression()
  {
		return unaryIntegerExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUnaryIntegerExpression_Value()
  {
		return (EReference)unaryIntegerExpressionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUnaryIntegerExpression_Operand()
  {
		return (EReference)unaryIntegerExpressionEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBooleanRef()
  {
		return booleanRefEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBooleanRef_ReferencedBool()
  {
		return (EReference)booleanRefEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealRef()
  {
		return realRefEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRealRef_RealElem()
  {
		return (EReference)realRefEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntegerRef()
  {
		return integerRefEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getIntegerRef_IntegerElem()
  {
		return (EReference)integerRefEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUnaryRealPlus()
  {
		return unaryRealPlusEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUnaryRealMinus()
  {
		return unaryRealMinusEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealPlus()
  {
		return realPlusEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealMinus()
  {
		return realMinusEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealMultiply()
  {
		return realMultiplyEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUnaryIntPlus()
  {
		return unaryIntPlusEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUnaryIntMinus()
  {
		return unaryIntMinusEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntPlus()
  {
		return intPlusEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntMinus()
  {
		return intMinusEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntMultiply()
  {
		return intMultiplyEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntDivide()
  {
		return intDivideEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getNot()
  {
		return notEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getAnd()
  {
		return andEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getOr()
  {
		return orEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getXor()
  {
		return xorEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealEqual()
  {
		return realEqualEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRealEqual_LeftValue()
  {
		return (EReference)realEqualEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRealEqual_RightValue()
  {
		return (EReference)realEqualEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealInf()
  {
		return realInfEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRealInf_LeftValue()
  {
		return (EReference)realInfEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRealInf_RightValue()
  {
		return (EReference)realInfEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealSup()
  {
		return realSupEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRealSup_LeftValue()
  {
		return (EReference)realSupEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRealSup_RightValue()
  {
		return (EReference)realSupEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntEqual()
  {
		return intEqualEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getIntEqual_LeftValue()
  {
		return (EReference)intEqualEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getIntEqual_RightValue()
  {
		return (EReference)intEqualEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntInf()
  {
		return intInfEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getIntInf_LeftValue()
  {
		return (EReference)intInfEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getIntInf_RightValue()
  {
		return (EReference)intInfEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntSup()
  {
		return intSupEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getIntSup_LeftValue()
  {
		return (EReference)intSupEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getIntSup_RightValue()
  {
		return (EReference)intSupEClass.getEStructuralFeatures().get(1);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSeqIsEmpty()
  {
		return seqIsEmptyEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getSeqIsEmpty_Operand()
  {
		return (EReference)seqIsEmptyEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getUnarySeqExpression()
  {
		return unarySeqExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getUnarySeqExpression_Operand()
  {
		return (EReference)unarySeqExpressionEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSeqGetTail()
  {
		return seqGetTailEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSeqGetHead()
  {
		return seqGetHeadEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getSeqGetHead_Operand()
  {
		return (EReference)seqGetHeadEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSeqDecr()
  {
		return seqDecrEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSeqSched()
  {
		return seqSchedEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getSeqSched_IntegerExpr()
  {
		return (EReference)seqSchedEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getBooleanVariableRef()
  {
		return booleanVariableRefEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getBooleanVariableRef_ReferencedVar()
  {
		return (EReference)booleanVariableRefEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getIntegerVariableRef()
  {
		return integerVariableRefEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getIntegerVariableRef_ReferencedVar()
  {
		return (EReference)integerVariableRefEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getRealVariableRef()
  {
		return realVariableRefEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getRealVariableRef_ReferencedVar()
  {
		return (EReference)realVariableRefEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getNumberSeqRef()
  {
		return numberSeqRefEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getNumberSeqRef_ReferencedNumberSeq()
  {
		return (EReference)numberSeqRefEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getNumberSeqVariableRef()
  {
		return numberSeqVariableRefEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EReference getNumberSeqVariableRef_ReferencedVar()
  {
		return (EReference)numberSeqVariableRefEClass.getEStructuralFeatures().get(0);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EClass getSeqExpression()
  {
		return seqExpressionEClass;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ClassicalExpressionFactory getClassicalExpressionFactory()
  {
		return (ClassicalExpressionFactory)getEFactoryInstance();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		classicalExpressionEClass = createEClass(CLASSICAL_EXPRESSION);

		booleanExpressionEClass = createEClass(BOOLEAN_EXPRESSION);

		realExpressionEClass = createEClass(REAL_EXPRESSION);

		integerExpressionEClass = createEClass(INTEGER_EXPRESSION);

		binaryBooleanExpressionEClass = createEClass(BINARY_BOOLEAN_EXPRESSION);
		createEReference(binaryBooleanExpressionEClass, BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE);
		createEReference(binaryBooleanExpressionEClass, BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE);

		unaryBooleanExpressionEClass = createEClass(UNARY_BOOLEAN_EXPRESSION);
		createEReference(unaryBooleanExpressionEClass, UNARY_BOOLEAN_EXPRESSION__OPERAND);

		binaryRealExpressionEClass = createEClass(BINARY_REAL_EXPRESSION);
		createEReference(binaryRealExpressionEClass, BINARY_REAL_EXPRESSION__LEFT_VALUE);
		createEReference(binaryRealExpressionEClass, BINARY_REAL_EXPRESSION__RIGHT_VALUE);

		unaryRealExpressionEClass = createEClass(UNARY_REAL_EXPRESSION);
		createEReference(unaryRealExpressionEClass, UNARY_REAL_EXPRESSION__VALUE);
		createEReference(unaryRealExpressionEClass, UNARY_REAL_EXPRESSION__OPERAND);

		binaryIntegerExpressionEClass = createEClass(BINARY_INTEGER_EXPRESSION);
		createEReference(binaryIntegerExpressionEClass, BINARY_INTEGER_EXPRESSION__LEFT_VALUE);
		createEReference(binaryIntegerExpressionEClass, BINARY_INTEGER_EXPRESSION__RIGHT_VALUE);

		unaryIntegerExpressionEClass = createEClass(UNARY_INTEGER_EXPRESSION);
		createEReference(unaryIntegerExpressionEClass, UNARY_INTEGER_EXPRESSION__VALUE);
		createEReference(unaryIntegerExpressionEClass, UNARY_INTEGER_EXPRESSION__OPERAND);

		booleanRefEClass = createEClass(BOOLEAN_REF);
		createEReference(booleanRefEClass, BOOLEAN_REF__REFERENCED_BOOL);

		realRefEClass = createEClass(REAL_REF);
		createEReference(realRefEClass, REAL_REF__REAL_ELEM);

		integerRefEClass = createEClass(INTEGER_REF);
		createEReference(integerRefEClass, INTEGER_REF__INTEGER_ELEM);

		unaryRealPlusEClass = createEClass(UNARY_REAL_PLUS);

		unaryRealMinusEClass = createEClass(UNARY_REAL_MINUS);

		realPlusEClass = createEClass(REAL_PLUS);

		realMinusEClass = createEClass(REAL_MINUS);

		realMultiplyEClass = createEClass(REAL_MULTIPLY);

		unaryIntPlusEClass = createEClass(UNARY_INT_PLUS);

		unaryIntMinusEClass = createEClass(UNARY_INT_MINUS);

		intPlusEClass = createEClass(INT_PLUS);

		intMinusEClass = createEClass(INT_MINUS);

		intMultiplyEClass = createEClass(INT_MULTIPLY);

		intDivideEClass = createEClass(INT_DIVIDE);

		notEClass = createEClass(NOT);

		andEClass = createEClass(AND);

		orEClass = createEClass(OR);

		xorEClass = createEClass(XOR);

		realEqualEClass = createEClass(REAL_EQUAL);
		createEReference(realEqualEClass, REAL_EQUAL__LEFT_VALUE);
		createEReference(realEqualEClass, REAL_EQUAL__RIGHT_VALUE);

		realInfEClass = createEClass(REAL_INF);
		createEReference(realInfEClass, REAL_INF__LEFT_VALUE);
		createEReference(realInfEClass, REAL_INF__RIGHT_VALUE);

		realSupEClass = createEClass(REAL_SUP);
		createEReference(realSupEClass, REAL_SUP__LEFT_VALUE);
		createEReference(realSupEClass, REAL_SUP__RIGHT_VALUE);

		intEqualEClass = createEClass(INT_EQUAL);
		createEReference(intEqualEClass, INT_EQUAL__LEFT_VALUE);
		createEReference(intEqualEClass, INT_EQUAL__RIGHT_VALUE);

		intInfEClass = createEClass(INT_INF);
		createEReference(intInfEClass, INT_INF__LEFT_VALUE);
		createEReference(intInfEClass, INT_INF__RIGHT_VALUE);

		intSupEClass = createEClass(INT_SUP);
		createEReference(intSupEClass, INT_SUP__LEFT_VALUE);
		createEReference(intSupEClass, INT_SUP__RIGHT_VALUE);

		seqIsEmptyEClass = createEClass(SEQ_IS_EMPTY);
		createEReference(seqIsEmptyEClass, SEQ_IS_EMPTY__OPERAND);

		unarySeqExpressionEClass = createEClass(UNARY_SEQ_EXPRESSION);
		createEReference(unarySeqExpressionEClass, UNARY_SEQ_EXPRESSION__OPERAND);

		seqGetTailEClass = createEClass(SEQ_GET_TAIL);

		seqGetHeadEClass = createEClass(SEQ_GET_HEAD);
		createEReference(seqGetHeadEClass, SEQ_GET_HEAD__OPERAND);

		seqDecrEClass = createEClass(SEQ_DECR);

		seqSchedEClass = createEClass(SEQ_SCHED);
		createEReference(seqSchedEClass, SEQ_SCHED__INTEGER_EXPR);

		booleanVariableRefEClass = createEClass(BOOLEAN_VARIABLE_REF);
		createEReference(booleanVariableRefEClass, BOOLEAN_VARIABLE_REF__REFERENCED_VAR);

		integerVariableRefEClass = createEClass(INTEGER_VARIABLE_REF);
		createEReference(integerVariableRefEClass, INTEGER_VARIABLE_REF__REFERENCED_VAR);

		realVariableRefEClass = createEClass(REAL_VARIABLE_REF);
		createEReference(realVariableRefEClass, REAL_VARIABLE_REF__REFERENCED_VAR);

		numberSeqRefEClass = createEClass(NUMBER_SEQ_REF);
		createEReference(numberSeqRefEClass, NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ);

		numberSeqVariableRefEClass = createEClass(NUMBER_SEQ_VARIABLE_REF);
		createEReference(numberSeqVariableRefEClass, NUMBER_SEQ_VARIABLE_REF__REFERENCED_VAR);

		seqExpressionEClass = createEClass(SEQ_EXPRESSION);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BasicTypePackage theBasicTypePackage = (BasicTypePackage)EPackage.Registry.INSTANCE.getEPackage(BasicTypePackage.eNS_URI);
		ClockExpressionAndRelationPackage theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackage)EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		classicalExpressionEClass.getESuperTypes().add(theBasicTypePackage.getPrimitiveElement());
		booleanExpressionEClass.getESuperTypes().add(this.getClassicalExpression());
		realExpressionEClass.getESuperTypes().add(this.getClassicalExpression());
		integerExpressionEClass.getESuperTypes().add(this.getClassicalExpression());
		binaryBooleanExpressionEClass.getESuperTypes().add(this.getBooleanExpression());
		unaryBooleanExpressionEClass.getESuperTypes().add(this.getBooleanExpression());
		binaryRealExpressionEClass.getESuperTypes().add(this.getRealExpression());
		unaryRealExpressionEClass.getESuperTypes().add(this.getRealExpression());
		binaryIntegerExpressionEClass.getESuperTypes().add(this.getIntegerExpression());
		unaryIntegerExpressionEClass.getESuperTypes().add(this.getIntegerExpression());
		booleanRefEClass.getESuperTypes().add(this.getBooleanExpression());
		realRefEClass.getESuperTypes().add(this.getRealExpression());
		integerRefEClass.getESuperTypes().add(this.getIntegerExpression());
		unaryRealPlusEClass.getESuperTypes().add(this.getUnaryRealExpression());
		unaryRealMinusEClass.getESuperTypes().add(this.getUnaryRealExpression());
		realPlusEClass.getESuperTypes().add(this.getBinaryRealExpression());
		realMinusEClass.getESuperTypes().add(this.getBinaryRealExpression());
		realMultiplyEClass.getESuperTypes().add(this.getBinaryRealExpression());
		unaryIntPlusEClass.getESuperTypes().add(this.getUnaryIntegerExpression());
		unaryIntMinusEClass.getESuperTypes().add(this.getUnaryIntegerExpression());
		intPlusEClass.getESuperTypes().add(this.getBinaryIntegerExpression());
		intMinusEClass.getESuperTypes().add(this.getBinaryIntegerExpression());
		intMultiplyEClass.getESuperTypes().add(this.getBinaryIntegerExpression());
		intDivideEClass.getESuperTypes().add(this.getBinaryIntegerExpression());
		notEClass.getESuperTypes().add(this.getUnaryBooleanExpression());
		andEClass.getESuperTypes().add(this.getBinaryBooleanExpression());
		orEClass.getESuperTypes().add(this.getBinaryBooleanExpression());
		xorEClass.getESuperTypes().add(this.getBinaryBooleanExpression());
		realEqualEClass.getESuperTypes().add(this.getBooleanExpression());
		realInfEClass.getESuperTypes().add(this.getBooleanExpression());
		realSupEClass.getESuperTypes().add(this.getBooleanExpression());
		intEqualEClass.getESuperTypes().add(this.getBooleanExpression());
		intInfEClass.getESuperTypes().add(this.getBooleanExpression());
		intSupEClass.getESuperTypes().add(this.getBooleanExpression());
		seqIsEmptyEClass.getESuperTypes().add(this.getBooleanExpression());
		unarySeqExpressionEClass.getESuperTypes().add(this.getSeqExpression());
		seqGetTailEClass.getESuperTypes().add(this.getUnarySeqExpression());
		seqGetHeadEClass.getESuperTypes().add(this.getIntegerExpression());
		seqDecrEClass.getESuperTypes().add(this.getUnarySeqExpression());
		seqSchedEClass.getESuperTypes().add(this.getUnarySeqExpression());
		booleanVariableRefEClass.getESuperTypes().add(this.getBooleanExpression());
		integerVariableRefEClass.getESuperTypes().add(this.getIntegerExpression());
		realVariableRefEClass.getESuperTypes().add(this.getRealExpression());
		numberSeqRefEClass.getESuperTypes().add(this.getSeqExpression());
		numberSeqVariableRefEClass.getESuperTypes().add(this.getSeqExpression());
		seqExpressionEClass.getESuperTypes().add(this.getClassicalExpression());

		// Initialize classes and features; add operations and parameters
		initEClass(classicalExpressionEClass, ClassicalExpression.class, "ClassicalExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(booleanExpressionEClass, BooleanExpression.class, "BooleanExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(realExpressionEClass, RealExpression.class, "RealExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(integerExpressionEClass, IntegerExpression.class, "IntegerExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(binaryBooleanExpressionEClass, BinaryBooleanExpression.class, "BinaryBooleanExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinaryBooleanExpression_LeftValue(), this.getBooleanExpression(), null, "leftValue", null, 1, 1, BinaryBooleanExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryBooleanExpression_RightValue(), this.getBooleanExpression(), null, "rightValue", null, 1, 1, BinaryBooleanExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unaryBooleanExpressionEClass, UnaryBooleanExpression.class, "UnaryBooleanExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnaryBooleanExpression_Operand(), this.getBooleanExpression(), null, "operand", null, 1, 1, UnaryBooleanExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(binaryRealExpressionEClass, BinaryRealExpression.class, "BinaryRealExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinaryRealExpression_LeftValue(), this.getRealExpression(), null, "leftValue", null, 1, 1, BinaryRealExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryRealExpression_RightValue(), this.getRealExpression(), null, "rightValue", null, 1, 1, BinaryRealExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unaryRealExpressionEClass, UnaryRealExpression.class, "UnaryRealExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnaryRealExpression_Value(), theBasicTypePackage.getRealElement(), null, "value", null, 1, 1, UnaryRealExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnaryRealExpression_Operand(), this.getRealExpression(), null, "operand", null, 1, 1, UnaryRealExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(binaryIntegerExpressionEClass, BinaryIntegerExpression.class, "BinaryIntegerExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinaryIntegerExpression_LeftValue(), this.getIntegerExpression(), null, "leftValue", null, 1, 1, BinaryIntegerExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryIntegerExpression_RightValue(), this.getIntegerExpression(), null, "rightValue", null, 1, 1, BinaryIntegerExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unaryIntegerExpressionEClass, UnaryIntegerExpression.class, "UnaryIntegerExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnaryIntegerExpression_Value(), theBasicTypePackage.getIntegerElement(), null, "value", null, 1, 1, UnaryIntegerExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnaryIntegerExpression_Operand(), this.getIntegerExpression(), null, "operand", null, 1, 1, UnaryIntegerExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanRefEClass, BooleanRef.class, "BooleanRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBooleanRef_ReferencedBool(), theBasicTypePackage.getBooleanElement(), null, "referencedBool", null, 1, 1, BooleanRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(realRefEClass, RealRef.class, "RealRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRealRef_RealElem(), theBasicTypePackage.getRealElement(), null, "realElem", null, 1, 1, RealRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerRefEClass, IntegerRef.class, "IntegerRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntegerRef_IntegerElem(), theBasicTypePackage.getIntegerElement(), null, "integerElem", null, 1, 1, IntegerRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unaryRealPlusEClass, UnaryRealPlus.class, "UnaryRealPlus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(unaryRealMinusEClass, UnaryRealMinus.class, "UnaryRealMinus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(realPlusEClass, RealPlus.class, "RealPlus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(realMinusEClass, RealMinus.class, "RealMinus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(realMultiplyEClass, RealMultiply.class, "RealMultiply", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(unaryIntPlusEClass, UnaryIntPlus.class, "UnaryIntPlus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(unaryIntMinusEClass, UnaryIntMinus.class, "UnaryIntMinus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(intPlusEClass, IntPlus.class, "IntPlus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(intMinusEClass, IntMinus.class, "IntMinus", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(intMultiplyEClass, IntMultiply.class, "IntMultiply", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(intDivideEClass, IntDivide.class, "IntDivide", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(notEClass, Not.class, "Not", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(andEClass, And.class, "And", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(orEClass, Or.class, "Or", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xorEClass, Xor.class, "Xor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(realEqualEClass, RealEqual.class, "RealEqual", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRealEqual_LeftValue(), this.getRealExpression(), null, "leftValue", null, 1, 1, RealEqual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRealEqual_RightValue(), this.getRealExpression(), null, "rightValue", null, 1, 1, RealEqual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(realInfEClass, RealInf.class, "RealInf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRealInf_LeftValue(), this.getRealExpression(), null, "leftValue", null, 1, 1, RealInf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRealInf_RightValue(), this.getRealExpression(), null, "rightValue", null, 1, 1, RealInf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(realSupEClass, RealSup.class, "RealSup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRealSup_LeftValue(), this.getRealExpression(), null, "leftValue", null, 1, 1, RealSup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRealSup_RightValue(), this.getRealExpression(), null, "rightValue", null, 1, 1, RealSup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(intEqualEClass, IntEqual.class, "IntEqual", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntEqual_LeftValue(), this.getIntegerExpression(), null, "leftValue", null, 1, 1, IntEqual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIntEqual_RightValue(), this.getIntegerExpression(), null, "rightValue", null, 1, 1, IntEqual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(intInfEClass, IntInf.class, "IntInf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntInf_LeftValue(), this.getIntegerExpression(), null, "leftValue", null, 1, 1, IntInf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIntInf_RightValue(), this.getIntegerExpression(), null, "rightValue", null, 1, 1, IntInf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(intSupEClass, IntSup.class, "IntSup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntSup_LeftValue(), this.getIntegerExpression(), null, "leftValue", null, 1, 1, IntSup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIntSup_RightValue(), this.getIntegerExpression(), null, "rightValue", null, 1, 1, IntSup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(seqIsEmptyEClass, SeqIsEmpty.class, "SeqIsEmpty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSeqIsEmpty_Operand(), this.getSeqExpression(), null, "operand", null, 1, 1, SeqIsEmpty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unarySeqExpressionEClass, UnarySeqExpression.class, "UnarySeqExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnarySeqExpression_Operand(), this.getSeqExpression(), null, "operand", null, 1, 1, UnarySeqExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(seqGetTailEClass, SeqGetTail.class, "SeqGetTail", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(seqGetHeadEClass, SeqGetHead.class, "SeqGetHead", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSeqGetHead_Operand(), this.getSeqExpression(), null, "operand", null, 1, 1, SeqGetHead.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(seqDecrEClass, SeqDecr.class, "SeqDecr", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(seqSchedEClass, SeqSched.class, "SeqSched", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSeqSched_IntegerExpr(), this.getIntegerExpression(), null, "integerExpr", null, 1, 1, SeqSched.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanVariableRefEClass, BooleanVariableRef.class, "BooleanVariableRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBooleanVariableRef_ReferencedVar(), theClockExpressionAndRelationPackage.getAbstractEntity(), null, "referencedVar", null, 1, 1, BooleanVariableRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerVariableRefEClass, IntegerVariableRef.class, "IntegerVariableRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIntegerVariableRef_ReferencedVar(), theClockExpressionAndRelationPackage.getAbstractEntity(), null, "referencedVar", null, 1, 1, IntegerVariableRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(realVariableRefEClass, RealVariableRef.class, "RealVariableRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRealVariableRef_ReferencedVar(), theClockExpressionAndRelationPackage.getAbstractEntity(), null, "referencedVar", null, 1, 1, RealVariableRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(numberSeqRefEClass, NumberSeqRef.class, "NumberSeqRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNumberSeqRef_ReferencedNumberSeq(), theBasicTypePackage.getSequenceElement(), null, "referencedNumberSeq", null, 1, 1, NumberSeqRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(numberSeqVariableRefEClass, NumberSeqVariableRef.class, "NumberSeqVariableRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNumberSeqVariableRef_ReferencedVar(), theClockExpressionAndRelationPackage.getAbstractEntity(), null, "referencedVar", null, 1, 1, NumberSeqVariableRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(seqExpressionEClass, SeqExpression.class, "SeqExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //ClassicalExpressionPackageImpl
