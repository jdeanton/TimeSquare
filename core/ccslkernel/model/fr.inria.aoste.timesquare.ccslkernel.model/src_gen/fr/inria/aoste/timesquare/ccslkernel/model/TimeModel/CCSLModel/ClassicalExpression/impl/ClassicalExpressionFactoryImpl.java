/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassicalExpressionFactoryImpl extends EFactoryImpl implements ClassicalExpressionFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public static ClassicalExpressionFactory init()
  {
		try {
			ClassicalExpressionFactory theClassicalExpressionFactory = (ClassicalExpressionFactory)EPackage.Registry.INSTANCE.getEFactory(ClassicalExpressionPackage.eNS_URI);
			if (theClassicalExpressionFactory != null) {
				return theClassicalExpressionFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ClassicalExpressionFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public ClassicalExpressionFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case ClassicalExpressionPackage.BOOLEAN_REF: return createBooleanRef();
			case ClassicalExpressionPackage.REAL_REF: return createRealRef();
			case ClassicalExpressionPackage.INTEGER_REF: return createIntegerRef();
			case ClassicalExpressionPackage.UNARY_REAL_PLUS: return createUnaryRealPlus();
			case ClassicalExpressionPackage.UNARY_REAL_MINUS: return createUnaryRealMinus();
			case ClassicalExpressionPackage.REAL_PLUS: return createRealPlus();
			case ClassicalExpressionPackage.REAL_MINUS: return createRealMinus();
			case ClassicalExpressionPackage.REAL_MULTIPLY: return createRealMultiply();
			case ClassicalExpressionPackage.UNARY_INT_PLUS: return createUnaryIntPlus();
			case ClassicalExpressionPackage.UNARY_INT_MINUS: return createUnaryIntMinus();
			case ClassicalExpressionPackage.INT_PLUS: return createIntPlus();
			case ClassicalExpressionPackage.INT_MINUS: return createIntMinus();
			case ClassicalExpressionPackage.INT_MULTIPLY: return createIntMultiply();
			case ClassicalExpressionPackage.INT_DIVIDE: return createIntDivide();
			case ClassicalExpressionPackage.NOT: return createNot();
			case ClassicalExpressionPackage.AND: return createAnd();
			case ClassicalExpressionPackage.OR: return createOr();
			case ClassicalExpressionPackage.XOR: return createXor();
			case ClassicalExpressionPackage.REAL_EQUAL: return createRealEqual();
			case ClassicalExpressionPackage.REAL_INF: return createRealInf();
			case ClassicalExpressionPackage.REAL_SUP: return createRealSup();
			case ClassicalExpressionPackage.INT_EQUAL: return createIntEqual();
			case ClassicalExpressionPackage.INT_INF: return createIntInf();
			case ClassicalExpressionPackage.INT_SUP: return createIntSup();
			case ClassicalExpressionPackage.SEQ_IS_EMPTY: return createSeqIsEmpty();
			case ClassicalExpressionPackage.SEQ_GET_TAIL: return createSeqGetTail();
			case ClassicalExpressionPackage.SEQ_GET_HEAD: return createSeqGetHead();
			case ClassicalExpressionPackage.SEQ_DECR: return createSeqDecr();
			case ClassicalExpressionPackage.SEQ_SCHED: return createSeqSched();
			case ClassicalExpressionPackage.BOOLEAN_VARIABLE_REF: return createBooleanVariableRef();
			case ClassicalExpressionPackage.INTEGER_VARIABLE_REF: return createIntegerVariableRef();
			case ClassicalExpressionPackage.REAL_VARIABLE_REF: return createRealVariableRef();
			case ClassicalExpressionPackage.NUMBER_SEQ_REF: return createNumberSeqRef();
			case ClassicalExpressionPackage.NUMBER_SEQ_VARIABLE_REF: return createNumberSeqVariableRef();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public BooleanRef createBooleanRef()
  {
		BooleanRefImpl booleanRef = new BooleanRefImpl();
		return booleanRef;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealRef createRealRef()
  {
		RealRefImpl realRef = new RealRefImpl();
		return realRef;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntegerRef createIntegerRef()
  {
		IntegerRefImpl integerRef = new IntegerRefImpl();
		return integerRef;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public UnaryRealPlus createUnaryRealPlus()
  {
		UnaryRealPlusImpl unaryRealPlus = new UnaryRealPlusImpl();
		return unaryRealPlus;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public UnaryRealMinus createUnaryRealMinus()
  {
		UnaryRealMinusImpl unaryRealMinus = new UnaryRealMinusImpl();
		return unaryRealMinus;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealPlus createRealPlus()
  {
		RealPlusImpl realPlus = new RealPlusImpl();
		return realPlus;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealMinus createRealMinus()
  {
		RealMinusImpl realMinus = new RealMinusImpl();
		return realMinus;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealMultiply createRealMultiply()
  {
		RealMultiplyImpl realMultiply = new RealMultiplyImpl();
		return realMultiply;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public UnaryIntPlus createUnaryIntPlus()
  {
		UnaryIntPlusImpl unaryIntPlus = new UnaryIntPlusImpl();
		return unaryIntPlus;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public UnaryIntMinus createUnaryIntMinus()
  {
		UnaryIntMinusImpl unaryIntMinus = new UnaryIntMinusImpl();
		return unaryIntMinus;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntPlus createIntPlus()
  {
		IntPlusImpl intPlus = new IntPlusImpl();
		return intPlus;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntMinus createIntMinus()
  {
		IntMinusImpl intMinus = new IntMinusImpl();
		return intMinus;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntMultiply createIntMultiply()
  {
		IntMultiplyImpl intMultiply = new IntMultiplyImpl();
		return intMultiply;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntDivide createIntDivide()
  {
		IntDivideImpl intDivide = new IntDivideImpl();
		return intDivide;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Not createNot()
  {
		NotImpl not = new NotImpl();
		return not;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public And createAnd()
  {
		AndImpl and = new AndImpl();
		return and;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Or createOr()
  {
		OrImpl or = new OrImpl();
		return or;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Xor createXor()
  {
		XorImpl xor = new XorImpl();
		return xor;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealEqual createRealEqual()
  {
		RealEqualImpl realEqual = new RealEqualImpl();
		return realEqual;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealInf createRealInf()
  {
		RealInfImpl realInf = new RealInfImpl();
		return realInf;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealSup createRealSup()
  {
		RealSupImpl realSup = new RealSupImpl();
		return realSup;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntEqual createIntEqual()
  {
		IntEqualImpl intEqual = new IntEqualImpl();
		return intEqual;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntInf createIntInf()
  {
		IntInfImpl intInf = new IntInfImpl();
		return intInf;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntSup createIntSup()
  {
		IntSupImpl intSup = new IntSupImpl();
		return intSup;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SeqIsEmpty createSeqIsEmpty()
  {
		SeqIsEmptyImpl seqIsEmpty = new SeqIsEmptyImpl();
		return seqIsEmpty;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SeqGetTail createSeqGetTail()
  {
		SeqGetTailImpl seqGetTail = new SeqGetTailImpl();
		return seqGetTail;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SeqGetHead createSeqGetHead()
  {
		SeqGetHeadImpl seqGetHead = new SeqGetHeadImpl();
		return seqGetHead;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SeqDecr createSeqDecr()
  {
		SeqDecrImpl seqDecr = new SeqDecrImpl();
		return seqDecr;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SeqSched createSeqSched()
  {
		SeqSchedImpl seqSched = new SeqSchedImpl();
		return seqSched;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public BooleanVariableRef createBooleanVariableRef()
  {
		BooleanVariableRefImpl booleanVariableRef = new BooleanVariableRefImpl();
		return booleanVariableRef;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public IntegerVariableRef createIntegerVariableRef()
  {
		IntegerVariableRefImpl integerVariableRef = new IntegerVariableRefImpl();
		return integerVariableRef;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public RealVariableRef createRealVariableRef()
  {
		RealVariableRefImpl realVariableRef = new RealVariableRefImpl();
		return realVariableRef;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public NumberSeqRef createNumberSeqRef()
  {
		NumberSeqRefImpl numberSeqRef = new NumberSeqRefImpl();
		return numberSeqRef;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public NumberSeqVariableRef createNumberSeqVariableRef()
  {
		NumberSeqVariableRefImpl numberSeqVariableRef = new NumberSeqVariableRefImpl();
		return numberSeqVariableRef;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ClassicalExpressionPackage getClassicalExpressionPackage()
  {
		return (ClassicalExpressionPackage)getEPackage();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static ClassicalExpressionPackage getPackage()
  {
		return ClassicalExpressionPackage.eINSTANCE;
	}

} //ClassicalExpressionFactoryImpl
