/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Real Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef#getRealElem <em>Real Elem</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#getRealRef()
 * @model
 * @generated
 */
public interface RealRef extends RealExpression
{
  /**
	 * Returns the value of the '<em><b>Real Elem</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Real Elem</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Elem</em>' reference.
	 * @see #setRealElem(RealElement)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#getRealRef_RealElem()
	 * @model required="true"
	 * @generated
	 */
  RealElement getRealElem();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef#getRealElem <em>Real Elem</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Elem</em>' reference.
	 * @see #getRealElem()
	 * @generated
	 */
  void setRealElem(RealElement value);

} // RealRef
