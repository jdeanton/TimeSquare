/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Non Strict Sampling</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.NonStrictSamplingImpl#getSampledClock <em>Sampled Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.NonStrictSamplingImpl#getSamplingClock <em>Sampling Clock</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NonStrictSamplingImpl extends KernelExpressionDeclarationImpl implements NonStrictSampling
{
  /**
	 * The cached value of the '{@link #getSampledClock() <em>Sampled Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSampledClock()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity sampledClock;
	/**
	 * The cached value of the '{@link #getSamplingClock() <em>Sampling Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSamplingClock()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity samplingClock;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected NonStrictSamplingImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return KernelExpressionPackage.Literals.NON_STRICT_SAMPLING;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getSampledClock() {
		return sampledClock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSampledClock(AbstractEntity newSampledClock, NotificationChain msgs) {
		AbstractEntity oldSampledClock = sampledClock;
		sampledClock = newSampledClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK, oldSampledClock, newSampledClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSampledClock(AbstractEntity newSampledClock) {
		if (newSampledClock != sampledClock) {
			NotificationChain msgs = null;
			if (sampledClock != null)
				msgs = ((InternalEObject)sampledClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK, null, msgs);
			if (newSampledClock != null)
				msgs = ((InternalEObject)newSampledClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK, null, msgs);
			msgs = basicSetSampledClock(newSampledClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK, newSampledClock, newSampledClock));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getSamplingClock() {
		return samplingClock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSamplingClock(AbstractEntity newSamplingClock, NotificationChain msgs) {
		AbstractEntity oldSamplingClock = samplingClock;
		samplingClock = newSamplingClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK, oldSamplingClock, newSamplingClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSamplingClock(AbstractEntity newSamplingClock) {
		if (newSamplingClock != samplingClock) {
			NotificationChain msgs = null;
			if (samplingClock != null)
				msgs = ((InternalEObject)samplingClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK, null, msgs);
			if (newSamplingClock != null)
				msgs = ((InternalEObject)newSamplingClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK, null, msgs);
			msgs = basicSetSamplingClock(newSamplingClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK, newSamplingClock, newSamplingClock));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK:
				return basicSetSampledClock(null, msgs);
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK:
				return basicSetSamplingClock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK:
				return getSampledClock();
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK:
				return getSamplingClock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK:
				setSampledClock((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK:
				setSamplingClock((AbstractEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK:
				setSampledClock((AbstractEntity)null);
				return;
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK:
				setSamplingClock((AbstractEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLED_CLOCK:
				return sampledClock != null;
			case KernelExpressionPackage.NON_STRICT_SAMPLING__SAMPLING_CLOCK:
				return samplingClock != null;
		}
		return super.eIsSet(featureID);
	}

} //NonStrictSamplingImpl
