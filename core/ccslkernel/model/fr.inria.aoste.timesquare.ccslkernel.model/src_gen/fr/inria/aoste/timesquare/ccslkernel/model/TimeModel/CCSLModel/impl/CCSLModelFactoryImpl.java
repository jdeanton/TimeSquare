/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CCSLModelFactoryImpl extends EFactoryImpl implements CCSLModelFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public static CCSLModelFactory init()
  {
		try {
			CCSLModelFactory theCCSLModelFactory = (CCSLModelFactory)EPackage.Registry.INSTANCE.getEFactory(CCSLModelPackage.eNS_URI);
			if (theCCSLModelFactory != null) {
				return theCCSLModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CCSLModelFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public CCSLModelFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM: return createClockConstraintSystem();
			case CCSLModelPackage.BLOCK: return createBlock();
			case CCSLModelPackage.BLOCK_TRANSITION: return createBlockTransition();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public ClockConstraintSystem createClockConstraintSystem()
  {
		ClockConstraintSystemImpl clockConstraintSystem = new ClockConstraintSystemImpl();
		return clockConstraintSystem;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public Block createBlock()
  {
		BlockImpl block = new BlockImpl();
		return block;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BlockTransition createBlockTransition() {
		BlockTransitionImpl blockTransition = new BlockTransitionImpl();
		return blockTransition;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public CCSLModelPackage getCCSLModelPackage()
  {
		return (CCSLModelPackage)getEPackage();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static CCSLModelPackage getPackage()
  {
		return CCSLModelPackage.eINSTANCE;
	}

} //CCSLModelFactoryImpl
