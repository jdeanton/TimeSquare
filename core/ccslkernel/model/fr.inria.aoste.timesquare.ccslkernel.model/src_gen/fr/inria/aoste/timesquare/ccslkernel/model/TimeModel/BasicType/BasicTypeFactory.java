/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage
 * @generated
 */
public interface BasicTypeFactory extends EFactory
{
  /**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  BasicTypeFactory eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.BasicTypeFactoryImpl.init();

  /**
	 * Returns a new object of class '<em>String</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>String</em>'.
	 * @generated
	 */
  String createString();

  /**
	 * Returns a new object of class '<em>Boolean</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean</em>'.
	 * @generated
	 */
  Boolean createBoolean();

  /**
	 * Returns a new object of class '<em>Integer</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer</em>'.
	 * @generated
	 */
  Integer createInteger();

  /**
	 * Returns a new object of class '<em>Real</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real</em>'.
	 * @generated
	 */
  Real createReal();

  /**
	 * Returns a new object of class '<em>Char</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Char</em>'.
	 * @generated
	 */
  Char createChar();

  /**
	 * Returns a new object of class '<em>String Element</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Element</em>'.
	 * @generated
	 */
  StringElement createStringElement();

  /**
	 * Returns a new object of class '<em>Boolean Element</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Element</em>'.
	 * @generated
	 */
  BooleanElement createBooleanElement();

  /**
	 * Returns a new object of class '<em>Integer Element</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Element</em>'.
	 * @generated
	 */
  IntegerElement createIntegerElement();

  /**
	 * Returns a new object of class '<em>Real Element</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Element</em>'.
	 * @generated
	 */
  RealElement createRealElement();

  /**
	 * Returns a new object of class '<em>Char Element</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Char Element</em>'.
	 * @generated
	 */
  CharElement createCharElement();

  /**
	 * Returns a new object of class '<em>Record</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record</em>'.
	 * @generated
	 */
  Record createRecord();

  /**
	 * Returns a new object of class '<em>Sequence Type</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequence Type</em>'.
	 * @generated
	 */
  SequenceType createSequenceType();

  /**
	 * Returns a new object of class '<em>Field</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field</em>'.
	 * @generated
	 */
  Field createField();

  /**
	 * Returns a new object of class '<em>Record Element</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Element</em>'.
	 * @generated
	 */
  RecordElement createRecordElement();

  /**
	 * Returns a new object of class '<em>Box</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Box</em>'.
	 * @generated
	 */
  Box createBox();

  /**
	 * Returns a new object of class '<em>Sequence Element</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequence Element</em>'.
	 * @generated
	 */
  SequenceElement createSequenceElement();

  /**
	 * Returns a new object of class '<em>Discrete Clock Type</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Clock Type</em>'.
	 * @generated
	 */
  DiscreteClockType createDiscreteClockType();

  /**
	 * Returns a new object of class '<em>Dense Clock Type</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dense Clock Type</em>'.
	 * @generated
	 */
  DenseClockType createDenseClockType();

  /**
	 * Returns a new object of class '<em>Enumeration Type</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Type</em>'.
	 * @generated
	 */
  EnumerationType createEnumerationType();

  /**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
  BasicTypePackage getBasicTypePackage();

} //BasicTypeFactory
