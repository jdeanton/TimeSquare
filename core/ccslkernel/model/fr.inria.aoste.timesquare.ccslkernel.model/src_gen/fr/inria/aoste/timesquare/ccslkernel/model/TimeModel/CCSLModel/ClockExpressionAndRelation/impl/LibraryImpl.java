/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Library</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.LibraryImpl#getExpressionLibraries <em>Expression Libraries</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.LibraryImpl#getRelationLibraries <em>Relation Libraries</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.LibraryImpl#getPredefinedTypes <em>Predefined Types</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.LibraryImpl#getImports <em>Imports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LibraryImpl extends NamedElementImpl implements Library
{
  /**
	 * The cached value of the '{@link #getExpressionLibraries() <em>Expression Libraries</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getExpressionLibraries()
	 * @generated
	 * @ordered
	 */
  protected EList<ExpressionLibrary> expressionLibraries;

  /**
	 * The cached value of the '{@link #getRelationLibraries() <em>Relation Libraries</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getRelationLibraries()
	 * @generated
	 * @ordered
	 */
  protected EList<RelationLibrary> relationLibraries;

  /**
	 * The cached value of the '{@link #getPredefinedTypes() <em>Predefined Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getPredefinedTypes()
	 * @generated
	 * @ordered
	 */
  protected EList<Type> predefinedTypes;

  /**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
  protected EList<ImportStatement> imports;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected LibraryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClockExpressionAndRelationPackage.Literals.LIBRARY;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ExpressionLibrary> getExpressionLibraries()
  {
		if (expressionLibraries == null) {
			expressionLibraries = new EObjectContainmentEList<ExpressionLibrary>(ExpressionLibrary.class, this, ClockExpressionAndRelationPackage.LIBRARY__EXPRESSION_LIBRARIES);
		}
		return expressionLibraries;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<RelationLibrary> getRelationLibraries()
  {
		if (relationLibraries == null) {
			relationLibraries = new EObjectContainmentEList<RelationLibrary>(RelationLibrary.class, this, ClockExpressionAndRelationPackage.LIBRARY__RELATION_LIBRARIES);
		}
		return relationLibraries;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Type> getPredefinedTypes()
  {
		if (predefinedTypes == null) {
			predefinedTypes = new EObjectContainmentEList<Type>(Type.class, this, ClockExpressionAndRelationPackage.LIBRARY__PREDEFINED_TYPES);
		}
		return predefinedTypes;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ImportStatement> getImports()
  {
		if (imports == null) {
			imports = new EObjectContainmentEList<ImportStatement>(ImportStatement.class, this, ClockExpressionAndRelationPackage.LIBRARY__IMPORTS);
		}
		return imports;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.LIBRARY__EXPRESSION_LIBRARIES:
				return ((InternalEList<?>)getExpressionLibraries()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.LIBRARY__RELATION_LIBRARIES:
				return ((InternalEList<?>)getRelationLibraries()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.LIBRARY__PREDEFINED_TYPES:
				return ((InternalEList<?>)getPredefinedTypes()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.LIBRARY__IMPORTS:
				return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.LIBRARY__EXPRESSION_LIBRARIES:
				return getExpressionLibraries();
			case ClockExpressionAndRelationPackage.LIBRARY__RELATION_LIBRARIES:
				return getRelationLibraries();
			case ClockExpressionAndRelationPackage.LIBRARY__PREDEFINED_TYPES:
				return getPredefinedTypes();
			case ClockExpressionAndRelationPackage.LIBRARY__IMPORTS:
				return getImports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.LIBRARY__EXPRESSION_LIBRARIES:
				getExpressionLibraries().clear();
				getExpressionLibraries().addAll((Collection<? extends ExpressionLibrary>)newValue);
				return;
			case ClockExpressionAndRelationPackage.LIBRARY__RELATION_LIBRARIES:
				getRelationLibraries().clear();
				getRelationLibraries().addAll((Collection<? extends RelationLibrary>)newValue);
				return;
			case ClockExpressionAndRelationPackage.LIBRARY__PREDEFINED_TYPES:
				getPredefinedTypes().clear();
				getPredefinedTypes().addAll((Collection<? extends Type>)newValue);
				return;
			case ClockExpressionAndRelationPackage.LIBRARY__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends ImportStatement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.LIBRARY__EXPRESSION_LIBRARIES:
				getExpressionLibraries().clear();
				return;
			case ClockExpressionAndRelationPackage.LIBRARY__RELATION_LIBRARIES:
				getRelationLibraries().clear();
				return;
			case ClockExpressionAndRelationPackage.LIBRARY__PREDEFINED_TYPES:
				getPredefinedTypes().clear();
				return;
			case ClockExpressionAndRelationPackage.LIBRARY__IMPORTS:
				getImports().clear();
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.LIBRARY__EXPRESSION_LIBRARIES:
				return expressionLibraries != null && !expressionLibraries.isEmpty();
			case ClockExpressionAndRelationPackage.LIBRARY__RELATION_LIBRARIES:
				return relationLibraries != null && !relationLibraries.isEmpty();
			case ClockExpressionAndRelationPackage.LIBRARY__PREDEFINED_TYPES:
				return predefinedTypes != null && !predefinedTypes.isEmpty();
			case ClockExpressionAndRelationPackage.LIBRARY__IMPORTS:
				return imports != null && !imports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LibraryImpl
