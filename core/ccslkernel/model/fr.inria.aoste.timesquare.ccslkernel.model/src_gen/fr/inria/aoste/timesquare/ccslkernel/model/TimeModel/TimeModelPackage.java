/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelFactory
 * @model kind="package"
 * @generated
 */
public interface TimeModelPackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNAME = "TimeModel";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_URI = "http://fr.inria.aoste.timemodel";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_PREFIX = "fr.inria.aoste.timemodel";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  TimeModelPackage eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl.init();

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getNamedElement()
	 * @generated
	 */
  int NAMED_ELEMENT = 0;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NAMED_ELEMENT__NAME = 0;

  /**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int NAMED_ELEMENT_FEATURE_COUNT = 1;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ClockImpl <em>Clock</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ClockImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getClock()
	 * @generated
	 */
  int CLOCK = 1;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK__NAME = BasicTypePackage.ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK__TYPE = BasicTypePackage.ELEMENT__TYPE;

  /**
	 * The feature id for the '<em><b>Ticking Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK__TICKING_EVENT = BasicTypePackage.ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Defining Event</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK__DEFINING_EVENT = BasicTypePackage.ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Clock</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int CLOCK_FEATURE_COUNT = BasicTypePackage.ELEMENT_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.EventImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getEvent()
	 * @generated
	 */
  int EVENT = 2;

  /**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EVENT__NAME = NAMED_ELEMENT__NAME;

  /**
	 * The feature id for the '<em><b>Referenced Object Refs</b></em>' reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EVENT__REFERENCED_OBJECT_REFS = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EVENT__KIND = NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int EVENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ImportStatementImpl <em>Import Statement</em>}' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ImportStatementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getImportStatement()
	 * @generated
	 */
  int IMPORT_STATEMENT = 3;

  /**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int IMPORT_STATEMENT__IMPORT_URI = 0;

  /**
	 * The feature id for the '<em><b>Alias</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int IMPORT_STATEMENT__ALIAS = 1;

  /**
	 * The number of structural features of the '<em>Import Statement</em>' class.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
  int IMPORT_STATEMENT_FEATURE_COUNT = 2;

  /**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind <em>Event Kind</em>}' enum.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getEventKind()
	 * @generated
	 */
  int EVENT_KIND = 4;


  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement
	 * @generated
	 */
  EClass getNamedElement();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
  EAttribute getNamedElement_Name();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock <em>Clock</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clock</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock
	 * @generated
	 */
  EClass getClock();

  /**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock#getTickingEvent <em>Ticking Event</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ticking Event</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock#getTickingEvent()
	 * @see #getClock()
	 * @generated
	 */
  EReference getClock_TickingEvent();

  /**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock#getDefiningEvent <em>Defining Event</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Defining Event</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock#getDefiningEvent()
	 * @see #getClock()
	 * @generated
	 */
  EReference getClock_DefiningEvent();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event
	 * @generated
	 */
  EClass getEvent();

  /**
	 * Returns the meta object for the reference list '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event#getReferencedObjectRefs <em>Referenced Object Refs</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Referenced Object Refs</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event#getReferencedObjectRefs()
	 * @see #getEvent()
	 * @generated
	 */
  EReference getEvent_ReferencedObjectRefs();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event#getKind()
	 * @see #getEvent()
	 * @generated
	 */
  EAttribute getEvent_Kind();

  /**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement <em>Import Statement</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import Statement</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement
	 * @generated
	 */
  EClass getImportStatement();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement#getImportURI()
	 * @see #getImportStatement()
	 * @generated
	 */
  EAttribute getImportStatement_ImportURI();

  /**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement#getAlias <em>Alias</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Alias</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement#getAlias()
	 * @see #getImportStatement()
	 * @generated
	 */
  EAttribute getImportStatement_Alias();

  /**
	 * Returns the meta object for enum '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind <em>Event Kind</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Event Kind</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind
	 * @generated
	 */
  EEnum getEventKind();

  /**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
  TimeModelFactory getTimeModelFactory();

  /**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getNamedElement()
		 * @generated
		 */
    EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

    /**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ClockImpl <em>Clock</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ClockImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getClock()
		 * @generated
		 */
    EClass CLOCK = eINSTANCE.getClock();

    /**
		 * The meta object literal for the '<em><b>Ticking Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CLOCK__TICKING_EVENT = eINSTANCE.getClock_TickingEvent();

    /**
		 * The meta object literal for the '<em><b>Defining Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference CLOCK__DEFINING_EVENT = eINSTANCE.getClock_DefiningEvent();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.EventImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getEvent()
		 * @generated
		 */
    EClass EVENT = eINSTANCE.getEvent();

    /**
		 * The meta object literal for the '<em><b>Referenced Object Refs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EReference EVENT__REFERENCED_OBJECT_REFS = eINSTANCE.getEvent_ReferencedObjectRefs();

    /**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute EVENT__KIND = eINSTANCE.getEvent_Kind();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ImportStatementImpl <em>Import Statement</em>}' class.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.ImportStatementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getImportStatement()
		 * @generated
		 */
    EClass IMPORT_STATEMENT = eINSTANCE.getImportStatement();

    /**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute IMPORT_STATEMENT__IMPORT_URI = eINSTANCE.getImportStatement_ImportURI();

    /**
		 * The meta object literal for the '<em><b>Alias</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @generated
		 */
    EAttribute IMPORT_STATEMENT__ALIAS = eINSTANCE.getImportStatement_Alias();

    /**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind <em>Event Kind</em>}' enum.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.TimeModelPackageImpl#getEventKind()
		 * @generated
		 */
    EEnum EVENT_KIND = eINSTANCE.getEventKind();

  }

} //TimeModelPackage
