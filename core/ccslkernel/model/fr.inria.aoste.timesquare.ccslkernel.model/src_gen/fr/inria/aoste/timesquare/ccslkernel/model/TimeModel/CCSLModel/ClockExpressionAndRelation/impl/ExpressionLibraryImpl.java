/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression Library</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionLibraryImpl#getExpressionDefinitions <em>Expression Definitions</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionLibraryImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionLibraryImpl#getExpressionDeclarations <em>Expression Declarations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExpressionLibraryImpl extends NamedElementImpl implements ExpressionLibrary
{
  /**
	 * The cached value of the '{@link #getExpressionDefinitions() <em>Expression Definitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getExpressionDefinitions()
	 * @generated
	 * @ordered
	 */
  protected EList<ExpressionDefinition> expressionDefinitions;

  /**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
  protected EList<Element> elements;

  /**
	 * The cached value of the '{@link #getExpressionDeclarations() <em>Expression Declarations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getExpressionDeclarations()
	 * @generated
	 * @ordered
	 */
  protected EList<ExpressionDeclaration> expressionDeclarations;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected ExpressionLibraryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClockExpressionAndRelationPackage.Literals.EXPRESSION_LIBRARY;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ExpressionDefinition> getExpressionDefinitions()
  {
		if (expressionDefinitions == null) {
			expressionDefinitions = new EObjectContainmentEList<ExpressionDefinition>(ExpressionDefinition.class, this, ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS);
		}
		return expressionDefinitions;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Element> getElements()
  {
		if (elements == null) {
			elements = new EObjectContainmentEList<Element>(Element.class, this, ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__ELEMENTS);
		}
		return elements;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<ExpressionDeclaration> getExpressionDeclarations()
  {
		if (expressionDeclarations == null) {
			expressionDeclarations = new EObjectContainmentEList<ExpressionDeclaration>(ExpressionDeclaration.class, this, ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS);
		}
		return expressionDeclarations;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS:
				return ((InternalEList<?>)getExpressionDefinitions()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS:
				return ((InternalEList<?>)getExpressionDeclarations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS:
				return getExpressionDefinitions();
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__ELEMENTS:
				return getElements();
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS:
				return getExpressionDeclarations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS:
				getExpressionDefinitions().clear();
				getExpressionDefinitions().addAll((Collection<? extends ExpressionDefinition>)newValue);
				return;
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends Element>)newValue);
				return;
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS:
				getExpressionDeclarations().clear();
				getExpressionDeclarations().addAll((Collection<? extends ExpressionDeclaration>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS:
				getExpressionDefinitions().clear();
				return;
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__ELEMENTS:
				getElements().clear();
				return;
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS:
				getExpressionDeclarations().clear();
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DEFINITIONS:
				return expressionDefinitions != null && !expressionDefinitions.isEmpty();
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case ClockExpressionAndRelationPackage.EXPRESSION_LIBRARY__EXPRESSION_DECLARATIONS:
				return expressionDeclarations != null && !expressionDeclarations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ExpressionLibraryImpl
