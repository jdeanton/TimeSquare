/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage
 * @generated
 */
public interface ClassicalExpressionFactory extends EFactory
{
  /**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  ClassicalExpressionFactory eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.ClassicalExpressionFactoryImpl.init();

  /**
	 * Returns a new object of class '<em>Boolean Ref</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Ref</em>'.
	 * @generated
	 */
  BooleanRef createBooleanRef();

  /**
	 * Returns a new object of class '<em>Real Ref</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Ref</em>'.
	 * @generated
	 */
  RealRef createRealRef();

  /**
	 * Returns a new object of class '<em>Integer Ref</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Ref</em>'.
	 * @generated
	 */
  IntegerRef createIntegerRef();

  /**
	 * Returns a new object of class '<em>Unary Real Plus</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Real Plus</em>'.
	 * @generated
	 */
  UnaryRealPlus createUnaryRealPlus();

  /**
	 * Returns a new object of class '<em>Unary Real Minus</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Real Minus</em>'.
	 * @generated
	 */
  UnaryRealMinus createUnaryRealMinus();

  /**
	 * Returns a new object of class '<em>Real Plus</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Plus</em>'.
	 * @generated
	 */
  RealPlus createRealPlus();

  /**
	 * Returns a new object of class '<em>Real Minus</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Minus</em>'.
	 * @generated
	 */
  RealMinus createRealMinus();

  /**
	 * Returns a new object of class '<em>Real Multiply</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Multiply</em>'.
	 * @generated
	 */
  RealMultiply createRealMultiply();

  /**
	 * Returns a new object of class '<em>Unary Int Plus</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Int Plus</em>'.
	 * @generated
	 */
  UnaryIntPlus createUnaryIntPlus();

  /**
	 * Returns a new object of class '<em>Unary Int Minus</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Int Minus</em>'.
	 * @generated
	 */
  UnaryIntMinus createUnaryIntMinus();

  /**
	 * Returns a new object of class '<em>Int Plus</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Plus</em>'.
	 * @generated
	 */
  IntPlus createIntPlus();

  /**
	 * Returns a new object of class '<em>Int Minus</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Minus</em>'.
	 * @generated
	 */
  IntMinus createIntMinus();

  /**
	 * Returns a new object of class '<em>Int Multiply</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Multiply</em>'.
	 * @generated
	 */
  IntMultiply createIntMultiply();

  /**
	 * Returns a new object of class '<em>Int Divide</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Divide</em>'.
	 * @generated
	 */
  IntDivide createIntDivide();

  /**
	 * Returns a new object of class '<em>Not</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not</em>'.
	 * @generated
	 */
  Not createNot();

  /**
	 * Returns a new object of class '<em>And</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>And</em>'.
	 * @generated
	 */
  And createAnd();

  /**
	 * Returns a new object of class '<em>Or</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or</em>'.
	 * @generated
	 */
  Or createOr();

  /**
	 * Returns a new object of class '<em>Xor</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Xor</em>'.
	 * @generated
	 */
  Xor createXor();

  /**
	 * Returns a new object of class '<em>Real Equal</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Equal</em>'.
	 * @generated
	 */
  RealEqual createRealEqual();

  /**
	 * Returns a new object of class '<em>Real Inf</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Inf</em>'.
	 * @generated
	 */
  RealInf createRealInf();

  /**
	 * Returns a new object of class '<em>Real Sup</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Sup</em>'.
	 * @generated
	 */
  RealSup createRealSup();

  /**
	 * Returns a new object of class '<em>Int Equal</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Equal</em>'.
	 * @generated
	 */
  IntEqual createIntEqual();

  /**
	 * Returns a new object of class '<em>Int Inf</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Inf</em>'.
	 * @generated
	 */
  IntInf createIntInf();

  /**
	 * Returns a new object of class '<em>Int Sup</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Sup</em>'.
	 * @generated
	 */
  IntSup createIntSup();

  /**
	 * Returns a new object of class '<em>Seq Is Empty</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Seq Is Empty</em>'.
	 * @generated
	 */
  SeqIsEmpty createSeqIsEmpty();

  /**
	 * Returns a new object of class '<em>Seq Get Tail</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Seq Get Tail</em>'.
	 * @generated
	 */
  SeqGetTail createSeqGetTail();

  /**
	 * Returns a new object of class '<em>Seq Get Head</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Seq Get Head</em>'.
	 * @generated
	 */
  SeqGetHead createSeqGetHead();

  /**
	 * Returns a new object of class '<em>Seq Decr</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Seq Decr</em>'.
	 * @generated
	 */
  SeqDecr createSeqDecr();

  /**
	 * Returns a new object of class '<em>Seq Sched</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Seq Sched</em>'.
	 * @generated
	 */
  SeqSched createSeqSched();

  /**
	 * Returns a new object of class '<em>Boolean Variable Ref</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Variable Ref</em>'.
	 * @generated
	 */
  BooleanVariableRef createBooleanVariableRef();

  /**
	 * Returns a new object of class '<em>Integer Variable Ref</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Variable Ref</em>'.
	 * @generated
	 */
  IntegerVariableRef createIntegerVariableRef();

  /**
	 * Returns a new object of class '<em>Real Variable Ref</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Variable Ref</em>'.
	 * @generated
	 */
  RealVariableRef createRealVariableRef();

  /**
	 * Returns a new object of class '<em>Number Seq Ref</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Number Seq Ref</em>'.
	 * @generated
	 */
  NumberSeqRef createNumberSeqRef();

  /**
	 * Returns a new object of class '<em>Number Seq Variable Ref</em>'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return a new object of class '<em>Number Seq Variable Ref</em>'.
	 * @generated
	 */
  NumberSeqVariableRef createNumberSeqVariableRef();

  /**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
  ClassicalExpressionPackage getClassicalExpressionPackage();

} //ClassicalExpressionFactory
