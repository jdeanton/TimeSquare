/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Wait</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.WaitImpl#getWaitingClock <em>Waiting Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.WaitImpl#getWaitingValue <em>Waiting Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WaitImpl extends KernelExpressionDeclarationImpl implements Wait {
	/**
	 * The cached value of the '{@link #getWaitingClock() <em>Waiting Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWaitingClock()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity waitingClock;

	/**
	 * The cached value of the '{@link #getWaitingValue() <em>Waiting Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWaitingValue()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity waitingValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WaitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return KernelExpressionPackage.Literals.WAIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getWaitingClock() {
		return waitingClock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWaitingClock(AbstractEntity newWaitingClock, NotificationChain msgs) {
		AbstractEntity oldWaitingClock = waitingClock;
		waitingClock = newWaitingClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.WAIT__WAITING_CLOCK, oldWaitingClock, newWaitingClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWaitingClock(AbstractEntity newWaitingClock) {
		if (newWaitingClock != waitingClock) {
			NotificationChain msgs = null;
			if (waitingClock != null)
				msgs = ((InternalEObject)waitingClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.WAIT__WAITING_CLOCK, null, msgs);
			if (newWaitingClock != null)
				msgs = ((InternalEObject)newWaitingClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.WAIT__WAITING_CLOCK, null, msgs);
			msgs = basicSetWaitingClock(newWaitingClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.WAIT__WAITING_CLOCK, newWaitingClock, newWaitingClock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getWaitingValue() {
		return waitingValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWaitingValue(AbstractEntity newWaitingValue, NotificationChain msgs) {
		AbstractEntity oldWaitingValue = waitingValue;
		waitingValue = newWaitingValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.WAIT__WAITING_VALUE, oldWaitingValue, newWaitingValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWaitingValue(AbstractEntity newWaitingValue) {
		if (newWaitingValue != waitingValue) {
			NotificationChain msgs = null;
			if (waitingValue != null)
				msgs = ((InternalEObject)waitingValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.WAIT__WAITING_VALUE, null, msgs);
			if (newWaitingValue != null)
				msgs = ((InternalEObject)newWaitingValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.WAIT__WAITING_VALUE, null, msgs);
			msgs = basicSetWaitingValue(newWaitingValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.WAIT__WAITING_VALUE, newWaitingValue, newWaitingValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KernelExpressionPackage.WAIT__WAITING_CLOCK:
				return basicSetWaitingClock(null, msgs);
			case KernelExpressionPackage.WAIT__WAITING_VALUE:
				return basicSetWaitingValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KernelExpressionPackage.WAIT__WAITING_CLOCK:
				return getWaitingClock();
			case KernelExpressionPackage.WAIT__WAITING_VALUE:
				return getWaitingValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KernelExpressionPackage.WAIT__WAITING_CLOCK:
				setWaitingClock((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.WAIT__WAITING_VALUE:
				setWaitingValue((AbstractEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.WAIT__WAITING_CLOCK:
				setWaitingClock((AbstractEntity)null);
				return;
			case KernelExpressionPackage.WAIT__WAITING_VALUE:
				setWaitingValue((AbstractEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.WAIT__WAITING_CLOCK:
				return waitingClock != null;
			case KernelExpressionPackage.WAIT__WAITING_VALUE:
				return waitingValue != null;
		}
		return super.eIsSet(featureID);
	}

} //WaitImpl
