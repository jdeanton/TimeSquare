/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.impl.NamedElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relation Library</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationLibraryImpl#getRelationDefinitions <em>Relation Definitions</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationLibraryImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.RelationLibraryImpl#getRelationDeclarations <em>Relation Declarations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RelationLibraryImpl extends NamedElementImpl implements RelationLibrary
{
  /**
	 * The cached value of the '{@link #getRelationDefinitions() <em>Relation Definitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getRelationDefinitions()
	 * @generated
	 * @ordered
	 */
  protected EList<RelationDefinition> relationDefinitions;

  /**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
  protected EList<Element> elements;

  /**
	 * The cached value of the '{@link #getRelationDeclarations() <em>Relation Declarations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getRelationDeclarations()
	 * @generated
	 * @ordered
	 */
  protected EList<RelationDeclaration> relationDeclarations;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected RelationLibraryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<RelationDefinition> getRelationDefinitions()
  {
		if (relationDefinitions == null) {
			relationDefinitions = new EObjectContainmentEList<RelationDefinition>(RelationDefinition.class, this, ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DEFINITIONS);
		}
		return relationDefinitions;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Element> getElements()
  {
		if (elements == null) {
			elements = new EObjectContainmentEList<Element>(Element.class, this, ClockExpressionAndRelationPackage.RELATION_LIBRARY__ELEMENTS);
		}
		return elements;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<RelationDeclaration> getRelationDeclarations()
  {
		if (relationDeclarations == null) {
			relationDeclarations = new EObjectContainmentEList<RelationDeclaration>(RelationDeclaration.class, this, ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DECLARATIONS);
		}
		return relationDeclarations;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DEFINITIONS:
				return ((InternalEList<?>)getRelationDefinitions()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DECLARATIONS:
				return ((InternalEList<?>)getRelationDeclarations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DEFINITIONS:
				return getRelationDefinitions();
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__ELEMENTS:
				return getElements();
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DECLARATIONS:
				return getRelationDeclarations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DEFINITIONS:
				getRelationDefinitions().clear();
				getRelationDefinitions().addAll((Collection<? extends RelationDefinition>)newValue);
				return;
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends Element>)newValue);
				return;
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DECLARATIONS:
				getRelationDeclarations().clear();
				getRelationDeclarations().addAll((Collection<? extends RelationDeclaration>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DEFINITIONS:
				getRelationDefinitions().clear();
				return;
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__ELEMENTS:
				getElements().clear();
				return;
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DECLARATIONS:
				getRelationDeclarations().clear();
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DEFINITIONS:
				return relationDefinitions != null && !relationDefinitions.isEmpty();
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DECLARATIONS:
				return relationDeclarations != null && !relationDeclarations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RelationLibraryImpl
