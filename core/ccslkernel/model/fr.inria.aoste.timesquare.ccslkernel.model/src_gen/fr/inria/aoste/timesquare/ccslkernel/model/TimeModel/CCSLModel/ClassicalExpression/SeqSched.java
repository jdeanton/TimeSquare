/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Seq Sched</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched#getIntegerExpr <em>Integer Expr</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#getSeqSched()
 * @model
 * @generated
 */
public interface SeqSched extends UnarySeqExpression
{
  /**
	 * Returns the value of the '<em><b>Integer Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Integer Expr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Expr</em>' containment reference.
	 * @see #setIntegerExpr(IntegerExpression)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#getSeqSched_IntegerExpr()
	 * @model containment="true" required="true"
	 * @generated
	 */
  IntegerExpression getIntegerExpr();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched#getIntegerExpr <em>Integer Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Expr</em>' containment reference.
	 * @see #getIntegerExpr()
	 * @generated
	 */
  void setIntegerExpr(IntegerExpression value);

} // SeqSched
