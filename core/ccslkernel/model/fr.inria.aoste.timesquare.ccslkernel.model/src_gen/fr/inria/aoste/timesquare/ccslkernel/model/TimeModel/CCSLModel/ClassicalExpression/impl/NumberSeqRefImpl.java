/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Number Seq Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.NumberSeqRefImpl#getReferencedNumberSeq <em>Referenced Number Seq</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NumberSeqRefImpl extends SeqExpressionImpl implements NumberSeqRef
{
  /**
	 * The cached value of the '{@link #getReferencedNumberSeq() <em>Referenced Number Seq</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getReferencedNumberSeq()
	 * @generated
	 * @ordered
	 */
  protected SequenceElement referencedNumberSeq;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected NumberSeqRefImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClassicalExpressionPackage.Literals.NUMBER_SEQ_REF;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public SequenceElement getReferencedNumberSeq()
  {
		if (referencedNumberSeq != null && referencedNumberSeq.eIsProxy()) {
			InternalEObject oldReferencedNumberSeq = (InternalEObject)referencedNumberSeq;
			referencedNumberSeq = (SequenceElement)eResolveProxy(oldReferencedNumberSeq);
			if (referencedNumberSeq != oldReferencedNumberSeq) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClassicalExpressionPackage.NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ, oldReferencedNumberSeq, referencedNumberSeq));
			}
		}
		return referencedNumberSeq;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public SequenceElement basicGetReferencedNumberSeq()
  {
		return referencedNumberSeq;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setReferencedNumberSeq(SequenceElement newReferencedNumberSeq)
  {
		SequenceElement oldReferencedNumberSeq = referencedNumberSeq;
		referencedNumberSeq = newReferencedNumberSeq;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassicalExpressionPackage.NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ, oldReferencedNumberSeq, referencedNumberSeq));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ:
				if (resolve) return getReferencedNumberSeq();
				return basicGetReferencedNumberSeq();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ:
				setReferencedNumberSeq((SequenceElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ:
				setReferencedNumberSeq((SequenceElement)null);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.NUMBER_SEQ_REF__REFERENCED_NUMBER_SEQ:
				return referencedNumberSeq != null;
		}
		return super.eIsSet(featureID);
	}

} //NumberSeqRefImpl
