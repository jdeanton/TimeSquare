/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType;

import java.lang.String;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dense Clock Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType#getBaseUnit <em>Base Unit</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType#getPhysicalMagnitude <em>Physical Magnitude</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage#getDenseClockType()
 * @model
 * @generated
 */
public interface DenseClockType extends Type
{
  /**
	 * Returns the value of the '<em><b>Base Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Base Unit</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Unit</em>' attribute.
	 * @see #setBaseUnit(String)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage#getDenseClockType_BaseUnit()
	 * @model dataType="fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.String"
	 * @generated
	 */
  String getBaseUnit();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType#getBaseUnit <em>Base Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Unit</em>' attribute.
	 * @see #getBaseUnit()
	 * @generated
	 */
  void setBaseUnit(String value);

  /**
	 * Returns the value of the '<em><b>Physical Magnitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Physical Magnitude</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Magnitude</em>' attribute.
	 * @see #setPhysicalMagnitude(String)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage#getDenseClockType_PhysicalMagnitude()
	 * @model dataType="fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.String"
	 * @generated
	 */
  String getPhysicalMagnitude();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType#getPhysicalMagnitude <em>Physical Magnitude</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Magnitude</em>' attribute.
	 * @see #getPhysicalMagnitude()
	 * @generated
	 */
  void setPhysicalMagnitude(String value);

} // DenseClockType
