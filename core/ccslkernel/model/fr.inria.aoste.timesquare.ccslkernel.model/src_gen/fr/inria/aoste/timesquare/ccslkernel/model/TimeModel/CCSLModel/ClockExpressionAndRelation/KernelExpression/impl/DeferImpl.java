/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Defer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeferImpl#getBaseClock <em>Base Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeferImpl#getDelayClock <em>Delay Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.DeferImpl#getDelayPattern <em>Delay Pattern</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeferImpl extends KernelExpressionDeclarationImpl implements Defer
{
  /**
	 * The cached value of the '{@link #getBaseClock() <em>Base Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseClock()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity baseClock;
	/**
	 * The cached value of the '{@link #getDelayClock() <em>Delay Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelayClock()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity delayClock;
	/**
	 * The cached value of the '{@link #getDelayPattern() <em>Delay Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelayPattern()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity delayPattern;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected DeferImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return KernelExpressionPackage.Literals.DEFER;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getBaseClock() {
		return baseClock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBaseClock(AbstractEntity newBaseClock, NotificationChain msgs) {
		AbstractEntity oldBaseClock = baseClock;
		baseClock = newBaseClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DEFER__BASE_CLOCK, oldBaseClock, newBaseClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBaseClock(AbstractEntity newBaseClock) {
		if (newBaseClock != baseClock) {
			NotificationChain msgs = null;
			if (baseClock != null)
				msgs = ((InternalEObject)baseClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DEFER__BASE_CLOCK, null, msgs);
			if (newBaseClock != null)
				msgs = ((InternalEObject)newBaseClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DEFER__BASE_CLOCK, null, msgs);
			msgs = basicSetBaseClock(newBaseClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DEFER__BASE_CLOCK, newBaseClock, newBaseClock));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getDelayClock() {
		return delayClock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDelayClock(AbstractEntity newDelayClock, NotificationChain msgs) {
		AbstractEntity oldDelayClock = delayClock;
		delayClock = newDelayClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DEFER__DELAY_CLOCK, oldDelayClock, newDelayClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDelayClock(AbstractEntity newDelayClock) {
		if (newDelayClock != delayClock) {
			NotificationChain msgs = null;
			if (delayClock != null)
				msgs = ((InternalEObject)delayClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DEFER__DELAY_CLOCK, null, msgs);
			if (newDelayClock != null)
				msgs = ((InternalEObject)newDelayClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DEFER__DELAY_CLOCK, null, msgs);
			msgs = basicSetDelayClock(newDelayClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DEFER__DELAY_CLOCK, newDelayClock, newDelayClock));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getDelayPattern() {
		return delayPattern;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDelayPattern(AbstractEntity newDelayPattern, NotificationChain msgs) {
		AbstractEntity oldDelayPattern = delayPattern;
		delayPattern = newDelayPattern;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DEFER__DELAY_PATTERN, oldDelayPattern, newDelayPattern);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDelayPattern(AbstractEntity newDelayPattern) {
		if (newDelayPattern != delayPattern) {
			NotificationChain msgs = null;
			if (delayPattern != null)
				msgs = ((InternalEObject)delayPattern).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DEFER__DELAY_PATTERN, null, msgs);
			if (newDelayPattern != null)
				msgs = ((InternalEObject)newDelayPattern).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.DEFER__DELAY_PATTERN, null, msgs);
			msgs = basicSetDelayPattern(newDelayPattern, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.DEFER__DELAY_PATTERN, newDelayPattern, newDelayPattern));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KernelExpressionPackage.DEFER__BASE_CLOCK:
				return basicSetBaseClock(null, msgs);
			case KernelExpressionPackage.DEFER__DELAY_CLOCK:
				return basicSetDelayClock(null, msgs);
			case KernelExpressionPackage.DEFER__DELAY_PATTERN:
				return basicSetDelayPattern(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KernelExpressionPackage.DEFER__BASE_CLOCK:
				return getBaseClock();
			case KernelExpressionPackage.DEFER__DELAY_CLOCK:
				return getDelayClock();
			case KernelExpressionPackage.DEFER__DELAY_PATTERN:
				return getDelayPattern();
		}
		return super.eGet(featureID, resolve, coreType);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KernelExpressionPackage.DEFER__BASE_CLOCK:
				setBaseClock((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.DEFER__DELAY_CLOCK:
				setDelayClock((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.DEFER__DELAY_PATTERN:
				setDelayPattern((AbstractEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.DEFER__BASE_CLOCK:
				setBaseClock((AbstractEntity)null);
				return;
			case KernelExpressionPackage.DEFER__DELAY_CLOCK:
				setDelayClock((AbstractEntity)null);
				return;
			case KernelExpressionPackage.DEFER__DELAY_PATTERN:
				setDelayPattern((AbstractEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.DEFER__BASE_CLOCK:
				return baseClock != null;
			case KernelExpressionPackage.DEFER__DELAY_CLOCK:
				return delayClock != null;
			case KernelExpressionPackage.DEFER__DELAY_PATTERN:
				return delayPattern != null;
		}
		return super.eIsSet(featureID);
	}

} //DeferImpl
