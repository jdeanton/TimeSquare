/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Relation Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage#getExternalRelationDefinition()
 * @model
 * @generated
 */
public interface ExternalRelationDefinition extends RelationDefinition {
} // ExternalRelationDefinition
