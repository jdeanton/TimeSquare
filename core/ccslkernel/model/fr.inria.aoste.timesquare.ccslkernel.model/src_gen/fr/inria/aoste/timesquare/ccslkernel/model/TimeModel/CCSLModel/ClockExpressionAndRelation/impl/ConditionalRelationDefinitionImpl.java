/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditional Relation Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalRelationDefinitionImpl#getRelCases <em>Rel Cases</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ConditionalRelationDefinitionImpl#getDefaultRelation <em>Default Relation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionalRelationDefinitionImpl extends UserRelationDefinitionImpl implements ConditionalRelationDefinition
{
  /**
	 * The cached value of the '{@link #getRelCases() <em>Rel Cases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getRelCases()
	 * @generated
	 * @ordered
	 */
  protected EList<RelCase> relCases;

  /**
	 * The cached value of the '{@link #getDefaultRelation() <em>Default Relation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getDefaultRelation()
	 * @generated
	 * @ordered
	 */
  protected EList<Relation> defaultRelation;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected ConditionalRelationDefinitionImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClockExpressionAndRelationPackage.Literals.CONDITIONAL_RELATION_DEFINITION;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<RelCase> getRelCases()
  {
		if (relCases == null) {
			relCases = new EObjectContainmentEList<RelCase>(RelCase.class, this, ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__REL_CASES);
		}
		return relCases;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public EList<Relation> getDefaultRelation()
  {
		if (defaultRelation == null) {
			defaultRelation = new EObjectContainmentEList<Relation>(Relation.class, this, ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION);
		}
		return defaultRelation;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__REL_CASES:
				return ((InternalEList<?>)getRelCases()).basicRemove(otherEnd, msgs);
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION:
				return ((InternalEList<?>)getDefaultRelation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__REL_CASES:
				return getRelCases();
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION:
				return getDefaultRelation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__REL_CASES:
				getRelCases().clear();
				getRelCases().addAll((Collection<? extends RelCase>)newValue);
				return;
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION:
				getDefaultRelation().clear();
				getDefaultRelation().addAll((Collection<? extends Relation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__REL_CASES:
				getRelCases().clear();
				return;
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION:
				getDefaultRelation().clear();
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__REL_CASES:
				return relCases != null && !relCases.isEmpty();
			case ClockExpressionAndRelationPackage.CONDITIONAL_RELATION_DEFINITION__DEFAULT_RELATION:
				return defaultRelation != null && !defaultRelation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConditionalRelationDefinitionImpl
