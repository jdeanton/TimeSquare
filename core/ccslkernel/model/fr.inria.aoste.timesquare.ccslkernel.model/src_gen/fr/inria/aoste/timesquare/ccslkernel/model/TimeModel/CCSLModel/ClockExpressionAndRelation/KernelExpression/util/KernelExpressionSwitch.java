/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.*;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage
 * @generated
 */
public class KernelExpressionSwitch<T>
{
  /**
	 * The cached model package
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected static KernelExpressionPackage modelPackage;

  /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public KernelExpressionSwitch()
  {
		if (modelPackage == null) {
			modelPackage = KernelExpressionPackage.eINSTANCE;
		}
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  public T doSwitch(EObject theEObject)
  {
		return doSwitch(theEObject.eClass(), theEObject);
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  protected T doSwitch(EClass theEClass, EObject theEObject)
  {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  protected T doSwitch(int classifierID, EObject theEObject)
  {
		switch (classifierID) {
			case KernelExpressionPackage.KERNEL_EXPRESSION_DECLARATION: {
				KernelExpressionDeclaration kernelExpressionDeclaration = (KernelExpressionDeclaration)theEObject;
				T result = caseKernelExpressionDeclaration(kernelExpressionDeclaration);
				if (result == null) result = caseExpressionDeclaration(kernelExpressionDeclaration);
				if (result == null) result = caseNamedElement(kernelExpressionDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.UP_TO: {
				UpTo upTo = (UpTo)theEObject;
				T result = caseUpTo(upTo);
				if (result == null) result = caseKernelExpressionDeclaration(upTo);
				if (result == null) result = caseExpressionDeclaration(upTo);
				if (result == null) result = caseNamedElement(upTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.DEFER: {
				Defer defer = (Defer)theEObject;
				T result = caseDefer(defer);
				if (result == null) result = caseKernelExpressionDeclaration(defer);
				if (result == null) result = caseExpressionDeclaration(defer);
				if (result == null) result = caseNamedElement(defer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.STRICT_SAMPLING: {
				StrictSampling strictSampling = (StrictSampling)theEObject;
				T result = caseStrictSampling(strictSampling);
				if (result == null) result = caseKernelExpressionDeclaration(strictSampling);
				if (result == null) result = caseExpressionDeclaration(strictSampling);
				if (result == null) result = caseNamedElement(strictSampling);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.CONCATENATION: {
				Concatenation concatenation = (Concatenation)theEObject;
				T result = caseConcatenation(concatenation);
				if (result == null) result = caseKernelExpressionDeclaration(concatenation);
				if (result == null) result = caseExpressionDeclaration(concatenation);
				if (result == null) result = caseNamedElement(concatenation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.UNION: {
				Union union = (Union)theEObject;
				T result = caseUnion(union);
				if (result == null) result = caseKernelExpressionDeclaration(union);
				if (result == null) result = caseExpressionDeclaration(union);
				if (result == null) result = caseNamedElement(union);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.INTERSECTION: {
				Intersection intersection = (Intersection)theEObject;
				T result = caseIntersection(intersection);
				if (result == null) result = caseKernelExpressionDeclaration(intersection);
				if (result == null) result = caseExpressionDeclaration(intersection);
				if (result == null) result = caseNamedElement(intersection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.SUP: {
				Sup sup = (Sup)theEObject;
				T result = caseSup(sup);
				if (result == null) result = caseKernelExpressionDeclaration(sup);
				if (result == null) result = caseExpressionDeclaration(sup);
				if (result == null) result = caseNamedElement(sup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.INF: {
				Inf inf = (Inf)theEObject;
				T result = caseInf(inf);
				if (result == null) result = caseKernelExpressionDeclaration(inf);
				if (result == null) result = caseExpressionDeclaration(inf);
				if (result == null) result = caseNamedElement(inf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.NON_STRICT_SAMPLING: {
				NonStrictSampling nonStrictSampling = (NonStrictSampling)theEObject;
				T result = caseNonStrictSampling(nonStrictSampling);
				if (result == null) result = caseKernelExpressionDeclaration(nonStrictSampling);
				if (result == null) result = caseExpressionDeclaration(nonStrictSampling);
				if (result == null) result = caseNamedElement(nonStrictSampling);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.DISCRETIZATION: {
				Discretization discretization = (Discretization)theEObject;
				T result = caseDiscretization(discretization);
				if (result == null) result = caseKernelExpressionDeclaration(discretization);
				if (result == null) result = caseExpressionDeclaration(discretization);
				if (result == null) result = caseNamedElement(discretization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.DEATH: {
				Death death = (Death)theEObject;
				T result = caseDeath(death);
				if (result == null) result = caseKernelExpressionDeclaration(death);
				if (result == null) result = caseExpressionDeclaration(death);
				if (result == null) result = caseNamedElement(death);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case KernelExpressionPackage.WAIT: {
				Wait wait = (Wait)theEObject;
				T result = caseWait(wait);
				if (result == null) result = caseKernelExpressionDeclaration(wait);
				if (result == null) result = caseExpressionDeclaration(wait);
				if (result == null) result = caseNamedElement(wait);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Declaration</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseKernelExpressionDeclaration(KernelExpressionDeclaration object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Up To</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Up To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUpTo(UpTo object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Defer</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseDefer(Defer object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Strict Sampling</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Strict Sampling</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseStrictSampling(StrictSampling object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Concatenation</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concatenation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseConcatenation(Concatenation object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Union</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Union</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseUnion(Union object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Intersection</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Intersection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseIntersection(Intersection object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Sup</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sup</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseSup(Sup object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Inf</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseInf(Inf object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Non Strict Sampling</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Non Strict Sampling</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNonStrictSampling(NonStrictSampling object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Discretization</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discretization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseDiscretization(Discretization object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Death</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Death</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseDeath(Death object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Wait</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wait</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWait(Wait object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseNamedElement(NamedElement object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Declaration</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
  public T caseExpressionDeclaration(ExpressionDeclaration object)
  {
		return null;
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
  public T defaultCase(EObject object)
  {
		return null;
	}

} //KernelExpressionSwitch
