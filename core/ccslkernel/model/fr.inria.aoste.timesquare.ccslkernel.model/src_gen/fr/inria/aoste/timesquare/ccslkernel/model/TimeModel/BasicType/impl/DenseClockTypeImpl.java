/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;

import java.lang.String;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dense Clock Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DenseClockTypeImpl#getBaseUnit <em>Base Unit</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.impl.DenseClockTypeImpl#getPhysicalMagnitude <em>Physical Magnitude</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DenseClockTypeImpl extends TypeImpl implements DenseClockType
{
  /**
	 * The default value of the '{@link #getBaseUnit() <em>Base Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getBaseUnit()
	 * @generated
	 * @ordered
	 */
  protected static final String BASE_UNIT_EDEFAULT = null;

  /**
	 * The cached value of the '{@link #getBaseUnit() <em>Base Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getBaseUnit()
	 * @generated
	 * @ordered
	 */
  protected String baseUnit = BASE_UNIT_EDEFAULT;

  /**
	 * The default value of the '{@link #getPhysicalMagnitude() <em>Physical Magnitude</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getPhysicalMagnitude()
	 * @generated
	 * @ordered
	 */
  protected static final String PHYSICAL_MAGNITUDE_EDEFAULT = null;

  /**
	 * The cached value of the '{@link #getPhysicalMagnitude() <em>Physical Magnitude</em>}' attribute.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getPhysicalMagnitude()
	 * @generated
	 * @ordered
	 */
  protected String physicalMagnitude = PHYSICAL_MAGNITUDE_EDEFAULT;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected DenseClockTypeImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return BasicTypePackage.Literals.DENSE_CLOCK_TYPE;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public String getBaseUnit()
  {
		return baseUnit;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setBaseUnit(String newBaseUnit)
  {
		String oldBaseUnit = baseUnit;
		baseUnit = newBaseUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BasicTypePackage.DENSE_CLOCK_TYPE__BASE_UNIT, oldBaseUnit, baseUnit));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public String getPhysicalMagnitude()
  {
		return physicalMagnitude;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setPhysicalMagnitude(String newPhysicalMagnitude)
  {
		String oldPhysicalMagnitude = physicalMagnitude;
		physicalMagnitude = newPhysicalMagnitude;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BasicTypePackage.DENSE_CLOCK_TYPE__PHYSICAL_MAGNITUDE, oldPhysicalMagnitude, physicalMagnitude));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case BasicTypePackage.DENSE_CLOCK_TYPE__BASE_UNIT:
				return getBaseUnit();
			case BasicTypePackage.DENSE_CLOCK_TYPE__PHYSICAL_MAGNITUDE:
				return getPhysicalMagnitude();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case BasicTypePackage.DENSE_CLOCK_TYPE__BASE_UNIT:
				setBaseUnit((String)newValue);
				return;
			case BasicTypePackage.DENSE_CLOCK_TYPE__PHYSICAL_MAGNITUDE:
				setPhysicalMagnitude((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case BasicTypePackage.DENSE_CLOCK_TYPE__BASE_UNIT:
				setBaseUnit(BASE_UNIT_EDEFAULT);
				return;
			case BasicTypePackage.DENSE_CLOCK_TYPE__PHYSICAL_MAGNITUDE:
				setPhysicalMagnitude(PHYSICAL_MAGNITUDE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case BasicTypePackage.DENSE_CLOCK_TYPE__BASE_UNIT:
				return BASE_UNIT_EDEFAULT == null ? baseUnit != null : !BASE_UNIT_EDEFAULT.equals(baseUnit);
			case BasicTypePackage.DENSE_CLOCK_TYPE__PHYSICAL_MAGNITUDE:
				return PHYSICAL_MAGNITUDE_EDEFAULT == null ? physicalMagnitude != null : !PHYSICAL_MAGNITUDE_EDEFAULT.equals(physicalMagnitude);
		}
		return super.eIsSet(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public String toString()
  {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (baseUnit: ");
		result.append(baseUnit);
		result.append(", physicalMagnitude: ");
		result.append(physicalMagnitude);
		result.append(')');
		return result.toString();
	}

} //DenseClockTypeImpl
