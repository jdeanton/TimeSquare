/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef#getReferencedBool <em>Referenced Bool</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#getBooleanRef()
 * @model
 * @generated
 */
public interface BooleanRef extends BooleanExpression
{
  /**
	 * Returns the value of the '<em><b>Referenced Bool</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Referenced Bool</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Bool</em>' reference.
	 * @see #setReferencedBool(BooleanElement)
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage#getBooleanRef_ReferencedBool()
	 * @model required="true"
	 * @generated
	 */
  BooleanElement getReferencedBool();

  /**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef#getReferencedBool <em>Referenced Bool</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Bool</em>' reference.
	 * @see #getReferencedBool()
	 * @generated
	 */
  void setReferencedBool(BooleanElement value);

} // BooleanRef
