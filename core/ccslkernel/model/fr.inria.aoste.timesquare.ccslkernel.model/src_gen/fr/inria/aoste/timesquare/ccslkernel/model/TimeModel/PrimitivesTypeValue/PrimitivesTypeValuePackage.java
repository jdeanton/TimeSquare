/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.PrimitivesTypeValueFactory
 * @model kind="package"
 * @generated
 */
public interface PrimitivesTypeValuePackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNAME = "PrimitivesTypeValue";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_URI = "http://fr.inria.aoste.timemodel.primitivetypes";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  String eNS_PREFIX = "fr.inria.aoste.timemodel.primitivetypes";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  PrimitivesTypeValuePackage eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl.init();

  /**
	 * The meta object id for the '<em>Integer</em>' data type.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see java.lang.Integer
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getInteger()
	 * @generated
	 */
  int INTEGER = 0;

  /**
	 * The meta object id for the '<em>Boolean</em>' data type.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see java.lang.Boolean
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getBoolean()
	 * @generated
	 */
  int BOOLEAN = 1;

  /**
	 * The meta object id for the '<em>Unlimited Natural</em>' data type.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see java.lang.Float
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getUnlimitedNatural()
	 * @generated
	 */
  int UNLIMITED_NATURAL = 2;

  /**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getString()
	 * @generated
	 */
  int STRING = 3;

  /**
	 * The meta object id for the '<em>Date Time</em>' data type.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see java.util.Date
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getDateTime()
	 * @generated
	 */
  int DATE_TIME = 4;

  /**
	 * The meta object id for the '<em>Real</em>' data type.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see java.lang.Float
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getReal()
	 * @generated
	 */
  int REAL = 5;

  /**
	 * The meta object id for the '<em>Char</em>' data type.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see java.lang.Character
	 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getChar()
	 * @generated
	 */
  int CHAR = 6;


  /**
	 * Returns the meta object for data type '{@link java.lang.Integer <em>Integer</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Integer</em>'.
	 * @see java.lang.Integer
	 * @model instanceClass="java.lang.Integer"
	 * @generated
	 */
  EDataType getInteger();

  /**
	 * Returns the meta object for data type '{@link java.lang.Boolean <em>Boolean</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Boolean</em>'.
	 * @see java.lang.Boolean
	 * @model instanceClass="java.lang.Boolean"
	 * @generated
	 */
  EDataType getBoolean();

  /**
	 * Returns the meta object for data type '{@link java.lang.Float <em>Unlimited Natural</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Unlimited Natural</em>'.
	 * @see java.lang.Float
	 * @model instanceClass="java.lang.Float"
	 * @generated
	 */
  EDataType getUnlimitedNatural();

  /**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
  EDataType getString();

  /**
	 * Returns the meta object for data type '{@link java.util.Date <em>Date Time</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Date Time</em>'.
	 * @see java.util.Date
	 * @model instanceClass="java.util.Date"
	 * @generated
	 */
  EDataType getDateTime();

  /**
	 * Returns the meta object for data type '{@link java.lang.Float <em>Real</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Real</em>'.
	 * @see java.lang.Float
	 * @model instanceClass="java.lang.Float"
	 * @generated
	 */
  EDataType getReal();

  /**
	 * Returns the meta object for data type '{@link java.lang.Character <em>Char</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Char</em>'.
	 * @see java.lang.Character
	 * @model instanceClass="java.lang.Character"
	 * @generated
	 */
  EDataType getChar();

  /**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
  PrimitivesTypeValueFactory getPrimitivesTypeValueFactory();

  /**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '<em>Integer</em>' data type.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see java.lang.Integer
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getInteger()
		 * @generated
		 */
    EDataType INTEGER = eINSTANCE.getInteger();

    /**
		 * The meta object literal for the '<em>Boolean</em>' data type.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see java.lang.Boolean
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getBoolean()
		 * @generated
		 */
    EDataType BOOLEAN = eINSTANCE.getBoolean();

    /**
		 * The meta object literal for the '<em>Unlimited Natural</em>' data type.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see java.lang.Float
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getUnlimitedNatural()
		 * @generated
		 */
    EDataType UNLIMITED_NATURAL = eINSTANCE.getUnlimitedNatural();

    /**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getString()
		 * @generated
		 */
    EDataType STRING = eINSTANCE.getString();

    /**
		 * The meta object literal for the '<em>Date Time</em>' data type.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see java.util.Date
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getDateTime()
		 * @generated
		 */
    EDataType DATE_TIME = eINSTANCE.getDateTime();

    /**
		 * The meta object literal for the '<em>Real</em>' data type.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see java.lang.Float
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getReal()
		 * @generated
		 */
    EDataType REAL = eINSTANCE.getReal();

    /**
		 * The meta object literal for the '<em>Char</em>' data type.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
		 * @see java.lang.Character
		 * @see fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.PrimitivesTypeValue.impl.PrimitivesTypeValuePackageImpl#getChar()
		 * @generated
		 */
    EDataType CHAR = eINSTANCE.getChar();

  }

} //PrimitivesTypeValuePackage
