/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Union</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UnionImpl#getClock1 <em>Clock1</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.UnionImpl#getClock2 <em>Clock2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnionImpl extends KernelExpressionDeclarationImpl implements Union
{
  /**
	 * The cached value of the '{@link #getClock1() <em>Clock1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClock1()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity clock1;
	/**
	 * The cached value of the '{@link #getClock2() <em>Clock2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClock2()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity clock2;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected UnionImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return KernelExpressionPackage.Literals.UNION;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getClock1() {
		return clock1;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClock1(AbstractEntity newClock1, NotificationChain msgs) {
		AbstractEntity oldClock1 = clock1;
		clock1 = newClock1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UNION__CLOCK1, oldClock1, newClock1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setClock1(AbstractEntity newClock1) {
		if (newClock1 != clock1) {
			NotificationChain msgs = null;
			if (clock1 != null)
				msgs = ((InternalEObject)clock1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.UNION__CLOCK1, null, msgs);
			if (newClock1 != null)
				msgs = ((InternalEObject)newClock1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.UNION__CLOCK1, null, msgs);
			msgs = basicSetClock1(newClock1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UNION__CLOCK1, newClock1, newClock1));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getClock2() {
		return clock2;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClock2(AbstractEntity newClock2, NotificationChain msgs) {
		AbstractEntity oldClock2 = clock2;
		clock2 = newClock2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UNION__CLOCK2, oldClock2, newClock2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setClock2(AbstractEntity newClock2) {
		if (newClock2 != clock2) {
			NotificationChain msgs = null;
			if (clock2 != null)
				msgs = ((InternalEObject)clock2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.UNION__CLOCK2, null, msgs);
			if (newClock2 != null)
				msgs = ((InternalEObject)newClock2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.UNION__CLOCK2, null, msgs);
			msgs = basicSetClock2(newClock2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.UNION__CLOCK2, newClock2, newClock2));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KernelExpressionPackage.UNION__CLOCK1:
				return basicSetClock1(null, msgs);
			case KernelExpressionPackage.UNION__CLOCK2:
				return basicSetClock2(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KernelExpressionPackage.UNION__CLOCK1:
				return getClock1();
			case KernelExpressionPackage.UNION__CLOCK2:
				return getClock2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KernelExpressionPackage.UNION__CLOCK1:
				setClock1((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.UNION__CLOCK2:
				setClock2((AbstractEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.UNION__CLOCK1:
				setClock1((AbstractEntity)null);
				return;
			case KernelExpressionPackage.UNION__CLOCK2:
				setClock2((AbstractEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.UNION__CLOCK1:
				return clock1 != null;
			case KernelExpressionPackage.UNION__CLOCK2:
				return clock2 != null;
		}
		return super.eIsSet(featureID);
	}

} //UnionImpl
