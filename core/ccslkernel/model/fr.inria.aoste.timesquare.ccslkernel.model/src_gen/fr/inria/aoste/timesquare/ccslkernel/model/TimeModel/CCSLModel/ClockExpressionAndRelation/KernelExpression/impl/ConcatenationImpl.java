/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concatenation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.ConcatenationImpl#getLeftClock <em>Left Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.ConcatenationImpl#getRightClock <em>Right Clock</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConcatenationImpl extends KernelExpressionDeclarationImpl implements Concatenation
{
  /**
	 * The cached value of the '{@link #getLeftClock() <em>Left Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftClock()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity leftClock;
	/**
	 * The cached value of the '{@link #getRightClock() <em>Right Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightClock()
	 * @generated
	 * @ordered
	 */
	protected AbstractEntity rightClock;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected ConcatenationImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return KernelExpressionPackage.Literals.CONCATENATION;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getLeftClock() {
		return leftClock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftClock(AbstractEntity newLeftClock, NotificationChain msgs) {
		AbstractEntity oldLeftClock = leftClock;
		leftClock = newLeftClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.CONCATENATION__LEFT_CLOCK, oldLeftClock, newLeftClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLeftClock(AbstractEntity newLeftClock) {
		if (newLeftClock != leftClock) {
			NotificationChain msgs = null;
			if (leftClock != null)
				msgs = ((InternalEObject)leftClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.CONCATENATION__LEFT_CLOCK, null, msgs);
			if (newLeftClock != null)
				msgs = ((InternalEObject)newLeftClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.CONCATENATION__LEFT_CLOCK, null, msgs);
			msgs = basicSetLeftClock(newLeftClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.CONCATENATION__LEFT_CLOCK, newLeftClock, newLeftClock));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractEntity getRightClock() {
		return rightClock;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightClock(AbstractEntity newRightClock, NotificationChain msgs) {
		AbstractEntity oldRightClock = rightClock;
		rightClock = newRightClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK, oldRightClock, newRightClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRightClock(AbstractEntity newRightClock) {
		if (newRightClock != rightClock) {
			NotificationChain msgs = null;
			if (rightClock != null)
				msgs = ((InternalEObject)rightClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK, null, msgs);
			if (newRightClock != null)
				msgs = ((InternalEObject)newRightClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK, null, msgs);
			msgs = basicSetRightClock(newRightClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK, newRightClock, newRightClock));
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case KernelExpressionPackage.CONCATENATION__LEFT_CLOCK:
				return basicSetLeftClock(null, msgs);
			case KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK:
				return basicSetRightClock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case KernelExpressionPackage.CONCATENATION__LEFT_CLOCK:
				return getLeftClock();
			case KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK:
				return getRightClock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case KernelExpressionPackage.CONCATENATION__LEFT_CLOCK:
				setLeftClock((AbstractEntity)newValue);
				return;
			case KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK:
				setRightClock((AbstractEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.CONCATENATION__LEFT_CLOCK:
				setLeftClock((AbstractEntity)null);
				return;
			case KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK:
				setRightClock((AbstractEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case KernelExpressionPackage.CONCATENATION__LEFT_CLOCK:
				return leftClock != null;
			case KernelExpressionPackage.CONCATENATION__RIGHT_CLOCK:
				return rightClock != null;
		}
		return super.eIsSet(featureID);
	}

} //ConcatenationImpl
