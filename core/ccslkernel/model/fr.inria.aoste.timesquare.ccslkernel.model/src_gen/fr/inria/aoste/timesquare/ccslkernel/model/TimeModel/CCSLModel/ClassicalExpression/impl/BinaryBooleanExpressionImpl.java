/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Boolean Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryBooleanExpressionImpl#getLeftValue <em>Left Value</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.impl.BinaryBooleanExpressionImpl#getRightValue <em>Right Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class BinaryBooleanExpressionImpl extends BooleanExpressionImpl implements BinaryBooleanExpression
{
  /**
	 * The cached value of the '{@link #getLeftValue() <em>Left Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getLeftValue()
	 * @generated
	 * @ordered
	 */
  protected BooleanExpression leftValue;

  /**
	 * The cached value of the '{@link #getRightValue() <em>Right Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @see #getRightValue()
	 * @generated
	 * @ordered
	 */
  protected BooleanExpression rightValue;

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  protected BinaryBooleanExpressionImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  protected EClass eStaticClass()
  {
		return ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public BooleanExpression getLeftValue()
  {
		return leftValue;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetLeftValue(BooleanExpression newLeftValue, NotificationChain msgs)
  {
		BooleanExpression oldLeftValue = leftValue;
		leftValue = newLeftValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE, oldLeftValue, newLeftValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setLeftValue(BooleanExpression newLeftValue)
  {
		if (newLeftValue != leftValue) {
			NotificationChain msgs = null;
			if (leftValue != null)
				msgs = ((InternalEObject)leftValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE, null, msgs);
			if (newLeftValue != null)
				msgs = ((InternalEObject)newLeftValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE, null, msgs);
			msgs = basicSetLeftValue(newLeftValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE, newLeftValue, newLeftValue));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public BooleanExpression getRightValue()
  {
		return rightValue;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  public NotificationChain basicSetRightValue(BooleanExpression newRightValue, NotificationChain msgs)
  {
		BooleanExpression oldRightValue = rightValue;
		rightValue = newRightValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE, oldRightValue, newRightValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
		public void setRightValue(BooleanExpression newRightValue)
  {
		if (newRightValue != rightValue) {
			NotificationChain msgs = null;
			if (rightValue != null)
				msgs = ((InternalEObject)rightValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE, null, msgs);
			if (newRightValue != null)
				msgs = ((InternalEObject)newRightValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE, null, msgs);
			msgs = basicSetRightValue(newRightValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE, newRightValue, newRightValue));
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE:
				return basicSetLeftValue(null, msgs);
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE:
				return basicSetRightValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE:
				return getLeftValue();
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE:
				return getRightValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eSet(int featureID, Object newValue)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE:
				setLeftValue((BooleanExpression)newValue);
				return;
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE:
				setRightValue((BooleanExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public void eUnset(int featureID)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE:
				setLeftValue((BooleanExpression)null);
				return;
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE:
				setRightValue((BooleanExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
  public boolean eIsSet(int featureID)
  {
		switch (featureID) {
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE:
				return leftValue != null;
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE:
				return rightValue != null;
		}
		return super.eIsSet(featureID);
	}

} //BinaryBooleanExpressionImpl
