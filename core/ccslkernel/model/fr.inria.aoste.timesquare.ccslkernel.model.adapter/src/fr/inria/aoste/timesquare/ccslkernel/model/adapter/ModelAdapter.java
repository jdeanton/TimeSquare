/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.model.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.EventKind;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization;
import fr.inria.aoste.timesquare.trace.util.TimeBase;
import fr.inria.aoste.timesquare.trace.util.adapter.DefaultModelAdapter;


public class ModelAdapter extends DefaultModelAdapter {
	protected ClockConstraintSystem getRoot(EObject r)
	{
		if (r instanceof ClockConstraintSystem)
			return (ClockConstraintSystem) r;
		EObject eo= r.eContainer();	
		if (eo!=null)
			return getRoot(eo);
		return null;
	}


	@Override
	public boolean accept(EObject o) {
		if (o instanceof NamedElement)
			return true;
		return false;
	}

	@Override
	public boolean isClock(EObject eo) {
		if (eo instanceof Clock)
			return true;
		if (eo instanceof Expression)
			return true;
		return super.isClock(eo);
	}

	@Override
	public String getName(EObject eo) {
		if (eo instanceof NamedElement) {
			return ((NamedElement) eo).getName();
		}
		return null;
	}
		
	public String getReferenceName(EObject o) {
		if (o == null)
			return "null";
		if (o instanceof NamedElement)
			return ((NamedElement) o).getName();
		return o.toString();
	}

	/**
	 * Fill the "list" with the set of elements referenced by "reference".
	 * The reference may itself reference other elements, the list is filled with "leaf elements": EObject in the model
	 * This is backend-dependent. See concrete implementations for details
	 * 
	 * This specific backend-dependent Model Adapter only considers clocks referenced by the model
	 * element. It will then fill the list "list" with all the tick events EObjects referenced by
	 * all the clocks referenced by the ModelElementReference mer
	 * 
	 * @param reference The reference to parse
	 * @param list A list to fill, or null if the list should be created
	 * @return the list containing the referenced elements
	 */
	@Override
	public List<EObject> fillWithReferencedElements(EObject reference, List<EObject> list) {
		if (!(reference instanceof Clock)) return super.fillWithReferencedElements(reference, list);
		List<EObject> res = list;
		if (res == null) res = new BasicEList<EObject>();
		Clock clock = (Clock)reference;
		Event ev = clock.getTickingEvent();
		if (ev != null)
			res.addAll(ev.getReferencedObjectRefs());
		return res;
	}

//	@Override
//	public String getAliasName(EObject o) {
//		if (o == null)
//			return "null";
//		if (o instanceof NamedElement)
//			return ((NamedElement) o).getName();
//		return o.toString();
//	}

//	@Override
//	public boolean isHidden(EObject r) {
//		if (r instanceof Clock || r instanceof Expression) {
//			if (r.eContainer() != null && r.eContainer() instanceof Block) {
//				return false;
//			}
//			else {
//				return true;
//			}
//		}
//		return super.isHidden(r);
//	}


	/**
	 * Returns a qualified name made by ascending the hierarchy of container starting at the object
	 * given as argument. All the object in the containement hierarchy have to be NamedElement or
	 * instances of a subclass of NamedElement, otherwise they are ignored.
	 */

	@Override
	public String getUID(EObject eo) {
		try
		{
			if (eo instanceof NamedElement) {
				StringBuilder qn= new StringBuilder();
				while (eo.eContainer() != null){
					if (eo instanceof NamedElement)
					{
						if (qn.length() != 0)
							qn.insert(0, "::") ;
						qn.insert(0, ((NamedElement)eo).getName());
					}
					eo = eo.eContainer();

				}
				return qn.toString();
			}	
		}catch (Throwable e) {
			System.err.println(e.toString());
		}
		return super.getUID(eo);
	}

	/**************************************************/

	private List<EObject> getDiscretyzeByClock(EObject r) {
		List<EObject> ls= new ArrayList<EObject>();
		if (r instanceof Expression)
			if (((Expression)r).getType() instanceof Discretization)
				for (Binding b : ((Expression)r).getBindings()){
					if ((b.getAbstract().getName().compareTo("DenseClock")==0)
							&& (b.getBindable() instanceof Clock)){
						ls.add(b.getBindable());
					}
				}
		return ls;
	}

	@Override
	public List<TimeBase> getDiscretyzeByValue(EObject r) {
		List<TimeBase> ls= new ArrayList<TimeBase>();

		if (r instanceof Expression)
			if (((Expression)r).getType() instanceof Discretization)
			{
				Double 
				//Float
				f=null;
				String unitname=null;
				for (Binding b : ((Expression)r).getBindings()){
					if ((b.getAbstract().getName().compareTo("DiscretizationFactor")==0)
							&& (b.getBindable() instanceof RealElement)){
						// ne pas faire de cast de float en double directement (==> erreur d"'arrondie CATASTROPHIQUE )
						f=Double.valueOf( ((RealElement)b.getBindable()).getValue().toString());

					}
					if ((b.getAbstract().getName().compareTo("DenseClock")==0) && (b.getBindable() instanceof Clock))
					{
						unitname= ((DenseClockType)((Clock)b.getBindable()).getType()).getBaseUnit();					
					}
				}
				if (f!=null )
				{
					if ( unitname!=null)
						ls.add(new TimeBase(f, unitname));
					else
						ls.add(new TimeBase(f));
				}
			}
		return ls;
	}



	@Override
	public HashMap<EObject, EObject> getDiscretyze(EObject r) {
		HashMap<EObject, EObject>  hm= new HashMap<EObject, EObject>();
		if (r instanceof Expression)
			if (((Expression)r).getType() instanceof Discretization){
				Element e = null;
				for (Binding b : ((Expression)r).getBindings()){
					if ((b.getAbstract().getName().compareTo("DiscretizationFactor")==0)
							&& (b.getBindable() instanceof RealElement)){
						e= ((RealElement)b.getBindable());
					}
				}
				hm.put(getDiscretyzeByClock(r).get(0) , e);					
			}
		return hm;
	}


	@Override
	public boolean isDiscrete(EObject r) {
		if (r instanceof Expression){
			if (((Expression)r).getType() instanceof Discretization){
				return true;
			}
		}
		return super.isDiscrete(r);
	}

	@Override
	public EventEnumerator getEventkind(EObject r) {
		if ( r instanceof Clock)
		{
			Event t=	((Clock) r).getTickingEvent();
			if( t!=null)
			{
				EventKind ek= t.getKind();
				for( EventEnumerator ee: EventEnumerator.values())
				{
					if ( ee.getName().equals((ek.getName())))
						return ee;
				}
			}
		}
		return super.getEventkind(r);
	}

}
