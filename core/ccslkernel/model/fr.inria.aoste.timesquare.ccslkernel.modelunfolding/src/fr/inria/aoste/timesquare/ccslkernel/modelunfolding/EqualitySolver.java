/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class EqualitySolver<T> {

	/**
	 * Register the equality between two objects of type T. 
	 * @param c1
	 * @param c2
	 */
	public void registerEquality(T c1, T c2) {
		if (c1 == c2) {
			return;
		}
		Integer class1 = equalityClassMap.get(c1);
		Integer class2 = equalityClassMap.get(c2);
		if (class1 != null) {
			if (class2 != null) {
				mergeClasses(class1, class2);
			}
			else {
				storeInClass(class1, c2);
			}
		}
		else {
			if (class2 != null) {
				storeInClass(class2, c1);
			}
			else {
				int newClass = newClassIndex();
				storeInClass(newClass, c1);
				storeInClass(newClass, c2);
			}
		}
	}

	public int getEqualityClassIndex(T cl) {
		Integer classIndex = equalityClassMap.get(cl);
		return classIndex != null ? classIndex : -1;
	}
	
	public Set<T> getEqualityClass(T cl) {
		Integer classIndex = equalityClassMap.get(cl);
		if (classIndex == null) {
			return null;
		}
		return equalityClasses.elementAt(classIndex);
	}
	
	public boolean areEqual(T c1, T c2) {
		Integer class1 = equalityClassMap.get(c1);
		Integer class2 = equalityClassMap.get(c2);
		return class1 != null && class2 != null && class1.equals(class2);
	}
	
	private Vector<Set<T>> equalityClasses = new Vector<Set<T>>();
	private HashMap<T, Integer> equalityClassMap = new HashMap<T, Integer>();

	private int newClassIndex() {
		/*
		 * First try to reuse on old class. At the beginning, the vector is
		 * created empty and a new class is created by setting a empty set
		 * structure in the vector at the right place. Equality classes can be
		 * merged with each other, and this creates holes in the vector.
		 */
		for (int i = 0; i < equalityClasses.size(); i++) {
			if (equalityClasses.elementAt(i) == null) {
				equalityClasses.setElementAt(new HashSet<T>(), i);
				return i;
			}
		}
		/*
		 * No hole found in the vector. Enlarge the vector and return the index
		 * of the newly created element, while creating a new empty set that is
		 * set in the vector in the new place.
		 */
		int newIndex = equalityClasses.size();
		equalityClasses.setSize(newIndex + 1);
		equalityClasses.setElementAt(new HashSet<T>(), newIndex);
		return newIndex;
	}

	private boolean storeInClass(Integer classIndex, T c2) {
		equalityClassMap.put(c2, classIndex);
		/*
		 * Returns true if the clock was not present in the class, false
		 * otherwise.
		 */
		return equalityClasses.elementAt(classIndex).add(c2);
	}

	/*
	 * Merges two classes, the one with the lowest index is kept and receives
	 * all clocks that pertain to the other. The class with the highest index is
	 * deleted and this creates a hole in the vector equalityClasses.
	 */
	private void mergeClasses(Integer class1, Integer class2) {
		int targetClass, removedClass;
		if (class1.compareTo(class2) == 0) {
			return;
		}
		if (class1.compareTo(class2) < 0) {
			targetClass = class1;
			removedClass = class2;
		}
		else {
			targetClass = class2;
			removedClass = class1;
		}
		for (T clock : equalityClasses.elementAt(removedClass)) {
			storeInClass(targetClass, clock);
		}
		equalityClasses.elementAt(removedClass).clear();
		equalityClasses.set(removedClass, null); // free the slot.
	}

}
