/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryIntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryRealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryIntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnaryRealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.UnarySeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util.ClassicalExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;

public class ConstantChecker {
	
	private AbstractConcreteMapping<InstantiatedElement> acm;
	private InstantiatedElement checkedElement;

	public ConstantChecker(AbstractConcreteMapping<InstantiatedElement> acm) {
		super();
		this.acm = acm;
	}

	public boolean isConstant(InstantiatedElement element) {
		if ( ! (element.getInstantiationPath().getLast() instanceof Element)) {
			return false;
		}
		if (element.isConstant()) {
			return true;
		}
		checkedElement = element;
		return isConstant((Element) element.getInstantiationPath().getLast());
	}
	
	private boolean isConstant(Element element) {
		if (element instanceof IntegerElement || element instanceof RealElement || element instanceof BooleanElement
				|| element instanceof StringElement) {
			return true;
		}
		else if (element instanceof SequenceElement) {
			boolean allConstant = true;
			for (PrimitiveElement partElement : ((SequenceElement) element).getFinitePart()) {
				allConstant = allConstant && isConstant(partElement);
				if ( ! allConstant) {
					break;
				}
			}
			for (PrimitiveElement partElement : ((SequenceElement) element).getNonFinitePart()) {
				allConstant = allConstant && isConstant(partElement);
				if ( ! allConstant) {
					break;
				}
			}
			return allConstant;
		}
		else if (element instanceof ClassicalExpression) {
			Boolean res = classicalExpressionChecker.doSwitch(element);
			if (res != null) {
				return res;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	private boolean isConstant(AbstractEntity abs) {
		// An abstract entity is bound to a value which is possibly not constant if the context of the
		// analyzed element (usually a classical expression) is a recursive expression definition. In
		// all other cases, it is constant.
		InstantiationPath path = checkedElement.getInstantiationPath();
		if (path.size() > 1) {
			// the last element of the path is the element which is checked, the element before this last one is
			// the container/context : if it is a expression definition check if it is recursive or not.
			NamedElement beforeLast = path.get(path.size() - 2);
			if (beforeLast instanceof ExpressionDefinition) {
				ExpressionDefinition def = (ExpressionDefinition) beforeLast;
				ExpressionDeclaration decl = def.getDeclaration();
				if (decl.getParameters().contains(abs)) {
					RecursiveDefinitionChecker recCheck = new RecursiveDefinitionChecker(def);
					if (recCheck.isRecursive()) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private ClassicalExpressionSwitch<Boolean> classicalExpressionChecker =
			new ClassicalExpressionSwitch<Boolean>() {
		
		// catch all clause
		public Boolean caseClassicalExpression(ClassicalExpression object) {
			return true;
		}
		
		public Boolean caseBinaryIntegerExpression(BinaryIntegerExpression object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}
		
		@Override
		public Boolean caseBinaryBooleanExpression(BinaryBooleanExpression object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}

		@Override
		public Boolean caseBinaryRealExpression(BinaryRealExpression object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}

		@Override
		public Boolean caseUnaryIntegerExpression(UnaryIntegerExpression object) {
			return isConstant(object.getOperand());
		}

		@Override
		public Boolean caseUnaryBooleanExpression(UnaryBooleanExpression object) {
			return isConstant(object.getOperand());
		}

		@Override
		public Boolean caseUnaryRealExpression(UnaryRealExpression object) {
			return isConstant(object.getOperand());
		}
		
		@Override
		public Boolean caseUnarySeqExpression(UnarySeqExpression object) {
			return isConstant(object.getOperand());
		}
		
		@Override
		public Boolean caseSeqGetHead(SeqGetHead object) {
			return isConstant(object.getOperand());
		}

		@Override
		public Boolean caseIntEqual(IntEqual object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}

		@Override
		public Boolean caseIntInf(IntInf object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}

		@Override
		public Boolean caseIntSup(IntSup object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}

		@Override
		public Boolean caseSeqIsEmpty(SeqIsEmpty object) {
			return isConstant(object.getOperand());
		}
		
		@Override
		public Boolean caseRealInf(RealInf object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}

		@Override
		public Boolean caseRealEqual(RealEqual object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}

		@Override
		public Boolean caseRealSup(RealSup object) {
			return isConstant(object.getLeftValue()) && isConstant(object.getRightValue());
		}

		@Override
		public Boolean caseIntegerRef(IntegerRef object) {
			return isConstant(object.getIntegerElem());
		}

		@Override
		public Boolean caseBooleanRef(BooleanRef object) {
			return isConstant(object.getReferencedBool());
		}

		@Override
		public Boolean caseRealRef(RealRef object) {
			return isConstant(object.getRealElem());
		}

		@Override
		public Boolean caseNumberSeqRef(NumberSeqRef object) {
			return isConstant(object.getReferencedNumberSeq());
		}

		/*
		 * References to AbstractEntity
		 */
		@Override
		public Boolean caseIntegerVariableRef(IntegerVariableRef object) {
			return isConstant(object.getReferencedVar())
					&& isConstant(acm.resolveAbstractEntity(object.getReferencedVar()));
		}
		
		@Override
		public Boolean caseBooleanVariableRef(BooleanVariableRef object) {
			return isConstant(object.getReferencedVar())
					&& isConstant(acm.resolveAbstractEntity(object.getReferencedVar()));
		}

		@Override
		public Boolean caseNumberSeqVariableRef(NumberSeqVariableRef object) {
			return isConstant(object.getReferencedVar())
					&& isConstant(acm.resolveAbstractEntity(object.getReferencedVar()));
		}

		@Override
		public Boolean caseRealVariableRef(RealVariableRef object) {
			return isConstant(object.getReferencedVar()) 
					&& isConstant(acm.resolveAbstractEntity(object.getReferencedVar()));
		}
		
	};
	
}
