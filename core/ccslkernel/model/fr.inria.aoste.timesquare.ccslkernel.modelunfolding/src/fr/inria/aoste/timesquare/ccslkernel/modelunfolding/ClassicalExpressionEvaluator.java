/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntDivide;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqDecr;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util.ClassicalExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.AbstractConcreteMapping;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.EvaluationTypeError;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.ExceptionWrapper;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnimplementedEvaluation;

public class ClassicalExpressionEvaluator {

	private AbstractConcreteMapping<InstantiatedElement> acm;
	
	public ClassicalExpressionEvaluator(AbstractConcreteMapping<InstantiatedElement> acm) {
		super();
		this.acm = acm;
	}
		
	public Element evaluate(InstantiatedElement elt) throws UnfoldingException {
		try {
			return evaluate( (Element) elt.getInstantiationPath().getLast());
		}
		catch (ExceptionWrapper e) {
			throw (UnfoldingException) e.getCause();
		}
	}
	
	private Element evaluate(Element elt) {
		if (elt instanceof ClassicalExpression) {
			if (elt instanceof SeqExpression) {
				return seqEvaluator.doSwitch(elt);
			}
			else {
				Element res;
				res = evaluator.doSwitch(elt);
				return res;
			}
		}
		else {
			return elt;
		}
	}

	/*
	 * Object that performs evaluation of classical expressions that return only PrimitiveElement and not
	 * SequenceElement because these are replaced internally by SolverSequenceElement.
	 */
	private ClassicalExpressionSwitch<Element> evaluator = new ClassicalExpressionSwitch<Element>() {
		
		/*
		 * Catch all clause
		 */
		public Element caseClassicalExpression(ClassicalExpression object) {
			throw new ExceptionWrapper(new UnimplementedEvaluation(object.getName() + ":" + object.toString()));
		}
		/*
		 * References throw the current Abstract->Concrete mapping (ie. the "context")
		 */
		@Override
		public Element caseIntegerVariableRef(IntegerVariableRef object) {
			InstantiatedElement value = acm.resolveAbstractEntity(object.getReferencedVar());
			if ( ! (value.getInstantiationPath().getLast() instanceof IntegerElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("IntegerVariableRef: IntegerElement expected"));
			}
			return (Element) value.getInstantiationPath().getLast();
		}
		
		@Override
		public Element caseRealVariableRef(RealVariableRef object) {
			InstantiatedElement value = acm.resolveAbstractEntity(object.getReferencedVar());
			if (!(value.getInstantiationPath().getLast() instanceof RealElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("RealVariableRef: RealElement expected"));
			}
			return (Element) value.getInstantiationPath().getLast();
		}
		
		/*
		 * Boolean expressions : return BooleanElement
		 */
		@Override
		public Element caseAnd(And object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof BooleanElement) ||
					! (right instanceof BooleanElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("And: BooleanElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getName() + " AND " + right.getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue( ((BooleanElement)left).getValue()
							&& ((BooleanElement)right).getValue());
			return res;
		}
		
		@Override
		public Element caseOr(Or object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof BooleanElement) ||
					! (right instanceof BooleanElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("And: BooleanElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getName() + " AND " + right.getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue( ((BooleanElement)left).getValue()
							|| ((BooleanElement)right).getValue());
			return res;
		}
		
		@Override
		public Element caseNot(Not object) {
			BooleanExpression operand = object.getOperand();
			Element operandValue = evaluate(operand);
			if ( ! (operandValue instanceof BooleanElement) ) {
				throw new ExceptionWrapper(new EvaluationTypeError("Not: BooleanElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(object.getName() + ":" + operand.getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue( ! ((BooleanElement) operandValue).getValue());
			return res;
		}

		@Override
		public Element caseIntEqual(IntEqual object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof IntegerElement) ||
					! (right instanceof IntegerElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("IntEqual: IntegerElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getName() + "==" + right.getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue(((IntegerElement) left).getValue()
						== ((IntegerElement) right).getValue());
			return res;
		}
		
		@Override
		public Element caseIntInf(IntInf object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof IntegerElement) ||
					! (right instanceof IntegerElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("IntEqual: IntegerElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getName() + "<" + right.getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue(((IntegerElement) left).getValue() 
							< ((IntegerElement) right).getValue());
			return res;
		};
		
		@Override
		public Element caseIntSup(IntSup object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof IntegerElement) ||
					! (right instanceof IntegerElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("IntEqual: IntegerElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getName() + ">" + right.getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue(((IntegerElement) left).getValue() 
							> ((IntegerElement) right).getValue());
			return res;
		};
		
		@Override
		public Element caseSeqIsEmpty(SeqIsEmpty object) {
			Element operand = evaluate(object.getOperand());
			if (! (operand instanceof SequenceElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("SeqIsEmpty: SolverSequenceElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName("isEmpty(" + operand.getName() + ")");
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue(isEmptySequence((SequenceElement)operand));
			return res;
		}
		
		private boolean isEmptySequence(SequenceElement seq) {
			return ( seq.getFinitePart().isEmpty() && seq.getNonFinitePart().isEmpty() );
		}
		/*
		 * Integer expressions : return IntegerElement
		 */
		@Override
		public Element caseIntegerRef(IntegerRef object) {
			return object.getIntegerElem();
		}
		
		@Override
		public Element caseIntPlus(IntPlus object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof IntegerElement) ||
					! (right instanceof IntegerElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("IntPlus: IntegerElement expected"));
			}
			IntegerElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createIntegerElement();
			res.setValue(((IntegerElement) left).getValue()
						+ ((IntegerElement) right).getValue());
			res.setType(left.getType());
			return res;
		}
		
		@Override
		public Element caseIntMinus(IntMinus object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof IntegerElement) ||
					! (right instanceof IntegerElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("IntMinus: IntegerElement expected"));
			}
			IntegerElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createIntegerElement();
			res.setValue(((IntegerElement) left).getValue()
						- ((IntegerElement) right).getValue());
			res.setType(left.getType());
			return res;			
		}
		
		public Element caseIntMultiply(IntMultiply object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof IntegerElement) ||
					! (right instanceof IntegerElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("IntMultiply: IntegerElement expected"));
			}
			IntegerElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createIntegerElement();
			res.setValue(((IntegerElement) left).getValue()
						* ((IntegerElement) right).getValue());
			res.setType(left.getType());
			return res;
		}
		
		@Override
		public Element caseIntDivide(IntDivide object) {
			Element left = evaluate(object.getLeftValue());
			Element right = evaluate(object.getRightValue());
			if (! (left instanceof IntegerElement) ||
					! (right instanceof IntegerElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("IntDivide: IntegerElement expected"));
			}
			IntegerElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createIntegerElement();
			res.setValue(((IntegerElement) left).getValue()
						/ ((IntegerElement) right).getValue());
			res.setType(left.getType());
			return res;
		}
		
		@Override
		public Element caseSeqGetHead(SeqGetHead object) {
			SeqExpression operand = object.getOperand();
			Element value = evaluate(operand);
			if ( ! (value instanceof SequenceElement)) {
				throw new ExceptionWrapper(new EvaluationTypeError("SeqGetHead: SolverSequenceElement expected"));
			}
			PrimitiveElement res = getSequenceHead((SequenceElement)value);
			return evaluate(res);

		}
	};
	
	private PrimitiveElement getSequenceHead(SequenceElement seq) {
		if (seq.getFinitePart().isEmpty()) {
			if (seq.getNonFinitePart().isEmpty())	{
				return null;
			}
			else {
				return seq.getNonFinitePart().get(0);
			}
		}
		else {
			return seq.getFinitePart().get(0);
		}
	}
	
	/*
	 * Object that evaluates only expressions that returns SequenceElement, actually SolverSequenceElement.
	 */
	private ClassicalExpressionSwitch<SequenceElement> seqEvaluator
		= new ClassicalExpressionSwitch<SequenceElement>() {
		/*
		 * Catch all clause
		 */
		@Override
		public SequenceElement caseClassicalExpression(ClassicalExpression object) {
			throw new ExceptionWrapper(new UnimplementedEvaluation(object.getName() + ":" + object.toString()));
		}
		@Override
		public SequenceElement caseSeqGetTail(SeqGetTail object) {
			SeqExpression operand = object.getOperand();
			Element value = evaluate(operand);
			if ( ! (value instanceof SequenceElement)) {
				throw new ExceptionWrapper( new EvaluationTypeError("SeqGetTail: SolverSequenceElement expected"));
			}
			SequenceElement res = getSequenceTail((SequenceElement)value);
			return res;
		}
		
		private SequenceElement getSequenceTail(SequenceElement seq) {
			SequenceElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createSequenceElement();
			if ( ! seq.getFinitePart().isEmpty()) {
				res.getFinitePart().addAll(seq.getFinitePart());
				res.getFinitePart().remove(0);
			}
			else {
				res.getFinitePart().addAll(seq.getNonFinitePart());
				res.getFinitePart().remove(0);
				res.getNonFinitePart().addAll(seq.getNonFinitePart());
			}
			return res;
		}
		
		@Override
		public SequenceElement caseSeqDecr(SeqDecr object) {
			throw new ExceptionWrapper(new UnimplementedEvaluation("SeqDecr: unimplemented"));
		}
		@Override
		public SequenceElement caseSeqSched(SeqSched object) {
			throw new ExceptionWrapper(new UnimplementedEvaluation("SeqSched: unimplemented"));
		}
		@Override
		public SequenceElement caseNumberSeqVariableRef(NumberSeqVariableRef object) {
			InstantiatedElement value = acm.resolveAbstractEntity(object.getReferencedVar());
			NamedElement res = value.getInstantiationPath().getLast();
			if (! (res instanceof SequenceElement)) {
				throw new ExceptionWrapper( new EvaluationTypeError("NumberSeqVariableRef: SolverSequenceElement expected"));
			}
			return (SequenceElement)res;
		}
	};
	
}
