/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.InstantiationTreeMergeConflict;

/**
 * A class that store an instantiation tree where the data that are stored in nodes are of type V. At each node of the
 * tree, the subnodes are associated with a key which is a NamedElement : this key acts as a labeling of the edge from the node
 * to each of its subnode. The tree allows to store and retrieve objects of class V given an instantiation path which
 * is a list of NamedElement. Each node of the tree has an instantiation path which
 * is the reverse of the list of NamedElement instances going from the node to the root of the tree.
 * A node in the tree may be empty (ie. does not contain an object of type V) if this node only exists to allow to store
 * other V objects in the subtree rooted at the empty node. 
 * 
 * @author Nicolas Chleq
 *
 * @param <V> the type (class) of object to store in the tree.
 * @see InstantiationPath
 * @see NamedElement
 */
public class InstantiationTree<V> extends HashMap<NamedElement, InstantiationTree<V> > {

	private static final long serialVersionUID = -8313768353453534851L;

	private V data;
	
	private InstantiationTree<V> parent;

	public InstantiationTree() {
		super();
		this.parent = null;
		this.data = null;
	}
	
	private InstantiationTree(InstantiationTree<V> parent) {
		super();
		this.parent = parent;
		this.data = null;
	}
	
	/**
	 * Gets the data associated to the node.
	 *
	 * @return the data
	 */
	public V getData() {
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(V data) {
		this.data = data;
	}
	
	/**
	 * Store an instance in the tree using the instantiation path to travel from the root of the tree to the
	 * node in which the instance will be stored.
	 *
	 * @param instantiationPath the instantiation path
	 * @param instance the instance to store
	 */
	public void storeInstance(InstantiationPath instantiationPath, V instance) {
		storeInstanceImpl(instantiationPath, 0, instance);
	}
	
	private void storeInstanceImpl(InstantiationPath instantiationPath, int index, V instance) {
		if (index == instantiationPath.size()) {
			data = instance;
		}
		else {
			NamedElement key = instantiationPath.get(index);
			InstantiationTree<V> node = get(key);
			if (node == null) {
				node = new InstantiationTree<V>(this);
				put(key, node);
			}
			node.storeInstanceImpl(instantiationPath, index + 1, instance);
		}
	}
	
	/**
	 * Lookup the instance that has the associated instantiation path. Returns null if no such instance exists
	 * in the tree.
	 *
	 * @param instantiationPath the instantiation path
	 * @return the instance if it exists, or null otherwise
	 */
	public V lookupInstance(InstantiationPath instantiationPath) {
		InstantiationTree<V> node = lookupNode(instantiationPath, 0);
		if (node == null) {
			return null;
		}
		else {
			return node.data;
		}
	}
	
	private InstantiationTree<V> lookupNode(InstantiationPath instantiationPath, int index) {
		if ( (instantiationPath == null) || (index >= instantiationPath.size()))
			return this;
		NamedElement key = instantiationPath.get(index);
		InstantiationTree<V> node = get(key);
		if (node == null) {
			return null;
		}
		return node.lookupNode(instantiationPath, index + 1);
	}
		
	public V lookupInstance(String qualifiedPath, String separator) {
		String[] subs = qualifiedPath.split(separator);
		InstantiationTree<V> node = lookupNode(subs, 0);
		if (node == null) {
			return null;
		}
		else {
			return node.data;
		}
	}
	
	private InstantiationTree<V> lookupNode(String[] subs, int index) {
		if ( (subs == null) || (index >= subs.length))
			return this;
		String keyName = subs[index];
		InstantiationTree<V> node = findSubNodeByName(keyName);
		if (node == null) {
			return null;
		}
		return node.lookupNode(subs, index + 1);
	}

	private InstantiationTree<V> findSubNodeByName(String name) {
		Set<Map.Entry<NamedElement, InstantiationTree<V>>> entries = this.entrySet();
		for (Map.Entry<NamedElement, InstantiationTree<V>> entry : entries) {
			if (entry.getKey().getName().compareTo(name) == 0) {
				return entry.getValue();
			}
		}
		return null;
	}
	
	public V lookupInstance(String qualifiedPath) {
		return lookupInstance(qualifiedPath, InstantiationPath.defaultSeparator);
	}
	
	/**
	 * Lookup instance in the tree with a depth limit. The root of the tree is at depth 0. With this function
	 * only the beginning part of the path is used up to a length which is the depth limit.
	 *
	 * @param path the path
	 * @param depth the depth limit
	 * @return the v
	 */
	public V lookupInstance(InstantiationPath path, int depth) {
		InstantiationTree<V> node = lookupNode(path, 0, depth);
		if (node == null)
			return null;
		else
			return node.data;
	}
	
	private InstantiationTree<V> lookupNode(InstantiationPath path, int index,
			int depth) {
		if ( (path == null) || (index >= path.size()) || depth == 0)
			return this;
		NamedElement key = path.get(index);
		InstantiationTree<V> node = get(key);
		if (node == null) {
			return null;
		}
		return node.lookupNode(path, index + 1, depth - 1);
	}

	/**
	 * Gets all the instances that share a common prefix in their instantiation path.
	 *
	 * @param instantiationPath the lookup path
	 * @return the list of instances
	 */
	public List<V> lookupInstances(InstantiationPath instantiationPath) {
		InstantiationTree<V> node = lookupNode(instantiationPath, 0);
		if (node == null) {
			return new ArrayList<V>();
		}
		return node.collectInSubTree();
	}
	
	private List<V> collectInSubTree() {
		List<V> res = new ArrayList<V>();
		if (this.data != null)
			res.add(data);
		for (InstantiationTree<V> node : this.values()) {
			res.addAll(node.collectInSubTree());
		}
		return res;
	}
	
	/**
	 * Lookup instances at a specified depth. If depth is zero and if there is an instance stored in the tree at the
	 * specified path, then the function returns this instance in a list with one element. If the depth is zero and there
	 * is no such instance, the returned list is empty. With depth set to 1, the function returns the direct sons of the
	 * instance stored at the instantiation path. 
	 *
	 * @param instantiationPath the instantiation path
	 * @param depth the depth
	 * @return the list
	 */
	public List<V> lookupInstances(InstantiationPath instantiationPath, int depth) {
		InstantiationTree<V> node = lookupNode(instantiationPath, 0);
		if (node == null) {
			return new ArrayList<V>();
		}
		return node.collectInSubTree(depth);
	}
	
	private List<V> collectInSubTree(int depth) {
		List<V> res = new ArrayList<V>();
		if (this.data != null)
			res.add(data);
		if (depth > 0) {
			for (InstantiationTree<V> node : this.values()) {
				res.addAll(node.collectInSubTree(depth - 1));
			}
		}
		return res;
	}
	
	/**
	 * Returns the list of elements stored in the tree along all the path of a specified node.
	 * The node itself does not belong to the returned list.
	 *   
	 * @param nodeData the element in the tree from which to get all the parents
	 * @return the list of all parents of nodeData in the tree 
	 */
	public List<V> getParentList(V nodeData) {
		List<V> res = new ArrayList<V>();
		// locate the node which has the same data
		InstantiationTree<V> tree = searchNode(nodeData);
		// ascend towards the root from this node
		while (tree != null) {
			if (tree.data != null && tree.data != nodeData) {
				res.add(tree.data);
			}
			tree = tree.parent;
		}
		return res;
	}

	private InstantiationTree<V> searchNode(V nodeData) {
		if (this.data != null && this.data == nodeData) {
			return this;
		}
		for (InstantiationTree<V> node : this.values()) {
			InstantiationTree<V> res = node.searchNode(nodeData);
			if (res != null)
				return res;
		}
		return null;
	}
	
	public InstantiationTree<V> mergeWith( InstantiationTree<V> other ) throws InstantiationTreeMergeConflict {
		if (other.data != null) {
			if (this.data == null) {
				this.data = other.data;
			}
			else if (this.data != other.data) {
				// Conflict !!
				throw new InstantiationTreeMergeConflict();
			}
		}
		// Iterate over all elements of the other tree and store them in the current tree.
		for (Map.Entry<NamedElement, InstantiationTree<V> > entry : other.entrySet()) {
			NamedElement key = entry.getKey();
			InstantiationTree<V> subTree = entry.getValue();
			if (this.containsKey(key)) {
				this.get(key).mergeWith(subTree);
			}
			else {
				InstantiationTree<V> newTree = new InstantiationTree<V>(this);
				this.put(key, newTree.mergeWith(subTree));
			}
		}
		return this;
	}
	
}
