/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception;

public class NoModelImportTreeRoot extends UnfoldingException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3004381658728053540L;

	public NoModelImportTreeRoot() {
	}

	public NoModelImportTreeRoot(String message) {
		super(message);
	}

	public NoModelImportTreeRoot(Throwable cause) {
		super(cause);
	}

	public NoModelImportTreeRoot(String message, Throwable cause) {
		super(message, cause);
	}

}
