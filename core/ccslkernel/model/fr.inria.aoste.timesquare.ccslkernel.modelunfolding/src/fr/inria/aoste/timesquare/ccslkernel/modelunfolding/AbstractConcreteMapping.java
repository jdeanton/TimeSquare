/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.util.HashMap;
import java.util.Map;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;

public class AbstractConcreteMapping<T> extends HashMap<AbstractEntity, T> {

	private static final long serialVersionUID = 5820014365703388868L;
	
	private AbstractConcreteMapping<T> parent;
	
	public AbstractConcreteMapping()
	{
		super();
		this.parent = null;
	}
	public AbstractConcreteMapping(AbstractConcreteMapping<T> acm)
	{
		super();
		this.parent = acm;
	}
	public AbstractConcreteMapping<T> getParent() {
		return parent;
	}
	
	public T resolveAbstractEntity(AbstractEntity abs)
	{
		if (this.containsKey(abs)) {
			return get(abs);
		}
		if (parent == null) {
			return null;
		}
		return parent.resolveAbstractEntity(abs);
	}
	
	public T resolveAbstractEntity(String abstractName)
	{
		T res = getLocalValue(abstractName);
		if (res == null) {
			if (parent == null) {
				return null;
			}
			else {
				return parent.resolveAbstractEntity(abstractName);
			}
		}
		else {
			return res;
		}
	}
	
	public T setAbstractEntityValue(AbstractEntity abs, T value) {
		if (this.containsKey(abs)) {
			return put(abs, value);
		}
		if (parent == null) {
			return null;
		}
		return parent.setAbstractEntityValue(abs, value);
	}
	
	public T getLocalValue(AbstractEntity key) {
		return get(key);
	}
	
	public T getLocalValue(String abstractName) {
		T res = null;
		for (java.util.Map.Entry<AbstractEntity, T> entry : this.entrySet()) {
			if (entry.getKey().getName().compareTo(abstractName) == 0) {
				res = entry.getValue();
			}
		}
		return res;
	}
	
	public T setLocalValue(AbstractEntity key, T value) {
		return put(key, value);
	}
	
	@Override
	public String toString() {
		String res = "{";
		for (Map.Entry<AbstractEntity, T> entry : this.entrySet()) {
			res += "(" + entry.getKey().getName() + " -> " + entry.getValue() + ")";
		}
		res += "}";
		return res;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public AbstractConcreteMapping<T> clone() {
		return (AbstractConcreteMapping<T>) super.clone();
	}
	
}
