/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.util.ArrayDeque;
import java.util.Deque;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.visitor.Visitor;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.DefinitionNotFound;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.ElementNotInstantiated;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.ExceptionWrapper;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.NotInScope;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnboundAbstract;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;

public class ModelUnfoldingPassTwo extends Visitor<InstantiatedElement> {

	protected Deque<Block> blockStack;
	protected InstantiationPath instantiationPath;
	protected UnfoldModel unfoldModel;
	protected Deque<AbstractConcreteMapping<InstantiatedElement>> mappingStack;
	protected InstantiatedElement dummy = new InstantiatedElement(null, null);
	protected EqualitySolver<InstantiatedElement> equalitySolver;

	public ModelUnfoldingPassTwo(UnfoldModel unfoldModel) {
		this.unfoldModel = unfoldModel;
		this.instantiationPath = new InstantiationPath();
		this.blockStack = new ArrayDeque<Block>();
		this.mappingStack = new ArrayDeque<AbstractConcreteMapping<InstantiatedElement>>();
		this.mappingStack.push(new AbstractConcreteMapping<InstantiatedElement>());
		this.equalitySolver = new EqualitySolver<InstantiatedElement>();
	}

	public EqualitySolver<InstantiatedElement> getEqualitySolver() {
		return equalitySolver;
	}
	
	private void pushAbstractConcreteMapping() {
		AbstractConcreteMapping<InstantiatedElement> current = mappingStack.peek();
		mappingStack.push(new AbstractConcreteMapping<InstantiatedElement>(current));
	}

	private void popAbstractConcreteMapping() {
		mappingStack.pop();
	}

	private boolean isElementInLibrary(NamedElement elt) {
		EObject container = elt.eContainer();
		return (container != null &&
				(container instanceof ExpressionLibrary
					|| container instanceof RelationLibrary));
	}
	
	private InstantiatedElement lookupElementInLibrary(NamedElement elt) {
		InstantiatedElement res = null;
		InstantiationPath physicalPath = new InstantiationPath();
		for (EObject container1 = elt; container1 != null; container1 = container1
				.eContainer()) {
			physicalPath.add(0, (NamedElement) container1);
		}
		res = unfoldModel.getInstantiationTree().lookupInstance(physicalPath);
		if (res == null) {
			res = new InstantiatedElement(physicalPath, null);
			unfoldModel.getInstantiationTree().storeInstance(physicalPath, res);
		}
		return res;
	}
	
	private boolean isElementInImportedModel(NamedElement elt) {
		EObject rootContainer = EcoreUtil.getRootContainer(elt);
		return rootContainer != null && rootContainer instanceof ClockConstraintSystem;
	}
	
	private InstantiatedElement lookupElementInImportedModel(NamedElement elt) {
		InstantiatedElement res = null;
		InstantiationPath physicalPath = new InstantiationPath();
		for (EObject container1 = elt; container1 != null; container1 = container1
				.eContainer()) {
			physicalPath.add(0, (NamedElement) container1);
		}
		res = unfoldModel.getInstantiationTree().lookupInstance(physicalPath);
		if (res == null) {
			res = new InstantiatedElement(physicalPath, null);
			unfoldModel.getInstantiationTree().storeInstance(physicalPath, res);
		}
		return res;
	}
	
	private InstantiatedElement lookupElement(NamedElement elt) {
		InstantiatedElement res = null;
		EObject container = elt.eContainer();
		// Special case of elements contained in libraries
		if (isElementInLibrary(elt)) {
			return lookupElementInLibrary(elt);
		}
		if (isElementInImportedModel(elt)) {
			return lookupElementInImportedModel(elt);
		}
		if ( ! instantiationPath.contains(container)) {
			throw new ExceptionWrapper(new NotInScope("Element " + elt.getName()
					+ " not defined in scope"));
		}
		InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
		while (!lookupPath.isEmpty()) {
			if (lookupPath.peekLast() == container) {
				break;
			}
			lookupPath.pop();
		}
		lookupPath.push(elt);
		res = unfoldModel.getInstantiationTree().lookupInstance(lookupPath);
		if (res == null) {
			throw new ExceptionWrapper(new ElementNotInstantiated("Element "
					+ elt.getName() + " not instantiated"));
		}
		return res;
	}

	private void resolveBinding(Binding bd) {
		AbstractEntity abs = bd.getAbstract();
		BindableEntity bindable = bd.getBindable();
		InstantiatedElement value = null;
		if (bindable instanceof AbstractEntity) {
			value = mappingStack.peek().resolveAbstractEntity((AbstractEntity) bindable);
		}
		else {
			value = lookupElement(bindable);
		}
		value.incUseCount();
		mappingStack.peek().setLocalValue(abs, value);
	}

	private void checkParameters(EList<AbstractEntity> paramList) {
		for (AbstractEntity param : paramList) {
			InstantiatedElement value = mappingStack.peek().resolveAbstractEntity(param);
			if (value == null) {
				throw new ExceptionWrapper(new UnboundAbstract("Parameter "
						+ param.getName() + " not bound"));
			}
		}
	}

	@Override
	protected InstantiatedElement visitClockConstraintSystem(ClockConstraintSystem cs) {
		blockStack.push(cs);
		instantiationPath.push(cs);
		InstantiatedElement elt = visit(cs.getSuperBlock());
		instantiationPath.pop();
		blockStack.pop();
		return elt;
	}

	@Override
	protected InstantiatedElement visitBlock(Block object) {
		if (blockStack.contains(object)) {
			return dummy;
		}
		blockStack.push(object);
		instantiationPath.push(object);
		for (Element elt : object.getElements()) {
			visit(elt);
		}
		for (Expression ex : object.getExpressions()) {
			visit(ex);
		}
		for (Relation rel : object.getRelations()) {
			visit(rel);
		}
		for (Block sub : object.getSubBlock()) {
			visit(sub);
		}
		for(ClassicalExpression classicalExpr : object.getClassicalExpression()){
			visit(classicalExpr);
		}
		instantiationPath.pop();
		blockStack.pop();
		return dummy;
	}

	@Override
	protected InstantiatedElement visitClock(Clock c) {
		instantiationPath.push(c);
		InstantiatedElement element = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		if (element == null) {
			throw new ExceptionWrapper(new ElementNotInstantiated("Clock "
					+ instantiationPath.toString() + " not instantiated"));
		}
		element.setAbstractMapping(mappingStack.peek());
		element.setLeaf(true);
		instantiationPath.pop();
		return element;
	}

	@Override
	protected InstantiatedElement visitClassicalExpression(ClassicalExpression object) {
		instantiationPath.push(object);
		InstantiatedElement element = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		if (element == null) {
			throw new ExceptionWrapper(new ElementNotInstantiated("ClassicalExpression "
					+ instantiationPath.toString() + " not instantiated"));
		}
		element.setAbstractMapping(mappingStack.peek());
		element.setLeaf(true);
		try {
			ConstantChecker constChecker = new ConstantChecker(mappingStack.peek());
			boolean isConstant = constChecker.isConstant(element);
			if (isConstant) {
				ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(
						mappingStack.peek());
				Element value = eval.evaluate(element);
				element.setConstant(true);
				element.setValue(value);
			}
		} catch (UnfoldingException e) {
			// indicates that evaluation is not statically possible.
			element.setConstant(false);
			element.setValue(null);
		}
		instantiationPath.pop();
		return element;
	}

	@Override
	protected InstantiatedElement visitPrimitiveElement(PrimitiveElement el) {
		instantiationPath.push(el);
		InstantiatedElement element = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		if (element == null) {
			throw new ExceptionWrapper(new ElementNotInstantiated("Element "
					+ instantiationPath.toString() + " not instantiated"));
		}
		element.setAbstractMapping(mappingStack.peek());
		element.setLeaf(true);
		instantiationPath.pop();
		return element;
	}

	@Override
	protected InstantiatedElement visitExpression(Expression ex) {
		instantiationPath.push(ex);
		pushAbstractConcreteMapping();
		for (Binding b : ex.getBindings()) {
			resolveBinding(b);
		}
		InstantiatedElement element = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		if (element == null) {
			throw new ExceptionWrapper(new ElementNotInstantiated("Expression "
					+ instantiationPath.toString() + " not instantiated"));
		}
		element.setAbstractMapping(mappingStack.peek());
		ExpressionDeclaration decl = ex.getType();
		if (decl instanceof KernelExpressionDeclaration) {
			element.setLeaf(true);
			element.setIsKernel(true);
		}
		else {
			if (decl instanceof ExpressionDeclaration) {
				checkParameters(decl.getParameters());
			}
			element.setLeaf(false);
			element.setIsKernel(false);
			ExpressionDefinition def = unfoldModel.lookupExpressionDefinition(decl);
			if (def == null) {
				throw new ExceptionWrapper(new DefinitionNotFound(
						"No definition found for expression declaration "
								+ decl.getName()));
			}
			element.setDefinition(def);
			unfoldModel.recordExpressionDefinitionUse(decl, def);
			if (def instanceof ExternalExpressionDefinition) {
				element.setLeaf(true);
			}
			else { // Is the current expression a recursive call in a tail
					// recursive definition ?
				int defIndex = instantiationPath.lastIndexOf(def);
				RecursiveDefinitionChecker checker = new RecursiveDefinitionChecker(def);
				if (defIndex >= 0 && defIndex == instantiationPath.size() - 2
						&& checker.isTailRecursive()) {
					// mark the element as a tail recursive call
					element.setRecursiveCall(true);
					element.setTailRecursiveCall(true);
				}
				else {
					// we can visit the definition (expanding it) without risk
					// of looping.
					visit(def);
				}
				if (def instanceof ConditionalExpressionDefinition) {
					element.setConditional(true);
				}
				else {
					element.setConditional(false);
				}
			}
		}
		popAbstractConcreteMapping();
		instantiationPath.pop();
		return element;
	}

	@Override
	protected InstantiatedElement visitUserExpressionDefinition(
			UserExpressionDefinition def) {
		InstantiatedElement current = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		instantiationPath.push(def);
		for (ConcreteEntity concrete : def.getConcreteEntities()) {
			visit(concrete);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(concrete);
			InstantiatedElement element = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			element.setParent(current);
			if (concrete == def.getRootExpression()) {
				InstantiationPath rootLookupPath = new InstantiationPath(
						instantiationPath);
				rootLookupPath.push(def.getRootExpression());
				InstantiatedElement rootExpression = unfoldModel.getInstantiationTree()
						.lookupInstance(rootLookupPath);
				if (rootExpression == null) {
					throw new ExceptionWrapper(new ElementNotInstantiated(
							"Root expression not instantiated "
									+ rootLookupPath.getLast().getName() + " in path "
									+ instantiationPath.toString()));
				}
				current.setRootExpression(rootExpression);
				equalitySolver.registerEquality(current, rootExpression);
			}
		}
		
		for(ClassicalExpression classicalExpression : def.getClassicalExpressions()){
			visit(classicalExpression);
		}
		
		instantiationPath.removeLast(def);
		return current;
	}

	@Override
	protected InstantiatedElement visitConditionalExpressionDefinition(
			ConditionalExpressionDefinition cd) {
		InstantiatedElement parent = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		parent.setConditional(true);
		instantiationPath.push(cd);
		for (ClassicalExpression expr : cd.getClassicalExpressions()) {
			visit(expr);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(expr);
			InstantiatedElement element = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			element.setParent(parent);
		}
		for (ConcreteEntity concrete : cd.getConcreteEntities()) {
			visit(concrete);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(concrete);
			InstantiatedElement element = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			element.setParent(parent);
		}
		for (ExprCase eCase : cd.getExprCases()) {
			visit(eCase);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(eCase.getExpression());
			InstantiatedElement caseExpression = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			parent.addConditionCase(caseExpression);
			caseExpression.setConditionCase(true);
			caseExpression.setParent(parent);
			lookupPath.pop();
			lookupPath.push(eCase.getCondition());
			InstantiatedElement caseTest = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			caseExpression.setConditionTest(caseTest);
		}
		if (cd.getDefaultExpression() != null) {
			visit(cd.getDefaultExpression());
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(cd.getDefaultExpression());
			InstantiatedElement defaultExpression = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			parent.setDefaultCase(defaultExpression);
			defaultExpression.setConditionTest(null);
			defaultExpression.setConditionCase(true);
			defaultExpression.setParent(parent);
		}
		instantiationPath.pop();
		return parent;
	}

	@Override
	protected InstantiatedElement visitExprCase(ExprCase object) {
		visit(object.getCondition());
		return visit(object.getExpression());
	}

	@Override
	protected InstantiatedElement visitRelation(Relation rel) {
		instantiationPath.push(rel);
		pushAbstractConcreteMapping();
		for (Binding b : rel.getBindings()) {
			resolveBinding(b);
		}
		InstantiatedElement element = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		if (element == null) {
			throw new ExceptionWrapper(new ElementNotInstantiated("Relation "
					+ instantiationPath.toString() + " not instantiated"));
		}
		element.setAbstractMapping(mappingStack.peek());
		element.setAssertion(rel.getIsAnAssertion());
		RelationDeclaration decl = rel.getType();
		if (decl instanceof KernelRelationDeclaration) {
			element.setLeaf(true);
			element.setIsKernel(true);
			if (decl instanceof Coincidence) {
				AbstractEntity left = ((KernelRelationDeclaration) decl).getLeftEntity();
				AbstractEntity right = ((KernelRelationDeclaration) decl).getRightEntity();
				InstantiatedElement leftElt = mappingStack.peek().resolveAbstractEntity(left);
				InstantiatedElement rightElt = mappingStack.peek().resolveAbstractEntity(right);
				equalitySolver.registerEquality(leftElt, rightElt);
			}
		}
		else {
			if (decl instanceof RelationDeclaration) {
				checkParameters(decl.getParameters());
			}
			element.setLeaf(false);
			element.setIsKernel(false);
			RelationDefinition def = unfoldModel.lookupRelationDefinition(decl);
			if (def == null) {
				throw new ExceptionWrapper(new DefinitionNotFound(
						"No definition found for relation declaration" + decl.getName()));
			}
			element.setDefinition(def);
			unfoldModel.recordRelationDefinitionUse(decl, def);
			if (def instanceof ExternalRelationDefinition) {
				element.setLeaf(true);
			}
			else {
				visit(def);
				if (def instanceof ConditionalRelationDefinition) {
					element.setConditional(true);
				}
			}
		}
		popAbstractConcreteMapping();
		instantiationPath.pop();
		return element;
	}

	@Override
	protected InstantiatedElement visitUserRelationDefinition(
			UserRelationDefinition object) {
		InstantiatedElement parent = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		instantiationPath.push(object);
		for (ConcreteEntity concrete : object.getConcreteEntities()) {
			visit(concrete);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(concrete);
			InstantiatedElement element = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			element.setParent(parent);
			// All childs of an assertion are also assertion, provided they are
			// also relation
			if (parent.isAssertion() && element.isRelation()) {
				element.setAssertion(true);
			}
		}
		instantiationPath.pop();
		return parent;
	}

	@Override
	protected InstantiatedElement visitConditionalRelationDefinition(
			ConditionalRelationDefinition object) {
		InstantiatedElement parent = unfoldModel.getInstantiationTree().lookupInstance(
				instantiationPath);
		instantiationPath.push(object);
		for (ClassicalExpression expr : object.getClassicalExpressions()) {
			visit(expr);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(expr);
			InstantiatedElement element = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			element.setParent(parent);
		}
		for (ConcreteEntity concrete : object.getConcreteEntities()) {
			visit(concrete);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(concrete);
			InstantiatedElement element = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			element.setParent(parent);
		}
		for (RelCase cas : object.getRelCases()) {
			visit(cas);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(cas.getCondition());
			InstantiatedElement condition = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			// {
			// System.out.println("**Eval: " + condition.toString() + " : "
			// + (condition.isConstant() ? "isConstant " : "notConstant ")
			// + (condition.isConstant() ? condition.getValue().toString() :
			// ""));
			// }
			parent.addConditionCase(condition);
			condition.setParent(parent);
			for (Relation rel : cas.getRelation()) {
				lookupPath = new InstantiationPath(instantiationPath);
				lookupPath.push(rel);
				InstantiatedElement element = unfoldModel.getInstantiationTree()
						.lookupInstance(lookupPath);
				element.setParent(parent);
				element.setConditionCase(true);
				element.setConditionTest(condition);
			}
		}
		for (Relation rel : object.getDefaultRelation()) {
			visit(rel);
			InstantiationPath lookupPath = new InstantiationPath(instantiationPath);
			lookupPath.push(rel);
			InstantiatedElement element = unfoldModel.getInstantiationTree()
					.lookupInstance(lookupPath);
			element.setParent(parent);
			parent.setDefaultCase(element);
			element.setConditionCase(true);
		}
		instantiationPath.pop();
		return parent;
	}

	@Override
	protected InstantiatedElement visitRelCase(RelCase object) {
		visit(object.getCondition());
		for (Relation rel : object.getRelation()) {
			visit(rel);
		}
		return dummy;
	}

}
