/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;

/**
 * The Class InstantiationPath.
 */
public class InstantiationPath extends ArrayList<NamedElement> {

	private static final long serialVersionUID = 487979826987308237L;
	
	/**
	 * Default string used as a separator betweeen components of the {@link InstantiationPath} when
	 * converting it to a String (using either {@link #toString()} or {@link #getQualifiedName()}.
	 */
	public static final String defaultSeparator = CCSLKernelUtils.defaultSeparator;
	
	/**
	 * Instantiates a new empty instantiation path.
	 */
	public InstantiationPath() {
		super();
	}

	/**
	 * Instantiates a new instantiation path.
	 *
	 * @param initialCapacity the initial capacity
	 */
	public InstantiationPath(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * Instantiates a new instantiation path filled with the elements of the collection given 
	 * as argument.
	 *
	 * @param c the collection to use to initialize the content of the path
	 */
	public InstantiationPath(Collection<? extends NamedElement> c) {
		super(c);
	}
	
	/**
	 * Push.
	 *
	 * @param elt the elt
	 */
	public void push(NamedElement elt) {
		add(elt);
	}
	
	/**
	 * Pop an element from the end of the path, ie. the last one.
	 *
	 * @return the NamedElement that has been removed
	 */
	public NamedElement pop() {
		return remove(size() - 1);
	}
	
	/**
	 * Gets the first element of the path.
	 *
	 * @return the first element
	 */
	public NamedElement getFirst() {
		return get(0);
	}
	
	/**
	 * Peek the last element of the path. 
	 *
	 * @return the last NamedElement of the path
	 * @see InstantiationPath#getLast()
	 */
	public NamedElement peekLast() {
		return get(size() - 1);
	}
	
	/**
	 * Gets the last element of the path.
	 *
	 * @return the last element.
	 * @see InstantiationPath#peekLast()
	 */
	public NamedElement getLast() {
		return peekLast();
	}
	
	/**
	 * Removes the last occurrence of the element given as argument, if it exists in the path.
	 *
	 * @param elt the elt the last occurrence of which is to be removed
	 */
	public void removeLast(NamedElement elt) {
		int index = lastIndexOf(elt);
		if (index >= 0)
			remove(index);
	}
	
	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#toString()
	 */
	@Override
	public String toString() {
		String res = "";
		for (Iterator<NamedElement> it = iterator(); it.hasNext(); ) {
			res += it.next().getName();
			if (it.hasNext()) {
				res += defaultSeparator;
			}
		}
		return res;
	}

	/**
	 * Returns a textual representation of the {@link InstantiationPath} object as a string built from
	 * the name of all elements in the path, separated by the default separator string {@value #defaultSeparator}.
	 *  
	 * @return the qualified name of the path
	 * @see #getQualifiedName(String)
	 */
	public String getQualifiedName() {
		return getQualifiedName(defaultSeparator);
	}
	
	/**
	 * Returns a textual representation of the {@link InstantiationPath} object as a string built from
	 * the name of all elements in the path, separated by the separator string.
	 * 
	 * @param separator String used as a separator between the names of two components of the path
	 * @return the qualified name of the path
	 * @see #getQualifiedName()
	 */
	public String getQualifiedName(String separator) {
		StringBuilder sb = new StringBuilder();
		for (Iterator<NamedElement> it = iterator(); it.hasNext(); ) {
			sb.append(it.next().getName());
			if (it.hasNext()) {
				sb.append(separator);
			}
		}
		return sb.toString();
	}
	
}
