/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception;

public class ExceptionWrapper extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6499871398043147258L;

	public ExceptionWrapper(Throwable cause) {
		super(cause);
	}

}
