/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.util.ClockExpressionAndRelationSwitch;

public class RecursiveDefinitionChecker extends
		ClockExpressionAndRelationSwitch<Boolean> {

	private ExpressionDeclaration declaration;
	private Deque<NamedElement> nodePath;
	private boolean recursive;
	private boolean tailRecursive;
	private boolean tailCall;
	private Expression recursiveCall;
	private List<NamedElement> recursionPath = Collections.emptyList();
	private List<BindableEntity> recursionArgs;
	
	public RecursiveDefinitionChecker(ExpressionDefinition definition) {
		this.declaration = definition.getDeclaration();
		nodePath = new ArrayDeque<NamedElement>();
		this.tailCall = true;
		recursive = doSwitch(definition);
	}

	public boolean isTailRecursive() {
		return tailRecursive;
	}

	public boolean isRecursive() {
		return recursive;
	}
	
	public Expression getRecursiveCall() {
		return recursiveCall;
	}
	
	public List<NamedElement> getRecursionPath() {
		return recursionPath;
	}
	
	public List<BindableEntity> getRecursionArguments() {
		return recursionArgs;
	}
	
	@Override
	public Boolean caseUserExpressionDefinition(UserExpressionDefinition object) {
		nodePath.push(object);
		boolean res = doSwitch( object.getRootExpression() );
		nodePath.pop();
		return res;
	}
	
	private void computeRecursionPath() {
		recursionPath = new ArrayList<NamedElement>();
		for (Iterator<NamedElement> iter = nodePath.descendingIterator(); iter.hasNext(); )
			recursionPath.add(iter.next());	
	}
	
	private void computeRecursionArguments(Expression object) {
		recursionArgs = new ArrayList<BindableEntity>();
		for (Binding binding : object.getBindings()) {
			recursionArgs.add(binding.getBindable());
		}
	}
	
	@Override
	public Boolean caseExpression(Expression object) {
		boolean res = false;
		if (nodePath.contains(object)) {
			return false;
		}
		nodePath.push(object);
		if (object.getType() == declaration) {
			tailRecursive = tailCall;
			res = true;
			recursiveCall = object;
			computeRecursionPath();
			computeRecursionArguments(object);
		}
		else {
			if (object.getType() instanceof KernelExpressionDeclaration
					&& object.getType().getName().compareTo("Concatenation") == 0) {
				for (Binding bd : object.getBindings()) {
					if (bd.getAbstract().getName().compareTo("LeftClock") == 0) {
						boolean savedTailCall = tailCall;
						tailCall = false;
						res = doSwitch(bd.getBindable());
						tailCall = savedTailCall;
					}
					else if (bd.getAbstract().getName().compareTo("RightClock") == 0) {
						res = doSwitch(bd.getBindable());
					}
				}
			}
		}
		nodePath.pop();
		return res;
	}
	
	@Override
	public Boolean caseExternalExpressionDefinition(ExternalExpressionDefinition object) {
		return Boolean.FALSE;
	}

	@Override
	public Boolean caseConditionalExpressionDefinition(
			ConditionalExpressionDefinition object) {
		boolean res = false;
		nodePath.push(object);
		for (ExprCase choice : object.getExprCases()) {
			res = res || doSwitch(choice.getExpression());
		}
		nodePath.pop();
		return res;
	}
	
	/*
	 * Fix for bug #14889 : the targets in a Binding of a Concatenation expression are visited
	 * to check for recursion. Various objects can appear as target of such a binding : we are
	 * only interested in specific cases that are checked in the caseExpression function, for
	 * all other kind of objects we can return false to avoid returning null.
	 * The default behavior of an Ecore switch (parent class of the current one) is to return null
	 * if the object given as argument to the doSwitch() function is not handled.
	 * As the result is stored in a java.lang.boolean we avoid to throw a NullPointerException if the
	 * Ecore Switch returns null.
	 */
	@Override
	public Boolean defaultCase(EObject object) {
		return Boolean.FALSE;
	}
	
}
