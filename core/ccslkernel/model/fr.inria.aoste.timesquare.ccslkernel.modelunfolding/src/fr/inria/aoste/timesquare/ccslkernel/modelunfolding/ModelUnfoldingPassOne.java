/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.util.ArrayDeque;
import java.util.Deque;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExprCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelCase;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.visitor.Visitor;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.DefinitionNotFound;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.ExceptionWrapper;

public class ModelUnfoldingPassOne extends Visitor<InstantiatedElement> {

	// First Pass : decide which elements are instantiated and creates entries
	// in the
	// instantiation tree of the unfold model.

	protected Deque<Block> blockStack;
	protected InstantiationPath instantiationPath;
	protected UnfoldModel unfoldModel;
	protected static final InstantiatedElement dummy = new InstantiatedElement(null, null);

	public ModelUnfoldingPassOne(UnfoldModel unfoldModel) {
		this.unfoldModel = unfoldModel;
		this.instantiationPath = new InstantiationPath();
		this.blockStack = new ArrayDeque<Block>();
	}

	private InstantiatedElement lookupInstance(InstantiationPath path) {
		InstantiatedElement existing = unfoldModel.getInstantiationTree().lookupInstance(
				path);
		return existing;
	}

	private InstantiatedElement initInstance(InstantiationPath path) {
		InstantiatedElement newElement = new InstantiatedElement(new InstantiationPath(
				path), null);
		unfoldModel.getInstantiationTree().storeInstance(path, newElement);
		return newElement;
	}

	private InstantiatedElement lookupOrCreateInstance(InstantiationPath path) {
		InstantiatedElement existing = lookupInstance(instantiationPath);
		if (existing == null) {
			return initInstance(instantiationPath);
		}
		return existing;
	}

	@Override
	protected InstantiatedElement visitClockConstraintSystem(ClockConstraintSystem cs) {
		if (blockStack.contains(cs)) {
			return dummy;
		}
		blockStack.push(cs);
		instantiationPath.push(cs);
		InstantiatedElement elt = visit(cs.getSuperBlock());
		instantiationPath.pop();
		blockStack.pop();
		return elt;
	}

	@Override
	protected InstantiatedElement visitBlock(Block object) {
		if (blockStack.contains(object)) {
			return dummy;
		}
		blockStack.push(object);
		instantiationPath.push(object);
		for (Element elt : object.getElements()) {
			visit(elt).setTopLevel(true);
		}
		for (Expression ex : object.getExpressions()) {
			visit(ex).setTopLevel(true);
		}
		for (Relation rel : object.getRelations()) {
			visit(rel).setTopLevel(true);
		}
		for (Block sub : object.getSubBlock()) {
			visit(sub);
		}
		for(ClassicalExpression classicalExpr : object.getClassicalExpression()){
			visit(classicalExpr).setTopLevel(true);
		}
		instantiationPath.pop();
		blockStack.pop();
		return dummy;
	}

	@Override
	protected InstantiatedElement visitClock(Clock c) {
		instantiationPath.push(c);
		InstantiatedElement elt = lookupOrCreateInstance(instantiationPath);
		elt.setLeaf(true);
		if (c.eContainer() instanceof Block) {
			elt.setTopLevel(true);
		}
		instantiationPath.pop();
		return elt;
	}

	@Override
	protected InstantiatedElement visitClassicalExpression(ClassicalExpression object) {
		instantiationPath.push(object);
		InstantiatedElement elt = lookupOrCreateInstance(instantiationPath);
		elt.setLeaf(true);
		if (object.eContainer() instanceof Block) {
			elt.setTopLevel(true);
		}
		instantiationPath.pop();
		return elt;
	}

	@Override
	protected InstantiatedElement visitSequenceElement(SequenceElement seq) {
		instantiationPath.push(seq);
		InstantiatedElement elt = lookupOrCreateInstance(instantiationPath);
		elt.setLeaf(true);
		if (seq.eContainer() instanceof Block) {
			elt.setTopLevel(true);
		}
		elt.setValue(seq);
		instantiationPath.pop();
		return elt;
	}
	
	@Override
	protected InstantiatedElement visitElement(Element el) {
		instantiationPath.push(el);
		InstantiatedElement elt = lookupOrCreateInstance(instantiationPath);
		elt.setLeaf(true);
		if (el.eContainer() instanceof Block) {
			elt.setTopLevel(true);
		}
		instantiationPath.pop();
		return elt;
	}

	@Override
	protected InstantiatedElement visitPrimitiveElement(PrimitiveElement el) {
		instantiationPath.push(el);
		InstantiatedElement elt = lookupOrCreateInstance(instantiationPath);
		elt.setLeaf(true);
		if (el.eContainer() instanceof Block) {
			elt.setTopLevel(true);
		}
		if (el instanceof IntegerElement || el instanceof RealElement
				|| el instanceof BooleanElement || el instanceof StringElement) {
			elt.setConstant(true);
			elt.setValue(el);
		}
		instantiationPath.pop();
		return elt;
	}

	@Override
	protected InstantiatedElement visitExpression(Expression ex) {
		instantiationPath.push(ex);
		InstantiatedElement newElement = lookupOrCreateInstance(instantiationPath);
		if (ex.eContainer() instanceof Block) {
			newElement.setTopLevel(true);
		}
		ExpressionDeclaration decl = ex.getType();
		if (decl instanceof KernelExpressionDeclaration) {
			newElement.setLeaf(true);
			newElement.setIsKernel(true);
		}
		else {
			newElement.setLeaf(false);
			newElement.setIsKernel(false);
			ExpressionDefinition def = unfoldModel.lookupExpressionDefinition(decl);
			if (def == null) {
				throw new ExceptionWrapper(
						new DefinitionNotFound(
								"No definition found for expression declaration"
										+ decl.getName()));
			}
			unfoldModel.recordExpressionDefinitionUse(decl, def);
			if (def instanceof ExternalExpressionDefinition) {
				newElement.setLeaf(true);
			}
			else { // Is the current expression a recursive call in a tail
					// recursive definition ?
				int defIndex = instantiationPath.lastIndexOf(def);
				RecursiveDefinitionChecker checker = new RecursiveDefinitionChecker(def);
				if (defIndex < 0 || defIndex != instantiationPath.size() - 2
						|| (!checker.isTailRecursive())) {
					visit(def);
				}
			}
		}
		unfoldModel.getInstantiationTree().storeInstance(instantiationPath, newElement);
		instantiationPath.pop();
		return newElement;
	}

	@Override
	protected InstantiatedElement visitUserExpressionDefinition(
			UserExpressionDefinition def) {
		instantiationPath.push(def);
		for (ClassicalExpression expr : def.getClassicalExpressions()) {
			visit(expr);
		}
		for (ConcreteEntity concrete : def.getConcreteEntities()) {
			visit(concrete);
		}
		instantiationPath.removeLast(def);
		return dummy;
	}

	@Override
	protected InstantiatedElement visitConditionalExpressionDefinition(
			ConditionalExpressionDefinition cd) {
		instantiationPath.push(cd);
		for (ClassicalExpression expr : cd.getClassicalExpressions()) {
			visit(expr);
		}
		for (ConcreteEntity concrete : cd.getConcreteEntities()) {
			visit(concrete);
		}
		for (ExprCase eCase : cd.getExprCases()) {
			visit(eCase);
		}
		if (cd.getDefaultExpression() != null) {
			visit(cd.getDefaultExpression());
		}
		instantiationPath.pop();
		return dummy;
	}

	@Override
	protected InstantiatedElement visitExprCase(ExprCase object) {
		visit(object.getCondition());
		visit(object.getExpression());
		return dummy;
	}

	@Override
	protected InstantiatedElement visitRelation(Relation object) {
		instantiationPath.push(object);
		InstantiatedElement newElement = lookupOrCreateInstance(instantiationPath);
		if (object.eContainer() instanceof Block) {
			newElement.setTopLevel(true);
		}
		RelationDeclaration decl = object.getType();
		if (decl instanceof KernelRelationDeclaration) {
			newElement.setLeaf(true);
			newElement.setIsKernel(true);
		}
		else {
			newElement.setLeaf(false);
			newElement.setIsKernel(false);
			RelationDefinition def = unfoldModel.lookupRelationDefinition(decl);
			if (def == null) {
				throw new ExceptionWrapper(new DefinitionNotFound(
						"No definition found for relation declaration" + decl.getName()));
			}
			unfoldModel.recordRelationDefinitionUse(decl, def);
			if (def instanceof ExternalRelationDefinition) {
				newElement.setLeaf(true);
			} else {
				visit(def);
			}
		}
		unfoldModel.getInstantiationTree().storeInstance(instantiationPath, newElement);
		instantiationPath.pop();
		return newElement;
	}

	@Override
	protected InstantiatedElement visitUserRelationDefinition(
			UserRelationDefinition object) {
		instantiationPath.push(object);
		for (ClassicalExpression expr : object.getClassicalExpressions()) {
			visit(expr);
		}
		for (ConcreteEntity concrete : object.getConcreteEntities()) {
			visit(concrete);
		}
		instantiationPath.pop();
		return dummy;
	}

	@Override
	protected InstantiatedElement visitConditionalRelationDefinition(
			ConditionalRelationDefinition object) {
		instantiationPath.push(object);
		for (ClassicalExpression expr : object.getClassicalExpressions()) {
			visit(expr);
		}
		for (ConcreteEntity concrete : object.getConcreteEntities()) {
			visit(concrete);
		}
		for (RelCase cas : object.getRelCases()) {
			visit(cas);
		}
		for (Relation rel : object.getDefaultRelation()) {
			visit(rel);
		}
		instantiationPath.pop();
		return dummy;
	}

	@Override
	protected InstantiatedElement visitRelCase(RelCase object) {
		visit(object.getCondition());
		for (Relation rel : object.getRelation()) {
			visit(rel);
		}
		return dummy;
	}

}
