/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DenseClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;

/**
 * The Class InstantiatedElement represents elements obtained by the unfolding of a CCSL model : ie. the
 * process by which all necessary definitions (for relations and expressions) are applied until the outcome
 * is made only of kernel relations and expressions.
 * @see UnfoldModelFromURI#UnfoldModel(ClockConstraintSystem) 
 */
public class InstantiatedElement {
	
	static private long elementCount = 0L;
	
	private static synchronized long nextCount() {
		return ++elementCount;
	}
	
	/**
	 * The string used as the default separator when computing the qualified name of an element. Value of this
	 * string is {@value #defaultSeparator}.
	 * 
	 * @see #getQualifiedName()
	 */
	public static final String defaultSeparator = "::";
	
	private InstantiationPath instantiationPath;
	private AbstractConcreteMapping<InstantiatedElement> abstractMapping;
	private long id;
	private boolean leaf;
	private NamedElement definition;
	private boolean isKernel;
	private boolean assertion;
	private boolean conditional;
	private boolean conditionCase;
	private boolean recursiveCall;
	private boolean tailRecursiveCall;
	private InstantiatedElement rootExpression;
	private List<InstantiatedElement> subCases;
	private InstantiatedElement defaultCase;
	private InstantiatedElement conditionTest;
	private InstantiatedElement parent;
	private List<InstantiatedElement> sons;
	private boolean topLevel;
	private long useCount;
	private boolean constant;
	private Element value;
	
	/**
	 * Instantiates a new instantiated element.
	 *
	 * @param path the instantiation path of the element
	 * @param acm the AbstractConcreteMapping to be associated with the element
	 */
	public InstantiatedElement(InstantiationPath path, AbstractConcreteMapping<InstantiatedElement> acm) {
		this.instantiationPath = path;
		this.abstractMapping = acm;
		this.id = nextCount();
		this.leaf = false;
		this.definition = null;
		this.conditional = false;
		this.conditionCase = false;
		this.recursiveCall = false;
		this.tailRecursiveCall = false;
		this.topLevel = false;
		this.useCount = 0;
		this.setRootExpression(null);
		this.defaultCase = null;
		this.conditionTest = null;
		this.parent = null;
	}
	
	/**
	 * Gets the instantiation path of the element
	 *
	 * @return the instantiation path
	 * @see InstantiationPath
	 */
	public InstantiationPath getInstantiationPath() {
		return instantiationPath;
	}
	
	/**
	 * Gets the AbstractConcreteMapping object associated with the element.
	 *
	 * @return the abstract mapping
	 */
	public AbstractConcreteMapping<InstantiatedElement> getAbstractMapping() {
		return abstractMapping;
	}
	
	/**
	 * Sets the abstract mapping.
	 *
	 * @param abstractMapping the new abstract mapping
	 */
	public void setAbstractMapping(AbstractConcreteMapping<InstantiatedElement> abstractMapping) {
		this.abstractMapping = abstractMapping;
	}

	/**
	 * Gets the qualified name of the element: ie. the concatenation of all the names of the
	 * components of the instantiation path, separated by the string given in the argument.
	 * Components of the path that have an empty name are shown with the string "<NoName>".
	 * 
	 * @param separator the separator
	 * @return the qualified name
	 * @see InstantiatedElement#getQualifiedName()
	 */
	public String getQualifiedName(String separator) {
		String res = "";
		for (Iterator<NamedElement> it = instantiationPath.iterator(); it.hasNext(); ) {
			String name = it.next().getName();
			if (name.isEmpty()) {
				res += "<NoName>";
			}
			else {
				res += name;
			}
			if (it.hasNext()) {
				res += separator;
			}
		}
		return res;
	}
	
	/**
	 * Returns the qualified name of the element using the default separator {@link #defaultSeparator}.
	 * 
	 * @return
	 * @see #getQualifiedName(String)
	 */
	public String getQualifiedName() {
		return getQualifiedName(defaultSeparator);
	}
	
	/**
	 * Gets the declaration of the element. Returns null if the element is not an expression or a relation.
	 *
	 * @return the declaration
	 */
	public NamedElement getDeclaration() {
		NamedElement last = instantiationPath.getLast();
		if (last == null) {
			return null;
		}
		if (last instanceof Relation) {
			return ((Relation) last).getType();
		}
		if (last instanceof Expression) {
			return ((Expression) last).getType();
		}
		return null;
	}
	
	public NamedElement getDefinition() {
		return definition;
	}
	
	public void setDefinition(NamedElement def) {
		definition = def;
	}
	
	/**
	 * Checks if the element is an expression : ie. an instance of an expression, either directly
	 * from the specification model, or from inside a definition.
	 * In both cases, the last element of the instantiation path is an instance of the Expression class in
	 * the TimeModel metamodel. 
	 *
	 * @return true, if it is an expression.
	 */
	public boolean isExpression() {
		NamedElement last = instantiationPath.getLast();
		return (last != null && (last instanceof Expression));
	}
	
	/**
	 * Gets the root expression.
	 *
	 * @return the root expression
	 */
	public InstantiatedElement getRootExpression() {
		return rootExpression;
	}

	public void setRootExpression(InstantiatedElement rootExpression) {
		this.rootExpression = rootExpression;
	}

	/**
	 * Checks if the element is an instance of a kernel expression. It returns true if the element instantiates
	 * an Expression (ie. isExpression() returns true) and if the element is a leaf in the instantiation tree
	 * (ie. isLeaf() returns true).
	 *
	 * @return true, if the element is a kernel expression.
	 */
	public boolean isKernelExpression() {
		return isExpression() && isKernel;
	}
	
	/**
	 * Checks if the element is a relation : ie. an instance of a relation, either directly
	 * from the specification model, or from inside a definition.
	 * In both cases, the last element of the instantiation path is an instance of the Relation class in
	 * the CCSL TimeModel metamodel. 
	 *
	 * @return true, if it is a relation
	 */
	public boolean isRelation() {
		NamedElement last = instantiationPath.getLast();
		return (last != null && (last instanceof Relation));
	}
	
	public void setAssertion(boolean assertion) {
		this.assertion = assertion;
	}
	
	public void setIsKernel(boolean k) {
		isKernel = k;
	}

	/**
	 * Checks if the element is an assertion: ie. both a relation and that this relation is marked as an
	 * assertion in the model.
	 *
	 * @return true, if it is an assertion
	 */
	public boolean isAssertion() {
		return assertion;
	}
	
	/**
	 * Checks if is kernel relation. It returns true if the element instantiates
	 * a Relation (ie. isRelation() returns true) and if the element is a leaf in the instantiation tree
	 * (ie. isLeaf() returns true).
	 *
	 * @return true, if the element is a kernel relation
	 * @see InstantiatedElement#isRelation()
	 * @see #isLeaf()
	 */
	public boolean isKernelRelation() {
		return isRelation() && isKernel;
	}
	
	/**
	 * Checks if the instantiated element comes from an Element, ie. a ConcreteEntity which is not a Relation
	 * nor an Expression.
	 *  
	 * @return true, if the element is an Element
	 */
	public boolean isElement() {
		NamedElement last = instantiationPath.getLast();
		return (last != null && (last instanceof Element));
	}
	
	/**
	 * Checks if is clock.
	 *
	 * @return true, if is clock
	 */
	public boolean isClock() {
		NamedElement last = instantiationPath.getLast();
		return (last != null && (last instanceof Clock));
	}
	
	/**
	 * Checks if a clock is dense
	 * 
	 * @return true if the clock is dense, ie. its type name is "Dense"
	 */
	public boolean isDenseClock() {
		NamedElement last = instantiationPath.getLast();
		if (last == null || ( ! (last instanceof Clock))) {
			return false;
		}
		Type type = ((Clock)last).getType();
		return (type != null && type instanceof DenseClockType);
	}
	/**
	 * Checks if is classical expression.
	 *
	 * @return true, if is classical expression
	 */
	public boolean isClassicalExpression() {
		NamedElement last = instantiationPath.getLast();
		return (last != null && (last instanceof ClassicalExpression));
	}
	
	
	public boolean isConstant() {
		return constant;
	}
	
	public void setConstant(boolean constant) {
		this.constant = constant;
	}
	
	public Element getValue() {
		return value;
	}
	
	public void setValue(Element value) {
		this.value = value;
	}
	
	/**
	 * Checks if is primitive element.
	 *
	 * @return true, if is primitive element
	 */
	public boolean isPrimitiveElement() {
		NamedElement last = instantiationPath.getLast();
		return (last != null && (last instanceof PrimitiveElement));
	}
	
	/**
	 * Resolve an abstract entity in the context of the instantiated element. Each instantiated element is
	 * possibly associated with an instance of {@link AbstractConcreteMapping} that provides the values of
	 * all the AbstractEntity encountered in all the bindings along the instantiation path of the element.
	 *
	 * @param abs the abs
	 * @return the instantiated element
	 */
	public InstantiatedElement resolveAbstractEntity(AbstractEntity abs) {
		return abstractMapping.resolveAbstractEntity(abs);
	}
	
	/**
	 * Resolve an AbstractEntity by its name.
	 *
	 * @param abstractName the abstract name
	 * @return the instantiated element
	 * @see #resolveAbstractEntity(AbstractEntity)
	 */
	public InstantiatedElement resolveAbstractEntity(String abstractName) {
		return abstractMapping.resolveAbstractEntity(abstractName);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String res = "";
		res += id;
		res += "=" + getQualifiedName("::");
		if (getDeclaration() != null) {
			res += "[" + getDeclaration().getName() + "]";
		}
		if (isConditionCase()) {
			if (getConditionTest() != null) {
				res += "|if(" + getConditionTest().getId() + ")";
			}
			else {
				res += "|else";
			}
		}
		if (isLeaf()) {
			res += "^";
		}
		return res;
	}

	/**
	 * Describe.
	 *
	 * @param ps the ps
	 */
	public void describe(PrintStream ps) {
		ps.print(toString());
		NamedElement decl = getDeclaration();
		if (decl != null) {
			ps.print('(');
			if (decl instanceof KernelRelationDeclaration) {
				AbstractEntity left = ((KernelRelationDeclaration)decl).getLeftEntity();
				ps.print(left.getName()); ps.print("->");
				ps.print(getAbstractMapping().resolveAbstractEntity(left));
				ps.print(", ");
				AbstractEntity right = ((KernelRelationDeclaration)decl).getRightEntity();
				ps.print(right.getName()); ps.print("->");
				ps.print(getAbstractMapping().resolveAbstractEntity(right));
			}
			else if (decl instanceof RelationDeclaration) {
				for (AbstractEntity param : ((RelationDeclaration)decl).getParameters()) {
					ps.print(param.getName()); ps.print("->");
					ps.print(getAbstractMapping().resolveAbstractEntity(param));
					ps.print(", ");
				}
			}
			else if (decl instanceof KernelExpressionDeclaration) {
				
			}
			else if (decl instanceof ExpressionDeclaration) {
				for (AbstractEntity param : ((ExpressionDeclaration)decl).getParameters()) {
					ps.print(param.getName()); ps.print("->");
					ps.print(getAbstractMapping().resolveAbstractEntity(param));
					ps.print(", ");
				}
			}
			ps.print(')');
		}
		ps.println();
	}
	
	/**
	 * Checks if the element is leaf in the instantiation tree. For expression and relation a leaf
	 * corresponds to an element which instantiates a kernel expression or a kernel relation. Elements
	 * that instantiates clocks, classical expressions and primitive elements are also leaf in the
	 * instantiation tree.
	 *
	 * @return true, if is leaf
	 */
	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	/**
	 * Checks if the element is expanded using a conditional definition. 
	 *
	 * @return true, if is conditional
	 * @see #getConditionalCases()
	 */
	public boolean isConditional() {
		return conditional;
	}

	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}
	
	/**
	 * Gets the list of elements that are the subcases instantiated using a conditional definition
	 * 
	 * @return
	 * @see #isConditional()
	 * @see #getDefaultCase()
	 */
	public List<InstantiatedElement> getConditionalCases() {
		if (subCases == null) {
			subCases = new ArrayList<InstantiatedElement>();
		}
		return subCases;
	}
	
	public void addConditionCase(InstantiatedElement element) {
		if (subCases == null) {
			subCases = new ArrayList<InstantiatedElement>();
		}
		subCases.add(element);
	}
	
	/**
	 * Gets the default case of an element that has been unfold using a conditional definition. This function
	 * gives a non-null result only is the method {@link #isConditional()} returns true;
	 *  
	 * @return the default case if it exists
	 * @see #isConditional()
	 */
	public InstantiatedElement getDefaultCase() {
		return defaultCase;
	}
	
	/**
	 * Checks if the element is a recursive call produced during the expansion of a recursive
	 * expression definition. A call is recursive if it corresponds to an expression that is both
	 * not a leaf, and the definition to be used is the same that has just been used to produce
	 * the current instantiated element.
	 *
	 * @return true, if the element is a recursive call inside the expansion of a definition
	 */
	public boolean isRecursiveCall() {
		return recursiveCall;
	}

	public void setRecursiveCall(boolean recursive) {
		this.recursiveCall = recursive;
	}

	/**
	 * Checks if the element is a tail recursive call.
	 *
	 * @return true, if is tail recursive
	 * @see #isRecursiveCall()
	 */
	public boolean isTailRecursiveCall() {
		return tailRecursiveCall;
	}

	public void setTailRecursiveCall(boolean tailRecursive) {
		this.tailRecursiveCall = tailRecursive;
	}

	/**
	 * Gets the id of the element. The id is a unique number generated when a new element is being
	 * created. It can be used to disambiguate two instances of InstantiatedElement.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Checks if the element is a toplevel one: ie. directly contained in a Block of the CCSL specification model.
	 *
	 * @return true, if it is a toplevel element.
	 */
	public boolean isTopLevel() {
		return topLevel;
	}

	public void setTopLevel(boolean topLevel) {
		this.topLevel = topLevel;
	}

	/**
	 * Returns the list of block (in containment order) that contains the element. This list contains
	 * all the blocks that appears at the beginning of the instantiation path, taken in the same order.
	 * 
	 * @return
	 */

	public InstantiationPath getBlockStack() {
		InstantiationPath res = new InstantiationPath();
		for (NamedElement elt : getInstantiationPath()) {
			if (elt instanceof Block) {
				res.add(elt);
			}
			else {
				break;
			}
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return ((obj instanceof InstantiatedElement)
				&& instantiationPath.equals(((InstantiatedElement)obj).getInstantiationPath()));
	}

	/**
	 * Gets the use count of the element: ie. the number of times it appears as a target in an
	 * AbstractConcreteMapping (resolved chain of Binding along the instantiation path).
	 *
	 * @return the use count
	 */
	public long getUseCount() {
		return useCount;
	}

	public synchronized void incUseCount() {
		this.useCount++;
	}
	
	public synchronized void decUseCount() {
		this.useCount--;
	}

	/**
	 * Checks if the element is an instance of a (sub)case of a conditional definition. If yes the
	 * method {@link #getConditionTest()} can be used to get the actual instance of the test that guards the
	 * case. Except for the default case, the test is not null.
	 * 
	 * @return true if the element is a sub-case of a conditional definition
	 * @see #getParent()
	 */
	public boolean isConditionCase() {
		return conditionCase;
	}

	public void setConditionCase(boolean conditionalCase) {
		this.conditionCase = conditionalCase;
	}

	/**
	 * 
	 * @return true if the element has an ancestor which is a subcase of a conditional.
	 * @see #isConditionCase()
	 * @see #getBranchConditionTest()
	 */
	public boolean inConditionalBranch() {
		if (isConditionCase()) {
			return true;
		}
		else if (parent == null) {
			return isConditionCase();
		}
		else {
			return parent.inConditionalBranch();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public InstantiatedElement getConditionTest() {
		return conditionTest;
	}

	public void setConditionTest(InstantiatedElement conditionTest) {
		this.conditionTest = conditionTest;
	}

	public void setDefaultCase(InstantiatedElement defaultCase) {
		this.defaultCase = defaultCase;
	}
	
	/**
	 * Cumulates all condition tests along the branch
	 * @return
	 * @see #inConditionalBranch()
	 */
	public List<InstantiatedElement> getBranchConditionTests() {
		List<InstantiatedElement> res;
		if (parent == null) {
			res = new ArrayList<InstantiatedElement>();
		}
		else {
			res = parent.getBranchConditionTests();
		}
		if (isConditionCase() && conditionTest != null) {
			res.add(conditionTest);
		}
		return res;
	}

	/**
	 * Returns the parent of the element defined as follows :
	 * - if it is a condition case (ie. if {@link #isConditionCase()} returns true, then the returned element
	 * is the one that contains the case as one of its alternatives, it can be either an expression or
	 * a relation : in any case, the method {@link #isConditional()} called on the parent returns true.
	 * - if it is a "normal" expression or relation obtained by expanding a definition, then the returned
	 * element is the expression or relation that has been expanded.
	 * 
	 * @return
	 */
	public InstantiatedElement getParent() {
		return parent;
	}

	/**
	 * Set the parent of the element. The element (this) is also added to the list of sons of the parent, so
	 * that the {@link #getSons()} method returns correctly all the sons.
	 * 
	 * @param parent
	 */
	public void setParent(InstantiatedElement parent) {
		this.parent = parent;
		parent.addSon(this);
	}

	private void addSon(InstantiatedElement son) {
		if (sons == null) {
			sons = new ArrayList<InstantiatedElement>();
		}
		sons.add(son);
	}
	
	/**
	 * Gets the list of elements that have the current element as their parent.
	 * 
	 * @return the list of elements that have the current element as their parent
	 * @see #setParent(InstantiatedElement)
	 */
	public List<InstantiatedElement> getSons() {
		return sons;
	}
	
}
