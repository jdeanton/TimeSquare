/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;

public class ModelClockUnfoldingPassOne extends ModelUnfoldingPassOne {

	public ModelClockUnfoldingPassOne(UnfoldModel unfoldModel) {
		super(unfoldModel);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected InstantiatedElement visitBlock(Block object) {
		if (blockStack.contains(object)) {
			return dummy;
		}
		blockStack.push(object);
		instantiationPath.push(object);
		for (Element elt : object.getElements()) {
			visit(elt).setTopLevel(true);
		}
//		for (Expression ex : object.getExpressions()) {
//			visit(ex).setTopLevel(true);
//		}
//		for (Relation rel : object.getRelations()) {
//			visit(rel).setTopLevel(true);
//		}
		for (Block sub : object.getSubBlock()) {
			visit(sub);
		}
		for(ClassicalExpression classicalExpr : object.getClassicalExpression()){
			visit(classicalExpr).setTopLevel(true);
		}
		instantiationPath.pop();
		blockStack.pop();
		return dummy;
	}

}
