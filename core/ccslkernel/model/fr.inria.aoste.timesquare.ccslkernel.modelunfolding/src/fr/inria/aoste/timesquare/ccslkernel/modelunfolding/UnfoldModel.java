/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.modelunfolding;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionLibrary;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.ExceptionWrapper;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.ImportedResourceNotFound;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.NoModelImportTreeRoot;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.QualifiedNameCollision;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;

public class UnfoldModel {

	private InstantiationTree<InstantiatedElement> instantiationTree;

	private ClockConstraintSystem model;
	private boolean unfoldDone;
	private List<UnfoldModel> importedModels;

	public List<UnfoldModel> getImportedModels() {
		return importedModels;
	}

	private String importedAsAlias;
	
	public String getImportedAsAlias() {
		return importedAsAlias;
	}
	
	public void setImportedAsAlias(String importedAsAlias) {
		this.importedAsAlias = importedAsAlias;
	}
	
	private List<Library> librairies;
	private List<RelationDefinition> allRelationDefinitions;
	private List<ExpressionDefinition> allExpressionDefinitions;

	private Map<RelationDeclaration, RelationDefinition> usedRelationDefinitions;
	private Map<ExpressionDeclaration, ExpressionDefinition> usedExpressionDefinitions;

	private EqualitySolver<InstantiatedElement> equalityRegistry;
	
	private void init() {
		unfoldDone = false;
		instantiationTree = new InstantiationTree<InstantiatedElement>();
		importedModels = new ArrayList<UnfoldModel>();
		librairies = new ArrayList<Library>();
		allRelationDefinitions = new ArrayList<RelationDefinition>();
		allExpressionDefinitions = new ArrayList<ExpressionDefinition>();
		usedExpressionDefinitions = new HashMap<ExpressionDeclaration, ExpressionDefinition>();
		usedRelationDefinitions = new HashMap<RelationDeclaration, RelationDefinition>();
	}

	/**
	 * Unfolds a CCSL model builds and return a new UnfoldModel object that
	 * contains the output of the unfolding process applied to the model given
	 * as argument. The output is contained int the instantiation tree that is
	 * returned by the method {@link #getInstantiationTree()}.
	 * 
	 * Constructor needs to be private since it will have to resolve all links,
	 * loadLibraries, collect definitions depending on the source (URI,
	 * ClockConstraintSystem, Resource) which is a sensitive process that can
	 * only be called one time after creation of the object.
	 * 
	 * @param model
	 *            the model to unfold
	 * @throws UnfoldingException
	 * @see #getInstantiationTree()
	 * @see #unfoldModel(ClockConstraintSystem)
	 * @see #unfoldModel(Resource)
	 * @see #unfoldModel(URI)
	 */
	protected UnfoldModel(ClockConstraintSystem model) throws IOException, UnfoldingException {
		assert (model != null) : "Null model to unfold";
		init();
		this.model = model;
	}

	public static UnfoldModel unfoldModel(URI uri) throws IOException, UnfoldingException {
		try {
			ResourceSet resourceSet = new ResourceSetImpl();
			Resource modelResource = resourceSet.createResource(uri);
			modelResource.load(null);
			return UnfoldModel.unfoldModels(resourceSet);
		} catch (IOException ioe) {
			System.out.println("UnfoldModel-Failed to read " + uri);
			throw (ioe);
		}
	}

	public static UnfoldModel unfoldModel(Resource resource) throws IOException, UnfoldingException {
		ResourceSet resourceSet = resource.getResourceSet();
		if (resourceSet == null) {
			resourceSet = new ResourceSetImpl();
			resourceSet.getResources().add(resource);
		}
		EcoreUtil.resolveAll(resourceSet);
		return unfoldModels(resourceSet);
	}

	public static UnfoldModel unfoldModels(ResourceSet resourceSet) throws IOException, UnfoldingException {
		EcoreUtil.resolveAll(resourceSet);
		return unfoldAllModels(resourceSet);
	}


	
	private static HashMap<ClockConstraintSystem, UnfoldModel> modelCache = new HashMap<ClockConstraintSystem, UnfoldModel>();

	
	
	private static UnfoldModel unfoldAllModels(ResourceSet resourceSet) throws IOException, UnfoldingException {
		modelCache.clear();
		// build a dependency tree between all ClockConstraintSystem models in
		// the resource set
		for (Resource resource : resourceSet.getResources()) {
			if (resource.getContents().size() > 0 && resource.getContents().get(0) instanceof ClockConstraintSystem) {
				createModelImportTree(resource, resourceSet);
			}
		}
		// Find the root of the tree : the one which is not imported by any
		// other model
		List<UnfoldModel> candidates = new ArrayList<UnfoldModel>(modelCache.values());
		for (UnfoldModel unfoldModel : modelCache.values()) {
			candidates.removeAll(unfoldModel.importedModels);
		}
		// At the end candidates should contain at least one element !
		if (candidates.isEmpty()) {
			// Houston, we've had a problem here.
			throw new NoModelImportTreeRoot("There is no model that is the root of the import tree");
		}
		if (candidates.size() > 1) {
			// still a problem ? Yes, maybe... But what can we do ?
		}
		UnfoldModel importTreeRoot = null;
		importTreeRoot = candidates.get(0);// Get first one, ignore
														
		// then unfold from the leaves of the tree, merging the instantiation
		// tree along the process.
		unfoldModelTree(importTreeRoot, false);
		return importTreeRoot;
	}

	private static UnfoldModel createModelImportTree(Resource resource, ResourceSet resourceSet) throws IOException,
			UnfoldingException {
		ClockConstraintSystem model = (ClockConstraintSystem) resource.getContents().get(0);
		if (modelCache.containsKey(model)) {
			return modelCache.get(model);
		}
		UnfoldModel unfoldModel = new UnfoldModel(model);
		modelCache.put(model, unfoldModel);
		
		for (ImportStatement importStatement : model.getImports()) {
			URI uri = URI.createURI(importStatement.getImportURI());
			String importAlias = importStatement.getAlias();
			Resource importedResource = resourceSet.getResource(uri.resolve(resource.getURI(), false), false);
			if (importedResource == null) {
				// Houston... the resource set is supposed to be totally
				// resolved, thus the imported resource should be there !
				//-> excepted if: it is a timemodel and no definition from the import is used... in this case emf does not pu it in the resource but it is not an error
				// --> I consequently remove the exception
				//throw new ImportedResourceNotFound("Imported model " + uri + " not found in the resource set");
			} else {
				if (importedResource.getContents().get(0) instanceof ClockConstraintSystem) {
					// create a new UnfoldModel for it.
					UnfoldModel importedUnfoldModel = createModelImportTree(importedResource, resourceSet);
					unfoldModel.importedModels.add(importedUnfoldModel);
					importedUnfoldModel.setImportedAsAlias(importAlias);
				} else if (importedResource.getContents().get(0) instanceof Library) {
					unfoldModel.librairies.add((Library) importedResource.getContents().get(0));
				}
			}
		}
		if (resource.getContents().size() > 1) {
			for (int i = 1; i < resource.getContents().size(); i++) {
				if (resource.getContents().get(i) instanceof Library) {
					unfoldModel.librairies.add((Library) resource.getContents().get(i));
				}
			}
		}
		return unfoldModel;
	}

	private static UnfoldModel unfoldModelTree(UnfoldModel root, boolean doPartialLoad) throws UnfoldingException {
		for (UnfoldModel imported : root.importedModels) {
			UnfoldModel res = unfoldModelTree(imported, isCoordinationLoad);
			root.instantiationTree.mergeWith(res.instantiationTree);
			// Validate the instantiation tree so that it doesnot contain name
			// clashes.
			if (!validateInstantiationTree(root.instantiationTree)) {
				throw new QualifiedNameCollision("");
			}
		}
		if (!root.unfoldDone) {
			root.collectDefinitions();
			if (doPartialLoad){
				root.doNoConstraintUnfold();
			}else{
				root.doFullUnfold();
			}
		}
		return root;
	}

	private static boolean validateInstantiationTree(InstantiationTree<InstantiatedElement> tree) {
		// A name collision exists if two elements have the same
		// "qualified name", ie, at each level search
		// for non unique names, then look under each of the relevant subtrees
		// if there are also non
		// unique names. As soon as one level is free of any name collision, the
		// tree is free of name
		// collision.
		Vector<NamedElement> keys = new Vector<NamedElement>(tree.keySet());
		if (keys.isEmpty()) {
			return true;
		}
		ArrayList<NamedElement> nonUniqueNameElements = new ArrayList<NamedElement>();
		for (int i = 0; i < keys.size(); i++) {
			NamedElement current = keys.elementAt(i);
			String name = current.getName();
			boolean foundWithSameName = false;
			for (int j = i + 1; j < keys.size(); j++) {
				if (keys.elementAt(j).getName().compareTo(name) == 0) {
					nonUniqueNameElements.add(keys.elementAt(j));
					foundWithSameName = true;
				}
			}
			if (foundWithSameName) {
				nonUniqueNameElements.add(current);
			}
		}
		if (nonUniqueNameElements.isEmpty()) {
			return true;
		}
		for (NamedElement key : nonUniqueNameElements) {
			InstantiationTree<InstantiatedElement> subTree = tree.get(key);
			if (!validateInstantiationTree(subTree))
				return false;
		}
		return true;
	}

	/**
	 * Gets the model that has been unfolded.
	 * 
	 * @return the model
	 */
	public ClockConstraintSystem getModel() {
		return model;
	}

	/**
	 * Gets the instantiation tree.
	 * 
	 * @return the instantiation tree
	 * @see InstantiationTree
	 */
	public InstantiationTree<InstantiatedElement> getInstantiationTree() {
		return instantiationTree;
	}

	private void collectDefinitions() {
		for (Library lib : librairies) {
			for (ExpressionLibrary eLib : lib.getExpressionLibraries()) {
				allExpressionDefinitions.addAll(eLib.getExpressionDefinitions());
			}
			for (RelationLibrary rLib : lib.getRelationLibraries()) {
				allRelationDefinitions.addAll(rLib.getRelationDefinitions());
			}
		}
	}

	public RelationDefinition lookupRelationDefinition(RelationDeclaration decl) {
		for (RelationDefinition def : allRelationDefinitions) {
			if (def.getDeclaration().equals(decl)) {
				return def;
			}
		}
		return null;
	}

	public ExpressionDefinition lookupExpressionDefinition(ExpressionDeclaration decl) {
		for (ExpressionDefinition def : allExpressionDefinitions) {
			if (def.getDeclaration().equals(decl)) {
				return def;
			}
		}
		return null;
	}

	public void recordRelationDefinitionUse(RelationDeclaration declaration, RelationDefinition definition) {
		usedRelationDefinitions.put(declaration, definition);
	}

	public void recordExpressionDefinitionUse(ExpressionDeclaration declaration, ExpressionDefinition definition) {
		usedExpressionDefinitions.put(declaration, definition);
	}

	public ExpressionDefinition getUsedExpressionDefinition(ExpressionDeclaration declaration) {
		return usedExpressionDefinitions.get(declaration);
	}

	public RelationDefinition getUsedRelationDefinition(RelationDeclaration declaration) {
		return usedRelationDefinitions.get(declaration);
	}
	
	public EqualitySolver<InstantiatedElement> getEqualityRegistry() {
		return equalityRegistry;
	}

	protected void doNoConstraintUnfold() throws UnfoldingException {
		ModelClockUnfoldingPassOne pass1 = new ModelClockUnfoldingPassOne(this);
		try {
			pass1.visit(getModel());
		} catch (ExceptionWrapper e) {
			throw (UnfoldingException) (e.getCause());
		}
		ModelClockUnfoldingPassTwo pass2 = new ModelClockUnfoldingPassTwo(this);
		try {
			pass2.visit(getModel());
		} catch (ExceptionWrapper e) {
			throw (UnfoldingException) (e.getCause());
		}
		equalityRegistry = pass2.getEqualitySolver();
		unfoldDone = true;
	}
	
	public static boolean isCoordinationLoad = false;
	
	private void doUnfold() throws UnfoldingException {
		if(isCoordinationLoad){
			doNoConstraintUnfold();
		}else{
			doFullUnfold();
		}
	}
	
	private void doFullUnfold() throws UnfoldingException {
		ModelUnfoldingPassOne pass1 = new ModelUnfoldingPassOne(this);
		try {
			pass1.visit(getModel());
		} catch (ExceptionWrapper e) {
			throw (UnfoldingException) (e.getCause());
		}
		ModelUnfoldingPassTwo pass2 = new ModelUnfoldingPassTwo(this);
		try {
			pass2.visit(getModel());
		} catch (ExceptionWrapper e) {
			throw (UnfoldingException) (e.getCause());
		}
		equalityRegistry = pass2.getEqualitySolver();
		unfoldDone = true;
	}

	// @SuppressWarnings("unused")
	// private void extractLibrairiesFromResourceSet(ResourceSet resourceSet) {
	// for (Resource res : resourceSet.getResources()) {
	// for (EObject object : res.getContents()) {
	// if (object instanceof Library) {
	// librairies.add((Library) object);
	// }
	// }
	// }
	// }

}
