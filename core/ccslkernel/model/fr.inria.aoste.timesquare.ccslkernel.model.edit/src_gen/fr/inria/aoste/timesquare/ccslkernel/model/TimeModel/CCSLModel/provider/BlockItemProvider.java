/**
 */
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.provider;


import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.provider.NamedElementItemProvider;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.provider.TimeModelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BlockItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addInitialBlockPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Initial Block feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitialBlockPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Block_initialBlock_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Block_initialBlock_feature", "_UI_Block_type"),
				 CCSLModelPackage.Literals.BLOCK__INITIAL_BLOCK,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CCSLModelPackage.Literals.BLOCK__SUB_BLOCK);
			childrenFeatures.add(CCSLModelPackage.Literals.BLOCK__ELEMENTS);
			childrenFeatures.add(CCSLModelPackage.Literals.BLOCK__RELATIONS);
			childrenFeatures.add(CCSLModelPackage.Literals.BLOCK__EXPRESSIONS);
			childrenFeatures.add(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION);
			childrenFeatures.add(CCSLModelPackage.Literals.BLOCK__BLOCK_TRANSITIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Block.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Block"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Block)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Block_type") :
			getString("_UI_Block_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Block.class)) {
			case CCSLModelPackage.BLOCK__SUB_BLOCK:
			case CCSLModelPackage.BLOCK__ELEMENTS:
			case CCSLModelPackage.BLOCK__RELATIONS:
			case CCSLModelPackage.BLOCK__EXPRESSIONS:
			case CCSLModelPackage.BLOCK__CLASSICAL_EXPRESSION:
			case CCSLModelPackage.BLOCK__BLOCK_TRANSITIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__SUB_BLOCK,
				 CCSLModelFactory.eINSTANCE.createBlock()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__SUB_BLOCK,
				 CCSLModelFactory.eINSTANCE.createClockConstraintSystem()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 TimeModelFactory.eINSTANCE.createClock()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntDivide()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetTail()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetHead()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqDecr()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqSched()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createStringElement()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createBooleanElement()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createIntegerElement()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createRealElement()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createCharElement()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createRecordElement()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createSequenceElement()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__RELATIONS,
				 ClockExpressionAndRelationFactory.eINSTANCE.createRelation()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__EXPRESSIONS,
				 ClockExpressionAndRelationFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createRealRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createRealMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntDivide()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetTail()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetHead()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createSeqDecr()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createSeqSched()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createRealVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.BLOCK__BLOCK_TRANSITIONS,
				 CCSLModelFactory.eINSTANCE.createBlockTransition()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CCSLModelPackage.Literals.BLOCK__ELEMENTS ||
			childFeature == CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return TimeModelEditPlugin.INSTANCE;
	}

}
