/**
 */
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.provider;


import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.CCSLModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ClockConstraintSystemItemProvider extends BlockItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClockConstraintSystemItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSuperBlockPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Super Block feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuperBlockPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ClockConstraintSystem_superBlock_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ClockConstraintSystem_superBlock_feature", "_UI_ClockConstraintSystem_type"),
				 CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__SUPER_BLOCK,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES);
			childrenFeatures.add(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__IMPORTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ClockConstraintSystem.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ClockConstraintSystem"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ClockConstraintSystem)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ClockConstraintSystem_type") :
			getString("_UI_ClockConstraintSystem_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ClockConstraintSystem.class)) {
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES:
			case CCSLModelPackage.CLOCK_CONSTRAINT_SYSTEM__IMPORTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createBoolean()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createInteger()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createReal()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createChar()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createRecord()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createSequenceType()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createDiscreteClockType()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createDenseClockType()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__DATA_TYPES,
				 BasicTypeFactory.eINSTANCE.createEnumerationType()));

		newChildDescriptors.add
			(createChildParameter
				(CCSLModelPackage.Literals.CLOCK_CONSTRAINT_SYSTEM__IMPORTS,
				 TimeModelFactory.eINSTANCE.createImportStatement()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CCSLModelPackage.Literals.BLOCK__ELEMENTS ||
			childFeature == CCSLModelPackage.Literals.BLOCK__CLASSICAL_EXPRESSION;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
