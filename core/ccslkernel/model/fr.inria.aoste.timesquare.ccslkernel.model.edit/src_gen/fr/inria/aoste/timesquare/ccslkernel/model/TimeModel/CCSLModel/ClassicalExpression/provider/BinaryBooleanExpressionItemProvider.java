/**
 */
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.provider;


import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BinaryBooleanExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BinaryBooleanExpressionItemProvider extends BooleanExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryBooleanExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE);
			childrenFeatures.add(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((BinaryBooleanExpression)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_BinaryBooleanExpression_type") :
			getString("_UI_BinaryBooleanExpression_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BinaryBooleanExpression.class)) {
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE:
			case ClassicalExpressionPackage.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__LEFT_VALUE ||
			childFeature == ClassicalExpressionPackage.Literals.BINARY_BOOLEAN_EXPRESSION__RIGHT_VALUE;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
