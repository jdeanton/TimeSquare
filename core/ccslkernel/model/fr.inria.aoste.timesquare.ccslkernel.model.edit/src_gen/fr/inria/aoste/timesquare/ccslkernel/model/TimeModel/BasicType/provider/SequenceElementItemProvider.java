/**
 */
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.provider;


import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SequenceElementItemProvider extends ElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequenceElementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART);
			childrenFeatures.add(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SequenceElement.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SequenceElement"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SequenceElement)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_SequenceElement_type") :
			getString("_UI_SequenceElement_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SequenceElement.class)) {
			case BasicTypePackage.SEQUENCE_ELEMENT__FINITE_PART:
			case BasicTypePackage.SEQUENCE_ELEMENT__NON_FINITE_PART:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createStringElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createBooleanElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createIntegerElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createRealElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createCharElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntDivide()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetTail()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetHead()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqDecr()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqSched()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createStringElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createBooleanElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createIntegerElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createRealElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 BasicTypeFactory.eINSTANCE.createCharElement()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntDivide()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetTail()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetHead()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqDecr()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createSeqSched()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createRealVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqRef()));

		newChildDescriptors.add
			(createChildParameter
				(BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqVariableRef()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == BasicTypePackage.Literals.SEQUENCE_ELEMENT__FINITE_PART ||
			childFeature == BasicTypePackage.Literals.SEQUENCE_ELEMENT__NON_FINITE_PART;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
