/**
 */
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.provider;


import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.provider.NamedElementItemProvider;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.provider.TimeModelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationLibrary} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RelationLibraryItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationLibraryItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DEFINITIONS);
			childrenFeatures.add(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS);
			childrenFeatures.add(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DECLARATIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns RelationLibrary.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RelationLibrary"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((RelationLibrary)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_RelationLibrary_type") :
			getString("_UI_RelationLibrary_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RelationLibrary.class)) {
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DEFINITIONS:
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__ELEMENTS:
			case ClockExpressionAndRelationPackage.RELATION_LIBRARY__RELATION_DECLARATIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DEFINITIONS,
				 ClockExpressionAndRelationFactory.eINSTANCE.createUserRelationDefinition()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DEFINITIONS,
				 ClockExpressionAndRelationFactory.eINSTANCE.createConditionalRelationDefinition()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DEFINITIONS,
				 ClockExpressionAndRelationFactory.eINSTANCE.createExternalRelationDefinition()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 TimeModelFactory.eINSTANCE.createClock()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntDivide()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetTail()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetHead()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqDecr()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqSched()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createRealVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createStringElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createBooleanElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createIntegerElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createRealElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createCharElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createRecordElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__ELEMENTS,
				 BasicTypeFactory.eINSTANCE.createSequenceElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DECLARATIONS,
				 ClockExpressionAndRelationFactory.eINSTANCE.createRelationDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DECLARATIONS,
				 KernelRelationFactory.eINSTANCE.createSubClock()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DECLARATIONS,
				 KernelRelationFactory.eINSTANCE.createCoincidence()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DECLARATIONS,
				 KernelRelationFactory.eINSTANCE.createExclusion()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DECLARATIONS,
				 KernelRelationFactory.eINSTANCE.createPrecedence()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.RELATION_LIBRARY__RELATION_DECLARATIONS,
				 KernelRelationFactory.eINSTANCE.createNonStrictPrecedence()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return TimeModelEditPlugin.INSTANCE;
	}

}
