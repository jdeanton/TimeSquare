/**
 */
package fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.provider;


import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpressionFactory;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.UserExpressionDefinition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UserExpressionDefinitionItemProvider extends ExpressionDefinitionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserExpressionDefinitionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRootExpressionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Root Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRootExpressionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UserExpressionDefinition_rootExpression_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UserExpressionDefinition_rootExpression_feature", "_UI_UserExpressionDefinition_type"),
				 ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__ROOT_EXPRESSION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES);
			childrenFeatures.add(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UserExpressionDefinition.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UserExpressionDefinition"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UserExpressionDefinition)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_UserExpressionDefinition_type") :
			getString("_UI_UserExpressionDefinition_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UserExpressionDefinition.class)) {
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES:
			case ClockExpressionAndRelationPackage.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClockExpressionAndRelationFactory.eINSTANCE.createConcreteEntity()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClockExpressionAndRelationFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClockExpressionAndRelationFactory.eINSTANCE.createRelation()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 TimeModelFactory.eINSTANCE.createClock()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createRealRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createRealMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntDivide()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetTail()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetHead()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createSeqDecr()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createSeqSched()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createRealVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 BasicTypeFactory.eINSTANCE.createStringElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 BasicTypeFactory.eINSTANCE.createBooleanElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 BasicTypeFactory.eINSTANCE.createIntegerElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 BasicTypeFactory.eINSTANCE.createRealElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 BasicTypeFactory.eINSTANCE.createCharElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 BasicTypeFactory.eINSTANCE.createRecordElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES,
				 BasicTypeFactory.eINSTANCE.createSequenceElement()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createRealRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createRealPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createRealMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createRealMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createUnaryIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntPlus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntMinus()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntMultiply()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntDivide()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createNot()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createAnd()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createOr()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createXor()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createRealEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createRealInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createRealSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntEqual()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntInf()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntSup()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqIsEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetTail()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqGetHead()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqDecr()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createSeqSched()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createBooleanVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createIntegerVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createRealVariableRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqRef()));

		newChildDescriptors.add
			(createChildParameter
				(ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS,
				 ClassicalExpressionFactory.eINSTANCE.createNumberSeqVariableRef()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CONCRETE_ENTITIES ||
			childFeature == ClockExpressionAndRelationPackage.Literals.USER_EXPRESSION_DEFINITION__CLASSICAL_EXPRESSIONS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
