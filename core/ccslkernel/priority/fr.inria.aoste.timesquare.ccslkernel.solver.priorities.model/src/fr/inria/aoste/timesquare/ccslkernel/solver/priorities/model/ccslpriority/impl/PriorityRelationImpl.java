/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;

import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityPackage;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Priority Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PriorityRelationImpl#getLower <em>Lower</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PriorityRelationImpl#getHigher <em>Higher</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PriorityRelationImpl extends EObjectImpl implements PriorityRelation {
	/**
	 * The cached value of the '{@link #getLower() <em>Lower</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLower()
	 * @generated
	 * @ordered
	 */
	protected ConcreteEntity lower;

	/**
	 * The cached value of the '{@link #getHigher() <em>Higher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHigher()
	 * @generated
	 * @ordered
	 */
	protected ConcreteEntity higher;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PriorityRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CcslpriorityPackage.Literals.PRIORITY_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteEntity getLower() {
		if (lower != null && lower.eIsProxy()) {
			InternalEObject oldLower = (InternalEObject)lower;
			lower = (ConcreteEntity)eResolveProxy(oldLower);
			if (lower != oldLower) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CcslpriorityPackage.PRIORITY_RELATION__LOWER, oldLower, lower));
			}
		}
		return lower;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteEntity basicGetLower() {
		return lower;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLower(ConcreteEntity newLower) {
		ConcreteEntity oldLower = lower;
		lower = newLower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CcslpriorityPackage.PRIORITY_RELATION__LOWER, oldLower, lower));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteEntity getHigher() {
		if (higher != null && higher.eIsProxy()) {
			InternalEObject oldHigher = (InternalEObject)higher;
			higher = (ConcreteEntity)eResolveProxy(oldHigher);
			if (higher != oldHigher) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CcslpriorityPackage.PRIORITY_RELATION__HIGHER, oldHigher, higher));
			}
		}
		return higher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteEntity basicGetHigher() {
		return higher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHigher(ConcreteEntity newHigher) {
		ConcreteEntity oldHigher = higher;
		higher = newHigher;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CcslpriorityPackage.PRIORITY_RELATION__HIGHER, oldHigher, higher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_RELATION__LOWER:
				if (resolve) return getLower();
				return basicGetLower();
			case CcslpriorityPackage.PRIORITY_RELATION__HIGHER:
				if (resolve) return getHigher();
				return basicGetHigher();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_RELATION__LOWER:
				setLower((ConcreteEntity)newValue);
				return;
			case CcslpriorityPackage.PRIORITY_RELATION__HIGHER:
				setHigher((ConcreteEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_RELATION__LOWER:
				setLower((ConcreteEntity)null);
				return;
			case CcslpriorityPackage.PRIORITY_RELATION__HIGHER:
				setHigher((ConcreteEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_RELATION__LOWER:
				return lower != null;
			case CcslpriorityPackage.PRIORITY_RELATION__HIGHER:
				return higher != null;
		}
		return super.eIsSet(featureID);
	}

} //PriorityRelationImpl
