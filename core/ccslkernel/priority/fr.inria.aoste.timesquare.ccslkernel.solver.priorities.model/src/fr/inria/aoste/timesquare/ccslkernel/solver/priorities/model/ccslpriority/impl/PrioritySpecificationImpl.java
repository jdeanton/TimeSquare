/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl;

import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityPackage;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Priority Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PrioritySpecificationImpl#getRelations <em>Relations</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PrioritySpecificationImpl#getImportedModel <em>Imported Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PrioritySpecificationImpl extends NamedElementImpl implements PrioritySpecification {
	/**
	 * The cached value of the '{@link #getRelations() <em>Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<PriorityRelation> relations;

	/**
	 * The cached value of the '{@link #getImportedModel() <em>Imported Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportedModel()
	 * @generated
	 * @ordered
	 */
	protected ImportStatement importedModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrioritySpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CcslpriorityPackage.Literals.PRIORITY_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityRelation> getRelations() {
		if (relations == null) {
			relations = new EObjectContainmentEList<PriorityRelation>(PriorityRelation.class, this, CcslpriorityPackage.PRIORITY_SPECIFICATION__RELATIONS);
		}
		return relations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportStatement getImportedModel() {
		return importedModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImportedModel(ImportStatement newImportedModel, NotificationChain msgs) {
		ImportStatement oldImportedModel = importedModel;
		importedModel = newImportedModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL, oldImportedModel, newImportedModel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportedModel(ImportStatement newImportedModel) {
		if (newImportedModel != importedModel) {
			NotificationChain msgs = null;
			if (importedModel != null)
				msgs = ((InternalEObject)importedModel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL, null, msgs);
			if (newImportedModel != null)
				msgs = ((InternalEObject)newImportedModel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL, null, msgs);
			msgs = basicSetImportedModel(newImportedModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL, newImportedModel, newImportedModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__RELATIONS:
				return ((InternalEList<?>)getRelations()).basicRemove(otherEnd, msgs);
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL:
				return basicSetImportedModel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__RELATIONS:
				return getRelations();
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL:
				return getImportedModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__RELATIONS:
				getRelations().clear();
				getRelations().addAll((Collection<? extends PriorityRelation>)newValue);
				return;
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL:
				setImportedModel((ImportStatement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__RELATIONS:
				getRelations().clear();
				return;
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL:
				setImportedModel((ImportStatement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__RELATIONS:
				return relations != null && !relations.isEmpty();
			case CcslpriorityPackage.PRIORITY_SPECIFICATION__IMPORTED_MODEL:
				return importedModel != null;
		}
		return super.eIsSet(featureID);
	}

} //PrioritySpecificationImpl
