/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Priority Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification#getRelations <em>Relations</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification#getImportedModel <em>Imported Model</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityPackage#getPrioritySpecification()
 * @model
 * @generated
 */
public interface PrioritySpecification extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Relations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relations</em>' containment reference list.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityPackage#getPrioritySpecification_Relations()
	 * @model containment="true"
	 * @generated
	 */
	EList<PriorityRelation> getRelations();

	/**
	 * Returns the value of the '<em><b>Imported Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imported Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imported Model</em>' containment reference.
	 * @see #setImportedModel(ImportStatement)
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityPackage#getPrioritySpecification_ImportedModel()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ImportStatement getImportedModel();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification#getImportedModel <em>Imported Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imported Model</em>' containment reference.
	 * @see #getImportedModel()
	 * @generated
	 */
	void setImportedModel(ImportStatement value);

} // PrioritySpecification
