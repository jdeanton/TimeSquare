/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityFactory
 * @model kind="package"
 * @generated
 */
public interface CcslpriorityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ccslpriority";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fr.inria.aoste.timesquare.ccslkernel.solver.priorities";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.inria.aoste.timesquare.ccslkernel.solver.priorities";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CcslpriorityPackage eINSTANCE = fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.NamedElementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PrioritySpecificationImpl <em>Priority Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PrioritySpecificationImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl#getPrioritySpecification()
	 * @generated
	 */
	int PRIORITY_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_SPECIFICATION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_SPECIFICATION__RELATIONS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Imported Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_SPECIFICATION__IMPORTED_MODEL = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Priority Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_SPECIFICATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PriorityRelationImpl <em>Priority Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PriorityRelationImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl#getPriorityRelation()
	 * @generated
	 */
	int PRIORITY_RELATION = 2;

	/**
	 * The feature id for the '<em><b>Lower</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_RELATION__LOWER = 0;

	/**
	 * The feature id for the '<em><b>Higher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_RELATION__HIGHER = 1;

	/**
	 * The number of structural features of the '<em>Priority Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_RELATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.ImportStatementImpl <em>Import Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.ImportStatementImpl
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl#getImportStatement()
	 * @generated
	 */
	int IMPORT_STATEMENT = 3;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_STATEMENT__IMPORT_URI = 0;

	/**
	 * The number of structural features of the '<em>Import Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_STATEMENT_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification <em>Priority Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Priority Specification</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification
	 * @generated
	 */
	EClass getPrioritySpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification#getRelations <em>Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification#getRelations()
	 * @see #getPrioritySpecification()
	 * @generated
	 */
	EReference getPrioritySpecification_Relations();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification#getImportedModel <em>Imported Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imported Model</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification#getImportedModel()
	 * @see #getPrioritySpecification()
	 * @generated
	 */
	EReference getPrioritySpecification_ImportedModel();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation <em>Priority Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Priority Relation</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation
	 * @generated
	 */
	EClass getPriorityRelation();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation#getLower <em>Lower</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lower</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation#getLower()
	 * @see #getPriorityRelation()
	 * @generated
	 */
	EReference getPriorityRelation_Lower();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation#getHigher <em>Higher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Higher</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation#getHigher()
	 * @see #getPriorityRelation()
	 * @generated
	 */
	EReference getPriorityRelation_Higher();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.ImportStatement <em>Import Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import Statement</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.ImportStatement
	 * @generated
	 */
	EClass getImportStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.ImportStatement#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.ImportStatement#getImportURI()
	 * @see #getImportStatement()
	 * @generated
	 */
	EAttribute getImportStatement_ImportURI();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CcslpriorityFactory getCcslpriorityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PrioritySpecificationImpl <em>Priority Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PrioritySpecificationImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl#getPrioritySpecification()
		 * @generated
		 */
		EClass PRIORITY_SPECIFICATION = eINSTANCE.getPrioritySpecification();

		/**
		 * The meta object literal for the '<em><b>Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIORITY_SPECIFICATION__RELATIONS = eINSTANCE.getPrioritySpecification_Relations();

		/**
		 * The meta object literal for the '<em><b>Imported Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIORITY_SPECIFICATION__IMPORTED_MODEL = eINSTANCE.getPrioritySpecification_ImportedModel();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.NamedElementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PriorityRelationImpl <em>Priority Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.PriorityRelationImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl#getPriorityRelation()
		 * @generated
		 */
		EClass PRIORITY_RELATION = eINSTANCE.getPriorityRelation();

		/**
		 * The meta object literal for the '<em><b>Lower</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIORITY_RELATION__LOWER = eINSTANCE.getPriorityRelation_Lower();

		/**
		 * The meta object literal for the '<em><b>Higher</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIORITY_RELATION__HIGHER = eINSTANCE.getPriorityRelation_Higher();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.ImportStatementImpl <em>Import Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.ImportStatementImpl
		 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl.CcslpriorityPackageImpl#getImportStatement()
		 * @generated
		 */
		EClass IMPORT_STATEMENT = eINSTANCE.getImportStatement();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT_STATEMENT__IMPORT_URI = eINSTANCE.getImportStatement_ImportURI();

	}

} //CcslpriorityPackage
