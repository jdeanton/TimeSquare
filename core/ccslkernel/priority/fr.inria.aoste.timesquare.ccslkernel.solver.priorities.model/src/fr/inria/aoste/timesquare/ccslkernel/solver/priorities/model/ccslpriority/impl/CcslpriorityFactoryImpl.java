/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.impl;

import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CcslpriorityFactoryImpl extends EFactoryImpl implements CcslpriorityFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CcslpriorityFactory init() {
		try {
			CcslpriorityFactory theCcslpriorityFactory = (CcslpriorityFactory)EPackage.Registry.INSTANCE.getEFactory(CcslpriorityPackage.eNS_URI);
			if (theCcslpriorityFactory != null) {
				return theCcslpriorityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CcslpriorityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcslpriorityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CcslpriorityPackage.PRIORITY_SPECIFICATION: return createPrioritySpecification();
			case CcslpriorityPackage.PRIORITY_RELATION: return createPriorityRelation();
			case CcslpriorityPackage.IMPORT_STATEMENT: return createImportStatement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrioritySpecification createPrioritySpecification() {
		PrioritySpecificationImpl prioritySpecification = new PrioritySpecificationImpl();
		return prioritySpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityRelation createPriorityRelation() {
		PriorityRelationImpl priorityRelation = new PriorityRelationImpl();
		return priorityRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportStatement createImportStatement() {
		ImportStatementImpl importStatement = new ImportStatementImpl();
		return importStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcslpriorityPackage getCcslpriorityPackage() {
		return (CcslpriorityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CcslpriorityPackage getPackage() {
		return CcslpriorityPackage.eINSTANCE;
	}

} //CcslpriorityFactoryImpl
