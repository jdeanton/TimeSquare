/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Priority Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation#getLower <em>Lower</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation#getHigher <em>Higher</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityPackage#getPriorityRelation()
 * @model
 * @generated
 */
public interface PriorityRelation extends EObject {
	/**
	 * Returns the value of the '<em><b>Lower</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower</em>' reference.
	 * @see #setLower(ConcreteEntity)
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityPackage#getPriorityRelation_Lower()
	 * @model required="true"
	 * @generated
	 */
	ConcreteEntity getLower();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation#getLower <em>Lower</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower</em>' reference.
	 * @see #getLower()
	 * @generated
	 */
	void setLower(ConcreteEntity value);

	/**
	 * Returns the value of the '<em><b>Higher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Higher</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Higher</em>' reference.
	 * @see #setHigher(ConcreteEntity)
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.CcslpriorityPackage#getPriorityRelation_Higher()
	 * @model required="true"
	 * @generated
	 */
	ConcreteEntity getHigher();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation#getHigher <em>Higher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Higher</em>' reference.
	 * @see #getHigher()
	 * @generated
	 */
	void setHigher(ConcreteEntity value);

} // PriorityRelation
