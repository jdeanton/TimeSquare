package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.services.CCSLPrioritiesGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCCSLPrioritiesParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'prevails on'", "'prevailsOn'", "'PrioritySpecification'", "'{'", "'import'", "'}'", "';'"
    };
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=4;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalCCSLPrioritiesParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCCSLPrioritiesParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCCSLPrioritiesParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCCSLPriorities.g"; }


    	private CCSLPrioritiesGrammarAccess grammarAccess;

    	public void setGrammarAccess(CCSLPrioritiesGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRulePrioritySpecification"
    // InternalCCSLPriorities.g:52:1: entryRulePrioritySpecification : rulePrioritySpecification EOF ;
    public final void entryRulePrioritySpecification() throws RecognitionException {
        try {
            // InternalCCSLPriorities.g:53:1: ( rulePrioritySpecification EOF )
            // InternalCCSLPriorities.g:54:1: rulePrioritySpecification EOF
            {
             before(grammarAccess.getPrioritySpecificationRule()); 
            pushFollow(FOLLOW_1);
            rulePrioritySpecification();

            state._fsp--;

             after(grammarAccess.getPrioritySpecificationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrioritySpecification"


    // $ANTLR start "rulePrioritySpecification"
    // InternalCCSLPriorities.g:61:1: rulePrioritySpecification : ( ( rule__PrioritySpecification__Group__0 ) ) ;
    public final void rulePrioritySpecification() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:65:2: ( ( ( rule__PrioritySpecification__Group__0 ) ) )
            // InternalCCSLPriorities.g:66:2: ( ( rule__PrioritySpecification__Group__0 ) )
            {
            // InternalCCSLPriorities.g:66:2: ( ( rule__PrioritySpecification__Group__0 ) )
            // InternalCCSLPriorities.g:67:3: ( rule__PrioritySpecification__Group__0 )
            {
             before(grammarAccess.getPrioritySpecificationAccess().getGroup()); 
            // InternalCCSLPriorities.g:68:3: ( rule__PrioritySpecification__Group__0 )
            // InternalCCSLPriorities.g:68:4: rule__PrioritySpecification__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPrioritySpecificationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrioritySpecification"


    // $ANTLR start "entryRuleEString"
    // InternalCCSLPriorities.g:77:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalCCSLPriorities.g:78:1: ( ruleEString EOF )
            // InternalCCSLPriorities.g:79:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalCCSLPriorities.g:86:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:90:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalCCSLPriorities.g:91:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalCCSLPriorities.g:91:2: ( ( rule__EString__Alternatives ) )
            // InternalCCSLPriorities.g:92:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalCCSLPriorities.g:93:3: ( rule__EString__Alternatives )
            // InternalCCSLPriorities.g:93:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRulePriorityRelation"
    // InternalCCSLPriorities.g:102:1: entryRulePriorityRelation : rulePriorityRelation EOF ;
    public final void entryRulePriorityRelation() throws RecognitionException {
        try {
            // InternalCCSLPriorities.g:103:1: ( rulePriorityRelation EOF )
            // InternalCCSLPriorities.g:104:1: rulePriorityRelation EOF
            {
             before(grammarAccess.getPriorityRelationRule()); 
            pushFollow(FOLLOW_1);
            rulePriorityRelation();

            state._fsp--;

             after(grammarAccess.getPriorityRelationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePriorityRelation"


    // $ANTLR start "rulePriorityRelation"
    // InternalCCSLPriorities.g:111:1: rulePriorityRelation : ( ( rule__PriorityRelation__Group__0 ) ) ;
    public final void rulePriorityRelation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:115:2: ( ( ( rule__PriorityRelation__Group__0 ) ) )
            // InternalCCSLPriorities.g:116:2: ( ( rule__PriorityRelation__Group__0 ) )
            {
            // InternalCCSLPriorities.g:116:2: ( ( rule__PriorityRelation__Group__0 ) )
            // InternalCCSLPriorities.g:117:3: ( rule__PriorityRelation__Group__0 )
            {
             before(grammarAccess.getPriorityRelationAccess().getGroup()); 
            // InternalCCSLPriorities.g:118:3: ( rule__PriorityRelation__Group__0 )
            // InternalCCSLPriorities.g:118:4: rule__PriorityRelation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PriorityRelation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPriorityRelationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePriorityRelation"


    // $ANTLR start "entryRuleImportStatement"
    // InternalCCSLPriorities.g:127:1: entryRuleImportStatement : ruleImportStatement EOF ;
    public final void entryRuleImportStatement() throws RecognitionException {
        try {
            // InternalCCSLPriorities.g:128:1: ( ruleImportStatement EOF )
            // InternalCCSLPriorities.g:129:1: ruleImportStatement EOF
            {
             before(grammarAccess.getImportStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleImportStatement();

            state._fsp--;

             after(grammarAccess.getImportStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalCCSLPriorities.g:136:1: ruleImportStatement : ( ( rule__ImportStatement__Group__0 ) ) ;
    public final void ruleImportStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:140:2: ( ( ( rule__ImportStatement__Group__0 ) ) )
            // InternalCCSLPriorities.g:141:2: ( ( rule__ImportStatement__Group__0 ) )
            {
            // InternalCCSLPriorities.g:141:2: ( ( rule__ImportStatement__Group__0 ) )
            // InternalCCSLPriorities.g:142:3: ( rule__ImportStatement__Group__0 )
            {
             before(grammarAccess.getImportStatementAccess().getGroup()); 
            // InternalCCSLPriorities.g:143:3: ( rule__ImportStatement__Group__0 )
            // InternalCCSLPriorities.g:143:4: rule__ImportStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalCCSLPriorities.g:151:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:155:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_STRING) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalCCSLPriorities.g:156:2: ( RULE_STRING )
                    {
                    // InternalCCSLPriorities.g:156:2: ( RULE_STRING )
                    // InternalCCSLPriorities.g:157:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalCCSLPriorities.g:162:2: ( RULE_ID )
                    {
                    // InternalCCSLPriorities.g:162:2: ( RULE_ID )
                    // InternalCCSLPriorities.g:163:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__PriorityRelation__Alternatives_1"
    // InternalCCSLPriorities.g:172:1: rule__PriorityRelation__Alternatives_1 : ( ( 'prevails on' ) | ( 'prevailsOn' ) );
    public final void rule__PriorityRelation__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:176:1: ( ( 'prevails on' ) | ( 'prevailsOn' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalCCSLPriorities.g:177:2: ( 'prevails on' )
                    {
                    // InternalCCSLPriorities.g:177:2: ( 'prevails on' )
                    // InternalCCSLPriorities.g:178:3: 'prevails on'
                    {
                     before(grammarAccess.getPriorityRelationAccess().getPrevailsOnKeyword_1_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getPriorityRelationAccess().getPrevailsOnKeyword_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalCCSLPriorities.g:183:2: ( 'prevailsOn' )
                    {
                    // InternalCCSLPriorities.g:183:2: ( 'prevailsOn' )
                    // InternalCCSLPriorities.g:184:3: 'prevailsOn'
                    {
                     before(grammarAccess.getPriorityRelationAccess().getPrevailsOnKeyword_1_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getPriorityRelationAccess().getPrevailsOnKeyword_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__Alternatives_1"


    // $ANTLR start "rule__PrioritySpecification__Group__0"
    // InternalCCSLPriorities.g:193:1: rule__PrioritySpecification__Group__0 : rule__PrioritySpecification__Group__0__Impl rule__PrioritySpecification__Group__1 ;
    public final void rule__PrioritySpecification__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:197:1: ( rule__PrioritySpecification__Group__0__Impl rule__PrioritySpecification__Group__1 )
            // InternalCCSLPriorities.g:198:2: rule__PrioritySpecification__Group__0__Impl rule__PrioritySpecification__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__PrioritySpecification__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__0"


    // $ANTLR start "rule__PrioritySpecification__Group__0__Impl"
    // InternalCCSLPriorities.g:205:1: rule__PrioritySpecification__Group__0__Impl : ( 'PrioritySpecification' ) ;
    public final void rule__PrioritySpecification__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:209:1: ( ( 'PrioritySpecification' ) )
            // InternalCCSLPriorities.g:210:1: ( 'PrioritySpecification' )
            {
            // InternalCCSLPriorities.g:210:1: ( 'PrioritySpecification' )
            // InternalCCSLPriorities.g:211:2: 'PrioritySpecification'
            {
             before(grammarAccess.getPrioritySpecificationAccess().getPrioritySpecificationKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getPrioritySpecificationAccess().getPrioritySpecificationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__0__Impl"


    // $ANTLR start "rule__PrioritySpecification__Group__1"
    // InternalCCSLPriorities.g:220:1: rule__PrioritySpecification__Group__1 : rule__PrioritySpecification__Group__1__Impl rule__PrioritySpecification__Group__2 ;
    public final void rule__PrioritySpecification__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:224:1: ( rule__PrioritySpecification__Group__1__Impl rule__PrioritySpecification__Group__2 )
            // InternalCCSLPriorities.g:225:2: rule__PrioritySpecification__Group__1__Impl rule__PrioritySpecification__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__PrioritySpecification__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__1"


    // $ANTLR start "rule__PrioritySpecification__Group__1__Impl"
    // InternalCCSLPriorities.g:232:1: rule__PrioritySpecification__Group__1__Impl : ( ( rule__PrioritySpecification__NameAssignment_1 )? ) ;
    public final void rule__PrioritySpecification__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:236:1: ( ( ( rule__PrioritySpecification__NameAssignment_1 )? ) )
            // InternalCCSLPriorities.g:237:1: ( ( rule__PrioritySpecification__NameAssignment_1 )? )
            {
            // InternalCCSLPriorities.g:237:1: ( ( rule__PrioritySpecification__NameAssignment_1 )? )
            // InternalCCSLPriorities.g:238:2: ( rule__PrioritySpecification__NameAssignment_1 )?
            {
             before(grammarAccess.getPrioritySpecificationAccess().getNameAssignment_1()); 
            // InternalCCSLPriorities.g:239:2: ( rule__PrioritySpecification__NameAssignment_1 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( ((LA3_0>=RULE_STRING && LA3_0<=RULE_ID)) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalCCSLPriorities.g:239:3: rule__PrioritySpecification__NameAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__PrioritySpecification__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPrioritySpecificationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__1__Impl"


    // $ANTLR start "rule__PrioritySpecification__Group__2"
    // InternalCCSLPriorities.g:247:1: rule__PrioritySpecification__Group__2 : rule__PrioritySpecification__Group__2__Impl rule__PrioritySpecification__Group__3 ;
    public final void rule__PrioritySpecification__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:251:1: ( rule__PrioritySpecification__Group__2__Impl rule__PrioritySpecification__Group__3 )
            // InternalCCSLPriorities.g:252:2: rule__PrioritySpecification__Group__2__Impl rule__PrioritySpecification__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__PrioritySpecification__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__2"


    // $ANTLR start "rule__PrioritySpecification__Group__2__Impl"
    // InternalCCSLPriorities.g:259:1: rule__PrioritySpecification__Group__2__Impl : ( '{' ) ;
    public final void rule__PrioritySpecification__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:263:1: ( ( '{' ) )
            // InternalCCSLPriorities.g:264:1: ( '{' )
            {
            // InternalCCSLPriorities.g:264:1: ( '{' )
            // InternalCCSLPriorities.g:265:2: '{'
            {
             before(grammarAccess.getPrioritySpecificationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getPrioritySpecificationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__2__Impl"


    // $ANTLR start "rule__PrioritySpecification__Group__3"
    // InternalCCSLPriorities.g:274:1: rule__PrioritySpecification__Group__3 : rule__PrioritySpecification__Group__3__Impl rule__PrioritySpecification__Group__4 ;
    public final void rule__PrioritySpecification__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:278:1: ( rule__PrioritySpecification__Group__3__Impl rule__PrioritySpecification__Group__4 )
            // InternalCCSLPriorities.g:279:2: rule__PrioritySpecification__Group__3__Impl rule__PrioritySpecification__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__PrioritySpecification__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__3"


    // $ANTLR start "rule__PrioritySpecification__Group__3__Impl"
    // InternalCCSLPriorities.g:286:1: rule__PrioritySpecification__Group__3__Impl : ( 'import' ) ;
    public final void rule__PrioritySpecification__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:290:1: ( ( 'import' ) )
            // InternalCCSLPriorities.g:291:1: ( 'import' )
            {
            // InternalCCSLPriorities.g:291:1: ( 'import' )
            // InternalCCSLPriorities.g:292:2: 'import'
            {
             before(grammarAccess.getPrioritySpecificationAccess().getImportKeyword_3()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getPrioritySpecificationAccess().getImportKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__3__Impl"


    // $ANTLR start "rule__PrioritySpecification__Group__4"
    // InternalCCSLPriorities.g:301:1: rule__PrioritySpecification__Group__4 : rule__PrioritySpecification__Group__4__Impl rule__PrioritySpecification__Group__5 ;
    public final void rule__PrioritySpecification__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:305:1: ( rule__PrioritySpecification__Group__4__Impl rule__PrioritySpecification__Group__5 )
            // InternalCCSLPriorities.g:306:2: rule__PrioritySpecification__Group__4__Impl rule__PrioritySpecification__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__PrioritySpecification__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__4"


    // $ANTLR start "rule__PrioritySpecification__Group__4__Impl"
    // InternalCCSLPriorities.g:313:1: rule__PrioritySpecification__Group__4__Impl : ( ( rule__PrioritySpecification__ImportedModelAssignment_4 ) ) ;
    public final void rule__PrioritySpecification__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:317:1: ( ( ( rule__PrioritySpecification__ImportedModelAssignment_4 ) ) )
            // InternalCCSLPriorities.g:318:1: ( ( rule__PrioritySpecification__ImportedModelAssignment_4 ) )
            {
            // InternalCCSLPriorities.g:318:1: ( ( rule__PrioritySpecification__ImportedModelAssignment_4 ) )
            // InternalCCSLPriorities.g:319:2: ( rule__PrioritySpecification__ImportedModelAssignment_4 )
            {
             before(grammarAccess.getPrioritySpecificationAccess().getImportedModelAssignment_4()); 
            // InternalCCSLPriorities.g:320:2: ( rule__PrioritySpecification__ImportedModelAssignment_4 )
            // InternalCCSLPriorities.g:320:3: rule__PrioritySpecification__ImportedModelAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__ImportedModelAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getPrioritySpecificationAccess().getImportedModelAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__4__Impl"


    // $ANTLR start "rule__PrioritySpecification__Group__5"
    // InternalCCSLPriorities.g:328:1: rule__PrioritySpecification__Group__5 : rule__PrioritySpecification__Group__5__Impl rule__PrioritySpecification__Group__6 ;
    public final void rule__PrioritySpecification__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:332:1: ( rule__PrioritySpecification__Group__5__Impl rule__PrioritySpecification__Group__6 )
            // InternalCCSLPriorities.g:333:2: rule__PrioritySpecification__Group__5__Impl rule__PrioritySpecification__Group__6
            {
            pushFollow(FOLLOW_6);
            rule__PrioritySpecification__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__5"


    // $ANTLR start "rule__PrioritySpecification__Group__5__Impl"
    // InternalCCSLPriorities.g:340:1: rule__PrioritySpecification__Group__5__Impl : ( ( rule__PrioritySpecification__RelationsAssignment_5 )* ) ;
    public final void rule__PrioritySpecification__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:344:1: ( ( ( rule__PrioritySpecification__RelationsAssignment_5 )* ) )
            // InternalCCSLPriorities.g:345:1: ( ( rule__PrioritySpecification__RelationsAssignment_5 )* )
            {
            // InternalCCSLPriorities.g:345:1: ( ( rule__PrioritySpecification__RelationsAssignment_5 )* )
            // InternalCCSLPriorities.g:346:2: ( rule__PrioritySpecification__RelationsAssignment_5 )*
            {
             before(grammarAccess.getPrioritySpecificationAccess().getRelationsAssignment_5()); 
            // InternalCCSLPriorities.g:347:2: ( rule__PrioritySpecification__RelationsAssignment_5 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=RULE_STRING && LA4_0<=RULE_ID)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalCCSLPriorities.g:347:3: rule__PrioritySpecification__RelationsAssignment_5
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__PrioritySpecification__RelationsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getPrioritySpecificationAccess().getRelationsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__5__Impl"


    // $ANTLR start "rule__PrioritySpecification__Group__6"
    // InternalCCSLPriorities.g:355:1: rule__PrioritySpecification__Group__6 : rule__PrioritySpecification__Group__6__Impl ;
    public final void rule__PrioritySpecification__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:359:1: ( rule__PrioritySpecification__Group__6__Impl )
            // InternalCCSLPriorities.g:360:2: rule__PrioritySpecification__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PrioritySpecification__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__6"


    // $ANTLR start "rule__PrioritySpecification__Group__6__Impl"
    // InternalCCSLPriorities.g:366:1: rule__PrioritySpecification__Group__6__Impl : ( '}' ) ;
    public final void rule__PrioritySpecification__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:370:1: ( ( '}' ) )
            // InternalCCSLPriorities.g:371:1: ( '}' )
            {
            // InternalCCSLPriorities.g:371:1: ( '}' )
            // InternalCCSLPriorities.g:372:2: '}'
            {
             before(grammarAccess.getPrioritySpecificationAccess().getRightCurlyBracketKeyword_6()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getPrioritySpecificationAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__Group__6__Impl"


    // $ANTLR start "rule__PriorityRelation__Group__0"
    // InternalCCSLPriorities.g:382:1: rule__PriorityRelation__Group__0 : rule__PriorityRelation__Group__0__Impl rule__PriorityRelation__Group__1 ;
    public final void rule__PriorityRelation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:386:1: ( rule__PriorityRelation__Group__0__Impl rule__PriorityRelation__Group__1 )
            // InternalCCSLPriorities.g:387:2: rule__PriorityRelation__Group__0__Impl rule__PriorityRelation__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__PriorityRelation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PriorityRelation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__Group__0"


    // $ANTLR start "rule__PriorityRelation__Group__0__Impl"
    // InternalCCSLPriorities.g:394:1: rule__PriorityRelation__Group__0__Impl : ( ( rule__PriorityRelation__HigherAssignment_0 ) ) ;
    public final void rule__PriorityRelation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:398:1: ( ( ( rule__PriorityRelation__HigherAssignment_0 ) ) )
            // InternalCCSLPriorities.g:399:1: ( ( rule__PriorityRelation__HigherAssignment_0 ) )
            {
            // InternalCCSLPriorities.g:399:1: ( ( rule__PriorityRelation__HigherAssignment_0 ) )
            // InternalCCSLPriorities.g:400:2: ( rule__PriorityRelation__HigherAssignment_0 )
            {
             before(grammarAccess.getPriorityRelationAccess().getHigherAssignment_0()); 
            // InternalCCSLPriorities.g:401:2: ( rule__PriorityRelation__HigherAssignment_0 )
            // InternalCCSLPriorities.g:401:3: rule__PriorityRelation__HigherAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__PriorityRelation__HigherAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPriorityRelationAccess().getHigherAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__Group__0__Impl"


    // $ANTLR start "rule__PriorityRelation__Group__1"
    // InternalCCSLPriorities.g:409:1: rule__PriorityRelation__Group__1 : rule__PriorityRelation__Group__1__Impl rule__PriorityRelation__Group__2 ;
    public final void rule__PriorityRelation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:413:1: ( rule__PriorityRelation__Group__1__Impl rule__PriorityRelation__Group__2 )
            // InternalCCSLPriorities.g:414:2: rule__PriorityRelation__Group__1__Impl rule__PriorityRelation__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__PriorityRelation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PriorityRelation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__Group__1"


    // $ANTLR start "rule__PriorityRelation__Group__1__Impl"
    // InternalCCSLPriorities.g:421:1: rule__PriorityRelation__Group__1__Impl : ( ( rule__PriorityRelation__Alternatives_1 ) ) ;
    public final void rule__PriorityRelation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:425:1: ( ( ( rule__PriorityRelation__Alternatives_1 ) ) )
            // InternalCCSLPriorities.g:426:1: ( ( rule__PriorityRelation__Alternatives_1 ) )
            {
            // InternalCCSLPriorities.g:426:1: ( ( rule__PriorityRelation__Alternatives_1 ) )
            // InternalCCSLPriorities.g:427:2: ( rule__PriorityRelation__Alternatives_1 )
            {
             before(grammarAccess.getPriorityRelationAccess().getAlternatives_1()); 
            // InternalCCSLPriorities.g:428:2: ( rule__PriorityRelation__Alternatives_1 )
            // InternalCCSLPriorities.g:428:3: rule__PriorityRelation__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__PriorityRelation__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getPriorityRelationAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__Group__1__Impl"


    // $ANTLR start "rule__PriorityRelation__Group__2"
    // InternalCCSLPriorities.g:436:1: rule__PriorityRelation__Group__2 : rule__PriorityRelation__Group__2__Impl ;
    public final void rule__PriorityRelation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:440:1: ( rule__PriorityRelation__Group__2__Impl )
            // InternalCCSLPriorities.g:441:2: rule__PriorityRelation__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PriorityRelation__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__Group__2"


    // $ANTLR start "rule__PriorityRelation__Group__2__Impl"
    // InternalCCSLPriorities.g:447:1: rule__PriorityRelation__Group__2__Impl : ( ( rule__PriorityRelation__LowerAssignment_2 ) ) ;
    public final void rule__PriorityRelation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:451:1: ( ( ( rule__PriorityRelation__LowerAssignment_2 ) ) )
            // InternalCCSLPriorities.g:452:1: ( ( rule__PriorityRelation__LowerAssignment_2 ) )
            {
            // InternalCCSLPriorities.g:452:1: ( ( rule__PriorityRelation__LowerAssignment_2 ) )
            // InternalCCSLPriorities.g:453:2: ( rule__PriorityRelation__LowerAssignment_2 )
            {
             before(grammarAccess.getPriorityRelationAccess().getLowerAssignment_2()); 
            // InternalCCSLPriorities.g:454:2: ( rule__PriorityRelation__LowerAssignment_2 )
            // InternalCCSLPriorities.g:454:3: rule__PriorityRelation__LowerAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__PriorityRelation__LowerAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPriorityRelationAccess().getLowerAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__Group__2__Impl"


    // $ANTLR start "rule__ImportStatement__Group__0"
    // InternalCCSLPriorities.g:463:1: rule__ImportStatement__Group__0 : rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1 ;
    public final void rule__ImportStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:467:1: ( rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1 )
            // InternalCCSLPriorities.g:468:2: rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__ImportStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__0"


    // $ANTLR start "rule__ImportStatement__Group__0__Impl"
    // InternalCCSLPriorities.g:475:1: rule__ImportStatement__Group__0__Impl : ( ( rule__ImportStatement__ImportURIAssignment_0 ) ) ;
    public final void rule__ImportStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:479:1: ( ( ( rule__ImportStatement__ImportURIAssignment_0 ) ) )
            // InternalCCSLPriorities.g:480:1: ( ( rule__ImportStatement__ImportURIAssignment_0 ) )
            {
            // InternalCCSLPriorities.g:480:1: ( ( rule__ImportStatement__ImportURIAssignment_0 ) )
            // InternalCCSLPriorities.g:481:2: ( rule__ImportStatement__ImportURIAssignment_0 )
            {
             before(grammarAccess.getImportStatementAccess().getImportURIAssignment_0()); 
            // InternalCCSLPriorities.g:482:2: ( rule__ImportStatement__ImportURIAssignment_0 )
            // InternalCCSLPriorities.g:482:3: rule__ImportStatement__ImportURIAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__ImportURIAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getImportURIAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__0__Impl"


    // $ANTLR start "rule__ImportStatement__Group__1"
    // InternalCCSLPriorities.g:490:1: rule__ImportStatement__Group__1 : rule__ImportStatement__Group__1__Impl ;
    public final void rule__ImportStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:494:1: ( rule__ImportStatement__Group__1__Impl )
            // InternalCCSLPriorities.g:495:2: rule__ImportStatement__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__1"


    // $ANTLR start "rule__ImportStatement__Group__1__Impl"
    // InternalCCSLPriorities.g:501:1: rule__ImportStatement__Group__1__Impl : ( ';' ) ;
    public final void rule__ImportStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:505:1: ( ( ';' ) )
            // InternalCCSLPriorities.g:506:1: ( ';' )
            {
            // InternalCCSLPriorities.g:506:1: ( ';' )
            // InternalCCSLPriorities.g:507:2: ';'
            {
             before(grammarAccess.getImportStatementAccess().getSemicolonKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getSemicolonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__1__Impl"


    // $ANTLR start "rule__PrioritySpecification__NameAssignment_1"
    // InternalCCSLPriorities.g:517:1: rule__PrioritySpecification__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__PrioritySpecification__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:521:1: ( ( ruleEString ) )
            // InternalCCSLPriorities.g:522:2: ( ruleEString )
            {
            // InternalCCSLPriorities.g:522:2: ( ruleEString )
            // InternalCCSLPriorities.g:523:3: ruleEString
            {
             before(grammarAccess.getPrioritySpecificationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPrioritySpecificationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__NameAssignment_1"


    // $ANTLR start "rule__PrioritySpecification__ImportedModelAssignment_4"
    // InternalCCSLPriorities.g:532:1: rule__PrioritySpecification__ImportedModelAssignment_4 : ( ruleImportStatement ) ;
    public final void rule__PrioritySpecification__ImportedModelAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:536:1: ( ( ruleImportStatement ) )
            // InternalCCSLPriorities.g:537:2: ( ruleImportStatement )
            {
            // InternalCCSLPriorities.g:537:2: ( ruleImportStatement )
            // InternalCCSLPriorities.g:538:3: ruleImportStatement
            {
             before(grammarAccess.getPrioritySpecificationAccess().getImportedModelImportStatementParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleImportStatement();

            state._fsp--;

             after(grammarAccess.getPrioritySpecificationAccess().getImportedModelImportStatementParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__ImportedModelAssignment_4"


    // $ANTLR start "rule__PrioritySpecification__RelationsAssignment_5"
    // InternalCCSLPriorities.g:547:1: rule__PrioritySpecification__RelationsAssignment_5 : ( rulePriorityRelation ) ;
    public final void rule__PrioritySpecification__RelationsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:551:1: ( ( rulePriorityRelation ) )
            // InternalCCSLPriorities.g:552:2: ( rulePriorityRelation )
            {
            // InternalCCSLPriorities.g:552:2: ( rulePriorityRelation )
            // InternalCCSLPriorities.g:553:3: rulePriorityRelation
            {
             before(grammarAccess.getPrioritySpecificationAccess().getRelationsPriorityRelationParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            rulePriorityRelation();

            state._fsp--;

             after(grammarAccess.getPrioritySpecificationAccess().getRelationsPriorityRelationParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrioritySpecification__RelationsAssignment_5"


    // $ANTLR start "rule__PriorityRelation__HigherAssignment_0"
    // InternalCCSLPriorities.g:562:1: rule__PriorityRelation__HigherAssignment_0 : ( ( ruleEString ) ) ;
    public final void rule__PriorityRelation__HigherAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:566:1: ( ( ( ruleEString ) ) )
            // InternalCCSLPriorities.g:567:2: ( ( ruleEString ) )
            {
            // InternalCCSLPriorities.g:567:2: ( ( ruleEString ) )
            // InternalCCSLPriorities.g:568:3: ( ruleEString )
            {
             before(grammarAccess.getPriorityRelationAccess().getHigherConcreteEntityCrossReference_0_0()); 
            // InternalCCSLPriorities.g:569:3: ( ruleEString )
            // InternalCCSLPriorities.g:570:4: ruleEString
            {
             before(grammarAccess.getPriorityRelationAccess().getHigherConcreteEntityEStringParserRuleCall_0_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPriorityRelationAccess().getHigherConcreteEntityEStringParserRuleCall_0_0_1()); 

            }

             after(grammarAccess.getPriorityRelationAccess().getHigherConcreteEntityCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__HigherAssignment_0"


    // $ANTLR start "rule__PriorityRelation__LowerAssignment_2"
    // InternalCCSLPriorities.g:581:1: rule__PriorityRelation__LowerAssignment_2 : ( ( ruleEString ) ) ;
    public final void rule__PriorityRelation__LowerAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:585:1: ( ( ( ruleEString ) ) )
            // InternalCCSLPriorities.g:586:2: ( ( ruleEString ) )
            {
            // InternalCCSLPriorities.g:586:2: ( ( ruleEString ) )
            // InternalCCSLPriorities.g:587:3: ( ruleEString )
            {
             before(grammarAccess.getPriorityRelationAccess().getLowerConcreteEntityCrossReference_2_0()); 
            // InternalCCSLPriorities.g:588:3: ( ruleEString )
            // InternalCCSLPriorities.g:589:4: ruleEString
            {
             before(grammarAccess.getPriorityRelationAccess().getLowerConcreteEntityEStringParserRuleCall_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPriorityRelationAccess().getLowerConcreteEntityEStringParserRuleCall_2_0_1()); 

            }

             after(grammarAccess.getPriorityRelationAccess().getLowerConcreteEntityCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PriorityRelation__LowerAssignment_2"


    // $ANTLR start "rule__ImportStatement__ImportURIAssignment_0"
    // InternalCCSLPriorities.g:600:1: rule__ImportStatement__ImportURIAssignment_0 : ( ruleEString ) ;
    public final void rule__ImportStatement__ImportURIAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalCCSLPriorities.g:604:1: ( ( ruleEString ) )
            // InternalCCSLPriorities.g:605:2: ( ruleEString )
            {
            // InternalCCSLPriorities.g:605:2: ( ruleEString )
            // InternalCCSLPriorities.g:606:3: ruleEString
            {
             before(grammarAccess.getImportStatementAccess().getImportURIEStringParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getImportStatementAccess().getImportURIEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__ImportURIAssignment_0"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000010030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});

}