/*n * generated by Xtext 
 by action of Julien Deantoni */
package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.CCSLPrioritiesRuntimeModule;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.CCSLPrioritiesStandaloneSetup;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
public class CCSLPrioritiesIdeSetup extends CCSLPrioritiesStandaloneSetup {

	@Override
	public Injector createInjector() {
		return Guice.createInjector(Modules2.mixin(new CCSLPrioritiesRuntimeModule(), new CCSLPrioritiesIdeModule()));
	}
	
}
