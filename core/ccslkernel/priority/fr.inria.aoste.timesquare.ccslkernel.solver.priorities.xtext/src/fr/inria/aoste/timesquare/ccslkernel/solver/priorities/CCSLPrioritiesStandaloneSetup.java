/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.ccslkernel.solver.priorities;

//import com.google.inject.Injector;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class CCSLPrioritiesStandaloneSetup extends CCSLPrioritiesStandaloneSetupGenerated{

	public static void doSetup() {
		new CCSLPrioritiesStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
	
//	// Change Julien DeAntoni
//		 @Override
//		 public void register(Injector injector) {
//		 super.register(injector);
//		 org.eclipse.xtext.resource.IResourceServiceProvider serviceProvider = injector.getInstance(LibResourceServiceProviderImpl.class);
//		 org.eclipse.xtext.resource.IResourceServiceProvider.Registry.INSTANCE.getExtensionToFactoryMap().put("extendedCCSL", serviceProvider);
//		 }
}

