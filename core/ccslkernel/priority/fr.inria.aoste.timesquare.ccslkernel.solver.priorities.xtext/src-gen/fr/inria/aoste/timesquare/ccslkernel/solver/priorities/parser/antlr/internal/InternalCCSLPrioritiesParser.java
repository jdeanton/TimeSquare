package fr.inria.aoste.timesquare.ccslkernel.solver.priorities.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.services.CCSLPrioritiesGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCCSLPrioritiesParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'PrioritySpecification'", "'{'", "'import'", "'}'", "'prevails on'", "'prevailsOn'", "';'"
    };
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=4;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalCCSLPrioritiesParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCCSLPrioritiesParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCCSLPrioritiesParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCCSLPriorities.g"; }



     	private CCSLPrioritiesGrammarAccess grammarAccess;

        public InternalCCSLPrioritiesParser(TokenStream input, CCSLPrioritiesGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "PrioritySpecification";
       	}

       	@Override
       	protected CCSLPrioritiesGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRulePrioritySpecification"
    // InternalCCSLPriorities.g:63:1: entryRulePrioritySpecification returns [EObject current=null] : iv_rulePrioritySpecification= rulePrioritySpecification EOF ;
    public final EObject entryRulePrioritySpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrioritySpecification = null;


        try {
            // InternalCCSLPriorities.g:63:62: (iv_rulePrioritySpecification= rulePrioritySpecification EOF )
            // InternalCCSLPriorities.g:64:2: iv_rulePrioritySpecification= rulePrioritySpecification EOF
            {
             newCompositeNode(grammarAccess.getPrioritySpecificationRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrioritySpecification=rulePrioritySpecification();

            state._fsp--;

             current =iv_rulePrioritySpecification; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrioritySpecification"


    // $ANTLR start "rulePrioritySpecification"
    // InternalCCSLPriorities.g:70:1: rulePrioritySpecification returns [EObject current=null] : (otherlv_0= 'PrioritySpecification' ( (lv_name_1_0= ruleEString ) )? otherlv_2= '{' otherlv_3= 'import' ( (lv_importedModel_4_0= ruleImportStatement ) ) ( (lv_relations_5_0= rulePriorityRelation ) )* otherlv_6= '}' ) ;
    public final EObject rulePrioritySpecification() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_importedModel_4_0 = null;

        EObject lv_relations_5_0 = null;



        	enterRule();

        try {
            // InternalCCSLPriorities.g:76:2: ( (otherlv_0= 'PrioritySpecification' ( (lv_name_1_0= ruleEString ) )? otherlv_2= '{' otherlv_3= 'import' ( (lv_importedModel_4_0= ruleImportStatement ) ) ( (lv_relations_5_0= rulePriorityRelation ) )* otherlv_6= '}' ) )
            // InternalCCSLPriorities.g:77:2: (otherlv_0= 'PrioritySpecification' ( (lv_name_1_0= ruleEString ) )? otherlv_2= '{' otherlv_3= 'import' ( (lv_importedModel_4_0= ruleImportStatement ) ) ( (lv_relations_5_0= rulePriorityRelation ) )* otherlv_6= '}' )
            {
            // InternalCCSLPriorities.g:77:2: (otherlv_0= 'PrioritySpecification' ( (lv_name_1_0= ruleEString ) )? otherlv_2= '{' otherlv_3= 'import' ( (lv_importedModel_4_0= ruleImportStatement ) ) ( (lv_relations_5_0= rulePriorityRelation ) )* otherlv_6= '}' )
            // InternalCCSLPriorities.g:78:3: otherlv_0= 'PrioritySpecification' ( (lv_name_1_0= ruleEString ) )? otherlv_2= '{' otherlv_3= 'import' ( (lv_importedModel_4_0= ruleImportStatement ) ) ( (lv_relations_5_0= rulePriorityRelation ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPrioritySpecificationAccess().getPrioritySpecificationKeyword_0());
            		
            // InternalCCSLPriorities.g:82:3: ( (lv_name_1_0= ruleEString ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=RULE_STRING && LA1_0<=RULE_ID)) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalCCSLPriorities.g:83:4: (lv_name_1_0= ruleEString )
                    {
                    // InternalCCSLPriorities.g:83:4: (lv_name_1_0= ruleEString )
                    // InternalCCSLPriorities.g:84:5: lv_name_1_0= ruleEString
                    {

                    					newCompositeNode(grammarAccess.getPrioritySpecificationAccess().getNameEStringParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_4);
                    lv_name_1_0=ruleEString();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getPrioritySpecificationRule());
                    					}
                    					set(
                    						current,
                    						"name",
                    						lv_name_1_0,
                    						"fr.inria.aoste.timesquare.ccslkernel.solver.priorities.CCSLPriorities.EString");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getPrioritySpecificationAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getPrioritySpecificationAccess().getImportKeyword_3());
            		
            // InternalCCSLPriorities.g:109:3: ( (lv_importedModel_4_0= ruleImportStatement ) )
            // InternalCCSLPriorities.g:110:4: (lv_importedModel_4_0= ruleImportStatement )
            {
            // InternalCCSLPriorities.g:110:4: (lv_importedModel_4_0= ruleImportStatement )
            // InternalCCSLPriorities.g:111:5: lv_importedModel_4_0= ruleImportStatement
            {

            					newCompositeNode(grammarAccess.getPrioritySpecificationAccess().getImportedModelImportStatementParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_7);
            lv_importedModel_4_0=ruleImportStatement();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPrioritySpecificationRule());
            					}
            					set(
            						current,
            						"importedModel",
            						lv_importedModel_4_0,
            						"fr.inria.aoste.timesquare.ccslkernel.solver.priorities.CCSLPriorities.ImportStatement");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalCCSLPriorities.g:128:3: ( (lv_relations_5_0= rulePriorityRelation ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=RULE_STRING && LA2_0<=RULE_ID)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalCCSLPriorities.g:129:4: (lv_relations_5_0= rulePriorityRelation )
            	    {
            	    // InternalCCSLPriorities.g:129:4: (lv_relations_5_0= rulePriorityRelation )
            	    // InternalCCSLPriorities.g:130:5: lv_relations_5_0= rulePriorityRelation
            	    {

            	    					newCompositeNode(grammarAccess.getPrioritySpecificationAccess().getRelationsPriorityRelationParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_relations_5_0=rulePriorityRelation();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPrioritySpecificationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"relations",
            	    						lv_relations_5_0,
            	    						"fr.inria.aoste.timesquare.ccslkernel.solver.priorities.CCSLPriorities.PriorityRelation");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_6=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getPrioritySpecificationAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrioritySpecification"


    // $ANTLR start "entryRuleEString"
    // InternalCCSLPriorities.g:155:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalCCSLPriorities.g:155:47: (iv_ruleEString= ruleEString EOF )
            // InternalCCSLPriorities.g:156:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalCCSLPriorities.g:162:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalCCSLPriorities.g:168:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalCCSLPriorities.g:169:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalCCSLPriorities.g:169:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_STRING) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalCCSLPriorities.g:170:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalCCSLPriorities.g:178:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRulePriorityRelation"
    // InternalCCSLPriorities.g:189:1: entryRulePriorityRelation returns [EObject current=null] : iv_rulePriorityRelation= rulePriorityRelation EOF ;
    public final EObject entryRulePriorityRelation() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePriorityRelation = null;


        try {
            // InternalCCSLPriorities.g:189:57: (iv_rulePriorityRelation= rulePriorityRelation EOF )
            // InternalCCSLPriorities.g:190:2: iv_rulePriorityRelation= rulePriorityRelation EOF
            {
             newCompositeNode(grammarAccess.getPriorityRelationRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePriorityRelation=rulePriorityRelation();

            state._fsp--;

             current =iv_rulePriorityRelation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePriorityRelation"


    // $ANTLR start "rulePriorityRelation"
    // InternalCCSLPriorities.g:196:1: rulePriorityRelation returns [EObject current=null] : ( ( ( ruleEString ) ) (otherlv_1= 'prevails on' | otherlv_2= 'prevailsOn' ) ( ( ruleEString ) ) ) ;
    public final EObject rulePriorityRelation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalCCSLPriorities.g:202:2: ( ( ( ( ruleEString ) ) (otherlv_1= 'prevails on' | otherlv_2= 'prevailsOn' ) ( ( ruleEString ) ) ) )
            // InternalCCSLPriorities.g:203:2: ( ( ( ruleEString ) ) (otherlv_1= 'prevails on' | otherlv_2= 'prevailsOn' ) ( ( ruleEString ) ) )
            {
            // InternalCCSLPriorities.g:203:2: ( ( ( ruleEString ) ) (otherlv_1= 'prevails on' | otherlv_2= 'prevailsOn' ) ( ( ruleEString ) ) )
            // InternalCCSLPriorities.g:204:3: ( ( ruleEString ) ) (otherlv_1= 'prevails on' | otherlv_2= 'prevailsOn' ) ( ( ruleEString ) )
            {
            // InternalCCSLPriorities.g:204:3: ( ( ruleEString ) )
            // InternalCCSLPriorities.g:205:4: ( ruleEString )
            {
            // InternalCCSLPriorities.g:205:4: ( ruleEString )
            // InternalCCSLPriorities.g:206:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPriorityRelationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getPriorityRelationAccess().getHigherConcreteEntityCrossReference_0_0());
            				
            pushFollow(FOLLOW_8);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalCCSLPriorities.g:220:3: (otherlv_1= 'prevails on' | otherlv_2= 'prevailsOn' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            else if ( (LA4_0==16) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalCCSLPriorities.g:221:4: otherlv_1= 'prevails on'
                    {
                    otherlv_1=(Token)match(input,15,FOLLOW_6); 

                    				newLeafNode(otherlv_1, grammarAccess.getPriorityRelationAccess().getPrevailsOnKeyword_1_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalCCSLPriorities.g:226:4: otherlv_2= 'prevailsOn'
                    {
                    otherlv_2=(Token)match(input,16,FOLLOW_6); 

                    				newLeafNode(otherlv_2, grammarAccess.getPriorityRelationAccess().getPrevailsOnKeyword_1_1());
                    			

                    }
                    break;

            }

            // InternalCCSLPriorities.g:231:3: ( ( ruleEString ) )
            // InternalCCSLPriorities.g:232:4: ( ruleEString )
            {
            // InternalCCSLPriorities.g:232:4: ( ruleEString )
            // InternalCCSLPriorities.g:233:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPriorityRelationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getPriorityRelationAccess().getLowerConcreteEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePriorityRelation"


    // $ANTLR start "entryRuleImportStatement"
    // InternalCCSLPriorities.g:251:1: entryRuleImportStatement returns [EObject current=null] : iv_ruleImportStatement= ruleImportStatement EOF ;
    public final EObject entryRuleImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportStatement = null;


        try {
            // InternalCCSLPriorities.g:251:56: (iv_ruleImportStatement= ruleImportStatement EOF )
            // InternalCCSLPriorities.g:252:2: iv_ruleImportStatement= ruleImportStatement EOF
            {
             newCompositeNode(grammarAccess.getImportStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImportStatement=ruleImportStatement();

            state._fsp--;

             current =iv_ruleImportStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalCCSLPriorities.g:258:1: ruleImportStatement returns [EObject current=null] : ( ( (lv_importURI_0_0= ruleEString ) ) otherlv_1= ';' ) ;
    public final EObject ruleImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_importURI_0_0 = null;



        	enterRule();

        try {
            // InternalCCSLPriorities.g:264:2: ( ( ( (lv_importURI_0_0= ruleEString ) ) otherlv_1= ';' ) )
            // InternalCCSLPriorities.g:265:2: ( ( (lv_importURI_0_0= ruleEString ) ) otherlv_1= ';' )
            {
            // InternalCCSLPriorities.g:265:2: ( ( (lv_importURI_0_0= ruleEString ) ) otherlv_1= ';' )
            // InternalCCSLPriorities.g:266:3: ( (lv_importURI_0_0= ruleEString ) ) otherlv_1= ';'
            {
            // InternalCCSLPriorities.g:266:3: ( (lv_importURI_0_0= ruleEString ) )
            // InternalCCSLPriorities.g:267:4: (lv_importURI_0_0= ruleEString )
            {
            // InternalCCSLPriorities.g:267:4: (lv_importURI_0_0= ruleEString )
            // InternalCCSLPriorities.g:268:5: lv_importURI_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getImportStatementAccess().getImportURIEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_9);
            lv_importURI_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportStatementRule());
            					}
            					set(
            						current,
            						"importURI",
            						lv_importURI_0_0,
            						"fr.inria.aoste.timesquare.ccslkernel.solver.priorities.CCSLPriorities.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getImportStatementAccess().getSemicolonKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportStatement"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004030L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});

}