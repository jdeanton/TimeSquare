/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeUpTo extends AbstractRuntimeExpression {

	private RuntimeClock clockToFollow;
	private RuntimeClock killerClock;
	private boolean isPreemptive = true;

	public RuntimeUpTo(RuntimeClock iClock, RuntimeClock clockToFollow, RuntimeClock killerClock) {
		super(iClock);
		this.clockToFollow = clockToFollow;
		this.killerClock = killerClock;
	}

	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if (clockToFollow instanceof ICCSLConstraint)
			((ICCSLConstraint) clockToFollow).semantic(helper);
		if (killerClock instanceof ICCSLConstraint)
			((ICCSLConstraint) killerClock).semantic(helper);
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if (state == State.DEAD) {
			helper.inhibitClock(getExpressionClock());
		}
		else {
			if (isPreemptive) {
				helper.semanticBDDAnd(helper.createEqual(getExpressionClock(),
						helper.createIntersection(getClockToFollow(), helper.createNot(getKillerClock()))));

			} else {
				helper.semanticBDDAnd(helper.createEqual(getExpressionClock(), getClockToFollow()));

			}
		}
		helper.registerClockUse(new RuntimeClock[] { getExpressionClock(), getClockToFollow(), getKillerClock() });
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (clockToFollow instanceof ICCSLConstraint)
			((ICCSLConstraint) clockToFollow).deathSemantic(helper);
		if (killerClock instanceof ICCSLConstraint)
			((ICCSLConstraint) killerClock).deathSemantic(helper);
		helper.registerDeathImplication(clockToFollow, getExpressionClock());
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (clockToFollow instanceof ICCSLConstraint)
			((ICCSLConstraint) clockToFollow).update(helper);
		if (killerClock instanceof ICCSLConstraint)
			((ICCSLConstraint) killerClock).update(helper);
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if (helper.clockHasFired(killerClock)) {
			terminate(helper);
		}
//		else if (clockToFollow.isDead()) {
//			terminate(helper);
//		}
	}
	
	public RuntimeClock getClockToFollow() {
		return clockToFollow;
	}

	public RuntimeClock getKillerClock() {
		return killerClock;
	}

	public void setPreemptive(boolean isPreemptive) {
		this.isPreemptive = isPreemptive;
	}

}
