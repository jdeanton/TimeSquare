/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime;


/**
 * Base class for all classes implementing the {@link ICCSLConstraint} interface. It allows to
 * implement an internal state machine to keep track of the use of the {@link ICCSLConstraint}
 * functions on the target constraint object.
 * 
 * <p>Two groups of function are defined by this class: those the name of which have the form
 * canCallXXX() are used to check if the state machine is in the right state to accept a transition
 * by executing the XXX() function, and functions of the form XXXTransition commit the state change
 * after execution of XXX(). Functions XXX() are those of th {@link ICCSLConstraint} interface.
 * 
 * <p>If a class both implements {@link ICCSLConstraint} and is a direct subclass of this class, then
 * the functions of the ICCSLConstraint interface have to be written in the following way :
 * <pre><blockquote>
 * 	{@literal @}Override
 *	public void start(AbstractSemanticHelper helper) throws SimulationException {
 *	if (canCallStart())
 *	  super.startTransition();
 *  else
 *    return;
 *  // Other statements of the start() function...
 *	}
 * </blockquote></pre>
 * 
 * @author nchleq
 */
public abstract class AbstractConstraint {
	
	protected enum State {
		INIT, SEMANTIC, UPDATE, DEAD
	}
	/*
	 * Statechart: at creation, state is INIT, goes to SEMANTIC after start().
	 * From SEMANTIC, can go to UPDATE after semantic() or DEAD after end()/terminate().
	 * From UPDATE, goes to SEMANTIC after update() or to DEAD after end()/terminate(). Stays in the
	 * same state on deathSemantic().
	 * From DEAD can only go to SEMANTIC after start(), or stay in the DEAD state on semantic().
	 */

	protected State state = State.INIT;
	protected boolean born = false;

	public AbstractConstraint() {
		state = State.INIT;
		born = false;
	}

	public State getState() {
		return state;
	}
	
	public boolean canCallStart() {
		return true;
	}
	
	protected void startTransition() {
		if (state == State.INIT) {
			born = true;
		}
		state = State.SEMANTIC;
	}
	
	public boolean canCallSemantic() {
		//we can actually ask for the semantics several time in a safe way
		return (state == State.UPDATE ||state == State.SEMANTIC || state == State.DEAD);
	}
	
	protected void semanticTransition() {
		if (state == State.SEMANTIC)
			state = State.UPDATE;
	}

	public boolean canCallUpdate() {
		return state == State.UPDATE;
	}
	
	protected void updateTransition() {
		if (state == State.UPDATE)
			state = State.SEMANTIC;
	}
	
	protected void deathSemantic() {
		// No state change.
	}
	
	public boolean canCallTerminate() {
		return true;
	}
	
	protected void terminateTransition() {
		state = State.DEAD;
	}
	
	public boolean isDead() {
		return state == State.DEAD;
	}
	
	public boolean isActiveState() {
		return (state == State.SEMANTIC || state == State.UPDATE);
	}

	public boolean wasBorn() {
		return born;
	}
}
