/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.relations;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class RuntimeSubClock extends AbstractRuntimeRelation {

	private RuntimeClock leftClock;
	private RuntimeClock rightClock;
	
	public RuntimeSubClock(RuntimeClock left, RuntimeClock right) {
		this.leftClock = left;
		this.rightClock = right;
	}

	@Override
	protected ICCSLConstraint[] getConstraints() {
		return new ICCSLConstraint[0];
	}

	@Override
	public List<RuntimeClock> getDiscreteClocks() {
		return Collections.emptyList();
	}

	@Override
	public Collection<? extends AbstractRuntimeRelation> getAllAssertions() {
		return Collections.emptyList();
	}

	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if (isAssertion()) {
			assertionSemantic(helper);
		}
		else {
			BuDDyBDD semantic = helper.createImplies(leftClock, rightClock);
			helper.semanticBDDAnd(semantic);
			helper.registerClockUse(leftClock);
			helper.registerClockUse(rightClock);
		}
	}
	
	@Override
	public void assertionSemantic(AbstractSemanticHelper helper) {
		BuDDyBDD semantic = helper.createImplies(leftClock, rightClock);
		helper.assertionSemantic(this, semantic);
		helper.registerClockUse(leftClock);
		helper.registerClockUse(rightClock);
	}

}
