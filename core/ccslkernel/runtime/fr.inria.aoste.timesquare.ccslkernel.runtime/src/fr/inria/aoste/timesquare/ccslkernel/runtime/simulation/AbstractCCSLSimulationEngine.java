/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.runtime.AbstractRuntimeModel;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.BDDHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;

public abstract class AbstractCCSLSimulationEngine {

	protected AbstractRuntimeModel model;
	
	protected HashMap<RuntimeClock, Integer> clockToIndexInSolution;
	protected HashMap<Integer, Integer> bddVarToIndexInSolution;
	
	protected HashMap<Integer, AbstractRuntimeRelation> bddAssertVarToRelation;
	protected ArrayList<AbstractRuntimeRelation> assertions;
	protected HashMap<AbstractRuntimeRelation, Integer> assertionsToIndexInSolution;
	
	public AbstractCCSLSimulationEngine() {
		init();
	}
	
	public AbstractCCSLSimulationEngine(AbstractRuntimeModel model) {
		this.model = model;

		init();
	}
	
	private void init() {
		this.bddAssertVarToRelation = new HashMap<Integer, AbstractRuntimeRelation>();
		this.assertions = new ArrayList<AbstractRuntimeRelation>();
		this.assertionsToIndexInSolution = new HashMap<AbstractRuntimeRelation, Integer>();
		this.clockToIndexInSolution = new HashMap<RuntimeClock, Integer>();
		this.bddVarToIndexInSolution = new HashMap<Integer, Integer>();		
		initializeBDDIndexes();
		initializeAssertions();
	}

	
	private int solutionIndexCounter = 0;
	protected int newSolutionIndexNumber() {
		int res = solutionIndexCounter;
		solutionIndexCounter++;
		return res;
	}

	protected void initializeBDDIndexes() {
		List<RuntimeClock> allClocks = getAllDiscreteClocks();
		for (RuntimeClock clock : allClocks) {
			Integer recordedIndex = bddVarToIndexInSolution.get(clock.bddVariableNumber);
			int index = (recordedIndex != null ? recordedIndex : newSolutionIndexNumber());
			clockToIndexInSolution.put(clock, index);
			bddVarToIndexInSolution.put(clock.bddVariableNumber, index);
		}
	}

	protected abstract List<RuntimeClock> getAllDiscreteClocks();
	
	protected void initializeAssertions() {
		for (AbstractRuntimeRelation assertion : getAllAssertions()) {
			assertion.setAssertionVariable( BDDHelper.newBDDVariableNumber() );
			bddAssertVarToRelation.put(assertion.getAssertionVariable(), assertion);
			int index = newSolutionIndexNumber();
			assertionsToIndexInSolution.put(assertion, index);
			bddVarToIndexInSolution.put(assertion.getAssertionVariable(), index);
		}
	}
	
	protected abstract List<AbstractRuntimeRelation> getAllAssertions();

	public abstract void initSimulation() throws SimulationException;

	public abstract void endSimulation() throws SimulationException;
	
	public AbstractRuntimeModel getModel() {
		return model;
	}
	
	public abstract AbstractStepExecutionEngine createStepExecutionEngine();
	
	public HashMap<Integer, Integer> getBddVarToIndexInSolution() {
		return bddVarToIndexInSolution;
	}
	
	public Integer getIndexInSolution(RuntimeClock clock) {
		return bddVarToIndexInSolution.get(clock);
	}
	
	public Integer getIndexInSolution(AbstractRuntimeRelation assertion) {
		if ( ! assertion.isAssertion())
			return null;
		return assertionsToIndexInSolution.get(assertion);
	}

	private Set<RuntimeClock> deadClocks = new HashSet<RuntimeClock>();

	/**
	 * Returns the set of dead clocks : all clocks recognized as dead up to now in the 
	 * current simulation.
	 * 
	 * @return
	 * @see #addDeadClock(RuntimeClock)
	 */
	public Set<RuntimeClock> getDeadClocks() {
		return deadClocks;
	}

	/**
	 * Adds a new clock clock to the set of dead clocks returned by {@link #getDeadClocks()}.
	 * @param clock
	 */
	public void addDeadClock(RuntimeClock clock) {
		deadClocks.add(clock);
	}
	
}
