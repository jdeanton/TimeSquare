/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.relations;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.NoBooleanSolution;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class RuntimeNonStrictPrecedes extends AbstractRuntimeRelation {

	private int delta;
	private RuntimeClock leftClock;
	private RuntimeClock rightClock;

	public RuntimeNonStrictPrecedes(RuntimeClock left, RuntimeClock right) {
		this.leftClock = left;
		this.rightClock = right;
		delta = 0;
	}

	@Override
	protected ICCSLConstraint[] getConstraints() {
		return new ICCSLConstraint[0];
	}

	@Override
	public List<RuntimeClock> getDiscreteClocks() {
		return Collections.emptyList();
	}

	@Override
	public Collection<? extends AbstractRuntimeRelation> getAllAssertions() {
		return Collections.emptyList();
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		delta = 0;
	}

	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if (!canCallSemantic())
			return;
		super.semantic(helper);
		if (isAssertion()) {
			assertionSemantic(helper);
		}
		else if (delta == 0) {
			helper.semanticBDDAnd(helper.createImplies(rightClock, leftClock));
		}
		else if (delta < 0) {
			throw new NoBooleanSolution("A precedence relation has been violated" + this.getQualifiedName());
		}
		helper.registerClockUse(new RuntimeClock[] { leftClock, rightClock });
	}

	@Override
	public void assertionSemantic(AbstractSemanticHelper helper) {
		if (delta < 0) {
			BuDDyBDD assertion = helper.getAssertionVariable(this);
			helper.semanticBDDAnd(assertion.not());
			helper.registerAssertion(this);
		}
		else if (delta == 0) {
			helper.assertionSemantic(this, helper.createImplies(rightClock, leftClock));
		}
		else if (delta > 0) {
			BuDDyBDD assertion = helper.getAssertionVariable(this);
			helper.semanticBDDAnd(assertion);
			helper.registerAssertion(this);
		}
		helper.registerClockUse(new RuntimeClock[] { leftClock, rightClock });
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (!canCallUpdate())
			return;
		super.update(helper);
		if (helper.clockHasFired(leftClock)) {
			delta++;
		}
		if (helper.clockHasFired(rightClock)) {
			delta--;
		}
	}
	
	
	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		currentState.dump(delta);
		return currentState;
	}
	
	@Override
	public void restoreState(SerializedConstraintState state) {
		super.restoreState(state);
		delta = (Integer) state.restore(0);
	}

}
