/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.simulation;

import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.AbstractRuntimeModel;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.BDDHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;

public class CCSLSimulationEngine extends AbstractCCSLSimulationEngine {

	private SimulationPolicyBase simulationPolicy = null;
	
	public CCSLSimulationEngine(AbstractRuntimeModel model) {
		super(model);
	}

	@Override
	protected List<RuntimeClock> getAllDiscreteClocks() {
		return model.getAllDiscreteClocks();
	}

	@Override
	protected List<AbstractRuntimeRelation> getAllAssertions() {
		return model.getAllAssertions();
	}
	
	@Override
	public AbstractStepExecutionEngine createStepExecutionEngine() {
		return new CCSLStepExecutionEngine(this);
	}
	
	@Override
	public void initSimulation() throws SimulationException {
		getModel().initSimulation(createStepExecutionEngine().getSemanticHelper());
	}
	
	@Override
	public void endSimulation() throws SimulationException {
		BDDHelper.terminateBDDUsage();
	}
	
	public SimulationPolicyBase getSimulationPolicy() {
		return simulationPolicy;
	}
	
	public void setSimulationPolicy(SimulationPolicyBase simulationPolicy) {
		this.simulationPolicy = simulationPolicy;
	}
	
}
