/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeSup extends AbstractRuntimeExpression {

	private RuntimeClock clock1;
	private RuntimeClock clock2;

	private int _deltaWithC1; // implicitClock.tickCount - (clock1.tickCount -
								// clock1.tickCount@self.birth)
	private int _deltaWithC2; // implicitClock.tickCount - (clock2.tickCount -
								// clock2.tickCount@self.birth)

	public RuntimeSup(RuntimeClock iClock, RuntimeClock clock1, RuntimeClock clock2) {
		super(iClock);
		this.clock1 = clock1;
		this.clock2 = clock2;
		this._deltaWithC1 = 0;
		this._deltaWithC2 = 0;
	}
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		_deltaWithC1 = 0;
		_deltaWithC2 = 0;
	}

	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if (clock1 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock1).semantic(helper);
		if (clock2 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock2).semantic(helper);
		if (state == State.DEAD) {
			helper.inhibitClock(getExpressionClock());
		}
		else {
			if (_deltaWithC1 == 0) {
				if (_deltaWithC2 == 0) {
					helper.semanticBDDAnd(helper.createEqual(getExpressionClock(),
							helper.createIntersection(getClock1(), getClock2())));
				}
				else {
					helper.semanticBDDAnd(helper.createEqual(getExpressionClock(), getClock1()));
				}
			}
			else {
				if (_deltaWithC2 == 0) {
					helper.semanticBDDAnd(helper.createEqual(getExpressionClock(), getClock2()));
				}
				else {
					helper.inhibitClock(getExpressionClock());
				}
			}
		}
		helper.registerClockUse(new RuntimeClock[] { getExpressionClock(), getClock1(), getClock2() });
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if (clock1 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock1).update(helper);
		if (clock2 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock2).update(helper);
		if (helper.clockHasFired( getExpressionClock() )) {
			_deltaWithC1++;
			_deltaWithC2++;
		}
		if (helper.clockHasFired(clock1)) {
			_deltaWithC1--;
		}
		if (helper.clockHasFired(clock2)) {
			_deltaWithC2--;
		}
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (clock1 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock1).deathSemantic(helper);
		if (clock2 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock2).deathSemantic(helper);
		helper.registerDeathConjunctionImplies(clock1, clock2, getExpressionClock());
	}
	
	public RuntimeClock getClock1() {
		return clock1;
	}

	public RuntimeClock getClock2() {
		return clock2;
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
		currentState.dump(new Integer(_deltaWithC1));
		currentState.dump(new Integer(_deltaWithC2));
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
		_deltaWithC1 = (Integer) newState.restore(2);
		_deltaWithC2 = (Integer) newState.restore(3);
	}

}
