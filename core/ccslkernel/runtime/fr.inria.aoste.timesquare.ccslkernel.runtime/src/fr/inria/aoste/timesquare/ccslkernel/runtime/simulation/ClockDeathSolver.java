/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.simulation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;


/**
 * A class dedicated to solve the clock death problem.
 * 
 * Death conditions are made only of equalities and implications between clocks
 * together with some simple clauses of one variable that assert the death of a
 * particular clock. The death problem solution is a list of clock that are dead
 * because either they are equal to a another dead clock, or they are the target
 * of an implication the left part of which is a dead clock.
 * 
 * This class solves such a problem by registering three forms of declaration :
 * <ul>
 * 	<li>
 * 	A single clock means that this clock is to be considered as dead 
 * 		{@link #registerDeadClock(RuntimeClock)}.
 *	 </li>
 * 	<li> An implication c1 => c2 means that if c1 is dead, then c2 is also dead
 *  	{@link #registerImplication(RuntimeClock, RuntimeClock)}.
 *   </li>
 *  <li>
 *  An equality c1 = c2 means that c1 and c2 are both dead or not dead at the same time
 *  	{@link #registerEquality(RuntimeClock, RuntimeClock)}.
 * 	 </li>
 * </ul>
 * Each time such a declaration is registered, all the internal structures are updated, the 
 * transitive closure of both the equalities and implications is maintained and the set of
 * dead clocks can be found using the {@link #getDeadClocks()} function.
 * @author nchleq
 * 
 */
public class ClockDeathSolver {

	public void clear() {
		deadClocks.clear();
		implications.clear();
		equalityClasses.clear();
		equalityClassMap.clear();
	}

	/**
	 * Register that the clock is dead.
	 * 
	 * @param clock
	 */
	public void registerDeadClock(RuntimeClock clock) {
		if (deadClocks.add(clock)) {
			// propagate using the implications
			Set<RuntimeClock> implied = implications.get(clock); // clock => implied
			if (implied != null) {
				for (RuntimeClock c : implied) {
					registerDeadClock(c);
				}
			}
			// propagate using the equalities
			if (equalityClassMap.containsKey(clock)) {
				int eqClass = equalityClassMap.get(clock);
				Set<RuntimeClock> others = equalityClasses.elementAt(eqClass);
				if (others != null) {
					for (RuntimeClock eq : others) {
						registerDeadClock(eq);
					}
				}
			}
			// propagate using conjunction implications
			for (ClockConjunction cj : findConjunction(clock)) {
				if (((cj.clock1 == clock) && deadClocks.contains(cj.clock2))
						||
					((cj.clock2 == clock) && deadClocks.contains(cj.clock1))) {
					for (RuntimeClock right : conjunctImplications.get(cj)) {
						registerDeadClock(right);
					}
				}
			}
		}
	}

	/**
	 * Register the equality between two clocks. 
	 * @param c1
	 * @param c2
	 */
	public void registerEquality(RuntimeClock c1, RuntimeClock c2) {
		if (c1 == c2) {
			return;
		}
		Integer class1 = equalityClassMap.get(c1);
		Integer class2 = equalityClassMap.get(c2);
		if (class1 != null) {
			if (class2 != null) {
				mergeClasses(class1, class2);
			}
			else {
				storeInClass(class1, c2);
			}
		}
		else {
			if (class2 != null) {
				storeInClass(class2, c1);
			}
			else {
				int newClass = newClassIndex();
				storeInClass(newClass, c1);
				storeInClass(newClass, c2);
			}
		}
		if (deadClocks.contains(c1))
			registerDeadClock(c2);
		else if (deadClocks.contains(c2))
			registerDeadClock(c1);
	}

	/**
	 * Register an implication
	 * @param left the left part of the implication
	 * @param right the right part of the implication, meaning "left is dead implies right is dead".
	 */
	public void registerImplication(RuntimeClock left, RuntimeClock right) {
		if (left == right) {
			return;
		}
		HashSet<RuntimeClock> implied = (HashSet<RuntimeClock>) implications.get(left);
		if (implied == null) {
			implied = new HashSet<RuntimeClock>();
			implications.put(left, implied);
		}
		if (implied.add(right)) {
			if (deadClocks.contains(left)) {
				registerDeadClock(right);
			}
		}
	}

	public List<RuntimeClock> getDeadClocks() {
		return new ArrayList<RuntimeClock>(deadClocks);
	}

	/**
	 * Returns the set of clock that are registered to be "equal" to the clock given as argument.
	 * Being "equal" means that a call to {@link #registerEquality(RuntimeClock, RuntimeClock)}	was
	 * done and the clock was one of the two arguments of this call.
	 * The returned list does not contain clock itself, it is removed before returning the list.
	 * @param clock
	 * @return
	 */
	public List<RuntimeClock> getEqualClocks(RuntimeClock clock) {
		Integer eqClass = equalityClassMap.get(clock);
		if (eqClass == null) {
			return Collections.emptyList();
		}
		Set<RuntimeClock> others = equalityClasses.elementAt(eqClass);
		List<RuntimeClock> res = new ArrayList<RuntimeClock>(others);
		res.remove(clock);
		return res;
	}
	
	private HashSet<RuntimeClock> deadClocks = new HashSet<RuntimeClock>();

	/* Implications are registered using a map that for each implications of the form c1 => c2
	 * (meaning "c1 dead implies c2 dead"), store the set {c2} as the value for the key c1 in the
	 * map. The value is a set in order to help store several implications of the form c1 => c2, and
	 * c1 => c3 : value is the set {c2, c3}.
	 */
	private HashMap<RuntimeClock, Set<RuntimeClock>> implications = new HashMap<RuntimeClock, Set<RuntimeClock>>();

	/* Equalities are registered by associating to each clock a "class number" such that all 
	 * equal clocks have the same number. This number is used as an index in the equalityClasses
	 * vector that gives the content of the class itself. For each clock, the map equalityClassMap
	 * gives the class of the clock. 
	 */
	private Vector<Set<RuntimeClock>> equalityClasses = new Vector<Set<RuntimeClock>>();
	private HashMap<RuntimeClock, Integer> equalityClassMap = new HashMap<RuntimeClock, Integer>();

	private int newClassIndex() {
		/* First try to reuse on old class. At the beginning, the vector is created empty and a new
		 * class is created by setting a empty set structure in the vector at the right place.
		 * Equality classes can be merged with each other, and this creates holes in the vector.
		 */
		for (int i = 0; i < equalityClasses.size(); i++) {
			if (equalityClasses.elementAt(i) == null) {
				equalityClasses.setElementAt(new HashSet<RuntimeClock>(), i);
				return i;
			}
		}
		/* No hole found in the vector.
		 * Enlarge the vector and return the index of the newly created element, while creating
		 * a new empty set that is set in the vector in the new place.
		 */
		int newIndex = equalityClasses.size();
		equalityClasses.setSize(newIndex + 1);
		equalityClasses.setElementAt(new HashSet<RuntimeClock>(), newIndex);
		return newIndex;
	}

	private boolean storeInClass(Integer classIndex, RuntimeClock c2) {
		equalityClassMap.put(c2, classIndex);
		/* Returns true if the clock was not present in the class, false otherwise. */
		return equalityClasses.elementAt(classIndex).add(c2);
	}

	/* Merges two classes, the one with the lowest index is kept and receives all clocks that 
	 * pertain to the other. The class with the highest index is deleted and this creates a hole
	 * in the vector equalityClasses.
	 */
	private void mergeClasses(Integer class1, Integer class2) {
		int targetClass, removedClass;
		if (class1.compareTo(class2) == 0) {
			return;
		}
		if (class1.compareTo(class2) < 0) {
			targetClass = class1;
			removedClass = class2;
		}
		else {
			targetClass = class2;
			removedClass = class1;
		}
		for (RuntimeClock clock : equalityClasses.elementAt(removedClass)) {
			storeInClass(targetClass, clock);
		}
		equalityClasses.elementAt(removedClass).clear();
		equalityClasses.set(removedClass, null); // free the slot.
	}

	private class ClockConjunction {
		public RuntimeClock clock1;
		public RuntimeClock clock2;
		public ClockConjunction(RuntimeClock clock1, RuntimeClock clock2) {
			this.clock1 = clock1;
			this.clock2 = clock2;
		}
	}
	
	private HashMap<RuntimeClock, HashSet<ClockConjunction>> clockConjunctions = new HashMap<RuntimeClock, HashSet<ClockDeathSolver.ClockConjunction>>();
	
	private HashMap<ClockConjunction, HashSet<RuntimeClock>> conjunctImplications = new HashMap<ClockDeathSolver.ClockConjunction, HashSet<RuntimeClock>>();

	/**
	 * Register an implication of the form (c1 AND c2) => c3.
	 * 
	 * @param clock1
	 * @param clock2
	 * @param clock3
	 */
	public void registerConjunctionImplication(RuntimeClock clock1, RuntimeClock clock2, RuntimeClock clock3) {
		ClockConjunction cj = findOrCreateConjunction(clock1, clock2);
		HashSet<RuntimeClock> right = conjunctImplications.get(cj);
		if (right == null) {
			right = new HashSet<RuntimeClock>();
			conjunctImplications.put(cj, right);
		}
		right.add(clock3);
		// propagate
		if (deadClocks.contains(clock1) && deadClocks.contains(clock2)) {
			registerDeadClock(clock3);
		}
	}
	
	private List<ClockConjunction> findConjunction(RuntimeClock ck) {
		List<ClockConjunction> res = new ArrayList<ClockDeathSolver.ClockConjunction>();
		Set<ClockConjunction> set = clockConjunctions.get(ck);
		if (set != null) {
			res.addAll(set);
		}
		return res;
	}
	
	private ClockConjunction findOrCreateConjunction(RuntimeClock clock1, RuntimeClock clock2) {
		Set<ClockConjunction> existing  = clockConjunctions.get(clock1);
		if (existing != null) {
			for (ClockConjunction elt : existing) {
				if (elt.clock2 == clock2) { // found a matching entry
					return elt;
				}
			}
		}
		ClockConjunction newConjunct = new ClockConjunction(clock1, clock2);
		storeConjunct(newConjunct);
		return newConjunct;
	}
	
	private void storeConjunct(ClockConjunction cj) {
		HashSet<ClockConjunction> existing  = clockConjunctions.get(cj.clock1);
		if (existing == null) {
			existing = new HashSet<ClockDeathSolver.ClockConjunction>();
			clockConjunctions.put(cj.clock1, existing);
		}
		existing.add(cj);
		existing = clockConjunctions.get(cj.clock2);
		if (existing == null) {
			existing = new HashSet<ClockDeathSolver.ClockConjunction>();
			clockConjunctions.put(cj.clock2, existing);
		}
		existing.add(cj);
	}

}
