/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import toools.io.ser.Serializer;

public class SerializedConstraintState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3406493293868578560L;

	public class SerializerData extends ArrayList<byte[]> {
		private static final long serialVersionUID = -4609988236874367746L;
		public SerializerData() { }
		public SerializerData(SerializerData copyFrom) {
			for (byte[] byteTab : copyFrom) {
				this.add( Arrays.copyOf(byteTab, byteTab.length) );
			}
		}
		public boolean equals(SerializerData other) {
			if (this.size() != other.size())
				return false;
			for (int i = 0; i < this.size(); i++) {
				if ( ! Arrays.equals(this.get(i), other.get(i)) )
					return false;
			}
			return true;
		}
	}
	
	private SerializerData data = null;
	
	public SerializedConstraintState() {
		this.data = new SerializerData();
	}
	
	public SerializedConstraintState(SerializedConstraintState copyFrom) {
		this.data = new SerializerData(copyFrom.data);
	}
	
	public boolean equals(SerializedConstraintState other) {
		return this.data.equals(other.data);
	}
	
	public <T extends Object> int dump(T object) {
		data.add(Serializer.getDefaultSerializer().toBytes(object));
		return data.size() - 1;		
	}
	
	public <T extends Object> int add(SerializedConstraintState serializedObject) {
		data.addAll(serializedObject.data);
		return data.size() - 1;		
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Object> T restore(int index) {
		return (T) Serializer.getDefaultSerializer().fromBytes(data.get(index));
	}
}
