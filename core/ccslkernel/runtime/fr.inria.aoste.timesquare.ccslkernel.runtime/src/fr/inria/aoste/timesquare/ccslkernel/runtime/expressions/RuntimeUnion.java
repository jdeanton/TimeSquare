/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeUnion extends AbstractRuntimeExpression {

	private RuntimeClock leftClock;
	private RuntimeClock rightClock;

	public RuntimeUnion(RuntimeClock implicitClock, RuntimeClock leftClock, RuntimeClock rightClock) {
		super(implicitClock);
		this.leftClock = leftClock;
		this.rightClock = rightClock;
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if (leftClock instanceof ICCSLConstraint)
			((ICCSLConstraint) leftClock).semantic(helper);
		if (rightClock instanceof ICCSLConstraint)
			((ICCSLConstraint) rightClock).semantic(helper);
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if ( isDead() ) {
			helper.inhibitClock(getExpressionClock());
		}
		else {
			helper.semanticBDDAnd(helper.createEqual(getExpressionClock(),
					helper.createUnion(getLeftClock(), getRightClock())));
		}
		helper.registerClockUse(new RuntimeClock[] { getExpressionClock(), getLeftClock(), getRightClock() });
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		super.update(helper);
		if (leftClock instanceof ICCSLConstraint)
			((ICCSLConstraint) leftClock).update(helper);
		if (rightClock instanceof ICCSLConstraint)
			((ICCSLConstraint) rightClock).update(helper);
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (leftClock instanceof ICCSLConstraint)
			((ICCSLConstraint) leftClock).deathSemantic(helper);
		if (rightClock instanceof ICCSLConstraint)
			((ICCSLConstraint) rightClock).deathSemantic(helper);
		helper.registerDeathConjunctionImplies(leftClock, rightClock, getExpressionClock());
	}
	
	public RuntimeClock getLeftClock() {
		return leftClock;
	}
	
	public RuntimeClock getRightClock() {
		return rightClock;
	}
	
}
