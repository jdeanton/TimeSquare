/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.helpers;

import net.sf.javabdd.BuDDyFactory;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class BDDHelper {
	
	private BuDDyFactory factory;

	public BDDHelper() {
		this.factory = createFactory();
	}
	
	public BDDHelper(BuDDyFactory factory) {
		super();
		this.factory = factory;
	}
	
	public BuDDyFactory getFactory() {
		return factory;
	}
	
	public BuDDyBDD createEqual(int leftVarNum, int rightVarNum){
		BuDDyBDD left = factory.ithVar(leftVarNum);
		BuDDyBDD right = factory.ithVar(rightVarNum);
		BuDDyBDD res = createEqual(left, right);
		left.free();
		right.free();
		return res;
	}
	
	/**
	 * Return a BuDDyBDD to represent the equality between the two arguments BuDDyBDD. The arguments are not "consumed", if they are
	 * not useful anymore, they have to be explicitly freed by the caller.
	 *  
	 * @param left
	 * @param right
	 * @return
	 */
	public BuDDyBDD createEqual(BuDDyBDD left, BuDDyBDD right){	
		
		BuDDyBDD notLeft = left.not();
		BuDDyBDD notRight = right.not();
		
		notRight.andWith(notLeft);		   // notRight = notRight AND notLeft, consumes notLeft.
		BuDDyBDD rightLeft = right.and(left);   // rightLeft = right AND left
		rightLeft.orWith(notRight);	       // rightLeft = rightLeft OR notRight, consumes notRight.
		return rightLeft;
	}
	
	public BuDDyBDD createExclusion(int leftVarNum, int rightVarNum){
		BuDDyBDD left = factory.ithVar(leftVarNum);
		BuDDyBDD right = factory.ithVar(rightVarNum);
		BuDDyBDD res = createExclusion(left, right);
		left.free();
		right.free();
		return res;
	}
	
	public BuDDyBDD createExclusion(BuDDyBDD left, BuDDyBDD right){
		
		BuDDyBDD notLeft = left.not();
		BuDDyBDD notRight = right.not();
		
		BuDDyBDD res = notRight.or(notLeft);
		notLeft.free();
		notRight.free();
		return res;
	}
	
	public BuDDyBDD createImplication(int leftVarNum, int rightVarNum){
		BuDDyBDD left = factory.ithVar(leftVarNum);
		BuDDyBDD right = factory.ithVar(rightVarNum);
		BuDDyBDD res = createImplication(left, right);
		left.free();
		right.free();
		return res;
	}
	
    public BuDDyBDD createImplication(BuDDyBDD left, BuDDyBDD right){
		BuDDyBDD res = left.imp(right);
		return res;
	}
	
	public BuDDyBDD createUnion(int leftVarNum, int rightVarNum){
		BuDDyBDD left = factory.ithVar(leftVarNum);
		BuDDyBDD right = factory.ithVar(rightVarNum);
		BuDDyBDD res = createUnion(left, right);
		left.free();
		right.free();
		return res;
	}
	
    public BuDDyBDD createUnion(BuDDyBDD left, BuDDyBDD right){
		BuDDyBDD res = left.or(right);
		return res;
	}
    
	public BuDDyBDD createInter(int leftVarNum, int rightVarNum){
		BuDDyBDD left = factory.ithVar(leftVarNum);
		BuDDyBDD right = factory.ithVar(rightVarNum);
		BuDDyBDD res = createInter(left, right);
		left.free();
		right.free();
		return res;
	}
	
    public BuDDyBDD createInter(BuDDyBDD left, BuDDyBDD right){
		BuDDyBDD res = left.and(right);
		return res;
	}
    
    // can be "buddy" or "cudd", or null
 	public static String defaultBDDLibName = "cudd";
	// Apparently javaBDD only allows to create one single factory (at least for the two backends Cudd and Buddy)
	// it throws an exception at the second call of BuDDyFactory.init().
	static private BuDDyFactory BuddyFactoryInstance = null;
	static public synchronized BuDDyFactory createFactory() {
		if (BuddyFactoryInstance != null && BuddyFactoryInstance.isInitialized()) {
			return BuddyFactoryInstance;
		}
		BuddyFactoryInstance = initBuddyFactory(defaultBDDLibName);
		return BuddyFactoryInstance;
	}
	static public synchronized BuDDyFactory createFactory(String bddLibName) {
		if (BuddyFactoryInstance != null && BuddyFactoryInstance.isInitialized()) {
			return BuddyFactoryInstance;
		}
		BuddyFactoryInstance = initBuddyFactory(bddLibName);
		return BuddyFactoryInstance;
	}
//	public static BuDDyFactory getFactory() {
//		return createFactory();
//	}
	static private synchronized BuDDyFactory initBuddyFactory(String bddLib) {
		BuDDyFactory bddf = null;
		int node = 200000;
		int cache = 10000;
		if (bddLib != null) {
			bddf = BuDDyFactory.init(node, cache);
		} else {
			bddf = BuDDyFactory.init(node, cache);
		}
		return bddf;
	}
	static public synchronized void terminateBDDUsage() {
		if (BuddyFactoryInstance != null) {
			BuddyFactoryInstance.done();
		}
		BuddyFactoryInstance = null;
	}

	static private int newBDDVariableNumber(BuDDyFactory factory) {
		int res = factory.varNum();
		factory.setVarNum(res + 1);
		return res;
	}
	static public int newBDDVariableNumber() {
		return newBDDVariableNumber(createFactory());
	}

}
