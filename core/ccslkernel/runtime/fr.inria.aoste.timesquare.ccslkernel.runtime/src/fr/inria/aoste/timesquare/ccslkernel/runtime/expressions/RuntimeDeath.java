/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;

public class RuntimeDeath extends AbstractRuntimeExpression {

	public RuntimeDeath(RuntimeClock iClock) {
		super(iClock);
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		helper.inhibitClock(getExpressionClock());
		helper.registerClockUse(getExpressionClock());
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		helper.forceDeath(getExpressionClock());
	}
	
	@Override
	public boolean isTerminated() {
		return true;
	}
	
	@Override
	public String toString() {
		String res = "[" + getExpressionClock().getName() + "]"
					+ (state != State.INIT && state != State.DEAD ? "*" : "") + "Death()";
		if (state == State.DEAD) {
			res += "/D";
		}
		return res;
	}
	
}
