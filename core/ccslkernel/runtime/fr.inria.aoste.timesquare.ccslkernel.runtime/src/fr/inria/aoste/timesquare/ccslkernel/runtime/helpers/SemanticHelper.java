/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.helpers;

import java.util.Collection;
import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.CCSLStepExecutionEngine;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.ClockDeathSolver;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class SemanticHelper extends AbstractSemanticHelper {

	public SemanticHelper(CCSLStepExecutionEngine stepEngine, BDDHelper bddHelper) {
		super(bddHelper);
		this.stepEngine = stepEngine;
	}
	
	private CCSLStepExecutionEngine stepEngine;

	/**
	 * Returns the BuDDyBDD variable used to record the semantic of the relation as
	 * an assertion
	 * 
	 * @param relation
	 * @return
	 */
	@Override
	public BuDDyBDD getAssertionVariable(AbstractRuntimeRelation relation) {
		return getBuddyFactory().ithVar(relation.getAssertionVariable());
	}
	
	/**
	 * Adds the BuDDyBDD argument to the simulation semantic using a conjunction. The
	 * BuDDyBDD given as argument is freed after the operation.
	 * 
	 * @param buDDyBDD
	 */
	@Override
	public void semanticBDDAnd(BuDDyBDD buDDyBDD) {
		getSemanticBdd().andWith(buDDyBDD);
	}
	
	/**
	 * This function registers that the relation given as argument is an
	 * assertion.
	 * 
	 * @param relation
	 */
	@Override
	public void registerAssertion(AbstractRuntimeRelation relation) {
		stepEngine.addAssertionVariable(relation.getAssertionVariable());
	}

	@Override
	public Set<RuntimeClock> getUsedClocks() {
		return stepEngine.getUsedClocks();
	}
	
	@Override
	public void registerClockUse(RuntimeClock clock) {
		getUsedClocks().add(clock);
	}

	private Set<RuntimeClock> getStartQueue() {
		return stepEngine.getStartQueue();
	}

	@Override
	public void registerClockToStart(RuntimeClock clock) {
		stepEngine.registerClockToStart(clock);
	}
	
	@Override
	public void unregisterClockToStart(RuntimeClock clock) {
		getStartQueue().remove(clock);
	}

	@Override
	public void forceDeath(RuntimeClock clock) {
		getClockDeathSolver().registerDeadClock(clock);
	}
	
	@Override
	public void registerDeathEquality(RuntimeClock clock1, RuntimeClock clock2) {
		getClockDeathSolver().registerEquality(clock1, clock2);
	}

	@Override
	public void registerDeathImplication(RuntimeClock clock1, RuntimeClock clock2) {
		getClockDeathSolver().registerImplication(clock1, clock2);
	}
	
	@Override
	public void registerDeathConjunctionImplies(RuntimeClock left1, RuntimeClock left2, RuntimeClock right) {
		getClockDeathSolver().registerConjunctionImplication(left1, left2, right);
	}
	
	private ClockDeathSolver getClockDeathSolver() {
		return stepEngine.getDeathSolver();
	}
	
	private Set<RuntimeClock> getDeadClocks() {
		return stepEngine.getDeadClocks();
	}

	public void setDeadClocks(Collection<? extends RuntimeClock> deadClocks) {
		getDeadClocks().addAll(deadClocks);
	}
	
	@Override
	public void registerDeadClock(RuntimeClock clock) {
		getDeadClocks().add(clock);
	}

	@Override
	public void unregisterDeadClock(RuntimeClock clock) {
		getDeadClocks().remove(clock);
	}

	@Override
	public boolean isSemanticDone(ICCSLConstraint ct) {
		return false;
	}

	@Override
	public void registerSemanticDone(ICCSLConstraint ct) {		
	}

	@Override
	public AbstractUpdateHelper getUpdateHelper() {
		return null;
	}

	private BuDDyBDD getSemanticBdd() {
		return stepEngine.getSemanticBdd();
	}

}
