/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeSequence;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeFilterBy extends AbstractRuntimeExpression {

	private RuntimeClock filteredClock;
	private RuntimeSequence<Integer> filteringSequence;
	private int currentDelay;
	
	public RuntimeFilterBy(RuntimeClock iClock, RuntimeClock clock, RuntimeSequence<Integer> sequence) {
		super(iClock);
		this.filteredClock = clock;
		this.filteringSequence = sequence;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (! canCallStart())
			return;
		super.start(helper);
		if (filteredClock instanceof ICCSLConstraint) {
			((ICCSLConstraint) filteredClock).start(helper);
		}
		if (filteringSequence.isEmpty()) {
			terminate(helper.getUpdateHelper());
			return;
		}
		filteringSequence.finiteIndex=0;
		filteringSequence.infiniteIndex = 0;
		currentDelay = filteringSequence.popHead();
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if (filteredClock instanceof ICCSLConstraint) {
			((ICCSLConstraint) filteredClock).semantic(helper);
		}
		if (currentDelay == 1) {
			helper.semanticBDDAnd(helper.createEqual(filteredClock, getExpressionClock()));
		}
		else {
			helper.inhibitClock(getExpressionClock());
		}
		helper.registerClockUse(new RuntimeClock[] { filteredClock, getExpressionClock() });
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (filteredClock instanceof ICCSLConstraint) {
			((ICCSLConstraint) filteredClock).deathSemantic(helper);
		}
		helper.registerDeathImplication(filteredClock, getExpressionClock());
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if (filteredClock instanceof ICCSLConstraint) {
			((ICCSLConstraint) filteredClock).update(helper);
		}
		if (helper.clockHasFired(filteredClock)) {
			if (currentDelay == 1) {
				// Get the next head of the sequence and start a new wait.
				if (filteringSequence.isEmpty()) {
					terminate(helper);
				}
				else {
					currentDelay = filteringSequence.popHead();
				}
			}
			else {
				currentDelay -= 1;
			}
		}
	}
	
}
