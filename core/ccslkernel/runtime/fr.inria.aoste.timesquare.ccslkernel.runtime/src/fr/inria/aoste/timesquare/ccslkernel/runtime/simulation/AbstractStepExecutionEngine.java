/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.simulation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.NoBooleanSolution;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;
import net.sf.javabdd.BuDDyFactory;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public abstract class AbstractStepExecutionEngine {

	protected AbstractCCSLSimulationEngine simulationEngine;
	protected AbstractSemanticHelper semanticHelper;
	protected AbstractUpdateHelper updateHelper;
	
	public BuDDyBDD semanticBdd;
	public BuDDyBDD semanticBdd_beforeClockForcing;
	
	protected Set<RuntimeClock> usedClocks;
	
	protected ClockDeathSolver deathSolver;
	
	protected Set<RuntimeClock> enabledClocks;
	protected Set<RuntimeClock> firedClocks;

	protected Set<RuntimeClock> newDeadClocks;
	protected Set<RuntimeClock> resurrectedClocks;

	protected Set<RuntimeClock> startQueue;
	
	protected Set<Integer> assertionVariableNumbers;

	public AbstractStepExecutionEngine() {
		init();
	}
	
	public AbstractStepExecutionEngine(AbstractCCSLSimulationEngine solverEngine) {
		this.simulationEngine = solverEngine;
		init();
	}
	
	private void init() {
		this.usedClocks = new HashSet<RuntimeClock>();
		this.deathSolver = new ClockDeathSolver();
		this.enabledClocks = new HashSet<RuntimeClock>();
		this.firedClocks = new HashSet<RuntimeClock>();
		this.newDeadClocks = new HashSet<RuntimeClock>();
		this.resurrectedClocks = new HashSet<RuntimeClock>();
		this.startQueue = new HashSet<RuntimeClock>();
		this.assertionVariableNumbers = new HashSet<Integer>();
	}
	
	/**
	 * Must be called explicitly from the subclasses at a time where all needed objects are fully
	 * build.
	 * Calls {@link #createSemanticHelper()} and {@link #createUpdateHelper()}.
	 */
	protected void initHelpers() {
		this.semanticHelper = createSemanticHelper();
		this.updateHelper = createUpdateHelper();	
	}
	
	/**
	 * Used to delegate to subclasses the creation of the specific helper object.
	 * @return An object used as the semantic helper in the step execution
	 */
	protected abstract AbstractSemanticHelper createSemanticHelper();
	
	/**
	 * Used to delegate to subclasses the creation of the specific helper object.
	 * @return An object used as the update helper in the step execution
	 */
	protected abstract AbstractUpdateHelper createUpdateHelper();
	
	public void executeStep() throws SimulationException
	{
		clearStepData();
		stepPreHook();
		//put res in allPossibleLogicalSteps (and all ClockTraceSet in allClockTracestateSets)
		//getPossibleLogicalSteps(); //also returns the possible logical steps
		computePossibleClockStates();
		allClockTraceStateSet = getAllBDDSolutions();
		//ask the choice to the simulation policy
		if (allClockTraceStateSet.size() > 0){
			int chosenLogicalStep = proposeLogicalStepByIndex();
			applyLogicalStepByIndex(chosenLogicalStep);
			
		}else{
			//we are in deadlock here
			fired = new ClockTraceStateSet(0);
			enabled = new ClockTraceStateSet(0);
		}
		stepPostHook();
	}
	
	public void clearFiredClock(){
		firedClocks.clear();
	}
	
	public void clearStepData() {
		usedClocks.clear();
		enabledClocks.clear();
		firedClocks.clear();
		newDeadClocks.clear();
		resurrectedClocks.clear();
		startQueue.clear();
		deathSolver.clear();
		assertionVariableNumbers.clear();
		if (semanticBdd != null) {
			semanticBdd.free();
		}
	}
	
	/**
	 * Called by {@link #executeStep()} before anything happens in the step execution. All internal
	 * data of the step execution are put in their default value or cleared (for collections).
	 */
	protected abstract void stepPreHook();
	
	/**
	 * Called by {@link #executeStep()} after completion of the simulation step.
	 */
	protected abstract void stepPostHook();
	
	public ArrayList<ClockTraceStateSet> allClockTraceStateSet = new ArrayList<ClockTraceStateSet>();

    public void computePossibleClockStates() throws SimulationException{
    	constructCurrentStepBDD();
    	inhibitUnusedClocks();
		return;
    }

	protected abstract void constructCurrentStepBDD() throws SimulationException;
    
	protected void inhibitUnusedClocks() {
		// The clock not used does not ticks (so that it restrict the uninteresting solutions in the BuDDyBDD)!
		List<RuntimeClock> allClocks = getAllDiscreteClocks();
		for (RuntimeClock clock : allClocks) {
			if ( ! getUsedClocks().contains(clock)){
				semanticHelper.inhibitClock(clock);
			}
		}
	}

	public abstract List<RuntimeClock> getAllDiscreteClocks();
	
    public abstract ArrayList<ClockTraceStateSet> getAllBDDSolutions() throws NoBooleanSolution;    
    
    public int proposeLogicalStepByIndex() {
    	//make some choice in the possibleLogicalStep; here the logical step at the index 0 is chosen
    	SimulationPolicyBase policy = getSimulationPolicy();
		if (policy == null) {
			ArrayList<ClockTraceStateSet> satSol = satOneSolution( getBddVarToIndexInSolution() );
			allClockTraceStateSet = new ArrayList<ClockTraceStateSet>();
			allClockTraceStateSet.add(satSol.get(0)); //a solution chosen by the BuDDyBDD we do not know how :)
			return 0;
		}
		else {
    	    return chooseBooleanSolution(allClockTraceStateSet);
		}
    }

    public abstract Map<Integer, Integer> getBddVarToIndexInSolution();
    
    public int getIndexInSolution(int bddVar) {
    	return getBddVarToIndexInSolution().get(bddVar);
    }
    
    protected abstract BuDDyFactory getBuddyFactory();
    
	protected ArrayList<ClockTraceStateSet> satOneSolution(Map<Integer, Integer> bddVarToIndexInSolution) {
		//construct the var set 
		BuDDyBDD varSet = getBuddyFactory().one();

		for (Iterator<Integer> iterator = bddVarToIndexInSolution.keySet().iterator();
				iterator.hasNext(); ) {
			Integer current = (Integer) iterator.next();
			varSet.andWith(getBuddyFactory().ithVar(current));
		}

		//restrict the BuDDyBDD to a unique satisfying solution
		BuDDyBDD resBDD = semanticBdd.satOne();
		Iterator<?> it = resBDD.iterator(varSet);

		//construct the result set
		int allClockSize = bddVarToIndexInSolution.size();
		ClockTraceStateSet tempF = new ClockTraceStateSet(allClockSize);
		ClockTraceStateSet tempE = new ClockTraceStateSet(allClockSize);
		if(it.hasNext()){
			BuDDyBDD v = (BuDDyBDD)it.next();
			for (Iterator<Integer> iterator = bddVarToIndexInSolution.keySet().iterator(); 
					iterator.hasNext(); ) {

				int bddVarNumber = (Integer) iterator.next();
				int index = getIndexInSolution(bddVarNumber);

				if ( ! v.and(getBuddyFactory().ithVar(bddVarNumber)).isZero()) {
					tempF.set(index, ClockTraceState.T);
					tempE.set(index, ClockTraceState.T);
				}
				else {
					tempF.set(index, ClockTraceState.F);
					tempE.set(index, ClockTraceState.F);
				}
			}	
		}
		varSet.free();
		resBDD.free();
		
		ArrayList<ClockTraceStateSet> res = new ArrayList<ClockTraceStateSet>();
		res.add(tempE);
		res.add(tempF);
		return res;
	}

	protected int chooseBooleanSolution(ArrayList<ClockTraceStateSet> allSolutionsF) {
		SimulationPolicyBase policy = getSimulationPolicy();
		int res = policy.getPolicySpecificSolution(allSolutionsF);
		return res;
	}

	protected ClockTraceStateSet enabled;
	protected ClockTraceStateSet fired;

    public void applyLogicalStepByIndex(int indexOfStepToApply) throws SimulationException {
    	fired = allClockTraceStateSet.get(indexOfStepToApply);
		enabled = allClockTraceStateSet.get(indexOfStepToApply);
		
		rewriteSemanticsAndPropagateDeath();
		
//		dealWithBlockTransition();

    }
	
    /**
     * @WARNING, here we should look if a clock trigger of a block transition 
     * is fired and if so, termitate and start corresponding blocks
     */
   abstract public void dealWithBlockTransition(); 

	/**
     * Returns the set of dead clocks. One possible implementation is to rely on
     * the {@link AbstractCCSLSimulationEngine#getDeadClocks()} function. The clocks returned by this
     * call are the ones that have been recognized as dead during all the previous step of the
     * simulation, except the current one.
     * @return
     * @see #addDeadClock(RuntimeClock)
     */
	public abstract Set<RuntimeClock> getDeadClocks();

	/**
	 * Add a clock to the set of dead clocks returned by {@link #getDeadClocks()}.
	 * @param clock
	 */
	public abstract void addDeadClock(RuntimeClock clock);
	
	/**
	 * Repetitively calls {@link #addDeadClock(RuntimeClock)} for a collection of clocks.
	 * @param clocks
	 */
	public void addDeadClocks(Collection<RuntimeClock> clocks) {
		for (RuntimeClock clock : clocks)
			addDeadClock(clock);
	}
		
	public void registerNewDeadClock(RuntimeClock clock) {
		newDeadClocks.add(clock);
	}
	
	protected void rewriteSemanticsAndPropagateDeath()
			throws SimulationException {
		// local copy of the set of currently dead clocks
    	Set<RuntimeClock> deadClocks = new HashSet<RuntimeClock>(getDeadClocks());
		boolean deadClocksChanged = false;
		do {
			deadClocksChanged = false;
			rewriteExpressions();
			
			deadClocksChanged = propagateClockTermination();
			for (RuntimeClock clock : startQueue) {
				if (clock instanceof ICCSLConstraint) {
					((ICCSLConstraint)clock).start(semanticHelper);
				}
			}
			startQueue.clear();
			
			// Compute the set of resurrected clocks : ie clocks that were once dead and are
			// now not dead
			for (RuntimeClock clock : deadClocks) {
				if ( ! clock.isDead()) {
					resurrectedClocks.add(clock);
				}
			}
			for (RuntimeClock clock : newDeadClocks) {
				if ( ! clock.isDead()) {
					resurrectedClocks.add(clock);
					for (RuntimeClock other : deathSolver.getEqualClocks(clock)) {
						resurrectedClocks.add(other);
					}
				}
			}
			newDeadClocks.removeAll(resurrectedClocks);
			deadClocksChanged = deadClocks.removeAll(resurrectedClocks) || deadClocksChanged;
			deadClocksChanged = deadClocks.addAll(newDeadClocks) || deadClocksChanged;
			deathSolver.clear();
			newDeadClocks.clear();
			resurrectedClocks.clear();
		} while (deadClocksChanged);
		// commit the result to the actual set of dead clocks.
		addDeadClocks(deadClocks);
	}

	/**
	 * Expressions and relations rewriting phase. This must call the {@link ICCSLConstraint#update(AbstractUpdateHelper)}
	 * function on all the relations and expressions. These functions can call the
	 * {@link AbstractUpdateHelper#registerClockToStart(RuntimeClock)}
	 * and 
	 * {@link AbstractUpdateHelper#registerNewDeadClock(RuntimeClock)} functions to schedule clocks and
	 * their associated expressions to be either restarted or killed.
	 * 
	 * @throws SimulationException
	 */
	protected abstract void rewriteExpressions() throws SimulationException;
	
	protected boolean propagateClockTermination() throws SimulationException {
		boolean newDeadClocksFound = false;
		for (RuntimeClock clock : getDeadClocks()) {
			if (clock.isDead()) {
				semanticHelper.forceDeath(clock);
			}
			else {
				resurrectedClocks.add(clock);
			}
		}
		for (RuntimeClock clock : getDeadClocks()) {
			if (clock.isDead()) {
				semanticHelper.forceDeath(clock);
			}
			else {
				resurrectedClocks.add(clock);
			}
		}

		buildDeathExpressions();
		newDeadClocksFound = registerDeadClocks(deathSolver.getDeadClocks());
		/* 
		 * If some clocks are present in the resurrectedClocks set, then all clocks that are
		 * registered as "equal" w.r.t to the death status must be in the resurrectedClocks set.
		 */
		for (int i= 0; i < resurrectedClocks.size(); i++) {
			RuntimeClock clock = (RuntimeClock) resurrectedClocks.toArray()[i];
			for (RuntimeClock other : deathSolver.getEqualClocks(clock)) {
				resurrectedClocks.add(other);
			}
		}
		/*
		 * remove resurrected clocks before to propagate their non death  !!
		 */
		newDeadClocks.removeAll(resurrectedClocks);
		
		terminateDeadExpressions();

		return newDeadClocksFound;
	}

	/**
	 * Builds and solve the death equations. Implementation of this function can use the {@link #deathSolver}
	 * object if they need to.
	 * Usually, this function should call the {@link ICCSLConstraint#deathSemantic(AbstractSemanticHelper)}
	 * functions of all the expressions and relations.
	 * @throws SimulationException
	 */
	protected abstract void buildDeathExpressions() throws SimulationException;

	protected boolean registerDeadClocks(List<RuntimeClock> list) throws SimulationException {
		boolean added = false, res = false;
		for (RuntimeClock clock : list) {
			added = newDeadClocks.add(clock);
			if (added && clock instanceof ICCSLConstraint) {
				clock.terminate(updateHelper);
			}
			clock.setDead(true);
			res = res || added;
		}
		return res;
	}

	private void terminateDeadExpressions() throws SimulationException {
		for (RuntimeClock clock : newDeadClocks) {
			if ( ! clock.isDead() ) {
				clock.terminate(updateHelper);
			}
		}
	}
	
	public ClockTraceState getClockEnabledState(RuntimeClock clock)
	{
		if (getUsedClocks().contains(clock)) {
			return enabled.get(getIndexInSolution(clock.bddVariableNumber));
		}
		return ClockTraceState.X;
	}

	public ClockTraceState getClockFiredState(RuntimeClock clock)
	{
		if (getUsedClocks().contains(clock)) {
			return fired.get(getIndexInSolution(clock.bddVariableNumber));
		}
		return ClockTraceState.F;
	}

	protected void registerClockFiring(RuntimeClock clock) {
		if (firedClocks.add(clock)) {
			clock.tickCount += 1;
		}
	}
	
	public boolean clockHasFired(RuntimeClock clock) {
		return firedClocks.contains(clock);
	}

	public Set<RuntimeClock> getEnabledClocks() {
		return new HashSet<RuntimeClock>(enabledClocks);
	}
	
	public Set<RuntimeClock> getFiredClocks() {
		return new HashSet<RuntimeClock>(firedClocks);
	}

	public Set<RuntimeClock> getUsedClocks() {
		return usedClocks;
	}
	
    public BuDDyBDD getSemanticBdd() {
		return semanticBdd;
	}
    
    public void setSemanticBdd(BuDDyBDD semanticBdd) {
		this.semanticBdd = semanticBdd;
	}
    
	public ClockDeathSolver getClockDeathSolver() {
		return getDeathSolver();
	}

    public ClockDeathSolver getDeathSolver() {
		return deathSolver;
	}
    
    public AbstractSemanticHelper getSemanticHelper() {
		return semanticHelper;
	}
    
    public AbstractUpdateHelper getUpdateHelper() {
		return updateHelper;
	}
    
	public Set<RuntimeClock> getStartQueue() {
		return startQueue;
	}

	public void registerClockToStart(RuntimeClock clock) {
		startQueue.add(clock);
	}
	
    public abstract SimulationPolicyBase getSimulationPolicy();

	public Set<Integer> getAssertionVariables() {
		return assertionVariableNumbers;
	}

    public void addAssertionVariable(int varNum) {
    	getAssertionVariables().add(varNum);
    }
    
	public boolean isAssertionViolated(AbstractRuntimeRelation relation) {
		if ( ! relation.isAssertion()) {
			return false;
		}
		Integer index = simulationEngine.getIndexInSolution(relation);
		return ( index == null ? false : (fired.get(index) == ClockTraceState.F) );
	}

	
}
