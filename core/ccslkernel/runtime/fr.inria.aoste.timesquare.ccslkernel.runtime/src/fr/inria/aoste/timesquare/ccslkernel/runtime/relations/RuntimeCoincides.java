/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.relations;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

/**
 * TODO: + if we want to use block transitions, then we have to put CCSLKernelSolver::clockEqualityOptimize 
 * to false otherwise the equalities are grouped into equivalence classes.
 *       + to allow the optimization, the relation should have a start and a terminate moethod, in this case the clock 
 *       could be added and removed dynamically to the register
 * 		 + we should take care adding/removing/updating correctly the boolean variables in the BuDDyBDD
 * 
 * @author jdeanton
 *
 */

public class RuntimeCoincides extends AbstractRuntimeBinaryRelation {

	
	
	public RuntimeCoincides(RuntimeClock clock1, RuntimeClock clock2) {
		super(clock1, clock2);
	}

	@Override
	protected ICCSLConstraint[] getConstraints() {
		return new ICCSLConstraint[0];
	}
	
	@Override
	public List<RuntimeClock> getDiscreteClocks() {
		return Collections.emptyList();
	}

	@Override
	public Collection<? extends AbstractRuntimeRelation> getAllAssertions() {
		return Collections.emptyList();
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic() )
			return;
		super.semantic(helper);
		if (isAssertion())
			assertionSemantic(helper);
		else {
			helper.registerClockEquality(leftClock, rightClock);
			helper.registerClockUse(leftClock);
			helper.registerClockUse(rightClock);
		}
	}
	
	@Override
	public void assertionSemantic(AbstractSemanticHelper helper) {
		BuDDyBDD semantic = helper.createEqual(leftClock, rightClock);
		helper.assertionSemantic(this, semantic);
		helper.registerClockUse(leftClock);
		helper.registerClockUse(rightClock);
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		helper.registerDeathEquality(leftClock, rightClock);
	}
	
}
