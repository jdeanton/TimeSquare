/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.ValueProvider;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeWait extends AbstractRuntimeExpression {

	public RuntimeWait(RuntimeClock iClock, RuntimeClock waitClock, ValueProvider<Integer> waitCount) {
		super(iClock);
		this.clock = waitClock;
		this.delayProvider = waitCount;
	}

	public RuntimeWait(RuntimeClock iClock, RuntimeClock waitClock, Integer waitCount) {
		super(iClock);
		this.clock = waitClock;
		this.initialDelay = waitCount;
		this.delayProvider = null;
	}

	private RuntimeClock clock;
	private int initialDelay;
	private ValueProvider<Integer> delayProvider = null;
	private int delay = -1;

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (!canCallStart())
			return;
		super.start(helper);
		if (delayProvider != null) {
			delay = delayProvider.getValue();
		}
		else {
			delay = initialDelay;
		}
	}

	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if (clock instanceof ICCSLConstraint)
			((ICCSLConstraint) clock).semantic(helper);
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if ( isDead()) {
			helper.inhibitClock( getExpressionClock() );
		}
		else if (delay == 1) {
			helper.semanticBDDAnd(helper.createEqual(getExpressionClock(), clock));
		}
		else {
			helper.inhibitClock(getExpressionClock());
		}
		helper.registerClockUse(new RuntimeClock[] { getExpressionClock(), clock });
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (clock instanceof ICCSLConstraint)
			((ICCSLConstraint) clock).update(helper);
		if (!canCallUpdate())
			return;
		super.update(helper);
		if (helper.clockHasFired(clock)) {
			if (delay == 1) {
				terminate(helper);
			}
			else {
				if (delay > 1){ // if not it means someone uses an infinite wait (i.e. wait -1). We do not decrement to avoid artificial infinite state space
					delay -= 1;
				}
			}
		}
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (clock instanceof ICCSLConstraint)
			((ICCSLConstraint) clock).deathSemantic(helper);
		helper.registerDeathImplication(clock, getExpressionClock());
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
		currentState.dump(new Integer(delay));
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
		delay = (Integer) newState.restore(2);
	}

}
