/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;

public class RuntimeDiscretization extends AbstractRuntimeExpression {

	@SuppressWarnings("unused")
	private RuntimeClock denseClock;
	@SuppressWarnings("unused")
	private float factor;

	public RuntimeDiscretization(RuntimeClock iClock, RuntimeClock denseClock, float factor) {
		super(iClock);
		this.denseClock = denseClock;
		this.factor = factor;
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic() )
			return;
		super.semantic(helper);
		if (isDead())
			helper.inhibitClock(getExpressionClock());
		helper.registerClockUse(getExpressionClock());
	}
	
}
