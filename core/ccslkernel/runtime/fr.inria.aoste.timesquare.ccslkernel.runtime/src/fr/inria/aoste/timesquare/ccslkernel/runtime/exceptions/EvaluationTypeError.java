/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions;


public class EvaluationTypeError extends SimulationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1369799748777761807L;

	public EvaluationTypeError() {
		// TODO Auto-generated constructor stub
	}

	public EvaluationTypeError(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EvaluationTypeError(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public EvaluationTypeError(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
