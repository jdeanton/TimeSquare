/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.AbstractConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IExpressionClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.INamedElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.QualifiedName;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;

public abstract class AbstractRuntimeExpression extends AbstractConstraint 
			implements ICCSLConstraint, IRuntimeContainer, INamedElement {

	protected RuntimeClock expressionClock;
	private String name;
	private QualifiedName qualifiedName;
	private IRuntimeContainer parent;
	
	public AbstractRuntimeExpression() {
		this.state = State.INIT;
	}
	
	public AbstractRuntimeExpression(RuntimeClock iClock) {
		this.expressionClock = iClock;
		if (iClock instanceof IExpressionClock)
			((IExpressionClock) iClock).setExpression(this);
		state = State.INIT;
	}
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (canCallStart())
			super.startTransition();
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if (canCallSemantic()) {
			if (isDead()) {
				helper.inhibitClock( getExpressionClock() );
				helper.registerClockUse( getExpressionClock() );
			}
			super.semanticTransition();
		}
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (canCallUpdate())
			super.updateTransition();
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		if (state == State.DEAD)
			helper.forceDeath( getExpressionClock() );
		super.deathSemantic();
	}
		
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		if (canCallTerminate())
			terminateTransition();
	}
	
	@Override
	public boolean isTerminated() {
		return isDead();
	}
	
	@Override
	public SerializedConstraintState dumpState() {		
		SerializedConstraintState currentState = new SerializedConstraintState();
		//warning the order is important !
		currentState.dump(wasBorn());
		currentState.dump(state.ordinal());
		return currentState;
	}
	
	@Override
	public void restoreState(SerializedConstraintState stateData) {
		born = (boolean) stateData.restore(0);
		state = State.values()[ (Integer) stateData.restore(1) ];
	}
	
	public RuntimeClock getExpressionClock() {
		return expressionClock;
	}
	
	public void setExpressionClock(RuntimeClock expressionClock) {
		this.expressionClock = expressionClock;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setQualifiedName(String qualifiedName) {
		this.qualifiedName = new QualifiedName(qualifiedName);
	}
	
	public void setQualifiedName(QualifiedName qualifiedName) {
		this.qualifiedName = qualifiedName;
	}
	
	public QualifiedName getQualifiedName() {
		if (qualifiedName != null) {
			return qualifiedName;
		}
		else if (parent != null && parent instanceof INamedElement) {
			QualifiedName res = new QualifiedName(((INamedElement) parent).getQualifiedName());
			res.append(getName());
			qualifiedName = res;
			return res;
		}
		else
			return new QualifiedName(getName());
	}
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}
	
	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null) {
			parent.addContent(this);
		}
	}

	private List<IRuntimeContainer> contents = new ArrayList<IRuntimeContainer>();
	
	private List<RuntimeClock> discreteClocks = new ArrayList<RuntimeClock>();
	private List<RuntimeClock> denseClocks = new ArrayList<RuntimeClock>();
	private List<AbstractRuntimeRelation> relations = new ArrayList<AbstractRuntimeRelation>();
	private List<AbstractRuntimeExpression> expressions = new ArrayList<AbstractRuntimeExpression>();
	
	@Override
	public List<IRuntimeContainer> getContents() {
		return contents;
	}
	
	@Override
	public void addContent(IRuntimeContainer element) {
		contents.add(element);
		if (element instanceof RuntimeClock) {
			if (((RuntimeClock) element).isDense()) {
				denseClocks.add((RuntimeClock) element);
			}
			else {
				discreteClocks.add((RuntimeClock) element);
			}
		}
		else if (element instanceof AbstractRuntimeRelation) {
			relations.add((AbstractRuntimeRelation) element);
		}
		else if (element instanceof AbstractRuntimeExpression) {
			expressions.add((AbstractRuntimeExpression) element);
		}
	}
	
	public List<RuntimeClock> getDiscreteClocks() {
		List<RuntimeClock> res = new ArrayList<>(discreteClocks);
		for (AbstractRuntimeRelation relation : relations) {
			res.addAll(relation.getDiscreteClocks());
		}
		for (AbstractRuntimeExpression expression : expressions) {
			res.addAll(expression.getDiscreteClocks());
		}
		return res;
	}
	
}
