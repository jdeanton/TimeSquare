/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.relations;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public abstract class AbstractRuntimeBinaryRelation extends AbstractRuntimeRelation {

	protected RuntimeClock leftClock;
	protected RuntimeClock rightClock;
	
	public AbstractRuntimeBinaryRelation(RuntimeClock leftClock, RuntimeClock rightClock) {
		this.leftClock = leftClock;
		this.rightClock = rightClock;
	}

	@Override
	protected ICCSLConstraint[] getConstraints() {
		return new ICCSLConstraint[0];
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		super.start(helper);
		if (leftClock instanceof ICCSLConstraint)
			((ICCSLConstraint) leftClock).start(helper);
		if (rightClock instanceof ICCSLConstraint)
			((ICCSLConstraint) rightClock).start(helper);
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		super.semantic(helper);
		if (leftClock instanceof ICCSLConstraint)
			((ICCSLConstraint) leftClock).semantic(helper);
		if (rightClock instanceof ICCSLConstraint)
			((ICCSLConstraint) rightClock).semantic(helper);
	}
	
	@Override
	public void assertionSemantic(AbstractSemanticHelper helper) {
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		super.update(helper);
		if (leftClock instanceof ICCSLConstraint)
			((ICCSLConstraint) leftClock).update(helper);
		if (rightClock instanceof ICCSLConstraint)
			((ICCSLConstraint) rightClock).update(helper);
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (leftClock instanceof ICCSLConstraint)
			((ICCSLConstraint) leftClock).deathSemantic(helper);
		if (rightClock instanceof ICCSLConstraint)
			((ICCSLConstraint) rightClock).deathSemantic(helper);
	}
	
	public RuntimeClock getLeftClock() {
		return leftClock;
	}

	public RuntimeClock getRightClock() {
		return rightClock;
	}

}
