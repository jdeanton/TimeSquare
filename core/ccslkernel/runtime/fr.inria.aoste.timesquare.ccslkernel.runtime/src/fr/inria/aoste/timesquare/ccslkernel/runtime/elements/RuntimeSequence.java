/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.elements;

import java.util.Arrays;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;


public class RuntimeSequence<T> extends RuntimeElement {

	public RuntimeSequence() {
		finiteIndex = 0;
		infiniteIndex = 0;
	}
	
	public RuntimeSequence(T[] finitePart, T[] infinitePart) {
		this.finitePart = finitePart;
		this.infinitePart = infinitePart;
		finiteIndex = 0;
		infiniteIndex = 0;
	}
	
	public RuntimeSequence(RuntimeSequence<T> original) {
		if (original.finitePart != null) {
			this.finitePart = Arrays.copyOf(original.finitePart, original.finitePart.length);
		}
		else {
			this.finitePart = null;
		}
		this.finiteIndex = original.finiteIndex;
		if (original.infinitePart != null) {
			this.infinitePart = Arrays.copyOf(original.infinitePart, original.infinitePart.length);
		}
		else {
			this.infinitePart = null;
		}
		this.infiniteIndex = original.infiniteIndex;
	}

	public T[] finitePart;
	public T[] infinitePart;
	
	public int finiteIndex;
	public int infiniteIndex;
	
	public void setFinitePart(T[] arg) {
		finitePart = arg;
		finiteIndex = 0;
	}
	
	public void setInfinitePart(T[] arg) {
		infinitePart = arg;
		infiniteIndex = 0;
	}
	
	public T popHead() {
		return popNext();
	}
	
	public RuntimeSequence<T> getTail() {
		RuntimeSequence<T> res = new RuntimeSequence<T>(this);
		res.popHead(); // pops the head, only modifies the indexes.
		return res;
	}
	
	public T getHead() {
		if (finitePart != null && finiteIndex < finitePart.length) {
			return finitePart[finiteIndex];
		}
		else if (infinitePart != null && infiniteIndex < infinitePart.length) {
			return infinitePart[infiniteIndex];
		}
		return null;
	}
	
	public T popNext() {
		if (finitePart != null && finiteIndex < finitePart.length) {
			T result = finitePart[finiteIndex];
			finiteIndex++;
			return result;
		}
		else if (infinitePart != null && infiniteIndex < infinitePart.length) {
			T result = infinitePart[infiniteIndex];
			infiniteIndex++;
			if (infiniteIndex == infinitePart.length) {
				infiniteIndex = 0;
			}
			return result;
		}
		return null;
	}
	
	public boolean hasNext() {
		return ( (finitePart != null && finiteIndex < finitePart.length)
				|| isInfinite() );
	}
	
	public boolean isFinite() {
		return (infinitePart == null || infinitePart.length == 0);
	}
	
	public boolean isInfinite() {
		return (infinitePart != null && infinitePart.length > 0);
	}
	
	public boolean isEmpty() {
		return ((finitePart == null || finitePart.length == 0)
				&& (infinitePart == null && infinitePart.length == 0));
	}
	
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = new SerializedConstraintState();
		currentState.dump(this.finiteIndex);
		currentState.dump(this.finitePart);
		currentState.dump(this.infiniteIndex);
		currentState.dump(this.infinitePart);
		return currentState;
	}

	public void restoreState(SerializedConstraintState newState) {
		this.finiteIndex = (Integer) newState.restore(0);
		this.finitePart = newState.restore(1);
		this.infiniteIndex = (Integer) newState.restore(2);
		this.infinitePart = newState.restore(3);
	}

	public boolean equals(RuntimeSequence<T> obj) {
		if (this.finiteIndex != obj.finiteIndex)
			return false;
		if (this.finitePart.length != obj.finitePart.length)
			return false;
		if ( ! Arrays.equals(this.finitePart, obj.finitePart))
			return false;
		if (this.infiniteIndex != obj.infiniteIndex)
			return false;
		if (this.infinitePart.length != obj.infinitePart.length)
			return false;
		if (! Arrays.equals(this.infinitePart, obj.infinitePart))
			return false;
		return true;
	}
}
