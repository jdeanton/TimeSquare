/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

/**
 * Interface that is implemented by all objects that implements CCSL kernel constraints.
 * 
 * @author nchleq
 */
public interface ICCSLConstraint {
	
	public void start(AbstractSemanticHelper helper) throws SimulationException;
	
	public void semantic(AbstractSemanticHelper helper) throws SimulationException;
	
	public void update(AbstractUpdateHelper helper) throws SimulationException;
	
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException;
	
	public void terminate(AbstractUpdateHelper helper) throws SimulationException;

	public boolean isTerminated();
	
	// the following is useful for state space exploration or to resume a simulation

	public SerializedConstraintState dumpState();

	public void restoreState(SerializedConstraintState state);
	
}
