/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeStrictSampling extends AbstractRuntimeExpression {

	private enum SamplingState {
		NORMAL, WAITSAMPLING
	};

	private SamplingState localState;
	private RuntimeClock sampledClock;
	private RuntimeClock samplingClock;

	public RuntimeStrictSampling(RuntimeClock iClock, RuntimeClock sampledClock, RuntimeClock samplingClock) {
		super(iClock);
		this.sampledClock = sampledClock;
		this.samplingClock = samplingClock;
		this.localState = SamplingState.NORMAL;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart() )
			return;
		super.start(helper);
		localState = SamplingState.NORMAL;
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if (sampledClock instanceof ICCSLConstraint)
			((ICCSLConstraint) sampledClock).semantic(helper);
		if (samplingClock instanceof ICCSLConstraint)
			((ICCSLConstraint) samplingClock).semantic(helper);
		if ( isDead() ) {
			helper.inhibitClock(getExpressionClock());
		}
		else {
			if (localState == SamplingState.NORMAL || state == State.DEAD) {
				helper.inhibitClock(getExpressionClock());
			}
			else if (localState == SamplingState.WAITSAMPLING) {
				helper.semanticBDDAnd(helper.createEqual(
							getExpressionClock(), getSamplingClock()));
			}
		}
		helper.registerClockUse(new RuntimeClock[] { getExpressionClock(),
				getSampledClock(), getSamplingClock() });
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if ( ! canCallUpdate() )
			return;
		super.update(helper);
		if (sampledClock instanceof ICCSLConstraint)
			((ICCSLConstraint) sampledClock).update(helper);
		if (samplingClock instanceof ICCSLConstraint)
			((ICCSLConstraint) samplingClock).update(helper);
		switch (localState) {
		case NORMAL:
			if (helper.clockHasFired(sampledClock)) {
//				if (helper.clockHasFired(samplingClock)) {
//					terminate(helper);
//				}
//				else {
					localState = SamplingState.WAITSAMPLING;
//				}
			}
			break;
		case WAITSAMPLING:
			if (helper.clockHasFired(samplingClock)) {
				terminate(helper);
			}
			break;
		default:
			break;
		}
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (sampledClock instanceof ICCSLConstraint)
			((ICCSLConstraint) sampledClock).deathSemantic(helper);
		if (samplingClock instanceof ICCSLConstraint)
			((ICCSLConstraint) samplingClock).deathSemantic(helper);
		switch (localState) {
		case NORMAL:
			helper.registerDeathImplication(getSampledClock(), getExpressionClock());
			break;
		case WAITSAMPLING:
			helper.registerDeathImplication(getSamplingClock(), getExpressionClock());
			break;
		}
	}
	
	public RuntimeClock getSampledClock() {
		return sampledClock;
	}
	
	public RuntimeClock getSamplingClock() {
		return samplingClock;
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
		currentState.dump(localState.ordinal());
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
		localState = (SamplingState) SamplingState.values()[(Integer)newState.restore(2)];
	}

}
