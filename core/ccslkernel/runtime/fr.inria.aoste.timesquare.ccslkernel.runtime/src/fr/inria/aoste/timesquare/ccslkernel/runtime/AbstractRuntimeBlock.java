/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.AbstractRuntimeExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;


public abstract class AbstractRuntimeBlock implements ICCSLConstraint, IRuntimeContainer, INamedElement {

	public AbstractRuntimeBlock(IRuntimeContainer parent, String name) {
		setParent(parent);
		this.name = name;
	}

	protected IRuntimeContainer parent;
	protected String name;
	
	protected abstract void init();	
	protected abstract ICCSLConstraint[] getConstraints();
	public abstract AbstractRuntimeBlock[] getSubBlocks();
	
	public String getName() {
		return name;
	}
	
	@Override
	public QualifiedName getQualifiedName() {
		if (parent != null && parent instanceof INamedElement)
			return new QualifiedName(((INamedElement) parent).getQualifiedName()).append(getName());
		else
			return new QualifiedName(getName());
	}
	
	@Override
	public void setQualifiedName(QualifiedName qualifiedName) {
	}
	
	@Override
	public void setQualifiedName(String qualifiedName) {
	}
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}
	
	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null) {
			parent.addContent(this);
		}
	}

	private List<IRuntimeContainer> contents = new ArrayList<IRuntimeContainer>();
	private List<AbstractRuntimeRelation> relations = new ArrayList<AbstractRuntimeRelation>();
	private List<AbstractRuntimeExpression> expressions = new ArrayList<AbstractRuntimeExpression>();
	private List<RuntimeClock> denseClocks = new ArrayList<RuntimeClock>();
	private List<RuntimeClock> discreteClocks = new ArrayList<RuntimeClock>();

	@Override
	public void addContent(IRuntimeContainer element) {
		if (element instanceof AbstractRuntimeRelation) {
			relations.add((AbstractRuntimeRelation) element);
		}
		else if (element instanceof AbstractRuntimeExpression) {
			expressions.add((AbstractRuntimeExpression) element);
		}
		else if (element instanceof RuntimeClock && ((RuntimeClock) element).isDense()) {
			denseClocks.add((RuntimeClock) element);
		}
		else if (element instanceof RuntimeClock) {
			discreteClocks.add((RuntimeClock) element);
		}
		contents.add(element);
	}

	@Override
	public List<IRuntimeContainer> getContents() {
		return contents;
	}
	
	protected List<AbstractRuntimeRelation> getRelations() {
		return relations;
	}
	
	protected List<AbstractRuntimeExpression> getExpressions() {
		return expressions;
	}
	
	public List<RuntimeClock> getDenseClocks() {
		return denseClocks;
	}
	
	public List<RuntimeClock> getDiscreteClocks() {
		return discreteClocks;
	}
	
	public List<RuntimeClock> getAllDiscreteClocks(List<RuntimeClock> res) {
		res.addAll(getDiscreteClocks());
		for (AbstractRuntimeRelation relation : getRelations()) {
			for (RuntimeClock clock : relation.getDiscreteClocks()) {
				res.add(clock);
			}
		}
		for (AbstractRuntimeExpression expression : getExpressions()) {
			res.addAll(expression.getDiscreteClocks());
		}
		for (AbstractRuntimeBlock subBlock : getSubBlocks()) {
			res.addAll(subBlock.getDiscreteClocks());
		}
		return res;
	}
	
	public List<AbstractRuntimeRelation> getAllAssertions() {
		List<AbstractRuntimeRelation> res = new ArrayList<AbstractRuntimeRelation>();
		for (AbstractRuntimeRelation relation : getRelations()) {
			if (relation.isAssertion()) {
				res.add(relation);
				res.addAll(relation.getAllAssertions());
			}
		}
		for (AbstractRuntimeBlock subBlock : getSubBlocks()) {
			res.addAll(subBlock.getAllAssertions());
		}
		return res;
	}

	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		for (ICCSLConstraint ct : getRelations()) {
			ct.semantic(helper);
		}
		for (ICCSLConstraint ct : getExpressions()) {
			ct.semantic(helper);
		}
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		for (ICCSLConstraint ct : getConstraints()) {
			ct.deathSemantic(helper);
		}
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		for (ICCSLConstraint ct : getConstraints()) {
			ct.update(helper);
		}
	}
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		for (ICCSLConstraint ct : getConstraints()) {
			ct.start(helper);
		}
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) {		
	}
	
	@Override
	public boolean isTerminated() {
		return false;
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		return null;
	}
	
	@Override
	public void restoreState(SerializedConstraintState state) {		
	}
	
}
