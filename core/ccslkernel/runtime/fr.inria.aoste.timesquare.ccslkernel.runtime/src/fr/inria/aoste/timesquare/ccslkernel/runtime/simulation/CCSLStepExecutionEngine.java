/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.simulation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.NoBooleanSolution;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.BDDHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.SemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.UpdateHelper;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;
import net.sf.javabdd.BuDDyFactory;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class CCSLStepExecutionEngine extends AbstractStepExecutionEngine {

	public CCSLStepExecutionEngine(CCSLSimulationEngine simulationEngine) {
		super(simulationEngine);
		this.bddHelper = new BDDHelper();
		initHelpers();
		setSemanticBdd(getBuddyFactory().one());
	}
	
	private BDDHelper bddHelper;
	
	@Override
	protected AbstractSemanticHelper createSemanticHelper() {
		return new SemanticHelper(this, bddHelper);
	}
	
	@Override
	protected AbstractUpdateHelper createUpdateHelper() {
		return new UpdateHelper(this);
	}
	
	@Override
	protected void stepPreHook() {
	}

	@Override
	protected void stepPostHook() {
		//dealWithBlockTransition();
	}

	@Override
	protected void constructCurrentStepBDD() throws SimulationException {
//		setSemanticBdd(getBuddyFactory().one());
		simulationEngine.getModel().semantic(getSemanticHelper());
		BuDDyBDD clockDisjunction = buildAllClocksDisjunction();
		semanticBdd.andWith(clockDisjunction);
	}

	private BuDDyBDD buildAllClocksDisjunction() {
		BuDDyBDD clockDisjunction = getBuddyFactory().zero();
		for (RuntimeClock clock : getUsedClocks()) {
			BuDDyBDD bddClockVar = getBuddyFactory().ithVar(clock.bddVariableNumber);
			clockDisjunction.orWith(bddClockVar);
		}
		return clockDisjunction;
	}

	@Override
	public List<RuntimeClock> getAllDiscreteClocks() {
		return simulationEngine.getModel().getAllDiscreteClocks();
	}
	
	public ArrayList<ArrayList<RuntimeClock>> getAllPossibleRuntimeClockSets() throws NoBooleanSolution {
		
		this.allClockTraceStateSet = this.getAllBDDSolutions();
        List<RuntimeClock> allClocks = this.getAllDiscreteClocks();
        ArrayList<RuntimeClock> afiredClockSet = new ArrayList<RuntimeClock>();
        ArrayList<ArrayList<RuntimeClock>>  allPossibleRuntimeClockSets = new ArrayList<ArrayList<RuntimeClock>>();
        for(ClockTraceStateSet aRes : allClockTraceStateSet){
        	for(RuntimeClock rc : allClocks){
        		if (this.getUsedClocks().contains(rc)) {
        			if(aRes.get(this.getIndexInSolution(rc.bddVariableNumber)) == ClockTraceState.F){
        				afiredClockSet.add(rc);
        			}
        		} 
        	}
        	allPossibleRuntimeClockSets.add(afiredClockSet);
        }
		return allPossibleRuntimeClockSets;
	}
	
	
	
	@Override
	public ArrayList<ClockTraceStateSet> getAllBDDSolutions() throws NoBooleanSolution {
		if (getSemanticBdd().isZero()) {
			// here we do not throws exception but returns an empty list. It is easier to deal with that and go back in the history
			//throw new NoBooleanSolution("There is a deadlock, no solution can be found");
			return new ArrayList<ClockTraceStateSet>();
		}
		//construct the var set 
		BuDDyBDD varSet = getBuddyFactory().one();
		for (Iterator<Integer> iterator = getBddVarToIndexInSolution().keySet().iterator();
				iterator.hasNext(); ) {
			Integer current = (Integer) iterator.next();
			varSet = varSet.and(getBuddyFactory().ithVar(current));
		}
		
		//get an iterator on all satisfying solutions
		Iterator<?> it = getSemanticBdd().iterator(varSet);
		ArrayList<ClockTraceStateSet> allSolutionsF = new ArrayList<ClockTraceStateSet>();
		
		int allClockSize = getBddVarToIndexInSolution().size();
		while (it.hasNext()) {
			ClockTraceStateSet tempF = new ClockTraceStateSet(allClockSize);
			ClockTraceStateSet tempE = new ClockTraceStateSet(allClockSize);
			BuDDyBDD v = (BuDDyBDD)it.next();
			for (Iterator<Integer> iterator = getBddVarToIndexInSolution().keySet().iterator();
					iterator.hasNext(); ) {
				int bddVarNumber = (Integer) iterator.next();
				int index = 0;
				
				index = getBddVarToIndexInSolution().get(bddVarNumber);
				
				if ( ! v.and(getBuddyFactory().ithVar(bddVarNumber)).isZero()) {
					tempF.set(index, ClockTraceState.T);
					tempE.set(index, ClockTraceState.T);
				} 
				else {
					tempF.set(index, ClockTraceState.F);
					tempE.set(index, ClockTraceState.F);
				}
			}
			allSolutionsF.add(tempF);
		}
		
		varSet.free();
		
		return allSolutionsF;
	}
	
	
	
	@Override
	public void rewriteExpressions() throws SimulationException {
		for (RuntimeClock clock : getAllDiscreteClocks()) {
			if (getClockFiredState(clock) == ClockTraceState.T) {
				registerClockFiring(clock);
			}
			if (getClockEnabledState(clock) == ClockTraceState.T) {
				enabledClocks.add(clock);
			}
		}
		simulationEngine.getModel().update(getUpdateHelper());
	}
		
	@Override
	protected void buildDeathExpressions() throws SimulationException {
		simulationEngine.getModel().deathSemantic(getSemanticHelper());
	}
		
	@Override
	public Map<Integer, Integer> getBddVarToIndexInSolution() {
		return simulationEngine.getBddVarToIndexInSolution();
	}

	@Override
	protected BuDDyFactory getBuddyFactory() {
		return bddHelper.getFactory();
	}
	
	@Override
	public SimulationPolicyBase getSimulationPolicy() {
		return ((CCSLSimulationEngine)simulationEngine).getSimulationPolicy();
	}

	@Override
	public Set<RuntimeClock> getDeadClocks() {
		return simulationEngine.getDeadClocks();
	}
	
	@Override
	public void addDeadClock(RuntimeClock clock) {
		simulationEngine.addDeadClock(clock);
	}

	@Override
	public void dealWithBlockTransition() {
		throw new RuntimeException("dealWithBlockTransition() not yet implemented in CCSLStepExecutionEngine");
//		for (SolverBlockTransition sbt : solver.getAllBlockTransitions()) {
//
//			if (getClockEnabledState(sbt.getOnClock()) == ClockTraceState.T) {
//				if (sbt.getSource().isActiveBlock()){
//					try {
//						sbt.getSource().terminate(updateHelper);
//					} catch (SimulationException e) {
//						e.printStackTrace();
//					}
//				}
//				try {
//					sbt.getTarget().start(semanticHelper);
//				} catch (SimulationException e) {
//					e.printStackTrace();
//				}
//			}
//		}
	}
}
