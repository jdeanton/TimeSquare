/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.relations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.AbstractConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.INamedElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.QualifiedName;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.AbstractRuntimeExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public abstract class AbstractRuntimeRelation extends AbstractConstraint
			implements ICCSLConstraint, IRuntimeContainer, INamedElement {

	private String name;
	private QualifiedName qualifiedName;
	private boolean isAssertion;
	private int assertionVariableNumber;

	public AbstractRuntimeRelation() {
		this.isAssertion = false;
	}
	
	public AbstractRuntimeRelation(String name) {
		this.name = name;
		this.isAssertion = false;
	}

	protected abstract ICCSLConstraint[] getConstraints();
		
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (canCallStart())
			super.startTransition();
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper)  throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semanticTransition();
		if (isAssertion) {
			assertionSemantic(helper);
			helper.registerAssertion(this);
		}
	}

	public abstract void assertionSemantic(AbstractSemanticHelper helper);
	
	@Override
	public void update(AbstractUpdateHelper helper)  throws SimulationException {
		if (canCallUpdate())
			super.updateTransition();
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
	}
		
	@Override
	public boolean canCallTerminate() {
		return false; // relations never die...
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		if (canCallTerminate())
			super.terminateTransition();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setQualifiedName(String qualifiedName) {
		this.qualifiedName = new QualifiedName(qualifiedName);
	}
	
	@Override
	public void setQualifiedName(QualifiedName qualifiedName) {
		this.qualifiedName = qualifiedName;
	}
	
	@Override
	public QualifiedName getQualifiedName() {
		if (qualifiedName != null) {
			return qualifiedName;
		}
		else if (parent != null && parent instanceof INamedElement) {
			QualifiedName qname = new QualifiedName(((INamedElement) parent).getQualifiedName());
			qname.append(getName());
			qualifiedName = qname;
			return qname;
		}
		else {
			return new QualifiedName(getName());
		}
	}

	private IRuntimeContainer parent;
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null) {
			parent.addContent(this);
		}
	}

	@Override
	public void addContent(IRuntimeContainer element) {
		if (element instanceof RuntimeClock) {
			if (((RuntimeClock) element).isDense()) {
				denseClocks.add((RuntimeClock) element);
			}
			else {
				discreteClocks.add((RuntimeClock) element);
			}
		}
		else if (element instanceof AbstractRuntimeRelation) {
			relations.add((AbstractRuntimeRelation) element);
		}
		else if (element instanceof AbstractRuntimeExpression) {
			expressions.add((AbstractRuntimeExpression) element);
		}
		contents.add(element);
	}
	
	private List<IRuntimeContainer> contents = new ArrayList<IRuntimeContainer>();
	private List<RuntimeClock> discreteClocks = new ArrayList<RuntimeClock>();
	private List<RuntimeClock> denseClocks = new ArrayList<RuntimeClock>();
	private List<AbstractRuntimeRelation> relations = new ArrayList<AbstractRuntimeRelation>();
	private List<AbstractRuntimeExpression> expressions = new ArrayList<AbstractRuntimeExpression>();
	
	public List<RuntimeClock> getDiscreteClocks() {
		List<RuntimeClock> res = new ArrayList<>(discreteClocks);
		for (AbstractRuntimeRelation relation : relations) {
			res.addAll(relation.getDiscreteClocks());
		}
		for (AbstractRuntimeExpression expression : expressions) {
			res.addAll(expression.getDiscreteClocks());
		}
		return res;
	}

	public Collection<? extends AbstractRuntimeRelation> getAllAssertions() {
		List<AbstractRuntimeRelation> res = new ArrayList<AbstractRuntimeRelation>();
		for (AbstractRuntimeRelation relation : relations) {
			if (relation.isAssertion) {
				res.add(relation);
				res.addAll(relation.getAllAssertions());
			}
		}
		return res;
	}

	@Override
	public List<IRuntimeContainer> getContents() {
		return contents;
	}

	public void setAssertion(boolean isAssertion) {
		this.isAssertion = isAssertion;
	}

	public boolean isAssertion() {
		return isAssertion;
	}
	
	public int getAssertionVariable() {
		return assertionVariableNumber;
	}
	
	public void setAssertionVariable(int assertionVariableNumber) {
		this.assertionVariableNumber = assertionVariableNumber;
	}
	
	@Override
	public boolean isTerminated() {
		return false;
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		return new SerializedConstraintState();
	}
	
	@Override
	public void restoreState(SerializedConstraintState state) {
	}
	
}
