/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;

public abstract class AbstractRuntimeModel implements IRuntimeContainer, INamedElement {

	private String name;
	
	public AbstractRuntimeModel(String name) {
		this.name = name;
		setParent(null);
	}
	
	public AbstractRuntimeModel(String name, IRuntimeContainer parent) {
		this.name = name;
		setParent(parent);
	}
	
	public abstract AbstractRuntimeBlock[] getSubBlocks();
	public abstract AbstractRuntimeBlock getEntryBlock();
	public abstract AbstractRuntimeModel[] getImportedModels();
	
	public String getName() {
		return name;
	}
	
	private IRuntimeContainer parent = null;
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
	}
	
	@Override
	public void addContent(IRuntimeContainer element) {	
		// empty implementation, work is done by the compiler that generates the getSubBlocks()
		// function. We use this to implement the getContents() function.
	}
	
	@Override
	public List<IRuntimeContainer> getContents() {
		List<IRuntimeContainer> res = new ArrayList<IRuntimeContainer>();
		for (AbstractRuntimeBlock block : getSubBlocks()) {
			res.add(block);
		}
		return res;
	}
	
	@Override
	public void setQualifiedName(QualifiedName qualifiedName) {		
	}
	
	@Override
	public void setQualifiedName(String qualifiedName) {		
	}

	@Override
	public QualifiedName getQualifiedName() {
		return new QualifiedName(getName());
	}
	
	public List<RuntimeClock> getAllDiscreteClocks() {
		List<RuntimeClock> res = new ArrayList<RuntimeClock>();
		for (AbstractRuntimeModel imported : getImportedModels()) {
			res.addAll(imported.getAllDiscreteClocks());
		}
		getEntryBlock().getAllDiscreteClocks(res);
		return res;
	}
	
	public List<AbstractRuntimeRelation> getAllAssertions() {
		List<AbstractRuntimeRelation> res = new ArrayList<AbstractRuntimeRelation>();
		for (AbstractRuntimeModel imported : getImportedModels()) {
			res.addAll( imported.getAllAssertions() );
		}
		res.addAll( getEntryBlock().getAllAssertions() );
		return res;
	}

	public void initSimulation(AbstractSemanticHelper helper) throws SimulationException {
		getEntryBlock().start(helper);
		for (AbstractRuntimeModel imported : getImportedModels()) {
			imported.initSimulation(helper);
		}
	}
	
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		getEntryBlock().semantic(helper);
		for (AbstractRuntimeModel imported : getImportedModels()) {
			imported.semantic(helper);
		}
	}
	
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		getEntryBlock().update(helper);
		for (AbstractRuntimeModel imported : getImportedModels()) {
			imported.update(helper);
		}
	}
	
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		getEntryBlock().deathSemantic(helper);
		for (AbstractRuntimeModel imported : getImportedModels()) {
			imported.deathSemantic(helper);
		}
	}

	
}
