/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.ValueProvider;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeSequence;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeDefer extends AbstractRuntimeExpression {

	
	private RuntimeClock baseClock;
	private RuntimeClock delayClock;
	private RuntimeSequence<? extends Integer> sigma;
	private ValueProvider<RuntimeSequence<? extends Integer>> sigmaProvider;
	
	private ArrayList<java.lang.Integer> ds = new ArrayList<java.lang.Integer>();

	public RuntimeDefer(RuntimeClock implicitClock, RuntimeClock baseClock, RuntimeClock delayClock,
			RuntimeSequence<? extends Integer> delayPattern) {
		super(implicitClock);
		this.baseClock = baseClock;
		this.delayClock = delayClock;
		this.sigma = delayPattern;
		this.sigmaProvider = null;
	}

	public RuntimeDefer(RuntimeClock implicitClock, RuntimeClock baseClock, RuntimeClock delayClock,
			ValueProvider<RuntimeSequence<? extends Integer>> delayPattern) {
		this.baseClock = baseClock;
		this.delayClock = delayClock;
		this.sigma = null;
		this.sigmaProvider = delayPattern;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		ds.clear();
		if (sigmaProvider != null) {
			sigma = sigmaProvider.getValue();
		}
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if (baseClock instanceof ICCSLConstraint)
			((ICCSLConstraint) baseClock).semantic(helper);
		if (delayClock instanceof ICCSLConstraint)
			((ICCSLConstraint) delayClock).semantic(helper);
		if (state == State.DEAD) {
			helper.inhibitClock(getExpressionClock());
		}
		else {
			boolean beta = false;	
			if (ds.isEmpty()) {
				beta = false;
			}
			else {
				beta = (getDelay() == 1 || getDelay() == 0);
			}
			if (beta) {
				helper.semanticBDDAnd(helper.createEqual(getExpressionClock(), delayClock));
			}
			else {
				helper.semanticBDDAnd(helper.createNot(getExpressionClock()));
			}

		}
		helper.registerClockUse(new RuntimeClock[] { getExpressionClock(), baseClock, delayClock });

	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (baseClock instanceof ICCSLConstraint)
			((ICCSLConstraint) baseClock).deathSemantic(helper);
		if (delayClock instanceof ICCSLConstraint)
			((ICCSLConstraint) delayClock).deathSemantic(helper);
		helper.registerDeathImplication(delayClock, getExpressionClock());
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if (baseClock instanceof ICCSLConstraint)
			((ICCSLConstraint) baseClock).update(helper);
		if (delayClock instanceof ICCSLConstraint)
			((ICCSLConstraint) delayClock).update(helper);
		if ( (ds.isEmpty() && sigma.isEmpty())
			 || delayClock.isDead()) {
			terminate(helper);
			return;
		}
		if (helper.clockHasFired(baseClock)) {
			if (helper.clockHasFired(delayClock)) {
				// RWDefer3
				nextDelay();
				Integer next = sigma.popHead();
				if (next != null) {
					sched(next, 0);
				}
			}
			else {
				// RWDefer2
				Integer next = sigma.popHead();
				if (next != null) {
					sched(next, 0);
				}
			}
		}
		else if (helper.clockHasFired(delayClock)) {
			// RWDefer1 nextDelay()
			nextDelay();
		}
	}
	
	
	private int getDelay() {
		if (ds.isEmpty()) {
			return java.lang.Integer.MAX_VALUE;
		}
		return ds.get(0);
	}
	
	private void nextDelay() {
		if (! ds.isEmpty()) {
			int head = ds.get(0);
			if (head == 1 || head == 0) {
				ds.remove(0);
			}
			else {
				ds.set(0, head - 1);
			}
		}
	}
	
	private void sched(int next, int start) {
		if (ds.size() == start) {
			ds.add(start, next);
		}
		else {
			int head = ds.get(start);
			if (next == head) {
				return;
			}
			else if (next < head) {
				int rem = head - next;
				ds.set(start, next);
				ds.add(start + 1, rem);
			}
			else { // next > head
				int rem = next - head;
				sched(rem, start + 1);
			}
		}
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		currentState.dump(ds.size());
		for(int i=0; i< ds.size(); i++){
			currentState.dump(ds.get(i).intValue());
		}
//		currentState.dump(ds);
		//TO FIX
		//currentState.add(sigma.dumpState());
		currentState.dump(sigma.finiteIndex);
		
//		currentState.dump(sigma.finitePart);
		
		currentState.dump(sigma.infiniteIndex);
//		currentState.dump(sigma.infinitePart);
		
//		currentState.dump(ds);
//		currentState.dump(sigma.dumpState());
		return currentState;
	}
	
	@Override
	public void restoreState(SerializedConstraintState stateData) {
		super.restoreState(stateData);
		ds.clear();
		int size = stateData.restore(2);
		int i = 3;
		for(i=3; i< size+3; i++){
			ds.add(new Integer((int)stateData.restore(i)));
		}
//		ds = stateData.restore(2);
		sigma.finiteIndex = stateData.restore(i++);
//		sigma.finitePart = stateData.restore(i++);
		sigma.infiniteIndex= stateData.restore(i++);
//		sigma.infinitePart= stateData.restore(i++);
		//TO FIX
		//sigma.restoreState((SerializedConstraintState) stateData.restore(3));
	}
	
}
