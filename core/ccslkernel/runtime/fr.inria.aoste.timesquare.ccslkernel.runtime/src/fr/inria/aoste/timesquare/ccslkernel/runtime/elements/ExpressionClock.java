/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.elements;

import fr.inria.aoste.timesquare.ccslkernel.runtime.IExpressionClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.AbstractRuntimeExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class ExpressionClock extends RuntimeClock implements IExpressionClock {

	public ExpressionClock(String name) {
		super(name);
	}
	
	private AbstractRuntimeExpression expression;
	
	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.ccslkernel.runtime.elements.IExpressionClock#setExpression(fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.AbstractRuntimeExpression)
	 */
	@Override
	public void setExpression(AbstractRuntimeExpression expression) {
		this.expression = expression;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.ccslkernel.runtime.elements.IExpressionClock#getExpression()
	 */
	@Override
	public AbstractRuntimeExpression getExpression() {
		return expression;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		super.start();
		getExpression().start(helper);
	}

	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		getExpression().semantic(helper);
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		getExpression().update(helper);
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		getExpression().deathSemantic(helper);
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		super.terminate(helper);
		getExpression().terminate(helper);
	}
	
	@Override
	public boolean isTerminated() {
		return getExpression().isTerminated();
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		return getExpression().dumpState();
	}
	
	@Override
	public void restoreState(SerializedConstraintState state) {
		getExpression().restoreState(state);
	}
	
}
