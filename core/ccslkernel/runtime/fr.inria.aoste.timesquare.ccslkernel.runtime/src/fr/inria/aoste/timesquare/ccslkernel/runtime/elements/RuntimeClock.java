/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.INamedElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.QualifiedName;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;


public class RuntimeClock implements INamedElement, IRuntimeContainer {

	public ArrayList<String> linkedElements = new ArrayList<String>();
	
	private String name;
	private QualifiedName qualifiedName;
	public int tickCount;
	public int bddVariableNumber;
	private boolean dense;
	private IRuntimeContainer parent;
	
	public boolean isDense() {
		return dense;
	}
	
	public void setDense(boolean dense) {
		this.dense = dense;
	}

	public static int UNALLOCATEDBDDVARIABLE = -1;

	static public enum ClockState {
		ALIVE, DEAD
	}
	
	protected ClockState state;
	
	public RuntimeClock(String name) {
		this.name = name;
		this.tickCount = 0;
		state = ClockState.ALIVE;
	}
	
	public RuntimeClock() {
		this.tickCount = 0;
		state = ClockState.ALIVE;
	}

	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null) {
			parent.addContent(this);
		}
	}
	
	@Override
	public void addContent(IRuntimeContainer element) {
	}
	
	@Override
	public List<IRuntimeContainer> getContents() {
		return Collections.emptyList();
	}
	
	public void setName(String s) {
		this.name = s;
	}
	
	public String getName() {
		return name;
	}

	public void setQualifiedName(String qualifiedName) {
		this.qualifiedName = new QualifiedName(qualifiedName);
	}
	
	public void setQualifiedName(QualifiedName qualifiedName) {
		this.qualifiedName = qualifiedName;
	}
	
	public QualifiedName getQualifiedName() {
		if (qualifiedName != null)
			return qualifiedName;
		else if (parent != null && parent instanceof INamedElement) {
			QualifiedName qname = new QualifiedName( ((INamedElement) parent).getQualifiedName() ).append(getName());
			setQualifiedName(qname);
			return qname;
		}
		else
			return new QualifiedName(getName());
	}

	@Override
	public String toString() {
		return "[" + name + "/" + tickCount + "]";
	}
	
	protected ClockState getState() {
		return state;
	}
	
	public void start() {
		state = ClockState.ALIVE;
	}
		
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		state = ClockState.DEAD;
	}
	
	public boolean isDead() {
		return state == ClockState.DEAD;
	}

	public void setDead(boolean isDead) {
		if (isDead) {
			this.state = ClockState.DEAD;
		}
		else {
			this.state = ClockState.ALIVE;
		}
	}

}

