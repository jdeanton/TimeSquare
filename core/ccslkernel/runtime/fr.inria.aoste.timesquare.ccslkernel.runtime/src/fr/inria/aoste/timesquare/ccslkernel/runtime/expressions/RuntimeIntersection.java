/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeIntersection extends AbstractRuntimeExpression {

	public RuntimeIntersection(RuntimeClock iClock, RuntimeClock clock1, RuntimeClock clock2) {
		super(iClock);
		this.clock1 = clock1;
		this.clock2 = clock2;
	}

	private RuntimeClock clock1;
	private RuntimeClock clock2;
		
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if (clock1 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock1).semantic(helper);
		if (clock2 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock2).semantic(helper);
		if (! canCallSemantic())
			return;
		super.semantic(helper);
		if ( isDead() ) {
			helper.inhibitClock(getExpressionClock());
		}
		else {
			helper.semanticBDDAnd(helper.createEqual(getExpressionClock(),
					helper.createIntersection(clock1, clock2)));
		}
		helper.registerClockUse(new RuntimeClock[] { getExpressionClock(), clock1, clock2 });
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		super.update(helper);
		if (clock1 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock1).update(helper);
		if (clock2 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock2).update(helper);
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (clock1 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock1).deathSemantic(helper);
		if (clock2 instanceof ICCSLConstraint)
			((ICCSLConstraint) clock2).deathSemantic(helper);
		helper.registerDeathConjunctionImplies(clock1, clock2, getExpressionClock());
	}
	
}
