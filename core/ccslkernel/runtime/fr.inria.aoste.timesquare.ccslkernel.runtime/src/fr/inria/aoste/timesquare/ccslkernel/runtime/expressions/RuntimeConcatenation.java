/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.expressions;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class RuntimeConcatenation extends AbstractRuntimeExpression {

	static public enum ConcatState {
		FOLLOWLEFT, FOLLOWRIGHT
	};
	
	private RuntimeClock leftClock;
	private RuntimeClock rightClock;
	
	public RuntimeConcatenation(RuntimeClock iClock, RuntimeClock leftClock, RuntimeClock rightClock) {
		super(iClock);
		this.leftClock = leftClock;
		this.rightClock = rightClock;
	}

	private ConcatState concatState;
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		concatState = ConcatState.FOLLOWLEFT;
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(helper);
		if (concatState == ConcatState.FOLLOWLEFT) {
			helper.registerClockEquality(getExpressionClock(), leftClock);
			helper.registerClockUse( new RuntimeClock[] { getExpressionClock(), leftClock } );
			if (leftClock instanceof ICCSLConstraint) {
				((ICCSLConstraint) leftClock).semantic(helper);
			}
		}
		else if (concatState == ConcatState.FOLLOWRIGHT) {
			helper.registerClockEquality(getExpressionClock(), rightClock);
			helper.registerClockUse( new RuntimeClock[] { getExpressionClock(), rightClock } );
			if (rightClock instanceof ICCSLConstraint) {
				((ICCSLConstraint) rightClock).semantic(helper);
			}
		}
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (concatState == ConcatState.FOLLOWLEFT) {
			if (leftClock instanceof ICCSLConstraint)
				((ICCSLConstraint) leftClock).update(helper);
		}
		else if (concatState == ConcatState.FOLLOWRIGHT) {
			if (rightClock != getExpressionClock()) {
				if (rightClock instanceof ICCSLConstraint)
					((ICCSLConstraint) rightClock).update(helper);
			}
		}
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if (concatState == ConcatState.FOLLOWLEFT) {
			if (leftClock.isDead()) {
				if (rightClock != getExpressionClock()) {
					concatState = ConcatState.FOLLOWRIGHT;
					helper.registerClockToStart(rightClock);
				}
				else {
					helper.registerClockToStart(leftClock);
				}
			}
		}
		else if (concatState == ConcatState.FOLLOWRIGHT) {
			if ( ! isRecursive() && rightClock.isDead()) {
				terminate(helper);
			}
		}
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper helper) throws SimulationException {
		super.deathSemantic(helper);
		if (concatState == ConcatState.FOLLOWLEFT) {
			if (leftClock instanceof ICCSLConstraint)
				((ICCSLConstraint) leftClock).deathSemantic(helper);
		}
		else if ( ! isRecursive() ) {
			if (rightClock instanceof ICCSLConstraint)
				((ICCSLConstraint) rightClock).deathSemantic(helper);
			helper.registerDeathImplication(rightClock, getExpressionClock());
		}
	}
		
	private boolean isRecursive() {
		return rightClock == getExpressionClock();
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
		currentState.dump(concatState.ordinal());
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
		concatState = (ConcatState) ConcatState.values()[ (Integer) newState.restore(2)];
	}

	@Override
	public String toString() {
		String res = "[" + getExpressionClock().getName() + "]"
				+ (state != State.DEAD && state != State.INIT ? "*" : "") + "Concat(";
		res += (concatState == ConcatState.FOLLOWLEFT ? "^" : "") + leftClock + ", ";
		res += (concatState == ConcatState.FOLLOWRIGHT ? "^" : "") + rightClock + ")";
		return res;
	}
	
}
