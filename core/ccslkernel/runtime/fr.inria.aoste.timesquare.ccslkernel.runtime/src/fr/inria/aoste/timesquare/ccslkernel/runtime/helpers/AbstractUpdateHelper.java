/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.helpers;

import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.AbstractStepExecutionEngine;

public abstract class AbstractUpdateHelper {

	protected AbstractStepExecutionEngine stepEngine;
	
	public AbstractUpdateHelper(AbstractStepExecutionEngine stepEngine) {
		this.stepEngine = stepEngine;
	}
	
	public boolean clockHasFired(RuntimeClock clock) {
		return stepEngine.clockHasFired(clock);
	}

	public void registerClockToStart(RuntimeClock clock) {
		stepEngine.registerClockToStart(clock);
	}

	public void registerNewDeadClock(RuntimeClock clock) {
		stepEngine.registerNewDeadClock(clock);
	}
	
}
