/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.elements;

import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;

public class RuntimeElement implements IRuntimeContainer {

	public RuntimeElement() {
	}
	
	protected IRuntimeContainer parent;
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null) {
			parent.getContents().add(this);
		}
	}

	@Override
	public List<IRuntimeContainer> getContents() {
		return Collections.emptyList();
	}
	
	@Override
	public void addContent(IRuntimeContainer element) {		
	}
}
