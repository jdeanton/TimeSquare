/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.helpers;

import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import net.sf.javabdd.BuDDyFactory;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public abstract class AbstractSemanticHelper {

	private BuDDyFactory BuDDyFactory;
	private BDDHelper bddHelper;

	public AbstractSemanticHelper(BDDHelper bddHelper) {
		this.setBDDHelper(bddHelper);
		this.setBuddyFactory(bddHelper.getFactory());
	}
	
	protected BDDHelper getBDDHelper() {
		return bddHelper;
	}

	protected BDDHelper getBddHelper() {
		return bddHelper;
	}

	protected void setBDDHelper(BDDHelper bddHelper) {
		this.bddHelper = bddHelper;
	}

	protected BuDDyFactory getBuddyFactory() {
		return BuDDyFactory;
	}


	protected void setBuddyFactory(BuDDyFactory BuDDyFactory) {
		this.BuDDyFactory = BuDDyFactory;
	}

	
	
	/**
	 * Returns the BuDDyBDD variable used for simulation semantic
	 * 
	 * @param clk
	 * @return
	 */
	public BuDDyBDD getBDDVariable(RuntimeClock clk) {
		return getBuddyFactory().ithVar(clk.bddVariableNumber);
	}

	/**
	 * Returns the BuDDyBDD false state of the variable used for simulation semantic
	 * 
	 * @param clk
	 * @return
	 */
	public BuDDyBDD getFalseBDDVariable(RuntimeClock clk) {
		return createNot(clk);
	}

	/**
	 * Creates and returns a BuDDyBDD term suitable for the simulation semantic and
	 * representing a conjunction of the two clocks.
	 * 
	 * @param c1
	 * @param c2
	 * @return
	 * @see SemanticHelper#semanticBDDAnd(BuDDyBDD)
	 */
	public BuDDyBDD createAnd(RuntimeClock c1, RuntimeClock c2) {
		BuDDyBDD arg1 = getBuddyFactory().ithVar(c1.bddVariableNumber);
		BuDDyBDD arg2 = getBuddyFactory().ithVar(c2.bddVariableNumber);
		return arg1.and(arg2);
	}

	/**
	 * WARNING: Consumes t2 !!
	 * 
	 * @param t1
	 * @param t2
	 * @return
	 */
	public BuDDyBDD createAnd(BuDDyBDD t1, BuDDyBDD t2) {
		return t1.andWith(t2);
	}

	public BuDDyBDD createNot(RuntimeClock clk) {
		BuDDyBDD arg = getBuddyFactory().nithVar(clk.bddVariableNumber);
		return arg;
	}

	/**
	 * Creates and returns a BuDDyBDD that represents the implication from clock c1
	 * to clock c2. The returned BuDDyBDD is intended to be used for the simulation
	 * semantic with the function {@link #semanticBDDAnd(BuDDyBDD)}
	 * 
	 * @param c1
	 * @param c2
	 * @return
	 * @see #semanticBDDAnd(BuDDyBDD).
	 */
	public BuDDyBDD createImplies(RuntimeClock c1, RuntimeClock c2) {
		if (c1 == c2 || c1.bddVariableNumber == c2.bddVariableNumber) {
			return createOne();
		}
		BuDDyBDD arg1 = getBuddyFactory().ithVar(c1.bddVariableNumber);
		BuDDyBDD arg2 = getBuddyFactory().ithVar(c2.bddVariableNumber);
		return arg1.imp(arg2);
	}

	/**
	 * 
	 * @param c1
	 * @param c2
	 * @return a BuDDyBDD to be used for the simulation semantic with the function
	 *         {@link #semanticBDDAnd(BuDDyBDD)}
	 */
	public BuDDyBDD createEqual(RuntimeClock c1, RuntimeClock c2) {
		if (c1 == c2 || c1.bddVariableNumber == c2.bddVariableNumber) {
			return createOne();
		}
		return getBddHelper().createEqual(c1.bddVariableNumber, c2.bddVariableNumber);
	}

	/**
	 * 
	 * @param clk
	 * @param term
	 * @return a BuDDyBDD to be used for the simulation semantic with the function
	 *         {@link #semanticBDDAnd(BuDDyBDD)}
	 */
	public BuDDyBDD createEqual(RuntimeClock clk, BuDDyBDD term) {
		return getBddHelper().createEqual(
				getBuddyFactory().ithVar(clk.bddVariableNumber), term);
	}

	/**
	 * 
	 * @param t1
	 * @param t2
	 * @return a BuDDyBDD to be used for the simulation semantic with the function
	 *         {@link #semanticBDDAnd(BuDDyBDD)}
	 */
	public BuDDyBDD createEqual(BuDDyBDD t1, BuDDyBDD t2) {
		return getBddHelper().createEqual(t1, t2);
	}

	/**
	 * 
	 * @param c1
	 * @param c2
	 * @return a BuDDyBDD to be used for the simulation semantic with the function
	 *         {@link #semanticBDDAnd(BuDDyBDD)}
	 */
	public BuDDyBDD createExclusion(RuntimeClock c1, RuntimeClock c2) {
		BuDDyBDD res = getBddHelper().createExclusion(c1.bddVariableNumber,
				c2.bddVariableNumber);
		return res;
	}

	/**
	 * 
	 * @param c1
	 * @param c2
	 * @return a BuDDyBDD to be used for the simulation semantic with the function
	 *         {@link #semanticBDDAnd(BuDDyBDD)}
	 */
	public BuDDyBDD createUnion(RuntimeClock c1, RuntimeClock c2) {
		return getBddHelper().createUnion(c1.bddVariableNumber, c2.bddVariableNumber);
	}

	/**
	 * 
	 * @param c1
	 * @param c2
	 * @return a BuDDyBDD to be used for the simulation semantic with the function
	 *         {@link #semanticBDDAnd(BuDDyBDD)}
	 */
	public BuDDyBDD createIntersection(RuntimeClock c1, RuntimeClock c2) {
		return getBddHelper().createInter(c1.bddVariableNumber, c2.bddVariableNumber);
	}

	/**
	 * 
	 * @param clk
	 * @param term
	 * @return a BuDDyBDD to be used for the simulation semantic with the function
	 *         {@link #semanticBDDAnd(BuDDyBDD)}
	 */
	public BuDDyBDD createIntersection(RuntimeClock clk, BuDDyBDD term) {
		return getBddHelper().createInter(
				getBuddyFactory().ithVar(clk.bddVariableNumber), term);
	}

	/**
	 * 
	 * @return a BuDDyBDD to be used for the simulation semantic with the function
	 *         {@link #semanticBDDAnd(BuDDyBDD)}
	 */
	public BuDDyBDD createOne() {
		return getBuddyFactory().one();
	}

	/**
	 * Creates and adds to the simulation semantic a term that forbids the
	 * occurrence of the clock clk.
	 * 
	 * @param clk
	 *            the clock to inhibit.
	 */
	public void inhibitClock(RuntimeClock clk) {
		semanticBDDAnd(createNot(clk));
	}

	public void registerClockEquality(RuntimeClock leftClock, RuntimeClock rightClock) {
		semanticBDDAnd(createEqual(leftClock, rightClock));
	}

	public void registerClockImplication(RuntimeClock leftClock, RuntimeClock rightClock) {
		semanticBDDAnd(createImplies(leftClock, rightClock));
	}

	/**
	 * Adds the BuDDyBDD argument to the simulation semantic using a conjunction. The
	 * BuDDyBDD given as argument is freed after the operation.
	 * 
	 * @param term
	 */
	public abstract void semanticBDDAnd(BuDDyBDD term);
	
	/**
	 * Adds to the simulation semantic an expression that relates the BuDDyBDD
	 * variable that represents the assertion relation with the BuDDyBDD term that
	 * represents the semantic of the relation itself. The BuDDyBDD added (with a
	 * conjuntion) to the simulation semantic is an equality between the
	 * variable that represent the assertion relation itself, and the BuDDyBDD term
	 * given as argument.
	 * 
	 * If the assertion semantic is not an equality, then the various functions
	 * to create the BuDDyBDD have to be called explicitely, then the function
	 * {@link #semanticBDDAnd(BuDDyBDD)} is called to conjunct the result with the
	 * BuDDyBDD simulation sematic, and finally the function
	 * {@link #registerAssertion(SolverRelation)} is called to register the fact
	 * that the assertion has been handled and that a semantic term has been
	 * produced.
	 * 
	 * @param relation
	 * @param term
	 * @see #getAssertionVariable(SolverRelation)
	 */
	public void assertionSemantic(AbstractRuntimeRelation relation, BuDDyBDD term) {
		BuDDyBDD assertionVar = getAssertionVariable(relation);
		BuDDyBDD res = getBddHelper().createEqual(term, assertionVar);
		semanticBDDAnd(res);
		registerAssertion(relation);
	}
	
	/**
	 * This function registers that the relation given as argument is an
	 * assertion.
	 * 
	 * @param relation
	 */
	public abstract void registerAssertion(AbstractRuntimeRelation relation);

	/**
	 * Returns the BuDDyBDD variable used to record the semantic of the relation as
	 * an assertion
	 * 
	 * @param relation
	 * @return
	 */
	public BuDDyBDD getAssertionVariable(AbstractRuntimeRelation relation) {
		return getBuddyFactory().ithVar(relation.getAssertionVariable());
	}

	public abstract Set<RuntimeClock> getUsedClocks();
	
	public abstract void registerClockUse(RuntimeClock clock);
	
	public void registerClockUse(RuntimeClock clocks[]) {
		for (RuntimeClock clock : clocks) {
			registerClockUse(clock);
		}
	}

	public abstract boolean isSemanticDone(ICCSLConstraint ct);

	public abstract void registerSemanticDone(ICCSLConstraint ct);
	
	public abstract void registerClockToStart(RuntimeClock clock);
	
	public abstract void unregisterClockToStart(RuntimeClock clock);

	public abstract void unregisterDeadClock(RuntimeClock clock);

	/**
	 * Builds a BuDDyBDD that represents an equality between the two clocks with
	 * respect to the death semantic. This BuDDyBDD is then and'ed with the current
	 * death semantic.
	 * 
	 * @param c1
	 * @param c2
	 */
	public abstract void registerDeathEquality(RuntimeClock clock1, RuntimeClock clock2);

	/**
	 * Builds a BuDDyBDD that represents an implication between the two clocks, c1
	 * implies c2, with respect to the death semantic. This BuDDyBDD is then and'ed
	 * with the current death semantic.
	 * 
	 * @param c1
	 * @param c2
	 */
	public abstract void registerDeathImplication(RuntimeClock leftClock, RuntimeClock rightClock);

	public abstract void registerDeathConjunctionImplies(RuntimeClock left1, RuntimeClock left2,
					RuntimeClock right);
	
	public abstract void registerDeadClock(RuntimeClock clock);
	
	/**
	 * Builds a BuDDyBDD that forces the death of the clock cl in the death semantic.
	 * This BuDDyBDD is then and'ed with the current death semantic.
	 * 
	 * @param cl
	 */
	public abstract void forceDeath(RuntimeClock clock);
	
	public abstract AbstractUpdateHelper getUpdateHelper();
	
}
