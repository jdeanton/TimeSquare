/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * 
 */
public class QualifiedName {

	final public static String separator = "::";
	
	private ArrayList<String> segments;
	
	public QualifiedName(String fqn, String separator) {
		this( fqn.split(separator) );
	}
	
	public QualifiedName(String fqn) {
		this(fqn, separator);
	}
	
	public QualifiedName(String[] segments) {
		this.segments = new ArrayList<String>(segments.length);
		for (int i = 0; i < segments.length; i++)
			this.append(segments[i]);
	}
	
	public QualifiedName(Collection<String> segments) {
		this.segments = new ArrayList<String>(segments);
	}
	
	public QualifiedName(QualifiedName old) {
		this.segments = new ArrayList<String>(old.getSegments());
	}
	
	public List<String> getSegments() {
		return segments;
	}
	
	public String[] getSegmentsArray() {
		return (String[]) segments.toArray();
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( ! (obj instanceof QualifiedName))
			return false;
		if ( ((QualifiedName)obj).segments.size() != segments.size() )
			return false;
		for (int i = 0; i < segments.size(); i++) {
			if (segments.get(i).compareTo(((QualifiedName)obj).segments.get(i)) != 0)
				return false;
		}
		return true;
	}
	
	public String toString() {
		String res = "";
		for (Iterator<String> iter = segments.iterator(); iter.hasNext(); ) {
			res += iter.next();
			if (iter.hasNext()) {
				res += separator;
			}
		}
		return res;
	}

	public QualifiedName append(QualifiedName qualifiedName) {
		this.getSegments().addAll(qualifiedName.getSegments());
		return this;
	}
	
	public QualifiedName append(String name) {
		this.getSegments().add(name);
		return this;
	}

}
