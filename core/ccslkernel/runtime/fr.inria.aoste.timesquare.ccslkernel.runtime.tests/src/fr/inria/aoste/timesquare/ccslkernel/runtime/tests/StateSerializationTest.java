/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.runtime.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeSequence;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeWait;

public class StateSerializationTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test1() {
		RuntimeSequence<Integer> seq1 = new RuntimeSequence<>(new Integer[] { 1, 2 }, new Integer[] { 3, 4});
		SerializedConstraintState state = seq1.dumpState();
		RuntimeSequence<Integer> seq2 = new RuntimeSequence<>();
		seq2.restoreState(state);
		assertTrue(seq1.equals(seq2));
	}

	@Test
	public void test2() {
		RuntimeSequence<Integer> seq1 = new RuntimeSequence<>(new Integer[] { 1, 2 }, new Integer[] { 3, 4});
		SerializedConstraintState state1 = seq1.dumpState();
		RuntimeSequence<Integer> seq2 = new RuntimeSequence<>(new Integer[] { 1, 2 }, new Integer[] { 3, 4});
		SerializedConstraintState state2 = seq2.dumpState();
		assertTrue(state1.equals(state2));
	}

	@Test
	public void test3() {
		RuntimeSequence<Integer> seq1 = new RuntimeSequence<>(new Integer[] { 1, 2 }, new Integer[] { 3, 4});
		SerializedConstraintState state1 = seq1.dumpState();
		RuntimeSequence<Integer> seq2 = new RuntimeSequence<>(new Integer[] { 1, 2 }, new Integer[] { 3, 4, 5});
		SerializedConstraintState state2 = seq2.dumpState();
		assertFalse(state1.equals(state2));
	}
	
	@Test
	public void test4() {
		RuntimeSequence<Integer> seq1 = new RuntimeSequence<>(new Integer[] { 1, 2 }, new Integer[] { 3, 4});
		SerializedConstraintState state1 = seq1.dumpState();
		RuntimeSequence<Integer> seq2 = new RuntimeSequence<>(new Integer[] { 1, 3 }, new Integer[] { 2, 4});
		SerializedConstraintState state2 = seq2.dumpState();
		assertFalse(state1.equals(state2));
	}
	
	@Test
	public void test5() {
		RuntimeSequence<Integer> seq1 = new RuntimeSequence<>(new Integer[] { 1, 2 }, null);
		SerializedConstraintState state1 = seq1.dumpState();
		RuntimeSequence<Integer> seq2 = new RuntimeSequence<>(null, new Integer[] { 3, 4});
		SerializedConstraintState state2 = seq2.dumpState();
		assertFalse(state1.equals(state2));
	}

	@Test
	public void test8() {
		RuntimeClock ik = new RuntimeClock("iClock");
		RuntimeClock ck = new RuntimeClock("clock");
		RuntimeWait wt1 = new RuntimeWait(ik, ck, 3);
		SerializedConstraintState state1 = wt1.dumpState();
		assertNotNull(state1);
	}
	
}
