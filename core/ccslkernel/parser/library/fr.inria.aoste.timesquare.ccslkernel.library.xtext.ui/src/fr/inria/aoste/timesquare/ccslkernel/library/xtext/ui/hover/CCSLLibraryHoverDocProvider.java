/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.library.xtext.ui.hover;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;

public class CCSLLibraryHoverDocProvider implements IEObjectDocumentationProvider {

	private int indent = 3;
	
	public String getDocumentation(EObject o) {
		return describeObject(o, 0);
	}
	
	private String describeObject(EObject o, int indentation) {
		ArrayList<String > names = new ArrayList<String>();
		getNames(o, names);
		int indentLevel = 0;
		StringBuilder res = new StringBuilder("<i>Containment hierarchy:</i>");
		for (String name : names) {
			res.append("<br>");
			for (int i = 0; i < indentLevel; i++) {
				res.append("&nbsp;");
			}
			res.append(name);
			indentLevel += indent;
		}
		return res.toString();
	}

	private void getNames(EObject o, List<String> result) {
		if (o.eContainer() != null) {
			getNames(o.eContainer(), result);
		}
		result.add("<b>" + CCSLKernelUtils.getSimpleName(o) + "</b> [" + o.eClass().getName() + "]");
	}

}
