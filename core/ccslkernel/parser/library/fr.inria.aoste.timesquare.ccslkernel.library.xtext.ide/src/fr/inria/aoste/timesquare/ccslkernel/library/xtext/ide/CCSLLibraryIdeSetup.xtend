/*n * generated by Xtext 
 by action of Julien Deantoni*/
package fr.inria.aoste.timesquare.ccslkernel.library.xtext.ide

import com.google.inject.Guice
import fr.inria.aoste.timesquare.ccslkernel.library.xtext.CCSLLibraryRuntimeModule
import fr.inria.aoste.timesquare.ccslkernel.library.xtext.CCSLLibraryStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class CCSLLibraryIdeSetup extends CCSLLibraryStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new CCSLLibraryRuntimeModule, new CCSLLibraryIdeModule))
	}
	
}
