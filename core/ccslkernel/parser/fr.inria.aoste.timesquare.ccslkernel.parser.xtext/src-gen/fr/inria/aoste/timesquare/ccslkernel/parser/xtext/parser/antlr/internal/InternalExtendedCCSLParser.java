package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.IUnorderedGroupHelper.UnorderedGroupState;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.inria.aoste.timesquare.ccslkernel.parser.xtext.services.ExtendedCCSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalExtendedCCSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_REAL0", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'ClockConstraintSystem'", "'{'", "'imports'", "'}'", "'entryBlock'", "'dataTypes'", "','", "'Transition'", "'from'", "'to'", "'when'", "'Block'", "'initialBlock'", "';'", "'assert'", "'Relation'", "'['", "']'", "'('", "')'", "'Expression'", "'='", "'import'", "'as'", "'Clock'", "':'", "'->'", "'definingEvent'", "'String'", "'Integer'", "'Real'", "'Sequence'", "'Bool'", "'True'", "'False'", "'ConcreteEntity'", "'BooleanRef'", "'name'", "'referencedBool'", "'RealRef'", "'realElem'", "'IntegerRef'", "'UnaryRealPlus'", "'value'", "'operand'", "'UnaryRealMinus'", "'RealPlus'", "'leftValue'", "'rightValue'", "'RealMinus'", "'RealMultiply'", "'UnaryIntPlus'", "'UnaryIntMinus'", "'IntPlus'", "'IntMinus'", "'IntMultiply'", "'IntDivide'", "'Not'", "'And'", "'Or'", "'Xor'", "'RealEqual'", "'RealInf'", "'RealSup'", "'IntEqual'", "'IntInf'", "'IntSup'", "'SeqIsEmpty'", "'SeqGetTail'", "'SeqGetHead'", "'SeqDecr'", "'SeqSched'", "'integerExpr'", "'BooleanVariableRef'", "'referencedVar'", "'IntegerVariableRef'", "'RealVariableRef'", "'NumberSeqRef'", "'referencedNumberSeq'", "'SequenceVarRef'", "'StringType'", "'BooleanType'", "'IntegerType'", "'RealType'", "'CharType'", "'RecordType'", "'SequenceType'", "'DiscreteClockType'", "'DenseClockType'", "'baseUnit'", "'physicalMagnitude'", "'EnumerationType'", "'enumLiteral'", "'Field'", "'type'", "'start'", "'finish'", "'stop'", "'consume'", "'produce'", "'receive'", "'send'", "'any'", "'all'", "'undefined'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int RULE_REAL0=7;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int T__115=115;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__114=114;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__116=116;
    public static final int T__80=80;
    public static final int T__111=111;
    public static final int T__81=81;
    public static final int T__110=110;
    public static final int T__82=82;
    public static final int T__113=113;
    public static final int T__83=83;
    public static final int T__112=112;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int T__87=87;
    public static final int T__105=105;

    // delegates
    // delegators


        public InternalExtendedCCSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalExtendedCCSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalExtendedCCSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalExtendedCCSL.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */

     	private ExtendedCCSLGrammarAccess grammarAccess;

        public InternalExtendedCCSLParser(TokenStream input, ExtendedCCSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "ClockConstraintSystem";
       	}

       	@Override
       	protected ExtendedCCSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleClockConstraintSystem"
    // InternalExtendedCCSL.g:71:1: entryRuleClockConstraintSystem returns [EObject current=null] : iv_ruleClockConstraintSystem= ruleClockConstraintSystem EOF ;
    public final EObject entryRuleClockConstraintSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockConstraintSystem = null;


        try {
            // InternalExtendedCCSL.g:71:62: (iv_ruleClockConstraintSystem= ruleClockConstraintSystem EOF )
            // InternalExtendedCCSL.g:72:2: iv_ruleClockConstraintSystem= ruleClockConstraintSystem EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockConstraintSystemRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockConstraintSystem=ruleClockConstraintSystem();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockConstraintSystem; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClockConstraintSystem"


    // $ANTLR start "ruleClockConstraintSystem"
    // InternalExtendedCCSL.g:78:1: ruleClockConstraintSystem returns [EObject current=null] : this_ClockConstraintSystem_Impl_0= ruleClockConstraintSystem_Impl ;
    public final EObject ruleClockConstraintSystem() throws RecognitionException {
        EObject current = null;

        EObject this_ClockConstraintSystem_Impl_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:84:2: (this_ClockConstraintSystem_Impl_0= ruleClockConstraintSystem_Impl )
            // InternalExtendedCCSL.g:85:2: this_ClockConstraintSystem_Impl_0= ruleClockConstraintSystem_Impl
            {
            if ( state.backtracking==0 ) {

              		/* */
              	
            }
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getClockConstraintSystemAccess().getClockConstraintSystem_ImplParserRuleCall());
              	
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_ClockConstraintSystem_Impl_0=ruleClockConstraintSystem_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_ClockConstraintSystem_Impl_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClockConstraintSystem"


    // $ANTLR start "entryRuleBlock"
    // InternalExtendedCCSL.g:99:1: entryRuleBlock returns [EObject current=null] : iv_ruleBlock= ruleBlock EOF ;
    public final EObject entryRuleBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlock = null;


        try {
            // InternalExtendedCCSL.g:99:46: (iv_ruleBlock= ruleBlock EOF )
            // InternalExtendedCCSL.g:100:2: iv_ruleBlock= ruleBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlockRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBlock=ruleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlock; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlock"


    // $ANTLR start "ruleBlock"
    // InternalExtendedCCSL.g:106:1: ruleBlock returns [EObject current=null] : this_Block_Impl_0= ruleBlock_Impl ;
    public final EObject ruleBlock() throws RecognitionException {
        EObject current = null;

        EObject this_Block_Impl_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:112:2: (this_Block_Impl_0= ruleBlock_Impl )
            // InternalExtendedCCSL.g:113:2: this_Block_Impl_0= ruleBlock_Impl
            {
            if ( state.backtracking==0 ) {

              		/* */
              	
            }
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getBlockAccess().getBlock_ImplParserRuleCall());
              	
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_Block_Impl_0=ruleBlock_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_Block_Impl_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlock"


    // $ANTLR start "entryRuleClockConstraintSystem_Impl"
    // InternalExtendedCCSL.g:127:1: entryRuleClockConstraintSystem_Impl returns [EObject current=null] : iv_ruleClockConstraintSystem_Impl= ruleClockConstraintSystem_Impl EOF ;
    public final EObject entryRuleClockConstraintSystem_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockConstraintSystem_Impl = null;



        	UnorderedGroupState myUnorderedGroupState = getUnorderedGroupHelper().snapShot(
        	grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup()
        	);

        try {
            // InternalExtendedCCSL.g:131:2: (iv_ruleClockConstraintSystem_Impl= ruleClockConstraintSystem_Impl EOF )
            // InternalExtendedCCSL.g:132:2: iv_ruleClockConstraintSystem_Impl= ruleClockConstraintSystem_Impl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockConstraintSystem_ImplRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockConstraintSystem_Impl=ruleClockConstraintSystem_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockConstraintSystem_Impl; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myUnorderedGroupState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleClockConstraintSystem_Impl"


    // $ANTLR start "ruleClockConstraintSystem_Impl"
    // InternalExtendedCCSL.g:141:1: ruleClockConstraintSystem_Impl returns [EObject current=null] : ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?) ) ) ;
    public final EObject ruleClockConstraintSystem_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        EObject lv_imports_6_0 = null;

        EObject lv_subBlock_10_0 = null;

        EObject lv_dataTypes_13_0 = null;

        EObject lv_dataTypes_15_0 = null;



        	enterRule();
        	UnorderedGroupState myUnorderedGroupState = getUnorderedGroupHelper().snapShot(
        	grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup()
        	);

        try {
            // InternalExtendedCCSL.g:150:2: ( ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?) ) ) )
            // InternalExtendedCCSL.g:151:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?) ) )
            {
            // InternalExtendedCCSL.g:151:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?) ) )
            // InternalExtendedCCSL.g:152:3: ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?) )
            {
            // InternalExtendedCCSL.g:152:3: ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?) )
            // InternalExtendedCCSL.g:153:4: ( ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?)
            {
            getUnorderedGroupHelper().enter(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup());
            // InternalExtendedCCSL.g:156:4: ( ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?)
            // InternalExtendedCCSL.g:157:5: ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+ {...}?
            {
            // InternalExtendedCCSL.g:157:5: ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )+
            int cnt6=0;
            loop6:
            do {
                int alt6=4;
                int LA6_0 = input.LA(1);

                if ( LA6_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 0) ) {
                    alt6=1;
                }
                else if ( (LA6_0==15) ) {
                    int LA6_3 = input.LA(2);

                    if ( LA6_3 == EOF && getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2) ) {
                        alt6=3;
                    }
                    else if ( LA6_3 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2) ) {
                        alt6=3;
                    }
                    else if ( LA6_3 == 15 && getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2) ) {
                        alt6=3;
                    }
                    else if ( LA6_3 == 16 && getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 1) ) {
                        alt6=2;
                    }


                }


                switch (alt6) {
            	case 1 :
            	    // InternalExtendedCCSL.g:158:3: ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) )
            	    {
            	    // InternalExtendedCCSL.g:158:3: ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) )
            	    // InternalExtendedCCSL.g:159:4: {...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleClockConstraintSystem_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 0)");
            	    }
            	    // InternalExtendedCCSL.g:159:120: ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) )
            	    // InternalExtendedCCSL.g:160:5: ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 0);
            	    // InternalExtendedCCSL.g:163:8: ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) )
            	    // InternalExtendedCCSL.g:163:9: {...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleClockConstraintSystem_Impl", "true");
            	    }
            	    // InternalExtendedCCSL.g:163:18: (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? )
            	    // InternalExtendedCCSL.g:163:19: otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )?
            	    {
            	    otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_1, grammarAccess.getClockConstraintSystem_ImplAccess().getClockConstraintSystemKeyword_0_0());
            	      							
            	    }
            	    // InternalExtendedCCSL.g:167:8: ( (lv_name_2_0= RULE_ID ) )
            	    // InternalExtendedCCSL.g:168:9: (lv_name_2_0= RULE_ID )
            	    {
            	    // InternalExtendedCCSL.g:168:9: (lv_name_2_0= RULE_ID )
            	    // InternalExtendedCCSL.g:169:10: lv_name_2_0= RULE_ID
            	    {
            	    lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      										newLeafNode(lv_name_2_0, grammarAccess.getClockConstraintSystem_ImplAccess().getNameIDTerminalRuleCall_0_1_0());
            	      									
            	    }
            	    if ( state.backtracking==0 ) {

            	      										if (current==null) {
            	      											current = createModelElement(grammarAccess.getClockConstraintSystem_ImplRule());
            	      										}
            	      										setWithLastConsumed(
            	      											current,
            	      											"name",
            	      											lv_name_2_0,
            	      											"org.eclipse.xtext.common.Terminals.ID");
            	      									
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,13,FollowSets000.FOLLOW_5); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_3, grammarAccess.getClockConstraintSystem_ImplAccess().getLeftCurlyBracketKeyword_0_2());
            	      							
            	    }
            	    // InternalExtendedCCSL.g:189:8: (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )?
            	    int alt2=2;
            	    int LA2_0 = input.LA(1);

            	    if ( (LA2_0==14) ) {
            	        alt2=1;
            	    }
            	    switch (alt2) {
            	        case 1 :
            	            // InternalExtendedCCSL.g:190:9: otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+
            	            {
            	            otherlv_4=(Token)match(input,14,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_4, grammarAccess.getClockConstraintSystem_ImplAccess().getImportsKeyword_0_3_0());
            	              								
            	            }
            	            otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_6); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_5, grammarAccess.getClockConstraintSystem_ImplAccess().getLeftCurlyBracketKeyword_0_3_1());
            	              								
            	            }
            	            // InternalExtendedCCSL.g:198:9: ( (lv_imports_6_0= ruleImport ) )+
            	            int cnt1=0;
            	            loop1:
            	            do {
            	                int alt1=2;
            	                int LA1_0 = input.LA(1);

            	                if ( (LA1_0==34) ) {
            	                    alt1=1;
            	                }


            	                switch (alt1) {
            	            	case 1 :
            	            	    // InternalExtendedCCSL.g:199:10: (lv_imports_6_0= ruleImport )
            	            	    {
            	            	    // InternalExtendedCCSL.g:199:10: (lv_imports_6_0= ruleImport )
            	            	    // InternalExtendedCCSL.g:200:11: lv_imports_6_0= ruleImport
            	            	    {
            	            	    if ( state.backtracking==0 ) {

            	            	      											newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getImportsImportParserRuleCall_0_3_2_0());
            	            	      										
            	            	    }
            	            	    pushFollow(FollowSets000.FOLLOW_7);
            	            	    lv_imports_6_0=ruleImport();

            	            	    state._fsp--;
            	            	    if (state.failed) return current;
            	            	    if ( state.backtracking==0 ) {

            	            	      											if (current==null) {
            	            	      												current = createModelElementForParent(grammarAccess.getClockConstraintSystem_ImplRule());
            	            	      											}
            	            	      											add(
            	            	      												current,
            	            	      												"imports",
            	            	      												lv_imports_6_0,
            	            	      												"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Import");
            	            	      											afterParserOrEnumRuleCall();
            	            	      										
            	            	    }

            	            	    }


            	            	    }
            	            	    break;

            	            	default :
            	            	    if ( cnt1 >= 1 ) break loop1;
            	            	    if (state.backtracking>0) {state.failed=true; return current;}
            	                        EarlyExitException eee =
            	                            new EarlyExitException(1, input);
            	                        throw eee;
            	                }
            	                cnt1++;
            	            } while (true);


            	            }
            	            break;

            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalExtendedCCSL.g:224:3: ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) )
            	    {
            	    // InternalExtendedCCSL.g:224:3: ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) )
            	    // InternalExtendedCCSL.g:225:4: {...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleClockConstraintSystem_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 1)");
            	    }
            	    // InternalExtendedCCSL.g:225:120: ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) )
            	    // InternalExtendedCCSL.g:226:5: ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 1);
            	    // InternalExtendedCCSL.g:229:8: ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) )
            	    // InternalExtendedCCSL.g:229:9: {...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleClockConstraintSystem_Impl", "true");
            	    }
            	    // InternalExtendedCCSL.g:229:18: (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? )
            	    // InternalExtendedCCSL.g:229:19: otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )?
            	    {
            	    otherlv_7=(Token)match(input,15,FollowSets000.FOLLOW_8); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_7, grammarAccess.getClockConstraintSystem_ImplAccess().getRightCurlyBracketKeyword_1_0());
            	      							
            	    }
            	    otherlv_8=(Token)match(input,16,FollowSets000.FOLLOW_9); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_8, grammarAccess.getClockConstraintSystem_ImplAccess().getEntryBlockKeyword_1_1());
            	      							
            	    }
            	    // InternalExtendedCCSL.g:237:8: ( ( ruleEString ) )
            	    // InternalExtendedCCSL.g:238:9: ( ruleEString )
            	    {
            	    // InternalExtendedCCSL.g:238:9: ( ruleEString )
            	    // InternalExtendedCCSL.g:239:10: ruleEString
            	    {
            	    if ( state.backtracking==0 ) {

            	      										/* */
            	      									
            	    }
            	    if ( state.backtracking==0 ) {

            	      										if (current==null) {
            	      											current = createModelElement(grammarAccess.getClockConstraintSystem_ImplRule());
            	      										}
            	      									
            	    }
            	    if ( state.backtracking==0 ) {

            	      										newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getSuperBlockBlockCrossReference_1_2_0());
            	      									
            	    }
            	    pushFollow(FollowSets000.FOLLOW_10);
            	    ruleEString();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      										afterParserOrEnumRuleCall();
            	      									
            	    }

            	    }


            	    }

            	    // InternalExtendedCCSL.g:256:8: ( (lv_subBlock_10_0= ruleBlock ) )*
            	    loop3:
            	    do {
            	        int alt3=2;
            	        int LA3_0 = input.LA(1);

            	        if ( (LA3_0==23) ) {
            	            alt3=1;
            	        }


            	        switch (alt3) {
            	    	case 1 :
            	    	    // InternalExtendedCCSL.g:257:9: (lv_subBlock_10_0= ruleBlock )
            	    	    {
            	    	    // InternalExtendedCCSL.g:257:9: (lv_subBlock_10_0= ruleBlock )
            	    	    // InternalExtendedCCSL.g:258:10: lv_subBlock_10_0= ruleBlock
            	    	    {
            	    	    if ( state.backtracking==0 ) {

            	    	      										newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getSubBlockBlockParserRuleCall_1_3_0());
            	    	      									
            	    	    }
            	    	    pushFollow(FollowSets000.FOLLOW_10);
            	    	    lv_subBlock_10_0=ruleBlock();

            	    	    state._fsp--;
            	    	    if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      										if (current==null) {
            	    	      											current = createModelElementForParent(grammarAccess.getClockConstraintSystem_ImplRule());
            	    	      										}
            	    	      										add(
            	    	      											current,
            	    	      											"subBlock",
            	    	      											lv_subBlock_10_0,
            	    	      											"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Block");
            	    	      										afterParserOrEnumRuleCall();
            	    	      									
            	    	    }

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop3;
            	        }
            	    } while (true);

            	    // InternalExtendedCCSL.g:275:8: (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )?
            	    int alt5=2;
            	    int LA5_0 = input.LA(1);

            	    if ( (LA5_0==17) ) {
            	        alt5=1;
            	    }
            	    switch (alt5) {
            	        case 1 :
            	            // InternalExtendedCCSL.g:276:9: otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}'
            	            {
            	            otherlv_11=(Token)match(input,17,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_11, grammarAccess.getClockConstraintSystem_ImplAccess().getDataTypesKeyword_1_4_0());
            	              								
            	            }
            	            otherlv_12=(Token)match(input,13,FollowSets000.FOLLOW_11); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_12, grammarAccess.getClockConstraintSystem_ImplAccess().getLeftCurlyBracketKeyword_1_4_1());
            	              								
            	            }
            	            // InternalExtendedCCSL.g:284:9: ( (lv_dataTypes_13_0= ruleType ) )
            	            // InternalExtendedCCSL.g:285:10: (lv_dataTypes_13_0= ruleType )
            	            {
            	            // InternalExtendedCCSL.g:285:10: (lv_dataTypes_13_0= ruleType )
            	            // InternalExtendedCCSL.g:286:11: lv_dataTypes_13_0= ruleType
            	            {
            	            if ( state.backtracking==0 ) {

            	              											newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getDataTypesTypeParserRuleCall_1_4_2_0());
            	              										
            	            }
            	            pushFollow(FollowSets000.FOLLOW_12);
            	            lv_dataTypes_13_0=ruleType();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              											if (current==null) {
            	              												current = createModelElementForParent(grammarAccess.getClockConstraintSystem_ImplRule());
            	              											}
            	              											add(
            	              												current,
            	              												"dataTypes",
            	              												lv_dataTypes_13_0,
            	              												"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Type");
            	              											afterParserOrEnumRuleCall();
            	              										
            	            }

            	            }


            	            }

            	            // InternalExtendedCCSL.g:303:9: (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )*
            	            loop4:
            	            do {
            	                int alt4=2;
            	                int LA4_0 = input.LA(1);

            	                if ( (LA4_0==18) ) {
            	                    alt4=1;
            	                }


            	                switch (alt4) {
            	            	case 1 :
            	            	    // InternalExtendedCCSL.g:304:10: otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) )
            	            	    {
            	            	    otherlv_14=(Token)match(input,18,FollowSets000.FOLLOW_11); if (state.failed) return current;
            	            	    if ( state.backtracking==0 ) {

            	            	      										newLeafNode(otherlv_14, grammarAccess.getClockConstraintSystem_ImplAccess().getCommaKeyword_1_4_3_0());
            	            	      									
            	            	    }
            	            	    // InternalExtendedCCSL.g:308:10: ( (lv_dataTypes_15_0= ruleType ) )
            	            	    // InternalExtendedCCSL.g:309:11: (lv_dataTypes_15_0= ruleType )
            	            	    {
            	            	    // InternalExtendedCCSL.g:309:11: (lv_dataTypes_15_0= ruleType )
            	            	    // InternalExtendedCCSL.g:310:12: lv_dataTypes_15_0= ruleType
            	            	    {
            	            	    if ( state.backtracking==0 ) {

            	            	      												newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getDataTypesTypeParserRuleCall_1_4_3_1_0());
            	            	      											
            	            	    }
            	            	    pushFollow(FollowSets000.FOLLOW_12);
            	            	    lv_dataTypes_15_0=ruleType();

            	            	    state._fsp--;
            	            	    if (state.failed) return current;
            	            	    if ( state.backtracking==0 ) {

            	            	      												if (current==null) {
            	            	      													current = createModelElementForParent(grammarAccess.getClockConstraintSystem_ImplRule());
            	            	      												}
            	            	      												add(
            	            	      													current,
            	            	      													"dataTypes",
            	            	      													lv_dataTypes_15_0,
            	            	      													"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Type");
            	            	      												afterParserOrEnumRuleCall();
            	            	      											
            	            	    }

            	            	    }


            	            	    }


            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop4;
            	                }
            	            } while (true);

            	            otherlv_16=(Token)match(input,15,FollowSets000.FOLLOW_13); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              									newLeafNode(otherlv_16, grammarAccess.getClockConstraintSystem_ImplAccess().getRightCurlyBracketKeyword_1_4_4());
            	              								
            	            }

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup());

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalExtendedCCSL.g:339:3: ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) )
            	    {
            	    // InternalExtendedCCSL.g:339:3: ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) )
            	    // InternalExtendedCCSL.g:340:4: {...}? => ( ({...}? => (otherlv_17= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleClockConstraintSystem_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2)");
            	    }
            	    // InternalExtendedCCSL.g:340:120: ( ({...}? => (otherlv_17= '}' ) ) )
            	    // InternalExtendedCCSL.g:341:5: ({...}? => (otherlv_17= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2);
            	    // InternalExtendedCCSL.g:344:8: ({...}? => (otherlv_17= '}' ) )
            	    // InternalExtendedCCSL.g:344:9: {...}? => (otherlv_17= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleClockConstraintSystem_Impl", "true");
            	    }
            	    // InternalExtendedCCSL.g:344:18: (otherlv_17= '}' )
            	    // InternalExtendedCCSL.g:344:19: otherlv_17= '}'
            	    {
            	    otherlv_17=(Token)match(input,15,FollowSets000.FOLLOW_13); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_17, grammarAccess.getClockConstraintSystem_ImplAccess().getRightCurlyBracketKeyword_2());
            	      							
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup()) ) {
                if (state.backtracking>0) {state.failed=true; return current;}
                throw new FailedPredicateException(input, "ruleClockConstraintSystem_Impl", "getUnorderedGroupHelper().canLeave(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup())");
            }

            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup());

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myUnorderedGroupState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleClockConstraintSystem_Impl"


    // $ANTLR start "entryRuleBlockTransition"
    // InternalExtendedCCSL.g:368:1: entryRuleBlockTransition returns [EObject current=null] : iv_ruleBlockTransition= ruleBlockTransition EOF ;
    public final EObject entryRuleBlockTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlockTransition = null;


        try {
            // InternalExtendedCCSL.g:368:56: (iv_ruleBlockTransition= ruleBlockTransition EOF )
            // InternalExtendedCCSL.g:369:2: iv_ruleBlockTransition= ruleBlockTransition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlockTransitionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBlockTransition=ruleBlockTransition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlockTransition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlockTransition"


    // $ANTLR start "ruleBlockTransition"
    // InternalExtendedCCSL.g:375:1: ruleBlockTransition returns [EObject current=null] : (otherlv_0= 'Transition' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= 'from' ( ( ruleEString ) ) otherlv_4= 'to' ( ( ruleEString ) ) otherlv_6= 'when' ( ( ruleEString ) ) ) ;
    public final EObject ruleBlockTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:381:2: ( (otherlv_0= 'Transition' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= 'from' ( ( ruleEString ) ) otherlv_4= 'to' ( ( ruleEString ) ) otherlv_6= 'when' ( ( ruleEString ) ) ) )
            // InternalExtendedCCSL.g:382:2: (otherlv_0= 'Transition' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= 'from' ( ( ruleEString ) ) otherlv_4= 'to' ( ( ruleEString ) ) otherlv_6= 'when' ( ( ruleEString ) ) )
            {
            // InternalExtendedCCSL.g:382:2: (otherlv_0= 'Transition' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= 'from' ( ( ruleEString ) ) otherlv_4= 'to' ( ( ruleEString ) ) otherlv_6= 'when' ( ( ruleEString ) ) )
            // InternalExtendedCCSL.g:383:3: otherlv_0= 'Transition' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= 'from' ( ( ruleEString ) ) otherlv_4= 'to' ( ( ruleEString ) ) otherlv_6= 'when' ( ( ruleEString ) )
            {
            otherlv_0=(Token)match(input,19,FollowSets000.FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getBlockTransitionAccess().getTransitionKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:387:3: ( (lv_name_1_0= RULE_ID ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalExtendedCCSL.g:388:4: (lv_name_1_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:388:4: (lv_name_1_0= RULE_ID )
                    // InternalExtendedCCSL.g:389:5: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_15); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_name_1_0, grammarAccess.getBlockTransitionAccess().getNameIDTerminalRuleCall_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getBlockTransitionRule());
                      					}
                      					setWithLastConsumed(
                      						current,
                      						"name",
                      						lv_name_1_0,
                      						"org.eclipse.xtext.common.Terminals.ID");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,20,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getBlockTransitionAccess().getFromKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:409:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:410:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:410:4: ( ruleEString )
            // InternalExtendedCCSL.g:411:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBlockTransitionRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBlockTransitionAccess().getSourceBlockCrossReference_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_16);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,21,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getBlockTransitionAccess().getToKeyword_4());
              		
            }
            // InternalExtendedCCSL.g:432:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:433:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:433:4: ( ruleEString )
            // InternalExtendedCCSL.g:434:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBlockTransitionRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBlockTransitionAccess().getTargetBlockCrossReference_5_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_17);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,22,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getBlockTransitionAccess().getWhenKeyword_6());
              		
            }
            // InternalExtendedCCSL.g:455:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:456:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:456:4: ( ruleEString )
            // InternalExtendedCCSL.g:457:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBlockTransitionRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBlockTransitionAccess().getOnClockCrossReference_7_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlockTransition"


    // $ANTLR start "entryRuleBlock_Impl"
    // InternalExtendedCCSL.g:478:1: entryRuleBlock_Impl returns [EObject current=null] : iv_ruleBlock_Impl= ruleBlock_Impl EOF ;
    public final EObject entryRuleBlock_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlock_Impl = null;


        try {
            // InternalExtendedCCSL.g:478:51: (iv_ruleBlock_Impl= ruleBlock_Impl EOF )
            // InternalExtendedCCSL.g:479:2: iv_ruleBlock_Impl= ruleBlock_Impl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlock_ImplRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBlock_Impl=ruleBlock_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlock_Impl; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlock_Impl"


    // $ANTLR start "ruleBlock_Impl"
    // InternalExtendedCCSL.g:485:1: ruleBlock_Impl returns [EObject current=null] : (otherlv_0= 'Block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'initialBlock' ( ( ruleEString ) ) )? ( ( ( (lv_subBlock_5_0= ruleBlock ) ) | ( (lv_blockTransitions_6_0= ruleBlockTransition ) ) | ( (lv_elements_7_0= ruleElement ) ) | ( (lv_relations_8_0= ruleRelation ) ) | ( (lv_expressions_9_0= ruleExpression ) ) | ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) ) ) (otherlv_11= ';' )? )* otherlv_12= '}' ) ;
    public final EObject ruleBlock_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        EObject lv_subBlock_5_0 = null;

        EObject lv_blockTransitions_6_0 = null;

        EObject lv_elements_7_0 = null;

        EObject lv_relations_8_0 = null;

        EObject lv_expressions_9_0 = null;

        EObject lv_classicalExpression_10_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:491:2: ( (otherlv_0= 'Block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'initialBlock' ( ( ruleEString ) ) )? ( ( ( (lv_subBlock_5_0= ruleBlock ) ) | ( (lv_blockTransitions_6_0= ruleBlockTransition ) ) | ( (lv_elements_7_0= ruleElement ) ) | ( (lv_relations_8_0= ruleRelation ) ) | ( (lv_expressions_9_0= ruleExpression ) ) | ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) ) ) (otherlv_11= ';' )? )* otherlv_12= '}' ) )
            // InternalExtendedCCSL.g:492:2: (otherlv_0= 'Block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'initialBlock' ( ( ruleEString ) ) )? ( ( ( (lv_subBlock_5_0= ruleBlock ) ) | ( (lv_blockTransitions_6_0= ruleBlockTransition ) ) | ( (lv_elements_7_0= ruleElement ) ) | ( (lv_relations_8_0= ruleRelation ) ) | ( (lv_expressions_9_0= ruleExpression ) ) | ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) ) ) (otherlv_11= ';' )? )* otherlv_12= '}' )
            {
            // InternalExtendedCCSL.g:492:2: (otherlv_0= 'Block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'initialBlock' ( ( ruleEString ) ) )? ( ( ( (lv_subBlock_5_0= ruleBlock ) ) | ( (lv_blockTransitions_6_0= ruleBlockTransition ) ) | ( (lv_elements_7_0= ruleElement ) ) | ( (lv_relations_8_0= ruleRelation ) ) | ( (lv_expressions_9_0= ruleExpression ) ) | ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) ) ) (otherlv_11= ';' )? )* otherlv_12= '}' )
            // InternalExtendedCCSL.g:493:3: otherlv_0= 'Block' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'initialBlock' ( ( ruleEString ) ) )? ( ( ( (lv_subBlock_5_0= ruleBlock ) ) | ( (lv_blockTransitions_6_0= ruleBlockTransition ) ) | ( (lv_elements_7_0= ruleElement ) ) | ( (lv_relations_8_0= ruleRelation ) ) | ( (lv_expressions_9_0= ruleExpression ) ) | ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) ) ) (otherlv_11= ';' )? )* otherlv_12= '}'
            {
            otherlv_0=(Token)match(input,23,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getBlock_ImplAccess().getBlockKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:497:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalExtendedCCSL.g:498:4: (lv_name_1_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:498:4: (lv_name_1_0= RULE_ID )
            // InternalExtendedCCSL.g:499:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getBlock_ImplAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBlock_ImplRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FollowSets000.FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getBlock_ImplAccess().getLeftCurlyBracketKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:519:3: (otherlv_3= 'initialBlock' ( ( ruleEString ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==24) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalExtendedCCSL.g:520:4: otherlv_3= 'initialBlock' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,24,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getBlock_ImplAccess().getInitialBlockKeyword_3_0());
                      			
                    }
                    // InternalExtendedCCSL.g:524:4: ( ( ruleEString ) )
                    // InternalExtendedCCSL.g:525:5: ( ruleEString )
                    {
                    // InternalExtendedCCSL.g:525:5: ( ruleEString )
                    // InternalExtendedCCSL.g:526:6: ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getBlock_ImplRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getBlock_ImplAccess().getInitialBlockBlockCrossReference_3_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_19);
                    ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalExtendedCCSL.g:544:3: ( ( ( (lv_subBlock_5_0= ruleBlock ) ) | ( (lv_blockTransitions_6_0= ruleBlockTransition ) ) | ( (lv_elements_7_0= ruleElement ) ) | ( (lv_relations_8_0= ruleRelation ) ) | ( (lv_expressions_9_0= ruleExpression ) ) | ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) ) ) (otherlv_11= ';' )? )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID||LA11_0==RULE_INT||LA11_0==19||LA11_0==23||(LA11_0>=26 && LA11_0<=27)||LA11_0==32||LA11_0==36||(LA11_0>=40 && LA11_0<=43)||LA11_0==48||LA11_0==51||(LA11_0>=53 && LA11_0<=54)||(LA11_0>=57 && LA11_0<=58)||(LA11_0>=61 && LA11_0<=83)||LA11_0==85||(LA11_0>=87 && LA11_0<=89)||LA11_0==91) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalExtendedCCSL.g:545:4: ( ( (lv_subBlock_5_0= ruleBlock ) ) | ( (lv_blockTransitions_6_0= ruleBlockTransition ) ) | ( (lv_elements_7_0= ruleElement ) ) | ( (lv_relations_8_0= ruleRelation ) ) | ( (lv_expressions_9_0= ruleExpression ) ) | ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) ) ) (otherlv_11= ';' )?
            	    {
            	    // InternalExtendedCCSL.g:545:4: ( ( (lv_subBlock_5_0= ruleBlock ) ) | ( (lv_blockTransitions_6_0= ruleBlockTransition ) ) | ( (lv_elements_7_0= ruleElement ) ) | ( (lv_relations_8_0= ruleRelation ) ) | ( (lv_expressions_9_0= ruleExpression ) ) | ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) ) )
            	    int alt9=6;
            	    switch ( input.LA(1) ) {
            	    case 23:
            	        {
            	        alt9=1;
            	        }
            	        break;
            	    case 19:
            	        {
            	        alt9=2;
            	        }
            	        break;
            	    case RULE_ID:
            	    case RULE_INT:
            	    case 36:
            	    case 40:
            	    case 41:
            	    case 42:
            	    case 43:
            	        {
            	        alt9=3;
            	        }
            	        break;
            	    case 26:
            	    case 27:
            	        {
            	        alt9=4;
            	        }
            	        break;
            	    case 32:
            	        {
            	        alt9=5;
            	        }
            	        break;
            	    case 48:
            	    case 51:
            	    case 53:
            	    case 54:
            	    case 57:
            	    case 58:
            	    case 61:
            	    case 62:
            	    case 63:
            	    case 64:
            	    case 65:
            	    case 66:
            	    case 67:
            	    case 68:
            	    case 69:
            	    case 70:
            	    case 71:
            	    case 72:
            	    case 73:
            	    case 74:
            	    case 75:
            	    case 76:
            	    case 77:
            	    case 78:
            	    case 79:
            	    case 80:
            	    case 81:
            	    case 82:
            	    case 83:
            	    case 85:
            	    case 87:
            	    case 88:
            	    case 89:
            	    case 91:
            	        {
            	        alt9=6;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 9, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt9) {
            	        case 1 :
            	            // InternalExtendedCCSL.g:546:5: ( (lv_subBlock_5_0= ruleBlock ) )
            	            {
            	            // InternalExtendedCCSL.g:546:5: ( (lv_subBlock_5_0= ruleBlock ) )
            	            // InternalExtendedCCSL.g:547:6: (lv_subBlock_5_0= ruleBlock )
            	            {
            	            // InternalExtendedCCSL.g:547:6: (lv_subBlock_5_0= ruleBlock )
            	            // InternalExtendedCCSL.g:548:7: lv_subBlock_5_0= ruleBlock
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getBlock_ImplAccess().getSubBlockBlockParserRuleCall_4_0_0_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_20);
            	            lv_subBlock_5_0=ruleBlock();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getBlock_ImplRule());
            	              							}
            	              							add(
            	              								current,
            	              								"subBlock",
            	              								lv_subBlock_5_0,
            	              								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Block");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalExtendedCCSL.g:566:5: ( (lv_blockTransitions_6_0= ruleBlockTransition ) )
            	            {
            	            // InternalExtendedCCSL.g:566:5: ( (lv_blockTransitions_6_0= ruleBlockTransition ) )
            	            // InternalExtendedCCSL.g:567:6: (lv_blockTransitions_6_0= ruleBlockTransition )
            	            {
            	            // InternalExtendedCCSL.g:567:6: (lv_blockTransitions_6_0= ruleBlockTransition )
            	            // InternalExtendedCCSL.g:568:7: lv_blockTransitions_6_0= ruleBlockTransition
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getBlock_ImplAccess().getBlockTransitionsBlockTransitionParserRuleCall_4_0_1_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_20);
            	            lv_blockTransitions_6_0=ruleBlockTransition();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getBlock_ImplRule());
            	              							}
            	              							add(
            	              								current,
            	              								"blockTransitions",
            	              								lv_blockTransitions_6_0,
            	              								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.BlockTransition");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 3 :
            	            // InternalExtendedCCSL.g:586:5: ( (lv_elements_7_0= ruleElement ) )
            	            {
            	            // InternalExtendedCCSL.g:586:5: ( (lv_elements_7_0= ruleElement ) )
            	            // InternalExtendedCCSL.g:587:6: (lv_elements_7_0= ruleElement )
            	            {
            	            // InternalExtendedCCSL.g:587:6: (lv_elements_7_0= ruleElement )
            	            // InternalExtendedCCSL.g:588:7: lv_elements_7_0= ruleElement
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getBlock_ImplAccess().getElementsElementParserRuleCall_4_0_2_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_20);
            	            lv_elements_7_0=ruleElement();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getBlock_ImplRule());
            	              							}
            	              							add(
            	              								current,
            	              								"elements",
            	              								lv_elements_7_0,
            	              								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Element");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 4 :
            	            // InternalExtendedCCSL.g:606:5: ( (lv_relations_8_0= ruleRelation ) )
            	            {
            	            // InternalExtendedCCSL.g:606:5: ( (lv_relations_8_0= ruleRelation ) )
            	            // InternalExtendedCCSL.g:607:6: (lv_relations_8_0= ruleRelation )
            	            {
            	            // InternalExtendedCCSL.g:607:6: (lv_relations_8_0= ruleRelation )
            	            // InternalExtendedCCSL.g:608:7: lv_relations_8_0= ruleRelation
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getBlock_ImplAccess().getRelationsRelationParserRuleCall_4_0_3_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_20);
            	            lv_relations_8_0=ruleRelation();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getBlock_ImplRule());
            	              							}
            	              							add(
            	              								current,
            	              								"relations",
            	              								lv_relations_8_0,
            	              								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Relation");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 5 :
            	            // InternalExtendedCCSL.g:626:5: ( (lv_expressions_9_0= ruleExpression ) )
            	            {
            	            // InternalExtendedCCSL.g:626:5: ( (lv_expressions_9_0= ruleExpression ) )
            	            // InternalExtendedCCSL.g:627:6: (lv_expressions_9_0= ruleExpression )
            	            {
            	            // InternalExtendedCCSL.g:627:6: (lv_expressions_9_0= ruleExpression )
            	            // InternalExtendedCCSL.g:628:7: lv_expressions_9_0= ruleExpression
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getBlock_ImplAccess().getExpressionsExpressionParserRuleCall_4_0_4_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_20);
            	            lv_expressions_9_0=ruleExpression();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getBlock_ImplRule());
            	              							}
            	              							add(
            	              								current,
            	              								"expressions",
            	              								lv_expressions_9_0,
            	              								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Expression");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 6 :
            	            // InternalExtendedCCSL.g:646:5: ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) )
            	            {
            	            // InternalExtendedCCSL.g:646:5: ( (lv_classicalExpression_10_0= ruleClassicalExpression0 ) )
            	            // InternalExtendedCCSL.g:647:6: (lv_classicalExpression_10_0= ruleClassicalExpression0 )
            	            {
            	            // InternalExtendedCCSL.g:647:6: (lv_classicalExpression_10_0= ruleClassicalExpression0 )
            	            // InternalExtendedCCSL.g:648:7: lv_classicalExpression_10_0= ruleClassicalExpression0
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getBlock_ImplAccess().getClassicalExpressionClassicalExpression0ParserRuleCall_4_0_5_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_20);
            	            lv_classicalExpression_10_0=ruleClassicalExpression0();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getBlock_ImplRule());
            	              							}
            	              							add(
            	              								current,
            	              								"classicalExpression",
            	              								lv_classicalExpression_10_0,
            	              								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.ClassicalExpression0");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    // InternalExtendedCCSL.g:666:4: (otherlv_11= ';' )?
            	    int alt10=2;
            	    int LA10_0 = input.LA(1);

            	    if ( (LA10_0==25) ) {
            	        alt10=1;
            	    }
            	    switch (alt10) {
            	        case 1 :
            	            // InternalExtendedCCSL.g:667:5: otherlv_11= ';'
            	            {
            	            otherlv_11=(Token)match(input,25,FollowSets000.FOLLOW_19); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              					newLeafNode(otherlv_11, grammarAccess.getBlock_ImplAccess().getSemicolonKeyword_4_1());
            	              				
            	            }

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_12=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_12, grammarAccess.getBlock_ImplAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlock_Impl"


    // $ANTLR start "entryRuleRelation"
    // InternalExtendedCCSL.g:681:1: entryRuleRelation returns [EObject current=null] : iv_ruleRelation= ruleRelation EOF ;
    public final EObject entryRuleRelation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelation = null;


        try {
            // InternalExtendedCCSL.g:681:49: (iv_ruleRelation= ruleRelation EOF )
            // InternalExtendedCCSL.g:682:2: iv_ruleRelation= ruleRelation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRelation=ruleRelation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelation; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // InternalExtendedCCSL.g:688:1: ruleRelation returns [EObject current=null] : ( ( (lv_isAnAssertion_0_0= 'assert' ) )? otherlv_1= 'Relation' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' otherlv_6= '(' ( ( (lv_bindings_7_0= ruleBinding ) ) (otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) ) )* )? otherlv_10= ')' ) ;
    public final EObject ruleRelation() throws RecognitionException {
        EObject current = null;

        Token lv_isAnAssertion_0_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_bindings_7_0 = null;

        EObject lv_bindings_9_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:694:2: ( ( ( (lv_isAnAssertion_0_0= 'assert' ) )? otherlv_1= 'Relation' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' otherlv_6= '(' ( ( (lv_bindings_7_0= ruleBinding ) ) (otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) ) )* )? otherlv_10= ')' ) )
            // InternalExtendedCCSL.g:695:2: ( ( (lv_isAnAssertion_0_0= 'assert' ) )? otherlv_1= 'Relation' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' otherlv_6= '(' ( ( (lv_bindings_7_0= ruleBinding ) ) (otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) ) )* )? otherlv_10= ')' )
            {
            // InternalExtendedCCSL.g:695:2: ( ( (lv_isAnAssertion_0_0= 'assert' ) )? otherlv_1= 'Relation' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' otherlv_6= '(' ( ( (lv_bindings_7_0= ruleBinding ) ) (otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) ) )* )? otherlv_10= ')' )
            // InternalExtendedCCSL.g:696:3: ( (lv_isAnAssertion_0_0= 'assert' ) )? otherlv_1= 'Relation' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' otherlv_6= '(' ( ( (lv_bindings_7_0= ruleBinding ) ) (otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) ) )* )? otherlv_10= ')'
            {
            // InternalExtendedCCSL.g:696:3: ( (lv_isAnAssertion_0_0= 'assert' ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==26) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalExtendedCCSL.g:697:4: (lv_isAnAssertion_0_0= 'assert' )
                    {
                    // InternalExtendedCCSL.g:697:4: (lv_isAnAssertion_0_0= 'assert' )
                    // InternalExtendedCCSL.g:698:5: lv_isAnAssertion_0_0= 'assert'
                    {
                    lv_isAnAssertion_0_0=(Token)match(input,26,FollowSets000.FOLLOW_21); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_isAnAssertion_0_0, grammarAccess.getRelationAccess().getIsAnAssertionAssertKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getRelationRule());
                      					}
                      					setWithLastConsumed(current, "isAnAssertion", lv_isAnAssertion_0_0 != null, "assert");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,27,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRelationAccess().getRelationKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:714:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:715:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:715:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:716:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getRelationAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRelationRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,28,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getRelationAccess().getLeftSquareBracketKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:736:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:737:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:737:4: ( ruleEString )
            // InternalExtendedCCSL.g:738:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRelationRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRelationAccess().getTypeRelationDeclarationCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_23);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_5=(Token)match(input,29,FollowSets000.FOLLOW_24); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getRelationAccess().getRightSquareBracketKeyword_5());
              		
            }
            otherlv_6=(Token)match(input,30,FollowSets000.FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRelationAccess().getLeftParenthesisKeyword_6());
              		
            }
            // InternalExtendedCCSL.g:763:3: ( ( (lv_bindings_7_0= ruleBinding ) ) (otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) ) )* )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>=RULE_ID && LA14_0<=RULE_STRING)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalExtendedCCSL.g:764:4: ( (lv_bindings_7_0= ruleBinding ) ) (otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) ) )*
                    {
                    // InternalExtendedCCSL.g:764:4: ( (lv_bindings_7_0= ruleBinding ) )
                    // InternalExtendedCCSL.g:765:5: (lv_bindings_7_0= ruleBinding )
                    {
                    // InternalExtendedCCSL.g:765:5: (lv_bindings_7_0= ruleBinding )
                    // InternalExtendedCCSL.g:766:6: lv_bindings_7_0= ruleBinding
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getRelationAccess().getBindingsBindingParserRuleCall_7_0_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_26);
                    lv_bindings_7_0=ruleBinding();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getRelationRule());
                      						}
                      						add(
                      							current,
                      							"bindings",
                      							lv_bindings_7_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Binding");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalExtendedCCSL.g:783:4: (otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) ) )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==18) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // InternalExtendedCCSL.g:784:5: otherlv_8= ',' ( (lv_bindings_9_0= ruleBinding ) )
                    	    {
                    	    otherlv_8=(Token)match(input,18,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_8, grammarAccess.getRelationAccess().getCommaKeyword_7_1_0());
                    	      				
                    	    }
                    	    // InternalExtendedCCSL.g:788:5: ( (lv_bindings_9_0= ruleBinding ) )
                    	    // InternalExtendedCCSL.g:789:6: (lv_bindings_9_0= ruleBinding )
                    	    {
                    	    // InternalExtendedCCSL.g:789:6: (lv_bindings_9_0= ruleBinding )
                    	    // InternalExtendedCCSL.g:790:7: lv_bindings_9_0= ruleBinding
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getRelationAccess().getBindingsBindingParserRuleCall_7_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_26);
                    	    lv_bindings_9_0=ruleBinding();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getRelationRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"bindings",
                    	      								lv_bindings_9_0,
                    	      								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Binding");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,31,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_10, grammarAccess.getRelationAccess().getRightParenthesisKeyword_8());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleExpression"
    // InternalExtendedCCSL.g:817:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalExtendedCCSL.g:817:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalExtendedCCSL.g:818:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalExtendedCCSL.g:824:1: ruleExpression returns [EObject current=null] : (otherlv_0= 'Expression' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( ( ruleEString ) ) otherlv_4= '(' ( ( (lv_bindings_5_0= ruleBinding ) ) (otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) ) )* )? otherlv_8= ')' ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_bindings_5_0 = null;

        EObject lv_bindings_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:830:2: ( (otherlv_0= 'Expression' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( ( ruleEString ) ) otherlv_4= '(' ( ( (lv_bindings_5_0= ruleBinding ) ) (otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) ) )* )? otherlv_8= ')' ) )
            // InternalExtendedCCSL.g:831:2: (otherlv_0= 'Expression' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( ( ruleEString ) ) otherlv_4= '(' ( ( (lv_bindings_5_0= ruleBinding ) ) (otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) ) )* )? otherlv_8= ')' )
            {
            // InternalExtendedCCSL.g:831:2: (otherlv_0= 'Expression' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( ( ruleEString ) ) otherlv_4= '(' ( ( (lv_bindings_5_0= ruleBinding ) ) (otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) ) )* )? otherlv_8= ')' )
            // InternalExtendedCCSL.g:832:3: otherlv_0= 'Expression' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( ( ruleEString ) ) otherlv_4= '(' ( ( (lv_bindings_5_0= ruleBinding ) ) (otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) ) )* )? otherlv_8= ')'
            {
            otherlv_0=(Token)match(input,32,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getExpressionAccess().getExpressionKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:836:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalExtendedCCSL.g:837:4: (lv_name_1_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:837:4: (lv_name_1_0= RULE_ID )
            // InternalExtendedCCSL.g:838:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getExpressionAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getExpressionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,33,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getExpressionAccess().getEqualsSignKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:858:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:859:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:859:4: ( ruleEString )
            // InternalExtendedCCSL.g:860:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getExpressionRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getExpressionAccess().getTypeExpressionDeclarationCrossReference_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_24);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,30,FollowSets000.FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getExpressionAccess().getLeftParenthesisKeyword_4());
              		
            }
            // InternalExtendedCCSL.g:881:3: ( ( (lv_bindings_5_0= ruleBinding ) ) (otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) ) )* )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=RULE_ID && LA16_0<=RULE_STRING)) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalExtendedCCSL.g:882:4: ( (lv_bindings_5_0= ruleBinding ) ) (otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) ) )*
                    {
                    // InternalExtendedCCSL.g:882:4: ( (lv_bindings_5_0= ruleBinding ) )
                    // InternalExtendedCCSL.g:883:5: (lv_bindings_5_0= ruleBinding )
                    {
                    // InternalExtendedCCSL.g:883:5: (lv_bindings_5_0= ruleBinding )
                    // InternalExtendedCCSL.g:884:6: lv_bindings_5_0= ruleBinding
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getExpressionAccess().getBindingsBindingParserRuleCall_5_0_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_26);
                    lv_bindings_5_0=ruleBinding();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getExpressionRule());
                      						}
                      						add(
                      							current,
                      							"bindings",
                      							lv_bindings_5_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Binding");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalExtendedCCSL.g:901:4: (otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==18) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // InternalExtendedCCSL.g:902:5: otherlv_6= ',' ( (lv_bindings_7_0= ruleBinding ) )
                    	    {
                    	    otherlv_6=(Token)match(input,18,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_6, grammarAccess.getExpressionAccess().getCommaKeyword_5_1_0());
                    	      				
                    	    }
                    	    // InternalExtendedCCSL.g:906:5: ( (lv_bindings_7_0= ruleBinding ) )
                    	    // InternalExtendedCCSL.g:907:6: (lv_bindings_7_0= ruleBinding )
                    	    {
                    	    // InternalExtendedCCSL.g:907:6: (lv_bindings_7_0= ruleBinding )
                    	    // InternalExtendedCCSL.g:908:7: lv_bindings_7_0= ruleBinding
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getExpressionAccess().getBindingsBindingParserRuleCall_5_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_26);
                    	    lv_bindings_7_0=ruleBinding();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getExpressionRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"bindings",
                    	      								lv_bindings_7_0,
                    	      								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Binding");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_8=(Token)match(input,31,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getExpressionAccess().getRightParenthesisKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleImport"
    // InternalExtendedCCSL.g:935:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalExtendedCCSL.g:935:47: (iv_ruleImport= ruleImport EOF )
            // InternalExtendedCCSL.g:936:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalExtendedCCSL.g:942:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_alias_3_0= ruleString0 ) ) otherlv_4= ';' ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_alias_3_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:948:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_alias_3_0= ruleString0 ) ) otherlv_4= ';' ) )
            // InternalExtendedCCSL.g:949:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_alias_3_0= ruleString0 ) ) otherlv_4= ';' )
            {
            // InternalExtendedCCSL.g:949:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_alias_3_0= ruleString0 ) ) otherlv_4= ';' )
            // InternalExtendedCCSL.g:950:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_alias_3_0= ruleString0 ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,34,FollowSets000.FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:954:3: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalExtendedCCSL.g:955:4: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalExtendedCCSL.g:955:4: (lv_importURI_1_0= RULE_STRING )
            // InternalExtendedCCSL.g:956:5: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getImportRule());
              					}
              					setWithLastConsumed(
              						current,
              						"importURI",
              						lv_importURI_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,35,FollowSets000.FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getImportAccess().getAsKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:976:3: ( (lv_alias_3_0= ruleString0 ) )
            // InternalExtendedCCSL.g:977:4: (lv_alias_3_0= ruleString0 )
            {
            // InternalExtendedCCSL.g:977:4: (lv_alias_3_0= ruleString0 )
            // InternalExtendedCCSL.g:978:5: lv_alias_3_0= ruleString0
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getImportAccess().getAliasString0ParserRuleCall_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_31);
            lv_alias_3_0=ruleString0();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getImportRule());
              					}
              					set(
              						current,
              						"alias",
              						lv_alias_3_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.String0");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getImportAccess().getSemicolonKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleClock"
    // InternalExtendedCCSL.g:1003:1: entryRuleClock returns [EObject current=null] : iv_ruleClock= ruleClock EOF ;
    public final EObject entryRuleClock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClock = null;


        try {
            // InternalExtendedCCSL.g:1003:46: (iv_ruleClock= ruleClock EOF )
            // InternalExtendedCCSL.g:1004:2: iv_ruleClock= ruleClock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClock=ruleClock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClock; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClock"


    // $ANTLR start "ruleClock"
    // InternalExtendedCCSL.g:1010:1: ruleClock returns [EObject current=null] : (otherlv_0= 'Clock' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? (otherlv_4= '->' ( (lv_tickingEvent_5_0= ruleEvent ) ) )? (otherlv_6= 'definingEvent' ( ( ruleEString ) ) )? ) ;
    public final EObject ruleClock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_tickingEvent_5_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:1016:2: ( (otherlv_0= 'Clock' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? (otherlv_4= '->' ( (lv_tickingEvent_5_0= ruleEvent ) ) )? (otherlv_6= 'definingEvent' ( ( ruleEString ) ) )? ) )
            // InternalExtendedCCSL.g:1017:2: (otherlv_0= 'Clock' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? (otherlv_4= '->' ( (lv_tickingEvent_5_0= ruleEvent ) ) )? (otherlv_6= 'definingEvent' ( ( ruleEString ) ) )? )
            {
            // InternalExtendedCCSL.g:1017:2: (otherlv_0= 'Clock' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? (otherlv_4= '->' ( (lv_tickingEvent_5_0= ruleEvent ) ) )? (otherlv_6= 'definingEvent' ( ( ruleEString ) ) )? )
            // InternalExtendedCCSL.g:1018:3: otherlv_0= 'Clock' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? (otherlv_4= '->' ( (lv_tickingEvent_5_0= ruleEvent ) ) )? (otherlv_6= 'definingEvent' ( ( ruleEString ) ) )?
            {
            otherlv_0=(Token)match(input,36,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getClockAccess().getClockKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:1022:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalExtendedCCSL.g:1023:4: (lv_name_1_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:1023:4: (lv_name_1_0= RULE_ID )
            // InternalExtendedCCSL.g:1024:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_32); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getClockAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getClockRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalExtendedCCSL.g:1040:3: (otherlv_2= ':' ( ( ruleEString ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==37) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalExtendedCCSL.g:1041:4: otherlv_2= ':' ( ( ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,37,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getClockAccess().getColonKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:1045:4: ( ( ruleEString ) )
                    // InternalExtendedCCSL.g:1046:5: ( ruleEString )
                    {
                    // InternalExtendedCCSL.g:1046:5: ( ruleEString )
                    // InternalExtendedCCSL.g:1047:6: ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getClockRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getClockAccess().getTypeTypeCrossReference_2_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_33);
                    ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalExtendedCCSL.g:1065:3: (otherlv_4= '->' ( (lv_tickingEvent_5_0= ruleEvent ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==38) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalExtendedCCSL.g:1066:4: otherlv_4= '->' ( (lv_tickingEvent_5_0= ruleEvent ) )
                    {
                    otherlv_4=(Token)match(input,38,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getClockAccess().getHyphenMinusGreaterThanSignKeyword_3_0());
                      			
                    }
                    // InternalExtendedCCSL.g:1070:4: ( (lv_tickingEvent_5_0= ruleEvent ) )
                    // InternalExtendedCCSL.g:1071:5: (lv_tickingEvent_5_0= ruleEvent )
                    {
                    // InternalExtendedCCSL.g:1071:5: (lv_tickingEvent_5_0= ruleEvent )
                    // InternalExtendedCCSL.g:1072:6: lv_tickingEvent_5_0= ruleEvent
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getClockAccess().getTickingEventEventParserRuleCall_3_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_34);
                    lv_tickingEvent_5_0=ruleEvent();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getClockRule());
                      						}
                      						set(
                      							current,
                      							"tickingEvent",
                      							lv_tickingEvent_5_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Event");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalExtendedCCSL.g:1090:3: (otherlv_6= 'definingEvent' ( ( ruleEString ) ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==39) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalExtendedCCSL.g:1091:4: otherlv_6= 'definingEvent' ( ( ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,39,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getClockAccess().getDefiningEventKeyword_4_0());
                      			
                    }
                    // InternalExtendedCCSL.g:1095:4: ( ( ruleEString ) )
                    // InternalExtendedCCSL.g:1096:5: ( ruleEString )
                    {
                    // InternalExtendedCCSL.g:1096:5: ( ruleEString )
                    // InternalExtendedCCSL.g:1097:6: ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getClockRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getClockAccess().getDefiningEventEventCrossReference_4_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClock"


    // $ANTLR start "entryRuleEvent"
    // InternalExtendedCCSL.g:1119:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // InternalExtendedCCSL.g:1119:46: (iv_ruleEvent= ruleEvent EOF )
            // InternalExtendedCCSL.g:1120:2: iv_ruleEvent= ruleEvent EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEventRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEvent=ruleEvent();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEvent; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalExtendedCCSL.g:1126:1: ruleEvent returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( ( ruleEString ) ) (otherlv_3= ',' ( ( ruleEString ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_kind_7_0= ruleEventKind ) ) )? ) ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Enumerator lv_kind_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:1132:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( ( ruleEString ) ) (otherlv_3= ',' ( ( ruleEString ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_kind_7_0= ruleEventKind ) ) )? ) )
            // InternalExtendedCCSL.g:1133:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( ( ruleEString ) ) (otherlv_3= ',' ( ( ruleEString ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_kind_7_0= ruleEventKind ) ) )? )
            {
            // InternalExtendedCCSL.g:1133:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( ( ruleEString ) ) (otherlv_3= ',' ( ( ruleEString ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_kind_7_0= ruleEventKind ) ) )? )
            // InternalExtendedCCSL.g:1134:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '(' ( ( ( ruleEString ) ) (otherlv_3= ',' ( ( ruleEString ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_kind_7_0= ruleEventKind ) ) )?
            {
            // InternalExtendedCCSL.g:1134:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalExtendedCCSL.g:1135:4: (lv_name_0_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:1135:4: (lv_name_0_0= RULE_ID )
            // InternalExtendedCCSL.g:1136:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_24); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_0_0, grammarAccess.getEventAccess().getNameIDTerminalRuleCall_0_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getEventRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_0_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_1=(Token)match(input,30,FollowSets000.FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getEventAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:1156:3: ( ( ( ruleEString ) ) (otherlv_3= ',' ( ( ruleEString ) ) )* )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=RULE_ID && LA21_0<=RULE_STRING)) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalExtendedCCSL.g:1157:4: ( ( ruleEString ) ) (otherlv_3= ',' ( ( ruleEString ) ) )*
                    {
                    // InternalExtendedCCSL.g:1157:4: ( ( ruleEString ) )
                    // InternalExtendedCCSL.g:1158:5: ( ruleEString )
                    {
                    // InternalExtendedCCSL.g:1158:5: ( ruleEString )
                    // InternalExtendedCCSL.g:1159:6: ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getEventRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getEventAccess().getReferencedObjectRefsEObjectCrossReference_2_0_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_26);
                    ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalExtendedCCSL.g:1176:4: (otherlv_3= ',' ( ( ruleEString ) ) )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==18) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalExtendedCCSL.g:1177:5: otherlv_3= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_3=(Token)match(input,18,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_3, grammarAccess.getEventAccess().getCommaKeyword_2_1_0());
                    	      				
                    	    }
                    	    // InternalExtendedCCSL.g:1181:5: ( ( ruleEString ) )
                    	    // InternalExtendedCCSL.g:1182:6: ( ruleEString )
                    	    {
                    	    // InternalExtendedCCSL.g:1182:6: ( ruleEString )
                    	    // InternalExtendedCCSL.g:1183:7: ruleEString
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							/* */
                    	      						
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElement(grammarAccess.getEventRule());
                    	      							}
                    	      						
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getEventAccess().getReferencedObjectRefsEObjectCrossReference_2_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_26);
                    	    ruleEString();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,31,FollowSets000.FOLLOW_35); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getEventAccess().getRightParenthesisKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:1206:3: (otherlv_6= ':' ( (lv_kind_7_0= ruleEventKind ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==37) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalExtendedCCSL.g:1207:4: otherlv_6= ':' ( (lv_kind_7_0= ruleEventKind ) )
                    {
                    otherlv_6=(Token)match(input,37,FollowSets000.FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getEventAccess().getColonKeyword_4_0());
                      			
                    }
                    // InternalExtendedCCSL.g:1211:4: ( (lv_kind_7_0= ruleEventKind ) )
                    // InternalExtendedCCSL.g:1212:5: (lv_kind_7_0= ruleEventKind )
                    {
                    // InternalExtendedCCSL.g:1212:5: (lv_kind_7_0= ruleEventKind )
                    // InternalExtendedCCSL.g:1213:6: lv_kind_7_0= ruleEventKind
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getEventAccess().getKindEventKindEnumRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_kind_7_0=ruleEventKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getEventRule());
                      						}
                      						set(
                      							current,
                      							"kind",
                      							lv_kind_7_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.EventKind");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleElement"
    // InternalExtendedCCSL.g:1235:1: entryRuleElement returns [EObject current=null] : iv_ruleElement= ruleElement EOF ;
    public final EObject entryRuleElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElement = null;


        try {
            // InternalExtendedCCSL.g:1235:48: (iv_ruleElement= ruleElement EOF )
            // InternalExtendedCCSL.g:1236:2: iv_ruleElement= ruleElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getElementRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleElement=ruleElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleElement; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // InternalExtendedCCSL.g:1242:1: ruleElement returns [EObject current=null] : (this_Clock_0= ruleClock | this_StringElement_1= ruleStringElement | this_IntegerElement_2= ruleIntegerElement | this_RealElement_3= ruleRealElement | this_SequenceElement_4= ruleSequenceElement ) ;
    public final EObject ruleElement() throws RecognitionException {
        EObject current = null;

        EObject this_Clock_0 = null;

        EObject this_StringElement_1 = null;

        EObject this_IntegerElement_2 = null;

        EObject this_RealElement_3 = null;

        EObject this_SequenceElement_4 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:1248:2: ( (this_Clock_0= ruleClock | this_StringElement_1= ruleStringElement | this_IntegerElement_2= ruleIntegerElement | this_RealElement_3= ruleRealElement | this_SequenceElement_4= ruleSequenceElement ) )
            // InternalExtendedCCSL.g:1249:2: (this_Clock_0= ruleClock | this_StringElement_1= ruleStringElement | this_IntegerElement_2= ruleIntegerElement | this_RealElement_3= ruleRealElement | this_SequenceElement_4= ruleSequenceElement )
            {
            // InternalExtendedCCSL.g:1249:2: (this_Clock_0= ruleClock | this_StringElement_1= ruleStringElement | this_IntegerElement_2= ruleIntegerElement | this_RealElement_3= ruleRealElement | this_SequenceElement_4= ruleSequenceElement )
            int alt23=5;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt23=1;
                }
                break;
            case 40:
                {
                alt23=2;
                }
                break;
            case RULE_ID:
            case RULE_INT:
            case 41:
                {
                alt23=3;
                }
                break;
            case 42:
                {
                alt23=4;
                }
                break;
            case 43:
                {
                alt23=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalExtendedCCSL.g:1250:3: this_Clock_0= ruleClock
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getElementAccess().getClockParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Clock_0=ruleClock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Clock_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:1262:3: this_StringElement_1= ruleStringElement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getElementAccess().getStringElementParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringElement_1=ruleStringElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_StringElement_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:1274:3: this_IntegerElement_2= ruleIntegerElement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getElementAccess().getIntegerElementParserRuleCall_2());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerElement_2=ruleIntegerElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntegerElement_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalExtendedCCSL.g:1286:3: this_RealElement_3= ruleRealElement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getElementAccess().getRealElementParserRuleCall_3());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealElement_3=ruleRealElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealElement_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalExtendedCCSL.g:1298:3: this_SequenceElement_4= ruleSequenceElement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getElementAccess().getSequenceElementParserRuleCall_4());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SequenceElement_4=ruleSequenceElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SequenceElement_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleClassicalExpression0"
    // InternalExtendedCCSL.g:1313:1: entryRuleClassicalExpression0 returns [EObject current=null] : iv_ruleClassicalExpression0= ruleClassicalExpression0 EOF ;
    public final EObject entryRuleClassicalExpression0() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassicalExpression0 = null;


        try {
            // InternalExtendedCCSL.g:1313:61: (iv_ruleClassicalExpression0= ruleClassicalExpression0 EOF )
            // InternalExtendedCCSL.g:1314:2: iv_ruleClassicalExpression0= ruleClassicalExpression0 EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassicalExpression0Rule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClassicalExpression0=ruleClassicalExpression0();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassicalExpression0; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassicalExpression0"


    // $ANTLR start "ruleClassicalExpression0"
    // InternalExtendedCCSL.g:1320:1: ruleClassicalExpression0 returns [EObject current=null] : (this_BooleanRef_0= ruleBooleanRef | this_RealRef_1= ruleRealRef | this_IntegerRef_2= ruleIntegerRef | this_UnaryRealPlus_3= ruleUnaryRealPlus | this_UnaryRealMinus_4= ruleUnaryRealMinus | this_RealPlus_5= ruleRealPlus | this_RealMinus_6= ruleRealMinus | this_RealMultiply_7= ruleRealMultiply | this_UnaryIntPlus_8= ruleUnaryIntPlus | this_UnaryIntMinus_9= ruleUnaryIntMinus | this_IntPlus_10= ruleIntPlus | this_IntMinus_11= ruleIntMinus | this_IntMultiply_12= ruleIntMultiply | this_IntDivide_13= ruleIntDivide | this_Not_14= ruleNot | this_And_15= ruleAnd | this_Or_16= ruleOr | this_Xor_17= ruleXor | this_RealEqual_18= ruleRealEqual | this_RealInf_19= ruleRealInf | this_RealSup_20= ruleRealSup | this_IntEqual_21= ruleIntEqual | this_IntInf_22= ruleIntInf | this_IntSup_23= ruleIntSup | this_SeqIsEmpty_24= ruleSeqIsEmpty | this_SeqGetTail_25= ruleSeqGetTail | this_SeqGetHead_26= ruleSeqGetHead | this_SeqDecr_27= ruleSeqDecr | this_SeqSched_28= ruleSeqSched | this_BooleanVariableRef_29= ruleBooleanVariableRef | this_IntegerVariableRef_30= ruleIntegerVariableRef | this_RealVariableRef_31= ruleRealVariableRef | this_NumberSeqRef_32= ruleNumberSeqRef | this_NumberSeqVariableRef_33= ruleNumberSeqVariableRef ) ;
    public final EObject ruleClassicalExpression0() throws RecognitionException {
        EObject current = null;

        EObject this_BooleanRef_0 = null;

        EObject this_RealRef_1 = null;

        EObject this_IntegerRef_2 = null;

        EObject this_UnaryRealPlus_3 = null;

        EObject this_UnaryRealMinus_4 = null;

        EObject this_RealPlus_5 = null;

        EObject this_RealMinus_6 = null;

        EObject this_RealMultiply_7 = null;

        EObject this_UnaryIntPlus_8 = null;

        EObject this_UnaryIntMinus_9 = null;

        EObject this_IntPlus_10 = null;

        EObject this_IntMinus_11 = null;

        EObject this_IntMultiply_12 = null;

        EObject this_IntDivide_13 = null;

        EObject this_Not_14 = null;

        EObject this_And_15 = null;

        EObject this_Or_16 = null;

        EObject this_Xor_17 = null;

        EObject this_RealEqual_18 = null;

        EObject this_RealInf_19 = null;

        EObject this_RealSup_20 = null;

        EObject this_IntEqual_21 = null;

        EObject this_IntInf_22 = null;

        EObject this_IntSup_23 = null;

        EObject this_SeqIsEmpty_24 = null;

        EObject this_SeqGetTail_25 = null;

        EObject this_SeqGetHead_26 = null;

        EObject this_SeqDecr_27 = null;

        EObject this_SeqSched_28 = null;

        EObject this_BooleanVariableRef_29 = null;

        EObject this_IntegerVariableRef_30 = null;

        EObject this_RealVariableRef_31 = null;

        EObject this_NumberSeqRef_32 = null;

        EObject this_NumberSeqVariableRef_33 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:1326:2: ( (this_BooleanRef_0= ruleBooleanRef | this_RealRef_1= ruleRealRef | this_IntegerRef_2= ruleIntegerRef | this_UnaryRealPlus_3= ruleUnaryRealPlus | this_UnaryRealMinus_4= ruleUnaryRealMinus | this_RealPlus_5= ruleRealPlus | this_RealMinus_6= ruleRealMinus | this_RealMultiply_7= ruleRealMultiply | this_UnaryIntPlus_8= ruleUnaryIntPlus | this_UnaryIntMinus_9= ruleUnaryIntMinus | this_IntPlus_10= ruleIntPlus | this_IntMinus_11= ruleIntMinus | this_IntMultiply_12= ruleIntMultiply | this_IntDivide_13= ruleIntDivide | this_Not_14= ruleNot | this_And_15= ruleAnd | this_Or_16= ruleOr | this_Xor_17= ruleXor | this_RealEqual_18= ruleRealEqual | this_RealInf_19= ruleRealInf | this_RealSup_20= ruleRealSup | this_IntEqual_21= ruleIntEqual | this_IntInf_22= ruleIntInf | this_IntSup_23= ruleIntSup | this_SeqIsEmpty_24= ruleSeqIsEmpty | this_SeqGetTail_25= ruleSeqGetTail | this_SeqGetHead_26= ruleSeqGetHead | this_SeqDecr_27= ruleSeqDecr | this_SeqSched_28= ruleSeqSched | this_BooleanVariableRef_29= ruleBooleanVariableRef | this_IntegerVariableRef_30= ruleIntegerVariableRef | this_RealVariableRef_31= ruleRealVariableRef | this_NumberSeqRef_32= ruleNumberSeqRef | this_NumberSeqVariableRef_33= ruleNumberSeqVariableRef ) )
            // InternalExtendedCCSL.g:1327:2: (this_BooleanRef_0= ruleBooleanRef | this_RealRef_1= ruleRealRef | this_IntegerRef_2= ruleIntegerRef | this_UnaryRealPlus_3= ruleUnaryRealPlus | this_UnaryRealMinus_4= ruleUnaryRealMinus | this_RealPlus_5= ruleRealPlus | this_RealMinus_6= ruleRealMinus | this_RealMultiply_7= ruleRealMultiply | this_UnaryIntPlus_8= ruleUnaryIntPlus | this_UnaryIntMinus_9= ruleUnaryIntMinus | this_IntPlus_10= ruleIntPlus | this_IntMinus_11= ruleIntMinus | this_IntMultiply_12= ruleIntMultiply | this_IntDivide_13= ruleIntDivide | this_Not_14= ruleNot | this_And_15= ruleAnd | this_Or_16= ruleOr | this_Xor_17= ruleXor | this_RealEqual_18= ruleRealEqual | this_RealInf_19= ruleRealInf | this_RealSup_20= ruleRealSup | this_IntEqual_21= ruleIntEqual | this_IntInf_22= ruleIntInf | this_IntSup_23= ruleIntSup | this_SeqIsEmpty_24= ruleSeqIsEmpty | this_SeqGetTail_25= ruleSeqGetTail | this_SeqGetHead_26= ruleSeqGetHead | this_SeqDecr_27= ruleSeqDecr | this_SeqSched_28= ruleSeqSched | this_BooleanVariableRef_29= ruleBooleanVariableRef | this_IntegerVariableRef_30= ruleIntegerVariableRef | this_RealVariableRef_31= ruleRealVariableRef | this_NumberSeqRef_32= ruleNumberSeqRef | this_NumberSeqVariableRef_33= ruleNumberSeqVariableRef )
            {
            // InternalExtendedCCSL.g:1327:2: (this_BooleanRef_0= ruleBooleanRef | this_RealRef_1= ruleRealRef | this_IntegerRef_2= ruleIntegerRef | this_UnaryRealPlus_3= ruleUnaryRealPlus | this_UnaryRealMinus_4= ruleUnaryRealMinus | this_RealPlus_5= ruleRealPlus | this_RealMinus_6= ruleRealMinus | this_RealMultiply_7= ruleRealMultiply | this_UnaryIntPlus_8= ruleUnaryIntPlus | this_UnaryIntMinus_9= ruleUnaryIntMinus | this_IntPlus_10= ruleIntPlus | this_IntMinus_11= ruleIntMinus | this_IntMultiply_12= ruleIntMultiply | this_IntDivide_13= ruleIntDivide | this_Not_14= ruleNot | this_And_15= ruleAnd | this_Or_16= ruleOr | this_Xor_17= ruleXor | this_RealEqual_18= ruleRealEqual | this_RealInf_19= ruleRealInf | this_RealSup_20= ruleRealSup | this_IntEqual_21= ruleIntEqual | this_IntInf_22= ruleIntInf | this_IntSup_23= ruleIntSup | this_SeqIsEmpty_24= ruleSeqIsEmpty | this_SeqGetTail_25= ruleSeqGetTail | this_SeqGetHead_26= ruleSeqGetHead | this_SeqDecr_27= ruleSeqDecr | this_SeqSched_28= ruleSeqSched | this_BooleanVariableRef_29= ruleBooleanVariableRef | this_IntegerVariableRef_30= ruleIntegerVariableRef | this_RealVariableRef_31= ruleRealVariableRef | this_NumberSeqRef_32= ruleNumberSeqRef | this_NumberSeqVariableRef_33= ruleNumberSeqVariableRef )
            int alt24=34;
            switch ( input.LA(1) ) {
            case 48:
                {
                alt24=1;
                }
                break;
            case 51:
                {
                alt24=2;
                }
                break;
            case 53:
                {
                alt24=3;
                }
                break;
            case 54:
                {
                alt24=4;
                }
                break;
            case 57:
                {
                alt24=5;
                }
                break;
            case 58:
                {
                alt24=6;
                }
                break;
            case 61:
                {
                alt24=7;
                }
                break;
            case 62:
                {
                alt24=8;
                }
                break;
            case 63:
                {
                alt24=9;
                }
                break;
            case 64:
                {
                alt24=10;
                }
                break;
            case 65:
                {
                alt24=11;
                }
                break;
            case 66:
                {
                alt24=12;
                }
                break;
            case 67:
                {
                alt24=13;
                }
                break;
            case 68:
                {
                alt24=14;
                }
                break;
            case 69:
                {
                alt24=15;
                }
                break;
            case 70:
                {
                alt24=16;
                }
                break;
            case 71:
                {
                alt24=17;
                }
                break;
            case 72:
                {
                alt24=18;
                }
                break;
            case 73:
                {
                alt24=19;
                }
                break;
            case 74:
                {
                alt24=20;
                }
                break;
            case 75:
                {
                alt24=21;
                }
                break;
            case 76:
                {
                alt24=22;
                }
                break;
            case 77:
                {
                alt24=23;
                }
                break;
            case 78:
                {
                alt24=24;
                }
                break;
            case 79:
                {
                alt24=25;
                }
                break;
            case 80:
                {
                alt24=26;
                }
                break;
            case 81:
                {
                alt24=27;
                }
                break;
            case 82:
                {
                alt24=28;
                }
                break;
            case 83:
                {
                alt24=29;
                }
                break;
            case 85:
                {
                alt24=30;
                }
                break;
            case 87:
                {
                alt24=31;
                }
                break;
            case 88:
                {
                alt24=32;
                }
                break;
            case 89:
                {
                alt24=33;
                }
                break;
            case 91:
                {
                alt24=34;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // InternalExtendedCCSL.g:1328:3: this_BooleanRef_0= ruleBooleanRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getBooleanRefParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanRef_0=ruleBooleanRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BooleanRef_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:1340:3: this_RealRef_1= ruleRealRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getRealRefParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealRef_1=ruleRealRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealRef_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:1352:3: this_IntegerRef_2= ruleIntegerRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntegerRefParserRuleCall_2());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerRef_2=ruleIntegerRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntegerRef_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalExtendedCCSL.g:1364:3: this_UnaryRealPlus_3= ruleUnaryRealPlus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getUnaryRealPlusParserRuleCall_3());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_UnaryRealPlus_3=ruleUnaryRealPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryRealPlus_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalExtendedCCSL.g:1376:3: this_UnaryRealMinus_4= ruleUnaryRealMinus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getUnaryRealMinusParserRuleCall_4());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_UnaryRealMinus_4=ruleUnaryRealMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryRealMinus_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalExtendedCCSL.g:1388:3: this_RealPlus_5= ruleRealPlus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getRealPlusParserRuleCall_5());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealPlus_5=ruleRealPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealPlus_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalExtendedCCSL.g:1400:3: this_RealMinus_6= ruleRealMinus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getRealMinusParserRuleCall_6());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealMinus_6=ruleRealMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealMinus_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 8 :
                    // InternalExtendedCCSL.g:1412:3: this_RealMultiply_7= ruleRealMultiply
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getRealMultiplyParserRuleCall_7());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealMultiply_7=ruleRealMultiply();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealMultiply_7;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 9 :
                    // InternalExtendedCCSL.g:1424:3: this_UnaryIntPlus_8= ruleUnaryIntPlus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getUnaryIntPlusParserRuleCall_8());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_UnaryIntPlus_8=ruleUnaryIntPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryIntPlus_8;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 10 :
                    // InternalExtendedCCSL.g:1436:3: this_UnaryIntMinus_9= ruleUnaryIntMinus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getUnaryIntMinusParserRuleCall_9());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_UnaryIntMinus_9=ruleUnaryIntMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryIntMinus_9;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 11 :
                    // InternalExtendedCCSL.g:1448:3: this_IntPlus_10= ruleIntPlus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntPlusParserRuleCall_10());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntPlus_10=ruleIntPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntPlus_10;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 12 :
                    // InternalExtendedCCSL.g:1460:3: this_IntMinus_11= ruleIntMinus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntMinusParserRuleCall_11());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntMinus_11=ruleIntMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntMinus_11;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 13 :
                    // InternalExtendedCCSL.g:1472:3: this_IntMultiply_12= ruleIntMultiply
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntMultiplyParserRuleCall_12());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntMultiply_12=ruleIntMultiply();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntMultiply_12;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 14 :
                    // InternalExtendedCCSL.g:1484:3: this_IntDivide_13= ruleIntDivide
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntDivideParserRuleCall_13());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntDivide_13=ruleIntDivide();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntDivide_13;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 15 :
                    // InternalExtendedCCSL.g:1496:3: this_Not_14= ruleNot
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getNotParserRuleCall_14());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Not_14=ruleNot();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Not_14;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 16 :
                    // InternalExtendedCCSL.g:1508:3: this_And_15= ruleAnd
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getAndParserRuleCall_15());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_And_15=ruleAnd();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_And_15;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 17 :
                    // InternalExtendedCCSL.g:1520:3: this_Or_16= ruleOr
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getOrParserRuleCall_16());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Or_16=ruleOr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Or_16;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 18 :
                    // InternalExtendedCCSL.g:1532:3: this_Xor_17= ruleXor
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getXorParserRuleCall_17());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Xor_17=ruleXor();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Xor_17;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 19 :
                    // InternalExtendedCCSL.g:1544:3: this_RealEqual_18= ruleRealEqual
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getRealEqualParserRuleCall_18());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealEqual_18=ruleRealEqual();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealEqual_18;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 20 :
                    // InternalExtendedCCSL.g:1556:3: this_RealInf_19= ruleRealInf
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getRealInfParserRuleCall_19());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealInf_19=ruleRealInf();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealInf_19;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 21 :
                    // InternalExtendedCCSL.g:1568:3: this_RealSup_20= ruleRealSup
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getRealSupParserRuleCall_20());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealSup_20=ruleRealSup();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealSup_20;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 22 :
                    // InternalExtendedCCSL.g:1580:3: this_IntEqual_21= ruleIntEqual
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntEqualParserRuleCall_21());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntEqual_21=ruleIntEqual();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntEqual_21;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 23 :
                    // InternalExtendedCCSL.g:1592:3: this_IntInf_22= ruleIntInf
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntInfParserRuleCall_22());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntInf_22=ruleIntInf();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntInf_22;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 24 :
                    // InternalExtendedCCSL.g:1604:3: this_IntSup_23= ruleIntSup
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntSupParserRuleCall_23());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntSup_23=ruleIntSup();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntSup_23;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 25 :
                    // InternalExtendedCCSL.g:1616:3: this_SeqIsEmpty_24= ruleSeqIsEmpty
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getSeqIsEmptyParserRuleCall_24());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqIsEmpty_24=ruleSeqIsEmpty();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqIsEmpty_24;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 26 :
                    // InternalExtendedCCSL.g:1628:3: this_SeqGetTail_25= ruleSeqGetTail
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getSeqGetTailParserRuleCall_25());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqGetTail_25=ruleSeqGetTail();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqGetTail_25;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 27 :
                    // InternalExtendedCCSL.g:1640:3: this_SeqGetHead_26= ruleSeqGetHead
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getSeqGetHeadParserRuleCall_26());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqGetHead_26=ruleSeqGetHead();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqGetHead_26;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 28 :
                    // InternalExtendedCCSL.g:1652:3: this_SeqDecr_27= ruleSeqDecr
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getSeqDecrParserRuleCall_27());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqDecr_27=ruleSeqDecr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqDecr_27;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 29 :
                    // InternalExtendedCCSL.g:1664:3: this_SeqSched_28= ruleSeqSched
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getSeqSchedParserRuleCall_28());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqSched_28=ruleSeqSched();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqSched_28;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 30 :
                    // InternalExtendedCCSL.g:1676:3: this_BooleanVariableRef_29= ruleBooleanVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getBooleanVariableRefParserRuleCall_29());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanVariableRef_29=ruleBooleanVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BooleanVariableRef_29;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 31 :
                    // InternalExtendedCCSL.g:1688:3: this_IntegerVariableRef_30= ruleIntegerVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getIntegerVariableRefParserRuleCall_30());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerVariableRef_30=ruleIntegerVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntegerVariableRef_30;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 32 :
                    // InternalExtendedCCSL.g:1700:3: this_RealVariableRef_31= ruleRealVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getRealVariableRefParserRuleCall_31());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealVariableRef_31=ruleRealVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealVariableRef_31;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 33 :
                    // InternalExtendedCCSL.g:1712:3: this_NumberSeqRef_32= ruleNumberSeqRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getNumberSeqRefParserRuleCall_32());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NumberSeqRef_32=ruleNumberSeqRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_NumberSeqRef_32;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 34 :
                    // InternalExtendedCCSL.g:1724:3: this_NumberSeqVariableRef_33= ruleNumberSeqVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getClassicalExpression0Access().getNumberSeqVariableRefParserRuleCall_33());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NumberSeqVariableRef_33=ruleNumberSeqVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_NumberSeqVariableRef_33;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassicalExpression0"


    // $ANTLR start "entryRuleType"
    // InternalExtendedCCSL.g:1739:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalExtendedCCSL.g:1739:45: (iv_ruleType= ruleType EOF )
            // InternalExtendedCCSL.g:1740:2: iv_ruleType= ruleType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleType; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalExtendedCCSL.g:1746:1: ruleType returns [EObject current=null] : (this_String1_0= ruleString1 | this_Boolean_1= ruleBoolean | this_Integer_2= ruleInteger | this_Real_3= ruleReal | this_Char_4= ruleChar | this_Record_5= ruleRecord | this_SequenceType_6= ruleSequenceType | this_DiscreteClockType_Impl_7= ruleDiscreteClockType_Impl | this_DenseClockType_8= ruleDenseClockType | this_EnumerationType_9= ruleEnumerationType ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_String1_0 = null;

        EObject this_Boolean_1 = null;

        EObject this_Integer_2 = null;

        EObject this_Real_3 = null;

        EObject this_Char_4 = null;

        EObject this_Record_5 = null;

        EObject this_SequenceType_6 = null;

        EObject this_DiscreteClockType_Impl_7 = null;

        EObject this_DenseClockType_8 = null;

        EObject this_EnumerationType_9 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:1752:2: ( (this_String1_0= ruleString1 | this_Boolean_1= ruleBoolean | this_Integer_2= ruleInteger | this_Real_3= ruleReal | this_Char_4= ruleChar | this_Record_5= ruleRecord | this_SequenceType_6= ruleSequenceType | this_DiscreteClockType_Impl_7= ruleDiscreteClockType_Impl | this_DenseClockType_8= ruleDenseClockType | this_EnumerationType_9= ruleEnumerationType ) )
            // InternalExtendedCCSL.g:1753:2: (this_String1_0= ruleString1 | this_Boolean_1= ruleBoolean | this_Integer_2= ruleInteger | this_Real_3= ruleReal | this_Char_4= ruleChar | this_Record_5= ruleRecord | this_SequenceType_6= ruleSequenceType | this_DiscreteClockType_Impl_7= ruleDiscreteClockType_Impl | this_DenseClockType_8= ruleDenseClockType | this_EnumerationType_9= ruleEnumerationType )
            {
            // InternalExtendedCCSL.g:1753:2: (this_String1_0= ruleString1 | this_Boolean_1= ruleBoolean | this_Integer_2= ruleInteger | this_Real_3= ruleReal | this_Char_4= ruleChar | this_Record_5= ruleRecord | this_SequenceType_6= ruleSequenceType | this_DiscreteClockType_Impl_7= ruleDiscreteClockType_Impl | this_DenseClockType_8= ruleDenseClockType | this_EnumerationType_9= ruleEnumerationType )
            int alt25=10;
            switch ( input.LA(1) ) {
            case 92:
                {
                alt25=1;
                }
                break;
            case 93:
                {
                alt25=2;
                }
                break;
            case 94:
                {
                alt25=3;
                }
                break;
            case 95:
                {
                alt25=4;
                }
                break;
            case 96:
                {
                alt25=5;
                }
                break;
            case 97:
                {
                alt25=6;
                }
                break;
            case 98:
                {
                alt25=7;
                }
                break;
            case 99:
                {
                alt25=8;
                }
                break;
            case 100:
                {
                alt25=9;
                }
                break;
            case 103:
                {
                alt25=10;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }

            switch (alt25) {
                case 1 :
                    // InternalExtendedCCSL.g:1754:3: this_String1_0= ruleString1
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getString1ParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_String1_0=ruleString1();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_String1_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:1766:3: this_Boolean_1= ruleBoolean
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getBooleanParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Boolean_1=ruleBoolean();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Boolean_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:1778:3: this_Integer_2= ruleInteger
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getIntegerParserRuleCall_2());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Integer_2=ruleInteger();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Integer_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalExtendedCCSL.g:1790:3: this_Real_3= ruleReal
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getRealParserRuleCall_3());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Real_3=ruleReal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Real_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalExtendedCCSL.g:1802:3: this_Char_4= ruleChar
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getCharParserRuleCall_4());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Char_4=ruleChar();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Char_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalExtendedCCSL.g:1814:3: this_Record_5= ruleRecord
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getRecordParserRuleCall_5());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Record_5=ruleRecord();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Record_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalExtendedCCSL.g:1826:3: this_SequenceType_6= ruleSequenceType
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getSequenceTypeParserRuleCall_6());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SequenceType_6=ruleSequenceType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SequenceType_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 8 :
                    // InternalExtendedCCSL.g:1838:3: this_DiscreteClockType_Impl_7= ruleDiscreteClockType_Impl
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getDiscreteClockType_ImplParserRuleCall_7());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_DiscreteClockType_Impl_7=ruleDiscreteClockType_Impl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_DiscreteClockType_Impl_7;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 9 :
                    // InternalExtendedCCSL.g:1850:3: this_DenseClockType_8= ruleDenseClockType
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getDenseClockTypeParserRuleCall_8());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_DenseClockType_8=ruleDenseClockType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_DenseClockType_8;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 10 :
                    // InternalExtendedCCSL.g:1862:3: this_EnumerationType_9= ruleEnumerationType
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTypeAccess().getEnumerationTypeParserRuleCall_9());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_EnumerationType_9=ruleEnumerationType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_EnumerationType_9;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRulePrimitiveElement"
    // InternalExtendedCCSL.g:1877:1: entryRulePrimitiveElement returns [EObject current=null] : iv_rulePrimitiveElement= rulePrimitiveElement EOF ;
    public final EObject entryRulePrimitiveElement() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveElement = null;


        try {
            // InternalExtendedCCSL.g:1877:57: (iv_rulePrimitiveElement= rulePrimitiveElement EOF )
            // InternalExtendedCCSL.g:1878:2: iv_rulePrimitiveElement= rulePrimitiveElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPrimitiveElementRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePrimitiveElement=rulePrimitiveElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePrimitiveElement; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveElement"


    // $ANTLR start "rulePrimitiveElement"
    // InternalExtendedCCSL.g:1884:1: rulePrimitiveElement returns [EObject current=null] : (this_StringElement_0= ruleStringElement | this_IntegerElement_1= ruleIntegerElement | this_RealElement_2= ruleRealElement ) ;
    public final EObject rulePrimitiveElement() throws RecognitionException {
        EObject current = null;

        EObject this_StringElement_0 = null;

        EObject this_IntegerElement_1 = null;

        EObject this_RealElement_2 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:1890:2: ( (this_StringElement_0= ruleStringElement | this_IntegerElement_1= ruleIntegerElement | this_RealElement_2= ruleRealElement ) )
            // InternalExtendedCCSL.g:1891:2: (this_StringElement_0= ruleStringElement | this_IntegerElement_1= ruleIntegerElement | this_RealElement_2= ruleRealElement )
            {
            // InternalExtendedCCSL.g:1891:2: (this_StringElement_0= ruleStringElement | this_IntegerElement_1= ruleIntegerElement | this_RealElement_2= ruleRealElement )
            int alt26=3;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt26=1;
                }
                break;
            case RULE_ID:
            case RULE_INT:
            case 41:
                {
                alt26=2;
                }
                break;
            case 42:
                {
                alt26=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // InternalExtendedCCSL.g:1892:3: this_StringElement_0= ruleStringElement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getPrimitiveElementAccess().getStringElementParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringElement_0=ruleStringElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_StringElement_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:1904:3: this_IntegerElement_1= ruleIntegerElement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getPrimitiveElementAccess().getIntegerElementParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerElement_1=ruleIntegerElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntegerElement_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:1916:3: this_RealElement_2= ruleRealElement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getPrimitiveElementAccess().getRealElementParserRuleCall_2());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealElement_2=ruleRealElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealElement_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveElement"


    // $ANTLR start "entryRuleRealExpression"
    // InternalExtendedCCSL.g:1931:1: entryRuleRealExpression returns [EObject current=null] : iv_ruleRealExpression= ruleRealExpression EOF ;
    public final EObject entryRuleRealExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealExpression = null;


        try {
            // InternalExtendedCCSL.g:1931:55: (iv_ruleRealExpression= ruleRealExpression EOF )
            // InternalExtendedCCSL.g:1932:2: iv_ruleRealExpression= ruleRealExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealExpression=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealExpression"


    // $ANTLR start "ruleRealExpression"
    // InternalExtendedCCSL.g:1938:1: ruleRealExpression returns [EObject current=null] : (this_RealRef_0= ruleRealRef | this_UnaryRealPlus_1= ruleUnaryRealPlus | this_UnaryRealMinus_2= ruleUnaryRealMinus | this_RealPlus_3= ruleRealPlus | this_RealMinus_4= ruleRealMinus | this_RealMultiply_5= ruleRealMultiply | this_RealVariableRef_6= ruleRealVariableRef ) ;
    public final EObject ruleRealExpression() throws RecognitionException {
        EObject current = null;

        EObject this_RealRef_0 = null;

        EObject this_UnaryRealPlus_1 = null;

        EObject this_UnaryRealMinus_2 = null;

        EObject this_RealPlus_3 = null;

        EObject this_RealMinus_4 = null;

        EObject this_RealMultiply_5 = null;

        EObject this_RealVariableRef_6 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:1944:2: ( (this_RealRef_0= ruleRealRef | this_UnaryRealPlus_1= ruleUnaryRealPlus | this_UnaryRealMinus_2= ruleUnaryRealMinus | this_RealPlus_3= ruleRealPlus | this_RealMinus_4= ruleRealMinus | this_RealMultiply_5= ruleRealMultiply | this_RealVariableRef_6= ruleRealVariableRef ) )
            // InternalExtendedCCSL.g:1945:2: (this_RealRef_0= ruleRealRef | this_UnaryRealPlus_1= ruleUnaryRealPlus | this_UnaryRealMinus_2= ruleUnaryRealMinus | this_RealPlus_3= ruleRealPlus | this_RealMinus_4= ruleRealMinus | this_RealMultiply_5= ruleRealMultiply | this_RealVariableRef_6= ruleRealVariableRef )
            {
            // InternalExtendedCCSL.g:1945:2: (this_RealRef_0= ruleRealRef | this_UnaryRealPlus_1= ruleUnaryRealPlus | this_UnaryRealMinus_2= ruleUnaryRealMinus | this_RealPlus_3= ruleRealPlus | this_RealMinus_4= ruleRealMinus | this_RealMultiply_5= ruleRealMultiply | this_RealVariableRef_6= ruleRealVariableRef )
            int alt27=7;
            switch ( input.LA(1) ) {
            case 51:
                {
                alt27=1;
                }
                break;
            case 54:
                {
                alt27=2;
                }
                break;
            case 57:
                {
                alt27=3;
                }
                break;
            case 58:
                {
                alt27=4;
                }
                break;
            case 61:
                {
                alt27=5;
                }
                break;
            case 62:
                {
                alt27=6;
                }
                break;
            case 88:
                {
                alt27=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // InternalExtendedCCSL.g:1946:3: this_RealRef_0= ruleRealRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getRealExpressionAccess().getRealRefParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealRef_0=ruleRealRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealRef_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:1958:3: this_UnaryRealPlus_1= ruleUnaryRealPlus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getRealExpressionAccess().getUnaryRealPlusParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_UnaryRealPlus_1=ruleUnaryRealPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryRealPlus_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:1970:3: this_UnaryRealMinus_2= ruleUnaryRealMinus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getRealExpressionAccess().getUnaryRealMinusParserRuleCall_2());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_UnaryRealMinus_2=ruleUnaryRealMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryRealMinus_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalExtendedCCSL.g:1982:3: this_RealPlus_3= ruleRealPlus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getRealExpressionAccess().getRealPlusParserRuleCall_3());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealPlus_3=ruleRealPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealPlus_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalExtendedCCSL.g:1994:3: this_RealMinus_4= ruleRealMinus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getRealExpressionAccess().getRealMinusParserRuleCall_4());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealMinus_4=ruleRealMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealMinus_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalExtendedCCSL.g:2006:3: this_RealMultiply_5= ruleRealMultiply
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getRealExpressionAccess().getRealMultiplyParserRuleCall_5());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealMultiply_5=ruleRealMultiply();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealMultiply_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalExtendedCCSL.g:2018:3: this_RealVariableRef_6= ruleRealVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getRealExpressionAccess().getRealVariableRefParserRuleCall_6());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealVariableRef_6=ruleRealVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealVariableRef_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealExpression"


    // $ANTLR start "entryRuleIntegerExpression"
    // InternalExtendedCCSL.g:2033:1: entryRuleIntegerExpression returns [EObject current=null] : iv_ruleIntegerExpression= ruleIntegerExpression EOF ;
    public final EObject entryRuleIntegerExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerExpression = null;


        try {
            // InternalExtendedCCSL.g:2033:58: (iv_ruleIntegerExpression= ruleIntegerExpression EOF )
            // InternalExtendedCCSL.g:2034:2: iv_ruleIntegerExpression= ruleIntegerExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerExpression=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerExpression"


    // $ANTLR start "ruleIntegerExpression"
    // InternalExtendedCCSL.g:2040:1: ruleIntegerExpression returns [EObject current=null] : (this_IntegerRef_0= ruleIntegerRef | this_UnaryIntPlus_1= ruleUnaryIntPlus | this_UnaryIntMinus_2= ruleUnaryIntMinus | this_IntPlus_3= ruleIntPlus | this_IntMinus_4= ruleIntMinus | this_IntMultiply_5= ruleIntMultiply | this_IntDivide_6= ruleIntDivide | this_SeqGetHead_7= ruleSeqGetHead | this_IntegerVariableRef_8= ruleIntegerVariableRef ) ;
    public final EObject ruleIntegerExpression() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerRef_0 = null;

        EObject this_UnaryIntPlus_1 = null;

        EObject this_UnaryIntMinus_2 = null;

        EObject this_IntPlus_3 = null;

        EObject this_IntMinus_4 = null;

        EObject this_IntMultiply_5 = null;

        EObject this_IntDivide_6 = null;

        EObject this_SeqGetHead_7 = null;

        EObject this_IntegerVariableRef_8 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:2046:2: ( (this_IntegerRef_0= ruleIntegerRef | this_UnaryIntPlus_1= ruleUnaryIntPlus | this_UnaryIntMinus_2= ruleUnaryIntMinus | this_IntPlus_3= ruleIntPlus | this_IntMinus_4= ruleIntMinus | this_IntMultiply_5= ruleIntMultiply | this_IntDivide_6= ruleIntDivide | this_SeqGetHead_7= ruleSeqGetHead | this_IntegerVariableRef_8= ruleIntegerVariableRef ) )
            // InternalExtendedCCSL.g:2047:2: (this_IntegerRef_0= ruleIntegerRef | this_UnaryIntPlus_1= ruleUnaryIntPlus | this_UnaryIntMinus_2= ruleUnaryIntMinus | this_IntPlus_3= ruleIntPlus | this_IntMinus_4= ruleIntMinus | this_IntMultiply_5= ruleIntMultiply | this_IntDivide_6= ruleIntDivide | this_SeqGetHead_7= ruleSeqGetHead | this_IntegerVariableRef_8= ruleIntegerVariableRef )
            {
            // InternalExtendedCCSL.g:2047:2: (this_IntegerRef_0= ruleIntegerRef | this_UnaryIntPlus_1= ruleUnaryIntPlus | this_UnaryIntMinus_2= ruleUnaryIntMinus | this_IntPlus_3= ruleIntPlus | this_IntMinus_4= ruleIntMinus | this_IntMultiply_5= ruleIntMultiply | this_IntDivide_6= ruleIntDivide | this_SeqGetHead_7= ruleSeqGetHead | this_IntegerVariableRef_8= ruleIntegerVariableRef )
            int alt28=9;
            switch ( input.LA(1) ) {
            case 53:
                {
                alt28=1;
                }
                break;
            case 63:
                {
                alt28=2;
                }
                break;
            case 64:
                {
                alt28=3;
                }
                break;
            case 65:
                {
                alt28=4;
                }
                break;
            case 66:
                {
                alt28=5;
                }
                break;
            case 67:
                {
                alt28=6;
                }
                break;
            case 68:
                {
                alt28=7;
                }
                break;
            case 81:
                {
                alt28=8;
                }
                break;
            case 87:
                {
                alt28=9;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }

            switch (alt28) {
                case 1 :
                    // InternalExtendedCCSL.g:2048:3: this_IntegerRef_0= ruleIntegerRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getIntegerRefParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerRef_0=ruleIntegerRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntegerRef_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:2060:3: this_UnaryIntPlus_1= ruleUnaryIntPlus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getUnaryIntPlusParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_UnaryIntPlus_1=ruleUnaryIntPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryIntPlus_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:2072:3: this_UnaryIntMinus_2= ruleUnaryIntMinus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getUnaryIntMinusParserRuleCall_2());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_UnaryIntMinus_2=ruleUnaryIntMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryIntMinus_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalExtendedCCSL.g:2084:3: this_IntPlus_3= ruleIntPlus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getIntPlusParserRuleCall_3());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntPlus_3=ruleIntPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntPlus_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalExtendedCCSL.g:2096:3: this_IntMinus_4= ruleIntMinus
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getIntMinusParserRuleCall_4());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntMinus_4=ruleIntMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntMinus_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalExtendedCCSL.g:2108:3: this_IntMultiply_5= ruleIntMultiply
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getIntMultiplyParserRuleCall_5());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntMultiply_5=ruleIntMultiply();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntMultiply_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalExtendedCCSL.g:2120:3: this_IntDivide_6= ruleIntDivide
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getIntDivideParserRuleCall_6());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntDivide_6=ruleIntDivide();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntDivide_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 8 :
                    // InternalExtendedCCSL.g:2132:3: this_SeqGetHead_7= ruleSeqGetHead
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getSeqGetHeadParserRuleCall_7());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqGetHead_7=ruleSeqGetHead();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqGetHead_7;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 9 :
                    // InternalExtendedCCSL.g:2144:3: this_IntegerVariableRef_8= ruleIntegerVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getIntegerExpressionAccess().getIntegerVariableRefParserRuleCall_8());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerVariableRef_8=ruleIntegerVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntegerVariableRef_8;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerExpression"


    // $ANTLR start "entryRuleBooleanExpression"
    // InternalExtendedCCSL.g:2159:1: entryRuleBooleanExpression returns [EObject current=null] : iv_ruleBooleanExpression= ruleBooleanExpression EOF ;
    public final EObject entryRuleBooleanExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanExpression = null;


        try {
            // InternalExtendedCCSL.g:2159:58: (iv_ruleBooleanExpression= ruleBooleanExpression EOF )
            // InternalExtendedCCSL.g:2160:2: iv_ruleBooleanExpression= ruleBooleanExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanExpression=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanExpression"


    // $ANTLR start "ruleBooleanExpression"
    // InternalExtendedCCSL.g:2166:1: ruleBooleanExpression returns [EObject current=null] : (this_BooleanRef_0= ruleBooleanRef | this_Not_1= ruleNot | this_And_2= ruleAnd | this_Or_3= ruleOr | this_Xor_4= ruleXor | this_RealEqual_5= ruleRealEqual | this_RealInf_6= ruleRealInf | this_RealSup_7= ruleRealSup | this_IntEqual_8= ruleIntEqual | this_IntInf_9= ruleIntInf | this_IntSup_10= ruleIntSup | this_SeqIsEmpty_11= ruleSeqIsEmpty | this_BooleanVariableRef_12= ruleBooleanVariableRef ) ;
    public final EObject ruleBooleanExpression() throws RecognitionException {
        EObject current = null;

        EObject this_BooleanRef_0 = null;

        EObject this_Not_1 = null;

        EObject this_And_2 = null;

        EObject this_Or_3 = null;

        EObject this_Xor_4 = null;

        EObject this_RealEqual_5 = null;

        EObject this_RealInf_6 = null;

        EObject this_RealSup_7 = null;

        EObject this_IntEqual_8 = null;

        EObject this_IntInf_9 = null;

        EObject this_IntSup_10 = null;

        EObject this_SeqIsEmpty_11 = null;

        EObject this_BooleanVariableRef_12 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:2172:2: ( (this_BooleanRef_0= ruleBooleanRef | this_Not_1= ruleNot | this_And_2= ruleAnd | this_Or_3= ruleOr | this_Xor_4= ruleXor | this_RealEqual_5= ruleRealEqual | this_RealInf_6= ruleRealInf | this_RealSup_7= ruleRealSup | this_IntEqual_8= ruleIntEqual | this_IntInf_9= ruleIntInf | this_IntSup_10= ruleIntSup | this_SeqIsEmpty_11= ruleSeqIsEmpty | this_BooleanVariableRef_12= ruleBooleanVariableRef ) )
            // InternalExtendedCCSL.g:2173:2: (this_BooleanRef_0= ruleBooleanRef | this_Not_1= ruleNot | this_And_2= ruleAnd | this_Or_3= ruleOr | this_Xor_4= ruleXor | this_RealEqual_5= ruleRealEqual | this_RealInf_6= ruleRealInf | this_RealSup_7= ruleRealSup | this_IntEqual_8= ruleIntEqual | this_IntInf_9= ruleIntInf | this_IntSup_10= ruleIntSup | this_SeqIsEmpty_11= ruleSeqIsEmpty | this_BooleanVariableRef_12= ruleBooleanVariableRef )
            {
            // InternalExtendedCCSL.g:2173:2: (this_BooleanRef_0= ruleBooleanRef | this_Not_1= ruleNot | this_And_2= ruleAnd | this_Or_3= ruleOr | this_Xor_4= ruleXor | this_RealEqual_5= ruleRealEqual | this_RealInf_6= ruleRealInf | this_RealSup_7= ruleRealSup | this_IntEqual_8= ruleIntEqual | this_IntInf_9= ruleIntInf | this_IntSup_10= ruleIntSup | this_SeqIsEmpty_11= ruleSeqIsEmpty | this_BooleanVariableRef_12= ruleBooleanVariableRef )
            int alt29=13;
            switch ( input.LA(1) ) {
            case 48:
                {
                alt29=1;
                }
                break;
            case 69:
                {
                alt29=2;
                }
                break;
            case 70:
                {
                alt29=3;
                }
                break;
            case 71:
                {
                alt29=4;
                }
                break;
            case 72:
                {
                alt29=5;
                }
                break;
            case 73:
                {
                alt29=6;
                }
                break;
            case 74:
                {
                alt29=7;
                }
                break;
            case 75:
                {
                alt29=8;
                }
                break;
            case 76:
                {
                alt29=9;
                }
                break;
            case 77:
                {
                alt29=10;
                }
                break;
            case 78:
                {
                alt29=11;
                }
                break;
            case 79:
                {
                alt29=12;
                }
                break;
            case 85:
                {
                alt29=13;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }

            switch (alt29) {
                case 1 :
                    // InternalExtendedCCSL.g:2174:3: this_BooleanRef_0= ruleBooleanRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getBooleanRefParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanRef_0=ruleBooleanRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BooleanRef_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:2186:3: this_Not_1= ruleNot
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getNotParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Not_1=ruleNot();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Not_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:2198:3: this_And_2= ruleAnd
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getAndParserRuleCall_2());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_And_2=ruleAnd();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_And_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalExtendedCCSL.g:2210:3: this_Or_3= ruleOr
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getOrParserRuleCall_3());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Or_3=ruleOr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Or_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalExtendedCCSL.g:2222:3: this_Xor_4= ruleXor
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getXorParserRuleCall_4());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Xor_4=ruleXor();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Xor_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalExtendedCCSL.g:2234:3: this_RealEqual_5= ruleRealEqual
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getRealEqualParserRuleCall_5());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealEqual_5=ruleRealEqual();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealEqual_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalExtendedCCSL.g:2246:3: this_RealInf_6= ruleRealInf
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getRealInfParserRuleCall_6());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealInf_6=ruleRealInf();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealInf_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 8 :
                    // InternalExtendedCCSL.g:2258:3: this_RealSup_7= ruleRealSup
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getRealSupParserRuleCall_7());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RealSup_7=ruleRealSup();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_RealSup_7;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 9 :
                    // InternalExtendedCCSL.g:2270:3: this_IntEqual_8= ruleIntEqual
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getIntEqualParserRuleCall_8());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntEqual_8=ruleIntEqual();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntEqual_8;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 10 :
                    // InternalExtendedCCSL.g:2282:3: this_IntInf_9= ruleIntInf
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getIntInfParserRuleCall_9());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntInf_9=ruleIntInf();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntInf_9;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 11 :
                    // InternalExtendedCCSL.g:2294:3: this_IntSup_10= ruleIntSup
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getIntSupParserRuleCall_10());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntSup_10=ruleIntSup();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntSup_10;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 12 :
                    // InternalExtendedCCSL.g:2306:3: this_SeqIsEmpty_11= ruleSeqIsEmpty
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getSeqIsEmptyParserRuleCall_11());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqIsEmpty_11=ruleSeqIsEmpty();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqIsEmpty_11;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 13 :
                    // InternalExtendedCCSL.g:2318:3: this_BooleanVariableRef_12= ruleBooleanVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBooleanExpressionAccess().getBooleanVariableRefParserRuleCall_12());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanVariableRef_12=ruleBooleanVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BooleanVariableRef_12;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanExpression"


    // $ANTLR start "entryRuleSeqExpression"
    // InternalExtendedCCSL.g:2333:1: entryRuleSeqExpression returns [EObject current=null] : iv_ruleSeqExpression= ruleSeqExpression EOF ;
    public final EObject entryRuleSeqExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeqExpression = null;


        try {
            // InternalExtendedCCSL.g:2333:54: (iv_ruleSeqExpression= ruleSeqExpression EOF )
            // InternalExtendedCCSL.g:2334:2: iv_ruleSeqExpression= ruleSeqExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSeqExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSeqExpression=ruleSeqExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSeqExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeqExpression"


    // $ANTLR start "ruleSeqExpression"
    // InternalExtendedCCSL.g:2340:1: ruleSeqExpression returns [EObject current=null] : (this_SeqGetTail_0= ruleSeqGetTail | this_SeqDecr_1= ruleSeqDecr | this_SeqSched_2= ruleSeqSched | this_NumberSeqRef_3= ruleNumberSeqRef | this_NumberSeqVariableRef_4= ruleNumberSeqVariableRef ) ;
    public final EObject ruleSeqExpression() throws RecognitionException {
        EObject current = null;

        EObject this_SeqGetTail_0 = null;

        EObject this_SeqDecr_1 = null;

        EObject this_SeqSched_2 = null;

        EObject this_NumberSeqRef_3 = null;

        EObject this_NumberSeqVariableRef_4 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:2346:2: ( (this_SeqGetTail_0= ruleSeqGetTail | this_SeqDecr_1= ruleSeqDecr | this_SeqSched_2= ruleSeqSched | this_NumberSeqRef_3= ruleNumberSeqRef | this_NumberSeqVariableRef_4= ruleNumberSeqVariableRef ) )
            // InternalExtendedCCSL.g:2347:2: (this_SeqGetTail_0= ruleSeqGetTail | this_SeqDecr_1= ruleSeqDecr | this_SeqSched_2= ruleSeqSched | this_NumberSeqRef_3= ruleNumberSeqRef | this_NumberSeqVariableRef_4= ruleNumberSeqVariableRef )
            {
            // InternalExtendedCCSL.g:2347:2: (this_SeqGetTail_0= ruleSeqGetTail | this_SeqDecr_1= ruleSeqDecr | this_SeqSched_2= ruleSeqSched | this_NumberSeqRef_3= ruleNumberSeqRef | this_NumberSeqVariableRef_4= ruleNumberSeqVariableRef )
            int alt30=5;
            switch ( input.LA(1) ) {
            case 80:
                {
                alt30=1;
                }
                break;
            case 82:
                {
                alt30=2;
                }
                break;
            case 83:
                {
                alt30=3;
                }
                break;
            case 89:
                {
                alt30=4;
                }
                break;
            case 91:
                {
                alt30=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // InternalExtendedCCSL.g:2348:3: this_SeqGetTail_0= ruleSeqGetTail
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSeqExpressionAccess().getSeqGetTailParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqGetTail_0=ruleSeqGetTail();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqGetTail_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:2360:3: this_SeqDecr_1= ruleSeqDecr
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSeqExpressionAccess().getSeqDecrParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqDecr_1=ruleSeqDecr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqDecr_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:2372:3: this_SeqSched_2= ruleSeqSched
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSeqExpressionAccess().getSeqSchedParserRuleCall_2());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_SeqSched_2=ruleSeqSched();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SeqSched_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalExtendedCCSL.g:2384:3: this_NumberSeqRef_3= ruleNumberSeqRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSeqExpressionAccess().getNumberSeqRefParserRuleCall_3());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NumberSeqRef_3=ruleNumberSeqRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_NumberSeqRef_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalExtendedCCSL.g:2396:3: this_NumberSeqVariableRef_4= ruleNumberSeqVariableRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSeqExpressionAccess().getNumberSeqVariableRefParserRuleCall_4());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NumberSeqVariableRef_4=ruleNumberSeqVariableRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_NumberSeqVariableRef_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeqExpression"


    // $ANTLR start "entryRuleString0"
    // InternalExtendedCCSL.g:2411:1: entryRuleString0 returns [String current=null] : iv_ruleString0= ruleString0 EOF ;
    public final String entryRuleString0() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleString0 = null;


        try {
            // InternalExtendedCCSL.g:2411:47: (iv_ruleString0= ruleString0 EOF )
            // InternalExtendedCCSL.g:2412:2: iv_ruleString0= ruleString0 EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getString0Rule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleString0=ruleString0();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleString0.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleString0"


    // $ANTLR start "ruleString0"
    // InternalExtendedCCSL.g:2418:1: ruleString0 returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= 'String' this_STRING_1= RULE_STRING ) | this_ID_2= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleString0() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_STRING_1=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:2424:2: ( ( (kw= 'String' this_STRING_1= RULE_STRING ) | this_ID_2= RULE_ID ) )
            // InternalExtendedCCSL.g:2425:2: ( (kw= 'String' this_STRING_1= RULE_STRING ) | this_ID_2= RULE_ID )
            {
            // InternalExtendedCCSL.g:2425:2: ( (kw= 'String' this_STRING_1= RULE_STRING ) | this_ID_2= RULE_ID )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==40) ) {
                alt31=1;
            }
            else if ( (LA31_0==RULE_ID) ) {
                alt31=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // InternalExtendedCCSL.g:2426:3: (kw= 'String' this_STRING_1= RULE_STRING )
                    {
                    // InternalExtendedCCSL.g:2426:3: (kw= 'String' this_STRING_1= RULE_STRING )
                    // InternalExtendedCCSL.g:2427:4: kw= 'String' this_STRING_1= RULE_STRING
                    {
                    kw=(Token)match(input,40,FollowSets000.FOLLOW_28); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getString0Access().getStringKeyword_0_0());
                      			
                    }
                    this_STRING_1=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_STRING_1);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newLeafNode(this_STRING_1, grammarAccess.getString0Access().getSTRINGTerminalRuleCall_0_1());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:2441:3: this_ID_2= RULE_ID
                    {
                    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ID_2);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_ID_2, grammarAccess.getString0Access().getIDTerminalRuleCall_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleString0"


    // $ANTLR start "entryRuleStringElement"
    // InternalExtendedCCSL.g:2452:1: entryRuleStringElement returns [EObject current=null] : iv_ruleStringElement= ruleStringElement EOF ;
    public final EObject entryRuleStringElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringElement = null;


        try {
            // InternalExtendedCCSL.g:2452:54: (iv_ruleStringElement= ruleStringElement EOF )
            // InternalExtendedCCSL.g:2453:2: iv_ruleStringElement= ruleStringElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringElementRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringElement=ruleStringElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringElement; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringElement"


    // $ANTLR start "ruleStringElement"
    // InternalExtendedCCSL.g:2459:1: ruleStringElement returns [EObject current=null] : (otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleString0 ) ) ) ;
    public final EObject ruleStringElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_value_3_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:2465:2: ( (otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleString0 ) ) ) )
            // InternalExtendedCCSL.g:2466:2: (otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleString0 ) ) )
            {
            // InternalExtendedCCSL.g:2466:2: (otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleString0 ) ) )
            // InternalExtendedCCSL.g:2467:3: otherlv_0= 'String' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleString0 ) )
            {
            otherlv_0=(Token)match(input,40,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getStringElementAccess().getStringKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:2471:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalExtendedCCSL.g:2472:4: (lv_name_1_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:2472:4: (lv_name_1_0= RULE_ID )
            // InternalExtendedCCSL.g:2473:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getStringElementAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getStringElementRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,33,FollowSets000.FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getStringElementAccess().getEqualsSignKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:2493:3: ( (lv_value_3_0= ruleString0 ) )
            // InternalExtendedCCSL.g:2494:4: (lv_value_3_0= ruleString0 )
            {
            // InternalExtendedCCSL.g:2494:4: (lv_value_3_0= ruleString0 )
            // InternalExtendedCCSL.g:2495:5: lv_value_3_0= ruleString0
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getStringElementAccess().getValueString0ParserRuleCall_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_value_3_0=ruleString0();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getStringElementRule());
              					}
              					set(
              						current,
              						"value",
              						lv_value_3_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.String0");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringElement"


    // $ANTLR start "entryRuleIntegerElement"
    // InternalExtendedCCSL.g:2516:1: entryRuleIntegerElement returns [EObject current=null] : iv_ruleIntegerElement= ruleIntegerElement EOF ;
    public final EObject entryRuleIntegerElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerElement = null;


        try {
            // InternalExtendedCCSL.g:2516:55: (iv_ruleIntegerElement= ruleIntegerElement EOF )
            // InternalExtendedCCSL.g:2517:2: iv_ruleIntegerElement= ruleIntegerElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerElementRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerElement=ruleIntegerElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerElement; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerElement"


    // $ANTLR start "ruleIntegerElement"
    // InternalExtendedCCSL.g:2523:1: ruleIntegerElement returns [EObject current=null] : ( (otherlv_0= 'Integer' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_INT ) ) ) | ( ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '=' ( (lv_value_8_0= RULE_INT ) ) ) | ( (lv_value_9_0= RULE_INT ) ) ) ;
    public final EObject ruleIntegerElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token lv_name_6_0=null;
        Token otherlv_7=null;
        Token lv_value_8_0=null;
        Token lv_value_9_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:2529:2: ( ( (otherlv_0= 'Integer' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_INT ) ) ) | ( ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '=' ( (lv_value_8_0= RULE_INT ) ) ) | ( (lv_value_9_0= RULE_INT ) ) ) )
            // InternalExtendedCCSL.g:2530:2: ( (otherlv_0= 'Integer' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_INT ) ) ) | ( ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '=' ( (lv_value_8_0= RULE_INT ) ) ) | ( (lv_value_9_0= RULE_INT ) ) )
            {
            // InternalExtendedCCSL.g:2530:2: ( (otherlv_0= 'Integer' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_INT ) ) ) | ( ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '=' ( (lv_value_8_0= RULE_INT ) ) ) | ( (lv_value_9_0= RULE_INT ) ) )
            int alt33=3;
            switch ( input.LA(1) ) {
            case 41:
                {
                alt33=1;
                }
                break;
            case RULE_ID:
                {
                alt33=2;
                }
                break;
            case RULE_INT:
                {
                alt33=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }

            switch (alt33) {
                case 1 :
                    // InternalExtendedCCSL.g:2531:3: (otherlv_0= 'Integer' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_INT ) ) )
                    {
                    // InternalExtendedCCSL.g:2531:3: (otherlv_0= 'Integer' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_INT ) ) )
                    // InternalExtendedCCSL.g:2532:4: otherlv_0= 'Integer' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_INT ) )
                    {
                    otherlv_0=(Token)match(input,41,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getIntegerElementAccess().getIntegerKeyword_0_0());
                      			
                    }
                    // InternalExtendedCCSL.g:2536:4: ( (lv_name_1_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:2537:5: (lv_name_1_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:2537:5: (lv_name_1_0= RULE_ID )
                    // InternalExtendedCCSL.g:2538:6: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_37); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_1_0, grammarAccess.getIntegerElementAccess().getNameIDTerminalRuleCall_0_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntegerElementRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_1_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }

                    // InternalExtendedCCSL.g:2554:4: (otherlv_2= ':' ( ( ruleEString ) ) )?
                    int alt32=2;
                    int LA32_0 = input.LA(1);

                    if ( (LA32_0==37) ) {
                        alt32=1;
                    }
                    switch (alt32) {
                        case 1 :
                            // InternalExtendedCCSL.g:2555:5: otherlv_2= ':' ( ( ruleEString ) )
                            {
                            otherlv_2=(Token)match(input,37,FollowSets000.FOLLOW_9); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_2, grammarAccess.getIntegerElementAccess().getColonKeyword_0_2_0());
                              				
                            }
                            // InternalExtendedCCSL.g:2559:5: ( ( ruleEString ) )
                            // InternalExtendedCCSL.g:2560:6: ( ruleEString )
                            {
                            // InternalExtendedCCSL.g:2560:6: ( ruleEString )
                            // InternalExtendedCCSL.g:2561:7: ruleEString
                            {
                            if ( state.backtracking==0 ) {

                              							/* */
                              						
                            }
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElement(grammarAccess.getIntegerElementRule());
                              							}
                              						
                            }
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getIntegerElementAccess().getTypeTypeCrossReference_0_2_1_0());
                              						
                            }
                            pushFollow(FollowSets000.FOLLOW_27);
                            ruleEString();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;

                    }

                    otherlv_4=(Token)match(input,33,FollowSets000.FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getIntegerElementAccess().getEqualsSignKeyword_0_3());
                      			
                    }
                    // InternalExtendedCCSL.g:2583:4: ( (lv_value_5_0= RULE_INT ) )
                    // InternalExtendedCCSL.g:2584:5: (lv_value_5_0= RULE_INT )
                    {
                    // InternalExtendedCCSL.g:2584:5: (lv_value_5_0= RULE_INT )
                    // InternalExtendedCCSL.g:2585:6: lv_value_5_0= RULE_INT
                    {
                    lv_value_5_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_value_5_0, grammarAccess.getIntegerElementAccess().getValueINTTerminalRuleCall_0_4_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntegerElementRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"value",
                      							lv_value_5_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.INT");
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:2603:3: ( ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '=' ( (lv_value_8_0= RULE_INT ) ) )
                    {
                    // InternalExtendedCCSL.g:2603:3: ( ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '=' ( (lv_value_8_0= RULE_INT ) ) )
                    // InternalExtendedCCSL.g:2604:4: ( (lv_name_6_0= RULE_ID ) ) otherlv_7= '=' ( (lv_value_8_0= RULE_INT ) )
                    {
                    // InternalExtendedCCSL.g:2604:4: ( (lv_name_6_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:2605:5: (lv_name_6_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:2605:5: (lv_name_6_0= RULE_ID )
                    // InternalExtendedCCSL.g:2606:6: lv_name_6_0= RULE_ID
                    {
                    lv_name_6_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_27); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_6_0, grammarAccess.getIntegerElementAccess().getNameIDTerminalRuleCall_1_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntegerElementRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_6_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }

                    otherlv_7=(Token)match(input,33,FollowSets000.FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getIntegerElementAccess().getEqualsSignKeyword_1_1());
                      			
                    }
                    // InternalExtendedCCSL.g:2626:4: ( (lv_value_8_0= RULE_INT ) )
                    // InternalExtendedCCSL.g:2627:5: (lv_value_8_0= RULE_INT )
                    {
                    // InternalExtendedCCSL.g:2627:5: (lv_value_8_0= RULE_INT )
                    // InternalExtendedCCSL.g:2628:6: lv_value_8_0= RULE_INT
                    {
                    lv_value_8_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_value_8_0, grammarAccess.getIntegerElementAccess().getValueINTTerminalRuleCall_1_2_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntegerElementRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"value",
                      							lv_value_8_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.INT");
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:2646:3: ( (lv_value_9_0= RULE_INT ) )
                    {
                    // InternalExtendedCCSL.g:2646:3: ( (lv_value_9_0= RULE_INT ) )
                    // InternalExtendedCCSL.g:2647:4: (lv_value_9_0= RULE_INT )
                    {
                    // InternalExtendedCCSL.g:2647:4: (lv_value_9_0= RULE_INT )
                    // InternalExtendedCCSL.g:2648:5: lv_value_9_0= RULE_INT
                    {
                    lv_value_9_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_value_9_0, grammarAccess.getIntegerElementAccess().getValueINTTerminalRuleCall_2_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getIntegerElementRule());
                      					}
                      					setWithLastConsumed(
                      						current,
                      						"value",
                      						lv_value_9_0,
                      						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.INT");
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerElement"


    // $ANTLR start "entryRuleRealElement"
    // InternalExtendedCCSL.g:2668:1: entryRuleRealElement returns [EObject current=null] : iv_ruleRealElement= ruleRealElement EOF ;
    public final EObject entryRuleRealElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealElement = null;


        try {
            // InternalExtendedCCSL.g:2668:52: (iv_ruleRealElement= ruleRealElement EOF )
            // InternalExtendedCCSL.g:2669:2: iv_ruleRealElement= ruleRealElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealElementRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealElement=ruleRealElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealElement; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealElement"


    // $ANTLR start "ruleRealElement"
    // InternalExtendedCCSL.g:2675:1: ruleRealElement returns [EObject current=null] : (otherlv_0= 'Real' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_REAL0 ) ) ) ;
    public final EObject ruleRealElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:2681:2: ( (otherlv_0= 'Real' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_REAL0 ) ) ) )
            // InternalExtendedCCSL.g:2682:2: (otherlv_0= 'Real' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_REAL0 ) ) )
            {
            // InternalExtendedCCSL.g:2682:2: (otherlv_0= 'Real' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_REAL0 ) ) )
            // InternalExtendedCCSL.g:2683:3: otherlv_0= 'Real' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( ruleEString ) ) )? otherlv_4= '=' ( (lv_value_5_0= RULE_REAL0 ) )
            {
            otherlv_0=(Token)match(input,42,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealElementAccess().getRealKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:2687:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalExtendedCCSL.g:2688:4: (lv_name_1_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:2688:4: (lv_name_1_0= RULE_ID )
            // InternalExtendedCCSL.g:2689:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_37); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getRealElementAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRealElementRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalExtendedCCSL.g:2705:3: (otherlv_2= ':' ( ( ruleEString ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==37) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalExtendedCCSL.g:2706:4: otherlv_2= ':' ( ( ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,37,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealElementAccess().getColonKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:2710:4: ( ( ruleEString ) )
                    // InternalExtendedCCSL.g:2711:5: ( ruleEString )
                    {
                    // InternalExtendedCCSL.g:2711:5: ( ruleEString )
                    // InternalExtendedCCSL.g:2712:6: ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealElementRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getRealElementAccess().getTypeTypeCrossReference_2_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_27);
                    ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,33,FollowSets000.FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealElementAccess().getEqualsSignKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:2734:3: ( (lv_value_5_0= RULE_REAL0 ) )
            // InternalExtendedCCSL.g:2735:4: (lv_value_5_0= RULE_REAL0 )
            {
            // InternalExtendedCCSL.g:2735:4: (lv_value_5_0= RULE_REAL0 )
            // InternalExtendedCCSL.g:2736:5: lv_value_5_0= RULE_REAL0
            {
            lv_value_5_0=(Token)match(input,RULE_REAL0,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_5_0, grammarAccess.getRealElementAccess().getValueReal0TerminalRuleCall_4_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRealElementRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Real0");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealElement"


    // $ANTLR start "entryRuleSequenceElement"
    // InternalExtendedCCSL.g:2756:1: entryRuleSequenceElement returns [EObject current=null] : iv_ruleSequenceElement= ruleSequenceElement EOF ;
    public final EObject entryRuleSequenceElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSequenceElement = null;


        try {
            // InternalExtendedCCSL.g:2756:56: (iv_ruleSequenceElement= ruleSequenceElement EOF )
            // InternalExtendedCCSL.g:2757:2: iv_ruleSequenceElement= ruleSequenceElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSequenceElementRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSequenceElement=ruleSequenceElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSequenceElement; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSequenceElement"


    // $ANTLR start "ruleSequenceElement"
    // InternalExtendedCCSL.g:2763:1: ruleSequenceElement returns [EObject current=null] : ( () otherlv_1= 'Sequence' ( (lv_name_2_0= RULE_ID ) )? (otherlv_3= ':' ( ( ruleEString ) ) )? otherlv_5= '=' ( ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )? )? (otherlv_10= '(' ( (lv_nonFinitePart_11_0= rulePrimitiveElement ) ) (otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) ) )* otherlv_14= ')' )? ) ;
    public final EObject ruleSequenceElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        EObject lv_finitePart_6_0 = null;

        EObject lv_finitePart_8_0 = null;

        EObject lv_nonFinitePart_11_0 = null;

        EObject lv_nonFinitePart_13_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:2769:2: ( ( () otherlv_1= 'Sequence' ( (lv_name_2_0= RULE_ID ) )? (otherlv_3= ':' ( ( ruleEString ) ) )? otherlv_5= '=' ( ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )? )? (otherlv_10= '(' ( (lv_nonFinitePart_11_0= rulePrimitiveElement ) ) (otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) ) )* otherlv_14= ')' )? ) )
            // InternalExtendedCCSL.g:2770:2: ( () otherlv_1= 'Sequence' ( (lv_name_2_0= RULE_ID ) )? (otherlv_3= ':' ( ( ruleEString ) ) )? otherlv_5= '=' ( ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )? )? (otherlv_10= '(' ( (lv_nonFinitePart_11_0= rulePrimitiveElement ) ) (otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) ) )* otherlv_14= ')' )? )
            {
            // InternalExtendedCCSL.g:2770:2: ( () otherlv_1= 'Sequence' ( (lv_name_2_0= RULE_ID ) )? (otherlv_3= ':' ( ( ruleEString ) ) )? otherlv_5= '=' ( ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )? )? (otherlv_10= '(' ( (lv_nonFinitePart_11_0= rulePrimitiveElement ) ) (otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) ) )* otherlv_14= ')' )? )
            // InternalExtendedCCSL.g:2771:3: () otherlv_1= 'Sequence' ( (lv_name_2_0= RULE_ID ) )? (otherlv_3= ':' ( ( ruleEString ) ) )? otherlv_5= '=' ( ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )? )? (otherlv_10= '(' ( (lv_nonFinitePart_11_0= rulePrimitiveElement ) ) (otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) ) )* otherlv_14= ')' )?
            {
            // InternalExtendedCCSL.g:2771:3: ()
            // InternalExtendedCCSL.g:2772:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getSequenceElementAccess().getSequenceElementAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,43,FollowSets000.FOLLOW_40); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSequenceElementAccess().getSequenceKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:2785:3: ( (lv_name_2_0= RULE_ID ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==RULE_ID) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalExtendedCCSL.g:2786:4: (lv_name_2_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:2786:4: (lv_name_2_0= RULE_ID )
                    // InternalExtendedCCSL.g:2787:5: lv_name_2_0= RULE_ID
                    {
                    lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_37); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_name_2_0, grammarAccess.getSequenceElementAccess().getNameIDTerminalRuleCall_2_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSequenceElementRule());
                      					}
                      					setWithLastConsumed(
                      						current,
                      						"name",
                      						lv_name_2_0,
                      						"org.eclipse.xtext.common.Terminals.ID");
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalExtendedCCSL.g:2803:3: (otherlv_3= ':' ( ( ruleEString ) ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==37) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalExtendedCCSL.g:2804:4: otherlv_3= ':' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,37,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getSequenceElementAccess().getColonKeyword_3_0());
                      			
                    }
                    // InternalExtendedCCSL.g:2808:4: ( ( ruleEString ) )
                    // InternalExtendedCCSL.g:2809:5: ( ruleEString )
                    {
                    // InternalExtendedCCSL.g:2809:5: ( ruleEString )
                    // InternalExtendedCCSL.g:2810:6: ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getSequenceElementRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getSequenceElementAccess().getTypeTypeCrossReference_3_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_27);
                    ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,33,FollowSets000.FOLLOW_41); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getSequenceElementAccess().getEqualsSignKeyword_4());
              		
            }
            // InternalExtendedCCSL.g:2832:3: ( ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )? )?
            int alt39=2;
            alt39 = dfa39.predict(input);
            switch (alt39) {
                case 1 :
                    // InternalExtendedCCSL.g:2833:4: ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )?
                    {
                    // InternalExtendedCCSL.g:2833:4: ( (lv_finitePart_6_0= rulePrimitiveElement ) )
                    // InternalExtendedCCSL.g:2834:5: (lv_finitePart_6_0= rulePrimitiveElement )
                    {
                    // InternalExtendedCCSL.g:2834:5: (lv_finitePart_6_0= rulePrimitiveElement )
                    // InternalExtendedCCSL.g:2835:6: lv_finitePart_6_0= rulePrimitiveElement
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getSequenceElementAccess().getFinitePartPrimitiveElementParserRuleCall_5_0_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_42);
                    lv_finitePart_6_0=rulePrimitiveElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getSequenceElementRule());
                      						}
                      						add(
                      							current,
                      							"finitePart",
                      							lv_finitePart_6_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.PrimitiveElement");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalExtendedCCSL.g:2852:4: (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )*
                    loop37:
                    do {
                        int alt37=2;
                        alt37 = dfa37.predict(input);
                        switch (alt37) {
                    	case 1 :
                    	    // InternalExtendedCCSL.g:2853:5: otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) )
                    	    {
                    	    otherlv_7=(Token)match(input,25,FollowSets000.FOLLOW_43); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_7, grammarAccess.getSequenceElementAccess().getSemicolonKeyword_5_1_0());
                    	      				
                    	    }
                    	    // InternalExtendedCCSL.g:2857:5: ( (lv_finitePart_8_0= rulePrimitiveElement ) )
                    	    // InternalExtendedCCSL.g:2858:6: (lv_finitePart_8_0= rulePrimitiveElement )
                    	    {
                    	    // InternalExtendedCCSL.g:2858:6: (lv_finitePart_8_0= rulePrimitiveElement )
                    	    // InternalExtendedCCSL.g:2859:7: lv_finitePart_8_0= rulePrimitiveElement
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getSequenceElementAccess().getFinitePartPrimitiveElementParserRuleCall_5_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_42);
                    	    lv_finitePart_8_0=rulePrimitiveElement();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getSequenceElementRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"finitePart",
                    	      								lv_finitePart_8_0,
                    	      								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.PrimitiveElement");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop37;
                        }
                    } while (true);

                    // InternalExtendedCCSL.g:2877:4: (otherlv_9= ';' )?
                    int alt38=2;
                    int LA38_0 = input.LA(1);

                    if ( (LA38_0==25) ) {
                        int LA38_1 = input.LA(2);

                        if ( (synpred115_InternalExtendedCCSL()) ) {
                            alt38=1;
                        }
                    }
                    switch (alt38) {
                        case 1 :
                            // InternalExtendedCCSL.g:2878:5: otherlv_9= ';'
                            {
                            otherlv_9=(Token)match(input,25,FollowSets000.FOLLOW_44); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_9, grammarAccess.getSequenceElementAccess().getSemicolonKeyword_5_2());
                              				
                            }

                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalExtendedCCSL.g:2884:3: (otherlv_10= '(' ( (lv_nonFinitePart_11_0= rulePrimitiveElement ) ) (otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) ) )* otherlv_14= ')' )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==30) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalExtendedCCSL.g:2885:4: otherlv_10= '(' ( (lv_nonFinitePart_11_0= rulePrimitiveElement ) ) (otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) ) )* otherlv_14= ')'
                    {
                    otherlv_10=(Token)match(input,30,FollowSets000.FOLLOW_43); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_10, grammarAccess.getSequenceElementAccess().getLeftParenthesisKeyword_6_0());
                      			
                    }
                    // InternalExtendedCCSL.g:2889:4: ( (lv_nonFinitePart_11_0= rulePrimitiveElement ) )
                    // InternalExtendedCCSL.g:2890:5: (lv_nonFinitePart_11_0= rulePrimitiveElement )
                    {
                    // InternalExtendedCCSL.g:2890:5: (lv_nonFinitePart_11_0= rulePrimitiveElement )
                    // InternalExtendedCCSL.g:2891:6: lv_nonFinitePart_11_0= rulePrimitiveElement
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getSequenceElementAccess().getNonFinitePartPrimitiveElementParserRuleCall_6_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_45);
                    lv_nonFinitePart_11_0=rulePrimitiveElement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getSequenceElementRule());
                      						}
                      						add(
                      							current,
                      							"nonFinitePart",
                      							lv_nonFinitePart_11_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.PrimitiveElement");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalExtendedCCSL.g:2908:4: (otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) ) )*
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( (LA40_0==25) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // InternalExtendedCCSL.g:2909:5: otherlv_12= ';' ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) )
                    	    {
                    	    otherlv_12=(Token)match(input,25,FollowSets000.FOLLOW_43); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_12, grammarAccess.getSequenceElementAccess().getSemicolonKeyword_6_2_0());
                    	      				
                    	    }
                    	    // InternalExtendedCCSL.g:2913:5: ( (lv_nonFinitePart_13_0= rulePrimitiveElement ) )
                    	    // InternalExtendedCCSL.g:2914:6: (lv_nonFinitePart_13_0= rulePrimitiveElement )
                    	    {
                    	    // InternalExtendedCCSL.g:2914:6: (lv_nonFinitePart_13_0= rulePrimitiveElement )
                    	    // InternalExtendedCCSL.g:2915:7: lv_nonFinitePart_13_0= rulePrimitiveElement
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getSequenceElementAccess().getNonFinitePartPrimitiveElementParserRuleCall_6_2_1_0());
                    	      						
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_45);
                    	    lv_nonFinitePart_13_0=rulePrimitiveElement();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getSequenceElementRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"nonFinitePart",
                    	      								lv_nonFinitePart_13_0,
                    	      								"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.PrimitiveElement");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop40;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,31,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_14, grammarAccess.getSequenceElementAccess().getRightParenthesisKeyword_6_3());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSequenceElement"


    // $ANTLR start "entryRuleBoolean0"
    // InternalExtendedCCSL.g:2942:1: entryRuleBoolean0 returns [String current=null] : iv_ruleBoolean0= ruleBoolean0 EOF ;
    public final String entryRuleBoolean0() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBoolean0 = null;


        try {
            // InternalExtendedCCSL.g:2942:48: (iv_ruleBoolean0= ruleBoolean0 EOF )
            // InternalExtendedCCSL.g:2943:2: iv_ruleBoolean0= ruleBoolean0 EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBoolean0Rule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBoolean0=ruleBoolean0();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBoolean0.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolean0"


    // $ANTLR start "ruleBoolean0"
    // InternalExtendedCCSL.g:2949:1: ruleBoolean0 returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= 'Bool' kw= 'True' ) | kw= 'False' ) ;
    public final AntlrDatatypeRuleToken ruleBoolean0() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:2955:2: ( ( (kw= 'Bool' kw= 'True' ) | kw= 'False' ) )
            // InternalExtendedCCSL.g:2956:2: ( (kw= 'Bool' kw= 'True' ) | kw= 'False' )
            {
            // InternalExtendedCCSL.g:2956:2: ( (kw= 'Bool' kw= 'True' ) | kw= 'False' )
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==44) ) {
                alt42=1;
            }
            else if ( (LA42_0==46) ) {
                alt42=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;
            }
            switch (alt42) {
                case 1 :
                    // InternalExtendedCCSL.g:2957:3: (kw= 'Bool' kw= 'True' )
                    {
                    // InternalExtendedCCSL.g:2957:3: (kw= 'Bool' kw= 'True' )
                    // InternalExtendedCCSL.g:2958:4: kw= 'Bool' kw= 'True'
                    {
                    kw=(Token)match(input,44,FollowSets000.FOLLOW_46); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getBoolean0Access().getBoolKeyword_0_0());
                      			
                    }
                    kw=(Token)match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getBoolean0Access().getTrueKeyword_0_1());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:2970:3: kw= 'False'
                    {
                    kw=(Token)match(input,46,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getBoolean0Access().getFalseKeyword_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolean0"


    // $ANTLR start "entryRuleBinding"
    // InternalExtendedCCSL.g:2979:1: entryRuleBinding returns [EObject current=null] : iv_ruleBinding= ruleBinding EOF ;
    public final EObject entryRuleBinding() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinding = null;


        try {
            // InternalExtendedCCSL.g:2979:48: (iv_ruleBinding= ruleBinding EOF )
            // InternalExtendedCCSL.g:2980:2: iv_ruleBinding= ruleBinding EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBindingRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBinding=ruleBinding();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinding; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinding"


    // $ANTLR start "ruleBinding"
    // InternalExtendedCCSL.g:2986:1: ruleBinding returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '->' ( ( ruleEString ) ) ) ;
    public final EObject ruleBinding() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:2992:2: ( ( ( ( ruleEString ) ) otherlv_1= '->' ( ( ruleEString ) ) ) )
            // InternalExtendedCCSL.g:2993:2: ( ( ( ruleEString ) ) otherlv_1= '->' ( ( ruleEString ) ) )
            {
            // InternalExtendedCCSL.g:2993:2: ( ( ( ruleEString ) ) otherlv_1= '->' ( ( ruleEString ) ) )
            // InternalExtendedCCSL.g:2994:3: ( ( ruleEString ) ) otherlv_1= '->' ( ( ruleEString ) )
            {
            // InternalExtendedCCSL.g:2994:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:2995:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:2995:4: ( ruleEString )
            // InternalExtendedCCSL.g:2996:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBindingRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBindingAccess().getAbstractAbstractEntityCrossReference_0_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_47);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,38,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBindingAccess().getHyphenMinusGreaterThanSignKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3017:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:3018:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:3018:4: ( ruleEString )
            // InternalExtendedCCSL.g:3019:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBindingRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBindingAccess().getBindableBindableEntityCrossReference_2_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinding"


    // $ANTLR start "entryRuleAbstractEntity"
    // InternalExtendedCCSL.g:3040:1: entryRuleAbstractEntity returns [EObject current=null] : iv_ruleAbstractEntity= ruleAbstractEntity EOF ;
    public final EObject entryRuleAbstractEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractEntity = null;


        try {
            // InternalExtendedCCSL.g:3040:55: (iv_ruleAbstractEntity= ruleAbstractEntity EOF )
            // InternalExtendedCCSL.g:3041:2: iv_ruleAbstractEntity= ruleAbstractEntity EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAbstractEntityRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAbstractEntity=ruleAbstractEntity();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAbstractEntity; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractEntity"


    // $ANTLR start "ruleAbstractEntity"
    // InternalExtendedCCSL.g:3047:1: ruleAbstractEntity returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( ruleEString ) ) ) ;
    public final EObject ruleAbstractEntity() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:3053:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( ruleEString ) ) ) )
            // InternalExtendedCCSL.g:3054:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( ruleEString ) ) )
            {
            // InternalExtendedCCSL.g:3054:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( ruleEString ) ) )
            // InternalExtendedCCSL.g:3055:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( ruleEString ) )
            {
            // InternalExtendedCCSL.g:3055:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalExtendedCCSL.g:3056:4: (lv_name_0_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:3056:4: (lv_name_0_0= RULE_ID )
            // InternalExtendedCCSL.g:3057:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_48); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_0_0, grammarAccess.getAbstractEntityAccess().getNameIDTerminalRuleCall_0_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getAbstractEntityRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_0_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getAbstractEntityAccess().getColonKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3077:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:3078:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:3078:4: ( ruleEString )
            // InternalExtendedCCSL.g:3079:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getAbstractEntityRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAbstractEntityAccess().getTypeTypeCrossReference_2_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractEntity"


    // $ANTLR start "entryRuleConcreteEntity_Impl"
    // InternalExtendedCCSL.g:3100:1: entryRuleConcreteEntity_Impl returns [EObject current=null] : iv_ruleConcreteEntity_Impl= ruleConcreteEntity_Impl EOF ;
    public final EObject entryRuleConcreteEntity_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConcreteEntity_Impl = null;


        try {
            // InternalExtendedCCSL.g:3100:60: (iv_ruleConcreteEntity_Impl= ruleConcreteEntity_Impl EOF )
            // InternalExtendedCCSL.g:3101:2: iv_ruleConcreteEntity_Impl= ruleConcreteEntity_Impl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConcreteEntity_ImplRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConcreteEntity_Impl=ruleConcreteEntity_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConcreteEntity_Impl; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConcreteEntity_Impl"


    // $ANTLR start "ruleConcreteEntity_Impl"
    // InternalExtendedCCSL.g:3107:1: ruleConcreteEntity_Impl returns [EObject current=null] : ( () otherlv_1= 'ConcreteEntity' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleConcreteEntity_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:3113:2: ( ( () otherlv_1= 'ConcreteEntity' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalExtendedCCSL.g:3114:2: ( () otherlv_1= 'ConcreteEntity' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalExtendedCCSL.g:3114:2: ( () otherlv_1= 'ConcreteEntity' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalExtendedCCSL.g:3115:3: () otherlv_1= 'ConcreteEntity' ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalExtendedCCSL.g:3115:3: ()
            // InternalExtendedCCSL.g:3116:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getConcreteEntity_ImplAccess().getConcreteEntityAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,47,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getConcreteEntity_ImplAccess().getConcreteEntityKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3129:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:3130:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:3130:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:3131:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getConcreteEntity_ImplAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getConcreteEntity_ImplRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConcreteEntity_Impl"


    // $ANTLR start "entryRuleBooleanRef"
    // InternalExtendedCCSL.g:3151:1: entryRuleBooleanRef returns [EObject current=null] : iv_ruleBooleanRef= ruleBooleanRef EOF ;
    public final EObject entryRuleBooleanRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanRef = null;


        try {
            // InternalExtendedCCSL.g:3151:51: (iv_ruleBooleanRef= ruleBooleanRef EOF )
            // InternalExtendedCCSL.g:3152:2: iv_ruleBooleanRef= ruleBooleanRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanRef=ruleBooleanRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanRef"


    // $ANTLR start "ruleBooleanRef"
    // InternalExtendedCCSL.g:3158:1: ruleBooleanRef returns [EObject current=null] : (otherlv_0= 'BooleanRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedBool' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleBooleanRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:3164:2: ( (otherlv_0= 'BooleanRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedBool' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:3165:2: (otherlv_0= 'BooleanRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedBool' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:3165:2: (otherlv_0= 'BooleanRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedBool' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:3166:3: otherlv_0= 'BooleanRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedBool' ( ( ruleEString ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,48,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getBooleanRefAccess().getBooleanRefKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_49); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBooleanRefAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3174:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==49) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalExtendedCCSL.g:3175:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getBooleanRefAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3179:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3180:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3180:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:3181:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_50); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getBooleanRefAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getBooleanRefRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,50,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getBooleanRefAccess().getReferencedBoolKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:3202:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:3203:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:3203:4: ( ruleEString )
            // InternalExtendedCCSL.g:3204:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBooleanRefRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBooleanRefAccess().getReferencedBoolBooleanElementCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getBooleanRefAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanRef"


    // $ANTLR start "entryRuleRealRef"
    // InternalExtendedCCSL.g:3229:1: entryRuleRealRef returns [EObject current=null] : iv_ruleRealRef= ruleRealRef EOF ;
    public final EObject entryRuleRealRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealRef = null;


        try {
            // InternalExtendedCCSL.g:3229:48: (iv_ruleRealRef= ruleRealRef EOF )
            // InternalExtendedCCSL.g:3230:2: iv_ruleRealRef= ruleRealRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealRef=ruleRealRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealRef"


    // $ANTLR start "ruleRealRef"
    // InternalExtendedCCSL.g:3236:1: ruleRealRef returns [EObject current=null] : (otherlv_0= 'RealRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'realElem' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleRealRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:3242:2: ( (otherlv_0= 'RealRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'realElem' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:3243:2: (otherlv_0= 'RealRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'realElem' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:3243:2: (otherlv_0= 'RealRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'realElem' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:3244:3: otherlv_0= 'RealRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'realElem' ( ( ruleEString ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,51,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealRefAccess().getRealRefKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_52); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealRefAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3252:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==49) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalExtendedCCSL.g:3253:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealRefAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3257:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3258:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3258:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:3259:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_53); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getRealRefAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealRefRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,52,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealRefAccess().getRealElemKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:3280:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:3281:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:3281:4: ( ruleEString )
            // InternalExtendedCCSL.g:3282:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRealRefRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealRefAccess().getRealElemRealElementCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRealRefAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealRef"


    // $ANTLR start "entryRuleIntegerRef"
    // InternalExtendedCCSL.g:3307:1: entryRuleIntegerRef returns [EObject current=null] : iv_ruleIntegerRef= ruleIntegerRef EOF ;
    public final EObject entryRuleIntegerRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerRef = null;


        try {
            // InternalExtendedCCSL.g:3307:51: (iv_ruleIntegerRef= ruleIntegerRef EOF )
            // InternalExtendedCCSL.g:3308:2: iv_ruleIntegerRef= ruleIntegerRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerRef=ruleIntegerRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerRef"


    // $ANTLR start "ruleIntegerRef"
    // InternalExtendedCCSL.g:3314:1: ruleIntegerRef returns [EObject current=null] : (otherlv_0= 'IntegerRef' (otherlv_1= 'name' ( (lv_name_2_0= RULE_ID ) ) )? otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' ) ;
    public final EObject ruleIntegerRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:3320:2: ( (otherlv_0= 'IntegerRef' (otherlv_1= 'name' ( (lv_name_2_0= RULE_ID ) ) )? otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' ) )
            // InternalExtendedCCSL.g:3321:2: (otherlv_0= 'IntegerRef' (otherlv_1= 'name' ( (lv_name_2_0= RULE_ID ) ) )? otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' )
            {
            // InternalExtendedCCSL.g:3321:2: (otherlv_0= 'IntegerRef' (otherlv_1= 'name' ( (lv_name_2_0= RULE_ID ) ) )? otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']' )
            // InternalExtendedCCSL.g:3322:3: otherlv_0= 'IntegerRef' (otherlv_1= 'name' ( (lv_name_2_0= RULE_ID ) ) )? otherlv_3= '[' ( ( ruleEString ) ) otherlv_5= ']'
            {
            otherlv_0=(Token)match(input,53,FollowSets000.FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntegerRefAccess().getIntegerRefKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:3326:3: (otherlv_1= 'name' ( (lv_name_2_0= RULE_ID ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==49) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalExtendedCCSL.g:3327:4: otherlv_1= 'name' ( (lv_name_2_0= RULE_ID ) )
                    {
                    otherlv_1=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getIntegerRefAccess().getNameKeyword_1_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3331:4: ( (lv_name_2_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3332:5: (lv_name_2_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3332:5: (lv_name_2_0= RULE_ID )
                    // InternalExtendedCCSL.g:3333:6: lv_name_2_0= RULE_ID
                    {
                    lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_2_0, grammarAccess.getIntegerRefAccess().getNameIDTerminalRuleCall_1_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntegerRefRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_2_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,28,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getIntegerRefAccess().getLeftSquareBracketKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:3354:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:3355:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:3355:4: ( ruleEString )
            // InternalExtendedCCSL.g:3356:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getIntegerRefRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntegerRefAccess().getIntegerElemIntegerElementCrossReference_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_23);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_5=(Token)match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getIntegerRefAccess().getRightSquareBracketKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerRef"


    // $ANTLR start "entryRuleUnaryRealPlus"
    // InternalExtendedCCSL.g:3381:1: entryRuleUnaryRealPlus returns [EObject current=null] : iv_ruleUnaryRealPlus= ruleUnaryRealPlus EOF ;
    public final EObject entryRuleUnaryRealPlus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryRealPlus = null;


        try {
            // InternalExtendedCCSL.g:3381:54: (iv_ruleUnaryRealPlus= ruleUnaryRealPlus EOF )
            // InternalExtendedCCSL.g:3382:2: iv_ruleUnaryRealPlus= ruleUnaryRealPlus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryRealPlusRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleUnaryRealPlus=ruleUnaryRealPlus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryRealPlus; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryRealPlus"


    // $ANTLR start "ruleUnaryRealPlus"
    // InternalExtendedCCSL.g:3388:1: ruleUnaryRealPlus returns [EObject current=null] : (otherlv_0= 'UnaryRealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleUnaryRealPlus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_operand_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:3394:2: ( (otherlv_0= 'UnaryRealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:3395:2: (otherlv_0= 'UnaryRealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:3395:2: (otherlv_0= 'UnaryRealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:3396:3: otherlv_0= 'UnaryRealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,54,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getUnaryRealPlusAccess().getUnaryRealPlusKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getUnaryRealPlusAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3404:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==49) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalExtendedCCSL.g:3405:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getUnaryRealPlusAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3409:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3410:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3410:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:3411:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_56); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getUnaryRealPlusAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getUnaryRealPlusRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,55,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getUnaryRealPlusAccess().getValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:3432:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:3433:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:3433:4: ( ruleEString )
            // InternalExtendedCCSL.g:3434:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getUnaryRealPlusRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryRealPlusAccess().getValueRealElementCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_57);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,56,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getUnaryRealPlusAccess().getOperandKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:3455:3: ( (lv_operand_7_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:3456:4: (lv_operand_7_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:3456:4: (lv_operand_7_0= ruleRealExpression )
            // InternalExtendedCCSL.g:3457:5: lv_operand_7_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryRealPlusAccess().getOperandRealExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_operand_7_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryRealPlusRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getUnaryRealPlusAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryRealPlus"


    // $ANTLR start "entryRuleUnaryRealMinus"
    // InternalExtendedCCSL.g:3482:1: entryRuleUnaryRealMinus returns [EObject current=null] : iv_ruleUnaryRealMinus= ruleUnaryRealMinus EOF ;
    public final EObject entryRuleUnaryRealMinus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryRealMinus = null;


        try {
            // InternalExtendedCCSL.g:3482:55: (iv_ruleUnaryRealMinus= ruleUnaryRealMinus EOF )
            // InternalExtendedCCSL.g:3483:2: iv_ruleUnaryRealMinus= ruleUnaryRealMinus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryRealMinusRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleUnaryRealMinus=ruleUnaryRealMinus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryRealMinus; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryRealMinus"


    // $ANTLR start "ruleUnaryRealMinus"
    // InternalExtendedCCSL.g:3489:1: ruleUnaryRealMinus returns [EObject current=null] : (otherlv_0= 'UnaryRealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleUnaryRealMinus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_operand_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:3495:2: ( (otherlv_0= 'UnaryRealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:3496:2: (otherlv_0= 'UnaryRealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:3496:2: (otherlv_0= 'UnaryRealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:3497:3: otherlv_0= 'UnaryRealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleRealExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,57,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getUnaryRealMinusAccess().getUnaryRealMinusKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getUnaryRealMinusAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3505:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==49) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalExtendedCCSL.g:3506:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getUnaryRealMinusAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3510:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3511:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3511:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:3512:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_56); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getUnaryRealMinusAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getUnaryRealMinusRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,55,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getUnaryRealMinusAccess().getValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:3533:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:3534:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:3534:4: ( ruleEString )
            // InternalExtendedCCSL.g:3535:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getUnaryRealMinusRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryRealMinusAccess().getValueRealElementCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_57);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,56,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getUnaryRealMinusAccess().getOperandKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:3556:3: ( (lv_operand_7_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:3557:4: (lv_operand_7_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:3557:4: (lv_operand_7_0= ruleRealExpression )
            // InternalExtendedCCSL.g:3558:5: lv_operand_7_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryRealMinusAccess().getOperandRealExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_operand_7_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryRealMinusRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getUnaryRealMinusAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryRealMinus"


    // $ANTLR start "entryRuleRealPlus"
    // InternalExtendedCCSL.g:3583:1: entryRuleRealPlus returns [EObject current=null] : iv_ruleRealPlus= ruleRealPlus EOF ;
    public final EObject entryRuleRealPlus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealPlus = null;


        try {
            // InternalExtendedCCSL.g:3583:49: (iv_ruleRealPlus= ruleRealPlus EOF )
            // InternalExtendedCCSL.g:3584:2: iv_ruleRealPlus= ruleRealPlus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealPlusRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealPlus=ruleRealPlus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealPlus; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealPlus"


    // $ANTLR start "ruleRealPlus"
    // InternalExtendedCCSL.g:3590:1: ruleRealPlus returns [EObject current=null] : (otherlv_0= 'RealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleRealPlus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:3596:2: ( (otherlv_0= 'RealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:3597:2: (otherlv_0= 'RealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:3597:2: (otherlv_0= 'RealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:3598:3: otherlv_0= 'RealPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,58,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealPlusAccess().getRealPlusKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealPlusAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3606:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==49) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalExtendedCCSL.g:3607:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealPlusAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3611:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3612:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3612:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:3613:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getRealPlusAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealPlusRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealPlusAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:3634:3: ( (lv_leftValue_5_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:3635:4: (lv_leftValue_5_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:3635:4: (lv_leftValue_5_0= ruleRealExpression )
            // InternalExtendedCCSL.g:3636:5: lv_leftValue_5_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealPlusAccess().getLeftValueRealExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealPlusRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRealPlusAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:3657:3: ( (lv_rightValue_7_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:3658:4: (lv_rightValue_7_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:3658:4: (lv_rightValue_7_0= ruleRealExpression )
            // InternalExtendedCCSL.g:3659:5: lv_rightValue_7_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealPlusAccess().getRightValueRealExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealPlusRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getRealPlusAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealPlus"


    // $ANTLR start "entryRuleRealMinus"
    // InternalExtendedCCSL.g:3684:1: entryRuleRealMinus returns [EObject current=null] : iv_ruleRealMinus= ruleRealMinus EOF ;
    public final EObject entryRuleRealMinus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealMinus = null;


        try {
            // InternalExtendedCCSL.g:3684:50: (iv_ruleRealMinus= ruleRealMinus EOF )
            // InternalExtendedCCSL.g:3685:2: iv_ruleRealMinus= ruleRealMinus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealMinusRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealMinus=ruleRealMinus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealMinus; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealMinus"


    // $ANTLR start "ruleRealMinus"
    // InternalExtendedCCSL.g:3691:1: ruleRealMinus returns [EObject current=null] : (otherlv_0= 'RealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleRealMinus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:3697:2: ( (otherlv_0= 'RealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:3698:2: (otherlv_0= 'RealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:3698:2: (otherlv_0= 'RealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:3699:3: otherlv_0= 'RealMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,61,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealMinusAccess().getRealMinusKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealMinusAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3707:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==49) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalExtendedCCSL.g:3708:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealMinusAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3712:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3713:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3713:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:3714:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getRealMinusAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealMinusRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealMinusAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:3735:3: ( (lv_leftValue_5_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:3736:4: (lv_leftValue_5_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:3736:4: (lv_leftValue_5_0= ruleRealExpression )
            // InternalExtendedCCSL.g:3737:5: lv_leftValue_5_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealMinusAccess().getLeftValueRealExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealMinusRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRealMinusAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:3758:3: ( (lv_rightValue_7_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:3759:4: (lv_rightValue_7_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:3759:4: (lv_rightValue_7_0= ruleRealExpression )
            // InternalExtendedCCSL.g:3760:5: lv_rightValue_7_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealMinusAccess().getRightValueRealExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealMinusRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getRealMinusAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealMinus"


    // $ANTLR start "entryRuleRealMultiply"
    // InternalExtendedCCSL.g:3785:1: entryRuleRealMultiply returns [EObject current=null] : iv_ruleRealMultiply= ruleRealMultiply EOF ;
    public final EObject entryRuleRealMultiply() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealMultiply = null;


        try {
            // InternalExtendedCCSL.g:3785:53: (iv_ruleRealMultiply= ruleRealMultiply EOF )
            // InternalExtendedCCSL.g:3786:2: iv_ruleRealMultiply= ruleRealMultiply EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealMultiplyRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealMultiply=ruleRealMultiply();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealMultiply; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealMultiply"


    // $ANTLR start "ruleRealMultiply"
    // InternalExtendedCCSL.g:3792:1: ruleRealMultiply returns [EObject current=null] : (otherlv_0= 'RealMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleRealMultiply() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:3798:2: ( (otherlv_0= 'RealMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:3799:2: (otherlv_0= 'RealMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:3799:2: (otherlv_0= 'RealMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:3800:3: otherlv_0= 'RealMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,62,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealMultiplyAccess().getRealMultiplyKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealMultiplyAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3808:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==49) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalExtendedCCSL.g:3809:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealMultiplyAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3813:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3814:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3814:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:3815:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getRealMultiplyAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealMultiplyRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealMultiplyAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:3836:3: ( (lv_leftValue_5_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:3837:4: (lv_leftValue_5_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:3837:4: (lv_leftValue_5_0= ruleRealExpression )
            // InternalExtendedCCSL.g:3838:5: lv_leftValue_5_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealMultiplyAccess().getLeftValueRealExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealMultiplyRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRealMultiplyAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:3859:3: ( (lv_rightValue_7_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:3860:4: (lv_rightValue_7_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:3860:4: (lv_rightValue_7_0= ruleRealExpression )
            // InternalExtendedCCSL.g:3861:5: lv_rightValue_7_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealMultiplyAccess().getRightValueRealExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealMultiplyRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getRealMultiplyAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealMultiply"


    // $ANTLR start "entryRuleUnaryIntPlus"
    // InternalExtendedCCSL.g:3886:1: entryRuleUnaryIntPlus returns [EObject current=null] : iv_ruleUnaryIntPlus= ruleUnaryIntPlus EOF ;
    public final EObject entryRuleUnaryIntPlus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryIntPlus = null;


        try {
            // InternalExtendedCCSL.g:3886:53: (iv_ruleUnaryIntPlus= ruleUnaryIntPlus EOF )
            // InternalExtendedCCSL.g:3887:2: iv_ruleUnaryIntPlus= ruleUnaryIntPlus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryIntPlusRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleUnaryIntPlus=ruleUnaryIntPlus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryIntPlus; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryIntPlus"


    // $ANTLR start "ruleUnaryIntPlus"
    // InternalExtendedCCSL.g:3893:1: ruleUnaryIntPlus returns [EObject current=null] : (otherlv_0= 'UnaryIntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleUnaryIntPlus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_operand_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:3899:2: ( (otherlv_0= 'UnaryIntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:3900:2: (otherlv_0= 'UnaryIntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:3900:2: (otherlv_0= 'UnaryIntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:3901:3: otherlv_0= 'UnaryIntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,63,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getUnaryIntPlusAccess().getUnaryIntPlusKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getUnaryIntPlusAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:3909:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==49) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalExtendedCCSL.g:3910:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getUnaryIntPlusAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:3914:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:3915:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:3915:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:3916:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_56); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getUnaryIntPlusAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getUnaryIntPlusRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,55,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getUnaryIntPlusAccess().getValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:3937:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:3938:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:3938:4: ( ruleEString )
            // InternalExtendedCCSL.g:3939:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getUnaryIntPlusRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryIntPlusAccess().getValueIntegerElementCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_57);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,56,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getUnaryIntPlusAccess().getOperandKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:3960:3: ( (lv_operand_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:3961:4: (lv_operand_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:3961:4: (lv_operand_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:3962:5: lv_operand_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryIntPlusAccess().getOperandIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_operand_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryIntPlusRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getUnaryIntPlusAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryIntPlus"


    // $ANTLR start "entryRuleUnaryIntMinus"
    // InternalExtendedCCSL.g:3987:1: entryRuleUnaryIntMinus returns [EObject current=null] : iv_ruleUnaryIntMinus= ruleUnaryIntMinus EOF ;
    public final EObject entryRuleUnaryIntMinus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryIntMinus = null;


        try {
            // InternalExtendedCCSL.g:3987:54: (iv_ruleUnaryIntMinus= ruleUnaryIntMinus EOF )
            // InternalExtendedCCSL.g:3988:2: iv_ruleUnaryIntMinus= ruleUnaryIntMinus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryIntMinusRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleUnaryIntMinus=ruleUnaryIntMinus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryIntMinus; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryIntMinus"


    // $ANTLR start "ruleUnaryIntMinus"
    // InternalExtendedCCSL.g:3994:1: ruleUnaryIntMinus returns [EObject current=null] : (otherlv_0= 'UnaryIntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleUnaryIntMinus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_operand_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4000:2: ( (otherlv_0= 'UnaryIntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4001:2: (otherlv_0= 'UnaryIntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4001:2: (otherlv_0= 'UnaryIntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4002:3: otherlv_0= 'UnaryIntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'value' ( ( ruleEString ) ) otherlv_6= 'operand' ( (lv_operand_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,64,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getUnaryIntMinusAccess().getUnaryIntMinusKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getUnaryIntMinusAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4010:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==49) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalExtendedCCSL.g:4011:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getUnaryIntMinusAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4015:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4016:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4016:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4017:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_56); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getUnaryIntMinusAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getUnaryIntMinusRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,55,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getUnaryIntMinusAccess().getValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4038:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:4039:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:4039:4: ( ruleEString )
            // InternalExtendedCCSL.g:4040:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getUnaryIntMinusRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryIntMinusAccess().getValueIntegerElementCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_57);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,56,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getUnaryIntMinusAccess().getOperandKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4061:3: ( (lv_operand_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4062:4: (lv_operand_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4062:4: (lv_operand_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4063:5: lv_operand_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryIntMinusAccess().getOperandIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_operand_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryIntMinusRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getUnaryIntMinusAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryIntMinus"


    // $ANTLR start "entryRuleIntPlus"
    // InternalExtendedCCSL.g:4088:1: entryRuleIntPlus returns [EObject current=null] : iv_ruleIntPlus= ruleIntPlus EOF ;
    public final EObject entryRuleIntPlus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntPlus = null;


        try {
            // InternalExtendedCCSL.g:4088:48: (iv_ruleIntPlus= ruleIntPlus EOF )
            // InternalExtendedCCSL.g:4089:2: iv_ruleIntPlus= ruleIntPlus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntPlusRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntPlus=ruleIntPlus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntPlus; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntPlus"


    // $ANTLR start "ruleIntPlus"
    // InternalExtendedCCSL.g:4095:1: ruleIntPlus returns [EObject current=null] : (otherlv_0= 'IntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleIntPlus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4101:2: ( (otherlv_0= 'IntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4102:2: (otherlv_0= 'IntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4102:2: (otherlv_0= 'IntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4103:3: otherlv_0= 'IntPlus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,65,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntPlusAccess().getIntPlusKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntPlusAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4111:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==49) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalExtendedCCSL.g:4112:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getIntPlusAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4116:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4117:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4117:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4118:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getIntPlusAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntPlusRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIntPlusAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4139:3: ( (lv_leftValue_5_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4140:4: (lv_leftValue_5_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4140:4: (lv_leftValue_5_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4141:5: lv_leftValue_5_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntPlusAccess().getLeftValueIntegerExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntPlusRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIntPlusAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4162:3: ( (lv_rightValue_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4163:4: (lv_rightValue_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4163:4: (lv_rightValue_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4164:5: lv_rightValue_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntPlusAccess().getRightValueIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntPlusRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getIntPlusAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntPlus"


    // $ANTLR start "entryRuleIntMinus"
    // InternalExtendedCCSL.g:4189:1: entryRuleIntMinus returns [EObject current=null] : iv_ruleIntMinus= ruleIntMinus EOF ;
    public final EObject entryRuleIntMinus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntMinus = null;


        try {
            // InternalExtendedCCSL.g:4189:49: (iv_ruleIntMinus= ruleIntMinus EOF )
            // InternalExtendedCCSL.g:4190:2: iv_ruleIntMinus= ruleIntMinus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntMinusRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntMinus=ruleIntMinus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntMinus; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntMinus"


    // $ANTLR start "ruleIntMinus"
    // InternalExtendedCCSL.g:4196:1: ruleIntMinus returns [EObject current=null] : (otherlv_0= 'IntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleIntMinus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4202:2: ( (otherlv_0= 'IntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4203:2: (otherlv_0= 'IntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4203:2: (otherlv_0= 'IntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4204:3: otherlv_0= 'IntMinus' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,66,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntMinusAccess().getIntMinusKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntMinusAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4212:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==49) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalExtendedCCSL.g:4213:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getIntMinusAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4217:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4218:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4218:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4219:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getIntMinusAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntMinusRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIntMinusAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4240:3: ( (lv_leftValue_5_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4241:4: (lv_leftValue_5_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4241:4: (lv_leftValue_5_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4242:5: lv_leftValue_5_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntMinusAccess().getLeftValueIntegerExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntMinusRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIntMinusAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4263:3: ( (lv_rightValue_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4264:4: (lv_rightValue_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4264:4: (lv_rightValue_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4265:5: lv_rightValue_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntMinusAccess().getRightValueIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntMinusRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getIntMinusAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntMinus"


    // $ANTLR start "entryRuleIntMultiply"
    // InternalExtendedCCSL.g:4290:1: entryRuleIntMultiply returns [EObject current=null] : iv_ruleIntMultiply= ruleIntMultiply EOF ;
    public final EObject entryRuleIntMultiply() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntMultiply = null;


        try {
            // InternalExtendedCCSL.g:4290:52: (iv_ruleIntMultiply= ruleIntMultiply EOF )
            // InternalExtendedCCSL.g:4291:2: iv_ruleIntMultiply= ruleIntMultiply EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntMultiplyRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntMultiply=ruleIntMultiply();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntMultiply; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntMultiply"


    // $ANTLR start "ruleIntMultiply"
    // InternalExtendedCCSL.g:4297:1: ruleIntMultiply returns [EObject current=null] : (otherlv_0= 'IntMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleIntMultiply() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4303:2: ( (otherlv_0= 'IntMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4304:2: (otherlv_0= 'IntMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4304:2: (otherlv_0= 'IntMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4305:3: otherlv_0= 'IntMultiply' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,67,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntMultiplyAccess().getIntMultiplyKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntMultiplyAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4313:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==49) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalExtendedCCSL.g:4314:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getIntMultiplyAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4318:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4319:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4319:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4320:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getIntMultiplyAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntMultiplyRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIntMultiplyAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4341:3: ( (lv_leftValue_5_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4342:4: (lv_leftValue_5_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4342:4: (lv_leftValue_5_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4343:5: lv_leftValue_5_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntMultiplyAccess().getLeftValueIntegerExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntMultiplyRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIntMultiplyAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4364:3: ( (lv_rightValue_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4365:4: (lv_rightValue_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4365:4: (lv_rightValue_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4366:5: lv_rightValue_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntMultiplyAccess().getRightValueIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntMultiplyRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getIntMultiplyAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntMultiply"


    // $ANTLR start "entryRuleIntDivide"
    // InternalExtendedCCSL.g:4391:1: entryRuleIntDivide returns [EObject current=null] : iv_ruleIntDivide= ruleIntDivide EOF ;
    public final EObject entryRuleIntDivide() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntDivide = null;


        try {
            // InternalExtendedCCSL.g:4391:50: (iv_ruleIntDivide= ruleIntDivide EOF )
            // InternalExtendedCCSL.g:4392:2: iv_ruleIntDivide= ruleIntDivide EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntDivideRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntDivide=ruleIntDivide();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntDivide; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntDivide"


    // $ANTLR start "ruleIntDivide"
    // InternalExtendedCCSL.g:4398:1: ruleIntDivide returns [EObject current=null] : (otherlv_0= 'IntDivide' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleIntDivide() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4404:2: ( (otherlv_0= 'IntDivide' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4405:2: (otherlv_0= 'IntDivide' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4405:2: (otherlv_0= 'IntDivide' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4406:3: otherlv_0= 'IntDivide' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,68,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntDivideAccess().getIntDivideKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntDivideAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4414:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==49) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalExtendedCCSL.g:4415:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getIntDivideAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4419:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4420:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4420:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4421:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getIntDivideAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntDivideRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIntDivideAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4442:3: ( (lv_leftValue_5_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4443:4: (lv_leftValue_5_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4443:4: (lv_leftValue_5_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4444:5: lv_leftValue_5_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntDivideAccess().getLeftValueIntegerExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntDivideRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIntDivideAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4465:3: ( (lv_rightValue_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:4466:4: (lv_rightValue_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:4466:4: (lv_rightValue_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:4467:5: lv_rightValue_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntDivideAccess().getRightValueIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntDivideRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getIntDivideAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntDivide"


    // $ANTLR start "entryRuleNot"
    // InternalExtendedCCSL.g:4492:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalExtendedCCSL.g:4492:44: (iv_ruleNot= ruleNot EOF )
            // InternalExtendedCCSL.g:4493:2: iv_ruleNot= ruleNot EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNotRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNot; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalExtendedCCSL.g:4499:1: ruleNot returns [EObject current=null] : (otherlv_0= 'Not' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleBooleanExpression ) ) otherlv_6= '}' ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_operand_5_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4505:2: ( (otherlv_0= 'Not' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleBooleanExpression ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:4506:2: (otherlv_0= 'Not' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleBooleanExpression ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:4506:2: (otherlv_0= 'Not' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleBooleanExpression ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:4507:3: otherlv_0= 'Not' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleBooleanExpression ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,69,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getNotAccess().getNotKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_63); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getNotAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4515:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==49) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalExtendedCCSL.g:4516:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getNotAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4520:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4521:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4521:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4522:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getNotAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getNotRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,56,FollowSets000.FOLLOW_64); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getNotAccess().getOperandKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4543:3: ( (lv_operand_5_0= ruleBooleanExpression ) )
            // InternalExtendedCCSL.g:4544:4: (lv_operand_5_0= ruleBooleanExpression )
            {
            // InternalExtendedCCSL.g:4544:4: (lv_operand_5_0= ruleBooleanExpression )
            // InternalExtendedCCSL.g:4545:5: lv_operand_5_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getNotAccess().getOperandBooleanExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_operand_5_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getNotRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getNotAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRuleAnd"
    // InternalExtendedCCSL.g:4570:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalExtendedCCSL.g:4570:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalExtendedCCSL.g:4571:2: iv_ruleAnd= ruleAnd EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAnd; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalExtendedCCSL.g:4577:1: ruleAnd returns [EObject current=null] : (otherlv_0= 'And' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4583:2: ( (otherlv_0= 'And' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4584:2: (otherlv_0= 'And' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4584:2: (otherlv_0= 'And' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4585:3: otherlv_0= 'And' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,70,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getAndAccess().getAndKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getAndAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4593:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==49) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalExtendedCCSL.g:4594:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getAndAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4598:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4599:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4599:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4600:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getAndAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getAndRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_64); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getAndAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4621:3: ( (lv_leftValue_5_0= ruleBooleanExpression ) )
            // InternalExtendedCCSL.g:4622:4: (lv_leftValue_5_0= ruleBooleanExpression )
            {
            // InternalExtendedCCSL.g:4622:4: (lv_leftValue_5_0= ruleBooleanExpression )
            // InternalExtendedCCSL.g:4623:5: lv_leftValue_5_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAndAccess().getLeftValueBooleanExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getAndRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_64); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getAndAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4644:3: ( (lv_rightValue_7_0= ruleBooleanExpression ) )
            // InternalExtendedCCSL.g:4645:4: (lv_rightValue_7_0= ruleBooleanExpression )
            {
            // InternalExtendedCCSL.g:4645:4: (lv_rightValue_7_0= ruleBooleanExpression )
            // InternalExtendedCCSL.g:4646:5: lv_rightValue_7_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAndAccess().getRightValueBooleanExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getAndRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getAndAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleOr"
    // InternalExtendedCCSL.g:4671:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalExtendedCCSL.g:4671:43: (iv_ruleOr= ruleOr EOF )
            // InternalExtendedCCSL.g:4672:2: iv_ruleOr= ruleOr EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOr; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalExtendedCCSL.g:4678:1: ruleOr returns [EObject current=null] : (otherlv_0= 'Or' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4684:2: ( (otherlv_0= 'Or' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4685:2: (otherlv_0= 'Or' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4685:2: (otherlv_0= 'Or' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4686:3: otherlv_0= 'Or' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,71,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getOrAccess().getOrKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getOrAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4694:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==49) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalExtendedCCSL.g:4695:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getOrAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4699:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4700:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4700:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4701:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getOrAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getOrRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_64); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getOrAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4722:3: ( (lv_leftValue_5_0= ruleBooleanExpression ) )
            // InternalExtendedCCSL.g:4723:4: (lv_leftValue_5_0= ruleBooleanExpression )
            {
            // InternalExtendedCCSL.g:4723:4: (lv_leftValue_5_0= ruleBooleanExpression )
            // InternalExtendedCCSL.g:4724:5: lv_leftValue_5_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getOrAccess().getLeftValueBooleanExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getOrRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_64); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getOrAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4745:3: ( (lv_rightValue_7_0= ruleBooleanExpression ) )
            // InternalExtendedCCSL.g:4746:4: (lv_rightValue_7_0= ruleBooleanExpression )
            {
            // InternalExtendedCCSL.g:4746:4: (lv_rightValue_7_0= ruleBooleanExpression )
            // InternalExtendedCCSL.g:4747:5: lv_rightValue_7_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getOrAccess().getRightValueBooleanExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getOrRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getOrAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleXor"
    // InternalExtendedCCSL.g:4772:1: entryRuleXor returns [EObject current=null] : iv_ruleXor= ruleXor EOF ;
    public final EObject entryRuleXor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXor = null;


        try {
            // InternalExtendedCCSL.g:4772:44: (iv_ruleXor= ruleXor EOF )
            // InternalExtendedCCSL.g:4773:2: iv_ruleXor= ruleXor EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXorRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleXor=ruleXor();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXor; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXor"


    // $ANTLR start "ruleXor"
    // InternalExtendedCCSL.g:4779:1: ruleXor returns [EObject current=null] : (otherlv_0= 'Xor' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleXor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4785:2: ( (otherlv_0= 'Xor' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4786:2: (otherlv_0= 'Xor' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4786:2: (otherlv_0= 'Xor' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4787:3: otherlv_0= 'Xor' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleBooleanExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleBooleanExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,72,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getXorAccess().getXorKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getXorAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4795:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==49) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalExtendedCCSL.g:4796:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getXorAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4800:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4801:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4801:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4802:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getXorAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getXorRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_64); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getXorAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4823:3: ( (lv_leftValue_5_0= ruleBooleanExpression ) )
            // InternalExtendedCCSL.g:4824:4: (lv_leftValue_5_0= ruleBooleanExpression )
            {
            // InternalExtendedCCSL.g:4824:4: (lv_leftValue_5_0= ruleBooleanExpression )
            // InternalExtendedCCSL.g:4825:5: lv_leftValue_5_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getXorAccess().getLeftValueBooleanExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getXorRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_64); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getXorAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4846:3: ( (lv_rightValue_7_0= ruleBooleanExpression ) )
            // InternalExtendedCCSL.g:4847:4: (lv_rightValue_7_0= ruleBooleanExpression )
            {
            // InternalExtendedCCSL.g:4847:4: (lv_rightValue_7_0= ruleBooleanExpression )
            // InternalExtendedCCSL.g:4848:5: lv_rightValue_7_0= ruleBooleanExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getXorAccess().getRightValueBooleanExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getXorRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.BooleanExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getXorAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXor"


    // $ANTLR start "entryRuleRealEqual"
    // InternalExtendedCCSL.g:4873:1: entryRuleRealEqual returns [EObject current=null] : iv_ruleRealEqual= ruleRealEqual EOF ;
    public final EObject entryRuleRealEqual() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealEqual = null;


        try {
            // InternalExtendedCCSL.g:4873:50: (iv_ruleRealEqual= ruleRealEqual EOF )
            // InternalExtendedCCSL.g:4874:2: iv_ruleRealEqual= ruleRealEqual EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealEqualRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealEqual=ruleRealEqual();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealEqual; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealEqual"


    // $ANTLR start "ruleRealEqual"
    // InternalExtendedCCSL.g:4880:1: ruleRealEqual returns [EObject current=null] : (otherlv_0= 'RealEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleRealEqual() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4886:2: ( (otherlv_0= 'RealEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4887:2: (otherlv_0= 'RealEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4887:2: (otherlv_0= 'RealEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4888:3: otherlv_0= 'RealEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,73,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealEqualAccess().getRealEqualKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealEqualAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4896:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==49) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // InternalExtendedCCSL.g:4897:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealEqualAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:4901:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:4902:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:4902:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:4903:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getRealEqualAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealEqualRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealEqualAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:4924:3: ( (lv_leftValue_5_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:4925:4: (lv_leftValue_5_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:4925:4: (lv_leftValue_5_0= ruleRealExpression )
            // InternalExtendedCCSL.g:4926:5: lv_leftValue_5_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealEqualAccess().getLeftValueRealExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealEqualRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRealEqualAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:4947:3: ( (lv_rightValue_7_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:4948:4: (lv_rightValue_7_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:4948:4: (lv_rightValue_7_0= ruleRealExpression )
            // InternalExtendedCCSL.g:4949:5: lv_rightValue_7_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealEqualAccess().getRightValueRealExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealEqualRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getRealEqualAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealEqual"


    // $ANTLR start "entryRuleRealInf"
    // InternalExtendedCCSL.g:4974:1: entryRuleRealInf returns [EObject current=null] : iv_ruleRealInf= ruleRealInf EOF ;
    public final EObject entryRuleRealInf() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealInf = null;


        try {
            // InternalExtendedCCSL.g:4974:48: (iv_ruleRealInf= ruleRealInf EOF )
            // InternalExtendedCCSL.g:4975:2: iv_ruleRealInf= ruleRealInf EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealInfRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealInf=ruleRealInf();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealInf; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealInf"


    // $ANTLR start "ruleRealInf"
    // InternalExtendedCCSL.g:4981:1: ruleRealInf returns [EObject current=null] : (otherlv_0= 'RealInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleRealInf() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:4987:2: ( (otherlv_0= 'RealInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:4988:2: (otherlv_0= 'RealInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:4988:2: (otherlv_0= 'RealInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:4989:3: otherlv_0= 'RealInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,74,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealInfAccess().getRealInfKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealInfAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:4997:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==49) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalExtendedCCSL.g:4998:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealInfAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5002:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5003:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5003:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5004:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getRealInfAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealInfRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealInfAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5025:3: ( (lv_leftValue_5_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:5026:4: (lv_leftValue_5_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:5026:4: (lv_leftValue_5_0= ruleRealExpression )
            // InternalExtendedCCSL.g:5027:5: lv_leftValue_5_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealInfAccess().getLeftValueRealExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealInfRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRealInfAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:5048:3: ( (lv_rightValue_7_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:5049:4: (lv_rightValue_7_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:5049:4: (lv_rightValue_7_0= ruleRealExpression )
            // InternalExtendedCCSL.g:5050:5: lv_rightValue_7_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealInfAccess().getRightValueRealExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealInfRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getRealInfAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealInf"


    // $ANTLR start "entryRuleRealSup"
    // InternalExtendedCCSL.g:5075:1: entryRuleRealSup returns [EObject current=null] : iv_ruleRealSup= ruleRealSup EOF ;
    public final EObject entryRuleRealSup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealSup = null;


        try {
            // InternalExtendedCCSL.g:5075:48: (iv_ruleRealSup= ruleRealSup EOF )
            // InternalExtendedCCSL.g:5076:2: iv_ruleRealSup= ruleRealSup EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealSupRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealSup=ruleRealSup();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealSup; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealSup"


    // $ANTLR start "ruleRealSup"
    // InternalExtendedCCSL.g:5082:1: ruleRealSup returns [EObject current=null] : (otherlv_0= 'RealSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleRealSup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5088:2: ( (otherlv_0= 'RealSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:5089:2: (otherlv_0= 'RealSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:5089:2: (otherlv_0= 'RealSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:5090:3: otherlv_0= 'RealSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleRealExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleRealExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,75,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealSupAccess().getRealSupKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealSupAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5098:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==49) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalExtendedCCSL.g:5099:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealSupAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5103:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5104:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5104:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5105:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getRealSupAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealSupRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealSupAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5126:3: ( (lv_leftValue_5_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:5127:4: (lv_leftValue_5_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:5127:4: (lv_leftValue_5_0= ruleRealExpression )
            // InternalExtendedCCSL.g:5128:5: lv_leftValue_5_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealSupAccess().getLeftValueRealExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealSupRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRealSupAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:5149:3: ( (lv_rightValue_7_0= ruleRealExpression ) )
            // InternalExtendedCCSL.g:5150:4: (lv_rightValue_7_0= ruleRealExpression )
            {
            // InternalExtendedCCSL.g:5150:4: (lv_rightValue_7_0= ruleRealExpression )
            // InternalExtendedCCSL.g:5151:5: lv_rightValue_7_0= ruleRealExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealSupAccess().getRightValueRealExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleRealExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRealSupRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.RealExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getRealSupAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealSup"


    // $ANTLR start "entryRuleIntEqual"
    // InternalExtendedCCSL.g:5176:1: entryRuleIntEqual returns [EObject current=null] : iv_ruleIntEqual= ruleIntEqual EOF ;
    public final EObject entryRuleIntEqual() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntEqual = null;


        try {
            // InternalExtendedCCSL.g:5176:49: (iv_ruleIntEqual= ruleIntEqual EOF )
            // InternalExtendedCCSL.g:5177:2: iv_ruleIntEqual= ruleIntEqual EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntEqualRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntEqual=ruleIntEqual();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntEqual; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntEqual"


    // $ANTLR start "ruleIntEqual"
    // InternalExtendedCCSL.g:5183:1: ruleIntEqual returns [EObject current=null] : (otherlv_0= 'IntEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleIntEqual() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5189:2: ( (otherlv_0= 'IntEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:5190:2: (otherlv_0= 'IntEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:5190:2: (otherlv_0= 'IntEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:5191:3: otherlv_0= 'IntEqual' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,76,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntEqualAccess().getIntEqualKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntEqualAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5199:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==49) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalExtendedCCSL.g:5200:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getIntEqualAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5204:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5205:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5205:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5206:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getIntEqualAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntEqualRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIntEqualAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5227:3: ( (lv_leftValue_5_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:5228:4: (lv_leftValue_5_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:5228:4: (lv_leftValue_5_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:5229:5: lv_leftValue_5_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntEqualAccess().getLeftValueIntegerExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntEqualRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIntEqualAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:5250:3: ( (lv_rightValue_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:5251:4: (lv_rightValue_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:5251:4: (lv_rightValue_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:5252:5: lv_rightValue_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntEqualAccess().getRightValueIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntEqualRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getIntEqualAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntEqual"


    // $ANTLR start "entryRuleIntInf"
    // InternalExtendedCCSL.g:5277:1: entryRuleIntInf returns [EObject current=null] : iv_ruleIntInf= ruleIntInf EOF ;
    public final EObject entryRuleIntInf() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntInf = null;


        try {
            // InternalExtendedCCSL.g:5277:47: (iv_ruleIntInf= ruleIntInf EOF )
            // InternalExtendedCCSL.g:5278:2: iv_ruleIntInf= ruleIntInf EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntInfRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntInf=ruleIntInf();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntInf; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntInf"


    // $ANTLR start "ruleIntInf"
    // InternalExtendedCCSL.g:5284:1: ruleIntInf returns [EObject current=null] : (otherlv_0= 'IntInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleIntInf() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5290:2: ( (otherlv_0= 'IntInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:5291:2: (otherlv_0= 'IntInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:5291:2: (otherlv_0= 'IntInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:5292:3: otherlv_0= 'IntInf' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,77,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntInfAccess().getIntInfKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntInfAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5300:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==49) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalExtendedCCSL.g:5301:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getIntInfAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5305:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5306:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5306:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5307:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getIntInfAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntInfRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIntInfAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5328:3: ( (lv_leftValue_5_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:5329:4: (lv_leftValue_5_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:5329:4: (lv_leftValue_5_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:5330:5: lv_leftValue_5_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntInfAccess().getLeftValueIntegerExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntInfRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIntInfAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:5351:3: ( (lv_rightValue_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:5352:4: (lv_rightValue_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:5352:4: (lv_rightValue_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:5353:5: lv_rightValue_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntInfAccess().getRightValueIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntInfRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getIntInfAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntInf"


    // $ANTLR start "entryRuleIntSup"
    // InternalExtendedCCSL.g:5378:1: entryRuleIntSup returns [EObject current=null] : iv_ruleIntSup= ruleIntSup EOF ;
    public final EObject entryRuleIntSup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntSup = null;


        try {
            // InternalExtendedCCSL.g:5378:47: (iv_ruleIntSup= ruleIntSup EOF )
            // InternalExtendedCCSL.g:5379:2: iv_ruleIntSup= ruleIntSup EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntSupRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntSup=ruleIntSup();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntSup; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntSup"


    // $ANTLR start "ruleIntSup"
    // InternalExtendedCCSL.g:5385:1: ruleIntSup returns [EObject current=null] : (otherlv_0= 'IntSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleIntSup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_leftValue_5_0 = null;

        EObject lv_rightValue_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5391:2: ( (otherlv_0= 'IntSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:5392:2: (otherlv_0= 'IntSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:5392:2: (otherlv_0= 'IntSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:5393:3: otherlv_0= 'IntSup' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'leftValue' ( (lv_leftValue_5_0= ruleIntegerExpression ) ) otherlv_6= 'rightValue' ( (lv_rightValue_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,78,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntSupAccess().getIntSupKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_59); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntSupAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5401:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==49) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalExtendedCCSL.g:5402:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getIntSupAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5406:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5407:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5407:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5408:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getIntSupAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntSupRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,59,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIntSupAccess().getLeftValueKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5429:3: ( (lv_leftValue_5_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:5430:4: (lv_leftValue_5_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:5430:4: (lv_leftValue_5_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:5431:5: lv_leftValue_5_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntSupAccess().getLeftValueIntegerExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_61);
            lv_leftValue_5_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntSupRule());
              					}
              					set(
              						current,
              						"leftValue",
              						lv_leftValue_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,60,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIntSupAccess().getRightValueKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:5452:3: ( (lv_rightValue_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:5453:4: (lv_rightValue_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:5453:4: (lv_rightValue_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:5454:5: lv_rightValue_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntSupAccess().getRightValueIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_rightValue_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntSupRule());
              					}
              					set(
              						current,
              						"rightValue",
              						lv_rightValue_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getIntSupAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntSup"


    // $ANTLR start "entryRuleSeqIsEmpty"
    // InternalExtendedCCSL.g:5479:1: entryRuleSeqIsEmpty returns [EObject current=null] : iv_ruleSeqIsEmpty= ruleSeqIsEmpty EOF ;
    public final EObject entryRuleSeqIsEmpty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeqIsEmpty = null;


        try {
            // InternalExtendedCCSL.g:5479:51: (iv_ruleSeqIsEmpty= ruleSeqIsEmpty EOF )
            // InternalExtendedCCSL.g:5480:2: iv_ruleSeqIsEmpty= ruleSeqIsEmpty EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSeqIsEmptyRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSeqIsEmpty=ruleSeqIsEmpty();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSeqIsEmpty; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeqIsEmpty"


    // $ANTLR start "ruleSeqIsEmpty"
    // InternalExtendedCCSL.g:5486:1: ruleSeqIsEmpty returns [EObject current=null] : (otherlv_0= 'SeqIsEmpty' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' ) ;
    public final EObject ruleSeqIsEmpty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_operand_5_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5492:2: ( (otherlv_0= 'SeqIsEmpty' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:5493:2: (otherlv_0= 'SeqIsEmpty' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:5493:2: (otherlv_0= 'SeqIsEmpty' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:5494:3: otherlv_0= 'SeqIsEmpty' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,79,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getSeqIsEmptyAccess().getSeqIsEmptyKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_63); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSeqIsEmptyAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5502:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==49) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // InternalExtendedCCSL.g:5503:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getSeqIsEmptyAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5507:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5508:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5508:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5509:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getSeqIsEmptyAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getSeqIsEmptyRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,56,FollowSets000.FOLLOW_65); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getSeqIsEmptyAccess().getOperandKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5530:3: ( (lv_operand_5_0= ruleSeqExpression ) )
            // InternalExtendedCCSL.g:5531:4: (lv_operand_5_0= ruleSeqExpression )
            {
            // InternalExtendedCCSL.g:5531:4: (lv_operand_5_0= ruleSeqExpression )
            // InternalExtendedCCSL.g:5532:5: lv_operand_5_0= ruleSeqExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSeqIsEmptyAccess().getOperandSeqExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_operand_5_0=ruleSeqExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSeqIsEmptyRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.SeqExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getSeqIsEmptyAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeqIsEmpty"


    // $ANTLR start "entryRuleSeqGetTail"
    // InternalExtendedCCSL.g:5557:1: entryRuleSeqGetTail returns [EObject current=null] : iv_ruleSeqGetTail= ruleSeqGetTail EOF ;
    public final EObject entryRuleSeqGetTail() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeqGetTail = null;


        try {
            // InternalExtendedCCSL.g:5557:51: (iv_ruleSeqGetTail= ruleSeqGetTail EOF )
            // InternalExtendedCCSL.g:5558:2: iv_ruleSeqGetTail= ruleSeqGetTail EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSeqGetTailRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSeqGetTail=ruleSeqGetTail();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSeqGetTail; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeqGetTail"


    // $ANTLR start "ruleSeqGetTail"
    // InternalExtendedCCSL.g:5564:1: ruleSeqGetTail returns [EObject current=null] : (otherlv_0= 'SeqGetTail' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' ) ;
    public final EObject ruleSeqGetTail() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_operand_5_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5570:2: ( (otherlv_0= 'SeqGetTail' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:5571:2: (otherlv_0= 'SeqGetTail' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:5571:2: (otherlv_0= 'SeqGetTail' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:5572:3: otherlv_0= 'SeqGetTail' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,80,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getSeqGetTailAccess().getSeqGetTailKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_63); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSeqGetTailAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5580:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==49) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalExtendedCCSL.g:5581:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getSeqGetTailAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5585:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5586:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5586:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5587:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getSeqGetTailAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getSeqGetTailRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,56,FollowSets000.FOLLOW_65); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getSeqGetTailAccess().getOperandKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5608:3: ( (lv_operand_5_0= ruleSeqExpression ) )
            // InternalExtendedCCSL.g:5609:4: (lv_operand_5_0= ruleSeqExpression )
            {
            // InternalExtendedCCSL.g:5609:4: (lv_operand_5_0= ruleSeqExpression )
            // InternalExtendedCCSL.g:5610:5: lv_operand_5_0= ruleSeqExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSeqGetTailAccess().getOperandSeqExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_operand_5_0=ruleSeqExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSeqGetTailRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.SeqExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getSeqGetTailAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeqGetTail"


    // $ANTLR start "entryRuleSeqGetHead"
    // InternalExtendedCCSL.g:5635:1: entryRuleSeqGetHead returns [EObject current=null] : iv_ruleSeqGetHead= ruleSeqGetHead EOF ;
    public final EObject entryRuleSeqGetHead() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeqGetHead = null;


        try {
            // InternalExtendedCCSL.g:5635:51: (iv_ruleSeqGetHead= ruleSeqGetHead EOF )
            // InternalExtendedCCSL.g:5636:2: iv_ruleSeqGetHead= ruleSeqGetHead EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSeqGetHeadRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSeqGetHead=ruleSeqGetHead();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSeqGetHead; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeqGetHead"


    // $ANTLR start "ruleSeqGetHead"
    // InternalExtendedCCSL.g:5642:1: ruleSeqGetHead returns [EObject current=null] : (otherlv_0= 'SeqGetHead' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_operand_3_0= ruleSeqExpression ) ) otherlv_4= ')' ) ;
    public final EObject ruleSeqGetHead() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_operand_3_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5648:2: ( (otherlv_0= 'SeqGetHead' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_operand_3_0= ruleSeqExpression ) ) otherlv_4= ')' ) )
            // InternalExtendedCCSL.g:5649:2: (otherlv_0= 'SeqGetHead' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_operand_3_0= ruleSeqExpression ) ) otherlv_4= ')' )
            {
            // InternalExtendedCCSL.g:5649:2: (otherlv_0= 'SeqGetHead' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_operand_3_0= ruleSeqExpression ) ) otherlv_4= ')' )
            // InternalExtendedCCSL.g:5650:3: otherlv_0= 'SeqGetHead' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_operand_3_0= ruleSeqExpression ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,81,FollowSets000.FOLLOW_66); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getSeqGetHeadAccess().getSeqGetHeadKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:5654:3: ( (lv_name_1_0= RULE_ID ) )?
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==RULE_ID) ) {
                alt69=1;
            }
            switch (alt69) {
                case 1 :
                    // InternalExtendedCCSL.g:5655:4: (lv_name_1_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5655:4: (lv_name_1_0= RULE_ID )
                    // InternalExtendedCCSL.g:5656:5: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_name_1_0, grammarAccess.getSeqGetHeadAccess().getNameIDTerminalRuleCall_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSeqGetHeadRule());
                      					}
                      					setWithLastConsumed(
                      						current,
                      						"name",
                      						lv_name_1_0,
                      						"org.eclipse.xtext.common.Terminals.ID");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,30,FollowSets000.FOLLOW_65); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getSeqGetHeadAccess().getLeftParenthesisKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:5676:3: ( (lv_operand_3_0= ruleSeqExpression ) )
            // InternalExtendedCCSL.g:5677:4: (lv_operand_3_0= ruleSeqExpression )
            {
            // InternalExtendedCCSL.g:5677:4: (lv_operand_3_0= ruleSeqExpression )
            // InternalExtendedCCSL.g:5678:5: lv_operand_3_0= ruleSeqExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSeqGetHeadAccess().getOperandSeqExpressionParserRuleCall_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_67);
            lv_operand_3_0=ruleSeqExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSeqGetHeadRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_3_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.SeqExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,31,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getSeqGetHeadAccess().getRightParenthesisKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeqGetHead"


    // $ANTLR start "entryRuleSeqDecr"
    // InternalExtendedCCSL.g:5703:1: entryRuleSeqDecr returns [EObject current=null] : iv_ruleSeqDecr= ruleSeqDecr EOF ;
    public final EObject entryRuleSeqDecr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeqDecr = null;


        try {
            // InternalExtendedCCSL.g:5703:48: (iv_ruleSeqDecr= ruleSeqDecr EOF )
            // InternalExtendedCCSL.g:5704:2: iv_ruleSeqDecr= ruleSeqDecr EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSeqDecrRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSeqDecr=ruleSeqDecr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSeqDecr; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeqDecr"


    // $ANTLR start "ruleSeqDecr"
    // InternalExtendedCCSL.g:5710:1: ruleSeqDecr returns [EObject current=null] : (otherlv_0= 'SeqDecr' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' ) ;
    public final EObject ruleSeqDecr() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_operand_5_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5716:2: ( (otherlv_0= 'SeqDecr' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:5717:2: (otherlv_0= 'SeqDecr' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:5717:2: (otherlv_0= 'SeqDecr' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:5718:3: otherlv_0= 'SeqDecr' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,82,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getSeqDecrAccess().getSeqDecrKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_63); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSeqDecrAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5726:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==49) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // InternalExtendedCCSL.g:5727:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getSeqDecrAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5731:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5732:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5732:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5733:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getSeqDecrAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getSeqDecrRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,56,FollowSets000.FOLLOW_65); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getSeqDecrAccess().getOperandKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5754:3: ( (lv_operand_5_0= ruleSeqExpression ) )
            // InternalExtendedCCSL.g:5755:4: (lv_operand_5_0= ruleSeqExpression )
            {
            // InternalExtendedCCSL.g:5755:4: (lv_operand_5_0= ruleSeqExpression )
            // InternalExtendedCCSL.g:5756:5: lv_operand_5_0= ruleSeqExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSeqDecrAccess().getOperandSeqExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_operand_5_0=ruleSeqExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSeqDecrRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.SeqExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getSeqDecrAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeqDecr"


    // $ANTLR start "entryRuleSeqSched"
    // InternalExtendedCCSL.g:5781:1: entryRuleSeqSched returns [EObject current=null] : iv_ruleSeqSched= ruleSeqSched EOF ;
    public final EObject entryRuleSeqSched() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSeqSched = null;


        try {
            // InternalExtendedCCSL.g:5781:49: (iv_ruleSeqSched= ruleSeqSched EOF )
            // InternalExtendedCCSL.g:5782:2: iv_ruleSeqSched= ruleSeqSched EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSeqSchedRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSeqSched=ruleSeqSched();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSeqSched; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSeqSched"


    // $ANTLR start "ruleSeqSched"
    // InternalExtendedCCSL.g:5788:1: ruleSeqSched returns [EObject current=null] : (otherlv_0= 'SeqSched' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= 'integerExpr' ( (lv_integerExpr_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) ;
    public final EObject ruleSeqSched() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_operand_5_0 = null;

        EObject lv_integerExpr_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:5794:2: ( (otherlv_0= 'SeqSched' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= 'integerExpr' ( (lv_integerExpr_7_0= ruleIntegerExpression ) ) otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:5795:2: (otherlv_0= 'SeqSched' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= 'integerExpr' ( (lv_integerExpr_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:5795:2: (otherlv_0= 'SeqSched' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= 'integerExpr' ( (lv_integerExpr_7_0= ruleIntegerExpression ) ) otherlv_8= '}' )
            // InternalExtendedCCSL.g:5796:3: otherlv_0= 'SeqSched' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleSeqExpression ) ) otherlv_6= 'integerExpr' ( (lv_integerExpr_7_0= ruleIntegerExpression ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,83,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getSeqSchedAccess().getSeqSchedKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_63); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSeqSchedAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5804:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( (LA71_0==49) ) {
                alt71=1;
            }
            switch (alt71) {
                case 1 :
                    // InternalExtendedCCSL.g:5805:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getSeqSchedAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5809:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5810:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5810:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5811:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getSeqSchedAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getSeqSchedRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,56,FollowSets000.FOLLOW_65); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getSeqSchedAccess().getOperandKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5832:3: ( (lv_operand_5_0= ruleSeqExpression ) )
            // InternalExtendedCCSL.g:5833:4: (lv_operand_5_0= ruleSeqExpression )
            {
            // InternalExtendedCCSL.g:5833:4: (lv_operand_5_0= ruleSeqExpression )
            // InternalExtendedCCSL.g:5834:5: lv_operand_5_0= ruleSeqExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSeqSchedAccess().getOperandSeqExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_68);
            lv_operand_5_0=ruleSeqExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSeqSchedRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_5_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.SeqExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,84,FollowSets000.FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getSeqSchedAccess().getIntegerExprKeyword_5());
              		
            }
            // InternalExtendedCCSL.g:5855:3: ( (lv_integerExpr_7_0= ruleIntegerExpression ) )
            // InternalExtendedCCSL.g:5856:4: (lv_integerExpr_7_0= ruleIntegerExpression )
            {
            // InternalExtendedCCSL.g:5856:4: (lv_integerExpr_7_0= ruleIntegerExpression )
            // InternalExtendedCCSL.g:5857:5: lv_integerExpr_7_0= ruleIntegerExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSeqSchedAccess().getIntegerExprIntegerExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            lv_integerExpr_7_0=ruleIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSeqSchedRule());
              					}
              					set(
              						current,
              						"integerExpr",
              						lv_integerExpr_7_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.IntegerExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getSeqSchedAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSeqSched"


    // $ANTLR start "entryRuleBooleanVariableRef"
    // InternalExtendedCCSL.g:5882:1: entryRuleBooleanVariableRef returns [EObject current=null] : iv_ruleBooleanVariableRef= ruleBooleanVariableRef EOF ;
    public final EObject entryRuleBooleanVariableRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanVariableRef = null;


        try {
            // InternalExtendedCCSL.g:5882:59: (iv_ruleBooleanVariableRef= ruleBooleanVariableRef EOF )
            // InternalExtendedCCSL.g:5883:2: iv_ruleBooleanVariableRef= ruleBooleanVariableRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanVariableRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanVariableRef=ruleBooleanVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanVariableRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanVariableRef"


    // $ANTLR start "ruleBooleanVariableRef"
    // InternalExtendedCCSL.g:5889:1: ruleBooleanVariableRef returns [EObject current=null] : (otherlv_0= 'BooleanVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleBooleanVariableRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:5895:2: ( (otherlv_0= 'BooleanVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:5896:2: (otherlv_0= 'BooleanVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:5896:2: (otherlv_0= 'BooleanVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:5897:3: otherlv_0= 'BooleanVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,85,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getBooleanVariableRefAccess().getBooleanVariableRefKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_69); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBooleanVariableRefAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5905:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt72=2;
            int LA72_0 = input.LA(1);

            if ( (LA72_0==49) ) {
                alt72=1;
            }
            switch (alt72) {
                case 1 :
                    // InternalExtendedCCSL.g:5906:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getBooleanVariableRefAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5910:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5911:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5911:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5912:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_70); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getBooleanVariableRefAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getBooleanVariableRefRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,86,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getBooleanVariableRefAccess().getReferencedVarKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:5933:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:5934:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:5934:4: ( ruleEString )
            // InternalExtendedCCSL.g:5935:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBooleanVariableRefRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBooleanVariableRefAccess().getReferencedVarAbstractEntityCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getBooleanVariableRefAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanVariableRef"


    // $ANTLR start "entryRuleIntegerVariableRef"
    // InternalExtendedCCSL.g:5960:1: entryRuleIntegerVariableRef returns [EObject current=null] : iv_ruleIntegerVariableRef= ruleIntegerVariableRef EOF ;
    public final EObject entryRuleIntegerVariableRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerVariableRef = null;


        try {
            // InternalExtendedCCSL.g:5960:59: (iv_ruleIntegerVariableRef= ruleIntegerVariableRef EOF )
            // InternalExtendedCCSL.g:5961:2: iv_ruleIntegerVariableRef= ruleIntegerVariableRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerVariableRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerVariableRef=ruleIntegerVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerVariableRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerVariableRef"


    // $ANTLR start "ruleIntegerVariableRef"
    // InternalExtendedCCSL.g:5967:1: ruleIntegerVariableRef returns [EObject current=null] : (otherlv_0= 'IntegerVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleIntegerVariableRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:5973:2: ( (otherlv_0= 'IntegerVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:5974:2: (otherlv_0= 'IntegerVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:5974:2: (otherlv_0= 'IntegerVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:5975:3: otherlv_0= 'IntegerVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,87,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIntegerVariableRefAccess().getIntegerVariableRefKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_69); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntegerVariableRefAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:5983:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt73=2;
            int LA73_0 = input.LA(1);

            if ( (LA73_0==49) ) {
                alt73=1;
            }
            switch (alt73) {
                case 1 :
                    // InternalExtendedCCSL.g:5984:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getIntegerVariableRefAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:5988:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:5989:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:5989:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:5990:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_70); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getIntegerVariableRefAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getIntegerVariableRefRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,86,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getIntegerVariableRefAccess().getReferencedVarKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:6011:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:6012:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:6012:4: ( ruleEString )
            // InternalExtendedCCSL.g:6013:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getIntegerVariableRefRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntegerVariableRefAccess().getReferencedVarAbstractEntityCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getIntegerVariableRefAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerVariableRef"


    // $ANTLR start "entryRuleRealVariableRef"
    // InternalExtendedCCSL.g:6038:1: entryRuleRealVariableRef returns [EObject current=null] : iv_ruleRealVariableRef= ruleRealVariableRef EOF ;
    public final EObject entryRuleRealVariableRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRealVariableRef = null;


        try {
            // InternalExtendedCCSL.g:6038:56: (iv_ruleRealVariableRef= ruleRealVariableRef EOF )
            // InternalExtendedCCSL.g:6039:2: iv_ruleRealVariableRef= ruleRealVariableRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealVariableRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRealVariableRef=ruleRealVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRealVariableRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealVariableRef"


    // $ANTLR start "ruleRealVariableRef"
    // InternalExtendedCCSL.g:6045:1: ruleRealVariableRef returns [EObject current=null] : (otherlv_0= 'RealVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleRealVariableRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6051:2: ( (otherlv_0= 'RealVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:6052:2: (otherlv_0= 'RealVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:6052:2: (otherlv_0= 'RealVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:6053:3: otherlv_0= 'RealVariableRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedVar' ( ( ruleEString ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,88,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRealVariableRefAccess().getRealVariableRefKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_69); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealVariableRefAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6061:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==49) ) {
                alt74=1;
            }
            switch (alt74) {
                case 1 :
                    // InternalExtendedCCSL.g:6062:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRealVariableRefAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:6066:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:6067:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:6067:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:6068:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_70); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getRealVariableRefAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRealVariableRefRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,86,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRealVariableRefAccess().getReferencedVarKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:6089:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:6090:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:6090:4: ( ruleEString )
            // InternalExtendedCCSL.g:6091:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRealVariableRefRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRealVariableRefAccess().getReferencedVarAbstractEntityCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRealVariableRefAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealVariableRef"


    // $ANTLR start "entryRuleNumberSeqRef"
    // InternalExtendedCCSL.g:6116:1: entryRuleNumberSeqRef returns [EObject current=null] : iv_ruleNumberSeqRef= ruleNumberSeqRef EOF ;
    public final EObject entryRuleNumberSeqRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberSeqRef = null;


        try {
            // InternalExtendedCCSL.g:6116:53: (iv_ruleNumberSeqRef= ruleNumberSeqRef EOF )
            // InternalExtendedCCSL.g:6117:2: iv_ruleNumberSeqRef= ruleNumberSeqRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberSeqRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNumberSeqRef=ruleNumberSeqRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberSeqRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberSeqRef"


    // $ANTLR start "ruleNumberSeqRef"
    // InternalExtendedCCSL.g:6123:1: ruleNumberSeqRef returns [EObject current=null] : (otherlv_0= 'NumberSeqRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedNumberSeq' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleNumberSeqRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6129:2: ( (otherlv_0= 'NumberSeqRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedNumberSeq' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:6130:2: (otherlv_0= 'NumberSeqRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedNumberSeq' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:6130:2: (otherlv_0= 'NumberSeqRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedNumberSeq' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:6131:3: otherlv_0= 'NumberSeqRef' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'referencedNumberSeq' ( ( ruleEString ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,89,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getNumberSeqRefAccess().getNumberSeqRefKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_71); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getNumberSeqRefAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6139:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt75=2;
            int LA75_0 = input.LA(1);

            if ( (LA75_0==49) ) {
                alt75=1;
            }
            switch (alt75) {
                case 1 :
                    // InternalExtendedCCSL.g:6140:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getNumberSeqRefAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:6144:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:6145:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:6145:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:6146:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_72); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getNumberSeqRefAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getNumberSeqRefRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,90,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getNumberSeqRefAccess().getReferencedNumberSeqKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:6167:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:6168:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:6168:4: ( ruleEString )
            // InternalExtendedCCSL.g:6169:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getNumberSeqRefRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getNumberSeqRefAccess().getReferencedNumberSeqSequenceElementCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getNumberSeqRefAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberSeqRef"


    // $ANTLR start "entryRuleNumberSeqVariableRef"
    // InternalExtendedCCSL.g:6194:1: entryRuleNumberSeqVariableRef returns [EObject current=null] : iv_ruleNumberSeqVariableRef= ruleNumberSeqVariableRef EOF ;
    public final EObject entryRuleNumberSeqVariableRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberSeqVariableRef = null;


        try {
            // InternalExtendedCCSL.g:6194:61: (iv_ruleNumberSeqVariableRef= ruleNumberSeqVariableRef EOF )
            // InternalExtendedCCSL.g:6195:2: iv_ruleNumberSeqVariableRef= ruleNumberSeqVariableRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberSeqVariableRefRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNumberSeqVariableRef=ruleNumberSeqVariableRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberSeqVariableRef; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberSeqVariableRef"


    // $ANTLR start "ruleNumberSeqVariableRef"
    // InternalExtendedCCSL.g:6201:1: ruleNumberSeqVariableRef returns [EObject current=null] : (otherlv_0= 'SequenceVarRef' otherlv_1= '=' ( ( ruleEString ) ) ) ;
    public final EObject ruleNumberSeqVariableRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6207:2: ( (otherlv_0= 'SequenceVarRef' otherlv_1= '=' ( ( ruleEString ) ) ) )
            // InternalExtendedCCSL.g:6208:2: (otherlv_0= 'SequenceVarRef' otherlv_1= '=' ( ( ruleEString ) ) )
            {
            // InternalExtendedCCSL.g:6208:2: (otherlv_0= 'SequenceVarRef' otherlv_1= '=' ( ( ruleEString ) ) )
            // InternalExtendedCCSL.g:6209:3: otherlv_0= 'SequenceVarRef' otherlv_1= '=' ( ( ruleEString ) )
            {
            otherlv_0=(Token)match(input,91,FollowSets000.FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getNumberSeqVariableRefAccess().getSequenceVarRefKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,33,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getNumberSeqVariableRefAccess().getEqualsSignKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6217:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:6218:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:6218:4: ( ruleEString )
            // InternalExtendedCCSL.g:6219:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getNumberSeqVariableRefRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getNumberSeqVariableRefAccess().getReferencedVarAbstractEntityCrossReference_2_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberSeqVariableRef"


    // $ANTLR start "entryRuleString1"
    // InternalExtendedCCSL.g:6240:1: entryRuleString1 returns [EObject current=null] : iv_ruleString1= ruleString1 EOF ;
    public final EObject entryRuleString1() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleString1 = null;


        try {
            // InternalExtendedCCSL.g:6240:48: (iv_ruleString1= ruleString1 EOF )
            // InternalExtendedCCSL.g:6241:2: iv_ruleString1= ruleString1 EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getString1Rule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleString1=ruleString1();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleString1; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleString1"


    // $ANTLR start "ruleString1"
    // InternalExtendedCCSL.g:6247:1: ruleString1 returns [EObject current=null] : ( () otherlv_1= 'StringType' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleString1() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6253:2: ( ( () otherlv_1= 'StringType' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalExtendedCCSL.g:6254:2: ( () otherlv_1= 'StringType' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalExtendedCCSL.g:6254:2: ( () otherlv_1= 'StringType' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalExtendedCCSL.g:6255:3: () otherlv_1= 'StringType' ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalExtendedCCSL.g:6255:3: ()
            // InternalExtendedCCSL.g:6256:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getString1Access().getStringAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,92,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getString1Access().getStringTypeKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6269:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6270:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6270:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:6271:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getString1Access().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getString1Rule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleString1"


    // $ANTLR start "entryRuleBoolean"
    // InternalExtendedCCSL.g:6291:1: entryRuleBoolean returns [EObject current=null] : iv_ruleBoolean= ruleBoolean EOF ;
    public final EObject entryRuleBoolean() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolean = null;


        try {
            // InternalExtendedCCSL.g:6291:48: (iv_ruleBoolean= ruleBoolean EOF )
            // InternalExtendedCCSL.g:6292:2: iv_ruleBoolean= ruleBoolean EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBoolean=ruleBoolean();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBoolean; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolean"


    // $ANTLR start "ruleBoolean"
    // InternalExtendedCCSL.g:6298:1: ruleBoolean returns [EObject current=null] : ( () otherlv_1= 'BooleanType' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleBoolean() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6304:2: ( ( () otherlv_1= 'BooleanType' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalExtendedCCSL.g:6305:2: ( () otherlv_1= 'BooleanType' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalExtendedCCSL.g:6305:2: ( () otherlv_1= 'BooleanType' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalExtendedCCSL.g:6306:3: () otherlv_1= 'BooleanType' ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalExtendedCCSL.g:6306:3: ()
            // InternalExtendedCCSL.g:6307:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getBooleanAccess().getBooleanAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,93,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBooleanAccess().getBooleanTypeKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6320:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6321:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6321:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:6322:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getBooleanAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBooleanRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolean"


    // $ANTLR start "entryRuleInteger"
    // InternalExtendedCCSL.g:6342:1: entryRuleInteger returns [EObject current=null] : iv_ruleInteger= ruleInteger EOF ;
    public final EObject entryRuleInteger() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteger = null;


        try {
            // InternalExtendedCCSL.g:6342:48: (iv_ruleInteger= ruleInteger EOF )
            // InternalExtendedCCSL.g:6343:2: iv_ruleInteger= ruleInteger EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteger=ruleInteger();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteger; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteger"


    // $ANTLR start "ruleInteger"
    // InternalExtendedCCSL.g:6349:1: ruleInteger returns [EObject current=null] : ( () otherlv_1= 'IntegerType' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleInteger() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6355:2: ( ( () otherlv_1= 'IntegerType' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalExtendedCCSL.g:6356:2: ( () otherlv_1= 'IntegerType' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalExtendedCCSL.g:6356:2: ( () otherlv_1= 'IntegerType' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalExtendedCCSL.g:6357:3: () otherlv_1= 'IntegerType' ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalExtendedCCSL.g:6357:3: ()
            // InternalExtendedCCSL.g:6358:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getIntegerAccess().getIntegerAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,94,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntegerAccess().getIntegerTypeKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6371:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6372:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6372:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:6373:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getIntegerAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getIntegerRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteger"


    // $ANTLR start "entryRuleReal"
    // InternalExtendedCCSL.g:6393:1: entryRuleReal returns [EObject current=null] : iv_ruleReal= ruleReal EOF ;
    public final EObject entryRuleReal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReal = null;


        try {
            // InternalExtendedCCSL.g:6393:45: (iv_ruleReal= ruleReal EOF )
            // InternalExtendedCCSL.g:6394:2: iv_ruleReal= ruleReal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRealRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleReal=ruleReal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReal; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReal"


    // $ANTLR start "ruleReal"
    // InternalExtendedCCSL.g:6400:1: ruleReal returns [EObject current=null] : ( () otherlv_1= 'RealType' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleReal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6406:2: ( ( () otherlv_1= 'RealType' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalExtendedCCSL.g:6407:2: ( () otherlv_1= 'RealType' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalExtendedCCSL.g:6407:2: ( () otherlv_1= 'RealType' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalExtendedCCSL.g:6408:3: () otherlv_1= 'RealType' ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalExtendedCCSL.g:6408:3: ()
            // InternalExtendedCCSL.g:6409:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getRealAccess().getRealAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,95,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getRealAccess().getRealTypeKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6422:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6423:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6423:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:6424:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getRealAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRealRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReal"


    // $ANTLR start "entryRuleChar"
    // InternalExtendedCCSL.g:6444:1: entryRuleChar returns [EObject current=null] : iv_ruleChar= ruleChar EOF ;
    public final EObject entryRuleChar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChar = null;


        try {
            // InternalExtendedCCSL.g:6444:45: (iv_ruleChar= ruleChar EOF )
            // InternalExtendedCCSL.g:6445:2: iv_ruleChar= ruleChar EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCharRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleChar=ruleChar();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleChar; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChar"


    // $ANTLR start "ruleChar"
    // InternalExtendedCCSL.g:6451:1: ruleChar returns [EObject current=null] : ( () otherlv_1= 'CharType' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleChar() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6457:2: ( ( () otherlv_1= 'CharType' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalExtendedCCSL.g:6458:2: ( () otherlv_1= 'CharType' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalExtendedCCSL.g:6458:2: ( () otherlv_1= 'CharType' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalExtendedCCSL.g:6459:3: () otherlv_1= 'CharType' ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalExtendedCCSL.g:6459:3: ()
            // InternalExtendedCCSL.g:6460:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getCharAccess().getCharAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,96,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getCharAccess().getCharTypeKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6473:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6474:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6474:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:6475:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getCharAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getCharRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChar"


    // $ANTLR start "entryRuleRecord"
    // InternalExtendedCCSL.g:6495:1: entryRuleRecord returns [EObject current=null] : iv_ruleRecord= ruleRecord EOF ;
    public final EObject entryRuleRecord() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRecord = null;


        try {
            // InternalExtendedCCSL.g:6495:47: (iv_ruleRecord= ruleRecord EOF )
            // InternalExtendedCCSL.g:6496:2: iv_ruleRecord= ruleRecord EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRecordRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRecord=ruleRecord();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRecord; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRecord"


    // $ANTLR start "ruleRecord"
    // InternalExtendedCCSL.g:6502:1: ruleRecord returns [EObject current=null] : (otherlv_0= 'RecordType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_field_3_0= ruleField ) ) (otherlv_4= ',' ( (lv_field_5_0= ruleField ) ) )* otherlv_6= '}' ) ;
    public final EObject ruleRecord() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_field_3_0 = null;

        EObject lv_field_5_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:6508:2: ( (otherlv_0= 'RecordType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_field_3_0= ruleField ) ) (otherlv_4= ',' ( (lv_field_5_0= ruleField ) ) )* otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:6509:2: (otherlv_0= 'RecordType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_field_3_0= ruleField ) ) (otherlv_4= ',' ( (lv_field_5_0= ruleField ) ) )* otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:6509:2: (otherlv_0= 'RecordType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_field_3_0= ruleField ) ) (otherlv_4= ',' ( (lv_field_5_0= ruleField ) ) )* otherlv_6= '}' )
            // InternalExtendedCCSL.g:6510:3: otherlv_0= 'RecordType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_field_3_0= ruleField ) ) (otherlv_4= ',' ( (lv_field_5_0= ruleField ) ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,97,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRecordAccess().getRecordTypeKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:6514:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6515:4: (lv_name_1_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6515:4: (lv_name_1_0= RULE_ID )
            // InternalExtendedCCSL.g:6516:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getRecordAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRecordRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,13,FollowSets000.FOLLOW_73); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getRecordAccess().getLeftCurlyBracketKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:6536:3: ( (lv_field_3_0= ruleField ) )
            // InternalExtendedCCSL.g:6537:4: (lv_field_3_0= ruleField )
            {
            // InternalExtendedCCSL.g:6537:4: (lv_field_3_0= ruleField )
            // InternalExtendedCCSL.g:6538:5: lv_field_3_0= ruleField
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRecordAccess().getFieldFieldParserRuleCall_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_12);
            lv_field_3_0=ruleField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRecordRule());
              					}
              					add(
              						current,
              						"field",
              						lv_field_3_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Field");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalExtendedCCSL.g:6555:3: (otherlv_4= ',' ( (lv_field_5_0= ruleField ) ) )*
            loop76:
            do {
                int alt76=2;
                int LA76_0 = input.LA(1);

                if ( (LA76_0==18) ) {
                    alt76=1;
                }


                switch (alt76) {
            	case 1 :
            	    // InternalExtendedCCSL.g:6556:4: otherlv_4= ',' ( (lv_field_5_0= ruleField ) )
            	    {
            	    otherlv_4=(Token)match(input,18,FollowSets000.FOLLOW_73); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_4, grammarAccess.getRecordAccess().getCommaKeyword_4_0());
            	      			
            	    }
            	    // InternalExtendedCCSL.g:6560:4: ( (lv_field_5_0= ruleField ) )
            	    // InternalExtendedCCSL.g:6561:5: (lv_field_5_0= ruleField )
            	    {
            	    // InternalExtendedCCSL.g:6561:5: (lv_field_5_0= ruleField )
            	    // InternalExtendedCCSL.g:6562:6: lv_field_5_0= ruleField
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getRecordAccess().getFieldFieldParserRuleCall_4_1_0());
            	      					
            	    }
            	    pushFollow(FollowSets000.FOLLOW_12);
            	    lv_field_5_0=ruleField();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getRecordRule());
            	      						}
            	      						add(
            	      							current,
            	      							"field",
            	      							lv_field_5_0,
            	      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.Field");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop76;
                }
            } while (true);

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRecordAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRecord"


    // $ANTLR start "entryRuleSequenceType"
    // InternalExtendedCCSL.g:6588:1: entryRuleSequenceType returns [EObject current=null] : iv_ruleSequenceType= ruleSequenceType EOF ;
    public final EObject entryRuleSequenceType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSequenceType = null;


        try {
            // InternalExtendedCCSL.g:6588:53: (iv_ruleSequenceType= ruleSequenceType EOF )
            // InternalExtendedCCSL.g:6589:2: iv_ruleSequenceType= ruleSequenceType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSequenceTypeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSequenceType=ruleSequenceType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSequenceType; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSequenceType"


    // $ANTLR start "ruleSequenceType"
    // InternalExtendedCCSL.g:6595:1: ruleSequenceType returns [EObject current=null] : (otherlv_0= 'SequenceType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleEString ) ) ) ;
    public final EObject ruleSequenceType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6601:2: ( (otherlv_0= 'SequenceType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleEString ) ) ) )
            // InternalExtendedCCSL.g:6602:2: (otherlv_0= 'SequenceType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleEString ) ) )
            {
            // InternalExtendedCCSL.g:6602:2: (otherlv_0= 'SequenceType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleEString ) ) )
            // InternalExtendedCCSL.g:6603:3: otherlv_0= 'SequenceType' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleEString ) )
            {
            otherlv_0=(Token)match(input,98,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getSequenceTypeAccess().getSequenceTypeKeyword_0());
              		
            }
            // InternalExtendedCCSL.g:6607:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6608:4: (lv_name_1_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6608:4: (lv_name_1_0= RULE_ID )
            // InternalExtendedCCSL.g:6609:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_48); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getSequenceTypeAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getSequenceTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,37,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getSequenceTypeAccess().getColonKeyword_2());
              		
            }
            // InternalExtendedCCSL.g:6629:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:6630:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:6630:4: ( ruleEString )
            // InternalExtendedCCSL.g:6631:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getSequenceTypeRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSequenceTypeAccess().getElementTypePrimitiveTypeCrossReference_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSequenceType"


    // $ANTLR start "entryRuleDiscreteClockType_Impl"
    // InternalExtendedCCSL.g:6652:1: entryRuleDiscreteClockType_Impl returns [EObject current=null] : iv_ruleDiscreteClockType_Impl= ruleDiscreteClockType_Impl EOF ;
    public final EObject entryRuleDiscreteClockType_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDiscreteClockType_Impl = null;


        try {
            // InternalExtendedCCSL.g:6652:63: (iv_ruleDiscreteClockType_Impl= ruleDiscreteClockType_Impl EOF )
            // InternalExtendedCCSL.g:6653:2: iv_ruleDiscreteClockType_Impl= ruleDiscreteClockType_Impl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDiscreteClockType_ImplRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDiscreteClockType_Impl=ruleDiscreteClockType_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDiscreteClockType_Impl; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDiscreteClockType_Impl"


    // $ANTLR start "ruleDiscreteClockType_Impl"
    // InternalExtendedCCSL.g:6659:1: ruleDiscreteClockType_Impl returns [EObject current=null] : ( () otherlv_1= 'DiscreteClockType' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleDiscreteClockType_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6665:2: ( ( () otherlv_1= 'DiscreteClockType' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalExtendedCCSL.g:6666:2: ( () otherlv_1= 'DiscreteClockType' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalExtendedCCSL.g:6666:2: ( () otherlv_1= 'DiscreteClockType' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalExtendedCCSL.g:6667:3: () otherlv_1= 'DiscreteClockType' ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalExtendedCCSL.g:6667:3: ()
            // InternalExtendedCCSL.g:6668:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDiscreteClockType_ImplAccess().getDiscreteClockTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,99,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getDiscreteClockType_ImplAccess().getDiscreteClockTypeKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6681:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6682:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6682:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:6683:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getDiscreteClockType_ImplAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDiscreteClockType_ImplRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDiscreteClockType_Impl"


    // $ANTLR start "entryRuleDenseClockType"
    // InternalExtendedCCSL.g:6703:1: entryRuleDenseClockType returns [EObject current=null] : iv_ruleDenseClockType= ruleDenseClockType EOF ;
    public final EObject entryRuleDenseClockType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDenseClockType = null;


        try {
            // InternalExtendedCCSL.g:6703:55: (iv_ruleDenseClockType= ruleDenseClockType EOF )
            // InternalExtendedCCSL.g:6704:2: iv_ruleDenseClockType= ruleDenseClockType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDenseClockTypeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDenseClockType=ruleDenseClockType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDenseClockType; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDenseClockType"


    // $ANTLR start "ruleDenseClockType"
    // InternalExtendedCCSL.g:6710:1: ruleDenseClockType returns [EObject current=null] : ( () otherlv_1= 'DenseClockType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'baseUnit' ( (lv_baseUnit_5_0= ruleString0 ) ) )? (otherlv_6= 'physicalMagnitude' ( (lv_physicalMagnitude_7_0= ruleString0 ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleDenseClockType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_baseUnit_5_0 = null;

        AntlrDatatypeRuleToken lv_physicalMagnitude_7_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:6716:2: ( ( () otherlv_1= 'DenseClockType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'baseUnit' ( (lv_baseUnit_5_0= ruleString0 ) ) )? (otherlv_6= 'physicalMagnitude' ( (lv_physicalMagnitude_7_0= ruleString0 ) ) )? otherlv_8= '}' ) )
            // InternalExtendedCCSL.g:6717:2: ( () otherlv_1= 'DenseClockType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'baseUnit' ( (lv_baseUnit_5_0= ruleString0 ) ) )? (otherlv_6= 'physicalMagnitude' ( (lv_physicalMagnitude_7_0= ruleString0 ) ) )? otherlv_8= '}' )
            {
            // InternalExtendedCCSL.g:6717:2: ( () otherlv_1= 'DenseClockType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'baseUnit' ( (lv_baseUnit_5_0= ruleString0 ) ) )? (otherlv_6= 'physicalMagnitude' ( (lv_physicalMagnitude_7_0= ruleString0 ) ) )? otherlv_8= '}' )
            // InternalExtendedCCSL.g:6718:3: () otherlv_1= 'DenseClockType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'baseUnit' ( (lv_baseUnit_5_0= ruleString0 ) ) )? (otherlv_6= 'physicalMagnitude' ( (lv_physicalMagnitude_7_0= ruleString0 ) ) )? otherlv_8= '}'
            {
            // InternalExtendedCCSL.g:6718:3: ()
            // InternalExtendedCCSL.g:6719:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDenseClockTypeAccess().getDenseClockTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,100,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getDenseClockTypeAccess().getDenseClockTypeKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6732:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalExtendedCCSL.g:6733:4: (lv_name_2_0= RULE_ID )
            {
            // InternalExtendedCCSL.g:6733:4: (lv_name_2_0= RULE_ID )
            // InternalExtendedCCSL.g:6734:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getDenseClockTypeAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDenseClockTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,13,FollowSets000.FOLLOW_74); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getDenseClockTypeAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:6754:3: (otherlv_4= 'baseUnit' ( (lv_baseUnit_5_0= ruleString0 ) ) )?
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0==101) ) {
                alt77=1;
            }
            switch (alt77) {
                case 1 :
                    // InternalExtendedCCSL.g:6755:4: otherlv_4= 'baseUnit' ( (lv_baseUnit_5_0= ruleString0 ) )
                    {
                    otherlv_4=(Token)match(input,101,FollowSets000.FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getDenseClockTypeAccess().getBaseUnitKeyword_4_0());
                      			
                    }
                    // InternalExtendedCCSL.g:6759:4: ( (lv_baseUnit_5_0= ruleString0 ) )
                    // InternalExtendedCCSL.g:6760:5: (lv_baseUnit_5_0= ruleString0 )
                    {
                    // InternalExtendedCCSL.g:6760:5: (lv_baseUnit_5_0= ruleString0 )
                    // InternalExtendedCCSL.g:6761:6: lv_baseUnit_5_0= ruleString0
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getDenseClockTypeAccess().getBaseUnitString0ParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_75);
                    lv_baseUnit_5_0=ruleString0();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getDenseClockTypeRule());
                      						}
                      						set(
                      							current,
                      							"baseUnit",
                      							lv_baseUnit_5_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.String0");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalExtendedCCSL.g:6779:3: (otherlv_6= 'physicalMagnitude' ( (lv_physicalMagnitude_7_0= ruleString0 ) ) )?
            int alt78=2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0==102) ) {
                alt78=1;
            }
            switch (alt78) {
                case 1 :
                    // InternalExtendedCCSL.g:6780:4: otherlv_6= 'physicalMagnitude' ( (lv_physicalMagnitude_7_0= ruleString0 ) )
                    {
                    otherlv_6=(Token)match(input,102,FollowSets000.FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getDenseClockTypeAccess().getPhysicalMagnitudeKeyword_5_0());
                      			
                    }
                    // InternalExtendedCCSL.g:6784:4: ( (lv_physicalMagnitude_7_0= ruleString0 ) )
                    // InternalExtendedCCSL.g:6785:5: (lv_physicalMagnitude_7_0= ruleString0 )
                    {
                    // InternalExtendedCCSL.g:6785:5: (lv_physicalMagnitude_7_0= ruleString0 )
                    // InternalExtendedCCSL.g:6786:6: lv_physicalMagnitude_7_0= ruleString0
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getDenseClockTypeAccess().getPhysicalMagnitudeString0ParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_51);
                    lv_physicalMagnitude_7_0=ruleString0();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getDenseClockTypeRule());
                      						}
                      						set(
                      							current,
                      							"physicalMagnitude",
                      							lv_physicalMagnitude_7_0,
                      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.String0");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getDenseClockTypeAccess().getRightCurlyBracketKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDenseClockType"


    // $ANTLR start "entryRuleEnumerationType"
    // InternalExtendedCCSL.g:6812:1: entryRuleEnumerationType returns [EObject current=null] : iv_ruleEnumerationType= ruleEnumerationType EOF ;
    public final EObject entryRuleEnumerationType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumerationType = null;


        try {
            // InternalExtendedCCSL.g:6812:56: (iv_ruleEnumerationType= ruleEnumerationType EOF )
            // InternalExtendedCCSL.g:6813:2: iv_ruleEnumerationType= ruleEnumerationType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumerationTypeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumerationType=ruleEnumerationType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumerationType; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumerationType"


    // $ANTLR start "ruleEnumerationType"
    // InternalExtendedCCSL.g:6819:1: ruleEnumerationType returns [EObject current=null] : (otherlv_0= 'EnumerationType' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'enumLiteral' otherlv_5= '{' ( (lv_enumLiteral_6_0= ruleString0 ) ) (otherlv_7= ',' ( (lv_enumLiteral_8_0= ruleString0 ) ) )* otherlv_9= '}' otherlv_10= '}' ) ;
    public final EObject ruleEnumerationType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_enumLiteral_6_0 = null;

        AntlrDatatypeRuleToken lv_enumLiteral_8_0 = null;



        	enterRule();

        try {
            // InternalExtendedCCSL.g:6825:2: ( (otherlv_0= 'EnumerationType' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'enumLiteral' otherlv_5= '{' ( (lv_enumLiteral_6_0= ruleString0 ) ) (otherlv_7= ',' ( (lv_enumLiteral_8_0= ruleString0 ) ) )* otherlv_9= '}' otherlv_10= '}' ) )
            // InternalExtendedCCSL.g:6826:2: (otherlv_0= 'EnumerationType' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'enumLiteral' otherlv_5= '{' ( (lv_enumLiteral_6_0= ruleString0 ) ) (otherlv_7= ',' ( (lv_enumLiteral_8_0= ruleString0 ) ) )* otherlv_9= '}' otherlv_10= '}' )
            {
            // InternalExtendedCCSL.g:6826:2: (otherlv_0= 'EnumerationType' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'enumLiteral' otherlv_5= '{' ( (lv_enumLiteral_6_0= ruleString0 ) ) (otherlv_7= ',' ( (lv_enumLiteral_8_0= ruleString0 ) ) )* otherlv_9= '}' otherlv_10= '}' )
            // InternalExtendedCCSL.g:6827:3: otherlv_0= 'EnumerationType' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'enumLiteral' otherlv_5= '{' ( (lv_enumLiteral_6_0= ruleString0 ) ) (otherlv_7= ',' ( (lv_enumLiteral_8_0= ruleString0 ) ) )* otherlv_9= '}' otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,103,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getEnumerationTypeAccess().getEnumerationTypeKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_76); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getEnumerationTypeAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6835:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt79=2;
            int LA79_0 = input.LA(1);

            if ( (LA79_0==49) ) {
                alt79=1;
            }
            switch (alt79) {
                case 1 :
                    // InternalExtendedCCSL.g:6836:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getEnumerationTypeAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:6840:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:6841:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:6841:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:6842:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_77); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getEnumerationTypeAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getEnumerationTypeRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,104,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getEnumerationTypeAccess().getEnumLiteralKeyword_3());
              		
            }
            otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getEnumerationTypeAccess().getLeftCurlyBracketKeyword_4());
              		
            }
            // InternalExtendedCCSL.g:6867:3: ( (lv_enumLiteral_6_0= ruleString0 ) )
            // InternalExtendedCCSL.g:6868:4: (lv_enumLiteral_6_0= ruleString0 )
            {
            // InternalExtendedCCSL.g:6868:4: (lv_enumLiteral_6_0= ruleString0 )
            // InternalExtendedCCSL.g:6869:5: lv_enumLiteral_6_0= ruleString0
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getEnumerationTypeAccess().getEnumLiteralString0ParserRuleCall_5_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_12);
            lv_enumLiteral_6_0=ruleString0();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getEnumerationTypeRule());
              					}
              					add(
              						current,
              						"enumLiteral",
              						lv_enumLiteral_6_0,
              						"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.String0");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalExtendedCCSL.g:6886:3: (otherlv_7= ',' ( (lv_enumLiteral_8_0= ruleString0 ) ) )*
            loop80:
            do {
                int alt80=2;
                int LA80_0 = input.LA(1);

                if ( (LA80_0==18) ) {
                    alt80=1;
                }


                switch (alt80) {
            	case 1 :
            	    // InternalExtendedCCSL.g:6887:4: otherlv_7= ',' ( (lv_enumLiteral_8_0= ruleString0 ) )
            	    {
            	    otherlv_7=(Token)match(input,18,FollowSets000.FOLLOW_30); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_7, grammarAccess.getEnumerationTypeAccess().getCommaKeyword_6_0());
            	      			
            	    }
            	    // InternalExtendedCCSL.g:6891:4: ( (lv_enumLiteral_8_0= ruleString0 ) )
            	    // InternalExtendedCCSL.g:6892:5: (lv_enumLiteral_8_0= ruleString0 )
            	    {
            	    // InternalExtendedCCSL.g:6892:5: (lv_enumLiteral_8_0= ruleString0 )
            	    // InternalExtendedCCSL.g:6893:6: lv_enumLiteral_8_0= ruleString0
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getEnumerationTypeAccess().getEnumLiteralString0ParserRuleCall_6_1_0());
            	      					
            	    }
            	    pushFollow(FollowSets000.FOLLOW_12);
            	    lv_enumLiteral_8_0=ruleString0();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getEnumerationTypeRule());
            	      						}
            	      						add(
            	      							current,
            	      							"enumLiteral",
            	      							lv_enumLiteral_8_0,
            	      							"fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSL.String0");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop80;
                }
            } while (true);

            otherlv_9=(Token)match(input,15,FollowSets000.FOLLOW_51); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_9, grammarAccess.getEnumerationTypeAccess().getRightCurlyBracketKeyword_7());
              		
            }
            otherlv_10=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_10, grammarAccess.getEnumerationTypeAccess().getRightCurlyBracketKeyword_8());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumerationType"


    // $ANTLR start "entryRuleField"
    // InternalExtendedCCSL.g:6923:1: entryRuleField returns [EObject current=null] : iv_ruleField= ruleField EOF ;
    public final EObject entryRuleField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleField = null;


        try {
            // InternalExtendedCCSL.g:6923:46: (iv_ruleField= ruleField EOF )
            // InternalExtendedCCSL.g:6924:2: iv_ruleField= ruleField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFieldRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleField=ruleField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleField; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleField"


    // $ANTLR start "ruleField"
    // InternalExtendedCCSL.g:6930:1: ruleField returns [EObject current=null] : (otherlv_0= 'Field' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'type' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleField() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:6936:2: ( (otherlv_0= 'Field' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'type' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalExtendedCCSL.g:6937:2: (otherlv_0= 'Field' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'type' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalExtendedCCSL.g:6937:2: (otherlv_0= 'Field' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'type' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalExtendedCCSL.g:6938:3: otherlv_0= 'Field' otherlv_1= '{' (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )? otherlv_4= 'type' ( ( ruleEString ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,105,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getFieldAccess().getFieldKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_78); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getFieldAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalExtendedCCSL.g:6946:3: (otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) ) )?
            int alt81=2;
            int LA81_0 = input.LA(1);

            if ( (LA81_0==49) ) {
                alt81=1;
            }
            switch (alt81) {
                case 1 :
                    // InternalExtendedCCSL.g:6947:4: otherlv_2= 'name' ( (lv_name_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getFieldAccess().getNameKeyword_2_0());
                      			
                    }
                    // InternalExtendedCCSL.g:6951:4: ( (lv_name_3_0= RULE_ID ) )
                    // InternalExtendedCCSL.g:6952:5: (lv_name_3_0= RULE_ID )
                    {
                    // InternalExtendedCCSL.g:6952:5: (lv_name_3_0= RULE_ID )
                    // InternalExtendedCCSL.g:6953:6: lv_name_3_0= RULE_ID
                    {
                    lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_79); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_3_0, grammarAccess.getFieldAccess().getNameIDTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getFieldRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_3_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,106,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getFieldAccess().getTypeKeyword_3());
              		
            }
            // InternalExtendedCCSL.g:6974:3: ( ( ruleEString ) )
            // InternalExtendedCCSL.g:6975:4: ( ruleEString )
            {
            // InternalExtendedCCSL.g:6975:4: ( ruleEString )
            // InternalExtendedCCSL.g:6976:5: ruleEString
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFieldRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFieldAccess().getTypeTypeCrossReference_4_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_51);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getFieldAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleField"


    // $ANTLR start "entryRuleEString"
    // InternalExtendedCCSL.g:7001:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalExtendedCCSL.g:7001:47: (iv_ruleEString= ruleEString EOF )
            // InternalExtendedCCSL.g:7002:2: iv_ruleEString= ruleEString EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEStringRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEString.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalExtendedCCSL.g:7008:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:7014:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalExtendedCCSL.g:7015:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalExtendedCCSL.g:7015:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt82=2;
            int LA82_0 = input.LA(1);

            if ( (LA82_0==RULE_STRING) ) {
                alt82=1;
            }
            else if ( (LA82_0==RULE_ID) ) {
                alt82=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 82, 0, input);

                throw nvae;
            }
            switch (alt82) {
                case 1 :
                    // InternalExtendedCCSL.g:7016:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_STRING_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:7024:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ID_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "ruleEventKind"
    // InternalExtendedCCSL.g:7035:1: ruleEventKind returns [Enumerator current=null] : ( (enumLiteral_0= 'start' ) | (enumLiteral_1= 'finish' ) | (enumLiteral_2= 'stop' ) | (enumLiteral_3= 'consume' ) | (enumLiteral_4= 'produce' ) | (enumLiteral_5= 'receive' ) | (enumLiteral_6= 'send' ) | (enumLiteral_7= 'any' ) | (enumLiteral_8= 'all' ) | (enumLiteral_9= 'undefined' ) ) ;
    public final Enumerator ruleEventKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;


        	enterRule();

        try {
            // InternalExtendedCCSL.g:7041:2: ( ( (enumLiteral_0= 'start' ) | (enumLiteral_1= 'finish' ) | (enumLiteral_2= 'stop' ) | (enumLiteral_3= 'consume' ) | (enumLiteral_4= 'produce' ) | (enumLiteral_5= 'receive' ) | (enumLiteral_6= 'send' ) | (enumLiteral_7= 'any' ) | (enumLiteral_8= 'all' ) | (enumLiteral_9= 'undefined' ) ) )
            // InternalExtendedCCSL.g:7042:2: ( (enumLiteral_0= 'start' ) | (enumLiteral_1= 'finish' ) | (enumLiteral_2= 'stop' ) | (enumLiteral_3= 'consume' ) | (enumLiteral_4= 'produce' ) | (enumLiteral_5= 'receive' ) | (enumLiteral_6= 'send' ) | (enumLiteral_7= 'any' ) | (enumLiteral_8= 'all' ) | (enumLiteral_9= 'undefined' ) )
            {
            // InternalExtendedCCSL.g:7042:2: ( (enumLiteral_0= 'start' ) | (enumLiteral_1= 'finish' ) | (enumLiteral_2= 'stop' ) | (enumLiteral_3= 'consume' ) | (enumLiteral_4= 'produce' ) | (enumLiteral_5= 'receive' ) | (enumLiteral_6= 'send' ) | (enumLiteral_7= 'any' ) | (enumLiteral_8= 'all' ) | (enumLiteral_9= 'undefined' ) )
            int alt83=10;
            switch ( input.LA(1) ) {
            case 107:
                {
                alt83=1;
                }
                break;
            case 108:
                {
                alt83=2;
                }
                break;
            case 109:
                {
                alt83=3;
                }
                break;
            case 110:
                {
                alt83=4;
                }
                break;
            case 111:
                {
                alt83=5;
                }
                break;
            case 112:
                {
                alt83=6;
                }
                break;
            case 113:
                {
                alt83=7;
                }
                break;
            case 114:
                {
                alt83=8;
                }
                break;
            case 115:
                {
                alt83=9;
                }
                break;
            case 116:
                {
                alt83=10;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 83, 0, input);

                throw nvae;
            }

            switch (alt83) {
                case 1 :
                    // InternalExtendedCCSL.g:7043:3: (enumLiteral_0= 'start' )
                    {
                    // InternalExtendedCCSL.g:7043:3: (enumLiteral_0= 'start' )
                    // InternalExtendedCCSL.g:7044:4: enumLiteral_0= 'start'
                    {
                    enumLiteral_0=(Token)match(input,107,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getStartEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getEventKindAccess().getStartEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalExtendedCCSL.g:7051:3: (enumLiteral_1= 'finish' )
                    {
                    // InternalExtendedCCSL.g:7051:3: (enumLiteral_1= 'finish' )
                    // InternalExtendedCCSL.g:7052:4: enumLiteral_1= 'finish'
                    {
                    enumLiteral_1=(Token)match(input,108,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getFinishEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getEventKindAccess().getFinishEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalExtendedCCSL.g:7059:3: (enumLiteral_2= 'stop' )
                    {
                    // InternalExtendedCCSL.g:7059:3: (enumLiteral_2= 'stop' )
                    // InternalExtendedCCSL.g:7060:4: enumLiteral_2= 'stop'
                    {
                    enumLiteral_2=(Token)match(input,109,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getFinishEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getEventKindAccess().getFinishEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalExtendedCCSL.g:7067:3: (enumLiteral_3= 'consume' )
                    {
                    // InternalExtendedCCSL.g:7067:3: (enumLiteral_3= 'consume' )
                    // InternalExtendedCCSL.g:7068:4: enumLiteral_3= 'consume'
                    {
                    enumLiteral_3=(Token)match(input,110,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getConsumeEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_3, grammarAccess.getEventKindAccess().getConsumeEnumLiteralDeclaration_3());
                      			
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalExtendedCCSL.g:7075:3: (enumLiteral_4= 'produce' )
                    {
                    // InternalExtendedCCSL.g:7075:3: (enumLiteral_4= 'produce' )
                    // InternalExtendedCCSL.g:7076:4: enumLiteral_4= 'produce'
                    {
                    enumLiteral_4=(Token)match(input,111,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getProduceEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_4, grammarAccess.getEventKindAccess().getProduceEnumLiteralDeclaration_4());
                      			
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalExtendedCCSL.g:7083:3: (enumLiteral_5= 'receive' )
                    {
                    // InternalExtendedCCSL.g:7083:3: (enumLiteral_5= 'receive' )
                    // InternalExtendedCCSL.g:7084:4: enumLiteral_5= 'receive'
                    {
                    enumLiteral_5=(Token)match(input,112,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getReceiveEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_5, grammarAccess.getEventKindAccess().getReceiveEnumLiteralDeclaration_5());
                      			
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalExtendedCCSL.g:7091:3: (enumLiteral_6= 'send' )
                    {
                    // InternalExtendedCCSL.g:7091:3: (enumLiteral_6= 'send' )
                    // InternalExtendedCCSL.g:7092:4: enumLiteral_6= 'send'
                    {
                    enumLiteral_6=(Token)match(input,113,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getSendEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_6, grammarAccess.getEventKindAccess().getSendEnumLiteralDeclaration_6());
                      			
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalExtendedCCSL.g:7099:3: (enumLiteral_7= 'any' )
                    {
                    // InternalExtendedCCSL.g:7099:3: (enumLiteral_7= 'any' )
                    // InternalExtendedCCSL.g:7100:4: enumLiteral_7= 'any'
                    {
                    enumLiteral_7=(Token)match(input,114,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getUndefinedEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_7, grammarAccess.getEventKindAccess().getUndefinedEnumLiteralDeclaration_7());
                      			
                    }

                    }


                    }
                    break;
                case 9 :
                    // InternalExtendedCCSL.g:7107:3: (enumLiteral_8= 'all' )
                    {
                    // InternalExtendedCCSL.g:7107:3: (enumLiteral_8= 'all' )
                    // InternalExtendedCCSL.g:7108:4: enumLiteral_8= 'all'
                    {
                    enumLiteral_8=(Token)match(input,115,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getUndefinedEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_8, grammarAccess.getEventKindAccess().getUndefinedEnumLiteralDeclaration_8());
                      			
                    }

                    }


                    }
                    break;
                case 10 :
                    // InternalExtendedCCSL.g:7115:3: (enumLiteral_9= 'undefined' )
                    {
                    // InternalExtendedCCSL.g:7115:3: (enumLiteral_9= 'undefined' )
                    // InternalExtendedCCSL.g:7116:4: enumLiteral_9= 'undefined'
                    {
                    enumLiteral_9=(Token)match(input,116,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getEventKindAccess().getUndefinedEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_9, grammarAccess.getEventKindAccess().getUndefinedEnumLiteralDeclaration_9());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventKind"

    // $ANTLR start synpred3_InternalExtendedCCSL
    public final void synpred3_InternalExtendedCCSL_fragment() throws RecognitionException {   
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_imports_6_0 = null;


        // InternalExtendedCCSL.g:158:3: ( ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) ) )
        // InternalExtendedCCSL.g:158:3: ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) )
        {
        // InternalExtendedCCSL.g:158:3: ({...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) ) )
        // InternalExtendedCCSL.g:159:4: {...}? => ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 0) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred3_InternalExtendedCCSL", "getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 0)");
        }
        // InternalExtendedCCSL.g:159:120: ( ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) ) )
        // InternalExtendedCCSL.g:160:5: ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 0);
        // InternalExtendedCCSL.g:163:8: ({...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? ) )
        // InternalExtendedCCSL.g:163:9: {...}? => (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred3_InternalExtendedCCSL", "true");
        }
        // InternalExtendedCCSL.g:163:18: (otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )? )
        // InternalExtendedCCSL.g:163:19: otherlv_1= 'ClockConstraintSystem' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )?
        {
        otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_3); if (state.failed) return ;
        // InternalExtendedCCSL.g:167:8: ( (lv_name_2_0= RULE_ID ) )
        // InternalExtendedCCSL.g:168:9: (lv_name_2_0= RULE_ID )
        {
        // InternalExtendedCCSL.g:168:9: (lv_name_2_0= RULE_ID )
        // InternalExtendedCCSL.g:169:10: lv_name_2_0= RULE_ID
        {
        lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return ;

        }


        }

        otherlv_3=(Token)match(input,13,FollowSets000.FOLLOW_80); if (state.failed) return ;
        // InternalExtendedCCSL.g:189:8: (otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+ )?
        int alt86=2;
        int LA86_0 = input.LA(1);

        if ( (LA86_0==14) ) {
            alt86=1;
        }
        switch (alt86) {
            case 1 :
                // InternalExtendedCCSL.g:190:9: otherlv_4= 'imports' otherlv_5= '{' ( (lv_imports_6_0= ruleImport ) )+
                {
                otherlv_4=(Token)match(input,14,FollowSets000.FOLLOW_4); if (state.failed) return ;
                otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_6); if (state.failed) return ;
                // InternalExtendedCCSL.g:198:9: ( (lv_imports_6_0= ruleImport ) )+
                int cnt85=0;
                loop85:
                do {
                    int alt85=2;
                    int LA85_0 = input.LA(1);

                    if ( (LA85_0==34) ) {
                        alt85=1;
                    }


                    switch (alt85) {
                	case 1 :
                	    // InternalExtendedCCSL.g:199:10: (lv_imports_6_0= ruleImport )
                	    {
                	    // InternalExtendedCCSL.g:199:10: (lv_imports_6_0= ruleImport )
                	    // InternalExtendedCCSL.g:200:11: lv_imports_6_0= ruleImport
                	    {
                	    if ( state.backtracking==0 ) {

                	      											newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getImportsImportParserRuleCall_0_3_2_0());
                	      										
                	    }
                	    pushFollow(FollowSets000.FOLLOW_81);
                	    lv_imports_6_0=ruleImport();

                	    state._fsp--;
                	    if (state.failed) return ;

                	    }


                	    }
                	    break;

                	default :
                	    if ( cnt85 >= 1 ) break loop85;
                	    if (state.backtracking>0) {state.failed=true; return ;}
                            EarlyExitException eee =
                                new EarlyExitException(85, input);
                            throw eee;
                    }
                    cnt85++;
                } while (true);


                }
                break;

        }


        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred3_InternalExtendedCCSL

    // $ANTLR start synpred7_InternalExtendedCCSL
    public final void synpred7_InternalExtendedCCSL_fragment() throws RecognitionException {   
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        EObject lv_subBlock_10_0 = null;

        EObject lv_dataTypes_13_0 = null;

        EObject lv_dataTypes_15_0 = null;


        // InternalExtendedCCSL.g:224:3: ( ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) ) )
        // InternalExtendedCCSL.g:224:3: ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) )
        {
        // InternalExtendedCCSL.g:224:3: ({...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) ) )
        // InternalExtendedCCSL.g:225:4: {...}? => ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 1) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred7_InternalExtendedCCSL", "getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 1)");
        }
        // InternalExtendedCCSL.g:225:120: ( ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) ) )
        // InternalExtendedCCSL.g:226:5: ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 1);
        // InternalExtendedCCSL.g:229:8: ({...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? ) )
        // InternalExtendedCCSL.g:229:9: {...}? => (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred7_InternalExtendedCCSL", "true");
        }
        // InternalExtendedCCSL.g:229:18: (otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )? )
        // InternalExtendedCCSL.g:229:19: otherlv_7= '}' otherlv_8= 'entryBlock' ( ( ruleEString ) ) ( (lv_subBlock_10_0= ruleBlock ) )* (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )?
        {
        otherlv_7=(Token)match(input,15,FollowSets000.FOLLOW_8); if (state.failed) return ;
        otherlv_8=(Token)match(input,16,FollowSets000.FOLLOW_9); if (state.failed) return ;
        // InternalExtendedCCSL.g:237:8: ( ( ruleEString ) )
        // InternalExtendedCCSL.g:238:9: ( ruleEString )
        {
        // InternalExtendedCCSL.g:238:9: ( ruleEString )
        // InternalExtendedCCSL.g:239:10: ruleEString
        {
        if ( state.backtracking==0 ) {

          										/* */
          									
        }
        pushFollow(FollowSets000.FOLLOW_82);
        ruleEString();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // InternalExtendedCCSL.g:256:8: ( (lv_subBlock_10_0= ruleBlock ) )*
        loop88:
        do {
            int alt88=2;
            int LA88_0 = input.LA(1);

            if ( (LA88_0==23) ) {
                alt88=1;
            }


            switch (alt88) {
        	case 1 :
        	    // InternalExtendedCCSL.g:257:9: (lv_subBlock_10_0= ruleBlock )
        	    {
        	    // InternalExtendedCCSL.g:257:9: (lv_subBlock_10_0= ruleBlock )
        	    // InternalExtendedCCSL.g:258:10: lv_subBlock_10_0= ruleBlock
        	    {
        	    if ( state.backtracking==0 ) {

        	      										newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getSubBlockBlockParserRuleCall_1_3_0());
        	      									
        	    }
        	    pushFollow(FollowSets000.FOLLOW_82);
        	    lv_subBlock_10_0=ruleBlock();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }


        	    }
        	    break;

        	default :
        	    break loop88;
            }
        } while (true);

        // InternalExtendedCCSL.g:275:8: (otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}' )?
        int alt90=2;
        int LA90_0 = input.LA(1);

        if ( (LA90_0==17) ) {
            alt90=1;
        }
        switch (alt90) {
            case 1 :
                // InternalExtendedCCSL.g:276:9: otherlv_11= 'dataTypes' otherlv_12= '{' ( (lv_dataTypes_13_0= ruleType ) ) (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )* otherlv_16= '}'
                {
                otherlv_11=(Token)match(input,17,FollowSets000.FOLLOW_4); if (state.failed) return ;
                otherlv_12=(Token)match(input,13,FollowSets000.FOLLOW_11); if (state.failed) return ;
                // InternalExtendedCCSL.g:284:9: ( (lv_dataTypes_13_0= ruleType ) )
                // InternalExtendedCCSL.g:285:10: (lv_dataTypes_13_0= ruleType )
                {
                // InternalExtendedCCSL.g:285:10: (lv_dataTypes_13_0= ruleType )
                // InternalExtendedCCSL.g:286:11: lv_dataTypes_13_0= ruleType
                {
                if ( state.backtracking==0 ) {

                  											newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getDataTypesTypeParserRuleCall_1_4_2_0());
                  										
                }
                pushFollow(FollowSets000.FOLLOW_12);
                lv_dataTypes_13_0=ruleType();

                state._fsp--;
                if (state.failed) return ;

                }


                }

                // InternalExtendedCCSL.g:303:9: (otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) ) )*
                loop89:
                do {
                    int alt89=2;
                    int LA89_0 = input.LA(1);

                    if ( (LA89_0==18) ) {
                        alt89=1;
                    }


                    switch (alt89) {
                	case 1 :
                	    // InternalExtendedCCSL.g:304:10: otherlv_14= ',' ( (lv_dataTypes_15_0= ruleType ) )
                	    {
                	    otherlv_14=(Token)match(input,18,FollowSets000.FOLLOW_11); if (state.failed) return ;
                	    // InternalExtendedCCSL.g:308:10: ( (lv_dataTypes_15_0= ruleType ) )
                	    // InternalExtendedCCSL.g:309:11: (lv_dataTypes_15_0= ruleType )
                	    {
                	    // InternalExtendedCCSL.g:309:11: (lv_dataTypes_15_0= ruleType )
                	    // InternalExtendedCCSL.g:310:12: lv_dataTypes_15_0= ruleType
                	    {
                	    if ( state.backtracking==0 ) {

                	      												newCompositeNode(grammarAccess.getClockConstraintSystem_ImplAccess().getDataTypesTypeParserRuleCall_1_4_3_1_0());
                	      											
                	    }
                	    pushFollow(FollowSets000.FOLLOW_12);
                	    lv_dataTypes_15_0=ruleType();

                	    state._fsp--;
                	    if (state.failed) return ;

                	    }


                	    }


                	    }
                	    break;

                	default :
                	    break loop89;
                    }
                } while (true);

                otherlv_16=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return ;

                }
                break;

        }


        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred7_InternalExtendedCCSL

    // $ANTLR start synpred8_InternalExtendedCCSL
    public final void synpred8_InternalExtendedCCSL_fragment() throws RecognitionException {   
        Token otherlv_17=null;

        // InternalExtendedCCSL.g:339:3: ( ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) ) )
        // InternalExtendedCCSL.g:339:3: ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) )
        {
        // InternalExtendedCCSL.g:339:3: ({...}? => ( ({...}? => (otherlv_17= '}' ) ) ) )
        // InternalExtendedCCSL.g:340:4: {...}? => ( ({...}? => (otherlv_17= '}' ) ) )
        {
        if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred8_InternalExtendedCCSL", "getUnorderedGroupHelper().canSelect(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2)");
        }
        // InternalExtendedCCSL.g:340:120: ( ({...}? => (otherlv_17= '}' ) ) )
        // InternalExtendedCCSL.g:341:5: ({...}? => (otherlv_17= '}' ) )
        {
        getUnorderedGroupHelper().select(grammarAccess.getClockConstraintSystem_ImplAccess().getUnorderedGroup(), 2);
        // InternalExtendedCCSL.g:344:8: ({...}? => (otherlv_17= '}' ) )
        // InternalExtendedCCSL.g:344:9: {...}? => (otherlv_17= '}' )
        {
        if ( !((true)) ) {
            if (state.backtracking>0) {state.failed=true; return ;}
            throw new FailedPredicateException(input, "synpred8_InternalExtendedCCSL", "true");
        }
        // InternalExtendedCCSL.g:344:18: (otherlv_17= '}' )
        // InternalExtendedCCSL.g:344:19: otherlv_17= '}'
        {
        otherlv_17=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return ;

        }


        }


        }


        }


        }
    }
    // $ANTLR end synpred8_InternalExtendedCCSL

    // $ANTLR start synpred114_InternalExtendedCCSL
    public final void synpred114_InternalExtendedCCSL_fragment() throws RecognitionException {   
        Token otherlv_7=null;
        EObject lv_finitePart_8_0 = null;


        // InternalExtendedCCSL.g:2853:5: (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )
        // InternalExtendedCCSL.g:2853:5: otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) )
        {
        otherlv_7=(Token)match(input,25,FollowSets000.FOLLOW_43); if (state.failed) return ;
        // InternalExtendedCCSL.g:2857:5: ( (lv_finitePart_8_0= rulePrimitiveElement ) )
        // InternalExtendedCCSL.g:2858:6: (lv_finitePart_8_0= rulePrimitiveElement )
        {
        // InternalExtendedCCSL.g:2858:6: (lv_finitePart_8_0= rulePrimitiveElement )
        // InternalExtendedCCSL.g:2859:7: lv_finitePart_8_0= rulePrimitiveElement
        {
        if ( state.backtracking==0 ) {

          							newCompositeNode(grammarAccess.getSequenceElementAccess().getFinitePartPrimitiveElementParserRuleCall_5_1_1_0());
          						
        }
        pushFollow(FollowSets000.FOLLOW_2);
        lv_finitePart_8_0=rulePrimitiveElement();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred114_InternalExtendedCCSL

    // $ANTLR start synpred115_InternalExtendedCCSL
    public final void synpred115_InternalExtendedCCSL_fragment() throws RecognitionException {   
        Token otherlv_9=null;

        // InternalExtendedCCSL.g:2878:5: (otherlv_9= ';' )
        // InternalExtendedCCSL.g:2878:5: otherlv_9= ';'
        {
        otherlv_9=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred115_InternalExtendedCCSL

    // $ANTLR start synpred116_InternalExtendedCCSL
    public final void synpred116_InternalExtendedCCSL_fragment() throws RecognitionException {   
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_finitePart_6_0 = null;

        EObject lv_finitePart_8_0 = null;


        // InternalExtendedCCSL.g:2833:4: ( ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )? )
        // InternalExtendedCCSL.g:2833:4: ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )?
        {
        // InternalExtendedCCSL.g:2833:4: ( (lv_finitePart_6_0= rulePrimitiveElement ) )
        // InternalExtendedCCSL.g:2834:5: (lv_finitePart_6_0= rulePrimitiveElement )
        {
        // InternalExtendedCCSL.g:2834:5: (lv_finitePart_6_0= rulePrimitiveElement )
        // InternalExtendedCCSL.g:2835:6: lv_finitePart_6_0= rulePrimitiveElement
        {
        if ( state.backtracking==0 ) {

          						newCompositeNode(grammarAccess.getSequenceElementAccess().getFinitePartPrimitiveElementParserRuleCall_5_0_0());
          					
        }
        pushFollow(FollowSets000.FOLLOW_83);
        lv_finitePart_6_0=rulePrimitiveElement();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // InternalExtendedCCSL.g:2852:4: (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )*
        loop97:
        do {
            int alt97=2;
            int LA97_0 = input.LA(1);

            if ( (LA97_0==25) ) {
                int LA97_1 = input.LA(2);

                if ( (LA97_1==RULE_ID||LA97_1==RULE_INT||(LA97_1>=40 && LA97_1<=42)) ) {
                    alt97=1;
                }


            }


            switch (alt97) {
        	case 1 :
        	    // InternalExtendedCCSL.g:2853:5: otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) )
        	    {
        	    otherlv_7=(Token)match(input,25,FollowSets000.FOLLOW_43); if (state.failed) return ;
        	    // InternalExtendedCCSL.g:2857:5: ( (lv_finitePart_8_0= rulePrimitiveElement ) )
        	    // InternalExtendedCCSL.g:2858:6: (lv_finitePart_8_0= rulePrimitiveElement )
        	    {
        	    // InternalExtendedCCSL.g:2858:6: (lv_finitePart_8_0= rulePrimitiveElement )
        	    // InternalExtendedCCSL.g:2859:7: lv_finitePart_8_0= rulePrimitiveElement
        	    {
        	    if ( state.backtracking==0 ) {

        	      							newCompositeNode(grammarAccess.getSequenceElementAccess().getFinitePartPrimitiveElementParserRuleCall_5_1_1_0());
        	      						
        	    }
        	    pushFollow(FollowSets000.FOLLOW_83);
        	    lv_finitePart_8_0=rulePrimitiveElement();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }


        	    }


        	    }
        	    break;

        	default :
        	    break loop97;
            }
        } while (true);

        // InternalExtendedCCSL.g:2877:4: (otherlv_9= ';' )?
        int alt98=2;
        int LA98_0 = input.LA(1);

        if ( (LA98_0==25) ) {
            alt98=1;
        }
        switch (alt98) {
            case 1 :
                // InternalExtendedCCSL.g:2878:5: otherlv_9= ';'
                {
                otherlv_9=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return ;

                }
                break;

        }


        }
    }
    // $ANTLR end synpred116_InternalExtendedCCSL

    // Delegated rules

    public final boolean synpred114_InternalExtendedCCSL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred114_InternalExtendedCCSL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_InternalExtendedCCSL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_InternalExtendedCCSL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred115_InternalExtendedCCSL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred115_InternalExtendedCCSL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred7_InternalExtendedCCSL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred7_InternalExtendedCCSL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred3_InternalExtendedCCSL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_InternalExtendedCCSL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred116_InternalExtendedCCSL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred116_InternalExtendedCCSL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA39 dfa39 = new DFA39(this);
    protected DFA37 dfa37 = new DFA37(this);
    static final String dfa_1s = "\33\uffff";
    static final String dfa_2s = "\1\6\32\uffff";
    static final String dfa_3s = "\3\4\1\41\1\0\1\4\1\uffff\2\41\1\6\1\uffff\1\41\2\4\1\6\1\0\1\4\1\7\1\5\1\0\2\41\1\0\2\41\2\0";
    static final String dfa_4s = "\1\133\2\4\1\41\1\0\1\4\1\uffff\1\41\1\45\1\6\1\uffff\1\45\1\50\1\5\1\6\1\0\1\5\1\7\1\5\1\0\2\41\1\0\2\41\2\0";
    static final String dfa_5s = "\6\uffff\1\2\3\uffff\1\1\20\uffff";
    static final String dfa_6s = "\4\uffff\1\2\12\uffff\1\1\3\uffff\1\4\2\uffff\1\3\2\uffff\1\5\1\0}>";
    static final String[] dfa_7s = {
            "\1\3\1\uffff\1\4\10\uffff\1\6\3\uffff\1\6\3\uffff\1\6\1\uffff\3\6\2\uffff\1\6\1\uffff\1\6\3\uffff\1\6\3\uffff\1\1\1\2\1\5\1\6\4\uffff\1\6\2\uffff\1\6\1\uffff\2\6\2\uffff\2\6\2\uffff\27\6\1\uffff\1\6\1\uffff\3\6\1\uffff\1\6",
            "\1\7",
            "\1\10",
            "\1\11",
            "\1\uffff",
            "\1\13",
            "",
            "\1\14",
            "\1\16\3\uffff\1\15",
            "\1\17",
            "",
            "\1\21\3\uffff\1\20",
            "\1\23\43\uffff\1\22",
            "\1\25\1\24",
            "\1\26",
            "\1\uffff",
            "\1\30\1\27",
            "\1\31",
            "\1\32",
            "\1\uffff",
            "\1\16",
            "\1\16",
            "\1\uffff",
            "\1\21",
            "\1\21",
            "\1\uffff",
            "\1\uffff"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA39 extends DFA {

        public DFA39(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 39;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "2832:3: ( ( (lv_finitePart_6_0= rulePrimitiveElement ) ) (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )* (otherlv_9= ';' )? )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA39_26 = input.LA(1);

                         
                        int index39_26 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred116_InternalExtendedCCSL()) ) {s = 10;}

                        else if ( (true) ) {s = 6;}

                         
                        input.seek(index39_26);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA39_15 = input.LA(1);

                         
                        int index39_15 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred116_InternalExtendedCCSL()) ) {s = 10;}

                        else if ( (true) ) {s = 6;}

                         
                        input.seek(index39_15);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA39_4 = input.LA(1);

                         
                        int index39_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred116_InternalExtendedCCSL()) ) {s = 10;}

                        else if ( (true) ) {s = 6;}

                         
                        input.seek(index39_4);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA39_22 = input.LA(1);

                         
                        int index39_22 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred116_InternalExtendedCCSL()) ) {s = 10;}

                        else if ( (true) ) {s = 6;}

                         
                        input.seek(index39_22);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA39_19 = input.LA(1);

                         
                        int index39_19 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred116_InternalExtendedCCSL()) ) {s = 10;}

                        else if ( (true) ) {s = 6;}

                         
                        input.seek(index39_19);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA39_25 = input.LA(1);

                         
                        int index39_25 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred116_InternalExtendedCCSL()) ) {s = 10;}

                        else if ( (true) ) {s = 6;}

                         
                        input.seek(index39_25);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 39, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String dfa_8s = "\34\uffff";
    static final String dfa_9s = "\2\2\32\uffff";
    static final String dfa_10s = "\2\4\1\uffff\2\4\1\41\1\0\1\4\2\41\1\6\1\uffff\1\41\2\4\1\6\1\0\1\4\1\7\1\5\1\0\2\41\1\0\2\41\2\0";
    static final String dfa_11s = "\2\133\1\uffff\2\4\1\41\1\0\1\4\1\41\1\45\1\6\1\uffff\1\45\1\50\1\5\1\6\1\0\1\5\1\7\1\5\1\0\2\41\1\0\2\41\2\0";
    static final String dfa_12s = "\2\uffff\1\2\10\uffff\1\1\20\uffff";
    static final String dfa_13s = "\6\uffff\1\2\11\uffff\1\1\3\uffff\1\3\2\uffff\1\4\2\uffff\1\5\1\0}>";
    static final String[] dfa_14s = {
            "\1\2\1\uffff\1\2\10\uffff\1\2\3\uffff\1\2\3\uffff\1\2\1\uffff\1\1\2\2\2\uffff\1\2\1\uffff\1\2\3\uffff\1\2\3\uffff\4\2\4\uffff\1\2\2\uffff\1\2\1\uffff\2\2\2\uffff\2\2\2\uffff\27\2\1\uffff\1\2\1\uffff\3\2\1\uffff\1\2",
            "\1\5\1\uffff\1\6\10\uffff\1\2\3\uffff\1\2\3\uffff\1\2\1\uffff\3\2\2\uffff\1\2\1\uffff\1\2\3\uffff\1\2\3\uffff\1\3\1\4\1\7\1\2\4\uffff\1\2\2\uffff\1\2\1\uffff\2\2\2\uffff\2\2\2\uffff\27\2\1\uffff\1\2\1\uffff\3\2\1\uffff\1\2",
            "",
            "\1\10",
            "\1\11",
            "\1\12",
            "\1\uffff",
            "\1\14",
            "\1\15",
            "\1\17\3\uffff\1\16",
            "\1\20",
            "",
            "\1\22\3\uffff\1\21",
            "\1\24\43\uffff\1\23",
            "\1\26\1\25",
            "\1\27",
            "\1\uffff",
            "\1\31\1\30",
            "\1\32",
            "\1\33",
            "\1\uffff",
            "\1\17",
            "\1\17",
            "\1\uffff",
            "\1\22",
            "\1\22",
            "\1\uffff",
            "\1\uffff"
    };

    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final char[] dfa_11 = DFA.unpackEncodedStringToUnsignedChars(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[] dfa_13 = DFA.unpackEncodedString(dfa_13s);
    static final short[][] dfa_14 = unpackEncodedStringArray(dfa_14s);

    class DFA37 extends DFA {

        public DFA37(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 37;
            this.eot = dfa_8;
            this.eof = dfa_9;
            this.min = dfa_10;
            this.max = dfa_11;
            this.accept = dfa_12;
            this.special = dfa_13;
            this.transition = dfa_14;
        }
        public String getDescription() {
            return "()* loopback of 2852:4: (otherlv_7= ';' ( (lv_finitePart_8_0= rulePrimitiveElement ) ) )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA37_27 = input.LA(1);

                         
                        int index37_27 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred114_InternalExtendedCCSL()) ) {s = 11;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index37_27);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA37_16 = input.LA(1);

                         
                        int index37_16 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred114_InternalExtendedCCSL()) ) {s = 11;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index37_16);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA37_6 = input.LA(1);

                         
                        int index37_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred114_InternalExtendedCCSL()) ) {s = 11;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index37_6);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA37_20 = input.LA(1);

                         
                        int index37_20 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred114_InternalExtendedCCSL()) ) {s = 11;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index37_20);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA37_23 = input.LA(1);

                         
                        int index37_23 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred114_InternalExtendedCCSL()) ) {s = 11;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index37_23);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA37_26 = input.LA(1);

                         
                        int index37_26 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred114_InternalExtendedCCSL()) ) {s = 11;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index37_26);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 37, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000000D002L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000400009002L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000829002L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000000L,0x0000009FF0000000L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000048000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000009002L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100010L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0xE6690F110D888050L,0x000000000BAFFFFFL});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0xE6690F110C888050L,0x000000000BAFFFFFL});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0xE6690F110E888050L,0x000000000BAFFFFFL});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000008000000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000010000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000020000000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000040000000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000080000030L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080040000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000800000000L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000010000000010L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x000000E000000002L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x000000C000000002L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000008000000002L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000002000000002L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000000000L,0x001FF80000000000L});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000002200000000L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000002200000010L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000070040000052L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000042000002L});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000070000000050L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000040000002L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000082000000L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000200000000000L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000004000000000L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000002000000000L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0006000000000000L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0012000000000000L});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0010000000000000L});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0002000010000000L});
        public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0082000000000000L});
        public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0080000000000000L});
        public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x6648000000000000L,0x0000000001000000L});
        public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0802000000000000L});
        public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0800000000000000L});
        public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x1000000000000000L});
        public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x8020000000000000L,0x000000000082001FL});
        public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0102000000000000L});
        public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x0001000000000000L,0x000000000020FFE0L});
        public static final BitSet FOLLOW_65 = new BitSet(new long[]{0xE6690F110C880050L,0x000000000BAFFFFFL});
        public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0000000040000010L});
        public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
        public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x0002000000000000L,0x0000000000400000L});
        public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
        public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x0002000000000000L,0x0000000004000000L});
        public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0000000000000000L,0x0000000004000000L});
        public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
        public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x0000000000008000L,0x0000006000000000L});
        public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x0000000000008000L,0x0000004000000000L});
        public static final BitSet FOLLOW_76 = new BitSet(new long[]{0x0002000000000000L,0x0000010000000000L});
        public static final BitSet FOLLOW_77 = new BitSet(new long[]{0x0000000000000000L,0x0000010000000000L});
        public static final BitSet FOLLOW_78 = new BitSet(new long[]{0x0002000000000000L,0x0000040000000000L});
        public static final BitSet FOLLOW_79 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
        public static final BitSet FOLLOW_80 = new BitSet(new long[]{0x0000000000004002L});
        public static final BitSet FOLLOW_81 = new BitSet(new long[]{0x0000000400000002L});
        public static final BitSet FOLLOW_82 = new BitSet(new long[]{0x0000000000820002L});
        public static final BitSet FOLLOW_83 = new BitSet(new long[]{0x0000000002000002L});
    }


}