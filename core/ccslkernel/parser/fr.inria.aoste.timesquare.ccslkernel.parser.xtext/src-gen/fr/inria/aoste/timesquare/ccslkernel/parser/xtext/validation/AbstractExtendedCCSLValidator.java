/*n * generated by Xtext 
 by action of Julien Deantoni*/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.validation;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.validation.AbstractDeclarativeValidator;

public abstract class AbstractExtendedCCSLValidator extends AbstractDeclarativeValidator {
	
	@Override
	protected List<EPackage> getEPackages() {
		List<EPackage> result = new ArrayList<EPackage>();
		result.add(EPackage.Registry.INSTANCE.getEPackage("http://fr.inria.aoste.timemodel.ccslmodel"));
		result.add(EPackage.Registry.INSTANCE.getEPackage("http://fr.inria.aoste.timemodel.ccslmodel.clockexpressionandrelation"));
		result.add(EPackage.Registry.INSTANCE.getEPackage("http://fr.inria.aoste.timemodel"));
		result.add(EPackage.Registry.INSTANCE.getEPackage("http://fr.inria.aoste.timemodel.basicTypes"));
		result.add(EPackage.Registry.INSTANCE.getEPackage("http://fr.inria.aoste.timemodel.classicalexpression"));
		return result;
	}
}
