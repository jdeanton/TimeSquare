/*n * generated by Xtext 
 by action of Julien Deantoni*/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.serializer;

import com.google.inject.Inject;
import fr.inria.aoste.timesquare.ccslkernel.parser.xtext.services.ExtendedCCSLGrammarAccess;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;

@SuppressWarnings("all")
public class ExtendedCCSLSyntacticSequencer extends AbstractSyntacticSequencer {

	protected ExtendedCCSLGrammarAccess grammarAccess;
	protected AbstractElementAlias match_Block_Impl_SemicolonKeyword_4_1_q;
	protected AbstractElementAlias match_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_a;
	protected AbstractElementAlias match_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_p;
	protected AbstractElementAlias match_SequenceElement_SemicolonKeyword_5_2_q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (ExtendedCCSLGrammarAccess) access;
		match_Block_Impl_SemicolonKeyword_4_1_q = new TokenAlias(false, true, grammarAccess.getBlock_ImplAccess().getSemicolonKeyword_4_1());
		match_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_a = new TokenAlias(true, true, grammarAccess.getClockConstraintSystem_ImplAccess().getRightCurlyBracketKeyword_2());
		match_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_p = new TokenAlias(true, false, grammarAccess.getClockConstraintSystem_ImplAccess().getRightCurlyBracketKeyword_2());
		match_SequenceElement_SemicolonKeyword_5_2_q = new TokenAlias(false, true, grammarAccess.getSequenceElementAccess().getSemicolonKeyword_5_2());
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if (match_Block_Impl_SemicolonKeyword_4_1_q.equals(syntax))
				emit_Block_Impl_SemicolonKeyword_4_1_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_a.equals(syntax))
				emit_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_p.equals(syntax))
				emit_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if (match_SequenceElement_SemicolonKeyword_5_2_q.equals(syntax))
				emit_SequenceElement_SemicolonKeyword_5_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * <pre>
	 * Ambiguous syntax:
	 *     ';'?
	 *
	 * This ambiguous syntax occurs at:
	 *     blockTransitions+=BlockTransition (ambiguity) '}' (rule end)
	 *     blockTransitions+=BlockTransition (ambiguity) blockTransitions+=BlockTransition
	 *     blockTransitions+=BlockTransition (ambiguity) classicalExpression+=ClassicalExpression0
	 *     blockTransitions+=BlockTransition (ambiguity) elements+=Element
	 *     blockTransitions+=BlockTransition (ambiguity) expressions+=Expression
	 *     blockTransitions+=BlockTransition (ambiguity) relations+=Relation
	 *     blockTransitions+=BlockTransition (ambiguity) subBlock+=Block
	 *     classicalExpression+=ClassicalExpression0 (ambiguity) '}' (rule end)
	 *     classicalExpression+=ClassicalExpression0 (ambiguity) blockTransitions+=BlockTransition
	 *     classicalExpression+=ClassicalExpression0 (ambiguity) classicalExpression+=ClassicalExpression0
	 *     classicalExpression+=ClassicalExpression0 (ambiguity) elements+=Element
	 *     classicalExpression+=ClassicalExpression0 (ambiguity) expressions+=Expression
	 *     classicalExpression+=ClassicalExpression0 (ambiguity) relations+=Relation
	 *     classicalExpression+=ClassicalExpression0 (ambiguity) subBlock+=Block
	 *     elements+=Element (ambiguity) '}' (rule end)
	 *     elements+=Element (ambiguity) blockTransitions+=BlockTransition
	 *     elements+=Element (ambiguity) classicalExpression+=ClassicalExpression0
	 *     elements+=Element (ambiguity) elements+=Element
	 *     elements+=Element (ambiguity) expressions+=Expression
	 *     elements+=Element (ambiguity) relations+=Relation
	 *     elements+=Element (ambiguity) subBlock+=Block
	 *     expressions+=Expression (ambiguity) '}' (rule end)
	 *     expressions+=Expression (ambiguity) blockTransitions+=BlockTransition
	 *     expressions+=Expression (ambiguity) classicalExpression+=ClassicalExpression0
	 *     expressions+=Expression (ambiguity) elements+=Element
	 *     expressions+=Expression (ambiguity) expressions+=Expression
	 *     expressions+=Expression (ambiguity) relations+=Relation
	 *     expressions+=Expression (ambiguity) subBlock+=Block
	 *     relations+=Relation (ambiguity) '}' (rule end)
	 *     relations+=Relation (ambiguity) blockTransitions+=BlockTransition
	 *     relations+=Relation (ambiguity) classicalExpression+=ClassicalExpression0
	 *     relations+=Relation (ambiguity) elements+=Element
	 *     relations+=Relation (ambiguity) expressions+=Expression
	 *     relations+=Relation (ambiguity) relations+=Relation
	 *     relations+=Relation (ambiguity) subBlock+=Block
	 *     subBlock+=Block (ambiguity) '}' (rule end)
	 *     subBlock+=Block (ambiguity) blockTransitions+=BlockTransition
	 *     subBlock+=Block (ambiguity) classicalExpression+=ClassicalExpression0
	 *     subBlock+=Block (ambiguity) elements+=Element
	 *     subBlock+=Block (ambiguity) expressions+=Expression
	 *     subBlock+=Block (ambiguity) relations+=Relation
	 *     subBlock+=Block (ambiguity) subBlock+=Block
	 
	 * </pre>
	 */
	protected void emit_Block_Impl_SemicolonKeyword_4_1_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * <pre>
	 * Ambiguous syntax:
	 *     '}'*
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) 'ClockConstraintSystem' name=ID
	 *     (rule start) (ambiguity) '}' 'entryBlock' superBlock=[Block|EString]
	 *     dataTypes+=Type '}' (ambiguity) 'ClockConstraintSystem' name=ID
	 *     dataTypes+=Type '}' (ambiguity) '}' 'entryBlock' superBlock=[Block|EString]
	 *     dataTypes+=Type '}' (ambiguity) (rule end)
	 *     imports+=Import (ambiguity) 'ClockConstraintSystem' name=ID
	 *     imports+=Import (ambiguity) '}' 'entryBlock' superBlock=[Block|EString]
	 *     imports+=Import (ambiguity) (rule end)
	 *     name=ID '{' (ambiguity) 'ClockConstraintSystem' name=ID
	 *     name=ID '{' (ambiguity) '}' 'entryBlock' superBlock=[Block|EString]
	 *     name=ID '{' (ambiguity) (rule end)
	 *     subBlock+=Block (ambiguity) 'ClockConstraintSystem' name=ID
	 *     subBlock+=Block (ambiguity) '}' 'entryBlock' superBlock=[Block|EString]
	 *     subBlock+=Block (ambiguity) (rule end)
	 *     superBlock=[Block|EString] (ambiguity) 'ClockConstraintSystem' name=ID
	 *     superBlock=[Block|EString] (ambiguity) '}' 'entryBlock' superBlock=[Block|EString]
	 *     superBlock=[Block|EString] (ambiguity) (rule end)
	 
	 * </pre>
	 */
	protected void emit_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * <pre>
	 * Ambiguous syntax:
	 *     '}'+
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) (rule start)
	 
	 * </pre>
	 */
	protected void emit_ClockConstraintSystem_Impl_RightCurlyBracketKeyword_2_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * <pre>
	 * Ambiguous syntax:
	 *     ';'?
	 *
	 * This ambiguous syntax occurs at:
	 *     finitePart+=PrimitiveElement (ambiguity) '(' nonFinitePart+=PrimitiveElement
	 *     finitePart+=PrimitiveElement (ambiguity) (rule end)
	 
	 * </pre>
	 */
	protected void emit_SequenceElement_SemicolonKeyword_5_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
