/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/*
 * generated by Xtext
 */
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.formatting;

import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter;
import org.eclipse.xtext.formatting.impl.FormattingConfig;
import org.eclipse.xtext.util.Pair;

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation/latest/xtext.html#formatting
 * on how and when to use it 
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
public class ExtendedCCSLFormatter extends AbstractDeclarativeFormatter {
	
	@Override
	protected void configureFormatting(FormattingConfig c) {
		c.setAutoLinewrap(120);
		fr.inria.aoste.timesquare.ccslkernel.parser.xtext.services.ExtendedCCSLGrammarAccess f = (fr.inria.aoste.timesquare.ccslkernel.parser.xtext.services.ExtendedCCSLGrammarAccess) getGrammarAccess();
		for(Pair<Keyword, Keyword> pair: f.findKeywordPairs("{", "}")) {
			c.setIndentation(pair.getFirst(), pair.getSecond());
			c.setLinewrap(1).after(pair.getFirst());
			c.setLinewrap(1).before(pair.getSecond());
			c.setLinewrap(1).after(pair.getSecond());
		}
		for(Keyword comma: f.findKeywords(",")) {
			c.setNoLinewrap().before(comma);
			c.setNoSpace().before(comma);
			c.setLinewrap().after(comma);
		}		
		for(Keyword comma: f.findKeywords(";")) {		
			c.setLinewrap().after(comma);
		}
		for(Keyword comma: f.findKeywords("superBlock")) {		
			c.setLinewrap().before(comma);
		}	
		for(Keyword comma: f.findKeywords("subBlock")) {		
			c.setLinewrap().before(comma);
		}	
		for(Pair<Keyword, Keyword> pair: f.findKeywordPairs("(", ")")) {
			//c.setIndentation(pair.getFirst(), pair.getSecond());
			c.setLinewrap(1).before(pair.getFirst());
			c.setLinewrap(1).after(pair.getSecond());
			//c.setLinewrap(1).after(pair.getSecond());
		}
		c.setLinewrap(0, 1, 2).before(f.getSL_COMMENTRule());
		c.setLinewrap(0, 1, 2).before(f.getML_COMMENTRule());
		c.setLinewrap(0, 1, 1).after(f.getML_COMMENTRule());
		c.setLinewrap().after(f.getClockRule());
		c.setLinewrap().after(f.getIntegerElementRule());
		c.setLinewrap().after(f.getRelationRule());
		c.setLinewrap().after(f.getExpressionRule());
		c.setLinewrap().before(f.getBlockRule());
		c.setLinewrap().after(f.getBlockRule());
		
	}
}
