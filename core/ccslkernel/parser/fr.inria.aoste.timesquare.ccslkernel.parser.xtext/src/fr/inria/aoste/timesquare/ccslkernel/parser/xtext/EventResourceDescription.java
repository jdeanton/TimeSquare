/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IReferenceDescription;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.impl.AbstractResourceDescription;

import com.google.common.collect.Lists;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.utils.umlhelpers.UmlHelpers;

/**
 * An implementation of {@link IResourceDescription} that generates entries in the index (and thus the
 * global scope) by prefixing a qualified name of each object by a prefix made of the import alias of the
 * containing resource.
 * <p>
 * This formatting of names is used in the {@link Event} objects linked from {@link Clock} objects.
 * 
 * @author Andreas Graf - Initial contribution and API
 * @author Benoit Ferrero
 * @author Julien Deantoni
 */
public class EventResourceDescription extends AbstractResourceDescription {

	private Resource resource;
	private String prefix = null;
	private URI uri;
	private IQualifiedNameConverter qualifiedNameConverter;

	public EventResourceDescription(Resource resource, String pref, IQualifiedNameConverter qualifiedNameConverter) {
		this.resource = resource;
		this.uri = super.getNormalizedURI(resource);
		this.prefix = pref;
		this.qualifiedNameConverter = qualifiedNameConverter;
	}
		
	public String getPrefix() {
		return prefix;
	}

	/*
	 * This list acts as a cache to avoid repeated computations of the object descriptions for the same
	 * resource.
	 * 
	 * This caching mechanism is crude and does not account for changes to the resource as soon as its
	 * corresponding EventResourceDescription object is created.
	 */
	private List<IEObjectDescription> result = null;
	
	public static final boolean addAliasPrefix = false;
	
	public static final String QUALIFIED_NAME_ATTRIBUTE = "qualifiedName";
	public static final String SIMPLE_NAME_ATTRIBUTE = "name";
	
	/**
	 * Returns all the objects description exported by the resource associated with this object.
	 */
	@Override
	protected List<IEObjectDescription> computeExportedObjects() {
		if (result != null)
			return result;
		Iterator<EObject> contents = resource.getAllContents();
		result = Lists.newArrayList();
		String _prefix;
		if (addAliasPrefix)
			_prefix = ( prefix != null ? prefix + "->" : "");
		else
			_prefix = "";
		
		UmlHelpers umlHelper = new UmlHelpers();
		
		while (contents.hasNext()) {
			EObject eObject = contents.next();
			IEObjectDescription desc = null;
			
			if ( eObject != null) {
				EClass ec = eObject.eClass();
				EStructuralFeature esf = ec.getEStructuralFeature(QUALIFIED_NAME_ATTRIBUTE);
				if (esf == null || ! esf.isDerived() )
					esf = ec.getEStructuralFeature(SIMPLE_NAME_ATTRIBUTE);
				Object objectName = (esf != null ? eObject.eGet(esf) : ec.getName() + "_instanceWithNoName");
				if (objectName != null && objectName instanceof String) {
					String objectKey = _prefix + objectName.toString() ;
					QualifiedName qualifiedName = qualifiedNameConverter.toQualifiedName(objectKey);
					desc = new EObjectDescription(qualifiedName, eObject, null);
					if (desc != null)
						result.add(desc);
				}else{
					objectName="";
				}
				/* Add a description for each operation of the object. Use the type description of the
				 * object (its metaclass) to find these operations.
				 * 
				 * First compute two lists with the EOperation objects in one and the names in the other, in
				 * the same order. The names and add an empty parenthesis pair to each of them for clarity
				 * purpose. 
				 */
				ArrayList<EObject> objectMethods = new ArrayList<EObject>();
				ArrayList<String> objectMethodNames = new ArrayList<String>();
				for(EObject eo : eObject.eClass().eContents()){
					if (eo instanceof EOperation){
						objectMethods.add(eo);
						objectMethodNames.add(((EOperation) eo).getName() + "()");
					}
				}
				/* Then actually create the object descriptions based on the object name plus the method name.
				 */
				for (int i=0 ; i < objectMethodNames.size(); i++) {
					String methodName = objectMethodNames.get(i);
					String methodKey =  _prefix + objectName.toString() + "::" + methodName;
					QualifiedName qualifiedName = qualifiedNameConverter.toQualifiedName(methodKey);
					desc = new EObjectDescription(qualifiedName, objectMethods.get(i), null);
					if (desc != null)
						result.add(desc);
				}
				/*
				 * Compute object descriptions for UML property attributes (mainly ports). 
				 */
				ArrayList<String> additionnalPropName = umlHelper.getPropertyNameToDisplayInXtext(eObject);
				ArrayList<EObject> additionnalProp = umlHelper.getPropertyToDisplayInXtext(eObject);
				for (int i = 0 ; i < additionnalPropName.size(); i++) {
					String propertyName = additionnalPropName.get(i);
					String propertyKey = _prefix + objectName.toString() + "::" + propertyName;
					QualifiedName qualifiedName = qualifiedNameConverter.toQualifiedName(propertyKey);
					desc = new EObjectDescription(qualifiedName, additionnalProp.get(i), null);
					if (desc != null)
						result.add(desc);
				}
			}
		}
		return result;
	}

	public Iterable<QualifiedName> getImportedNames() {
		return Collections.emptyList();
	}

	public Iterable<IReferenceDescription> getReferenceDescriptions() {
		return Collections.emptyList();
	}

	public URI getURI() {
		return uri;
	}

}
