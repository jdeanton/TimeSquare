/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;
import org.eclipse.xtext.util.IAcceptor;

import com.google.inject.Inject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;

/**
 * This class describe how some objects are inserted inside the index and global scope
 * of Xtext.
 * <p>
 * An instance of this class is associated with an instance of {@link DefaultResourceDescription}
 * via injection. This object is finally responsible of building all the {@link EObjectDescription}
 * for a given resource in the {@link DefaultResourceDescription#computeExportedObjects(}} function.
 * <p>
 * Here we rely on the default behavior of the parent class, except that for {@link Clock} instances
 * we also contribute an object description entry based on the fully qualified name. This allows us
 * to establish a cross-reference to a Clock, either by using its simple name, or its qualified name.
 *  
 * @see DefaultResourceDescriptionStrategy
 * @see DefaultResourceDescription
 * @author nchleq
 *
 */
public class ExtendedCCSLResourceDescriptionStrategy extends
		DefaultResourceDescriptionStrategy {

	/**
	 * The following should be an instance of {@link CCSLQualifiedNameConverter} in our runtine
	 * configuration.
	 */
	@Inject
	IQualifiedNameConverter qualifiedNameConverter;
	
	@Override
	public boolean createEObjectDescriptions(EObject eObject,
			IAcceptor<IEObjectDescription> acceptor) {
		if (eObject instanceof Clock) {
			String name = CCSLKernelUtils.getQualifiedName(eObject);
			QualifiedName qualifiedName = qualifiedNameConverter.toQualifiedName(name);
			if (qualifiedName != null) {
				acceptor.accept(EObjectDescription.create(qualifiedName, eObject));
			}
		}
		return super.createEObjectDescriptions(eObject, acceptor);
	}

}
