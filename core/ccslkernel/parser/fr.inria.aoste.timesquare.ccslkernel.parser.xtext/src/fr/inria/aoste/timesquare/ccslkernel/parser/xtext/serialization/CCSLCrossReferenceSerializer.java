/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.serialization;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.parsetree.reconstr.impl.CrossReferenceSerializer;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;
import fr.inria.aoste.timesquare.ccslkernel.parser.xtext.EventResourceDescription;

public  class CCSLCrossReferenceSerializer extends CrossReferenceSerializer
{
	
	@Override
	public String serializeCrossRef(EObject context, CrossReference grammarElement, EObject target, INode node) {
		if (context instanceof Event) {
			EReference ref = GrammarUtil.getReference(grammarElement, context.eClass());
			if (ref == TimeModelPackage.Literals.EVENT__REFERENCED_OBJECT_REFS) {
				String prefix;
				if (EventResourceDescription.addAliasPrefix)
					prefix = CCSLKernelUtils.getImportAlias(context, target) + "->";
				else
					prefix = "";
				/* The following code does the same as the code in the class
				 * fr.inria.aoste.timesquare.ccslkernel.parser.xtext.EventResourceDescription used to build the
				 * descriptions of objects exported from a model imported in a CCSL specification. It works
				 * for UML models, at least. */
				/*
				 * HACK: if the target is an EOperation, this means that the first object in the referenced
				 * objects is the actual target of the call (the EOperation object comes from the class of this
				 * object.) 
				 */
				String operationName = null;
				EObject actualTarget = target;
				if (target instanceof EOperation){
					operationName = ((EOperation)target).getName();
					actualTarget = ((Event) context).getReferencedObjectRefs().get(0);
				}
				EClass targetClass = actualTarget.eClass();
				EStructuralFeature esf = targetClass.getEStructuralFeature(EventResourceDescription.QUALIFIED_NAME_ATTRIBUTE);
				if (esf == null || ! esf.isDerived())
					esf = targetClass.getEStructuralFeature(EventResourceDescription.SIMPLE_NAME_ATTRIBUTE);
				if (esf != null) {
					Object name = actualTarget.eGet(esf);
					if (name instanceof String) {
						if (operationName != null) {
							return "\"" + prefix + name + "::" + operationName + "()\"";
						}
						return "\"" + prefix + name + "\"";
					}
				}
				return "";
			}
		}
		else if (target instanceof Clock && target.eResource() != context.eResource()) {
			String name = CCSLKernelUtils.getQualifiedName(target);
			return "\"" + name + "\"";
		}
		return serializeDefault(context, grammarElement, target, node);
	}
		
	private String serializeDefault(EObject context, CrossReference grammarElement, EObject target, INode node) {
		String s = super.serializeCrossRef(context, grammarElement, target, node);			
		if( s == null) {
			// to fix problem of binding's homonymy (AbstractEntity)  
			if( target instanceof NamedElement)
				return ((NamedElement) target).getName();
		}
		return s;
	}
	
}