/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider;
import org.eclipse.xtext.scoping.impl.ImportUriResolver;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;

/**
 * A resolver for the importURI attributes of objects. It extends
 * {@link ImportUriResolver} by normalizing and resolving import URI so that ou
 * special URIs "ccsl:kernel" and "ccsl:lib" are properly replaced by valid
 * URIs.
 * 
 * This class is bound (through the Guice injector) to the
 * {@link ImportUriResolver} class so that Guice injection in
 * {@link org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider} builds an
 * instance of this class rather than {@link ImportUriResolver}.
 * 
 * @see ImportUriGlobalScopeProvider#getImportUriResolver()
 * @see CCSLImportUriGlobalScopeProvider
 * @see fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ExtendedCCSLRuntimeModule#configure(com.google.inject.Binder)
 * @author Benoit Ferrero
 * @author Nicolas Chleq: code cleanup and simplification, javadoc.
 * 
 */
public class CCSLImportURIResolver extends ImportUriResolver {

	@Override
	public String resolve(EObject object) {
		if (!(object instanceof ImportStatement)) {
			return null;
		}
		String importedURI = ((ImportStatement) object).getImportURI();
		if (! importedURI.startsWith("ccsl:")){
			return super.resolve(object);
		}
		// is almost equivalent to the following: importedURI = super.resolve(object)
		// but allow usage of pathmap.
		String alias = ((ImportStatement) object).getAlias();
		if (importedURI == null || alias == null)
			return null;
		URI contextURI = EcoreUtil2.getNormalizedResourceURI(object);
		URIConverter converter = object.eResource().getResourceSet().getURIConverter();
		URI importUri = URI.createURI(importedURI);
		URI normalizedURI = converter.normalize(importUri);
		URI resolvedURI = normalizedURI;
		if (normalizedURI.isRelative())
			resolvedURI = normalizedURI.resolve(contextURI, false);
		importedURI = resolvedURI.toString();
		return importedURI;
	}

}