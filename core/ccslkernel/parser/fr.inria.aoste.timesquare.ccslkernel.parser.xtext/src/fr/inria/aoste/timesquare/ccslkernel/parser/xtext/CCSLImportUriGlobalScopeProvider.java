/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.linking.impl.DefaultLinkingService;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.scoping.IGlobalScopeProvider;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider;
import org.eclipse.xtext.scoping.impl.ImportUriResolver;
import org.eclipse.xtext.scoping.impl.SelectableBasedScope;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;

/**
 * An implementation of {@link IGlobalScopeProvider} adapted for extendedCCSL resources. Purpose of this
 * scope provider is to contribute to the Xtext index (and thus the global scope) special names for 
 * cross-referenced objects in the {@link Event} objects.
 * These names are built using the importAlias of the {@link ImportStatement} object from which the objects
 * are made available in the index and global scope.
 * 
 * Implementation of this mechanism relies on:
 * <ul>
 * <li>A dedicated implementation of {@link ImportUriResolver} that normalizes and resolves import URIs;
 * <li>The function {@link CCSLKernelUtils#getImportAlias(Resource, Resource)} that returns the alias used in
 * the import statement for a specific resource;
 * <li>This global scope provider that builds dedicated resource descriptions whenever necessary;
 * <li>An dedicated {@link IResourceDescription} implementation ({@link EventResourceDescription}) that
 * contributes special names to the global scope for imported objects.
 * </ul>
 * @see CCSLImportURIResolver
 * @see EventResourceDescription
 * @author Benoit Ferrero
 * @author Nicolas Chleq: code cleanup and javadoc.
 */
public final class CCSLImportUriGlobalScopeProvider extends ImportUriGlobalScopeProvider {
	
	public CCSLImportUriGlobalScopeProvider() {
		super();
	}

	/**
	 * Adds to super.getImportedUris() a filtering step to remove empty strings. Relies on the overriden
	 * function to get the actual URIs.
	 * The URIs returned by {@link ImportUriGlobalScopeProvider#getImportedUris()} are extracted by trying
	 * to resolve "importURI" attributes in the objects (contents) of the resource. In our setup, this relies
	 * on the {@link CCSLImportURIResolver#resolve(org.eclipse.emf.ecore.EObject)} function.
	 * Returned URIs are resolved again the importing resource to get absolute URI, then normalized and
	 * also checkedas valid URIs using the
	 * {@link org.eclipse.xtext.EcoreUtil2#isValidUri(Resource, URI)} function.
	 * @see org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider#getImportedUris(Resource)
	 * @see fr.inria.aoste.timesquare.ccslkernel.parser.xtext.CCSLImportURIResolver.resolve(EObject)
	 * @see org.eclipse.xtext.scoping.impl.ImportUriResolver#resolve(EObject)
	 */
	@Override
	protected LinkedHashSet<URI> getImportedUris(Resource resource) {
		final LinkedHashSet<URI> uniqueImportURIs = super.getImportedUris(resource);		
		Iterator<URI> uriIter = uniqueImportURIs.iterator();
		while (uriIter.hasNext()) {
			URI uri = uriIter.next();
			if ( uri == null || uri.toString().length() == 0 )
				uriIter.remove();
		}
		return uniqueImportURIs;
	}

	/**
	 * Almost the same code as the overriden function 
	 * {@link ImportUriGlobalScopeProvider#getScope(Resource, boolean, EClass, Predicate)}
	 * except that we call our own version of #createLazyResourceScope() with an extra argument that is the
	 * current ressource. This allows us to resolve the imported URIs with respect to the current resource and
	 * using this URIs as a key to find the import alias as managed by the CCSLImportURIResolver object.
	 * This import alias is used as a prefix to generate object names in the Xtext index (and the scope).
	 * @see org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider#getScope(Resource, boolean, EClass, Predicate)
	 */
	@Override
	protected IScope getScope(Resource resource, boolean ignoreCase, EClass type, Predicate<IEObjectDescription> filter) {
		final LinkedHashSet<URI> uniqueImportURIs = getImportedUris(resource);
		IResourceDescriptions descriptions = getResourceDescriptions(resource, uniqueImportURIs);
		List<URI> urisAsList = Lists.newArrayList(uniqueImportURIs);
		Collections.reverse(urisAsList);
		IScope scope = IScope.NULLSCOPE;
		for (URI uri : urisAsList) {
			scope = createLazyResourceScope(resource, scope, uri, descriptions, type, filter, ignoreCase);
		}
		return scope;
	}
	
	/**
	 * Get a reference to an actual {@link IQualifiedNameConverter} used in our runtime configuration.
	 * During the linking phase (class {@link DefaultLinkingService}, the text found in the file is
	 * analyzed by this converter to get a {@link QualifiedName} which is compared to the actual
	 * QualifiedName objects of the {@link IEObjectDescription} found in the global scope (index).
	 * By giving to the {@link EventResourceDescription} a reference to this converter, the qualified
	 * names are built in a consistent way with what the linking mechanism expects.
	 * 
	 *  @see org.eclipse.xtext.linking.impl.DefaultLinkingService#getLinkedObjects(EObject, EReference, INode)
	 */
	@Inject
	IQualifiedNameConverter qualifiedNameConverter;
	
	private HashMap<Resource, EventResourceDescription> resourceDescriptionCache = new HashMap<Resource, EventResourceDescription>();
	
	protected EventResourceDescription getOrCreateResourceDescription(Resource resource, String prefix) {
		EventResourceDescription resourceDescription = resourceDescriptionCache.get(resource);
		if (resourceDescription != null && ! resource.isModified()) {
			boolean samePrefix = true;
			if (prefix == null ) {
				samePrefix = ( resourceDescription.getPrefix() == null );
			}
			else {
				samePrefix = prefix.equals(resourceDescription.getPrefix());
			}			
			if (samePrefix) {
				return resourceDescription;
			}			
		}
		// Case :  (resourceDescription == null || resource.isModified()) or different prefix/alias
		resourceDescription = new EventResourceDescription(resource, prefix, qualifiedNameConverter);
		resource.setModified(false);
		resource.setTrackingModification(true);
		resourceDescriptionCache.put(resource, resourceDescription);
		return resourceDescription;			
	}
	
	protected IScope createLazyResourceScope(Resource importingResource, IScope parent, URI uri,
			IResourceDescriptions descriptions, EClass type,
			Predicate<IEObjectDescription> filter, boolean ignoreCase) {
		IResourceDescription description = null;
		/* descriptions is actually an instance of
		 * {@link org.eclipse.xtext.scoping.impl.LoadOnDemandResourceDescriptions}. The call of
		 * {@link LoadOnDemandResourceDescriptions#getResourceDescription(URI)} may throw an
		 * java.lang.IllegalStateException exception in two conditions :
		 * - an IResourceServiceProvider is not registered for the uri
		 * - an IResourceServiceProvider.Manager is not returned by the IResourceServiceProvider object.
		 * Those conditions exists as soon as no Xtext language has been defined for a given file extension.
		 * For example, .ecore files have an associated Xtext language (OCLInEcore) if the OCL tools are
		 * installed in the runtime Eclipse : in this case, our resource descriptions will not be used.
		 * However, without OCL Tools in the runtime Eclipse, we will use our description system.
		 * In both cases, we fallback on our own code that builds resource description with a name based
		 * on the import alias of the resource  
		 */
		try {
			description = descriptions.getResourceDescription(uri);
		}
		catch (IllegalStateException e) {
			Resource importedResource = EcoreUtil2.getResource(importingResource, uri.toString());
			String importAlias = CCSLKernelUtils.getImportAlias(importingResource, importedResource);
			description = getOrCreateResourceDescription(importedResource, importAlias);
		}
		return SelectableBasedScope.createScope(parent, description, filter, type, ignoreCase);
	}
	
	
}