/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.validation;

import java.util.Collection;

import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.validation.ComposedChecks;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.parser.xtext.CCSLImportURIResolver;
import fr.inria.aoste.timesquare.ccslkernel.xtext.util.BindingValidation;
import fr.inria.aoste.timesquare.ccslkernel.xtext.util.NamedElementValidation;

@ComposedChecks(validators = { BindingValidation.class, NamedElementValidation.class })
public class ExtendedCCSLValidator extends AbstractExtendedCCSLValidator {

	// @Check
	// public void checkGreetingStartsWithCapital(Greeting greeting) {
	// if (!Character.isUpperCase(greeting.getName().charAt(0))) {
	// warning("Name should start with a capital", MyDslPackage.GREETING__NAME);
	// }
	// }

	@Check
	public void checkImportStatement(ImportStatement s) {
		if (s.getImportURI() == null || s.getImportURI().length() == 0) {
			error("Missing Import URI",
					TimeModelPackage.eINSTANCE.getImportStatement_ImportURI());
			return;
		}
		if (s.getAlias() == null || s.getAlias().length() == 0) {
			error("Missing alias name",
					TimeModelPackage.eINSTANCE.getImportStatement_Alias());
			return;
		}
		try {
			String importedURI = (new CCSLImportURIResolver()).resolve(s);
			// null or platform:/ indicates that the call to
			// CCSLImportURIResolver.resolve() fails
			if (importedURI != null && !"platform:/".equals(importedURI)) {
				/*
				 * EcoreUtil2.getResource() wraps a call to
				 * org.eclipse.emf.ecore.resource.ResourceSet.getResource(URI, boolean)
				 * after resolving the URI with respect to the importing resource.
				 */
				Resource resource = EcoreUtil2.getResource(s.eResource(), importedURI);
				if (resource == null) {
					error("Import load failure",
							TimeModelPackage.eINSTANCE.getImportStatement_ImportURI());
					return;
				}
				/* 14 nov 2013:
				 * Now that we allow to import CCSL models from CCSL models, it happens that imported
				 * libraries are seen from several import statements located in different models, and thus
				 * if we validate these libraries here, we trigger an exception :
				 * 		java.lang.IllegalStateException: State is already assigned
				 * from Xtext internals. This generates some irrelevant errors in the Editor.
				 */
//				EcoreUtil.resolveAll(resource);
//				if (resource.getContents().size() == 0) {
//					return;
//				}
//				EObject eo = resource.getContents().get(0);
//				EcoreUtil.resolveAll(eo); // useless ??
//				Diagnostic d = Diagnostician.INSTANCE.validate(eo);
//				int actualerror = d.getChildren().size();
//				if (actualerror != 0) {
//					if (eo instanceof Library)
//						error("Library error " + actualerror,
//								TimeModelPackage.eINSTANCE.getImportStatement_ImportURI());
//					else {
//						for (Diagnostic cd : d.getChildren()) {
//							warning("Imported model has error : " + cd.getMessage(),
//									TimeModelPackage.eINSTANCE.getImportStatement_ImportURI());
//						}
//					}
//				}
			}
		} catch (Throwable e) {
			// e.printStackTrace();
			error("unLoading", TimeModelPackage.eINSTANCE.getImportStatement_ImportURI());
		}
	}

	@Check
	public void checkClock(Clock clock) {
		Collection<Setting> refs = EcoreUtil.UsageCrossReferencer.find(clock, clock
				.eResource().getResourceSet());
		if (refs.size() == 0) {
			warning("Clock " + clock.getName() + " is currently unused.",
			// following arguments make the name of the clock to be underlined
			// in the editor
			// in the "warning" color (yellow by default).
					clock, TimeModelPackage.eINSTANCE.getNamedElement_Name(), 0);
		}
	}

}
