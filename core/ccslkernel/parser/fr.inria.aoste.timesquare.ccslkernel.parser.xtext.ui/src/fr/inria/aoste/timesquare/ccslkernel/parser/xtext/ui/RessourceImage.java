/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ui;

import org.eclipse.swt.graphics.Image;

import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;

public class RessourceImage {
	
	public final static Image relationimage = PluginHelpers.getImage("fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ui", "icon/relation_small.png");
	public final static Image expressionimage=PluginHelpers.getImage("fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ui", "icon/equation_small.png");
	public final static Image clockimage=PluginHelpers.getImage("fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ui", "icon/clock_small.gif");
}
