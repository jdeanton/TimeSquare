/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.parser.xtext.ui.hover;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;

public class ExtendedCCSLHoverProvider extends DefaultEObjectHoverProvider {

	@Override
	protected String getFirstLine(EObject o) {
		String label = getLabel(o);
		String qualifiedName = CCSLKernelUtils.getQualifiedName(o);
		return o.eClass().getName() + ((label != null) ? " <b>"+label+"</b>" : "")
				+ (qualifiedName != null ? " : " + qualifiedName : "") ;
	}
	
}
