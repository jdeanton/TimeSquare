/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.eclipse.xtext.common.services.Ecore2XtextTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter;
import org.eclipse.xtext.nodemodel.INode;



public class CCSLTerminalConverters extends
		Ecore2XtextTerminalConverters {

	private static final class Real0FloatConverter extends
			AbstractNullSafeConverter<Float> {
		private DecimalFormat df;

		public Real0FloatConverter() {
			super();
			df=new DecimalFormat("#####.#############");
			df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance( Locale.ENGLISH));
			df.setDecimalSeparatorAlwaysShown(true);
		}

		@Override
		protected String internalToString(Float value) {
			try
			{						
				String s= df.format(Double.parseDouble(value.toString()));
				System.out.println(value +"=>"+s);
				return s;
			}
			catch (Exception e) {
				//e.printStackTrace();
				return null;
			}
		}

		@Override
		protected Float internalToValue(String string, INode node)	throws ValueConverterException {
			
			return Float.valueOf(string);
		}
	}

	@ValueConverter(rule = "Real0")
	 public IValueConverter<Float> Real0() {
	   return new Real0FloatConverter();
	 }
}