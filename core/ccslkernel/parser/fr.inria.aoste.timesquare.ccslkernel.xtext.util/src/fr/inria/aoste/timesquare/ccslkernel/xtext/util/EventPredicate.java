/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.IEObjectDescription;

import com.google.common.base.Predicate;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Library;

/**
 * A predicate implementation to check if an object is acceptable as a referenced object from an
 * {@link Event} instance.
 * <p>The test rejects objects that are in the same resource as the Event object and objects that are
 * contained in a CCSL {@link Library}.
 * @author Benoit Ferrero
 * @author Julien Deantoni
 *
 */
public final class EventPredicate implements Predicate<IEObjectDescription> 
{
	EObject context;
	EReference reference;

	public EventPredicate(EObject context, EReference reference) {
		super();
		this.context = context;
		this.reference = reference;
	}

	@Override
	public boolean apply(IEObjectDescription input) {
		if (context instanceof Event  && reference==TimeModelPackage.eINSTANCE.getEvent_ReferencedObjectRefs())
		{
			Resource r=input.getEObjectOrProxy().eResource();
			if ( r == context.eResource()) // est dans le extendCCSL courant
				return false;
			if (r != null && r.getContents() != null && r.getContents().get(0) instanceof Library)// est dans une lib 
				return false;
			return true;
		}
		return true;
	}
}