/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import java.io.InputStream;

import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.xtext.Constants;
import org.eclipse.xtext.ui.editor.LanguageSpecificURIEditorOpener;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.XtextReadonlyEditorInput;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class CCSLLanguageSpecificURIEditorOpener extends
		LanguageSpecificURIEditorOpener {

	public static  final class PluginStorage extends PlatformObject implements IStorage {

		public PluginStorage(URI uri) {
			super();
			assert (uri.isPlatformPlugin());
			// /this.uri = uri;
			uri2 = uri;//
		}

		public URI getUri2() {
			return uri2;
		}

		URI uri2;

		// URI uri;

		@Override
		public boolean isReadOnly() {

			return true;
		}

		@Override
		public String getName() {
			return uri2.toString();
		}

		@Override
		public IPath getFullPath() {		
			return new Path(uri2.toString());
					//"%plugin%"+uri2.toPlatformString(true));
		}

		@Override
		public InputStream getContents() throws CoreException {
			try {
				ResourceSetImpl rs = new ResourceSetImpl();
				return rs.getURIConverter().createInputStream(uri2);

			} catch (Throwable e) {
				throw new RuntimeException(e);
			}
		}
	}

	public CCSLLanguageSpecificURIEditorOpener() {
		super();

	}

	@Inject
	@Named(Constants.LANGUAGE_NAME)
	private String editorID2;

	private IWorkbenchPage activePage;

	@Override
	public IEditorPart open(URI uri, EReference crossReference,
			int indexInList, boolean select) {
 		IEditorPart iep = super.open(uri, crossReference, indexInList, select);
		if (iep == null && PlatformUI.isWorkbenchRunning()) {
			if (uri.isPlatformResource() )
			{
				if ("platform:".equals(uri.segment(1)))
				{
					String s=uri.toString();
					uri=URI.createPlatformPluginURI(s.substring("platform:/resource/platform:/plugin".length()),false);
				}
			}
			if (uri.isPlatformPlugin() ) {
				try {
					activePage = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage();

					IEditorInput editorInput = getOrCreatePluginStorage(uri);

					IEditorPart editor = IDE.openEditor(activePage,
							editorInput, editorID2);
					selectAndReveal(editor, uri, crossReference, indexInList,
							select);
					return EditorUtils.getXtextEditor(editor);
				} catch (Throwable e) {
					System.err.println(e);
					// e.printStackTrace();
				}
			}
		}
		return iep;
	}

	private IEditorInput getOrCreatePluginStorage(URI uri) {
		URI uri2 = URI.createPlatformPluginURI(uri.toPlatformString(true),
				false);
		try {
			IEditorReference[] iert = activePage.getEditorReferences();
			for (IEditorReference ier : iert) {
				IEditorPart iep = ier.getEditor(false);
				if (iep instanceof XtextEditor) {
					IEditorInput ie = iep.getEditorInput();
					if (ie instanceof XtextReadonlyEditorInput) {
						IStorage is = ((XtextReadonlyEditorInput) ie)
								.getStorage();
						if (is instanceof PluginStorage) {
							if (uri2.equals(((PluginStorage) is).getUri2())) {
								return ie;
							}
						}
					}
				}
			}

		} catch (Throwable e) {
			System.err.println(e);
		}
		IStorage storage = new PluginStorage(uri2);
		return new XtextReadonlyEditorInput(storage);

	}

}
