/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ide.editor.syntaxcoloring.ISemanticHighlightingCalculator;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.KernelExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;

public class CCSLSemanticHighlightingCalculator implements ISemanticHighlightingCalculator {

	public CCSLSemanticHighlightingCalculator() {
		super();
		initlst();
		initlst2();

	}

	ArrayList<String> lst = null;
	ArrayList<String> lst2 = null;

	protected void initlst() {
		lst = new ArrayList<String>();
		lst.add("concreteEntities");
		lst.add("expressionLibraries");
		lst.add("relationLibraries");
		lst.add("ClockConstraintSystem");
		lst.add("imports");
		lst.add("expressionDeclarations");
		lst.add("relationDeclarations");
		lst.add("expressionDefinitions");
		lst.add("relationDefinitions");
		lst.add("Library");
		lst.add("superBlock");
		lst.add("subBlock");
		lst.add("Block");
		lst.add("ExpressionLibrary");
		lst.add("elements");
		lst.add("RelationLibrary");
	}

	protected void initlst2() {
		lst2 = new ArrayList<String>();
		lst2.add("Integer");
		// lst2.add("Clock");
		lst2.add("Sequence");
		lst2.add("IntegerSequence");
		lst2.add("IntegerVariableRef");
		lst2.add("root");
		lst2.add("SequenceType");
		lst2.add("DiscreteClockType");
		lst2.add("IntegerType");
		lst2.add("DenseClockType");
		lst2.add("RealType");
	}

	private void nodeKeyword(Keyword k, IHighlightedPositionAcceptor acceptor, INode node) {
		String kw = k.getValue();
		// System.out.println(kw);
		if (lst.indexOf(kw) != -1) {
			acceptor.addPosition(node.getOffset(), node.getLength(),
					CCSLDefaultHighlightingConfiguration.Structural_Keyword);
			return;
		}
		if (lst2.indexOf(kw) != -1) {
			acceptor.addPosition(node.getOffset(), node.getLength(),
					CCSLDefaultHighlightingConfiguration.Structural_Keyword2);
			return;
		}
		if ("Clock".equals(kw)) {
			acceptor.addPosition(node.getOffset(), node.getLength(), CCSLDefaultHighlightingConfiguration.Clock);
		}
		if ("assert".equals(kw)) {
			acceptor.addPosition(node.getOffset(), node.getLength(), CCSLDefaultHighlightingConfiguration.Assertion);
		}
		if ("Relation".equals(kw)) {
			EObject eo = node.getParent().getSemanticElement();
			if (eo instanceof Relation && ((Relation) eo).getIsAnAssertion()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), CCSLDefaultHighlightingConfiguration.Assertion);
			}
		}

	}

	private void nodeIDRuleCall(IHighlightedPositionAcceptor acceptor, INode node) {
		boolean b = false;
		EObject nd = null;

		if (node.getSemanticElement() instanceof Clock) {
			acceptor.addPosition(node.getOffset(), node.getLength(), CCSLDefaultHighlightingConfiguration.Clock);
			return;
		}
		if (node.getSemanticElement() instanceof Type) {
			acceptor.addPosition(node.getOffset(), node.getLength(),
					CCSLDefaultHighlightingConfiguration.Predefined_Types);
			return;
		}
		if (node.getSemanticElement() instanceof AbstractEntity) {
			b = true;
			nd = ((AbstractEntity) node.getSemanticElement()).getType();
		}
		if (node.getSemanticElement() instanceof Element) {
			b = true;
			nd = ((Element) node.getSemanticElement()).getType();
		}
		if (node.getSemanticElement() instanceof ExpressionDeclaration) {
			b = true;
			nd = ((ExpressionDeclaration) node.getSemanticElement()).getReturnType();
		}
		if (node.getSemanticElement() instanceof Relation) {
			b = true;
			nd = ((Relation) node.getSemanticElement()).getType();
		}
		if (node.getSemanticElement() instanceof Expression) {
			b = true;
			nd = ((Expression) node.getSemanticElement()).getType();
			// name or type
			if (!(node.getParent().getGrammarElement() instanceof CrossReference)) {
				// name  
				acceptor.addPosition(node.getOffset(), node.getLength(), CCSLDefaultHighlightingConfiguration.Clock);
				return;
			}

		}
		if (node.getSemanticElement() instanceof Binding) {

			nd = ((Binding) node.getSemanticElement()).getBindable();
			if (node.getParent().getGrammarElement() instanceof CrossReference) {
				CrossReference cr = ((CrossReference) node.getParent().getGrammarElement());
				// System.out.println(cr.getType().getClassifier().getName());
				if (nd instanceof Clock
						&& cr.getType().getClassifier() == ClockExpressionAndRelationPackage.eINSTANCE
								.getBindableEntity()) {
					acceptor.addPosition(node.getParent().getOffset(), node.getParent().getLength(),
							CCSLDefaultHighlightingConfiguration.Clock);
					return;
				}
				if (nd instanceof Expression
						&& cr.getType().getClassifier() == ClockExpressionAndRelationPackage.eINSTANCE
								.getBindableEntity()) {
					acceptor.addPosition(node.getParent().getOffset(), node.getParent().getLength(),
							CCSLDefaultHighlightingConfiguration.Clock);
					return;
				}
				
			}
		}

		if (b & node.getParent().getGrammarElement() instanceof CrossReference) {

			if (nd instanceof Type) {

				acceptor.addPosition(node.getOffset(), node.getLength(),
						CCSLDefaultHighlightingConfiguration.Predefined_Types);
				return;
			}
			if (nd instanceof KernelRelationDeclaration || nd instanceof KernelExpressionDeclaration) {
				acceptor.addPosition(node.getOffset(), node.getLength(),
						CCSLDefaultHighlightingConfiguration.Kernel_Relation_Expression);
			} else {
				acceptor.addPosition(node.getOffset(), node.getLength(),
						CCSLDefaultHighlightingConfiguration.Lib_Relation_Expression);
			}
		}
	}



	@Override
	public void provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor,
			CancelIndicator cancelIndicator) {
		if (resource == null)
			return;
		if (lst == null) {
			initlst();
		}
		if (lst2 == null) {
			initlst2();
		}
		Iterable<INode> allNodes = resource.getParseResult().getRootNode().getAsTreeIterable();
		for (INode node : allNodes) {

			try {
				if (node.getGrammarElement() instanceof Keyword) {
					Keyword k = (Keyword) node.getGrammarElement();
					nodeKeyword(k, acceptor, node);
				}
				if (node.getGrammarElement() instanceof RuleCall) {

					if ("ID".equals(((RuleCall) node.getGrammarElement()).getRule().getName())) {
						nodeIDRuleCall(acceptor, node);

					}
				}
			} catch (Throwable e) {
				// TODO: handle exception
			}

		}
		
	}


}