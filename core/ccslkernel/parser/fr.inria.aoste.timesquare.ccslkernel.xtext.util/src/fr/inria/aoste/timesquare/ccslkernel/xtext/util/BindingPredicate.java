/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.resource.IEObjectDescription;

import com.google.common.base.Predicate;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;

public final class BindingPredicate implements
		Predicate<IEObjectDescription> {

	static public final EReference binding_Abstract = ClockExpressionAndRelationPackage.eINSTANCE
			.getBinding_Abstract();
	static public final EReference binding_Bindable = ClockExpressionAndRelationPackage.eINSTANCE
			.getBinding_Bindable();
	private final EObject context;
	private final EReference reference;
	private final EObject contextcontainer;
	public BindingPredicate(EObject context, EReference reference) {
		super();
		this.context = context;
		this.reference = reference;
		if (context instanceof Binding)
		{
			contextcontainer = context.eContainer();
		}
		else
		{
			if (context instanceof Relation || context instanceof Expression)
			{
				contextcontainer = context;
			}
			else
			{
				contextcontainer=null;
			}
		}
		
		
	}

	@Override
	public boolean apply(IEObjectDescription input) {
		
		final EObject inputEObject= input.getEObjectOrProxy();		
		if (/*context instanceof Binding &&*/ reference == binding_Abstract) {
			
			if (contextcontainer instanceof Relation) {
				RelationDeclaration r = ((Relation) contextcontainer).getType();
				/*EObject c = inputEObject.eContainer();
				if ( c instanceof RelationDeclaration)
				{				
					return (c == r) ;
				}*/
				if ((r != null) && r.eContents().contains(inputEObject))
					return true;
				return false;
			}
			if (contextcontainer instanceof Expression) {
				ExpressionDeclaration r = ((Expression) contextcontainer).getType();
				/*EObject c = inputEObject.eContainer();
				if ( c instanceof ExpressionDeclaration)
				{
					return (c == r) ;
				}*/
				if ( r.eContents().contains(inputEObject))
					return true;
				return false;
			}
			return false;
		}
		if (context instanceof Binding && reference == binding_Bindable) {
//			AbstractEntity ae= ((Binding )context).getAbstract();
//			if (ae!=null && inputEObject instanceof BindableEntity)
//			{
//				if( BindingValidation.validateBinding(ae, (BindableEntity) inputEObject).getSeverity()== IStatus.ERROR)
//					return false;
//			}
			NamedElement definition=getContainerDefinition(context);
			if ( definition!=null)
			{
				NamedElement definition2= getContainerDefinition(inputEObject);
				if ( definition2!=null)
				{
					if (definition!=definition2)
						return false;
				}
				else
				{
					definition2= getContainerDeclaration(inputEObject);
					if (definition2!=null)
					{
						if( definition instanceof RelationDefinition)
							return ((RelationDefinition) definition).getDeclaration()==definition2 ;
						if( definition instanceof ExpressionDefinition)
							return ((ExpressionDefinition) definition).getDeclaration()==definition2 ;
					}
					
				}
			}
			if (inputEObject instanceof Element)
			{				
//				NamedElement definition2= getContainerDefinition(inputEObject);
//				if ( definition2!=null && definition==null)
//				{
//					return false;
//				}
//				if (inputEObject.eContainer() instanceof Element)
//				{
//					return false;
//				}
				return true;
			}
			if (inputEObject.eResource() == context.eResource()) {
				// AbstractEntity a=((Binding) context).getAbstract();
				// BindableEntity be= (( BindableEntity)
				// input.getEObjectOrProxy());
				return true;
			} else {
				
				return false;
			}
			
		} 			
		return false;
	}
	
	
	private NamedElement getContainerDefinition(EObject eo)
	{
				
		if (eo==null)
			return null;
		if (eo instanceof RelationDefinition )
			return (NamedElement) eo;
		if (eo instanceof ExpressionDefinition )
			return (NamedElement) eo;
		return getContainerDefinition( eo.eContainer());
		
	}
	
	private NamedElement getContainerDeclaration(EObject eo)
	{
				
		if (eo==null)
			return null;
		if (eo instanceof RelationDeclaration )
			return (NamedElement) eo;
		if (eo instanceof ExpressionDeclaration )
			return (NamedElement) eo;
		return getContainerDeclaration( eo.eContainer());
		
	}
}