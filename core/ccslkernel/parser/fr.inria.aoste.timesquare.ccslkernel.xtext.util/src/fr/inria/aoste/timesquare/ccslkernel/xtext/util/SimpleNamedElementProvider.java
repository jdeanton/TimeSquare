/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;

import com.google.inject.Inject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;

public class SimpleNamedElementProvider extends org.eclipse.xtext.naming.SimpleNameProvider {
	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;

	public QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj instanceof NamedElement) {
			String name = ((NamedElement) obj).getName();
			if (name == null)
				return null;
			if (name.length() == 0)
				return null;
			return qualifiedNameConverter.toQualifiedName(name);
		}
		return null;
	}
}