/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import org.eclipse.xtext.scoping.impl.ImportUriResolver;
import org.eclipse.xtext.validation.AbstractDeclarativeValidator;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.validation.CheckType;
import org.eclipse.xtext.validation.EValidatorRegistrar;

import com.google.inject.Inject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;

public class NamedElementValidation  extends AbstractDeclarativeValidator {

	@Inject
	private ImportUriResolver resolver;

	/*@Check(value=CheckType.FAST)
	public void checkNamedElement(NamedElement r)
	{
		try
		{
		if ( r instanceof IntegerElement)
		{
			if (r.eContainer()instanceof SequenceElement) 
			{
				return;
			}
		}
		if ( r instanceof ClassicalExpression)
		{
			return ;
		}
		String s=r.getName();		
		if( s==null || s.length()==0)
		{
			error(r.eClass().getName()+".name are undefined", TimeModelPackage.eINSTANCE.getNamedElement_Name());	
		}
		}
		catch (Throwable e) {
			System.err.println(e);
		}
	}*/

	@Check(value=CheckType.FAST)
	public void checkIntegerElement(IntegerElement r)
	{
		try
		{
		//if ( r instanceof IntegerElement)
		{
			if (r.eContainer()instanceof SequenceElement) 
			{
				return;
			}
		}
		
		String s=r.getName();		
		if( s==null || s.length()==0)
		{
			error(r.eClass().getName()+".name are undefined", TimeModelPackage.eINSTANCE.getNamedElement_Name());	
		}
		}
		catch (Throwable e) {
			System.err.println(e);
		}
	}
	
	
	
	
	

	@Override
	public void register(EValidatorRegistrar registrar) {
		// not registered for any package
	}
	
	public void setResolver(ImportUriResolver resolver) {
		this.resolver = resolver;
	}

	public ImportUriResolver getResolver() {
		return resolver;
	}

}