/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import org.eclipse.core.runtime.Status;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.scoping.impl.ImportUriResolver;
import org.eclipse.xtext.validation.AbstractDeclarativeValidator;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.validation.CheckType;
import org.eclipse.xtext.validation.EValidatorRegistrar;

import com.google.inject.Inject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.CharElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RecordElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.StringElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDefinition;

public class BindingValidation extends AbstractDeclarativeValidator {

	@Inject
	private ImportUriResolver resolver;

	@Check(value = CheckType.FAST)
	public void checkExpression(Expression r) {
		try {
			String name = "Expression " + r.getName() + " :";
			// error("missing Binding",ClockExpressionAndRelationPackage.eINSTANCE.getRelation_Type()
			// );
			if (r.getType() == null)
				error("missing Expression",
						ClockExpressionAndRelationPackage.eINSTANCE
								.getExpression_Type());
			for (EObject eo : r.getType().eContents()) {
				if (eo instanceof AbstractEntity) {
					boolean b = true;
					int n = -1;
					EList<Binding> l = r.getBindings();
					for (int i = 0; i < l.size(); i++) {
						if (l.get(i).getAbstract() != null
								&& l.get(i).getAbstract().equals(eo)) {
							if (n == -1) {
								n = i;
								// System.out.println("find "+ eo);
							} else {
								if (b)
									error(name + " duplicated binding "
											+ ((AbstractEntity) eo).getName()
											,
											l.get(n),
											ClockExpressionAndRelationPackage.eINSTANCE
													.getBinding_Abstract(), n);
								b = false;
								error(name + " duplicated binding "
										+ ((AbstractEntity) eo).getName()
										,
										l.get(i),
										ClockExpressionAndRelationPackage.eINSTANCE
												.getBinding_Abstract(), i);
							}
						}
					}
					if (n == -1) {
						error(name + " missing binding "
								+ ((AbstractEntity) eo).getName() ,
								ClockExpressionAndRelationPackage.eINSTANCE
										.getExpression_Type());
					}
				}
			}

		} catch (Throwable e) {
			System.err.println(e);
		}
	}

	@Check
	public void checkRelation(Relation r) {
		try {
			String name = "Relation " + r.getName() + " :";
			// error("missing Binding",ClockExpressionAndRelationPackage.eINSTANCE.getRelation_Type()
			// );
			if (r.getType() == null)
				error("missing Relation",
						ClockExpressionAndRelationPackage.eINSTANCE
								.getRelation_Type());
			for (EObject eo : r.getType().eContents()) {
				if (eo instanceof AbstractEntity) {
					boolean b = true;
					int n = -1;
					EList<Binding> l = r.getBindings();
					for (int i = 0; i < l.size(); i++) {
						// System.out.println(i+ " "+l.get(i).getAbstract());
						if (l.get(i).getAbstract() != null) {
							if (l.get(i).getAbstract().equals(eo)) {
								if (n == -1) {
									n = i;
									// System.out.println("find "+ eo);
								} else {
									if (b)
										error(name
												+ " duplicated binding "
												+ ((AbstractEntity) eo)
														.getName(),
												l.get(n),
												ClockExpressionAndRelationPackage.eINSTANCE
														.getBinding_Abstract(),
												n);
									b = false;
									error(name + " duplicated binding "
											+ ((AbstractEntity) eo).getName(),
											l.get(i),
											ClockExpressionAndRelationPackage.eINSTANCE
													.getBinding_Abstract(), i);
								}
							}
						}
					}
					if (n == -1) {
						error(name + " missing binding "
								+ ((AbstractEntity) eo).getName(),
								ClockExpressionAndRelationPackage.eINSTANCE
										.getRelation_Type());
					}
				}
			}

		} catch (Throwable e) {
			System.err.println(e);
		}
	}

	@Check
	public void checkBinding(Binding b) {
		AbstractEntity abstractEntity = b.getAbstract();
		BindableEntity bindableEntity = b.getBindable();
		NamedElement defcontentbinding = getContainerDefinition(b);
		NamedElement defcontentbindableEntity = getContainerDefinition(bindableEntity);
		if (defcontentbinding != null && defcontentbindableEntity != null) {
			if (defcontentbinding != defcontentbindableEntity)
				error("binding (D): Definition", b,
						ClockExpressionAndRelationPackage.eINSTANCE
								.getBinding_Bindable(), 0);
		}
		if (abstractEntity == null)
			return;
		if (bindableEntity == null)
			return;
		testbinding(abstractEntity, bindableEntity);
	}

	private void testbinding(AbstractEntity abstractEntity,
			BindableEntity bindableEntity) {
		IStatus status = validateBinding(abstractEntity, bindableEntity);
		if (status.getSeverity() == IStatus.ERROR) {
			errorBindind(status.getMessage());
		}

	}

	public static IStatus validateBinding(AbstractEntity abstractEntity,
			BindableEntity bindableEntity) {
		Type t = abstractEntity.getType();
		if (bindableEntity instanceof AbstractEntity) {
			Type t2 = ((AbstractEntity) bindableEntity).getType();
			if (t2 != null) {
				if (!t.getClass().isInstance(t2)) {
					return createErrorStatus("binding (T3): type " + t.eClass().getName()
							+ " -> " + t2.eClass().getName() + "");
					 
				}
			}

		}
		if (bindableEntity instanceof ConcreteEntity) {
			if (bindableEntity instanceof Element) {

				EClass ctype = getType((Element) bindableEntity);
				if (ctype != null && t!=null) {
					if (!ctype.getInstanceClass()
							.isAssignableFrom(t.getClass())) {
						return createErrorStatus("binding (T0): type "
								+ t.eClass().getName() + "-> "
								+ ctype.getName() + "");
						
					}
				}

			}
			if (bindableEntity instanceof Expression) {
				Type t2 = ((Expression) bindableEntity).getType()
						.getReturnType();
				if (t2 != null) {
					if (!t.getClass().isInstance(t2)) {
						return createErrorStatus("binding (T1.a): type "
								+ t.eClass().getName() + " -> "
								+ t2.eClass().getName() + " ");
						
					}
				} else {
					if (!(t instanceof DiscreteClockType)) {
						return createErrorStatus("binding (T1.b): type  ");
						
					}
				}
			}
			if (bindableEntity instanceof Relation) {
				return createErrorStatus("binding (T2): type [Relation ]");
				
			}
		}
		return Status.OK_STATUS;
		// bindableEntity.
	}
	
	public static EClass getType(Element e) {
		if (e.getType() != null)
			return e.getType().eClass();
		if (e instanceof Clock) {
			return BasicTypePackage.eINSTANCE.getDiscreteClockType();
		}
		if (e instanceof RecordElement) {
			return BasicTypePackage.eINSTANCE.getRecord();
		}
		if (e instanceof SequenceElement) {
			return BasicTypePackage.eINSTANCE.getSequenceType();
		}
		if (e instanceof PrimitiveElement) {
			if (e instanceof BooleanElement)
				return BasicTypePackage.eINSTANCE.getBoolean();
			if (e instanceof IntegerElement)
				return BasicTypePackage.eINSTANCE.getInteger();
			if (e instanceof RealElement)
				return BasicTypePackage.eINSTANCE.getReal();
			if (e instanceof StringElement)
				return BasicTypePackage.eINSTANCE.getString();
			if (e instanceof CharElement)
				return BasicTypePackage.eINSTANCE.getChar();
			if (e instanceof ClassicalExpression) {
				if (e instanceof BooleanExpression)
					return BasicTypePackage.eINSTANCE.getBoolean();
				if (e instanceof IntegerExpression)
					return BasicTypePackage.eINSTANCE.getInteger();
				if (e instanceof RealExpression)
					return BasicTypePackage.eINSTANCE.getReal();
				if (e instanceof SeqExpression)
					return BasicTypePackage.eINSTANCE.getSequenceType();
			}
		}
		return null;
	}

	private static IStatus createErrorStatus(String message)
	{
		return new Status(IStatus.ERROR, Activator.PLUGIN_ID, message);
	}

	public void errorBindind(String s) {
		try {
			error(s,
					ClockExpressionAndRelationPackage.eINSTANCE
							.getBinding_Bindable());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private NamedElement getContainerDefinition(EObject eo) {

		if (eo == null)
			return null;
		if (eo instanceof RelationDefinition)
			return (NamedElement) eo;
		if (eo instanceof ExpressionDefinition)
			return (NamedElement) eo;
		return getContainerDefinition(eo.eContainer());

	}

	@Override
	public void register(EValidatorRegistrar registrar) {
		// not registered for any package
	}

	public void setResolver(ImportUriResolver resolver) {
		this.resolver = resolver;
	}

	public ImportUriResolver getResolver() {
		return resolver;
	}

}