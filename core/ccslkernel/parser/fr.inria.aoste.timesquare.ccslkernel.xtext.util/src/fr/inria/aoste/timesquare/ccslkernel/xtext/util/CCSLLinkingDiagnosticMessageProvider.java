/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.diagnostics.Diagnostic;
import org.eclipse.xtext.diagnostics.DiagnosticMessage;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.linking.impl.LinkingDiagnosticMessageProvider;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Event;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;

public class CCSLLinkingDiagnosticMessageProvider extends
		LinkingDiagnosticMessageProvider {

	public DiagnosticMessage getUnresolvedProxyMessage(
			ILinkingDiagnosticContext context) {
		EObject eo = context.getContext();
		EClass referenceType = context.getReference().getEReferenceType();
		if (eo instanceof Event
				&& TimeModelPackage.eINSTANCE.getEvent_ReferencedObjectRefs() == context
						.getReference()) {
			return new DiagnosticMessage("For clock  '"
					+ ((Clock) (eo.eContainer())).getName()
					+ "' , not resolve  '" + context.getLinkText() + "'.",
					Severity.WARNING, Diagnostic.LINKING_DIAGNOSTIC);
		}
		if (eo instanceof Binding
				&& ClockExpressionAndRelationPackage.eINSTANCE
						.getBinding_Bindable() == context.getReference()) {
			AbstractEntity ae = ((Binding) eo).getAbstract();
			if (ae != null) {
				Type t = ae.getType();
				if (t != null) {
					String msg = "Binding :  '" + context.getLinkText()
							+ "' is not " + " [" + t.getName() + ":"
							+ t.eClass().getName() + "].";
					return new DiagnosticMessage(msg, Severity.ERROR,
							Diagnostic.LINKING_DIAGNOSTIC);
				}
			}
		}

		String msg = "Couldn't resolve reference to " + referenceType.getName()
				+ " '" + context.getLinkText() + "'.";
		return new DiagnosticMessage(msg, Severity.WARNING,
				Diagnostic.LINKING_DIAGNOSTIC);
	}

}
