/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.xtext.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;

public class CCSLDefaultHighlightingConfiguration extends
		DefaultHighlightingConfiguration {

	public static final String Kernel_Relation_Expression="kre";
	public static final String Lib_Relation_Expression="lre";
	public static final String Structural_Keyword="sk";
	
	public static final String Structural_Keyword2="sk2";
	public static final String Predefined_Types="pdt";
	
	public static final String Assertion="assert";
	public static final String Clock = "clock";
	public void configure(IHighlightingConfigurationAcceptor acceptor) {
		super.configure(acceptor);
		acceptor.acceptDefaultHighlighting(Kernel_Relation_Expression, "Kernel Relation/Expression",
				kernel_Relation_ExpressionTextStyle());
		acceptor.acceptDefaultHighlighting(Lib_Relation_Expression, "Lib Relation/Expression",
				lib_Relation_ExpressionTextStyle());
		acceptor.acceptDefaultHighlighting(Structural_Keyword, "Structural Keyword",
				structural_KeywordTextStyle());
		acceptor.acceptDefaultHighlighting(Structural_Keyword2, "Structural Keyword 2",
				structural_Keyword2TextStyle());
		acceptor.acceptDefaultHighlighting(Predefined_Types, "predefined Types",
				predefined_TypesTextStyle());
		acceptor.acceptDefaultHighlighting(Assertion, "Assertion",
				assertionTextStyle());
		acceptor.acceptDefaultHighlighting(Clock, "Clock",
				clockTextStyle());
	}

	
	private TextStyle clockTextStyle() {
		TextStyle textStyle = keywordTextStyle().copy();
		textStyle.setColor(new RGB(255, 32, 32));
		textStyle.setStyle(  SWT.ITALIC | SWT.BOLD);
		return textStyle;
	}
	
	private TextStyle assertionTextStyle() {
		TextStyle textStyle = keywordTextStyle().copy();
		//textStyle.setColor(new RGB(0, 0, 255));
		textStyle.setStyle(textStyle.getStyle() |  SWT.ITALIC );
		return textStyle;
	}


	public TextStyle predefined_TypesTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(new RGB(0, 0, 255));
		textStyle.setStyle(SWT.ITALIC );
		return textStyle;
	}
	
	public TextStyle kernel_Relation_ExpressionTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();//new TextStyle();
		// textStyle.setBackgroundColor(new RGB(255, 255, 255));
		textStyle.setColor(new RGB(40, 225, 225));
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}
	
	public TextStyle lib_Relation_ExpressionTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();//new TextStyle();
		// textStyle.setBackgroundColor(new RGB(255, 255, 255));
		textStyle.setColor(new RGB(0, 200, 60));
		return textStyle;
	}
	
	public TextStyle structural_KeywordTextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(new RGB(127, 0, 66));
		textStyle.setStyle(SWT.ITALIC );
		return textStyle;
	}
	
	public TextStyle structural_Keyword2TextStyle() {
		TextStyle textStyle = defaultTextStyle().copy();
		textStyle.setColor(new RGB(255, 0, 66));
		textStyle.setStyle(SWT.ITALIC );
		return textStyle;
	}
}