/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.explorer.ui;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IGraphContentProvider;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.viewers.IZoomableWorkbenchPart;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;

import fr.inria.aoste.timesquare.ccslkernel.explorer.CCSLConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.explorer.StateSpace;
import grph.oo.ObjectGrph;

public class StateSpace4zest implements IZoomableWorkbenchPart {

	public static class ClockTreeFilter extends ViewerFilter {

		public ClockTreeFilter() {
			super();
		}

		@Override
		public boolean select(Viewer viewer, Object parentElement, Object element) {
//			// System.out.println("F+"+element);
//			if ( element instanceof StringBuffer)
//			{
//			}
//			if ( element instanceof CCSLConstraintState)
//			{
//				
//			}
			return true;
		}
	}

	public static class GraphContentProvider implements IGraphContentProvider {// ,INestedContentProvider{

		public GraphContentProvider() {
			super();			
		}

		private StateSpace _og = null;

		@Override
		public void dispose() {		

		}

		@SuppressWarnings("unchecked")
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			_og = null;
			if (newInput instanceof ObjectGrph) {
				_og = (StateSpace)newInput;
			}

		}

		@Override
		public Object getSource(Object rel) {
			if (rel instanceof StringBuffer) {
				if (_og != null)
					return _og.getDirectedSimpleEdgeTail((StringBuffer) rel);
			}
			if (rel instanceof CCSLConstraintState) {
				return rel;
			}
			return null;
		}

		@Override
		public Object getDestination(Object rel) {
			if (rel instanceof StringBuffer) {
				if (_og != null)
					return _og.getDirectedSimpleEdgeHead((StringBuffer) rel);
			}
			return null;
		}

		@Override
		public Object[] getElements(Object input) {
			if (input instanceof ObjectGrph) {
				Collection<CCSLConstraintState> tmp = _og.getVertices();
				Collection<Object> lst = new HashSet<Object>(tmp);
				// add vertex for vertex whithotu edge)
				lst.addAll(_og.getEdges());
				return lst.toArray(new Object[lst.size()]);
			}
			return new Object[] {};
		}
	}

	public static class LabellingProvider implements IBaseLabelProvider, ISelfStyleProvider {

		private StateSpace statespace;
		public LabellingProvider(StateSpace s) {
			super();
			statespace = s;

		}

		@Override
		public void addListener(ILabelProviderListener listener) {

		}

		@Override
		public void dispose() {

		}

		@Override
		public boolean isLabelProperty(Object element, String property) {

			return true;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {

		}

		@Override
		public void selfStyleConnection(Object element, GraphConnection c) {
			if (element instanceof StringBuffer) {
				StringBuffer e = (StringBuffer) element;
				c.setTooltip(new Label(e.toString()));
//				Color color = e.getColor(null);
//				c.setLineColor(color);
				c.setCurveDepth(10);
				c.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
				
			}

		}

		@Override
		public void selfStyleNode(Object element, GraphNode node) {
			// GraphNode n = new GraphNode(_g, SWT.NONE, v.getVertexLabel());
			if (element instanceof CCSLConstraintState) {
				CCSLConstraintState v = (CCSLConstraintState) element;
				if (statespace.getOutNeighbors(v).size() == 0){
					node.setBackgroundColor(new Color(null, 255, 0, 0));
				}
//				boolean isInitial = true;
//				for( StringBuffer s : statespace.getIncidentEdges(v)){
//					if (statespace.getDirectedSimpleEdgeHead(s).equals(v)){
//						isInitial = false;
//					}
//				}
//				if (isInitial){
//					node.setBackgroundColor(new Color(null, 0, 255, 0));
//				}
			}
		}

	}

	private StateSpace _og = new StateSpace();
	private Graph _g;
	private GraphViewer _gv = null;

	public StateSpace4zest(StateSpace g, Composite parent) {
		this(parent);
		setGrph( g);
	}

	public StateSpace4zest(Composite parent) {
		_gv = new GraphViewer(parent, SWT.NONE);
		_g = _gv.getGraphControl();
		_gv.setLabelProvider(new LabellingProvider(_og));
		_gv.setContentProvider(new GraphContentProvider());
		_gv.addFilter(new ClockTreeFilter());
	}

	public void setGrph(StateSpace g) {
		dispose();
		_og = g;
		((LabellingProvider)_gv.getLabelProvider()).statespace = _og;
		_gv.setInput(g);	
	}

	public StateSpace getGrph() {
		return _og;
	}

	public Graph getZestGraph() {
		return _g;
	}

	@Override
	public GraphViewer getZoomableViewer() {
		return _gv;
	}

	public void dispose() {
		try {
			_gv.setInput(new Object());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*	public void createVertex(){

			for(CoincidentClocks v : _og.vertexSet()){
			//	_gv.addNode(v);
				
				GraphNode n = new GraphNode(_g, SWT.NONE, v.getVertexLabel());
				Color color =  v.getEdgeColor(_d);
				n.setBorderColor(color);
				
				Label fig = new Label(v.getLongVertexLabel());
				n.setTooltip(fig);
				_gNodes.put(v, n);
				_listItem.add(n);
			}
		}
		
		public void createEdges(){
			for(Edge e : _og.edgeSet()){
				
				CoincidentClocks dest= _og.getEdgeTarget(e);
				CoincidentClocks source= _og.getEdgeSource(e);
				//_gv.addRelationship(e, source, dest);
				GraphConnection c = new GraphConnection(_g, SWT.NONE, _gNodes.get(source), _gNodes.get(dest));
				
				Color color = e.getColor(_d);
				c.setLineColor(color);
				c.setConnectionStyle(Graphics.LINE_DASH);
				c.setText(e.getLabel());
				c.setWeight(e.getSize());
				_gEdges.put(e,c);
				_listItem.add(0,c);
			}
		}*/

}