/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.explorer.ui.popup.action;

import java.io.IOException;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;

import fr.inria.aoste.timesquare.ccslkernel.explorer.CCSLConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.explorer.CCSLKernelExplorer;
import fr.inria.aoste.timesquare.ccslkernel.explorer.StateSpace;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.kairos.timesquare.grph.viewer.GrphViewControler;
import fr.inria.kairos.timesquare.statespace.view.views.MoCCMLStateSpaceView;
import toools.collections.primitive.IntCursor;
import toools.io.file.RegularFile;

public class CreateStateSpace extends AbstractHandler {

	private IFile ccslFile;
	private CCSLKernelExplorer clockExplorer;
	private String ccslFilePath;
	private String ccslPriorityFilePath;
	
	/**
	 * Constructor for Action1.
	 */
	public CreateStateSpace() {
		super();
	}

	StateSpace stateSpace = null;
	

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			if (((IStructuredSelection) selection).size() == 1) {
				Object selected = ((IStructuredSelection) selection).getFirstElement();
				if (selected instanceof IFile) {
					ccslFile = (IFile) selected;
					ccslFilePath = ccslFile.getLocation().toString();
					ccslPriorityFilePath = ccslFilePath.replaceAll("extendedCCSL", "ccslPriority");
				}
			}
		}
		
	Job job = new Job("TimeSquare State Space Exploration on "+ccslFile.toString()) {
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		                
		
		clockExplorer = new CCSLKernelExplorer(monitor);
		
		Resource ccslResource = null;
		
		try {
			ccslResource = ResourceLoader.INSTANCE.loadResource(ccslFile.getFullPath());
		} catch (Throwable e) {
			System.err.println("load ccsl file problem on "+ccslFilePath+ "\nexception:");
			e.printStackTrace();
			return null;
		}
		Resource priorityResource = null;
		try {
			priorityResource = ResourceLoader.INSTANCE.loadResource(ccslFile.getFullPath().removeFileExtension()+".ccslPriority");
		} catch (Throwable e) {
			System.out.println("no priority file "+ccslPriorityFilePath+ "\n:");
		}
		try {
			
			clockExplorer.loadModel(ccslResource);
			if(priorityResource != null) {
				System.err.println("WARNING: state space is construct with priority file: "+ccslPriorityFilePath);
				clockExplorer.loadPriorityModel(priorityResource);
			}
		} catch (IOException | UnfoldingException | SimulationException e) {
			e.printStackTrace();
		}
		
		try {
			clockExplorer.initSimulation();
		} catch (SimulationException e1) {
			e1.printStackTrace();
		}
			
		
		stateSpace = null;
		
		try {
			stateSpace = clockExplorer.explore(false); //set true to create the dot file at each step (near the eclipse executable)
		} catch (SimulationException e) {
			e.printStackTrace();
		}
		

		
		RegularFile dotRegFile = new RegularFile(ccslFilePath+".dot"); 
		dotRegFile.setContentAsASCII(stateSpace.getGrph().toDot());
		
		dotRegFile.create();		
		
		//createCiprianStateSpaceFormat();
		createAutStateSpaceFormat();
		clockExplorer = null;

		for(IntCursor v : IntCursor.fromFastUtil(stateSpace.getGrph().getVertices())){ //removes the labels
		stateSpace.getGrph().getVertexLabelProperty().setValue(v.value, "");
		}
	
		Display.getDefault().asyncExec(new Runnable() {
		    public void run() {
		    	MoCCMLStateSpaceView statespaceView = MoCCMLStateSpaceView.getCourant();
		    	
		    	GrphViewControler statespaceViewControler = statespaceView.controller;
		    	if (statespaceViewControler != null) {
		    		statespaceViewControler.model = stateSpace.getGrph();
		    		statespaceViewControler.addGrph(stateSpace.v2i(stateSpace.initialState));
		    	}else {
		    		throw new RuntimeException("MoCCML state space view is now open, please retry");
		    	}
		    	
		    }
		});
		
		
		
		
//		Set<Path> allcycles = stateSpace.getGrph().getAllCycles();
//		System.out.println(allcycles);
		        	
		

		System.out.println(
			"# of states: "+stateSpace.getGrph().getVertices().size()+"\n# of transitions: "+stateSpace.getGrph().getEdges().size()+
			" is there an infinite schedule: "+new Boolean(stateSpace.getGrph().isCyclic())+
			"which is "+ stateSpace.getGrph().getShortestCycle()
			);
		return Status.OK_STATUS;
     }
	
	
	
	
	
	
	
	
	
	
	
	private void createAutStateSpaceFormat() {		
		RegularFile cipRegFile = new RegularFile(ccslFilePath+".aut"); 
		Iterator<CCSLConstraintState> iterVertices = stateSpace.getVertices().iterator();
		CCSLConstraintState aState = iterVertices.next();
		if (aState == null){
			System.err.println("no State space to serialize");
			return;
		}
		StringBuilder fileContent = new StringBuilder();
		fileContent.append("des(");
		fileContent.append(stateSpace.v2i(stateSpace.initialState));
		fileContent.append(",");
		fileContent.append(stateSpace.getGrph().getEdges().size());
		fileContent.append(",");
		fileContent.append(stateSpace.getGrph().getVertices().size());
		fileContent.append(")\n");

		//print all transitions
		//TODO check with Luc
//			Iterator<String> iterEdges = stateSpace.getEdges().iterator();
//			while (iterEdges.hasNext()){
//				String transition = iterEdges.next();
//				String aLine= serializeTransition(transition, clockNameToIndex);
//				fileContent.append(aLine);
//			}
		
		for(CCSLConstraintState s1 : stateSpace.getVertices()){
			for(CCSLConstraintState s2 : stateSpace.getVertices()){
				for(StringBuffer t: stateSpace.getEdges(s1, s2)){
					String aLine= "("+(stateSpace.v2i(s1)+", "+ mclFormat(t) +"\", "+stateSpace.v2i(s2))+")";
					fileContent.append(aLine);
					fileContent.append("\n");
				}
			}
		}
		
		cipRegFile.setContentAsASCII(fileContent.toString());
		
		cipRegFile.create();
		
	}











	private String mclFormat(StringBuffer t) {
		StringBuffer res = new StringBuffer("\"LS !");
		for(String s : t.toString().split(", ")) {
			res.append(":");
			res.append(s);
		}
		return res.toString().replaceAll("\\[", "").replaceAll("\\]", "");
	}
	
	
	
	
	
//
//	private void createCiprianStateSpaceFormat() {		
//		RegularFile cipRegFile = new RegularFile(ccslFilePath+".cip"); 
//		try {
//			
//			Iterator<CCSLConstraintState> iterVertices = stateSpace.getVertices().iterator();
//			CCSLConstraintState aState = iterVertices.next();
//			if (aState == null){
//				System.err.println("no State space to serialize");
//				return;
//			}
//			StringBuilder fileContent = new StringBuilder();
//			int numberConfigurationVariable = countNumberOfConfigurationVariable(aState);
//			fileContent.append(stateSpace.getGrph().getVertices().size()+" "+stateSpace.getGrph().getEdges().size()+" "+clockExplorer.getAllDiscreteClocks().size()+" "+numberConfigurationVariable+"\n");
//			
//			
//			fileContent.append("- clocks-names\n");
//			//print clock names and fill a map
//			int index = 0;
//			HashMap<String, Integer> clockNameToIndex = new HashMap<String, Integer>();
//			for(RuntimeClock c : clockExplorer.getAllDiscreteClocks()){
//				String n = c.getName();
//				clockNameToIndex.put(n, index);
//				index++;
//				fileContent.append(n+" ");
//			}
//			fileContent.append('\n');
//			
//			fileContent.append("- variable-names\n");
//
//			//print configuration variable names
//			fileContent.append(retrieveAllVariableNames(aState));
//			
//			fileContent.append("- configurations\n");
//			//print all configurations
//			String aSimpleLine= serializeState(aState);
//			fileContent.append(aSimpleLine);
//			while (iterVertices.hasNext()){
//				aState = iterVertices.next();
//				String aLine= serializeState(aState);
//				fileContent.append(aLine);
//			}
//			fileContent.append("- initial\n");			
//			fileContent.append(clockExplorer.initialState.hashCode());
//			
//			fileContent.append('\n');
//
//			fileContent.append("- transitions\n");
//			//print all transitions
//			//TODO check with Luc
////			Iterator<String> iterEdges = stateSpace.getEdges().iterator();
////			while (iterEdges.hasNext()){
////				String transition = iterEdges.next();
////				String aLine= serializeTransition(transition, clockNameToIndex);
////				fileContent.append(aLine);
////			}
//			
//			for(CCSLConstraintState s1 : stateSpace.getVertices()){
//				for(CCSLConstraintState s2 : stateSpace.getVertices()){
//					for(String t: stateSpace.getEdges(s1, s2)){
//						String aLine= serializeTransition(s1.hashCode(), s2.hashCode(), t, clockNameToIndex);
//						fileContent.append(aLine);
//					}
//				}
//			}
//			
//			cipRegFile.setContentAsASCII(fileContent.toString());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		try {
//			cipRegFile.create();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//	}
//
//	private String serializeTransition(int src, int target, String transition, HashMap<String, Integer> clockNameToIndex) {
//		StringBuilder res = new StringBuilder();
//		res.append(src).append(' ');
//		for(String clockName: transition.split(",")){
//			clockName = clockName.replaceAll("\\[", "").replace("]", "");
//			res.append(clockNameToIndex.get(clockName)).append(' ');
//		}
//		res.append(target).append(' ');
//		return res.append("\n").toString();
//	}
//
//	private int countNumberOfConfigurationVariable(CCSLConstraintState aState) {
//		int res = 0;
//		
//		for(String concreteQN : aState.keySet()){
//			 ISolverConcrete concrete = clockExplorer.getConcreteInstantiationTree().lookupInstance(concreteQN);
//			 if(concrete == null){
//				 concrete = clockExplorer.getElementInstantiationTree().lookupInstance(concreteQN);
//			 }
//			 if(concrete == null){
//				 concrete = clockExplorer.getClockInstantiationTree().lookupInstance(concreteQN);
//			 }
//			 
//			 if (concrete instanceof SolverRelationWrapper && ((SolverRelationWrapper)concrete).getWrappedRelation() instanceof StateMachineRelationDefinitionSemantics){
//				 res++;
//				 res+=((StateMachineRelationDefinitionSemantics)((SolverRelationWrapper)concrete).getWrappedRelation()).get_orderedLocalInteger().size();
//			 }
//		 }
//		return res;
//	}
//	
//	private String serializeState(CCSLConstraintState aState) {
//		StringBuilder res = new StringBuilder();
//		
//		res.append(aState.hashCode()).append(' ');
//		for(String concreteQN : aState.keySet()){
//			 ISolverConcrete concrete = clockExplorer.getConcreteInstantiationTree().lookupInstance(concreteQN);
//			 if(concrete == null){
//				 concrete = clockExplorer.getElementInstantiationTree().lookupInstance(concreteQN);
//			 }
//			 if(concrete == null){
//				 concrete = clockExplorer.getClockInstantiationTree().lookupInstance(concreteQN);
//			 }
//			 
//			 if (concrete instanceof SolverRelationWrapper && ((SolverRelationWrapper)concrete).getWrappedRelation() instanceof StateMachineRelationDefinitionSemantics){
//				 StateMachineRelationDefinitionSemantics theStateMachine = (StateMachineRelationDefinitionSemantics)((SolverRelationWrapper)concrete).getWrappedRelation();
//				 SerializedConstraintState concreteState = aState.get(concreteQN);
//				 int currentState = (Integer) concreteState.restore(0);
//				 res.append(currentState).append(" ");
//				for(int i = 1; i <= theStateMachine.get_orderedLocalInteger().size(); i++){
//					res.append((Integer) concreteState.restore(i++)).append(" ");
//				}
//				
//			 }
//		 }
//		return res.append("\n").toString();
//	}
//	
//	private String retrieveAllVariableNames(CCSLConstraintState aState) {
//		StringBuilder res = new StringBuilder();
//		for(String concreteQN : aState.keySet()){
//			 ISolverConcrete concrete = clockExplorer.getConcreteInstantiationTree().lookupInstance(concreteQN);
//			 if(concrete == null){
//				 concrete = clockExplorer.getElementInstantiationTree().lookupInstance(concreteQN);
//			 }
//			 if(concrete == null){
//				 concrete = clockExplorer.getClockInstantiationTree().lookupInstance(concreteQN);
//			 }
//			 
//			 if (concrete instanceof SolverRelationWrapper && ((SolverRelationWrapper)concrete).getWrappedRelation() instanceof StateMachineRelationDefinitionSemantics){
//				 StateMachineRelationDefinitionSemantics theStateMachine = (StateMachineRelationDefinitionSemantics)((SolverRelationWrapper)concrete).getWrappedRelation();
//				 String concreteName = ((SolverRelationWrapper)concrete).getInstantiatedElement().getQualifiedName("_");
//				 res.append(concreteName).append("_currentState").append(' ');
//				for(IntegerElement ie : theStateMachine.get_orderedLocalInteger()){
//					res.append(concreteName).append('_').append(ie.getName()).append(' ');
//				}
//				
//			 }
//		 }
//		return res.append("\n").toString();
//	}
	
  };
		job.schedule(); // start as soon as possible
		
	return null;
}
	  

}
