package fr.inria.kairos.timesquare.statespace.view.views;


import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.part.ViewPart;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.kairos.timesquare.grph.viewer.GrphView;
import fr.inria.kairos.timesquare.grph.viewer.GrphViewControler;
import javafx.embed.swt.FXCanvas;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;


/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class MoCCMLStateSpaceView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "fr.inria.kairos.timesquare.statespace.view.views.MoCCMLStateSpaceView";

	@Inject IWorkbench workbench;
	
	private static MoCCMLStateSpaceView courant = null;

	public MoCCMLStateSpaceView() {
		setCourant(this);
	}
	
	public synchronized static MoCCMLStateSpaceView getCourant() {
		if (courant == null) {
			try {
				PluginHelpers.getShowView(ID);

				if (courant == null) {
					PluginHelpers.getCreateView(ID);
				}
				PluginHelpers.getShowView(ID);
				if (courant != null) {
					courant.setFocus();
				}

			} catch (Throwable ex) {
				ErrorConsole.printError(ex, "problem initializing moccml state space view");
			}
		}
		return courant;
	}

	@Override
	public void dispose() {
		super.dispose();
		setCourant(null);
	}

	private synchronized static final void setCourant(MoCCMLStateSpaceView courant) {
		MoCCMLStateSpaceView.courant = courant;
	}


    GrphView view;
    public  GrphViewControler controller;

	private FXCanvas fxCanvas;
    @Override
    public void createPartControl(final Composite parent) {

         fxCanvas = new FXCanvas(parent, SWT.None);
         
         Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
	       		view = new GrphView();
	    		controller = new GrphViewControler(view);
	    		
	    		BorderPane root = new BorderPane();
	    		int width = parent.getSize().x;
	    		int height = parent.getSize().y;
	    		Scene scene = new Scene(root,width,height,Color.WHITE);
	    		view.internalView.modifyWidth(width - 10);
	    		view.internalView.modifyHeight(height + 10);
	    		root.getChildren().add(view.internalView);
            	fxCanvas.setScene(scene);
            	root.setOnKeyTyped(new EventHandler<Event>() {
         			@Override
         			public void handle(Event event) {
         				if (event instanceof KeyEvent) {
         					if (((((KeyEvent)event).getCode() == KeyCode.C) || ((KeyEvent)event).getCharacter().compareTo("c") == 0)) {
         						controller.highlightNextSchedule();
         						return;
         					}
         					if (((((KeyEvent)event).getCode() == KeyCode.P) || ((KeyEvent)event).getCharacter().compareTo("p") == 0)) {
         						controller.clearAllInitPath();
         						controller.highlightNextSchedule();
         						return;
         					}
         						System.out.println("skip cycle with Levenshtein distance lower than "+Integer.parseInt(((KeyEvent) event).getCharacter()));
         						controller.highlightNextDifferentSchedule(Integer.parseInt(((KeyEvent) event).getCharacter()));
         		        	
         				}
         			}
         		});
            	
            	root.setOnContextMenuRequested(new EventHandler<Event>() {
                 	@Override
         			public void handle(Event event) {
                 		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
                 		int width = parent.getSize().x;
        	    		int height = parent.getSize().y;
                 		
                 		view.internalView.modifyWidth(width -10);
                 		view.internalView.modifyHeight(height - 10);
         			}
         		});
             }
         });
 }

			private void addGrph(int initialState) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						controller.addGrph(initialState);
					}
				});
				return;
			}

  
	

	@Override
	public void setFocus() {
		fxCanvas.setFocus();
	}
}
