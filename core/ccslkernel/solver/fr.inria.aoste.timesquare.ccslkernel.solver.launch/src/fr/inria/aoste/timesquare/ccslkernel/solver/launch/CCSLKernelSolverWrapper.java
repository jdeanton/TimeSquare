/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.launch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelation;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolver;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.select.ConstraintEnable;
import fr.inria.aoste.timesquare.trace.util.QualifiedNameBuilder;
import fr.inria.aoste.timesquare.trace.util.ReferenceNameBuilder;
import fr.inria.aoste.timesquare.utils.timedsystem.TimedSystem;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.Reference;

public class CCSLKernelSolverWrapper implements ISolver, ISolverForBackend {

	private CCSLKernelSolver solver;
	private Exception exception;
	List<ModelElementReference> constraints = null;
	
	public CCSLKernelSolver getSolver() {
		return solver;
	}

	public CCSLKernelSolverWrapper() {
		super();
		solver = new CCSLKernelSolver();
	}
	
	public CCSLKernelSolverWrapper(CCSLKernelSolver existingSolver) {
		super();
		solver = existingSolver;
	}

	@Override
	public void setListReference(List<Reference> lrf) {
		solver.setTraceReferences(lrf);
		lrf.addAll(getClockList());
		lrf.addAll(getConstraint());
		lrf.addAll(getAssertList());
	}

	@Override
	public LogicalStep solveNextSimulationStep(TimedSystem ts) {
		try {
			return solver.doOneSimulationStep();
		} catch (SimulationException e) {
			exception = e;
		}
		return null;
	}

	@Override
	public void endSimulation() {
		solver.endSimulation();
	}
	
	/**
	 * This method computes the semantics of the step and all the possible solutions for this step. The result is given in a list of LogicalStep. 
	 * Note that these logical steps are partial and the complete logical step, suitable for a trace, will be given only after applying one of these steps
	 * @return a list containing all the possible steps for the next step of the simulation
	 * @throws SimulationException 
	 */
	public List<LogicalStep> computeAndGetPossibleLogicalSteps() throws SimulationException {
		return solver.computeAndGetPossibleLogicalSteps();
	}
	
	/**
	 * This method give all solution of the BDD in the current step executor. It must be called after constructBDD(). To clean the BDD in memory, please call clearBDD()
	 * @return void
	 * @throws SimulationException 
	 */
	public void constructBDD() throws SimulationException {
		solver.constructBDD();
		return;
	}
	
	/**
	 * This method construct the BDD that represents the semantics of the step. The result is of all possible logical step can be retrieved by using getAllPossibleLogicalSteps
	 * It creates a new StepExecutor storing the BDD
	 * @return a list containing all the possible steps for the next step of the simulation
	 * @throws SimulationException 
	 */
	public List<LogicalStep> getAllPossibleSteps() throws SimulationException {
		return solver.getAllPossibleSteps();
		
	}
	
	/**
	 * This method construct the BDD that represents the semantics of the step. The result is of all possible logical step can be retrieved by using getAllPossibleLogicalSteps
	 * It creates a new StepExecutor storing the BDD
	 * @throws SimulationException 
	 */
	public void clearBDD() throws SimulationException {
		solver.clearBDD();
		return;		
	}

	
	public List<LogicalStep> updatePossibleLogicalSteps() throws SimulationException {
		return solver.updatePossibleLogicalSteps();
	}
	
	/**
	 * this method is strongly associated to {@link #getPossibleLogicalSteps(CCSLStepExecutionEngine)}. It asks the chosen simulation policy to choose a Logical in the set returned by the {@link #getPossibleLogicalSteps(CCSLStepExecutionEngine)} call.
	 * So it must be called AFTER the already site method and it returns an index representing a specific LogicalStep in the list
	 * @param stepExecutor
	 * @return the chosen logical step
	 */
	public int proposeLogicalStepByIndex(){
		return solver.proposeLogicalStepByIndex();
	}
	
	/**
	 * This method is strongly associated to {@link #getPossibleLogicalSteps(CCSLStepExecutionEngine)}.
	 * It must be called after this method. Its goal is to apply the step which is at the index 'chosenLogicalStep' in the collection returned by {@link #getPossibleLogicalSteps(CCSLStepExecutionEngine)}.
	 * It rewrites the SOS rules and create the logical step resulting from the simulation step. 
	 * @param stepExecutor
	 * @param chosenLogicalStep
	 * @return the logical step resulting from the simulation
	 * @throws SimulationException  
	 */
	public LogicalStep applyLogicalStepByIndex(int chosenLogicalStep) throws SimulationException {
		return solver.applyLogicalStepByIndex(chosenLogicalStep);
	}

	@Override
	public Throwable getException() {
		return exception;
	}

	@Override
	public void endSimulationStep() {
	}

	@Override
	public ResourceSet getResourceSet() {
		return solver.getResourceSet();
	}

	@Override
	public void start() throws Throwable {
		solver.initSimulation();
	}

	@Override
	public ArrayList<ModelElementReference> getClockList() {
		ArrayList<ModelElementReference> ls= new ArrayList<ModelElementReference>();
		for (RuntimeClock c : solver.getAllDiscreteClocks()) {
			ls.add(((SolverClock)c).traceReference);
		}
		return ls;
	}
		

	
	public List<ModelElementReference> getConstraint() 
	{
		if( constraints == null) {
			constraints = new ArrayList<ModelElementReference>();
			for (InstantiatedElement element : solver.getUnfoldModel().getInstantiationTree().lookupInstances(null)) {
				if (element.isKernelRelation() && (! element.isAssertion()) ) {
					SolverRelation rel = (SolverRelation) solver.getConcreteInstantiationTree().lookupInstance(element.getInstantiationPath());
					if (rel != null) {
						constraints.add(rel.getTraceReference());
					}
				}
				/* For expressions, we do not build a dedicated ModelElementReference to designate the expression as a
				 * constraint. We reuse the ModelElementReference that has been built for the implicitClock of the
				 * expression. This way, when a LifeActivationCreator object is created later, for example in the function
				 *   theFactory.i.a.t.instantrelation.generator.InstantRelationModelGenerator.addandCreateInterOrUnion()
				 * this object will be able to find the right EventOccurrence in the LogicalStep of the trace, based on
				 * this ModelElementReference object. This search occurs in
				 *   theFactory.i.a.t.trace.util.HelperFactory.getEventOccurence(LogicalStep, Reference)
				 * and compares two ModelElementReference with == rather than with equals().
				 * Then, the found EventOccurence object will have the right information for the
				 * LifeActivationCreator object to decide about the liveness or the death of the constraint itself.
				 * See also Bug [#12848] : one symptom of this bug is that in the VCD viewer, selecting an expression in the
				 * constraint view shows nothing (ie. no instant relation) in the VCD view.
				 */
				else if (element.isKernelExpression()) {
					SolverClock iClock = solver.getClockInstantiationTree().lookupInstance(element.getInstantiationPath());
					if (iClock != null) {
						constraints.add(iClock.traceReference);
					}
					else {
						throw new NullPointerException("Null implicitClock for Kernel Expression");
					}
				}
				else if (element.isExpression() &&  ( ! element.isConditional())) {
					/* Allows to establish later a coincidence between the expression and its root, otherwise the relation
					 * built by method CCSLKernelInstantRelationModelStructure.setClockConstraintList() has
					 * an empty reference to the relation (CCSLConstraintRef.getCcslConstraint() returns null) and the
					 * label in the constraint view is unreadable.
					 */
					SolverClock iClock = solver.getClockInstantiationTree().lookupInstance(element.getInstantiationPath());
					if (iClock != null) {
						constraints.add(iClock.traceReference);
					}
					else {
						throw new NullPointerException("Null implicitClock for Expression");
					}
				}
			}
		}
		return constraints;
	}

	@Override
	public ArrayList<ConstraintEnable> getSelectableModel() {
		return new ArrayList<ConstraintEnable>();
	}
	
	public boolean revertForceClockEffect() throws SimulationException{
		solver.revertForceClockEffect();
		return true;
	}

	@Override
	public void forceClockPresence(ModelElementReference mer) {
		String qualifiedName = ReferenceNameBuilder.buildQualifiedName(mer, CCSLKernelUtils.defaultSeparator);
		SolverClock clockToForce = solver.findClockByPath(qualifiedName);
		solver.forceClockPresence(clockToForce);
	}

	@Override
	public void forceClockAbsence(ModelElementReference mer) {
		String qualifiedName = ReferenceNameBuilder.buildQualifiedName(mer, CCSLKernelUtils.defaultSeparator);
		SolverClock clockToForce = solver.findClockByPath(qualifiedName);
		solver.forceClockAbscence(clockToForce);	
	}	
	
	public void addClockCoincidence(ModelElementReference mer1, ModelElementReference mer2) {
		String qualifiedName1 = ReferenceNameBuilder.buildQualifiedName(mer1, CCSLKernelUtils.defaultSeparator);
		String qualifiedName2 = ReferenceNameBuilder.buildQualifiedName(mer2, CCSLKernelUtils.defaultSeparator);
		SolverClock clock1 = solver.findClockByPath(qualifiedName1);
		SolverClock clock2 = solver.findClockByPath(qualifiedName2);
		solver.addClockCoincidence(clock1, clock2);
	}	
	
	public boolean hasSolution(){
		return solver.hasSolution();
	}
	
	@Override
	public ArrayList<ModelElementReference> getAssertList() {
		 ArrayList<ModelElementReference> res = new ArrayList<ModelElementReference>();	
		 for (Iterator<SolverRelation> iterator = solver.assertions.iterator(); iterator.hasNext();) {
			 SolverRelation rel = (SolverRelation) iterator.next();
			res.add(rel.getTraceReference());
		 }
		 return res;
	}
	
	
	/**
	 * ask the solver to provide if the relation corresponding to the ModelElementReference is actually a violated assertion.
	 * must be called after a {@link #applyLogicalStep(Step<?> logicalStep)}
	 * @return a list of constraints where the assertion boolean is set to 'true' and which has been violated during the last step
	 */
	public boolean isAssertionViolated(ModelElementReference assertion) {
		return solver.getCurrentStepExecutor().isAssertionViolated(QualifiedNameBuilder.buildQualifiedName(assertion));
	}

}
