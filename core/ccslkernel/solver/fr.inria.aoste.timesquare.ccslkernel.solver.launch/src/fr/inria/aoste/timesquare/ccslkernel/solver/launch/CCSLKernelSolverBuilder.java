/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.launch;

import org.eclipse.core.resources.IFile;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.launcher.core.PropertySimulation;
import fr.inria.aoste.timesquare.launcher.core.inter.IBuildSolver;
import fr.inria.aoste.timesquare.launcher.core.inter.ICCSLProxy;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolver;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyManager;

public class CCSLKernelSolverBuilder implements IBuildSolver {

	public CCSLKernelSolverBuilder() {
		super();
	}

	private CCSLKernelSolverWrapper solverWrapper;
	private Throwable exception;

	@Override
	public ISolver buildSolver(ICCSLProxy iccslproxy) {
		buildSolverWrapper(iccslproxy);
		if (solverWrapper.getSolver().getModel() == null) {
			try {
				IFile ccslFile = iccslproxy.get_SourceIFile();
				solverWrapper.getSolver().loadModel(ResourceLoader.INSTANCE.loadResource(ccslFile.getFullPath()));
				
				IFile prioFile = iccslproxy.get_PrioritySourceIFile();
				if(prioFile != null)
				{
					solverWrapper.getSolver().loadPriorityModel(ResourceLoader.INSTANCE.loadResource(prioFile.getFullPath()));
				}
			} catch (Throwable e) {
				exception = e;
				return null;
			}
		}
		return solverWrapper;
	}
	
	private void buildSolverWrapper(ICCSLProxy iccslproxy) {
		solverWrapper = new CCSLKernelSolverWrapper();
		int policyId = iccslproxy.getPolicyId();
		SimulationPolicyBase policy = SimulationPolicyManager.getDefault().getNewInstance(policyId);
		solverWrapper.getSolver().setPolicy(policy);
	}
	

	@Override
	public Throwable getException() {
		return exception;
	}

	@Override
	public void clearCache(int level) {
	}

	@Override
	public int getDefaultValue() {
		return 100;
	}

	@Override
	public PropertySimulation[] getSupportPropertySimulation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean validateSolver(ICCSLProxy iccslproxy) {
		if (solverWrapper == null) {
			exception = new Exception("Internal Error: NULL solver object in validateSolver()");
			return false;
		}
		IFile ccslFile = iccslproxy.get_SourceIFile();
		if (ccslFile.getFileExtension().compareTo("extendedCCSL") != 0 &&
				ccslFile.getFileExtension().compareTo("timemodel") != 0)
			return false;
		
		if (solverWrapper.getSolver().getModel() == null) {
			try {
				solverWrapper.getSolver().loadModel(ResourceLoader.INSTANCE.loadResource(ccslFile.getFullPath()));
				return true;
			} catch (Throwable e) {
				exception = e;
				return false;
			}
		}
		return true;
	}
}
