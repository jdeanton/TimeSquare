/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.extension.filterby;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.BasicType.SolverSequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.AbstractWrappedExpression;

public class BlackboxFilterby extends AbstractWrappedExpression {

	private SolverClock filteredClock;
	private SolverSequenceElement sequence;
	private int currentDelay;

	public BlackboxFilterby(SolverClock filteredClock, SolverSequenceElement sequence) {
		super();
		this.filteredClock = filteredClock;
		this.sequence = sequence;
	}

	public void setFilteredClock(SolverClock filteredClock) {
		this.filteredClock = filteredClock;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (sequence.isEmpty()) {
			terminate(helper.getUpdateHelper());
			return;
		}
		PrimitiveElement head = sequence.nextHead();
		if (head instanceof IntegerElement) {
			currentDelay = ((IntegerElement) head).getValue();
		}
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (currentDelay == 1) {
			semanticHelper.semanticBDDAnd(semanticHelper.createEqual(filteredClock,
					getImplicitClock()));
		}
		else {
			semanticHelper.inhibitClock(getImplicitClock());
		}
		semanticHelper
				.registerClockUse(new SolverClock[] { filteredClock, getImplicitClock() });
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (helper.clockHasFired(filteredClock)) {
			if (currentDelay == 1) {
				// Get the next head of the sequence and start a new wait.
				if (sequence.isEmpty())
					terminate(helper);
				else {
					PrimitiveElement head = sequence.nextHead();
					if (head instanceof IntegerElement) {
						currentDelay = ((IntegerElement) head).getValue();
					}
				}
			}
			else {
				currentDelay -= 1;
			}
		}
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		semanticHelper.registerDeathImplication(filteredClock, getImplicitClock());
	}
	
		@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState state =  super.dumpState();
		state.dump(this.currentDelay);
		state.dump(this.sequence);
		return state;
	}

	@Override
	public void restoreState(SerializedConstraintState state) {	
		super.restoreState(state);
		currentDelay = (Integer) state.restore(0);
		sequence = (SolverSequenceElement) state.restore(1);
	}

}