/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.extension.filterby;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.AbstractConcreteMapping;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.BasicType.SolverSequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.AbstractWrappedExpression;
import fr.inria.aoste.timesquare.ccslkernel.solver.extension.ISolverExpressionFactory;

public class FilterByExpressionFactory implements ISolverExpressionFactory {

	@Override
	public boolean canHandle(ExpressionDeclaration declaration,
			ExternalExpressionDefinition definition) {
		if (declaration.getName().compareTo("FilterBy") != 0)
			return false;
		if (declaration.getParameters().size() != 2)
			return false;
		boolean filterByClockFound = false, filterBySeqFound = false;
		for (AbstractEntity parameter : declaration.getParameters()) {
			if (parameter.getName().compareTo("FilterByClock") == 0)
				filterByClockFound = true;
			else if (parameter.getName().compareTo("FilterBySeq") == 0)
				filterBySeqFound = true;
		}
		return filterByClockFound && filterBySeqFound;
	}

	@Override
	public AbstractWrappedExpression createExpression(ExpressionDeclaration declaration,
			ExternalExpressionDefinition definition, SolverClock implicitClock,
			AbstractConcreteMapping<ISolverElement> context) {
		ISolverElement clock = context.getLocalValue("FilterByClock");
		ISolverElement sequence = context.getLocalValue("FilterBySeq");
		if ( ( !(clock instanceof SolverClock) ) || ( !(sequence instanceof SolverSequenceElement) ))
			return null;
		BlackboxFilterby newExpression = new BlackboxFilterby((SolverClock) clock, (SolverSequenceElement) sequence);		
		return newExpression;
	}

}
