/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import org.junit.Before;

import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;

public abstract class SolverTestBase {

	public SolverTestBase() {
	}
	
	protected CCSLKernelSolver solver;
	
	@Before
	public void setUp() throws Exception {
		solver = new CCSLKernelSolver();
	}
	
	protected void printConcretes() {
//		for (ISolverConcrete conc : (solver.getEntryBlock()).getConcretes()) {
//			System.out.println(conc);
//		}
	}

	protected void printBddTerms(StepExecutor se) {
//		for (Term bddTerm : se.getBddTerms()) {
//			System.out.println(bddTerm);
//		}
	}

}
