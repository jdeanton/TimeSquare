/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
//package fr.inria.aoste.timesquare.ccslkernel.solver.tests;
//
//import static org.junit.Assert.*;
//import java.net.URL;
//
//import org.eclipse.core.filesystem.EFS;
//import org.eclipse.core.resources.IFile;
//import org.eclipse.core.resources.IProject;
//import org.eclipse.core.resources.IResource;
//import org.eclipse.core.resources.ResourcesPlugin;
//import org.eclipse.core.runtime.CoreException;
//import org.eclipse.core.runtime.IPath;
//import org.junit.Before;
//import org.junit.Test;
//
//import fr.inria.aoste.timesquare.launcher.debug.model.proxy.CCSLSimulationConfigurationHelper;
//import fr.inria.aoste.timesquare.launcher.debug.model.proxy.ISimulatorControl;
//import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
//import fr.inria.aoste.trace.Trace;
//
//public class TraceBuildingTest {
//
//	private IProject project;
//	private IFile example;
//
//	@Before
//	public void setUp() throws Exception {
//		// create test project
//		project = ResourcesPlugin.getWorkspace().getRoot().getProject("test");
//		if (! project.exists())
//			project.create(null);
//		project.open(null);
//		project.refreshLocal(IResource.DEPTH_INFINITE, null);
//		// copy example file from the plugin data folder
//		copyfileCCSL(project, "data/Alternates.extendedCCSL", "Alternates.extendedCCSL");
//		project.refreshLocal(IResource.DEPTH_INFINITE, null);
//		example = project.getFile("Alternates.extendedCCSL");
//		example.refreshLocal(IResource.DEPTH_INFINITE, null);
//	}
//
//	@Test
//	public void traceTest1() {
//		Trace trace = null;
//		System.out.println("Trace building Test : 1");
//		try {
//			project.open(null);
//			CCSLSimulationConfigurationHelper cLaunch = CCSLSimulationConfigurationHelper.createlaunch(example);
//			cLaunch.setStepNbr(4);
//			ISimulatorControl simControl = cLaunch.launch();
//			simControl.syncRunAllStep();
//			trace = simControl.getTrace();
//		} catch (CoreException e) {
//			AssertionError ae = new AssertionError("Core Exception");
//			ae.initCause(e);
//			throw ae;
//		} catch (InterruptedException e) {
//			System.out.println("Trace building Test : 1. InterruptedException.");
//		}
//		catch (Exception e) {
//			System.out.println("Trace building Test : 2. Exception.");
//		}
//		assertNotNull(trace);
//		assertEquals(4, trace.getLogicalSteps().size());
//		assertEquals(6, trace.getReferences().size());
//	}
//
//
//	private void copyfileCCSL(IProject ip, String source, String dest) throws Exception {
//		URL url = PluginHelpers.getInstallUrl(Activator.getDefault().getBundle(), source);
//		IPath ph = ip.getLocation().append(dest);
//		PluginHelpers.copy(PluginHelpers.getIFileStore(url), PluginHelpers.getIFileStore(ph), EFS.OVERWRITE, null);
//	}
//	
//}
