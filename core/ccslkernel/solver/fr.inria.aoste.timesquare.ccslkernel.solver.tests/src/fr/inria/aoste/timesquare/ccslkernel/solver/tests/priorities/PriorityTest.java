/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests.priorities;

import java.io.IOException;

import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.SolverTestBase;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;

public class PriorityTest extends SolverTestBase {

	@Test
	public void testPriority1() throws RuntimeException {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/priorities/priority1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		
		try {
			solver.loadPriorityModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/priorities/priority1.ccslPriority"));
		} catch (IOException e2) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e2);
			throw ae;
		} catch (UnfoldingException e2) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e2);
			throw ae;
		} catch (SolverException e2) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e2);
			throw ae;
		}
		
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			RuntimeException ae = new RuntimeException("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		SolverClock c1 = solver.findClockByPath("MySpec::main::c1");
		SolverClock c2 = solver.findClockByPath("MySpec::main::c2");

		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 40; step += 1) {
			System.out.println("testPriority1: step " + step);

			try {
				se.executeStep();
			} catch (SimulationException e) {
				RuntimeException pe = new RuntimeException("Solver Priority Exception: " + e.toString());
				pe.initCause(e);
				throw pe;
			}
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			if (!se.clockHasFired(c1)) {
				RuntimeException pe = new RuntimeException("Solver Priority Exception: c1 should tick ! It's the only available solution ");
				throw pe;
			}
			if (se.clockHasFired(c2)) {
				RuntimeException pe = new RuntimeException("Solver Priority Exception: c2 should not tick ! c1 ticks is the only available solution ");
				throw pe;
			}
			
		}
	}

}
