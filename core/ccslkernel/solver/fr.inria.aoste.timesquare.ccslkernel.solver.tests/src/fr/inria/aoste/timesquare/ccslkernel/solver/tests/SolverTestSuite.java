/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.inria.aoste.timesquare.ccslkernel.solver.tests.deathsolver.DeathSolverTest;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.priorities.PriorityTest;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.regression.bug12655;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.regression.bug12744;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.regression.bug12982;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.regression.bug13110;

@RunWith(Suite.class)
@SuiteClasses( { ModelLoadTest.class, ClockCollectionTest.class, DefinitionExpansionTest.class, 
				 UnionTest.class, PrecedesTest.class, DeferTest.class, AlternatesTest.class,
				 WaitTest.class, ConcatTest.class, UpToTest.class, InfTest.class, SupTest.class,
				 SamplingTest.class, PeriodicTest.class, FilterByTest.class,
				 AssertionTest.class, DeathSolverTest.class, PriorityTest.class,
				 bug12655.class, bug12744.class, bug12982.class, bug13110.class } )
				 
public class SolverTestSuite {
}
