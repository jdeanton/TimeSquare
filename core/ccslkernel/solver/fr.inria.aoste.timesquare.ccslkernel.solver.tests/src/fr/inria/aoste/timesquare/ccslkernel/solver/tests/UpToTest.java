/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;
import fr.inria.aoste.timesquare.simulationpolicy.random.RandomSimulationPolicy;

public class UpToTest extends SolverTestBase {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStepExecutionUpTo1() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/UpTo1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// CCSL Specification is C2 = C1 ^ 2 ; C3 = C1 UpTo C2
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		StepExecutor se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionUpTo1: step 1");
		printBddTerms(se);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c3));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c3));
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.getDeadClocks().isEmpty());
		// Step 2
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionUpTo1: step 2");
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(se.getClockEnabledState(c1).equals(ClockTraceState.T) || se.getClockEnabledState(c1).equals(ClockTraceState.X));
		assertTrue(se.getClockEnabledState(c2).equals(ClockTraceState.T) || se.getClockEnabledState(c2).equals(ClockTraceState.X));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c3));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
		assertTrue(solver.getDeadClocks().contains(c2));
		assertTrue(solver.getDeadClocks().contains(c3));
		// Step 3
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionUpTo1: step 3");
		printBddTerms(se);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c3));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.getDeadClocks().contains(c2));
		assertTrue(solver.getDeadClocks().contains(c3));
	}
	
	@Test
	public void testStepExecutionUpTo2() {
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/UpTo2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception: " + e.getMessage());
			ae.initCause(e);
			throw ae;
		}
		SolverClock c1 = solver.findClockByPath("MySpec::main::C1");
		SolverClock c2 = solver.findClockByPath("MySpec::main::C2");
		SolverClock c3 = solver.findClockByPath("MySpec::main::C3");
		Element delay = solver.findElementByPath("MySpec::main::Delay");
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		int c1Ticks = 0;
		assertNotNull(delay);
		assertTrue(delay instanceof IntegerElement);
		int delayValue = ((IntegerElement)delay).getValue();
		for (int step = 1; step <= 50; step += 1) {
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			System.out.println("Step " + step + ": Fired Clocks " + se.getFiredClocks() + " Dead Clocks " + solver.getDeadClocks());
			if (ClockTraceState.T == se.getClockFiredState(c1)) {
				c1Ticks += 1;
				// Delay is 10 in the specification.
				if (c1Ticks == delayValue) {
					assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
				}
				else {
					assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
				}
			}
			if (c1Ticks < delayValue) {
				assertEquals(se.getClockFiredState(c3), se.getClockFiredState(c1));
			}
			else {
				assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
				assertTrue(solver.getDeadClocks().contains(c3));
			}
		}
	}

	@Test
	public void testStepExecutionUpTo3() {
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/UpTo3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception " + e.getMessage());
			ae.initCause(e);
			throw ae;
		}
		SolverClock c1 = solver.findClockByPath("MySpec::main::c1");
		SolverClock kill = solver.findClockByPath("MySpec::main::killClock");
		SolverClock exp0 = solver.findClockByPath("MySpec::main::expression_0");
		SolverClock exp1 = solver.findClockByPath("MySpec::main::expression_1");
		Element delay = solver.findElementByPath("MySpec::main::delay");
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		int c1Ticks = 0;
		assertNotNull(delay);
		assertTrue(delay instanceof IntegerElement);
		int delayValue = ((IntegerElement)delay).getValue();
		for (int step = 1; step <= 50; step += 1) {
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			if (ClockTraceState.T == se.getClockFiredState(c1)) {
				c1Ticks += 1;
				// Delay is 10 in the specification.
				if (c1Ticks == delayValue) {
					assertEquals(ClockTraceState.T, se.getClockFiredState(kill));
				}
				else {
					assertEquals(ClockTraceState.F, se.getClockFiredState(kill));
				}
			}
			if (c1Ticks < delayValue) {
				assertEquals(se.getClockFiredState(exp0), se.getClockFiredState(c1));
				assertEquals(se.getClockFiredState(exp1), se.getClockFiredState(c1));
			}
			else {
				assertEquals(ClockTraceState.F, se.getClockFiredState(exp0));
				assertTrue(solver.getDeadClocks().contains(exp0));
				assertEquals(ClockTraceState.F, se.getClockFiredState(exp1));
				assertTrue(solver.getDeadClocks().contains(exp1));
			}
		}
	}
}
