/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;

public class DeferTest extends SolverTestBase {

	private ArrayList<Integer> rendezVous;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		rendezVous = new ArrayList<Integer>();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStepExecutionDefer1()
	{
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Defer.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		// Spec is res = c1 deferedBy  3,4 (3)  on c2;
		System.out.println("testStepExecutionDefer1:");
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock res = solver.findClock("res");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(res));
	}

	@Test
	public void testStepExecutionDefer2()
	{
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/DeferByOne.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock res = solver.findClock("res");
		// Spec is res = Defer(c1, c2, (1)) Each tick of c1 is supposed to set a rendez-vous after one future tick of
		// c2
		for (int step = 1; step <= 20; step++) {
			//System.out.println("testStepExecutionDefer2, step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e1) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e1);
				throw ae;
			}
			printBddTerms(se);
			if ((! rendezVous.isEmpty()) && (rendezVous.get(0) == 1) && (se.clockHasFired(c2))) {
				assertTrue(se.clockHasFired(res));
			}
			if (se.clockHasFired(res)) {
				assertFalse(rendezVous.isEmpty());
				assertEquals(1, rendezVous.get(0).intValue());
				assertTrue(se.clockHasFired(c2));
			}
			if (se.clockHasFired(c1)) {
				if (se.clockHasFired(c2)) {
					nextDelay(); sched(1);
				}
				else {
					sched(1);
				}
			}
			else if (se.clockHasFired(c2)) {
				nextDelay();
			}
		}
	}

	
	private void nextDelay() {
		if (! rendezVous.isEmpty()) {
			if (rendezVous.get(0) == 1) {
				rendezVous.remove(0);
			}
			else {
				rendezVous.set(0, rendezVous.get(0) - 1);
			}
		}
	}
	
	private void sched(int d) {
		sched(d, 0);
	}
	
	private void sched(int d, int start) {
		if (rendezVous.size() == start) {
			rendezVous.add(start, d);
		}
		else {
			int h = rendezVous.get(start);
			if (d < h) {
				rendezVous.set(start, d);
				rendezVous.add(start + 1, h - d);
			}
			else if (d > h) {
				sched(d - h, start + 1);
			}
		}
	}

}
