/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;

import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;

/**
 * @author nchleq
 *
 */
public class ModelLoadTest {
	
	private CCSLKernelSolver solver;


	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		solver = new CCSLKernelSolver();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver#loadModel(java.lang.String)}.
	 */
	@Test
	public void testLoadModel() {
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/SimplePrecedes.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		ClockConstraintSystem spec = solver.getUnfoldModel().getModel();
		/* The model contained in the test file above is simply a unique relation "C1 precedes C2".
		 * It imports the CCSL kernel only.
		 */
		assertTrue(spec != null);
		assertTrue(spec instanceof ClockConstraintSystem);
		Block bl = spec.getSuperBlock();
		assertTrue(bl != null);
		assertTrue(bl.getName().equals("main"));
		assertTrue(bl.getElements().size() == 2);
		assertTrue(bl.getRelations().size() == 1);
		// must have two bindings in the relation
		Relation relation = bl.getRelations().get(0);
		for (Binding bd : relation.getBindings()) {
			assertTrue((bd.getAbstract().getName().equals("LeftClock"))
					|| (bd.getAbstract().getName().equals("RightClock")));
			assertTrue(bl.getElements().contains(bd.getBindable()));
			if (bd.getAbstract().getName().equals("LeftClock")) {
				assertTrue(bd.getBindable().getName().equals("C1"));
			}
			else if (bd.getAbstract().getName().equals("RightClock")) {
				assertTrue(bd.getBindable().getName().equals("C2"));
			}
		}
		RelationDeclaration decl = relation.getType();
		assertTrue(decl != null);
		for (Binding bd : relation.getBindings()) {
			if (bd.getAbstract().getName().equals("LeftClock")) {
				assertTrue(bd.getAbstract().equals(((Precedence)decl).getLeftEntity()));
			}
			else if (bd.getAbstract().getName().equals("RightClock")) {
				assertTrue(bd.getAbstract().equals(((Precedence)decl).getRightEntity()));
			}
		}
		assertTrue(decl != null);
	}

	@Test
	public void testLoadKernel() {
		try {
			ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib");
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		}
		// Kernel doesnot contain definitions, only declarations
//		catch (SolverException e) {
//			AssertionError ae = new AssertionError("Solver Exception");
//			ae.initCause(e);
//			throw ae;
//		}
//		catch (UnfoldingException e) {
//			AssertionError ae = new AssertionError("Unfolding Exception");
//			ae.initCause(e);
//			throw ae;
//		}
//		assertTrue((solver.getAllExpressionDefinitions() == null)
//					|| (solver.getAllExpressionDefinitions().size() == 0));
//		assertTrue((solver.getAllRelationDefinitions() == null)
//					|| (solver.getAllRelationDefinitions().size() == 0));
	}
	
	@Test
	public void testLoadLibrary() {
		try {
			ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib");
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
//		} catch (SolverException e) {
//			AssertionError ae = new AssertionError("Solver Exception");
//			ae.initCause(e);
//			throw ae;
//		} catch (UnfoldingException e) {
//			AssertionError ae = new AssertionError("Unfolding Exception");
//			ae.initCause(e);
//			throw ae;
		}
//		assertNotNull(solver.getAllExpressionDefinitions());
//		assertNotNull(solver.getAllRelationDefinitions());
//		assertFalse(solver.getAllExpressionDefinitions().isEmpty());
//		assertFalse(solver.getAllRelationDefinitions().isEmpty());
	}
	
}
