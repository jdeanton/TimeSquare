/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.NoBooleanSolution;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;

public class PrecedesTest extends SolverTestBase {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStepExecutionPrecedes1()
	{
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/SimplePrecedes.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionPrecedes1:");
		printBddTerms(se);
//		assertNotNull(se.getBddTerms());
//		assertEquals(2, se.getBddTerms().size());
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		assertNotNull(c1);
		assertNotNull(c2);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
	}
	
	@Test
	public void testStepExecutionPrecedes2()
	{
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/SimplePrecedes.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionPrecedes2: step 1");
		printBddTerms(se);
		// Check that C1 has fired one more time that C2
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		assertNotNull(c1);
		assertNotNull(c2);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		// Step 2
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionPrecedes2: step 2");
		printBddTerms(se);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertTrue((se.getClockFiredState(c1) == ClockTraceState.T) || (se.getClockFiredState(c2) == ClockTraceState.T));
		// Pretend that C2 has fired 3 times, at least one more time than C1 : the C1 < C2 relation
		// is violated.
		c2.tickCount = 3;
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		}
		catch (SimulationException ex) {
			assertTrue(ex instanceof NoBooleanSolution);
		}
		System.out.println("testStepExecutionPrecedes2: step 3");
		printBddTerms(se);
	}

	
	@Test
	public void testStepExecutionPrecedes3()
	{
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/PrecedesU.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionPrecedes3:");
		printBddTerms(se);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(solver.findClock("c1")));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(solver.findClock("c2")));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(solver.findClock("c3")));
		assertEquals(ClockTraceState.T, se.getClockFiredState(solver.findClock("c1")));
		assertEquals(ClockTraceState.F, se.getClockFiredState(solver.findClock("c2")));
		assertEquals(ClockTraceState.F, se.getClockFiredState(solver.findClock("c3")));
	}
	
	@Test
	public void testStepExecutionPrecedes4()
	{
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/PrecedesU3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionPrecedes4:");
		printBddTerms(se);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(solver.findClock("c1")));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(solver.findClock("c2")));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(solver.findClock("c3")));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(solver.findClock("c4")));
		assertEquals(ClockTraceState.T, se.getClockFiredState(solver.findClock("c1")));
		assertEquals(ClockTraceState.F, se.getClockFiredState(solver.findClock("c2")));
		assertEquals(ClockTraceState.F, se.getClockFiredState(solver.findClock("c3")));
		assertEquals(ClockTraceState.F, se.getClockFiredState(solver.findClock("c4")));
	}

}
