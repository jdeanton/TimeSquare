/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests.regression;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.SolverTestBase;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;

public class bug12655 extends SolverTestBase{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void test12655() {
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12655.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// First step of execution.
		StepExecutor se = new StepExecutor(solver);;
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
//		printBooleanExpressions(se);
		printBddTerms(se);
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));

		// Second step of execution.
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));

		// Third step of execution.
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));

		// Fourth step of execution.
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));

	}

}
