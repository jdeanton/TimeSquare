/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.random.RandomSimulationPolicy;

public class AlternatesTest extends SolverTestBase {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testStepExecutionAlternates1()
	{
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		StepExecutor se = new StepExecutor(solver);;
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testAlternates1:");
		printBddTerms(se);
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
	}

	@Test
	@Ignore
	public void testStepExecutionBuiltinAlternates1()
	{
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		StepExecutor se = new StepExecutor(solver);;
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testBuitinAlternates1:");
		printBddTerms(se);
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
	}

	@Test
	public void testStepExecutionAlternates2()
	{
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testAlternates2:");
		printBddTerms(se);
		@SuppressWarnings("unused")
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		@SuppressWarnings("unused")
		SolverClock c3 = solver.findClock("c3");
		SolverClock c4 = solver.findClock("c4");
		//assertEquals(ClockTraceState.X, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		//assertEquals(ClockTraceState.X, se.getClockEnabledState(c3));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c4));
	}

	
	@Test
	@Ignore
	public void testStepExecutionBuiltinAlternates2()
	{
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testBuiltinAlternates2:");
		printBddTerms(se);
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock c3 = solver.findClock("c3");
		SolverClock c4 = solver.findClock("c4");
		assertEquals(ClockTraceState.X, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.X, se.getClockEnabledState(c3));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c4));
	}

	@Test
	public void testStepExecutionAlternates3()
	{
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testAlternates3:");
		printBddTerms(se);
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock c3 = solver.findClock("c3");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c3));
	}

	@Test
	@Ignore
	public void testStepExecutionBuiltinAlternates3()
	{
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testBuiltinAlternates3:");
		printBddTerms(se);
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock c3 = solver.findClock("c3");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c3));
	}

	@Test
	public void testStepExecutionAlternates4()
	{
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testAlternates4: step 1");
		printBddTerms(se);
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		assertNotNull(c1);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		// Second step of execution.
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testAlternates4: step 2");
		printBddTerms(se);
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		// Third Step of execution
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testAlternates4: step 3");
		printBddTerms(se);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
	}

	@Test
	@Ignore
	public void testStepExecutionBuiltinAlternates4()
	{
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testBuiltinAlternates4: step 1");
		printBddTerms(se);
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		assertNotNull(c1);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		// Second step of execution.
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testBuiltinAlternates4: step 2");
		printBddTerms(se);
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		// Third Step of execution
		se = new StepExecutor(solver);
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testBuiltinAlternates4: step 3");
		printBddTerms(se);
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
	}

}
