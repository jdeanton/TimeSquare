/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests.deathsolver;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.ClockDeathSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;

public class DeathSolverTest {

	private SolverClock c1;
	private SolverClock c2;
	private SolverClock c3;
	private SolverClock c4;
	private SolverClock c5;
	private SolverClock c6;
	private SolverClock c7;
	private SolverClock c8;
	private SolverClock c9;
	private ClockDeathSolver solver;
	
	@Before
	public void setUp() throws Exception {
		c1 = new SolverClock(); c1.setName("c1");
		c2 = new SolverClock(); c2.setName("c2");
		c3 = new SolverClock(); c3.setName("c3");
		c4 = new SolverClock(); c4.setName("c4");
		c5 = new SolverClock(); c5.setName("c5");
		c6 = new SolverClock(); c6.setName("c6");
		c7 = new SolverClock(); c7.setName("c7");
		c8 = new SolverClock(); c8.setName("c8");
		c9 = new SolverClock(); c9.setName("c9");
		solver = new ClockDeathSolver();
	}

	@Test
	public void testEqualityPropagation1() {
		/* c1 = c2
		 * c1
		 * check that both c1 and c2 are dead
		 * c3 = c4
		 * check that c1 and c2 are still dead, while c3 and c4 are not */
		assertTrue(solver.getDeadClocks().isEmpty());
		solver.registerEquality(c1, c2);
		assertTrue(solver.getDeadClocks().isEmpty());
		solver.registerDeadClock(c1);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));
		solver.registerEquality(c3, c4);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));
		assertFalse(solver.getDeadClocks().contains(c3));
		assertFalse(solver.getDeadClocks().contains(c4));
	}

	@Test
	public void testEqualityPropagation2() {
		/* c1
		 * c1 = c2
		 * check that c2 is dead
		 */
		assertTrue(solver.getDeadClocks().isEmpty());
		solver.registerDeadClock(c1);
		assertEquals(1, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		solver.registerEquality(c1, c2);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));		
	}

	@Test
	public void testEqualityPropagation3() {
		/* c1
		 * c1 = c2
		 * c3 = c2
		 * both c1, c2 and c3 should be dead
		 */
		assertTrue(solver.getDeadClocks().isEmpty());
		solver.registerDeadClock(c1);
		assertEquals(1, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		solver.registerEquality(c1, c2);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));		
		solver.registerEquality(c3, c2);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));		
		assertTrue(solver.getDeadClocks().contains(c3));		
	}

	@Test
	public void testEqualityPropagation3bis() {
		/* c1
		 * c1 = c2
		 * c2 = c3      inverse of the equality in previous test
		 * both c1, c2 and c3 should be dead
		 */
		assertTrue(solver.getDeadClocks().isEmpty());
		solver.registerDeadClock(c1);
		assertEquals(1, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		solver.registerEquality(c1, c2);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));		
		solver.registerEquality(c2, c3);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));		
		assertTrue(solver.getDeadClocks().contains(c3));		
	}

	@Test
	public void testEqualityPropagation4() {
		/* c1
		 * c1 = c2    an equality class exists with c1 and c2 
		 * c3 = c4    another equality class {c3, c4}
		 * c3 = c2    merges the two classes. All clocks should be dead.
		 */
		assertTrue(solver.getDeadClocks().isEmpty());
		solver.registerDeadClock(c1);
		assertEquals(1, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		solver.registerEquality(c1, c2);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));		

		solver.registerEquality(c3, c4);
		solver.registerEquality(c3, c2);
		assertEquals(4, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));		
		assertTrue(solver.getDeadClocks().contains(c3));		
		assertTrue(solver.getDeadClocks().contains(c4));		
	}

	@Test
	public void testImplication1() {
		/* c1
		 * c1 => c2
		 * both c1 and c2 should be dead */
		solver.registerDeadClock(c1);
		assertEquals(1, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		solver.registerImplication(c1, c2);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));				
	}

	@Test
	public void testImplication2() {
		/* c1
		 * c2 => c1   inverse of the implication in the previous test
		 * only c1 should be dead */
		solver.registerDeadClock(c1);
		assertEquals(1, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		solver.registerImplication(c2, c1);
		assertEquals(1, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertFalse(solver.getDeadClocks().contains(c2));				
	}

	@Test
	public void testImplication3() {
		/* c1 => c2
		 * c1
		 * c1 => c3
		 * all clocks c1, c2, and c3 should be dead */
		solver.registerImplication(c1, c2);
		solver.registerDeadClock(c1);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		solver.registerImplication(c1, c3);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
	}

	@Test
	public void testImplication3bis() {
		/* c1 => c2     same clauses as the previous test, in a different order
		 * c1 => c3
		 * c1
		 * all clocks c1, c2, and c3 should be dead */
		solver.registerImplication(c1, c2);
		assertEquals(0, solver.getDeadClocks().size());
		assertFalse(solver.getDeadClocks().contains(c1));
		assertFalse(solver.getDeadClocks().contains(c2));	
		solver.registerImplication(c1, c3);
		solver.registerDeadClock(c1);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
	}

	@Test
	public void testImplication4() {
		/* c1 => c2
		 * c1 => c3
		 * c3 => c4
		 * c1
		 * all clocks should be dead */
		solver.registerImplication(c1, c2);
		assertEquals(0, solver.getDeadClocks().size());
		assertFalse(solver.getDeadClocks().contains(c1));
		assertFalse(solver.getDeadClocks().contains(c2));	
		solver.registerImplication(c1, c3);
		solver.registerImplication(c3, c4);
		solver.registerDeadClock(c1);
		assertEquals(4, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
		assertTrue(solver.getDeadClocks().contains(c4));		
	}
	
	@Test
	public void testMixed1() {
		/* c1 => c2
		 * c1 = c2
		 * c1
		 */
		solver.registerImplication(c1, c2);
		solver.registerEquality(c1, c2);
		solver.registerDeadClock(c1);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));
	}

	@Test
	public void testMixed2() {
		/* c1 => c2
		 * c1 = c2
		 * c2
		 */
		solver.registerImplication(c1, c2);
		solver.registerEquality(c1, c2);
		solver.registerDeadClock(c2);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));
	}
	
	@Test
	public void testMixed3() {
		/* c1 => c2
		 * c3 = c4
		 * c1
		 * c1 => c3
		 * all clocks should be dead. */
		solver.registerImplication(c1, c2);
		solver.registerEquality(c3, c4);
		solver.registerDeadClock(c1);
		solver.registerImplication(c1, c3);
		assertEquals(4, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
		assertTrue(solver.getDeadClocks().contains(c4));		
	}

	@Test
	public void testMixed3bis() {
		/* c1 => c2
		 * c3 = c4
		 * c1 => c3
		 * c1
		 * all clocks should be dead. */
		solver.registerImplication(c1, c2);
		solver.registerEquality(c3, c4);
		solver.registerImplication(c1, c3);
		solver.registerDeadClock(c1);
		assertEquals(4, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
		assertTrue(solver.getDeadClocks().contains(c4));		
	}

	@Test
	public void testConjunctions1() {
		/*
		 * c1 & c2 => c3
		 * c1
		 * c2
		 * all clocks must be dead */
		solver.registerConjunctionImplication(c1, c2, c3);
		solver.registerDeadClock(c1);
		solver.registerDeadClock(c2);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
	}
	
	@Test
	public void testConjunctions2() {
		/*
		 * c1
		 * c1 & c2 => c3
		 * c2
		 * all clocks must be dead */
		solver.registerDeadClock(c1);
		solver.registerConjunctionImplication(c1, c2, c3);
		solver.registerDeadClock(c2);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
	}

	@Test
	public void testConjunctions3() {
		/*
		 * c2
		 * c1 & c2 => c3
		 * c1
		 * all clocks must be dead */
		solver.registerDeadClock(c2);
		solver.registerConjunctionImplication(c1, c2, c3);
		solver.registerDeadClock(c1);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
	}

	@Test
	public void testConjunctions4() {
		/*
		 * c2
		 * c1
		 * c1 & c2 => c3
		 * all clocks must be dead */
		solver.registerDeadClock(c2);
		solver.registerDeadClock(c1);
		solver.registerConjunctionImplication(c1, c2, c3);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
	}

	@Test
	public void testConjunctions5() {
		/*
		 * c1
		 * c2
		 * c1 & c2 => c3
		 * all clocks must be dead */
		solver.registerDeadClock(c1);
		solver.registerDeadClock(c2);
		solver.registerConjunctionImplication(c1, c2, c3);
		assertEquals(3, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
	}

	@Test
	public void testConjunctions6() {
		/*
		 * c1
		 * c1 & c2 => c3
		 * c2 = c4
		 * c4
		 * all clocks must be dead */
		solver.registerDeadClock(c1);
		solver.registerConjunctionImplication(c1, c2, c3);
		solver.registerEquality(c2, c4);
		solver.registerDeadClock(c4);
		assertEquals(4, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
		assertTrue(solver.getDeadClocks().contains(c4));
	}

	@Test
	public void testConjunctions7() {
		/*
		 * c1
		 * c1 & c2 => c3
		 * c4
		 * c4 => c2
		 * all clocks must be dead */
		solver.registerDeadClock(c1);
		solver.registerConjunctionImplication(c1, c2, c3);
		solver.registerDeadClock(c4);
		solver.registerImplication(c4, c2);
		assertEquals(4, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertTrue(solver.getDeadClocks().contains(c2));	
		assertTrue(solver.getDeadClocks().contains(c3));
		assertTrue(solver.getDeadClocks().contains(c4));
	}

	@Test
	public void testConjunctions8() {
		/*
		 * c1
		 * c1 & c2 => c3
		 * c4
		 * c1 and c4 must be dead */
		solver.registerDeadClock(c1);
		solver.registerConjunctionImplication(c1, c2, c3);
		solver.registerDeadClock(c4);
		assertEquals(2, solver.getDeadClocks().size());
		assertTrue(solver.getDeadClocks().contains(c1));
		assertFalse(solver.getDeadClocks().contains(c2));	
		assertFalse(solver.getDeadClocks().contains(c3));
		assertTrue(solver.getDeadClocks().contains(c4));
	}
	
}
