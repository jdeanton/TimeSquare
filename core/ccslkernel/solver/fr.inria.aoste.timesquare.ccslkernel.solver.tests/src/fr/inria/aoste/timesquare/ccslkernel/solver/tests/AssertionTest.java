/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;

public class AssertionTest extends SolverTestBase {

	@Test
	public void testAssertion1() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		SolverClock ref = solver.findClockByPath("MySpec::main::ref");
		SolverClock expr0 = solver.findClockByPath("MySpec::main::expression_0");
		SolverClock expr1 = solver.findClockByPath("MySpec::main::expression_1");
		Element offsetElement = solver.findElementByPath("MySpec::main::offset");
		Element periodElement0 = solver.findElementByPath("MySpec::main::period_0");
		Element periodElement1 = solver.findElementByPath("MySpec::main::period_1");
		assertNotNull(ref);
		assertNotNull(expr0);
		assertNotNull(expr1);
		assertNotNull(offsetElement);
		assertNotNull(periodElement0);
		assertNotNull(periodElement1);
		assertTrue(offsetElement instanceof IntegerElement);
		assertTrue(periodElement0 instanceof IntegerElement);
		assertTrue(periodElement1 instanceof IntegerElement);
		
		int offset = ((IntegerElement)offsetElement).getValue();
		int period0 = ((IntegerElement)periodElement0).getValue();
		int period1 = ((IntegerElement)periodElement1).getValue();
		int refTicks = 0;
		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 40; step += 1) {
			System.out.println("testAssertion1: step " + step);

			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception: " + e.toString());
				ae.initCause(e);
				throw ae;
			}
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			if (se.clockHasFired(ref)) {
				refTicks += 1;
			}
			boolean assertionViolated = se.isAssertionViolated("MySpec::main::assertion_0");
			if (assertionViolated) {
				assertNotSame(se.getClockFiredState(expr0), se.getClockFiredState(expr1));
				assertTrue((refTicks - offset) % (period0 * period1) != 0);
			}
			else {
				assertEquals(se.getClockFiredState(expr0), se.getClockFiredState(expr1));
			}
		}
	}

	
	@Test
	public void testAssertion2() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		SolverClock ref = solver.findClockByPath("MySpec::main::ref");
//		SolverClock expr0 = solver.findClockByPath("main::expression_0");
//		SolverClock expr1 = solver.findClockByPath("main::expression_1");
		Element offsetElement = solver.findElementByPath("MySpec::main::offset");
		Element periodElement0 = solver.findElementByPath("MySpec::main::period_0");
		Element periodElement1 = solver.findElementByPath("MySpec::main::period_1");
		assertNotNull(ref);
//		assertNotNull(expr0);
//		assertNotNull(expr1);
		assertNotNull(offsetElement);
		assertNotNull(periodElement0);
		assertNotNull(periodElement1);
		assertTrue(offsetElement instanceof IntegerElement);
		assertTrue(periodElement0 instanceof IntegerElement);
		assertTrue(periodElement1 instanceof IntegerElement);
		
		int offset = ((IntegerElement)offsetElement).getValue();
		int period0 = ((IntegerElement)periodElement0).getValue();
		int period1 = ((IntegerElement)periodElement1).getValue();
		int refTicks = 0;
		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 40; step += 1) {
			System.out.println("testAssertion2: step " + step);

			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception: " + e.toString());
				ae.initCause(e);
				throw ae;
			}
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			if (se.clockHasFired(ref)) {
				refTicks += 1;
			}
			boolean assertion0 = se.isAssertionViolated("MySpec::main::assertion_0"); // assert exp0 < exp1
			boolean assertion1 = se.isAssertionViolated("MySpec::main::assertion_1"); // assert exp1 < exp0
			if (refTicks <= offset) {
				assertFalse(assertion0);
				assertFalse(assertion1);
			}
			else if (refTicks == offset + 1) {
				assertTrue(assertion0);
				assertTrue(assertion1);
			}
			else if (refTicks == offset + 2) {
				assertFalse(assertion0);
				assertFalse(assertion1);
			}
			else {
				if (period0 < period1) {
					assertFalse(assertion0);
					assertTrue(assertion1);
				}
				else {
					assertTrue(assertion0);
					assertFalse(assertion1);
				}
			}
		}
	}

	
	
	
	@Test
	public void testAssertion3() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		SolverClock ref = solver.findClockByPath("MySpec::main::ref");
		SolverClock expr0 = solver.findClockByPath("MySpec::main::expression_0");
		SolverClock expr1 = solver.findClockByPath("MySpec::main::expression_1");
		Element offsetElement = solver.findElementByPath("MySpec::main::offset");
		Element periodElement0 = solver.findElementByPath("MySpec::main::period_0");
		Element periodElement1 = solver.findElementByPath("MySpec::main::period_1");
		assertNotNull(ref);
		assertNotNull(expr0);
		assertNotNull(expr1);
		assertNotNull(offsetElement);
		assertNotNull(periodElement0);
		assertNotNull(periodElement1);
		assertTrue(offsetElement instanceof IntegerElement);
		assertTrue(periodElement0 instanceof IntegerElement);
		assertTrue(periodElement1 instanceof IntegerElement);
		
//		int offset = ((IntegerElement)offsetElement).getValue();
//		int period0 = ((IntegerElement)periodElement0).getValue();
//		int period1 = ((IntegerElement)periodElement1).getValue();
		@SuppressWarnings("unused")
		int refTicks = 0;
		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 40; step += 1) {
			System.out.println("testAssertion3: step " + step);

			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception: " + e.toString());
				ae.initCause(e);
				throw ae;
			}
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			if (se.clockHasFired(ref)) {
				refTicks += 1;
			}
			boolean assertion0 = se.isAssertionViolated("MySpec::main::assertion_0");
			boolean assertion1 = se.isAssertionViolated("MySpec::main::assertion_1");
			boolean assertion2 = se.isAssertionViolated("MySpec::main::assertion_2");
			assertFalse(assertion0);
			assertFalse(assertion1);
			if (se.clockHasFired(expr0) && ( ! se.clockHasFired(expr1))) {
				assertTrue(assertion2);
			}
		}
	}
	
	@Test
	public void testAssertion4() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion4.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 40; step += 1) {
			System.out.println("testAssertion4: step " + step);

			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception: " + e.toString());
				ae.initCause(e);
				throw ae;
			}
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			boolean assertion0 = se.isAssertionViolated("MySpec::main::assertion0");
			assertFalse(assertion0);
		}
	}

	@Test
	public void testAssertion5() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Assertion5.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		SolverClock ref = solver.findClockByPath("MySpec::main::ref");
		SolverClock expr0 = solver.findClockByPath("MySpec::main::expression_0");
		SolverClock expr1 = solver.findClockByPath("MySpec::main::expression_1");
		int refTicks = 0;
		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 10; step += 1) {
			System.out.println("testAssertion5: step " + step);

			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception: " + e.toString());
				ae.initCause(e);
				throw ae;
			}
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			if (se.clockHasFired(ref)) {
				refTicks += 1;
			}
			boolean assertion0 = se.isAssertionViolated("MySpec::main::assertion0");
			if (refTicks < 6) {
				assertFalse(assertion0);
			}
			else if (se.clockHasFired(expr0) || se.clockHasFired(expr1)) {
				assertTrue(assertion0);
			}
		}
	}

}
