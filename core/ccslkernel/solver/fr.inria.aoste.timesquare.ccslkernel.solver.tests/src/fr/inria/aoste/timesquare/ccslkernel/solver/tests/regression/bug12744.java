/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests.regression;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.SolverTestBase;

public class bug12744 extends SolverTestBase {

	private ArrayList<Integer> rendezVous;
	@Before
	public void setUp() throws Exception {
		super.setUp();
		rendezVous = new ArrayList<Integer>();
	}
	
	/*
	 * cf. https://gforge.inria.fr/tracker/index.php?func=detail&aid=12744&group_id=2246&atid=7917
	 */

	@Test
	public void test() {
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12744.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		//printConcretes();
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock c3 = solver.findClock("c3");

		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 200; step++) {
			//System.out.println("TestBug12744: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			//printBddTerms(se);
			//System.out.println("Fired clocks: " + se.getFiredClocks());
			if ((! rendezVous.isEmpty()) && (rendezVous.get(0) == 1) && (se.clockHasFired(c2))) {
				assertTrue(se.clockHasFired(c3));
			}
			if (se.clockHasFired(c3)) {
				assertFalse(rendezVous.isEmpty());
				assertEquals(1, rendezVous.get(0).intValue());
				assertTrue(se.clockHasFired(c2));
			}
			if (se.clockHasFired(c1)) {
				if (se.clockHasFired(c2)) {
					nextDelay(); sched(5);
				}
				else {
					sched(5);
				}
			}
			else if (se.clockHasFired(c2)) {
				nextDelay();
			}
		}

	}
	
	private void nextDelay() {
		if (! rendezVous.isEmpty()) {
			if (rendezVous.get(0) == 1) {
				rendezVous.remove(0);
			}
			else {
				rendezVous.set(0, rendezVous.get(0) - 1);
			}
		}
	}
	
	private void sched(int d) {
		sched(d, 0);
	}
	
	private void sched(int d, int start) {
		if (rendezVous.size() == start) {
			rendezVous.add(start, d);
		}
		else {
			int h = rendezVous.get(start);
			if (d < h) {
				rendezVous.set(start, d);
				rendezVous.add(start + 1, h - d);
			}
			else if (d > h) {
				sched(d - h, start + 1);
			}
		}
	}

}
