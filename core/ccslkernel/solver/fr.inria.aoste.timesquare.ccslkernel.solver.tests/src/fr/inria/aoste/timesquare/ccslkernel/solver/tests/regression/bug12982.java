/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests.regression;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.SolverTestBase;

public class bug12982 extends SolverTestBase {

	private ArrayList<Integer> rendezVous;
	@Before
	public void setUp() throws Exception {
		super.setUp();
		rendezVous = new ArrayList<Integer>();
	}

	@Test
	public void test12982_1() {
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12982-1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		//printConcretes();
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock res = solver.findClock("res");

		StepExecutor se = new StepExecutor(solver);
		// defer sequence is (12 , 3) : use a boolean state variable to either sched 12 or 3, state will toggle at each sched
		// operation.
		boolean sched12 = true;
		for (int step = 1; step <= 200; step++) {
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			if ( (! rendezVous.isEmpty()) && (rendezVous.get(0) == 1) && (se.clockHasFired(c2))) {
				assertTrue(se.clockHasFired(res));
			}
			if (se.clockHasFired(res)) {
				assertFalse(rendezVous.isEmpty());
				assertEquals(1, rendezVous.get(0).intValue());
				assertTrue(se.clockHasFired(c2));
			}
			// update local reference of future rendez-vous.
			// defer sequence is (12 , 3)
			if (se.clockHasFired(c1)) {
				if (se.clockHasFired(c2)) {
					nextDelay();
				}
				if (sched12) {
					sched(12); sched12 = false;
				}
				else {
					sched(3); sched12 = true;
				}
			}
			else if (se.clockHasFired(c2)) {
				nextDelay();
			}
		}
	}
	
	@Test
	public void test12982_2() {
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12982-2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		//printConcretes();
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock res = solver.findClock("res");

		StepExecutor se = new StepExecutor(solver);
		// defer sequence is 12 , 3 with no infinite part. Use an int for the state : 0 will sched 12, 1 will
		// sched 3, and 2 will sched nothing (empty sequence).
		int schedState = 0;
		for (int step = 1; step <= 200; step++) {
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			if ( (! rendezVous.isEmpty()) && (rendezVous.get(0) == 1) && (se.clockHasFired(c2))) {
				assertTrue(se.clockHasFired(res));
			}
			if (se.clockHasFired(res)) {
				assertFalse(rendezVous.isEmpty());
				assertEquals(1, rendezVous.get(0).intValue());
				assertTrue(se.clockHasFired(c2));
			}
			// update local reference of future rendez-vous.
			if (se.clockHasFired(c1)) {
				if (se.clockHasFired(c2)) {
					nextDelay();
				}
				switch (schedState) {
				case 0:
					sched(12); schedState += 1; break;
				case 1:
					sched(3); schedState += 1; break;
				default:
					break;
				}
			}
			else if (se.clockHasFired(c2)) {
				nextDelay();
			}

		}
	}
	
	@Test
	public void test12982_3() {
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug12982-3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		//printConcretes();
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock res = solver.findClock("res");

		StepExecutor se = new StepExecutor(solver);
		// defer sequence is 12 (3). Use an int for the state : 0 will sched 12 (one time), 1 will
		// sched 3 forever.
		int schedState = 0;
		for (int step = 1; step <= 200; step++) {
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			if ( (! rendezVous.isEmpty()) && (rendezVous.get(0) == 1) && (se.clockHasFired(c2))) {
				assertTrue(se.clockHasFired(res));
			}
			if (se.clockHasFired(res)) {
				assertFalse(rendezVous.isEmpty());
				assertEquals(1, rendezVous.get(0).intValue());
				assertTrue(se.clockHasFired(c2));
			}
			// update local reference of future rendez-vous.
			if (se.clockHasFired(c1)) {
				if (se.clockHasFired(c2)) {
					nextDelay();
				}
				switch (schedState) {
				case 0:
					sched(12); schedState += 1; break;
				case 1:
					sched(3); break;
				default:
					break;
				}
			}
			else if (se.clockHasFired(c2)) {
				nextDelay();
			}

		}
	}
	
	private void nextDelay() {
		if (! rendezVous.isEmpty()) {
			if (rendezVous.get(0) == 1) {
				rendezVous.remove(0);
			}
			else {
				rendezVous.set(0, rendezVous.get(0) - 1);
			}
		}
	}
	
	private void sched(int d) {
		sched(d, 0);
	}
	
	private void sched(int d, int start) {
		if (rendezVous.size() == start) {
			rendezVous.add(start, d);
		}
		else {
			int h = rendezVous.get(start);
			if (d < h) {
				rendezVous.set(start, d);
				rendezVous.add(start + 1, h - d);
			}
			else if (d > h) {
				sched(d - h, start + 1);
			}
		}
	}

	
}
