/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.random.RandomSimulationPolicy;

public class WaitTest extends SolverTestBase {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStepExecutionWait1() {
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Wait1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// CCSL Specification is C2 = C1 ^ 2
		StepExecutor se = new StepExecutor(solver);
		try {
			solver.initSimulation();
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionWait1: step 1");
		printBddTerms(se);
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		// Step 2
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionWait1: step 2");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.getDeadClocks().contains(c2));
		// Step 3
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionWait1: step 3");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.getDeadClocks().contains(c2));
		// Step 4
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionWait1: step 4");
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.getDeadClocks().contains(c2));
	}

}
