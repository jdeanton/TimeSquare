/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests.regression;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.ccslkernel.solver.tests.SolverTestBase;
import fr.inria.aoste.timesquare.simulationpolicy.random.RandomSimulationPolicy;

public class bug13110 extends SolverTestBase {

	@Test
	public void test() {
		solver.setPolicy(new RandomSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/bug13110.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		printConcretes();
		
		SolverClock tick0 = solver.findClockByPath("MySpec::main::tick0");
		SolverClock bUnionD = solver.findClockByPath("MySpec::main::bUnionD");
		SolverClock tick = solver.findClockByPath("MySpec::main::tick");
		SolverClock expr0 = solver.findClockByPath("MySpec::main::expression_0");
		
		StepExecutor se = new StepExecutor(solver);

		int tickCount = 0;
		int expr0Count = 0;
		for (int step = 1; step <= 50; step += 1) {
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			if (se.clockHasFired(tick)) {
				tickCount += 1;
			}
			assertEquals(se.getClockFiredState(bUnionD), se.getClockFiredState(tick0));
			assertFalse( expr0.isDead() );
			assertFalse(solver.getDeadClocks().contains(expr0));
			if (se.clockHasFired(expr0)) {
				expr0Count += 1;
			}
			// FilterBy sequence is 1,2(3) : as soon as tick has fired at least one time, then
			// expr0 will also tick, ie. it was born.
			if (tickCount > 1) {
				assertTrue(expr0Count > 0);
			}
		}
	}

}
