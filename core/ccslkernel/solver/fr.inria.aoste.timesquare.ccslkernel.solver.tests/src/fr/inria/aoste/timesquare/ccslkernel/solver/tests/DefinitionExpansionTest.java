/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.CCSLModel.SolverBlock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;

public class DefinitionExpansionTest {

	private CCSLKernelSolver solver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		solver = new CCSLKernelSolver();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void expansionTest1() {
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/SimplePrecedes.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverBlock mainBlock = solver.getSolverBlock(solver.getUnfoldModel().getModel().getSuperBlock());
		assertNotNull(mainBlock.getConcretes());
		assertEquals(1, mainBlock.getConcretes().size());
	}

	@Test
	public void expansionTest2() {
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/PrecedesU.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverBlock mainBlock = solver.getSolverBlock(solver.getUnfoldModel().getModel().getSuperBlock());
		assertNotNull(mainBlock.getConcretes());
		assertEquals(2, mainBlock.getConcretes().size());
	}

	@Test
	public void expansionTest3() {
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Defer.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverBlock mainBlock = solver.getSolverBlock(solver.getUnfoldModel().getModel().getSuperBlock());
		assertEquals(2, mainBlock.getConcretes().size());
	}

	@Test
	public void expansionTest4() {
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverBlock mainBlock = solver.getSolverBlock(solver.getUnfoldModel().getModel().getSuperBlock());
		assertEquals(4, mainBlock.getConcretes().size());
		for (RuntimeClock clock : solver.getAllDiscreteClocks()){
			if (clock instanceof ImplicitClock) {
				assertEquals(5, ((ImplicitClock)clock).getInstantiationPath().size());
				ImplicitClock registered = solver.lookupImplicitClockFromPath(((ImplicitClock) clock).getInstantiationPath());
				assertEquals(registered, clock);
			}
		} 
	}

	@Test
	public void expansionTest5() {
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverBlock mainBlock = solver.getSolverBlock(solver.getUnfoldModel().getModel().getSuperBlock());
		assertEquals(8, mainBlock.getConcretes().size());
	}

	@Test
	public void expansionTest6() {
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Alternates3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverBlock mainBlock = solver.getSolverBlock(solver.getUnfoldModel().getModel().getSuperBlock());
		assertEquals(8, mainBlock.getConcretes().size());
	}

	@Test
	public void expansionTest7() {
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Periodic1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverBlock mainBlock = solver.getSolverBlock(solver.getUnfoldModel().getModel().getSuperBlock());
		assertEquals(2, mainBlock.getConcretes().size());
	}

}
