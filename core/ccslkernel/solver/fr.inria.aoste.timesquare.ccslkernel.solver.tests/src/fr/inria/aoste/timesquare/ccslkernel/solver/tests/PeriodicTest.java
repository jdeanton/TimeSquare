/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;

public class PeriodicTest extends SolverTestBase {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void testPeriodic1() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Periodic1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		StepExecutor se = new StepExecutor(solver);
		// Step 1
		System.out.println("testPeriodic1: step 1");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		//Step 2
		System.out.println("testPeriodic1: step 2");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
		// Step 3
		System.out.println("testPeriodic1: step 3");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		// Step 4
		System.out.println("testPeriodic1: step 4");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		// Step 5
		System.out.println("testPeriodic1: step 5");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
		// Step 6
		System.out.println("testPeriodic1: step 6");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		// Step 7
		System.out.println("testPeriodic1: step 7");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		// Step 8
		System.out.println("testPeriodic1: step 8");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
		// Step 9
		System.out.println("testPeriodic1: step 9");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		// Step 10
		System.out.println("testPeriodic1: step 10");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Fired Clocks: " + se.getFiredClocks());
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));

	}

}
