/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;

public class FilterByTest extends SolverTestBase {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	@Ignore
	public void FilterByBuiltinTest1() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/FilterBy1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");

		StepExecutor se = new StepExecutor(solver);

		for (int step = 1; step <= 20; step += 1) {
			System.out.println("testFilterByBuiltin1: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			printBddTerms(se);
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
			assertEquals(ClockTraceState.T, se.getClockFiredState(c1));	
			// Sequence is 2,3 (4).
			if (step == 2 || step == 5 || step == 9 || step == 13 || step == 17) {
				assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
				assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
			}
			else {
				assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
				assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
			}
		}
	}
	
	@Test
	public void FilterByTest1() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/FilterBy1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClockByPath("MySpec::mainBlock::c1");
		SolverClock c2 = solver.findClockByPath("MySpec::mainBlock::c2");

		StepExecutor se = new StepExecutor(solver);

		for (int step = 1; step <= 20; step += 1) {
			System.out.println("testFilterBy1: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			printBddTerms(se);
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
			assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
			// Sequence is 2,3 (4).
			if (step == 2 || step == 5 || step == 9 || step == 13 || step == 17) {
				assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
				assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
			}
			else {
				assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
				assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
			}
		}
	}

	
	@Test
	@Ignore
	public void FilterByBuiltinTest2() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/FilterBy2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		SolverClock c1 = solver.findClockByPath("MySpec::mainBlock::c1");
		SolverClock c2 = solver.findClockByPath("MySpec::mainBlock::c2");

		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 10; step += 1) {
			System.out.println("testFilterByBuiltin2: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			printBddTerms(se);
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
			assertEquals(ClockTraceState.T, se.getClockFiredState(c1));			
			if (step == 2 || step == 5) {
				assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
				assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
			}
			else if (step > 5) {
				assertTrue(c2.isDead());
			}
			else {
				assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
				assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
			}
		}

	}

	
	@Test
	public void FilterByTest2() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/FilterBy2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		SolverClock c1 = solver.findClockByPath("MySpec::mainBlock::c1");
		SolverClock c2 = solver.findClockByPath("MySpec::mainBlock::c2");
		printConcretes();
		StepExecutor se = new StepExecutor(solver);
		for (int step = 1; step <= 10; step += 1) {
			System.out.println("testFilterBy2: step " + step);
			printConcretes();
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception: " + e.toString());
				ae.initCause(e);
				throw ae;
			}
			printConcretes();
			printBddTerms(se);
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
			assertEquals(ClockTraceState.T, se.getClockFiredState(c1));			
			if (step == 2 || step == 5) {
				assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
				assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
			}
			else if (step > 5) {
				assertTrue(c2.isDead());
			}
			else {
				assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
				assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
			}
		}

	}

}
