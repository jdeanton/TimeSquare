/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;

public class InfTest extends SolverTestBase {

	@Test
	public void Inftest1() {
		//solver.setPolicy(new SimFireBalancedRandom());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Inf.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("c1");
		SolverClock c2 = solver.findClock("c2");
		SolverClock c3 = solver.findClock("c3");

		StepExecutor se = new StepExecutor(solver);

		for (int step = 1; step <= 50; step += 1) {
			System.out.println("testInf1: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			printBddTerms(se);
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());

			int max = ( c1.tickCount > c2.tickCount ? c1.tickCount : c2.tickCount );
			assertEquals(c3.tickCount, max);
		}
	}

}
