/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;


import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;

public class SamplingTest extends SolverTestBase {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void TestStepExecutionSampling1() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Sampling1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e1) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e1);
			throw ae;
		}
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		StepExecutor se1 = new StepExecutor(solver);
		try {
			se1.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionSampling1: step 1");
		printBddTerms(se1);
		System.out.println("Fired Clocks: " + se1.getFiredClocks());
		assertEquals(ClockTraceState.T, se1.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se1.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se1.getClockEnabledState(c3));
		assertEquals(ClockTraceState.T, se1.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se1.getClockFiredState(c2));
		assertEquals(ClockTraceState.F, se1.getClockFiredState(c3));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.getDeadClocks().isEmpty());
		// Step 2
		StepExecutor se2 = new StepExecutor(solver);
		try {
			se2.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionSampling1: step 2");
		printBddTerms(se2);
		System.out.println("Fired Clocks: " + se2.getFiredClocks());
		assertEquals(ClockTraceState.T, se2.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se2.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se2.getClockEnabledState(c3));
		assertEquals(ClockTraceState.T, se2.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se2.getClockFiredState(c2));
		assertEquals(ClockTraceState.T, se2.getClockFiredState(c3));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.isDeadClock(c2));
		assertTrue(solver.isDeadClock(c3));
		// Step 3
		StepExecutor se3 = new StepExecutor(solver);
		try {
			se3.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionSampling1: step 3");
		printBddTerms(se3);
		System.out.println("Fired Clocks: " + se3.getFiredClocks());
		assertEquals(ClockTraceState.T, se3.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se3.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se3.getClockEnabledState(c3));
		assertEquals(ClockTraceState.T, se3.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se3.getClockFiredState(c2));
		assertEquals(ClockTraceState.F, se3.getClockFiredState(c3));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.isDeadClock(c2));
		assertTrue(solver.isDeadClock(c3));
	}
	
	@Test
	public void TestStepExecutionSampling2() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Sampling2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		// CCSL Specification is C3 = C1 SampledOn C2 and C2 = C4 ^ 2
		printConcretes();
		@SuppressWarnings("unused")
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		SolverClock c4 = solver.findClock("C4");
		StepExecutor se1 = new StepExecutor(solver);
		int c4ticks = 0;
		for (int step = 1; step <= 10; step += 1) {
			System.out.println("testStepExecutionSampling2: step " + step);
			printBddTerms(se1);
			if (se1.clockHasFired(c4)) {
				c4ticks += 1;
			}
			if (c4ticks < 2) {
				// c3 is not supposed to tick
				assertFalse(se1.clockHasFired(c3));
			}
			else if (c4ticks == 2) {
				// c2 is supposed to fire and also c3. c2 is also dead and implies that c3
				// is also dead (death propagation through the sampling expression). This
				// outcome is independent of c1 behavior (the sampled clock).
				assertTrue(se1.clockHasFired(c2));
				assertTrue(se1.clockHasFired(c3));
				assertTrue(solver.getDeadClocks().contains(c2));
				assertTrue(solver.getDeadClocks().contains(c3));
			}
			else {
				// c2 and c3 are supposed to be dead forever
				assertTrue(solver.getDeadClocks().contains(c2));
				assertTrue(solver.getDeadClocks().contains(c3));
			}
		}
	}
	
	@Test
	public void TestStepExecutionSampling3() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/StrictSampling3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		// CCSL Specification is C3 = C1 SampledOn (c1wait1 union c1wait2) 
		printConcretes();
		@SuppressWarnings("unused")
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		SolverClock c4 = solver.findClock("C4");
		StepExecutor se1 = new StepExecutor(solver);
		int c3ticks = 0;
		for (int step = 1; step <= 10; step += 1) {
			System.out.println("testStepExecutionSampling2: step " + step);
			printBddTerms(se1);
			if (se1.clockHasFired(c3)) {
				c3ticks += 1;
			}
			if (c3ticks < 2) {
				// c3 is not supposed to tick
				assertFalse(se1.clockHasFired(c4));
			}
			else if (c3ticks == 2) {
				// c2 is supposed to fire and also c3. c2 is also dead and implies that c3
				// is also dead (death propagation through the sampling expression). This
				// outcome is independent of c1 behavior (the sampled clock).
				assertTrue(se1.clockHasFired(c4));
				assertTrue(se1.clockHasFired(c3));
				assertTrue(solver.getDeadClocks().contains(c3));
				assertTrue(solver.getDeadClocks().contains(c4));
			}
			else {
				// c2, c3 and c4 are supposed to be dead forever
				assertTrue(solver.getDeadClocks().contains(c4));
				assertTrue(solver.getDeadClocks().contains(c3));
				assertTrue(solver.getDeadClocks().contains(c2));
				}
		}
	}
	
	
	@Test
	public void TestStepExecutionStrictSampling1() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/StrictSampling1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		// CCSL Specification is : C2 = C1 ^ 1 ; C3 = C1 ^ 2 ; C4 = C2 StrictlySampledOn C3
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		SolverClock c4 = solver.findClock("C4");
		StepExecutor se1 = new StepExecutor(solver);
		try {
			se1.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionStrictSampling1: step 1");
		printBddTerms(se1);
		System.out.println("Fired Clocks: " + se1.getFiredClocks());
		assertEquals(ClockTraceState.T, se1.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se1.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se1.getClockEnabledState(c3));
		assertEquals(ClockTraceState.F, se1.getClockEnabledState(c4));
		assertEquals(ClockTraceState.T, se1.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se1.getClockFiredState(c2));
		assertEquals(ClockTraceState.F, se1.getClockFiredState(c3));
		assertEquals(ClockTraceState.F, se1.getClockFiredState(c4));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.isDeadClock(c2));
		// Step 2
		StepExecutor se2 = new StepExecutor(solver);
		try {
			se2.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionStrictSampling1: step 2");
		printBddTerms(se2);
		System.out.println("Fired Clocks: " + se2.getFiredClocks());
		assertEquals(ClockTraceState.T, se2.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se2.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se2.getClockEnabledState(c3));
		assertEquals(ClockTraceState.T, se2.getClockEnabledState(c4));
		assertEquals(ClockTraceState.T, se2.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se2.getClockFiredState(c2));
		assertEquals(ClockTraceState.T, se2.getClockFiredState(c3));
		assertEquals(ClockTraceState.T, se2.getClockFiredState(c4));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.isDeadClock(c2));
		assertTrue(solver.isDeadClock(c3));
		assertTrue(solver.isDeadClock(c4));
		// Step 3
		StepExecutor se3 = new StepExecutor(solver);
		try {
			se3.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionStrictSampling1: step 3");
		printBddTerms(se3);
		System.out.println("Fired Clocks: " + se3.getFiredClocks());
		assertEquals(ClockTraceState.T, se3.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se3.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se3.getClockEnabledState(c3));
		assertEquals(ClockTraceState.F, se3.getClockEnabledState(c4));
		assertEquals(ClockTraceState.T, se3.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se3.getClockFiredState(c2));
		assertEquals(ClockTraceState.F, se3.getClockFiredState(c3));
		assertEquals(ClockTraceState.F, se3.getClockFiredState(c4));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.isDeadClock(c2));
		assertTrue(solver.isDeadClock(c3));
		assertTrue(solver.isDeadClock(c4));
	}

	@Test
	public void TestStepExecutionStrictSampling2() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/StrictSampling2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// CCSL Specification is : C2 = C1 ^ 1 ; C3 = C1 ^ 1 ; C4 = C2 StrictlySampledOn C3
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		SolverClock c4 = solver.findClock("C4");
		StepExecutor se1 = new StepExecutor(solver);
		try {
			se1.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionStrictSampling2: step 1");
		printBddTerms(se1);
		System.out.println("Fired Clocks: " + se1.getFiredClocks());
		assertEquals(ClockTraceState.T, se1.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se1.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se1.getClockEnabledState(c3));
		assertEquals(ClockTraceState.F, se1.getClockEnabledState(c4));
		assertEquals(ClockTraceState.T, se1.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se1.getClockFiredState(c2));
		assertEquals(ClockTraceState.T, se1.getClockFiredState(c3));
		assertEquals(ClockTraceState.F, se1.getClockFiredState(c4));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.isDeadClock(c2));
		assertTrue(solver.isDeadClock(c3));
		assertTrue(solver.isDeadClock(c4));
		// Step 2
		StepExecutor se2 = new StepExecutor(solver);
		try {
			se2.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionStrictSampling2: step 2");
		printBddTerms(se2);
		System.out.println("Fired Clocks: " + se2.getFiredClocks());
		assertEquals(ClockTraceState.T, se2.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se2.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se2.getClockEnabledState(c3));
		assertEquals(ClockTraceState.F, se2.getClockEnabledState(c4));
		assertEquals(ClockTraceState.T, se2.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se2.getClockFiredState(c2));
		assertEquals(ClockTraceState.F, se2.getClockFiredState(c3));
		assertEquals(ClockTraceState.F, se2.getClockFiredState(c4));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.isDeadClock(c2));
		assertTrue(solver.isDeadClock(c3));
		assertTrue(solver.isDeadClock(c4));
		// Step 3
		StepExecutor se3 = new StepExecutor(solver);
		try {
			se3.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		System.out.println("testStepExecutionStrictSampling2: step 3");
		printBddTerms(se3);
		System.out.println("Fired Clocks: " + se3.getFiredClocks());
		assertEquals(ClockTraceState.T, se3.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se3.getClockEnabledState(c2));
		assertEquals(ClockTraceState.F, se3.getClockEnabledState(c3));
		assertEquals(ClockTraceState.F, se3.getClockEnabledState(c4));
		assertEquals(ClockTraceState.T, se3.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se3.getClockFiredState(c2));
		assertEquals(ClockTraceState.F, se3.getClockFiredState(c3));
		assertEquals(ClockTraceState.F, se3.getClockFiredState(c4));
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertTrue(solver.isDeadClock(c2));
		assertTrue(solver.isDeadClock(c3));
		assertTrue(solver.isDeadClock(c4));
	}
	
}
