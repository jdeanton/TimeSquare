/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.ResourceLoader;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy.MaxCardSimulationPolicy;

public class ConcatTest extends SolverTestBase {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStepExecutionConcat1() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat1.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// CCSL specification is C3 = C1 ^ 2 . C2
		// Goal of the test is to check that as soon as C1 has ticked two times, then C3 follows
		// C2.
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		Element delayElement = solver.findElementByPath("TestSpec::main::Delay");
		assertTrue(delayElement instanceof IntegerElement);
		int delay = ((IntegerElement)delayElement).getValue();
		StepExecutor se = new StepExecutor(solver);
		int c1ticks = 0;
		for (int step = 1; step <= 20; step += 1) {
			System.out.println("testStepExecutionConcat1: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			printBddTerms(se);
			if (se.clockHasFired(c1)) {
				c1ticks += 1;
				assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
			}
			if (c1ticks < delay) {
				assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
				assertFalse(se.clockHasFired(c3));
			}
			else if (c1ticks == delay) {
				assertEquals(ClockTraceState.T, se.getClockFiredState(c3));
				assertTrue(se.clockHasFired(c3));
			}
			else {
				assertEquals(se.getClockFiredState(c3), se.getClockFiredState(c2));
			}
		}
	}
	
	
	@Test
	public void testStepExecutionConcat2() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat2.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// Spec is C2 = C1 ^ 2 . C2
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		StepExecutor se = new StepExecutor(solver);
		// Step 1
		System.out.println("testStepExecutionConcat2: step 1");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		// Step 2
		System.out.println("testStepExecutionConcat2: step 2");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
		// Step 3
		System.out.println("testStepExecutionConcat2: step 3");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));
		//Step 4
		System.out.println("testStepExecutionConcat2: step 4");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c2));
		// Step 5
		System.out.println("testStepExecutionConcat2: step 5");
		try {
			se.executeStep();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printBddTerms(se);
		System.out.println("Dead Clocks: " + solver.getDeadClocks());
		assertEquals(ClockTraceState.T, se.getClockEnabledState(c1));
		assertEquals(ClockTraceState.F, se.getClockEnabledState(c2));
		assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
		assertEquals(ClockTraceState.F, se.getClockFiredState(c2));

	}
	
	
	@Test
	public void testStepExecutionConcat3() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat3StartSemantics.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// C3 = C1 ^ 2 . ( C1 ^ 3 . C2 )
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		
		Element delayElement1 = solver.findElementByPath("TestSpec::main::Delay1");
		assertTrue(delayElement1 instanceof IntegerElement);
		int delay1 = ((IntegerElement)delayElement1).getValue();

		Element delayElement2 = solver.findElementByPath("TestSpec::main::Delay2");
		assertTrue(delayElement2 instanceof IntegerElement);
		int delay2 = ((IntegerElement)delayElement2).getValue();

		StepExecutor se = new StepExecutor(solver);
		int c1ticks = 0;
		for (int step = 1; step <= 20; step += 1) {
			System.out.println("testStepExecutionConcat3: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			printBddTerms(se);
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			if (se.clockHasFired(c1)) {
				c1ticks += 1;
				assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
			}
			if (c1ticks < delay1) {
				assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
			}
			else if (c1ticks == delay1) {
				if (se.clockHasFired(c1)) {
					assertEquals(ClockTraceState.T, se.getClockFiredState(c3));
				}
			}
			else {
				if (c1ticks - delay1 < delay2) {
					assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
				}
				else if (c1ticks - delay1 == delay2) {
					assertEquals(ClockTraceState.T, se.getClockFiredState(c3));
				}
				else {
					assertEquals(se.getClockFiredState(c2), se.getClockFiredState(c3));
				}
			}
		}
	}

	@Test
	public void testStepExecutionConcat4() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat3.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// C3 = C1 ^ 2 . ( C1 ^ 3 . C2 )
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		
		Element delayElement1 = solver.findElementByPath("TestSpec::main::Delay1");
		assertTrue(delayElement1 instanceof IntegerElement);
		int delay1 = ((IntegerElement)delayElement1).getValue();

		Element delayElement2 = solver.findElementByPath("TestSpec::main::Delay2");
		assertTrue(delayElement2 instanceof IntegerElement);
		int delay2 = ((IntegerElement)delayElement2).getValue();

		StepExecutor se = new StepExecutor(solver);
		int c1ticks = 0;
		for (int step = 1; step <= 20; step += 1) {
			System.out.println("testStepExecutionConcat3: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			printBddTerms(se);
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			if (se.clockHasFired(c1)) {
				c1ticks += 1;
				assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
			}
			if (c1ticks < delay1) {
				assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
			}
			else if (c1ticks == delay1) {
				if (se.clockHasFired(c1)) {
					assertEquals(ClockTraceState.T, se.getClockFiredState(c3));
				}
			}
			else {
				if (c1ticks == delay2) {
					assertEquals(ClockTraceState.T, se.getClockFiredState(c3));
				}
				else {
					assertEquals(se.getClockFiredState(c2), se.getClockFiredState(c3));
				}
			}
		}
	}
	
	@Test
	public void testStepExecutionConcat5() {
		solver.setPolicy(new MaxCardSimulationPolicy());
		try {
			solver.loadModel(ResourceLoader.INSTANCE.loadPluginResource("/fr.inria.aoste.timesquare.ccslkernel.solver.tests/data/Concat4StartSemantics.extendedCCSL"));
		} catch (IOException e) {
			AssertionError ae = new AssertionError("IO Exception");
			ae.initCause(e);
			throw ae;
		} catch (SolverException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		} catch (UnfoldingException e) {
			AssertionError ae = new AssertionError("Unfolding Exception");
			ae.initCause(e);
			throw ae;
		}
		// C3 = C1 ^ 3 . ( C1 ^ 2 . C2 )
		try {
			solver.initSimulation();
		} catch (SimulationException e) {
			AssertionError ae = new AssertionError("Solver Exception");
			ae.initCause(e);
			throw ae;
		}
		printConcretes();
		SolverClock c1 = solver.findClock("C1");
		SolverClock c2 = solver.findClock("C2");
		SolverClock c3 = solver.findClock("C3");
		
		Element delayElement1 = solver.findElementByPath("TestSpec::main::Delay1");
		assertTrue(delayElement1 instanceof IntegerElement);
		int delay1 = ((IntegerElement)delayElement1).getValue();

		Element delayElement2 = solver.findElementByPath("TestSpec::main::Delay2");
		assertTrue(delayElement2 instanceof IntegerElement);
		int delay2 = ((IntegerElement)delayElement2).getValue();

		StepExecutor se = new StepExecutor(solver);
		int c1ticks = 0;
		for (int step = 1; step <= 20; step += 1) {
			System.out.println("testStepExecutionConcat5: step " + step);
			try {
				se.executeStep();
			} catch (SimulationException e) {
				AssertionError ae = new AssertionError("Solver Exception");
				ae.initCause(e);
				throw ae;
			}
			printBddTerms(se);
			System.out.println("Fired Clocks: " + se.getFiredClocks());
			System.out.println("Dead Clocks: " + solver.getDeadClocks());
			if (se.clockHasFired(c1)) {
				c1ticks += 1;
				assertEquals(ClockTraceState.T, se.getClockFiredState(c1));
			}
			if (c1ticks < delay1) {
				assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
			}
			else if (c1ticks == delay1) {
				if (se.clockHasFired(c1)) {
					assertEquals(ClockTraceState.T, se.getClockFiredState(c3));
				}
			}
			else if (c1ticks < delay1 + delay2) {
				assertEquals(ClockTraceState.F, se.getClockFiredState(c3));
			}
			else if (c1ticks  == delay1 + delay2) {
				assertEquals(ClockTraceState.T, se.getClockFiredState(c3));
			}
			else {
				assertEquals(se.getClockFiredState(c2), se.getClockFiredState(c3));
			}
		}
	}

	
}
