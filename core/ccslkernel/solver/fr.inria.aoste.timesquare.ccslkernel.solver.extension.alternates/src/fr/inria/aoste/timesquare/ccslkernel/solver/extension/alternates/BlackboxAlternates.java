/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.extension.alternates;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.AbstractWrappedRelation;

public class BlackboxAlternates extends AbstractWrappedRelation {

	private enum AlternatesState {
		INHIBIT_LEFT, INHIBIT_RIGHT
	};

	private AlternatesState state;
	private SolverClock leftClock;
	private SolverClock rightClock;

	public BlackboxAlternates() {
		state = AlternatesState.INHIBIT_RIGHT;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		state = AlternatesState.INHIBIT_RIGHT;
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (semanticHelper.isSemanticDone(this)) {
			return;
		}
		semanticHelper.registerSemanticDone(this);

		switch (state) {
		case INHIBIT_LEFT:
			semanticHelper.semanticBDDAnd( semanticHelper.createNot(leftClock));
			break;
		case INHIBIT_RIGHT:
			semanticHelper.semanticBDDAnd( semanticHelper.createNot(rightClock));
			break;
		}
		semanticHelper.registerClockUse(new SolverClock[] { leftClock, rightClock });
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper helper)
			throws SimulationException {
	}

	public void setLeftClock(SolverClock leftClock) {
		this.leftClock = leftClock;
	}

	public void setRightClock(SolverClock rightClock) {
		this.rightClock = rightClock;
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		switch (state) {
		case INHIBIT_LEFT:
			if (helper.clockHasFired(rightClock)) {
				state = AlternatesState.INHIBIT_RIGHT;
			}
			break;
		case INHIBIT_RIGHT:
			if (helper.clockHasFired(leftClock)) {
				state = AlternatesState.INHIBIT_LEFT;
			}
			break;
		}
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState state =  super.dumpState();
		state.dump(this.state.ordinal());
		return state;
	}

	@Override
	public void restoreState(SerializedConstraintState state) {
		super.restoreState(state);
		this.state = AlternatesState.values()[ (Integer)state.restore(0) ];
	}

	@Override
	protected ICCSLConstraint[] getConstraints() {
		return null;
	}

	@Override
	public void assertionSemantic(AbstractSemanticHelper helper) {
		// TODO Auto-generated method stub
		
	}

}
