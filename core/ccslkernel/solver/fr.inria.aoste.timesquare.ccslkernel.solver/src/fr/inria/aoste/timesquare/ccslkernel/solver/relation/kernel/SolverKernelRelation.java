/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelation;

public abstract class SolverKernelRelation extends SolverRelation {

	protected SolverClock leftClock;
	protected SolverClock rightClock;

	public SolverKernelRelation() {
	}

	public SolverClock getLeftClock() {
		return leftClock;
	}

	public void setLeftClock(SolverClock leftClock) {
		this.leftClock = leftClock;
	}

	public SolverClock getRightClock() {
		return rightClock;
	}

	public void setRightClock(SolverClock rightClock) {
		this.rightClock = rightClock;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		leftClock.start(helper);
		rightClock.start(helper);
		super.start(helper);
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		leftClock.semantic(semanticHelper);
		rightClock.semantic(semanticHelper);
		super.semantic(semanticHelper);
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		leftClock.deathSemantic(semanticHelper);
		rightClock.deathSemantic(semanticHelper);
		super.deathSemantic(semanticHelper);
	}
	
	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		leftClock.update(helper);
		rightClock.update(helper);
		super.update(helper);
	}
}
