/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.exception;

public class SolverWrappedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2617419243761055242L;

	public SolverWrappedException() {
		// TODO Auto-generated constructor stub
	}

	public SolverWrappedException(String message) {
		super(message);
	}

	public SolverWrappedException(SolverException cause) {
		super(cause);
	}
	
	public SolverWrappedException(Exception cause) {
		super(cause);
	}

	public SolverWrappedException(String message, SolverException cause) {
		super(message, cause);
	}

}
