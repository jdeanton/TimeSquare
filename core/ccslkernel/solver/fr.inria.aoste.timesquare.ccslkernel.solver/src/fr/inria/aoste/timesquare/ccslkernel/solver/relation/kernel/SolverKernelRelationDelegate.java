/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;

public class SolverKernelRelationDelegate extends SolverKernelRelation {

	private AbstractRuntimeRelation delegate;
	
	public AbstractRuntimeRelation getDelegate() {
		return delegate;
	}

	public void setDelegate(AbstractRuntimeRelation delegate) {
		this.delegate = delegate;
	}

	public SolverKernelRelationDelegate() {
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		super.start(helper);
		if (delegate != null)
			delegate.start(helper);
	}
	
	@Override
	public void semantic(AbstractSemanticHelper helper) throws SimulationException {
		super.semantic(helper);
		if (delegate != null)
			delegate.semantic(helper);
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		super.deathSemantic(semanticHelper);
		if (delegate != null)
			delegate.deathSemantic(semanticHelper);
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		super.update(helper);
		if (delegate != null)
			delegate.update(helper);
	}
		
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		super.terminate(helper);
		if (delegate != null)
			delegate.terminate(helper);
	}
	
	@Override
	public int getAssertionVariable() {
		if (delegate != null)
			return delegate.getAssertionVariable();
		else
			return RuntimeClock.UNALLOCATEDBDDVARIABLE;
	}
	
	@Override
	public void setAssertion(boolean isAssertion) {
		super.setAssertion(isAssertion);
		if (delegate != null)
			delegate.setAssertion(isAssertion);
	}
	
	@Override
	public void setAssertionVariable(int assertionVariableNumber) {
		super.setAssertionVariable(assertionVariableNumber);
		if (delegate != null)
			delegate.setAssertionVariable(assertionVariableNumber);
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		return delegate.dumpState();
	}
	
	@Override
	public void restoreState(SerializedConstraintState state) {
		delegate.restoreState(state);
	}
	
}
