/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.extension;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;

public final class FactoryManager {

	private final List<ISolverExpressionFactory> expressionFactories = new ArrayList<ISolverExpressionFactory>();
	private final List<ISolverRelationFactory> relationFactories = new ArrayList<ISolverRelationFactory>();

	private FactoryManager() {
		expressionFactories.addAll(ExpressionsExtensionManager.getInstance().getFactories());
		relationFactories.addAll(RelationsExtensionManager.getInstance().getFactories());
	}

	// Singleton without Double Checked Locking. See:
	// http://www.cs.umd.edu/~pugh/java/memoryModel/DoubleCheckedLocking.html
	private static class FactoryManagerHolder {
		private final static FactoryManager instance = new FactoryManager();
	}

	static public FactoryManager getInstance() {
		return FactoryManagerHolder.instance;
	}

	public void registerRelationFactory(ISolverRelationFactory factory) {
		synchronized (relationFactories) {
			relationFactories.add(factory);
		}
	}

	public void registerExpressionFactory(ISolverExpressionFactory factory) {
		synchronized (expressionFactories) {
			expressionFactories.add(factory);
		}
	}

	public ISolverExpressionFactory getExpressionFactory(ExpressionDeclaration declaration,
						ExternalExpressionDefinition definition) {
		synchronized (expressionFactories) {
			for (ISolverExpressionFactory factory : expressionFactories) {
				if (factory.canHandle(declaration, definition))
					return factory;
			}
		}
		return null;
	}

	public ISolverRelationFactory getRelationFactory(RelationDeclaration declaration,
						ExternalRelationDefinition definition) {
		synchronized (relationFactories) {
			for (ISolverRelationFactory factory : relationFactories) {
				if (factory.canHandle(declaration, definition)) {
					return factory;
				}
			}
		}
		return null;
	}

}
