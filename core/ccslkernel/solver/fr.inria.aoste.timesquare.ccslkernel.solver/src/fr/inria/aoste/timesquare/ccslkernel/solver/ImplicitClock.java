/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver;

import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IExpressionClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.AbstractRuntimeExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;

public class ImplicitClock extends SolverClock implements IExpressionClock {

	static private int count = 1;
	private AbstractRuntimeExpression entity;

	public ImplicitClock() {
		super();
		init();
	}

	public ImplicitClock(InstantiatedElement element) {
		super(element);
		init();
		setName("IC_" + element.getInstantiationPath().getLast().getName() + element.getId());
	}
	
	private void init() {
		setName("IC_" + count);
		count += 1;
		setDead(false);
		bddVariableNumber = UNALLOCATEDBDDVARIABLE;
	}
	
	@Override
	public AbstractRuntimeExpression getExpression() {
		return entity;
	}

	@Override
	public void setExpression(AbstractRuntimeExpression expression) {
		this.entity = expression;
	}
	
	public InstantiationPath getInstantiationPath() {
		return instantiationPath;
	}

	@Override
	public String toString() {
		return "[" + (getName() != null ? getName() : "") + "/" + tickCount + (isDead() ? "/D" : "") + "]";
	}
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (entity != null) {
			entity.start(helper);
		}
		super.start(helper);
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (entity != null) {
			entity.semantic(semanticHelper);
		}
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (entity != null) {
			entity.deathSemantic(semanticHelper);
		}
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (entity != null) {
			entity.update(helper);
			if (entity.isDead()) {
				setDead(true);
			}
		}
	}

	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		state = ClockState.DEAD;
		if (entity != null) {
			entity.terminate(helper);
		}
	}
	
	@Override
	public boolean isTerminated() {
		if (entity == null)
			return false;
		return entity.isTerminated();
	}
	
}
