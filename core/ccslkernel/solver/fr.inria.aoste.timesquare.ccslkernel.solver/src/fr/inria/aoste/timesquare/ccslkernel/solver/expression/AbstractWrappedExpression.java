/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;

public abstract class AbstractWrappedExpression implements ISolverConcrete {

	private SolverExpressionWrapper wrapper;
	
	public void setWrapper(SolverExpressionWrapper wrapper) {
		this.wrapper = wrapper;
	}
		
	protected ImplicitClock getImplicitClock() {
		return wrapper.getImplicitClock();
	}
	
	@Override
	public abstract void start(AbstractSemanticHelper helper) throws SimulationException;

	@Override
	public abstract void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException;

	@Override
	public abstract void deathSemantic(AbstractSemanticHelper semanticHelper) throws SimulationException;

	@Override
	public abstract void update(AbstractUpdateHelper helper) throws SimulationException;

	@Override
	public final void terminate(AbstractUpdateHelper helper) throws SimulationException {
		wrapper.terminate(helper);
	}

	@Override
	public boolean isTerminated() {
		return wrapper.isTerminated();
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		return new SerializedConstraintState();
	}

	@Override
	public void restoreState(SerializedConstraintState state) {
	}
	
}
