/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.CCSLModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelationWrapper;

public class SolverBlock implements IRuntimeContainer {

	public SolverBlock() {
		super();
		this.concreteClocks = new HashSet<SolverClock>();
		this.denseClocks = new HashSet<SolverClock>();
		this.implicitClocks = new HashSet<ImplicitClock>();
		this.concretes = new HashSet<ISolverConcrete>();
		this.subBlocks = new ArrayList<SolverBlock>();
		
		
	}
	
	private Set<SolverClock> concreteClocks;
	private Set<SolverClock> denseClocks;
	private Set<ImplicitClock> implicitClocks;
	private Set<ISolverConcrete> concretes;
	private List<SolverBlock> subBlocks;
	private Set<SolverBlockTransition> blockTransitions;
	private SolverBlock parent;
	private Block modelBlock;
	private boolean isActive = false;

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Set<SolverBlockTransition> getSolverBlockTransitions() {
		return blockTransitions;
	}
	
	public Block getModelBlock() {
		return modelBlock;
	}

	public void setModelBlock(Block modelBlock) {
		this.modelBlock = modelBlock;
	}

	/**
	 * Returns the set of all concrete discrete clocks declared in the Block
	 * @return
	 */
	public Set<SolverClock> getConcreteClocks() {
		return concreteClocks;
	}

	/**
	 * Return the set of all implicit discrete clocks built from expressions in the Block 
	 * @return
	 */
	public Set<ImplicitClock> getImplicitClocks() {
		return implicitClocks;
	}
	
	public Set<SolverClock> getDenseClocks() {
		return denseClocks;
	}

	public Set<ISolverConcrete> getConcretes() {
		return concretes;
	}

	public List<SolverBlock> getSubBlocks() {
		return subBlocks;
	}
	
	public List<SolverBlock> getActiveSubBlocks() {
		List<SolverBlock> res = new ArrayList<SolverBlock>();
		for(SolverBlock sb : getSubBlocks()){
			if (sb.isActive){
				res.add(sb);
			}
		}
		return res;
	}
	
	public boolean isActiveBlock() {
		return isActive;
	}
	
	public void start(AbstractSemanticHelper semanticHelper) throws SimulationException {
		this.isActive = true;
		for (SolverClock clock : getConcreteClocks()) {
			clock.start(semanticHelper);
		}
		for (ISolverConcrete concrete : concretes) {
			if (concrete instanceof SolverRelation || concrete instanceof SolverRelationWrapper) {
				concrete.start(semanticHelper);
			}
			if (concrete instanceof SolverExpression /*&& ((SolverExpression) concrete).isOrphan()*/) {
				((SolverExpression) concrete).getImplicitClock().start(semanticHelper);
			}
		}
		
	}
	
	/**
	 * first test to allow stopping a block in order to go toward blocks as different modes
	 * @param updateHelper
	 * @throws SimulationException
	 */
	public void terminate(AbstractUpdateHelper updateHelper) throws SimulationException {
		this.isActive = false;
		for (SolverClock clock : getConcreteClocks()) {
			clock.terminate(updateHelper);
		}
		for (ISolverConcrete concrete : concretes) {
			if (concrete instanceof SolverRelation || concrete instanceof SolverRelationWrapper) {
				concrete.terminate(updateHelper);
			}
			if (concrete instanceof SolverExpression /*&& ((SolverExpression) concrete).isOrphan()*/) {
				((SolverExpression) concrete).getImplicitClock().terminate(updateHelper);
			}
		}
	}

	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = (SolverBlock) parent;
		if (parent != null) {
			parent.addContent(this);
		}
	}

	@Override
	public List<IRuntimeContainer> getContents() {
		return Collections.emptyList();
	}

	@Override
	public void addContent(IRuntimeContainer element) {
		if (element instanceof ImplicitClock) {
			implicitClocks.add((ImplicitClock) element);
		}
		else if (element instanceof SolverClock) {
			if (((SolverClock) element).isDense())
				denseClocks.add((SolverClock) element);
			else
				concreteClocks.add((SolverClock) element);
		}
		else if (element instanceof SolverBlock){
			subBlocks.add((SolverBlock)element);
		}
		else if (element instanceof ISolverConcrete) {
			concretes.add((ISolverConcrete) element);
		}
	}


}
