/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.preference.IPreferenceStore;

import fr.inria.aoste.timesquare.ccslkernel.explorer.CCSLConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.explorer.UniqueString;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalExpressionDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExternalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.AbstractConcreteMapping;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationTree;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.BDDHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.BasicType.SolverSequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.CCSLModel.SolverBlock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.CCSLModel.SolverBlockTransition;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverWrappedException;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.UnboundAbstract;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.AbstractWrappedExpression;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.ConditionalExpression;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.RecursiveTailCallJump;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpressionWrapper;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.UserDefinedExpression;
import fr.inria.aoste.timesquare.ccslkernel.solver.extension.FactoryManager;
import fr.inria.aoste.timesquare.ccslkernel.solver.extension.ISolverExpressionFactory;
import fr.inria.aoste.timesquare.ccslkernel.solver.extension.ISolverRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.solver.helpers.SemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.PrioritySolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.SolverPrioritySpecification;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.AbstractWrappedRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.ConditionalRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelationWrapper;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.kernel.SolverKernelRelationDelegate;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.kernel.SolverRuntimeRelationFactory;
import fr.inria.aoste.timesquare.ccslkernel.solver.statistics.SolverRuntimeStats;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.TraceFactory;
import fr.inria.aoste.trace.TracePackage;
import net.sf.javabdd.BuDDyFactory;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class CCSLKernelSolver {
	
	public static boolean runtimeStatsCollection = false;
//	if we want to use block transitions, then we have to put CCSLKernelSolver::clockEqualityOptimize 
//	to false otherwise the equalities are grouped into equivalence classes and it overpass the coincides semantics rule
	public static boolean clockEqualityOptimize = true;
	
	public BuDDyFactory BuDDyFactory;
	public Map<Integer, SolverClock> bddVarToClock;
		
	public Map<Integer, SolverRelation> bddAssertVarToRelation;
	public ArrayList<SolverRelation> assertions;
	public Map<SolverRelation, Integer> assertionsToIndexInSolution;
	protected Map<SolverClock, Integer> clockToIndexInSolution;
	protected Map<Integer, Integer> bddVarToIndexInSolution;
	
	public BDDHelper bddHelper;
	
	private EqualitySolver clockEqualityRegister;
	
	public EqualitySolver getClockEqualityRegister() {
		return clockEqualityRegister;
	}

	public void revertForceClockEffect() throws SimulationException{
		if( currentStepExecutor.semanticBdd_beforeClockForcing == null){
			return;
		}else{
			currentStepExecutor.semanticBdd/*.free();
			_currentStepExecutor.semanticBdd */= currentStepExecutor.semanticBdd_beforeClockForcing;
			currentStepExecutor.semanticBdd_beforeClockForcing = null;
		}
		return;
	}

	/**
	 * TOCHECK: with this method, the bddvariable number changes when restored... what is the impact ?
	 */
	protected void saveBDDBeforeClockForcing() {
			BuDDyFactory.save("saveBDD.txt", currentStepExecutor.semanticBdd);
			currentStepExecutor.semanticBdd_beforeClockForcing = BuDDyFactory.load("saveBDD.txt");
		
		}
	
	public boolean forceClockPresence(SolverClock c){
		if(currentStepExecutor.semanticBdd_beforeClockForcing== null ){
			saveBDDBeforeClockForcing();
		}
		currentStepExecutor.getSemanticBdd().andWith(BuDDyFactory.ithVar(c.bddVariableNumber));
		return true;
	}
	
	public boolean hasSolution(){
//		System.out.println("ici");
		return ! currentStepExecutor.semanticBdd.isZero();
	}

	public boolean forceClockAbscence(SolverClock c){
		if(currentStepExecutor == null) {
			currentStepExecutor = new StepExecutor(this);
		}
		if(currentStepExecutor.semanticBdd_beforeClockForcing== null ){
			saveBDDBeforeClockForcing();
		}
		currentStepExecutor.getSemanticBdd().andWith(BuDDyFactory.nithVar(c.bddVariableNumber));
		return true;
	}
	
	public boolean addClockCoincidence(SolverClock c1, SolverClock c2){
		if(currentStepExecutor == null) {
			currentStepExecutor = new StepExecutor(this);
		}
		if(currentStepExecutor.semanticBdd_beforeClockForcing== null ){
			saveBDDBeforeClockForcing();
		}
		BuDDyBDD semantic = currentStepExecutor.getSemanticHelper().createEqual(c1, c2);
		currentStepExecutor.getSemanticHelper().semanticBDDAnd(semantic);
		return true;
	}
	
	
	
	
	public CCSLKernelSolver() {
		this.clockInstantiationTree = new InstantiationTree<SolverClock>();
		this.concreteInstantiationTree = new InstantiationTree<ISolverConcrete>();
		this.elementInstantiationTree = new InstantiationTree<SolverElement>();
		
		this.bddVarToClock = new HashMap<Integer, SolverClock>();
		this.bddAssertVarToRelation = new HashMap<Integer, SolverRelation>();
		this.assertions = new ArrayList<SolverRelation>();
		this.assertionsToIndexInSolution = new HashMap<SolverRelation, Integer>();
		this.clockToIndexInSolution = new HashMap<SolverClock, Integer>();
		this.bddVarToIndexInSolution = new HashMap<Integer, Integer>();
//		String bddLibName = null;
//		String prefKey = fr.inria.aoste.timesquare.booleanequation.Activator.bdd;
//		IPreferenceStore prefStore = fr.inria.aoste.timesquare.booleanequation.Activator.getDefault().getPreferenceStore();
//		bddLibName = prefStore.getString(prefKey);
		BuDDyFactory = BDDHelper.createFactory();
		this.bddHelper = new BDDHelper(BuDDyFactory);
		if (CCSLKernelSolver.clockEqualityOptimize) {
			this.clockEqualityRegister = new EqualitySolver();
		}
		initTrace();
	}

	private List<SolverBlock> topBlocks = new ArrayList<SolverBlock>();
	
	
	private ResourceSet resourceSet;

	public ResourceSet getResourceSet() {
		return resourceSet;
	}
	
	/**
	 * the specification of the priority relations (a clone of PrioritySpecification with SolverClock instead of Clock
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model
	 */
	private SolverPrioritySpecification _priorities = null;

	private PrioritySolver _prioSolver = null;
	
	public SolverPrioritySpecification getPrioritySpecification() {
		return _priorities;
	}

	/**
	 * initialize the priority relations from a resource. 
	 * WARNING, the resource should contains a PrioritySpecification as the first content (getContent().get(0))
	 * @param resource the resource should contains a PrioritySpecification as the first content (getContent().get(0))
	 * @throws IOException
	 * @throws UnfoldingException
	 * @throws SolverException
	 */
	public void loadPriorityModel(Resource resource) throws IOException, UnfoldingException, SolverException {
		try{
			_priorities = new SolverPrioritySpecification(resource, this);
		}catch (RuntimeException e) {
			_priorities = null; //if priority spec is empty then it is not necessary to use the priority mechanism.
		}
	}

	public void loadPriorityModel(PrioritySpecification model) throws IOException, UnfoldingException, SolverException {
		_priorities = new SolverPrioritySpecification(model, this);
	}
	
	private List<RuntimeClock> allDiscreteClocks;
	private List<SolverClock> allDenseClocks;
	private List<ImplicitClock> allImplicitClocks;
	
	protected UnfoldModel unfoldModel = null;

	public UnfoldModel getUnfoldModel() {
		return unfoldModel;
	}
	
	public ClockConstraintSystem getModel() {
		if (unfoldModel == null)
			return null;
		return unfoldModel.getModel();
	}

	public void loadModel(Resource resource) throws IOException, UnfoldingException, SolverException {
		this.resourceSet = resource.getResourceSet();
		unfoldModel = UnfoldModel.unfoldModels(resourceSet);
		buildBlockHierarchies(unfoldModel);
		postLoadModel();
	}
	
	/**
	 * this method should load uniquely the constraint of the file and the referenced clocks 
	 * from the imported ccsl spec but not the constrained from the imported spec.
	 * @param resource
	 * @throws IOException
	 * @throws UnfoldingException
	 * @throws SolverException
	 */
	public void loadCoordinationModel(Resource resource) throws IOException, UnfoldingException, SolverException {
		this.resourceSet = resource.getResourceSet();
		UnfoldModel.isCoordinationLoad = true;
		unfoldModel = UnfoldModel.unfoldModels(resourceSet);
		buildBlockHierarchies(unfoldModel);
		postLoadModel();
	}


	private void buildBlockHierarchies(UnfoldModel unfoldModel) {
		for (UnfoldModel imported : unfoldModel.getImportedModels()) {
			buildBlockHierarchies(imported);
		}
		SolverBlock entryBlock = buildBlockHierarchy(unfoldModel.getModel().getSuperBlock());
		topBlocks.add(entryBlock);
	}
	

	private void postLoadModel() throws SolverException {
		initializeConcreteElements(unfoldModel);
		createImplicitClocks(unfoldModel);
		buildSolverRelationsAndExpressions(unfoldModel);
		allDiscreteClocks = collectDiscreteClocks();
		allDenseClocks = collectDenseClocks();
		allImplicitClocks = collectImplicitClocks();
		allocateBDDVariablesToClocks();
		allocateBDDVariablesToAssertions();
	}
	
	private HashMap<Block, SolverBlock> blocksMap = new HashMap<Block, SolverBlock>();
	private ArrayList<SolverBlockTransition> allBlockTransitions = new ArrayList<SolverBlockTransition>();

	public ArrayList<SolverBlockTransition> getAllBlockTransitions() {
		return allBlockTransitions;
	}

	private SolverBlock buildBlockHierarchy(Block block) {
		SolverBlock sBlock = new SolverBlock();
		sBlock.setModelBlock(block);
		blocksMap.put(block, sBlock);
		for (Block subBlock : block.getSubBlock()) {
			sBlock.getSubBlocks().add(buildBlockHierarchy(subBlock));
		}
		return sBlock;
	}

	public SolverBlock getSolverBlock(Block block) {
		return blocksMap.get(block);
	}

	public SolverBlock findSolverBlock(List<NamedElement> blockStack) {
		return blocksMap.get(blockStack.get(blockStack.size() - 1));
	}

	public void initSimulation() throws SimulationException {
		if (runtimeStatsCollection)
			SolverRuntimeStats.clearStats();
		for (SolverBlock entryBlock : topBlocks) {
			initBlock(entryBlock);
		}
		if (_priorities != null && _priorities.getPriorityRelations().size() > 0){
			_prioSolver = new PrioritySolver((BuDDyFactory) BuDDyFactory, _priorities, this);
			_prioSolver.propagatePriority();
		}
	}

	public void endSimulation() {
		
		BDDHelper.terminateBDDUsage();
		if (runtimeStatsCollection)
			SolverRuntimeStats.print();
	}

	/**
	 * initialize the content of the block. if initialBlock is defined then only this subblock is initialized. Otherwise, all subBlocks are initialized
	 * @param block
	 * @throws SimulationException
	 */
	private void initBlock(SolverBlock block) throws SimulationException {
		block.start(new SemanticHelper(this, null));
				
		if(block.getModelBlock().getInitialBlock() != null){
			initBlock(getSolverBlock(block.getModelBlock().getInitialBlock()));
		}else{
			for (SolverBlock sub : block.getSubBlocks()) {
				initBlock(sub);
			}
		}
		
		for(BlockTransition t : block.getModelBlock().getBlockTransitions()){
			SolverBlockTransition st = new SolverBlockTransition(t, this);
			allBlockTransitions.add(st);
		}
			
	}

	
	public SolverClock findClock(String clockName) {
		for (SolverBlock entryBlock : topBlocks) {
			SolverClock res = findClockInBlock(clockName, entryBlock);
			if (res != null)
				return res;
		}
		return null;
	}

	public SolverClock findClockByPath(String qualifiedPath) {
		return findClockByPath(qualifiedPath, InstantiationPath.defaultSeparator);
	}

	public SolverClock findClockByPath(String qualifiedPath, String separator) {
		return getClockInstantiationTree().lookupInstance(qualifiedPath, separator);
	}

	private SolverClock findClockInBlock(String clockName, SolverBlock block) {
		for (SolverClock clock : block.getConcreteClocks()) {
			if (clock.getModelClock().getName().equals(clockName)) {
				return clock;
			}
		}
		for (ImplicitClock clock : block.getImplicitClocks()) {
			if (clock.getName().equals(clockName)) {
				return clock;
			}
		}
		for (SolverBlock sub : block.getSubBlocks()) {
			SolverClock res = findClockInBlock(clockName, sub);
			if (res != null) {
				return res;
			}
		}
		return null;
	}

	public Element findElementByPath(String qualifiedPath) {
		return findElementByPath(qualifiedPath, InstantiationPath.defaultSeparator);
	}

	public Element findElementByPath(String qualifiedPath, String separator) {
		SolverElement solverElement = getElementInstantiationTree().lookupInstance(qualifiedPath, separator);
		if (solverElement == null) {
			return null;
		}
		return solverElement.getModelElement();
	}

	private void initializeConcreteElements(UnfoldModel unfoldModel) {
		if (unfoldModel != null) {
			for (InstantiatedElement element : unfoldModel.getInstantiationTree().lookupInstances(null)) {
				if (element.isClock()) {
					SolverClock newClock = createSolverClock(element);
					if (element.isTopLevel()) {
						EObject container = element.getInstantiationPath().getLast().eContainer();
						if (element.getUseCount() == 0) {
							System.out.println("Clock " + element.getQualifiedName() + " is not used.");
						}
						if (container instanceof Block) {
							SolverBlock sBlock = getSolverBlock((Block)container);
							if (newClock.isDense()) {
								sBlock.getDenseClocks().add(newClock);
							}
							else {
								sBlock.getConcreteClocks().add(newClock);
							}
						}
					}
					else {
						InstantiationPath blocks = element.getBlockStack();
						Block last = (Block) blocks.get(blocks.size() - 1);
						if (last != null) {
							SolverBlock sBlock = getSolverBlock(last);
							if (newClock.isDense()) {
								sBlock.getDenseClocks().add(newClock);
							}
							else {
								sBlock.getConcreteClocks().add(newClock);	
							}
						}
					}
				}
				else if (element.isPrimitiveElement()) {
					InstantiationPath path = element.getInstantiationPath();
					SolverPrimitiveElement newElement = new SolverPrimitiveElement(element);
					elementInstantiationTree.storeInstance(path, newElement);
				}
				else if (element.isElement()) {
					Element last = (Element) element.getInstantiationPath().getLast();
					if (last instanceof SequenceElement) {
						SolverSequenceElement newElement = new SolverSequenceElement((SequenceElement) last, element);
						elementInstantiationTree.storeInstance(element.getInstantiationPath(), newElement);
					}
					// TODO Handle other case of Element which are neither Sequence nor Primitive : Record ?
				}
			}
		}
	}

	private void createImplicitClocks(UnfoldModel unfoldModel)
	{
		for (InstantiatedElement element : unfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (element.isExpression()) {
				ImplicitClock iClock = createImplicitClock(element);

				InstantiationPath blocks = element.getBlockStack();
				Block last = (Block) blocks.get(blocks.size() - 1);
				if (last != null) {
					SolverBlock sBlock = getSolverBlock(last);
					sBlock.getImplicitClocks().add(iClock);
				}
			}
		}
	}

	public ArrayList<EObject> getDiscreteClockList() {
		ArrayList<EObject> clockList = new ArrayList<EObject>();
		for (RuntimeClock sclock : getAllDiscreteClocks()) {
			clockList.add(((SolverClock)sclock).getModelClock());
		}
		return clockList;
	}

	public List<RuntimeClock> getAllDiscreteClocks() {
		return allDiscreteClocks;
	}
	
	private List<RuntimeClock> collectDiscreteClocks() {
		List<RuntimeClock> res = new ArrayList<RuntimeClock>();
		for (SolverBlock entryBlock : topBlocks) {
			if (entryBlock != null) {
				res.addAll(getDiscreteClocksInBlock(entryBlock));
			}
		}
		return res;
	}

	private List<RuntimeClock> getDiscreteClocksInBlock(SolverBlock block) {
		ArrayList<RuntimeClock> localClocks;
		localClocks = new ArrayList<RuntimeClock>(block.getConcreteClocks());
		localClocks.addAll(block.getImplicitClocks());
		for (SolverBlock sub : block.getSubBlocks()) {
			List<RuntimeClock> subs = getDiscreteClocksInBlock(sub);
			localClocks.addAll(subs);
		}
		return localClocks;
	}
	
	public List<ImplicitClock> getAllImplicitClocks() {
		return allImplicitClocks;
	}
	
	private List<ImplicitClock> collectImplicitClocks() {
		List<ImplicitClock> res = new ArrayList<ImplicitClock>();
		for (SolverBlock topBlock : topBlocks) {
			res.addAll(getImplicitClocksInBlock(topBlock));
		}
		return res;
	}
	
	private List<ImplicitClock> getImplicitClocksInBlock(SolverBlock block) {
		ArrayList<ImplicitClock> localClocks = new ArrayList<ImplicitClock>(block.getImplicitClocks());
		for (SolverBlock sub : block.getSubBlocks()) {
			localClocks.addAll(getImplicitClocksInBlock(sub));
		}
		return localClocks;
	}
	
	public List<SolverClock> getAllDenseClocks() {
		return allDenseClocks;
	}
	
	private List<SolverClock> collectDenseClocks() {
		List<SolverClock> res = new ArrayList<SolverClock>();
		for (SolverBlock entryBlock : topBlocks) {
			if (entryBlock != null) {
				res.addAll(getDenseClocksInBlock(entryBlock));
			}
		}
		return res;
	}
	
	private List<SolverClock> getDenseClocksInBlock(SolverBlock block) {
		ArrayList<SolverClock> localClocks;
		localClocks = new ArrayList<SolverClock>(block.getDenseClocks());
		for (SolverBlock sub : block.getSubBlocks()) {
			List<SolverClock> subs = getDenseClocksInBlock(sub);
			localClocks.addAll(subs);
		}
		return localClocks;
	}

	private int newBDDVariableNumber(BuDDyFactory factory) {
		int res = factory.varNum();
		factory.setVarNum(res + 1);
		return res;
	}

	private ModelElementReference createModelElementReference(InstantiationPath path) {
		ModelElementReference ref = TracePackage.eINSTANCE.getTraceFactory().createModelElementReference();
		ref.getElementRef().addAll(path);
		return ref;
	}
	
	private void initSolverClock(SolverClock newClock, InstantiationPath path) {
		newClock.traceReference = createModelElementReference(path);
		newClock.setInstantiationPath(path);
		newClock.setModelElement(path.getLast());
		clockInstantiationTree.storeInstance(newClock.getInstantiationPath(), newClock);
		//elementInstantiationTree.storeInstance(newClock.getInstantiationPath(), newClock);
	}
	
	public ImplicitClock createImplicitClock(InstantiatedElement element)
	{
		InstantiationPath instantiationPath = element.getInstantiationPath();
		ImplicitClock newClock = new ImplicitClock(element);
		initSolverClock(newClock, instantiationPath);
		return newClock;
	}

	private SolverClock createSolverClock(InstantiatedElement element) {
		InstantiationPath path = element.getInstantiationPath();
		Clock modelClock = (Clock) path.getLast();
		SolverClock newClock = new SolverClock(element);
		newClock.setName(modelClock.getName());
		newClock.setDense(element.isDenseClock());
		initSolverClock(newClock, path);
		return newClock;
	}
	
	private int solutionIndexCounter = 0;
	private int newSolutionIndexNumber() {
		int res = solutionIndexCounter;
		solutionIndexCounter++;
		return res;
	}
	
	private void allocateBDDVariablesToClocks() {
		for (RuntimeClock clock : getAllDiscreteClocks()) {

			int bddVar = SolverClock.UNALLOCATEDBDDVARIABLE;
			int index = -1;
			
			if (CCSLKernelSolver.clockEqualityOptimize) {
				Set<SolverClock> equalClocks = clockEqualityRegister.getEqualityClass((SolverClock) clock);
				if (equalClocks != null) {
					// There are other clock(s) coincident with this one. Check if any such clock already
					// has a bdd variable asoociated with and reuse this variable number. Same for the
					// indexInSolution number.
					for (SolverClock otherclock : equalClocks) {
						if (otherclock.bddVariableNumber != SolverClock.UNALLOCATEDBDDVARIABLE) {
							bddVar = otherclock.bddVariableNumber;
							index = clockToIndexInSolution.get(otherclock);
							break; // No need to go further in the list
						}
					}
				}
			}
			if (bddVar == SolverClock.UNALLOCATEDBDDVARIABLE) {
				bddVar = newBDDVariableNumber(BuDDyFactory);
				index = newSolutionIndexNumber();
			}			
			clock.bddVariableNumber = bddVar;
			bddVarToClock.put(clock.bddVariableNumber, (SolverClock) clock);
			clockToIndexInSolution.put((SolverClock) clock, index);
			bddVarToIndexInSolution.put(clock.bddVariableNumber, index);
		}
	}
	
	private void allocateBDDVariablesToAssertions() {
		for (SolverRelation relation : assertions) {
			relation.setAssertionVariable( newBDDVariableNumber(BuDDyFactory) );
			bddAssertVarToRelation.put(relation.getAssertionVariable(), relation);
			int index = newSolutionIndexNumber();
			assertionsToIndexInSolution.put(relation, index);
			bddVarToIndexInSolution.put(relation.getAssertionVariable(), index);
		}
	}

	public boolean keepBDDSteps = false;

	public void doSimulation(int nSteps) throws SimulationException {
		for (int stepCount = 0; stepCount < nSteps; stepCount += 1) {
			StepExecutor stepExecutor = new StepExecutor(this);
			stepExecutor.executeStep();
		}
	}

	public LogicalStep doOneSimulationStep() throws SimulationException {
		StepExecutor stepExecutor = new StepExecutor(this);
		stepExecutor.executeStep();
		return stepExecutor.getTraceStep();
	}

	private StepExecutor currentStepExecutor = null;
	
	public StepExecutor getCurrentStepExecutor() {
		return currentStepExecutor;
	}
	
	public void setCurrentStepExecutor(StepExecutor se) {
		currentStepExecutor = se;
	}

	public void clearStepData() {
		if (currentStepExecutor != null) {
			currentStepExecutor.clearStepData();
		}
	}
	
		/**
	 * This method construct the BuDDyBDD that represents the semantics of the step. The result is of all possible logical step can be retrieved by using getAllPossibleLogicalSteps
	 * It creates a new StepExecutor storing the BuDDyBDD
	 * @return a list containing all the possible steps for the next step of the simulation
	 * @throws SimulationException 
	 */
	
	public void constructBDD() throws SimulationException {
		if(currentStepExecutor == null) {
			currentStepExecutor = new StepExecutor(this);
		}
		currentStepExecutor.computePossibleClockStates();
		return;
	}
	
	/**
	 * This method give all solution of the BuDDyBDD in the current step executor. It must be called after constructBDD(). To clean the BuDDyBDD in memory, please call clearBDD()
	 * @return void
	 * @throws SimulationException 
	 */
	public List<LogicalStep> getAllPossibleSteps() throws SimulationException {
		return currentStepExecutor.getAllSolutions();
	}
	
	/**
	 *just clear the BuDDyBDD
	 * @throws SimulationException 
	 */
	public void clearBDD() throws SimulationException {
		if (currentStepExecutor != null) {
			currentStepExecutor.freeAll();
			
		}
		currentStepExecutor = null;
		return;		
	}
	
	
	/**
	 * This method computes the semantics of the step and all the possible solutions for this step. The result is given in a list of LogicalStep. 
	 * Note that these logical steps are partial and the complete logical step, suitable for a trace, will be given only after applying one of these steps
	 * @param stepExecutor (a newly created StepExecutor)
	 * @return a list containing all the possible steps for the next step of the simulation
	 * @throws SimulationException  
	 */
	public List<LogicalStep> computeAndGetPossibleLogicalSteps() throws SimulationException {
		if(currentStepExecutor == null) { //not null if a force has been applied before the step computation to avoid contradiction between priorities and force mechanisms
			currentStepExecutor = new StepExecutor(this);
		}
		return currentStepExecutor.computeAndGetPossibleLogicalSteps();
	}
	
	
	/**
	 * This method just extract the solutions from the 'currentStepBDD' without recomputing the semantic BuDDyBDD. This should be call after a getPossibleLogicalSteps().
	 * if the forceClackAbsence/forceClockPresence has been used, the solution will take that into account 
	 * @return
	 * @throws SimulationException
	 */
	public List<LogicalStep> updatePossibleLogicalSteps() throws SimulationException {
		return currentStepExecutor.getAllSolutions();
	}
	
	/**
	 * this method is strongly associated to {@link #getPossibleLogicalSteps(StepExecutor)}. It asks the chosen simulation policy to choose a Logical in the set returned by the {@link #getPossibleLogicalSteps(StepExecutor)} call.
	 * So it must be called AFTER the already site method and it returns an index representing a specific LogicalStep in the list
	 * @param stepExecutor
	 * @return the chosen logical step
	 */
	public int proposeLogicalStepByIndex(){
		return currentStepExecutor.proposeLogicalStepByIndex();
	}
	
	/**
	 * This method is strongly associated to {@link #getPossibleLogicalSteps(StepExecutor)}.
	 * It must be called after this method. Its goal is to apply the step which is at the index 'chosenLogicalStep' in the collection returned by {@link #getPossibleLogicalSteps(StepExecutor)}.
	 * It rewrites the SOS rules and create the logical step resulting from the simulation step. 
	 * @param stepExecutor
	 * @param chosenLogicalStep
	 * @return the logical step resulting from the simulation
	 * @throws SimulationException  
	 */
	public LogicalStep applyLogicalStepByIndex(int chosenLogicalStep) throws SimulationException {
		currentStepExecutor.applyLogicalStepByIndex(chosenLogicalStep);
		LogicalStep res = currentStepExecutor.getTraceStep();
		currentStepExecutor = null;
		return res;
	}
	
	private void buildSolverRelationsAndExpressions(UnfoldModel unfoldModel) throws SolverException {
		InstantiationPath blockStack = new InstantiationPath();
		blockStack.push(unfoldModel.getModel());
		try {
			buildSolverRelationsAndExpressionsInBlock(unfoldModel, blockStack,
					unfoldModel.getModel().getSuperBlock());
		}
		catch (SolverWrappedException e) {
			throw (SolverException)(e.getCause());
		}
		for (UnfoldModel imported : unfoldModel.getImportedModels()) {
			buildSolverRelationsAndExpressions(imported);
		}
	}

	/* Navigate the block hierarchy of the model, using blockStack as the instantiation path prefix to
	 * find the elements to instantiate.
	 */
	private void buildSolverRelationsAndExpressionsInBlock(UnfoldModel unfoldModel, InstantiationPath blockStack, Block block) throws SolverException, UnboundAbstract {
		blockStack.push(block);
		SolverBlock solverBlock = findSolverBlock(blockStack);
		// Get all toplevel concrete in the current block.
		List<InstantiatedElement> rawList = unfoldModel.getInstantiationTree().lookupInstances(blockStack, 1);
		//		java.util.Collections.sort(rawList, new Comparator<InstantiatedElement>() {
		//			@Override
		//			public int compare(InstantiatedElement o1, InstantiatedElement o2) {
		//				return o1.getInstantiationPath().size() - o2.getInstantiationPath().size();
		//			}
		//		});
		for (InstantiatedElement element : rawList) {
			@SuppressWarnings("unused")
			ISolverConcrete concrete = buildConcrete(element, unfoldModel, solverBlock, solverBlock, true);
			// solverBlock.getConcretes().add(concrete);
		}
		for (Block subBlock : block.getSubBlock()) {
			buildSolverRelationsAndExpressionsInBlock(unfoldModel, blockStack, subBlock);
		}
		blockStack.pop();
	}

	private ISolverConcrete buildConcrete(InstantiatedElement element, UnfoldModel unfoldModel,
			IRuntimeContainer parent,
			SolverBlock solverBlock, boolean storeInBlock)
			throws SolverException {
		InstantiationPath instPath = element.getInstantiationPath();
		if (element.isElement()) {
			ISolverElement existing = elementInstantiationTree.lookupInstance(instPath);
			if (existing != null) {
				return (ISolverConcrete) existing;
			}
			return (ISolverConcrete) buildElement(element, parent, solverBlock, storeInBlock);
		}
		else {
			ISolverConcrete existing = concreteInstantiationTree.lookupInstance(instPath);
			if (existing != null) {
				return existing;
			}
			if (element.isExpression()) {
				return buildExpression(element, unfoldModel, parent, solverBlock, storeInBlock);
			}
			else if (element.isRelation()) {
				return buildRelation(element, unfoldModel, parent, solverBlock, storeInBlock);
			}
			return null;
		}
	}

	private ISolverElement buildElement(InstantiatedElement element, IRuntimeContainer parent, SolverBlock sBlock, boolean storeInBlock) {
		InstantiationPath path = element.getInstantiationPath();
		ISolverElement existing;
		if (element.isClock()) {
			existing = clockInstantiationTree.lookupInstance(path);
		}
		else {
			existing = elementInstantiationTree.lookupInstance(path);
		}
		if (existing != null) {
			return existing;
		}
		if (element.isClock()) {
			if (element.getUseCount() == 0) {
				System.out.println("Clock " + element.getQualifiedName() + " is not used.");
			}
			SolverClock newClock = createSolverClock(element);
			newClock.setParent(parent);
			if (storeInBlock && sBlock != null) {
				if (newClock.isDense()) {
					sBlock.getDenseClocks().add(newClock);
				}
				else {
					sBlock.getConcreteClocks().add(newClock);
				}
			}
			return newClock;
		}
		else if (element.isPrimitiveElement()) {
			PrimitiveElement primElt = (PrimitiveElement) path.getLast();
			SolverPrimitiveElement newElement = new SolverPrimitiveElement(element, primElt);
			elementInstantiationTree.storeInstance(path, newElement);
			return newElement;
		}
		else if (element.isElement()) {
			Element last = (Element) element.getInstantiationPath().getLast();
			if (last instanceof SequenceElement) {
				SolverSequenceElement newElement = new SolverSequenceElement(element, (SequenceElement) last);
				elementInstantiationTree.storeInstance(element.getInstantiationPath(), newElement);
				return newElement;
			}
		}
		return null;
	}

	public ISolverElement buildElement(InstantiatedElement element, IRuntimeContainer parent) {
		return buildElement(element, parent, null, false);
	}

	private SolverExpression buildExpression(InstantiatedElement element, UnfoldModel unfoldModel,
				IRuntimeContainer parent,
				SolverBlock solverBlock, boolean storeInBlock) throws SolverException 
	{
		InstantiationPath instPath = element.getInstantiationPath();
		SolverExpression existing = (SolverExpression) concreteInstantiationTree.lookupInstance(instPath);
		if (existing != null) {
			return existing;
		}
		ImplicitClock implicitClock = (ImplicitClock) getClockInstantiationTree().lookupInstance(instPath);
		if (element.isKernelExpression()) {
			AbstractConcreteMapping<ISolverElement> parentEnv;
			if (parent instanceof SolverExpression) {
				parentEnv = ((SolverExpression) parent).getAbstractMapping();
			}
			else if (parent instanceof SolverRelation) {
				parentEnv = ((SolverRelation) parent).getAbstractMapping();
			}
			else
				parentEnv = new AbstractConcreteMapping<ISolverElement>();
			SolverKernelExpressionBuilder builder = new SolverKernelExpressionBuilder(this, implicitClock, parentEnv);
			SolverExpression newExpr = builder.buildExpression(element);
			AbstractConcreteMapping<ISolverElement> env = buildAbstractMapping(element, unfoldModel, newExpr, solverBlock, storeInBlock);
			newExpr.setImplicitClock(implicitClock);
			newExpr.setInstantiatedElement(element);
			newExpr.setAbstractMapping(env);
			implicitClock.setExpression(newExpr);
			if (storeInBlock) {
				solverBlock.getImplicitClocks().add(implicitClock);
				solverBlock.getConcretes().add(newExpr);
			}
			implicitClock.setParent(parent);
			newExpr.setParent(parent);
			getConcreteInstantiationTree().storeInstance(instPath, newExpr);
			if (element.getUseCount() == 0) {
				newExpr.setOrphan(true);
			}
			return newExpr;
		}
		else if (element.getDefinition() instanceof ExternalExpressionDefinition) {
			return buildExternallyDefinedExpression(element, unfoldModel, parent, solverBlock, storeInBlock);
		}
		else if (element.isTailRecursiveCall()) { // Check recursion
			return buildTailRecursiveCallExpression(element, unfoldModel, parent, solverBlock, storeInBlock);
		}
		else if (element.isConditional()) {
			return buildConditionalExpression(element, unfoldModel, parent, solverBlock, storeInBlock);
		}
		else {
			return buildUserDefinedExpression(element, unfoldModel, parent, solverBlock, storeInBlock);
		}
	}
		
	private SolverExpression buildExternallyDefinedExpression(InstantiatedElement element,
				UnfoldModel unfoldModel, IRuntimeContainer parent,
				SolverBlock solverBlock, boolean storeInBlock) throws SolverException
	{
		ExpressionDeclaration declaration = (ExpressionDeclaration) element.getDeclaration();
		ExternalExpressionDefinition definition = (ExternalExpressionDefinition) element.getDefinition();
		ISolverExpressionFactory factory = FactoryManager.getInstance().getExpressionFactory(declaration, definition);
		if (factory == null) {
			// TODO
		}
		InstantiationPath instPath = element.getInstantiationPath();
		ImplicitClock implicitClock = (ImplicitClock) getClockInstantiationTree().lookupInstance(instPath);
		AbstractConcreteMapping<ISolverElement> context = buildAbstractMapping(element, unfoldModel, parent, solverBlock, storeInBlock);
		
		AbstractWrappedExpression newExpression = factory.createExpression(declaration, definition, implicitClock, context);
		SolverExpressionWrapper wrapper = new SolverExpressionWrapper(newExpression, implicitClock);
		newExpression.setWrapper(wrapper);
		
		wrapper.setImplicitClock(implicitClock);
		implicitClock.setExpression(wrapper);
		wrapper.setInstantiationPath(instPath);
		implicitClock.incRefCount();
		wrapper.setAbstractMapping(context);
		wrapper.setInstantiatedElement(element);
		if (element.getUseCount() == 0) {
			wrapper.setOrphan(true);
		}
		implicitClock.setParent(parent);
		wrapper.setParent(parent);
		if (storeInBlock) {
			solverBlock.getImplicitClocks().add(implicitClock);
			solverBlock.getConcretes().add(wrapper);
		}
		concreteInstantiationTree.storeInstance(instPath, wrapper);
		return wrapper;
	}

	private UserDefinedExpression buildUserDefinedExpression(InstantiatedElement element, UnfoldModel unfoldModel,
					IRuntimeContainer parent, SolverBlock solverBlock, boolean storeInBlock)
			throws SolverException {
		InstantiationPath instPath = element.getInstantiationPath();
		ImplicitClock implicitClock = (ImplicitClock) getClockInstantiationTree().lookupInstance(instPath);
		ExpressionDeclaration declaration = (ExpressionDeclaration) element.getDeclaration();
		UserDefinedExpression topExpression = new UserDefinedExpression(element.getDefinition().getName());
		topExpression.setImplicitClock(implicitClock);
		implicitClock.setExpression(topExpression);
		topExpression.setInstantiationPath(instPath);
		topExpression.setAbstractMapping(buildAbstractMapping(element, unfoldModel, topExpression, solverBlock, storeInBlock));
		InstantiatedElement root = element.getRootExpression();
		InstantiationPath lookupPath = root.getInstantiationPath();
		ImplicitClock rootClock = (ImplicitClock) getClockInstantiationTree().lookupInstance(lookupPath);
		if (rootClock == null) {
			throw new SolverWrappedException(new SolverException("No clock for path: " + lookupPath.toString()));
		}
		topExpression.setRootClock(rootClock);
		rootClock.incRefCount();
		if (CCSLKernelSolver.clockEqualityOptimize) {
			clockEqualityRegister.registerEquality(implicitClock, rootClock);
		}
		topExpression.setInstantiatedElement(element);
		for (AbstractEntity parameter : declaration.getParameters()) {
			InstantiatedElement value = element.resolveAbstractEntity(parameter);
			ISolverElement solverElement = buildElement(value, topExpression, solverBlock, false);
			topExpression.setParameterValue(parameter, solverElement);
		}
		if (element.getUseCount() == 0) {
			topExpression.setOrphan(true);
		}
		topExpression.setParent(parent);
		implicitClock.setParent(parent);
		if (storeInBlock) {
			solverBlock.getImplicitClocks().add(implicitClock);
			solverBlock.getConcretes().add(topExpression);
		}
		concreteInstantiationTree.storeInstance(instPath, topExpression);
		for (InstantiatedElement son : element.getSons()) {
			@SuppressWarnings("unused")
			ISolverConcrete sub = buildConcrete(son, unfoldModel, topExpression, solverBlock, false);
//			topExpression.getSubConcretes().add(sub);
		}
		return topExpression;
	}

	private ConditionalExpression buildConditionalExpression(InstantiatedElement element, UnfoldModel unfoldModel, 
			IRuntimeContainer parent,	
			SolverBlock solverBlock, boolean storeInBlock)
			throws SolverException {
		InstantiationPath instPath = element.getInstantiationPath();
		ImplicitClock implicitClock = (ImplicitClock) getClockInstantiationTree().lookupInstance(instPath);
		ConditionalExpression newExpression = new ConditionalExpression();
		newExpression.setAbstractMapping(buildAbstractMapping(element, unfoldModel, newExpression, solverBlock, storeInBlock));
		newExpression.setInstantiatedElement(element);
		newExpression.setInstantiationPath(new InstantiationPath(instPath));
		newExpression.setImplicitClock(implicitClock);
		implicitClock.setExpression(newExpression);
		concreteInstantiationTree.storeInstance(newExpression.getInstantiationPath(), newExpression);
		if (element.getUseCount() == 0) {
			newExpression.setOrphan(true);
		}
		if (storeInBlock) {
			solverBlock.getConcretes().add(newExpression);
			solverBlock.getImplicitClocks().add(implicitClock);
		}
		newExpression.setParent(parent);
		implicitClock.setParent(parent);
		// build all the sons and build a ConditionalCase for each of them
		for (InstantiatedElement son : element.getSons()) {
			if (son.isConditionCase()) {
				buildConcrete(son, unfoldModel, newExpression, solverBlock, false);
				ConditionalCase newExpCase = new ConditionalCase();
				SolverExpression caseExpr = (SolverExpression) concreteInstantiationTree.lookupInstance(son.getInstantiationPath());
				ImplicitClock iClock = (ImplicitClock) clockInstantiationTree.lookupInstance(son.getInstantiationPath());
				newExpCase.setRootClock(iClock);
				newExpCase.getConcretes().add(caseExpr);
				newExpCase.getImplicitClocks().add(iClock);
				iClock.incRefCount();
				InstantiatedElement test = son.getConditionTest();
				if (test != null) {
					SolverElement testElement = elementInstantiationTree.lookupInstance(test.getInstantiationPath());
					if (testElement == null) {
						testElement = (SolverElement) buildElement(test, newExpression, solverBlock, false);
					}
					newExpCase.setCondition((BooleanExpression) testElement.getModelElement());
					newExpression.getCases().add(newExpCase);
				}
				else {
					// it is the default case
					newExpression.getDefaultCase().add(caseExpr);
				}
			}
			else {
				ISolverConcrete sub = buildConcrete(son, unfoldModel, newExpression, solverBlock, false);
				newExpression.getConcretes().add(sub);
			}
		}
		return newExpression;
	}

	private RecursiveTailCallJump buildTailRecursiveCallExpression(InstantiatedElement element, UnfoldModel unfoldModel,
				IRuntimeContainer parent, SolverBlock solverBlock, boolean storeInBlock) throws SolverException {
		InstantiationPath instPath = element.getInstantiationPath();
		ImplicitClock implicitClock = (ImplicitClock) getClockInstantiationTree().lookupInstance(instPath);
		InstantiationPath lookupPath = new InstantiationPath(instPath);
		lookupPath.pop(); // remove the recursive call
		lookupPath.pop(); // remove the definition -> we have the target
		InstantiatedElement targetElement = unfoldModel.getInstantiationTree().lookupInstance(lookupPath);
		SolverExpression targetExpr = (SolverExpression) concreteInstantiationTree.lookupInstance(lookupPath);
		if (targetExpr == null) {
			buildExpression(targetElement, unfoldModel, parent, solverBlock, storeInBlock);
			targetExpr = (SolverExpression) concreteInstantiationTree.lookupInstance(lookupPath);
		}
		RecursiveTailCallJump newExpression = new RecursiveTailCallJump((Expression) instPath.getLast(), targetExpr);
		newExpression.setInstantiatedElement(element);
		newExpression.setImplicitClock(implicitClock);
		implicitClock.setExpression(newExpression);
		newExpression.setInstantiationPath(instPath);
		newExpression.setAbstractMapping(targetExpr.getAbstractMapping());
		ExpressionDeclaration declaration = (ExpressionDeclaration) element.getDeclaration();
		for (AbstractEntity parameter : declaration.getParameters()) {
			InstantiatedElement value = element.resolveAbstractEntity(parameter);
			ISolverElement solverElement = buildElement(value, newExpression, null, false);
			newExpression.setParameterValue(parameter, solverElement);
		}
		if (storeInBlock) {
			solverBlock.getConcretes().add(newExpression);
			solverBlock.getImplicitClocks().add(implicitClock);
		}
		newExpression.setParent(parent);
		concreteInstantiationTree.storeInstance(instPath, newExpression);
		return newExpression;
	}

	private ISolverConcrete buildRelation(InstantiatedElement relation, UnfoldModel unfoldModel,
				IRuntimeContainer parent, SolverBlock block, boolean storeInBlock)
			throws SolverException {
		InstantiationPath instPath = relation.getInstantiationPath();
		SolverRelation existing = (SolverRelation) concreteInstantiationTree.lookupInstance(instPath);
		if (existing != null) {
			return existing;
		}
		if (relation.isKernelRelation()) {
			KernelRelationDeclaration decl = (KernelRelationDeclaration) relation.getDeclaration();
			SolverKernelRelationDelegate newRelation = new SolverKernelRelationDelegate();
//			SolverKernelRelation newRelation = SolverKernelRelationFactory.INSTANCE.createSolverKernelRelation(decl);
			InstantiatedElement left = relation.resolveAbstractEntity(decl.getLeftEntity());
			InstantiatedElement right = relation.resolveAbstractEntity(decl.getRightEntity());
			SolverClock leftClock = clockInstantiationTree.lookupInstance(left.getInstantiationPath());
			SolverClock rightClock = clockInstantiationTree.lookupInstance(right.getInstantiationPath());
			newRelation.setAbstractMapping(buildAbstractMapping(relation, unfoldModel, newRelation, block, storeInBlock));
			newRelation.setInstantiationPath(new InstantiationPath(instPath));
			newRelation.setInstantiatedElement(relation);
			newRelation.setLeftClock(leftClock);
			newRelation.setRightClock(rightClock);
			leftClock.incRefCount();
			rightClock.incRefCount();
			AbstractRuntimeRelation delegate = SolverRuntimeRelationFactory.INSTANCE.createRuntimeKernelRelation(decl, leftClock, rightClock);
			newRelation.setDelegate(delegate);
			newRelation.setParent(parent);
			if (storeInBlock) {
				block.getConcretes().add(newRelation);
			}
			// All kernel relations have a ModelElementReference, either as a constraint, or as an assertion.
			newRelation.setTraceReference(createModelElementReference(instPath));
			if (relation.isAssertion()) {
				newRelation.setAssertion(true);
				this.assertions.add(newRelation);
			}
			else if (decl instanceof Coincidence && ( ! relation.inConditionalBranch() ) ) {
				if (CCSLKernelSolver.clockEqualityOptimize) {
					clockEqualityRegister.registerEquality(leftClock, rightClock);
				}
			}
			concreteInstantiationTree.storeInstance(instPath, newRelation);
			return newRelation;
		}
		else if (relation.getDefinition() instanceof ExternalRelationDefinition) {
			return buildExternallyDefinedRelation(relation, unfoldModel, parent, block, storeInBlock);
		}
		else if (relation.isConditional()) {
			return buildConditionalRelation(relation, unfoldModel, parent, block, storeInBlock);
		}
		else {
			return buildUserDefinedRelation(relation, unfoldModel, parent, block, storeInBlock);
		}
	}
	
	private ISolverConcrete buildExternallyDefinedRelation(InstantiatedElement relation,
			UnfoldModel unfoldModel, IRuntimeContainer parent, 
			SolverBlock block, boolean storeInBlock) throws SolverException {
		ExternalRelationDefinition definition = (ExternalRelationDefinition) relation.getDefinition();
		RelationDeclaration declaration = (RelationDeclaration) relation.getDeclaration();
		ISolverRelationFactory factory = FactoryManager.getInstance().getRelationFactory(declaration, definition);
		if (factory == null) {
			// TODO			
		}
		SolverRelationWrapper wrapper = new SolverRelationWrapper(null);
		AbstractConcreteMapping<ISolverElement> context = buildAbstractMapping(relation, unfoldModel, wrapper, block, storeInBlock);
		AbstractWrappedRelation newRelation = factory.createRelation(declaration, definition, context);
		wrapper.setWrappedRelation(newRelation);
		wrapper.setAbstractMapping(context);
		wrapper.setInstantiatedElement(relation);
		wrapper.setInstantiationPath(relation.getInstantiationPath());
		if (storeInBlock) {
			block.getConcretes().add(wrapper);
		}
		wrapper.setParent(parent);
		wrapper.setTraceReference(createModelElementReference(relation.getInstantiationPath()));
		concreteInstantiationTree.storeInstance(relation.getInstantiationPath(), wrapper);
		return wrapper;
	}
	
	private ConditionalRelation buildConditionalRelation(InstantiatedElement relation, UnfoldModel unfoldModel,
				IRuntimeContainer parent,
				SolverBlock block, boolean storeInBlock)
			throws SolverException {
		InstantiationPath instPath = relation.getInstantiationPath();
		ConditionalRelation newRelation = new ConditionalRelation();
		newRelation.setInstantiationPath(new InstantiationPath(instPath));
		newRelation.setInstantiatedElement(relation);
		newRelation.setAbstractMapping(buildAbstractMapping(relation, unfoldModel, newRelation, block, storeInBlock));
		if (storeInBlock) {
			block.getConcretes().add(newRelation);
		}
		concreteInstantiationTree.storeInstance(newRelation.getInstantiationPath(), newRelation);

		HashMap<InstantiatedElement, ConditionalCase> subCases = new HashMap<InstantiatedElement, ConditionalCase>();
		for (InstantiatedElement son : relation.getSons()) {
			if (son.isConditionCase()) {
				/* Conditional relation definitions are different than conditional expression definitions : they can have
				 * several relations in the switch case part. However the unfolding build a different InstantiatedElement
				 * for each of these sub-relations, and the only way to rebuild the gathering in a single case is to
				 * check that sub-relations share the same condition test.
				 */
				InstantiatedElement test = son.getConditionTest();
				if (test == null) {
					newRelation.getDefaultCase().add(buildConcrete(son, unfoldModel, newRelation, block, false));
				}
				else {
					ConditionalCase caseObject = null;
					if (subCases.containsKey(test)) {
						caseObject = subCases.get(test);
					}
					else {
						caseObject = new ConditionalCase();
						ISolverElement testElement = buildElement(test, newRelation, block, storeInBlock); 
						caseObject.setCondition((BooleanExpression) testElement.getModelElement());
						newRelation.getCases().add(caseObject);
						subCases.put(test, caseObject);
					}
					caseObject.getConcretes().add(buildConcrete(son, unfoldModel, newRelation, block, false));
				}
			}
			else {
				ISolverConcrete concrete = buildConcrete(son, unfoldModel, block, block, storeInBlock);
				newRelation.getSubConcretes().add(concrete);
			}
		}
		return newRelation;
	}

	private SolverRelation buildUserDefinedRelation(InstantiatedElement relation, UnfoldModel unfoldModel,
				IRuntimeContainer parent, SolverBlock block, boolean storeInBlock)
			throws SolverException {
		InstantiationPath instPath = relation.getInstantiationPath();
		SolverRelation newRelation = new SolverRelation();
		newRelation.setInstantiationPath(new InstantiationPath(instPath));
		newRelation.setInstantiatedElement(relation);
		newRelation.setAbstractMapping(buildAbstractMapping(relation, unfoldModel, newRelation, block, storeInBlock));
		concreteInstantiationTree.storeInstance(newRelation.getInstantiationPath(), newRelation);
		if (storeInBlock) {
			block.getConcretes().add(newRelation);
		}
		newRelation.setParent(parent);
		if (relation.isAssertion()) {
			newRelation.setAssertion(true);
			this.assertions.add(newRelation);
			newRelation.setTraceReference( createModelElementReference(instPath) );
		}
		for (InstantiatedElement son : relation.getSons()) {
			ISolverConcrete concrete = buildConcrete(son, unfoldModel, newRelation, block, storeInBlock);
			newRelation.getSubConcretes().add(concrete);
		}
		return newRelation;
	}

	private AbstractConcreteMapping<ISolverElement> buildAbstractMapping(InstantiatedElement element,
			UnfoldModel unfoldModel, IRuntimeContainer container, SolverBlock block, boolean storeInBlock)
					throws SolverException {
		AbstractConcreteMapping<ISolverElement> acm;
		InstantiatedElement parent = element.getParent();
		if (parent == null) {
			acm = new AbstractConcreteMapping<ISolverElement>();
		}
		else {
			AbstractConcreteMapping<ISolverElement> parentAcm = null;
			ISolverConcrete parentConcrete = buildConcrete(parent, unfoldModel, container, block, storeInBlock);
			if (parentConcrete instanceof SolverRelation) {
				parentAcm = ((SolverRelation)parentConcrete).getAbstractMapping();
			}
			else if (parentConcrete instanceof SolverExpression) {
				parentAcm = ((SolverExpression)parentConcrete).getAbstractMapping();
			}
			if (parentAcm == null) {
				System.out.println();
			}
			acm = new AbstractConcreteMapping<ISolverElement>(parentAcm);
		}
		AbstractConcreteMapping<InstantiatedElement> orig = element.getAbstractMapping();
		for (AbstractEntity abs : orig.keySet()) {
			InstantiatedElement value = orig.resolveAbstractEntity(abs);
			if (value.isElement()) {
				ISolverElement solverElement = buildElement(value, container, block, storeInBlock);
				if (solverElement == null) {
					throw new UnboundAbstract(abs.toString() + " in " + element.toString());
				}
				acm.setLocalValue(abs, solverElement);
			}
			else if (value.isExpression()) {
				ImplicitClock iClock = (ImplicitClock) clockInstantiationTree.lookupInstance(value.getInstantiationPath());
				acm.setLocalValue(abs, iClock);
				iClock.incRefCount();
			}
		}
		return acm;
	}
	
	protected InstantiationTree<ISolverConcrete> concreteInstantiationTree;
	protected InstantiationTree<SolverClock> clockInstantiationTree;
	protected InstantiationTree<SolverElement> elementInstantiationTree;

	public InstantiationTree<ISolverConcrete> getConcreteInstantiationTree() {
		return concreteInstantiationTree;
	}

	public InstantiationTree<SolverClock> getClockInstantiationTree() {
		return clockInstantiationTree;
	}

	public InstantiationTree<SolverElement> getElementInstantiationTree() {
		return elementInstantiationTree;
	}

	public ImplicitClock lookupImplicitClockFromPath(InstantiationPath path) {
		return (ImplicitClock) clockInstantiationTree.lookupInstance(path);
	}

	public List<SolverClock> lookupImplicitClocks(InstantiationPath path) {
		return clockInstantiationTree.lookupInstances(path);
	}

	private SimulationPolicyBase policy;

	public SimulationPolicyBase getPolicy() {
		return policy;
	}

	public void setPolicy(SimulationPolicyBase policy) {
		this.policy = policy;
	}

	private Set<RuntimeClock> deadClocks = new HashSet<RuntimeClock>();

	public Set<RuntimeClock> getDeadClocks() {
		return deadClocks;
	}

	public boolean isDeadClock(SolverClock clock) {
		return deadClocks.contains(clock);
	}
	
	public boolean addDeadClock(RuntimeClock clock) {
		return deadClocks.add(clock);
	}
	
	public boolean removeDeadClock(RuntimeClock clock) {
		return deadClocks.remove(clock);
	}

	private List<Reference> traceReferences;

	public List<Reference> getTraceReferences() {
		return traceReferences;
	}

	public void setTraceReferences(List<Reference> traceReferences) {
		this.traceReferences = traceReferences;
	}

	public ModelElementReference findReference(InstantiatedElement element) {
		return findReference(element.getInstantiationPath());
	}

	public ModelElementReference findReference(InstantiationPath path) {
		ModelElementReference found = null;
		if (traceReferences == null) {
			return null;
		}
		for (Reference ref : traceReferences) {
			if (ref instanceof ModelElementReference) {
				List<EObject> references = ((ModelElementReference) ref).getElementRef();
				if (references.size() == path.size()) {
					boolean allEqual = true;
					Iterator<EObject> refIterator = references.iterator();
					Iterator<NamedElement> pathIterator = path.iterator();
					while (refIterator.hasNext() && pathIterator.hasNext()) {
						EObject reference = refIterator.next();
						NamedElement pathElement = pathIterator.next();
						if (reference != pathElement) {
							allEqual = false;
							break;
						}
					}
					if (allEqual) {
						found = (ModelElementReference) ref;
						break;
					}
				}
			}
		}
		return found;
	}

	private TraceFactory traceFactory;

	public TraceFactory getTraceFactory() {
		return traceFactory;
	}

	private void initTrace() {
		traceFactory = TracePackage.eINSTANCE.getTraceFactory();
	}

	public BuDDyFactory getBuddyFactory() {
		return BuDDyFactory;
	}
	
	public BDDHelper getBddHelper() {
		return bddHelper;
	}
	
	public PrioritySolver getPrioSolver() {
		return _prioSolver;
	}

	public void setPrioSolver(PrioritySolver _prioSolver) {
		this._prioSolver = _prioSolver;
	}

	public List<SolverBlock> getTopBlocks() {
		return topBlocks;
	}
	
	
	
	/**
	 * methods used for state space exploration or for dump/restore functionalities
	 */
	
	public CCSLConstraintState  getCurrentState(){
		 CCSLConstraintState currentState = new CCSLConstraintState();

		 for(ISolverConcrete concrete: clockInstantiationTree.lookupInstances(null)){
			 if (concrete instanceof SolverClock){
				 SolverClock clock = (SolverClock) concrete;
				 currentState.put(clock.getInstantiatedElement().getQualifiedName(), clock.dumpState());
			 }
		 }
		 
		 for(ISolverConcrete concrete: elementInstantiationTree.lookupInstances(null)){
			 if (concrete instanceof SolverSequenceElement){
				 SolverSequenceElement seq = (SolverSequenceElement) concrete;
				 currentState.put(seq.getInstantiatedElement().getQualifiedName(), seq.dumpState());
			 }
		 }
		 
		 List<ISolverConcrete> allConcreteElements = concreteInstantiationTree.lookupInstances(null);
		 for(ISolverConcrete concrete : allConcreteElements){
			 if (concrete instanceof SolverExpression){
				 SolverExpression expr = (SolverExpression) concrete;
				 currentState.put(expr.getInstantiatedElement().getQualifiedName(), expr.dumpState());
			 }
			 if (concrete instanceof SolverRelation){
				 SolverRelation rel = (SolverRelation) concrete;
				 currentState.put(rel.getInstantiatedElement().getQualifiedName(), rel.dumpState());
			 }
			 if (concrete instanceof SolverRelationWrapper){
				 SolverRelationWrapper rel = (SolverRelationWrapper) concrete;
				 currentState.put(rel.getInstantiatedElement().getQualifiedName(), rel.dumpState());
			 }
		 }
		 return currentState;
	}
	
	public void  setCurrentState(CCSLConstraintState newState){
		 	
		 for(String concreteQN : newState.keySet()){
			 ISolverConcrete concrete = concreteInstantiationTree.lookupInstance(concreteQN);
			 if(concrete == null){
				 concrete = elementInstantiationTree.lookupInstance(concreteQN);
			 }
			 if(concrete == null){
				 concrete = clockInstantiationTree.lookupInstance(concreteQN);
			 }
			 SerializedConstraintState concreteNewState = newState.get(concreteQN);
			
			 concrete.restoreState(concreteNewState);
		 }
		 return;
	}
	
	
	
}
