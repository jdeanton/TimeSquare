package fr.inria.aoste.timesquare.ccslkernel.explorer;

import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.statistics.SolverRuntimeStats;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;
import grph.GrphWebNotifications;
import toools.io.file.RegularFile;

public class CCSLKernelExplorer extends CCSLKernelSolver {
	protected IProgressMonitor monitor = null;
	
	public CCSLKernelExplorer(IProgressMonitor monitor) {
		super();
		this.monitor= monitor;
	}

	public StateSpace explore(boolean writeTempsDotFile) throws SimulationException {
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.enterMethod(getClass().getName() + ".explore");

		// remove Grph internet communication
		GrphWebNotifications.enabled = false;

		StateSpace stateSpace = new StateSpace();
		StepExecutor stepExecutor = new StepExecutor(this);
		stepExecutor.inSimulationMode = false;
		ArrayList<CCSLConstraintState> statesToExplore = new ArrayList<CCSLConstraintState>();

		CCSLConstraintState initialState = getCurrentState();
		stateSpace.initialState = initialState;
		stateSpace.addVertex(initialState);
		statesToExplore.add(initialState);
		System.out.println("Exploration of the state space started\n");
		System.out.flush();

		
		for (int j = 0; j < statesToExplore.size(); j++) {
			if (monitor.isCanceled()) {
				stepExecutor.clearStepData();
				stepExecutor.freeAll();
				stepExecutor.clearFiredClock();
				stepExecutor = null;
				return null; //check if stop is required
			}
			monitor.worked(j/statesToExplore.size());
			
			if (writeTempsDotFile){
				RegularFile dotRegFile = new RegularFile("tempStateSpace.dot");
				dotRegFile.setContentAsASCII(stateSpace.getGrph().toDot());
			}

			int stateToExploreSize = statesToExplore.size();
			System.out.println(//"     state #" + j + " on " + stateToExploreSize
					(stateToExploreSize - j) + " states to explore");
			//System.out.flush();
			CCSLConstraintState previousState = new CCSLConstraintState();
			previousState = statesToExplore.get(j);
			setCurrentState(previousState);
			// System.out.println("### previousState: "+previousState.toString());
			
			stepExecutor.clearStepData();
			stepExecutor.stepPreHook();
			stepExecutor.computePossibleClockStates();
		//	stepExecutor.getAllBDDSolutions();
			ArrayList<ClockTraceStateSet> allTransitionsFromCurrentState = stepExecutor
					.getAllBDDSolutions();
			int sizeSoluces = allTransitionsFromCurrentState.size();
		//	System.out.println("        nb_new state " + sizeSoluces);
			allSoluces: for (int i = sizeSoluces - 1; i >= 0; i--) {
				//System.out.print(i+" on "+ sizeSoluces +  " ---- ");
				stepExecutor.clearFiredClock();
				stepExecutor.computePossibleClockStates(); // hack to change
															// constraint state
															// from semantics!!

				ArrayList<String> currentTransition = stepExecutor
						.lightApplyLogicalStepByIndex(i);
				CCSLConstraintState currentState = getCurrentState();
				// System.out.println("  -- currentState: "+currentState.toString());

				boolean stateExists = false;
				for (CCSLConstraintState existingState : stateSpace
						.getVertices()) {
					if (currentState.equals(existingState)) {
						// System.out.println("  "+i+" ----> equals");
						stateExists = true;
						stateSpace.addDirectedSimpleEdge(previousState,
								new StringBuffer(currentTransition.toString()), existingState);
						setCurrentState(previousState);
						continue allSoluces; // states are unique in the
												// stateSpace...
					}
				}
				if (!stateExists) {
					// System.out.println("   ----> NOT found: "+i+" on : "+stepExecutor.allClockTraceStateSet.size());
					statesToExplore.add(currentState);
					stateSpace.addVertex(currentState);
					stateSpace.addDirectedSimpleEdge(previousState,
							new StringBuffer(currentTransition.toString()), currentState);
					setCurrentState(previousState);
				}

			}
			stepExecutor.clearStepData();
			stepExecutor.freeAll();
			

		}
			stepExecutor.clearFiredClock();
			stepExecutor = null;
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.leaveMethod(getClass().getName() + ".explore");

		// String dotGraph = stateSpace.getGrph().toDot();
		// System.out.println("number of schedules:"+
		// stateSpace.getGrph().getAllCycles().size());
		// try {
		// System.out.println("URL="+stateSpace.getGrph().postOnTheWeb());
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// Collection<IntSet> stronglyConnectedVertices =
		// stateSpace.getGrph().getStronglyConnectedComponents();
		// for(Iterator<IntSet> it = stronglyConnectedVertices.iterator();
		// it.hasNext(); ){
		// IntSet is = it.next();
		// if (is.size() <=1){
		// it.remove();
		// }
		// }
		// Grph
		// stateSpace.getGrph().getSubgraphInducedByVertices(stronglyConnectedVertices.iterator().next());
		// System.out.println("partie fortement connexe: "+stronglyConnectedVertices);
		System.out
				.println("******************************************************************************\n state space completed: "
						+ stateSpace.getGrph().getVertices().size()
						+ " states and "
						+ stateSpace.getGrph().getEdges().size()
						+ " transitions \n******************************************************************************\n");
		return stateSpace;
	}

	/**
	 * This method computes the semantics of the step and all the possible
	 * solutions for this step. The result is given in a list of LogicalStep.
	 * Note that these logical steps are partial and the complete logical step,
	 * suitable for a trace, will be given only after applying one of these
	 * steps
	 * 
	 * @param stepExecutor
	 *            (a newly created StepExecutor)
	 * @return a list containing all the possible steps for the next step of the
	 *         simulation
	 * @throws NoBooleanSolution
	 *             (there is a deadlock, no solution can be found)
	 * @throws SolverException
	 *             (shit happened :-/)
	 */
	// public List<LogicalStep> getPossibleLogicalSteps() throws
	// NoBooleanSolution, SolverException{
	// _currentStepExecutor = new StepExecutor(this);
	// return _currentStepExecutor.getPossibleLogicalSteps();
	// }

	/**
	 * this method is strongly associated to
	 * {@link #getPossibleLogicalSteps(StepExecutor)}. It asks the chosen
	 * simulation policy to choose a Logical in the set returned by the
	 * {@link #getPossibleLogicalSteps(StepExecutor)} call. So it must be called
	 * AFTER the already site method and it returns an index representing a
	 * specific LogicalStep in the list
	 * 
	 * @param stepExecutor
	 * @return the chosen logical step
	 */
	// public int proposeLogicalStepByIndex(){
	// return _currentStepExecutor.proposeLogicalStepByIndex();
	// }

	/**
	 * This method is strongly associated to
	 * {@link #getPossibleLogicalSteps(StepExecutor)}. It must be called after
	 * this method. Its goal is to apply the step which is at the index
	 * 'chosenLogicalStep' in the collection returned by
	 * {@link #getPossibleLogicalSteps(StepExecutor)}. It rewrites the SOS rules
	 * and create the logical step resulting from the simulation step.
	 * 
	 * @param stepExecutor
	 * @param chosenLogicalStep
	 * @return the logical step resulting from the simulation
	 * @throws SolverException
	 */
	// public LogicalStep applyLogicalStepByIndex(int chosenLogicalStep) throws
	// SolverException{
	// LogicalStep res =
	// _currentStepExecutor.applyLogicalStepByIndex(chosenLogicalStep);
	// _currentStepExecutor = null;
	// return res;
	// }

}
