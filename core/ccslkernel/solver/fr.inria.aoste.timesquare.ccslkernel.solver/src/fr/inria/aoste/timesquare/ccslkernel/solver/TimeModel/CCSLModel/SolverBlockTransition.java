/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.CCSLModel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.BlockTransition;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement;

public class SolverBlockTransition implements ISolverElement {

	public SolverBlockTransition(BlockTransition modelElement, CCSLKernelSolver ccslKernelSolver) {
		this.modelElement = modelElement;
		this.source = ccslKernelSolver.getSolverBlock(modelElement.getSource());
		this.target = ccslKernelSolver.getSolverBlock(modelElement.getTarget());
		try {
			this.onClockQualifiedName = getQualifiedName(modelElement.getOn());
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		for(RuntimeClock rc : ccslKernelSolver.getAllDiscreteClocks()){
			//warning rc.getQualifiedName is wrong...
			if (rc.getQualifiedName().toString().compareTo(modelElement.getOn().getName()) == 0){//(onClockQualifiedName)){
				this.onClock = rc;
			}
		}
		
	}

	private BlockTransition modelElement = null;
	private SolverBlock source = null;
	private SolverBlock target = null;
	private RuntimeClock onClock = null;
	private String onClockQualifiedName = null;	
	
	public SolverBlock getSource() {
		return source;
	}

	public SolverBlock getTarget() {
		return target;
	}

	public RuntimeClock getOnClock() {
		return onClock;
	}

	@Override
	public InstantiatedElement getInstantiatedElement() {
		throw new RuntimeException("this method (getInstantiatedElement()) makes no sense on:" +this.toString());
	}

	@Override
	public void setInstantiatedElement(InstantiatedElement ie) {
		throw new RuntimeException("this method (setInstantiatedElement()) makes no sense on:" +this.toString());
	}

	@Override
	public NamedElement getModelElement() {
		return modelElement;
	}

	@Override
	public void setModelElement(NamedElement elt) {
		this.modelElement = (BlockTransition) elt;
	}

	@Override
	public Type getElementType() {
		return null;
	}

	public String getQualifiedName(EObject eo) throws NoSuchFieldException {
		String res = "";
		Method methodField = null;
		try {
			methodField = eo.getClass().getMethod("getName");
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}

		if(methodField == null){
			return res;
		}
			try {
				res += (String) methodField.invoke(eo,null);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		if (eo.eContainer() != null) {
			return getQualifiedName(eo.eContainer())+"::"+res;
		}
		else
			return res;
	}
	
	
}
