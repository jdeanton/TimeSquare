/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.ccslkernel.explorer;

/**
 * @author jdeanton
 *
 */
public class UniqueString {
	private static long counter = 0;
	private long uid = 0;
	public String internalString = "";
	
	public UniqueString(String s) {
		uid = counter++;
		internalString = s;
	}
	
	public String toString(){
		return internalString;
	}
}
