/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.BasicType;

import java.util.Iterator;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypeFactory;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceType;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.solver.SolverElement;

public class SolverSequenceElement extends SolverElement {

	private int finitePartIndex;
	public void setFinitePartIndex(int finitePartIndex) {
		this.finitePartIndex = finitePartIndex;
	}

	public void setNonFinitePartIndex(int nonFinitePartIndex) {
		this.nonFinitePartIndex = nonFinitePartIndex;
	}

	public int getFinitePartIndex() {
		return finitePartIndex;
	}

	public int getNonFinitePartIndex() {
		return nonFinitePartIndex;
	}

	private int nonFinitePartIndex;
	
	public SolverSequenceElement(SequenceElement seq, InstantiatedElement ie) {
		super(seq);
		this.setInstantiatedElement(ie);
		this.finitePartIndex = 0;
		this.nonFinitePartIndex = 0;
	}
	
	public SolverSequenceElement(SolverSequenceElement seq, InstantiatedElement ie) {
		super(seq.getSequenceElement());
		this.setInstantiatedElement(ie);
		this.finitePartIndex = seq.finitePartIndex;
		this.nonFinitePartIndex = seq.nonFinitePartIndex;
	}
	
	public SolverSequenceElement(InstantiatedElement element, SequenceElement seq) {
		super(element, seq);
		this.finitePartIndex = 0;
		this.nonFinitePartIndex = 0;
	}
	
	public SequenceElement getSequenceElement() {
		return (SequenceElement) getModelElement();
	}
	
	public boolean isEmpty() {
		return ( finitePartIsEmpty() && nonFinitePartIsEmpty() );
	}
	
	public boolean finitePartIsEmpty() {
		return (getSequenceElement().getFinitePart().isEmpty()
				|| (finitePartIndex >= getSequenceElement().getFinitePart().size()));
	}
	
	public boolean nonFinitePartIsEmpty() {
		return (getSequenceElement().getNonFinitePart().isEmpty());
	}
	
	public void reset() {
		finitePartIndex = 0;
		nonFinitePartIndex = 0;
	}
	
	public PrimitiveElement getHead() {
		if (finitePartIsEmpty()) {
			if (nonFinitePartIsEmpty()) {	//safe precaution
					IntegerElement fake = BasicTypeFactory.eINSTANCE.createIntegerElement();
					fake.setValue(-1);
					fake.setName("fake");
					return fake; //safe precaution
			}
			else {
				return getSequenceElement().getNonFinitePart().get(nonFinitePartIndex);
			}
		}
		else {
			return getSequenceElement().getFinitePart().get(finitePartIndex);
		}
	}
		
	public PrimitiveElement nextHead() {
		if (finitePartIsEmpty()) {
			if (nonFinitePartIsEmpty()) {
				return null;
			}
			else {
				PrimitiveElement res = getSequenceElement().getNonFinitePart().get(nonFinitePartIndex);
				nonFinitePartIndex += 1;
				if (nonFinitePartIndex == getSequenceElement().getNonFinitePart().size()) {
					nonFinitePartIndex = 0;
				}
				return res;
			}
		}
		else {
			PrimitiveElement res = getSequenceElement().getFinitePart().get(finitePartIndex);
			finitePartIndex += 1;
			return res;
		}
	}
	
	public SolverSequenceElement getTail() {
		SolverSequenceElement res = this;//new SolverSequenceElement(getSequenceElement(), this.getInstantiatedElement());
		res.finitePartIndex = this.finitePartIndex;
		res.nonFinitePartIndex = this.nonFinitePartIndex;
		res.nextHead();
		return res;
	}
	
	public boolean isAnIntegerSequence() {
		if (getSequenceElement().getType() != null && (getSequenceElement().getType() instanceof SequenceType)
				&& ((SequenceType)getSequenceElement().getType()).getElementType() instanceof fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Integer) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		String res = getSequenceElement().getName() + "=";
		if (! finitePartIsEmpty()) {
			for (int i = finitePartIndex ; i < getSequenceElement().getFinitePart().size(); i++) {
				res += getSequenceElement().getFinitePart().get(i);
				if (i < getSequenceElement().getFinitePart().size() - 1) {
					res += "; ";
				}
			}
		}
		if (! nonFinitePartIsEmpty()) {
			res += "(";
			for (Iterator<PrimitiveElement> it = getSequenceElement().getNonFinitePart().iterator() ; it.hasNext(); ) {
				PrimitiveElement elt = it.next();
				if (elt == getSequenceElement().getNonFinitePart().get(nonFinitePartIndex)) {
					res += "^";
				}
				res += elt;
				if (it.hasNext()) {
					res += "; ";
				}
			}
			res += ")";
		}
		return res;
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = new SerializedConstraintState();
		currentState.dump(this.getFinitePartIndex());
		currentState.dump(this.getNonFinitePartIndex());
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		this.setFinitePartIndex((Integer) newState.restore(0));
		this.setNonFinitePartIndex((Integer) newState.restore(1));
	}
	
}
