/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver;

import javax.management.BadAttributeValueExpException;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Death;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Discretization;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.AbstractConcreteMapping;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.ValueProvider;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeSequence;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeConcatenation;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeDeath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeDefer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeDiscretization;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeInf;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeIntersection;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeSampling;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeSup;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeUnion;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeUpTo;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.RuntimeWait;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.BasicType.SolverSequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.ClockNotFound;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverWrappedException;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.UnboundAbstract;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpressionDelegate;

class SolverKernelExpressionBuilder extends KernelExpressionSwitch<SolverExpression> {
	
	/**
	 * 
	 */
	private CCSLKernelSolver solver;
	private ImplicitClock iClock;
	private AbstractConcreteMapping<ISolverElement> environment;
	private InstantiatedElement expression;

	/**
	 * @param definitionExpansionVisitor
	 */
	SolverKernelExpressionBuilder(CCSLKernelSolver solver, ImplicitClock iClock,
					AbstractConcreteMapping<ISolverElement> env) {
		this.solver = solver;
		this.iClock = iClock;
		this.environment = env;
	}

	public SolverExpression buildExpression(InstantiatedElement expression) throws UnboundAbstract {
		this.expression = expression;
		ExpressionDeclaration decl = (ExpressionDeclaration) expression.getDeclaration();
		SolverExpression expr = (SolverExpression) doSwitch(decl);
		expr.setInstantiationPath(new InstantiationPath(expression.getInstantiationPath()));
		expr.setInstantiatedElement(expression);
		return expr;
	}
	
	private SolverClock findClock(InstantiationPath path) {
		SolverClock res = solver.getClockInstantiationTree().lookupInstance(path);
		if (res == null) {
			throw new SolverWrappedException(new ClockNotFound("No clock for path: " + path.toString()));
		}
		return res;
	}

	@Override
	public SolverExpression caseDeath(Death object) {
		SolverExpressionDelegate expr = new SolverExpressionDelegate(new RuntimeDeath(iClock));
		return expr;
	}
	
	@Override
	public SolverExpression caseUnion(Union object) {
		SolverExpressionDelegate expr = new SolverExpressionDelegate();
		InstantiatedElement left = expression.resolveAbstractEntity(object.getClock1());
		SolverClock leftClock = findClock(left.getInstantiationPath());
		InstantiatedElement right = expression.resolveAbstractEntity(object.getClock2());
		SolverClock rightClock = findClock(right.getInstantiationPath());
		leftClock.incRefCount();
		rightClock.incRefCount();
		expr.setDelegate(new RuntimeUnion(iClock, leftClock, rightClock));
		return expr;
	}
	
	@Override
	public SolverExpression caseWait(Wait object) {
		SolverExpressionDelegate expr = new SolverExpressionDelegate();
		InstantiatedElement waiting = expression.resolveAbstractEntity(object.getWaitingClock());
		InstantiatedElement value = expression.resolveAbstractEntity(object.getWaitingValue());
		SolverClock waitClock = findClock(waiting.getInstantiationPath());
		final PrimitiveElement waitCount = (PrimitiveElement) solver.buildElement(value, expr).getModelElement();
		waitClock.incRefCount();
		RuntimeWait wait = null;
		if (waitCount instanceof IntegerElement) {
			wait = new RuntimeWait(iClock, waitClock, ((IntegerElement)waitCount).getValue());
		}
		else if (waitCount instanceof ClassicalExpression) {
			ValueProvider<Integer> temp = new ValueProvider<Integer>() {
				private ClassicalExpression expr = (ClassicalExpression)waitCount;
				@Override
				public Integer getValue() {
					ClassicalExpressionEvaluator eval
						= new ClassicalExpressionEvaluator(environment);
					SolverElement value = eval.evaluate(expr);
					if ((value instanceof SolverPrimitiveElement) && value.getModelElement() instanceof IntegerElement)
						return ((IntegerElement) value.getModelElement()).getValue();
					else
						return 0;
				}
			};
			wait = new RuntimeWait(iClock, waitClock, temp);
		}
		expr.setDelegate(wait);
		return expr;
	}

	@Override
	public SolverExpression caseDefer(Defer object) {
		SolverExpressionDelegate expr = new SolverExpressionDelegate();
		InstantiatedElement baseClockElt = expression.resolveAbstractEntity(object.getBaseClock());
		InstantiatedElement delayClockElt = expression.resolveAbstractEntity(object.getDelayClock());
		InstantiatedElement delayPattern = expression.resolveAbstractEntity(object.getDelayPattern());
		SolverClock baseClock = findClock(baseClockElt.getInstantiationPath());
		SolverClock delayClock = findClock(delayClockElt.getInstantiationPath());
		baseClock.incRefCount();
		delayClock.incRefCount();
		SolverElement delayElt = (SolverElement) solver.buildElement(delayPattern, expr);
		RuntimeDefer defer = null;
		if (delayElt instanceof SolverSequenceElement) {
			SequenceElement seq = ((SolverSequenceElement) delayElt).getSequenceElement();
			Integer fp[] = new Integer[ seq.getFinitePart().size()  ];
			int i = 0;
			for (PrimitiveElement elt : seq.getFinitePart()) {
				if (elt instanceof IntegerElement) {
					fp[i] = ((IntegerElement) elt).getValue();
					i += 1;
				}
			}
			Integer nfp[] = new Integer[ seq.getNonFinitePart().size() ];
			i = 0;
			for (PrimitiveElement elt : seq.getNonFinitePart()) {
				if (elt instanceof IntegerElement) {
					nfp[i] = ((IntegerElement) elt).getValue();
					i += 1;
				}else
				if(elt instanceof IntegerVariableRef){
					InstantiatedElement ie = expression.resolveAbstractEntity(((IntegerVariableRef) elt).getReferencedVar());
					if (!( ie.getInstantiationPath().getLast() instanceof IntegerElement)){
						try {
							throw new BadAttributeValueExpException("the integerVaribaleRef ("+elt+") is not bounded to an integerElement !");
						} catch (BadAttributeValueExpException e) {
							e.printStackTrace();
						}
					}else{
						nfp[i] = ((IntegerElement)ie.getInstantiationPath().getLast()).getValue();
						i += 1;
					}
				}
			}
			RuntimeSequence<Integer> rseq  = new RuntimeSequence<Integer>(fp, nfp);
			defer = new RuntimeDefer(iClock, baseClock, delayClock, rseq);
		}
		else if (delayElt.getModelElement() instanceof ClassicalExpression) {
			final Element delay = delayElt.getModelElement();
			ValueProvider<RuntimeSequence<? extends Integer>> tmp = new ValueProvider<RuntimeSequence<? extends Integer>>() {
				private ClassicalExpression expr = (ClassicalExpression)delay;
				@Override
				public RuntimeSequence<Integer> getValue() {
					ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(environment);
					SolverElement value = eval.evaluate(expr);
					if (value instanceof SolverSequenceElement) {
						SequenceElement seq = ((SolverSequenceElement) value).getSequenceElement();
						Integer fp[] = new Integer[ seq.getFinitePart().size()  ];
						int i = 0;
						for (PrimitiveElement elt : seq.getFinitePart()) {
							if (elt instanceof IntegerElement) {
								fp[i] = ((IntegerElement) elt).getValue();
								i += 1;
							}
						}
						Integer nfp[] = new Integer[ seq.getNonFinitePart().size() ];
						i = 0;
						for (PrimitiveElement elt : seq.getNonFinitePart()) {
							if (elt instanceof IntegerElement) {
								nfp[i] = ((IntegerElement) elt).getValue();
								i += 1;
							}
						}
						RuntimeSequence<Integer> rseq  = new RuntimeSequence<Integer>(fp, nfp);
						return rseq;						
					}
					return null;
				}
			};
			defer = new RuntimeDefer(iClock, baseClock, delayClock, tmp);
		}
		expr.setDelegate(defer);
		return expr;
	}
	
	@Override
	public SolverExpression caseIntersection(Intersection object) {
		InstantiatedElement left = expression.resolveAbstractEntity(object.getClock1());
		InstantiatedElement right = expression.resolveAbstractEntity(object.getClock2());
		SolverClock leftClock = findClock(left.getInstantiationPath());
		SolverClock rightClock = findClock(right.getInstantiationPath());
		leftClock.incRefCount();
		rightClock.incRefCount();
		SolverExpressionDelegate expr = new SolverExpressionDelegate();
		expr.setDelegate(new RuntimeIntersection(iClock, leftClock, rightClock));
		return expr;
	}

	@Override
	public SolverExpression caseUpTo(UpTo object) {
		InstantiatedElement follow = expression.resolveAbstractEntity(object.getClockToFollow());
		InstantiatedElement killer = expression.resolveAbstractEntity(object.getKillerClock());
		SolverClock clockToFollow = findClock(follow.getInstantiationPath());
		SolverClock killerClock = findClock(killer.getInstantiationPath());
		clockToFollow.incRefCount();
		killerClock.incRefCount();
		RuntimeUpTo upTo = new RuntimeUpTo(iClock, clockToFollow, killerClock);
		upTo.setPreemptive(object.isIsPreemptive());
		SolverExpressionDelegate expr = new SolverExpressionDelegate(upTo);
		return expr;
	}
	
	@Override
	public SolverExpression caseStrictSampling(StrictSampling object) {
		InstantiatedElement sampled = expression.resolveAbstractEntity(object.getSampledClock());
		InstantiatedElement sampling = expression.resolveAbstractEntity(object.getSamplingClock());
		SolverClock sampledClock = findClock(sampled.getInstantiationPath());
		SolverClock samplingClock = findClock(sampling.getInstantiationPath());
		sampledClock.incRefCount();
		samplingClock.incRefCount();
		RuntimeStrictSampling smp = new RuntimeStrictSampling(iClock, sampledClock, samplingClock);
		SolverExpressionDelegate expr = new SolverExpressionDelegate(smp);
		return expr;
	}
	
	@Override
	public SolverExpression caseNonStrictSampling(NonStrictSampling object) {
		InstantiatedElement sampled = expression.resolveAbstractEntity(object.getSampledClock());
		InstantiatedElement sampling = expression.resolveAbstractEntity(object.getSamplingClock());
		SolverClock sampledClock = findClock(sampled.getInstantiationPath());
		SolverClock samplingClock = findClock(sampling.getInstantiationPath());
		sampledClock.incRefCount();
		samplingClock.incRefCount();
		RuntimeSampling smp = new RuntimeSampling(iClock, sampledClock, samplingClock);
		SolverExpressionDelegate expr = new SolverExpressionDelegate(smp);
		return expr;			
	}
	
	@Override
	public SolverExpression caseInf(Inf object) {
		InstantiatedElement left = expression.resolveAbstractEntity(object.getClock1());
		InstantiatedElement right = expression.resolveAbstractEntity(object.getClock2());
		SolverClock clock1 = findClock(left.getInstantiationPath());
		SolverClock clock2 = findClock(right.getInstantiationPath());
		clock1.incRefCount();
		clock2.incRefCount();
		RuntimeInf inf = new RuntimeInf(iClock, clock1, clock2);
		SolverExpressionDelegate expr = new SolverExpressionDelegate(inf);
		return expr;
	}
	
	@Override
	public SolverExpression caseSup(Sup object) {
		InstantiatedElement left = expression.resolveAbstractEntity(object.getClock1());
		InstantiatedElement right = expression.resolveAbstractEntity(object.getClock2());
		SolverClock clock1 = findClock(left.getInstantiationPath());
		SolverClock clock2 = findClock(right.getInstantiationPath());
		clock1.incRefCount();
		clock2.incRefCount();
		RuntimeSup sup = new RuntimeSup(iClock, clock1, clock2);
		SolverExpressionDelegate expr = new SolverExpressionDelegate(sup);
		return expr;			
	}
	
	@Override
	public SolverExpression caseConcatenation(Concatenation object) {
		InstantiatedElement left = expression.resolveAbstractEntity(object.getLeftClock());
		InstantiatedElement right = expression.resolveAbstractEntity(object.getRightClock());
		SolverClock leftClock = findClock(left.getInstantiationPath());
		SolverClock rightClock = findClock(right.getInstantiationPath());
		leftClock.incRefCount();
		// Only if non recursive
		if ( ! expression.getInstantiationPath().equals(right.getInstantiationPath())){
			rightClock.incRefCount();
		}
		RuntimeConcatenation concat = new RuntimeConcatenation(iClock, leftClock, rightClock);
		SolverExpressionDelegate expr = new SolverExpressionDelegate(concat);
		return expr;
	}
	
	@Override
	public SolverExpression caseDiscretization(Discretization object) {
		SolverExpressionDelegate expr = new SolverExpressionDelegate();
		InstantiatedElement clock = expression.resolveAbstractEntity(object.getDenseClock());
		InstantiatedElement discFactor = expression.resolveAbstractEntity(object.getDiscretizationFactor());
		SolverClock denseClock = findClock(clock.getInstantiationPath());
		SolverElement factorElement = (SolverElement) solver.buildElement(discFactor, expr);
		float factor = (float) 0.0;
		if (factorElement.getModelElement() instanceof RealElement)
			factor = ((RealElement) factorElement.getModelElement()).getValue();
		else if (factorElement.getModelElement() instanceof RealElement)
			factor = ((IntegerElement)factorElement.getModelElement()).getValue();
		RuntimeDiscretization disc = new RuntimeDiscretization(iClock, denseClock, factor);
		expr.setDelegate(disc);
		return expr;
	}
	
}