/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.priorities;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.CoincidentClocks;
import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.Edge;
import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.GrphClockTreeConstructor;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import grph.oo.ObjectGrph;

public class PriorityPropagator {
	
	private final ObjectGrph<CoincidentClocks, Edge> _clockTree;
	private SolverPrioritySpecification _prioSpec=null;
	private CCSLKernelSolver _solver;
	private boolean _relationAdded=true;
	
	
	public PriorityPropagator(SolverPrioritySpecification p, CCSLKernelSolver s) {
		_prioSpec = p;
		_solver = s;
		GrphClockTreeConstructor treeConstructor = new GrphClockTreeConstructor();
		_clockTree = treeConstructor.createClockGrphDag(s.getUnfoldModel());
	}
	
	public void addPriorityRelationsToSpec(){
		
		//fix point
		while(_relationAdded){
			_relationAdded = false;

			
			for(CoincidentClocks coincidentClocks: _clockTree.getVertices())
				addPriorityRelation(coincidentClocks);
			}
		
			for(Edge edge : _clockTree.getEdges()) {
				if (edge.isPriorityPropagator())
				{
					addPriorityRelation(edge);
				}
			}		
	}
	
	
	private void addPriorityRelation(Edge edge) {
		//retrieve the subClocks
		CoincidentClocks ccS = _clockTree.getDirectedSimpleEdgeTail(edge);
		CoincidentClocks ccT = _clockTree.getDirectedSimpleEdgeHead(edge);

		SolverPrioritySpecification tmpSpec = new SolverPrioritySpecification(_prioSpec);
		//if the subclocks are used as lower in a spec, propagate
		for (Iterator<SolverPriorityRelation> iterator = tmpSpec.getPriorityRelations().iterator(); iterator.hasNext();) {
			SolverPriorityRelation pr = iterator.next();
			if (ccS.containsByInstantiationPathName(pr.getLower().getInstantiationPath().toString())){
				propagate(ccT,pr.getLower(), pr.getHigher());
				//to propagate through coincidences
				propagate(ccS,pr.getLower(), pr.getHigher());
			}
		}
		
	}

	private void addPriorityRelation(CoincidentClocks cc) {
		//retrieve the subClocks
		SolverPrioritySpecification tmpSpec = new SolverPrioritySpecification(_prioSpec);
		//if the subclocks are used as lower in a spec, propagate
		for (Iterator<SolverPriorityRelation> iterator = tmpSpec.getPriorityRelations().iterator(); iterator.hasNext();) {
			SolverPriorityRelation pr = iterator.next();
			if (cc.containsByInstantiationPathName(pr.getLower().getInstantiationPath().toString())){
				propagate(cc,pr.getLower(), pr.getHigher());
			}
		}
		
	}
	

	private void propagate(CoincidentClocks pr, SolverClock lower, SolverClock higher) {
		for (InstantiatedElement ie : pr) {
			if (ie.getInstantiationPath().toString().compareTo(lower.getInstantiationPath().toString()) != 0){
//				int sizeBefore = _prioSpec.getPriorityRelations().size();
				SolverPriorityRelation newPR = new SolverPriorityRelation();
				newPR.setHigher(higher);
				newPR.setLower(_solver.findClockByPath(ie.getQualifiedName()));
				
				if(_prioSpec.add(newPR)){
					_relationAdded =true;
				}
//				int sizeAfter = _prioSpec.getPriorityRelations().size();
//				if (sizeAfter != sizeBefore){
//					System.out.println("sizeAfter = "+sizeAfter);
//				}
			}
			
		}
	}

	/**
	 * warning, this method works only if the clock is not one from a Definition. 
	 * This means that the instantiation path is exactly the same than the QualifiedName
	 * @param ce, a concreteEntity
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getInstantiationPathName(ConcreteEntity ce) {
		return getQualifiedName(ce);
	}
	
    private String getQualifiedName(EObject eo) {
		String res="";
		if(! (eo.eContainer() instanceof ClockConstraintSystem)){
			res+= getQualifiedName(eo.eContainer());
			res += "::";
		}
		res+=((NamedElement)eo).getName();
    	return res;
	}
	
	public SolverPrioritySpecification getPrioSpec() {
		return _prioSpec;
	}
	public void setPrioSpec(SolverPrioritySpecification _prioSpec) {
		this._prioSpec = _prioSpec;
	}
	public ObjectGrph<CoincidentClocks, Edge> getClockTree() {
		return _clockTree;
	}
//	public void setClockTree(ObjectGrph<CoincidentClocks, Edge> _clockTree) {
//		this._clockTree = _clockTree;
//	}
	
	

}
