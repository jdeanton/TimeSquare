/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

@Deprecated
public class UnionExpression extends SolverExpression {

	private SolverClock leftClock;
	private SolverClock rightClock;

	public UnionExpression() {
		super();
	}

	public SolverClock getLeftClock() {
		return leftClock;
	}

	public void setLeftClock(SolverClock leftClock) {
		this.leftClock = leftClock;
	}

	public SolverClock getRightClock() {
		return rightClock;
	}

	public void setRightClock(SolverClock rightClock) {
		this.rightClock = rightClock;
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if ( ! canCallSemantic() /* isActiveState() && state != State.DEAD */) {
			return;
		}
		super.semantic(semanticHelper);
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		leftClock.semantic(semanticHelper);
		rightClock.semantic(semanticHelper);
//		wasBorn = true;
		if (state == State.DEAD) {
			semanticHelper.inhibitClock(getImplicitClock());
		}
		else {
			semanticHelper.semanticBDDAnd(semanticHelper.createEqual(getImplicitClock(),
					semanticHelper.createUnion(getLeftClock(), getRightClock())));
		}
		semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(),
				getLeftClock(), getRightClock() });
	}

	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if (! canCallUpdate()) {
			return;
		}
		super.update(helper);
		leftClock.update(helper);
		rightClock.update(helper);
//		if (leftClock.isDead() && rightClock.isDead()) {
//			terminate(helper);
//		}
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		super.deathSemantic(semanticHelper);
		leftClock.deathSemantic(semanticHelper);
		rightClock.deathSemantic(semanticHelper);
		semanticHelper.registerDeathConjunctionImplies(leftClock, rightClock, getImplicitClock());
	}
	
//	@Override
//	public void deathUpdate(AbstractUpdateHelper helper) throws SimulationException {
//		super.deathUpdate(helper);
//		if (leftClock.isDead() && rightClock.isDead()) {
//			terminate(helper);
//		}
//	}

	@Override
	public String toString() {
		return "[" + getImplicitClock().getName() + "]Union(" + leftClock.getName()
				+ ", " + rightClock.getName() + ")";
	}


	
}
