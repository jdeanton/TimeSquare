/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression;

import java.util.HashMap;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ClassicalExpressionEvaluator;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.SolverPrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.BasicType.SolverSequenceElement;

public class RecursiveTailCallJump extends SolverExpression {

	private SolverExpression target;
	private Expression modelExpression;
	private HashMap<AbstractEntity, ISolverElement> parameterValues;
	private boolean updateDone;
	private boolean resetJustDone;
	
	public RecursiveTailCallJump(Expression modelExpression, SolverExpression target) {
		super();
		this.modelExpression = modelExpression;
		this.target = target;
		this.parameterValues = new HashMap<AbstractEntity, ISolverElement>();
		this.updateDone = true;
	}
	
	public void setParameterValue(AbstractEntity parameter, ISolverElement value) {
		parameterValues.put(parameter, value);
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (traceStart)
			System.out.println("Entry: RecursiveTailCallJump.start(" + toString() + ")");
		//avoid to reset and forgot the initial parameter
		if( ! resetJustDone){
			super.start(helper);
			for (Binding bd : modelExpression.getBindings()) {
				AbstractEntity parameter = bd.getAbstract();
				ISolverElement value = parameterValues.get(parameter);
				if (value instanceof SolverPrimitiveElement) {
					ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(getAbstractMapping());
					value = eval.evaluate((SolverPrimitiveElement) value);
					if (value == null) {
						throw new NullPointerException("evaluation of a primitive element failled in "+this);
					}
				}
				getAbstractMapping().setAbstractEntityValue(parameter, value);
			}
			updateDone = true;
			//there is no other way to point the target so it acts like a container of the target and starts it
			if (target instanceof SolverExpression){
				((SolverExpression)target).getImplicitClock().start(helper);
			}
			else {
				target.start(helper);
			}
			state = State.INIT;
		}else{
			this.resetJustDone = false;
		}
		if (traceStart)
			System.out.println("Exit: RecursiveTailCallJump.start(" + toString() + ")");
	}
	
	
	
	public void reset(AbstractSemanticHelper helper) throws SimulationException {
		if (traceStart)
			System.out.println("Entry: RecursiveTailCallJump.start(" + toString() + ")");
			
		super.start(helper);
		for (Binding bd : modelExpression.getBindings()) {
			AbstractEntity parameter = bd.getAbstract();
			ISolverElement value = parameterValues.get(parameter);
			if (value instanceof SolverPrimitiveElement) {
				value = getAbstractMapping().resolveAbstractEntity(parameter);
				if (value instanceof SolverSequenceElement){
				((SolverSequenceElement)value).reset();
				}
			}
			getAbstractMapping().setAbstractEntityValue(parameter, value);
		}
		updateDone = true;
		state = State.INIT;
		
		if (target instanceof SolverExpression){
			((SolverExpression)target).getImplicitClock().setDead(false);
			((SolverExpression)target).getImplicitClock().start(helper);
		}
		else{
			target.start(helper);
		}
		
		this.resetJustDone = true;
			
		if (traceStart)
			System.out.println("Exit: RecursiveTailCallJump.start(" + toString() + ")");
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (traceSemantic)
			System.out.println("Entry: RecursiveTailCallJump.semantic(" + toString() + ")");
		
		this.resetJustDone = false;
		if ( ( ! isActiveState()) || semanticHelper.isSemanticDone(this)) {
			System.out.println("Exit: RecursiveTailCallJump.semantic(" + toString() + ") not active or semantic done");
			return;
		}
		semanticHelper.semanticBDDAnd(semanticHelper.createEqual(getImplicitClock(), target.getImplicitClock()));
		semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(), target.getImplicitClock() });
		semanticHelper.registerSemanticDone(this);
		this.updateDone = false;
//		this.wasBorn=true;
		if (traceSemantic)
			System.out.println("Exit: RecursiveTailCallJump.semantic(" + toString() + ")");
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		this.resetJustDone = false;
		if ( ! isActiveState()) {
			return;
		}
		semanticHelper.registerDeathEquality(getImplicitClock(), target.getImplicitClock());
	}
	
	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if (traceUpdate)
			System.out.println("Entry: RecursiveTailCallJump.update(" + toString() + ")");
		
		this.resetJustDone = false;
		if ( ! isActiveState() || updateDone) {
			System.out.println("Exit: RecursiveTailCallJump.update(" + toString() + ") not active or update done");
			return;
		}
		this.updateDone = true;
		target.update(helper);
		if (traceUpdate)
			System.out.println("Exit: RecursiveTailCallJump.update(" + toString() + ")");
	}
	
	@Override
	public boolean isTerminated() {
		return target.isTerminated();
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		super.terminate(helper);
		if (traceTerminate) {
			System.out.println("Entry: RecursiveTailExpression.terminate(" + toString() + ")");
		}
		state = State.DEAD;
		target.terminate(helper);
		if (traceTerminate) {
			System.out.println("Exit: RecursiveTailExpression.terminate(" + toString() + ")");
		}		
	}
	
	@Override
	public String toString() {
		return "[" + getImplicitClock().getName() + "]" + (isActiveState() ? "*" : "")
				+ "JumpTo(" + target.getImplicitClock().getName() + ")";
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//supper added 2 'state variables' in the set
		currentState.dump(this.resetJustDone);
//		currentState.addAll(target.getState());
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
		resetJustDone = (Boolean) newState.restore(2);
//		newState.remove(0);newState.remove(1);newState.remove(2);
//		target.setState(newState);
	}
	
}
