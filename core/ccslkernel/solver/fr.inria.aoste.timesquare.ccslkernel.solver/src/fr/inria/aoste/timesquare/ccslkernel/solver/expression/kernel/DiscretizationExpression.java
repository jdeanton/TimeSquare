/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

@Deprecated
public class DiscretizationExpression extends SolverExpression {
	
	private SolverClock denseClock;
	private float factor;
	
	public DiscretizationExpression() {
		super();
	}

	public SolverClock getDenseClock() {
		return denseClock;
	}

	public void setDenseClock(SolverClock denseClock) {
		this.denseClock = denseClock;
	}

	public float getFactor() {
		return factor;
	}

	public void setFactor(float factor) {
		this.factor = factor;
	}
		
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if ( ! canCallSemantic() /* ! isActiveState() && state != State.DEAD */) {
			return;
		}
		super.semantic(semanticHelper);
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		if (state == State.DEAD) {
			semanticHelper.inhibitClock(getImplicitClock());
		}
		semanticHelper.registerClockUse(getImplicitClock());
	}
	
	@Override
	public String toString() {
		return "[" + getImplicitClock().getName() + "]" + ( isActiveState() ? "*" : "") + "Discretizes(" + denseClock.getName() + ", " + factor + ")";
	}
	
}
