/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

@Deprecated
public class ConcatExpression extends SolverExpression {

	private SolverClock leftClock;
	private SolverClock rightClock;
	private boolean updateDone;

	private enum ConcatState {
		FOLLOWLEFT, FOLLOWRIGHT
	};

	private ConcatState localState;

	public ConcatExpression() {
		super();
		updateDone = false;
	}

	public SolverClock getLeftClock() {
		return leftClock;
	}

	public void setLeftClock(SolverClock leftClock) {
		this.leftClock = leftClock;
	}

	public SolverClock getRightClock() {
		return rightClock;
	}

	public void setRightClock(SolverClock rightClock) {
		this.rightClock = rightClock;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (traceStart)
			System.out.println("Entry: ConcatExpression.start(" + toString() + ")");
		if ( ! canCallStart())
			return;
		super.start(helper);
		localState = ConcatState.FOLLOWLEFT;
		if (traceStart)
			System.out.println("Exit: ConcatExpression.start(" + toString() + ")");
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (traceSemantic)
			System.out.println("Entry: ConcatExpression.semantic(" + toString() + ")");
		if ( ! canCallSemantic() /* ! isActiveState() && state != State.DEAD */) {
			if (traceSemantic)
				System.out.println("Exit: ConcatExpression.semantic(" + toString()
						+ ") not active.");
			return;
		}
		super.semantic(semanticHelper);
		if (semanticHelper.isSemanticDone(this)) {
			if (traceSemantic)
				System.out.println("Exit: ConcatExpression.semantic(" + toString()
						+ ") semantic done.");
			return;
		}
		semanticHelper.registerSemanticDone(this);
		if (state == State.DEAD) {
			semanticHelper.inhibitClock(getImplicitClock());
		}
//		wasBorn = true;
		updateDone = false;
		if (localState == ConcatState.FOLLOWRIGHT) {
			if (getImplicitClock() != rightClock) {
				semanticHelper.semanticBDDAnd(semanticHelper.createEqual(
						getImplicitClock(), getRightClock()));
				semanticHelper.registerClockUse(getRightClock());
				semanticHelper.registerClockUse(getImplicitClock());
			}
			rightClock.semantic(semanticHelper);
		}
		else {
			semanticHelper.semanticBDDAnd(semanticHelper.createEqual(getImplicitClock(),
					getLeftClock()));
			semanticHelper.registerClockUse(new SolverClock[] {getImplicitClock(), getLeftClock()});
			leftClock.semantic(semanticHelper);
		}
		
		if (traceSemantic)
			System.out.println("Exit: ConcatExpression.semantic(" + toString() + ")");
	}

	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if (traceUpdate)
			System.out.println("Entry: ConcatExpression.update(" + toString() + ")");
		
		if ( ! canCallUpdate() /* isActiveState() */) {
			if (traceUpdate)
				System.out.println("Exit: ConcatExpression.update(" + toString()
						+ ") not active.");
			return;
		}
		super.update(helper);
		if (updateDone) {
			if (traceUpdate)
				System.out.println("Exit: ConcatExpression.update(" + toString()
						+ ") update done.");
			return;
		}
		if (localState == ConcatState.FOLLOWLEFT) {
			leftClock.update(helper);
			if (leftClock.isDead()) {
				if ( ! isRecursive()){
					helper.registerClockToStart(rightClock);
					localState = ConcatState.FOLLOWRIGHT;
				}else{
					helper.registerClockToStart(leftClock);
					localState = ConcatState.FOLLOWLEFT;
				}
			}
		}
		else {
			if ((rightClock instanceof ImplicitClock)
					&& (getImplicitClock() != rightClock)) {
				rightClock.update(helper);
			}
			if (rightClock.isDead() && (!isRecursive())) {
				terminate(helper);
			}
		}
		updateDone = true;
		if (traceUpdate)
			System.out.println("Exit: ConcatExpression.update(" + toString() + ")");
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		if (state == State.INIT) {
			return;
		}
		else if (state == State.DEAD) {
			semanticHelper.forceDeath(getImplicitClock());
			return;
		}
		if (localState == ConcatState.FOLLOWLEFT) {
			leftClock.deathSemantic(semanticHelper);
		}
		else if ( ! isRecursive() ) {
			semanticHelper.registerDeathImplication(getRightClock(),getImplicitClock());
			rightClock.deathSemantic(semanticHelper);
		}
	}

	@Override
	public String toString() {
		String res = "[" + getImplicitClock().getName() + "]"
				+ ( isActiveState() ? "*" : "") + "Concat(";
		res += (localState == ConcatState.FOLLOWLEFT ? "^" : "") + leftClock + ", ";
		res += (localState == ConcatState.FOLLOWRIGHT ? "^" : "") + rightClock + ")";
		return res;
	}

	private boolean isRecursive() {
		return (rightClock instanceof ImplicitClock)
				&& (getImplicitClock() == rightClock);
	}

	
	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
		currentState.dump(localState.ordinal());
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
		localState = (ConcatState) ConcatState.values()[(Integer) newState.restore(2)];
	}

}
