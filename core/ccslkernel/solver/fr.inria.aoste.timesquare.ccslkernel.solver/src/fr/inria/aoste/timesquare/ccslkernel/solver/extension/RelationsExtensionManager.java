/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.extension;


import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;

import fr.inria.aoste.timesquare.ccslkernel.solver.Activator;
import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;

public class RelationsExtensionManager implements IExtensionManager {

	private static RelationsExtensionManager INSTANCE;
	public synchronized static RelationsExtensionManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new RelationsExtensionManager();
		}
		return INSTANCE;
	}
	private RelationsExtensionManager() {
		ExtensionPointManager.findAllExtensions(this);
	}

	@Override
	public String getExtensionPointName() {
		return Activator.PLUGIN_ID + ".extension.relations";
	}

	@Override
	public String getPluginName() {
		return Activator.PLUGIN_ID;
	}
	
	private List<ISolverRelationFactory> factories = new ArrayList<ISolverRelationFactory>();

	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable {
		Class<? extends ISolverRelationFactory> clazz = ExtensionPointManager.getPointExtensionClass(ice, "class", ISolverRelationFactory.class);
		factories.add( clazz.newInstance() );
	}
	
	public List<ISolverRelationFactory> getFactories() {
		return factories;
	}
	
}
