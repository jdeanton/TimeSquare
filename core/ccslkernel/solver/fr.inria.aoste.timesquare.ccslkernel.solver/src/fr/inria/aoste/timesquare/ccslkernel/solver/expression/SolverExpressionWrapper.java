/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;

public class SolverExpressionWrapper extends SolverExpression {

	private AbstractWrappedExpression wrapped;
	private boolean updateDone;

	public SolverExpressionWrapper(AbstractWrappedExpression wrapped, ImplicitClock implicitClock) {
		super();
		this.wrapped = wrapped;
		setImplicitClock(implicitClock);
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
//		if (state != State.INIT && state != State.DEAD)
//			return;
		super.start(helper);
		wrapped.start(helper);
//		state = State.ACTIVE;
//		wasBorn = true;
		updateDone = true;
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (! canCallSemantic())
			return;
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		super.semantic(semanticHelper);
		if (isTerminated()) {
			semanticHelper.inhibitClock(getImplicitClock());
			semanticHelper.registerClockUse(getImplicitClock());
		}
		else {
			wrapped.semantic(semanticHelper);
		}
		updateDone = false;
	}

	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if ( ! updateDone)
			wrapped.update(helper);
		updateDone = true;
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		wrapped.deathSemantic(semanticHelper);
		semanticHelper.registerSemanticDone(this);
	}

}
