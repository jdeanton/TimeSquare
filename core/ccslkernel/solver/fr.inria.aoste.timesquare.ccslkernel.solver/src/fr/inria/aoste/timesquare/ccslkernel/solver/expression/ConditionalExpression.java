/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.EvaluationTypeError;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ClassicalExpressionEvaluator;
import fr.inria.aoste.timesquare.ccslkernel.solver.ConditionalCase;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.SolverElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;

public class ConditionalExpression extends SolverExpression {

	private List<ISolverConcrete> concretes;
	private List<ImplicitClock> implicitClocks;
	private List<ConditionalCase> cases;
	private ImplicitClock defaultRootClock;
	private List<ISolverConcrete> defaultCase;
	private List<ImplicitClock> defaultCaseClocks;
	private ConditionalCase selectedCase;
	private ConditionalCase previousCase;
	private boolean goDefault = true;

	public ConditionalExpression() {
		this.concretes = new ArrayList<ISolverConcrete>();
		this.implicitClocks = new ArrayList<ImplicitClock>();
		this.cases = new ArrayList<ConditionalCase>();
		this.defaultCase = new ArrayList<ISolverConcrete>();
		this.defaultCaseClocks = new ArrayList<ImplicitClock>();
	}
	
	public List<ISolverConcrete> getConcretes() {
		return concretes;
	}

	public List<ImplicitClock> getImplicitClocks() {
		return implicitClocks;
	}

	public List<ConditionalCase> getCases() {
		return cases;
	}
	
	public ImplicitClock getDefaultRootClock() {
		return defaultRootClock;
	}

	public void setDefaultRootClock(ImplicitClock defaultRootClock) {
		this.defaultRootClock = defaultRootClock;
	}

	public List<ISolverConcrete> getDefaultCase() {
		return defaultCase;
	}

	public List<ImplicitClock> getDefaultCaseClocks() {
		return defaultCaseClocks;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (traceStart)
			System.out.println("Entry: ConditionalExpression.start(" + toString() + ")");
		if (! canCallStart())
			return;
		super.start(helper);
		goDefault = true;
		selectedCase = null;
		for (ConditionalCase relationCase : getCases()) {
			BooleanExpression condition = relationCase.getCondition();
			ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(getAbstractMapping());
			SolverElement test = eval.evaluate(condition);
			if ( ! (test.getModelElement() instanceof BooleanElement)) {
				throw new EvaluationTypeError("ConditionalExpression.start(): BooleanElement expected");
			}
			if (((BooleanElement) test.getModelElement()).getValue()) {
				selectedCase = relationCase;
				goDefault = false;
				break;
			}
		}
			
			for (ISolverConcrete concrete : selectedCase.getConcretes()) {
				if (! (concrete instanceof RecursiveTailCallJump)){
					if (concrete instanceof SolverExpression){
						((SolverExpression)concrete).getImplicitClock().start(helper);
					}else{
						concrete.start(helper);
					}
				}
			}
		for (ISolverConcrete concrete : this.concretes) {
			if (! (concrete instanceof RecursiveTailCallJump)){
				if (concrete instanceof SolverExpression){
					((SolverExpression)concrete).getImplicitClock().start(helper);
				}else{
					concrete.start(helper);
				}
			}
		}
		
		//if the death comes from the outside and not from a recursion
		if(getImplicitClock().isDead()){
			for (ISolverConcrete concrete : this.concretes) {
				if ((concrete instanceof RecursiveTailCallJump)){
					(( RecursiveTailCallJump)concrete).reset(helper);
				}
			}
		}
		
		previousCase = selectedCase;
		if (traceStart)
			System.out.println("Exit: ConditionalExpression.start(" + toString() + ")");
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(semanticHelper);
		if (traceSemantic)
			System.out.println("Entry: ConditionalExpression.semantic(" + toString() + ")");
		if ( ! isActiveState() && state != State.DEAD) {
			if (traceSemantic)
				System.out.println("Exit: ConditionalExpression.semantic(" + toString() + ") neither active nor dead");
			return;
		}
		if (state == State.DEAD) {
			semanticHelper.semanticBDDAnd( semanticHelper.createNot(getImplicitClock()) );
			semanticHelper.registerClockUse(getImplicitClock());
		}
//		wasBorn=true;
		goDefault = true;
		selectedCase = null;
		for (ConditionalCase relationCase : getCases()) {
			BooleanExpression condition = relationCase.getCondition();
			ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(getAbstractMapping());
			SolverElement test = eval.evaluate(condition);
			if ( ! (test.getModelElement() instanceof BooleanElement)) {
				throw new EvaluationTypeError("ConditionalExpression.semantic(): BooleanElement expected");
			}
			if (((BooleanElement) test.getModelElement()).getValue()) {
				selectedCase = relationCase;
				goDefault = false;
				break;
			}
		}
		if (selectedCase != null) {
			for (ConditionalCase relationCase : getCases()) {
				if (relationCase == selectedCase) {
					if (previousCase != selectedCase) {
						for (ISolverConcrete conc : relationCase.getConcretes()) {
							if (conc instanceof SolverExpression){
								((SolverExpression)conc).getImplicitClock().start(semanticHelper);
							}else{
								conc.start(semanticHelper);
							}
						}
					}
					for (ISolverConcrete conc : relationCase.getConcretes()) {
						conc.semantic(semanticHelper);
					}
					semanticHelper.semanticBDDAnd( semanticHelper.createEqual(getImplicitClock(), selectedCase.getRootClock()) );
					semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(), selectedCase.getRootClock() });
				}
				else {
					for (ISolverConcrete conc : relationCase.getConcretes()) {
						conc.terminate(semanticHelper.getUpdateHelper());
					}
				}
			}
			previousCase = selectedCase;
		}
		else if ( goDefault) {
			for (ISolverConcrete conc : getDefaultCase()) {
				if (conc instanceof SolverExpression){
					((SolverExpression)conc).getImplicitClock().start(semanticHelper);
				}else{
					conc.start(semanticHelper);
				}
				conc.semantic(semanticHelper);
			}
			semanticHelper.semanticBDDAnd(semanticHelper.createEqual(getImplicitClock(), defaultRootClock));
			semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(), getDefaultRootClock() });
		}
		semanticHelper.registerClockUse(getImplicitClock());
		if (traceSemantic)
			System.out.println("Exit: ConditionalExpression.semantic(" + toString() + ")");
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if ( ! isActiveState() && state != State.DEAD) {
			return;
		}
		if (state == State.DEAD) {
			semanticHelper.forceDeath(getImplicitClock());
		}
		if (selectedCase != null) {
			for (ISolverConcrete conc : selectedCase.getConcretes()) {
				conc.deathSemantic(semanticHelper);
			}
			semanticHelper.registerDeathEquality(getImplicitClock(), selectedCase.getRootClock());
		}
		else if (goDefault) {
			for (ISolverConcrete conc : getDefaultCase()) {
				conc.deathSemantic(semanticHelper);
			}
			semanticHelper.registerDeathEquality(getImplicitClock(), getDefaultRootClock());
		}
	}
	
	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if (traceUpdate)
			System.out.println("Entry: ConditionalExpression.update(" + toString() + ")");
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if ( ! isActiveState()) {
			if (traceUpdate)
				System.out.println("Exit: ConditionalExpression.update(" + toString() + ") not active");
			return;
		}
		if (selectedCase == null) {
			if (goDefault) {
				for (ISolverConcrete conc : getDefaultCase()) {
					conc.update(helper);
				}
				if (getDefaultRootClock().isDead()) {
					terminate(helper);
				}
			}
		}
		else {
			for (ISolverConcrete conc : selectedCase.getConcretes()) {
				conc.update(helper);
			}
			if (selectedCase.getRootClock().isDead()) {
				terminate(helper);
			}
		}
		if (traceUpdate)
			System.out.println("Exit: ConditionalExpression.update(" + toString() + ")");
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		super.terminate(helper);
		for (ConditionalCase relationCase : getCases()) {
			for (ISolverConcrete conc : relationCase.getConcretes()) {
				conc.terminate(helper);
			}
		}
		for (ISolverConcrete conc : getDefaultCase()) {
			conc.terminate(helper);
		}
	}
	
	@Override
	public String toString() {
		String res = "[" + getImplicitClock().getName() + "]" + ( isActiveState() ? "*" : "");
		res += "Conditional(";
		for (ConditionalCase alternative : getCases()) {
			if (alternative == selectedCase) {
				res += "^";
			}
			if (alternative.getCondition() != null) {
				res += "if ";
				res += alternative.getCondition().eClass().getName() + " then ";
			}
			else {
				res += " else ";
			}
			res += alternative.getConcretes().get(0);
			res += ";";
		}
		res += ")";
		if (state == State.DEAD) {
			res += "/D";
		}
		return res;
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//supper added 2 'state variables' in the set
		int selectedCaseIndex = cases.indexOf(selectedCase);
		
		currentState.dump(selectedCaseIndex);
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
		selectedCase = cases.get((Integer) newState.restore(2));
	}
	

}
