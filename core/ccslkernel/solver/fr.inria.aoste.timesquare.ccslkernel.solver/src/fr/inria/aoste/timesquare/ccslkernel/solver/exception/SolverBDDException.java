/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.exception;

public class SolverBDDException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3166534221959093298L;

	public SolverBDDException() {
	}

	public SolverBDDException(String message) {
		super(message);
	}

	public SolverBDDException(Throwable cause) {
		super(cause);
	}

	public SolverBDDException(String message, Throwable cause) {
		super(message, cause);
	}

}
