/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation;

import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.AbstractConcreteMapping;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement;
import fr.inria.aoste.trace.ModelElementReference;

public class SolverRelationWrapper implements ISolverConcrete, IRuntimeContainer {

//	private boolean assertion;
//	private int assertionVariableNumber;
	private InstantiatedElement instantiatedElement;
	@SuppressWarnings("unused")
	private AbstractConcreteMapping<ISolverElement> abstractMapping;
	@SuppressWarnings("unused")
	private InstantiationPath instantiationPath;
	@SuppressWarnings("unused")
	private ModelElementReference traceReference;
	AbstractWrappedRelation wrappedRelation;
	boolean updateDone;

	public SolverRelationWrapper(AbstractWrappedRelation relation) {
		this.wrappedRelation = relation;
	}
	
	public InstantiatedElement getInstantiatedElement(){
		return instantiatedElement;
	}

	public AbstractWrappedRelation getWrappedRelation() {
		return wrappedRelation;
	}
	
	public void setWrappedRelation(AbstractWrappedRelation wrappedRelation) {
		this.wrappedRelation = wrappedRelation;
	}
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {

		wrappedRelation.start(helper);
		updateDone = true;
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		wrappedRelation.semantic(semanticHelper);
		updateDone = false;
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		wrappedRelation.deathSemantic(semanticHelper);
	}

	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
//		if (updateDone) {
//			return;
//		}
		wrappedRelation.update(helper);
//		updateDone = true;
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		wrappedRelation.terminate(helper);
	}

	@Override
	public boolean isTerminated() {
		return false;
	}

	public void setAbstractMapping(AbstractConcreteMapping<ISolverElement> context) {
		this.abstractMapping = context;
	}

	public void setInstantiatedElement(InstantiatedElement relation) {
		this.instantiatedElement = relation;
	}

	public void setInstantiationPath(InstantiationPath instantiationPath) {
		this.instantiationPath = instantiationPath;
	}

	public void setTraceReference(ModelElementReference modelElementReference) {
		this.traceReference = modelElementReference;
	}

	@Override
	public SerializedConstraintState dumpState() {
		return wrappedRelation.dumpState();
	}

	@Override
	public void restoreState(SerializedConstraintState state) {
		wrappedRelation.restoreState(state);
	}

	private IRuntimeContainer parent;
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null)
			parent.addContent(this);
	}

	@Override
	public List<IRuntimeContainer> getContents() {
		return Collections.emptyList();
	}

	@Override
	public void addContent(IRuntimeContainer element) { }

}
