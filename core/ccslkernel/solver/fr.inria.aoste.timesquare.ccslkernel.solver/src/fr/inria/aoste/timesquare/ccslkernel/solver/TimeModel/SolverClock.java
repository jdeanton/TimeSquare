/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel;

import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement;
import fr.inria.aoste.trace.ModelElementReference;

public class SolverClock extends RuntimeClock implements ISolverElement, ISolverConcrete, IRuntimeContainer {

	private InstantiatedElement instantiatedElement;
	private NamedElement modelElement;

	public SolverClock() {
		super();
		init();
	}
	
	public SolverClock(InstantiatedElement element) {
		super();
		this.instantiatedElement = element;
		this.modelElement = element.getInstantiationPath().getLast();
		setName(modelElement.getName());
		init();
	}
	
	public SolverClock(Clock modelClock) {
		super(modelClock.getName());
		this.modelElement = modelClock;
		init();
	}
	
	@Override
	public InstantiatedElement getInstantiatedElement() {
		return instantiatedElement;
	}
	
	@Override
	public void setInstantiatedElement(InstantiatedElement ie) {
		this.instantiatedElement = ie;
	}
	
	@Override
	public NamedElement getModelElement() {
		return modelElement;
	}
	
	@Override
	public void setModelElement(NamedElement elt) {
		this.modelElement = elt;
	}
	
	@Override
	public Type getElementType() {
		if (modelElement instanceof Element)
			return ((Element) modelElement).getType();
		else
			return null;
	}
	
	private void init() {
		this.tickCount = 0;
		setDead(false);
		setDense(false);
		refCount = 0;
		bddVariableNumber = RuntimeClock.UNALLOCATEDBDDVARIABLE;
	}
	
	public ModelElementReference traceReference;
	protected InstantiationPath instantiationPath;
	
	protected int refCount;
		
	public InstantiationPath getInstantiationPath() {
		return instantiationPath;
	}

	public void setInstantiationPath(InstantiationPath instantiationPath) {
		this.instantiationPath = instantiationPath;
	}
	
	public void incRefCount() {
		refCount += 1;
	}
	
	public int getRefCount() {
		return refCount;
	}
	
	@Override
	public String toString() {
		return "[" + getName()
				+ "/" + tickCount + (state == ClockState.DEAD ? "/D" : "") + "]";
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
			state = ClockState.ALIVE;
			// The following does not seem useful, tests run successfully without it.
			helper.unregisterDeadClock(this);
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		setDead(true);
	}
	
	public Clock getModelClock() {
		return (Clock) getModelElement();
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = new SerializedConstraintState();
		//warning the order is important !
		currentState.dump(new Integer(state.ordinal()));
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		state = ClockState.values()[(Integer) newState.restore(0) ];
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
	}
	
	@Override
	public void update(AbstractUpdateHelper updateHelper) throws SimulationException {		
	}
	
	@Override
	public boolean isTerminated() {
		return false;
	}

	private IRuntimeContainer parent;
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null) {
			parent.addContent(this);
		}
	}

	@Override
	public List<IRuntimeContainer> getContents() {
		return Collections.emptyList();
	}

	@Override
	public void addContent(IRuntimeContainer element) { }

}
