/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation.kernel;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;

@Deprecated
public class SolverKernelRelationFactory extends KernelRelationSwitch<SolverKernelRelation> {

	private SolverKernelRelationFactory() {
	}
	
	static public SolverKernelRelationFactory INSTANCE = new SolverKernelRelationFactory();
	
	public SolverKernelRelation createSolverKernelRelation(KernelRelationDeclaration decl) {
		return doSwitch(decl);
	}
	
	@Override
	public SolverKernelRelation caseCoincidence(Coincidence object) {
		return new CoincidenceRelation();
	}
	
	@Override
	public SolverKernelRelation caseExclusion(Exclusion object) {
		return new ExclusionRelation();
	}
	
	@Override
	public SolverKernelRelation casePrecedence(Precedence object) {
		return new PrecedesRelation();
	}
	
	@Override
	public SolverKernelRelation caseSubClock(SubClock object) {
		return new SubclockRelation();
	}
	
	@Override
	public SolverKernelRelation caseNonStrictPrecedence(
			NonStrictPrecedence object) {
		return new NonStrictPrecedesRelation();
	}

}
