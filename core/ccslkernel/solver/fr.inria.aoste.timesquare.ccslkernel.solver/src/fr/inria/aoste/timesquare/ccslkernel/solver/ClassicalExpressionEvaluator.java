/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BasicTypePackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.RealElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntDivide;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMultiply;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.RealVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqDecr;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqSched;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util.ClassicalExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.AbstractConcreteMapping;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.EvaluationTypeError;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.BasicType.SolverSequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverWrappedException;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.UnimplementedEvaluation;

public class ClassicalExpressionEvaluator /* extends Visitor<Element> */ {

	private AbstractConcreteMapping<ISolverElement> acm;
	
	public ClassicalExpressionEvaluator(AbstractConcreteMapping<ISolverElement> abstractConcreteMapping) {
		super();
		this.acm = abstractConcreteMapping;
	}
		
	public SolverElement evaluate(SolverPrimitiveElement elt) throws EvaluationTypeError {
		if (elt.getPrimitiveElement() instanceof ClassicalExpression) {
			if (elt.getPrimitiveElement() instanceof SeqExpression) {
				return seqEvaluator.doSwitch(elt.getPrimitiveElement());
			}
			else {
				Element res = evaluator.doSwitch(elt.getPrimitiveElement());
				if (res instanceof SequenceElement) {
					throw new EvaluationTypeError("");
				}
				else if (res instanceof PrimitiveElement) {
					return new SolverPrimitiveElement((PrimitiveElement)res);
				}
				else {
					throw new EvaluationTypeError("");
				}
			}
		}
		else {
			return elt;
		}
	}
	
	public SolverElement evaluate(SolverElement elt) throws EvaluationTypeError {
		if (elt instanceof SolverPrimitiveElement) {
			return evaluate((SolverPrimitiveElement)elt);
		}
		else if (elt instanceof SolverSequenceElement) {
			return elt;
		}
		else {
			throw new EvaluationTypeError("Evaluate(SolverElement): Non evaluable element");
		}
	}
	
	public SolverElement evaluate(PrimitiveElement elt) {
		if (elt instanceof ClassicalExpression) {
			if (elt instanceof SeqExpression) {
				return seqEvaluator.doSwitch(elt);
			}
			else {
				PrimitiveElement res = (PrimitiveElement) evaluator.doSwitch(elt);
				return new SolverPrimitiveElement(res);
			}
		}
		else {
			return new SolverPrimitiveElement(elt);
		}
	}
	
	public SolverElement evaluate(SequenceElement seq) {
		return new SolverSequenceElement(seq,null);
	}

	/*
	 * Object that performs evaluation of classical expressions that return only PrimitiveElement and not
	 * SequenceElement because these are replaced internally by SolverSequenceElement.
	 */
	private ClassicalExpressionSwitch<Element> evaluator = new ClassicalExpressionSwitch<Element>() {
		
		/*
		 * Catch all clause
		 */
		public Element caseClassicalExpression(ClassicalExpression object) {
			throw new SolverWrappedException(new UnimplementedEvaluation(object.getName() + ":" + object.toString()));
		}
		/*
		 * References throw the current Abstract->Concrete mapping (ie. the "context")
		 */
		@Override
		public Element caseIntegerVariableRef(IntegerVariableRef object) {
			ISolverElement value = acm.resolveAbstractEntity(object.getReferencedVar());
			if (!(value.getModelElement() instanceof IntegerElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("IntegerVariableRef: IntegerElement expected"));
			}
			return (Element) value.getModelElement();
		}
		
		@Override
		public Element caseRealVariableRef(RealVariableRef object) {
			ISolverElement value = acm.resolveAbstractEntity(object.getReferencedVar());
			if (!(value.getModelElement() instanceof RealElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("RealVariableRef: RealElement expected"));
			}
			return (Element) value.getModelElement();
		}
		
		/*
		 * Boolean expressions : return BooleanElement
		 */
		@Override
		public Element caseAnd(And object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof BooleanElement) ||
					! (right.getModelElement() instanceof BooleanElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("And: BooleanElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getModelElement().getName() + " AND " + right.getModelElement().getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue( ((BooleanElement)left.getModelElement()).getValue()
							&& ((BooleanElement)right.getModelElement()).getValue());
			return res;
		}
		
		@Override
		public Element caseOr(Or object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof BooleanElement) ||
					! (right.getModelElement() instanceof BooleanElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("And: BooleanElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getModelElement().getName() + " AND " + right.getModelElement().getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue( ((BooleanElement)left.getModelElement()).getValue()
							|| ((BooleanElement)right.getModelElement()).getValue());
			return res;
		}
		
		@Override
		public Element caseNot(Not object) {
			BooleanExpression operand = object.getOperand();
			SolverElement operandValue = evaluate(operand);
			if ( ! (operandValue.getModelElement() instanceof BooleanElement) ) {
				throw new SolverWrappedException(new EvaluationTypeError("Not: BooleanElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(object.getName() + ":" + operand.getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue( ! ((BooleanElement) operandValue.getModelElement()).getValue());
			return res;
		}

		@Override
		public Element caseIntEqual(IntEqual object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof IntegerElement) ||
					! (right.getModelElement() instanceof IntegerElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("IntEqual: IntegerElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getModelElement().getName() + "==" + right.getModelElement().getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue(((IntegerElement) left.getModelElement()).getValue()
						== ((IntegerElement) right.getModelElement()).getValue());
			return res;
		}
		
		@Override
		public Element caseIntInf(IntInf object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof IntegerElement) ||
					! (right.getModelElement() instanceof IntegerElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("IntEqual: IntegerElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getModelElement().getName() + "<" + right.getModelElement().getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue(((IntegerElement) left.getModelElement()).getValue() 
							< ((IntegerElement) right.getModelElement()).getValue());
			return res;
		};
		
		@Override
		public Element caseIntSup(IntSup object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof IntegerElement) ||
					! (right.getModelElement() instanceof IntegerElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("IntEqual: IntegerElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName(left.getModelElement().getName() + ">" + right.getModelElement().getName());
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue(((IntegerElement) left.getModelElement()).getValue() 
							> ((IntegerElement) right.getModelElement()).getValue());
			return res;
		};
		
		@Override
		public Element caseSeqIsEmpty(SeqIsEmpty object) {
			SolverElement operand = evaluate(object.getOperand());
			if (! (operand instanceof SolverSequenceElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("SeqIsEmpty: SolverSequenceElement expected"));
			}
			BooleanElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBooleanElement();
			res.setName("isEmpty(" + operand.getModelElement().getName() + ")");
			res.setType(BasicTypePackage.eINSTANCE.getBasicTypeFactory().createBoolean());
			res.setValue(((SolverSequenceElement)operand).isEmpty());
			return res;
		}
		
		/*
		 * Integer expressions : return IntegerElement
		 */
		@Override
		public Element caseIntegerRef(IntegerRef object) {
			return object.getIntegerElem();
		}
		
		@Override
		public Element caseIntPlus(IntPlus object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof IntegerElement) ||
					! (right.getModelElement() instanceof IntegerElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("IntPlus: IntegerElement expected"));
			}
			IntegerElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createIntegerElement();
			res.setValue(((IntegerElement) left.getModelElement()).getValue()
						+ ((IntegerElement) right.getModelElement()).getValue());
			res.setType(left.getElementType());
			return res;
		}
		
		@Override
		public Element caseIntMinus(IntMinus object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof IntegerElement) ||
					! (right.getModelElement() instanceof IntegerElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("IntMinus: IntegerElement expected"));
			}
			IntegerElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createIntegerElement();
			res.setValue(((IntegerElement) left.getModelElement()).getValue()
						- ((IntegerElement) right.getModelElement()).getValue());
			res.setType(left.getElementType());
			return res;			
		}
		
		public Element caseIntMultiply(IntMultiply object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof IntegerElement) ||
					! (right.getModelElement() instanceof IntegerElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("IntMultiply: IntegerElement expected"));
			}
			IntegerElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createIntegerElement();
			res.setValue(((IntegerElement) left.getModelElement()).getValue()
						* ((IntegerElement) right.getModelElement()).getValue());
			res.setType(left.getElementType());
			return res;
		}
		
		@Override
		public Element caseIntDivide(IntDivide object) {
			SolverElement left = evaluate(object.getLeftValue());
			SolverElement right = evaluate(object.getRightValue());
			if (! (left.getModelElement() instanceof IntegerElement) ||
					! (right.getModelElement() instanceof IntegerElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("IntDivide: IntegerElement expected"));
			}
			IntegerElement res = BasicTypePackage.eINSTANCE.getBasicTypeFactory().createIntegerElement();
			res.setValue(((IntegerElement) left.getModelElement()).getValue()
						/ ((IntegerElement) right.getModelElement()).getValue());
			res.setType(left.getElementType());
			return res;
		}
		
		@Override
		public Element caseSeqGetHead(SeqGetHead object) {
			SeqExpression operand = object.getOperand();
			SolverElement value = evaluate(operand);
			if ( ! (value instanceof SolverSequenceElement)) {
				throw new SolverWrappedException(new EvaluationTypeError("SeqGetHead: SolverSequenceElement expected"));
			}
			PrimitiveElement res = ((SolverSequenceElement)value).getHead();
			return evaluate(res).getModelElement();

		}
	};
	
	/*
	 * Object that evaluates only expressions that returns SequenceElement, actually SolverSequenceElement.
	 */
	private ClassicalExpressionSwitch<SolverSequenceElement> seqEvaluator
		= new ClassicalExpressionSwitch<SolverSequenceElement>() {
		/*
		 * Catch all clause
		 */
		@Override
		public SolverSequenceElement caseClassicalExpression(ClassicalExpression object) {
			throw new SolverWrappedException(new UnimplementedEvaluation(object.getName() + ":" + object.toString()));
		}
		@Override
		public SolverSequenceElement caseSeqGetTail(SeqGetTail object) {
			SeqExpression operand = object.getOperand();
			SolverElement value = evaluate(operand);
			if ( ! (value instanceof SolverSequenceElement)) {
				throw new SolverWrappedException( new EvaluationTypeError("SeqGetTail: SolverSequenceElement expected"));
			}
			SolverSequenceElement res = ((SolverSequenceElement)value).getTail();
			return res;

		}
		@Override
		public SolverSequenceElement caseSeqDecr(SeqDecr object) {
			throw new SolverWrappedException(new UnimplementedEvaluation("SeqDecr: unimplemented"));
		}
		@Override
		public SolverSequenceElement caseSeqSched(SeqSched object) {
			throw new SolverWrappedException(new UnimplementedEvaluation("SeqSched: unimplemented"));
		}
		@Override
		public SolverSequenceElement caseNumberSeqVariableRef(NumberSeqVariableRef object) {
			ISolverElement res = acm.resolveAbstractEntity(object.getReferencedVar());
			if (! (res instanceof SolverSequenceElement)) {
				throw new SolverWrappedException( new EvaluationTypeError("NumberSeqVariableRef: SolverSequenceElement expected"));
			}
			return (SolverSequenceElement)res;
		}
	};
	
}
