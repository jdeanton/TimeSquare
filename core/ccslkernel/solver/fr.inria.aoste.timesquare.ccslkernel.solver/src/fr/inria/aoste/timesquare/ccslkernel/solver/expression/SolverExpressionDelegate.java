/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.AbstractRuntimeExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class SolverExpressionDelegate extends SolverExpression {

	private AbstractRuntimeExpression delegate;
	
	public SolverExpressionDelegate() {
	}
	
	public SolverExpressionDelegate(AbstractRuntimeExpression delegate) {
		this.delegate = delegate;
	}
	
	public void setDelegate(AbstractRuntimeExpression delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		if (delegate != null) {
			delegate.start(helper);
		}
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (! canCallSemantic())
			return;
		super.semantic(semanticHelper);
		if (delegate != null) {
			delegate.semantic(semanticHelper);
		}
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		super.deathSemantic(semanticHelper);
		if (delegate != null) {
			delegate.deathSemantic(semanticHelper);
		}
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if (delegate != null) {
			delegate.update(helper);
		}
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		if (! canCallTerminate())
			return;
		super.terminate(helper);
		if (delegate != null) {
			delegate.terminate(helper);
		}
	}
	
	@Override
	public boolean canCallSemantic() {
		return delegate.canCallSemantic();
	}
	
	@Override
	public boolean canCallStart() {
		return delegate.canCallStart();
	}
	
	@Override
	public boolean canCallTerminate() {
		return delegate.canCallTerminate();
	}
	
	@Override
	public boolean canCallUpdate() {
		return delegate.canCallUpdate();
	}
	
	@Override
	public RuntimeClock getExpressionClock() {
		return delegate.getExpressionClock();
	}
	
	@Override
	public void setExpressionClock(RuntimeClock expressionClock) {
		delegate.setExpressionClock(expressionClock);
	}
	
	@Override
	public boolean isActiveState() {
		return delegate.isActiveState();
	}
	
	@Override
	public boolean isDead() {
		return delegate.isDead();
	}
	
	@Override
	public boolean isTerminated() {
		return delegate.isTerminated();
	}
	
	@Override
	public String toString() {
		return delegate.toString();
	}

	@Override
	public SerializedConstraintState dumpState() {
		return delegate.dumpState();
	}
	
	@Override
	public void restoreState(SerializedConstraintState newState) {
		delegate.restoreState(newState);
	}
	
}
