/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.AbstractConcreteMapping;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement;
import fr.inria.aoste.trace.ModelElementReference;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class SolverRelation extends AbstractRuntimeRelation implements ISolverConcrete {
	
	private InstantiatedElement instantiatedElement;
	private AbstractConcreteMapping<ISolverElement> abstractMapping;
	private ModelElementReference traceReference;

	private InstantiationPath instantiationPath;
	private List<ISolverConcrete> subConcretes;
	
	public SolverRelation() {
		super();
		this.instantiatedElement = null;
		this.subConcretes = new ArrayList<ISolverConcrete>();
	}
	
	public AbstractConcreteMapping<ISolverElement> getAbstractMapping() {
		return abstractMapping;
	}

	public void setAbstractMapping(AbstractConcreteMapping<ISolverElement> abstractMapping) {
		this.abstractMapping = abstractMapping;
	}

	public InstantiationPath getInstantiationPath() {
		return instantiationPath;
	}

	public void setInstantiationPath(InstantiationPath arrayList) {
		this.instantiationPath = arrayList;
	}

	public InstantiatedElement getInstantiatedElement() {
		return instantiatedElement;
	}

	public void setInstantiatedElement(InstantiatedElement instantiatedElement) {
		this.instantiatedElement = instantiatedElement;
	}

	public ModelElementReference getTraceReference() {
		return traceReference;
	}

	public void setTraceReference(ModelElementReference traceReference) {
		this.traceReference = traceReference;
	}

	public List<ISolverConcrete> getSubConcretes() {
		return subConcretes;
	}
	
	public String getNameFromPath(String separator) {
		String res = "";
		for (Iterator<NamedElement> it = instantiationPath.iterator(); it.hasNext(); ) {
			res += it.next().getName();
			if (it.hasNext()) {
				res += separator;
			}
		}
		return res;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		for (ISolverConcrete subs : subConcretes) {
			subs.start(helper);
		}
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		for (ISolverConcrete subs : subConcretes) {
			subs.semantic(semanticHelper);
		}
		if ( ! canCallSemantic())
			return;
		super.semantic(semanticHelper);
		if ( isAssertion() ) {
			assertionSemantic(semanticHelper);
		}
	}

	public void assertionSemantic(AbstractSemanticHelper semanticHelper) {
		// Locate all the sons that are relations, build a AND of all their assertion variables and
		// generate an equality
		if (subConcretes.isEmpty())
			return;
		BuDDyBDD andTerm = semanticHelper.createOne();
		for (ISolverConcrete subs : subConcretes) {
			if (subs instanceof SolverRelation && ((SolverRelation) subs).isAssertion()) {
				BuDDyBDD var = semanticHelper.getAssertionVariable((SolverRelation) subs);
				andTerm = semanticHelper.createAnd(andTerm, var);
			}
		}
		semanticHelper.assertionSemantic(this, andTerm);
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		super.deathSemantic(semanticHelper);
		for (ISolverConcrete subs : subConcretes) {
			subs.deathSemantic(semanticHelper);
		}
	}

	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		for (ISolverConcrete subs : subConcretes) {
			subs.update(helper);
		}
		if ( ! canCallUpdate())
			return;
		super.update(helper);
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		super.terminate(helper);
		for (ISolverConcrete subs : subConcretes) {
			subs.terminate(helper);
		}
	}

	@Override
	public boolean isTerminated() {
		return false;
	}

	@Override
	protected ICCSLConstraint[] getConstraints() {
		return (ICCSLConstraint[]) Collections.emptyList().toArray();
	}

	@Override
	public List<RuntimeClock> getDiscreteClocks() {
		return Collections.emptyList();
	}

	@Override
	public SerializedConstraintState dumpState() {
		return new SerializedConstraintState();
	}

	@Override
	public void restoreState(SerializedConstraintState state) {
		return;
	}
	
}
