/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.NoBooleanSolution;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.BDDHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.AbstractStepExecutionEngine;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.CCSLModel.SolverBlock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.CCSLModel.SolverBlockTransition;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;
import fr.inria.aoste.timesquare.ccslkernel.solver.helpers.SemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.helpers.UpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelationWrapper;
import fr.inria.aoste.timesquare.ccslkernel.solver.statistics.SolverRuntimeStats;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;
import fr.inria.aoste.trace.AssertionState;
import fr.inria.aoste.trace.EnableStateKind;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.TracePackage;
import net.sf.javabdd.BuDDyFactory;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class StepExecutor extends AbstractStepExecutionEngine {

	private CCSLKernelSolver solver;
		
	private List<ISolverConcrete> semanticDoneConcretes;
	private LogicalStep traceStep;

	
	public  List<LogicalStep> reinitializeCurrentStepBDD() throws SimulationException{
		if (semanticBdd != null){
			semanticBdd.free();
		}
		return computeAndGetPossibleLogicalSteps();
	}

	public StepExecutor(CCSLKernelSolver solver) {
		this.solver = solver;
		semanticBdd = solver.BuDDyFactory.one();
		this.semanticDoneConcretes = new ArrayList<ISolverConcrete>();
		initHelpers();
	}
	
	@Override
	public void clearStepData() {
		super.clearStepData();
		semanticBdd = solver.BuDDyFactory.one();
	}

	@Override
	protected SemanticHelper createSemanticHelper() {
		return new SemanticHelper(solver, this);
	}
	
	@Override
	protected UpdateHelper createUpdateHelper() {
		return new UpdateHelper(this);
	}
	
	@Override
	public List<RuntimeClock> getAllDiscreteClocks() {
		return solver.getAllDiscreteClocks();
	}
	
	public List<LogicalStep> computeAndGetPossibleLogicalSteps() throws SimulationException {
    	computePossibleClockStates();
		return getAllSolutions();
    }
        
	public Map<Integer, Integer> getBddVarToIndexInSolution() {
		return solver.bddVarToIndexInSolution;
	}
	
	/*
	 * Mal nomme, fait la meme chose que {@link #applyLogicalStepByIndex(int)} mais construit
	 * en plus une chaine qui contient la liste des noms des clocks ayant ticke. 
	 */
    public ArrayList<String> lightApplyLogicalStepByIndex(int indexOfStepToApply) throws SimulationException {
    	fired = allClockTraceStateSet.get(indexOfStepToApply);
		enabled = allClockTraceStateSet.get(indexOfStepToApply);
		rewriteSemanticsAndPropagateDeath();
		
    	ArrayList<String> res = new ArrayList<String>();
		for (RuntimeClock clock : getAllDiscreteClocks()) {
			if (getUsedClocks().contains(clock) && getClockFiredState(clock) == ClockTraceState.T) {
				if(! (clock instanceof ImplicitClock)){
					res.add(clock.getName());
				}
			}
		}
		return res;
    }
    
	public void freeAll(){
		/*
		 * "Requests" from environment are collected when the backend(s) are executed. Such requests are
		 * intended to be used at the next simulation step : we release the BuDDyBDD used at the current step
		 * and create a new one that will be used at the next step. 
		 */
    	// free the bdd used for semantics
		semanticBdd.free();
		this.semanticHelper = null;
		this.semanticHelper = createSemanticHelper();
		//free collection of solutions
		//allPossibleLogicalStep.clear();
		allClockTraceStateSet.clear();
	}
	
	@Override
	public void stepPreHook() {
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.enterMethod(getClass().getName() + ".executeStep");
	}
	
	@Override
	protected void stepPostHook() {
		traceStep = solver.getTraceFactory().createLogicalStep();
		fillTraceStep(solver, traceStep);
		
		dealWithBlockTransition();

		
		freeAll();
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.leaveMethod(getClass().getName() + ".executeStep");
	}
	
	@Override
	public Set<RuntimeClock> getDeadClocks() {
		return solver.getDeadClocks();
	}
	
	@Override
	public void addDeadClock(RuntimeClock clock) {
		solver.addDeadClock(clock);
	}
		
	@Override
	protected void constructCurrentStepBDD() throws SimulationException {
		
		semanticDoneConcretes.clear();
		/* uses the fresh bdd  create in the constructor for the semantic equations. During traversal of all the CCSL expressions and
		 * relations, all the individual boolean expressions will be AND'ed to this BuDDyBDD using the
		 * semanticBDDAnd(BuDDyBDD newTerm) function.
		 * Use a bdd (stored in the solver) that allows to force clock presence or absence depending on the
		 * execution/simulation environment. If this BuDDyBDD is not initialized, create a new one initialized to 1.
		*/
		
		
		buildBooleanExpressions();
		
		// create and add a disjunction of all clocks to avoid a solution where no clock ticks
		BuDDyBDD clockDisjunction = buildAllClocksDisjunction();
		semanticBdd.andWith(clockDisjunction);
		
		//filter the solutions of the BuDDyBDD according to a priority specification
		if (solver.getPrioritySpecification() != null && solver.getPrioritySpecification().getPriorityRelations().size() > 0){
			semanticBdd.andWith(solver.getPrioSolver().ResolvePrio(semanticBdd, usedClocks));
		}
		
	}
	
	private void buildBooleanExpressions() throws SimulationException {
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.enterMethod(getClass().getName() + ".buildBooleanExpressions");
		for (SolverBlock topBlock : solver.getTopBlocks()) {
			buildBooleanExpressionsInBlock(topBlock);
		}
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.leaveMethod(getClass().getName() + ".buildBooleanExpressions");
	}
	private void buildBooleanExpressionsInBlock(SolverBlock block) throws SimulationException {
		for (ISolverConcrete conc : block.getConcretes()) {
			if (conc instanceof SolverRelation || conc instanceof SolverRelationWrapper){
				conc.semantic(semanticHelper);
			}
		}
		
		for (ISolverConcrete conc : block.getConcretes()) {
			if (conc instanceof SolverExpression){
				conc.semantic(semanticHelper);
			}
		}
		
		// Concrete clocks are always considered as "active"
		for (SolverClock clock : block.getConcreteClocks()) {
			usedClocks.add(clock);
		}
		for (SolverBlock subBlock : block.getActiveSubBlocks()) {
			buildBooleanExpressionsInBlock(subBlock);
		}
	}

	private BuDDyBDD buildAllClocksDisjunction() {
		BuDDyBDD clockDisjunction = this.solver.BuDDyFactory.zero();
		for (RuntimeClock clock : usedClocks) {
			BuDDyBDD bddClockVar = this.solver.BuDDyFactory.ithVar(clock.bddVariableNumber);
			clockDisjunction.orWith(bddClockVar);
		}
		return clockDisjunction;
	}

	/**
	 * Adds newTerm as a new conjunct to the semantic BuDDyBDD. The newTerm BuDDyBDD is consumed according to
	 * javaBDD : it becomes an invalid BuDDyBDD that cannot be used again.
	 * 
	 * @see net.BuDDyBDD.BDD#andWith(BuDDyBDD)
	 * @param newTerm
	 */
	public void semanticBDDAnd(BuDDyBDD newTerm) {
		semanticBdd.andWith(newTerm);
	}
		
	public ArrayList<LogicalStep> getAllSolutions() throws NoBooleanSolution{
		allClockTraceStateSet = getAllBDDSolutions();
		ArrayList<LogicalStep> allNextStepSolutions = new ArrayList<LogicalStep>();
		//for all computed solution, we create a logical step
		for (ClockTraceStateSet clockTraceStateSet : allClockTraceStateSet) {
			LogicalStep newSolution = TracePackage.eINSTANCE.getTraceFactory().createLogicalStep();
			allNextStepSolutions.add(newSolution);
			
			//warning we temporarily set fired and enabled to the current not chosen solution
			fired = clockTraceStateSet;
			enabled = clockTraceStateSet;
			//for all the system clock, if used, we add it in the logicalStep
			for (RuntimeClock clock : solver.getAllDiscreteClocks()) {
				if (usedClocks.contains(clock)) {
					setTraceClockState((SolverClock) clock, newSolution);
				}
			}
		}
		//we set back the chosen solution to unset
		fired = null;
		enabled = null;
		
		return allNextStepSolutions;
	}
	
	/**
	 * boolean value used to avoid stopping the exploration in explorer mode
	 */
	public boolean inSimulationMode = true;
	
	@Override
	public ArrayList<ClockTraceStateSet> getAllBDDSolutions() throws NoBooleanSolution {
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.enterMethod(getClass().getName() + ".getAllSolutions");
				
		if (semanticBdd.isZero()) {
			System.err.println("there is a deadlock ! no solution can be found");
			if (CCSLKernelSolver.runtimeStatsCollection) {
				SolverRuntimeStats.leaveMethod(getClass().getName() + ".getAllSolutions");
			}
			//if (inSimulationMode) {
				// here we do not throws exception but returns an empty list. It is easier to deal with that and go back in the history
				//throw new NoBooleanSolution("There is a deadlock, no solution can be found");
			//}
			//else {
			System.err.println("no more futures are found in the model");
				return new ArrayList<ClockTraceStateSet>();
			//}
		}
		
		
		//construct the var set 
		BuDDyBDD varSet = solver.BuDDyFactory.one();
		
		for (Iterator<Integer> iterator = solver.bddVarToIndexInSolution.keySet().iterator(); iterator.hasNext();) {
			Integer current = (Integer) iterator.next();
			varSet = varSet.and(solver.BuDDyFactory.ithVar(current));
		}
		
		//get an iterator on all satisfying solutions
		Iterator<?> it = semanticBdd.iterator(varSet);
		
		//for debugging purpose
		//double nbSolution = semanticBdd.satCount(varSet);
		//System.out.println("number of  Solution: "+nbSolution);
		
		ArrayList<ClockTraceStateSet> allSolutionsF = new ArrayList<ClockTraceStateSet>();
		
		int allClockSize=solver.bddVarToIndexInSolution.size();
		while(it.hasNext()){
			ClockTraceStateSet tempF = new ClockTraceStateSet(allClockSize);
			ClockTraceStateSet tempE = new ClockTraceStateSet(allClockSize);
			BuDDyBDD v = (BuDDyBDD)it.next();
			for (Iterator<Integer> iterator = solver.bddVarToIndexInSolution.keySet().iterator(); iterator.hasNext();) {
				int bddVarNumber = (Integer) iterator.next();
				int index=0;
				
				index = solver.bddVarToIndexInSolution.get(bddVarNumber);
				
				if ( ! v.and(solver.BuDDyFactory.ithVar(bddVarNumber)).isZero()) {
					tempF.set(index, ClockTraceState.T);
					tempE.set(index, ClockTraceState.T);
				} else {
					tempF.set(index, ClockTraceState.F);
					tempE.set(index, ClockTraceState.F);
				}
			}
			
			allSolutionsF.add((ClockTraceStateSet)tempF);
		}
		
		varSet.free();
		
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.leaveMethod(getClass().getName() + ".getAllSolutions");
		
		allClockTraceStateSet = allSolutionsF;
		return allSolutionsF;
	}
	
	@Override
	public SimulationPolicyBase getSimulationPolicy() {
		return solver.getPolicy();
	}
	
	@Override
	protected BuDDyFactory getBuddyFactory() {
		return solver.getBuddyFactory();
	}
	
	@Override
	protected void rewriteExpressions() throws SimulationException {
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.enterMethod(getClass().getName() + ".rewriteExpressions");
		for (SolverBlock block : solver.getTopBlocks()) {
			rewriteExpressionsInBlock(block);
		}
		for (SolverBlock block : solver.getTopBlocks()) {
			findDeadClocks(block);
		}
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.leaveMethod(getClass().getName() + ".rewriteExpressions");
	}
	
	private void rewriteExpressionsInBlock(SolverBlock block) throws SimulationException {	
		for (SolverClock clock : block.getConcreteClocks()) {
			if (getClockFiredState(clock) == ClockTraceState.T) {
				registerClockFiring(clock);
			}
			if (getClockEnabledState(clock) == ClockTraceState.T) {
				enabledClocks.add(clock);
			}
		}
		for (ImplicitClock clock : block.getImplicitClocks()) {
			if (getClockFiredState(clock) == ClockTraceState.T) {
				registerClockFiring(clock);
			}
			if (getClockEnabledState(clock) == ClockTraceState.T) {
				enabledClocks.add(clock);
			}
		}
		for (ISolverConcrete concrete : block.getConcretes()) {
			concrete.update(updateHelper);
		}
		for (SolverBlock sub : block.getActiveSubBlocks()) {
			rewriteExpressionsInBlock(sub);
		}
	}
	
	private void findDeadClocks(SolverBlock block) {
		for (SolverClock clock : block.getConcreteClocks()) {
			if (clock.isDead()) {
				newDeadClocks.add(clock);
			}
		}
		for (ImplicitClock clock : block.getImplicitClocks()) {
			if (clock.isDead()) {
				newDeadClocks.add(clock);
			}
		}
		for (SolverBlock sub : block.getSubBlocks()) {
			findDeadClocks(sub);
		}
		
	}

	@Override
	protected void buildDeathExpressions()  throws SimulationException {
		for (SolverBlock block : solver.getTopBlocks()) {
			buildDeathExpressions(block);
		}
	}

	private void buildDeathExpressions(SolverBlock block) throws SimulationException {
		for (ISolverConcrete concrete : block.getConcretes()) {
			concrete.deathSemantic(semanticHelper);
		}
		for (SolverBlock sub : block.getActiveSubBlocks()) {
			buildDeathExpressions(sub);
		}
	}
	
//	@Override
//	public Set<RuntimeClock> getFiredClocks() {
//		return firedClocks;
//	}
//		
//	@Override
//	public Set<RuntimeClock> getEnabledClocks() {
//		return enabledClocks;
//	}
	
	public synchronized List<ISolverConcrete> getSemanticDoneConcretes() {
		return semanticDoneConcretes;
	}

//	public Set<RuntimeClock> getNewDeadClocks() {
//		return newDeadClocks;
//	}

//	public Set<RuntimeClock> getResurrectedClocks() {
//		return resurrectedClocks;
//	}

	public LogicalStep getTraceStep() {
		return traceStep;
	}

	private void fillTraceStep(CCSLKernelSolver ccslKernelSolver, LogicalStep step) {
		for (SolverBlock block : solver.getTopBlocks()) {
			setTraceStates(block, step);
		}
	}
	
	private void setTraceStates(SolverBlock block, LogicalStep step) {
		for (SolverClock cl : block.getConcreteClocks()) {
			setTraceClockState(cl, step);
		}
		for (ImplicitClock cl : block.getImplicitClocks()) {
			setTraceClockState(cl, step);
		}
		for (ISolverConcrete concrete : block.getConcretes()) {
			if ( (concrete instanceof SolverRelation)
					&& ((SolverRelation)concrete).isAssertion() ) {
				setAssertionState((SolverRelation) concrete, step);
			}
		}
		for (SolverBlock sub : block.getActiveSubBlocks()) {
			setTraceStates(sub, step);
		}
	}
	
	private void setTraceClockState(SolverClock clock, LogicalStep step) {
		EventOccurrence eventOcc = TracePackage.eINSTANCE.getTraceFactory().createEventOccurrence();
		step.getEventOccurrences().add(eventOcc);
		eventOcc.setCounter(clock.tickCount);
		eventOcc.setReferedElement(clock.traceReference);
		// At the time the internal structures of the solver are built and initialized, each clock has a
		// ModelElementReference created, but when used from the launcher the CCSLKernelSolverWrapper.setListReference()
		// function has not yet been called, so these Reference objects are not added to this list.
		// Normally, when the setListReference() is called, then all useful ModelElementReference are collected and added
		// to the list used by the trace.
		// The following test is there to ensure that all encountered Reference are properly attached to the trace. 
		if ((solver.getTraceReferences() != null) &&
				(! solver.getTraceReferences().contains(clock.traceReference))) {
			solver.getTraceReferences().add(clock.traceReference);
		}
		
		//deal with birth
		if (clock instanceof ImplicitClock){
			ImplicitClock ic = (ImplicitClock) clock;
			eventOcc.setWasBorn(((SolverExpression) ic.getExpression()).wasBorn());
		} else {
			eventOcc.setWasBorn(true);
		}
		//deal with dead
		if (solver.getDeadClocks().contains(clock)) {
			eventOcc.setIsClockDead(true);
		}
		else {
			eventOcc.setIsClockDead(false);
		}
		if (getClockFiredState(clock) == ClockTraceState.T) {
			eventOcc.setFState(FiredStateKind.TICK);
		}
		else if (getClockFiredState(clock) == ClockTraceState.F) {
			eventOcc.setFState(FiredStateKind.NO_TICK);
		}
		if (getClockEnabledState(clock) != null) {
			switch (getClockEnabledState(clock)) {
			case F:
				eventOcc.setEState(EnableStateKind.NO_TICK); break;
			case T:
				eventOcc.setEState(EnableStateKind.TICK); break;
			case X:
				eventOcc.setEState(EnableStateKind.INDETERMINED); break;
			case x:
				eventOcc.setEState(EnableStateKind.FREE); break;
			default:
				break;
			}
		}
		else { //the expression is not born yet, so it does not tick
			eventOcc.setEState(EnableStateKind.NO_TICK);
			eventOcc.setFState(FiredStateKind.NO_TICK);
		}
	}

	private void setAssertionState(SolverRelation assertion, LogicalStep step) {
		if (isAssertionViolated(assertion)) {
			ModelElementReference ref = solver.findReference(assertion.getInstantiatedElement());
			if (ref == null) {
				return;
			}
			AssertionState aState = TracePackage.eINSTANCE.getTraceFactory().createAssertionState();
			aState.setIsViolated(true);
			aState.setReferedElement(ref);
			step.getAssertionStates().add(aState);
		}
	}
	
	public boolean isAssertionViolated(String qualifiedPath) {
		return isAssertionViolated(qualifiedPath, InstantiationPath.defaultSeparator);
	}
	
	public boolean isAssertionViolated(String qualifiedPath, String separator) {
		ISolverConcrete concrete = solver.getConcreteInstantiationTree().lookupInstance(qualifiedPath, separator);
		if (concrete != null && concrete instanceof SolverRelation) {
			if (((SolverRelation)concrete).isAssertion()) {
				return isAssertionViolated((SolverRelation) concrete);
			}
		}
		return false;
	}
	
	private boolean isAssertionViolated(SolverRelation assertion) {
		//assertion states are put after the clock states
		return ( fired.get(solver.assertionsToIndexInSolution.get(assertion)) == ClockTraceState.F );
	}

	public AbstractUpdateHelper getUpdateHelper() {
		return updateHelper;
	}

	@Override
	public void dealWithBlockTransition() {
		ArrayList<SolverBlockTransition> transitionToFire = new ArrayList<SolverBlockTransition>();
		 for (SolverBlockTransition sbt : solver.getAllBlockTransitions()) {
				
				if (getClockEnabledState(sbt.getOnClock()) == ClockTraceState.T) {
					if (sbt.getSource().isActiveBlock()){
						transitionToFire.add(sbt);
					}
				}
			}
		 for(SolverBlockTransition sbt : transitionToFire){
			 try {
					sbt.getSource().terminate(updateHelper);
				} catch (SimulationException e) {
						e.printStackTrace();
				}
			try {
				sbt.getTarget().start(semanticHelper);
				} catch (SimulationException e) {
					e.printStackTrace();
				}
		 }
	}
	
}
