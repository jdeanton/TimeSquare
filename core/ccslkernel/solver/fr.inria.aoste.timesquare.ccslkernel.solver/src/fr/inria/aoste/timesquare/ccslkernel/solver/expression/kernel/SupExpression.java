/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

@Deprecated
public class SupExpression extends SolverExpression {

	private SolverClock clock1;
	private SolverClock clock2;
	private int _deltaWithC1;// implicitClock.tickCount - (clock1.tickCount -
								// clock1.tickCount@self.birth)
	private int _deltaWithC2;// implicitClock.tickCount - (clock2.tickCount -
								// clock2.tickCount@self.birth)
	private boolean updateDone;

	public SupExpression() {
		super();
		_deltaWithC1 = 0;
		_deltaWithC2 = 0;
	}

	public SolverClock getClock1() {
		return clock1;
	}

	public void setClock1(SolverClock clock1) {
		this.clock1 = clock1;
	}

	public SolverClock getClock2() {
		return clock2;
	}

	public void setClock2(SolverClock clock2) {
		this.clock2 = clock2;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
			super.start(helper);
			updateDone = true;
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if ( ! canCallSemantic() /* isActiveState() && state != State.DEAD */) {
			return;
		}
		super.semantic(semanticHelper);
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		clock1.semantic(semanticHelper);
		clock2.semantic(semanticHelper);
		if (state == State.DEAD) {
			semanticHelper.inhibitClock(getImplicitClock());
		}
		else {
//			wasBorn = true;
			updateDone = false;
			if (_deltaWithC1 == 0) {
				if (_deltaWithC2 == 0) {
					semanticHelper.semanticBDDAnd(semanticHelper.createEqual(
							getImplicitClock(),
							semanticHelper.createIntersection(getClock1(), getClock2())));
				}
				else {
					semanticHelper.semanticBDDAnd(semanticHelper.createEqual(
							getImplicitClock(), getClock1()));
				}
			}
			else {
				if (_deltaWithC2 == 0) {
					semanticHelper.semanticBDDAnd(semanticHelper.createEqual(getImplicitClock(), getClock2()));
				}
				else {
					semanticHelper.inhibitClock(getImplicitClock());
				}
			}
		}
		semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(), getClock1(), getClock2() });
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if ( ! isActiveState()) {
			return;
		}
		if ((state == State.DEAD) || updateDone) {
			return;
		}
		clock1.update(helper);
		clock2.update(helper);
		ImplicitClock eClock = getImplicitClock();
		if (helper.clockHasFired(eClock)) {
			_deltaWithC1++;
			_deltaWithC2++;
		}
		if (helper.clockHasFired(clock1)) {
			_deltaWithC1--;
		}
		if (helper.clockHasFired(clock2)) {
			_deltaWithC2--;
		}

		updateDone = true;
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		super.deathSemantic(semanticHelper);
		clock1.deathSemantic(semanticHelper);
		clock2.deathSemantic(semanticHelper);
		semanticHelper.registerDeathConjunctionImplies(clock1, clock2, getImplicitClock());
	}
	
	@Override
	public String toString() {
		return "[" + getImplicitClock().getName() + "]Sup(" + clock1.getName() + ", "
				+ clock2.getName() + ")";
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
//		currentState.add(Serializer.getDefaultSerializer().toBytes(new Integer(_deltaWithC1)));
//		currentState.add(Serializer.getDefaultSerializer().toBytes(new Integer(_deltaWithC2)));
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
//		_deltaWithC1 = (Integer) Serializer.getDefaultSerializer().fromBytes(newState.get(2));
//		_deltaWithC2 = (Integer) Serializer.getDefaultSerializer().fromBytes(newState.get(3));
	}
	

}
