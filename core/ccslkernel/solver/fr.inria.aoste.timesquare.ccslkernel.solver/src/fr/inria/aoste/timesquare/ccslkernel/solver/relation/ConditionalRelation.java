/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.EvaluationTypeError;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ClassicalExpressionEvaluator;
import fr.inria.aoste.timesquare.ccslkernel.solver.ConditionalCase;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.SolverElement;

public class ConditionalRelation extends SolverRelation implements
		ISolverConcrete {

	private List<ConditionalCase> cases;
	private List<ISolverConcrete> defaultCase;
	private List<ImplicitClock> defaultCaseClocks;
	private ConditionalCase selectedCase;
	
	public ConditionalRelation() {
		this.cases = new ArrayList<ConditionalCase>();
		this.defaultCase = new ArrayList<ISolverConcrete>();
		this.defaultCaseClocks = new ArrayList<ImplicitClock>();
	}
	
	public List<ConditionalCase> getCases() {
		return cases;
	}

	public List<ISolverConcrete> getDefaultCase() {
		return defaultCase;
	}

	public List<ImplicitClock> getDefaultCaseClocks() {
		return defaultCaseClocks;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		for (ConditionalCase relationCase : getCases()) {
			for (ISolverConcrete conc : relationCase.getConcretes()) {
				conc.start(helper);
			}
			for (ImplicitClock clock : relationCase.getImplicitClocks()) {
				if (clock.getRefCount() == 0) {
					clock.start(helper);
				}
			}
		}
		for (ISolverConcrete conc : getDefaultCase()) {
			conc.start(helper);
		}
		for (ImplicitClock clock : getDefaultCaseClocks()) {
			if (clock.getRefCount() == 0) {
				clock.start(helper);
			}
		}
		for (ConditionalCase relationCase : getCases()) {
			BooleanExpression condition = relationCase.getCondition();
			ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(getAbstractMapping());
			SolverElement test = eval.evaluate(condition);
			if ( ! (test.getModelElement() instanceof BooleanElement)) {
				throw new EvaluationTypeError("ConditionalRelation.semantic(): BooleanElement expected");
			}
			if (((BooleanElement) test.getModelElement()).getValue()) {
				selectedCase = relationCase;
				break;
			}
		}
		
		
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		boolean goDefault = true;
		selectedCase = null;
		for (ConditionalCase relationCase : getCases()) {
			BooleanExpression condition = relationCase.getCondition();
			ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(getAbstractMapping());
			SolverElement test = eval.evaluate(condition);
			if ( ! (test.getModelElement() instanceof BooleanElement)) {
				throw new EvaluationTypeError("ConditionalRelation.semantic(): BooleanElement expected");
			}
			if (((BooleanElement) test.getModelElement()).getValue()) {
				selectedCase = relationCase;
				for (ISolverConcrete conc : relationCase.getConcretes()) {
					conc.semantic(semanticHelper);
				}
				goDefault = false;
				break;
			}
		}
		if ( goDefault) {
			for (ISolverConcrete conc : getDefaultCase()) {
				conc.semantic(semanticHelper);
			}
		}
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (selectedCase != null) {
			for (ISolverConcrete conc : selectedCase.getConcretes()) {
				conc.deathSemantic(semanticHelper);
			}
		}
		else {
			for (ISolverConcrete conc : getDefaultCase()) {
				conc.deathSemantic(semanticHelper);
			}
		}
	}
	
	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if (selectedCase != null) {
			for (ISolverConcrete conc : selectedCase.getConcretes()) {
				conc.update(helper);
			}
		}
		else {
			for (ISolverConcrete conc : getDefaultCase()) {
				conc.update(helper);
			}
		}
	}
	
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		Integer selectedCaseIndex = cases.indexOf(selectedCase);
		currentState.dump(selectedCaseIndex);
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
		int index = (Integer) newState.restore(0);
		if (index >= 0) {
			selectedCase = cases.get(index);
		} else {
			selectedCase = null;
		}
	}
	

}
