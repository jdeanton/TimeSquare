/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver;

import javax.xml.crypto.NoSuchMechanismException;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;

public class SolverPrimitiveElement extends SolverElement {


	public SolverPrimitiveElement(InstantiatedElement instantiatedElement, PrimitiveElement primElement) {
		super(instantiatedElement, primElement);
	}

	public SolverPrimitiveElement(InstantiatedElement instantiatedElement) {
		super(instantiatedElement);
	}
	
	public SolverPrimitiveElement(PrimitiveElement primElement) {
		super(primElement);
	}
	
	public PrimitiveElement getPrimitiveElement() {
		return (PrimitiveElement) getModelElement();
	}
		
	@Override
	public String toString() {
		return "Primitive:" + getPrimitiveElement().toString();
	}

	@Override
	public SerializedConstraintState dumpState() {
		throw new NoSuchMechanismException("primitive element should have to give their states");
	}

	@Override
	public void restoreState(SerializedConstraintState state) {
		throw new NoSuchMechanismException("primitive element should have to set their states");
	}
}
