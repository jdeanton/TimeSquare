/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.exception;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;

public class SolverException extends SimulationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1626244416694226889L;

	public SolverException() {
	}

	public SolverException(String message) {
		super(message);

	}

	public SolverException(Throwable cause) {
		super(cause);

	}

	public SolverException(String message, Throwable cause) {
		super(message, cause);

	}

}
