/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver;

import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Type;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;

public class SolverElement implements ISolverConcrete, ISolverElement, IRuntimeContainer {
	
	public SolverElement() {
		this.instantiatedElement = null;
		this.modelElement = null;
	}
	
	public SolverElement(InstantiatedElement instantiatedElement) {
		super();
		this.instantiatedElement = instantiatedElement;
		NamedElement last = instantiatedElement.getInstantiationPath().getLast();
		if (last instanceof Element) {
			this.modelElement = (Element)last;
		}
	}

	public SolverElement(Element modelElement) {
		super();
		this.modelElement = modelElement;
		this.instantiatedElement = null;
	}
	
	public SolverElement(InstantiatedElement instantiatedElement, Element modelElement) {
		super();
		this.instantiatedElement = instantiatedElement;
		this.modelElement = modelElement;
	}
	
	private InstantiatedElement instantiatedElement;
	private Element modelElement;

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement#getInstantiatedElement()
	 */
	@Override
	public InstantiatedElement getInstantiatedElement() {
		return instantiatedElement;
	}
	
	@Override
	public void setInstantiatedElement(InstantiatedElement ie) {
		instantiatedElement = ie;
	}
	
	public Element getModelElement() {
		return modelElement;
	}
	
	@Override
	public void setModelElement(NamedElement elt) {
		this.modelElement = (Element) elt;
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement#getElementType()
	 */
	@Override
	public Type getElementType() {
		return modelElement.getType();
	}
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {		
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
	}

	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
	}

	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
	}

	@Override
	public boolean isTerminated() {
		return false;
	}

	@Override
	public SerializedConstraintState dumpState() {
		return new SerializedConstraintState();
	}

	@Override
	public void restoreState(SerializedConstraintState state) {		
	}

	private IRuntimeContainer parent;
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null) {
			parent.addContent(this);
		}
	}

	@Override
	public List<IRuntimeContainer> getContents() {
		return Collections.emptyList();
	}

	@Override
	public void addContent(IRuntimeContainer element) { }
	
	

}
