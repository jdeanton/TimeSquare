/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation.kernel;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimeCoincides;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimeExclusion;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimeNonStrictPrecedes;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimePrecedes;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.RuntimeSubClock;

public class SolverRuntimeRelationFactory extends KernelRelationSwitch<AbstractRuntimeRelation> {

	private SolverRuntimeRelationFactory() {
	}

	static public final SolverRuntimeRelationFactory INSTANCE = new SolverRuntimeRelationFactory();
	
	public AbstractRuntimeRelation createRuntimeKernelRelation(KernelRelationDeclaration declaration,
				RuntimeClock leftClock, RuntimeClock rightClock) {
		this.leftClock = leftClock;
		this.rightClock = rightClock;
		return doSwitch(declaration);
	}
	
	private RuntimeClock leftClock;
	private RuntimeClock rightClock;
	
	@Override
	public AbstractRuntimeRelation caseCoincidence(Coincidence object) {
		return new RuntimeCoincides(leftClock, rightClock);
	}
	
	@Override
	public AbstractRuntimeRelation caseExclusion(Exclusion object) {
		return new RuntimeExclusion(leftClock, rightClock);
	}
	
	@Override
	public AbstractRuntimeRelation caseNonStrictPrecedence(NonStrictPrecedence object) {
		return new RuntimeNonStrictPrecedes(leftClock, rightClock);
	}
	
	@Override
	public AbstractRuntimeRelation casePrecedence(Precedence object) {
		return new RuntimePrecedes(leftClock, rightClock);
	}
	
	@Override
	public AbstractRuntimeRelation caseSubClock(SubClock object) {
		return new RuntimeSubClock(leftClock, rightClock);
	}
	
}
