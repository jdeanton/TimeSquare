//package fr.inria.aoste.timesquare.ccslkernel.solver;
//
//import org.sat4j.core.VecInt;
//import org.sat4j.specs.ContradictionException;
//import org.sat4j.specs.TimeoutException;
//import org.sat4j.tools.ModelIterator;
//
//import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.BDDHelper;
//
//public class TESTBDD {
//	
//	
//	
//	
//	public static void main(String[] args) {
//		BDDHelper helper = new BDDHelper();
////		int v1 = helper.newBDDVariableNumber();
////		int v2 = helper.newBDDVariableNumber();
//		try {
////			helper.createExclusion(v1, v2, true);
//			
////			int v1True = helper.createConjunction(v1, helper.root, false);
////			int v1False = helper.createConjunction(-v1, helper.root, false);
////			int[] v1TC = {v1True};
////			int[] v1FC = {v1False};
////			helper.bddSolver.isSatisfiable(new VecInt(v1TC));
////			helper.bddSolver.isSatisfiable(new VecInt(v1FC));
////			
////			int v2True = helper.createConjunction(v2, helper.root, false);
////			int v2False = helper.createConjunction(-v2, helper.root, false);
////			int[] v2TC = {v2True};
////			int[] v2FC = {v2False};
////			helper.bddSolver.isSatisfiable(new VecInt(v2TC));
////			helper.bddSolver.isSatisfiable(new VecInt(v2FC));
//			//			int v1andTrue = helper.createConjunction(v1, helper.createOne(), false);
//
//			
//			/**
//			 * this is a non sat !
//			 */
//			int res = helper.createFalse();
//			res = helper.createDisjunction(res, helper.createOne(), false);
//		
//			/**
//			 * this is a sat 
//			 */
////			int res = helper.createOne();
////			helper.createConjunction(res, res, helper.createOne());
//			
//			
//			
//			
//			ModelIterator modit = new ModelIterator(helper.bddSolver);
//		
//			int i =0;
//			int[] model = null;
//			int[] assumption = {res}; // the spec is true
//			while(modit.isSatisfiable(new VecInt(assumption)))
//			{
//				model = modit.modelWithInternalVariables();
//				int[] pi = modit.primeImplicant(); //this is the guy that consume the model !
//				if (model != null) {
//					System.out.print(i+++": ");
//					for(int c : model) {
//						System.out.print(c+", ");
//					}
//					System.out.print("   (");
//					for(int c : pi) {
//						System.out.print(c+", ");
//					}
//					System.out.print(")");
//					System.out.println(" ");
//				}
//				else {System.out.println("model null");}
//			}
//		} catch (TimeoutException e) {
//			e.printStackTrace();
//		} catch (ContradictionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	/**
//	 * ca marche comme l'ancien. On cree des var
//	 * , elles sont indépendantes du bdd jusqu'à 
//	 * ce que l'on les mette en conjunction avec. On a 
//	 * donc deux API, une qui prend le res en 
//	 * param et qui fait la conjunction directe et l'autre qi cré un BDD isolé
//	 */
//
//}
