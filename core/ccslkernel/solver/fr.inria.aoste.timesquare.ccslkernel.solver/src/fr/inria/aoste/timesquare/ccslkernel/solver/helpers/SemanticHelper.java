/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.helpers;

import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.runtime.ICCSLConstraint;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
//import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.StepExecutor;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.statistics.SolverRuntimeStats;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;



/**
 * Helper functions for the semantic actions of all CCSL expressions and
 * relations. An instance of this class is given to all calls of
 * {@link ICCSLConstraint#semantic(AbstractSemanticHelper)},
 * {@link ICCSLConstraint#deathSemantic(AbstractSemanticHelper)} and
 * {@link ICCSLConstraint#start(AbstractSemanticHelper)}. This class is intended to be
 * used both for the simulation semantic at each step, and the clock death
 * semantic used during the death propagation phase of the simulation step.
 * 
 * @author Nicolas Chleq
 * 
 */
public class SemanticHelper extends AbstractSemanticHelper {

	private CCSLKernelSolver solver;
	private StepExecutor stepExecutor;
	
	public SemanticHelper(CCSLKernelSolver solver, StepExecutor stepExecutor) {
		super(solver.bddHelper);
		this.solver = solver;
		this.stepExecutor = stepExecutor;
	}

	/**
	 * Adds the BuDDyBDD argument to the simulation semantic using a conjunction. The
	 * BuDDyBDD given as argument is freed after the operation.
	 * 
	 * @param term
	 */
	public void semanticBDDAnd(BuDDyBDD term) {
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.enterMethod(this.getClass().getName() + ".semanticBDDAnd");
		stepExecutor.semanticBDDAnd(term);
		term.free();
		if (CCSLKernelSolver.runtimeStatsCollection)
			SolverRuntimeStats.leaveMethod(this.getClass().getName() + ".semanticBDDAnd");
	}

	/**
	 * This function registers that the relation given as argument is an
	 * assertion.
	 * 
	 * @param relation
	 */
	public void registerAssertion(AbstractRuntimeRelation relation) {
		stepExecutor.addAssertionVariable(relation.getAssertionVariable());
	}

	/**
	 * Builds a BuDDyBDD that represents an equality between the two clocks with
	 * respect to the death semantic. This BuDDyBDD is then and'ed with the current
	 * death semantic.
	 * 
	 * @param c1
	 * @param c2
	 */
	public void registerDeathEquality(RuntimeClock c1, RuntimeClock c2) {
		stepExecutor.getClockDeathSolver().registerEquality(c1, c2);
	}

	/**
	 * Builds a BuDDyBDD that represents an implication between the two clocks, c1
	 * implies c2, with respect to the death semantic. This BuDDyBDD is then and'ed
	 * with the current death semantic.
	 * 
	 * @param c1
	 * @param c2
	 */
	public void registerDeathImplication(RuntimeClock c1, RuntimeClock c2) {
		stepExecutor.getClockDeathSolver().registerImplication(c1, c2);
	}

	@Override
	public void registerDeathConjunctionImplies(RuntimeClock left1, RuntimeClock left2, RuntimeClock right) {
		stepExecutor.getClockDeathSolver().registerConjunctionImplication(left1, left2, right);
	}
	
	/**
	 * Builds a BuDDyBDD that forces the death of the clock cl in the death semantic.
	 * This BuDDyBDD is then and'ed with the current death semantic.
	 * 
	 * @param cl
	 */
	public void forceDeath(RuntimeClock cl) {
		stepExecutor.getClockDeathSolver().registerDeadClock(cl);
	}

	@Override
	public Set<RuntimeClock> getUsedClocks() {
		return stepExecutor.getUsedClocks();
	}
	
	public void registerClockUse(RuntimeClock clock) {
		stepExecutor.getUsedClocks().add((SolverClock) clock);
	}

	public void registerClockUse(RuntimeClock clocks[]) {
		for (RuntimeClock clock : clocks) {
			stepExecutor.getUsedClocks().add((SolverClock) clock);
		}
	}

	public boolean isSemanticDone(ICCSLConstraint concrete) {
		return stepExecutor.getSemanticDoneConcretes().contains(concrete);
	}

	public void registerSemanticDone(ICCSLConstraint concrete) {
		stepExecutor.getSemanticDoneConcretes().add((ISolverConcrete) concrete);
	}

	public void registerDeadClock(RuntimeClock clock) {
		solver.addDeadClock(clock);
	}

	public void unregisterDeadClock(RuntimeClock clock) {
		solver.removeDeadClock(clock);
	}
	
	public void registerClockToStart(RuntimeClock clock){
		stepExecutor.getStartQueue().add((SolverClock) clock);
	}
	
	public void unregisterClockToStart(RuntimeClock clock){
		stepExecutor.getStartQueue().remove(clock);
	}

	@Override
	public AbstractUpdateHelper getUpdateHelper() {
		return stepExecutor.getUpdateHelper();
	}

}
