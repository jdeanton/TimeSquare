/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;

public class ConditionalCase {
	
	private BooleanExpression condition;
	private List<ISolverConcrete> concretes;
	private Set<ImplicitClock> implicitClocks;
	private ImplicitClock rootClock;
	
	public ConditionalCase() {
		this.concretes = new ArrayList<ISolverConcrete>();
		this.implicitClocks = new HashSet<ImplicitClock>();
	}
	
	public BooleanExpression getCondition() {
		return condition;
	}
	public void setCondition(BooleanExpression condition) {
		this.condition = condition;
	}
	public List<ISolverConcrete> getConcretes() {
		return concretes;
	}
	public Set<ImplicitClock> getImplicitClocks() {
		return implicitClocks;
	}
	public ImplicitClock getRootClock() {
		return rootClock;
	}
	public void setRootClock(ImplicitClock rootClock) {
		this.rootClock = rootClock;
	}
}
