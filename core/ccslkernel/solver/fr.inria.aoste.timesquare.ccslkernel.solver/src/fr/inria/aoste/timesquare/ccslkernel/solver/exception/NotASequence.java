/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.exception;

public class NotASequence extends SolverException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8859046403094228483L;

	public NotASequence() {
	}

	public NotASequence(String message) {
		super(message);

	}

	public NotASequence(Throwable cause) {
		super(cause);

	}

	public NotASequence(String message, Throwable cause) {
		super(message, cause);

	}

}
