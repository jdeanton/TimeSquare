/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

public class DeathExpression extends SolverExpression {

	public DeathExpression() {
		super();
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if ( ! canCallSemantic() /* isActiveState() */) {
			return;
		}
		super.semantic(semanticHelper);
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
//		wasBorn = true;
		semanticHelper.inhibitClock(getImplicitClock());
		semanticHelper.registerClockUse(getImplicitClock());
	}
	
	@Override
	public boolean isTerminated() {
		return true;
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		super.deathSemantic(semanticHelper);
		semanticHelper.registerSemanticDone(this);
		semanticHelper.forceDeath(getImplicitClock());
	}
	
	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if ( ! canCallUpdate() /* isActiveState() */) {
			return;
		}
		super.update(helper);
		helper.registerNewDeadClock(getImplicitClock());
	}
	
	@Override
	public String toString() {
		String res = "[" + getImplicitClock().getName() + "]" + ( isActiveState() ? "*" : "") + "Death()";
		if (state == State.DEAD) {
			res += "/D";
		}
		return res;
	}
}
