/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.relation.SolverRelation;

public class UserDefinedExpression extends SolverExpression {

	private String definitionName;
	private ImplicitClock rootClock;
	private HashMap<AbstractEntity, ISolverElement> parameterValues;
	private List<ISolverConcrete> subConcretes;
	
	public UserDefinedExpression(String definitionName) {
		this.definitionName = definitionName;
		this.parameterValues = new HashMap<AbstractEntity, ISolverElement>();
		this.subConcretes = new ArrayList<ISolverConcrete>();
	}
	
	@Override
	public void addContent(IRuntimeContainer element) {
		if (element instanceof SolverRelation || element instanceof SolverExpression)
			getSubConcretes().add((ISolverConcrete) element);
	}
	
	public ImplicitClock getRootClock() {
		return rootClock;
	}

	public void setRootClock(ImplicitClock rootClock) {
		this.rootClock = rootClock;
	}
	
	public void setParameterValue(AbstractEntity parameter, ISolverElement value) {
		parameterValues.put(parameter, value);
	}
	
	public List<ISolverConcrete> getSubConcretes() {
		return subConcretes;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		
		for (ISolverConcrete subConcrete : subConcretes) {
			if (subConcrete instanceof SolverExpression){
				((SolverExpression) subConcrete).getImplicitClock().start(helper);
			}
			else {
				subConcrete.start(helper);
			}
		}
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (traceSemantic)
			System.out.println("Entry: UserDefinedExpression.semantic(" + toString() + ")");
		if ( ! canCallSemantic()) {
			if (traceSemantic)
				System.out.println("Exit: UserDefinedExpression.semantic(" + toString() + ") not active.");
			return;
		}
		super.semantic(semanticHelper);
//		wasBorn = true;
		if (getImplicitClock() != rootClock) {
			rootClock.semantic(semanticHelper);
			semanticHelper.semanticBDDAnd(semanticHelper.createEqual(getImplicitClock(), getRootClock()));
			semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(), getRootClock() });
		}
		if (traceSemantic)
			System.out.println("Exit: UserDefinedExpression.semantic(" + toString() + ")");
		
		for (ISolverConcrete subConcrete : subConcretes) {
			if (subConcrete instanceof SolverRelation){
				((SolverRelation) subConcrete).semantic(semanticHelper);
			}
		}
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		super.deathSemantic(semanticHelper);
		for (ISolverConcrete subConcrete : subConcretes) {
			if (subConcrete instanceof SolverRelation){
				((SolverRelation) subConcrete).deathSemantic(semanticHelper);
			}
		}
//		if (state != State.ACTIVE) {
//			return;
//		}
		if (getImplicitClock() != rootClock) {
			rootClock.deathSemantic(semanticHelper);
			if (semanticHelper.isSemanticDone(this))
				return;
			semanticHelper.registerSemanticDone(this);
			semanticHelper.registerDeathEquality(getImplicitClock(), getRootClock());
		}
		
		
	}

	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if (traceUpdate)
			System.out.println("Entry: UserDefinedExpression.update(" + toString() + ")");
		if ( ! canCallUpdate()) {
			if (traceUpdate)
				System.out.println("Exit: UserDefinedExpression.update(" + toString() + ") not active.");			
			return;
		}
		super.update(helper);
		rootClock.update(helper);
		for (ISolverConcrete subConcrete : getSubConcretes()) {
			if (subConcrete instanceof SolverRelation) {
				subConcrete.update(helper);
			}
		}
		if (rootClock.isDead()) {
			terminate(helper);
		}		
		if (traceUpdate)
			System.out.println("Exit: UserDefinedExpression.update(" + toString() + ")");
	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		if ( ! canCallTerminate()) {
			return;
		}
		super.terminate(helper);
		if (traceTerminate)
			System.out.println("Entry: UserDefinedExpression.terminate(" + toString() + ")");
		super.terminate(helper);
		for (ISolverConcrete subConcrete : subConcretes) {
			subConcrete.terminate(helper);
		}
		if (traceTerminate)
			System.out.println("Exit: UserDefinedExpression.terminate(" + toString() + ")");
	}
	
	@Override
	public boolean isTerminated() {
		return rootClock.isTerminated();
	}

	@Override
	public String toString() {
		return "[" + getImplicitClock().getName() + "]"
				   + (state != State.INIT ? "*" : (state == State.DEAD ? "D" : "") )
				+ "Call(" + definitionName + ")" + "->" + rootClock.getName();
	}

}
