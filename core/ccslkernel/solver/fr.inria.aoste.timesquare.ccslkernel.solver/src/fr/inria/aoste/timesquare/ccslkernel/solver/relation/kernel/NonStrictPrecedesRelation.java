/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.NoBooleanSolution;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

@Deprecated
public class NonStrictPrecedesRelation extends SolverKernelRelation {

	private int delta;
	private boolean updateDone = false;

	public NonStrictPrecedesRelation() {
		delta = 0;
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		super.semantic(semanticHelper);
		updateDone = false;
		if (isAssertion()) {
			assertionSemantic(semanticHelper);
		}
		else {
			if (delta == 0) {
				semanticHelper.semanticBDDAnd(semanticHelper.createImplies(rightClock,
						leftClock));
			}
			else if (delta < 0) {
				throw new NoBooleanSolution("A precedence relation has been violated"
						+ this.getNameFromPath("::"));
			}
			semanticHelper.registerClockUse(new SolverClock[] { leftClock, rightClock });
		}
	}

	public void assertionSemantic(AbstractSemanticHelper semanticHelper) {
		if (delta < 0) {
			BuDDyBDD assertion = semanticHelper.getAssertionVariable(this);
			semanticHelper.semanticBDDAnd(assertion.not());
			semanticHelper.registerAssertion(this);
		}
		else if (delta == 0) {
			semanticHelper.assertionSemantic(this,
					semanticHelper.createImplies(rightClock, leftClock));
		}
		else if (delta > 0) {
			BuDDyBDD assertion = semanticHelper.getAssertionVariable(this);
			semanticHelper.semanticBDDAnd(assertion);
			semanticHelper.registerAssertion(this);
		}
		semanticHelper.registerClockUse(new SolverClock[] { leftClock, rightClock });
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		if (delta == 0) {
			semanticHelper.registerDeathImplication(getLeftClock(), getRightClock());
		}
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (!updateDone) {
			super.update(helper);
			if (helper.clockHasFired(leftClock)) {
				delta++;
			}
			if (helper.clockHasFired(rightClock)) {
				delta--;
			}
		}
		updateDone = true;
	}

	@Override
	public String toString() {
		return leftClock.getName() + " Precedes " + rightClock.getName();
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		//super does not add anything in the list
		SerializedConstraintState currentState = super.dumpState();
		currentState.dump(delta);
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState state) {
		super.restoreState(state);
		delta = (Integer) state.restore(0);
	}
	
}
