/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.priorities;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;
import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PriorityRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.priorities.model.ccslpriority.PrioritySpecification;

/**
The PrioSet class represents a set of priority rules represented in the attribute prioRules.
The ith element of prioRules represents a set of clocks on which the ith clock has priority.
ATTENTION: The current implementation does not compute the transitivity of priorities in
prioRules. The attribute clockNames keeps track of the names of the clocks.  
*/
public class SolverPrioritySpecification{

//    public Vector<String> clockNames;
//    public Vector<Vector<Integer>> prioRules;
    
    private Resource _resource=null;
    private CCSLKernelSolver _solver=null;
    //no doubles
    private Set<SolverPriorityRelation> _priorityRelations=new HashSet<SolverPriorityRelation>();
   
    
    
    public Set<SolverPriorityRelation> getPriorityRelations() {
		return _priorityRelations;
	}

    
    public SolverPrioritySpecification(SolverPrioritySpecification origin){
    	for (SolverPriorityRelation pr : origin.getPriorityRelations()) {
			this._priorityRelations.add(pr);
		}
    }
	/**
     * @return A ready to use instance of the Specification. 
     */
    public SolverPrioritySpecification (String pathName, CCSLKernelSolver solver) {
    	_solver = solver;
    	
    	ResourceSet resourceSet = new ResourceSetImpl();
		_resource = resourceSet.createResource(URI.createFileURI(pathName));
    	
		EcoreUtil.resolveAll(resourceSet);

		PrioritySpecification spec = (PrioritySpecification) _resource.getContents().get(0);
		for (@SuppressWarnings("unchecked")
		Iterator<PriorityRelation> iterator = spec.getRelations().iterator(); iterator.hasNext();) {
			PriorityRelation relation = (PriorityRelation) iterator.next();
			SolverPriorityRelation r = new SolverPriorityRelation();
			r.setLower(_solver.findClockByPath(getQualifiedName(relation.getLower())));
			r.setHigher(_solver.findClockByPath(getQualifiedName(relation.getHigher())));
			_priorityRelations.add(r);
		}
		
    }
    
    public SolverPrioritySpecification (Resource resource, CCSLKernelSolver solver) {
    	_solver = solver;
		_resource = resource;
		ResourceSet resourceSet = resource.getResourceSet();
		EcoreUtil.resolveAll(resourceSet);
		if(_resource.getContents().size() > 0) {
			PrioritySpecification spec = (PrioritySpecification) _resource.getContents().get(0);
			for (@SuppressWarnings("unchecked")
			Iterator<PriorityRelation> iterator = spec.getRelations().iterator(); iterator.hasNext();) {
				PriorityRelation relation = (PriorityRelation) iterator.next();
				SolverPriorityRelation r = new SolverPriorityRelation();
				r.setLower(_solver.findClockByPath(getQualifiedName(relation.getLower())));
				r.setHigher(_solver.findClockByPath(getQualifiedName(relation.getHigher())));
				_priorityRelations.add(r);
			}
		}else {
			throw new RuntimeException("priority spec is empty");
		}
		
    }
    
    public SolverPrioritySpecification (PrioritySpecification specModel, CCSLKernelSolver solver) {
		PrioritySpecification spec = specModel;
		for (@SuppressWarnings("unchecked")
		Iterator<PriorityRelation> iterator = spec.getRelations().iterator(); iterator.hasNext();) {
			PriorityRelation relation = (PriorityRelation) iterator.next();
			SolverPriorityRelation r = new SolverPriorityRelation();
			r.setLower(_solver.findClockByPath(getQualifiedName(relation.getLower())));
			r.setHigher(_solver.findClockByPath(getQualifiedName(relation.getHigher())));
			_priorityRelations.add(r);
		}
		
    }
    
    private String getQualifiedName(EObject eo) {
    	return CCSLKernelUtils.getQualifiedName(eo);
	}

//    
//    Vector<Vector<Integer>> getPrio () {
//    	return prioRules;
//    }
    
//    //looks wrong !!!
//    Vector<Integer> getLowerPrio(int clock){
//    	return prioRules.get(clock);
//    }
//
//    /**
//     * Change the dictionary of clocks and update the date structure of priority rules accordingly.
//     * @param newClockNames The new set of clocks.
//     */
//    void setClockNames(){
//    	ResourceSet resourceSet = _resource.getResourceSet();
//		EcoreUtil.resolveAll(resourceSet);
//    	TreeIterator<EObject> allContentIterator = _resource.getAllContents();
//    	// Vector<String> newClockNames = new Vector<String> ();
//    	Vector<Vector<Integer>> newPrioRules = new Vector<Vector<Integer>> ();
//    	
//    	
//    	for(int i=0; i<newClockNames.size();i++){
//    		int index = getIndex(newClockNames.elementAt(i));
//    		if(index >= 0)
//    			newPrioRules.add(this.prioRules.elementAt(index));
//    		else
//    			newPrioRules.add(new Vector<Integer>());
//    	}
//    	this.clockNames = newClockNames;
//    	this.prioRules = newPrioRules;
//    }
    
    
    /**
     * @param clock A clock index. 
     * @return The name of the clock or null if the index does not exists.
     */
    //what's this !!!
//    String getName(int clock){
//    	return clockNames.get(clock);
//    }
    
    /**
     * @param name The name of a clock.
     * @return The index of the clock or -1 if the name does not exist.
     */
//    int getIndex(String name){
//    	return clockNames.indexOf(name);
//    }
    
    // Input: a set of clocks
    // Output: the set of clocks that have no clock of higher priority in the input set
    /**
     * Remove from a set of clocks the variables that have lower priority than a given reference variable.
     * @param clock A reference clock index.
     * @param result2 A set of clock indexes.
     * @return The maximal subset of clock indexes where no clock has lower priority than the reference clock.
     */
    public Set<RuntimeClock> removeLowerPrio(RuntimeClock sc, Set<RuntimeClock> result2){
		Set<RuntimeClock> result = new  HashSet<RuntimeClock> (result2);
		for (Iterator<SolverPriorityRelation> iterator = _priorityRelations.iterator(); iterator.hasNext();) 
		{
			SolverPriorityRelation r= (SolverPriorityRelation) iterator.next();
			if (r.higher == sc)
			{
				result.remove(r.lower);
				result = removeLowerPrio(r.lower, result);
			}
		}
		return result;
    }
    
    public Set<RuntimeClock> removeLowerPrio(Set<RuntimeClock> eSet){
    	Set<RuntimeClock> result = new  HashSet<RuntimeClock> (eSet);
    	for (Iterator<RuntimeClock> iterator = result.iterator(); iterator.hasNext();) {
    		RuntimeClock sc = (RuntimeClock) iterator.next();
			result = removeLowerPrio(sc, result);
		}
    	return result;
    }

	/**
	 * add a priority relation to the set if and only if it is not already contains. 
	 * It returns true it the relation has actually been added or false otherwise
	 * @param newPR, the priorityRelation to add
	 * @return true if actually added, false otherwise
	 */
	public boolean add(SolverPriorityRelation newPR) {
		SolverClock lower = newPR.getLower();
		SolverClock higher = newPR.getHigher();
		for (Iterator<SolverPriorityRelation> it = _priorityRelations.iterator(); it.hasNext();) {
			SolverPriorityRelation pr = (SolverPriorityRelation) it.next();
			if (pr.getLower().equals(lower)
					&& 
				(pr.getHigher().equals(higher))){
				return false;
			}
		}
		_priorityRelations.add(newPR);
		return true;
	}


//    /* Print priorities */
//	
//    public void printPrio(){
//      for(int i=0; i<clockNames.size(); i++){
//    	  Vector<Integer> aux =  prioRules.elementAt(i);
//    	  System.out.print("Clock "+clockNames.elementAt(i)+" has priority on {");
//    	  for(int j=0; j<aux.size(); j++)
//    		  System.out.print(" "+clockNames.elementAt(aux.elementAt(j)));	
//	System.out.println(" }");
//	}
//    }

}


