/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

@Deprecated
public class CoincidenceRelation extends SolverKernelRelation {

	public CoincidenceRelation() {
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		if ( ! canCallSemantic())
			return;
		super.semantic(semanticHelper);
		if (leftClock.bddVariableNumber != rightClock.bddVariableNumber) {
			BuDDyBDD semantic = semanticHelper.createEqual(leftClock, rightClock);
			if (isAssertion()) {
				semanticHelper.assertionSemantic(this, semantic);
			}
			else {
				semanticHelper.semanticBDDAnd(semantic);
			}
		}
		semanticHelper.registerClockUse(new SolverClock[] { leftClock, rightClock });
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (semanticHelper.isSemanticDone(this))
			return;
		super.deathSemantic(semanticHelper);
		semanticHelper.registerSemanticDone(this);
		semanticHelper.registerDeathEquality(getLeftClock(), getRightClock());
	}

	@Override
	public String toString() {
		return leftClock.getName() + " = " + rightClock.getName();
	}

}
