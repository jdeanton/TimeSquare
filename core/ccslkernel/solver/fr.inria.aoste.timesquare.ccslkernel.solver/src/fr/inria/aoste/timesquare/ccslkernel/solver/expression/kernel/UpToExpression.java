/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

@Deprecated
public class UpToExpression extends SolverExpression {

	private SolverClock clockToFollow;
	private SolverClock killerClock;
	private boolean updateDone;
	private boolean isPreemptive=true;

	public UpToExpression() {
		super();
		updateDone = false;
	}

	public SolverClock getClockToFollow() {
		return clockToFollow;
	}

	public void setIsPreemptive(boolean mustPreempt){
		this.isPreemptive = mustPreempt;
	}
	
	public void setClockToFollow(SolverClock clockToFollow) {
		this.clockToFollow = clockToFollow;
	}

	public SolverClock getKillerClock() {
		return killerClock;
	}

	public void setKillerClock(SolverClock killerClock) {
		this.killerClock = killerClock;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (traceStart)
			System.out.println("Entry: UpToExpression.start(" + toString() + ").");
		super.start(helper);
		updateDone = true;
		if (traceStart)
			System.out.println("Exit: UpToExpression.start(" + toString() + ").");
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(semanticHelper);
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		clockToFollow.semantic(semanticHelper);
		killerClock.semantic(semanticHelper);
//		wasBorn = true;
		updateDone = false;
		if (state == State.DEAD) {
			semanticHelper.inhibitClock(getImplicitClock());
		}
		else {
			if (isPreemptive){
				semanticHelper.semanticBDDAnd(semanticHelper.createEqual(
					getImplicitClock(),
					semanticHelper.createIntersection(getClockToFollow(),
							semanticHelper.createNot(getKillerClock()))));
			}else{
				semanticHelper.semanticBDDAnd(semanticHelper.createEqual(
					getImplicitClock(),
					getClockToFollow()));
			}
		}
		semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(),
				getClockToFollow(), getKillerClock() });
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (traceUpdate)
			System.out.println("Entry: UpToExpression.update(" + toString() + ").");
		if ( ! canCallUpdate())
			return;
		super.update(helper);
		if ( ! isActiveState()) {
			if (traceUpdate)
				System.out.println("Exit: UpToExpression.update(" + toString()
						+ ") not active.");
			return;
		}
		if (updateDone) {
			if (traceUpdate)
				System.out.println("Exit: UpToExpression.update(" + toString()
						+ ") update done.");
			return;
		}
		updateDone = true;
		clockToFollow.update(helper);
		killerClock.update(helper);
		if (helper.clockHasFired(killerClock)) {
			terminate(helper);
		}
//		else if (clockToFollow.isDead()) {
//			terminate(helper);
//		}
		if (traceUpdate)
			System.out.println("Exit: UpToExpression.update(" + toString() + ").");
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		semanticHelper.registerDeathImplication(clockToFollow, getImplicitClock());
		clockToFollow.deathSemantic(semanticHelper);
		killerClock.deathSemantic(semanticHelper);
	}

	@Override
	public String toString() {
		return "[" + getImplicitClock().getName() + "] " + clockToFollow.getName()
				+ " UpTo " + killerClock.getName();
	}

}
