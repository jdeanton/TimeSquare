/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.statistics;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

/**
 * This class allows to collect crude runtime statistics using {@link System#nanoTime()} to compute
 * running time of code.
 * 
 * @author nchleq
 *
 */
public class SolverRuntimeStats {
	
	private SolverRuntimeStats(String method) {
		this.runtime = 0;
		this.runtimeMax = Long.MIN_VALUE;
		this.runtimeMin = Long.MAX_VALUE;
		this.invocations = 0;
		this.method = method;
	}
	
	private static Map<String, SolverRuntimeStats> statsByMethod = new HashMap<String, SolverRuntimeStats>();
	
	private static synchronized SolverRuntimeStats getRuntimeStats(String method) {
		SolverRuntimeStats stats = statsByMethod.get(method);
		if (stats == null) {
			stats = new SolverRuntimeStats(method);
			statsByMethod.put(method, stats);
		}
		return stats;
	}
	
	private static ArrayDeque<SolverRuntimeStats> callStack = new ArrayDeque<SolverRuntimeStats>();
	
	private String method;
	private long runtime;
	private long runtimeMin;
	private long runtimeMax;
	private long enterTime;
	private int invocations;
		
	private long getRuntime() {
		return runtime;
	}
	
	public static synchronized void clearStats() {
		statsByMethod.clear();	
	}
	
	public long getRuntime(String method) {
		SolverRuntimeStats stats = getRuntimeStats(method);
		return stats.getRuntime();
	}
	
	public static void enterMethod(String method) {
		SolverRuntimeStats stats = getRuntimeStats(method);
		callStack.push(stats);
		stats.invocations += 1;
		stats.enterTime = System.nanoTime();
	}
	
	public static void leaveMethod(String method) {
		SolverRuntimeStats top = callStack.peek();
		if (top == null || top.method.compareTo(method) != 0) {
			// Problem
		}
		long runtime = System.nanoTime() - top.enterTime;
		top.runtime += runtime;
		if (runtime > top.runtimeMax)
			top.runtimeMax = runtime;
		if (runtime < top.runtimeMin)
			top.runtimeMin = runtime;
		top = callStack.pop();
	}
	
	public String toString() {
		return method + ": " + runtime / 1000.0 + " us, " + invocations + " calls, "
				+ (invocations > 0 ? runtime / invocations / 1000.0 : 0) + " us/call "
				+ "(min: " + runtimeMin / 1000.0 + ", max: " + runtimeMax / 1000.0 + ")";
	}
	
	public static void print() {
		for (String method : statsByMethod.keySet()) {
			System.out.println(statsByMethod.get(method));
		}
	}
}
