/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.EvaluationTypeError;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ClassicalExpressionEvaluator;
import fr.inria.aoste.timesquare.ccslkernel.solver.SolverElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.SolverPrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.BasicType.SolverSequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.NotASequence;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

@Deprecated
public class DeferExpression extends SolverExpression {
	
	public SolverClock baseClock;
	public SolverClock delayClock;
	public SolverSequenceElement sigma;
	public SolverElement delayPattern;
	
	private ArrayList<java.lang.Integer> ds = new ArrayList<java.lang.Integer>();
	
	private boolean updateDone;

	public DeferExpression() {
		super();
		updateDone = false;
	}
	
	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart() )
			return;
		super.start(helper);
			
		ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(getAbstractMapping());
		SolverElement value = eval.evaluate(delayPattern);
		if (value instanceof SolverSequenceElement && value.getModelElement() instanceof SequenceElement) {
			sigma = new SolverSequenceElement((SolverSequenceElement)value, value.getInstantiatedElement());
		}
		else {
			throw new NotASequence("DeferExpression.start(): SequenceElement expected for delayPattern, got: " + value.toString());
		}
		updateDone = true;
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if ( ! canCallSemantic() /* ! isActiveState() && state != State.DEAD */) {
			return;
		}
		super.semantic(semanticHelper);
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
//		wasBorn = true;
		updateDone = false;
		if (state == State.DEAD) {
			semanticHelper.inhibitClock(getImplicitClock());
		}
		else {
			baseClock.semantic(semanticHelper);
			delayClock.semantic(semanticHelper);
			boolean beta = false;	
			if (ds.isEmpty()) {
				beta = false;
			}
			else {
				beta = (getDelay() == 1 || getDelay() == 0);
			}
			if (beta) {
				semanticHelper.semanticBDDAnd(semanticHelper.createEqual(getImplicitClock(), delayClock));
			}
			else {
				semanticHelper.semanticBDDAnd(semanticHelper.createNot(getImplicitClock()));
			}

		}
		semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(), baseClock, delayClock });
	}
	
	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if ( ! canCallUpdate() /* isActiveState() */) {
			return;
		}
		super.update(helper);
		baseClock.update(helper);
		delayClock.update(helper);
		if ( (ds.isEmpty() && sigma.isEmpty())
				|| 
			delayClock.isDead()) {
			terminate(helper);
			updateDone = true;
			return;
		}
		if (updateDone) {
			return;
		}
		if (helper.clockHasFired(baseClock)) {
			if (helper.clockHasFired(delayClock)) {
				// RWDefer3
				nextDelay();
				IntegerElement next = null;
				PrimitiveElement nextHead = sigma.nextHead();
				ClassicalExpressionEvaluator evaluator = new ClassicalExpressionEvaluator(getAbstractMapping());
				SolverElement value = evaluator.evaluate(nextHead);
				if ( ! (value instanceof SolverPrimitiveElement) && ! ( value.getModelElement() instanceof IntegerElement)) {
					throw new EvaluationTypeError("DeferExpression.update()/RWDefer3: IntegerElement expected, got "
												  + value.getModelElement().toString());
				}
				else {
					next = (IntegerElement) value.getModelElement();
				}
				if (next != null) {
					sched(next.getValue(), 0);
				}
			}
			else {
				// RWDefer2
				IntegerElement next = null;
				PrimitiveElement nextHead = sigma.nextHead();
				ClassicalExpressionEvaluator evaluator = new ClassicalExpressionEvaluator(getAbstractMapping());
				SolverElement value = evaluator.evaluate(nextHead);
				if ( ! (value instanceof SolverPrimitiveElement) && ! ( value.getModelElement() instanceof IntegerElement)) {
					throw new EvaluationTypeError("DeferExpression.update()/RWDefer2: IntegerElement expected, got "
												  + value.getModelElement().toString());
				}
				else {
					next = (IntegerElement) value.getModelElement();
				}
				if (next != null) {
					sched(next.getValue(), 0);
				}
			}
		}
		else if (helper.clockHasFired(delayClock)) {
			// RWDefer1 nextDelay()
			nextDelay();
		}
		updateDone = true;
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		super.deathSemantic(semanticHelper);
		baseClock.deathSemantic(semanticHelper);
		delayClock.deathSemantic(semanticHelper);
		semanticHelper.registerDeathImplication(delayClock, getImplicitClock());
	}
	
	@Override
	public String toString() {
		String res = "[" + getImplicitClock().getName() + "]" + ( isActiveState() ? "*" : "")
				+ "Defer(" + baseClock.getName() + ", " + delayClock.getName() + ", " + sigma + ")";
		if (state == State.DEAD) {
			res += "/D";
		}
		return res;
	}
	
	private int getDelay() {
		if (ds.isEmpty()) {
			return java.lang.Integer.MAX_VALUE;
		}
		return ds.get(0);
	}
	
	private void nextDelay() {
		if (! ds.isEmpty()) {
			int head = ds.get(0);
			if (head == 1 || head == 0) {
				ds.remove(0);
			}
			else {
				ds.set(0, head - 1);
			}
		}
	}
	
	private void sched(int next, int start) {
		if (ds.size() == start) {
			ds.add(start, next);
		}
		else {
			int head = ds.get(start);
			if (next == head) {
				return;
			}
			else if (next < head) {
				int rem = head - next;
				ds.set(start, next);
				ds.add(start + 1, rem);
			}
			else { // next > head
				int rem = next - head;
				sched(rem, start + 1);
			}
		}
	}
	
	
	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
//		currentState.add(Utilities.objectToBytes(sigma.getFinitePartIndex()));
//		currentState.add(Utilities.objectToBytes(sigma.getNonFinitePartIndex()));
		
//		currentState.add(Serializer.getDefaultSerializer().toBytes(ds));
//		currentState.add(Serializer.getDefaultSerializer().toBytes(sigma.dumpState()));
//		currentState.add(Serializer.getDefaultSerializer().toBytes(delayPattern.dumpState()));
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
//		sigma.setFinitePartIndex((Integer) Utilities.bytesToObject(newState.get(2)));
//		sigma.setNonFinitePartIndex((Integer) Utilities.bytesToObject(newState.get(3)));
		// Utilities.bytesToObject(newState.get(4));
//		ds = (ArrayList<Integer>) Serializer.getDefaultSerializer().fromBytes(newState.get(2));
//		sigma.restoreState((SerializedConstraintState) Serializer.getDefaultSerializer().fromBytes(newState.get(3)));
//		delayPattern.restoreState((SerializedConstraintState)Serializer.getDefaultSerializer().fromBytes(newState.get(4)));
	}
	
	
}
