/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.extension;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;

import fr.inria.aoste.timesquare.ccslkernel.solver.Activator;
import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;

public class ExpressionsExtensionManager implements IExtensionManager {

	private static ExpressionsExtensionManager INSTANCE;
	public static ExpressionsExtensionManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ExpressionsExtensionManager();
		}
		return INSTANCE;
	}
	
	private ExpressionsExtensionManager() {
		ExtensionPointManager.findAllExtensions(this);
	}
	
	@Override
	public String getExtensionPointName() {
		return Activator.PLUGIN_ID + ".extension.expressions";
	}

	@Override
	public String getPluginName() {
		return Activator.PLUGIN_ID;
	}
	
	private List<ISolverExpressionFactory> factories = new ArrayList<ISolverExpressionFactory>();

	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable {
		Class<? extends ISolverExpressionFactory> clazz = ExtensionPointManager.getPointExtensionClass(ice, "class", ISolverExpressionFactory.class);
		factories.add( clazz.newInstance() );
	}

	public List<ISolverExpressionFactory> getFactories() {
		return factories;
	}
	
}
