/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.priorities;

import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;


public class  SolverPriorityRelation {
	
	protected SolverClock lower;

	protected SolverClock higher;
	
	SolverClock getLower(){
		return lower;
	}

	void setLower(SolverClock value){
		lower = value;
	}

	SolverClock getHigher(){
		return higher;
	}

	void setHigher(SolverClock value){
		higher = value;
	}

} 
