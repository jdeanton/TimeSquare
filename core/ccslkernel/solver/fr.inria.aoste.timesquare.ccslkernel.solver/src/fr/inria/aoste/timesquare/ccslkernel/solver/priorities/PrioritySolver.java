/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.priorities;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import fr.inria.aoste.timesquare.ccslkernel.runtime.elements.RuntimeClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;
import net.sf.javabdd.BuDDyFactory;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD;

public class PrioritySolver{
	
    private static BuDDyFactory _factory;
    static Vector<String> dico;
    private SolverPrioritySpecification _prioSpec; 
    private PriorityPropagator _pp;

    
    public PrioritySolver(BuDDyFactory factory, SolverPrioritySpecification prioritySpec, CCSLKernelSolver solver){
    	_factory = factory;
    	_prioSpec = prioritySpec;
    	_pp = new PriorityPropagator(_prioSpec, solver);

    	
    }
    
    /* ************************** */    
   /*     Priority resolution    */    
  /* ************************** */    
    
    
    /**
     *  Compute the subset of clockSet that is "enabled" in the bdd, i.e. variables 
     *  that are not fixed. For such a variable x, there is a solution of of the
     *  bdd where x is true and another one where x is false.
     *  
     * !! Naive algorithm !! can probably be improved if we manipulate
     * directly the BuDDyBDD.
     * 
     * @param buDDyBDD Representation of the formula.
     * @param varSet Set of variable.
     * @return Subset of enabled variables in bdd.
     */
    private  Set<RuntimeClock> enabled(BuDDyBDD buDDyBDD, Set<RuntimeClock> usedClocks){
	//debug
	//System.out.println("\nenabled start");
		Set<RuntimeClock> eSet = new HashSet<RuntimeClock>();
		for (RuntimeClock c : usedClocks){
		    if((!buDDyBDD.and(_factory.ithVar(c.bddVariableNumber)).isZero() )
		    &
			(!buDDyBDD.and(_factory.nithVar(c.bddVariableNumber)).isZero()))
			{ 
				//debug
				//System.out.println(var+" is enabled");
				eSet.add(c);
			}
		}
		//debug
		//System.out.println("enabled end \n");
		return eSet;
    }



    /* ********************************************************************* */

    public void propagatePriority(){
    	_pp.addPriorityRelationsToSpec();
    	_prioSpec = _pp.getPrioSpec();
    }

    /**
     * Restrict the set of solutions of the BuDDyBDD wrt the rules in the priority set. 
     * The argument varSet is used for optimisations. As the set of enabled variables is
     * decreasing it is better to pass it as argument to avoid unneeded tests in recursive 
     * calls. The first call to the method should be with varSet equal to the set of 
     * variables in bdd.
     * 
     * NOTE: The same optimisation should be possible for P since priorities that are not 
     * used at some step will note be used anymore in recursive calls.
     * 
     * @param buDDyBDD An input BuDDyBDD.
     * @param varSet A set of variables.
     * @return The BuDDyBDD representation of the restriction needed to enforce priorities on bdd, 
     */
       
    public BuDDyBDD ResolvePrio(BuDDyBDD buDDyBDD,  Set<RuntimeClock> usedClocks){
    
	Set<RuntimeClock> eSet = enabled(buDDyBDD, usedClocks);
	Set<RuntimeClock> temp_eSet = new HashSet<RuntimeClock>(eSet);
	//removing implicit clocks that blur priority propagations
	for(RuntimeClock rc : temp_eSet) {
		if (rc instanceof ImplicitClock) {
			eSet.remove(rc);
		}
	}
	temp_eSet = null;
	Set<RuntimeClock> fSet = _prioSpec.removeLowerPrio(eSet);
	

	// check if the firable set is equal to the enable set
	// !!! Must ensure that max(eSet,pSet) really returns a subset of eSet
	if (eSet.size() != fSet.size()){
	    BuDDyBDD res = _factory.zero();
	    for(RuntimeClock sc : fSet) {

		//debug
		//System.out.println("\n"+x+" is firable \n");
		

		// bdd_x is obtained by setting x to true in bdd
		BuDDyBDD bdd_sc = buDDyBDD.and(_factory.ithVar(sc.bddVariableNumber));

		// remove x from the enabled set for recursive call
	    Set<RuntimeClock> eSetIt = new HashSet<RuntimeClock> (eSet);
		eSetIt.remove(sc);

		// Sol += x && ResolvePrio(bdd_x, P, eSetIt)		
		BuDDyBDD temp = _factory.ithVar(sc.bddVariableNumber).and(ResolvePrio(bdd_sc,  eSetIt));
		res = res.or(temp);

		// BuDDyBDD where all enabled variables are set to false
		BuDDyBDD bdd_sc_eNot = buDDyBDD.and(_factory.one());
		for(RuntimeClock sc2 : eSet)
			bdd_sc_eNot = bdd_sc_eNot.and(_factory.nithVar(sc2.bddVariableNumber)); 

		//System.out.println("res"+res);
		//System.out.println("bdd_sc_eNot"+bdd_x_eNot+" "+bdd_s_eNot.satCount());

		if(bdd_sc_eNot.satCount()>=1) // ATTENTION A CE TEST
		    res = res.or(bdd_sc_eNot);
		//System.out.println("new res"+res);
	    }
	    return res;
	}
	return _factory.one();
    }
       

    
}