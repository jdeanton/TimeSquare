/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.relation;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.relations.AbstractRuntimeRelation;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;

public abstract class AbstractWrappedRelation extends AbstractRuntimeRelation implements ISolverConcrete  {

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException{
		if ( ! canCallStart())
			return;
		super.start(helper);
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException{
		super.semantic(semanticHelper);
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper) throws SimulationException{
		super.deathSemantic(semanticHelper);
	}

	@Override
	public  void update(AbstractUpdateHelper helper) throws SimulationException{
		super.update(helper);
	}

	@Override
	public final void terminate(AbstractUpdateHelper helper) throws SimulationException {
		super.terminate(helper);
	}

	@Override
	public boolean isTerminated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SerializedConstraintState dumpState() {
		return new SerializedConstraintState();
	}
	
	@Override
	public void restoreState(SerializedConstraintState state) {
	}
	
}
