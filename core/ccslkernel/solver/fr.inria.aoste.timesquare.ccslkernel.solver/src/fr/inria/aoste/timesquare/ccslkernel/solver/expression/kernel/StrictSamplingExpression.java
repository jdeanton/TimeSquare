/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

@Deprecated
public class StrictSamplingExpression extends SolverExpression {

	private enum SamplingState {
		NORMAL, WAITSAMPLING
	};

	private SamplingState localState;
	private SolverClock sampledClock;
	private SolverClock samplingClock;
	private boolean updateDone;

	public StrictSamplingExpression() {
		super();
		localState = SamplingState.NORMAL;
		updateDone = false;
	}

	public SolverClock getSampledClock() {
		return sampledClock;
	}

	public void setSampledClock(SolverClock sampledClock) {
		this.sampledClock = sampledClock;
	}

	public SolverClock getSamplingClock() {
		return samplingClock;
	}

	public void setSamplingClock(SolverClock samplingClock) {
		this.samplingClock = samplingClock;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		localState = SamplingState.NORMAL;
		updateDone = true;
	}

	@Override
	public void semantic(AbstractSemanticHelper semanticHelper) throws SimulationException {
		sampledClock.semantic(semanticHelper);
		samplingClock.semantic(semanticHelper);
		if ( ! canCallSemantic() /* isActiveState() && state != State.DEAD */) {
			return;
		}
		super.semantic(semanticHelper);
		if (semanticHelper.isSemanticDone(this))
			return;
		semanticHelper.registerSemanticDone(this);
		updateDone = false;
		if (state == State.DEAD) {
			semanticHelper.inhibitClock(getImplicitClock());
		}
		else {
//			wasBorn = true;
			if (state == State.DEAD) { // ????????
				semanticHelper.inhibitClock(getImplicitClock());
			}
			else {
				if (localState == SamplingState.NORMAL || state == State.DEAD) {
					semanticHelper.inhibitClock(getImplicitClock());
				}
				else if (localState == SamplingState.WAITSAMPLING) {
					semanticHelper.semanticBDDAnd(semanticHelper.createEqual(
							getImplicitClock(), getSamplingClock()));
				}
			}
		}
		semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(),
				getSampledClock(), getSamplingClock() });
	}

	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		sampledClock.update(helper);
		samplingClock.update(helper);
		if (! canCallUpdate())
			return;
		super.update(helper);
		if (updateDone) {
			return;
		}
		updateDone = true;
		switch (localState) {
		case NORMAL:
			if (helper.clockHasFired(sampledClock)) {
				localState = SamplingState.WAITSAMPLING;
			}
			break;
		case WAITSAMPLING:
			if (helper.clockHasFired(samplingClock)) {
				terminate(helper);
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		super.deathSemantic(semanticHelper);
		sampledClock.deathSemantic(semanticHelper);
		samplingClock.deathSemantic(semanticHelper);
		switch (localState) {
		case NORMAL:
			semanticHelper.registerDeathImplication(getSampledClock(), getImplicitClock());
			break;
		case WAITSAMPLING:
			semanticHelper.registerDeathImplication(getSamplingClock(), getImplicitClock());
			break;
		}
	}
	
	@Override
	public String toString() {
		return "[" + getImplicitClock().getName() + "] " + sampledClock.getName()
				+ " StrictlySampledOn " + samplingClock.getName();
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
//		currentState.add(Serializer.getDefaultSerializer().toBytes(localState.ordinal()));
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
//		localState = (SamplingState) SamplingState.values()[(Integer) Serializer.getDefaultSerializer().fromBytes(newState.get(2))];
	}
	
}
