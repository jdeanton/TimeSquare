/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression.kernel;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.EvaluationTypeError;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ClassicalExpressionEvaluator;
import fr.inria.aoste.timesquare.ccslkernel.solver.SolverElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.SolverPrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.TimeModel.SolverClock;
import fr.inria.aoste.timesquare.ccslkernel.solver.expression.SolverExpression;

@Deprecated
public class WaitExpression extends SolverExpression {

	public PrimitiveElement waitValue;
	private int initialDelay;
	public int delayCount;
	private SolverClock clock;
	private boolean updateDone;

	public WaitExpression() {
		super();
		delayCount = 0;
	}
	
	public SolverClock getClock() {
		return clock;
	}

	public void setClock(SolverClock clock) {
		this.clock = clock;
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if (traceStart)
			System.out.println("Entry: WaitExpression.start(" + toString() + ")");
		super.start(helper);
		ClassicalExpressionEvaluator eval = new ClassicalExpressionEvaluator(getAbstractMapping());
		SolverElement delay = eval.evaluate(waitValue);
		if ((delay instanceof SolverPrimitiveElement) && delay.getModelElement() instanceof IntegerElement) {
			initialDelay = ((IntegerElement) delay.getModelElement()).getValue();
			delayCount = initialDelay;
//			state = State.ACTIVE;
			updateDone = true;
		}
		else {
			throw new EvaluationTypeError("WaitExpression.start(): IntegerElement expected");
		}				
		if (traceStart)
			System.out.println("Exit: WaitExpression.start(" + toString() + ")");
	}

	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		super.terminate(helper);
		initialDelay = -1;
		delayCount = -1;
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (traceSemantic)
			System.out.println("Entry: WaitExpression.semantic(" + toString() + ")");
		if ( ! isActiveState() && state != State.DEAD) {
			if (traceSemantic)
				System.out.println("Exit: WaitExpression.semantic(" + toString() + ") neither active nor dead");
			return;
		}
		if (semanticHelper.isSemanticDone(this)) {
			if (traceSemantic)
				System.out.println("Exit: WaitExpression.semantic(" + toString() + ") semantic done");
			return;			
		}
		semanticHelper.registerSemanticDone(this);
		clock.semantic(semanticHelper);
		if (state == State.DEAD) {
			semanticHelper.inhibitClock(getImplicitClock());
			terminate(semanticHelper.getUpdateHelper());
		}
		else {
//			wasBorn=true;
			updateDone = false;
			if (delayCount == 1) {
				semanticHelper.semanticBDDAnd(semanticHelper.createEqual(getImplicitClock(), getClock()));
			}
			else {
				semanticHelper.inhibitClock(getImplicitClock());
			}
		}
		semanticHelper.registerClockUse(new SolverClock[] { getImplicitClock(), clock });
		if (traceSemantic)
			System.out.println("Exit: WaitExpression.semantic(" + toString() + ")");
	}
	
	@Override
	public void update(AbstractUpdateHelper helper) throws SimulationException {
		if (traceUpdate)
			System.out.println("Entry: WaitExpression.update(" + toString() + ")");
		if ( ! isActiveState()) {
			if (traceUpdate)
				System.out.println("Exit: WaitExpression.update(" + toString() + ") not active");
			return;
		}
		if ((state == State.DEAD) || updateDone) {
			if (traceUpdate)
				System.out.println("Exit: WaitExpression.update(" + toString() + ") update done");
			return;
		}
		clock.update(helper);
		if (clock.isDead()){
			terminate(helper);
		}
		if (helper.clockHasFired(clock)) {
			if (delayCount == 1) {
				// RWwait1 in RR6925
				terminate(helper);
			}
			else {
				//RWwait2 in RR6925.
				delayCount -= 1;
			}
		}
		updateDone = true;
		if (traceUpdate)
			System.out.println("Exit: WaitExpression.update(" + toString() + ")");
	}
	
	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		semanticHelper.registerDeathImplication(clock, getImplicitClock());
		clock.deathSemantic(semanticHelper);
	}
	
	@Override
	public String toString() {
		String res = "[" + getImplicitClock().getName() + "]";
		if (state == State.INIT) {
			res += "^";
		}
		if ( isActiveState()) {
			res += "*";
		}
		res += " " + clock.getName() + " ^ " + initialDelay + "(" + delayCount + ")";
		if (state == State.DEAD) {
			res += "/D";
		}
		return res;
	}
	
	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = super.dumpState();
		//warning the order is important ! super puts 2 fields in the list
//		currentState.add(Serializer.getDefaultSerializer().toBytes(new Integer(delayCount)));
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		super.restoreState(newState);
//		delayCount = (Integer) Serializer.getDefaultSerializer().fromBytes(newState.get(2));
	}

	
}
