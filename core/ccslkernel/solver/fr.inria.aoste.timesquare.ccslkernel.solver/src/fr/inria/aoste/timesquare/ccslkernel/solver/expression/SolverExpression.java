/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.expression;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.AbstractConcreteMapping;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.runtime.IRuntimeContainer;
import fr.inria.aoste.timesquare.ccslkernel.runtime.SerializedConstraintState;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.expressions.AbstractRuntimeExpression;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractSemanticHelper;
import fr.inria.aoste.timesquare.ccslkernel.runtime.helpers.AbstractUpdateHelper;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverConcrete;
import fr.inria.aoste.timesquare.ccslkernel.solver.ISolverElement;
import fr.inria.aoste.timesquare.ccslkernel.solver.ImplicitClock;

public class SolverExpression extends AbstractRuntimeExpression implements ISolverConcrete, IRuntimeContainer {

	public static boolean traceStart = false;
	public static boolean traceSemantic = false;
	public static boolean traceUpdate = false;
	public static boolean traceTerminate = false;
	
//	private ImplicitClock implicitClock;
	private AbstractConcreteMapping<ISolverElement> abstractMapping;
	private InstantiationPath instantiationPath;
	private boolean orphan;
	
//	public enum State {INIT, ACTIVE, DEAD };
//	protected boolean wasBorn;
//	protected State state;
	
	public SolverExpression() {
		super();
		setOrphan(false);
//		this.state = State.INIT;
//		this.wasBorn = false;
		this.instantiatedElement = null;
	}
	
	public ImplicitClock getImplicitClock() {
		return (ImplicitClock) getExpressionClock();
	}

	public void setImplicitClock(ImplicitClock implicitClock) {
		setExpressionClock(implicitClock);
	}
	
	public AbstractConcreteMapping<ISolverElement> getAbstractMapping() {
		return abstractMapping;
	}

	public void setAbstractMapping(AbstractConcreteMapping<ISolverElement> abstractMapping) {
		this.abstractMapping = abstractMapping;
	}

	public InstantiationPath getInstantiationPath() {
		return instantiationPath;
	}

	public void setInstantiationPath(InstantiationPath arrayList) {
		this.instantiationPath = arrayList;
	}
	
	private InstantiatedElement instantiatedElement;

	public InstantiatedElement getInstantiatedElement() {
		return instantiatedElement;
	}

	public void setInstantiatedElement(InstantiatedElement instantiatedElement) {
		this.instantiatedElement = instantiatedElement;
	}

	public String getNameFromPath(String separator) {
		String res = "";
		for (Iterator<NamedElement> it = instantiationPath.iterator(); it.hasNext(); ) {
			res += it.next().getName();
			if (it.hasNext()) {
				res += separator;
			}
		}
		return res;
	}
	
	public boolean isTerminated() {
		return isDead();
	}

	@Override
	public void start(AbstractSemanticHelper helper) throws SimulationException {
		if ( ! canCallStart())
			return;
		super.start(helper);
		if (traceStart) {
			System.out.println("Entry: SolverExpression.start(" + toString() + ")");
		}
//			state = State.ACTIVE;
		if (traceStart) {
			System.out.println("Exit: SolverExpression.start(" + toString() + ")");
		}
	}
	
	@Override
	public void semantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if ( ! canCallSemantic())
			return;
		super.semantic(semanticHelper);
		if (traceSemantic) {
			System.out.println("Entry: SolverExpression.semantic(" + toString() + ")");
		}
		if (traceSemantic) {
			System.out.println("Exit: SolverExpression.semantic(" + toString() + ")");
		}
	}

	@Override
	public void deathSemantic(AbstractSemanticHelper semanticHelper)
			throws SimulationException {
		if (isDead()) {
			semanticHelper.registerDeadClock(getImplicitClock());
		}
	}

	@Override
	public void update(AbstractUpdateHelper helper)
			throws SimulationException {
		if ( ! canCallUpdate()) 
			return;
		super.update(helper);
		if (traceUpdate) {
			System.out.println("Entry: SolverExpression.update(" + toString() + ")");
		}
		if (traceUpdate) {
			System.out.println("Exit: SolverExpression.update(" + toString() + ")");
		}
	}

//	@Override
//	public void deathUpdate(AbstractUpdateHelper helper) throws SimulationException {
//		
//	}
	
	@Override
	public void terminate(AbstractUpdateHelper helper) throws SimulationException {
		if ( ! canCallTerminate())
			return;
		super.terminate(helper);
		if (traceTerminate) {
			System.out.println("Entry: SolverExpression.terminate(" + toString() + ")");
		}
//		if (state != State.ACTIVE) {
//			return;
//		}
//		state = State.DEAD; //useless done in super.terminate()
		getImplicitClock().setDead(true);
		helper.registerNewDeadClock(getImplicitClock());
		if (traceTerminate) {
			System.out.println("Exit: SolverExpression.terminate(" + toString() + ")");
		}		
	}
	
//	public boolean wasBorn() {
//		return wasBorn;		
//	}

	public boolean isOrphan() {
		return orphan;
	}

	public void setOrphan(boolean orphan) {
		this.orphan = orphan;
	}

	@Override
	public SerializedConstraintState dumpState() {
		SerializedConstraintState currentState = new SerializedConstraintState();
		//warning the order is important !
		currentState.dump(wasBorn());
		currentState.dump(new Integer(state.ordinal()));
		return currentState;
	}

	@Override
	public void restoreState(SerializedConstraintState newState) {
		born = (Boolean) newState.restore(0);
		state = State.values()[(Integer) newState.restore(1)];
	}

	private IRuntimeContainer parent;
	
	@Override
	public IRuntimeContainer getParent() {
		return parent;
	}

	@Override
	public void setParent(IRuntimeContainer parent) {
		this.parent = parent;
		if (parent != null)
			parent.addContent(this);
	}

	@Override
	public List<IRuntimeContainer> getContents() {
		return Collections.emptyList();
	}

	@Override
	public void addContent(IRuntimeContainer element) {
		
	}
	
}
