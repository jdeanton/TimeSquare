/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.explorer;

import grph.Grph;
import grph.oo.ObjectGrph;
import grph.path.Path;

public class StateSpace extends ObjectGrph<CCSLConstraintState, StringBuffer> {
	
	public Grph getGrph(){
		return this.backingGrph;
	}
	
	public CCSLConstraintState initialState = null;

	public int v2i(CCSLConstraintState v) {
		return super.v2i(v);
	}
	
	public int e2i(StringBuffer sb) {
		return super.e2i(sb);
	}
	
	public void changeEdgeColor(StringBuffer label, int color){
		this.getGrph().getEdgeColorProperty().setValue(e2i(label), color);
		this.getGrph().getEdgeWidthProperty().setValue(e2i(label), 3);
	}
	public void changeEdgeColorAndBig(StringBuffer label, int color){
		this.getGrph().getEdgeColorProperty().setValue(e2i(label), color);
		this.getGrph().getEdgeWidthProperty().setValue(e2i(label), 6);
	}
	
	public void changeVertexShape(CCSLConstraintState s, int shape){
		this.getGrph().getVertexShapeProperty().setValue(v2i(s), shape);
	}
	
	public void changeVertexColor(CCSLConstraintState s, int c){
		this.getGrph().getVertexColorProperty().setValue(v2i(s), c);
	}
	
	public Path getShortestPath(CCSLConstraintState source, CCSLConstraintState destination){
		return getGrph().getShortestPath(v2i(source), v2i(destination));
	}
	
	public void ChangeSinkColor(int color){
		for (Integer ic : this.getGrph().getSinks()){
			this.getGrph().getVertexColorProperty().setValue(ic, 15);
		}
	}
	
	public CCSLConstraintState i2v(int i){
		return super.i2v(i);
	}

	public StringBuffer i2e(int i){
		return super.i2e(i);
	}
	
}
