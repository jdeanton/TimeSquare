/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.solver.exception;

public class NotAnElement extends SolverException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7511559314872779586L;

	public NotAnElement() {
		// TODO Auto-generated constructor stub
	}

	public NotAnElement(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotAnElement(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NotAnElement(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
