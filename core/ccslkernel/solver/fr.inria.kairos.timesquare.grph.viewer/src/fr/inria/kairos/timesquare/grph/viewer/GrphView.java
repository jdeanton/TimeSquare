/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.kairos.timesquare.grph.viewer;

import java.util.ArrayList;

import javafx.scene.web.WebEngine;

public class GrphView {
	public GrphWebEngine internalView;
	public WebEngine engine;
	
	public GrphView() {
		internalView = new GrphWebEngine();
		engine = internalView.webEngine;
	}


	
	
	
	public String getSource(String edge) {
		return (String) engine.executeScript("cy.edges('#"+edge+"').source().id()");
	}

	public String getTarget(String edge) {
		return (String) engine.executeScript("cy.edges('#"+edge+"').target().id()");
	}
	
	public void addNode(String id, int index) {
		String command = "cy.add({group: \"nodes\",data:";
		command += "{id: \""+id+"\",index: "+index+"}});";
		engine.executeScript(command);
	}
	
	public void addEdge(String id, String label, String source, String target, boolean directed, int index) {
		String command = "cy.add({group: 'edges',data:{";
		command += "id:'"+id+"',";
		command += "label:'"+label+"',";
		command += "source:'"+source+"',";
		command += "target:'"+target+"',";
		command += "index:"+index+"}";
		command += ",classes: 'directed"+directed+"'})";
		engine.executeScript(command);
	}

	public int searchIndex(String string) {
		int temp = (int) engine.executeScript("searcheIndex('"+string+"')");
		return temp;
	}
	
	public String searchName(int index) {
		String temp = (String) engine.executeScript("searcheName("+index+")");
		return temp;
	}
	
	public void layout(String layout) {
		engine.executeScript("var layout = cy.layout({name: '"+layout+"'})");
		engine.executeScript("layout.run()");
	}

	public int layoutRun() {
		 Object temp = engine.executeScript("runLayout()");
		System.out.println(temp);
		return (int) temp ;
	}

	public void setLayout(String layout) {
		engine.executeScript("layout = cy.layout({name: '"+layout+"'})");
	}

	public void remove(String name) {
		engine.executeScript("cy.remove('#"+name+"')");
	}

	public boolean isNode(String node) {
		return (boolean) engine.executeScript("cy.elements('#"+node+"').isNode()");
	}

	public void removeAll(String clas) {
		engine.executeScript("cy.elements().removeClass('"+clas+"')");
	}

	public void clearAll() {
		engine.executeScript("cy.elements().remove()");
	}
	
	public void removeClass(String name, String clas) {
		engine.executeScript("cy.elements('#"+name+"').removeClass('"+clas+"')");
	}


	
	/*
	 * fonction de mise en valeur des information
	 */

	public void selectElements(ArrayList<String> names) {
		String toExec = "";
		for(String n : names) {
			toExec+="cy.getElementById('"+n+"').select()\n";
		}
		engine.executeScript(toExec);
		return;
	}
	
	public void unSelectElements(ArrayList<String> names) {
		String toExec = "";
		for(String n : names) {
			toExec+="cy.getElementById('"+n+"').unselect()\n";
		}
		engine.executeScript(toExec);
		return;
	}

	public void addClass(String element, String clas) {
		engine.executeScript("cy.elements('#"+element+"').addClass(\""+clas+"\");");
		return;
	}
	
//	public void removeClass(String element, String clas) {
//		engine.executeScript("cy.elements('#"+element+"').classList.remove(\""+clas+"\");");
//		return;
//	}
	

	public void unselectAll() {
		engine.executeScript("cy.$(':selected').unselect();");
	}
}
