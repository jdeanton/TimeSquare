/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.kairos.timesquare.grph.viewer;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GraphViewer extends Application {
	GrphView view;
	public GrphViewControler controller;
	
	public GraphViewer() {
		
	}
	
	
	@Override
	public void start(Stage primaryStage) throws Exception {
				
		view = new GrphView();
		controller = new GrphViewControler(view);
		
		BorderPane root = new BorderPane();
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth()/2;
		int height = gd.getDisplayMode().getHeight()-30;
		Scene scene = new Scene(root,width,height,Color.WHITE);
		view.internalView.modifyWidth(width -10);
		view.internalView.modifyHeight(height - 10);
		root.getChildren().add(view.internalView);
		
        root.setOnKeyPressed(event -> {
        	if (event.getCode() == KeyCode.C) {
					controller.highlightNextSchedule();
			}
        	if (event.getCode() == KeyCode.D) {
				controller.highlightNextDifferentSchedule(4);
        	}
		});
        
        root.setOnContextMenuRequested(new EventHandler<Event>() {
        	@Override
			public void handle(Event event) {
        		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        		int width = gd.getDisplayMode().getWidth()/2;
        		int height = gd.getDisplayMode().getHeight()-30;
        		
        		view.internalView.modifyWidth(width -10);
        		view.internalView.modifyHeight(height - 10);
			}
		});
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void addGrph() {
		controller.addGrph(0);
		return;
	}

	public static void main(String[] args) {
		        launch();    
	}


}
