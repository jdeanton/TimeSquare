/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.kairos.timesquare.grph.viewer;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;

//import java.net.URI;
//import java.nio.file.Paths;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.event.EventHandler;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;

public class GrphWebEngine extends Region{
	public static final String htmlPath = "/libs/Graph.html";
	public BooleanProperty ready = new SimpleBooleanProperty(false);
	public WebView browser = new WebView();
	public WebEngine webEngine = browser.getEngine();
	
	public GrphWebEngine() {
		
		URL bundleResourceURL = this.getClass().getResource(htmlPath);
		URL fileURL = null;
		try {
			/**
			 * warning, this next line is eclipse dependent...
			 */
			fileURL = FileLocator.resolve(bundleResourceURL);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		//TODO: make it pure Java clean
		webEngine.load(fileURL.toString());//"file:///home/jdeanton/boulot/recherche/Marte/eclipse/grphVisu/GrphVisu/fr.inria.kairos.grph.visualization/libs/Graph.html");//anotherurl.toString());
		getChildren().add(browser);
		//click();
		webEngine.getLoadWorker().stateProperty().addListener(
				new ChangeListener<State>() {
					@Override
					public void changed(ObservableValue<? extends State> observable, State oldValue, State newValue) {
						if (newValue == State.SUCCEEDED) {
							ready.setValue(true);
						}
					}
				});
	}
	
//	private void click() {
//		webEngine.setOnAlert(new EventHandler<WebEvent<String>>() {	
//			@Override
//			public void handle(WebEvent<String> event) {
//				System.out.println(event.getData());
//			}
//		});
//	}
	
	public void modifyWidth(double nw){
		browser.setPrefWidth(nw);
	}
	public void modifyHeight(double nh){
		browser.setPrefHeight(nh);
	}
}
