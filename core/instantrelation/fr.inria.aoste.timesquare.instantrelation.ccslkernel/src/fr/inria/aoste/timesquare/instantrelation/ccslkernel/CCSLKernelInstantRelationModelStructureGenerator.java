/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.ccslkernel;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.instantrelation.extensionpoint.IRelationModelStructureGenerator;
import fr.inria.aoste.timesquare.instantrelation.generator.InstantRelationModelGenerator;

public class CCSLKernelInstantRelationModelStructureGenerator implements IRelationModelStructureGenerator {

	public CCSLKernelInstantRelationModelStructureGenerator() {
		super();
		System.out.println("CCSL Factory");
	}


	public boolean createRelationModelStructure(EObject modelroot,
			InstantRelationModelGenerator relationModelSolver) {
		if (acceptModel(modelroot))
		{			
			CCSLKernelInstantRelationModelStructure ir = new CCSLKernelInstantRelationModelStructure(relationModelSolver);
			if (ir.getModelRoot(modelroot))
				return true;
		}
		return false;
	}

	public boolean acceptModel(EObject modelroot) {
		if (modelroot instanceof ClockConstraintSystem){
			return true;
		}
		return false;
	}

}
