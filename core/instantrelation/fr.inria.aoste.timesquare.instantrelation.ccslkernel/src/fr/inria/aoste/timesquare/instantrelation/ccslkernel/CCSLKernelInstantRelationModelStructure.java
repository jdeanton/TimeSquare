/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.ccslkernel;

import java.io.IOException;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.KernelRelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiationPath;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.generator.InstantRelationModelGenerator;
import fr.inria.aoste.trace.ModelElementReference;

public class CCSLKernelInstantRelationModelStructure {

	private InstantRelationModelGenerator instantRelationModelGenerator;
	
	public CCSLKernelInstantRelationModelStructure(InstantRelationModelGenerator relationModelSolver) {
		super();
		this.instantRelationModelGenerator = relationModelSolver;
	}
	
	public void setCCSLRelationModelSolver(InstantRelationModelGenerator relationModelSolver) {
		this.instantRelationModelGenerator = relationModelSolver;
	}
	
	public InstantRelationModelGenerator getCCSLRelationModelSolver() {
		return this.instantRelationModelGenerator;
	}

	public boolean getModelRoot(EObject eo1) {
		while (true) {
			if (eo1 == null)
				return false;
			if (eo1 instanceof ClockConstraintSystem) {
				setClockConstraintList((ClockConstraintSystem) eo1);
				return true;
			} else {
				eo1 = eo1.eContainer();
			}
		}
	}

	private void setClockConstraintList(ClockConstraintSystem clockConstraintSystem) {
		UnfoldModel umodel = null;
		try {
			Resource resource = clockConstraintSystem.eResource();
			if (resource == null) {
				resource = new XMIResourceImpl();
				resource.getContents().add(clockConstraintSystem);
			}
			umodel = UnfoldModel.unfoldModel(resource);
		} catch (IOException e1) {
			return;
		} catch (UnfoldingException e) {
			return;
		}
		
		KernelRelationConstraintBuilder rBuilder = new KernelRelationConstraintBuilder();
		KernelExpressionConstraintBuilder eBuilder = new KernelExpressionConstraintBuilder();
		for (InstantiatedElement element : umodel.getInstantiationTree().lookupInstances(null)) {
			/* TODO : the two following tests use the isConditionalCase() call to verify that the relation is
			 * conditional. For relations, relation which are not from the kernel do not carry any useful semantic
			 * information (contrary to non-kernel expression that specify an equality with the "root" of the expression).
			 * The right test to use is probably element.inConditionalBranch() that allows to get the case where non-kernel
			 * relations are used in a switch case of a conditional relation definition.
			 * The tests with inConditionalBranch() follow below but are not tested because such conditional relations
			 * have not been used up to now (8 nov. 2011).
			 */
			if (element.isKernelRelation() && (! element.isAssertion()) && ( ! element.isConditionCase())) {
				rBuilder.generateInstantRelation(element);
			}
			else if (element.isKernelRelation() && (! element.isAssertion()) && element.isConditionCase()) {
				InstantiatedElement test = element.getConditionTest();
				if (test != null && test.isConstant()) {
					Element testValue = test.getValue();
					if (testValue instanceof BooleanElement && ((BooleanElement)testValue).getValue()) {
						rBuilder.generateInstantRelation(element);
					}
				}
			}
			else if (element.isKernelRelation() && (! element.isAssertion()) && ( ! element.inConditionalBranch())) {
				rBuilder.generateInstantRelation(element);
			}
			else if (element.isKernelRelation() && (! element.isAssertion()) && element.inConditionalBranch()) {
				List<InstantiatedElement> tests = element.getBranchConditionTests();
				boolean allTrue = true;
				for (InstantiatedElement test : tests) {
					if (test.isConstant()) {
						Element testValue = test.getValue();
						if ( ! (testValue instanceof BooleanElement) || ((BooleanElement)testValue).getValue() == Boolean.FALSE) {
							allTrue = false;
							break;
						}
					}
					else {
						allTrue = false;
						break;
					}
				}
				if (allTrue) {
					rBuilder.generateInstantRelation(element);
				}
			}
			else if (element.isKernelExpression()) {
				eBuilder.generateInstantRelation(element);
			}
			else if (element.isRecursiveCall()) {
				// Do nothing for the moment...
			}
			else if (element.isExpression() && ( ! element.isConditional())) { // And not a Kernel expression (handled before)
				ModelElementReference expRef = getConstraintModelElementReference(element);
				ModelElementReference iClockRef = getClockModelElementReference(element);
				if (element.getRootExpression() != null) {
					ModelElementReference rootRef = getClockModelElementReference(element.getRootExpression());
					CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreateEqual(iClockRef, rootRef);
					cRef.setCcslConstraint(expRef);
				}
			}
			else if (element.isExpression() && element.isConditional()) {
				for (InstantiatedElement son : element.getSons()) {
					if (son.isConditionCase()) {
						InstantiatedElement test = son.getConditionTest();
						if (test != null && test.isConstant()) {
							Element testValue = test.getValue();
							if (testValue instanceof BooleanElement && ((BooleanElement)testValue).getValue()) {
								ModelElementReference expRef = getConstraintModelElementReference(element);
								ModelElementReference iClockRef = getClockModelElementReference(element);
								ModelElementReference caseRef = getClockModelElementReference(son);
								CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreateEqual(iClockRef, caseRef);
								cRef.setCcslConstraint(expRef);
							}
						}
					}
				}
			}
			else if (element.isRelation() && element.isConditional()) {
				
			}
		}
	}
	
	private ModelElementReference getClockModelElementReference(InstantiatedElement element) {
		InstantiationPath instPath = element.getInstantiationPath();
		EObject path[] = instPath.toArray(new EObject[instPath.size()]);
		ModelElementReference ref = instantRelationModelGenerator.searchClock(path);
		return ref;
	}

	private ModelElementReference getConstraintModelElementReference(InstantiatedElement element) {
		InstantiationPath instPath = element.getInstantiationPath();
		EObject path[] = instPath.toArray(new EObject[instPath.size()]);
		ModelElementReference ref = instantRelationModelGenerator.searchConstraint(path);
		return ref;
	}

	private class KernelRelationConstraintBuilder extends KernelRelationSwitch<CCSLConstraintRef> {
		
		private ModelElementReference relationRef;
		private ModelElementReference leftRef;
		private ModelElementReference rightRef;
		
		public void generateInstantRelation(InstantiatedElement relation) {
			relationRef = getConstraintModelElementReference(relation);
			KernelRelationDeclaration decl = (KernelRelationDeclaration) relation.getDeclaration();
			InstantiatedElement left = relation.resolveAbstractEntity(decl.getLeftEntity());
			leftRef = getClockModelElementReference(left);
			InstantiatedElement right = relation.resolveAbstractEntity(decl.getRightEntity());
			rightRef = getClockModelElementReference(right);
			CCSLConstraintRef cRef = doSwitch(decl);
			if (cRef != null) {
				cRef.setCcslConstraint(relationRef);
			}
		}
		
		@Override
		public CCSLConstraintRef caseCoincidence(Coincidence object) {
			CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreateEqual(leftRef, rightRef);
			return cRef;
		}
	
		@Override
		public CCSLConstraintRef casePrecedence(Precedence object) {
			CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreatePrecedes(leftRef, rightRef, 1, 1, true);
			return cRef;
		}
	
		@Override
		public CCSLConstraintRef caseNonStrictPrecedence(NonStrictPrecedence object) {
			CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreatePrecedes(leftRef, rightRef, 1, 1, false);
			return cRef;
		}

		@Override
		public CCSLConstraintRef caseSubClock(SubClock object) {
			CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreateEqual(leftRef, rightRef);
			return cRef;
		}
		
	}
	
	private class KernelExpressionConstraintBuilder extends KernelExpressionSwitch<CCSLConstraintRef> {
		
		private InstantiatedElement expression;
		private ModelElementReference expressionRef;
		
		public void generateInstantRelation(InstantiatedElement expression) {
			this.expression = expression;
			expressionRef = getConstraintModelElementReference(expression);
			CCSLConstraintRef cRef = doSwitch(expression.getDeclaration());
			if (cRef != null) {
				cRef.setCcslConstraint(expressionRef);
			}
		}
		
		@Override
		public CCSLConstraintRef caseConcatenation(Concatenation object) {
			InstantiatedElement left = expression.resolveAbstractEntity(object.getLeftClock());
			InstantiatedElement right = expression.resolveAbstractEntity(object.getRightClock());
			ModelElementReference leftRef = getClockModelElementReference(left);
			ModelElementReference rightRef = getClockModelElementReference(right);
			CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreateConcatenation(expressionRef, leftRef, rightRef);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseIntersection(Intersection object) {
			InstantiatedElement left = expression.resolveAbstractEntity(object.getClock1());
			InstantiatedElement right = expression.resolveAbstractEntity(object.getClock2());
			ModelElementReference leftRef = getClockModelElementReference(left);
			ModelElementReference rightRef = getClockModelElementReference(right);
			CCSLConstraintRef cRef = instantRelationModelGenerator.addandCreateInterOrUnion(expressionRef, leftRef, rightRef);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseUnion(Union object) {
			InstantiatedElement left = expression.resolveAbstractEntity(object.getClock1());
			InstantiatedElement right = expression.resolveAbstractEntity(object.getClock2());
			ModelElementReference leftRef = getClockModelElementReference(left);
			ModelElementReference rightRef = getClockModelElementReference(right);
			CCSLConstraintRef cRef = instantRelationModelGenerator.addandCreateInterOrUnion(expressionRef, leftRef, rightRef);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseDefer(Defer object) {
			InstantiatedElement base = expression.resolveAbstractEntity(object.getBaseClock());
			InstantiatedElement delay = expression.resolveAbstractEntity(object.getDelayClock());
			ModelElementReference baseRef = getClockModelElementReference(base);
			ModelElementReference delayRef = getClockModelElementReference(delay);
			InstantiatedElement delayPattern =  expression.resolveAbstractEntity(object.getDelayPattern());
			int constantDelay = -1;
			if (delayPattern.isConstant() && delayPattern.getValue() instanceof SequenceElement) {
				SequenceElement value = (SequenceElement) delayPattern.getValue();
				if (value.getFinitePart().isEmpty() && value.getNonFinitePart().size() == 1
						&& value.getNonFinitePart().get(0) instanceof IntegerElement) {
					constantDelay = ((IntegerElement)value.getNonFinitePart().get(0)).getValue();
				}
			}
			CCSLConstraintRef cRef = null;
			cRef = instantRelationModelGenerator.addandCreateDelayedFor(expressionRef, baseRef, delayRef, constantDelay);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseStrictSampling(StrictSampling object) {
			InstantiatedElement sampling = expression.resolveAbstractEntity(object.getSamplingClock());
			InstantiatedElement sampled = expression.resolveAbstractEntity(object.getSampledClock());
			ModelElementReference samplingRef = getClockModelElementReference(sampling);
			ModelElementReference sampledRef = getClockModelElementReference(sampled);
			CCSLConstraintRef cRef = instantRelationModelGenerator.addandCreateSampledOn(expressionRef,
					samplingRef, sampledRef, true);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseNonStrictSampling(NonStrictSampling object) {
			InstantiatedElement sampling = expression.resolveAbstractEntity(object.getSamplingClock());
			InstantiatedElement sampled = expression.resolveAbstractEntity(object.getSampledClock());
			ModelElementReference samplingRef = getClockModelElementReference(sampling);
			ModelElementReference sampledRef = getClockModelElementReference(sampled);
			CCSLConstraintRef cRef = instantRelationModelGenerator.addandCreateSampledOn(expressionRef,
					samplingRef, sampledRef, false);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseInf(Inf object) {
			InstantiatedElement left = expression.resolveAbstractEntity(object.getClock1());
			InstantiatedElement right = expression.resolveAbstractEntity(object.getClock2());
			ModelElementReference leftRef = getClockModelElementReference(left);
			ModelElementReference rightRef = getClockModelElementReference(right);
			ModelElementReference[] refTab = new ModelElementReference[2];
			refTab[0] = leftRef;
			refTab[1] = rightRef;
			CCSLConstraintRef cRef = instantRelationModelGenerator.addandCreateInf(expressionRef, refTab);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseSup(Sup object) {
			InstantiatedElement left = expression.resolveAbstractEntity(object.getClock1());
			InstantiatedElement right = expression.resolveAbstractEntity(object.getClock2());
			ModelElementReference leftRef = getClockModelElementReference(left);
			ModelElementReference rightRef = getClockModelElementReference(right);
			ModelElementReference[] refTab = new ModelElementReference[2];
			refTab[0] = leftRef;
			refTab[1] = rightRef;
			CCSLConstraintRef cRef = instantRelationModelGenerator.addandCreateSup(expressionRef, refTab);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseUpTo(UpTo object) {
			InstantiatedElement follow = expression.resolveAbstractEntity(object.getClockToFollow());
			InstantiatedElement killer = expression.resolveAbstractEntity(object.getKillerClock());
			ModelElementReference followRef = getClockModelElementReference(follow);
			ModelElementReference killerRef = getClockModelElementReference(killer);
			CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreateUpTo(expressionRef, followRef, killerRef);
			return cRef;
		}
		
		@Override
		public CCSLConstraintRef caseWait(Wait object) {
			InstantiatedElement waiting = expression.resolveAbstractEntity(object.getWaitingClock());
			ModelElementReference waitingRef = getClockModelElementReference(waiting);
			CCSLConstraintRef cRef = instantRelationModelGenerator.addAndCreateEqual(expressionRef, waitingRef);
			return cRef;
		}
		
	}
		
}