/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace.relation;

import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;

public interface IRelation {

	public final Class<IRelation> CLASS= IRelation.class;
	
	public void saveRelationModel(IPath folderin, String namefilein);
	public void saveRelationModel(ResourceSet rs,  IPath folderin, String namefilein);
	public void addClockConstraint(LogicalStep stepTrace) ;
	public void resolve(LogicalStep stepTrace) ;
	public void unload();
	
	public void setIOutputTraceList(IOutputTraceList outputlist);
	
	public void extract(EObject eo1);
	public List<IDescription> getDescription();
	
	public void disp();
	
	
	public void setListClock(List<ModelElementReference> listClock) ;

	public void setListRelation(List<ModelElementReference> listrelation) ;

	
	
}
