/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelationModel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Occurrence Relation Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.OccurrenceRelationModelImpl#getRefsToCCSLConstraints <em>Refs To CCSL Constraints</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OccurrenceRelationModelImpl extends EObjectImpl implements OccurrenceRelationModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "AOSTE INRIA / I3S";

	/**
	 * The cached value of the '{@link #getRefsToCCSLConstraints() <em>Refs To CCSL Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefsToCCSLConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<CCSLConstraintRef> refsToCCSLConstraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OccurrenceRelationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CCSLRelationModelPackage.eINSTANCE.getOccurrenceRelationModel();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CCSLConstraintRef> getRefsToCCSLConstraints() {
		if (refsToCCSLConstraints == null) {
			refsToCCSLConstraints = new EObjectContainmentEList<CCSLConstraintRef>(CCSLConstraintRef.class, this, CCSLRelationModelPackage.OCCURRENCE_RELATION_MODEL__REFS_TO_CCSL_CONSTRAINTS);
		}
		return refsToCCSLConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CCSLRelationModelPackage.OCCURRENCE_RELATION_MODEL__REFS_TO_CCSL_CONSTRAINTS:
				return ((InternalEList<?>)getRefsToCCSLConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CCSLRelationModelPackage.OCCURRENCE_RELATION_MODEL__REFS_TO_CCSL_CONSTRAINTS:
				return getRefsToCCSLConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CCSLRelationModelPackage.OCCURRENCE_RELATION_MODEL__REFS_TO_CCSL_CONSTRAINTS:
				getRefsToCCSLConstraints().clear();
				getRefsToCCSLConstraints().addAll((Collection<? extends CCSLConstraintRef>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CCSLRelationModelPackage.OCCURRENCE_RELATION_MODEL__REFS_TO_CCSL_CONSTRAINTS:
				getRefsToCCSLConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CCSLRelationModelPackage.OCCURRENCE_RELATION_MODEL__REFS_TO_CCSL_CONSTRAINTS:
				return refsToCCSLConstraints != null && !refsToCCSLConstraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OccurrenceRelationModelImpl
