/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence;
import fr.inria.aoste.trace.EventOccurrence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Coincidence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CoincidenceImpl#getCoincidentOccurrences <em>Coincident Occurrences</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CoincidenceImpl extends OccurrenceRelationImpl implements Coincidence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "AOSTE INRIA / I3S";

	/**
	 * The cached value of the '{@link #getCoincidentOccurrences() <em>Coincident Occurrences</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoincidentOccurrences()
	 * @generated
	 * @ordered
	 */
	protected EList<EventOccurrence> coincidentOccurrences;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoincidenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CCSLRelationModelPackage.eINSTANCE.getCoincidence();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventOccurrence> getCoincidentOccurrences() {
		if (coincidentOccurrences == null) {
			coincidentOccurrences = new EObjectResolvingEList<EventOccurrence>(EventOccurrence.class, this, CCSLRelationModelPackage.COINCIDENCE__COINCIDENT_OCCURRENCES);
		}
		return coincidentOccurrences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CCSLRelationModelPackage.COINCIDENCE__COINCIDENT_OCCURRENCES:
				return getCoincidentOccurrences();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CCSLRelationModelPackage.COINCIDENCE__COINCIDENT_OCCURRENCES:
				getCoincidentOccurrences().clear();
				getCoincidentOccurrences().addAll((Collection<? extends EventOccurrence>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CCSLRelationModelPackage.COINCIDENCE__COINCIDENT_OCCURRENCES:
				getCoincidentOccurrences().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CCSLRelationModelPackage.COINCIDENCE__COINCIDENT_OCCURRENCES:
				return coincidentOccurrences != null && !coincidentOccurrences.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CoincidenceImpl
