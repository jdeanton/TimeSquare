/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl;

import java.io.IOException;
import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelFactory;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage;
import fr.inria.aoste.trace.TracePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CCSLRelationModelPackageImpl extends EPackageImpl implements CCSLRelationModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "AOSTE INRIA / I3S";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected String packageFilename = "CCSLRelationModel.ecore";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass occurrenceRelationModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass occurrenceRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccslConstraintRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coincidenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass precedenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CCSLRelationModelPackageImpl() {
		super(eNS_URI, CCSLRelationModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CCSLRelationModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static CCSLRelationModelPackage init() {
		if (isInited) return (CCSLRelationModelPackage)EPackage.Registry.INSTANCE.getEPackage(CCSLRelationModelPackage.eNS_URI);

		// Obtain or create and register package
		CCSLRelationModelPackageImpl theCCSLRelationModelPackage = (CCSLRelationModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CCSLRelationModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CCSLRelationModelPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TracePackage.eINSTANCE.eClass();

		// Load packages
		theCCSLRelationModelPackage.loadPackage();

		// Fix loaded packages
		theCCSLRelationModelPackage.fixPackageContents();

		// Mark meta-data to indicate it can't be changed
		theCCSLRelationModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CCSLRelationModelPackage.eNS_URI, theCCSLRelationModelPackage);
		return theCCSLRelationModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOccurrenceRelationModel() {
		if (occurrenceRelationModelEClass == null) {
			occurrenceRelationModelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(CCSLRelationModelPackage.eNS_URI).getEClassifiers().get(0);
		}
		return occurrenceRelationModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOccurrenceRelationModel_RefsToCCSLConstraints() {
        return (EReference)getOccurrenceRelationModel().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOccurrenceRelation() {
		if (occurrenceRelationEClass == null) {
			occurrenceRelationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(CCSLRelationModelPackage.eNS_URI).getEClassifiers().get(1);
		}
		return occurrenceRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCCSLConstraintRef() {
		if (ccslConstraintRefEClass == null) {
			ccslConstraintRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(CCSLRelationModelPackage.eNS_URI).getEClassifiers().get(2);
		}
		return ccslConstraintRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCCSLConstraintRef_CcslConstraint() {
        return (EReference)getCCSLConstraintRef().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCCSLConstraintRef_CcslElements() {
        return (EReference)getCCSLConstraintRef().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCCSLConstraintRef_RelatedOccurrenceRelations() {
        return (EReference)getCCSLConstraintRef().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCoincidence() {
		if (coincidenceEClass == null) {
			coincidenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(CCSLRelationModelPackage.eNS_URI).getEClassifiers().get(3);
		}
		return coincidenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCoincidence_CoincidentOccurrences() {
        return (EReference)getCoincidence().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrecedence() {
		if (precedenceEClass == null) {
			precedenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(CCSLRelationModelPackage.eNS_URI).getEClassifiers().get(4);
		}
		return precedenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrecedence_Source() {
        return (EReference)getPrecedence().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrecedence_Target() {
        return (EReference)getPrecedence().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPrecedence_IsStrict() {
        return (EAttribute)getPrecedence().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPacket() {
		if (packetEClass == null) {
			packetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(CCSLRelationModelPackage.eNS_URI).getEClassifiers().get(5);
		}
		return packetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPacket_Occurrences() {
        return (EReference)getPacket().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CCSLRelationModelFactory getCCSLRelationModelFactory() {
		return (CCSLRelationModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isLoaded = false;

	/**
	 * Laods the package and any sub-packages from their serialized form.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadPackage() {
		if (isLoaded) return;
		isLoaded = true;

		URL url = getClass().getResource(packageFilename);
		if (url == null) {
			throw new RuntimeException("Missing serialized package: " + packageFilename);
		}
		URI uri = URI.createURI(url.toString());
		Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
		try {
			resource.load(null);
		}
		catch (IOException exception) {
			throw new WrappedException(exception);
		}
		initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
		createResource(eNS_URI);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed) return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName("fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //CCSLRelationModelPackageImpl
