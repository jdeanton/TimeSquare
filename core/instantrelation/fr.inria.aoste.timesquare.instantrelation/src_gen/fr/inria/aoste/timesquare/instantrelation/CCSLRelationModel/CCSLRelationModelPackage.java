/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelFactory
 * @model kind="package"
 * @generated
 */
public interface CCSLRelationModelPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "AOSTE INRIA / I3S";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CCSLRelationModel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CCSLRelationModelPackage eINSTANCE = fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLRelationModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.OccurrenceRelationModelImpl <em>Occurrence Relation Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.OccurrenceRelationModelImpl
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLRelationModelPackageImpl#getOccurrenceRelationModel()
	 * @generated
	 */
	int OCCURRENCE_RELATION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Refs To CCSL Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCURRENCE_RELATION_MODEL__REFS_TO_CCSL_CONSTRAINTS = 0;

	/**
	 * The number of structural features of the '<em>Occurrence Relation Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCURRENCE_RELATION_MODEL_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.OccurrenceRelationImpl <em>Occurrence Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.OccurrenceRelationImpl
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLRelationModelPackageImpl#getOccurrenceRelation()
	 * @generated
	 */
	int OCCURRENCE_RELATION = 1;

	/**
	 * The number of structural features of the '<em>Occurrence Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCURRENCE_RELATION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLConstraintRefImpl <em>CCSL Constraint Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLConstraintRefImpl
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLRelationModelPackageImpl#getCCSLConstraintRef()
	 * @generated
	 */
	int CCSL_CONSTRAINT_REF = 2;

	/**
	 * The feature id for the '<em><b>Ccsl Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCSL_CONSTRAINT_REF__CCSL_CONSTRAINT = 0;

	/**
	 * The feature id for the '<em><b>Ccsl Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCSL_CONSTRAINT_REF__CCSL_ELEMENTS = 1;

	/**
	 * The feature id for the '<em><b>Related Occurrence Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCSL_CONSTRAINT_REF__RELATED_OCCURRENCE_RELATIONS = 2;

	/**
	 * The number of structural features of the '<em>CCSL Constraint Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCSL_CONSTRAINT_REF_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CoincidenceImpl <em>Coincidence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CoincidenceImpl
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLRelationModelPackageImpl#getCoincidence()
	 * @generated
	 */
	int COINCIDENCE = 3;

	/**
	 * The feature id for the '<em><b>Coincident Occurrences</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COINCIDENCE__COINCIDENT_OCCURRENCES = OCCURRENCE_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Coincidence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COINCIDENCE_FEATURE_COUNT = OCCURRENCE_RELATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.PrecedenceImpl <em>Precedence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.PrecedenceImpl
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLRelationModelPackageImpl#getPrecedence()
	 * @generated
	 */
	int PRECEDENCE = 4;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECEDENCE__SOURCE = OCCURRENCE_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECEDENCE__TARGET = OCCURRENCE_RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Strict</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECEDENCE__IS_STRICT = OCCURRENCE_RELATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Precedence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRECEDENCE_FEATURE_COUNT = OCCURRENCE_RELATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.PacketImpl <em>Packet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.PacketImpl
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLRelationModelPackageImpl#getPacket()
	 * @generated
	 */
	int PACKET = 5;

	/**
	 * The feature id for the '<em><b>Occurrences</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET__OCCURRENCES = OCCURRENCE_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Packet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_FEATURE_COUNT = OCCURRENCE_RELATION_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelationModel <em>Occurrence Relation Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Occurrence Relation Model</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelationModel
	 * @generated
	 */
	EClass getOccurrenceRelationModel();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelationModel#getRefsToCCSLConstraints <em>Refs To CCSL Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Refs To CCSL Constraints</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelationModel#getRefsToCCSLConstraints()
	 * @see #getOccurrenceRelationModel()
	 * @generated
	 */
	EReference getOccurrenceRelationModel_RefsToCCSLConstraints();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation <em>Occurrence Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Occurrence Relation</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation
	 * @generated
	 */
	EClass getOccurrenceRelation();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef <em>CCSL Constraint Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CCSL Constraint Ref</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef
	 * @generated
	 */
	EClass getCCSLConstraintRef();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getCcslConstraint <em>Ccsl Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ccsl Constraint</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getCcslConstraint()
	 * @see #getCCSLConstraintRef()
	 * @generated
	 */
	EReference getCCSLConstraintRef_CcslConstraint();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getCcslElements <em>Ccsl Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ccsl Elements</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getCcslElements()
	 * @see #getCCSLConstraintRef()
	 * @generated
	 */
	EReference getCCSLConstraintRef_CcslElements();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getRelatedOccurrenceRelations <em>Related Occurrence Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Related Occurrence Relations</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getRelatedOccurrenceRelations()
	 * @see #getCCSLConstraintRef()
	 * @generated
	 */
	EReference getCCSLConstraintRef_RelatedOccurrenceRelations();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence <em>Coincidence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coincidence</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence
	 * @generated
	 */
	EClass getCoincidence();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence#getCoincidentOccurrences <em>Coincident Occurrences</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Coincident Occurrences</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence#getCoincidentOccurrences()
	 * @see #getCoincidence()
	 * @generated
	 */
	EReference getCoincidence_CoincidentOccurrences();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence <em>Precedence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Precedence</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence
	 * @generated
	 */
	EClass getPrecedence();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence#getSource()
	 * @see #getPrecedence()
	 * @generated
	 */
	EReference getPrecedence_Source();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence#getTarget()
	 * @see #getPrecedence()
	 * @generated
	 */
	EReference getPrecedence_Target();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence#isIsStrict <em>Is Strict</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Strict</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence#isIsStrict()
	 * @see #getPrecedence()
	 * @generated
	 */
	EAttribute getPrecedence_IsStrict();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet <em>Packet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Packet</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet
	 * @generated
	 */
	EClass getPacket();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet#getOccurrences <em>Occurrences</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Occurrences</em>'.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet#getOccurrences()
	 * @see #getPacket()
	 * @generated
	 */
	EReference getPacket_Occurrences();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CCSLRelationModelFactory getCCSLRelationModelFactory();

} //CCSLRelationModelPackage
