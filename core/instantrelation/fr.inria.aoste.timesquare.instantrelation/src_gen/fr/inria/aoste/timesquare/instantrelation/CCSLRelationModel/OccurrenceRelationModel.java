/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Occurrence Relation Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelationModel#getRefsToCCSLConstraints <em>Refs To CCSL Constraints</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getOccurrenceRelationModel()
 * @model
 * @generated
 */
public interface OccurrenceRelationModel extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "AOSTE INRIA / I3S";

	/**
	 * Returns the value of the '<em><b>Refs To CCSL Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refs To CCSL Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refs To CCSL Constraints</em>' containment reference list.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getOccurrenceRelationModel_RefsToCCSLConstraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<CCSLConstraintRef> getRefsToCCSLConstraints();

} // OccurrenceRelationModel
