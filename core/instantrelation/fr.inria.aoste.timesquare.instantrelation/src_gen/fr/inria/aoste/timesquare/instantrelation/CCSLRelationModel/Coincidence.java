/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel;

import org.eclipse.emf.common.util.EList;

import fr.inria.aoste.trace.EventOccurrence;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Coincidence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence#getCoincidentOccurrences <em>Coincident Occurrences</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getCoincidence()
 * @model
 * @generated
 */
public interface Coincidence extends OccurrenceRelation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "AOSTE INRIA / I3S";

	/**
	 * Returns the value of the '<em><b>Coincident Occurrences</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.aoste.trace.EventOccurrence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coincident Occurrences</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coincident Occurrences</em>' reference list.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getCoincidence_CoincidentOccurrences()
	 * @model lower="2"
	 * @generated
	 */
	EList<EventOccurrence> getCoincidentOccurrences();

} // Coincidence
