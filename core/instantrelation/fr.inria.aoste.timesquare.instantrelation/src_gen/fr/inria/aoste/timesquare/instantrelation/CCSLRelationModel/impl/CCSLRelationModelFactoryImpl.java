/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelFactory;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelationModel;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CCSLRelationModelFactoryImpl extends EFactoryImpl implements CCSLRelationModelFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "AOSTE INRIA / I3S";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CCSLRelationModelFactory init() {
		try {
			CCSLRelationModelFactory theCCSLRelationModelFactory = (CCSLRelationModelFactory)EPackage.Registry.INSTANCE.getEFactory("http://fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel"); 
			if (theCCSLRelationModelFactory != null) {
				return theCCSLRelationModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CCSLRelationModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CCSLRelationModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CCSLRelationModelPackage.OCCURRENCE_RELATION_MODEL: return createOccurrenceRelationModel();
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF: return createCCSLConstraintRef();
			case CCSLRelationModelPackage.COINCIDENCE: return createCoincidence();
			case CCSLRelationModelPackage.PRECEDENCE: return createPrecedence();
			case CCSLRelationModelPackage.PACKET: return createPacket();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OccurrenceRelationModel createOccurrenceRelationModel() {
		OccurrenceRelationModelImpl occurrenceRelationModel = new OccurrenceRelationModelImpl();
		return occurrenceRelationModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CCSLConstraintRef createCCSLConstraintRef() {
		CCSLConstraintRefImpl ccslConstraintRef = new CCSLConstraintRefImpl();
		return ccslConstraintRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coincidence createCoincidence() {
		CoincidenceImpl coincidence = new CoincidenceImpl();
		return coincidence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Precedence createPrecedence() {
		PrecedenceImpl precedence = new PrecedenceImpl();
		return precedence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Packet createPacket() {
		PacketImpl packet = new PacketImpl();
		return packet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CCSLRelationModelPackage getCCSLRelationModelPackage() {
		return (CCSLRelationModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CCSLRelationModelPackage getPackage() {
		return CCSLRelationModelPackage.eINSTANCE;
	}

} //CCSLRelationModelFactoryImpl
