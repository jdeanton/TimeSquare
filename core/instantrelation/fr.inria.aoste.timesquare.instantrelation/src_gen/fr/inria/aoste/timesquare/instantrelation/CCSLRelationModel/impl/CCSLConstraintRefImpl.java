/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CCSL Constraint Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLConstraintRefImpl#getCcslConstraint <em>Ccsl Constraint</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLConstraintRefImpl#getCcslElements <em>Ccsl Elements</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.impl.CCSLConstraintRefImpl#getRelatedOccurrenceRelations <em>Related Occurrence Relations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CCSLConstraintRefImpl extends EObjectImpl implements CCSLConstraintRef {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "AOSTE INRIA / I3S";

	/**
	 * The cached value of the '{@link #getCcslConstraint() <em>Ccsl Constraint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCcslConstraint()
	 * @generated
	 * @ordered
	 */
	protected Reference ccslConstraint;

	/**
	 * The cached value of the '{@link #getCcslElements() <em>Ccsl Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCcslElements()
	 * @generated
	 * @ordered
	 */
	protected EList<Reference> ccslElements;

	/**
	 * The cached value of the '{@link #getRelatedOccurrenceRelations() <em>Related Occurrence Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedOccurrenceRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<OccurrenceRelation> relatedOccurrenceRelations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CCSLConstraintRefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CCSLRelationModelPackage.eINSTANCE.getCCSLConstraintRef();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference getCcslConstraint() {
		if (ccslConstraint != null && ccslConstraint.eIsProxy()) {
			InternalEObject oldCcslConstraint = (InternalEObject)ccslConstraint;
			ccslConstraint = (Reference)eResolveProxy(oldCcslConstraint);
			if (ccslConstraint != oldCcslConstraint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_CONSTRAINT, oldCcslConstraint, ccslConstraint));
			}
		}
		return ccslConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetCcslConstraint() {
		return ccslConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCcslConstraint(Reference newCcslConstraint) {
		Reference oldCcslConstraint = ccslConstraint;
		ccslConstraint = newCcslConstraint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_CONSTRAINT, oldCcslConstraint, ccslConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Reference> getCcslElements() {
		if (ccslElements == null) {
			ccslElements = new EObjectResolvingEList<Reference>(Reference.class, this, CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_ELEMENTS);
		}
		return ccslElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OccurrenceRelation> getRelatedOccurrenceRelations() {
		if (relatedOccurrenceRelations == null) {
			relatedOccurrenceRelations = new EObjectContainmentEList<OccurrenceRelation>(OccurrenceRelation.class, this, CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__RELATED_OCCURRENCE_RELATIONS);
		}
		return relatedOccurrenceRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__RELATED_OCCURRENCE_RELATIONS:
				return ((InternalEList<?>)getRelatedOccurrenceRelations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_CONSTRAINT:
				if (resolve) return getCcslConstraint();
				return basicGetCcslConstraint();
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_ELEMENTS:
				return getCcslElements();
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__RELATED_OCCURRENCE_RELATIONS:
				return getRelatedOccurrenceRelations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_CONSTRAINT:
				setCcslConstraint((Reference)newValue);
				return;
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_ELEMENTS:
				getCcslElements().clear();
				getCcslElements().addAll((Collection<? extends Reference>)newValue);
				return;
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__RELATED_OCCURRENCE_RELATIONS:
				getRelatedOccurrenceRelations().clear();
				getRelatedOccurrenceRelations().addAll((Collection<? extends OccurrenceRelation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_CONSTRAINT:
				setCcslConstraint((Reference)null);
				return;
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_ELEMENTS:
				getCcslElements().clear();
				return;
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__RELATED_OCCURRENCE_RELATIONS:
				getRelatedOccurrenceRelations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_CONSTRAINT:
				return ccslConstraint != null;
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__CCSL_ELEMENTS:
				return ccslElements != null && !ccslElements.isEmpty();
			case CCSLRelationModelPackage.CCSL_CONSTRAINT_REF__RELATED_OCCURRENCE_RELATIONS:
				return relatedOccurrenceRelations != null && !relatedOccurrenceRelations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CCSLConstraintRefImpl
