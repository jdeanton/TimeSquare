/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Occurrence Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getOccurrenceRelation()
 * @model abstract="true"
 * @generated
 */
public interface OccurrenceRelation extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "AOSTE INRIA / I3S";

} // OccurrenceRelation
