/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * AOSTE INRIA / I3S
 */
package fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel;

import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.ModelElementReference;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CCSL Constraint Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getCcslConstraint <em>Ccsl Constraint</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getCcslElements <em>Ccsl Elements</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getRelatedOccurrenceRelations <em>Related Occurrence Relations</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getCCSLConstraintRef()
 * @model
 * @generated
 */
public interface CCSLConstraintRef extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "AOSTE INRIA / I3S";

	/**
	 * Returns the value of the '<em><b>Ccsl Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ccsl Constraint</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ccsl Constraint</em>' reference.
	 * @see #setCcslConstraint(Reference)
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getCCSLConstraintRef_CcslConstraint()
	 * @model required="true"
	 * @generated
	 */
	Reference getCcslConstraint();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef#getCcslConstraint <em>Ccsl Constraint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ccsl Constraint</em>' reference.
	 * @see #getCcslConstraint()
	 * @generated
	 */
	void setCcslConstraint(Reference value);

	/**
	 * Returns the value of the '<em><b>Ccsl Elements</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.aoste.trace.Reference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ccsl Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ccsl Elements</em>' reference list.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getCCSLConstraintRef_CcslElements()
	 * @model
	 * @generated
	 */
	EList<Reference> getCcslElements();

	/**
	 * Returns the value of the '<em><b>Related Occurrence Relations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related Occurrence Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related Occurrence Relations</em>' containment reference list.
	 * @see fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelPackage#getCCSLConstraintRef_RelatedOccurrenceRelations()
	 * @model containment="true"
	 * @generated
	 */
	EList<OccurrenceRelation> getRelatedOccurrenceRelations();

} // CCSLConstraintRef
