/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * for all i>=0 , create a CCSLRelationModel.Precedence (strict ) between
 * (<b>source[i*sizesource+offsetsource]</b> and
 * <b>target[(i-1)*sizetarget+1+offsettarget]</b> )
 * 
 * @author bferrero
 * 
 */
public class CreatorPrecedesByOffset extends CreatorPrecedesBy {

	int offsetsource = 0;
	int offsettarget = 0;

	int initoffsetsource = 0;
	int initoffsettarget = 0;

	public CreatorPrecedesByOffset(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target,
			boolean strict, int sizesource, int offsetsource, int sizetarget, int offsettarget) {
		super(cref, source, target, strict, sizesource, sizetarget);
		this.offsetsource = offsetsource;
		this.offsettarget = offsettarget;
		this.initoffsetsource = offsetsource;
		this.initoffsettarget = offsettarget;
	}

	@Override
	protected boolean sourcevalid() {
		if (offsetsource == 0)
			return super.sourcevalid();
		offsetsource--;
		return false;
	}

	@Override
	protected boolean targetvalid() {
		if (offsettarget == 0)
			return super.targetvalid();
		offsettarget--;
		return false;
	}

	@Override
	protected void init() {

		offsetsource = initoffsetsource;
		offsettarget = initoffsettarget;
	}

}
