/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * for all i>=0 create a CCSLRelationModel.Precedence (strict ) between
 * (source[i] and target[i] ) if (source[i] and target[i] ) are not coincidence
 * 
 * @author bferrero
 * 
 */
public class CreatorPrecedesSup extends CreatorPrecedes {

	public CreatorPrecedesSup(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target) {
		super(cref, source, target, false);

	}

	protected void resolve(EventOccurrence esource, EventOccurrence etarget) throws ResolveClockConstraintException {
		boolean bsource = clockTick(esource);
		boolean btarget = clockTick(etarget);
		if (bsource && btarget) {
			if ((esource.getCounter() - deltasource) == (etarget.getCounter() - deltadest))
				/* source[i-deltasource] et target[i-deltadest] => coincidence */
				return;
		}
		super.resolve(esource, etarget);
	}

	int deltasource = 0;
	int deltadest = 0;

	@Override
	protected void init() {

		deltasource = getEventOccurrenceOnStepTrace(getSource()).getCounter();
		deltadest = getEventOccurrenceOnStepTrace(getTarget()).getCounter();
		if (clockTick(getEventOccurrenceOnStepTrace(getSource()))) {
			deltasource--;
		}
		if (clockTick(getEventOccurrenceOnStepTrace(getTarget()))) {
			deltadest--;
		}
	}

}
