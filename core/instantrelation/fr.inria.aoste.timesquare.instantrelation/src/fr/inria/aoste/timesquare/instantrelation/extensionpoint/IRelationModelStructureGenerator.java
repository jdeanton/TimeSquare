/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.extensionpoint;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.instantrelation.generator.InstantRelationModelGenerator;
// TODO Empty
public interface IRelationModelStructureGenerator {

	public boolean createRelationModelStructure(EObject modelroot,InstantRelationModelGenerator relationModelSolver );
	
	public boolean  acceptModel(EObject modelroot);
	
	
	
}
