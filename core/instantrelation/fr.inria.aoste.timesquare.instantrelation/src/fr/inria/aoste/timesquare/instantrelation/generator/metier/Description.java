/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import java.util.Arrays;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.relation.IDescription;

public class Description implements IDescription {

	public static Description createDescriptionCoincidence(ModelElementReference eo1, ModelElementReference eo2,
			CCSLConstraintRef cref) {
		return new Description(Type.coincidence, new ModelElementReference[] { eo1, eo2 }, true, cref);
	}

	public static Description createDescriptionPrecede(ModelElementReference eo1, ModelElementReference eo2,
			boolean strictly, CCSLConstraintRef cref) {
		return new Description(Type.precedence, new ModelElementReference[] { eo1, eo2 }, strictly, cref);
	}

	public static Description createDescriptionPacket(ModelElementReference eo1, CCSLConstraintRef cref) {
		return new Description(Type.packet, new ModelElementReference[] { eo1 }, true, cref);
	}

	final protected Type type;

	final protected ModelElementReference clock[];

	final protected boolean strict;

	final protected CCSLConstraintRef source;

	public CCSLConstraintRef getRelationConstraint() {
		return source;
	}

	private Description(Type type, ModelElementReference[] clock, boolean strict, CCSLConstraintRef cref) {
		super();
		assert (cref != null);
		assert (type != null);
		assert (clock != null);
		for (EObject eo : clock)
			assert (eo != null);
		this.type = type;
		this.clock = clock;
		this.strict = strict;
		this.source = cref;
	}

	public final Type getType() {
		return type;
	}

	public final EObject[] getClock() {
		return clock.clone();
	}

	public final boolean isStrict() {
		return strict;
	}

	protected boolean checkEventOcurrence(EventOccurrence src, ModelElementReference eo) {
		if (src.getReferedElement() instanceof ModelElementReference) {
			ModelElementReference mer = (ModelElementReference) src.getReferedElement();
			// if (mer.getElementRef().get(0) != eo)
			if (mer == eo) {
				return true;
			}
		} else {
			return false;
		}
		return true;

	}

	public boolean check(EObject eo) {
		if (eo instanceof OccurrenceRelation)
			return checkOccurrenceRelation((OccurrenceRelation) eo);
		return false;
	}

	public boolean checkOccurrenceRelation(OccurrenceRelation oc) {
		if (oc == null)
			return false;
		if (oc.eContainer() != null && source != null) {
			if (oc.eContainer() != source)
				return false;
		}

		switch (type) {
		case coincidence:
			if (oc instanceof Coincidence) {
				Coincidence c = (Coincidence) oc;
				for (EventOccurrence ev : c.getCoincidentOccurrences()) {
					boolean b = false;
					for (ModelElementReference e : clock) {
						if (checkEventOcurrence(ev, e))
							b = true;

					}
					if (!b)
						return false;
				}
				return true;
			}
			return false;

		case precedence:
			if (oc instanceof Precedence) {
				Precedence p = (Precedence) oc;
				ModelElementReference e0 = clock[0];
				ModelElementReference e1 = clock[1];
				EventOccurrence src = p.getSource();
				EventOccurrence trg = p.getTarget();
				if (!checkEventOcurrence(src, e0))
					return false;
				if (!checkEventOcurrence(trg, e1))
					return false;
				if (p.isIsStrict() != strict)
					return false;
				return true;
			}
			return false;

		case packet:
			if (oc instanceof Packet) {
				Packet p = (Packet) oc;
				for (EventOccurrence eo : p.getOccurrences()) {
					if (!checkEventOcurrence(eo, clock[0]))
						return false;

				}
				return true;
			}

			// default:
			// break;
		}

		return false;
	}

	String uid = null;

	public String getID() {
		if (uid == null) {
			StringBuilder sb = new StringBuilder("");
			switch (type) {
			case coincidence:
				sb.append("C");
				break;
			case precedence:
				sb.append("P");
				if (strict)
					sb.append("s");
				else
					sb.append("W");
				break;
			case packet:
				sb.append("G");
				break;
			default:
				// Never run;
			}
			sb.append("[");
			sb.append(AdapterRegistry.getAdapter(getCcslConstraint()).getUID(getCcslConstraint()));
			sb.append("]");
			String s[] = new String[clock.length];
			for (int i = 0; i < clock.length; i++)
				s[i] = AdapterRegistry.getAdapter(clock[i]).getUID(clock[i]);

			switch (type) {
			case coincidence:
			case precedence:
				sb.append("{[");
				sb.append(s[0]);
				sb.append("],[");
				sb.append(s[1]);
				sb.append("]}");
				break;
			case packet:
				sb.append("{[");
				sb.append(s[0]);
				sb.append("]}");
				break;
			default:
				// never run
			}
			uid = sb.toString();
		}
		return uid;
	}

	public EObject getCcslConstraint() {
		return source.getCcslConstraint();
	}

	public String getBaseInfo() {
		if (source == null)
			return "..";
		String rule = AdapterRegistry.getAdapter(source.getCcslConstraint()).getReferenceName(source.getCcslConstraint());
		if (rule == null)
			rule = "";
		return rule;
	}

	public String getSubInfo() {
		if (clock == null)
			return "..";
		String s[] = new String[clock.length];
		for (int i = 0; i < clock.length; i++)
			s[i] = AdapterRegistry.getAdapter(clock[i]).getReferenceName(clock[i]);
		switch (type) {
		case coincidence:
			return "Coincidence :" + s[0] + " and " + s[1] + " ;";
		case precedence:
			String tmp = (strict) ? " < " : " <= ";
			return "Precedence :" + s[0] + tmp + s[1] + " ;";
		case packet:
			return "Packet : " + s[0] + " ;";

		}
		return "";
	}

	@Override
	public String toString() {
		if (source.getCcslConstraint()!=null)
			return getBaseInfo() + "=>" + getSubInfo();
		return getSubInfo();
	}

	@Override
	public int hashCode() {
		getID();
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(clock);
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + (strict ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		getID();
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Description other = (Description) obj;
		other.getID();
		if (!Arrays.equals(clock, other.clock))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (strict != other.strict)
			return false;
		if (type != other.type)
			return false;
		if (uid == null) {
			if (other.uid != null)
				return false;
		} else if (!uid.equals(other.uid))
			return false;
		return true;
	}

}
