/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelFactory;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Precedence;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.Reference;

/**
 * 
 * <br>
 * @author bferrero <br>
 * <br>
 * abstract class for compute CCSLRelationModel.Precedence; <br>
 * <br>
 * if (strict ) <br>
 * compute target ; compute source <br>
 * else <br>
 * compute source ; compute target <br>
 * <br>
 * with <br>
 * compute source <br>
 * if (source tick && source is valid) { <br>
 * add source in pool <br>
 * <br>
 * <br>
 * compute target <br>
 * if (target tick && target is valid) { <br>
 * extract source in pool <br>
 * create Precedence bewteen source ansd target <br>
 * <br>
 */

public abstract class AbstractCreatorPrecedes extends AbstractCreator {

	private boolean strict = true;

	final protected ModelElementReference source;
	final protected ModelElementReference target;

	/**
	 * 
	 * @param cref
	 *            (CCSLConstraintRef)
	 * @param source
	 *            (ModelElementReference)
	 * @param target
	 *            (ModelElementReference) strict=true;
	 */
	public AbstractCreatorPrecedes(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target) {
		super(cref);
		this.source = source;
		this.target = target;

	}

	/**
	 * 
	 *            (CCSLConstraintRef)
	 * @param source
	 *            (ModelElementReference)
	 * @param target
	 *            (ModelElementReference)
	 * @param strict
	 */
	public AbstractCreatorPrecedes(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target,
			boolean strict) {
		super(cref);
		this.source = source;
		this.target = target;
		this.strict = strict;

	}

	final public Precedence createRelationPrecedence(EventOccurrence first, EventOccurrence second, boolean strictly) {
		Precedence relationPrecedence = CCSLRelationModelFactory.eINSTANCE.createPrecedence();
		relationPrecedence.setIsStrict(strictly);
		relationPrecedence.setSource(first);
		relationPrecedence.setTarget(second);
		return relationPrecedence;
	}

	public boolean addChecking() {
		return (source != target) && (source != null) && (target != null);
		// &&(checkClock(source)) && (checkClock(target));
	}

	public synchronized final ModelElementReference getSource() {
		return source;
	}

	public synchronized final ModelElementReference getTarget() {
		return target;
	}

	public synchronized final boolean isStrict() {
		return strict;
	}

	/**
	 * Extract source's EventOccurrence and target's EventOccurrence
	 */
	@Override
	public void resolve() throws ResolveClockConstraintException {
//		Reference tmp = this.getCcslConstraintRef().getCcslConstraint();
//		if (tmp instanceof ModelElementReference){
//			EList<EObject> path = ((ModelElementReference) tmp).getElementRef();
//			for(int i = path.size()-1; i >= 0; i--){
//				EObject eop = path.get(i);
//				if (eop.eClass().getName().compareTo("Block") == 0){
//					try {
//						this.
//						if (((Boolean)(eop.getClass().getDeclaredField("isActive")).get(eop)) == false){
//							return;
//						}
//					} catch (IllegalArgumentException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (IllegalAccessException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (NoSuchFieldException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (SecurityException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			}
//		}
		EventOccurrence esource = getEventOccurrenceOnStepTrace(source);
		EventOccurrence etarget = getEventOccurrenceOnStepTrace(target);
		resolve(esource, etarget);
	}

	
	
	
	
	protected void workTarget(EventOccurrence etarget) throws ResolveClockConstraintException {
		boolean btarget = clockTick(etarget); 
		if (btarget && targetvalid()) {
			EventOccurrence msource = poolsource(); // extract source
			if (msource != null) {
				addRelation(createRelationPrecedence(msource, etarget, strict));
			} else {
				// target non precedes par une source => normal ?
				//notifyTargetWithoutSource();
			}
		}
	}
	
	protected void workSource(EventOccurrence esource) {
	
		boolean bsource = clockTick(esource); // source tick ?
		// source
		if (bsource && sourcevalid()) {
			addsource(esource); // register source
		}
		
	}

	/**
	 * 
	 * @param esource
	 * @param etarget
	 * @throws ResolveClockConstraintException
	 * 
	 * 
	 */
	protected void resolve(EventOccurrence esource, EventOccurrence etarget) throws ResolveClockConstraintException {
		
	
		if (strict) {
			// target	
			workTarget(etarget);
			workSource(esource);
		} else {
			// source			
			workSource(esource);
			workTarget(etarget);

		}

	}

	

	abstract protected void notifyTargetWithoutSource() throws ResolveClockConstraintException;

	// recupere la source
	abstract protected EventOccurrence poolsource();

	// memorise la source
	abstract protected void addsource(EventOccurrence esource);

	// * prise en compte de la source
	protected boolean sourcevalid() {
		return true;
	}

	// * prise en compte de la source
	protected boolean targetvalid() {

		return true;
	}

	@Override
	public Description createDescription() {
		return Description.createDescriptionPrecede(source, target, strict, getCcslConstraintRef());
	}

}
