/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.extensionpoint;

import java.util.ArrayList;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.instantrelation.generator.InstantRelationModelGenerator;
import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;

public class LookForExtensions implements IExtensionManager {
	private static LookForExtensions _default;

	private LookForExtensions() {
		ExtensionPointManager.findAllExtensions(this);
	}

	private static final String extensionPointID = "fr.inria.aoste.timesquare.instantrelation.ccslmodels";
	@Override
	final public String getExtensionPointName() {
		return extensionPointID;
	}
	
	@Override
	final public String getPluginName() {
		return "fr.inria.aoste.timesquare.instantrelation";
	}

	public synchronized static final LookForExtensions getDefault() {
		if (_default == null) {
			_default = new LookForExtensions();
		}
		return _default;
	}

	private ArrayList<IRelationModelStructureGenerator> lst = new ArrayList<IRelationModelStructureGenerator>();

	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable {
		Class<? extends IRelationModelStructureGenerator> tmp = ExtensionPointManager.getPointExtensionClass(ice, "InitCCSLModel", IRelationModelStructureGenerator.class);
		lst.add(tmp.newInstance());
	}

	public boolean getICCSLModel(EObject eo,InstantRelationModelGenerator relationModelSolver )
	{
		for(IRelationModelStructureGenerator iccslmf: lst)
		{
			if( iccslmf.acceptModel(eo))
			{
				boolean icc = iccslmf.createRelationModelStructure(eo,relationModelSolver);
				if (icc)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public IRelationModelStructureGenerator getRelationModelStructureGenerator(EObject eo,InstantRelationModelGenerator relationModelSolver )
	{
		for(IRelationModelStructureGenerator iccslmf: lst)
		{
			if( iccslmf.acceptModel(eo))
			{
				boolean icc = iccslmf.createRelationModelStructure(eo,relationModelSolver);
				if (icc)
				{
					return iccslmf;
				}
			}
		}
		return null;
	}

	public IRelationModelStructureGenerator[] looksForCCSLModel() {
		IConfigurationElement[] contributions = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(extensionPointID);
		IRelationModelStructureGenerator[] model = new IRelationModelStructureGenerator[contributions.length];
		for (int i = 0; i < contributions.length; i++) {
			try {
				model[i] = (IRelationModelStructureGenerator) contributions[i]
						.createExecutableExtension("IRelationModelStructureGenerator");

			} catch (Throwable e) {
				e.printStackTrace();
			}

		}
		return model;
	}
}
