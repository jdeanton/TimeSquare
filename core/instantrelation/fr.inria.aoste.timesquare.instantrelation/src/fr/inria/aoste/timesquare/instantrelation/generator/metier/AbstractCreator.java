/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.timesquare.instantrelation.generator.metier.activation.ActivationCreator;
import fr.inria.aoste.timesquare.instantrelation.listener.IRelationModelListener;
import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;

public abstract class AbstractCreator {

	protected CCSLConstraintRef ccslConstraintRef = null;
	protected IRelationModelListener irml = null;

	public AbstractCreator(CCSLConstraintRef cref) {
		super();
		if (cref == null)
			throw new NullPointerException("CCSLConstraintRef is null");
		ccslConstraintRef = cref;
	}

	final public boolean clockTick(EventOccurrence cs) {
		if (cs != null) {
			return cs.getFState() == FiredStateKind.TICK;
		}
		return false;
	}

	public boolean addChecking() {
		return true;
	}

	public void finish()throws ResolveClockConstraintException {

	}

	public final CCSLConstraintRef getCcslConstraintRef() {
		return ccslConstraintRef;
	}

	protected ActivationCreator activationCreator = new ActivationCreator();

	public final ActivationCreator getActivationCreator() {
		return activationCreator;
	}

	public final void setActivationCreator(ActivationCreator _activationCreator) {
		this.activationCreator = _activationCreator;
		if (activationCreator == null) {
			activationCreator = new ActivationCreator();
		}
	}

	public final EventOccurrence getEventOccurrenceOnStepTrace(final ModelElementReference clk) {
		return HelperFactory.getEventOccurence(currentstep, clk);
	}

	public final IRelationModelListener getIrml() {
		return irml;
	}

	LogicalStep currentstep = null;

	final public void resolve(LogicalStep stepTrace) throws ResolveClockConstraintException {
		currentstep = stepTrace;
		activationCreator.updateStep(stepTrace);
		if (activationCreator.mustInit()) {
			init();
		}
		if (activationCreator.isActive()) {
			resolve();
		}
		if (activationCreator.mustFinish()) {
			finish();
		}

		currentstep = null;
	}

	protected abstract void init();

	abstract protected void resolve() throws ResolveClockConstraintException;

	public final void setConstraint(ModelElementReference newSource) {
		if (ccslConstraintRef != null)
			ccslConstraintRef.setCcslConstraint(newSource);
	}

	public final void setIrml(IRelationModelListener irml) {
		this.irml = irml;
	}

	public String toString() {
		if (ccslConstraintRef.getCcslConstraint() != null) {
			EObject eo = ccslConstraintRef.getCcslConstraint();
			return eo.toString();
		}
		return super.toString();
	}

	final protected void addRelation(OccurrenceRelation r) {
		if (r != null) {
			if (getDescription().checkOccurrenceRelation(r)) {
				ccslConstraintRef.getRelatedOccurrenceRelations().add(r);
				if (irml != null)
					irml.aNewRelation(r);
			} else {
				System.err.println("*************************************************************");
			}
		} else {

		}
	}

	Description d = null;

	final public synchronized Description getDescription() {
		if (d == null) {
			d = createDescription();
		}
		return d;
	}

	abstract protected Description createDescription();

	/**
	 * 
	 * @param clock
	 * @return true if clock was born
	 */
	public boolean clockwasBorn(ModelElementReference clock) {

		return clockwasBorn(getEventOccurrenceOnStepTrace(clock));
	}

	/**
	 * 
	 * @param cs
	 * @return value of .... attribut
	 */
	public boolean clockwasBorn(EventOccurrence cs) {
		if (cs != null) {
			return cs.isWasBorn();
		}
		return false;
	}

} // AbstractCreator
