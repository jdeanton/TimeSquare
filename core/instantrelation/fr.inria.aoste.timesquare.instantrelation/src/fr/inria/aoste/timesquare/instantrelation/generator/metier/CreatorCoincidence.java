/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelFactory;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Coincidence;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * 
 * <BR>
 * Compute Coincidence between a <i>left</i> and <i>right</i> <BR>
 * &nbsp;&nbsp;&nbsp;when the left and right tick in the same instant <BR>
 * ( and the
 * <code>public boolean filter(EventOccurrence left,EventOccurrence rigth	)</code>
 * return true <BR>
 * * <BR>
 * (in CreatorCoincidence (return true always )) <BR>
 * <BR>
 */
public class CreatorCoincidence extends AbstractCreator {

	final protected ModelElementReference left;

	final protected ModelElementReference right;

	public CreatorCoincidence(CCSLConstraintRef cref, ModelElementReference left, ModelElementReference right) {
		super(cref);
		this.left = left;
		this.right = right;
		// check();
	}

	public boolean addChecking() {
		return (left != right) && (left != null) && (right != null);// &&(checkClock(left))
																	// &&
																	// (checkClock(right));
	}

	final public Coincidence createRelationCoincidence(EventOccurrence a, EventOccurrence b) {
		if (a == b)
			return null;
		Coincidence relationCoincidence = CCSLRelationModelFactory.eINSTANCE.createCoincidence();
		relationCoincidence.getCoincidentOccurrences().add(a);
		relationCoincidence.getCoincidentOccurrences().add(b);
		return relationCoincidence;
	}

	public ModelElementReference getLeft() {
		return left;
	}

	public ModelElementReference getRight() {
		return right;
	}

	public void resolve() throws ResolveClockConstraintException {
		try {

			EventOccurrence l = getEventOccurrenceOnStepTrace(getLeft());
			EventOccurrence r = getEventOccurrenceOnStepTrace(getRight());

			boolean bl = clockTick(l);
			boolean br = clockTick(r);

			if (bl && br) {
				if (filter(l, r))
					addRelation(createRelationCoincidence(l, r));
			}

		} catch (Throwable e) {
			throw new ResolveClockConstraintException(e, this);
		}

	}

	public boolean filter(EventOccurrence left, EventOccurrence rigth) {
		return true;
	}

	@Override
	public Description createDescription() {
		return Description.createDescriptionCoincidence(left, right, getCcslConstraintRef());
	}

	@Override
	protected void init() {

	}

} // CreatorCoincidence
