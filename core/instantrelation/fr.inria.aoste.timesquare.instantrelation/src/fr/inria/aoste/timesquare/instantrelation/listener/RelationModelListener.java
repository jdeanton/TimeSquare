/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.listener;

import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.trace.relation.IOutputTraceList;

public class RelationModelListener implements IRelationModelListener {
	// private static IRelationModelListener relationModelListener = null;
	private IOutputTraceList iOutputTraceList;

	public RelationModelListener() {
		System.out.println();
		System.out.println("Create RelationModelListener " + this);
		
	}

	public ArrayList<EObject> lrelation = new ArrayList<EObject>(); // Relation
	ArrayList<EObject> lconstraint = new ArrayList<EObject>(); // ClockConstraint

	public void clear() {
		lrelation.clear();
	}

	public void postNewRelation() {
		if (iOutputTraceList != null) {
			iOutputTraceList.aNewRelation(Collections.unmodifiableList(lrelation));
		}
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.umlrelationmodel.listener.IRelationModelListener#setIOutputTraceList(fr.inria.aoste.ccsl.treetrace.relation.IOutputTraceList)
	 */
	public void setIOutputTraceList(IOutputTraceList IOutputTraceList) {
		this.iOutputTraceList = IOutputTraceList;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.umlrelationmodel.listener.IRelationModelListener#aNewRelation(fr.inria.aoste.umlrelationmodel.CCSLRelationModel.Relation)
	 */
	public void aNewRelation(OccurrenceRelation relation) {
		if ((relation != null) && (lrelation.indexOf(relation)==-1))
			lrelation.add(relation);
		if (iOutputTraceList != null) {
			// iOutputTraceList.aNewRelation(relation);
		}
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.umlrelationmodel.listener.IRelationModelListener#aNewClockConstraint(fr.inria.aoste.umlrelationmodel.CCSLRelationModel.ClockConstraint)
	 */
	public void aNewClockConstraint(CCSLConstraintRef clockConstraint) {
		if ((clockConstraint != null) && (lconstraint.indexOf(clockConstraint)==-1))
			lconstraint.add(clockConstraint);
		if (iOutputTraceList != null) {
			// iOutputTraceList.aNewClockConstraint(clockConstraint);
		}

	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.umlrelationmodel.listener.IRelationModelListener#reInit()
	 */
	public void reInit() {
		// relationModelListener = null;
		iOutputTraceList = null;
	}
}
