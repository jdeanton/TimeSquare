/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

public class CreatorPrecedesTrigger extends AbstractCreatorPrecedes {

	public CreatorPrecedesTrigger(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target,
			boolean strict) {
		super(cref, source, target, strict);

	}

	public CreatorPrecedesTrigger(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target) {
		super(cref, source, target);

	}

	protected EventOccurrence ov = null;
	protected boolean override = false;

	public CreatorPrecedesTrigger(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target,
			boolean strict, boolean override) {
		super(cref, source, target, strict);
		this.override = override;
	}

	@Override
	protected void addsource(EventOccurrence esource) {
		if (ov != null && !override)
			return;
		ov = esource;
	}

	@Override
	protected EventOccurrence poolsource() {
		EventOccurrence cp = ov;
		ov = null;
		return cp;
	}

	@Override
	protected void notifyTargetWithoutSource() throws ResolveClockConstraintException {
		// normal : la cible peut ne pas avoir se predecesseur ( sampledOn ..)
	}

	@Override
	protected void init() {

		ov = null;
	}

	@Override
	public void finish() throws ResolveClockConstraintException {
		workTarget(getEventOccurrenceOnStepTrace(getTarget()));
	}
	
	

}
