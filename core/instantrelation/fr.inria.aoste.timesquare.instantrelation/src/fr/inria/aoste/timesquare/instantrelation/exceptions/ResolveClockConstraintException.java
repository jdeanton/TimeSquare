/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.exceptions;

import java.util.List;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.generator.metier.AbstractCreator;


public class ResolveClockConstraintException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6206761238437190838L;

	transient private CCSLConstraintRef relation;

	int stepnumber = -1;

	public ResolveClockConstraintException(Throwable cause, AbstractCreator relation, int stepnumber) {
		super(cause);
		this.relation = relation.getCcslConstraintRef();
		this.stepnumber = stepnumber;
	}

	public ResolveClockConstraintException(Throwable cause, AbstractCreator relation) {
		super(cause);
		this.relation = relation.getCcslConstraintRef();
	}

	/*@Override
	public void printStackTrace() {
		super.printStackTrace();
	}*/

	@Override
	public String toString() {
		// TODO
		if (stepnumber != -1)
			return relation.toString() + " @ step " + stepnumber;
		return relation.toString();
	}
	
	public static class MultipleException extends Exception {
		
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 3290477400851339607L;
		private final List<ResolveClockConstraintException> fResolveClockConstraintExceptions;

		public MultipleException(List<ResolveClockConstraintException> resolveClockConstraintExceptions) {
			fResolveClockConstraintExceptions= resolveClockConstraintExceptions;
		}

		public List<ResolveClockConstraintException> getResolveClockConstraintExceptions() {
			return fResolveClockConstraintExceptions;
		}

		public static void assertEmpty(List<ResolveClockConstraintException> errors) throws MultipleException, ResolveClockConstraintException {
			if (errors.isEmpty())
				return;
			if (errors.size() == 1)
			{
				throw errors.get(0);
			}
			throw new MultipleException(errors);
		}
	}


	
}
