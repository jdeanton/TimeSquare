/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier.activation;

import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * 
 * @author bferrero
 * 
 */
public class LifeActivationCreator extends ActivationCreator {

	final protected ModelElementReference clock;
	final protected boolean tickofdeath;

	/***
	 * 
	 * @param <i>ModelElementReference</i> clock 
	 * ActivationCreator(clock, true)
	 */
	public LifeActivationCreator(ModelElementReference clock) {
		this(clock, true);
	}

	/**
	 * 
	 * @param <i>ModelElementReference</i> clock
	 * @param <I>boolean</i> tickofdeath
	 */
	public LifeActivationCreator(ModelElementReference clock, boolean tickofdeath) {
		super();
		this.clock = clock;
		this.tickofdeath = tickofdeath;
	}

	@Override
	public boolean evalIsActive() {
		EventOccurrence eventOccurrenceClock = getEventOccurrenceOnStepTrace(clock);
		return ((clockwasBorn(eventOccurrenceClock)) && (!clockisDead(eventOccurrenceClock) || (tickofdeath && clockTick(eventOccurrenceClock))));
	}

	public ModelElementReference getInitiatorClock() {
		return clock;
	}

}
