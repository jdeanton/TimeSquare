/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelFactory;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

public class CreatorPacketGlissant extends AbstractCreator {

	final ModelElementReference clock;
	final ModelElementReference trigger;

	final int size;

	public CreatorPacketGlissant(CCSLConstraintRef cref, ModelElementReference clock, int size,
			ModelElementReference trigger) {
		super(cref);
		this.clock = clock;
		this.size = size;
		this.trigger = trigger;
	}

	public synchronized final ModelElementReference getClock() {
		return clock;
	}

	public synchronized final int getSize() {
		return size;
	}

	boolean flag;

	@Override
	public void resolve() throws ResolveClockConstraintException {
		EventOccurrence l = getEventOccurrenceOnStepTrace(clock);
		EventOccurrence t = getEventOccurrenceOnStepTrace(trigger);
		boolean bt = clockTick(t);
		boolean b1 = clockTick(l);

		if (b1) {
			create();
			add(l);
			remove();
		}
		if (bt) {
			flag = true;
		}
	}

	protected Queue<Packet> queue = new LinkedBlockingDeque<Packet>();

	protected void remove() {
		if (queue.size() != 0) {
			if (queue.element().getOccurrences().size() == size) {
				addRelation(queue.remove());
			}
		}
	}

	protected void add(EventOccurrence l) {
		for (Packet relationPacket : queue)
			relationPacket.getOccurrences().add(l);
	}

	protected void create() {
		if (flag) {
			Packet relationPacket = CCSLRelationModelFactory.eINSTANCE.createPacket();
			queue.add(relationPacket);
			flag = false;
		}
	}

	@Override
	public Description createDescription() {
		return Description.createDescriptionPacket(clock, getCcslConstraintRef());
	}

	@Override
	public void finish() {
		if (queue != null) {
			for (Packet p : queue) {
				if (p.getOccurrences().size() != 0)
					addRelation(p);
			}
			queue.clear();
		}

	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub

	}

}
