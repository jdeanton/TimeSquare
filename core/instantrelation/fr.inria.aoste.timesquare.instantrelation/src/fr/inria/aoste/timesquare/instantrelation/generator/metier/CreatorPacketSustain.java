/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelFactory;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

public class CreatorPacketSustain extends AbstractCreator {

	final ModelElementReference clock;
	final ModelElementReference trigger;
	final ModelElementReference upto;

	public CreatorPacketSustain(CCSLConstraintRef cref, ModelElementReference clock, ModelElementReference trigger,
			ModelElementReference upto) {
		super(cref);
		this.clock = clock;
		this.trigger = trigger;
		this.upto = upto;
	}

	// boolean flag;

	@Override
	public void resolve() throws ResolveClockConstraintException {
		EventOccurrence l = getEventOccurrenceOnStepTrace(clock);
		EventOccurrence t = getEventOccurrenceOnStepTrace(trigger);
		EventOccurrence u = getEventOccurrenceOnStepTrace(upto);
		boolean bt = clockTick(t);
		boolean b1 = clockTick(l);
		boolean bu = clockTick(u);
		if (bt) {
			create();
		}
		if (b1) {
			add(l);
		}
		if (bu) {
			remove();
		}

	}

	protected void remove() {
		addRelation(relationPacket);
		relationPacket = null;
	}

	protected void add(EventOccurrence l) {
		if (relationPacket != null) // true
			relationPacket.getOccurrences().add(l);
	}

	Packet relationPacket = null;

	protected void create() {
		if (relationPacket == null)
			relationPacket = CCSLRelationModelFactory.eINSTANCE.createPacket();
	}

	@Override
	public Description createDescription() {
		return Description.createDescriptionPacket(clock, getCcslConstraintRef());
	}

	@Override
	public void finish() {
		if (relationPacket != null) {
			if (relationPacket.getOccurrences().size() != 0)
				addRelation(relationPacket);
		}
		relationPacket = null;
	}

	@Override
	protected void init() {

	}

}
