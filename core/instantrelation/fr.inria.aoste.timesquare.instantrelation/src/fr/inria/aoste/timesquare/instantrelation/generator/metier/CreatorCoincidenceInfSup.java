/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * 
 * <BR>
 * Compute Coincidence between a <i>left</i> and <i>right</i> <BR>
 * &nbsp;&nbsp;&nbsp;when the left and right tick in the same instant <BR>
 * ( and the
 * <code>public boolean filter(EventOccurrence left,EventOccurrence rigth	)</code>
 * return true <BR>
 * <BR>
 * <BR>
 * (in CreatorCoincidenceInfSup (return true if left and right have a same number of
 * tick )) <BR>
 * <BR>
 */
public class CreatorCoincidenceInfSup extends CreatorCoincidence {

	public CreatorCoincidenceInfSup(CCSLConstraintRef cref, ModelElementReference left, ModelElementReference right) {
		super(cref, left, right);

	}

	@Override
	public boolean filter(EventOccurrence left, EventOccurrence rigth) {
		return (left.getCounter() - deltaleft) == (rigth.getCounter() - deltarigth);
	}

	int deltaleft = 0;
	int deltarigth = 0;

	@Override
	protected void init() {
		super.init();
		deltaleft = getEventOccurrenceOnStepTrace(getLeft()).getCounter();		
		deltarigth = getEventOccurrenceOnStepTrace(getRight()).getCounter();		
		if (clockTick(getEventOccurrenceOnStepTrace(getLeft())))
			{deltaleft--; }
		if (clockTick(getEventOccurrenceOnStepTrace(getRight()))) 
			{deltarigth--; }
	}
}
