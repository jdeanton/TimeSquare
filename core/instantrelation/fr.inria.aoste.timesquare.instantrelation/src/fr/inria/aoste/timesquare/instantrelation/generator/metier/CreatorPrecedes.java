/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * for all i>=0 create a CCSLRelationModel.Precedence (strict ) between
 * (source[i] and target[i] )
 * 
 * @author bferrero
 * 
 */
public class CreatorPrecedes extends AbstractCreatorPrecedes {

	/**
	 * 
	 * @param cref
	 * @param source
	 * @param target
	 * @param strict
	 */
	public CreatorPrecedes(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target,
			boolean strict) {
		super(cref, source, target, strict);
	}

	/**
	 * 
	 * @param cref
	 * @param source
	 * @param target
	 *            strict=true;
	 */
	public CreatorPrecedes(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target) {
		super(cref, source, target);
	}

	protected Queue<EventOccurrence> queue = new LinkedBlockingDeque<EventOccurrence>();

	protected EventOccurrence poolsource() {
		return queue.poll();
	}

	protected void addsource(EventOccurrence esource) {
		queue.add(esource);
	}

	@Override
	protected void notifyTargetWithoutSource() throws ResolveClockConstraintException {

		System.out.println("Error ..." + getDescription().toString());
		throw new ResolveClockConstraintException(new RuntimeException("Precedence :target without source "), this);
	}

	@Override
	protected void init() {
		queue.clear();
	}

	@Override
	public void finish() throws ResolveClockConstraintException {
		queue.clear();
	}
	
	

}
