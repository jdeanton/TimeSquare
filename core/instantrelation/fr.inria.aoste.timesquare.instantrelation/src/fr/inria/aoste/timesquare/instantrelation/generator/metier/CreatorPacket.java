/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLRelationModelFactory;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.Packet;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

public class CreatorPacket extends AbstractCreator {

	/*public CreatorPacket(CCSLConstraintRef cref) {
		super(cref);
	
	}*/

	final ModelElementReference clock;

	Packet relationPacket = null;

	final int size;

	public CreatorPacket(CCSLConstraintRef cref, ModelElementReference clock, int size) {
		super(cref);
		this.clock = clock;
		this.size = size;
	}

	public synchronized final ModelElementReference getClock() {
		return clock;
	}

	public synchronized final int getSize() {
		return size;
	}

	@Override
	public void resolve() throws ResolveClockConstraintException {
		EventOccurrence l = getEventOccurrenceOnStepTrace(clock);
		boolean b1 = clockTick(l);
		if (b1) {
			if (relationPacket == null)
				relationPacket = CCSLRelationModelFactory.eINSTANCE.createPacket();
			relationPacket.getOccurrences().add(l);
			if (relationPacket.getOccurrences().size() == size) {
				addRelation(relationPacket);
				relationPacket = null;
			}
		}

	}

	@Override
	public Description createDescription() {
		return Description.createDescriptionPacket(clock, getCcslConstraintRef());
	}

	@Override
	public void finish() {
		if (relationPacket != null) {
			if (relationPacket.getOccurrences().size() != 0)
				addRelation(relationPacket);
		}
		relationPacket = null;
	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub

	}

}
