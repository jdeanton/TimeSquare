/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * for all i>=0 , create a CCSLRelationModel.Precedence (strict ) between
 * (source[i*sizesource] and target[(i-1)*sizetarget+1] )
 * 
 * @author bferrero
 * 
 */

public class CreatorPrecedesBy extends CreatorPrecedes {

	final int sizesource;
	final int sizetarget;

	public CreatorPrecedesBy(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target,
			int sizesource, int sizetarget) {
		super(cref, source, target);
		this.sizesource = sizesource;
		this.sizetarget = sizetarget;
	}

	public CreatorPrecedesBy(CCSLConstraintRef cref, ModelElementReference source, ModelElementReference target,
			boolean strict, int sizesource, int sizetarget) {
		super(cref, source, target, strict);
		this.sizesource = sizesource;
		this.sizetarget = sizetarget;
	}

	public synchronized final int getSizesource() {
		return sizesource;
	}

	public synchronized final int getSizetarget() {
		return sizetarget;
	}

	private int s = 0;

	@Override
	protected boolean sourcevalid() {
		s++;
		if (s == sizesource) {
			s = 0;
			return true;
		}
		return false;
	}

	private int t = 0;

	@Override
	protected boolean targetvalid() {
		boolean b = (t == 0);
		t++;
		if (t == sizetarget) {
			t = 0;
			return b;
		}
		return b;

	}

	@Override
	protected void init() {
		super.init();
		s = 0;
		t = 0;
	}

}
