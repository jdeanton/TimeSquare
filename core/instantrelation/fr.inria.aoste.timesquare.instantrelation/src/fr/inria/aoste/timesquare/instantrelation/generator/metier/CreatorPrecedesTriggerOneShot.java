/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.CCSLConstraintRef;
import fr.inria.aoste.timesquare.instantrelation.exceptions.ResolveClockConstraintException;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;

public class CreatorPrecedesTriggerOneShot extends AbstractCreatorPrecedes {

	/**
	 * 
	 * @param cref
	 * @param source
	 *            (ModelElementReference)
	 * @param target
	 *            (ModelElementReference)
	 * @param strict
	 *            (boolean)
	 * 
	 *            creer une precedence entre la premiere target et la derni�re
	 *            source arriv� ( strictement ?)avant cette derniere.
	 */

	public CreatorPrecedesTriggerOneShot(CCSLConstraintRef cref, ModelElementReference source,
			ModelElementReference target, boolean strict) {
		super(cref, source, target, strict);

	}

	public CreatorPrecedesTriggerOneShot(CCSLConstraintRef cref, ModelElementReference source,
			ModelElementReference target, boolean strict, boolean override) {
		super(cref, source, target, strict);
		this.override = override;
	}

	boolean override = true;
	protected EventOccurrence ov = null;

	boolean flag = true;

	@Override
	protected void addsource(EventOccurrence esource) {
		if (flag) {
			if (override || ov == null)
				ov = esource; // enregistre la derniere source arrive � la place
								// de la precedence tant que n'est pas consomme
		}
	}

	@Override
	protected EventOccurrence poolsource() {
		EventOccurrence cp = ov;
		ov = null;
		if (cp != null)
			flag = false; // nous ne voulons qu'une precedence.
		return cp;
	}

	@Override
	protected void notifyTargetWithoutSource() throws ResolveClockConstraintException {
		// normal : la cible peut ne pas avoir se predecesseur ( )
	}

	@Override
	protected void init() {

		flag = false;
		ov = null;
	}
	
	@Override
	public void finish() throws ResolveClockConstraintException {
		//workTarget(getEventOccurrenceOnStepTrace(getTarget()));
	}

}
