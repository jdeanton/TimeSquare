/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.instantrelation.generator.metier.activation;

import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
/***
 * 
 * @author bferrero
 *
 */
public class ActivationCreator {

	public ActivationCreator() {
		super();
	}

	LogicalStep currentstep = null;
	
	
	boolean mustinit=false;
	boolean active=true;
	boolean mustFinish=false;
	
	public void updateStep(LogicalStep stepTrace) {		
		if ( currentstep!=stepTrace)
		{
			currentstep = stepTrace;
			active=evalIsActive();
			mustinit=evalMustInit();
			mustFinish=evalMustFinish();
		}
			
	}


	protected boolean evalIsActive() {
		
		return true;
	}

	
	
	boolean lastActive4init=false;
	
	
	// mustinit=true when isactive 0=> 1
	protected boolean evalMustInit() {		
		if (active && !lastActive4init)
		{
			lastActive4init=true;
			return true;
		}
		lastActive4init=active;
		return false;
	}

	
		
	boolean lastActive4finish=false;
	
	// mustfinish=true when isactive 0=> 1
	protected boolean evalMustFinish() {		
		if (!active && lastActive4finish)
		{
		//	lastActive4finish=false;
			return true;
		}
		if ( active )		
			lastActive4finish=active;
		return false;
	}
	

	public final boolean  mustInit() {
		return mustinit;
	}


	public final boolean isActive() {
		return active;
	}



	public final boolean mustFinish() {
	
		return mustFinish;
	}


	/**
	 * 
	 * @param clock
	 * @return true if clock tick 
	 */
	 public boolean clockTick(ModelElementReference clock) {		
		return clockTick(getEventOccurrenceOnStepTrace(clock));
	}
	
	/**
	 * 
	 * @param clock
	 * @return true if clock is dead 
	 */
	 public boolean clockisDead(ModelElementReference clock) {
		return clockisDead(getEventOccurrenceOnStepTrace(clock));
	}
	
	/**
	 * 
	 * @param clock
	 * @return true if clock was born 
	 */
	 public boolean clockwasBorn(ModelElementReference clock) {
		
		return clockwasBorn(getEventOccurrenceOnStepTrace(clock));
	}
	
	
	
	/**
	 * 
	 * @param cs 
	 * @return true value of fstate attribut is firedStateKind.TICK 
	 */

	 public boolean clockTick(EventOccurrence cs) {
		if (cs != null) {
			return cs.getFState() == FiredStateKind.TICK;
		}
		return false;
	}
	
	/**
	 * 
	 * @param cs 
	 * @return value of isclockdead attribut
	 */
	 public boolean clockisDead(EventOccurrence cs) {
		if (cs != null) {
			return cs.isIsClockDead();
		}
		return true;
	}
	
	/**
	 * 
	 * @param cs
	 * @return value of .... attribut
	 */
	 public boolean clockwasBorn(EventOccurrence cs) {
		if (cs != null) {			
			return  cs.isWasBorn();
		}
		return false;
	}
	

	 /*
	  * Returns the EventOccurrence object in the current trace step (the one given as argument to
	  * updateStep()) that has the ModelElementReference clk as its ReferedElement. Implicit assumption
	  * is that the ModelElementReference clk is the one of a Clock.
	  * TODO
	  * In the future, if Relations become also live and dead, this code should be adapted.
	  */
	public final EventOccurrence getEventOccurrenceOnStepTrace(final ModelElementReference clk) {
		return HelperFactory.getEventOccurence(currentstep, clk);
	}

}
