/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.extensionpoint;

import fr.inria.aoste.timesquare.launcher.debug.model.output.OutputSetupOption;

public abstract class OutputControler {
	private String key;
	private String name;

	protected OutputControler(String name, String key) {
		this.key = key;
		this.name = name;
	}
	final public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	final public String getName() {
		return name;
	}
	
	public abstract IOutputTrace newOutputtrace() ;
	public abstract IOutputOption newOption();
	public abstract OutputSetupOption newUi() ;
			
	abstract public boolean isCompatibleTo(Class<?> c) ;
}
