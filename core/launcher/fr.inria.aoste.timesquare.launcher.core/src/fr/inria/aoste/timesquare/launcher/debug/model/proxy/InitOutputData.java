/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.proxy;

import java.util.List;

import org.eclipse.core.runtime.IPath;

import fr.inria.aoste.trace.PhysicalBase;

public class InitOutputData {

	
	/***
	 * 
	 * @param folder   : nom du folder de sortie
	 * @param namefile : nom de base du fichier de sortie
	 * @param runtimein : indique si c 'est une simulation
	 * @param lst : liste des clocks discretise
	 */
	
	public InitOutputData(IPath folder, String namefile, boolean runtimein, List<PhysicalBase> lst) {
		super();
		this.folder = folder;
		this.namefile = namefile;
		this.runtimein = runtimein;
		this.lst = lst;
	}
	final IPath folder;
	final String namefile;	
	final boolean runtimein;
	final List<PhysicalBase> lst;
	
	
	public final IPath getFolder() {
		return folder;
	}
	public final String getNamefile() {
		return namefile;
	}
	public final boolean isRuntimein() {
		return runtimein;
	}
	public final List<PhysicalBase> getLst() {
		return lst;
	}
	
	
	
	
}
