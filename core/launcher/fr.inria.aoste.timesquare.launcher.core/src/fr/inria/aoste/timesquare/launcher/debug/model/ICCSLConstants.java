/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyManager;

public interface ICCSLConstants {
	final String POLICY = "SimulationPolicy";
	final String STEPS = "SimulationSteps";
	final String SOURCE = "Source";
	final String ESTATE = "EState";
	final String PRIORITYSOURCE = "PrioritySource";

	final int DEFAULT_STEPS = 0;
	final String DEFAULT_POLICY = SimulationPolicyManager.getDefault().getDefaultUID(); // "random";

	final boolean DEFAULT_ESTATE = false;
	final String ID_DEBUG_MODEL = "fr.inria.aoste.timesquare.launcher.debug.model";

	final String SELECTEDTABLE = "SelectConstraint";
	final String OPTIONMODEL = "Model.option";

	final String TIME="TimeDisplay" ;
	final boolean DEFAULT_TIME = false;//
	final String CPT = "copysource";
	
}
