/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.output;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.widgets.Display;

import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.InitOutputData;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputTrace;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.relation.IOutputTraceList;

public class OutputTraceList implements IOutputTraceList {
	private ArrayList<IOutputTrace> listoutput = new ArrayList<IOutputTrace>();

	private ConsoleSimulation cs = null;
	private static ISolverForBackend _solver = null;
	public OutputTraceList(ISolverForBackend commonSolver) {
		_solver = commonSolver;
	}

	public synchronized final void setCs(ConsoleSimulation cs) {
		this.cs = cs; 
		cs.printSimulationMessageln("Output....");
	}

	public synchronized void add(IOutputTrace iot) {
		listoutput.add(iot);
	}

	public synchronized void aFinalStep(int step) {
		for (IOutputTrace iot : listoutput) {
			try {
				iot.aFinalStep(step);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	synchronized public void aNewStep(LogicalStep step, boolean timed) {
		Thread[] threadarray = new Thread[listoutput.size()];
		int i = 0;
		for (IOutputTrace iot : listoutput) {
			threadarray[i] = new ThreadNewStep(iot, step, cs, timed, relation);
			threadarray[i].start();
			i++;
		}
		for (Thread t : threadarray) {
			try {
				if (t != null)
					t.join();

			} catch (Throwable e) {

				e.printStackTrace();
			}
		}
		for (i = 0; i < threadarray.length; i++)
			threadarray[i] = null;

		Thread.yield();
	}

	synchronized public void aStep(LogicalStep step, boolean timed) {
		Thread[] threadarray = new Thread[listoutput.size()];
		int i = 0;
		for (IOutputTrace iot : listoutput) {
			threadarray[i] = new ThreadStep(iot, step, timed);
			// Display.getDefault().asyncExec(threadarray[i]);
			threadarray[i].start();

			i++;
		}
		Thread.yield();
		for (Thread t : threadarray) {
			try {
				if (t != null)
					t.join();
				t = null;
			} catch (Throwable e) {

				e.printStackTrace();
			}
		}
		for (i = 0; i < threadarray.length; i++)
			threadarray[i] = null;
	}

	public synchronized void killTimer() {
		for (IOutputTrace iot : listoutput) {
			iot.killTimer();
		}
	}

	public synchronized void init(InitOutputData iod) {
		for (IOutputTrace iot : listoutput) {
			System.out.println("Init " + iot.getName());
			RunnableInit rs = new RunnableInit(iot, iod);
			Display.getDefault().syncExec(rs);
			rs.getThrowable();
		}
		Thread.yield();
	}

	public synchronized  void clearRelation() {
		relation = Collections.emptyList() ;
	}

	private List<EObject> relation = null;

	@Override
	public synchronized void aNewRelation(List<EObject> _relation) {
		relation = _relation;
	}

	private static class RunnableInit implements Runnable {
		private IOutputTrace iot;
		private InitOutputData iod=null;
		private Throwable t=null;
		private RunnableInit(IOutputTrace iotin,InitOutputData iodin) {
			iot = iotin;
			iod=iodin;
		}

		@Override
		public void run() {
			try {
				iot.initAndOutputFile(iod, _solver);
			} catch (Throwable e) {
				t = e;
				e.printStackTrace();
			}
		}

		public final Throwable getThrowable() {
			return t;
		}

	}

	private static class ThreadNewStep extends Thread {
		private IOutputTrace iot;
		private LogicalStep aStep;
		private boolean b;
		private ConsoleSimulation cs = null;

		private List<EObject> relation;

		private ThreadNewStep(IOutputTrace iotin, LogicalStep step, ConsoleSimulation csin, boolean bin, List<EObject> relation) {
			super("Thread New Step Output " + iotin.getName());
			iot = iotin;
			aStep = step;
			b = bin;
			cs = csin;
			this.relation = relation;
		}

		@Override
		public void run() {
			long time1 = System.currentTimeMillis();
			iot.aNewStep_And_Relation(aStep, b, relation);
			long time2 = System.currentTimeMillis();
			if (cs != null)
				cs.printSimulationMessageln("Timing Output (" + iot.getName() + ") :" + (time2 - time1) + " ms ");
		}

	}

	private static class ThreadStep extends Thread {
		private IOutputTrace iot;
		private LogicalStep aStep;
		private boolean b;

		private ThreadStep(IOutputTrace iotin, LogicalStep step, boolean bin) {
			super("Thread Step Output " + iotin.getName());
			iot = iotin;
			aStep = step;
			b = bin;
		}

		@Override
		public void run() {
			iot.aStep(aStep, b);
		}

	}
}
