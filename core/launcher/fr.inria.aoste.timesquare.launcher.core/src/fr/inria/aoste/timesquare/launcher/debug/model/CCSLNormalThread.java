/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugException;

final public class CCSLNormalThread implements Runnable, ThreadInterface {
	private int exitValue = 1;

	private ISimulationInterface sim;

	private CCSLProcess process;

	public CCSLNormalThread(ISimulationInterface sim,CCSLProcess process) {
		assert(sim != null);
		this.sim = sim;
		this.process = process;
	}

	public ISimulationInterface getSim()
	{
		return sim;
	}

	
	public int getExitValue() {
		return exitValue;
	}

	public boolean isTerminated() {
		return sim.isTerminated();
	}

	// Runnable
	public void run() {
		try {
			exitValue = 0;
			this.sim.init();
			while (!sim.isTerminated()) {
				this.sim.step(true);
				Thread.yield();
			}
			this.sim.finish();			
		} catch (Throwable ce) {
			System.err.println("CCSL Thread abnormal ending " + ce);
			ce.printStackTrace(System.err);
			exitValue = -1;
		}
		try {
			process.disconnect();
		} catch (DebugException e) {
			
			e.printStackTrace();
		}
	}

	public void terminate() throws DebugException {
		sim.finish();
		IWorkspaceRoot iw = ResourcesPlugin.getWorkspace().getRoot();
		try {
			iw.refreshLocal(IResource.DEPTH_INFINITE, null);
			if (iw.getProject() != null) {
				iw.getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
			}
		} catch (CoreException e) {
			
		}
		process.disconnect();
	}
}
