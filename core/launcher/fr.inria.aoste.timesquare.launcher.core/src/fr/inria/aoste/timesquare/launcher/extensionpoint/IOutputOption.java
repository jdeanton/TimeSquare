/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.extensionpoint;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.resource.Resource;

import fr.inria.aoste.timesquare.launcher.core.PropertySimulation;
import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.core.inter.ICCSLProxy;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.Configurator;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.relation.IDescription;

public abstract class IOutputOption {

	public static final Class<IOutputOption> CLASS = IOutputOption.class;

	protected IOutputOption(String key, String name) {
		this.key = key;
		this.name = name;
	}

	private String name;
	private String key;


	public String getName() {
		return name;
	}

	ConsoleSimulation cs = null;

	public final ConsoleSimulation getCs() {
		return cs;
	}

	public final void setCs(ConsoleSimulation cs) {
		this.cs = cs;
	}

	public final String getKey() {
		return key;
	}

	private Map<String, String> table = new HashMap<String, String>();

	public final static String IOutputOption_ACTIVE = "IOO.active";

	public boolean activebydefault = false;

	private ICCSLProxy ccslhelper = null;

	public final Map<String, String> getTable() {
		return table;
	}

	public final void setTable(Map<String, String> table) {
		this.table = table;
	}

	public final boolean isActive() {
		String s = table.get(IOutputOption_ACTIVE);
		if (s != null) {
			return Boolean.parseBoolean(s);
		}
		return activebydefault;
	}

	final public void setActive(boolean b) {
		try {
			table.put(IOutputOption_ACTIVE, Boolean.toString(b));
		} catch (Throwable e) {

		}
	}

	final public void setFlag(String flag, String value) {
		if (value != null)
			table.put(flag, value);
		else
			table.remove(flag);
	}

	final public String getFlag(String flag) {
		return table.get(flag);
	}

	public void setDefault() {

	}

	public final Set<String> getTableKey() {
		if (table == null)
			return null;
		return table.keySet();
	}

	public final void setCcslhelper(ICCSLProxy ccslSimulationConfigHelper) {
		this.ccslhelper = ccslSimulationConfigHelper;
	}

	public CCSLInfo getCcslhelper() {
		return ccslhelper.getDelegateCCSLInfo();
	}

	public final List<Resource> getRessourceOfReferencedObject() {
		return ccslhelper.getRessourceOfReferencedObject();
	}

	protected final IFile getModel() {
		return ccslhelper.get_SourceIFile();
	}

	public final List<ModelElementReference> getListofClock() {
		return ccslhelper.getListofClock();
	}

	public final List<ModelElementReference> getListofAssert() {
		return ccslhelper.getListofAssert();
	}

	public final List<IDescription> getListofRelation() {
		return ccslhelper.getListofRelation();
	}

	ReportMessage activableMessage = new ReportMessage();

	public String getActivableMessage() {
		return activableMessage.getMessage();
	}

	public final boolean isActivable() {
		try {
			activableMessage.clear();
			if (!ccslhelper.haveSolver() ) {
				activableMessage.setMessage("==> No Valid Model in input");
				return false;
			}
			if (ccslhelper.getEstate() == false) {
				if (!PropertySimulation.EnableInactive.iscontaint(getSupportPropertySimulation())) {
					activableMessage.setMessage("==> EState Requis");
					return false;
				}
			}
			return _isActivable(activableMessage);
		} catch (Throwable e) {
			activableMessage.setMessage("Error on isActivable() :" + e.getMessage());
			return false;
		}
	}




	protected boolean _isActivable(ReportMessage rm) {
		return true;
	}

	public String validate() {
		return null;
	}

	public final IFile getIFile() {
		return ccslhelper.get_SourceIFile();
	}

	public final void write() {
		ccslhelper.writeTable(this);
	}

	public final Set<String> getKeyTable() {
		return table.keySet();
	}

	/*****************************/

	public PropertySimulation[] getSupportPropertySimulation() {
		return new PropertySimulation[] {};// PropertySimulation.EnableInactive
		// };
	}

	public void updateModel() {

	}

	public void restore() {

	}

	public abstract Configurator getConfigurator();
}
