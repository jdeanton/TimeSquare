/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;

import fr.inria.aoste.timesquare.launcher.debug.model.output.OutputSetupOption;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputTrace;
import fr.inria.aoste.timesquare.launcher.extensionpoint.OutputControler;
import fr.inria.aoste.timesquare.launcher.extensionpoint.OutputControlerFactory;
import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;

public class OutputManager implements IExtensionManager {

	private static OutputManager _default = null;

	public synchronized static OutputManager getDefault() {
		if (_default == null)
			_default = new OutputManager();
		return _default;
	}

	private OutputManager() {
		ExtensionPointManager.findAllExtensions(this);
	}

	@Override
	final public String getExtensionPointName() {
		return "fr.inria.aoste.timesquare.launcher.core.outputformat";
	}
	
	@Override
	final public String getPluginName() {
		return "fr.inria.aoste.timesquare.launcher.core";
	}

	private static class AttributOutput extends OutputControler 
	{
		private Class<? extends IOutputTrace> outputtrace;
		private Class<? extends IOutputOption> option;
		private Class<? extends OutputSetupOption> ui;

		public AttributOutput(IConfigurationElement ice) {
			super(ice.getAttribute("name"), ice.getAttribute("id"));
		}

		public  IOutputTrace newOutputtrace() {
			try {
				return outputtrace.newInstance();
			} catch (Throwable e) {			
				e.printStackTrace();
				return null;
			}
		}
		public IOutputOption newOption() {
			try{
				return option.newInstance();
			} catch (Throwable e) {			
				e.printStackTrace();
				return null;
			}
		}
		public OutputSetupOption newUi() {
			try{
				return ui.newInstance();
			} catch (Throwable e) {			
				e.printStackTrace();
				return null;
			}
		}

		public boolean isCompatibleTo(Class<?> c) {	
			if ( outputtrace.isAssignableFrom(c))
				return true;
			if ( option.isAssignableFrom(c))
				return true;
			if ( ui.isAssignableFrom(c))
				return true;
			return false;
		}
	}

	private  void initExtensionOutputFormat(IConfigurationElement ice) throws Throwable {
		System.out.println("icename : OutputFormat");			

		AttributOutput ao = new AttributOutput(ice);
		ao.outputtrace = ExtensionPointManager.getPointExtensionClass(ice, "outputTrace", IOutputTrace.CLASS);
		ao.outputtrace.newInstance(); // Test newInstance

		ao.ui = ExtensionPointManager.getPointExtensionClass(ice, "optionUI", OutputSetupOption.CLASS);
		ao.ui.newInstance(); // Test newInstance

		ao.option = ExtensionPointManager.getPointExtensionClass(ice, "optionData", IOutputOption.CLASS);
		IOutputOption iot=ao.option.newInstance(); // Test newInstance					
		if ( !PropertySimulation.isOutput( iot.getSupportPropertySimulation()))
			throw new Exception("Output have Input Property");

		listkey.add(ao.getKey());
		hashAttribut.put(ao.getKey(), ao);
		System.out.println("  <OutputManager>: Ok :" + ao.getName() + " " + ao.getKey() + " ");
	}

	private  void initExtensionOutputController(IConfigurationElement ice) throws Throwable {
		System.out.println("icename : OutputController");	
		/* For example, the backend manager registers a class named
		 * fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.BehaviorOutputControlerFactory
		 * with the associated name "behavior"
		 * The class is a factory that will be asked to create  */
		Class<? extends OutputControlerFactory> cocf = ExtensionPointManager.getPointExtensionClass(ice, "controler", OutputControlerFactory.CLASS);

		String name = ice.getAttribute("name");
		if (name==null) { name=""; }
		String keybase=	ExtensionPointManager.getPluginName(ice) +"@"+name;
		OutputControlerFactory ocf=cocf.newInstance();
		OutputControler oct[]= ocf.create(keybase);
		for (OutputControler oc: oct)
		{
			String key=oc.getKey();
			if( key.startsWith(keybase))
			{
				listkey.add(key);
				hashAttribut.put(key,oc	);
			}
		}
	}


	private List<String> listkey = new ArrayList<String>();
	private HashMap<String, OutputControler> hashAttribut = new HashMap<String, OutputControler>();
	
	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable {
		String id=ice.getName();
		if ("OutputController".equals(id))
		{
			/* Timesquare Backend manager is such a controller registered to the extension point
			 * with a "OutputController" element. */
			initExtensionOutputController(ice);
			return ;
		}
		/* The following case seems to be unused at the moment, the extension point schema does not
		 * define the "OutputFormat" element */
		if ("OutputFormat".equals(id))
		{
			initExtensionOutputFormat(ice);
		}
	}

	public List<String> getListkey() {
		return Collections.unmodifiableList(listkey); 		
	}

	/**
	 * 
	 * @param c
	 * @return return lsit of key compatible with class c
	 */
	public List<String> getListkey(Class<?> c ) {
		List<String> lst = new ArrayList<String>();
		for( String s : listkey)
		{
			OutputControler oc= hashAttribut.get(s);
			if (oc.isCompatibleTo(c))
			{
				lst.add(s);
			}
		}
		return lst; 		
	}


	/**
	 * return a unique key   
	 *  
	 */	
	public String getkey(Class<?> c ) {
		List<String> lst =getListkey(c);
		if( lst.size()==1)
			return lst.get(0);
		return null;
	}

	public IOutputTrace getTraceforKey(String s) {
		try {
			OutputControler ao = hashAttribut.get(s);
			if (ao == null)
				return null;
			/*	if (ao.getValid() != null)
				if (!ao.getValid().isActivable("output-trace"))
					return null;*/
			return ao.newOutputtrace();
		} catch (Throwable t) { // TODO .....
			return null;

		}
	}




	public IOutputOption getOptionforKey(String s) {
		try {
			OutputControler ao = hashAttribut.get(s);
			if (ao == null)
				return null;
			/*	if (ao.getValid() != null)
				if (!ao.getValid().isActivable("output-data"))
					return null;*/
			IOutputOption io = ao.newOption();
			return io;
		} catch (Throwable t) {
			return null;

		}
	}

	public OutputSetupOption getUIforKey(String s) {
		try {
			OutputControler ao = hashAttribut.get(s);
			if (ao == null)
				return null;
			/*	if (ao.getValid() != null)
				if (!ao.getValid().isActivable("output-ui"))
					return null;*/
			return ao.newUi();
		} catch (Throwable t) {
			return null;

		}
	}
}
