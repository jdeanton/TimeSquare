/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.inter;

import java.util.List;

import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.NoBooleanSolution;
import fr.inria.aoste.timesquare.ccslkernel.runtime.exceptions.SimulationException;
import fr.inria.aoste.timesquare.ccslkernel.runtime.simulation.CCSLStepExecutionEngine;
import fr.inria.aoste.timesquare.ccslkernel.solver.exception.SolverException;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;

public interface ISolverForBackend {

//what are the useful functions from the backend point of view

//	public List<ModelElementReference> getClockList() ;
//	
//	public List<ModelElementReference> getConstraint() ;
//	
//	public List<ModelElementReference> getAssertList();
	
	public void forceClockPresence(ModelElementReference mer);
	
	public void forceClockAbsence(ModelElementReference mer);

	/**
	 * This method computes the semantics of the step and all the possible solutions for this step. The result is given in a list of LogicalStep. 
	 * Note that these logical steps are partial and the complete logical step, suitable for a trace, will be given only after applying one of these steps
	 * @param stepExecutor (a newly created StepExecutor)
	 * @return a list containing all the possible steps for the next step of the simulation
	 * @throws NoBooleanSolution (there is a deadlock, no solution can be found)
	 * @throws SolverException (shit happened :-/)
	 * @throws SimulationException 
	 */
	public List<LogicalStep> computeAndGetPossibleLogicalSteps() throws NoBooleanSolution, SolverException, SimulationException;
	
	/**
	 * this method is strongly associated to {@link #getPossibleLogicalSteps(CCSLStepExecutionEngine)}. It asks the chosen simulation policy to choose a Logical in the set returned by the {@link #getPossibleLogicalSteps(CCSLStepExecutionEngine)} call.
	 * So it must be called AFTER the already site method and it returns an index representing a specific LogicalStep in the list
	 * @param stepExecutor
	 * @return the chosen logical step
	 */
	public int proposeLogicalStepByIndex();
	
	/**
	 * This method is strongly associated to {@link #getPossibleLogicalSteps(CCSLStepExecutionEngine)}.
	 * It must be called after this method. Its goal is to apply the step which is at the index 'chosenLogicalStep' in the collection returned by {@link #getPossibleLogicalSteps(CCSLStepExecutionEngine)}.
	 * It rewrites the SOS rules and create the logical step resulting from the simulation step. 
	 * @param stepExecutor
	 * @param chosenLogicalStep
	 * @return the logical step resulting from the simulation
	 * @throws SolverException
	 * @throws SimulationException 
	 */
	public LogicalStep applyLogicalStepByIndex(int chosenLogicalStep) throws SolverException, SimulationException;

}