/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import org.eclipse.debug.core.DebugException;

public interface ThreadInterface extends Runnable {

	public abstract boolean isTerminated();

	public abstract void terminate() throws DebugException;

	public abstract int getExitValue();
	
	public ISimulationInterface getSim();

}