/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.Trace;


public interface ISimulationInterface {
	void init() throws Exception;  

	void steprun() throws Exception;

	void step(boolean timed) throws Exception ;

	void step(int nb) throws Exception;

	int getCurrentStepIndice();

	public int getViewStepIndice();

	void finish();
	
	public void disconnect();

	boolean isTerminated();

	String getSourceFile() throws Exception;

	public int viewStep(int n);

	public StateEngine getStateEngine();

	public LogicalStep getStep();

	public LogicalStep getStepview();

	public void stopstep();
	
	public Trace getTrace();

}
