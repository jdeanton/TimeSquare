/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.output;

public class ReportMessage {

	public ReportMessage() {
		super();
		clear();
	}	

	public final String getMessage() {
		return message;
	}

	public final void setMessage(String message) {
		this.message = message;
	}


	String message;

	public void clear() {		
		message = "";
	}

}
