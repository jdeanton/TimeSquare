/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.values;

import org.eclipse.debug.core.DebugException;

import fr.inria.aoste.timesquare.launcher.debug.model.CCSLVariable;
import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget;

final public class CCSLIntVariable extends CCSLVariable {

	public CCSLIntVariable(ICCSLDebugTarget target, String name) {
		super(target, name, "int");
		setValue(new CCSLIntValue(target, 0));
	}

	public boolean verifyValue(String expression) throws DebugException {
		try {
			Integer.parseInt(expression);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	private CCSLIntValue getIntValue() {
		return (CCSLIntValue) super.getValue();
	}

	public void setValue(String expression) {
		super.setValue(expression);
		try
		{
		getIntValue().setValue(Integer.parseInt(expression));
		}
		catch (Exception e) {
			getIntValue().setMessage(expression);
		}
	}

	public void setValue(int v) {
		super.setValue(v + "");
		getIntValue().setValue(v);
	}
}
