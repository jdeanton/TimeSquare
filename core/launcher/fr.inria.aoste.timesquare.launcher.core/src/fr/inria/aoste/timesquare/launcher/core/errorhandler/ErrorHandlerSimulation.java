/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.errorhandler;

import org.eclipse.core.runtime.IStatus;

public class ErrorHandlerSimulation extends ErrorHandler {

	@Override
	public void notifyError(String title, String message, IStatus status) throws Exception {
		
		super.notifyError(title, message, status);
		Throwable t=status.getException();
		if ( t instanceof Exception)
			throw (Exception) t;
		throw new RuntimeException(t);
	}



	
	
}
