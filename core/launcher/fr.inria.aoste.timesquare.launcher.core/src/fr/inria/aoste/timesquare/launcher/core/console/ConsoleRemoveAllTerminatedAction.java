/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.console;

/**  Base : copie on org.eclipse.debug.internal.ui.views.console.ConsoleRemoveAllTerminatedAction */
import java.util.ArrayList;
import java.util.List;

import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.ILaunchesListener2;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IUpdate;

public class ConsoleRemoveAllTerminatedAction extends Action implements IUpdate, ILaunchesListener2 {

	public ConsoleRemoveAllTerminatedAction() {
		super("&Remove All Termminated");
		// super(ConsoleMessages.ConsoleRemoveAllTerminatedAction_0);
		// setId("id2" );
		PlatformUI.getWorkbench().getHelpSystem().setHelp(this, "org.eclipse.debug.ui.console_remove_all_terminated_context");
		// IDebugHelpContextIds.CONSOLE_REMOVE_ALL_TERMINATED);
		setToolTipText("Remove All Termminated Launches");
		setImageDescriptor(DebugUITools.getImageDescriptor("IMG_LCL_REMOVE_ALL")); // IInternalDebugUIConstants
		setDisabledImageDescriptor(DebugUITools.getImageDescriptor("IMG_DLCL_REMOVE_ALL"));
		setHoverImageDescriptor(DebugUITools.getImageDescriptor("IMG_LCL_REMOVE_ALL"));
		DebugPlugin.getDefault().getLaunchManager().addLaunchListener(this);
		update();
	}

	public void dispose() {
		DebugPlugin.getDefault().getLaunchManager().removeLaunchListener(this);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.texteditor.IUpdate#update()
	 */
	public void update() {
		ILaunch[] launches = DebugPlugin.getDefault().getLaunchManager().getLaunches();
		for (int i = 0; i < launches.length; i++) {
			ILaunch launch = launches[i];
			if (launch.isTerminated()) {
				setEnabled(true);
				return;
			}
		}
		setEnabled(false);

	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run() {
		ILaunch[] launches = DebugPlugin.getDefault().getLaunchManager().getLaunches();
		// RemoveAllTerminatedAction.
		removeTerminatedLaunches(launches);
	}

	public static void removeTerminatedLaunches(ILaunch[] elements) {
		List<ILaunch> removed = new ArrayList<ILaunch>();
		for (int i = 0; i < elements.length; i++) {
			ILaunch launch = elements[i];
			if (launch.isTerminated()) {
				removed.add(launch);
			}
		}
		if (!removed.isEmpty()) {
			ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
			manager.removeLaunches(removed.toArray(new ILaunch[removed.size()]));
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.ILaunchesListener#launchesRemoved(org.eclipse.debug.core.ILaunch[])
	 */
	public void launchesRemoved(ILaunch[] launches) {
		if (isEnabled()) {
			update();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.ILaunchesListener#launchesAdded(org.eclipse.debug.core.ILaunch[])
	 */
	public void launchesAdded(ILaunch[] launches) {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.ILaunchesListener#launchesChanged(org.eclipse.debug.core.ILaunch[])
	 */
	public void launchesChanged(ILaunch[] launches) {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.ILaunchesListener2#launchesTerminated(org.eclipse.debug.core.ILaunch[])
	 */
	public void launchesTerminated(ILaunch[] launches) {
		update();
	}
}
