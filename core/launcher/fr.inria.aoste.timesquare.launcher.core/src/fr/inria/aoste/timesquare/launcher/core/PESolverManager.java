/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IConfigurationElement;

import fr.inria.aoste.timesquare.launcher.core.inter.IBuildSolver;
import fr.inria.aoste.timesquare.launcher.core.inter.ICCSLProxy;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolver;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.CCSLSimulationConfigurationHelper;
import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;

/**
 * Manages the extension point <strong>fr.inria.aoste.timesquare.launcher.core.solver</strong> to which CCSL
 * solvers are registered together with the file extension they claim to handle.
 * 
 * This class is a singleton, the instance is accessible using the {@link PESolverManager#getDefault()} static
 * function.
 * 
 * Inside the class, a map is maintained to associate one file extension with a list of instance of
 * {@link ISolver} that are supposed to handle models coming from file using the extension.
 * 
 * @author Benoit Ferrero
 *
 */
public class PESolverManager implements IExtensionManager {
	private static PESolverManager _default = null;

	public synchronized static PESolverManager getDefault() {
		if (_default == null)
			_default = new PESolverManager();
		return _default;
	}

	// SINGLETON
	private PESolverManager() {
		/* The call of this function will in turn call initExtension(IConfigurationElement) for every 
		 * registration to the extension point. It will build the content of the listAttribut field, and
		 * from this list the postinit() function will setup the content of the hashmapSolver field. */
		ExtensionPointManager.findAllExtensions(this);
		postinit();
//		BDDKernel.getDefault(); 
	}
	
	@Override
	final public String getExtensionPointName() {
		return "fr.inria.aoste.timesquare.launcher.core.solver";
	}

	@Override
	final public String getPluginName() {
		return "fr.inria.aoste.timesquare.launcher.core";
	}

	public static class Attribut {
		List<String> listextension=null;
		String name=null;
		IBuildSolver solverbuild=null;
	}

	private HashMap<String, List<IBuildSolver>> hashmapSolver = new HashMap<String, List<IBuildSolver>>();

	/**
	 * Clears the cache of {@link ISolver} objects. The <strong>level</strong> argument is not used.
	 * @param level
	 */
	public void clearCache(int level) {
		table.clear();
		for (Attribut atb : listAttribut) {
			try {
				atb.solverbuild.clearCache(level);
			} catch (Throwable e) {
				System.err.println(e);
			}
		}
	}

	/**
	 * The cache of {@link ISolver} objects indexed with respect to the {@link IFile} containing the resource.
	 */
	HashMap<IFile, Cache> table = new HashMap<IFile, Cache>();

	/**
	 * This class describes a cache entry that associates an {@link ISolver} object to an {@link IFile} and
	 * its last modification time stamp.
	 * 
	 * @author Benoit Ferrero
	 * @see #table
	 */
	public static class Cache {
		long stamp=0;
		IFile file=null;
		ISolver solver;
		public Cache(IFile file) {
			super();
			this.file = file;				
			stamp=file.getModificationStamp();
		}
		public final IFile getFile() {
			return file;
		}

	}

	/**
	 * Cache lookup based on the {@link IFile} found in the {@link ICCSLProxy} object (probably an instance of
	 * {@link CCSLSimulationConfigurationHelper}. If no {@link Cache} object exists, a new one is created and inserted in
	 * the cache itsel (#table).
	 * 
	 * If the {@link IFile} is null in the {@link ICCSLProxy} object, the function returns null.
	 * @param iccslproxy
	 * @return
	 */
	private  synchronized Cache getCache(ICCSLProxy iccslproxy)
	{
		IFile file = iccslproxy.get_SourceIFile();
		if (file == null)
			return null;
		Cache c = table.get(file);
		if ( c == null || c.stamp != file.getModificationStamp())
		{
			// TODO : Potential bug, two identical objects are created !! Why ?? 
			// The returned object is not the one which is stored in the cache map. Next call of getCache()
			// with the same IFile will return a Cache object with an empty isolver field, and a new solver
			// will be created again, and finally stored in the right Cache object.
			c = new Cache(file);
			table.put(file, new Cache(file));
			// Proposed change: table.put(file, c);
		}		
		return c;
	}

	/**
	 * This function returns a solver suitable to perform a simulation according to the given {@link ICCSLProxy}
	 * object given as argument (will probably be an instance of {@link CCSLSimulationConfigurationHelper} as this is
	 * currently the only implementation of this interface).
	 * 
	 * The solver object to be returned is selected according to the file extension of the source file (an
	 * {@link IFile} object) found in the {@link ICCSLProxy} object (using {@link ICCSLProxy#get_SourceIFile()}).
	 * If there is no {@link IFile} object, the function returns null.
	 * 
	 * This function uses the {@link #table} attribute and the {@link #getCache(ICCSLProxy)} function to maintain
	 * a cache of already built {@link ISolver} objects, based on the {@link IFile} object
	 * object found in the {@link ICCSLProxy} instance and the last modification stamp of the IFile.
	 * 
	 * Cache rationale:
	 * As the {@link ISolver} object is supposed to load the model in order to be able to give back information
	 * to the {@link CCSLSimulationConfigurationHelper} object it is strongly tied to the resource file at one moment.
	 *  
	 * @param iccslproxy an instance of the {@link ICCSLProxy} interface.
	 * @return an {@link ISolver} object, or <strong>null</strong> if the function
	 * {@link ICCSLProxy#get_SourceIFile()} returns null on the <strong>iccslproxy</strong> object.
	 */
	public ISolver createSolver(ICCSLProxy iccslproxy) {
		try {
			Cache cache = getCache(iccslproxy);
			if ( cache == null)
				return null;
			if (cache.solver != null)
				return cache.solver;

			IBuildSolver solverBuilder = getSolverBuilder(iccslproxy);
			ISolver isol = solverBuilder.buildSolver(iccslproxy);
			throwable = solverBuilder.getException();
			cache.solver = isol;
			return isol;
		} catch (Throwable e) {
			throwable = e;
			e.printStackTrace();
			return null;
		}
	}

	public PropertySimulation[] getSupportPropertySimulation(ICCSLProxy iccslproxy) {
		throwable = null;
		try {
			IBuildSolver solver = getSolverBuilder(iccslproxy);
			return solver.getSupportPropertySimulation();
		} catch (Error e) {
			throwable = e;
			return new PropertySimulation[]{};
		}
	}

	/**
	 * Returns an instance of {@link IBuildSolver} using the file extension of the source file found in
	 * the {@link ICCSLProxy} object, and the locally maintained map of association between file extension
	 * and {@link IBuildSolver} objects.
	 * 
	 * If several {@link IBuildSolver} are registered for the same file extension, it returns the first one.
	 * 
	 * @param iccslproxy
	 * @return
	 * @throws Error
	 * @see {@link #hashmapSolver}
	 */
	private IBuildSolver getSolverBuilder(ICCSLProxy iccslproxy) throws Error {
		IFile sourceFile = iccslproxy.get_SourceIFile();
		if (sourceFile==null) throw new Error("No Source file in iccslproxy");

		String extension = sourceFile.getFileExtension();
		List<IBuildSolver> ls = hashmapSolver.get(extension);

		if (ls == null)
			throw new Error("No IBuildSolver List registered for extension " + extension);

		if (ls.size() != 0)
			return ls.get(0);

		throw new Error("No IBuildSolver registered for extension " + extension);
	}


	public int getDefaultStep(ICCSLProxy iccslproxy) {
		throwable = null;
		try {
			IBuildSolver solver = getSolverBuilder(iccslproxy);
			return solver.getDefaultValue();
		} catch (Error e) {
			throwable = e;
			return 100;
		}
	}

	/**
	 * Returns true if the file extension given in the argument <em>ext</em> is associated to at least a solver
	 * that claims to handle a file with this extension.
	 * 
	 * @param ext
	 * @return
	 */
	public boolean isExtensionSuppported(String ext) {
		List<IBuildSolver> ls = hashmapSolver.get(ext);
		if (ls == null)
			return false;
		if (ls.size() == 0)
			return false;
		return true;
	}

	public String[] getListExtension() {
		return hashmapSolver.keySet().toArray(new String[hashmapSolver.size()] );
	}

	private Throwable throwable=null;
	public final Throwable getThrowable() {
		return throwable;
	}

	synchronized public boolean validate(ICCSLProxy iccslproxy) {
		try {
			IBuildSolver solver = getSolverBuilder(iccslproxy);
			boolean sel = solver.validateSolver(iccslproxy);
			throwable = solver.getException();
			return sel;
		} catch (Error e) {
			throwable = e;
			return false;
		}
	}

	private List<Attribut> listAttribut = new ArrayList<Attribut>();
	
	/**
	 * This function is called from the {@link ExtensionPointManager#findAllExtensions(IExtensionManager)} to
	 * initialize internal data based on the {@link IConfigurationElement} given in argument ice.
	 * 
	 * Each {@link IConfigurationElement} is converted in a {@link Attribut} instance: the fields of this object
	 * are filled from information found in the attributes of the {@link IConfigurationElement} object. The
	 * {@link #listAttribut} field holds the collected {@link Attribut} objects.
	 */
	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable {
		Attribut at = new Attribut();
		at.name = ice.getAttribute("name");

		Class<? extends IBuildSolver> c = ExtensionPointManager.getPointExtensionClass(ice, "class", IBuildSolver.CLASS);
		at.solverbuild = c.newInstance();

		String ext = ice.getAttribute("extensionfile");
		String st[] = ext.split(",");
		List<String> listextension = new ArrayList<String>();
		for (String s : st) {
			listextension.add(s.trim());
		}
		at.listextension = listextension;
		listAttribut.add(at);

		System.out.println("Read " + at.name + "  " + c.getName() + " <" + ext + ">");
	}

	/**
	 * Builds the {@link #hashmapSolver} map from the data collected at the extension point.
	 */
	private void postinit() {
		// BUILD data
		for (Attribut a : listAttribut) {
			for (String ext : a.listextension) {
				List<IBuildSolver> ls = hashmapSolver.get(ext);
				if (ls == null) {
					ls = new ArrayList<IBuildSolver>();
					hashmapSolver.put(ext, ls);
				}
				ls.add(a.solverbuild);
			}
		}
	}
}
