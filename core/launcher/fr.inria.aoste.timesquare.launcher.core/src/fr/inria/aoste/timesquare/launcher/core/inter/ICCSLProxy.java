/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.inter;


import org.eclipse.core.resources.IFile;

import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.timesquare.launcher.core.errorhandler.ErrorHandler;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;
import fr.inria.aoste.timesquare.simulationpolicy.OptionPolitic;

public interface ICCSLProxy extends CCSLInfo {

	/**** ICCSLConstants.POLICY */

	public abstract int getPolicyId();

	/*** IFile of ICCSLConstants.SOURCE */
	public abstract IFile get_SourceIFile();
	
	/*** IFile of ICCSLConstants.PRIORITYSOURCE */
	public abstract IFile get_PrioritySourceIFile();

	public abstract String getExtension();

	/*** name without ext + optional date of ICCSLConstants.SOURCE */
	public abstract String get_NameFile(boolean timed);

	public abstract ConsoleSimulation getConsole();	

	public abstract OptionPolitic getOptionPolicy();

	public abstract IOutputOption getTable(String key);

	public abstract String getDatestr();	

	public abstract boolean haveSolver();

	public abstract void writeTable(IOutputOption iOutputOption);

	public abstract ErrorHandler getErrorHandler();
	
	public abstract CCSLInfo getDelegateCCSLInfo();
	
	public abstract boolean getTime();
}