/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.proxy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;

public interface ILaunchWrapper {

	public boolean getAttribute(String attributeName, boolean defaultValue) throws CoreException;	
	
	public int getAttribute(String attributeName, int defaultValue) throws CoreException;	
	
	@SuppressWarnings("rawtypes")
	public List getAttribute(String attributeName, List defaultValue) throws CoreException;	
	
	@SuppressWarnings("rawtypes")
	public Set getAttribute(String attributeName, Set defaultValue) throws CoreException;	

	@SuppressWarnings("rawtypes")
	public HashMap getAttribute(String attributeName, HashMap defaultValue) throws CoreException;	
	
	public String getAttribute(String attributeName, String defaultValue) throws CoreException;
	
	public void setAttribute(String attributeName, int value);
	
	public void setAttribute(String attributeName, String value);
	
	@SuppressWarnings("rawtypes")
	public void setAttribute(String attributeName, List value);
	
	@SuppressWarnings("rawtypes")
	public void setAttribute(String attributeName, Map value);	
	
	public void setAttribute(String attributeName, boolean value);	
	
	public void save();
}
