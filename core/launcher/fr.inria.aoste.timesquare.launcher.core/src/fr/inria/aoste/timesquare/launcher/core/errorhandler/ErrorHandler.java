/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.errorhandler;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import fr.inria.aoste.timesquare.launcher.core.Activator;

public abstract class  ErrorHandler {

	
	
	public ErrorHandler() {
		super();		
	}

	public void notifyError(String title, String message, IStatus status) throws Exception
	{
		
	}
	
	
	boolean writelog=true;
	
	public IStatus createStatusError(String message , Throwable throwable)
	{
		return createStatusError(message, throwable, true);
	}
	
	
	public IStatus createStatusError(String message , Throwable throwable, boolean log)
	{
		IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, message, throwable);
		if (log && writelog)
		{
			Activator.getDefault().getLog().log(status);
		}
		return status;
	}

	public boolean isWritelog() {
		return writelog;
	}

	public void setWritelog(boolean writelog) {
		this.writelog = writelog;
	}
	
}
