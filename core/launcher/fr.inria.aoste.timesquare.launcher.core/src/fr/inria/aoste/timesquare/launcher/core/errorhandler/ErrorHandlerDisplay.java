/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.errorhandler;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;

public class ErrorHandlerDisplay extends ErrorHandler {

	// usage Futur , ne pas enlever
	
	
	/*static protected class RunnableAskMessage implements Runnable {
		private String message;
		private boolean result = false;
		private String title;

		RunnableAskMessage(String title, String message) {
			super();
			this.title = title;
			this.message = message;
		}

		public void run() {
			result = MessageDialog.openQuestion(new Shell(), title, message);
		}

		protected final boolean getResult() {
			return result;
		}

	}*/

	static private class RunnableErrorMessage implements Runnable {
		private String message;
		private String title;
		private Throwable exp = null;
		private IStatus status = null;

		public RunnableErrorMessage(String title, String message, IStatus status) {
			// super("Error Message");
			this.title = title;
			this.message = message;
			exp = status.getException();
			this.status = status;

		}


		public void run() 
		{
			if (exp != null)
				ErrorConsole.printError(exp);		
			ErrorDialog.openError(new Shell(), title, message, status);
		}
	}
	
	
	public void notifyError(String title, String message, IStatus status) throws Exception 
	{
		super.notifyError(title, message, status);
		Display.getDefault().asyncExec(new RunnableErrorMessage(title, message, status));
	}
	
/*	public void notifyRunnableErrorMessage(String title, String message, Throwable ex )
	{
		super.notifyRunnableErrorMessage(title, message, ex);
		Display.getDefault().asyncExec(new RunnableErrorMessage(title, message, ex));
	}*/
	
}
