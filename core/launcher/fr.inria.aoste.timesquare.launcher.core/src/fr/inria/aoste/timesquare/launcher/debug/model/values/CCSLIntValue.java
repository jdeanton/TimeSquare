/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.values;

import org.eclipse.debug.core.DebugException;

import fr.inria.aoste.timesquare.launcher.debug.model.CCSLValue;
import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget;

final public class CCSLIntValue extends CCSLValue {
	private int intValue;

	public CCSLIntValue(ICCSLDebugTarget target, int value) {
		super(target, "int");
		this.intValue = value;
	}

	String message=null;
	
	
	public int getIntValue() {
		return intValue;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setValue(int v) {
		this.intValue = v;
		this.message=null;
	}

	public String getValueString() throws DebugException {
		if (message!=null)
		{
			return  message;
		}
		return this.intValue + "";
	}

}
