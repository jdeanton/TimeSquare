/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.proxy;

import fr.inria.aoste.timesquare.launcher.debug.model.StateEngine;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.Trace;

public interface ISimulatorControl { 

	public abstract Trace getTrace();

	public abstract void join() throws InterruptedException, Exception;

	public abstract void syncRunStep() throws Exception ;

	public abstract void syncRunStep(int nb) throws Exception;
	
	public abstract void syncRunAllStep() throws Exception;

	public abstract void asyncRunStep() throws Exception;

	public abstract void asyncRunStep(int nb) throws Exception;
	
	public abstract void asyncRunAllStep() throws Exception;
		
	public abstract boolean isJoinable();

	public abstract boolean isTerminated();
	
	public abstract void syncPreviousStep() throws Exception ;
	
	public abstract void asyncPreviousStep() throws Exception ;
	
	public abstract LogicalStep getCurrentSolverStep();	

	public abstract LogicalStep getStepView();
	
	public abstract void terminate() throws InterruptedException, Throwable;

	public abstract StateEngine getStateEngine();	
	
	public abstract void pause();
	
	

}
