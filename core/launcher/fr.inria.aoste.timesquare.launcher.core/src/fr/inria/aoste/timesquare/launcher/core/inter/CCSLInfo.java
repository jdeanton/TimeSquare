/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.inter;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.relation.IDescription;

public interface CCSLInfo {

	
	/**
	 * 
	 * @return a integer N 
	 *  if the value changed , ==>  at least one return values of other methods (of <b>CCSLINFO</b>)  have changed
	 */
	public abstract int getCCSLInfoEvolution();
	
	/**
	 * 
	 * @return nbr of step 
	 */
	public abstract int getStepNbr(); //setStepnbr

	/**
	 *@ return path to file with input in simulation
	 */
	public abstract String getSource(); //SetSource
	
	/**
	 *@ return path to the file that contains the priority specification
	 */
	public abstract String getPrioritySource(); //SetSource

	/**
	 * 
	 * @return true is  a Solver create EnableState information
	 */
	public abstract boolean getEstate(); //setEState	

	/***
	 * 
	 * @return true if a model is copied when simulation start
	 */
	public abstract boolean getCopy(); //setCopy
	
	/***
	 * 
	 * @return list of clock /Expression find in the model
	 */
	public abstract List<ModelElementReference> getListofClock();

	/***
	 * 
	 * @return  list of assertion find in the model
	 */
	public abstract List<ModelElementReference> getListofAssert();

	

	/**
	 * 
	 * @return all description of relation
	 */
	public abstract List<IDescription> getListofRelation();

	/**
	 * 
	 * @return a set of all ReferencedObject
	 */
	public abstract List<EObject> getListReferencedObject();
	
	/**
	 * 
	 * @return list of ressource containts  <i>List<EObject> getListReferencedObject()</i>
	 */
	public abstract List<Resource> getRessourceOfReferencedObject();
	

	/**
	 * 
	 * @return true if exist a  object in <i>List<EObject> getListReferencedObject() </i> which is instance of eclass.
	 */
	public abstract boolean haveClockWithReferenceTo(EClass eclass);
	
	
	/**
	 *
	 */
	public abstract  Map<URI, Resource> getMappingURIReferencedObject() ;

}