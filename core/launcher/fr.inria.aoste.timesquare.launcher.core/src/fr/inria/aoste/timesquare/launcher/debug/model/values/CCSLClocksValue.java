/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.values;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IVariable;

import fr.inria.aoste.timesquare.launcher.debug.model.CCSLValue;
import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.LogicalStep;

final public class CCSLClocksValue extends CCSLValue {
	private LogicalStep stValue;

	public CCSLClocksValue(ICCSLDebugTarget target, LogicalStep value) {
		super(target, "ccsl");
		this.stValue = value;
	}

	public void setValue(LogicalStep st) {
		this.stValue = st;
	}

	public String getValueString() throws DebugException {
		if (stValue==null)
			return "null";
		return " LogicalStep  " + this.stValue.getStepNumber() + "";
	}

	@Override
	public IVariable[] getVariables() throws DebugException {

		try {

			ICCSLDebugTarget ccsldt = getDebugTarget();
			if (stValue == null)
				return new IVariable[0];
			int n = stValue.getEventOccurrences().size();
			IVariable[] vt = new IVariable[n];
			for (int i = 0; i < n; i++) {
				EventOccurrence cs = stValue.getEventOccurrences().get(i);
				//System.out.println(cs.eClass());
				vt[i] = new CCSLIntVariable(ccsldt, getClockName(cs));
				vt[i].setValue("" + cs.getCounter());
			}
			// for (v)
			return vt;
		} catch (Throwable e) {
			return new IVariable[0];
		}
	}

	@Override
	public boolean hasVariables() throws DebugException {
		return stValue != null;
	}

	
}