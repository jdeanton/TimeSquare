/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.launcher.debug.model.output;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Widget;

import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;

public abstract class OutputSetupOption {
	/** identifier view */
	/* Group */
	private static final String GROUP2 = "@group";
	/** Button SWT.CHECK  */
	private static final String ASK = "@ask";
	/** Button SWT.ARROW */
	private static final String COLLAPSE_BUTTON = "@collapseButton";
	private static final String COMPOSITE = "@composite";
	
	/****/
	public static final Class<OutputSetupOption> CLASS = OutputSetupOption.class;

	private Class<? extends IOutputOption> classoption = null;
		
	protected OutputSetupOption(String key, String name, Class<? extends IOutputOption> classoption) {
		super();
		this.key = key;
		this.name = name;
		this.classoption = classoption;
		this.ask = "activate "+name;		
	}

	public static <T extends Widget>  T registerID(T w, String id) {
		if (id != null && id.length() != 0) {
			w.setData(PluginHelpers.IDENTIFIER_WIDGET, id);
		}
		return w;
	}	
	
	public  <T extends Widget>  T register(T w, String id) {
		if (id != null && id.length() != 0) {
			registerID(w,getKey()+id);
		}
		return w;
	}	

	private String key;

	public final String getKey() {
		return key;
	}

	private ICCSLModelTab modelSimulationTab;
	private static class SelectionMetaActive extends SelectionAdapter {

		OutputSetupOption oso;

		public SelectionMetaActive(OutputSetupOption cin) {
			super();
			oso = cin;
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			oso.modelSimulationTab.updateLaunchConfigurationDialog(oso);
			oso.activeCheck();

		}
	}

	private Composite _c;
	private Button b;

	private void initMeta(Composite c, Button b) {
		this._c = c;
		this.b = b;
		if (b != null) {
			b.addSelectionListener(new SelectionMetaActive(this));
		}
	}

	public final String getName() {
		return name;
	}

	public final String getAsk() {
		return ask;
	}

	String name = "", ask = "";

	private static final int CONTROL_OFFSET = 3;

	// ExpandItem item0 = null;
	// ExpandBar bar = null;

	Group group = null;
	
	Button collapseButton =null;

	public static class CollapseButtonListener implements SelectionListener {

		private Composite _subcomposite;
	
		private Composite _container;
		private FormData _formdata;
		private Button button;
		/**
		 * @param timeNotesBox	
		 * @param container
		 * @param formdata
		 */
		public CollapseButtonListener(Button b,Composite timeNotesBox, Composite container, FormData formdata) {
			super();
			// save references to the various controls to update
			_subcomposite = timeNotesBox;
			
			_container = container;
			_formdata = formdata;
			button=b;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets
		 * .Event)
		 */

		public void handleEvent() {
		
			if (_subcomposite.getVisible()) {
				// collapse t
				_subcomposite.setSize(_subcomposite.computeSize(1, 1));
				_formdata.height = 1;
				_subcomposite.setLayoutData(_formdata);
				_subcomposite.setVisible(false);
				_subcomposite.pack(true);

				button.setAlignment(SWT.DOWN);
			} else {
				// expand 
				_subcomposite.setSize(_subcomposite.computeSize(-1, -1));
				_formdata.height = -1;
				_subcomposite.setLayoutData(_formdata);
				_subcomposite.setVisible(true);
				_subcomposite.pack(true);
				button.setAlignment(SWT.UP);				
			}

			// in this case the container is the Group
			_container.pack(true);
			_container.layout();
			// resize the parent shell too
			
			Composite tmp=_container.getParent();			
			tmp.pack(true);
			tmp.layout();
			 (( ScrolledComposite) tmp.getParent()).setShowFocusedControl(true);
			 (( ScrolledComposite) tmp.getParent()).layout();
			//_container.getParent().layout();
			//_container.getParent().getParent().layout();
			//_container.getParent().getParent().pack(true);	
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			handleEvent();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			handleEvent();
		}
	}

	
	Label textinfo=null;
	public Group initMeta(ICCSLModelTab modelSimulationTab, final Composite _parent) {
		this.modelSimulationTab = modelSimulationTab;
		group = new Group(_parent, SWT.RESIZE);
		register(group, GROUP2);
		// group.setLayout(new GridLayout(1, true));
		FormLayout layout = new FormLayout();
		group.setLayout(layout);
		group.setText(name);
		Composite info= new Composite(group, SWT.RESIZE);
		Button askb = modelSimulationTab.createCheckButton(info, ask);	
		askb.pack();
		register(askb, ASK);
		textinfo=new Label(info, SWT.RESIZE);
		info.setLayout(new GridLayout(1,false));
	
		collapseButton = new Button(group, SWT.ARROW | SWT.DOWN);
		register(collapseButton, COLLAPSE_BUTTON);
		
		final Composite c2 = new Group(group, SWT.NONE);
		c2.setLayout(new GridLayout(1, false));
		register(c2, COMPOSITE);
		FormData data4 = new FormData();
		//data4.top = new FormAttachment(0, CONTROL_OFFSET);
		data4.left = new FormAttachment(0, CONTROL_OFFSET);
		data4.bottom=new FormAttachment(c2, -CONTROL_OFFSET);
		data4.width=30;		
		data4.height=20;
		collapseButton.setLayoutData(data4);
		
		
		FormData data = new FormData();
		data.top = new FormAttachment(0, CONTROL_OFFSET);
		data.left = new FormAttachment(collapseButton, CONTROL_OFFSET);
		data.right = new FormAttachment(100, -CONTROL_OFFSET);
		info.setLayoutData(data);
	


		

		
		// c2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		FormData data5 = new FormData();
		data5.top = new FormAttachment(info, CONTROL_OFFSET);
		data5.height=1;
		data5.left = new FormAttachment(0, CONTROL_OFFSET);
		data5.right = new FormAttachment(100, -CONTROL_OFFSET);
		data5.bottom = new FormAttachment(100, -CONTROL_OFFSET);
		askb.setSelection(true);
		initMeta(c2, askb);
		c2.setLayoutData(data5);
		createControl(c2);
		c2.setLayoutData(data5);
		CollapseButtonListener cbl=new CollapseButtonListener( collapseButton ,c2, group, data5);
		//cbl.handleEvent();
		collapseButton.addSelectionListener(cbl);
		c2.setVisible(false);
		
		return group;
	}

	public final boolean isValid(IOutputOption configuration) {
		if (classoption != null)
			if (configuration != null)
				if (classoption.isAssignableFrom(configuration.getClass()))
					return _isValid(configuration);
		return false;
	}

	public final void setDefaults(IOutputOption configuration) {
		if (classoption != null)
			if (configuration != null)
				if (classoption.isAssignableFrom(configuration.getClass()))
					_setDefaults(configuration);
	}

	public final void initializeFrom(IOutputOption configuration) {
		if (classoption != null)
			if (configuration != null)
				if (classoption.isAssignableFrom(configuration.getClass())) {
					boolean bol = configuration.isActivable();
					if (!bol) {
						activeCheck(false);
						b.setEnabled(false);
					} else {
						b.setEnabled(true);
						activeCheck(configuration.isActive());
					}
					_initializeFrom(configuration);
				}
	}

	public final void performApply(IOutputOption configuration) {
		if (classoption != null)
			if (configuration != null)
				if (classoption.isAssignableFrom(configuration.getClass())) {
					boolean bol = configuration.isActivable();
					if (!bol) {
						activeCheck(false);
						b.setEnabled(false);
					} else {
						b.setEnabled(true);
						activeCheck(configuration.isActive());
					}
					_performApply(configuration);

				}
	}

	protected final boolean _isValid(IOutputOption configuration) {

		if (configuration.isActivable()) {
			if (configuration.isActive()) {
				String errorMessage = configuration.validate();
				if (errorMessage != null) {
					// if (!configuration.isValide(re) || re.isError()){
					modelSimulationTab.setErrorMessage(errorMessage);
					textinfo.setText(errorMessage);
					textinfo.setForeground(new Color(null, 255, 0, 0));
					return false;
				}

			}
			textinfo.setText("");
		} else {
			textinfo.setForeground(new Color(null, 255, 125, 0));
			textinfo.setText("Inactivable : " + configuration.getActivableMessage());
		}
		return true;
	}

	/* 
	 * @param configuration
	 */
	protected void _setDefaults(IOutputOption configuration) {

	}

	protected void _initializeFrom(IOutputOption configuration) {
		setSelected(configuration.isActive());
	}

	protected void _performApply(IOutputOption configuration) {
		configuration.setActive(isSelected());

	}

	protected void createControl(Composite parent) {

	}

	public final boolean isSelected() {
		return b.getSelection();
	}

	public final void setSelected(boolean sel) {
		activeCheck(sel);
		b.setSelection(sel);

	}

	private int activeCheck(boolean bol) {
		// bol = true;
		recurviceActive(_c, bol);
		return 0;
	}

	private int recurviceActive(Control ctl, boolean bol) {
		ctl.setEnabled(bol);
		if (ctl instanceof Composite) {
			for (Control ctl2 : ((Composite) ctl).getChildren()) {
				recurviceActive(ctl2, bol);
			}
		}
		return 0;
	}

	public int activeCheck() {
		activeCheck(b.getSelection());
		return 0;
	}

	// ModelData d
/*	public int newModel(ModelData d)
	// String model, List<EObject> lsteo ,List<EObject> lsassert)
	{
		return 0; // newModel(model, lsteo);
	}*/

	/**
	 * 
	 * @param model
	 * @param lsteo
	 * @return
	 */
	/*
	 * final public int newModel(String model, List<EObject> lsteo) {
	 * 
	 * //if (lsteo!=null) //for( EObject eo : lsteo) //{ //
	 * System.out.println(eo); //} return 0; }
	 */

	public void notifyChange() {		
		modelSimulationTab.updateLaunchConfigurationDialog(this);
	}

	public void dispose() {

	}
}