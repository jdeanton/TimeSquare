/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.proxy;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.relation.IDescription;

public class CCSLInfoDelegate implements CCSLInfo {

	
	public CCSLInfoDelegate(CCSLInfo ccsinfo) {
		super();
		this.ccsinfo = ccsinfo;
	}

	private CCSLInfo ccsinfo=null;

	public int getStepNbr() {
		return ccsinfo.getStepNbr();
	}

	public String getSource() {
		return ccsinfo.getSource();
	}

	public boolean getEstate() {
		return ccsinfo.getEstate();
	}

	public boolean getCopy() {
		return ccsinfo.getCopy();
	}

	public List<ModelElementReference> getListofClock() {
		return ccsinfo.getListofClock();
	}

	public List<ModelElementReference> getListofAssert() {
		return ccsinfo.getListofAssert();
	}

	public List<Resource> getRessourceOfReferencedObject() {
		return ccsinfo.getRessourceOfReferencedObject();
	}

	public List<IDescription> getListofRelation() {
		return ccsinfo.getListofRelation();
	}

	public List<EObject> getListReferencedObject() {
		return ccsinfo.getListReferencedObject();
	}

	public boolean haveClockWithReferenceTo(EClass element) {
		return ccsinfo.haveClockWithReferenceTo(element);
	}

	public int getCCSLInfoEvolution() {
		return ccsinfo.getCCSLInfoEvolution();
	}

	public Map<URI, Resource> getMappingURIReferencedObject() {
		return ccsinfo.getMappingURIReferencedObject();
	}

	@Override
	public String getPrioritySource() {
		return ccsinfo.getPrioritySource();
	}
	


}
