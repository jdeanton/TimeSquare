/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;

import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.NamedReference;
import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.State;

public abstract class CCSLValue extends CCSLDebugElement implements IValue {
	private String type;

	protected CCSLValue(ICCSLDebugTarget target, String type) {
		super(target);
		this.type = type;
	}

	public String getReferenceTypeName() throws DebugException {
		return this.type;
	}

	public IVariable[] getVariables() throws DebugException {
		return new IVariable[0];
	}

	public boolean hasVariables() throws DebugException {
		return false;
	}

	public boolean isAllocated() throws DebugException {
		return true;
	}
	
	protected String getClockName(State cs) {
		StringBuilder sb =new StringBuilder("");
		Reference rf = cs.getReferedElement();
		if (rf != null)
			sb.append( getname(rf));
		else
			sb.append(cs.toString());
		sb.append( ""); 
		return sb.toString();

	}
	public final String getname(Reference rf) {
		if (rf == null)
		{
			return "null";
		}
		if (rf instanceof NamedReference) {
			return ((NamedReference) rf).getValue();
		}
		if (rf instanceof ModelElementReference) {
			//EObject eo = ((ModelElementReference) rf).getElementRef();
			return AdapterRegistry.getAdapter(rf).getReferenceName(rf);
		}
		return rf.toString();
	}
}
