/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.console;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamsProxy;
import org.eclipse.debug.core.model.IStreamsProxy2;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.contexts.DebugContextEvent;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsolePageParticipant;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.actions.CloseConsoleAction;
import org.eclipse.ui.contexts.IContextActivation;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.handlers.IHandlerActivation;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.IPageBookViewPage;
import org.eclipse.ui.part.IPageSite;

import fr.inria.aoste.timesquare.launcher.core.Activator;

/*
 * 
 * EXperimental
 */

public class SimulationConsolePageParticipant implements IConsolePageParticipant
// , IShowInSource, IShowInTargetList, IDebugEventSetListener, IDebugContextListener
{

	public static final String CONSOLE_OPEN_ON_OUT = "DEBUG.consoleOpenOnOut"; //$NON-NLS-1$
	public static final String CONSOLE_OPEN_ON_ERR = "DEBUG.consoleOpenOnErr"; //$NON-NLS-1$

	public SimulationConsolePageParticipant() {
		super();

	}

	private CloseConsoleAction close;
	private ConsoleTerminateAction fTerminate;
	private ConsoleRemoveLaunchAction fRemoveTerminated;
	private ConsoleRemoveAllTerminatedAction fRemoveAllTerminated;
	private ShowWhenContentChangesAction fStdOut;
	private ShowWhenContentChangesAction fStdErr;

	private ConsoleSimulation fConsole;

	private IPageBookViewPage fPage;

	private IConsoleView fView;

	private class EOFHandler extends AbstractHandler {
		public Object execute(ExecutionEvent event) throws org.eclipse.core.commands.ExecutionException {
			try {
				IStreamsProxy proxy = getProcess().getStreamsProxy();
				if (proxy instanceof IStreamsProxy2) {
					IStreamsProxy2 proxy2 = (IStreamsProxy2) proxy;

					proxy2.closeInputStream();
				}

			} catch (Throwable e) {
				e.printStackTrace();
			}
			return null;
		}

	}

	private EOFHandler fEOFHandler;
	private String fContextId = "org.eclipse.debug.ui.console"; //$NON-NLS-1$;
	private IContextActivation fActivatedContext;
	private IHandlerActivation fActivatedHandler;

	public void activated() {
		// add EOF submissions
		IPageSite site = fPage.getSite();
		IHandlerService handlerService = (IHandlerService) site.getService(IHandlerService.class);
		IContextService contextService = (IContextService) site.getService(IContextService.class);
		fActivatedContext = contextService.activateContext(fContextId);
		fActivatedHandler = handlerService.activateHandler("org.eclipse.debug.ui.commands.eof", fEOFHandler); //$NON-NLS-1$
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.console.IConsolePageParticipant#deactivated()
	 */
	public void deactivated() {
		// remove EOF submissions
		IPageSite site = fPage.getSite();
		IHandlerService handlerService = (IHandlerService) site.getService(IHandlerService.class);
		IContextService contextService = (IContextService) site.getService(IContextService.class);
		handlerService.deactivateHandler(fActivatedHandler);
		contextService.deactivateContext(fActivatedContext);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.internal.ui.contexts.provisional.IDebugContextListener#contextEvent(org.eclipse.debug.internal.ui.contexts.provisional.DebugContextEvent)
	 */
	public void debugContextChanged(DebugContextEvent event) {
		if ((event.getFlags() & DebugContextEvent.ACTIVATED) > 0) {
			if (fView != null && getProcess().equals(DebugUITools.getCurrentProcess())) {
				fView.display(fConsole);
			}
		}

	}

	public void dispose() {
		// TODO Auto-generated method stub

	}

	public void init(IPageBookViewPage page, IConsole console) {
		try {
			fPage = page;
			fConsole = (ConsoleSimulation) console;

			fRemoveTerminated = new ConsoleRemoveLaunchAction(fConsole.getiProcess().getLaunch());
			fRemoveAllTerminated = new ConsoleRemoveAllTerminatedAction();
			fTerminate = new ConsoleTerminateAction(page.getSite().getWorkbenchWindow(), fConsole);
			close = new CloseConsoleAction(fConsole);
			fStdOut = new ShowStandardOutAction();
			fStdErr = new ShowStandardErrorAction();

			fView = (IConsoleView) fPage.getSite().getPage().findView(IConsoleConstants.ID_CONSOLE_VIEW);

			// DebugPlugin.getDefault().addDebugEventListener(this);
			// DebugUITools.getDebugContextManager().getContextService(fPage.getSite().getWorkbenchWindow()).addDebugContextListener(this);

			// contribute to toolbar
			IActionBars actionBars = fPage.getSite().getActionBars();
			configureToolBar(actionBars.getToolBarManager());

			// create handler and submissions for EOF
			fEOFHandler = new EOFHandler();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class required) {
		/*if (IShowInSource.class.equals(required)) {
			return this;
		}
		if (IShowInTargetList.class.equals(required)) {
			return this;
		}*/
		// CONTEXTLAUNCHING
		if (ILaunchConfiguration.class.equals(required)) {
			ILaunch launch = getProcess().getLaunch();
			if (launch != null) {
				return launch.getLaunchConfiguration();
			}
			return null;
		}		
		return null;
	}

	protected void configureToolBar(IToolBarManager mgr) {
		if (fTerminate != null)
			mgr.appendToGroup(IConsoleConstants.LAUNCH_GROUP, fTerminate);
		if (fRemoveTerminated != null)
			mgr.appendToGroup(IConsoleConstants.LAUNCH_GROUP, fRemoveTerminated);
		if (fRemoveAllTerminated != null)
			mgr.appendToGroup(IConsoleConstants.LAUNCH_GROUP, fRemoveAllTerminated);
		if (close != null)
			mgr.appendToGroup(IConsoleConstants.OUTPUT_GROUP, close);
		mgr.appendToGroup(IConsoleConstants.OUTPUT_GROUP, fStdOut);
		mgr.appendToGroup(IConsoleConstants.OUTPUT_GROUP, fStdErr);

	}

	protected IProcess getProcess() {
		return fConsole != null ? fConsole.getiProcess() : null;
	}

	public abstract class ShowWhenContentChangesAction extends Action implements IPropertyChangeListener {

		/**
		 * Constructs an action to toggle console auto activation preferences
		 */
		public ShowWhenContentChangesAction(String name) {
			super(name, IAction.AS_CHECK_BOX);
			setToolTipText(name);
			getPreferenceStore().addPropertyChangeListener(this);
			update();
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.util.IPropertyChangeListener#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
		 */
		public void propertyChange(PropertyChangeEvent event) {
			String property = event.getProperty();
			if (property.equals(getKey())) {
				update();
			}
		}

		protected abstract String getKey();

		private void update() {
			IPreferenceStore store = getPreferenceStore();
			if (store.getBoolean(getKey())) {
				// on
				setChecked(true);
			} else {
				// off
				setChecked(false);
			}
		}

		/**
		 * @return
		 */
		private IPreferenceStore getPreferenceStore() {
			return Activator.getDefault().getPreferenceStore();
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.action.Action#run()
		 */
		public void run() {
			IPreferenceStore store = getPreferenceStore();
			boolean show = isChecked();
			store.removePropertyChangeListener(this);
			store.setValue(getKey(), show);
			store.addPropertyChangeListener(this);
		}

		/**
		 * Must be called to dispose this action.
		 */
		public void dispose() {
			getPreferenceStore().removePropertyChangeListener(this);
		}

	}

	public class ShowStandardOutAction extends ShowWhenContentChangesAction {

		/**
		 * Constructs an action to toggle console auto activation preferences
		 */
		public ShowStandardOutAction() {
			super("Show Console When Standard Out Changes");
			setId(Activator.PLUGIN_ID + ".ShowWhenStdoutChangesAction"); //$NON-NLS-1$
			PlatformUI.getWorkbench().getHelpSystem().setHelp(this, "org.eclipse.debug.ui.show_stdout_action_context");
			// IDebugHelpContextIds.SHOW_WHEN_STDOUT_CHANGES_ACTION);
			setImageDescriptor(DebugUITools.getImageDescriptor("IMG_ELCL_STANDARD_OUT"));
			// IInternalDebugUIConstants.IMG_ELCL_STANDARD_OUT));
		}

		protected String getKey() {
			return CONSOLE_OPEN_ON_OUT;
		}

	}

	public class ShowStandardErrorAction extends ShowWhenContentChangesAction {

		/**
		 * Constructs an action to toggle console auto activation preferences
		 */
		public ShowStandardErrorAction() {
			super("Show Console When Standard Error Changes"); // ConsoleMessages.ShowStandardErrorAction_0);
			// DebugPlugin.getUniqueIdentifier(),
			// DebugUIPlugin.getUniqueIdentifier()
			setId(Activator.PLUGIN_ID + ".ShowWhenStderrChangesAction"); //$NON-NLS-1$
			PlatformUI.getWorkbench().getHelpSystem().setHelp(this, "org.eclipse.debug.ui.show_stderr_action_context");
			// IDebugHelpContextIds.SHOW_WHEN_STDERR_CHANGES_ACTION);
			setImageDescriptor(DebugUITools.getImageDescriptor("IMG_ELCL_STANDARD_ERR"));
			// IInternalDebugUIConstants.IMG_ELCL_STANDARD_ERR));
		}

		protected String getKey() {
			return CONSOLE_OPEN_ON_ERR;
		}

	}
}
