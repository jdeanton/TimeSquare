/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.output;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public interface ICCSLModelTab {

	public abstract void updateLaunchConfigurationDialog(Object context);


	public Button createCheckButton(Composite parent, String label);

	public Button createPushButton(Composite parent, String label, Image image);

	public Text createLabelInput(Composite parent, String labelName, int width);

	public Text createLabelInput(Composite parent, String labelName, int width, boolean b);

	public Combo createCombo(Composite parent, String labelName, String[] values, int width);

	public void setErrorMessage(String message);
	
	public void setMessage(String message) ;

	public abstract void notifyChange();
}