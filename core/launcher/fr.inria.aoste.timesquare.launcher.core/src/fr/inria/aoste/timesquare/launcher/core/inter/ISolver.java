/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.inter;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.resource.ResourceSet;

import fr.inria.aoste.timesquare.launcher.select.ConstraintEnable;
import fr.inria.aoste.timesquare.utils.timedsystem.TimedSystem;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.Reference;

public interface ISolver {

	
	public abstract void setListReference(List<Reference>  lrf);
	/**
	 * Solve one step in the CCSL model
	 * 
	 * @return StepTrace: a trace of the step simulation
	 */
	public abstract LogicalStep solveNextSimulationStep(TimedSystem ts);

	/**
	 * Ends the simulation. Can be used to cleanup internal structures in the solver.
	 */
	public abstract void endSimulation();

	public abstract Throwable getException();

	public abstract void endSimulationStep();

	public abstract ResourceSet getResourceSet();
	
	public void start() throws Throwable ;
	
	public List<ModelElementReference> getClockList() ;
	
	public List<ModelElementReference> getConstraint() ;
	
	public List<ModelElementReference> getAssertList();
	
	//TODO remove it ?
	public ArrayList<ConstraintEnable> getSelectableModel() ;
	

}