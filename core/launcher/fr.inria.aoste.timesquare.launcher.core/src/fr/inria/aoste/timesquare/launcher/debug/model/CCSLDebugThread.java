/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDisconnect;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.debug.core.model.IVariable;

import fr.inria.aoste.timesquare.launcher.debug.model.values.CCSLAssertVariable;
import fr.inria.aoste.timesquare.launcher.debug.model.values.CCSLClockVariable;
import fr.inria.aoste.timesquare.launcher.debug.model.values.CCSLIntVariable;

final public class CCSLDebugThread extends CCSLDebugElement implements IThread, Runnable, ThreadInterface , IDisconnect{
	
	final private ISimulationInterface sim;
	static final private boolean isStepping = false;
	final private IVariable[] variables;
	final private CCSLIntVariable stepVar;
	final private CCSLIntVariable stepView;
	final private CCSLClockVariable stepclock;
	final private CCSLAssertVariable stepsassert ;

	public CCSLDebugThread(ICCSLDebugTarget target, ISimulationInterface sim) {
		super(target);
		assert(sim != null);
		this.sim = sim;
		stepVar = new CCSLIntVariable(target, "Next Step To Compute");
		stepView = new CCSLIntVariable(target, "Visible Step");
		stepclock = new CCSLClockVariable(target, "Clocks");
		stepsassert = new CCSLAssertVariable(target, "Assert");
		variables = new IVariable[] { stepVar, stepView, stepclock,stepsassert };
	}
	
	
	

	IVariable[] getVariables() {
		return variables;
	}

	int getCurrentStep() {
		return sim.getCurrentStepIndice();
	}

	public IBreakpoint[] getBreakpoints() {
		System.out.println("getBreakpoints");
		return new IBreakpoint[0];
	}

	/*******/

	public boolean canResume() {
		return isRunnable();// !isTerminated() && isSuspended() && isPause();
	}

	public boolean canSuspend() {
		return isRun();
	}

	public boolean canStepInto() {
		return isRunnable(); //|| isFinish();

	}

	public boolean canStepOver() {
		return isRunnable();//|| isFinish(); // (!isTerminated()) && isSuspended() && isPause();
	}

	public boolean canStepReturn() {
		return false;
	}

	public boolean canTerminate() {
		return !isFinish() && isInitialize();
	}

	private boolean isInitialize() {
		return sim.getStateEngine() != StateEngine.init;
	}

	private boolean isFinish() {
		if (sim != null)
			return sim.getStateEngine().isFinish();
		return false;
	}

	private boolean isTerminateStatus() {
		if (sim != null)
			return sim.getStateEngine().isTerminate();
		return false;
	}

	/***********/

	public boolean isSuspended() {
		return getDebugTarget().isSuspended();
	}

	public void resume() throws DebugException {
		setRunStart();
		if (sim.getStateEngine().isTerminate()){
				//isTerminated()) {
			terminate();
			return;
		}
		theThread = new ThreadRun();
		// Display.getDefault().asyncExec(theThread);
		theThread.start();
	}

	public void suspend() throws DebugException {
		// getDebugTarget().suspend();
		sim.stopstep();
	}

	public boolean isRun() {
		if (sim != null)
			return sim.getStateEngine().isRunning();
		return false;
	}

	public boolean isRunnable() {
		if (sim != null)
			return sim.getStateEngine().isRunnable();
		return false;
	}

	public boolean isStepping() {
		return isStepping;
	}

	public void setRunStart() {
		// this.run = true;
		fireEvent(new DebugEvent(this, DebugEvent.STEP_OVER));
	}

	public void setRunFinish() {
		// this.run = false;
		fireEvent(new DebugEvent(this, DebugEvent.STEP_OVER));
	}

	public void updateVar() {
		try {
			if (isTerminated())
			{
				stepVar.setValue("---");
			}	
			else
			{
				stepVar.setValue(getCurrentStep());
			}
			stepView.setValue(sim.getViewStepIndice());
			stepclock.setValue(sim.getStepview());
			stepsassert.setValue(sim.getStepview());
			if (sim.getStateEngine().isFinish())// isTerminated())
				terminate();
			else
				fireEvent(new DebugEvent(this, DebugEvent.STEP_OVER));
		} catch (Throwable e) {

		}

	}

	public void endofStep() throws DebugException {
		if (theThread != null && theThread.isAlive()) {
			theThread = null;
		}
		if (isTerminated())
		{
			stepVar.setValue("---");
		}	
		else
		{
			stepVar.setValue(getCurrentStep());
		}
		stepView.setValue(sim.getViewStepIndice());
		stepclock.setValue(sim.getStepview());
		stepsassert.setValue(sim.getStepview());
		// this.run = false;
		if (isTerminateStatus())	
		{			
			fireEvent(new DebugEvent(this, DebugEvent.STEP_OVER));
		//	terminate();
		}
		else
		{
			fireEvent(new DebugEvent(this, DebugEvent.STEP_OVER));
		}	
	}

	Thread theThread = null;

	private class ThreadRun extends Thread {

		public ThreadRun() {
			super("Running solver");

		}

		@Override
		public void run() {
			try {
				sim.steprun();
			} catch (Throwable e) {
				e.printStackTrace();
			}
			try {
				endofStep();
			} catch (Throwable e) {
				e.printStackTrace();
			}
			sim.finish();			
		}

	}

	private class ThreadStepN extends Thread {
		int nb;

		public ThreadStepN(int nb) {
			super("resolve " + nb + " steps");
			this.nb = nb;
		}

		@Override
		public void run() {
			try {
				sim.step(nb);

			} catch (Throwable e) {
				e.printStackTrace();
			}
			try {
				endofStep();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

	}

	private class ThreadStep extends Thread {

		public ThreadStep() {
			super("resolve 1 step");

		}

		@Override
		public void run() {
			try {
				sim.step(false);

			} catch (Throwable e) {
				e.printStackTrace();
			}
			try {
				endofStep();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

	}

	public class ThreadStart extends Thread {

		protected ThreadStart() {
			super("Starting Solver");

		}

		@Override
		public void run() {
			try {
				sim.init();
				fireCreationEvent();
			} catch (Throwable e) {

			}
		}

	}

	public void stepInto() throws DebugException {
		try {
			setRunStart();
			if (isTerminateStatus()) {
				terminate();
				// run = false ;
				return;
			}
			// sim.step();
			theThread = new ThreadStep();
			// Display.getDefault().asyncExec(theThread);
			theThread.start();
			// endofStep();
		} catch (Throwable e) {
			// run = false;
			abort("Error in myStep", e);

		}
	}

	public void stepOver() throws DebugException {
		try {

			setRunStart();
			if (isTerminateStatus()) {
				terminate();
				return;
			}
			theThread = new ThreadStepN(10);
			// Display.getDefault().asyncExec(theThread);
			theThread.start();
			// sim.step(10);
			// endofStep();
		} catch (Throwable e) {
			// run = false;
			abort("Error in myStep", e);
		}

	}

	public void stepReturn() throws DebugException {
	}

	private int exitValue = 1;

	public int getExitValue() {
		return exitValue;
	}
	ThreadStart th=null; 
	// Runnable
	public void run() {
		super.fireCreationEvent();
		System.out.println("CCSL Thread run");

		try {
			// sim.init();
			th = new ThreadStart();
			// Display.getDefault().asyncExec(th);
			th.start();
			// Display.getDefault().syncExec(th);

			// while(!isTerminated()) {
			// try {
			// Thread.sleep(1000);
			// } catch (InterruptedException e) {
			// System.err.println("CCSLThread : "+e);
			// e.printStackTrace();
			// }
			// Thread.yield();
			// }
			th.join();
			exitValue = 0;
		} catch (Exception ce) {
			System.err.println("CCSL Thread abnormal ending " + ce);
			super.fireTerminateEvent();
			exitValue = -1;
		} finally {
			// super.fireTerminateEvent();
		}
	}
	
	public void joinThreadStart() throws InterruptedException
	{
		if (th!=null)
			th.join();
	}

	static private int count = 0;
	private int num = count++;

	final public String getName() throws DebugException {
		return "CCSLDebugThread [" + num + "]";
	}

	final public int getPriority() throws DebugException {
		return 0;
	}

	final public IStackFrame[] getStackFrames() {
		if (isSuspended()) {
			return getDebugTarget().getStackFrames();
		} else {
			return new IStackFrame[0];
		}
	}

	final public IStackFrame getTopStackFrame() throws DebugException {
		IStackFrame[] frames = getStackFrames();
		if (frames.length > 0) {
			return frames[0];
		}
		return null;
	}

	public boolean hasStackFrames() throws DebugException {
		return isSuspended();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ThreadInterface#isTerminated()
	 */
	public boolean isTerminated() {
		return  sim.isTerminated();
	}

	private class TerminateThread extends Thread {

		protected TerminateThread() {
			super("Terminiate Thread");
		}

		@Override
		public void run() {
			try {
				sim.finish();
				// MyPluginManager.refreshWorkspace();
				//fireEvent(new DebugEvent(this, DebugEvent.TERMINATE));
				getDebugTarget().terminate();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ThreadInterface#terminate()
	 */
	public void terminate() throws DebugException {
		// Notifie End ...
		if (theThread != null && theThread.isAlive()) {
			sim.stopstep();
			try {
				theThread.join();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Thread t = new TerminateThread(); // t
		// Display.getDefault().asyncExec(t);
		t.start();

	}

	public ISimulationInterface getSim() {
		return sim;
	}




	@Override
	public boolean canDisconnect() {	
		return true;
	}



	boolean disconnected=false;
	@Override
	public void disconnect() throws DebugException {
		disconnected=true;
		
	}




	@Override
	public boolean isDisconnected() {		
		return disconnected;
	}

}
