package fr.inria.aoste.timesquare.launcher.core.console;

/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/* Copy on org.eclipse.debug.internal.ui.views.console.ConsoleTerminateAction;
 * 
 * 
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.ILaunchesListener2;
import org.eclipse.debug.core.commands.IDebugCommandHandler;
import org.eclipse.debug.core.commands.IDebugCommandRequest;
import org.eclipse.debug.core.commands.ITerminateHandler;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IUpdate;

import fr.inria.aoste.timesquare.launcher.debug.model.CCSLProcess;

/**
 * ConsoleTerminateAction
 */
public class ConsoleTerminateAction extends Action implements IUpdate, ILaunchesListener2 {

	private ConsoleSimulation fConsole;

	// private IWorkbenchWindow fWindow;

	/**
	 * Creates a terminate action for the console
	 * @param window 
	 * @param console
	 */
	public ConsoleTerminateAction(IWorkbenchWindow window, ConsoleSimulation console) {
		super("&Terminate");
		// super(ConsoleMessages.ConsoleTerminateAction_0);
		// setId("id3" );
		PlatformUI.getWorkbench().getHelpSystem().setHelp(this, "org.eclipse.debug.ui.console_terminate_action_context");
		// IDebugHelpContextIds.CONSOLE_TERMINATE_ACTION);
		fConsole = console;
		// fWindow = window;
		setToolTipText("Terminate");
		setImageDescriptor(DebugUITools.getImageDescriptor("IMG_LCL_TERMINATE")); // IInternalDebugUIConstants
		setDisabledImageDescriptor(DebugUITools.getImageDescriptor("IMG_DLCL_TERMINATE"));
		setHoverImageDescriptor(DebugUITools.getImageDescriptor("IMG_LCL_TERMINATE"));
		DebugPlugin.getDefault().getLaunchManager().addLaunchListener(this);
		update();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.texteditor.IUpdate#update()
	 */
	public void update() {
		CCSLProcess process = fConsole.getiProcess();

		ILaunch launch = process.getLaunch();
		if (launch != null) {
			setEnabled(!launch.isTerminated() && launch.canTerminate());
		} else {
			setEnabled(false);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run() {
		CCSLProcess process = fConsole.getiProcess();
		List<IAdaptable> targets = collectTargets(process);
		targets.add(process);

		// DebugCommandService service = DebugCommandService.getService(fWindow);
		// service.
		// new DebugCommandService(fWindow).
		executeCommand(targets.toArray());
		setEnabled(false);

	}

	/**
	 * Collects targets associated with a process.
	 * 
	 * @param process
	 * @return associated targets
	 */

	private List<IAdaptable> collectTargets(CCSLProcess process) {
		ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
		ILaunch[] launches = launchManager.getLaunches();
		List<IAdaptable> targets = new ArrayList<IAdaptable>();
		for (int i = 0; i < launches.length; i++) {
			ILaunch launch = launches[i];
			IProcess[] processes = launch.getProcesses();
			for (int j = 0; j < processes.length; j++) {
				IProcess process2 = processes[j];
				if (process2.equals(process)) {
					IDebugTarget[] debugTargets = launch.getDebugTargets();
					for (int k = 0; k < debugTargets.length; k++) {
						targets.add(debugTargets[k]);
					}
					return targets; // all possible targets have been terminated for the launch.
				}
			}
		}
		return targets;
	}

	public void dispose() {
		fConsole = null;
	}

	/****************************************/

	private static class DebugCommandRequest implements IDebugCommandRequest {
		private Object[] fElements;

		public DebugCommandRequest(Object[] elements) {
			fElements = elements;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.debug.core.commands.IDebugCommandRequest#getElements()
		 */
		public Object[] getElements() {
			return fElements;
		}

		private IStatus fStatus;
		private boolean fCanceled = false;

		/* (non-Javadoc)
		 * @see org.eclipse.debug.core.commands.IStatusCollector#done()
		 */
		public void done() {
		}

		/* (non-Javadoc)
		 * @see org.eclipse.debug.core.commands.IStatusCollector#getStatus()
		 */
		public IStatus getStatus() {
			return fStatus;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.debug.core.commands.IStatusCollector#setStatus(org.eclipse.core.runtime.IStatus)
		 */
		public void setStatus(IStatus status) {
			fStatus = status;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.debug.core.commands.IRequest#cancel()
		 */
		public synchronized void cancel() {
			fCanceled = true;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.debug.core.commands.IRequest#isCanceled()
		 */
		public synchronized boolean isCanceled() {
			return fCanceled;
		}
	}

	
	public boolean executeCommand(Object[] elements) {
		if (elements.length == 1) {
			// usual case - one element
			Object element = elements[0];
			IDebugCommandHandler handler = getHandler(element);
			if (handler != null) {
				IDebugCommandRequest request = new DebugCommandRequest(elements);
				// request.setCommandParticipant(participant);
				return handler.execute(request);
			}
		} else {
			Map<?, ?> map = collate(elements);
			if (map != null) {
				boolean enabled = true;
				Iterator<?> entries = map.entrySet().iterator();
				while (entries.hasNext()) {
					Entry<?, ?> entry = (Entry<?, ?>) entries.next();
					IDebugCommandHandler handler = (IDebugCommandHandler) entry.getKey();
					List<?> list = (List<?>) entry.getValue();
					IDebugCommandRequest request = new DebugCommandRequest(list.toArray());
					// request.setCommandParticipant(participant);
					// specifically use & so handler is executed
					enabled = enabled & handler.execute(request);
				}
				return enabled;
			}
		}
		// ABORT - no command processors
		return false;
	}

	/**
	 * Returns a map of command handlers to associated elements, or <code>null</code> if one is missing.
	 * 
	 * @param elements
	 * @return map of command handlers to associated elements or <code>null</code>
	 */
	private Map<IDebugCommandHandler, List<Object>> collate(Object[] elements) {
		Map<IDebugCommandHandler, List<Object>> map = new HashMap<IDebugCommandHandler, List<Object>>();
		for (int i = 0; i < elements.length; i++) {
			Object element = elements[i];
			IDebugCommandHandler handler = getHandler(element);
			if (handler == null) {
				return null;
			} else {
				List<Object> list = map.get(handler);
				if (list == null) {
					list = new ArrayList<Object>();
					map.put(handler, list);
				}
				list.add(element);
			}
		}
		return map;
	}

	private IDebugCommandHandler getHandler(Object element) {
		return (IDebugCommandHandler) DebugPlugin.getAdapter(element, ITerminateHandler.class);
	}

	public void launchesTerminated(ILaunch[] launches) {
		update();
	}

	public void launchesAdded(ILaunch[] launches) {
		update();

	}

	public void launchesChanged(ILaunch[] launches) {
		update();

	}

	public void launchesRemoved(ILaunch[] launches) {
		update();

	}

}
