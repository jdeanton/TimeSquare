/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.timesquare.launcher.core.errorhandler.ErrorHandler;
import fr.inria.aoste.timesquare.launcher.core.errorhandler.ErrorHandlerDisplay;
import fr.inria.aoste.timesquare.launcher.core.errorhandler.ErrorHandlerSimulation;
import fr.inria.aoste.timesquare.launcher.debug.model.CCSLDebugTarget;
import fr.inria.aoste.timesquare.launcher.debug.model.CCSLNormalThread;
import fr.inria.aoste.timesquare.launcher.debug.model.CCSLProcess;
import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget;
import fr.inria.aoste.timesquare.launcher.debug.model.MySimulationEngine;
import fr.inria.aoste.timesquare.launcher.debug.model.ThreadInterface;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.CCSLSimulationConfigurationHelper;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.ICCSLModelLauncher;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.ISimulatorControl;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;

/**
 * This class is registered by the theFactory.i.a.t.launcher.core plugin as an extension on the extension point
 * <strong>org.eclipse.debug.core.launch.launchConfigurationTypes</strong>. It is responsible of launching
 * a CCSL simulation when the function {@link #launch(ILaunchConfiguration, String, ILaunch, IProgressMonitor)}
 * is called by the launch framework.
 *  
 * @author Benoit Ferrero
 * @see #launch(ILaunchConfiguration, String, ILaunch, IProgressMonitor)
 */
public class CCSLModelLauncher implements ILaunchConfigurationDelegate, ICCSLModelLauncher {

	static final private String noFileFoundMessage = "No CCSL source file found ";

	static private final class DisplayRunnable implements Runnable {
		private final IWorkbench workBench;

		private DisplayRunnable(IWorkbench workBench) {
			this.workBench = workBench;
		}

		public void run() {
			IWorkbenchWindow window = workBench.getActiveWorkbenchWindow();
			try {
				IWorkbenchPage iwp = workBench.showPerspective("org.eclipse.debug.ui.DebugPerspective", window);
				iwp.findView("org.eclipse.debug.ui.DebugView");
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	static private final class AskRunnable implements Runnable {

		boolean b = false;

		private AskRunnable() {

		}

		public void run() {
			try {
				b = MessageDialog.openQuestion(Display.getDefault().getActiveShell(), "Simulation ",
						"the input model is not saved . Do you want run simulation ?");
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		public boolean get() {
			return b;
		}
	}

	public CCSLModelLauncher() {
		super();
		System.out.println("Init delegate");
	}
	
	/**
	 * This function is called by the launch framework to initiate and start a CCSL simulation according
	 * to the launchConfiguration given in argument.
	 */
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
			throws CoreException {
		try {
			PESolverManager.getDefault().clearCache(0);
			// PESolverManager.getDefault().clearCache(1);
			CCSLSimulationConfigurationHelper helper = CCSLSimulationConfigurationHelper.create(configuration);
			try {
				if (!configuration.exists()) {
					if (configuration instanceof ILaunchConfigurationWorkingCopy) {
						ILaunchConfigurationWorkingCopy loc = ((ILaunchConfigurationWorkingCopy) configuration);
						String name=DebugPlugin.getDefault().getLaunchManager().generateLaunchConfigurationName(helper.get_NameFile(false));
						//loc.rename(configuration.getName() + new Date().getTime());
						// loc.getMemento()
						loc.rename(name);
						loc.doSave();
					}
				}
			} catch (Throwable e) {
				System.err.println(e);
			}

			//CCSLSimulationConfigurationHelper helper = CCSLSimulationConfigurationHelper.create(configuration);
			CCSLSimulationConfigurationHelper.clearCache();
			launch(helper, mode, launch);
		} catch (Throwable t) {
			t.printStackTrace();
			if (t instanceof CoreException)
				throw (CoreException) t;
			throw new CoreException(new Status(IStatus.ERROR, Activator.PLUGIN_ID, "error launching ", t)); // RuntimeException(t);
		}
	}

	public void getShowPerspective() {
		final IWorkbench workBench = PlatformUI.getWorkbench();
		try {
			Display display = workBench.getDisplay();
			display.syncExec(new DisplayRunnable(workBench));
		} catch (Throwable e) {

		}

	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.core.ICCSLModelLauncher#launch(fr.inria.aoste.timesquare.launcher.core.inter.ICCSLProxy, java.lang.String, org.eclipse.debug.core.ILaunch)
	 */
	@Override
	public ISimulatorControl launch(CCSLSimulationConfigurationHelper helper, String mode, ILaunch launch) throws CoreException,
			InterruptedException { // Throwable
		// {
		IFile file = helper.get_SourceIFile();
		if (file == null || (file.exists() == false)) {
			System.err.println(noFileFoundMessage + " :" + file);
			throw new CoreException(new Status(IStatus.ERROR, Activator.PLUGIN_ID, noFileFoundMessage, new Error(noFileFoundMessage)));

		}
		boolean b = false;
		try {
			if (PluginHelpers.getActiveWorkbench() != null) {
				for (IEditorPart iep : PluginHelpers.getActiveWorkbench().getActivePage().getDirtyEditors()) {
					IEditorInput iei = iep.getEditorInput();
					IPersistableElement ipe = iei.getPersistable();
					if (ipe instanceof FileEditorInput) {
						if (file.equals(((FileEditorInput) ipe).getFile()))
							b = true;
					}
				}

			}
		} catch (Throwable e) {

		}
		if (b) {
			AskRunnable ask = new AskRunnable();
			Display.getDefault().syncExec(ask);
			if (!ask.get())
				return null;
		}
		CCSLProcess process = new CCSLProcess(launch, helper);
		MySimulationEngine sim = new MySimulationEngine(helper);		
	

		if (mode.equals("debug")) {
			sim.setErrorHandler(new ErrorHandlerDisplay());
			PluginHelpers.getShowView("org.eclipse.debug.ui.DebugView");
			helper.setConsole(new ConsoleSimulation(process, "CCSL : " + helper.getSource() + " " + helper.getDatestr()));
			getShowPerspective();
			helper.setConsole(new ConsoleSimulation(process, "   priority specification : " + helper.getPrioritySource() + " " + helper.getDatestr()));
			getShowPerspective();
			ICCSLDebugTarget ccslDebugTarget = new CCSLDebugTarget(launch, process, sim);
			return new SimulatorControl(ccslDebugTarget.getRun(), sim);
		} 
		if (mode.equals("simulation")) {
			//helper.
			ErrorHandler errorHandler=helper.getErrorHandler();
			if ( errorHandler==null)
				sim.setErrorHandler(new ErrorHandlerSimulation());
			else
				sim.setErrorHandler(errorHandler);
			
			helper.setConsole(new ConsoleSimulation());
			ICCSLDebugTarget ccslDebugTarget = new CCSLDebugTarget(launch, process, sim);
			return new SimulatorControl(ccslDebugTarget.getRun(), sim);
		} 
		if (mode.equals("run"))
		{
			sim.setErrorHandler(new ErrorHandlerDisplay());
			PluginHelpers.getShowView("org.eclipse.debug.ui.DebugView");
			helper.setConsole(new ConsoleSimulation(process, "CCSL : " + helper.getSource() + " " + helper.getDatestr()));
			ThreadInterface runnable = new CCSLNormalThread(sim ,process);
			Thread d = process.addThread(runnable);
			return new SimulatorControl(d, sim);
			
		}
		throw new RuntimeException("Unknow Simulation mode :" +mode  );
		//return null;
	}

}
