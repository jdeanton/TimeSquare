/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.extensionpoint;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import fr.inria.aoste.trace.LogicalStep;

/**
 * @author bferrero
 * 
 */
public abstract class OutputCommon implements IOutputTrace {

	
	/**
	 * @author fmallet
	 * @param name
	 */
	public OutputCommon(String name) {
		super();
		this.name = name;
	}

//	static protected  AdapterRegistry adapterRegistry=AdapterRegistry.getDefault();
//	/**
//	 * @param rf
//	 * @return
//	 */
//	public final String refgetname(Reference rf) {
//		if (rf == null)
//			return "null";
//		if (rf instanceof NamedReference) {
//			return ((NamedReference) rf).getValue();
//		}
//		if (rf instanceof ModelElementReference) {
//		//	EObject eo = ((ModelElementReference) rf).getElementRef();
//			//if (eo != null)
//				String s = AdapterRegistry.getAdapter(rf).getReferenceName(rf);
//				if( s!=null && s.length()!=0) 
//					return s;
//			return rf.toString();
//		}
//		return rf.toString();
//	}

//	public final String refgetAliasName(Reference rf) {
//		if (rf == null)
//			return "null";
//		if (rf instanceof NamedReference) {
//			return ((NamedReference) rf).getValue();
//		}
//		if (rf instanceof ModelElementReference) {
//		//	EObject eo = ((ModelElementReference) rf).getElementRef();
//			//if (eo != null)
//				//return 
//				String s = AdapterRegistry.getAdapter(rf).getAliasName(rf);
//				if( s!=null && s.length()!=0) 
//					return s;
//			return rf.toString();
//			
//		}
//		return rf.toString();
//	}

	/***
	 * 
	 * @param rf
	 * @return
	 */
//	protected final Boolean isHidden(Reference rf) {
//		if (rf instanceof NamedReference) {
//			return Boolean.TRUE;
//		}
//		if (rf instanceof ModelElementReference) {
//			//EObject eo = ((ModelElementReference) rf).getElementRef();
//			//if (eo != null)
//				return Boolean.valueOf(AdapterRegistry.getAdapter(rf).isHidden(rf));
//		}
//		return Boolean.FALSE;
//	}

	// TODO hide methods

//	protected final String getRef(Reference rf) {
//		try {
//			if (rf instanceof NamedReference) {
//				return null;
//			}
//			if (rf instanceof ModelElementReference) {
//				EObject eo = HelperFactory.getFirstReference((ModelElementReference) rf); 
//					//((ModelElementReference) rf).getElementRef();
//				Resource rs = rf.eResource();
//				// Resource rs2 = eo.eResource();
//				if (rs instanceof XMLResource) {
//					XMLHelper xmlhelper = new XMLHelperImpl((XMLResource) rs);
//					HashMap<String, Object> map = new HashMap<String, Object>();
//					URIHandlerImpl urih = new URIHandlerImpl();
//					urih.setBaseURI(rs.getURI());
//					map.put(XMLResource.OPTION_URI_HANDLER, urih);
//					xmlhelper.setOptions(map);
//					System.out.println(eo.eResource());
//					String s = xmlhelper.getHREF(eo);
//					return s;
//				}
//			}
//		} catch (Throwable e) {
//			System.err.println(e.getStackTrace()[1] + " : " + e.getMessage());
//		}
//		return null;
//	}

	@Override
	final public void aNewStep_And_Relation(LogicalStep aStep, boolean timed, List<EObject> relation) {
		aNewStep(aStep, timed);
		aNewRelation(relation);
	}
	
	protected abstract void aNewStep(LogicalStep aStep, boolean timed);

	/**
	 * 
	 * @param relation
	 * @Deprecated replace by <BR>
	 *             public void aNewStep_And_Relation(LogicalStep aStep, boolean timed, List<EObject> relation) {<BR>
	 *             aNewStep(aStep, timed);<BR>
	 *             aNewRelation(relation);<BR>
	 *             }
	 */
	protected abstract void aNewRelation(List<EObject> relation);
	
	private String name = "";
	public final  String getName() {
		if (name == null || name.length() == 0)
			return getkey();
		return name;
	}
}
