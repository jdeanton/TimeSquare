/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.values;

import org.eclipse.debug.core.DebugException;

import fr.inria.aoste.timesquare.launcher.debug.model.CCSLVariable;
import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget;
import fr.inria.aoste.trace.LogicalStep;

final public class CCSLClockVariable extends CCSLVariable {

	public CCSLClockVariable(ICCSLDebugTarget target, String name) {
		super(target, name, "ccsl");
		setValue(new CCSLClocksValue(target, null));
	}

	public boolean verifyValue(String expression) throws DebugException {
		try {
			// Integer.parseInt(expression);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	private CCSLClocksValue getClockValue() {
		return (CCSLClocksValue) super.getValue();
	}

	@Override
	public void setValue(String expression) {
		super.setValue(expression);
		// getIntValue().setValue(Integer.parseInt(expression));
	}

	public void setValue(LogicalStep v) {
		super.setValue("" + v + "");
		getClockValue().setValue(v);
	}
}
