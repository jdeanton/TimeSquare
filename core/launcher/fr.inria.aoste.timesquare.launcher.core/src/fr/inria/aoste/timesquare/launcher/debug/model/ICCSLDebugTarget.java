/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import org.eclipse.debug.core.IBreakpointListener;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IDisconnect;
import org.eclipse.debug.core.model.IMemoryBlockRetrieval;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.ISuspendResume;
import org.eclipse.debug.core.model.ITerminate;

/**
 * A CCSL debug target is a debuggable execution context for CSSL. 
 * 
 * Clients may implement this interface.
 * </p>
 * @see IDebugTarget
 * @see ITerminate
 * @see ISuspendResume
 * @see IBreakpointListener
 * @see IDisconnect
 * @see IMemoryBlockRetrieval
 * @see org.eclipse.debug.core.ILaunch
 */
public interface ICCSLDebugTarget extends IDebugTarget {

	public abstract IStackFrame[] getStackFrames();

	public abstract Thread getRun();

	/**
	 * Returns the <b>CCSL Process</b> associated with this debug target.
	 * 
	 * @return the <b>CCSL Process</b> associated with this debug target
	 */
	public abstract CCSLProcess getProcess();
	
}