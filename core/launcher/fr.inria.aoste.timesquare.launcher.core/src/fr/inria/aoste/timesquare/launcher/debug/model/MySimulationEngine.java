/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchListener;
import org.eclipse.debug.core.model.IDisconnect;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;

import fr.inria.aoste.timesquare.ccslkernel.solver.CCSLKernelSolver;
import fr.inria.aoste.timesquare.launcher.core.OutputManager;
import fr.inria.aoste.timesquare.launcher.core.PESolverManager;
import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.timesquare.launcher.core.errorhandler.ErrorHandler;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolver;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.core.inter.ITrace;
import fr.inria.aoste.timesquare.launcher.debug.model.output.OutputTraceList;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.CCSLSimulationConfigurationHelper;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.InitOutputData;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputTrace;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyManager;
import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.timesquare.trace.util.HelperPhysicalBase;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.utils.timedsystem.TimedSystem;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.PhysicalBase;
import fr.inria.aoste.trace.Trace;
import fr.inria.aoste.trace.TraceFactory;
import fr.inria.aoste.trace.relation.IRelation;

public class MySimulationEngine implements ISimulationInterface {
	
	
	static {
		DebugPlugin.getDefault().getLaunchManager().addLaunchListener(new CCSLProcessLaunchListener());
	}

	private static PESolverManager peSolverManager = PESolverManager.getDefault();

	public static final class CCSLProcessLaunchListener implements
			ILaunchListener {
		@Override
		public void launchRemoved(ILaunch launch) {
			//System.out.println(launch);
			if (launch.getProcesses()!=null)
			{
				//System.out.println( launch.getProcesses().length);
				for ( IProcess process:launch.getProcesses())
				{
					if (process  instanceof CCSLProcess)
					{						
							try {
								if (((IDisconnect) process).canDisconnect())
									((IDisconnect) process).disconnect();
							} catch (DebugException e) {								
								e.printStackTrace();
							}
					}
				}
			}
			System.gc();
		}

		@Override
		public void launchChanged(ILaunch launch) {
		
			
		}

		@Override
		public void launchAdded(ILaunch launch) {
			
			
		}
	}

	private class ThreadInitCCSLModel extends Thread {
		Throwable t = null;

		public ThreadInitCCSLModel() {
			super("Init Model for Simulator");
		}

		public final Throwable getException() {
			return t;
		}

		@Override
		public void run() {
			try {
				//System.out.println("Create Solver");
				peSolverManager.clearCache(0);
				commonSolver = ccslHelper.getSolver();
				outputlist = new OutputTraceList((ISolverForBackend) commonSolver);
				/**/
				//System.out.println("Solver loading :" + commonSolver);
				if (commonSolver == null) {
					t = peSolverManager.getThrowable();
				} else {
					//System.out.println("Solver starting :" + commonSolver);
					commonSolver.start();
					clocklist = commonSolver.getClockList();
				}
				//System.out.println("Solver running :" + commonSolver);

			} catch (Throwable e) {
				t = e;
				System.err.println("test");
				e.printStackTrace();
				commonSolver = null;
				// clocklist = null;
			}
		}

	}

	private class ThreadSelectViewStepPapyrus extends Thread {
		ThreadSelectViewStepPapyrus() {
			super("Viev Old Step");
		}

		@Override
		public void run() {
			super.run();
			if (viewStep >= aCCSL_Trace.getLogicalSteps().size())
				return;
			LogicalStep step = aCCSL_Trace.getLogicalSteps().get(viewStep);
			stepview = step;
			if (outputlist!=null)
			{
			outputlist.killTimer();
			outputlist.aStep(step, false);
			}

		}
	}

	private class ThreadSolveCCSLModel extends Thread {

		private Throwable exception = null;
		private Throwable killexception = null;

		public ThreadSolveCCSLModel() {
			super("Solve  CCSLModel [step  " + pos + " ] ");
			setDaemon(true);
			setPriority(MAX_PRIORITY);
		}

		public final Throwable getException() {
			return exception;
		}

		@Override
		public void run() {
			if (finish)
				return;
			step = null;
			try {

				final TimedSystem ts = new TimedSystem();
				step = AccessController.doPrivileged(new PrivilegedExceptionAction<LogicalStep>() {
					public LogicalStep run() throws Exception {
						return (commonSolver.solveNextSimulationStep(ts));// checkstep
					}
				});
				if (commonSolver.getException() != null) {
					throw commonSolver.getException();
				}
				if (step != null) {
					step.setStepNumber(pos);
				}else{
					throw new NullPointerException("You experienced a deadlock in your specification...");
				}
				
				for(fr.inria.aoste.trace.AssertionState as : step.getAssertionStates()){
					if (as.isIsViolated()){
						String feedback="violation of :";
						for (EObject eo : ((ModelElementReference)as.getReferedElement()).getElementRef()) {
							//reflexion to get the name of the eobject
							EClass ec= eo.eClass();
							EStructuralFeature  nameFeature= null;
							nameFeature=ec.getEStructuralFeature("name");
							Object name=null;
							if (nameFeature!=null){
								name=eo.eGet(nameFeature);
								if (name instanceof String ) {
									feedback+=name.toString()+"::";
								}
							}
						}
						cs.printErrMessageln(feedback);
					}
				}
				
				ts.finish();
				if (showtime) {
					cs.printSimulationMessageln("Timing :");
					cs.printStdMessageln(ts.toStringEvent());
				}
			} catch (Throwable e) {
				// commonSolver.clearFlag();
				System.err.println("Exception step :" + e);
				exception = e;
				step = null;
				return;
			} finally {
				commonSolver.endSimulationStep();
			}
			if (step == null) {
				exception = commonSolver.getException();
			}

		}

		protected final Throwable getKillexception() {
			return killexception;
		}

		protected final void setKillexception(Throwable killexception) {
			this.killexception = killexception;
		}

	}

	private ErrorHandler errorHandler = new ErrorHandler() {	};
	
	private Trace aCCSL_Trace;
	private boolean showtime = false;
	private CCSLSimulationConfigurationHelper ccslHelper = null;
	private HashMap<EObject, PhysicalBase> clkdiscretize = new HashMap<EObject, PhysicalBase>();
	private List<ModelElementReference> clocklist = null;
	private ISolver commonSolver;
	private ConsoleSimulation cs = null;
	private boolean finish = false;
	private IPath local = null;
	private IPath local2 = null;
	private int maxStep;
	private String namefile = null;
	private LogicalStep oldstep;
	private OutputTraceList outputlist =null;
	private int pos = 0;
	private IRelation relationModelSolver;
	private Resource resource;
	private ThreadSolveCCSLModel runsolve = null;
	private StateEngine stateEngine = StateEngine.init;
	private LogicalStep step;
	private LogicalStep stepview;
	private volatile boolean stopstep = false;
	private Throwable unthrow = null;
	private int viewStep = 0;
	private ResourceSet resourceSet = null;
	private List<PhysicalBase> lstdst = null;
	private HashMap<Resource, URI> hrs = new HashMap<Resource, URI>();
	private ArrayList<HelperPhysicalBase> listPhysicalBases = new ArrayList<HelperPhysicalBase>();
	private boolean valide = false;

	public MySimulationEngine(CCSLSimulationConfigurationHelper _ccslHelper) {
		super();
		ccslHelper = _ccslHelper;
		maxStep = ccslHelper.getStepNbr();
		cs = ccslHelper.getConsole();
		showtime = ccslHelper.getTime();
		if (maxStep <= 0)
			maxStep = peSolverManager.getDefaultStep(ccslHelper);
	}

	public Trace getTrace() {
		return aCCSL_Trace;
	}

	public void setErrorHandler(ErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}

	public int getCurrentStepIndice() {
		return this.pos;
	}

	public String getSourceFile() throws CoreException {
		return ccslHelper.getSource();
	}
	
	public String getPrioritySourceFile() throws CoreException {
		return ccslHelper.getPrioritySource();
	}

	public StateEngine getStateEngine() {
		return stateEngine;
	}

	public LogicalStep getStep() {
		return step;
	}

	public LogicalStep getStepview() {
		return stepview;
	}

	public int getViewStepIndice() {
		return viewStep;
	}

	public void failedInit() throws CoreException {
		Throwable t = peSolverManager.getThrowable();
		if (t == null) {
			t = unthrow;
		}
		stateEngine = StateEngine.errorInitModel;
		IStatus status = errorHandler.createStatusError("Error init " + ccslHelper.getSource(), t, true);
		try {
			errorHandler.notifyError("Error init :" + ccslHelper.getSource(), "Exception  :" + t, status);
		} catch (Throwable e) {
		}
		throw new CoreException(status);
	}

	public boolean isAbortException(Throwable th) {
		try {
			if (th != null) {
				if (th instanceof AbortException)
					return true;

				return isAbortException(th.getCause());
			}
		} catch (Throwable e) {

		}
		return false;
	}

	public boolean isTerminated() {
		if (stateEngine.isTerminate() || finish) {
			return true;
		}
		return false;
	}

	public void stopstep() {
		stopstep = true;
	}

	public int viewStep(int n) {
		Thread t;
		try {
			viewStep = n;
			if (stateEngine == StateEngine.terminate)
				stateEngine = StateEngine.ter_back;
			t = new ThreadSelectViewStepPapyrus();
			t.start();
			t.join();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void init() throws CoreException {
		peSolverManager.clearCache(1);
		if (cs == null)
			cs = ccslHelper.getConsole();
		cs.printSimulationMessageln("Initialisation....");
		valide = false;
		try {
			_init_Validation();
			_init_Relationmodel();
			_init_Workspace();
			_init_Model();
			if (commonSolver == null) {
				failedInit(); // throws
			}

			_init_Trace();
			_init_Policy();
			_init_Discretize();
			_init_Output();
		} catch (Throwable t) {
			throw _init_Throwable2CoreException(t);
		}
		stateEngine = StateEngine.pause;

	}

	private CoreException _init_Throwable2CoreException(Throwable t) throws CoreException {
		t.printStackTrace();
		if (commonSolver != null)
			commonSolver.endSimulation();
		IStatus status = null;
		if (valide) {
			status = errorHandler.createStatusError("Error init ( Validation Phase )", t.getCause(), true); // false);
		} else {
			status = errorHandler.createStatusError("Error init ", t, true);// false);
		}
		if (!stateEngine.isError()) {
			stateEngine = StateEngine.errorInitModel;
		}
		try {
			errorHandler.notifyError("Error init", t.getMessage(), status);
		} catch (Throwable e) {

		}
		return new CoreException(status);
	}

	private void _init_Validation() throws Exception {
		cs.printSimulationMessageln("Validation Model....");
		boolean b = peSolverManager.validate(ccslHelper);
		if (b == false) {
			cs.printErrMessageln("Failed !!");
			Throwable th = peSolverManager.getThrowable();
			valide = true;
			cs.printErrMessageln(th.getMessage());
			throw new Exception(" Error In validation phase  ", th);
		}
	}

	private void _init_Relationmodel() {
		relationModelSolver = (ccslHelper).getRelation();
		if (relationModelSolver != null) {
			relationModelSolver.setIOutputTraceList(outputlist);
			cs.printSimulationMessageln("Relation model is created");
		} else {
			cs.printErrMessageln("Erreur Relation Model (Not Found)");

		}
	}

	private void _init_Workspace() throws Exception {
		IFile ccslFile = ccslHelper.get_SourceIFile();
		/* le folder du fichier source */
		IPath folder = ((ccslFile).getParent()).getLocation();
		IPath tmp = PluginHelpers.createdir(folder, "trace");
		String foldername = "Simulation" + ccslHelper.getDatestr();
		local = PluginHelpers.createdir(tmp, foldername);
		((ccslFile).getParent()).refreshLocal(IResource.DEPTH_INFINITE, null);
		local2 = ((IContainer) ((ccslFile).getParent()).findMember("trace")).findMember(foldername).getFullPath();
		namefile = ccslHelper.get_NameFile(true);
	}

	private void _init_Policy() {
		cs.printSimulationMessage("Policy  : ");
		int n = ccslHelper.getPolicyId();
		if (n < 0) {
			throw new RuntimeException("Unknown Simulation Policy :\n");
		}
		cs.printStdMessageln(SimulationPolicyManager.getDefault().getName(n));
	}

	private void _init_Discretize() {
		System.out.print("Discretize Checking:\n");
		cs.printSimulationMessageln("Discretize Checking:");
		int n = 0;
		for (ModelElementReference mer : clocklist) {

			String name = AdapterRegistry.getAdapter(mer).getReferenceName(mer);
			EObject eo = HelperFactory.getLastReference(mer);
			boolean b = AdapterRegistry.getAdapter(eo).isDiscrete(eo);
			if (b) {
				cs.printStdMessageln(name + " is  discretize  ");
				HashMap<EObject, EObject> map = AdapterRegistry.getDiscrete(mer);
				PhysicalBase pb = HelperFactory.createPhysicalBase(mer);
				aCCSL_Trace.getPhysicalBases().add(pb);
				listPhysicalBases.add(new HelperPhysicalBase(pb));
				clkdiscretize.put(mer, pb);
				for (java.util.Map.Entry<EObject, EObject> e : map.entrySet()) {
					cs.printStdMessageln("\t\t" + AdapterRegistry.getAdapter(e.getKey()).getReferenceName(e.getKey()) + " / "
							+ AdapterRegistry.getAdapter(e.getValue()).getReferenceName(e.getValue()));
				}
				n++;
			} else {
				// cs.getConsolestd().println(name + " is not discretize  ");
			}

		}
		if (n == 0) {
			cs.printStdMessageln("\t\t0 discretized clock found  ");
		}
		lstdst = Collections.unmodifiableList(aCCSL_Trace.getPhysicalBases());
		cs.printSimulationMessageln("Discretize Checking End:");

	}

	private void _init_Model() throws Exception {
		cs.printSimulationMessageln("Solver....");
		ThreadInitCCSLModel d = null;
		commonSolver = null;
		d = new ThreadInitCCSLModel();
		d.start();
		d.join();
		unthrow = d.getException();

	}

	private void _init_Output() throws Throwable {
		outputlist.setCs(cs);
		OutputManager outputManager = OutputManager.getDefault();
		for (String key : outputManager.getListkey()) {

			IOutputOption ioo = ccslHelper.getTable(key);
			if (ioo != null) {

				if (ioo.isActivable() && ioo.isActive()) {
					String errorMessage = ioo.validate();
					if (errorMessage == null) 
					{
						cs.printSimulationMessageln(ioo.getName() + " :  active ");
						IOutputTrace iot = outputManager.getTraceforKey(key);
						if (iot != null) {
							iot.setOption(ioo);
							outputlist.add(iot);
						}
					} else {
						cs.printErrMessageln(ioo.getName() + " : bad configuration ( deactive ) :" + errorMessage);
					}
				} else {
					cs.printSimulationMessageln(ioo.getName() + " :  inactive ");
				}
			}
		}
		outputlist.init(new InitOutputData(local, namefile, true, lstdst));
	}

	private void _init_Trace() {
		aCCSL_Trace = TraceFactory.eINSTANCE.createTrace();
		aCCSL_Trace.setName(namefile);
		commonSolver.setListReference(aCCSL_Trace.getReferences());
		cs.printSimulationMessageln("Trace....");
		String nametrace = local2.append(namefile + ITrace.traceextension).toOSString();
		resourceSet = commonSolver.getResourceSet();
		if (resourceSet == null)
			resourceSet = new ResourceSetImpl();

		List<Resource> lr = new ArrayList<Resource>();
		if (ccslHelper.getCopy()) {
			for (Resource r : resourceSet.getResources()) {
				URI uri = r.getURI();
				if (uri.isPlatformResource()) {
					lr.add(0, r);
					List<String> s = uri.segmentsList();
					String tmpfile = local2.append(s.get(s.size() - 1)).toOSString();
					URI tmpuri = URI.createPlatformResourceURI(tmpfile, false);
					r.setURI(tmpuri);
					hrs.put(r, tmpuri);
					r.setModified(true);
					r.setTrackingModification(true);
					}
			}
		}
		XMLResourceFactoryImpl xmiresource = new XMLResourceFactoryImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, xmiresource);
		URI fileURI = URI.createPlatformResourceURI(nametrace, false);
		resource = resourceSet.createResource(fileURI);
		resource.setTrackingModification(true);
		// if (aCCSL_Trace != null)
		{
			resource.getContents().add(aCCSL_Trace);
		}
		try {
			//System.out.println(resource);
			resource.save(PluginHelpers.getEcoreSaveOption());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		for (Resource r : lr) {
			try {
				//System.out.println("--->" + r);
				r.save(PluginHelpers.getEcoreSaveOption());
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		try {
			((ccslHelper.get_SourceIFile()).getParent()).refreshLocal(IResource.DEPTH_ONE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}

	}

	public void step(boolean timed) throws Exception {
		_oneStep(timed);
		updateStatus();
		System.out.flush();

	}

	public void step(int nb) throws Exception {

		stopstep = false;
		for (int i = 0; (i < nb) && (viewStep + 1 < maxStep); i++) {
			// --->
			// viewStep
			// ?
			if (stopstep) {

				break;
			}
			if (stateEngine.isTerminate())
				break;
			// if (simulabort) { break; }
			_oneStep(true);
		}
		stopstep = false;
		updateStatus();
		System.out.flush();

	}

	public void steprun() throws Exception {
		stopstep = false;
		while (true) {
			if (stopstep) {

				break;
			}
			if (stateEngine.isTerminate())
				break;
			// if (simulabort) { break; }
			_oneStep(true);
		}
		stopstep = false;
		updateStatus();
		System.out.flush();
	}

	protected Throwable throwablerelation = null;

	protected void _oneStep(boolean timed) throws Exception {

		_oneStep_StateEngineIn();

		// Replay
		if ((pos != 0) && (pos > viewStep + 1)) {
			_oneStep_Replay();
			return;
		}
		Throwable th = null;
		try {
			viewStep = pos;
			if (commonSolver != null) {
				cs.printStepSimulation(pos);
				if (stopstep)
					return;

				runsolve = new ThreadSolveCCSLModel();
				runsolve.start();
				runsolve.join();

				th = runsolve.getException();
				if (th == null)
					th = runsolve.getKillexception();
				runsolve = null;
			}
			if (isAbortException(th)) {
				stateEngine = StateEngine.terminate;
				return;
			}
		} catch (Throwable e) {
			th = e;
		}

		if (step == null) {
			_oneStep_registryError(th);
			return;
		}

		_oneStep_updateTrace();
		try {
			throwablerelation = null;
			/*** Step for OutPut ***/

			_oneStep_step(timed);

			if (throwablerelation != null) {
				errorHandler.createStatusError(throwablerelation.getMessage(), throwablerelation, true);
				throw throwablerelation;
			}
		} catch (Throwable e) {
			_oneStep_registryError(e);
			return;
		}
		stepview = step;
		pos++;
		if (pos >= maxStep)
			stateEngine = StateEngine.terminate;

	}

	private void _oneStep_step(boolean timed) {
		/*** Step for OutPut ***/
		try {
			if (relationModelSolver != null)
				relationModelSolver.resolve(step);
		} catch (Throwable e) {

			cs.printErrMessage("****************\nErreur Relation Model\n*****************\n");
			throwablerelation = e;
		}
		outputlist.aNewStep(step, timed);
		outputlist.clearRelation();
	}

	private void _oneStep_updateTrace() {
		/** Add Step on Trace */
		aCCSL_Trace.getLogicalSteps().add(step);
		for (HelperPhysicalBase pb : listPhysicalBases) {
			pb.createPhysicalSteps(step);
		}
		if (oldstep != null)
			oldstep.setNextStep(step);
		step.setPreviousStep(oldstep);
		oldstep = step;
		stepview = step;
	}

	private void _oneStep_StateEngineIn() {
		if (stateEngine == StateEngine.ter_back)
			stateEngine = StateEngine.ter_replay;
		else
			stateEngine = StateEngine.run;
	}

	private void _oneStep_Replay() {
	//	System.out.println("----> replay " + viewStep);
		cs.printStep(viewStep, true);// pos
		viewStep(viewStep + 1);
		if (stateEngine == StateEngine.ter_replay) {
			if (pos == viewStep + 1) {
				stateEngine = StateEngine.terminate;
			} else {
				stateEngine = StateEngine.ter_back;
			}
		}
	}

	private void _oneStep_registryError(Throwable th) throws Exception {
		if (th == null) {
			th = new Exception("Unknow Error : Error not specified (step" + pos + ")");
		} else {
			th.printStackTrace();
		}
		// message= th.getMessage();
		IStatus status = errorHandler.createStatusError(th.getMessage(), th, true);

		stateEngine = StateEngine.error;
		String message = "in step " + pos + " : \n" + th.getMessage();
		pos++;
		errorHandler.notifyError("Error during simulation", message, status);

		return;
	}

	private void updateStatus() {
		if (stateEngine == StateEngine.run) {
			if (pos == maxStep) {
				stateEngine = StateEngine.terminate;
			} else {
				stateEngine = StateEngine.pause;
			}
		}
	}

	public void finish() {
		stopstep = true;
		//System.out.println("Call Finish");
		if (finish)
			return;
		finish = true;
		try {
			if (runsolve != null) {
				try {
					runsolve.setKillexception(new AbortException());
					runsolve.interrupt();
				} catch (Throwable e) {
					e.printStackTrace();
				}
				// runsolve.join();
			}
			if (commonSolver != null) {
				commonSolver.endSimulation();
			}
			System.out.println();
			writeTrace();

			try {
				ccslHelper.get_SourceIFile().getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
			} catch (Throwable e) {

			}
			try {
				if (relationModelSolver != null)
					relationModelSolver.saveRelationModel(resourceSet, local2, namefile);
			}
			catch (Throwable e) {
				
			}		

			outputlist.aFinalStep(pos);
			cs.printSimulationMessageln("<<Simulation: finish>>");
			if (relationModelSolver != null)
				relationModelSolver.unload();
		} catch (Throwable t) {
			//
			System.out.println(t);
			t.printStackTrace();
			//
		}	
		System.out.println("\nNormal ending");

	}
	
	public void disconnect()
	{
		finish();
		outputlist=null;
		relationModelSolver=null;
		ccslHelper=null;
		
	}

	private void writeTrace() throws Exception {
		/*if (resourceSet != null) {
			for (Resource r : resourceSet.getResources()) {
			//	System.out.println("Resource r :" + r);
			}
		}*/
		if (resource != null) {
			try {
				resource.save(PluginHelpers.getEcoreSaveOption()); // Collections.EMPTY_MAP);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

	}
}
