/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import java.util.HashMap;

import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IDisconnect;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamsProxy;

import fr.inria.aoste.timesquare.launcher.core.inter.ICCSLProxy;

final public class CCSLProcess extends PlatformObject implements IProcess ,IDisconnect {
	
	private ILaunch launch;
	//protected 
	private boolean isTerminated = false;
	private ThreadInterface currentCCSLThread;
	private HashMap<String, String> tableattribut = new HashMap<String, String>();
	
	public CCSLProcess(ILaunch launch,ICCSLProxy helper) {
		super();
		this.launch = launch;
		launch.addProcess(this);
		setAttribute(IProcess.ATTR_CMDLINE, "run  " + helper.get_SourceIFile());
	}

	public Thread addThread(ThreadInterface thread) {

		currentCCSLThread = thread;
		try {
			Thread myThread = new Thread(currentCCSLThread, "CCSLProcess");
			myThread.start();
			return myThread;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getAttribute(String key) {
		return tableattribut.get(key);
	}

	public int getExitValue() throws DebugException {
		if (currentCCSLThread != null)
			return currentCCSLThread.getExitValue();
		return 0;
	}

	public String getLabel() {
		return "CCSL simulation process ";// "(42)"
	}

	// IStreamsProxy isp = null;

	public IStreamsProxy getStreamsProxy() {
		/*if (isp == null) {

		}*/
		return null; // isp;
	}

	public void setAttribute(String key, String value) {
		tableattribut.put(key, value);
	}

	public boolean canTerminate() {
		return !isTerminated();
	}

	public boolean isTerminated() {
		if (currentCCSLThread != null)
			return isTerminated || currentCCSLThread.isTerminated() ;
		return isTerminated;
		//return false;
	}

	public void terminate() throws DebugException {
		System.out.println("terminate " + currentCCSLThread);
		if (!isTerminated()) {
			if (currentCCSLThread != null) {
				currentCCSLThread.terminate();

			}
		}
		isTerminated = true;
	//	fireEvent(new DebugEvent(this, DebugEvent.TERMINATE));
		if (currentCCSLThread != null) {
			//fireEvent(new DebugEvent(currentCCSLThread, DebugEvent.TERMINATE));
		}	
	}

	public ILaunch getLaunch() {
		return this.launch;
	}

	protected void fireEvent(DebugEvent event) {
		DebugPlugin.getDefault().fireDebugEventSet(new DebugEvent[] { event });
	}

	@Override
	public boolean canDisconnect() {
		
		return true;
	}

	boolean disconnected=false;
	@Override
	public void disconnect() throws DebugException {
		disconnected=true;
		currentCCSLThread.getSim().disconnect();
		DebugPlugin.getDefault().getLaunchManager().removeLaunch(launch);
		if (launch!=null)
		{
		launch.removeProcess(this);
		for ( IProcess p:launch.getProcesses())
		{
			launch.removeProcess(p);
		}
		}
		launch=null;	
	}

	@Override
	public boolean isDisconnected() {		
		return disconnected;
	}

}
