/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.proxy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchDelegate;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.Launch;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Display;

import fr.inria.aoste.timesquare.launcher.core.OutputManager;
import fr.inria.aoste.timesquare.launcher.core.PESolverManager;
import fr.inria.aoste.timesquare.launcher.core.PropertySimulation;
import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.timesquare.launcher.core.errorhandler.ErrorHandler;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.core.inter.ICCSLProxy;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolver;
import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLConstants;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;
import fr.inria.aoste.timesquare.simulationpolicy.OptionPolitic;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyManager;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.relation.IDescription;
import fr.inria.aoste.trace.relation.IRelation;

/**
 * This class is both a collection of helper functions intended to be used during the launch
 * and the execution of CCSL simulations, and a cache of various information about the model
 * currently being executed.
 * 
 * Instances of {@link CCSLSimulationConfigurationHelper} are uniquely associated to an instance of
 * {@link ILaunchConfiguration}. The caching functionality of this class is based on this
 * association: it tries to avoid recreating an object if the same {@link ILaunchConfiguration}
 * was used at the previous object creation. The cache is actually a very reduced one: it only
 * retains the last created instance of {@link CCSLSimulationConfigurationHelper}.
 * 
 * The "helper" functionality of this class is based on the availability of a solver for the
 * actual model being simulated. This solver is obtained using the extension point managed by
 * an instance of {@link PESolverManager}. From this object, various information about the model
 * are computed (clocks, relations, etc.) and locally cached in the {@link CCSLSimulationConfigurationHelper}
 * object.
 * 
 * @author Benoit Ferrero
 */
public class CCSLSimulationConfigurationHelper implements ICCSLProxy {

	static final private OutputManager outputManager = OutputManager.getDefault();
	static final private PESolverManager peSolverManager = PESolverManager.getDefault();
	static final private DateFormat dateFormat = new SimpleDateFormat("yyyy_MMdd_HHmmss");

	static private CCSLSimulationConfigurationHelper cache = null;
	public synchronized static void clearCache() {
		cache = null;
	}

	/**
	 * Returns an instance of {@link CCSLSimulationConfigurationHelper} initialized with data from the
	 * {@link ILaunchConfiguration} argument.
	 * If the previously created {@link CCSLSimulationConfigurationHelper} was created for the same LaunchConfiguration
	 * object, it is returned and no other object is created.
	 * @param configin
	 * @return
	 */
	public synchronized static CCSLSimulationConfigurationHelper create(ILaunchConfiguration configin) {
		if (cache != null && cache.config == configin) {
			// System.out.println("*************** Cache  Configuration ***************");
			cache.getSolver();
			return cache;
		}

		if (cache != null && configin instanceof ILaunchConfigurationWorkingCopy) {
			// System.out.println("=============== Update Cache Configuration ===============");
			ILaunchConfiguration ilc = ((ILaunchConfigurationWorkingCopy) configin).getOriginal();
			if (cache.config == ilc) {
				System.out.println(ilc);
				cache._config = (ILaunchConfigurationWorkingCopy) configin;
				cache.config = configin;
				return cache;
			}
		}
		// System.out.println("=============== Create Configuration ===============");
		cache = new CCSLSimulationConfigurationHelper(configin);
		return cache;
	}

	public static CCSLSimulationConfigurationHelper createlaunch(IFile file) throws CoreException {
		clearCache();
		if (file == null) {
			throw new NullPointerException("file is null ");
		}
		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType lcType = manager
				.getLaunchConfigurationType("fr.inria.aoste.timesquare.launcher.core.launchConfigurationType");
		ILaunchConfiguration conf = lcType.newInstance(file.getParent(), "default");
		ILaunchConfigurationWorkingCopy workingCopy = conf.getWorkingCopy();
		CCSLSimulationConfigurationHelper helper = CCSLSimulationConfigurationHelper.create(workingCopy);
		helper.setSource(file);
		helper.setSaveconf(false);
		if ( !helper.haveSolver()) {
			throw new RuntimeException("Solver init fail", PESolverManager.getDefault().getThrowable());
		}
		return helper;
	}

	public static String getDateTime() {
		synchronized (dateFormat) {
			return dateFormat.format(new Date());
		}
	}

	private CCSLSimulationConfigurationHelper(ILaunchConfiguration configin) {
		super();
		assert(configin != null);
		this.config = configin;
		datestr = getDateTime();

		if (configin instanceof ILaunchConfigurationWorkingCopy)
			this._config = (ILaunchConfigurationWorkingCopy) configin;
		else
			this._config = null;
		cachedtable = new HashMap<String, IOutputOption>();
		init();
	}

	private ILaunchConfigurationWorkingCopy _config;

	private ILaunchConfiguration config;

	private final String datestr;

	private ConsoleSimulation console;

	private List<ModelElementReference> listofassert = null;

	private List<ModelElementReference> listofclock = null;

	private List<ModelElementReference> listofconstraint = null;

	private List<IDescription> listofrelation = null;

	private List<Resource> listressource = new ArrayList<Resource>();

	private List<EObject> listReferencedObject = new ArrayList<EObject>();

	private ISolver isolver = null;

	private ErrorHandler errorHandler = null;

	private long stamp = 0;

	private HashMap<String, IOutputOption> cachedtable;

	private IRelation relation = null;

	private List<IDescription> getRelationDescription() {
		List<IDescription> lst = new ArrayList<IDescription>();
		try {
			EObject eo = isolver.getResourceSet().getResources().get(0).getContents().get(0);
			relation = PluginHelpers.getNewInstance("fr.inria.aoste.timesquare.instantrelation",
					"fr.inria.aoste.timesquare.instantrelation.generator.InstantRelationModelGenerator",
					IRelation.CLASS);
			if (relation == null)
				return lst; // relation ;
			relation.setListClock(listofclock);
			relation.setListRelation(listofconstraint);
			relation.extract(eo);
			lst = relation.getDescription();
		} catch (Throwable e) {
			System.err.println(e);
		}
		return lst;
	}

	Map<URI, Resource> mappingURI = null;

	/**
	 * Initialize a {@link CCSLSimulationConfigurationHelper} object.
	 * First a {@link ISolver} object is created using {@link PESolverManager#createSolver(ICCSLProxy)}, and
	 * this solver is supposed to load the model from the file specified in the Launch Configuration object.
	 * Second, this solver object is used to collect various information about the model (clock list, all
	 * the relations, etc.).
	 */
	private void init() {
		relation = null;
		isolver = peSolverManager.createSolver(this);
		if (isolver == null) {
			stamp = -1;
			listofclock = Collections.unmodifiableList(new ArrayList<ModelElementReference>());
			listofassert = Collections.unmodifiableList(new ArrayList<ModelElementReference>());
			listReferencedObject = Collections.unmodifiableList(new ArrayList<EObject>());
			listofconstraint = Collections.unmodifiableList(new ArrayList<ModelElementReference>());
			listofrelation = Collections.unmodifiableList(new ArrayList<IDescription>());
			listressource = Collections.unmodifiableList(new ArrayList<Resource>());
			mappingURI = Collections.unmodifiableMap(new HashMap<URI, Resource>());
			return;
		}
		if (stamp == get_SourceIFile().getModificationStamp()) return;
		stamp = get_SourceIFile().getModificationStamp();
		listofclock = Collections.unmodifiableList(isolver.getClockList());
		listofassert = Collections.unmodifiableList(isolver.getAssertList());
		listofconstraint = Collections.unmodifiableList(isolver.getConstraint());
		listofrelation = Collections.unmodifiableList(getRelationDescription());
		/***** Collect all reference Object *****/
		listReferencedObject = new ArrayList<EObject>();
		for (ModelElementReference r : listofclock) {
			AdapterRegistry.getAdapter(r).fillWithReferencedElements(r, listReferencedObject);
		}
		listReferencedObject = Collections.unmodifiableList(listReferencedObject);
		/****** Collect Resource of all reference Object ***/
		listressource = new ArrayList<Resource>();
		for (EObject el : listReferencedObject) {
			Resource rs = el.eResource();
			if (rs != null)
				if (!listressource.contains(rs)) {
					listressource.add(rs);
				}
		}
		listressource = Collections.unmodifiableList(listressource);
		mappingURI = new HashMap<URI, Resource>();
		/********/
		for (Resource r : listressource) {
			System.out.println("URI :" + r.getURI());
			mappingURI.put(r.getURI(), r);
		}
		mappingURI = Collections.unmodifiableMap(mappingURI);
		/**********/
		for (IOutputOption ioo : cachedtable.values()) {
			try {
				if (ioo.isActivable())
					ioo.updateModel();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public IRelation getRelation() {
		return relation;
	}	

	@Override
	public boolean haveSolver() {		
		return getSolver()!=null;
	}

	/**
	 * 
	 * @return
	 */
	public ISolver getSolver() {
		if (get_SourceIFile() == null || get_SourceIFile().getModificationStamp() != stamp) {
			init();
		}
		return isolver;
	}

	@Override
	public List<EObject> getListReferencedObject() {
		return listReferencedObject;
	}
	@Override
	public final List<Resource> getRessourceOfReferencedObject() {
		return listressource;
	}
	@Override
	public final List<ModelElementReference> getListofClock() {
		return listofclock;
	}
	@Override
	public List<IDescription> getListofRelation() {
		return listofrelation;
	}
	@Override
	public final Map<URI, Resource> getMappingURIReferencedObject() {
		return mappingURI;
	}
	@Override
	public final List<ModelElementReference> getListofAssert() {
		return listofassert;
	}

	@Override
	public boolean haveClockWithReferenceTo(EClass element) {
		if (element != null) {
			for (EObject eo : listReferencedObject) {
				EClass eClass = eo.eClass();
				if (element.isSuperTypeOf(eClass))
					return true;
			}
		}
		return false;
	}
	@Override
	public final ConsoleSimulation getConsole() {
		return console;
	}

	public final void setConsole(ConsoleSimulation console) {
		this.console = console;
	}

	private boolean saveconf = true;

	private void setSaveconf(boolean saveconf) {
		this.saveconf = saveconf;
	}

	public void save() {
		try {
			if (saveconf && _config != null) {
				_config.doSave();
			}
		} catch (CoreException e) {

			e.printStackTrace();
		}
	}

	/********/

	public void setDefault() {
		setPolicyId(SimulationPolicyManager.getDefault().getIndexofUID(ICCSLConstants.DEFAULT_POLICY));
		setSource(null);
		setPrioritySource(null);
		setStepNbr(ICCSLConstants.DEFAULT_STEPS);
		setEstate(ICCSLConstants.DEFAULT_ESTATE);
		setTime(ICCSLConstants.DEFAULT_TIME);
	}

	public boolean solverUsedEstate() {
		if (get_SourceIFile() == null) {
			return true;
		}
		return PropertySimulation.EnableInactive.iscontaint(peSolverManager.getSupportPropertySimulation(this));
	}
	@Override
	public boolean getEstate() {
		if (!solverUsedEstate())
			return true;
		try {
			return config.getAttribute(ICCSLConstants.ESTATE, ICCSLConstants.DEFAULT_ESTATE);
		} catch (Throwable e) {

		}
		return ICCSLConstants.DEFAULT_ESTATE;
	}

	public void setEstate(boolean estate) {
		try {

			if (solverUsedEstate()) {
				if (_config != null) {
					if (getEstate() != estate) {
						incCCCSLIinfoEvolution();
						_config.setAttribute(ICCSLConstants.ESTATE, estate);
					}
				}
			}

		} catch (Throwable e) {

		}

	}

	@Override
	public boolean getTime() {
		try {
			return config.getAttribute(ICCSLConstants.TIME, ICCSLConstants.DEFAULT_TIME);
		} catch (Throwable e) {

		}
		return ICCSLConstants.DEFAULT_TIME;
	}

	public void setTime(boolean nbr) {
		try {
			if (_config != null)
				_config.setAttribute(ICCSLConstants.TIME, nbr);
		} catch (Throwable e) {

		}
	}

	public void setCopy(boolean cpy) {
		try {
			if (_config != null) {
				if (getCopy() != cpy) {
					incCCCSLIinfoEvolution();
					_config.setAttribute(ICCSLConstants.CPT, cpy);
				}
			}
		} catch (Throwable e) {

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.inria.aoste.timesquare.launcher.debug.model.proxy.ICCSLProxy#getStepNbr
	 * ()
	 */
	@Override
	public int getStepNbr() {
		try {
			return config.getAttribute(ICCSLConstants.STEPS, ICCSLConstants.DEFAULT_STEPS);
		} catch (Throwable e) {

		}
		return ICCSLConstants.DEFAULT_STEPS;
	}

	public void setStepNbr(int nbr) {
		try {
			if (_config != null) {
				if (getStepNbr() != nbr) {
					incCCCSLIinfoEvolution();
					_config.setAttribute(ICCSLConstants.STEPS, nbr);
				}
			}
		} catch (Throwable e) {

		}

	}

	@Override
	public OptionPolitic getOptionPolicy() {
		OptionPolitic op = new OptionPolitic(getEstate(), getPolicyId());
		if (console != null)
			op.setSysout(console.getConsolestd());
		return op;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.inria.aoste.timesquare.launcher.debug.model.proxy.ICCSLProxy#getPolicyId
	 * ()
	 */
	@Override
	public int getPolicyId() {
		String s = ICCSLConstants.DEFAULT_POLICY;
		try {
			s = config.getAttribute(ICCSLConstants.POLICY, ICCSLConstants.DEFAULT_POLICY);
		} catch (Throwable e) {

		}
		return SimulationPolicyManager.getDefault().getIndexofUID(s);
	}


	public void setPolicyId(int nbr) {
		try {
			if (_config != null)
				_config.setAttribute(ICCSLConstants.POLICY, SimulationPolicyManager.getDefault().getUID(nbr));
		} catch (Throwable e) {

		}

	}

	@Override
	public String getSource() {
		try {
			return config.getAttribute(ICCSLConstants.SOURCE, "");
		} catch (Throwable e) {

		}
		return "";
	}
	@Override
	public IFile get_SourceIFile() {
		if (sourceFile != null) return sourceFile;
		try {
			String name = config.getAttribute(ICCSLConstants.SOURCE, (String)null);
			if (name == null || name.trim().length()==0)
				return null;

			/* recupere Ifile ccslFile qui reference le fichier source */
			Path p = new Path(name);
			IWorkspaceRoot iWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			IResource r = iWorkspaceRoot.findMember(p);
			if (r instanceof IFile) {
				sourceFile = (IFile)r;
				return sourceFile;
			}
			return null;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getPrioritySource() {
		try {
			return config.getAttribute(ICCSLConstants.PRIORITYSOURCE, "");
		} catch (Throwable e) {

		}
		return "";
	}

	@Override
	public IFile get_PrioritySourceIFile() {
		if (prioritySourceFile != null) return prioritySourceFile;
		try {
			String name = config.getAttribute(ICCSLConstants.PRIORITYSOURCE, (String)null);
			if (name == null || name.trim().length()==0)
				return null;

			/* recupere Ifile ccslFile qui reference le fichier source */
			Path p = new Path(name);
			IWorkspaceRoot iWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			IResource r = iWorkspaceRoot.findMember(p);
			if (r instanceof IFile) {
				prioritySourceFile = (IFile)r;
				return prioritySourceFile;
			}
			return null;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getExtension() {
		if (get_SourceIFile() == null) {
			return null;
		}
		return get_SourceIFile().getFileExtension();
	}
	@Override
	public String get_NameFile(boolean timed) {
		try {
			IFile ifilelocal = get_SourceIFile();
			if (ifilelocal == null)
				return null;
			String namefile = ifilelocal.getName();
			String ext = ifilelocal.getFileExtension();
			// non du fichier sans extension
			namefile = namefile.substring(0, namefile.length() - ext.length() - 1);
			// Date Courante

			if (timed)
				return namefile + datestr;
			return namefile;

		} catch (Throwable e) {

		}
		return null;
	}

	private IFile prioritySourceFile;
	public void setPrioritySource(IFile file) {
		try {
			this.prioritySourceFile = file;
			if (_config != null) {
				incCCCSLIinfoEvolution();
				_config.setAttribute(ICCSLConstants.PRIORITYSOURCE, file==null?"":file.getFullPath().toString());
				//init();
			}
		} catch (Throwable e) {
			System.err.println(e);
		}

	}

	private IFile sourceFile;
	public void setSource(IFile file) {
		try {
			this.sourceFile = file;
			if (_config != null) {
				incCCCSLIinfoEvolution();
				_config.setAttribute(ICCSLConstants.SOURCE, file==null?"":file.getFullPath().toString());
			}
		} catch (Throwable e) {
			System.err.println(e);
		}

	}

	public Configurator getConfigurator(Class<?> _class) {
		String key = outputManager.getkey(_class);
		if (key == null) {
			return null;
		}
		IOutputOption iot = getTable(key);
		if (iot != null)
			return iot.getConfigurator();
		return null;
	}

	@Override
	public IOutputOption getTable(String key) {
		if (key == null)
			return null;
		IOutputOption ioo = null;
		try {
			ioo = cachedtable.get(key);
			if (ioo != null) {
				// System.out.println("used cache");
				ioo.setCs(console);
				ioo.setCcslhelper(this);
				if (ioo.isActivable())
					ioo.updateModel();
				return ioo;
			}
			ioo = outputManager.getOptionforKey(key);
			if (ioo == null)
				return null;
			if (key.compareTo(ioo.getKey()) == 0) // la cle est bonne
			{
				ioo.setCs(console);
				@SuppressWarnings("unchecked")
				Map<String, String> table = config.getAttribute(key, new HashMap<String, String>());
				Map<String, String> hmcp = new HashMap<String, String>();
				for (Entry<String, String> es : table.entrySet()) {
					hmcp.put(es.getKey(), es.getValue());
				}
				ioo.setTable(hmcp);
				ioo.setCcslhelper(this.getDelegateICCSLProxy());
				if (ioo.isActivable()) {
					ioo.updateModel();
					ioo.restore();
				}
				cachedtable.put(key, ioo);
				return ioo;
			}
			return null;
		} catch (Throwable e) {

		}

		return ioo;
	}

	@Override
	public void writeTable(IOutputOption ioo) {

		try {
			Map<String, String> hmin = ioo.getTable();
			Map<String, String> hmcp = new HashMap<String, String>();
			for (Entry<String, String> es : hmin.entrySet()) {
				hmcp.put(es.getKey(), es.getValue());
			}
			setTable(ioo.getKey(), hmcp);

		} catch (Throwable e) {

		}

	}

	private void setTable(String key, Map<String, String> table) {
		try {
			if (_config != null) {
				// System.out.println("table is update");
				_config.setAttribute(key, table);
			}
		} catch (Throwable e) {

		}
	}


	@SuppressWarnings("unchecked")
	public Map<String, String> getOptionModel() {
		try {
			Map<String, String> table = config.getAttribute(ICCSLConstants.OPTIONMODEL, new HashMap<String, String>());
			return table;
		} catch (Throwable e) {

		}
		return null;
	}

	public void setOptionModel(HashMap<String, String> hmcp) {
		setTable(ICCSLConstants.OPTIONMODEL, hmcp);
	}

	public void clearOptionModel() {
		setTable(ICCSLConstants.OPTIONMODEL, null);
	}

	@Override
	public final String getDatestr() {
		return datestr;
	}


	public ISimulatorControl launch() throws CoreException, InterruptedException, Exception {
		String mode = "simulation";
		if (config == null) {
			throw new NullPointerException("config is null");
		}
		if (Display.getCurrent() != null) {
			throw new SWTException(SWT.ERROR_THREAD_INVALID_ACCESS, "Runnning on UI Thread");
		}
		@SuppressWarnings("unchecked")
		Set<String> modes = config.getModes();
		modes.add(mode);
		ILaunchDelegate[] delegates = config.getType().getDelegates(modes);
		ILaunchConfigurationDelegate delegate = null;
		delegate = delegates[0].getDelegate();
		ILaunch launch = null;
		launch = new Launch(config, mode, null);
		launch.setAttribute(DebugPlugin.ATTR_CAPTURE_OUTPUT, "false"); //$NON-NLS-1$	
		launch.setAttribute(DebugPlugin.ATTR_CONSOLE_ENCODING, (DebugPlugin.getDefault()).getLaunchManager()
				.getEncoding(config));
		if (delegate instanceof ICCSLModelLauncher) {
			CCSLSimulationConfigurationHelper.clearCache();
			ISimulatorControl iSimulatorControl = ((ICCSLModelLauncher) delegate).launch(this, mode, launch);
			iSimulatorControl.join();
			return iSimulatorControl;
		}
		return null;

	}
	@Override
	public boolean getCopy() {

		try {
			return config.getAttribute(ICCSLConstants.CPT, true);
		} catch (Throwable e) {

		}
		return true;

	}

	@Override
	public ErrorHandler getErrorHandler() {
		return errorHandler;
	}

	private final CCSLInfoDelegate delegate = new CCSLInfoDelegate(this);

	@Override
	public final CCSLInfo getDelegateCCSLInfo() {
		return delegate;
	}

	public final ICCSLProxy getDelegateICCSLProxy() {
		return this;
	}

	private void incCCCSLIinfoEvolution() {
		ccslinfoevolution++;
	}

	private int ccslinfoevolution = 0;

	@Override
	public int getCCSLInfoEvolution() {
		return ccslinfoevolution;
	}
}
