/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core;

public enum PropertySimulation {
	EnableInactive("enabled", 0, true, true);

	private PropertySimulation(String name, int ordinal, boolean in, boolean out) {
		this.name = name;
		this.ordinal = ordinal;
		this.in = in;
		this.out = out;
	}

	private String name;
	private int ordinal;

	public final String getName() {
		return name;
	}

	public final int getOrdinal() {
		return ordinal;
	}

	public final boolean isIn() {
		return in;
	}

	public final boolean isOut() {
		return out;
	}

	private boolean in, out;

	public boolean iscontaint(PropertySimulation pst[]) {
		if (pst==null)
		{
			return false;
		}
		for (PropertySimulation ps : pst) {
			if (ps == this) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean  isOutput(PropertySimulation pst[])
	{
		if (pst==null)
		{
			return true;
		}
		for (PropertySimulation ps : pst) {
			if (ps.out==false) {
				return false;
			}
		}
		return true ;
		
	}
	
	public static boolean  isInput(PropertySimulation pst[])
	{
		if (pst==null)
		{
			return true;
		}
		for (PropertySimulation ps : pst) {
			if (ps.in==false) {
				return false;
			}
		}
		return true ;
		
	}

}
