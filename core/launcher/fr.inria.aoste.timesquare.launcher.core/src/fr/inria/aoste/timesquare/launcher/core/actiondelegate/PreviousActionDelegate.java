/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.actiondelegate;

import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.ui.contexts.IDebugContextListener;
import org.eclipse.jface.action.IAction;
import org.eclipse.ui.IViewActionDelegate;

import fr.inria.aoste.timesquare.launcher.debug.model.CCSLDebugThread;
import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget;
import fr.inria.aoste.timesquare.launcher.debug.model.ISimulationInterface;

public class PreviousActionDelegate extends AbstractCCSLActionDelegate implements IViewActionDelegate,
		IDebugContextListener {

	public PreviousActionDelegate() {
		super();
	}

	public void runningAction(IAction action) {
		try {
			if (ccsldt != null) {
				ccsldt.getProcess();
				if ((ccsldt.getThreads() != null) && (ccsldt.getThreads().length != 0)) {

					ISimulationInterface isim = ((CCSLDebugThread) (ccsldt.getThreads()[0])).getSim();
					int vstep = isim.getViewStepIndice() - 1;
					if (vstep == 0)
						action.setEnabled(false);
					if (vstep >= 0)
						isim.viewStep(vstep);
					fireEvent(new DebugEvent(this, DebugEvent.STEP_OVER));
					((CCSLDebugThread) (ccsldt.getThreads()[0])).updateVar();
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void updateAction(IAction action, ISimulationInterface isim, ICCSLDebugTarget ccsldt2) {
		int vstep = isim.getViewStepIndice();
		if (vstep == 0)
			action.setEnabled(false);
		else
			action.setEnabled(true);
	}

}
