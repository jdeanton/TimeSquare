/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.actiondelegate;

import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.contexts.DebugContextEvent;
import org.eclipse.debug.ui.contexts.IDebugContextListener;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IViewActionDelegate;
import org.eclipse.ui.IViewPart;

import fr.inria.aoste.timesquare.launcher.debug.model.CCSLDebugTarget;
import fr.inria.aoste.timesquare.launcher.debug.model.CCSLDebugThread;
import fr.inria.aoste.timesquare.launcher.debug.model.CCSLStackFrame;
import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget;
import fr.inria.aoste.timesquare.launcher.debug.model.ISimulationInterface;

public abstract class AbstractCCSLActionDelegate implements IViewActionDelegate, IDebugContextListener {

	public AbstractCCSLActionDelegate() {
		super();
		DebugUITools.getDebugContextManager().addDebugContextListener(this);
	}

	public void debugContextChanged(DebugContextEvent event) {
	}

	public void init(IViewPart view) {

	}

	public void run(final IAction action) {
		try {
			Thread t= new Thread(getClass().getName()){

				@Override
				public void run() {
					runningAction(action );
				}};
			t.start();
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	protected abstract void runningAction(IAction action);
	


	protected ICCSLDebugTarget ccsldt;

	protected ICCSLDebugTarget getDebugTargetOnSelection(ISelection selection) {
		if (selection instanceof TreeSelection) {

			Object o = ((TreeSelection) selection).getFirstElement();

			if (o instanceof CCSLDebugTarget) {

				return ((CCSLDebugTarget) o);
			}
			if (o instanceof CCSLStackFrame) {

				return ((CCSLStackFrame) o).getDebugTarget();
			}
			if (o instanceof CCSLDebugThread) {

				return ((CCSLDebugThread) o).getDebugTarget();
			}
		}
		return null;
	}

	public void selectionChanged(IAction action, ISelection _selection) {
		try {
			ccsldt = getDebugTargetOnSelection(_selection);
			if (ccsldt != null) {
				ISimulationInterface isim = ((CCSLDebugThread) (ccsldt.getThreads()[0])).getSim();
				updateAction(action, isim,ccsldt);
			} else {
				action.setEnabled(false);
			}
			
		} catch (Throwable e) {
			action.setEnabled(false);
		}

	}

	abstract protected void updateAction(IAction action, ISimulationInterface isim, ICCSLDebugTarget ccsldt2) ;

	protected void fireEvent(DebugEvent event) {
		DebugPlugin.getDefault().fireDebugEventSet(new DebugEvent[] { event });
	}

}
