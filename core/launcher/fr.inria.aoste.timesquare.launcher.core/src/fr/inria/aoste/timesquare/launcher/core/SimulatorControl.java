/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core;

import fr.inria.aoste.timesquare.launcher.debug.model.ISimulationInterface;
import fr.inria.aoste.timesquare.launcher.debug.model.StateEngine;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.ISimulatorControl;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.Trace;

/**
 * 
 * @author bferrero
 * 
 */
public class SimulatorControl implements ISimulatorControl {

	public static abstract class ThreadwithExceptionNotification extends Thread {
		protected Throwable throwable = null;

		public ThreadwithExceptionNotification(String name) {
			super(name);
		}

		@Override
		public final void run() {
			try {
				runwithException();
			} catch (Throwable e) {
				throwable = e;
			}
		}

		abstract protected void runwithException() throws Throwable;

		public void reThrows() throws Exception {

			if (throwable instanceof Exception)
				throw (Exception) throwable;
			if (throwable instanceof Error)
				throw (Error) throwable;
		}

		public Throwable getThrowable() {
			return throwable;
		}

	}

	private ISimulationInterface isimulation;
	public SimulatorControl(Thread t, ISimulationInterface isimulation) {
		super();
		assert(isimulation != null);
		this.thread = t;
		this.isimulation = isimulation;
	}

	private Thread thread = null;

	@Override
	public Trace getTrace() {
		return isimulation.getTrace();
	}

	@Override
	public synchronized boolean isJoinable() {
		if (thread != null) {
			// t != null && t.isAlive();
			if (thread.isAlive()) {
				return true;
			}
			// ?
			thread = null;
			return false;
		}
		return false;
	}
	
	/*private void finish() {
		if (isimulation.isTerminated())
		{
			isimulation.finish();
		}
		
	}*/
	

	@Override
	public synchronized void join() throws InterruptedException, Exception {
		if (thread != null) {
			thread.join();
			if (thread instanceof ThreadwithExceptionNotification) {
				//finish();
				((ThreadwithExceptionNotification) thread).reThrows();
			}
		}
		thread = null;
	}

	
	
	@Override
	public synchronized void syncRunStep() throws Exception {
		if (thread == null) {
			isimulation.step(true);
			//finish();
		}
	}

	@Override
	public synchronized void syncRunStep(int nb) throws Exception {
		if (thread == null) {
			isimulation.step(nb);
			//finish();
		}
	}

	@Override
	public synchronized void syncRunAllStep() throws Exception {
		if (thread == null) {
			isimulation.steprun();
			//finish();
		}
	}

	@Override
	public synchronized void syncPreviousStep() throws Exception {
		if (thread == null) {
			int vstep = isimulation.getViewStepIndice() - 1;
			if (vstep >= 0)
				isimulation.viewStep(vstep);
		}
		
	}

	@Override
	public synchronized void asyncPreviousStep() throws Exception {
		if (thread == null) {
			thread = new ThreadwithExceptionNotification("Prog : Previous Step (Prog)") {

				@Override
				protected void runwithException() throws Throwable {
					int vstep = isimulation.getViewStepIndice() - 1;
					if (vstep >= 0)
						isimulation.viewStep(vstep);						
				}
			};
			thread.start();
		}
		
	}

	@Override
	public synchronized void asyncRunAllStep() throws Exception {
		if (thread == null) {
			thread = new ThreadwithExceptionNotification("Prog : Run All Step (Prog)") {

				@Override
				protected void runwithException() throws Throwable {
					isimulation.steprun();
				}
			};
			thread.start();
		}

	}

	@Override
	public synchronized void asyncRunStep() throws Exception {
		if (thread == null) {
			thread = new ThreadwithExceptionNotification("Prog : Run One Step(prog)") {

				@Override
				protected void runwithException() throws Throwable {
					isimulation.step(true);
				}
			};
			thread.start();
		}
	}

	@Override
	public synchronized void asyncRunStep(final int nb) throws Exception {
		if (thread == null) {
			thread = new ThreadwithExceptionNotification("Prog : Run  " + nb + " Step(s) )") {
				@Override
				protected void runwithException() throws Throwable {
					isimulation.step(nb);
				}
			};
			thread.start();
		}

	}

	@Override
	public synchronized LogicalStep getCurrentSolverStep() {
		if (thread == null) {
			return isimulation.getStep();
		}
		return null;
	}
	
	
	@Override
	public synchronized LogicalStep getStepView() {
		if (thread == null) {
			return isimulation.getStepview();
		}
		return null;
	}

	@Override
	public synchronized boolean isTerminated() {
		if (thread == null) {
			return isimulation.isTerminated();
		}
		return false;
	}

	@Override
	public synchronized void terminate() throws InterruptedException, Exception {
		try {
			join();
		} /*catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}*/
		finally {
			isimulation.finish();
			isimulation.disconnect();
		}
	}

	@Override
	public void pause() {
		isimulation.stopstep();
	}

	@Override
	public StateEngine getStateEngine() {
		return isimulation.getStateEngine();
	}
}