/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.console;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IOConsole;
import org.eclipse.ui.console.IOConsoleOutputStream;

import fr.inria.aoste.timesquare.launcher.debug.model.CCSLProcess;

public class ConsoleSimulation extends IOConsole implements org.eclipse.ui.console.IConsole {

	private CCSLProcess iProcess = null;

	public ConsoleSimulation(CCSLProcess process, String name) {
		super(name, null);
		iProcess = process;
		initandregister();
	}

	public ConsoleSimulation() {
		super("noconsole", null);

	}

	private IOConsoleOutputStream std = null, err = null, step = null;
	private InputStream input = null;

	private PrintStream consolestd, consoleerr, consolestep;

	public final PrintStream getConsolestd() {
		return consolestd;
	}

	public void printStdMessageln(String message) {
		try {
			if (consolestd != null)
				consolestd.println(message);
		} catch (Throwable e) {

		}
	}

	public void printStdMessage(String message) {
		try {
			if (consolestd != null)
				consolestd.print(message);
		} catch (Throwable e) {

		}
	}

	protected final PrintStream getConsoleerr() {
		return consoleerr;
	}

	public void printErrMessageln(String message) {
		try {
			if (consoleerr != null)
				consoleerr.println(message);
		} catch (Throwable e) {

		}
	}

	public void printErrMessageln(String message, Throwable _e) {
		try {
			if (consoleerr != null) {
				consoleerr.println(message);
				_e.printStackTrace(consoleerr);
			}
		} catch (Throwable e) {

		}
	}

	public void printErrMessage(String message) {
		try {
			if (consoleerr != null)
				consoleerr.print(message);
		} catch (Throwable e) {

		}
	}

	protected final OutputStream getStd() {
		return std;
	}

	protected final OutputStream getErr() {
		return err;
	}

	public final InputStream getInput() {
		return input;
	}

	private void initandregister() {
		IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();
		manager.addConsoles(new IConsole[] { this });
		manager.showConsoleView(this);

		std = newOutputStream();
		err = newOutputStream();
		step = newOutputStream();
		input = getInputStream();

		std.setActivateOnWrite(false);
		err.setActivateOnWrite(false);
		step.setActivateOnWrite(false);

		std.setColor(new Color(null, 0, 0, 0));
		err.setColor(new Color(null, 255, 0, 0));
		step.setColor(new Color(null, 0, 0, 255));

		consolestd = new PrintStream(std);
		consoleerr = new PrintStream(err);
		consolestep = new PrintStream(step);
		// iook.setColor(new Color(null, 0,0,255));
	}

	public void printStepSimulation(int n) {
		try {
			if (consolestep != null) {
				consolestep.println();
				consolestep.println("Simulation (Step " + n + ") ");
			}
			//System.out.println("Step " + n);
		} catch (Throwable e) {

		}
	}

	/**
	 * 
	 * @param n
	 * @param replay
	 */
	public void printStep(int n, boolean replay) {
		try {
			if (consolestep != null)
				consolestep.println("Display (Step " + n + ") ");
		} catch (Throwable e) {

		}
	}

	public void printSimulationMessageln(String message) {
		try {
			if (consolestep != null)
				consolestep.println(message);
		} catch (Throwable e) {

		}
	}

	public void printSimulationMessage(String message) {
		try {
			if (consolestep != null)
				consolestep.print(message);
		} catch (Throwable e) {

		}
	}

	protected final CCSLProcess getiProcess() {
		return iProcess;
	}

	@Override
	protected void dispose() 
	{
		super.dispose();		
	}

}
