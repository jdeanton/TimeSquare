/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model.proxy;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunch;

public interface ICCSLModelLauncher {

	public abstract ISimulatorControl launch(CCSLSimulationConfigurationHelper helper, String mode, ILaunch launch) throws CoreException,
			InterruptedException;

}