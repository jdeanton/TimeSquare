/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;

public abstract class CCSLVariable extends CCSLDebugElement implements IVariable {
	private String name;
	private String type;
	private IValue value = null;
	private boolean hasValueChanged = false;
	private boolean modifiable = false;

	protected void setModifiable(boolean modifiable) {
		this.modifiable = modifiable;
	}

	protected CCSLVariable(ICCSLDebugTarget target, String name, String type) {
		super(target);
		this.name = name;
		this.type = type;
	}

	final public String getName() {
		return this.name;
	}

	final public String getReferenceTypeName() {
		return type;
	}

	final public IValue getValue() {
		return this.value;
	}

	final public boolean hasValueChanged() {
		return this.hasValueChanged;
	}

	public void setValue(String expression) {
		this.hasValueChanged = true;
	}

	final public void setValue(IValue value) {
		this.hasValueChanged = true;
		this.value = value;
	}

	final public boolean supportsValueModification() {
		return modifiable;
	}

	final public boolean verifyValue(IValue value) {
		try {
			return value.getReferenceTypeName().equals(this.getReferenceTypeName());
		} catch (DebugException de) {
			return false;
		}
	}
}
