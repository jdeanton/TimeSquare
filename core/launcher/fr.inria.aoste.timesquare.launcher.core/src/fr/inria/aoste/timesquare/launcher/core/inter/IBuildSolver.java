/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.core.inter;

import fr.inria.aoste.timesquare.launcher.core.PropertySimulation;

public interface IBuildSolver {
	
	public static final Class<IBuildSolver> CLASS = IBuildSolver.class;
	
	public ISolver buildSolver(ICCSLProxy iccslproxy);

	public Throwable getException();	

	
	public void clearCache(int level);

	public int getDefaultValue();

	public PropertySimulation[] getSupportPropertySimulation();	
	
	public boolean validateSolver(ICCSLProxy iccslproxy);
	
	

}
