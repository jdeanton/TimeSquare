/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.debug.model;

import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IMemoryBlock;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.IThread;


/**
 * A CCSL debug target is a debuggable execution context for CSSL. 
 * 
 */
final public class CCSLDebugTarget extends CCSLDebugElement implements  ICCSLDebugTarget {	
	private CCSLProcess process;
	private CCSLDebugThread thread;
	private IThread[] threads;
	private ILaunch launch;
	private CCSLStackFrame stackFrame;
	private IStackFrame[] frames;
	private String name;
	private boolean isSuspended = true;
	private Thread run=null;
	
	public CCSLDebugTarget(ILaunch launch, CCSLProcess process, ISimulationInterface sim) {
		super(null);
		this.process = process;
		this.thread = new CCSLDebugThread(this, sim);
		this.launch = launch;

		threads = new IThread[] { thread };

		try {
			name = "CCSL simulator " + sim.getSourceFile();
		} catch (Throwable e) {
			name = "CCSL Simulator ";
		}

		this.stackFrame = new CCSLStackFrame(this, thread);
		frames = new IStackFrame[1];
		frames[0] = this.stackFrame;

		// launch.addProcess(process); DO on process Constructor
		launch.addDebugTarget(this);
		run =this.process.addThread(thread);

	}
	
	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#getRun()
	 */
	public Thread getRun() {
		return run;
	}


	@Override
	protected void finalize() throws Throwable {
		System.out.println("finalize :" + toString());
		super.finalize();
	}

	public IStackFrame[] getStackFrames() {
		return this.frames;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#getThreads()
	 */
	public IThread[] getThreads() throws DebugException {
		if (thread==null)
			return new IThread[]{};
		return threads.clone();
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#hasThreads()
	 */
	public boolean hasThreads() throws DebugException {
		return true;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#supportsBreakpoint(org.eclipse.debug.core.model.IBreakpoint)
	 */
	public boolean supportsBreakpoint(IBreakpoint breakpoint) {
		return false;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#getProcess()
	 */
	public CCSLProcess getProcess() {
		return this.process;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#getLaunch()
	 */
	@Override
	public ILaunch getLaunch() {
		return this.launch;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#terminate()
	 */
	final public void terminate() throws DebugException {
		fireTerminateEvent();
		fireSuspendEvent(DebugEvent.STATE);
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#canTerminate()
	 */
	public boolean canTerminate() {
		return getProcess().canTerminate();
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#isTerminated()
	 */
	public boolean isTerminated() {
		return getProcess().isTerminated();
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#canResume()
	 */
	public boolean canResume() {
		return !isTerminated() && isSuspended();
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#canSuspend()
	 */
	public boolean canSuspend() {
		return !isTerminated() && !isSuspended();
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#isSuspended()
	 */
	public boolean isSuspended() {
		return this.isSuspended;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#getName()
	 */
	public String getName() throws DebugException {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#resume()
	 */
	public void resume() throws DebugException {
		isSuspended = false;
		fireResumeEvent(DebugEvent.SUSPEND);
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#suspend()
	 */
	public void suspend() throws DebugException {
		isSuspended = true;
		fireSuspendEvent(DebugEvent.SUSPEND);
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#breakpointAdded(org.eclipse.debug.core.model.IBreakpoint)
	 */
	public void breakpointAdded(IBreakpoint breakpoint) {

	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#breakpointChanged(org.eclipse.debug.core.model.IBreakpoint, org.eclipse.core.resources.IMarkerDelta)
	 */
	public void breakpointChanged(IBreakpoint breakpoint, IMarkerDelta delta) {

	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#breakpointRemoved(org.eclipse.debug.core.model.IBreakpoint, org.eclipse.core.resources.IMarkerDelta)
	 */
	public void breakpointRemoved(IBreakpoint breakpoint, IMarkerDelta delta) {

	}

	// Interface IDisconnect
	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#canDisconnect()
	 */
	public boolean canDisconnect() {
		return getProcess().canDisconnect();
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#disconnect()
	 */
	public void disconnect() throws DebugException {
		getProcess().disconnect();
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#isDisconnected()
	 */
	public boolean isDisconnected() {
		return getProcess().isDisconnected();
	}

	// IMemoryBlockRetrieval

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#getMemoryBlock(long, long)
	 */
	public IMemoryBlock getMemoryBlock(long startAddress, long length)
			throws DebugException {
		throw new DebugException(new Status(IStatus.ERROR, "fr.inria.aoste.timesquare.launcher.debug.model", "Storege Retrieval not supported"));
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.launcher.debug.model.ICCSLDebugTarget#supportsStorageRetrieval()
	 */
	public boolean supportsStorageRetrieval() {
		return false;
	}
}
