/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.extensionpoint;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.InitOutputData;
import fr.inria.aoste.trace.LogicalStep;

public interface IOutputTrace {

	public static final Class<IOutputTrace> CLASS = IOutputTrace.class;
	
	abstract public void setOption(IOutputOption o) throws Throwable;

	abstract public String getkey();
	
	abstract public String getName();

	abstract public IOutputOption getOption();

	abstract public void initAndOutputFile(InitOutputData data, ISolverForBackend solver) throws Throwable ; 
	

	//
	abstract public void killTimer();

	// Step for init a Ouput
	//abstract public void aInitStep(LogicalStep aStep); ( call by initStep_And_ClockConstraint in OutputCommon )

	// Step en mode play ( premiere persentaion )
//	abstract public void aNewStep(LogicalStep aStep, boolean timed); ( call by aNewStep_And_Relation in OutputCommon )

	// Step en mode replay ( seconde ( ou + ) presentation )
	abstract public void aStep(LogicalStep aStep, boolean timed);

	// Step dernier Step
	abstract public void aFinalStep(int aStep);

	// Arret du format de trace
	abstract public int terminate();

//	abstract void aNewRelation(List<EObject> relation); ( call by initStep_And_ClockConstraint in OutputCommon )

//	abstract void aNewClockConstraint(List<EObject> clockConstraint); ( call by aNewStep_And_Relation in OutputCommon )
		
	abstract public void aNewStep_And_Relation(LogicalStep aStep,boolean timed,List<EObject> relation) ;
	
	

}
