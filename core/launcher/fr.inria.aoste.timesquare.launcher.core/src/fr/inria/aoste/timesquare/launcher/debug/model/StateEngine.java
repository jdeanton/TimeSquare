/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.launcher.debug.model;

public enum StateEngine {
	init("init", 0), run("run", 1), pause("pause", 2), terminate("terminate", 3), ter_back("replay", 4), ter_replay("replay", 5), error(
			"error", 6), errorInitModel("errorInitModel", 7), errorInitView("errorInitView", 8);

	private final int id;
	private final String name;

	private StateEngine(String _name, int n) {
		id = n;
		name = _name;
	}

	public boolean isFinish() {
		switch (this) {
		case terminate:
		case ter_back:
		case ter_replay:
		case error:
		case errorInitModel:
		case errorInitView:
			return true;

		default:
			return false;
		}
	}

	public boolean isTerminate() {
		switch (this) {
		case terminate:
		case error:
		case errorInitModel:
		case errorInitView:
			return true;

		default:
			return false;
		}
	}

	public boolean isRunning() {
		return (this == run) || (this == ter_replay);
	}

	public boolean isRunnable() {
		switch (this) {
		case pause:
		case ter_back:
			return true;

		default:
			return false;
		}
	}

	public boolean isError() {
		switch (this) {
		case error:
		case errorInitModel:
		case errorInitView:
			return true;

		default:
			return false;
		}
	}

	public final int getId() {
		return id;
	}

	public final String getName() {
		return name;
	}

}