/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.ui;

import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import fr.inria.aoste.timesquare.launcher.debug.model.proxy.CCSLSimulationConfigurationHelper;
import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter;
import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter.EventEnumerator;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.relation.IDescription;

public class BindingTab extends MyAbstractLaunchConfigurationTab {

	private IFile nameoffile = null;
	private Composite simulationParameters = null;
	private Table table1 = null;
	private Tree table2 = null;
	private Table table3 = null;

	private List<ModelElementReference> listclock = null;
	private List<ModelElementReference> listassert = null;
	private List<IDescription> listrelation = null;
	CCSLSimulationConfigurationHelper ccslproxy = null;

	public BindingTab() {
		super();

	}
	
	
	
	@Override
	public void notifyChange() {

	}

	@Override
	protected void updateLaunchConfigurationDialog() {
		if (!flaginit)
			super.updateLaunchConfigurationDialog();
	}
	
	

	@Override
	public void updateLaunchConfigurationDialog(Object context) {
	
		
	}



	@Override
	public void createControl(Composite parent) {
		nameoffile = null;
		// setControl(tmp);
		
		simulationParameters = new SashForm(parent, SWT.RESIZE |SWT.BORDER | SWT.VERTICAL);		
		setControl(simulationParameters);
		GridData gData = new GridData(SWT.FILL, SWT.FILL, true, true);
		simulationParameters.setLayoutData(gData);
		GridLayout layout = new GridLayout(1, true);
		layout.marginLeft=10;
		layout.marginRight=10;
		layout.verticalSpacing=3;
		layout.marginBottom=10;
		layout.marginTop=10;
		simulationParameters.setLayout(layout);

		table1 = createTableClock(
		new MetaGroup(this, simulationParameters, "Clock", SWT.RESIZE).g);

		table2 = createTree(
		new MetaGroup(this, simulationParameters, "Relation", SWT.RESIZE).g);
		table3 = createTableAssert(
		new MetaGroup(this, simulationParameters, "Assertion", SWT.RESIZE).g);
		//simulationParameters.addControlListener(new SimulationParametersControlListener());
		Dialog.applyDialogFont(parent);

	}

	public Table createTableClock(Composite option) {
		GridData gData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gData.minimumHeight = 150;
		option.setLayoutData(gData);
		Table table = null;
		table = new Table(option, SWT.SINGLE);// | SWT.BORDER|
												// SWT.FULL_SELECTION |
												// SWT.RESIZE);
		table.setHeaderVisible(true);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		//data.minimumHeight = 100;
	
		table.setLayoutData(data);

		TableColumn column = null;
		// add column with check box
		column = new TableColumn(table, SWT.LEFT, 0);
		// column.setText("Constraint Name");
		column.setMoveable(false);
		column.setWidth(175);
		column = new TableColumn(table, SWT.LEFT, 1);
		column.setText("Id");
		column.setMoveable(false);
		column.setWidth(600);
		
		column = new TableColumn(table, SWT.LEFT, 2);
		column.setText("referenced Object ");
		column.setMoveable(false);
		column.setWidth(65);
		
		column = new TableColumn(table, SWT.LEFT, 3);
		column.setText("Event");
		column.setMoveable(false);
		column.setWidth(40);
		
		// table.addMouseListener(new MouseConstraintListener());
		// table.addMouseMoveListener(new MouseOverToolTips());

		return table;
	}
	
	
	public Table createTableAssert(Composite option) {
		GridData gData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gData.minimumHeight = 150;
		option.setLayoutData(gData);
		Table table = null;
		table = new Table(option, SWT.SINGLE);// | SWT.BORDER|
												// SWT.FULL_SELECTION |
												// SWT.RESIZE);
		table.setHeaderVisible(true);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		//data.minimumHeight = 100;
	
		table.setLayoutData(data);

		TableColumn column = null;
		// add column with check box
		column = new TableColumn(table, SWT.LEFT, 0);
		// column.setText("Constraint Name");
		column.setMoveable(false);
		column.setWidth(175);
		column = new TableColumn(table, SWT.LEFT, 1);
		column.setText("Id");
		column.setMoveable(false);
		column.setWidth(400);
		// table.addMouseListener(new MouseConstraintListener());
		// table.addMouseMoveListener(new MouseOverToolTips());

		return table;
	}

	public Tree createTree(Composite option) {
		GridData gData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gData.minimumHeight = 150;
		option.setLayoutData(gData);
		Tree tree = null;
		tree = new Tree(option, SWT.SINGLE);// | SWT.BORDER| SWT.FULL_SELECTION
											// |SWT.RESIZE);
		tree.setHeaderVisible(true);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		//data.minimumHeight = 100;
		tree.setLayoutData(data);
		TreeColumn column = null;
		// add column with check box
		column = new TreeColumn(tree, SWT.LEFT, 0);
		// column.setText("Constraint Name");
		column.setMoveable(false);
		column.setWidth(375);
		column = new TreeColumn(tree, SWT.LEFT, 1);
		column.setText("UID");
		column.setMoveable(false);
		column.setWidth(425);
		return tree;
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		ccslproxy = CCSLSimulationConfigurationHelper.create(configuration);
		refreshliste();
		updatelist();

	}

	protected boolean flaginit = false;

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		 ccslproxy = CCSLSimulationConfigurationHelper.create(configuration);
		try {
			flaginit = true;
			refreshliste();
			updatelist();
		} finally {
			flaginit = false;
		}

	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		ccslproxy = CCSLSimulationConfigurationHelper.create(configuration);
		refreshliste();
		updatelist();
	}

	@Override
	public Image getImage() {
		return PluginHelpers.getImage(Activator.PLUGIN_ID, "icons/tsq.gif");
	}

	public String getName() {
		return "Information";
	}

	private void refreshliste() {
		IFile file = ccslproxy.get_SourceIFile();
		if ((file == null) || (nameoffile == null)
				|| (file!=nameoffile)) {
			nameoffile = file;
		
			listclock = ccslproxy.getListofClock();
			listassert = ccslproxy.getListofAssert();
			listrelation = ccslproxy.getListofRelation();
		}

	}

	public void updatelist() {
		// correction of bug #12645
		// this test is added because eclipse calls this method before the createControl
		// method... to investigate...
		if (simulationParameters != null)
		{
			table1.removeAll();
				
			for (ModelElementReference ce : listclock) {
				TableItem ti = new TableItem(table1, SWT.NONE);
				IModelAdapter adapter = AdapterRegistry.getAdapter(ce);
				ti.setText(0, adapter.getReferenceName(ce));
				ti.setText(1, AdapterRegistry.getAdapter(ce).getUID(ce));
				int s= adapter.fillWithReferencedElements(ce, null).size();
				if(s==0)
					ti.setText(2, "0");
				else
					ti.setText(2, "found: " + s +" " );
				if ( ce.getElementRef().size()==1)
				{
					EObject eo = HelperFactory.getLastReference(ce);
					EventEnumerator evEnum = AdapterRegistry.getAdapter(eo).getEventkind(eo);
					if (evEnum!=null)
					{
						ti.setText(3, evEnum.getName());
					}
				}
				ti.setData(ce);
			}
			
			table1.pack();
			table2.removeAll();
			TreeItem item = null;
			HashMap<EObject, TreeItem> map = new HashMap<EObject, TreeItem>();
			for (IDescription ce : listrelation) {
				TreeItem tin = map.get(ce.getCcslConstraint());
				if (tin == null) {
					tin = new TreeItem(table2, SWT.NONE);
					map.put(ce.getCcslConstraint(), tin);
					tin.setText(0, ce.getBaseInfo());
					tin.setExpanded(true);
					tin.setData(ce.getCcslConstraint());
					table2.showItem(tin);
				}
				TreeItem ti = new TreeItem(tin, SWT.NONE);
				// ti.setText(0, ce.getBaseInfo());
				ti.setText(0, ce.getSubInfo());
				ti.setText(1, ce.getID());
				ti.setData(ce);
				ti.setExpanded(true);
				table2.showItem(ti);
				if (item == null)
					item = tin;
	
			}
	
			if (item != null)
				table2.showItem(item);
		
			// table2.update();
	
			table2.pack();
			table3.removeAll();
			for (ModelElementReference ce : listassert) {
				TableItem ti = new TableItem(table3, SWT.NONE);
				ti.setText(0, AdapterRegistry.getAdapter(ce).getReferenceName(ce));
				ti.setText(1, AdapterRegistry.getAdapter(ce).getUID(ce));
				ti.setData(ce);
			}
			
			table3.pack();
			table1.getParent().pack();
			table2.getParent().pack();
			table3.getParent().pack();
			simulationParameters.pack();
		}

	}
	

}
