/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.ui;

import java.util.ArrayList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.debug.ui.ILaunchShortcut2;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.eclipse.ui.part.FileEditorInput;

import fr.inria.aoste.timesquare.launcher.debug.model.ICCSLConstants;

public class CCSLLaunchSimulationShortcut implements ILaunchShortcut2 {

	/**** ILaunchShortcut2 */

	public IFile getLaunchableResource(IEditorPart editorpart) {
		try {
			IFile file = ((FileEditorInput) (editorpart.getEditorInput()
					.getPersistable())).getFile();
			// return ResourcesPlugin.getWorkspace().getRoot();
			return file;
		} catch (Exception e) {
			return null;
		}
	}

	public IResource getLaunchableResource(ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection ss = (IStructuredSelection) selection;
			if (ss.size() >= 1) {
				Object element = ss.getFirstElement();
				if (element instanceof IResource) {
					return ((IResource) element);
				}

			}
		}
		return null;

	}

	public ILaunchConfiguration[] getLaunchConfigurations(IEditorPart editorpart) {
		try {
			ILaunchManager manager = DebugPlugin.getDefault()
					.getLaunchManager();
			manager.getLaunchConfigurationType("fr.inria.aoste.timesquare.launcher.core.launchConfigurationType");
			return new ILaunchConfiguration[] {};// manager.getLaunchConfigurations(lcType);
		} catch (Throwable e) {
			// TODO: handle exception
		}
		return new ILaunchConfiguration[] {};
	}

	public ILaunchConfiguration[] getLaunchConfigurations(ISelection selection) {
		try {
			DebugPlugin.getDefault().getLaunchManager();
			// ILaunchConfigurationType lcType =
			// manager.getLaunchConfigurationType("fr.inria.aoste.timesquare.launcher.core.launchConfigurationType");
			// return manager.getLaunchConfigurations(lcType);
		} catch (Throwable e) {
			// TODO: handle exception
		}
		return new ILaunchConfiguration[] {};
	}

	public void launch(IEditorPart editor, String mode) {
		System.err.println();
		IFile file = ((FileEditorInput) (editor.getEditorInput()
				.getPersistable())).getFile();
		System.out.println(file);
		launch(file, mode);
	}

	/**** ILaunchShortcut */
	public void launch(ISelection selection, String mode) {
		if (selection instanceof IStructuredSelection) {
			searchAndLaunch(((IStructuredSelection) selection).toArray(), mode);
		}

	}

	private void launch(IFile selection, String mode) {
		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType lcType = manager
				.getLaunchConfigurationType("fr.inria.aoste.timesquare.launcher.core.launchConfigurationType");
		try {
			ILaunchConfiguration[] lConfs = manager
					.getLaunchConfigurations(lcType);
			ArrayList<ILaunchConfiguration> conflist = new ArrayList<ILaunchConfiguration>();

			for (ILaunchConfiguration conf : lConfs) {
				if (conf.getAttribute(ICCSLConstants.SOURCE, "").equals(
						selection.getFullPath().toString())) {
					conflist.add(conf);
				}
			}
			if (conflist.size() != 0) {
				launch(chooseConfiguration(conflist), mode);
				return;
			}

			ILaunchConfiguration conf = lcType.newInstance(
					selection.getParent(), "default");
			ILaunchConfigurationWorkingCopy workingCopy = conf.getWorkingCopy();
			workingCopy.setAttribute(ICCSLConstants.SOURCE, selection
					.getFullPath().toString());
			workingCopy.doSave();
			launch(conf, mode);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	private void launch(ILaunchConfiguration conf, String mode)
			throws CoreException {
		if (conf != null)
			conf.launch(mode, null);
	}

	private void searchAndLaunch(Object[] selections, String mode) {
		for (Object sel : selections) {
			if (sel instanceof IFile)
				launch((IFile) sel, mode);
		}
	}

	protected ILaunchConfiguration chooseConfiguration(
			ArrayList<ILaunchConfiguration> configList) {
		if (configList.size()==1) return configList.get(0);
		IDebugModelPresentation labelProvider = DebugUITools
				.newDebugModelPresentation();
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				new Shell(), labelProvider);
		dialog.setElements(configList.toArray());
		dialog.setTitle("CCSL Simulation");
		dialog.setMessage("Choose a CCSL Simulation");
		dialog.setMultipleSelection(false);
		int result = dialog.open();
		labelProvider.dispose();
		if (result == Window.OK) {
			return (ILaunchConfiguration) dialog.getFirstResult();
		}
		return null;
	}

}
