/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.ui;

import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.debug.ui.IValueDetailListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorInput;

public class DebugModelPresentation1 implements IDebugModelPresentation {

	public void addListener(ILabelProviderListener listener) {

	}

	public void computeDetail(IValue value, IValueDetailListener listener) {

	}

	public void dispose() {

	}

	public String getEditorId(IEditorInput input, Object element) {

		return null;
	}

	public IEditorInput getEditorInput(Object element) {

		return null;
	}

	public Image getImage(Object element) {

		return null;
	}

	public String getText(Object element) {

		return null;
	}

	public boolean isLabelProperty(Object element, String property) {

		return false;
	}

	public void removeListener(ILabelProviderListener listener) {

	}

	public void setAttribute(String attribute, Object value) {

	}

}
