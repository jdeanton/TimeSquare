/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.ui;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.inria.aoste.timesquare.launcher.debug.model.output.ICCSLModelTab;

public abstract class MyAbstractLaunchConfigurationTab extends AbstractLaunchConfigurationTab implements ICCSLModelTab, SelectionListener, ModifyListener {

	@Override
	public Button createCheckButton(Composite parent, String label) {
		return super.createCheckButton(parent, label);
	}

	public Combo createCombo(Composite parent, String labelName,
			String[] values, int width) {
		Composite comp = new Composite(parent, SWT.FILL);
		RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
		comp.setLayout(rowLayout);
		Label label = new Label(comp, SWT.READ_ONLY);
		label.setLayoutData(new RowData(180, 20));
		label.setText(labelName);

		Combo combo = new Combo(comp, SWT.SINGLE | SWT.BORDER);
		combo.setItems(values);
		//combo.setText(values[0]);
		combo.setLayoutData(new RowData(width, 20));
		combo.addSelectionListener(this);
		return combo;
	}

	public Text createLabelInput(Composite parent, String labelName, int width) {

		return createLabelInput(parent, labelName, width, false);
	}

	public Text createLabelInput(Composite parent, String labelName,
			int width, boolean b) {
		Composite comp = new Composite(parent, SWT.FILL);
		RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
		comp.setLayout(rowLayout);
		Label label = new Label(comp, SWT.READ_ONLY);
		label.setLayoutData(new RowData(180, 20));
		label.setText(labelName);
		Text text = new Text(comp, SWT.SINGLE | SWT.BORDER | (b ? SWT.RIGHT_TO_LEFT : 0));
		text.setLayoutData(new RowData(width, 20));
		text.addSelectionListener(this);
		text.addModifyListener(this);
		return text;
	}

	public Button createPushButton(Composite parent, String label, Image image) {
		return super.createPushButton(parent, label, image);
	}

	@Override
	public void setErrorMessage(String errorMessage) {
		super.setErrorMessage(errorMessage);
	}

	@Override
	public void setMessage(String message) {
		super.setMessage(message);
	}

	// ModifyListener {
	@Override
	public void modifyText(ModifyEvent e) {
		updateLaunchConfigurationDialog();
	}
	// } ModifyListener

	// SelectionListener {
	@Override
	public void widgetSelected(SelectionEvent e) {
		updateLaunchConfigurationDialog();
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		widgetSelected(e);		
	}	
	// } SelectionListener

	@Override
	protected void updateLaunchConfigurationDialog() {
		super.updateLaunchConfigurationDialog();
	}
}
