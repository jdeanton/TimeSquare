/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.launcher.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;

public class MetaGroup {
	private static class SelectionMetaActive extends SelectionAdapter {

		MetaGroup mg;

		public SelectionMetaActive(MetaGroup cin) {
			super();
			mg = cin;
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			mg.modelSimulationTab.updateLaunchConfigurationDialog();
			mg.activeCheck();

		}
	}

	private Button b;

	/**
	 * 
	 */
	private MyAbstractLaunchConfigurationTab modelSimulationTab;
	protected Composite c;
	protected Group g;

	public MetaGroup(MyAbstractLaunchConfigurationTab modelSimulationTab, Composite parent, String name) {
		this.modelSimulationTab = modelSimulationTab;
		Group group = new Group(parent, SWT.NONE);
		group.setLayout(new GridLayout(1, true));
		group.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		group.setText(name);
		initMeta(group, group, null);
	}

	
	public MetaGroup(MyAbstractLaunchConfigurationTab modelSimulationTab, Composite parent, String name, int style) {
		this.modelSimulationTab = modelSimulationTab;
		Group group = new Group(parent, style);
		group.setLayout(new GridLayout(1, true));
		group.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		group.setText(name);
		initMeta(group, group, null);
	}
	
	
	public int activeCheck() {
		activeCheck(b.getSelection());
		return 0;
	}

	public boolean isSelected() {
		return b.getSelection();
	}

	public void setSelected(boolean sel) {
		activeCheck(sel);
		b.setSelection(sel);
	}

	private int activeCheck(boolean bol) {
		recurviceActive(c, bol);
		return 0;
	}

	private void initMeta(Group g, Composite c, Button b) {

		this.g = g;
		this.c = c;
		this.b = b;
		if (b != null) {
			b.addSelectionListener(new SelectionMetaActive(this));
		}
	}

	private int recurviceActive(Control ctl, boolean bol) {
		ctl.setEnabled(bol);
		if (ctl instanceof Composite) {
			for (Control ctl2 : ((Composite) ctl).getChildren()) {
				recurviceActive(ctl2, bol);
			}
		}
		return 0;
	}
}