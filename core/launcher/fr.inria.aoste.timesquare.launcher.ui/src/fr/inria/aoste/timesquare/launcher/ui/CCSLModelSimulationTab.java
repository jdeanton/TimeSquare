/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;

import fr.inria.aoste.timesquare.launcher.core.OutputManager;
import fr.inria.aoste.timesquare.launcher.core.PESolverManager;
import fr.inria.aoste.timesquare.launcher.core.inter.ICCSLProxy;
import fr.inria.aoste.timesquare.launcher.debug.model.output.OutputSetupOption;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.CCSLSimulationConfigurationHelper;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;
import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyManager;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.utils.ui.listeners.IntListener;

public class CCSLModelSimulationTab extends MyAbstractLaunchConfigurationTab {

	public static final class SimulationParametersControlListener implements ControlListener {

		public SimulationParametersControlListener(Composite simulationParameters) {
			super();
			this.composite = simulationParameters;
		}

		Composite composite = null;

		public void controlMoved(ControlEvent e) {

			Point pt = composite.getSize(); // new Point(0, 0);

			for (Control c : composite.getChildren()) {
				Point tmp = c.getSize();
				if (tmp.x > pt.x) {
					pt.x = tmp.x;
				}
			}

			// composite.setSize(pt);
		}

		public void controlResized(ControlEvent e) {

			try {

				Point pt = composite.getSize();
				pt.y = 0;

				for (Control c : composite.getChildren()) {
					Point tmp = c.getSize();
					pt.y += tmp.y;// + 10;
					if (tmp.x > pt.x) {
						pt.x = tmp.x;
					}
				}
				// System.out.println("test pt :" + pt);
				if (pt.x < 400) {
					pt.x = 400;
				}
				if (composite.getParent() instanceof ScrolledComposite) {
					((ScrolledComposite) composite.getParent()).setMinHeight(pt.y);
					// setMinSize(pt);
				}
				composite.layout();
			} catch (Throwable t) {
				System.err.println(t.getMessage());
			}

		}
	}

	public final class SelectionPolicyListener implements SelectionListener {

		public void widgetDefaultSelected(SelectionEvent e) {

		}

		public void widgetSelected(SelectionEvent e) {
			policyCombo.setToolTipText("Select Simulation Policy :"
					+ SimulationPolicyManager.getDefault().getComment(policyCombo.getSelectionIndex()));

		}
	}

	private TextBrowse fNameText;
	private TextBrowse prioritySpecificationBrowser;
	
	private ArrayList<OutputSetupOption> listoption = new ArrayList<OutputSetupOption>();
	private MetaGroup metoption;
	private String[] policies = SimulationPolicyManager.getDefault().getPolicyNamesAsArray();

	private Combo policyCombo;
	private Button eState;
	private Button eTime,ecpsrc;

	private Text stepsText;
	private Composite simulationParameters;

	private Composite tampon;
	// private ScrolledComposite tmp;
	private IFile valuefile;
	private IFile prioritySpecificationIFile=null;

	private Composite parent;

	public CCSLModelSimulationTab() {
		super();
	}

	public void createControl(Composite _parent) {
		parent = _parent;
		// ScrolledComposite sc = (ScrolledComposite) parent;
		// sc.setAlwaysShowScrollBars(true);
		// parent.setLayout(new FormLayout());
		simulationParameters = new Composite(parent, SWT.RESIZE);
		setControl(simulationParameters);
		// GridData gData = new GridData(SWT.FILL, SWT.FILL, true, true);
		// simulationParameters.setLayoutData(gData);

		FormData data3 = new FormData();
		data3.top = new FormAttachment(0, 2);
		data3.left = new FormAttachment(0, 2);
		data3.right = new FormAttachment(100, -2);
		data3.bottom = new FormAttachment(100, -2);
		simulationParameters.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		FormLayout layout = new FormLayout();
		simulationParameters.setLayout(layout);

		callSource();
		callSimulation();
		callOuptut();
		tampon = new Composite(simulationParameters, SWT.RESIZE);
		generateLayout();
		simulationParameters.addControlListener(new SimulationParametersControlListener(simulationParameters));
		// parent.addControlListener(new
		// SimulationParametersControlListener(parent));
		// simulationParameters.pack();
		Dialog.applyDialogFont(parent);
	}

	private int callSource() {
		fNameText = new TextBrowse(this, simulationParameters, "Source:", PESolverManager.getDefault()
				.getListExtension());
		String[] prioExtensions = {"ccslPriority", "ccslprioritymodel", "prioritymodel"}; 
		prioritySpecificationBrowser = new TextBrowse(this, simulationParameters, "Priority Source:", prioExtensions);
		return 0;
	}

	private int callSimulation() {
		metoption = new MetaGroup(this, simulationParameters, "Simulation", SWT.RESIZE);
		{
			Group option = metoption.g;
			stepsText = createLabelInput(option, "Simulation steps: ", 150);
			stepsText.addListener(SWT.Verify, new IntListener());
			// ste
			policyCombo = createCombo(option, "Simulation policy:", policies, 150);
			policyCombo.addSelectionListener(new SelectionPolicyListener());

			policyCombo.setToolTipText("Select Simulation Policy");
			eState = createCheckButton(option, "Compute ghost");
			eState.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			eState.addSelectionListener(this);
			eTime = createCheckButton(option, "Show Time");
			eTime.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			eTime.addSelectionListener(this);
			ecpsrc = createCheckButton(option, "Copy Source");
			ecpsrc.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			ecpsrc.addSelectionListener(this);

			option.pack();
		}
		return 0;
	}

	List<Group> lsgroup = new ArrayList<Group>();

	private int callOuptut() {
		OutputManager outputManager = OutputManager.getDefault();
		for (String key : outputManager.getListkey()) {
			OutputSetupOption oso;
			oso = outputManager.getUIforKey(key);
			if (oso != null) {
				Group group = oso.initMeta(this, simulationParameters);
				lsgroup.add(group);
				group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				listoption.add(oso);
			}
		}
		return 0;
	}

	private static final int CONTROL_OFFSET = 5;

	private int generateLayout() {

		FormData data = new FormData();
		data.top = new FormAttachment(0, CONTROL_OFFSET);
		data.left = new FormAttachment(0, CONTROL_OFFSET);
		data.right = new FormAttachment(100, -CONTROL_OFFSET);
		fNameText.getGroup().setLayoutData(data);
		
		FormData prioFormData = new FormData();
		prioFormData.top = new FormAttachment(fNameText.getGroup(), CONTROL_OFFSET);
		prioFormData.left = new FormAttachment(0, CONTROL_OFFSET);
		prioFormData.right = new FormAttachment(100, -CONTROL_OFFSET);
		prioritySpecificationBrowser.getGroup().setLayoutData(prioFormData);

		FormData data2 = new FormData();
		data2.top = new FormAttachment(prioritySpecificationBrowser.getGroup(), CONTROL_OFFSET);
		data2.left = new FormAttachment(0, CONTROL_OFFSET);
		data2.right = new FormAttachment(100, -CONTROL_OFFSET);
		metoption.g.setLayoutData(data2);

		int size = lsgroup.size();
		Group oldlgroup = metoption.g;
		for (int i = 0; i < size; i++) {
			FormData data3 = new FormData();
			data3.top = new FormAttachment(oldlgroup, CONTROL_OFFSET);
			data3.left = new FormAttachment(0, CONTROL_OFFSET);
			data3.right = new FormAttachment(100, -CONTROL_OFFSET);

			Group g = lsgroup.get(i);
			g.setLayoutData(data3);
			oldlgroup = g;
		}

		FormData data3 = new FormData();
		data3.top = new FormAttachment(oldlgroup, CONTROL_OFFSET);
		data3.height = 100;
		data3.left = new FormAttachment(0, CONTROL_OFFSET);
		data3.right = new FormAttachment(100, -CONTROL_OFFSET);
		data3.bottom = new FormAttachment(100, -CONTROL_OFFSET);
		tampon.setLayoutData(data3);
		simulationParameters.layout();
		return 0;
	}

	@Override
	public void dispose() {
		for (OutputSetupOption oso : listoption) {
			oso.dispose();
		}

		super.dispose();
		PESolverManager.getDefault().clearCache(0);
	}

	@Override
	public String getErrorMessage() {

		return super.getErrorMessage();
	}

	@Override
	public Image getImage() {
		return PluginHelpers.getImage(Activator.PLUGIN_ID, "icons/tsq.gif");
	}

	public String getName() {
		return "CCSLModel Main";
	}

	public void initializeFrom(ILaunchConfiguration config) {

		try {
			flaginit = true;
			CCSLSimulationConfigurationHelper ccslproxy = CCSLSimulationConfigurationHelper.create(config);
			valuefile = ccslproxy.get_SourceIFile();
			fNameText.setSourceFile(valuefile);
			
			prioritySpecificationIFile = ccslproxy.get_PrioritySourceIFile();
			prioritySpecificationBrowser.setSourceFile(prioritySpecificationIFile);

			int n = ccslproxy.getStepNbr();
			if (n == 0) {
				n = PESolverManager.getDefault().getDefaultStep(ccslproxy);
			}
			stepsText.setText(Integer.toString(n));
			policyCombo.select(ccslproxy.getPolicyId());
			eState.setSelection(ccslproxy.getEstate());
			eState.setEnabled(ccslproxy.solverUsedEstate());
			eTime.setSelection(ccslproxy.getTime());
			eTime.setEnabled(true);
			ecpsrc.setSelection(ccslproxy.getCopy());
			ecpsrc.setEnabled(true);
			
			// equals(obj)true); // TODO
			try {

				for (OutputSetupOption oso : listoption) {

					IOutputOption ioo = ccslproxy.getTable(oso.getKey());
					/*try {
						if (ioo.isActivable())
							oso.newModel(new ModelData(ccslproxy));
					} catch (Throwable e) {
						oso.newModel(new ModelData());
						e.printStackTrace();
					}*/
					oso.initializeFrom(ioo);
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
		} finally {
			flaginit = false;
		}
	}

	@Override
	public boolean isValid(ILaunchConfiguration launchConfig) {
		setErrorMessage(null);
		ICCSLProxy ccslProxyWrite = CCSLSimulationConfigurationHelper.create(launchConfig);
		try {

			if (ccslProxyWrite.get_SourceIFile() == null) {
				setErrorMessage("No File");
				return false;
			}
			if (!ccslProxyWrite.get_SourceIFile().exists()) {
				setErrorMessage("File Does Not Exist");
				return false;
			}
			if (!PESolverManager.getDefault().isExtensionSuppported(
					ccslProxyWrite.get_SourceIFile().getFileExtension())) {
				setErrorMessage("No Solver For This File Type");
				return false;
			}
			if (!ccslProxyWrite.haveSolver())
			{
				setErrorMessage("Init solver: " +PESolverManager.getDefault().getThrowable());
				return false;
			}
			try {
				if (stepsText.getText().length() != 0) {
					Integer.parseInt(stepsText.getText());
				}
			} catch (NumberFormatException nfe) {
				setErrorMessage("Steps should be an integer");
				return false;
			} catch (Throwable e) {
				setErrorMessage("Steps should be an integer");
				return false;
			}
			try {
				boolean b = PESolverManager.getDefault().validate(ccslProxyWrite);
				if (b == false) {
					setErrorMessage(" Validation Error \n"
							+ PESolverManager.getDefault().getThrowable().getLocalizedMessage());
					return false;
				}
			} catch (Throwable e) {

				setErrorMessage(" Exception ... Validation Problem \n" + e.getLocalizedMessage());
				return false;
			}
			setErrorMessage(null);
		} finally {
			try {
				for (OutputSetupOption oso : listoption) {
					if (!oso.isValid(ccslProxyWrite.getTable(oso.getKey()))) {
						// return false;
					}
				}
			} catch (Throwable e) {
				if (getErrorMessage()!=null	)
					setErrorMessage(" Exception ... " + e.getLocalizedMessage());
				return false;

			}
		}
		return true;
	}

	public void notifyChange() {

	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		CCSLSimulationConfigurationHelper ccslProxyWrite = CCSLSimulationConfigurationHelper.create(configuration);
		
		IFile newvaluefile = fNameText.getSourceFile();
		ccslProxyWrite.setSource(newvaluefile);
		
		IFile newPrioSpecFile = prioritySpecificationBrowser.getSourceFile();
		ccslProxyWrite.setPrioritySource(newPrioSpecFile);
		
		ccslProxyWrite.setPolicyId(policyCombo.getSelectionIndex());
		ccslProxyWrite.setEstate(eState.getSelection());
		ccslProxyWrite.setTime(eTime.getSelection());
		ccslProxyWrite.setCopy(ecpsrc.getSelection());
		String s = stepsText.getText();
		if (s == null)
			s = "0";
		else if (s.length() == 0)
			s = "0";
		try {
			int n = Integer.parseInt(s);
			ccslProxyWrite.setStepNbr(n);
			// /ccslProxyWrite.setVCDactive(mvcd.isSelected());
		} catch (Throwable e) {

		}
		try {
			PESolverManager.getDefault();
			//System.out.println("update ; oso");
			for (OutputSetupOption oso : listoption) {
				if ( !(registercontext instanceof OutputSetupOption ) || ( registercontext==oso))
				{
					//System.out.println("update "+oso.getKey());
				IOutputOption ioo = ccslProxyWrite.getTable(oso.getKey());
				
				
				oso.performApply(ioo);
			
				ccslProxyWrite.writeTable(ioo);
				}
			}
		
			valuefile = newvaluefile;
			prioritySpecificationIFile = newPrioSpecFile;
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}

	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {

		CCSLSimulationConfigurationHelper ccslProxyWrite = CCSLSimulationConfigurationHelper.create(configuration);
		ccslProxyWrite.setDefault();
		IWorkbench workbench = Activator.getDefault().getWorkbench();
		ISelection sel = workbench.getActiveWorkbenchWindow().getSelectionService().getSelection();
		for (OutputSetupOption oso : listoption) {
			IOutputOption ioo = ccslProxyWrite.getTable(oso.getKey());
			if (ioo != null) {
				ioo.setDefault();
				oso.setDefaults(ioo);
				ccslProxyWrite.writeTable(ioo);
			}

		}
		if (sel instanceof IStructuredSelection) {
			Object s = ((IStructuredSelection) sel).getFirstElement();
			if (s instanceof IFile) {
				IFile file = (IFile) s;
				ccslProxyWrite.setSource(file);
			}
		}
	}

	@Override
	public void setErrorMessage(String message) {
		super.setErrorMessage(message);
	}

	boolean flaginit = false;

	Object registercontext=null;
	
	@Override
	public void updateLaunchConfigurationDialog(Object context) {
			try
			{
				registercontext=context;
				updateLaunchConfigurationDialog();
			}
			finally
			{
				registercontext=null;
			}
		
	}
	
	@Override
	protected void updateLaunchConfigurationDialog() {
		if (!flaginit) {
			flaginit = true;
			super.updateLaunchConfigurationDialog();
			flaginit = false;
		}
		if (simulationParameters != null) {

			simulationParameters.layout(true, true);
			simulationParameters.setVisible(true);
			simulationParameters.update();
			simulationParameters.pack(true);
			parent.layout(true, true);
			// parent.setVisible(true);
			parent.update();
			parent.getParent().update();
			parent.getParent().layout();

		}
		// System.out.println("... Update ");
	}

}
