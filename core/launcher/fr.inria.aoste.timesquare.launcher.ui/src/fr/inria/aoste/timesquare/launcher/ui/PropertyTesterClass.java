/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.launcher.ui;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;

import fr.inria.aoste.timesquare.launcher.core.PESolverManager;

public class PropertyTesterClass extends PropertyTester {

	public PropertyTesterClass() {
		System.out.println("Init PropertyTesterClass");
	}

	/*@Override
	public boolean isDeclaringPluginActive() {
		return super.isDeclaringPluginActive();
	}*/

	public boolean test(Object receiver, String method, Object[] args,
			Object expectedValue) {
		IResource resource = (IResource) ((IAdaptable) receiver)
				.getAdapter(IResource.class);
		if (resource == null) {

			return false;
		} else {
			return matchesContentType(resource);
		}

	}

	private boolean matchesContentType(IResource resource) {
		if (resource == null || !(resource instanceof IFile)
				|| !resource.exists()) {
			return false;
		}
		IFile file = (IFile) resource;
		// IContentDescription description;
		try {
			// description = file.getContentDescription();
			String extension = file.getFileExtension();
			// System.out.println(file.getFileExtension());
			return PESolverManager.getDefault().isExtensionSuppported(
					extension);
		} catch (Throwable e) {
			return false;
		}

		// return false;
	}

}
