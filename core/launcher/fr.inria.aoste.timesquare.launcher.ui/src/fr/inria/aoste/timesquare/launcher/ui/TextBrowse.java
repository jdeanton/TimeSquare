/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.launcher.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import fr.inria.aoste.timesquare.utils.ui.listeners.FileExtensionFilter;
import fr.inria.aoste.timesquare.utils.ui.listeners.SelectionFile;

public class TextBrowse implements SelectionListener, ModifyListener {
	private IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

	private Button b;
	private final MyAbstractLaunchConfigurationTab modelSimulationTab;
	private Text t;
	private Group group;
	private String listExtension[] = null;
	private String textlabel = "";
	private IFile value = null;

	public TextBrowse(MyAbstractLaunchConfigurationTab modelSimulationTab, Composite parent, String label, String extension[]) {
		super();
		this.modelSimulationTab = modelSimulationTab;
		if (extension!=null)
			listExtension = extension.clone();
		textlabel = label;
		createSourceEditor(parent, label);
	}

	protected IFile getSourceFile() {
		if (value != null) return value;
		IResource res = root.findMember(t.getText());
		if (res instanceof IFile)
			value = (IFile)res;
		else
			modelSimulationTab.setErrorMessage("Wrong file name "+t.getText()+".");
//		value = t.getText();
		return value;
	}

	protected void setSourceFile(IFile file) {
		this.value = file;
		if (file == null)
			t.setText("");
		else
			t.setText(file.getFullPath().toString());
	}

	protected void createSourceEditor(Composite parent, String label) {
		Font font = parent.getFont();
		group = new Group(parent, SWT.NONE);
		group.setText(label);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		group.setLayoutData(gd);
		FormLayout layout = new FormLayout();
				group.setLayout(layout);
		group.setFont(font);
		t = new Text(group, SWT.SINGLE | SWT.BORDER);
		
		
		b = modelSimulationTab.createPushButton(group, "Browse...", null);
		
		int k=3;
		FormData data = new FormData();
		data.top = new FormAttachment(0, k);
		data.left = new FormAttachment(0, k);
		data.right = new FormAttachment(b, -k);
		data.bottom = new FormAttachment(100, -k);	
		t.setLayoutData(data);
		t.setFont(font);
		
		 data = new FormData();
		 data.top = new FormAttachment(0,k );
		data.width=100;
		data.right = new FormAttachment(100, -k);
		data.bottom = new FormAttachment(100, -k);	
		b.setLayoutData(data);
		t.addModifyListener(this);
		b.addSelectionListener(this);
	}

	public Group getGroup() {
		return group;
	}

	private void handleSearchButtonSelected() {
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
				new Shell(), new WorkbenchLabelProvider(),
				new WorkbenchContentProvider());
		dialog.setInput(root);
		dialog.setAllowMultiple(false);
		dialog.setDoubleClickSelects(false);
		dialog.setTitle(textlabel);
		dialog.setMessage("message");
		dialog.setInitialSelection(value);
		// dialog.setSorter(new ResourceSorter(ResourceSorter.NAME));
		dialog.addFilter(new FileExtensionFilter(listExtension));
		dialog.setValidator(new SelectionFile());
		if (dialog.open() == Window.CANCEL) {
			return;
		}
		Object[] results = dialog.getResult();

		if (results != null) {
			if (results.length != 0) {
				if (results[0] instanceof IFile) {
					setSourceFile((IFile)results[0]);
				}
			}
		}
	}

	@Override
	public void modifyText(ModifyEvent e) {
		modelSimulationTab.updateLaunchConfigurationDialog();		
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		this.handleSearchButtonSelected();
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		widgetSelected(e);		
	}
}