/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.launcher.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;

final class ControlAdapterScroll extends ControlAdapter {

	private ScrolledComposite cont;
	private Composite in;

	/**
	 * 
	 */

	protected ControlAdapterScroll(ScrolledComposite tmp,
			Composite simulationParameters) {
		super();

		this.cont = tmp;
		this.in = simulationParameters;
	}

	@Override
	public void controlResized(ControlEvent e) {
		// Rectangle r =
		in.getClientArea();
		in.pack();
		Point pt = in.computeSize(SWT.DEFAULT, /*r.width,*/SWT.DEFAULT);
		cont.setMinSize(pt.x, pt.y + 100);
		// cont.setSize(r.width, r.height);
	}
}