/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.console;

import java.io.OutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * @author Benoit Ferrero
 * 
 */
public class ErrorConsole {

	private static DateFormat dateFormat = new SimpleDateFormat("yyyy_MMdd_HHmmss");

	static private MessageConsole mc = null;

	static private MessageConsoleStream iostd = null;
	static private MessageConsoleStream ioerr = null;
	static private MessageConsoleStream iook = null;

	static private PrintStream consolestd = null;
	static private PrintStream consoleerr = null;

	static private PrintStream consoleok = null;

	static private MessageConsole mcstack = null;
	static private MessageConsoleStream iostack = null;
	static private PrintStream consolestack = null;

	static {
		try {
			consolestd = System.out;
			consoleerr = System.err;
			consoleok = System.out;
			consolestack= System.err;
			if (ConsolePlugin.getDefault() != null) {
				if (ConsolePlugin.getDefault().getConsoleManager() != null) {
					mc = new MessageConsole("Default Console", null);
					mcstack = new MessageConsole("Stack Error", null);

					iostd = mc.newMessageStream();
					ioerr = mc.newMessageStream();
					iook = mc.newMessageStream();

					iostd.setActivateOnWrite(false);
					ioerr.setActivateOnWrite(false);
					iook.setActivateOnWrite(false);

					consolestd = new PrintStream(iostd);
					consoleerr = new PrintStream(ioerr);
					consoleok = new PrintStream(iook);

					iostack = mcstack.newMessageStream();
					consolestack = new PrintStream(iostack);
					iostack.setActivateOnWrite(false);
					iostd.setActivateOnWrite(false);

					ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { mcstack, mc });

					// ConsolePlugin.getDefault().getConsoleManager().
					iostd.setColor(ColorConstants.black); // new Color(null, 0, 0, 0));
					ioerr.setColor(ColorConstants.red);// new Color(null, 255, 0, 0));
					iook.setColor(ColorConstants.blue);// new Color(null, 0, 0, 255));
					// mc.activate();
				}
			}
		} catch (Throwable ex) {
			consolestd = System.out;
			consoleerr = System.err;
			consoleok = System.out;
		}

	}

	private ErrorConsole() {
		super();

	}

	static public int println(String s) {
		try {
			if (consolestd != null)
				consolestd.println(s);
		} catch (Throwable e) {

		}
		return 1;
	}

	static public int print(String s) {
		try {
			consolestd.print(s);
		} catch (Throwable e) {

		}
		return 1;
	}

	static public int println(Object o) {
		try {
			consolestd.println(o);
		} catch (Throwable e) {

		}
		return 1;
	}

	static public int printOKln(String s) {
		try {
			consolestd.flush();
			if (ioerr != null)
				ioerr.flush();
			consoleok.println(s);
			consoleok.flush();

			if (ioerr != null)
				ioerr.flush();
		} catch (Throwable t) {

		}
		return 1;
	}

	static public int println() {
		try {
			consolestd.println();
		} catch (Throwable e) {

		}
		return 1;
	}

	static public int printWhere() {
		Exception e = new Exception();
		consolestd.println(e.getStackTrace()[1]);
		System.err.println(e.getStackTrace()[1]);
		return 1;
	}

	static public int printWhere(String s) {
		Exception e = new Exception();
		consolestd.println(e.getStackTrace()[1] + " : " + s);
		System.err.println(e.getStackTrace()[1] + " : " + s);
		return 1;
	}

	static public int printWhere(int n, String s) {
		Exception e = new Exception();
		consolestd.println(e.getStackTrace()[1] + " : " + s);
		System.err.println(e.getStackTrace()[1] + " : " + s);
		n = n < e.getStackTrace().length - 2 ? n : e.getStackTrace().length - 2;
		for (int i = 1; i < n; i++) {
			consolestd.println("\t" + e.getStackTrace()[1 + i]);
			System.err.println("\t" + e.getStackTrace()[1 + i]);
		}
		return 1;
	}

	static public int flush() {
		consolestd.flush();
		return 1;
	}

	static public int printlnError(String s) {
		try {
			consolestd.flush();
			if (ioerr != null)
				ioerr.flush();
			consoleerr.println(s);
			consoleerr.flush();
			if (ioerr != null)
				ioerr.flush();
		} catch (Throwable ex) {

		}
		return 1;
	}

	static public int printError(Throwable t, String s) {
		try {
			if (consolestack == null)
				return 0;
			t.printStackTrace();
			t.printStackTrace(consolestack);

			consolestd.flush();
			if (ioerr != null)
				ioerr.flush();
			String type = "<...>"; // for Throwable
			if (t instanceof Error) {
				type = "ERROR";
			} else {
				if (t instanceof Exception) {
					type = "EXCEPTION ";
				} else {
					type = "THROWABLE ";
				}
			}

			consoleerr.println(type + s + " :" + t);

			consoleerr.flush();
			if (ioerr != null)
				ioerr.flush();

		} catch (Throwable ex) {

		}
		return 1;
	}

	static public int printError(Throwable t) {

		return printError(t, "");
	}

	/***
	 * 
	 * @return Date/Time in String
	 */
	public static String getDateTime() {
		synchronized (dateFormat) {

			return dateFormat.format(new Date());
		}
	}

	public static StreamHandler createErrorHandler(Formatter ft) {
		if (ft == null)
			ft = new SimpleFormatter();
		return new MyConsoleHandler(consoleerr, ft);
	}

	static protected class MyConsoleHandler extends StreamHandler {

		/**
		 * Creates a ConsoleStreamHandler for out.
		 * 
		 * @param out
		 *            the console
		 */
		public MyConsoleHandler(OutputStream out, Formatter ft) {
			super(out, ft);
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see java.util.logging.StreamHandler#publish(java.util.logging.LogRecord)
		 */
		public synchronized void publish(LogRecord record) {
			super.publish(record);
			flush();
		}

		@Override
		public synchronized void flush() {
			super.flush();
			ErrorConsole.flush();
		}

		/**
		 * Don't close output stream.
		 */
		public synchronized void close() {
			// don't close
		}
	}

}
