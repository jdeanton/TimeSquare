/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.timedsystem;

import java.util.ArrayList;

public class TimedSystem {

	public TimedSystem() {
		super();
	}

	private boolean flagout = false;
	private long lastdate = 0;
	private ArrayList<EVent> levent = new ArrayList<EVent>(10);
	private long datefinish = 0;

	public static class EVent {

		public EVent(String _s, long _date) {
			super();
			s = _s;
			date = _date;
		}

		public EVent(String _s) {
			super();
			s = _s;
			date = System.currentTimeMillis();
		}

		public EVent() {
			super();

		}

		private String s;
		private long date;
		public final String getS() {
			return s;
		}

		public final long getDate() {
			return date;
		}
	}

	public void addEvent(String s) {
		long _date = System.currentTimeMillis();
		String olds = "..";
		if (levent.size() != 0) {
			olds = levent.get(levent.size() - 1).getS();
		}
		levent.add(new EVent(s, _date));
		if (lastdate != 0 && flagout) {

			System.out.println("---------> Time (" + olds + "):" + (_date - lastdate) + " <--------------");
		}
		lastdate = _date;
	}

	public void finish() {
		datefinish = System.currentTimeMillis();
	}

	public String toStringEvent() {
		int size = levent.size();
		if (size > 0) {
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i < size; i++) {
				sb.append("   > " + levent.get(i - 1).getS() + " :");
				long val = levent.get(i).getDate() - levent.get(i - 1).getDate();
				sb.append(val);
				sb.append(" ms \n");
			}
			if (datefinish != 0) {
				sb.append("   > " + levent.get(size - 1).getS() + " :");
				long val = datefinish - levent.get(size - 1).getDate();
				sb.append(val);
				sb.append(" ms \n");
			}

			return sb.toString();
		}
		return "no";
	}
}
