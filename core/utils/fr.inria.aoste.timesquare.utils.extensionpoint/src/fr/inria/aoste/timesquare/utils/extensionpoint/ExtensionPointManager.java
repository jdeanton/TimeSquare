/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.extensionpoint;

import java.util.ArrayList;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Bundle;

/**
 * Point Extension manager : Common code for classes that have to deal with extension points
 * in charge of calling the ExtensionRegistry to get all the declared extensions.
 * 
 * should call the init Method for each specific IExtensionManager
 */
public final class ExtensionPointManager {
	
	/**
	 * Returns a Class object according to the class name found in the {@link IConfigurationElement} using
	 * namefield as the name of the attribute in which the name of the class is to be retrieved.
	 * 
	 * The class is loaded by first retrieving the bundle/plugin that declares the extension attached to the
	 * extension point, and asking this bundle to load the class itself.
	 * 
	 * The loaded class must be a subclass of cbase (or cbase itself).
	 *  
	 * @param ice the {@link IConfigurationElement} instance
	 * @param namefield the name of the attribute to lookup in the ice object.
	 * @param cbase the base class of the class to look for.
	 * @return
	 * @throws Throwable
	 */
	public static <T> Class<? extends T> getPointExtensionClass(
			IConfigurationElement ice, String namefield, Class<T> cbase) throws Throwable {
		String classname = ice.getAttribute(namefield);
		if (classname == null)
			throw  new NullPointerException("flag :" + namefield + ": Class name is null");

		String plugin = getPluginName(ice);
		Bundle b = Platform.getBundle(plugin);
		Class<?> c = b.loadClass(classname);
		if (c==null)
			throw new NullPointerException("flag :" + namefield + ": cannot load the class "+classname+" from plugin "+plugin);

		if (!(cbase.isAssignableFrom(c)))
			throw new ClassCastException( "flag :" + namefield + ": " +classname + "cannot cast to "+ c.getName());

		return (c.asSubclass(cbase)); // cbase
	}

	public static String getPluginName(IConfigurationElement ice) {
		String s =ice.getDeclaringExtension().getContributor().getName();
		//System.out.println(s);
		return s;
	}

	/**
	 * Called by an instance of {@link IExtensionManager} to retrieve all the Configuration Element from
	 * the extension point and initialize each of them. This function calls the method 
	 * {@link IExtensionManager#initExtension(IConfigurationElement)} of the actual {@link IExtensionManager}
	 * object with the {@link IConfigurationElement} that describe one extension "attached" to the extension point.
	 * 
	 * @param extensionManager
	 */
	public static void findAllExtensions(IExtensionManager extensionManager) {
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IExtensionPoint iep = reg.getExtensionPoint(extensionManager.getExtensionPointName());
		if (iep == null)
			throw new Error("not found " + extensionManager.getExtensionPointName() + " id for Point Extention Manager");
		IConfigurationElement[] extensions = iep.getConfigurationElements();

		for (IConfigurationElement ice : extensions) {
			try {
				extensionManager.initExtension(ice);
			} catch (Throwable e) {
				System.err.println(ice + " failed ");
				addException(e, ice + "Failed", extensionManager.getPluginName());
			}
		}
		if (liststatus.size() != 0) {
			MultiStatus mst = new MultiStatus(extensionManager.getPluginName(), IStatus.ERROR, liststatus.toArray(new IStatus[liststatus.size()] ),
					"Error on Point Extension Manager :\"" + extensionManager.getExtensionPointName() + "\" init", null);
			Activator.getDefault().getLog().log(mst);
		}
	}

	static private ArrayList<IStatus> liststatus = new ArrayList<IStatus>();

	static private final void addException(Throwable e, String message, String pluginName) {
		if (message == null) {
			if (e != null)
				message = e.getMessage();
			else
				message = "UK";
		} else {	
			if (e==null)
				e= new Exception("null exception");
			if (e.getMessage() != null)
				message += e;
		}
		liststatus.add(new Status(IStatus.ERROR, pluginName, message, e));
	}
}
