/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.extensionpoint;

import org.eclipse.core.runtime.IConfigurationElement;

/**
 * Used by the Extension Point Manager to initialize specific extensions
 * 
 * @author fmallet
 * @see ExtensionPointManager
 */
public interface IExtensionManager {
	/**
	 * @return the name of the extension point of interest
	 */
	String getExtensionPointName();

	/**
	 * @return the name of the plugin from which the extension is defined, mainly used to display error messages
	 */
	String getPluginName();

	/**
	 * This method is called by the ExtensionPointManager for each specific extension and each of its configuration element
	 * @param ice
	 * @throws Throwable
	 */
	abstract void initExtension(IConfigurationElement ice) throws Throwable;
}
