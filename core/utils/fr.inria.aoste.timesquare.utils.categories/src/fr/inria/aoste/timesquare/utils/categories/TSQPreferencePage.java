/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.categories;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;

public class TSQPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public TSQPreferencePage() {
		super("TimeSquare " , PluginHelpers.getImageDescriptor("fr.inria.aoste.timesquare.utils.ui", "icons/tsq.gif")  ,GRID);
		setDescription("TimeSquare Option Page");
	}

	

	@Override
	public void init(IWorkbench workbench) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void createFieldEditors() {
		// TODO Auto-generated method stub

	}

}
