/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.pluginhelpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.views.IViewDescriptor;
import org.eclipse.ui.views.IViewRegistry;

import org.osgi.framework.Bundle;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;

public class PluginHelpers {

	/**
	 * 	Identifier for widget ( Use for test with SWTBot )
	 * 		widget.setData(IDENTIFIER_WIDGET, id);
	 */
	public static  String IDENTIFIER_WIDGET="fr.inria.aoste.timesquare.widget.identifier";
	
	private static final class ImageRegistryInitialization implements Runnable {
		public void run() {
			if (imageRegistry == null) {
				imageRegistry = new ImageRegistry(Display.getCurrent());
			}
		}
	}

	private static ImageRegistry imageRegistry = null;
	private static final String DEFAULT_IMAGE = "icons/Default.gif";

	private static ArrayList<String> list = null;

	private static HashMap<String, IConfigurationElement> plugin = null;

	/**
	 * 
	 * @param plugin
	 *            : name of plugin where is a image
	 * @param key
	 *            : relative path of the image
	 * @return Image
	 */
	public static Image getImage(String plugin, String key) {

		if (imageRegistry == null) {
			Display display = Display.getDefault();
			display.syncExec(new ImageRegistryInitialization());
		}
		String rkey = plugin + "@" + key;
		Image image = imageRegistry.get(rkey);
		if (image == null) {
			try {
				ImageDescriptor desc = getimageDescriptorFromPlugin(plugin, key);
				imageRegistry.put(rkey, desc);
				image = imageRegistry.get(rkey);
			} catch (Throwable e) {
				
			}
		}
		if ((image == null) && !key.equals(DEFAULT_IMAGE)) {
			image = getImage(Activator.PLUGIN_ID, DEFAULT_IMAGE);
		}
		return image;
	}

	private static ImageDescriptor getimageDescriptorFromPlugin(String plugin, String key) {
		ImageDescriptor desc = null;
		try {
			desc = AbstractUIPlugin.imageDescriptorFromPlugin(plugin, key);
			if (desc == null) {
				if (Platform.getBundle(Activator.PLUGIN_ID) == null) {
					// on est en dehors d'eclipse (Fonctionnel StandAlone
					
					int plulen = plugin.length();
					String f = System.getProperty("java.class.path");
					for (String s : f.split(File.pathSeparator)) {
						// System.out.println(s);
						int n = s.lastIndexOf(plugin);
						if (n != -1) {
							if (s.endsWith(".jar")) {
								// plugin is jar
								int n2 = s.indexOf(plugin + ".jar");
								// check name 
								if ((s.charAt(n + plulen) != '.') || (n2 == n + plulen)) { // (jar avec version )ou (sans version )
									System.out.println("read jar");
									desc = ImageDescriptor.createFromURL(new URL("jar:file://" + s + "!/" + key));
									return desc;
								}

							} else {
								// plugin is folder
								// check name 
								if (n != -1 && ((s.length()< n+plulen )||  s.charAt(n + plulen) != '.')) {
									
									// System.out.println("*" + s);
									// plugin en repertoire
									String s2 = s.substring(0, s.indexOf(File.separator /*"/"*/, n + 1));
									// System.out.println(s.charAt(n+plugin.length())+"s2" + s2+ " "+ plugin);
									desc = ImageDescriptor.createFromURL(new URL("file://" + s2 + "/" + key));
									return desc;
								}
								return null;
							}

						}
					}
					System.out.println("+" + plugin);
				}
			}
		} catch (Throwable e) {
			System.err.println("Img " + e);
			// e.printStackTrace();
		}
		return desc;
	}

	/**
	 * 
	 * @param plugin
	 *            : name of plugin where is a image
	 * @param key
	 *            : relative path of the image
	 * @return ImageDescriptor.createFromImage( ErrorConsole.getImage(plugin, key));
	 */
	public static ImageDescriptor getImageDescriptor(String plugin, String key) {

		if (imageRegistry == null) {
			Display display = Display.getDefault();
			display.syncExec(new ImageRegistryInitialization());
		}

		String rkey = plugin + "@" + key;
		ImageDescriptor desc = imageRegistry.getDescriptor(rkey);
		if (desc == null) {
			desc = getimageDescriptorFromPlugin(plugin, key);
			imageRegistry.put(rkey, desc);
			desc = imageRegistry.getDescriptor(rkey);
		}
		if ((desc == null) && !key.equals(DEFAULT_IMAGE)) {
			desc = getImageDescriptor(Activator.PLUGIN_ID, DEFAULT_IMAGE);
		}
		return desc;
	}

	static public Image getEditorIcon(String name) {
		Image image = null;
		IConfigurationElement ice = plugin.get(name);
		if (ice == null)
			return null;
		String s = ice.getAttribute("icon");
		String sp = ice.getContributor().getName();
		image = getImage(sp, s);
		return image;
	}

	synchronized static public ArrayList<String> listingEditor(boolean refresh) {
		if (list == null || refresh) {
			IExtensionRegistry reg = Platform.getExtensionRegistry();
			IConfigurationElement[] extensions = reg
					.getConfigurationElementsFor("org.eclipse.ui.editors");
			list = new ArrayList<String>();
			plugin = new HashMap<String, IConfigurationElement>();
			for (IConfigurationElement ice : extensions) {
				String editor = ice.getAttribute("class");
				list.add(editor);
				plugin.put(editor, ice);
			}
		}
		return list;
	}

	public static IFileStore getIFileStore(URL url) throws URISyntaxException {
		return EFS.getLocalFileSystem().getStore(url.toURI());
	}

	public static IFileStore getIFileStore(URI uri) {
		return EFS.getLocalFileSystem().getStore(uri);
	}

	public static IFileStore getIFileStore(IPath ph) {
		return EFS.getLocalFileSystem().getStore(ph);
	}

	public static URL getInstallUrl(Bundle bundle, String path) {
		try {
			URL url = bundle.getEntry(path);
			// URL url = FileLocator.find(bundle, new Path(path), null);
			url = FileLocator.resolve(FileLocator.toFileURL(url));
			// ErrorConsole.println(FileLocator.toFileURL(url).getFile());
			String ls = url.toString();
			String nls = ls.replaceAll(" ", "%20");
			if (nls.compareTo(ls) != 0) {
				url = new URL(nls);
			}
			return url;
		} catch (Throwable e) {
			ErrorConsole.printError(e);
			return null;
		}
	}

	public static IPath getIpath(AbstractUIPlugin plug) {
		return plug.getStateLocation();
	}

	static public IViewPart getShowView(String strid) {
		try {
			IWorkbench iw = PlatformUI.getWorkbench();
			if (iw == null)
				return null;
			if (iw.getActiveWorkbenchWindow() == null)
				return null;
			if (iw.getActiveWorkbenchWindow().getActivePage() == null)
				return null;
			return iw.getActiveWorkbenchWindow().getActivePage()
					.showView(strid);

		} catch (Throwable e) {
			ErrorConsole.printError(e);
			return null;
		}
	}

	static public IViewPart getCreateView(String strid) {
		try {
			IWorkbench iw = PlatformUI.getWorkbench();
			if (iw == null)
				return null;
			IViewRegistry vr = iw.getViewRegistry();
			IViewDescriptor vd = vr.find(strid);
			return vd.createView();
		} catch (Throwable e) {
			ErrorConsole.printError(e);
			return null;
		}
	}

	static public IViewPart getPackageExplorer() {
		try {
			return getShowView("org.eclipse.jdt.ui.PackageExplorer");
		} catch (Throwable e) {
			return null;
		}
	}

	public static IFile touchFile(IPath filePath) {
		if (filePath == null)
			throw new IllegalArgumentException("filePath");

		if (!filePath.toFile().exists())
			throw new IllegalArgumentException("filePath " + filePath.toString() + " do not exists");

		IPath folder = filePath.removeLastSegments(1);
		String fileName = filePath.lastSegment();
		return touchFile(folder, fileName);
	}

	/**
	 * 
	 * 
	 * @return Ifile to file f create File is not existe
	 * 
	 */
	public static IFile touchFile(IPath folder, String name) {
		try {
			IPath path = folder.append(name);
			IContainer ic = ResourcesPlugin.getWorkspace().getRoot()
					.getContainerForLocation(folder);
			System.out.println(ic);

			if (!path.toFile().exists()) {
				File f = path.toFile();
				OutputStream is = new FileOutputStream(f);
				is.flush();
				is.close();
			}
			refreshWorkspace();
			IResource ifile = ((IFolder) ic).findMember(name);
			return (IFile) ifile;

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int copy(IFileStore src, IFileStore dst, int options,
			IProgressMonitor monitor) throws CoreException {
		src.copy(dst, options, monitor);
		return 0;
	}

	public static IPath getfolder(String namefile) {
		try {
			IPath p = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(namefile);
			if (p.toFile().isDirectory())
				return p;
			// if (p.toFile().isFile())
			return p.append("..");
		} catch (Throwable e) {
			ErrorConsole.printError(e);
			return null;
		}
	}

	/*
	 * 
	 * 
	 */
	public static IPath createdir(IPath p, String folder) {
		try {
			EFS.getLocalFileSystem().getStore(p.append(folder)).mkdir(EFS.OVERWRITE, null);
			return p.append(folder);
		} catch (Throwable e) {
			ErrorConsole.printError(e);
		}
		return null;
	}

	/*public static String convertName(String name) {

		if (name == null)
			return "";
		if (name.contains("::")) {
			name = name.replaceAll("::", "_");
		}
		return name;

	}

	/**
	 * Gets the selection.
	 * 
	 * @return the selection
	 */
	static public ISelection getSelection() {
		IWorkbenchWindow iww = getActiveWorkbench();
		if (iww == null)
			return null;
		ISelectionService iss = iww.getSelectionService();
		if (iss == null)
			return null;
		return iss.getSelection();
	}

	static IWorkspaceRoot iw = null;

	static public IWorkspaceRoot getWorkspaceRoot() {
		try {
			// if (iw == null)
			{
				iw = ResourcesPlugin.getWorkspace().getRoot();
			}
			return iw;
		} catch (Throwable e) {
			return null;
		}
	}

	/***
	 * 
	 */
	public static void refreshWorkspace() {
		try {
			getWorkspaceRoot().refreshLocal(IResource.DEPTH_INFINITE, null);
			if (getWorkspaceRoot().getProject() != null)
				getWorkspaceRoot().getProject().refreshLocal(
						IResource.DEPTH_INFINITE, null);
		} catch (Throwable e) {
			ErrorConsole.printError(e);
		}
	}

	/**
	 * Gets the active workbench.
	 * 
	 * @return the active workbench
	 */
	static public IWorkbenchWindow getActiveWorkbench() {
		try {
			return PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		} catch (Throwable e) {
			ErrorConsole.printError(e);
			return null;
		}

	}

	static public IEditorPart getCourantEditor() {
		// PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().
		// getActiveEditor();
		IWorkbenchWindow iww = getActiveWorkbench();
		if (iww == null)
			return null;
		// iww.addPageListener(new IPageListener(){});

		IWorkbenchPage iwp = iww.getActivePage();
		if (iwp == null)
			return null;
		IEditorPart editorPart = (iwp.getActiveEditor());
		return editorPart; // editorPart;
	}

	static public IEditorPart getActiveEditor() {
		try {
			IWorkbenchWindow iww = getActiveWorkbench();
			if (iww == null)
				return null;
			// iww.addPageListener(new IPageListener(){});

			IWorkbenchPage iwp = iww.getActivePage();
			if (iwp == null)
				return null;
			IEditorPart editorPart = (iwp.getActiveEditor());
			if (editorPart == null)
				return null;
			if (iwp.getActivePartReference() == null)
				return null;
			String s1 = iwp.getActivePartReference().getId();
			if (editorPart.getEditorSite() == null)
				return null;
			String s2 = editorPart.getEditorSite().getId();
			if (!s1.equals(s2)) {
				return null;
			}

			return editorPart; // editorPart;
		} catch (Throwable e) {
			return null;
		}
	}

	static public ClassLoader getPluginClassLoader(String plug) {
		try {
			String s = getPluginData(plug, "Bundle-Activator");
			Bundle bdl = Platform.getBundle(plug);
			return bdl.loadClass(s).getClassLoader();
		} catch (Throwable e) {
			System.err.println(e);
		}
		return null;
	}

	static public <T> T getNewInstance(String plugin, String object,
			Class<? extends T> cls) {
		try {
			Bundle b = null;

			b = Platform.getBundle(plugin);
			if (b == null)
				return null;
			Class<?> c = b.loadClass(object);
			Class<? extends T> c2 = c.asSubclass(cls); // c2 = c
			if (c2 != null) {
				return c2.newInstance();
			}
			/*if (cls.isAssignableFrom(c)) {
				return (T) c.newInstance();
			}*/
			System.out.println("Erreur de type");
		} catch (Throwable e) {
			System.err.println(e);
		}
		return null;
	}

	static public <T> T getNewInstance(String plugin, String object,
			Class<? extends T> cls, Class<?> lst[], Object targ[]) {
		try {
			Bundle b = null;

			b = Platform.getBundle(plugin);

			Class<?> c = b.loadClass(object);
			if (cls.isAssignableFrom(c)) {
				int n = lst.length;
				int n2 = targ.length;
				if (n != n2)
					return null;
				for (int i = 0; i < n; i++) {
					if (lst[i] == null) {
						System.err.println("class " + i + " is null");
						return null;
					}
					if (targ[i] != null) {
						if (!(lst[i].isAssignableFrom(targ[i].getClass()))) {
							System.err.println("qrg " + i + " is not valid");
							return null;
						}
					}
				}
				Class<? extends T> cls2 = c.asSubclass(cls); // cls2 =c
				if (cls2 != null) {
					return cls2.getConstructor(lst).newInstance(targ);
				}
				// return (T) c.newInstance();
			}
			System.out.println("Erreur de type");
		} catch (Throwable e) {
			System.err.println(e);
		}
		return null;
	}

	static public String getPluginVersion(String plug) {
		return getPluginData(plug, "Bundle-Version");
	}

	static public String getPluginVendor(String plug) {
		return getPluginData(plug, "Bundle-Vendor");
	}

	static public String getPluginDescription(String plug) {
		return getPluginData(plug, "Bundle-Description");
	}

	static public String getPluginData(String plug, String label) {
		try {
			Bundle bdl = Platform.getBundle(plug);
			if (bdl == null)
				return "";
			if (bdl.getHeaders() == null)
				return "";
			if (bdl.getHeaders().get(label) == null)
				return "";
			String s = bdl.getHeaders().get(label).toString();
			if (s == null)
				return "";
			return s;
		} catch (Throwable e) {

		}
		return "";
	}

	static public <T> Class<? extends T> getPointExtensionClass(
			IConfigurationElement ice, String namefield, Class<T> cls) {
		try {
			Bundle b = null;
			String plugin = ice.getDeclaringExtension()
					.getNamespaceIdentifier();
			b = Platform.getBundle(plugin);
			String classname = ice.getAttribute(namefield);
			if (classname == null)
				return null;
			Class<?> c = b.loadClass(classname);
			return (c.asSubclass(cls));// throw new ClassCastException
			// /if (cls.isAssignableFrom(c)) {
			// return (Class<? extends T>) c;
			// }
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static Map<String, Object> getEcoreSaveOption()
	{	
		HashMap<String, Object> map = new HashMap<String, Object>();				
				map.put(XMLResource.OPTION_CONFIGURATION_CACHE,Boolean.TRUE);				
				map.put(XMLResource.OPTION_USE_FILE_BUFFER,Boolean.TRUE);
				map.put(XMLResource.OPTION_FORMATTED,Boolean.TRUE);
				map.put(XMLResource.OPTION_FLUSH_THRESHOLD,Integer.valueOf(1000000));
				map.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.PlatformSchemeAware());
		return map;
	}

}
