/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.utils.ui.listeners;

import java.io.Serializable;
import java.util.Comparator;

import org.eclipse.emf.common.util.URI;

public final class ComparatorURI implements Comparator<URI> ,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6852703879311189337L;

	public int compare(URI o1, URI o2) {
		try
		{
		String s1=o1.toString();
		String s2=o2.toString();
		return s1.compareTo(s2);
		}
		catch (Throwable e) {
			return 0;
		}
	}
}