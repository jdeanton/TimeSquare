/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import fr.inria.aoste.timesquare.utils.ui.ILabel;
import fr.inria.aoste.timesquare.utils.ui.idialog.ICallback4Box;
import fr.inria.aoste.timesquare.utils.ui.listeners.LabelInputModifyListener;

public class ClabelInput extends Composite implements ILabel {

	// protected Composite parents;
	protected String input = "";
	protected Label lb = null;
	protected Text tx = null;
	protected String name;
	protected ICallback4Box icb = null;
	protected int lbx = 110, txx = 250;

	@Override
	public void dispose() {

		lb.dispose();
		tx.dispose();
		// parents.dispose();
		super.dispose();
	}

	public ClabelInput(Composite parent, int style, String name) {
		super(parent, style);
		this.name = name;
		lbx = 110;
		txx = 250;
		init();
		verify();
	}

	protected ClabelInput(Composite parent, int style, String name, int lbx, int txx) {
		super(parent, style);
		// this.parents = parent;
		this.name = name;
		this.lbx = lbx;
		this.txx = txx;
		init();
		verify();
	}

	protected void init() {
		lb = new Label(this, SWT.READ_ONLY);
		lb.setText(name);
		lb.setSize(lbx, 20);
		lb.setLocation(10, 05);

		tx = new Text(this, SWT.NONE);
		tx.setToolTipText("  ");
		tx.setSize(txx, 20);
		tx.setLocation(10 + lbx, 05);
		tx.setEditable(true);
		tx.setEnabled(true);
		tx.setText("");
		tx.addModifyListener(new LabelInputModifyListener(this,tx, 0));

	}

	/* (non-Javadoc)
	 * @see fr.inria.base.widget.ILabel#getIcb()
	 */
	public ICallback4Box getIcb() {
		return icb;
	}

	public void setIcb(ICallback4Box icb) {
		this.icb = icb;
	}

	protected void verify() {

	}

	public String getValue() {
		return input;
	}

	@Override
	public void setEnabled(boolean enabled) {

		super.setEnabled(enabled);
		tx.setEnabled(enabled);
		lb.setEnabled(enabled);
	}

	public void setText(String s) {
		tx.setText(s);
	}

	public void setTextEditable(boolean b) {
		tx.setEditable(b);
		// tx.setEnabled(b);
	}

	public void setTextBackground(Color c) {
		tx.setBackground(c);
	}

	@Override
	public void addListener(int eventType, Listener listener) {

		// tx.addListener(eventType, listener);
		// super.addListener(eventType, listener);
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.base.widget.ILabel#setString(java.lang.String, int)
	 */
	public void setString(String s, int i)
	{
		if (i==0)
		{
			input =s;
		}
	}

}
