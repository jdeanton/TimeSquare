/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import fr.inria.aoste.timesquare.utils.ui.listeners.MyListernerID;


public class ClabelInputID extends ClabelInput {

	public ClabelInputID(Composite parent, int style, String name) {
		super(parent, style, name);
	}

	@Override
	protected void verify() {
		tx.addListener(SWT.Verify, new MyListernerID());
		super.verify();
	}

	@Override
	public String getValue() {
		if (input == null)
			return "0";
		if (input.compareTo("") == 0)
			return "0";
		return input;
	}

}
