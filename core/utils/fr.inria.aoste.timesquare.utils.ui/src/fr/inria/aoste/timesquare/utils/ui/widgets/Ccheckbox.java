/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.ui.idialog.ICallback4Box;


public class Ccheckbox extends Composite {
	private final class CcheckboxListenerMouseUp implements Listener {
		public void handleEvent(Event ev) {
		if (ev.type == SWT.MouseUp) {
			bol = button.getSelection();
			try {
				computenewvalue(bol);
				if (icb != null)
					icb.validation();
			} catch (Throwable ex) {
				ErrorConsole.printError(ex);
			}
		}
		}
	}

	// private CCombo c=null;
	protected ICallback4Box icb = null;

	private Label l;

	private int value = -1;

	private Composite parents;

	private boolean bol;
	private Button button;

	public Ccheckbox(Composite parent, String namein, String tooltipin, boolean b) {
		super(parent, SWT.NONE);

		parents = this;
		setSize(520, 55);
		setLayout(null);// new FillLayout ());
		parents.setLayout(null);

		parent.setSize(400, 40);
		l = new Label(parents, SWT.READ_ONLY);
		l.setText(namein);
		l.setSize(80, 20);
		l.setLocation(10, 5);
		l.setToolTipText("");

		button = new Button(parents, SWT.CHECK);
		button.setToolTipText(" ... ");
		button.setText(tooltipin);
		button.setLocation(90, 5);
		button.setSize(100, 20);
		button.setSelection(b);
		button.setSelection(b);
		bol = b;

		button.addListener(SWT.MouseUp, new CcheckboxListenerMouseUp());

		value = 0;
	}

	public void removeAll() {
		// if (c!=null)
		// c.removeAll();
	}

	@Override
	public void addListener(int eventType, Listener listener) {
		// if (c!=null)
		// c.addListener(eventType, listener);

	}

	public void updatelabel(String s) {

	}

	public void updateTooltips(String s) {

	}

	public boolean getSelection() {
		return bol;

	}

	@Override
	public void dispose() {
		if (button != null)
			button.dispose();
		// if (c!=null)
		// c.dispose();
		if (l != null)
			l.dispose();
		super.dispose();
	}

	public int getValue() {
		return value;
	}

	protected int computenewvalue(boolean b) {
		return 0;
	}

	public ICallback4Box getIcb() {
		return icb;
	}

	public void setIcb(ICallback4Box icb) {
		this.icb = icb;
	}

}