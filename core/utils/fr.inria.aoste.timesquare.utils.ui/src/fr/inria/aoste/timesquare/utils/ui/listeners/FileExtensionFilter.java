/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.ui.listeners;

import java.util.ArrayList;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class FileExtensionFilter extends ViewerFilter {

	public ArrayList<String> keepext = null;

	public FileExtensionFilter(String[] _keepext) {
		super();
		this.keepext = new ArrayList<String>();
		for (String s : _keepext) {
			keepext.add(s);
		}
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {

		if (keepext.size() == 0)
			return true;
		// Project.close --> ignored
		if (element instanceof IProject && !((IProject) element).isOpen()) {
			return false;
		}
		/*if (element instanceof IFolder )
		{
			return true;
		}*/
		if (element instanceof IContainer) { // i.e. IProject, IFolder
			try {
				IResource[] resources = ((IContainer) element).members();
				for (int i = 0; i < resources.length; i++) {
					if (select(viewer, parentElement, resources[i]))
						return true;
				}
			} catch (CoreException e) {
			}
			return false;
		}
		if (element instanceof IFile) {
			String ext = ((IFile) element).getFileExtension();
			if (keepext.indexOf(ext) != -1) {
				return true;
			}
			return false;
		}

		return true;
	}

}

/*
 * 
 * org.eclipse.pde.internal.ui.util.FileExtensionFilter 
 * public boolean select(Viewer viewer, Object parent, Object element) {
		if (element instanceof IFile) {
			return ((IFile) element).getName().toLowerCase(Locale.ENGLISH).endsWith("." + fTargetExtension); //$NON-NLS-1$
		}

		if (element instanceof IProject && !((IProject) element).isOpen())
			return false;

		if (element instanceof IContainer) { // i.e. IProject, IFolder
			try {
				IResource[] resources = ((IContainer) element).members();
				for (int i = 0; i < resources.length; i++) {
					if (select(viewer, parent, resources[i]))
						return true;
				}
			} catch (CoreException e) {
			}
		}
		return false;
	}*/
