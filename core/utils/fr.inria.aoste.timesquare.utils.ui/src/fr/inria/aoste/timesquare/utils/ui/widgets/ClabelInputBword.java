/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import fr.inria.aoste.timesquare.utils.ui.idialog.IInputBword;
import fr.inria.aoste.timesquare.utils.ui.listeners.LabelInputModifyListener;
import fr.inria.aoste.timesquare.utils.ui.listeners.MyListernerBinary;


public class ClabelInputBword extends ClabelInput implements IInputBword {

/*	private final class LabelInputBwordModifyListener implements ModifyListener {
		public void modifyText(ModifyEvent e) {
			inp2 = tx2.getText();
			if (icb != null)
				icb.validation();
		}
	}*/

	protected String inp2 = "";

	protected Text tx2 = null;

	@Override
	public void dispose() {
		tx2.dispose();
		super.dispose();
	}

	public ClabelInputBword(Composite parent, int style, String name) {
		super(parent, style, name, 110, 100);
		init2();
	}

	protected void init2() {

		tx.addListener(SWT.Verify, new MyListernerBinary());
		tx.setToolTipText(" Initialisation ");
		tx2 = new Text(this, SWT.NONE);
		tx2.setToolTipText(" Periodic ");
		tx2.setSize(250, 20);
		tx2.setLocation(230, 05);
		tx2.setEditable(true);
		tx2.setEnabled(true);
		tx2.setText("");
		tx2.addModifyListener(new LabelInputModifyListener(this,tx2, 1));
				//new LabelInputBwordModifyListener());
		tx2.addListener(SWT.Verify, new MyListernerBinary());

	}

	// tx.get(value();
	/* (non-Javadoc)
	 * @see fr.inria.ctrte.baseuml.core.IInputBword#getValue()
	 */
	@Override
	public String getValue() {
		String inputs = "0b" + input + "(" + inp2 + ")";
		return inputs;

	}
	public void setString(String s, int i)
	{
		super.setString(s, i);
		if (i==1)
		{
			inp2 =s;
		}
		
	}

}
