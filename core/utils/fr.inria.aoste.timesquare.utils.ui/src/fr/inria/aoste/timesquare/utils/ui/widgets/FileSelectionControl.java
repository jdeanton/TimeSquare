/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.ui.widgets;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.PixelConverter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import fr.inria.aoste.timesquare.utils.ui.listeners.FileExtensionFilter;
import fr.inria.aoste.timesquare.utils.ui.listeners.SelectionFile;

public class FileSelectionControl implements SelectionListener, ModifyListener {

	private IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();

	private String[] extensions;
	private Group group;
	private Text textField;
	private Button browseButton;
	private IFile value;
	//private String fileName;
	private String titleLabel;

	public FileSelectionControl(Composite parent, String label, String[] extensions) {
		this.extensions = extensions.clone();
		this.titleLabel = label;
		Font font = parent.getFont();
		
		group = new Group(parent, SWT.NONE);
		group.setText(label);
		group.setFont(font);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.makeColumnsEqualWidth = false;
		group.setLayout(layout);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		group.setLayoutData(gd);

		textField = new Text(group, SWT.SINGLE | SWT.BORDER);
		GridData gd2 = new GridData();
		gd2.horizontalAlignment = SWT.FILL;
		gd2.grabExcessHorizontalSpace = true;
		textField.setLayoutData(gd2);
		
		browseButton = new Button(group, SWT.PUSH);
		browseButton.setFont(parent.getFont());
		browseButton.setText("Browse...");
		GridData gd1 = new GridData();
		browseButton.setLayoutData(gd1);
		PixelConverter converter= new PixelConverter(browseButton);
		int buttonWidth= converter.convertHorizontalDLUsToPixels(IDialogConstants.BUTTON_WIDTH);
		int buttonWidthHint = Math.max(buttonWidth, browseButton.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
		gd1.widthHint = buttonWidthHint;
		gd1.horizontalAlignment = GridData.FILL;	 
		
		textField.addModifyListener(this);
		browseButton.addSelectionListener(this);

	}
	
	@Override
	public void modifyText(ModifyEvent e) {
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		this.handleBrowseButtonSelected();
	}

	private void handleBrowseButtonSelected() {
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
				new Shell(), new WorkbenchLabelProvider(),
				new WorkbenchContentProvider());
		dialog.setInput(workspaceRoot);
		dialog.setAllowMultiple(false);
		dialog.setDoubleClickSelects(false);
		dialog.setTitle(titleLabel);
		dialog.setMessage("message");
		dialog.setInitialSelection(value);
		// dialog.setSorter(new ResourceSorter(ResourceSorter.NAME));
		dialog.addFilter(new FileExtensionFilter(extensions));
		dialog.setValidator(new SelectionFile());
		if (dialog.open() == Window.CANCEL) {
			return;
		}
		Object[] results = dialog.getResult();
		if (results != null) {
			if (results.length != 0) {
				if (results[0] instanceof IFile) {
					setSelectedFile((IFile)results[0]);
				}
			}
		}
	}

	private void setSelectedFile(IFile file) {
		if (file == null) {
			//this.fileName = null;
			textField.setText("");
		}
		else {
			String fileName = file.getFullPath().toOSString();
			textField.setText(fileName);
		}
	}

	public void setSelectedFile(String fileName) {
		//this.fileName = fileName;
		textField.setText(fileName);
	}
	
	public String getSelectedFile() {
		return textField.getText();
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		widgetSelected(e);
	}

}
