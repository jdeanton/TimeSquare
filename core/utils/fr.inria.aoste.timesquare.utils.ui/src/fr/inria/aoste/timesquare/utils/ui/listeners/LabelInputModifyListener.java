/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.utils.ui.listeners;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Text;

import fr.inria.aoste.timesquare.utils.ui.ILabel;



public final class LabelInputModifyListener implements ModifyListener {
	
	ILabel ci=null;
	Text tx=null;
	int i=0;
	public  LabelInputModifyListener(ILabel _ci, Text _tx, int _i) {
		super();
		ci=_ci;
		tx=_tx;
		i=_i;
		// TODO Auto-generated constructor stub
	}

	public void modifyText(ModifyEvent e) {
		ci.setString(tx.getText(), i);
		if (ci.getIcb() != null)
			ci.getIcb().validation();
	}
}