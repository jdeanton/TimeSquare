/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.utils.ui.listeners;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

public final class SelectionFile implements ISelectionStatusValidator {
	public IStatus validate(Object[] selection) {
		boolean enableOK = false;
		if (selection.length != 0) {
			if (!(selection[0] instanceof IContainer)) {
				enableOK = true;
			}
		}
		return enableOK ?
				new Status(IStatus.OK, "org.eclipse.emf.common.ui", 0, "", null) :
				new Status(IStatus.ERROR, "org.eclipse.emf.common.ui", 0, "", null);
	}
}