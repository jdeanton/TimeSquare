/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.ui.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.ui.IList;
import fr.inria.aoste.timesquare.utils.ui.idialog.IComboClock;
import fr.inria.aoste.timesquare.utils.ui.listeners.IntListener;


public class ClabelcomboClock extends Clabelcombo implements IComboClock  {

	private final class LabelComboClockModifyListener implements ModifyListener {
		public void modifyText(ModifyEvent e) {
			input = tx.getText();			
		}
	}

	public boolean isActiveGroup() {
		return activegroup;
	}

	private boolean extendby = false;
	private boolean activegroup = false;
	private Label l2;
	private Text tx;
	private String input;

	public ClabelcomboClock(Composite parent, String namein, String tooltipin,
			IList lin) {
		super(parent, namein, tooltipin, lin);
		activegroup = false;
	}

	public ClabelcomboClock(Composite parent, String namein, String tooltipin,
			IList lin, boolean b) {
		super(parent, namein, tooltipin, lin);
		activegroup = b;
	}

	public boolean isExtendby() {
		return extendby;
	}

	public void setExtendby(boolean extendby) {
		this.extendby = extendby;
		l2.setVisible(extendby && activegroup);
		tx.setVisible(extendby && activegroup);
		l2.redraw();
		tx.redraw();
	}

	@Override
	public void dispose() {
		l2.dispose();
		tx.dispose();
		super.dispose();
	}

	@Override
	public void redraw() {
		l2.redraw();
		tx.redraw();
		super.redraw();
	}

	@Override
	public void pack() {
		l2.pack();
		tx.pack();
		super.pack();
	}

	@Override
	public void update() {
		l2.update();
		tx.update();
		super.update();
	}

	@Override
	public int make() {
		super.make();

		l2 = new Label(this, SWT.FILL);
		l2.setText(" by ");
		l2.setSize(30, 20);
		l2.setLocation(385, 05);
		l2.setVisible(false);

		tx = new Text(this, SWT.NONE);
		tx.setToolTipText(" ");
		tx.setSize(50, 20);
		tx.setLocation(425, 05);
		tx.setEditable(true);
		tx.setEnabled(true);
		tx.setText("");
		tx.addModifyListener(new LabelComboClockModifyListener());
		tx.addListener(SWT.Verify, new IntListener());
		tx.setVisible(false);
		return 1;
	}

	/* (non-Javadoc)
	 * @see fr.inria.ctrte.baseuml.core.IComboClock#getValueString()
	 */
	@Override
	public String getValueString() {
		try {
			if (extendby == true)
				if (input != null) {
					return super.getValueString() + " by " + input + " ";
				}
			return super.getValueString();
		} catch (Throwable ex) {
			ErrorConsole.printError(ex);
			return null;
		}

	}

	/* (non-Javadoc)
	 * @see fr.inria.ctrte.baseuml.core.IComboClock#getValueObject()
	 */
	@Override
	public Object getValueObject() {
		try {
			int value = getValue();
			if (value == -1)
				return null;
			if (li == null)
				return null;
			return li.getC(value);

		} catch (Throwable ex) {
			ErrorConsole.printError(ex);
			return null;
		}

	}

}
