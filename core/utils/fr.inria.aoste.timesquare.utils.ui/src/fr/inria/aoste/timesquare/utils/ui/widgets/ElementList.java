/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.ui.widgets;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class ElementList<T> implements IStructuredContentProvider {

	private ArrayList<T> elements = new ArrayList<T>();

	public ElementList() {

	}

	public ArrayList<T> getElements() {
		return elements;
	}

	public void addElement(T element) {
		elements.add(elements.size(), element);
	}

	public void removeElement(T element) {
		elements.remove(element);
	}

	public int moveElementUp(T element) {
		int index = elements.indexOf(element);
		if (index > 0) {
			elements.remove(element);
			elements.add(index - 1, element);
		}
		return index;
	}

	public int moveElementDown(T element) {
		int index = elements.indexOf(element);
		if (index >= 0 && index < elements.size() - 1) {
			elements.remove(element);
			elements.add(index + 1, element);
		}
		return index;
	}

	public boolean contains(Object element) {
		return elements.contains(element);
	}

	public int size() {
		return elements.size();
	}

	public boolean isEmpty() {
		return elements.isEmpty();
	}

	/**
	 * org.eclipse.jface.viewers.IContentProvider.inputChanged(Viewer, Object, Object)
	 * 
	 * */
	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
	}

	/***
	 * org.eclipse.jface.viewers.IContentProvider.dispose()
	 */
	public void dispose() {

	}

	/***
	 * org.eclipse.jface.viewers.IStructuredContentProvider.getElements(Object)
	 */
	public Object[] getElements(Object parent) {
		return elements.toArray();
	}
}
