/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.exceptions;

public class SimulationState implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8890022609829108059L;
	private String name = null;
	private String value = null;
	private SimulationState child[] = null;
	private transient Object ressource = null;

	public SimulationState(String name, String value, SimulationState[] child) {
		super();
		this.name = name;
		this.value = value;
		if (child !=null)
			this.child = child.clone();
		else
			this.child= new SimulationState[]{};
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SimulationState(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public SimulationState[] getChild() {
		return child.clone();
	}

	public void setChild(SimulationState[] child) {
		if ( child!=null)
			this.child = child.clone();
		else
			this.child= new SimulationState[]{};
	}

	public Object getRessource() {
		return ressource;
	}

	public void setRessource(Object ressource) {
		this.ressource = ressource;
	}

	@Override
	public String toString() {

		return toString("\t");
	}

	public String toString(String s) {
		StringBuilder sb = new StringBuilder();
		String arg = s + "\t";
		sb = sb.append(s).append(name).append(":\n").append(arg).append(value);
		if (child != null) {

			for (SimulationState ss : child) {
				sb.append("\n").append(ss.toString(arg));
			}
		}
		return sb.toString();
	}

}
