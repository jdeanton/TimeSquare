/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;

public class ExceptionSimulation extends Exception {

	private SimulationState listconstraint[]=new SimulationState[]{};
	private ClockState listClock[]=new ClockState[]{};

	public ExceptionSimulation() {
		super();

	}

	public ExceptionSimulation(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	public ExceptionSimulation(String arg0) {
		super(arg0);

	}

	public ExceptionSimulation(Throwable arg0) {
		super(arg0);

	}

	public final ClockState[] getListClock() {
		return listClock.clone();
	}

	public final void setListClock(ClockState[] listClock) {
		if (listClock!=null)
			this.listClock = listClock.clone();
		else
			this.listClock= new ClockState[]{};
	}

	public SimulationState[] getListconstraint() {
		return listconstraint.clone();
	}

	public void setListconstraint(SimulationState[] listconstraint) {
		if( listconstraint!=null)
			this.listconstraint = listconstraint.clone();
		else
			this.listconstraint = new SimulationState[] {};
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -45617867897370358L;

	@Override
	public void printStackTrace(PrintStream s) {
		synchronized (s) {
			s.println(this);
			s.println("Constraint:");
			for (int i = 0; i < listconstraint.length; i++)
				s.println(listconstraint[i].toString("\t"));
			s.println("Clock:");
			for (int i = 0; i < listClock.length; i++)
				s.println(listClock[i].toString());

			/*	StackTraceElement[] trace = getStackTrace();
			        for (int i=0; i < trace.length; i++)
			            s.println("\tat " + trace[i]);*/
			Throwable ourCause = getCause();
			if (ourCause != null) {
				s.println("Caused by: " + ourCause);
				ourCause.printStackTrace(s);
			}
		}
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		synchronized (s) {
			s.println(this);
			s.println("Constraint:");
			for (int i = 0; i < listconstraint.length; i++)
				s.println(listconstraint[i].toString("        "));
			s.println("Clock:");
			for (int i = 0; i < listClock.length; i++)
				s.println(listClock[i].toString());
			/*StackTraceElement[] trace = getStackTrace();
			for (int i=0; i < trace.length; i++)
			    s.println("\tat " + trace[i]);*/
			Throwable ourCause = getCause();
			if (ourCause != null) {
				s.println("Caused by: " + ourCause);
				ourCause.printStackTrace(s);
			}
		}
	}

}
