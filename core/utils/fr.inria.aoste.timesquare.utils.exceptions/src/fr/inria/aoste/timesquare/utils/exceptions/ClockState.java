/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.exceptions;

public class ClockState  implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6171991892813728231L;
	private String name;
	private int tick;
	private boolean death;
	private boolean active;
	private boolean constraint=true;

	public synchronized final boolean isConstraint() {
		return constraint;
	}

	public ClockState(String _name, int _tick, boolean _death, boolean _active ) {
		super();
		this.name = _name;
		this.tick = _tick;
		this.death = _death;
		this.active = _active;
	}
	
	public ClockState(String _name, int _tick, boolean _death, boolean _active , boolean _constraint) {
		super();
		this.name = _name;
		this.tick = _tick;
		this.death = _death;
		this.active = _active;
		this.constraint = _constraint ;
	}

	public final String getName() {
		return name;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final int getTick() {
		return tick;
	}

	public final void setTick(int tick) {
		this.tick = tick;
	}

	@Override
	public String toString() {
		String deathstr = death ? " Death " : "";
		String status = active ? " Sustain " : "";
		if ( !constraint)
		{
			return "       (Clock \"" +name +"\"is free)  "+ tick ;
		}
		return "       " + name + "      :" + tick + deathstr + " " + status;
	}

}
