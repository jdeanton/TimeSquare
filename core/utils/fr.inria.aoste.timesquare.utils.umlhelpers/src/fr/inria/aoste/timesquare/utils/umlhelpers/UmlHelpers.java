/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.utils.umlhelpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.ProfileApplication;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.util.UMLUtil;

public class UmlHelpers {

	 /**
     * Check if the StereotypedElement has the given stereotype.
     * 
     * @param stereotypeName The name of the stereotype to find.
     * @return the Stereotype application EObject.
     */
    public static EObject hasStereotype(Element elt, EClass stereotypeClass) {
    	EObject stereotypeApplication = null;
    	
		// Stereotype parsing
		Iterator<EObject> stAppIt = elt.getStereotypeApplications().iterator();
		while (stAppIt.hasNext() && (stereotypeApplication == null)) {
			EObject stApp = stAppIt.next();
			if (stApp.eClass().getEAllSuperTypes().contains(stereotypeClass) || (stApp.eClass().equals(stereotypeClass))) {
				stereotypeApplication = stApp;
			}
		}
		
		return stereotypeApplication;
    }
	
	
	
	
	/***
	 * 
	 * Clone de la methode : ElementOperations.applyStereotype(element, stereotype) Changement : gestion d'erreur detaille
	 * 
	 * @param element
	 * @param stereotype
	 * @return
	 */

	public static EObject applyStereotype(Element element, Stereotype stereotype) {
		if (element == null)
			throw new IllegalArgumentException(String.valueOf(stereotype) + ": Base is null");

		// EClass definition =
		getDefinition(element, stereotype);

		// if (definition == null ||
		if (getExtension(element, stereotype) == null) {
			throw new IllegalArgumentException(String.valueOf(stereotype) + ": Extension is Null");
		}
		if (element.getStereotypeApplication(stereotype) != null) {

			throw new IllegalArgumentException(String.valueOf(stereotype) + ": StereotypeApplciation is not null ");
		}
		// return ElementOperations.applyStereotype(element, stereotype);
		return element.applyStereotype(stereotype);
		// applyStereotype(element, definition);
	}

	protected static EClass getDefinition(Element element, Stereotype stereotype) {

		if (stereotype == null)
			throw new IllegalArgumentException("Stereotype is Null ");

		Profile profile = stereotype.getProfile();
		if (profile == null)
			throw new IllegalArgumentException(String.valueOf(stereotype) + ": Profil is null");

		org.eclipse.uml2.uml.Package package_ = element.getNearestPackage();

		if (package_ == null)
			throw new IllegalArgumentException(String.valueOf(stereotype) + ": Package is null");
		ProfileApplication profileApplication = package_.getProfileApplication(profile, true);

		if (profileApplication == null)
			throw new IllegalArgumentException(String.valueOf(stereotype) + ": ProfileApplication is null");

		ENamedElement appliedDefinition = profileApplication.getAppliedDefinition(stereotype);

		if (appliedDefinition instanceof EClass) {
			EClass eClass = (EClass) appliedDefinition;

			if (!eClass.isAbstract()) {
				return eClass;
			}
			throw new IllegalArgumentException(String.valueOf(stereotype) + ": Definition is  abstract Eclass");

		}
		throw new IllegalArgumentException(String.valueOf(stereotype) + ": Definition is not EClass ");
	}

	protected static Extension getExtension(Element element, Stereotype stereotype) {

		for (Property attribute : stereotype.getAllAttributes()) {
			Association association = attribute.getAssociation();

			if (association instanceof Extension) {
				String name = attribute.getName();

				if (!UML2Util.isEmpty(name)
						&& name.startsWith(Extension.METACLASS_ROLE_PREFIX)) {

					Type type = attribute.getType();

					if (type instanceof org.eclipse.uml2.uml.Class) {
						EClassifier eClassifier = getEClassifier((org.eclipse.uml2.uml.Class) type);

						if (eClassifier != null
								&& eClassifier.isInstance(element)) {

							return (Extension) association;
						}
					}
				}
			}
		}
		throw new IllegalArgumentException(String.valueOf(stereotype) + ": Stereotype  is not for  " + element.eClass().getName());
		// return null;
	}

	/*	protected static EObject applyStereotype(Element element, EClass definition) {
			
			return StereotypeApplicationHelper.INSTANCE.applyStereotype(element,definition);
		}*/

	/*	
		protected EList<EObject> getContainmentList(Element element,
				EClass definition) {

			if (element.eResource() != null) {
				return element.eResource().getContents();
			}
			return null;
		}

		public EObject applyStereotype(Element element, EClass definition) {
			EObject stereotypeApplication = EcoreUtil.create(definition);

			CacheAdapter.INSTANCE.adapt(stereotypeApplication);

			EList<EObject> containmentList = getContainmentList(element,
				definition);
			if (containmentList != null) {
				containmentList.add(stereotypeApplication);
			}
			setBaseElement(stereotypeApplication, element);

			return stereotypeApplication;
		}
		*/

	protected static EClassifier getEClassifier(Type type) {
		Resource eResource = type.eResource();

		if (eResource == null
				|| !UMLResource.UML_METAMODEL_URI.equals(String.valueOf(eResource
				.getURI()))) {

			org.eclipse.uml2.uml.Package package_ = type.getNearestPackage();

			if (package_ != null) {
				EPackage ePackage = getEPackage(package_);

				if (ePackage != null) {
					return ePackage.getEClassifier(type.getName());
				}
			}

			return null;
		} else {
			return UMLPackage.eINSTANCE.getEClassifier(type.getName());
		}
	}

	protected static EClassifier getEClassifier(
			org.eclipse.uml2.uml.Class metaclass) {
		return getEClassifier((Type) metaclass);
	}

	protected static EPackage getEPackage(org.eclipse.uml2.uml.Package package_) {
		return EPackage.Registry.INSTANCE.getEPackage((String) UMLUtil.getTaggedValue(
				package_, UMLUtil.PROFILE__ECORE + NamedElement.SEPARATOR
				+ UMLUtil.STEREOTYPE__E_PACKAGE, UMLUtil.TAG_DEFINITION__NS_URI));
	}
	
	public ArrayList<String> getPropertyNameToDisplayInXtext(EObject anObject){
		ArrayList<String> res = new ArrayList<String>();
		if (anObject instanceof Property){
			Property aProperty = (Property)anObject;
			if (aProperty.getType() != null)
			{
				for(Element e : aProperty.getType().getOwnedElements()){
					if (e instanceof Port){
						res.add(((Port) e).getName());
					}
				}
			}
		}
		
		return res;
	}
	
	public ArrayList<EObject> getPropertyToDisplayInXtext(EObject anObject){
		ArrayList<EObject> res = new ArrayList<EObject>();
		if (anObject instanceof Property){
			Property aProperty = (Property)anObject;
			if (aProperty.getType() != null)
			{
				for(Element e : aProperty.getType().getOwnedElements()){
					if (e instanceof Port){
						res.add(e);
					}
				}
			}
		}
		
		return res;
	}
	
/*	public final static  Map<String, Object> option = getDefaultOption(); 
	
	private static Map<String, Object> getDefaultOption()
	{
	
	HashMap<String, Object> map = new HashMap<String, Object>();				
				map.put(XMLResource.OPTION_CONFIGURATION_CACHE,Boolean.TRUE);				
				map.put(XMLResource.OPTION_USE_FILE_BUFFER,Boolean.TRUE);
				map.put(XMLResource.OPTION_FORMATTED,Boolean.TRUE);
				map.put(XMLResource.OPTION_FLUSH_THRESHOLD,Integer.valueOf(1000000));				
	return  Collections.unmodifiableMap(map);
	
	}*/
	
/*	public static Map<String, Object> getOption()
	{	
		HashMap<String, Object> map = new HashMap<String, Object>();				
				map.put(XMLResource.OPTION_CONFIGURATION_CACHE,Boolean.TRUE);				
				map.put(XMLResource.OPTION_USE_FILE_BUFFER,Boolean.TRUE);
				map.put(XMLResource.OPTION_FORMATTED,Boolean.TRUE);
				map.put(XMLResource.OPTION_FLUSH_THRESHOLD,Integer.valueOf(1000000));
				map.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.PlatformSchemeAware());
		return map;
	}*/

}
