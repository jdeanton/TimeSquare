package fr.inria.aoste.timesquare.duration.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.inria.aoste.timesquare.duration.xtext.services.DurationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDurationParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'reference'", "'ref'", "'from'", "'start'", "'to'", "'end'", "'DurationModel'", "'{'", "'}'", "'import'", "'Timer'", "';'", "'delay'", "'='", "'ComputationDuration'", "'CommunicationDuration'", "'-'", "'displayedOverlap'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDurationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDurationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDurationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDuration.g"; }


    	private DurationGrammarAccess grammarAccess;

    	public void setGrammarAccess(DurationGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDurationModel"
    // InternalDuration.g:52:1: entryRuleDurationModel : ruleDurationModel EOF ;
    public final void entryRuleDurationModel() throws RecognitionException {
        try {
            // InternalDuration.g:53:1: ( ruleDurationModel EOF )
            // InternalDuration.g:54:1: ruleDurationModel EOF
            {
             before(grammarAccess.getDurationModelRule()); 
            pushFollow(FOLLOW_1);
            ruleDurationModel();

            state._fsp--;

             after(grammarAccess.getDurationModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDurationModel"


    // $ANTLR start "ruleDurationModel"
    // InternalDuration.g:61:1: ruleDurationModel : ( ( rule__DurationModel__Group__0 ) ) ;
    public final void ruleDurationModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:65:2: ( ( ( rule__DurationModel__Group__0 ) ) )
            // InternalDuration.g:66:2: ( ( rule__DurationModel__Group__0 ) )
            {
            // InternalDuration.g:66:2: ( ( rule__DurationModel__Group__0 ) )
            // InternalDuration.g:67:3: ( rule__DurationModel__Group__0 )
            {
             before(grammarAccess.getDurationModelAccess().getGroup()); 
            // InternalDuration.g:68:3: ( rule__DurationModel__Group__0 )
            // InternalDuration.g:68:4: rule__DurationModel__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DurationModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDurationModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDurationModel"


    // $ANTLR start "entryRuleImportStatement"
    // InternalDuration.g:77:1: entryRuleImportStatement : ruleImportStatement EOF ;
    public final void entryRuleImportStatement() throws RecognitionException {
        try {
            // InternalDuration.g:78:1: ( ruleImportStatement EOF )
            // InternalDuration.g:79:1: ruleImportStatement EOF
            {
             before(grammarAccess.getImportStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleImportStatement();

            state._fsp--;

             after(grammarAccess.getImportStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalDuration.g:86:1: ruleImportStatement : ( ( rule__ImportStatement__Group__0 ) ) ;
    public final void ruleImportStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:90:2: ( ( ( rule__ImportStatement__Group__0 ) ) )
            // InternalDuration.g:91:2: ( ( rule__ImportStatement__Group__0 ) )
            {
            // InternalDuration.g:91:2: ( ( rule__ImportStatement__Group__0 ) )
            // InternalDuration.g:92:3: ( rule__ImportStatement__Group__0 )
            {
             before(grammarAccess.getImportStatementAccess().getGroup()); 
            // InternalDuration.g:93:3: ( rule__ImportStatement__Group__0 )
            // InternalDuration.g:93:4: rule__ImportStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleDuration"
    // InternalDuration.g:102:1: entryRuleDuration : ruleDuration EOF ;
    public final void entryRuleDuration() throws RecognitionException {
        try {
            // InternalDuration.g:103:1: ( ruleDuration EOF )
            // InternalDuration.g:104:1: ruleDuration EOF
            {
             before(grammarAccess.getDurationRule()); 
            pushFollow(FOLLOW_1);
            ruleDuration();

            state._fsp--;

             after(grammarAccess.getDurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDuration"


    // $ANTLR start "ruleDuration"
    // InternalDuration.g:111:1: ruleDuration : ( ( rule__Duration__Alternatives ) ) ;
    public final void ruleDuration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:115:2: ( ( ( rule__Duration__Alternatives ) ) )
            // InternalDuration.g:116:2: ( ( rule__Duration__Alternatives ) )
            {
            // InternalDuration.g:116:2: ( ( rule__Duration__Alternatives ) )
            // InternalDuration.g:117:3: ( rule__Duration__Alternatives )
            {
             before(grammarAccess.getDurationAccess().getAlternatives()); 
            // InternalDuration.g:118:3: ( rule__Duration__Alternatives )
            // InternalDuration.g:118:4: rule__Duration__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Duration__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDurationAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDuration"


    // $ANTLR start "entryRuleTimer"
    // InternalDuration.g:127:1: entryRuleTimer : ruleTimer EOF ;
    public final void entryRuleTimer() throws RecognitionException {
        try {
            // InternalDuration.g:128:1: ( ruleTimer EOF )
            // InternalDuration.g:129:1: ruleTimer EOF
            {
             before(grammarAccess.getTimerRule()); 
            pushFollow(FOLLOW_1);
            ruleTimer();

            state._fsp--;

             after(grammarAccess.getTimerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTimer"


    // $ANTLR start "ruleTimer"
    // InternalDuration.g:136:1: ruleTimer : ( ( rule__Timer__Group__0 ) ) ;
    public final void ruleTimer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:140:2: ( ( ( rule__Timer__Group__0 ) ) )
            // InternalDuration.g:141:2: ( ( rule__Timer__Group__0 ) )
            {
            // InternalDuration.g:141:2: ( ( rule__Timer__Group__0 ) )
            // InternalDuration.g:142:3: ( rule__Timer__Group__0 )
            {
             before(grammarAccess.getTimerAccess().getGroup()); 
            // InternalDuration.g:143:3: ( rule__Timer__Group__0 )
            // InternalDuration.g:143:4: rule__Timer__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Timer__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTimerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTimer"


    // $ANTLR start "entryRuleComputationDuration"
    // InternalDuration.g:152:1: entryRuleComputationDuration : ruleComputationDuration EOF ;
    public final void entryRuleComputationDuration() throws RecognitionException {
        try {
            // InternalDuration.g:153:1: ( ruleComputationDuration EOF )
            // InternalDuration.g:154:1: ruleComputationDuration EOF
            {
             before(grammarAccess.getComputationDurationRule()); 
            pushFollow(FOLLOW_1);
            ruleComputationDuration();

            state._fsp--;

             after(grammarAccess.getComputationDurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComputationDuration"


    // $ANTLR start "ruleComputationDuration"
    // InternalDuration.g:161:1: ruleComputationDuration : ( ( rule__ComputationDuration__Group__0 ) ) ;
    public final void ruleComputationDuration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:165:2: ( ( ( rule__ComputationDuration__Group__0 ) ) )
            // InternalDuration.g:166:2: ( ( rule__ComputationDuration__Group__0 ) )
            {
            // InternalDuration.g:166:2: ( ( rule__ComputationDuration__Group__0 ) )
            // InternalDuration.g:167:3: ( rule__ComputationDuration__Group__0 )
            {
             before(grammarAccess.getComputationDurationAccess().getGroup()); 
            // InternalDuration.g:168:3: ( rule__ComputationDuration__Group__0 )
            // InternalDuration.g:168:4: rule__ComputationDuration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getComputationDurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComputationDuration"


    // $ANTLR start "entryRuleCommunicationDuration"
    // InternalDuration.g:177:1: entryRuleCommunicationDuration : ruleCommunicationDuration EOF ;
    public final void entryRuleCommunicationDuration() throws RecognitionException {
        try {
            // InternalDuration.g:178:1: ( ruleCommunicationDuration EOF )
            // InternalDuration.g:179:1: ruleCommunicationDuration EOF
            {
             before(grammarAccess.getCommunicationDurationRule()); 
            pushFollow(FOLLOW_1);
            ruleCommunicationDuration();

            state._fsp--;

             after(grammarAccess.getCommunicationDurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCommunicationDuration"


    // $ANTLR start "ruleCommunicationDuration"
    // InternalDuration.g:186:1: ruleCommunicationDuration : ( ( rule__CommunicationDuration__Group__0 ) ) ;
    public final void ruleCommunicationDuration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:190:2: ( ( ( rule__CommunicationDuration__Group__0 ) ) )
            // InternalDuration.g:191:2: ( ( rule__CommunicationDuration__Group__0 ) )
            {
            // InternalDuration.g:191:2: ( ( rule__CommunicationDuration__Group__0 ) )
            // InternalDuration.g:192:3: ( rule__CommunicationDuration__Group__0 )
            {
             before(grammarAccess.getCommunicationDurationAccess().getGroup()); 
            // InternalDuration.g:193:3: ( rule__CommunicationDuration__Group__0 )
            // InternalDuration.g:193:4: rule__CommunicationDuration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCommunicationDurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCommunicationDuration"


    // $ANTLR start "entryRuleEInt"
    // InternalDuration.g:202:1: entryRuleEInt : ruleEInt EOF ;
    public final void entryRuleEInt() throws RecognitionException {
        try {
            // InternalDuration.g:203:1: ( ruleEInt EOF )
            // InternalDuration.g:204:1: ruleEInt EOF
            {
             before(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getEIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalDuration.g:211:1: ruleEInt : ( ( rule__EInt__Group__0 ) ) ;
    public final void ruleEInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:215:2: ( ( ( rule__EInt__Group__0 ) ) )
            // InternalDuration.g:216:2: ( ( rule__EInt__Group__0 ) )
            {
            // InternalDuration.g:216:2: ( ( rule__EInt__Group__0 ) )
            // InternalDuration.g:217:3: ( rule__EInt__Group__0 )
            {
             before(grammarAccess.getEIntAccess().getGroup()); 
            // InternalDuration.g:218:3: ( rule__EInt__Group__0 )
            // InternalDuration.g:218:4: rule__EInt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEIntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleEString"
    // InternalDuration.g:227:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalDuration.g:228:1: ( ruleEString EOF )
            // InternalDuration.g:229:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDuration.g:236:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:240:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalDuration.g:241:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalDuration.g:241:2: ( ( rule__EString__Alternatives ) )
            // InternalDuration.g:242:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalDuration.g:243:3: ( rule__EString__Alternatives )
            // InternalDuration.g:243:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "rule__Duration__Alternatives"
    // InternalDuration.g:251:1: rule__Duration__Alternatives : ( ( ruleTimer ) | ( ruleComputationDuration ) | ( ruleCommunicationDuration ) );
    public final void rule__Duration__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:255:1: ( ( ruleTimer ) | ( ruleComputationDuration ) | ( ruleCommunicationDuration ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt1=1;
                }
                break;
            case 28:
                {
                int LA1_2 = input.LA(2);

                if ( (LA1_2==26) ) {
                    alt1=3;
                }
                else if ( (LA1_2==25) ) {
                    alt1=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 2, input);

                    throw nvae;
                }
                }
                break;
            case 25:
                {
                alt1=2;
                }
                break;
            case 26:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalDuration.g:256:2: ( ruleTimer )
                    {
                    // InternalDuration.g:256:2: ( ruleTimer )
                    // InternalDuration.g:257:3: ruleTimer
                    {
                     before(grammarAccess.getDurationAccess().getTimerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleTimer();

                    state._fsp--;

                     after(grammarAccess.getDurationAccess().getTimerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:262:2: ( ruleComputationDuration )
                    {
                    // InternalDuration.g:262:2: ( ruleComputationDuration )
                    // InternalDuration.g:263:3: ruleComputationDuration
                    {
                     before(grammarAccess.getDurationAccess().getComputationDurationParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleComputationDuration();

                    state._fsp--;

                     after(grammarAccess.getDurationAccess().getComputationDurationParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDuration.g:268:2: ( ruleCommunicationDuration )
                    {
                    // InternalDuration.g:268:2: ( ruleCommunicationDuration )
                    // InternalDuration.g:269:3: ruleCommunicationDuration
                    {
                     before(grammarAccess.getDurationAccess().getCommunicationDurationParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleCommunicationDuration();

                    state._fsp--;

                     after(grammarAccess.getDurationAccess().getCommunicationDurationParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duration__Alternatives"


    // $ANTLR start "rule__Timer__Alternatives_4"
    // InternalDuration.g:278:1: rule__Timer__Alternatives_4 : ( ( 'reference' ) | ( 'ref' ) );
    public final void rule__Timer__Alternatives_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:282:1: ( ( 'reference' ) | ( 'ref' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalDuration.g:283:2: ( 'reference' )
                    {
                    // InternalDuration.g:283:2: ( 'reference' )
                    // InternalDuration.g:284:3: 'reference'
                    {
                     before(grammarAccess.getTimerAccess().getReferenceKeyword_4_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getTimerAccess().getReferenceKeyword_4_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:289:2: ( 'ref' )
                    {
                    // InternalDuration.g:289:2: ( 'ref' )
                    // InternalDuration.g:290:3: 'ref'
                    {
                     before(grammarAccess.getTimerAccess().getRefKeyword_4_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getTimerAccess().getRefKeyword_4_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Alternatives_4"


    // $ANTLR start "rule__ComputationDuration__Alternatives_2"
    // InternalDuration.g:299:1: rule__ComputationDuration__Alternatives_2 : ( ( 'from' ) | ( 'start' ) );
    public final void rule__ComputationDuration__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:303:1: ( ( 'from' ) | ( 'start' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalDuration.g:304:2: ( 'from' )
                    {
                    // InternalDuration.g:304:2: ( 'from' )
                    // InternalDuration.g:305:3: 'from'
                    {
                     before(grammarAccess.getComputationDurationAccess().getFromKeyword_2_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getComputationDurationAccess().getFromKeyword_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:310:2: ( 'start' )
                    {
                    // InternalDuration.g:310:2: ( 'start' )
                    // InternalDuration.g:311:3: 'start'
                    {
                     before(grammarAccess.getComputationDurationAccess().getStartKeyword_2_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getComputationDurationAccess().getStartKeyword_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Alternatives_2"


    // $ANTLR start "rule__ComputationDuration__Alternatives_4"
    // InternalDuration.g:320:1: rule__ComputationDuration__Alternatives_4 : ( ( 'to' ) | ( 'end' ) );
    public final void rule__ComputationDuration__Alternatives_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:324:1: ( ( 'to' ) | ( 'end' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            else if ( (LA4_0==16) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalDuration.g:325:2: ( 'to' )
                    {
                    // InternalDuration.g:325:2: ( 'to' )
                    // InternalDuration.g:326:3: 'to'
                    {
                     before(grammarAccess.getComputationDurationAccess().getToKeyword_4_0()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getComputationDurationAccess().getToKeyword_4_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:331:2: ( 'end' )
                    {
                    // InternalDuration.g:331:2: ( 'end' )
                    // InternalDuration.g:332:3: 'end'
                    {
                     before(grammarAccess.getComputationDurationAccess().getEndKeyword_4_1()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getComputationDurationAccess().getEndKeyword_4_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Alternatives_4"


    // $ANTLR start "rule__ComputationDuration__Alternatives_6_0"
    // InternalDuration.g:341:1: rule__ComputationDuration__Alternatives_6_0 : ( ( 'reference' ) | ( 'ref' ) );
    public final void rule__ComputationDuration__Alternatives_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:345:1: ( ( 'reference' ) | ( 'ref' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==11) ) {
                alt5=1;
            }
            else if ( (LA5_0==12) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalDuration.g:346:2: ( 'reference' )
                    {
                    // InternalDuration.g:346:2: ( 'reference' )
                    // InternalDuration.g:347:3: 'reference'
                    {
                     before(grammarAccess.getComputationDurationAccess().getReferenceKeyword_6_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getComputationDurationAccess().getReferenceKeyword_6_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:352:2: ( 'ref' )
                    {
                    // InternalDuration.g:352:2: ( 'ref' )
                    // InternalDuration.g:353:3: 'ref'
                    {
                     before(grammarAccess.getComputationDurationAccess().getRefKeyword_6_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getComputationDurationAccess().getRefKeyword_6_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Alternatives_6_0"


    // $ANTLR start "rule__CommunicationDuration__Alternatives_2"
    // InternalDuration.g:362:1: rule__CommunicationDuration__Alternatives_2 : ( ( 'from' ) | ( 'start' ) );
    public final void rule__CommunicationDuration__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:366:1: ( ( 'from' ) | ( 'start' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==13) ) {
                alt6=1;
            }
            else if ( (LA6_0==14) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalDuration.g:367:2: ( 'from' )
                    {
                    // InternalDuration.g:367:2: ( 'from' )
                    // InternalDuration.g:368:3: 'from'
                    {
                     before(grammarAccess.getCommunicationDurationAccess().getFromKeyword_2_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getCommunicationDurationAccess().getFromKeyword_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:373:2: ( 'start' )
                    {
                    // InternalDuration.g:373:2: ( 'start' )
                    // InternalDuration.g:374:3: 'start'
                    {
                     before(grammarAccess.getCommunicationDurationAccess().getStartKeyword_2_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getCommunicationDurationAccess().getStartKeyword_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Alternatives_2"


    // $ANTLR start "rule__CommunicationDuration__Alternatives_4"
    // InternalDuration.g:383:1: rule__CommunicationDuration__Alternatives_4 : ( ( 'to' ) | ( 'end' ) );
    public final void rule__CommunicationDuration__Alternatives_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:387:1: ( ( 'to' ) | ( 'end' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==15) ) {
                alt7=1;
            }
            else if ( (LA7_0==16) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalDuration.g:388:2: ( 'to' )
                    {
                    // InternalDuration.g:388:2: ( 'to' )
                    // InternalDuration.g:389:3: 'to'
                    {
                     before(grammarAccess.getCommunicationDurationAccess().getToKeyword_4_0()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getCommunicationDurationAccess().getToKeyword_4_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:394:2: ( 'end' )
                    {
                    // InternalDuration.g:394:2: ( 'end' )
                    // InternalDuration.g:395:3: 'end'
                    {
                     before(grammarAccess.getCommunicationDurationAccess().getEndKeyword_4_1()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getCommunicationDurationAccess().getEndKeyword_4_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Alternatives_4"


    // $ANTLR start "rule__CommunicationDuration__Alternatives_6_0"
    // InternalDuration.g:404:1: rule__CommunicationDuration__Alternatives_6_0 : ( ( 'reference' ) | ( 'ref' ) );
    public final void rule__CommunicationDuration__Alternatives_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:408:1: ( ( 'reference' ) | ( 'ref' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==11) ) {
                alt8=1;
            }
            else if ( (LA8_0==12) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalDuration.g:409:2: ( 'reference' )
                    {
                    // InternalDuration.g:409:2: ( 'reference' )
                    // InternalDuration.g:410:3: 'reference'
                    {
                     before(grammarAccess.getCommunicationDurationAccess().getReferenceKeyword_6_0_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getCommunicationDurationAccess().getReferenceKeyword_6_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:415:2: ( 'ref' )
                    {
                    // InternalDuration.g:415:2: ( 'ref' )
                    // InternalDuration.g:416:3: 'ref'
                    {
                     before(grammarAccess.getCommunicationDurationAccess().getRefKeyword_6_0_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getCommunicationDurationAccess().getRefKeyword_6_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Alternatives_6_0"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalDuration.g:425:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:429:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_STRING) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_ID) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalDuration.g:430:2: ( RULE_STRING )
                    {
                    // InternalDuration.g:430:2: ( RULE_STRING )
                    // InternalDuration.g:431:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDuration.g:436:2: ( RULE_ID )
                    {
                    // InternalDuration.g:436:2: ( RULE_ID )
                    // InternalDuration.g:437:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__DurationModel__Group__0"
    // InternalDuration.g:446:1: rule__DurationModel__Group__0 : rule__DurationModel__Group__0__Impl rule__DurationModel__Group__1 ;
    public final void rule__DurationModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:450:1: ( rule__DurationModel__Group__0__Impl rule__DurationModel__Group__1 )
            // InternalDuration.g:451:2: rule__DurationModel__Group__0__Impl rule__DurationModel__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__DurationModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DurationModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__0"


    // $ANTLR start "rule__DurationModel__Group__0__Impl"
    // InternalDuration.g:458:1: rule__DurationModel__Group__0__Impl : ( () ) ;
    public final void rule__DurationModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:462:1: ( ( () ) )
            // InternalDuration.g:463:1: ( () )
            {
            // InternalDuration.g:463:1: ( () )
            // InternalDuration.g:464:2: ()
            {
             before(grammarAccess.getDurationModelAccess().getDurationModelAction_0()); 
            // InternalDuration.g:465:2: ()
            // InternalDuration.g:465:3: 
            {
            }

             after(grammarAccess.getDurationModelAccess().getDurationModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__0__Impl"


    // $ANTLR start "rule__DurationModel__Group__1"
    // InternalDuration.g:473:1: rule__DurationModel__Group__1 : rule__DurationModel__Group__1__Impl rule__DurationModel__Group__2 ;
    public final void rule__DurationModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:477:1: ( rule__DurationModel__Group__1__Impl rule__DurationModel__Group__2 )
            // InternalDuration.g:478:2: rule__DurationModel__Group__1__Impl rule__DurationModel__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__DurationModel__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DurationModel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__1"


    // $ANTLR start "rule__DurationModel__Group__1__Impl"
    // InternalDuration.g:485:1: rule__DurationModel__Group__1__Impl : ( 'DurationModel' ) ;
    public final void rule__DurationModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:489:1: ( ( 'DurationModel' ) )
            // InternalDuration.g:490:1: ( 'DurationModel' )
            {
            // InternalDuration.g:490:1: ( 'DurationModel' )
            // InternalDuration.g:491:2: 'DurationModel'
            {
             before(grammarAccess.getDurationModelAccess().getDurationModelKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getDurationModelAccess().getDurationModelKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__1__Impl"


    // $ANTLR start "rule__DurationModel__Group__2"
    // InternalDuration.g:500:1: rule__DurationModel__Group__2 : rule__DurationModel__Group__2__Impl rule__DurationModel__Group__3 ;
    public final void rule__DurationModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:504:1: ( rule__DurationModel__Group__2__Impl rule__DurationModel__Group__3 )
            // InternalDuration.g:505:2: rule__DurationModel__Group__2__Impl rule__DurationModel__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__DurationModel__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DurationModel__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__2"


    // $ANTLR start "rule__DurationModel__Group__2__Impl"
    // InternalDuration.g:512:1: rule__DurationModel__Group__2__Impl : ( ( rule__DurationModel__ImportStatementAssignment_2 ) ) ;
    public final void rule__DurationModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:516:1: ( ( ( rule__DurationModel__ImportStatementAssignment_2 ) ) )
            // InternalDuration.g:517:1: ( ( rule__DurationModel__ImportStatementAssignment_2 ) )
            {
            // InternalDuration.g:517:1: ( ( rule__DurationModel__ImportStatementAssignment_2 ) )
            // InternalDuration.g:518:2: ( rule__DurationModel__ImportStatementAssignment_2 )
            {
             before(grammarAccess.getDurationModelAccess().getImportStatementAssignment_2()); 
            // InternalDuration.g:519:2: ( rule__DurationModel__ImportStatementAssignment_2 )
            // InternalDuration.g:519:3: rule__DurationModel__ImportStatementAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DurationModel__ImportStatementAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDurationModelAccess().getImportStatementAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__2__Impl"


    // $ANTLR start "rule__DurationModel__Group__3"
    // InternalDuration.g:527:1: rule__DurationModel__Group__3 : rule__DurationModel__Group__3__Impl rule__DurationModel__Group__4 ;
    public final void rule__DurationModel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:531:1: ( rule__DurationModel__Group__3__Impl rule__DurationModel__Group__4 )
            // InternalDuration.g:532:2: rule__DurationModel__Group__3__Impl rule__DurationModel__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__DurationModel__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DurationModel__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__3"


    // $ANTLR start "rule__DurationModel__Group__3__Impl"
    // InternalDuration.g:539:1: rule__DurationModel__Group__3__Impl : ( '{' ) ;
    public final void rule__DurationModel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:543:1: ( ( '{' ) )
            // InternalDuration.g:544:1: ( '{' )
            {
            // InternalDuration.g:544:1: ( '{' )
            // InternalDuration.g:545:2: '{'
            {
             before(grammarAccess.getDurationModelAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getDurationModelAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__3__Impl"


    // $ANTLR start "rule__DurationModel__Group__4"
    // InternalDuration.g:554:1: rule__DurationModel__Group__4 : rule__DurationModel__Group__4__Impl rule__DurationModel__Group__5 ;
    public final void rule__DurationModel__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:558:1: ( rule__DurationModel__Group__4__Impl rule__DurationModel__Group__5 )
            // InternalDuration.g:559:2: rule__DurationModel__Group__4__Impl rule__DurationModel__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__DurationModel__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DurationModel__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__4"


    // $ANTLR start "rule__DurationModel__Group__4__Impl"
    // InternalDuration.g:566:1: rule__DurationModel__Group__4__Impl : ( ( rule__DurationModel__Group_4__0 )? ) ;
    public final void rule__DurationModel__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:570:1: ( ( ( rule__DurationModel__Group_4__0 )? ) )
            // InternalDuration.g:571:1: ( ( rule__DurationModel__Group_4__0 )? )
            {
            // InternalDuration.g:571:1: ( ( rule__DurationModel__Group_4__0 )? )
            // InternalDuration.g:572:2: ( rule__DurationModel__Group_4__0 )?
            {
             before(grammarAccess.getDurationModelAccess().getGroup_4()); 
            // InternalDuration.g:573:2: ( rule__DurationModel__Group_4__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==21||(LA10_0>=25 && LA10_0<=26)||LA10_0==28) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDuration.g:573:3: rule__DurationModel__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DurationModel__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDurationModelAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__4__Impl"


    // $ANTLR start "rule__DurationModel__Group__5"
    // InternalDuration.g:581:1: rule__DurationModel__Group__5 : rule__DurationModel__Group__5__Impl ;
    public final void rule__DurationModel__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:585:1: ( rule__DurationModel__Group__5__Impl )
            // InternalDuration.g:586:2: rule__DurationModel__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DurationModel__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__5"


    // $ANTLR start "rule__DurationModel__Group__5__Impl"
    // InternalDuration.g:592:1: rule__DurationModel__Group__5__Impl : ( '}' ) ;
    public final void rule__DurationModel__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:596:1: ( ( '}' ) )
            // InternalDuration.g:597:1: ( '}' )
            {
            // InternalDuration.g:597:1: ( '}' )
            // InternalDuration.g:598:2: '}'
            {
             before(grammarAccess.getDurationModelAccess().getRightCurlyBracketKeyword_5()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getDurationModelAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group__5__Impl"


    // $ANTLR start "rule__DurationModel__Group_4__0"
    // InternalDuration.g:608:1: rule__DurationModel__Group_4__0 : rule__DurationModel__Group_4__0__Impl rule__DurationModel__Group_4__1 ;
    public final void rule__DurationModel__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:612:1: ( rule__DurationModel__Group_4__0__Impl rule__DurationModel__Group_4__1 )
            // InternalDuration.g:613:2: rule__DurationModel__Group_4__0__Impl rule__DurationModel__Group_4__1
            {
            pushFollow(FOLLOW_7);
            rule__DurationModel__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DurationModel__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group_4__0"


    // $ANTLR start "rule__DurationModel__Group_4__0__Impl"
    // InternalDuration.g:620:1: rule__DurationModel__Group_4__0__Impl : ( ( rule__DurationModel__DurationsAssignment_4_0 ) ) ;
    public final void rule__DurationModel__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:624:1: ( ( ( rule__DurationModel__DurationsAssignment_4_0 ) ) )
            // InternalDuration.g:625:1: ( ( rule__DurationModel__DurationsAssignment_4_0 ) )
            {
            // InternalDuration.g:625:1: ( ( rule__DurationModel__DurationsAssignment_4_0 ) )
            // InternalDuration.g:626:2: ( rule__DurationModel__DurationsAssignment_4_0 )
            {
             before(grammarAccess.getDurationModelAccess().getDurationsAssignment_4_0()); 
            // InternalDuration.g:627:2: ( rule__DurationModel__DurationsAssignment_4_0 )
            // InternalDuration.g:627:3: rule__DurationModel__DurationsAssignment_4_0
            {
            pushFollow(FOLLOW_2);
            rule__DurationModel__DurationsAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getDurationModelAccess().getDurationsAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group_4__0__Impl"


    // $ANTLR start "rule__DurationModel__Group_4__1"
    // InternalDuration.g:635:1: rule__DurationModel__Group_4__1 : rule__DurationModel__Group_4__1__Impl ;
    public final void rule__DurationModel__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:639:1: ( rule__DurationModel__Group_4__1__Impl )
            // InternalDuration.g:640:2: rule__DurationModel__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DurationModel__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group_4__1"


    // $ANTLR start "rule__DurationModel__Group_4__1__Impl"
    // InternalDuration.g:646:1: rule__DurationModel__Group_4__1__Impl : ( ( rule__DurationModel__DurationsAssignment_4_1 )* ) ;
    public final void rule__DurationModel__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:650:1: ( ( ( rule__DurationModel__DurationsAssignment_4_1 )* ) )
            // InternalDuration.g:651:1: ( ( rule__DurationModel__DurationsAssignment_4_1 )* )
            {
            // InternalDuration.g:651:1: ( ( rule__DurationModel__DurationsAssignment_4_1 )* )
            // InternalDuration.g:652:2: ( rule__DurationModel__DurationsAssignment_4_1 )*
            {
             before(grammarAccess.getDurationModelAccess().getDurationsAssignment_4_1()); 
            // InternalDuration.g:653:2: ( rule__DurationModel__DurationsAssignment_4_1 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==21||(LA11_0>=25 && LA11_0<=26)||LA11_0==28) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalDuration.g:653:3: rule__DurationModel__DurationsAssignment_4_1
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__DurationModel__DurationsAssignment_4_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getDurationModelAccess().getDurationsAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__Group_4__1__Impl"


    // $ANTLR start "rule__ImportStatement__Group__0"
    // InternalDuration.g:662:1: rule__ImportStatement__Group__0 : rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1 ;
    public final void rule__ImportStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:666:1: ( rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1 )
            // InternalDuration.g:667:2: rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__ImportStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__0"


    // $ANTLR start "rule__ImportStatement__Group__0__Impl"
    // InternalDuration.g:674:1: rule__ImportStatement__Group__0__Impl : ( 'import' ) ;
    public final void rule__ImportStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:678:1: ( ( 'import' ) )
            // InternalDuration.g:679:1: ( 'import' )
            {
            // InternalDuration.g:679:1: ( 'import' )
            // InternalDuration.g:680:2: 'import'
            {
             before(grammarAccess.getImportStatementAccess().getImportKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__0__Impl"


    // $ANTLR start "rule__ImportStatement__Group__1"
    // InternalDuration.g:689:1: rule__ImportStatement__Group__1 : rule__ImportStatement__Group__1__Impl rule__ImportStatement__Group__2 ;
    public final void rule__ImportStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:693:1: ( rule__ImportStatement__Group__1__Impl rule__ImportStatement__Group__2 )
            // InternalDuration.g:694:2: rule__ImportStatement__Group__1__Impl rule__ImportStatement__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__ImportStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__1"


    // $ANTLR start "rule__ImportStatement__Group__1__Impl"
    // InternalDuration.g:701:1: rule__ImportStatement__Group__1__Impl : ( ( rule__ImportStatement__ImportURIAssignment_1 ) ) ;
    public final void rule__ImportStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:705:1: ( ( ( rule__ImportStatement__ImportURIAssignment_1 ) ) )
            // InternalDuration.g:706:1: ( ( rule__ImportStatement__ImportURIAssignment_1 ) )
            {
            // InternalDuration.g:706:1: ( ( rule__ImportStatement__ImportURIAssignment_1 ) )
            // InternalDuration.g:707:2: ( rule__ImportStatement__ImportURIAssignment_1 )
            {
             before(grammarAccess.getImportStatementAccess().getImportURIAssignment_1()); 
            // InternalDuration.g:708:2: ( rule__ImportStatement__ImportURIAssignment_1 )
            // InternalDuration.g:708:3: rule__ImportStatement__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__1__Impl"


    // $ANTLR start "rule__ImportStatement__Group__2"
    // InternalDuration.g:716:1: rule__ImportStatement__Group__2 : rule__ImportStatement__Group__2__Impl ;
    public final void rule__ImportStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:720:1: ( rule__ImportStatement__Group__2__Impl )
            // InternalDuration.g:721:2: rule__ImportStatement__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__2"


    // $ANTLR start "rule__ImportStatement__Group__2__Impl"
    // InternalDuration.g:727:1: rule__ImportStatement__Group__2__Impl : ( ( rule__ImportStatement__AliasAssignment_2 ) ) ;
    public final void rule__ImportStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:731:1: ( ( ( rule__ImportStatement__AliasAssignment_2 ) ) )
            // InternalDuration.g:732:1: ( ( rule__ImportStatement__AliasAssignment_2 ) )
            {
            // InternalDuration.g:732:1: ( ( rule__ImportStatement__AliasAssignment_2 ) )
            // InternalDuration.g:733:2: ( rule__ImportStatement__AliasAssignment_2 )
            {
             before(grammarAccess.getImportStatementAccess().getAliasAssignment_2()); 
            // InternalDuration.g:734:2: ( rule__ImportStatement__AliasAssignment_2 )
            // InternalDuration.g:734:3: rule__ImportStatement__AliasAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__AliasAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getAliasAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__2__Impl"


    // $ANTLR start "rule__Timer__Group__0"
    // InternalDuration.g:743:1: rule__Timer__Group__0 : rule__Timer__Group__0__Impl rule__Timer__Group__1 ;
    public final void rule__Timer__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:747:1: ( rule__Timer__Group__0__Impl rule__Timer__Group__1 )
            // InternalDuration.g:748:2: rule__Timer__Group__0__Impl rule__Timer__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Timer__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timer__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__0"


    // $ANTLR start "rule__Timer__Group__0__Impl"
    // InternalDuration.g:755:1: rule__Timer__Group__0__Impl : ( 'Timer' ) ;
    public final void rule__Timer__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:759:1: ( ( 'Timer' ) )
            // InternalDuration.g:760:1: ( 'Timer' )
            {
            // InternalDuration.g:760:1: ( 'Timer' )
            // InternalDuration.g:761:2: 'Timer'
            {
             before(grammarAccess.getTimerAccess().getTimerKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getTimerAccess().getTimerKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__0__Impl"


    // $ANTLR start "rule__Timer__Group__1"
    // InternalDuration.g:770:1: rule__Timer__Group__1 : rule__Timer__Group__1__Impl rule__Timer__Group__2 ;
    public final void rule__Timer__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:774:1: ( rule__Timer__Group__1__Impl rule__Timer__Group__2 )
            // InternalDuration.g:775:2: rule__Timer__Group__1__Impl rule__Timer__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Timer__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timer__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__1"


    // $ANTLR start "rule__Timer__Group__1__Impl"
    // InternalDuration.g:782:1: rule__Timer__Group__1__Impl : ( ( rule__Timer__Group_1__0 )? ) ;
    public final void rule__Timer__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:786:1: ( ( ( rule__Timer__Group_1__0 )? ) )
            // InternalDuration.g:787:1: ( ( rule__Timer__Group_1__0 )? )
            {
            // InternalDuration.g:787:1: ( ( rule__Timer__Group_1__0 )? )
            // InternalDuration.g:788:2: ( rule__Timer__Group_1__0 )?
            {
             before(grammarAccess.getTimerAccess().getGroup_1()); 
            // InternalDuration.g:789:2: ( rule__Timer__Group_1__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==23) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalDuration.g:789:3: rule__Timer__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Timer__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTimerAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__1__Impl"


    // $ANTLR start "rule__Timer__Group__2"
    // InternalDuration.g:797:1: rule__Timer__Group__2 : rule__Timer__Group__2__Impl rule__Timer__Group__3 ;
    public final void rule__Timer__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:801:1: ( rule__Timer__Group__2__Impl rule__Timer__Group__3 )
            // InternalDuration.g:802:2: rule__Timer__Group__2__Impl rule__Timer__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__Timer__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timer__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__2"


    // $ANTLR start "rule__Timer__Group__2__Impl"
    // InternalDuration.g:809:1: rule__Timer__Group__2__Impl : ( 'start' ) ;
    public final void rule__Timer__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:813:1: ( ( 'start' ) )
            // InternalDuration.g:814:1: ( 'start' )
            {
            // InternalDuration.g:814:1: ( 'start' )
            // InternalDuration.g:815:2: 'start'
            {
             before(grammarAccess.getTimerAccess().getStartKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getTimerAccess().getStartKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__2__Impl"


    // $ANTLR start "rule__Timer__Group__3"
    // InternalDuration.g:824:1: rule__Timer__Group__3 : rule__Timer__Group__3__Impl rule__Timer__Group__4 ;
    public final void rule__Timer__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:828:1: ( rule__Timer__Group__3__Impl rule__Timer__Group__4 )
            // InternalDuration.g:829:2: rule__Timer__Group__3__Impl rule__Timer__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__Timer__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timer__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__3"


    // $ANTLR start "rule__Timer__Group__3__Impl"
    // InternalDuration.g:836:1: rule__Timer__Group__3__Impl : ( ( rule__Timer__StartAssignment_3 ) ) ;
    public final void rule__Timer__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:840:1: ( ( ( rule__Timer__StartAssignment_3 ) ) )
            // InternalDuration.g:841:1: ( ( rule__Timer__StartAssignment_3 ) )
            {
            // InternalDuration.g:841:1: ( ( rule__Timer__StartAssignment_3 ) )
            // InternalDuration.g:842:2: ( rule__Timer__StartAssignment_3 )
            {
             before(grammarAccess.getTimerAccess().getStartAssignment_3()); 
            // InternalDuration.g:843:2: ( rule__Timer__StartAssignment_3 )
            // InternalDuration.g:843:3: rule__Timer__StartAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Timer__StartAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTimerAccess().getStartAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__3__Impl"


    // $ANTLR start "rule__Timer__Group__4"
    // InternalDuration.g:851:1: rule__Timer__Group__4 : rule__Timer__Group__4__Impl rule__Timer__Group__5 ;
    public final void rule__Timer__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:855:1: ( rule__Timer__Group__4__Impl rule__Timer__Group__5 )
            // InternalDuration.g:856:2: rule__Timer__Group__4__Impl rule__Timer__Group__5
            {
            pushFollow(FOLLOW_12);
            rule__Timer__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timer__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__4"


    // $ANTLR start "rule__Timer__Group__4__Impl"
    // InternalDuration.g:863:1: rule__Timer__Group__4__Impl : ( ( rule__Timer__Alternatives_4 ) ) ;
    public final void rule__Timer__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:867:1: ( ( ( rule__Timer__Alternatives_4 ) ) )
            // InternalDuration.g:868:1: ( ( rule__Timer__Alternatives_4 ) )
            {
            // InternalDuration.g:868:1: ( ( rule__Timer__Alternatives_4 ) )
            // InternalDuration.g:869:2: ( rule__Timer__Alternatives_4 )
            {
             before(grammarAccess.getTimerAccess().getAlternatives_4()); 
            // InternalDuration.g:870:2: ( rule__Timer__Alternatives_4 )
            // InternalDuration.g:870:3: rule__Timer__Alternatives_4
            {
            pushFollow(FOLLOW_2);
            rule__Timer__Alternatives_4();

            state._fsp--;


            }

             after(grammarAccess.getTimerAccess().getAlternatives_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__4__Impl"


    // $ANTLR start "rule__Timer__Group__5"
    // InternalDuration.g:878:1: rule__Timer__Group__5 : rule__Timer__Group__5__Impl rule__Timer__Group__6 ;
    public final void rule__Timer__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:882:1: ( rule__Timer__Group__5__Impl rule__Timer__Group__6 )
            // InternalDuration.g:883:2: rule__Timer__Group__5__Impl rule__Timer__Group__6
            {
            pushFollow(FOLLOW_10);
            rule__Timer__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timer__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__5"


    // $ANTLR start "rule__Timer__Group__5__Impl"
    // InternalDuration.g:890:1: rule__Timer__Group__5__Impl : ( ( rule__Timer__RefAssignment_5 ) ) ;
    public final void rule__Timer__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:894:1: ( ( ( rule__Timer__RefAssignment_5 ) ) )
            // InternalDuration.g:895:1: ( ( rule__Timer__RefAssignment_5 ) )
            {
            // InternalDuration.g:895:1: ( ( rule__Timer__RefAssignment_5 ) )
            // InternalDuration.g:896:2: ( rule__Timer__RefAssignment_5 )
            {
             before(grammarAccess.getTimerAccess().getRefAssignment_5()); 
            // InternalDuration.g:897:2: ( rule__Timer__RefAssignment_5 )
            // InternalDuration.g:897:3: rule__Timer__RefAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Timer__RefAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getTimerAccess().getRefAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__5__Impl"


    // $ANTLR start "rule__Timer__Group__6"
    // InternalDuration.g:905:1: rule__Timer__Group__6 : rule__Timer__Group__6__Impl ;
    public final void rule__Timer__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:909:1: ( rule__Timer__Group__6__Impl )
            // InternalDuration.g:910:2: rule__Timer__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Timer__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__6"


    // $ANTLR start "rule__Timer__Group__6__Impl"
    // InternalDuration.g:916:1: rule__Timer__Group__6__Impl : ( ';' ) ;
    public final void rule__Timer__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:920:1: ( ( ';' ) )
            // InternalDuration.g:921:1: ( ';' )
            {
            // InternalDuration.g:921:1: ( ';' )
            // InternalDuration.g:922:2: ';'
            {
             before(grammarAccess.getTimerAccess().getSemicolonKeyword_6()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getTimerAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group__6__Impl"


    // $ANTLR start "rule__Timer__Group_1__0"
    // InternalDuration.g:932:1: rule__Timer__Group_1__0 : rule__Timer__Group_1__0__Impl rule__Timer__Group_1__1 ;
    public final void rule__Timer__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:936:1: ( rule__Timer__Group_1__0__Impl rule__Timer__Group_1__1 )
            // InternalDuration.g:937:2: rule__Timer__Group_1__0__Impl rule__Timer__Group_1__1
            {
            pushFollow(FOLLOW_14);
            rule__Timer__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timer__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group_1__0"


    // $ANTLR start "rule__Timer__Group_1__0__Impl"
    // InternalDuration.g:944:1: rule__Timer__Group_1__0__Impl : ( 'delay' ) ;
    public final void rule__Timer__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:948:1: ( ( 'delay' ) )
            // InternalDuration.g:949:1: ( 'delay' )
            {
            // InternalDuration.g:949:1: ( 'delay' )
            // InternalDuration.g:950:2: 'delay'
            {
             before(grammarAccess.getTimerAccess().getDelayKeyword_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getTimerAccess().getDelayKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group_1__0__Impl"


    // $ANTLR start "rule__Timer__Group_1__1"
    // InternalDuration.g:959:1: rule__Timer__Group_1__1 : rule__Timer__Group_1__1__Impl rule__Timer__Group_1__2 ;
    public final void rule__Timer__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:963:1: ( rule__Timer__Group_1__1__Impl rule__Timer__Group_1__2 )
            // InternalDuration.g:964:2: rule__Timer__Group_1__1__Impl rule__Timer__Group_1__2
            {
            pushFollow(FOLLOW_15);
            rule__Timer__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timer__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group_1__1"


    // $ANTLR start "rule__Timer__Group_1__1__Impl"
    // InternalDuration.g:971:1: rule__Timer__Group_1__1__Impl : ( '=' ) ;
    public final void rule__Timer__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:975:1: ( ( '=' ) )
            // InternalDuration.g:976:1: ( '=' )
            {
            // InternalDuration.g:976:1: ( '=' )
            // InternalDuration.g:977:2: '='
            {
             before(grammarAccess.getTimerAccess().getEqualsSignKeyword_1_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getTimerAccess().getEqualsSignKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group_1__1__Impl"


    // $ANTLR start "rule__Timer__Group_1__2"
    // InternalDuration.g:986:1: rule__Timer__Group_1__2 : rule__Timer__Group_1__2__Impl ;
    public final void rule__Timer__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:990:1: ( rule__Timer__Group_1__2__Impl )
            // InternalDuration.g:991:2: rule__Timer__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Timer__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group_1__2"


    // $ANTLR start "rule__Timer__Group_1__2__Impl"
    // InternalDuration.g:997:1: rule__Timer__Group_1__2__Impl : ( ( rule__Timer__DelayAssignment_1_2 ) ) ;
    public final void rule__Timer__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1001:1: ( ( ( rule__Timer__DelayAssignment_1_2 ) ) )
            // InternalDuration.g:1002:1: ( ( rule__Timer__DelayAssignment_1_2 ) )
            {
            // InternalDuration.g:1002:1: ( ( rule__Timer__DelayAssignment_1_2 ) )
            // InternalDuration.g:1003:2: ( rule__Timer__DelayAssignment_1_2 )
            {
             before(grammarAccess.getTimerAccess().getDelayAssignment_1_2()); 
            // InternalDuration.g:1004:2: ( rule__Timer__DelayAssignment_1_2 )
            // InternalDuration.g:1004:3: rule__Timer__DelayAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Timer__DelayAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getTimerAccess().getDelayAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__Group_1__2__Impl"


    // $ANTLR start "rule__ComputationDuration__Group__0"
    // InternalDuration.g:1013:1: rule__ComputationDuration__Group__0 : rule__ComputationDuration__Group__0__Impl rule__ComputationDuration__Group__1 ;
    public final void rule__ComputationDuration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1017:1: ( rule__ComputationDuration__Group__0__Impl rule__ComputationDuration__Group__1 )
            // InternalDuration.g:1018:2: rule__ComputationDuration__Group__0__Impl rule__ComputationDuration__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__ComputationDuration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__0"


    // $ANTLR start "rule__ComputationDuration__Group__0__Impl"
    // InternalDuration.g:1025:1: rule__ComputationDuration__Group__0__Impl : ( ( rule__ComputationDuration__DisplayedOverlapAssignment_0 )? ) ;
    public final void rule__ComputationDuration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1029:1: ( ( ( rule__ComputationDuration__DisplayedOverlapAssignment_0 )? ) )
            // InternalDuration.g:1030:1: ( ( rule__ComputationDuration__DisplayedOverlapAssignment_0 )? )
            {
            // InternalDuration.g:1030:1: ( ( rule__ComputationDuration__DisplayedOverlapAssignment_0 )? )
            // InternalDuration.g:1031:2: ( rule__ComputationDuration__DisplayedOverlapAssignment_0 )?
            {
             before(grammarAccess.getComputationDurationAccess().getDisplayedOverlapAssignment_0()); 
            // InternalDuration.g:1032:2: ( rule__ComputationDuration__DisplayedOverlapAssignment_0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==28) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalDuration.g:1032:3: rule__ComputationDuration__DisplayedOverlapAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComputationDuration__DisplayedOverlapAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getComputationDurationAccess().getDisplayedOverlapAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__0__Impl"


    // $ANTLR start "rule__ComputationDuration__Group__1"
    // InternalDuration.g:1040:1: rule__ComputationDuration__Group__1 : rule__ComputationDuration__Group__1__Impl rule__ComputationDuration__Group__2 ;
    public final void rule__ComputationDuration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1044:1: ( rule__ComputationDuration__Group__1__Impl rule__ComputationDuration__Group__2 )
            // InternalDuration.g:1045:2: rule__ComputationDuration__Group__1__Impl rule__ComputationDuration__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__ComputationDuration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__1"


    // $ANTLR start "rule__ComputationDuration__Group__1__Impl"
    // InternalDuration.g:1052:1: rule__ComputationDuration__Group__1__Impl : ( 'ComputationDuration' ) ;
    public final void rule__ComputationDuration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1056:1: ( ( 'ComputationDuration' ) )
            // InternalDuration.g:1057:1: ( 'ComputationDuration' )
            {
            // InternalDuration.g:1057:1: ( 'ComputationDuration' )
            // InternalDuration.g:1058:2: 'ComputationDuration'
            {
             before(grammarAccess.getComputationDurationAccess().getComputationDurationKeyword_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getComputationDurationAccess().getComputationDurationKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__1__Impl"


    // $ANTLR start "rule__ComputationDuration__Group__2"
    // InternalDuration.g:1067:1: rule__ComputationDuration__Group__2 : rule__ComputationDuration__Group__2__Impl rule__ComputationDuration__Group__3 ;
    public final void rule__ComputationDuration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1071:1: ( rule__ComputationDuration__Group__2__Impl rule__ComputationDuration__Group__3 )
            // InternalDuration.g:1072:2: rule__ComputationDuration__Group__2__Impl rule__ComputationDuration__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__ComputationDuration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__2"


    // $ANTLR start "rule__ComputationDuration__Group__2__Impl"
    // InternalDuration.g:1079:1: rule__ComputationDuration__Group__2__Impl : ( ( rule__ComputationDuration__Alternatives_2 ) ) ;
    public final void rule__ComputationDuration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1083:1: ( ( ( rule__ComputationDuration__Alternatives_2 ) ) )
            // InternalDuration.g:1084:1: ( ( rule__ComputationDuration__Alternatives_2 ) )
            {
            // InternalDuration.g:1084:1: ( ( rule__ComputationDuration__Alternatives_2 ) )
            // InternalDuration.g:1085:2: ( rule__ComputationDuration__Alternatives_2 )
            {
             before(grammarAccess.getComputationDurationAccess().getAlternatives_2()); 
            // InternalDuration.g:1086:2: ( rule__ComputationDuration__Alternatives_2 )
            // InternalDuration.g:1086:3: rule__ComputationDuration__Alternatives_2
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getComputationDurationAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__2__Impl"


    // $ANTLR start "rule__ComputationDuration__Group__3"
    // InternalDuration.g:1094:1: rule__ComputationDuration__Group__3 : rule__ComputationDuration__Group__3__Impl rule__ComputationDuration__Group__4 ;
    public final void rule__ComputationDuration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1098:1: ( rule__ComputationDuration__Group__3__Impl rule__ComputationDuration__Group__4 )
            // InternalDuration.g:1099:2: rule__ComputationDuration__Group__3__Impl rule__ComputationDuration__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__ComputationDuration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__3"


    // $ANTLR start "rule__ComputationDuration__Group__3__Impl"
    // InternalDuration.g:1106:1: rule__ComputationDuration__Group__3__Impl : ( ( rule__ComputationDuration__StartAssignment_3 ) ) ;
    public final void rule__ComputationDuration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1110:1: ( ( ( rule__ComputationDuration__StartAssignment_3 ) ) )
            // InternalDuration.g:1111:1: ( ( rule__ComputationDuration__StartAssignment_3 ) )
            {
            // InternalDuration.g:1111:1: ( ( rule__ComputationDuration__StartAssignment_3 ) )
            // InternalDuration.g:1112:2: ( rule__ComputationDuration__StartAssignment_3 )
            {
             before(grammarAccess.getComputationDurationAccess().getStartAssignment_3()); 
            // InternalDuration.g:1113:2: ( rule__ComputationDuration__StartAssignment_3 )
            // InternalDuration.g:1113:3: rule__ComputationDuration__StartAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__StartAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getComputationDurationAccess().getStartAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__3__Impl"


    // $ANTLR start "rule__ComputationDuration__Group__4"
    // InternalDuration.g:1121:1: rule__ComputationDuration__Group__4 : rule__ComputationDuration__Group__4__Impl rule__ComputationDuration__Group__5 ;
    public final void rule__ComputationDuration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1125:1: ( rule__ComputationDuration__Group__4__Impl rule__ComputationDuration__Group__5 )
            // InternalDuration.g:1126:2: rule__ComputationDuration__Group__4__Impl rule__ComputationDuration__Group__5
            {
            pushFollow(FOLLOW_12);
            rule__ComputationDuration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__4"


    // $ANTLR start "rule__ComputationDuration__Group__4__Impl"
    // InternalDuration.g:1133:1: rule__ComputationDuration__Group__4__Impl : ( ( rule__ComputationDuration__Alternatives_4 ) ) ;
    public final void rule__ComputationDuration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1137:1: ( ( ( rule__ComputationDuration__Alternatives_4 ) ) )
            // InternalDuration.g:1138:1: ( ( rule__ComputationDuration__Alternatives_4 ) )
            {
            // InternalDuration.g:1138:1: ( ( rule__ComputationDuration__Alternatives_4 ) )
            // InternalDuration.g:1139:2: ( rule__ComputationDuration__Alternatives_4 )
            {
             before(grammarAccess.getComputationDurationAccess().getAlternatives_4()); 
            // InternalDuration.g:1140:2: ( rule__ComputationDuration__Alternatives_4 )
            // InternalDuration.g:1140:3: rule__ComputationDuration__Alternatives_4
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Alternatives_4();

            state._fsp--;


            }

             after(grammarAccess.getComputationDurationAccess().getAlternatives_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__4__Impl"


    // $ANTLR start "rule__ComputationDuration__Group__5"
    // InternalDuration.g:1148:1: rule__ComputationDuration__Group__5 : rule__ComputationDuration__Group__5__Impl rule__ComputationDuration__Group__6 ;
    public final void rule__ComputationDuration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1152:1: ( rule__ComputationDuration__Group__5__Impl rule__ComputationDuration__Group__6 )
            // InternalDuration.g:1153:2: rule__ComputationDuration__Group__5__Impl rule__ComputationDuration__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__ComputationDuration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__5"


    // $ANTLR start "rule__ComputationDuration__Group__5__Impl"
    // InternalDuration.g:1160:1: rule__ComputationDuration__Group__5__Impl : ( ( rule__ComputationDuration__EndAssignment_5 ) ) ;
    public final void rule__ComputationDuration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1164:1: ( ( ( rule__ComputationDuration__EndAssignment_5 ) ) )
            // InternalDuration.g:1165:1: ( ( rule__ComputationDuration__EndAssignment_5 ) )
            {
            // InternalDuration.g:1165:1: ( ( rule__ComputationDuration__EndAssignment_5 ) )
            // InternalDuration.g:1166:2: ( rule__ComputationDuration__EndAssignment_5 )
            {
             before(grammarAccess.getComputationDurationAccess().getEndAssignment_5()); 
            // InternalDuration.g:1167:2: ( rule__ComputationDuration__EndAssignment_5 )
            // InternalDuration.g:1167:3: rule__ComputationDuration__EndAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__EndAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getComputationDurationAccess().getEndAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__5__Impl"


    // $ANTLR start "rule__ComputationDuration__Group__6"
    // InternalDuration.g:1175:1: rule__ComputationDuration__Group__6 : rule__ComputationDuration__Group__6__Impl rule__ComputationDuration__Group__7 ;
    public final void rule__ComputationDuration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1179:1: ( rule__ComputationDuration__Group__6__Impl rule__ComputationDuration__Group__7 )
            // InternalDuration.g:1180:2: rule__ComputationDuration__Group__6__Impl rule__ComputationDuration__Group__7
            {
            pushFollow(FOLLOW_19);
            rule__ComputationDuration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__6"


    // $ANTLR start "rule__ComputationDuration__Group__6__Impl"
    // InternalDuration.g:1187:1: rule__ComputationDuration__Group__6__Impl : ( ( rule__ComputationDuration__Group_6__0 )? ) ;
    public final void rule__ComputationDuration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1191:1: ( ( ( rule__ComputationDuration__Group_6__0 )? ) )
            // InternalDuration.g:1192:1: ( ( rule__ComputationDuration__Group_6__0 )? )
            {
            // InternalDuration.g:1192:1: ( ( rule__ComputationDuration__Group_6__0 )? )
            // InternalDuration.g:1193:2: ( rule__ComputationDuration__Group_6__0 )?
            {
             before(grammarAccess.getComputationDurationAccess().getGroup_6()); 
            // InternalDuration.g:1194:2: ( rule__ComputationDuration__Group_6__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>=11 && LA14_0<=12)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalDuration.g:1194:3: rule__ComputationDuration__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ComputationDuration__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getComputationDurationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__6__Impl"


    // $ANTLR start "rule__ComputationDuration__Group__7"
    // InternalDuration.g:1202:1: rule__ComputationDuration__Group__7 : rule__ComputationDuration__Group__7__Impl ;
    public final void rule__ComputationDuration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1206:1: ( rule__ComputationDuration__Group__7__Impl )
            // InternalDuration.g:1207:2: rule__ComputationDuration__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__7"


    // $ANTLR start "rule__ComputationDuration__Group__7__Impl"
    // InternalDuration.g:1213:1: rule__ComputationDuration__Group__7__Impl : ( ';' ) ;
    public final void rule__ComputationDuration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1217:1: ( ( ';' ) )
            // InternalDuration.g:1218:1: ( ';' )
            {
            // InternalDuration.g:1218:1: ( ';' )
            // InternalDuration.g:1219:2: ';'
            {
             before(grammarAccess.getComputationDurationAccess().getSemicolonKeyword_7()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getComputationDurationAccess().getSemicolonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group__7__Impl"


    // $ANTLR start "rule__ComputationDuration__Group_6__0"
    // InternalDuration.g:1229:1: rule__ComputationDuration__Group_6__0 : rule__ComputationDuration__Group_6__0__Impl rule__ComputationDuration__Group_6__1 ;
    public final void rule__ComputationDuration__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1233:1: ( rule__ComputationDuration__Group_6__0__Impl rule__ComputationDuration__Group_6__1 )
            // InternalDuration.g:1234:2: rule__ComputationDuration__Group_6__0__Impl rule__ComputationDuration__Group_6__1
            {
            pushFollow(FOLLOW_12);
            rule__ComputationDuration__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group_6__0"


    // $ANTLR start "rule__ComputationDuration__Group_6__0__Impl"
    // InternalDuration.g:1241:1: rule__ComputationDuration__Group_6__0__Impl : ( ( rule__ComputationDuration__Alternatives_6_0 ) ) ;
    public final void rule__ComputationDuration__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1245:1: ( ( ( rule__ComputationDuration__Alternatives_6_0 ) ) )
            // InternalDuration.g:1246:1: ( ( rule__ComputationDuration__Alternatives_6_0 ) )
            {
            // InternalDuration.g:1246:1: ( ( rule__ComputationDuration__Alternatives_6_0 ) )
            // InternalDuration.g:1247:2: ( rule__ComputationDuration__Alternatives_6_0 )
            {
             before(grammarAccess.getComputationDurationAccess().getAlternatives_6_0()); 
            // InternalDuration.g:1248:2: ( rule__ComputationDuration__Alternatives_6_0 )
            // InternalDuration.g:1248:3: rule__ComputationDuration__Alternatives_6_0
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Alternatives_6_0();

            state._fsp--;


            }

             after(grammarAccess.getComputationDurationAccess().getAlternatives_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group_6__0__Impl"


    // $ANTLR start "rule__ComputationDuration__Group_6__1"
    // InternalDuration.g:1256:1: rule__ComputationDuration__Group_6__1 : rule__ComputationDuration__Group_6__1__Impl ;
    public final void rule__ComputationDuration__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1260:1: ( rule__ComputationDuration__Group_6__1__Impl )
            // InternalDuration.g:1261:2: rule__ComputationDuration__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group_6__1"


    // $ANTLR start "rule__ComputationDuration__Group_6__1__Impl"
    // InternalDuration.g:1267:1: rule__ComputationDuration__Group_6__1__Impl : ( ( rule__ComputationDuration__RefAssignment_6_1 ) ) ;
    public final void rule__ComputationDuration__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1271:1: ( ( ( rule__ComputationDuration__RefAssignment_6_1 ) ) )
            // InternalDuration.g:1272:1: ( ( rule__ComputationDuration__RefAssignment_6_1 ) )
            {
            // InternalDuration.g:1272:1: ( ( rule__ComputationDuration__RefAssignment_6_1 ) )
            // InternalDuration.g:1273:2: ( rule__ComputationDuration__RefAssignment_6_1 )
            {
             before(grammarAccess.getComputationDurationAccess().getRefAssignment_6_1()); 
            // InternalDuration.g:1274:2: ( rule__ComputationDuration__RefAssignment_6_1 )
            // InternalDuration.g:1274:3: rule__ComputationDuration__RefAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__ComputationDuration__RefAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getComputationDurationAccess().getRefAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__Group_6__1__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group__0"
    // InternalDuration.g:1283:1: rule__CommunicationDuration__Group__0 : rule__CommunicationDuration__Group__0__Impl rule__CommunicationDuration__Group__1 ;
    public final void rule__CommunicationDuration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1287:1: ( rule__CommunicationDuration__Group__0__Impl rule__CommunicationDuration__Group__1 )
            // InternalDuration.g:1288:2: rule__CommunicationDuration__Group__0__Impl rule__CommunicationDuration__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__CommunicationDuration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__0"


    // $ANTLR start "rule__CommunicationDuration__Group__0__Impl"
    // InternalDuration.g:1295:1: rule__CommunicationDuration__Group__0__Impl : ( ( rule__CommunicationDuration__DisplayedOverlapAssignment_0 )? ) ;
    public final void rule__CommunicationDuration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1299:1: ( ( ( rule__CommunicationDuration__DisplayedOverlapAssignment_0 )? ) )
            // InternalDuration.g:1300:1: ( ( rule__CommunicationDuration__DisplayedOverlapAssignment_0 )? )
            {
            // InternalDuration.g:1300:1: ( ( rule__CommunicationDuration__DisplayedOverlapAssignment_0 )? )
            // InternalDuration.g:1301:2: ( rule__CommunicationDuration__DisplayedOverlapAssignment_0 )?
            {
             before(grammarAccess.getCommunicationDurationAccess().getDisplayedOverlapAssignment_0()); 
            // InternalDuration.g:1302:2: ( rule__CommunicationDuration__DisplayedOverlapAssignment_0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==28) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalDuration.g:1302:3: rule__CommunicationDuration__DisplayedOverlapAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CommunicationDuration__DisplayedOverlapAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCommunicationDurationAccess().getDisplayedOverlapAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__0__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group__1"
    // InternalDuration.g:1310:1: rule__CommunicationDuration__Group__1 : rule__CommunicationDuration__Group__1__Impl rule__CommunicationDuration__Group__2 ;
    public final void rule__CommunicationDuration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1314:1: ( rule__CommunicationDuration__Group__1__Impl rule__CommunicationDuration__Group__2 )
            // InternalDuration.g:1315:2: rule__CommunicationDuration__Group__1__Impl rule__CommunicationDuration__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__CommunicationDuration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__1"


    // $ANTLR start "rule__CommunicationDuration__Group__1__Impl"
    // InternalDuration.g:1322:1: rule__CommunicationDuration__Group__1__Impl : ( 'CommunicationDuration' ) ;
    public final void rule__CommunicationDuration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1326:1: ( ( 'CommunicationDuration' ) )
            // InternalDuration.g:1327:1: ( 'CommunicationDuration' )
            {
            // InternalDuration.g:1327:1: ( 'CommunicationDuration' )
            // InternalDuration.g:1328:2: 'CommunicationDuration'
            {
             before(grammarAccess.getCommunicationDurationAccess().getCommunicationDurationKeyword_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getCommunicationDurationAccess().getCommunicationDurationKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__1__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group__2"
    // InternalDuration.g:1337:1: rule__CommunicationDuration__Group__2 : rule__CommunicationDuration__Group__2__Impl rule__CommunicationDuration__Group__3 ;
    public final void rule__CommunicationDuration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1341:1: ( rule__CommunicationDuration__Group__2__Impl rule__CommunicationDuration__Group__3 )
            // InternalDuration.g:1342:2: rule__CommunicationDuration__Group__2__Impl rule__CommunicationDuration__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__CommunicationDuration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__2"


    // $ANTLR start "rule__CommunicationDuration__Group__2__Impl"
    // InternalDuration.g:1349:1: rule__CommunicationDuration__Group__2__Impl : ( ( rule__CommunicationDuration__Alternatives_2 ) ) ;
    public final void rule__CommunicationDuration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1353:1: ( ( ( rule__CommunicationDuration__Alternatives_2 ) ) )
            // InternalDuration.g:1354:1: ( ( rule__CommunicationDuration__Alternatives_2 ) )
            {
            // InternalDuration.g:1354:1: ( ( rule__CommunicationDuration__Alternatives_2 ) )
            // InternalDuration.g:1355:2: ( rule__CommunicationDuration__Alternatives_2 )
            {
             before(grammarAccess.getCommunicationDurationAccess().getAlternatives_2()); 
            // InternalDuration.g:1356:2: ( rule__CommunicationDuration__Alternatives_2 )
            // InternalDuration.g:1356:3: rule__CommunicationDuration__Alternatives_2
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getCommunicationDurationAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__2__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group__3"
    // InternalDuration.g:1364:1: rule__CommunicationDuration__Group__3 : rule__CommunicationDuration__Group__3__Impl rule__CommunicationDuration__Group__4 ;
    public final void rule__CommunicationDuration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1368:1: ( rule__CommunicationDuration__Group__3__Impl rule__CommunicationDuration__Group__4 )
            // InternalDuration.g:1369:2: rule__CommunicationDuration__Group__3__Impl rule__CommunicationDuration__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__CommunicationDuration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__3"


    // $ANTLR start "rule__CommunicationDuration__Group__3__Impl"
    // InternalDuration.g:1376:1: rule__CommunicationDuration__Group__3__Impl : ( ( rule__CommunicationDuration__StartAssignment_3 ) ) ;
    public final void rule__CommunicationDuration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1380:1: ( ( ( rule__CommunicationDuration__StartAssignment_3 ) ) )
            // InternalDuration.g:1381:1: ( ( rule__CommunicationDuration__StartAssignment_3 ) )
            {
            // InternalDuration.g:1381:1: ( ( rule__CommunicationDuration__StartAssignment_3 ) )
            // InternalDuration.g:1382:2: ( rule__CommunicationDuration__StartAssignment_3 )
            {
             before(grammarAccess.getCommunicationDurationAccess().getStartAssignment_3()); 
            // InternalDuration.g:1383:2: ( rule__CommunicationDuration__StartAssignment_3 )
            // InternalDuration.g:1383:3: rule__CommunicationDuration__StartAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__StartAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCommunicationDurationAccess().getStartAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__3__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group__4"
    // InternalDuration.g:1391:1: rule__CommunicationDuration__Group__4 : rule__CommunicationDuration__Group__4__Impl rule__CommunicationDuration__Group__5 ;
    public final void rule__CommunicationDuration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1395:1: ( rule__CommunicationDuration__Group__4__Impl rule__CommunicationDuration__Group__5 )
            // InternalDuration.g:1396:2: rule__CommunicationDuration__Group__4__Impl rule__CommunicationDuration__Group__5
            {
            pushFollow(FOLLOW_12);
            rule__CommunicationDuration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__4"


    // $ANTLR start "rule__CommunicationDuration__Group__4__Impl"
    // InternalDuration.g:1403:1: rule__CommunicationDuration__Group__4__Impl : ( ( rule__CommunicationDuration__Alternatives_4 ) ) ;
    public final void rule__CommunicationDuration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1407:1: ( ( ( rule__CommunicationDuration__Alternatives_4 ) ) )
            // InternalDuration.g:1408:1: ( ( rule__CommunicationDuration__Alternatives_4 ) )
            {
            // InternalDuration.g:1408:1: ( ( rule__CommunicationDuration__Alternatives_4 ) )
            // InternalDuration.g:1409:2: ( rule__CommunicationDuration__Alternatives_4 )
            {
             before(grammarAccess.getCommunicationDurationAccess().getAlternatives_4()); 
            // InternalDuration.g:1410:2: ( rule__CommunicationDuration__Alternatives_4 )
            // InternalDuration.g:1410:3: rule__CommunicationDuration__Alternatives_4
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Alternatives_4();

            state._fsp--;


            }

             after(grammarAccess.getCommunicationDurationAccess().getAlternatives_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__4__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group__5"
    // InternalDuration.g:1418:1: rule__CommunicationDuration__Group__5 : rule__CommunicationDuration__Group__5__Impl rule__CommunicationDuration__Group__6 ;
    public final void rule__CommunicationDuration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1422:1: ( rule__CommunicationDuration__Group__5__Impl rule__CommunicationDuration__Group__6 )
            // InternalDuration.g:1423:2: rule__CommunicationDuration__Group__5__Impl rule__CommunicationDuration__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__CommunicationDuration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__5"


    // $ANTLR start "rule__CommunicationDuration__Group__5__Impl"
    // InternalDuration.g:1430:1: rule__CommunicationDuration__Group__5__Impl : ( ( rule__CommunicationDuration__EndAssignment_5 ) ) ;
    public final void rule__CommunicationDuration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1434:1: ( ( ( rule__CommunicationDuration__EndAssignment_5 ) ) )
            // InternalDuration.g:1435:1: ( ( rule__CommunicationDuration__EndAssignment_5 ) )
            {
            // InternalDuration.g:1435:1: ( ( rule__CommunicationDuration__EndAssignment_5 ) )
            // InternalDuration.g:1436:2: ( rule__CommunicationDuration__EndAssignment_5 )
            {
             before(grammarAccess.getCommunicationDurationAccess().getEndAssignment_5()); 
            // InternalDuration.g:1437:2: ( rule__CommunicationDuration__EndAssignment_5 )
            // InternalDuration.g:1437:3: rule__CommunicationDuration__EndAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__EndAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getCommunicationDurationAccess().getEndAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__5__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group__6"
    // InternalDuration.g:1445:1: rule__CommunicationDuration__Group__6 : rule__CommunicationDuration__Group__6__Impl rule__CommunicationDuration__Group__7 ;
    public final void rule__CommunicationDuration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1449:1: ( rule__CommunicationDuration__Group__6__Impl rule__CommunicationDuration__Group__7 )
            // InternalDuration.g:1450:2: rule__CommunicationDuration__Group__6__Impl rule__CommunicationDuration__Group__7
            {
            pushFollow(FOLLOW_19);
            rule__CommunicationDuration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__6"


    // $ANTLR start "rule__CommunicationDuration__Group__6__Impl"
    // InternalDuration.g:1457:1: rule__CommunicationDuration__Group__6__Impl : ( ( rule__CommunicationDuration__Group_6__0 )? ) ;
    public final void rule__CommunicationDuration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1461:1: ( ( ( rule__CommunicationDuration__Group_6__0 )? ) )
            // InternalDuration.g:1462:1: ( ( rule__CommunicationDuration__Group_6__0 )? )
            {
            // InternalDuration.g:1462:1: ( ( rule__CommunicationDuration__Group_6__0 )? )
            // InternalDuration.g:1463:2: ( rule__CommunicationDuration__Group_6__0 )?
            {
             before(grammarAccess.getCommunicationDurationAccess().getGroup_6()); 
            // InternalDuration.g:1464:2: ( rule__CommunicationDuration__Group_6__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=11 && LA16_0<=12)) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalDuration.g:1464:3: rule__CommunicationDuration__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CommunicationDuration__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCommunicationDurationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__6__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group__7"
    // InternalDuration.g:1472:1: rule__CommunicationDuration__Group__7 : rule__CommunicationDuration__Group__7__Impl ;
    public final void rule__CommunicationDuration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1476:1: ( rule__CommunicationDuration__Group__7__Impl )
            // InternalDuration.g:1477:2: rule__CommunicationDuration__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__7"


    // $ANTLR start "rule__CommunicationDuration__Group__7__Impl"
    // InternalDuration.g:1483:1: rule__CommunicationDuration__Group__7__Impl : ( ';' ) ;
    public final void rule__CommunicationDuration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1487:1: ( ( ';' ) )
            // InternalDuration.g:1488:1: ( ';' )
            {
            // InternalDuration.g:1488:1: ( ';' )
            // InternalDuration.g:1489:2: ';'
            {
             before(grammarAccess.getCommunicationDurationAccess().getSemicolonKeyword_7()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getCommunicationDurationAccess().getSemicolonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group__7__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group_6__0"
    // InternalDuration.g:1499:1: rule__CommunicationDuration__Group_6__0 : rule__CommunicationDuration__Group_6__0__Impl rule__CommunicationDuration__Group_6__1 ;
    public final void rule__CommunicationDuration__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1503:1: ( rule__CommunicationDuration__Group_6__0__Impl rule__CommunicationDuration__Group_6__1 )
            // InternalDuration.g:1504:2: rule__CommunicationDuration__Group_6__0__Impl rule__CommunicationDuration__Group_6__1
            {
            pushFollow(FOLLOW_12);
            rule__CommunicationDuration__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group_6__0"


    // $ANTLR start "rule__CommunicationDuration__Group_6__0__Impl"
    // InternalDuration.g:1511:1: rule__CommunicationDuration__Group_6__0__Impl : ( ( rule__CommunicationDuration__Alternatives_6_0 ) ) ;
    public final void rule__CommunicationDuration__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1515:1: ( ( ( rule__CommunicationDuration__Alternatives_6_0 ) ) )
            // InternalDuration.g:1516:1: ( ( rule__CommunicationDuration__Alternatives_6_0 ) )
            {
            // InternalDuration.g:1516:1: ( ( rule__CommunicationDuration__Alternatives_6_0 ) )
            // InternalDuration.g:1517:2: ( rule__CommunicationDuration__Alternatives_6_0 )
            {
             before(grammarAccess.getCommunicationDurationAccess().getAlternatives_6_0()); 
            // InternalDuration.g:1518:2: ( rule__CommunicationDuration__Alternatives_6_0 )
            // InternalDuration.g:1518:3: rule__CommunicationDuration__Alternatives_6_0
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Alternatives_6_0();

            state._fsp--;


            }

             after(grammarAccess.getCommunicationDurationAccess().getAlternatives_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group_6__0__Impl"


    // $ANTLR start "rule__CommunicationDuration__Group_6__1"
    // InternalDuration.g:1526:1: rule__CommunicationDuration__Group_6__1 : rule__CommunicationDuration__Group_6__1__Impl ;
    public final void rule__CommunicationDuration__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1530:1: ( rule__CommunicationDuration__Group_6__1__Impl )
            // InternalDuration.g:1531:2: rule__CommunicationDuration__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group_6__1"


    // $ANTLR start "rule__CommunicationDuration__Group_6__1__Impl"
    // InternalDuration.g:1537:1: rule__CommunicationDuration__Group_6__1__Impl : ( ( rule__CommunicationDuration__RefAssignment_6_1 ) ) ;
    public final void rule__CommunicationDuration__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1541:1: ( ( ( rule__CommunicationDuration__RefAssignment_6_1 ) ) )
            // InternalDuration.g:1542:1: ( ( rule__CommunicationDuration__RefAssignment_6_1 ) )
            {
            // InternalDuration.g:1542:1: ( ( rule__CommunicationDuration__RefAssignment_6_1 ) )
            // InternalDuration.g:1543:2: ( rule__CommunicationDuration__RefAssignment_6_1 )
            {
             before(grammarAccess.getCommunicationDurationAccess().getRefAssignment_6_1()); 
            // InternalDuration.g:1544:2: ( rule__CommunicationDuration__RefAssignment_6_1 )
            // InternalDuration.g:1544:3: rule__CommunicationDuration__RefAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__CommunicationDuration__RefAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getCommunicationDurationAccess().getRefAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__Group_6__1__Impl"


    // $ANTLR start "rule__EInt__Group__0"
    // InternalDuration.g:1553:1: rule__EInt__Group__0 : rule__EInt__Group__0__Impl rule__EInt__Group__1 ;
    public final void rule__EInt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1557:1: ( rule__EInt__Group__0__Impl rule__EInt__Group__1 )
            // InternalDuration.g:1558:2: rule__EInt__Group__0__Impl rule__EInt__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__EInt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EInt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0"


    // $ANTLR start "rule__EInt__Group__0__Impl"
    // InternalDuration.g:1565:1: rule__EInt__Group__0__Impl : ( ( '-' )? ) ;
    public final void rule__EInt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1569:1: ( ( ( '-' )? ) )
            // InternalDuration.g:1570:1: ( ( '-' )? )
            {
            // InternalDuration.g:1570:1: ( ( '-' )? )
            // InternalDuration.g:1571:2: ( '-' )?
            {
             before(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 
            // InternalDuration.g:1572:2: ( '-' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==27) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalDuration.g:1572:3: '-'
                    {
                    match(input,27,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0__Impl"


    // $ANTLR start "rule__EInt__Group__1"
    // InternalDuration.g:1580:1: rule__EInt__Group__1 : rule__EInt__Group__1__Impl ;
    public final void rule__EInt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1584:1: ( rule__EInt__Group__1__Impl )
            // InternalDuration.g:1585:2: rule__EInt__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1"


    // $ANTLR start "rule__EInt__Group__1__Impl"
    // InternalDuration.g:1591:1: rule__EInt__Group__1__Impl : ( RULE_INT ) ;
    public final void rule__EInt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1595:1: ( ( RULE_INT ) )
            // InternalDuration.g:1596:1: ( RULE_INT )
            {
            // InternalDuration.g:1596:1: ( RULE_INT )
            // InternalDuration.g:1597:2: RULE_INT
            {
             before(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1__Impl"


    // $ANTLR start "rule__DurationModel__ImportStatementAssignment_2"
    // InternalDuration.g:1607:1: rule__DurationModel__ImportStatementAssignment_2 : ( ruleImportStatement ) ;
    public final void rule__DurationModel__ImportStatementAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1611:1: ( ( ruleImportStatement ) )
            // InternalDuration.g:1612:2: ( ruleImportStatement )
            {
            // InternalDuration.g:1612:2: ( ruleImportStatement )
            // InternalDuration.g:1613:3: ruleImportStatement
            {
             before(grammarAccess.getDurationModelAccess().getImportStatementImportStatementParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImportStatement();

            state._fsp--;

             after(grammarAccess.getDurationModelAccess().getImportStatementImportStatementParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__ImportStatementAssignment_2"


    // $ANTLR start "rule__DurationModel__DurationsAssignment_4_0"
    // InternalDuration.g:1622:1: rule__DurationModel__DurationsAssignment_4_0 : ( ruleDuration ) ;
    public final void rule__DurationModel__DurationsAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1626:1: ( ( ruleDuration ) )
            // InternalDuration.g:1627:2: ( ruleDuration )
            {
            // InternalDuration.g:1627:2: ( ruleDuration )
            // InternalDuration.g:1628:3: ruleDuration
            {
             before(grammarAccess.getDurationModelAccess().getDurationsDurationParserRuleCall_4_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDuration();

            state._fsp--;

             after(grammarAccess.getDurationModelAccess().getDurationsDurationParserRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__DurationsAssignment_4_0"


    // $ANTLR start "rule__DurationModel__DurationsAssignment_4_1"
    // InternalDuration.g:1637:1: rule__DurationModel__DurationsAssignment_4_1 : ( ruleDuration ) ;
    public final void rule__DurationModel__DurationsAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1641:1: ( ( ruleDuration ) )
            // InternalDuration.g:1642:2: ( ruleDuration )
            {
            // InternalDuration.g:1642:2: ( ruleDuration )
            // InternalDuration.g:1643:3: ruleDuration
            {
             before(grammarAccess.getDurationModelAccess().getDurationsDurationParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDuration();

            state._fsp--;

             after(grammarAccess.getDurationModelAccess().getDurationsDurationParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DurationModel__DurationsAssignment_4_1"


    // $ANTLR start "rule__ImportStatement__ImportURIAssignment_1"
    // InternalDuration.g:1652:1: rule__ImportStatement__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__ImportStatement__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1656:1: ( ( RULE_STRING ) )
            // InternalDuration.g:1657:2: ( RULE_STRING )
            {
            // InternalDuration.g:1657:2: ( RULE_STRING )
            // InternalDuration.g:1658:3: RULE_STRING
            {
             before(grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__ImportURIAssignment_1"


    // $ANTLR start "rule__ImportStatement__AliasAssignment_2"
    // InternalDuration.g:1667:1: rule__ImportStatement__AliasAssignment_2 : ( ( ';' ) ) ;
    public final void rule__ImportStatement__AliasAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1671:1: ( ( ( ';' ) ) )
            // InternalDuration.g:1672:2: ( ( ';' ) )
            {
            // InternalDuration.g:1672:2: ( ( ';' ) )
            // InternalDuration.g:1673:3: ( ';' )
            {
             before(grammarAccess.getImportStatementAccess().getAliasSemicolonKeyword_2_0()); 
            // InternalDuration.g:1674:3: ( ';' )
            // InternalDuration.g:1675:4: ';'
            {
             before(grammarAccess.getImportStatementAccess().getAliasSemicolonKeyword_2_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getAliasSemicolonKeyword_2_0()); 

            }

             after(grammarAccess.getImportStatementAccess().getAliasSemicolonKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__AliasAssignment_2"


    // $ANTLR start "rule__Timer__DelayAssignment_1_2"
    // InternalDuration.g:1686:1: rule__Timer__DelayAssignment_1_2 : ( ruleEInt ) ;
    public final void rule__Timer__DelayAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1690:1: ( ( ruleEInt ) )
            // InternalDuration.g:1691:2: ( ruleEInt )
            {
            // InternalDuration.g:1691:2: ( ruleEInt )
            // InternalDuration.g:1692:3: ruleEInt
            {
             before(grammarAccess.getTimerAccess().getDelayEIntParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getTimerAccess().getDelayEIntParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__DelayAssignment_1_2"


    // $ANTLR start "rule__Timer__StartAssignment_3"
    // InternalDuration.g:1701:1: rule__Timer__StartAssignment_3 : ( ( ruleEString ) ) ;
    public final void rule__Timer__StartAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1705:1: ( ( ( ruleEString ) ) )
            // InternalDuration.g:1706:2: ( ( ruleEString ) )
            {
            // InternalDuration.g:1706:2: ( ( ruleEString ) )
            // InternalDuration.g:1707:3: ( ruleEString )
            {
             before(grammarAccess.getTimerAccess().getStartConcreteEntityCrossReference_3_0()); 
            // InternalDuration.g:1708:3: ( ruleEString )
            // InternalDuration.g:1709:4: ruleEString
            {
             before(grammarAccess.getTimerAccess().getStartConcreteEntityEStringParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTimerAccess().getStartConcreteEntityEStringParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getTimerAccess().getStartConcreteEntityCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__StartAssignment_3"


    // $ANTLR start "rule__Timer__RefAssignment_5"
    // InternalDuration.g:1720:1: rule__Timer__RefAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__Timer__RefAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1724:1: ( ( ( ruleEString ) ) )
            // InternalDuration.g:1725:2: ( ( ruleEString ) )
            {
            // InternalDuration.g:1725:2: ( ( ruleEString ) )
            // InternalDuration.g:1726:3: ( ruleEString )
            {
             before(grammarAccess.getTimerAccess().getRefConcreteEntityCrossReference_5_0()); 
            // InternalDuration.g:1727:3: ( ruleEString )
            // InternalDuration.g:1728:4: ruleEString
            {
             before(grammarAccess.getTimerAccess().getRefConcreteEntityEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTimerAccess().getRefConcreteEntityEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getTimerAccess().getRefConcreteEntityCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timer__RefAssignment_5"


    // $ANTLR start "rule__ComputationDuration__DisplayedOverlapAssignment_0"
    // InternalDuration.g:1739:1: rule__ComputationDuration__DisplayedOverlapAssignment_0 : ( ( 'displayedOverlap' ) ) ;
    public final void rule__ComputationDuration__DisplayedOverlapAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1743:1: ( ( ( 'displayedOverlap' ) ) )
            // InternalDuration.g:1744:2: ( ( 'displayedOverlap' ) )
            {
            // InternalDuration.g:1744:2: ( ( 'displayedOverlap' ) )
            // InternalDuration.g:1745:3: ( 'displayedOverlap' )
            {
             before(grammarAccess.getComputationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0()); 
            // InternalDuration.g:1746:3: ( 'displayedOverlap' )
            // InternalDuration.g:1747:4: 'displayedOverlap'
            {
             before(grammarAccess.getComputationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getComputationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0()); 

            }

             after(grammarAccess.getComputationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__DisplayedOverlapAssignment_0"


    // $ANTLR start "rule__ComputationDuration__StartAssignment_3"
    // InternalDuration.g:1758:1: rule__ComputationDuration__StartAssignment_3 : ( ( ruleEString ) ) ;
    public final void rule__ComputationDuration__StartAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1762:1: ( ( ( ruleEString ) ) )
            // InternalDuration.g:1763:2: ( ( ruleEString ) )
            {
            // InternalDuration.g:1763:2: ( ( ruleEString ) )
            // InternalDuration.g:1764:3: ( ruleEString )
            {
             before(grammarAccess.getComputationDurationAccess().getStartConcreteEntityCrossReference_3_0()); 
            // InternalDuration.g:1765:3: ( ruleEString )
            // InternalDuration.g:1766:4: ruleEString
            {
             before(grammarAccess.getComputationDurationAccess().getStartConcreteEntityEStringParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getComputationDurationAccess().getStartConcreteEntityEStringParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getComputationDurationAccess().getStartConcreteEntityCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__StartAssignment_3"


    // $ANTLR start "rule__ComputationDuration__EndAssignment_5"
    // InternalDuration.g:1777:1: rule__ComputationDuration__EndAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__ComputationDuration__EndAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1781:1: ( ( ( ruleEString ) ) )
            // InternalDuration.g:1782:2: ( ( ruleEString ) )
            {
            // InternalDuration.g:1782:2: ( ( ruleEString ) )
            // InternalDuration.g:1783:3: ( ruleEString )
            {
             before(grammarAccess.getComputationDurationAccess().getEndConcreteEntityCrossReference_5_0()); 
            // InternalDuration.g:1784:3: ( ruleEString )
            // InternalDuration.g:1785:4: ruleEString
            {
             before(grammarAccess.getComputationDurationAccess().getEndConcreteEntityEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getComputationDurationAccess().getEndConcreteEntityEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getComputationDurationAccess().getEndConcreteEntityCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__EndAssignment_5"


    // $ANTLR start "rule__ComputationDuration__RefAssignment_6_1"
    // InternalDuration.g:1796:1: rule__ComputationDuration__RefAssignment_6_1 : ( ( ruleEString ) ) ;
    public final void rule__ComputationDuration__RefAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1800:1: ( ( ( ruleEString ) ) )
            // InternalDuration.g:1801:2: ( ( ruleEString ) )
            {
            // InternalDuration.g:1801:2: ( ( ruleEString ) )
            // InternalDuration.g:1802:3: ( ruleEString )
            {
             before(grammarAccess.getComputationDurationAccess().getRefConcreteEntityCrossReference_6_1_0()); 
            // InternalDuration.g:1803:3: ( ruleEString )
            // InternalDuration.g:1804:4: ruleEString
            {
             before(grammarAccess.getComputationDurationAccess().getRefConcreteEntityEStringParserRuleCall_6_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getComputationDurationAccess().getRefConcreteEntityEStringParserRuleCall_6_1_0_1()); 

            }

             after(grammarAccess.getComputationDurationAccess().getRefConcreteEntityCrossReference_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputationDuration__RefAssignment_6_1"


    // $ANTLR start "rule__CommunicationDuration__DisplayedOverlapAssignment_0"
    // InternalDuration.g:1815:1: rule__CommunicationDuration__DisplayedOverlapAssignment_0 : ( ( 'displayedOverlap' ) ) ;
    public final void rule__CommunicationDuration__DisplayedOverlapAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1819:1: ( ( ( 'displayedOverlap' ) ) )
            // InternalDuration.g:1820:2: ( ( 'displayedOverlap' ) )
            {
            // InternalDuration.g:1820:2: ( ( 'displayedOverlap' ) )
            // InternalDuration.g:1821:3: ( 'displayedOverlap' )
            {
             before(grammarAccess.getCommunicationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0()); 
            // InternalDuration.g:1822:3: ( 'displayedOverlap' )
            // InternalDuration.g:1823:4: 'displayedOverlap'
            {
             before(grammarAccess.getCommunicationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getCommunicationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0()); 

            }

             after(grammarAccess.getCommunicationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__DisplayedOverlapAssignment_0"


    // $ANTLR start "rule__CommunicationDuration__StartAssignment_3"
    // InternalDuration.g:1834:1: rule__CommunicationDuration__StartAssignment_3 : ( ( ruleEString ) ) ;
    public final void rule__CommunicationDuration__StartAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1838:1: ( ( ( ruleEString ) ) )
            // InternalDuration.g:1839:2: ( ( ruleEString ) )
            {
            // InternalDuration.g:1839:2: ( ( ruleEString ) )
            // InternalDuration.g:1840:3: ( ruleEString )
            {
             before(grammarAccess.getCommunicationDurationAccess().getStartConcreteEntityCrossReference_3_0()); 
            // InternalDuration.g:1841:3: ( ruleEString )
            // InternalDuration.g:1842:4: ruleEString
            {
             before(grammarAccess.getCommunicationDurationAccess().getStartConcreteEntityEStringParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCommunicationDurationAccess().getStartConcreteEntityEStringParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getCommunicationDurationAccess().getStartConcreteEntityCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__StartAssignment_3"


    // $ANTLR start "rule__CommunicationDuration__EndAssignment_5"
    // InternalDuration.g:1853:1: rule__CommunicationDuration__EndAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__CommunicationDuration__EndAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1857:1: ( ( ( ruleEString ) ) )
            // InternalDuration.g:1858:2: ( ( ruleEString ) )
            {
            // InternalDuration.g:1858:2: ( ( ruleEString ) )
            // InternalDuration.g:1859:3: ( ruleEString )
            {
             before(grammarAccess.getCommunicationDurationAccess().getEndConcreteEntityCrossReference_5_0()); 
            // InternalDuration.g:1860:3: ( ruleEString )
            // InternalDuration.g:1861:4: ruleEString
            {
             before(grammarAccess.getCommunicationDurationAccess().getEndConcreteEntityEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCommunicationDurationAccess().getEndConcreteEntityEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getCommunicationDurationAccess().getEndConcreteEntityCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__EndAssignment_5"


    // $ANTLR start "rule__CommunicationDuration__RefAssignment_6_1"
    // InternalDuration.g:1872:1: rule__CommunicationDuration__RefAssignment_6_1 : ( ( ruleEString ) ) ;
    public final void rule__CommunicationDuration__RefAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDuration.g:1876:1: ( ( ( ruleEString ) ) )
            // InternalDuration.g:1877:2: ( ( ruleEString ) )
            {
            // InternalDuration.g:1877:2: ( ( ruleEString ) )
            // InternalDuration.g:1878:3: ( ruleEString )
            {
             before(grammarAccess.getCommunicationDurationAccess().getRefConcreteEntityCrossReference_6_1_0()); 
            // InternalDuration.g:1879:3: ( ruleEString )
            // InternalDuration.g:1880:4: ruleEString
            {
             before(grammarAccess.getCommunicationDurationAccess().getRefConcreteEntityEStringParserRuleCall_6_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCommunicationDurationAccess().getRefConcreteEntityEStringParserRuleCall_6_1_0_1()); 

            }

             after(grammarAccess.getCommunicationDurationAccess().getRefConcreteEntityCrossReference_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommunicationDuration__RefAssignment_6_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000016280000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000016200000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000016200002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000804000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000008000040L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000012000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000401800L});

}