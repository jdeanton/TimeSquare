/*n * generated by Xtext 
 by action of Julien Deantoni */
package fr.inria.aoste.timesquare.duration.xtext.ui

import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class DurationUiModule extends AbstractDurationUiModule {
}
