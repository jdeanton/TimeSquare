/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.duration.xtext;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class DurationStandaloneSetup extends DurationStandaloneSetupGenerated{

	public static void doSetup() {
		new DurationStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

