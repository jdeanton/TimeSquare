package fr.inria.aoste.timesquare.duration.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.inria.aoste.timesquare.duration.xtext.services.DurationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDurationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_INT", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'DurationModel'", "'{'", "'}'", "'import'", "';'", "'Timer'", "'delay'", "'='", "'start'", "'reference'", "'ref'", "'displayedOverlap'", "'ComputationDuration'", "'from'", "'to'", "'end'", "'CommunicationDuration'", "'-'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=6;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDurationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDurationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDurationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDuration.g"; }



     	private DurationGrammarAccess grammarAccess;

        public InternalDurationParser(TokenStream input, DurationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "DurationModel";
       	}

       	@Override
       	protected DurationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDurationModel"
    // InternalDuration.g:63:1: entryRuleDurationModel returns [EObject current=null] : iv_ruleDurationModel= ruleDurationModel EOF ;
    public final EObject entryRuleDurationModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDurationModel = null;


        try {
            // InternalDuration.g:63:54: (iv_ruleDurationModel= ruleDurationModel EOF )
            // InternalDuration.g:64:2: iv_ruleDurationModel= ruleDurationModel EOF
            {
             newCompositeNode(grammarAccess.getDurationModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDurationModel=ruleDurationModel();

            state._fsp--;

             current =iv_ruleDurationModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDurationModel"


    // $ANTLR start "ruleDurationModel"
    // InternalDuration.g:70:1: ruleDurationModel returns [EObject current=null] : ( () otherlv_1= 'DurationModel' ( (lv_importStatement_2_0= ruleImportStatement ) ) otherlv_3= '{' ( ( (lv_durations_4_0= ruleDuration ) ) ( (lv_durations_5_0= ruleDuration ) )* )? otherlv_6= '}' ) ;
    public final EObject ruleDurationModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        EObject lv_importStatement_2_0 = null;

        EObject lv_durations_4_0 = null;

        EObject lv_durations_5_0 = null;



        	enterRule();

        try {
            // InternalDuration.g:76:2: ( ( () otherlv_1= 'DurationModel' ( (lv_importStatement_2_0= ruleImportStatement ) ) otherlv_3= '{' ( ( (lv_durations_4_0= ruleDuration ) ) ( (lv_durations_5_0= ruleDuration ) )* )? otherlv_6= '}' ) )
            // InternalDuration.g:77:2: ( () otherlv_1= 'DurationModel' ( (lv_importStatement_2_0= ruleImportStatement ) ) otherlv_3= '{' ( ( (lv_durations_4_0= ruleDuration ) ) ( (lv_durations_5_0= ruleDuration ) )* )? otherlv_6= '}' )
            {
            // InternalDuration.g:77:2: ( () otherlv_1= 'DurationModel' ( (lv_importStatement_2_0= ruleImportStatement ) ) otherlv_3= '{' ( ( (lv_durations_4_0= ruleDuration ) ) ( (lv_durations_5_0= ruleDuration ) )* )? otherlv_6= '}' )
            // InternalDuration.g:78:3: () otherlv_1= 'DurationModel' ( (lv_importStatement_2_0= ruleImportStatement ) ) otherlv_3= '{' ( ( (lv_durations_4_0= ruleDuration ) ) ( (lv_durations_5_0= ruleDuration ) )* )? otherlv_6= '}'
            {
            // InternalDuration.g:78:3: ()
            // InternalDuration.g:79:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDurationModelAccess().getDurationModelAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getDurationModelAccess().getDurationModelKeyword_1());
            		
            // InternalDuration.g:89:3: ( (lv_importStatement_2_0= ruleImportStatement ) )
            // InternalDuration.g:90:4: (lv_importStatement_2_0= ruleImportStatement )
            {
            // InternalDuration.g:90:4: (lv_importStatement_2_0= ruleImportStatement )
            // InternalDuration.g:91:5: lv_importStatement_2_0= ruleImportStatement
            {

            					newCompositeNode(grammarAccess.getDurationModelAccess().getImportStatementImportStatementParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_importStatement_2_0=ruleImportStatement();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDurationModelRule());
            					}
            					set(
            						current,
            						"importStatement",
            						lv_importStatement_2_0,
            						"fr.inria.aoste.timesquare.duration.xtext.Duration.ImportStatement");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getDurationModelAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalDuration.g:112:3: ( ( (lv_durations_4_0= ruleDuration ) ) ( (lv_durations_5_0= ruleDuration ) )* )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==16||(LA2_0>=22 && LA2_0<=23)||LA2_0==27) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalDuration.g:113:4: ( (lv_durations_4_0= ruleDuration ) ) ( (lv_durations_5_0= ruleDuration ) )*
                    {
                    // InternalDuration.g:113:4: ( (lv_durations_4_0= ruleDuration ) )
                    // InternalDuration.g:114:5: (lv_durations_4_0= ruleDuration )
                    {
                    // InternalDuration.g:114:5: (lv_durations_4_0= ruleDuration )
                    // InternalDuration.g:115:6: lv_durations_4_0= ruleDuration
                    {

                    						newCompositeNode(grammarAccess.getDurationModelAccess().getDurationsDurationParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_durations_4_0=ruleDuration();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDurationModelRule());
                    						}
                    						add(
                    							current,
                    							"durations",
                    							lv_durations_4_0,
                    							"fr.inria.aoste.timesquare.duration.xtext.Duration.Duration");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDuration.g:132:4: ( (lv_durations_5_0= ruleDuration ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==16||(LA1_0>=22 && LA1_0<=23)||LA1_0==27) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalDuration.g:133:5: (lv_durations_5_0= ruleDuration )
                    	    {
                    	    // InternalDuration.g:133:5: (lv_durations_5_0= ruleDuration )
                    	    // InternalDuration.g:134:6: lv_durations_5_0= ruleDuration
                    	    {

                    	    						newCompositeNode(grammarAccess.getDurationModelAccess().getDurationsDurationParserRuleCall_4_1_0());
                    	    					
                    	    pushFollow(FOLLOW_5);
                    	    lv_durations_5_0=ruleDuration();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getDurationModelRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"durations",
                    	    							lv_durations_5_0,
                    	    							"fr.inria.aoste.timesquare.duration.xtext.Duration.Duration");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getDurationModelAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDurationModel"


    // $ANTLR start "entryRuleImportStatement"
    // InternalDuration.g:160:1: entryRuleImportStatement returns [EObject current=null] : iv_ruleImportStatement= ruleImportStatement EOF ;
    public final EObject entryRuleImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportStatement = null;


        try {
            // InternalDuration.g:160:56: (iv_ruleImportStatement= ruleImportStatement EOF )
            // InternalDuration.g:161:2: iv_ruleImportStatement= ruleImportStatement EOF
            {
             newCompositeNode(grammarAccess.getImportStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImportStatement=ruleImportStatement();

            state._fsp--;

             current =iv_ruleImportStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalDuration.g:167:1: ruleImportStatement returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ( (lv_alias_2_0= ';' ) ) ) ;
    public final EObject ruleImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;
        Token lv_alias_2_0=null;


        	enterRule();

        try {
            // InternalDuration.g:173:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ( (lv_alias_2_0= ';' ) ) ) )
            // InternalDuration.g:174:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ( (lv_alias_2_0= ';' ) ) )
            {
            // InternalDuration.g:174:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ( (lv_alias_2_0= ';' ) ) )
            // InternalDuration.g:175:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ( (lv_alias_2_0= ';' ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getImportStatementAccess().getImportKeyword_0());
            		
            // InternalDuration.g:179:3: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalDuration.g:180:4: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalDuration.g:180:4: (lv_importURI_1_0= RULE_STRING )
            // InternalDuration.g:181:5: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_7); 

            					newLeafNode(lv_importURI_1_0, grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImportStatementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"importURI",
            						lv_importURI_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalDuration.g:197:3: ( (lv_alias_2_0= ';' ) )
            // InternalDuration.g:198:4: (lv_alias_2_0= ';' )
            {
            // InternalDuration.g:198:4: (lv_alias_2_0= ';' )
            // InternalDuration.g:199:5: lv_alias_2_0= ';'
            {
            lv_alias_2_0=(Token)match(input,15,FOLLOW_2); 

            					newLeafNode(lv_alias_2_0, grammarAccess.getImportStatementAccess().getAliasSemicolonKeyword_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImportStatementRule());
            					}
            					setWithLastConsumed(current, "alias", lv_alias_2_0, ";");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleDuration"
    // InternalDuration.g:215:1: entryRuleDuration returns [EObject current=null] : iv_ruleDuration= ruleDuration EOF ;
    public final EObject entryRuleDuration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDuration = null;


        try {
            // InternalDuration.g:215:49: (iv_ruleDuration= ruleDuration EOF )
            // InternalDuration.g:216:2: iv_ruleDuration= ruleDuration EOF
            {
             newCompositeNode(grammarAccess.getDurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDuration=ruleDuration();

            state._fsp--;

             current =iv_ruleDuration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDuration"


    // $ANTLR start "ruleDuration"
    // InternalDuration.g:222:1: ruleDuration returns [EObject current=null] : (this_Timer_0= ruleTimer | this_ComputationDuration_1= ruleComputationDuration | this_CommunicationDuration_2= ruleCommunicationDuration ) ;
    public final EObject ruleDuration() throws RecognitionException {
        EObject current = null;

        EObject this_Timer_0 = null;

        EObject this_ComputationDuration_1 = null;

        EObject this_CommunicationDuration_2 = null;



        	enterRule();

        try {
            // InternalDuration.g:228:2: ( (this_Timer_0= ruleTimer | this_ComputationDuration_1= ruleComputationDuration | this_CommunicationDuration_2= ruleCommunicationDuration ) )
            // InternalDuration.g:229:2: (this_Timer_0= ruleTimer | this_ComputationDuration_1= ruleComputationDuration | this_CommunicationDuration_2= ruleCommunicationDuration )
            {
            // InternalDuration.g:229:2: (this_Timer_0= ruleTimer | this_ComputationDuration_1= ruleComputationDuration | this_CommunicationDuration_2= ruleCommunicationDuration )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt3=1;
                }
                break;
            case 22:
                {
                int LA3_2 = input.LA(2);

                if ( (LA3_2==27) ) {
                    alt3=3;
                }
                else if ( (LA3_2==23) ) {
                    alt3=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 2, input);

                    throw nvae;
                }
                }
                break;
            case 23:
                {
                alt3=2;
                }
                break;
            case 27:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalDuration.g:230:3: this_Timer_0= ruleTimer
                    {

                    			newCompositeNode(grammarAccess.getDurationAccess().getTimerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Timer_0=ruleTimer();

                    state._fsp--;


                    			current = this_Timer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDuration.g:239:3: this_ComputationDuration_1= ruleComputationDuration
                    {

                    			newCompositeNode(grammarAccess.getDurationAccess().getComputationDurationParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ComputationDuration_1=ruleComputationDuration();

                    state._fsp--;


                    			current = this_ComputationDuration_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalDuration.g:248:3: this_CommunicationDuration_2= ruleCommunicationDuration
                    {

                    			newCompositeNode(grammarAccess.getDurationAccess().getCommunicationDurationParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_CommunicationDuration_2=ruleCommunicationDuration();

                    state._fsp--;


                    			current = this_CommunicationDuration_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDuration"


    // $ANTLR start "entryRuleTimer"
    // InternalDuration.g:260:1: entryRuleTimer returns [EObject current=null] : iv_ruleTimer= ruleTimer EOF ;
    public final EObject entryRuleTimer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimer = null;


        try {
            // InternalDuration.g:260:46: (iv_ruleTimer= ruleTimer EOF )
            // InternalDuration.g:261:2: iv_ruleTimer= ruleTimer EOF
            {
             newCompositeNode(grammarAccess.getTimerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTimer=ruleTimer();

            state._fsp--;

             current =iv_ruleTimer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimer"


    // $ANTLR start "ruleTimer"
    // InternalDuration.g:267:1: ruleTimer returns [EObject current=null] : (otherlv_0= 'Timer' (otherlv_1= 'delay' otherlv_2= '=' ( (lv_delay_3_0= ruleEInt ) ) )? otherlv_4= 'start' ( ( ruleEString ) ) (otherlv_6= 'reference' | otherlv_7= 'ref' ) ( ( ruleEString ) ) otherlv_9= ';' ) ;
    public final EObject ruleTimer() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_delay_3_0 = null;



        	enterRule();

        try {
            // InternalDuration.g:273:2: ( (otherlv_0= 'Timer' (otherlv_1= 'delay' otherlv_2= '=' ( (lv_delay_3_0= ruleEInt ) ) )? otherlv_4= 'start' ( ( ruleEString ) ) (otherlv_6= 'reference' | otherlv_7= 'ref' ) ( ( ruleEString ) ) otherlv_9= ';' ) )
            // InternalDuration.g:274:2: (otherlv_0= 'Timer' (otherlv_1= 'delay' otherlv_2= '=' ( (lv_delay_3_0= ruleEInt ) ) )? otherlv_4= 'start' ( ( ruleEString ) ) (otherlv_6= 'reference' | otherlv_7= 'ref' ) ( ( ruleEString ) ) otherlv_9= ';' )
            {
            // InternalDuration.g:274:2: (otherlv_0= 'Timer' (otherlv_1= 'delay' otherlv_2= '=' ( (lv_delay_3_0= ruleEInt ) ) )? otherlv_4= 'start' ( ( ruleEString ) ) (otherlv_6= 'reference' | otherlv_7= 'ref' ) ( ( ruleEString ) ) otherlv_9= ';' )
            // InternalDuration.g:275:3: otherlv_0= 'Timer' (otherlv_1= 'delay' otherlv_2= '=' ( (lv_delay_3_0= ruleEInt ) ) )? otherlv_4= 'start' ( ( ruleEString ) ) (otherlv_6= 'reference' | otherlv_7= 'ref' ) ( ( ruleEString ) ) otherlv_9= ';'
            {
            otherlv_0=(Token)match(input,16,FOLLOW_8); 

            			newLeafNode(otherlv_0, grammarAccess.getTimerAccess().getTimerKeyword_0());
            		
            // InternalDuration.g:279:3: (otherlv_1= 'delay' otherlv_2= '=' ( (lv_delay_3_0= ruleEInt ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalDuration.g:280:4: otherlv_1= 'delay' otherlv_2= '=' ( (lv_delay_3_0= ruleEInt ) )
                    {
                    otherlv_1=(Token)match(input,17,FOLLOW_9); 

                    				newLeafNode(otherlv_1, grammarAccess.getTimerAccess().getDelayKeyword_1_0());
                    			
                    otherlv_2=(Token)match(input,18,FOLLOW_10); 

                    				newLeafNode(otherlv_2, grammarAccess.getTimerAccess().getEqualsSignKeyword_1_1());
                    			
                    // InternalDuration.g:288:4: ( (lv_delay_3_0= ruleEInt ) )
                    // InternalDuration.g:289:5: (lv_delay_3_0= ruleEInt )
                    {
                    // InternalDuration.g:289:5: (lv_delay_3_0= ruleEInt )
                    // InternalDuration.g:290:6: lv_delay_3_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getTimerAccess().getDelayEIntParserRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_delay_3_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTimerRule());
                    						}
                    						set(
                    							current,
                    							"delay",
                    							lv_delay_3_0,
                    							"fr.inria.aoste.timesquare.duration.xtext.Duration.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,19,FOLLOW_12); 

            			newLeafNode(otherlv_4, grammarAccess.getTimerAccess().getStartKeyword_2());
            		
            // InternalDuration.g:312:3: ( ( ruleEString ) )
            // InternalDuration.g:313:4: ( ruleEString )
            {
            // InternalDuration.g:313:4: ( ruleEString )
            // InternalDuration.g:314:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTimerRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTimerAccess().getStartConcreteEntityCrossReference_3_0());
            				
            pushFollow(FOLLOW_13);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDuration.g:328:3: (otherlv_6= 'reference' | otherlv_7= 'ref' )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            else if ( (LA5_0==21) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalDuration.g:329:4: otherlv_6= 'reference'
                    {
                    otherlv_6=(Token)match(input,20,FOLLOW_12); 

                    				newLeafNode(otherlv_6, grammarAccess.getTimerAccess().getReferenceKeyword_4_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalDuration.g:334:4: otherlv_7= 'ref'
                    {
                    otherlv_7=(Token)match(input,21,FOLLOW_12); 

                    				newLeafNode(otherlv_7, grammarAccess.getTimerAccess().getRefKeyword_4_1());
                    			

                    }
                    break;

            }

            // InternalDuration.g:339:3: ( ( ruleEString ) )
            // InternalDuration.g:340:4: ( ruleEString )
            {
            // InternalDuration.g:340:4: ( ruleEString )
            // InternalDuration.g:341:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTimerRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTimerAccess().getRefConcreteEntityCrossReference_5_0());
            				
            pushFollow(FOLLOW_7);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_9=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getTimerAccess().getSemicolonKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimer"


    // $ANTLR start "entryRuleComputationDuration"
    // InternalDuration.g:363:1: entryRuleComputationDuration returns [EObject current=null] : iv_ruleComputationDuration= ruleComputationDuration EOF ;
    public final EObject entryRuleComputationDuration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComputationDuration = null;


        try {
            // InternalDuration.g:363:60: (iv_ruleComputationDuration= ruleComputationDuration EOF )
            // InternalDuration.g:364:2: iv_ruleComputationDuration= ruleComputationDuration EOF
            {
             newCompositeNode(grammarAccess.getComputationDurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComputationDuration=ruleComputationDuration();

            state._fsp--;

             current =iv_ruleComputationDuration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComputationDuration"


    // $ANTLR start "ruleComputationDuration"
    // InternalDuration.g:370:1: ruleComputationDuration returns [EObject current=null] : ( ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'ComputationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';' ) ;
    public final EObject ruleComputationDuration() throws RecognitionException {
        EObject current = null;

        Token lv_displayedOverlap_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;


        	enterRule();

        try {
            // InternalDuration.g:376:2: ( ( ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'ComputationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';' ) )
            // InternalDuration.g:377:2: ( ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'ComputationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';' )
            {
            // InternalDuration.g:377:2: ( ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'ComputationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';' )
            // InternalDuration.g:378:3: ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'ComputationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';'
            {
            // InternalDuration.g:378:3: ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==22) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalDuration.g:379:4: (lv_displayedOverlap_0_0= 'displayedOverlap' )
                    {
                    // InternalDuration.g:379:4: (lv_displayedOverlap_0_0= 'displayedOverlap' )
                    // InternalDuration.g:380:5: lv_displayedOverlap_0_0= 'displayedOverlap'
                    {
                    lv_displayedOverlap_0_0=(Token)match(input,22,FOLLOW_14); 

                    					newLeafNode(lv_displayedOverlap_0_0, grammarAccess.getComputationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getComputationDurationRule());
                    					}
                    					setWithLastConsumed(current, "displayedOverlap", lv_displayedOverlap_0_0 != null, "displayedOverlap");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,23,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getComputationDurationAccess().getComputationDurationKeyword_1());
            		
            // InternalDuration.g:396:3: (otherlv_2= 'from' | otherlv_3= 'start' )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==24) ) {
                alt7=1;
            }
            else if ( (LA7_0==19) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalDuration.g:397:4: otherlv_2= 'from'
                    {
                    otherlv_2=(Token)match(input,24,FOLLOW_12); 

                    				newLeafNode(otherlv_2, grammarAccess.getComputationDurationAccess().getFromKeyword_2_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalDuration.g:402:4: otherlv_3= 'start'
                    {
                    otherlv_3=(Token)match(input,19,FOLLOW_12); 

                    				newLeafNode(otherlv_3, grammarAccess.getComputationDurationAccess().getStartKeyword_2_1());
                    			

                    }
                    break;

            }

            // InternalDuration.g:407:3: ( ( ruleEString ) )
            // InternalDuration.g:408:4: ( ruleEString )
            {
            // InternalDuration.g:408:4: ( ruleEString )
            // InternalDuration.g:409:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getComputationDurationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getComputationDurationAccess().getStartConcreteEntityCrossReference_3_0());
            				
            pushFollow(FOLLOW_16);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDuration.g:423:3: (otherlv_5= 'to' | otherlv_6= 'end' )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==25) ) {
                alt8=1;
            }
            else if ( (LA8_0==26) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalDuration.g:424:4: otherlv_5= 'to'
                    {
                    otherlv_5=(Token)match(input,25,FOLLOW_12); 

                    				newLeafNode(otherlv_5, grammarAccess.getComputationDurationAccess().getToKeyword_4_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalDuration.g:429:4: otherlv_6= 'end'
                    {
                    otherlv_6=(Token)match(input,26,FOLLOW_12); 

                    				newLeafNode(otherlv_6, grammarAccess.getComputationDurationAccess().getEndKeyword_4_1());
                    			

                    }
                    break;

            }

            // InternalDuration.g:434:3: ( ( ruleEString ) )
            // InternalDuration.g:435:4: ( ruleEString )
            {
            // InternalDuration.g:435:4: ( ruleEString )
            // InternalDuration.g:436:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getComputationDurationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getComputationDurationAccess().getEndConcreteEntityCrossReference_5_0());
            				
            pushFollow(FOLLOW_17);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDuration.g:450:3: ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( ((LA10_0>=20 && LA10_0<=21)) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDuration.g:451:4: (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) )
                    {
                    // InternalDuration.g:451:4: (otherlv_8= 'reference' | otherlv_9= 'ref' )
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( (LA9_0==20) ) {
                        alt9=1;
                    }
                    else if ( (LA9_0==21) ) {
                        alt9=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 9, 0, input);

                        throw nvae;
                    }
                    switch (alt9) {
                        case 1 :
                            // InternalDuration.g:452:5: otherlv_8= 'reference'
                            {
                            otherlv_8=(Token)match(input,20,FOLLOW_12); 

                            					newLeafNode(otherlv_8, grammarAccess.getComputationDurationAccess().getReferenceKeyword_6_0_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalDuration.g:457:5: otherlv_9= 'ref'
                            {
                            otherlv_9=(Token)match(input,21,FOLLOW_12); 

                            					newLeafNode(otherlv_9, grammarAccess.getComputationDurationAccess().getRefKeyword_6_0_1());
                            				

                            }
                            break;

                    }

                    // InternalDuration.g:462:4: ( ( ruleEString ) )
                    // InternalDuration.g:463:5: ( ruleEString )
                    {
                    // InternalDuration.g:463:5: ( ruleEString )
                    // InternalDuration.g:464:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComputationDurationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getComputationDurationAccess().getRefConcreteEntityCrossReference_6_1_0());
                    					
                    pushFollow(FOLLOW_7);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getComputationDurationAccess().getSemicolonKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComputationDuration"


    // $ANTLR start "entryRuleCommunicationDuration"
    // InternalDuration.g:487:1: entryRuleCommunicationDuration returns [EObject current=null] : iv_ruleCommunicationDuration= ruleCommunicationDuration EOF ;
    public final EObject entryRuleCommunicationDuration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommunicationDuration = null;


        try {
            // InternalDuration.g:487:62: (iv_ruleCommunicationDuration= ruleCommunicationDuration EOF )
            // InternalDuration.g:488:2: iv_ruleCommunicationDuration= ruleCommunicationDuration EOF
            {
             newCompositeNode(grammarAccess.getCommunicationDurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommunicationDuration=ruleCommunicationDuration();

            state._fsp--;

             current =iv_ruleCommunicationDuration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommunicationDuration"


    // $ANTLR start "ruleCommunicationDuration"
    // InternalDuration.g:494:1: ruleCommunicationDuration returns [EObject current=null] : ( ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'CommunicationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';' ) ;
    public final EObject ruleCommunicationDuration() throws RecognitionException {
        EObject current = null;

        Token lv_displayedOverlap_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;


        	enterRule();

        try {
            // InternalDuration.g:500:2: ( ( ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'CommunicationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';' ) )
            // InternalDuration.g:501:2: ( ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'CommunicationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';' )
            {
            // InternalDuration.g:501:2: ( ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'CommunicationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';' )
            // InternalDuration.g:502:3: ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )? otherlv_1= 'CommunicationDuration' (otherlv_2= 'from' | otherlv_3= 'start' ) ( ( ruleEString ) ) (otherlv_5= 'to' | otherlv_6= 'end' ) ( ( ruleEString ) ) ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )? otherlv_11= ';'
            {
            // InternalDuration.g:502:3: ( (lv_displayedOverlap_0_0= 'displayedOverlap' ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==22) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDuration.g:503:4: (lv_displayedOverlap_0_0= 'displayedOverlap' )
                    {
                    // InternalDuration.g:503:4: (lv_displayedOverlap_0_0= 'displayedOverlap' )
                    // InternalDuration.g:504:5: lv_displayedOverlap_0_0= 'displayedOverlap'
                    {
                    lv_displayedOverlap_0_0=(Token)match(input,22,FOLLOW_18); 

                    					newLeafNode(lv_displayedOverlap_0_0, grammarAccess.getCommunicationDurationAccess().getDisplayedOverlapDisplayedOverlapKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getCommunicationDurationRule());
                    					}
                    					setWithLastConsumed(current, "displayedOverlap", lv_displayedOverlap_0_0 != null, "displayedOverlap");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,27,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getCommunicationDurationAccess().getCommunicationDurationKeyword_1());
            		
            // InternalDuration.g:520:3: (otherlv_2= 'from' | otherlv_3= 'start' )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==24) ) {
                alt12=1;
            }
            else if ( (LA12_0==19) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalDuration.g:521:4: otherlv_2= 'from'
                    {
                    otherlv_2=(Token)match(input,24,FOLLOW_12); 

                    				newLeafNode(otherlv_2, grammarAccess.getCommunicationDurationAccess().getFromKeyword_2_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalDuration.g:526:4: otherlv_3= 'start'
                    {
                    otherlv_3=(Token)match(input,19,FOLLOW_12); 

                    				newLeafNode(otherlv_3, grammarAccess.getCommunicationDurationAccess().getStartKeyword_2_1());
                    			

                    }
                    break;

            }

            // InternalDuration.g:531:3: ( ( ruleEString ) )
            // InternalDuration.g:532:4: ( ruleEString )
            {
            // InternalDuration.g:532:4: ( ruleEString )
            // InternalDuration.g:533:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCommunicationDurationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getCommunicationDurationAccess().getStartConcreteEntityCrossReference_3_0());
            				
            pushFollow(FOLLOW_16);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDuration.g:547:3: (otherlv_5= 'to' | otherlv_6= 'end' )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==25) ) {
                alt13=1;
            }
            else if ( (LA13_0==26) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalDuration.g:548:4: otherlv_5= 'to'
                    {
                    otherlv_5=(Token)match(input,25,FOLLOW_12); 

                    				newLeafNode(otherlv_5, grammarAccess.getCommunicationDurationAccess().getToKeyword_4_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalDuration.g:553:4: otherlv_6= 'end'
                    {
                    otherlv_6=(Token)match(input,26,FOLLOW_12); 

                    				newLeafNode(otherlv_6, grammarAccess.getCommunicationDurationAccess().getEndKeyword_4_1());
                    			

                    }
                    break;

            }

            // InternalDuration.g:558:3: ( ( ruleEString ) )
            // InternalDuration.g:559:4: ( ruleEString )
            {
            // InternalDuration.g:559:4: ( ruleEString )
            // InternalDuration.g:560:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCommunicationDurationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getCommunicationDurationAccess().getEndConcreteEntityCrossReference_5_0());
            				
            pushFollow(FOLLOW_17);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDuration.g:574:3: ( (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=20 && LA15_0<=21)) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalDuration.g:575:4: (otherlv_8= 'reference' | otherlv_9= 'ref' ) ( ( ruleEString ) )
                    {
                    // InternalDuration.g:575:4: (otherlv_8= 'reference' | otherlv_9= 'ref' )
                    int alt14=2;
                    int LA14_0 = input.LA(1);

                    if ( (LA14_0==20) ) {
                        alt14=1;
                    }
                    else if ( (LA14_0==21) ) {
                        alt14=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 0, input);

                        throw nvae;
                    }
                    switch (alt14) {
                        case 1 :
                            // InternalDuration.g:576:5: otherlv_8= 'reference'
                            {
                            otherlv_8=(Token)match(input,20,FOLLOW_12); 

                            					newLeafNode(otherlv_8, grammarAccess.getCommunicationDurationAccess().getReferenceKeyword_6_0_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalDuration.g:581:5: otherlv_9= 'ref'
                            {
                            otherlv_9=(Token)match(input,21,FOLLOW_12); 

                            					newLeafNode(otherlv_9, grammarAccess.getCommunicationDurationAccess().getRefKeyword_6_0_1());
                            				

                            }
                            break;

                    }

                    // InternalDuration.g:586:4: ( ( ruleEString ) )
                    // InternalDuration.g:587:5: ( ruleEString )
                    {
                    // InternalDuration.g:587:5: ( ruleEString )
                    // InternalDuration.g:588:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCommunicationDurationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getCommunicationDurationAccess().getRefConcreteEntityCrossReference_6_1_0());
                    					
                    pushFollow(FOLLOW_7);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getCommunicationDurationAccess().getSemicolonKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommunicationDuration"


    // $ANTLR start "entryRuleEInt"
    // InternalDuration.g:611:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalDuration.g:611:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalDuration.g:612:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalDuration.g:618:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalDuration.g:624:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalDuration.g:625:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalDuration.g:625:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalDuration.g:626:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalDuration.g:626:3: (kw= '-' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==28) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalDuration.g:627:4: kw= '-'
                    {
                    kw=(Token)match(input,28,FOLLOW_19); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleEString"
    // InternalDuration.g:644:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalDuration.g:644:47: (iv_ruleEString= ruleEString EOF )
            // InternalDuration.g:645:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDuration.g:651:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalDuration.g:657:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalDuration.g:658:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalDuration.g:658:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_STRING) ) {
                alt17=1;
            }
            else if ( (LA17_0==RULE_ID) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalDuration.g:659:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDuration.g:667:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000008C12000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000010000020L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001080000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000006000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000308000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000020L});

}