/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.duration.model.duration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationFactory
 * @model kind="package"
 * @generated
 */
public interface DurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "duration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fr.inria.aoste.duration/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "duration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DurationPackage eINSTANCE = fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.DurationImpl <em>Duration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationImpl
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getDuration()
	 * @generated
	 */
	int DURATION = 0;

	/**
	 * The number of structural features of the '<em>Duration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.DurationModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationModelImpl
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getDurationModel()
	 * @generated
	 */
	int DURATION_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Durations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION_MODEL__DURATIONS = 0;

	/**
	 * The feature id for the '<em><b>Import Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION_MODEL__IMPORT_STATEMENT = 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION_MODEL_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.TimerImpl <em>Timer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.TimerImpl
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getTimer()
	 * @generated
	 */
	int TIMER = 2;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__DELAY = DURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__START = DURATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__REF = DURATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Timer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER_FEATURE_COUNT = DURATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.GenericDurationImpl <em>Generic Duration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.GenericDurationImpl
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getGenericDuration()
	 * @generated
	 */
	int GENERIC_DURATION = 5;

	/**
	 * The feature id for the '<em><b>Displayed Overlap</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_DURATION__DISPLAYED_OVERLAP = DURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_DURATION__START = DURATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_DURATION__END = DURATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_DURATION__REF = DURATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Generic Duration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_DURATION_FEATURE_COUNT = DURATION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.ComputationDurationImpl <em>Computation Duration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.ComputationDurationImpl
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getComputationDuration()
	 * @generated
	 */
	int COMPUTATION_DURATION = 3;

	/**
	 * The feature id for the '<em><b>Displayed Overlap</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_DURATION__DISPLAYED_OVERLAP = GENERIC_DURATION__DISPLAYED_OVERLAP;

	/**
	 * The feature id for the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_DURATION__START = GENERIC_DURATION__START;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_DURATION__END = GENERIC_DURATION__END;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_DURATION__REF = GENERIC_DURATION__REF;

	/**
	 * The number of structural features of the '<em>Computation Duration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTATION_DURATION_FEATURE_COUNT = GENERIC_DURATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.CommunicationDurationImpl <em>Communication Duration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.CommunicationDurationImpl
	 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getCommunicationDuration()
	 * @generated
	 */
	int COMMUNICATION_DURATION = 4;

	/**
	 * The feature id for the '<em><b>Displayed Overlap</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DURATION__DISPLAYED_OVERLAP = GENERIC_DURATION__DISPLAYED_OVERLAP;

	/**
	 * The feature id for the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DURATION__START = GENERIC_DURATION__START;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DURATION__END = GENERIC_DURATION__END;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DURATION__REF = GENERIC_DURATION__REF;

	/**
	 * The number of structural features of the '<em>Communication Duration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_DURATION_FEATURE_COUNT = GENERIC_DURATION_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.duration.model.duration.Duration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Duration</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.Duration
	 * @generated
	 */
	EClass getDuration();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.duration.model.duration.DurationModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationModel
	 * @generated
	 */
	EClass getDurationModel();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.duration.model.duration.DurationModel#getDurations <em>Durations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Durations</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationModel#getDurations()
	 * @see #getDurationModel()
	 * @generated
	 */
	EReference getDurationModel_Durations();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.duration.model.duration.DurationModel#getImportStatement <em>Import Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Statement</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationModel#getImportStatement()
	 * @see #getDurationModel()
	 * @generated
	 */
	EReference getDurationModel_ImportStatement();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.duration.model.duration.Timer <em>Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timer</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.Timer
	 * @generated
	 */
	EClass getTimer();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.Timer#getDelay()
	 * @see #getTimer()
	 * @generated
	 */
	EAttribute getTimer_Delay();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.Timer#getStart()
	 * @see #getTimer()
	 * @generated
	 */
	EReference getTimer_Start();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.Timer#getRef()
	 * @see #getTimer()
	 * @generated
	 */
	EReference getTimer_Ref();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.duration.model.duration.ComputationDuration <em>Computation Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computation Duration</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.ComputationDuration
	 * @generated
	 */
	EClass getComputationDuration();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.duration.model.duration.CommunicationDuration <em>Communication Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Duration</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.CommunicationDuration
	 * @generated
	 */
	EClass getCommunicationDuration();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration <em>Generic Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic Duration</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.GenericDuration
	 * @generated
	 */
	EClass getGenericDuration();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#isDisplayedOverlap <em>Displayed Overlap</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Displayed Overlap</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#isDisplayedOverlap()
	 * @see #getGenericDuration()
	 * @generated
	 */
	EAttribute getGenericDuration_DisplayedOverlap();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getStart()
	 * @see #getGenericDuration()
	 * @generated
	 */
	EReference getGenericDuration_Start();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>End</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getEnd()
	 * @see #getGenericDuration()
	 * @generated
	 */
	EReference getGenericDuration_End();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref</em>'.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getRef()
	 * @see #getGenericDuration()
	 * @generated
	 */
	EReference getGenericDuration_Ref();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DurationFactory getDurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.DurationImpl <em>Duration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationImpl
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getDuration()
		 * @generated
		 */
		EClass DURATION = eINSTANCE.getDuration();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.DurationModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationModelImpl
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getDurationModel()
		 * @generated
		 */
		EClass DURATION_MODEL = eINSTANCE.getDurationModel();

		/**
		 * The meta object literal for the '<em><b>Durations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DURATION_MODEL__DURATIONS = eINSTANCE.getDurationModel_Durations();

		/**
		 * The meta object literal for the '<em><b>Import Statement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DURATION_MODEL__IMPORT_STATEMENT = eINSTANCE.getDurationModel_ImportStatement();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.TimerImpl <em>Timer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.TimerImpl
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getTimer()
		 * @generated
		 */
		EClass TIMER = eINSTANCE.getTimer();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMER__DELAY = eINSTANCE.getTimer_Delay();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMER__START = eINSTANCE.getTimer_Start();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMER__REF = eINSTANCE.getTimer_Ref();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.ComputationDurationImpl <em>Computation Duration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.ComputationDurationImpl
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getComputationDuration()
		 * @generated
		 */
		EClass COMPUTATION_DURATION = eINSTANCE.getComputationDuration();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.CommunicationDurationImpl <em>Communication Duration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.CommunicationDurationImpl
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getCommunicationDuration()
		 * @generated
		 */
		EClass COMMUNICATION_DURATION = eINSTANCE.getCommunicationDuration();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.duration.model.duration.impl.GenericDurationImpl <em>Generic Duration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.GenericDurationImpl
		 * @see fr.inria.aoste.timesquare.duration.model.duration.impl.DurationPackageImpl#getGenericDuration()
		 * @generated
		 */
		EClass GENERIC_DURATION = eINSTANCE.getGenericDuration();

		/**
		 * The meta object literal for the '<em><b>Displayed Overlap</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERIC_DURATION__DISPLAYED_OVERLAP = eINSTANCE.getGenericDuration_DisplayedOverlap();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERIC_DURATION__START = eINSTANCE.getGenericDuration_Start();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERIC_DURATION__END = eINSTANCE.getGenericDuration_End();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERIC_DURATION__REF = eINSTANCE.getGenericDuration_Ref();

	}

} //DurationPackage
