/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.duration.model.duration.impl;

import fr.inria.aoste.timesquare.duration.model.duration.ComputationDuration;
import fr.inria.aoste.timesquare.duration.model.duration.DurationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Computation Duration</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ComputationDurationImpl extends GenericDurationImpl implements ComputationDuration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputationDurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DurationPackage.Literals.COMPUTATION_DURATION;
	}

} //ComputationDurationImpl
