/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.duration.model.duration;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Duration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#isDisplayedOverlap <em>Displayed Overlap</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getStart <em>Start</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getEnd <em>End</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getGenericDuration()
 * @model
 * @generated
 */
public interface GenericDuration extends Duration {
	/**
	 * Returns the value of the '<em><b>Displayed Overlap</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Displayed Overlap</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Displayed Overlap</em>' attribute.
	 * @see #setDisplayedOverlap(boolean)
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getGenericDuration_DisplayedOverlap()
	 * @model
	 * @generated
	 */
	boolean isDisplayedOverlap();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#isDisplayedOverlap <em>Displayed Overlap</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Displayed Overlap</em>' attribute.
	 * @see #isDisplayedOverlap()
	 * @generated
	 */
	void setDisplayedOverlap(boolean value);

	/**
	 * Returns the value of the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' reference.
	 * @see #setStart(ConcreteEntity)
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getGenericDuration_Start()
	 * @model required="true"
	 * @generated
	 */
	ConcreteEntity getStart();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getStart <em>Start</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' reference.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(ConcreteEntity value);

	/**
	 * Returns the value of the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' reference.
	 * @see #setEnd(ConcreteEntity)
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getGenericDuration_End()
	 * @model required="true"
	 * @generated
	 */
	ConcreteEntity getEnd();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getEnd <em>End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' reference.
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(ConcreteEntity value);

	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref</em>' reference.
	 * @see #setRef(ConcreteEntity)
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getGenericDuration_Ref()
	 * @model
	 * @generated
	 */
	ConcreteEntity getRef();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.duration.model.duration.GenericDuration#getRef <em>Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref</em>' reference.
	 * @see #getRef()
	 * @generated
	 */
	void setRef(ConcreteEntity value);

} // GenericDuration
