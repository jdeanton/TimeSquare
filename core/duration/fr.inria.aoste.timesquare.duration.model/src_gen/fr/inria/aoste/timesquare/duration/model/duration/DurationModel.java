/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.duration.model.duration;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.DurationModel#getDurations <em>Durations</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.DurationModel#getImportStatement <em>Import Statement</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getDurationModel()
 * @model
 * @generated
 */
public interface DurationModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Durations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.duration.model.duration.Duration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Durations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Durations</em>' containment reference list.
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getDurationModel_Durations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Duration> getDurations();

	/**
	 * Returns the value of the '<em><b>Import Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Statement</em>' containment reference.
	 * @see #setImportStatement(ImportStatement)
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getDurationModel_ImportStatement()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ImportStatement getImportStatement();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.duration.model.duration.DurationModel#getImportStatement <em>Import Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Statement</em>' containment reference.
	 * @see #getImportStatement()
	 * @generated
	 */
	void setImportStatement(ImportStatement value);

} // DurationModel
