/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.duration.model.duration.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;
import fr.inria.aoste.timesquare.duration.model.duration.Duration;
import fr.inria.aoste.timesquare.duration.model.duration.DurationModel;
import fr.inria.aoste.timesquare.duration.model.duration.DurationPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.impl.DurationModelImpl#getDurations <em>Durations</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.impl.DurationModelImpl#getImportStatement <em>Import Statement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DurationModelImpl extends EObjectImpl implements DurationModel {
	/**
	 * The cached value of the '{@link #getDurations() <em>Durations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurations()
	 * @generated
	 * @ordered
	 */
	protected EList<Duration> durations;
	/**
	 * The cached value of the '{@link #getImportStatement() <em>Import Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportStatement()
	 * @generated
	 * @ordered
	 */
	protected ImportStatement importStatement;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DurationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DurationPackage.Literals.DURATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Duration> getDurations() {
		if (durations == null) {
			durations = new EObjectContainmentEList<Duration>(Duration.class, this, DurationPackage.DURATION_MODEL__DURATIONS);
		}
		return durations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ImportStatement getImportStatement() {
		return importStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImportStatement(ImportStatement newImportStatement, NotificationChain msgs) {
		ImportStatement oldImportStatement = importStatement;
		importStatement = newImportStatement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DurationPackage.DURATION_MODEL__IMPORT_STATEMENT, oldImportStatement, newImportStatement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setImportStatement(ImportStatement newImportStatement) {
		if (newImportStatement != importStatement) {
			NotificationChain msgs = null;
			if (importStatement != null)
				msgs = ((InternalEObject)importStatement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DurationPackage.DURATION_MODEL__IMPORT_STATEMENT, null, msgs);
			if (newImportStatement != null)
				msgs = ((InternalEObject)newImportStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DurationPackage.DURATION_MODEL__IMPORT_STATEMENT, null, msgs);
			msgs = basicSetImportStatement(newImportStatement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DurationPackage.DURATION_MODEL__IMPORT_STATEMENT, newImportStatement, newImportStatement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DurationPackage.DURATION_MODEL__DURATIONS:
				return ((InternalEList<?>)getDurations()).basicRemove(otherEnd, msgs);
			case DurationPackage.DURATION_MODEL__IMPORT_STATEMENT:
				return basicSetImportStatement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DurationPackage.DURATION_MODEL__DURATIONS:
				return getDurations();
			case DurationPackage.DURATION_MODEL__IMPORT_STATEMENT:
				return getImportStatement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DurationPackage.DURATION_MODEL__DURATIONS:
				getDurations().clear();
				getDurations().addAll((Collection<? extends Duration>)newValue);
				return;
			case DurationPackage.DURATION_MODEL__IMPORT_STATEMENT:
				setImportStatement((ImportStatement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DurationPackage.DURATION_MODEL__DURATIONS:
				getDurations().clear();
				return;
			case DurationPackage.DURATION_MODEL__IMPORT_STATEMENT:
				setImportStatement((ImportStatement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DurationPackage.DURATION_MODEL__DURATIONS:
				return durations != null && !durations.isEmpty();
			case DurationPackage.DURATION_MODEL__IMPORT_STATEMENT:
				return importStatement != null;
		}
		return super.eIsSet(featureID);
	}

} //DurationModelImpl
