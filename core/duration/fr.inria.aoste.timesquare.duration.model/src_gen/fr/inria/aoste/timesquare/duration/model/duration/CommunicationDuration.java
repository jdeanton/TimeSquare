/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.duration.model.duration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Duration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getCommunicationDuration()
 * @model
 * @generated
 */
public interface CommunicationDuration extends GenericDuration {
} // CommunicationDuration
