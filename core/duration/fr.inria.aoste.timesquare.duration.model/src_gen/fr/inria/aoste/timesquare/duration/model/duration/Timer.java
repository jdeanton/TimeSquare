/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.duration.model.duration;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getDelay <em>Delay</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getStart <em>Start</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getTimer()
 * @model
 * @generated
 */
public interface Timer extends Duration {
	/**
	 * Returns the value of the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay</em>' attribute.
	 * @see #setDelay(int)
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getTimer_Delay()
	 * @model
	 * @generated
	 */
	int getDelay();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getDelay <em>Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay</em>' attribute.
	 * @see #getDelay()
	 * @generated
	 */
	void setDelay(int value);

	/**
	 * Returns the value of the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' reference.
	 * @see #setStart(ConcreteEntity)
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getTimer_Start()
	 * @model required="true"
	 * @generated
	 */
	ConcreteEntity getStart();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getStart <em>Start</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' reference.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(ConcreteEntity value);

	/**
	 * Returns the value of the '<em><b>Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref</em>' reference.
	 * @see #setRef(ConcreteEntity)
	 * @see fr.inria.aoste.timesquare.duration.model.duration.DurationPackage#getTimer_Ref()
	 * @model required="true"
	 * @generated
	 */
	ConcreteEntity getRef();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.duration.model.duration.Timer#getRef <em>Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref</em>' reference.
	 * @see #getRef()
	 * @generated
	 */
	void setRef(ConcreteEntity value);

} // Timer
