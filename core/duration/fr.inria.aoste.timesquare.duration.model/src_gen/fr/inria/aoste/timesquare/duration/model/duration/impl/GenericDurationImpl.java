/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.duration.model.duration.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;

import fr.inria.aoste.timesquare.duration.model.duration.DurationPackage;
import fr.inria.aoste.timesquare.duration.model.duration.GenericDuration;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Duration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.impl.GenericDurationImpl#isDisplayedOverlap <em>Displayed Overlap</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.impl.GenericDurationImpl#getStart <em>Start</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.impl.GenericDurationImpl#getEnd <em>End</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.duration.model.duration.impl.GenericDurationImpl#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GenericDurationImpl extends DurationImpl implements GenericDuration {
	/**
	 * The default value of the '{@link #isDisplayedOverlap() <em>Displayed Overlap</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisplayedOverlap()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISPLAYED_OVERLAP_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDisplayedOverlap() <em>Displayed Overlap</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisplayedOverlap()
	 * @generated
	 * @ordered
	 */
	protected boolean displayedOverlap = DISPLAYED_OVERLAP_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStart() <em>Start</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected ConcreteEntity start;

	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected ConcreteEntity end;

	/**
	 * The cached value of the '{@link #getRef() <em>Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRef()
	 * @generated
	 * @ordered
	 */
	protected ConcreteEntity ref;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GenericDurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DurationPackage.Literals.GENERIC_DURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isDisplayedOverlap() {
		return displayedOverlap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDisplayedOverlap(boolean newDisplayedOverlap) {
		boolean oldDisplayedOverlap = displayedOverlap;
		displayedOverlap = newDisplayedOverlap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DurationPackage.GENERIC_DURATION__DISPLAYED_OVERLAP, oldDisplayedOverlap, displayedOverlap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConcreteEntity getStart() {
		if (start != null && start.eIsProxy()) {
			InternalEObject oldStart = (InternalEObject)start;
			start = (ConcreteEntity)eResolveProxy(oldStart);
			if (start != oldStart) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DurationPackage.GENERIC_DURATION__START, oldStart, start));
			}
		}
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteEntity basicGetStart() {
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStart(ConcreteEntity newStart) {
		ConcreteEntity oldStart = start;
		start = newStart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DurationPackage.GENERIC_DURATION__START, oldStart, start));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConcreteEntity getEnd() {
		if (end != null && end.eIsProxy()) {
			InternalEObject oldEnd = (InternalEObject)end;
			end = (ConcreteEntity)eResolveProxy(oldEnd);
			if (end != oldEnd) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DurationPackage.GENERIC_DURATION__END, oldEnd, end));
			}
		}
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteEntity basicGetEnd() {
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEnd(ConcreteEntity newEnd) {
		ConcreteEntity oldEnd = end;
		end = newEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DurationPackage.GENERIC_DURATION__END, oldEnd, end));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConcreteEntity getRef() {
		if (ref != null && ref.eIsProxy()) {
			InternalEObject oldRef = (InternalEObject)ref;
			ref = (ConcreteEntity)eResolveProxy(oldRef);
			if (ref != oldRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DurationPackage.GENERIC_DURATION__REF, oldRef, ref));
			}
		}
		return ref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteEntity basicGetRef() {
		return ref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRef(ConcreteEntity newRef) {
		ConcreteEntity oldRef = ref;
		ref = newRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DurationPackage.GENERIC_DURATION__REF, oldRef, ref));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DurationPackage.GENERIC_DURATION__DISPLAYED_OVERLAP:
				return isDisplayedOverlap();
			case DurationPackage.GENERIC_DURATION__START:
				if (resolve) return getStart();
				return basicGetStart();
			case DurationPackage.GENERIC_DURATION__END:
				if (resolve) return getEnd();
				return basicGetEnd();
			case DurationPackage.GENERIC_DURATION__REF:
				if (resolve) return getRef();
				return basicGetRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DurationPackage.GENERIC_DURATION__DISPLAYED_OVERLAP:
				setDisplayedOverlap((Boolean)newValue);
				return;
			case DurationPackage.GENERIC_DURATION__START:
				setStart((ConcreteEntity)newValue);
				return;
			case DurationPackage.GENERIC_DURATION__END:
				setEnd((ConcreteEntity)newValue);
				return;
			case DurationPackage.GENERIC_DURATION__REF:
				setRef((ConcreteEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DurationPackage.GENERIC_DURATION__DISPLAYED_OVERLAP:
				setDisplayedOverlap(DISPLAYED_OVERLAP_EDEFAULT);
				return;
			case DurationPackage.GENERIC_DURATION__START:
				setStart((ConcreteEntity)null);
				return;
			case DurationPackage.GENERIC_DURATION__END:
				setEnd((ConcreteEntity)null);
				return;
			case DurationPackage.GENERIC_DURATION__REF:
				setRef((ConcreteEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DurationPackage.GENERIC_DURATION__DISPLAYED_OVERLAP:
				return displayedOverlap != DISPLAYED_OVERLAP_EDEFAULT;
			case DurationPackage.GENERIC_DURATION__START:
				return start != null;
			case DurationPackage.GENERIC_DURATION__END:
				return end != null;
			case DurationPackage.GENERIC_DURATION__REF:
				return ref != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (displayedOverlap: ");
		result.append(displayedOverlap);
		result.append(')');
		return result.toString();
	}

} //GenericDurationImpl
