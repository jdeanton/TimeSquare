/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.simulationpolicy.maxcardpolicy;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;
/**
 * This policy choose randomly among the biggest solutions
 * @author Julien DeAntoni
 */
public class MaxCardSimulationPolicy extends SimulationPolicyBase {

	public MaxCardSimulationPolicy() {
		super();
	}

	@Override
	public int getPolicySpecificSolution(ArrayList<ClockTraceStateSet> allSolutionsF) {
		//pick a solution in the set according to the policy. here the min
		int maxNbTick= 0;
		ArrayList<Integer> indexOfMax = new ArrayList<Integer>();
		int currentNbTick = 0;
		for (int i=0; i<allSolutionsF.size(); i++) {
			ClockTraceStateSet currentSet = allSolutionsF.get(i);
			currentNbTick=currentSet.cardinal(ClockTraceState.T);
			if (currentNbTick > maxNbTick){
				maxNbTick = currentNbTick;
				indexOfMax.clear();
				indexOfMax.add(i);
			} else {
				if (currentNbTick == maxNbTick){
					indexOfMax.add(i);
				}
			}
			
		}
		
		// pick randomly in the max solutions
		int randomInMax = (int)((Math.random() * 1000)%indexOfMax.size());
		return indexOfMax.get(randomInMax);
	}
	
}
