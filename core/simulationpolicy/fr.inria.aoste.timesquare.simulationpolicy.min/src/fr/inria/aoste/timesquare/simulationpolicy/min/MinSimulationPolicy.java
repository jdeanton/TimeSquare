/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.simulationpolicy.min;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceState;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;
/**
 * This policy choose randomly among the smallest solutions
 * @author Julien DeAntoni
 */
public class MinSimulationPolicy extends SimulationPolicyBase {

	public MinSimulationPolicy() {
	}

	@Override
	public int getPolicySpecificSolution(ArrayList<ClockTraceStateSet> allSolutionsF) {
	
		//pick a solution in the set according to the policy. here the min
		int minNbTick= Integer.MAX_VALUE;
		ArrayList<Integer> indexOfMin = new ArrayList<Integer>();
		int currentNbTick = 0;
		for (int i = 0; i < allSolutionsF.size(); i++) {
			ClockTraceStateSet currentSet = allSolutionsF.get(i);
			currentNbTick=currentSet.cardinal(ClockTraceState.T);
			if (currentNbTick < minNbTick){
				minNbTick = currentNbTick;
				indexOfMin.clear();
				indexOfMin.add(i);
			} else {
				if (currentNbTick == minNbTick){
					indexOfMin.add(i);
				}
			}
			
		}
		// pick randomly in the min solutions
		int randomInMin = (int)((Math.random() * 1000)%indexOfMin.size());
		return indexOfMin.get(randomInMin);
	}
}
