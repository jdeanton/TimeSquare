/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.simulationpolicy;

import java.io.PrintStream;

public class OptionPolitic {

	boolean estate=true;
	int politicid =0;
	PrintStream sysout=null;
	
	

	public final PrintStream getSysout() {
		return sysout;
	}

	public final void setSysout(PrintStream sysout) {
		this.sysout = sysout;
	}

	public OptionPolitic() {
		super();		
	}

	public OptionPolitic(boolean estate, int politicid) {
		super();
		this.estate = estate;
		this.politicid = politicid;
	}

	public final int getPoliticid() {
		return politicid;
	}

	public final void setPoliticid(int politicid) {
		this.politicid = politicid;
	}

	public final boolean isEstate() {
		return estate;
	}

	public final void setEstate(boolean estate) {
		this.estate = estate;
	}
	
}
