/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.simulationpolicy;

import java.util.ArrayList;

import org.eclipse.core.runtime.IConfigurationElement;

import fr.inria.aoste.timesquare.simulationpolicy.random.RandomSimulationPolicy;
import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;

public class SimulationPolicyManager implements IExtensionManager {

	static private SimulationPolicyManager instance;

	/**
	 * @return Singleton
	 */
	static synchronized public SimulationPolicyManager getDefault() {
		if (instance == null)
			instance = new SimulationPolicyManager();
		return instance;
	}

	private SimulationPolicyManager() {
		ExtensionPointManager.findAllExtensions(this); 
		//disp();
	}

	@Override
	final public String getExtensionPointName() {
		return "fr.inria.aoste.timesquare.simulationpolicy.simulationPolicies";
	}
	@Override
	final public String getPluginName() {
		return "fr.inria.aoste.timesquare.simulationpolicy";
	}

	private ArrayList<String> nameList;
	private ArrayList<String> commentList;
	private ArrayList<String> policyUidList;

	public ArrayList<String> getPolicyNames() {
		return nameList;
	}

	public String[] getPolicyNamesAsArray() {
		return nameList.toArray(new String[nameList.size()] );
	}

	private ArrayList<Class<? extends  SimulationPolicyBase>> policiesClassList;

	public int getSize() {
		return policiesClassList.size();
	}

	public String getDefaultUID()
	{
		try {
			int i = policiesClassList.indexOf(RandomSimulationPolicy.class);
			return policyUidList.get(i);
		}
		catch (Throwable e) {
			return policyUidList.get(0);
		}
	}

	public String getUID(int i) {
		try {
			return policyUidList.get(i);
		} catch (Throwable t) {
			return null;
		}
	}

	public int getIndexofUID(String uid)
	{
		try {
			return policyUidList.indexOf(uid);
		} catch (Throwable t) {
			return -1;
		}
	}

	public String getName(int i) {
		try {
			return nameList.get(i);
		} catch (Throwable t) {
			return null;
		}
	}

	public String getComment(int i) {
		try {
			return commentList.get(i);
		} catch (Throwable t) {
			return "";
		}
	}

	public SimulationPolicyBase getNewInstance(int i) {
		try {
			if (i==-1)
				//return new SimFireBalancedRandom();
				return null;
			SimulationPolicyBase o = policiesClassList.get(i).newInstance();		
			return  o;
		} catch (Throwable e) {

			e.printStackTrace();

			//return new SimFireBalancedRandom();
			return null;
		}
	}

	public Class<? extends SimulationPolicyBase> getClass(int i) {
		try {
			return policiesClassList.get(i);
		} catch (Throwable e) {

			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private int add(Class<?> c, String name, String comment, String uid) {
		if (c == null)
			return 0;
		policiesClassList.add((Class<? extends SimulationPolicyBase>) c);
		if (name == null)
			name = "CLASS :" + c.getName();
		nameList.add(name);
		if (comment == null)
			comment = "..";
		commentList.add(comment);
		policyUidList.add(uid);
		return 0;

	}

	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable {
		String name = ice.getAttribute("name");
		if ( name == null )
			throw new RuntimeException("No name for policy "+ice);

		String comment = ice.getAttribute("comment");
		if (comment == null) {
			comment = "";
		}

		Class<? extends  SimulationPolicyBase> c = ExtensionPointManager.getPointExtensionClass(ice, "class", SimulationPolicyBase.class);	
		if (policiesClassList==null) policiesClassList = new ArrayList<Class<? extends SimulationPolicyBase>>();
		policiesClassList.add(c);

		if(nameList==null) nameList = new ArrayList<String>();
		nameList.add(name);

		if (commentList==null) commentList = new ArrayList<String>();
		commentList.add(comment);

		String uid= ExtensionPointManager.getPluginName(ice) +"@"+ice.getAttribute("class");
		if (policyUidList==null) policyUidList = new ArrayList<String>();
		policyUidList.add(uid);
	}

	//	public void disp() {
	//		System.out.println("======================");
	//		int n = nameList.size();
	//		for (int i = 0; i < n; i++) {
	//			System.out.println("\t" + i + " :" + nameList.get(i)+" ID: "+ policyUidList.get(i));
	//		}
	//		System.out.println("======================");
	//	}


	/*

	@Override
	protected void createCollum(Table table5) {		
		super.createCollum(table5);
		createTableColumn(table5, SWT.LEFT, 0, "   Politic Name   ", 350, false, true);
		createTableColumn(table5, SWT.LEFT, 1, "   Comment      ", 250, false, true);
		createTableColumn(table5, SWT.LEFT, 2, "   Class      ", 250, false, true);

	}

	@Override
	protected void createItem(Table table5) {

		super.createItem(table5);

		int n = PolicyDictionary.getDefault().getSize();
		for (int i = 0; i < n; i++) {
			TableItem item = new TableItem(table5, SWT.NONE);
			item.setText(0, PolicyDictionary.getDefault().getName(i));
			item.setText(1, PolicyDictionary.getDefault().getComment(i));
			item.setText(2, PolicyDictionary.getDefault().getClass(i).getName());
		}

		for (int i = 0; i < table5.getColumnCount(); i++) {
			table5.getColumn(i).pack();
			table5.computeSize(-1, -1);
		}
		table5.pack();
	}

	 */

	// used BDDutil.reEvaluatePolitic
	/*static public DataBdd2 reEvaluatePolitic(DataBdd2 theBddStruct, int n) {


		TriStateSet[] tss = new TriStateSet[2];
		tss[0] = new TriStateSet(theBddStruct.getDico().size());
		tss[1] = new TriStateSet(theBddStruct.getDico().size());
		Imode im = getDefault().getNewInstance(n);
		im.init(new LocalSim(theBddStruct.getDico(), BDDutil.bddf));
		im.startSimul();
		im.fireDone(tss[0], tss[1], theBddStruct.getBdd());
		im.endSimul();
		DataBdd2 dbdd = new DataBdd2(theBddStruct.getDico(), tss[0], tss[1], theBddStruct.getBdd());
		return dbdd;
	}*/
}
