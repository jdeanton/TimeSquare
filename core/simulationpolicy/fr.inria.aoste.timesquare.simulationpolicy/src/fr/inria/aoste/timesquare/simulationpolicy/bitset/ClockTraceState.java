/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.simulationpolicy.bitset;

/***
 * 
 * @author DeAntoni 
 * 		   EState : T : 1 -> must be fired; F : 0 -> must not be fired; X : 2 -> undeternimed ; x : 4 -> free ;
 *         FState : T : 1 --> fired F : 0 --> not fired
 * 
 * 
 */
public enum ClockTraceState {

	/**E-> must fired , F-> fired*/
	T(1, "1"), 
	/**E-> must not fired , F-> not fired*/
	F(0, "0"), 
	/**E-> undetermined*/
	X(2, "X"), 
	/**E-> free*/
	x(4, "x");
	String s;
	int n;

	ClockTraceState(int n, String s) {
		this.s = s;
		this.n = n;
	}

	@Override
	public String toString() {
		return s;
	}

}
