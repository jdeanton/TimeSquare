/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.simulationpolicy;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;

public abstract class SimulationPolicyBase {

	/**
	 * This method is the one call once the BDD has been constructed and restricted by the priorities.
	 * It must return an ArrayList of size two where the first element is a ClockTreeStateSet that represents the EState
	 * (i.e. the enable state used to construct ghost and co). The second element is more important size it must contains the state of
	 * the clock for the current step (the Fstate). consequently it is one of the solution of the bdd semnticBdd.
	 * The ClockStateSet must put the state of the clock represented by a bddVarNumber in the semanticBDD at a specific index. This index is the one returned by the map bddVarToIndexInSolution.get(bddVarNumber)
	 * Note that if you don't care about the Estates, please put the FState two times in the resulting ArrayList.
	 * @param BuDDyFactory
	 * @param semanticBdd
	 * @param bddVarToIndexInSolution
	 * @return
	 */
//	public abstract ArrayList<ClockTraceStateSet> getPolicySpecificSolution(IBuddyFactory BuDDyFactory, BDD semanticBdd, Map<Integer, Integer> bddVarToIndexInSolution );
	public abstract int getPolicySpecificSolution(ArrayList<ClockTraceStateSet> allSolutionsF);
}