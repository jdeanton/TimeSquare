/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.simulationpolicy.bitset;


public class ClockTraceStateSet{
	private int size;

	/**
	 * return the number of clock in the set whose state is cs (a Clockstate)
	 * @param cs
	 * @return
	 */
	public int cardinal(ClockTraceState cs) {
		int n = 0;
		for (int i = 0; i < size; i++) {
			if (b[i] == cs)
				n++;
		}
		return n;

	}

	private ClockTraceState b[];

	public ClockTraceStateSet(int size) {
		super();
		this.size = size;
		b = new ClockTraceState[size];
		clearX();
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.simulationpolicy.bitset.IClockTraceStateSet#clearX()
	 */
	public ClockTraceStateSet clearX() {
		for (int i = 0; i < size; i++) {
			b[i] = ClockTraceState.X;
		}
		return this;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.simulationpolicy.bitset.IClockTraceStateSet#clear0()
	 */
	public ClockTraceStateSet clear0() {
		for (int i = 0; i < size; i++) {
			b[i] = ClockTraceState.F;
		}
		return this;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.simulationpolicy.bitset.IClockTraceStateSet#set(int, fr.inria.aoste.timesquare.simulationpolicy.bitset.TriState)
	 */
	public ClockTraceStateSet set(int i, ClockTraceState bin) {

		if (i >= size)
			return null;
		if (i < 0)
			return null; 
		b[i] = bin;
		return this;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.simulationpolicy.bitset.IClockTraceStateSet#get(int)
	 */
	public ClockTraceState get(int i) {

		if (i >= size)
			return null;
		if (i < 0)
			return null;
		return b[i];
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.simulationpolicy.bitset.IClockTraceStateSet#set2bitset(fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet)
	 */
	public void set2bitset(ClockTraceStateSet bs) {
		for (int i = 0; i < size; i++)
			b[i] = bs.b[i];
	}


	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.simulationpolicy.bitset.IClockTraceStateSet#toString()
	 */
	@Override
	public String toString() {

		StringBuffer s = new StringBuffer("");
		for (ClockTraceState bl : b)
			s.append(bl.toString());
		return s.toString(); 

	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.simulationpolicy.bitset.IClockTraceStateSet#getSize()
	 */
	public int getSize() {
		return size;
	}


}
