/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.simulationpolicy.random;

import java.util.ArrayList;
import java.util.Random;

import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;
/**
 * This policy choose randomly all the solutions
 * @author Julien DeAntoni
 */
public class RandomSimulationPolicy extends SimulationPolicyBase {

	public RandomSimulationPolicy() {
		super();
	}

	private Random _random= new Random();
	
	@Override
	public int getPolicySpecificSolution(ArrayList<ClockTraceStateSet> allSolutionsF) {
		//pick a solution in the set according to the policy. For now only random
		int random = _random.nextInt(allSolutionsF.size());
		return random;
	}
	
	
			

}
