/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.simulationpolicy.onesat;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.simulationpolicy.SimulationPolicyBase;
import fr.inria.aoste.timesquare.simulationpolicy.bitset.ClockTraceStateSet;
/**
 * This policy choose randomly among all the solutions by using oneSat() and without computing all solutions
 * @author Julien DeAntoni
 */
public class OneSatSimulationPolicy extends SimulationPolicyBase {

	public OneSatSimulationPolicy() {
		super();
	}

	@Override
	public int getPolicySpecificSolution(ArrayList<ClockTraceStateSet> allSolutionsF){
		//return the first solution, arbitrarily
		return 0;
	}
	
	
			

}
