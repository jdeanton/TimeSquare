/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;

import org.eclipse.core.runtime.Plugin;

import org.osgi.framework.BundleContext;

public class Activator extends Plugin {

	
	public final String PLUGIN_ID= "fr.inria.aoste.trace" ;
	public Activator() {
	
	}
	static Activator plugin =null;
	@Override
	public void start(BundleContext context) throws Exception {
		plugin=this;
		super.start(context);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		
		super.stop(context);
		plugin=null;
	}

	public static final Activator getDefault() {
		return plugin;
	}

	
}
