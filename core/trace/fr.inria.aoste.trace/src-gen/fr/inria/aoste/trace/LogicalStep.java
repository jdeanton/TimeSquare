/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logical Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.LogicalStep#getConstraintStates <em>Constraint States</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.LogicalStep#getEventOccurrences <em>Event Occurrences</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.LogicalStep#getNextStep <em>Next Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.LogicalStep#getPreviousStep <em>Previous Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.LogicalStep#getStepNumber <em>Step Number</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.LogicalStep#getAssertionStates <em>Assertion States</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.trace.TracePackage#getLogicalStep()
 * @model
 * @generated
 */
public interface LogicalStep extends EObject {
	/**
	 * Returns the value of the '<em><b>Constraint States</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.trace.ConstraintState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint States</em>' containment reference list.
	 * @see fr.inria.aoste.trace.TracePackage#getLogicalStep_ConstraintStates()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConstraintState> getConstraintStates();

	/**
	 * Returns the value of the '<em><b>Event Occurrences</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.trace.EventOccurrence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Occurrences</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Occurrences</em>' containment reference list.
	 * @see fr.inria.aoste.trace.TracePackage#getLogicalStep_EventOccurrences()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<EventOccurrence> getEventOccurrences();

	/**
	 * Returns the value of the '<em><b>Next Step</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.inria.aoste.trace.LogicalStep#getPreviousStep <em>Previous Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next Step</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Step</em>' reference.
	 * @see #setNextStep(LogicalStep)
	 * @see fr.inria.aoste.trace.TracePackage#getLogicalStep_NextStep()
	 * @see fr.inria.aoste.trace.LogicalStep#getPreviousStep
	 * @model opposite="previousStep"
	 * @generated
	 */
	LogicalStep getNextStep();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.LogicalStep#getNextStep <em>Next Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next Step</em>' reference.
	 * @see #getNextStep()
	 * @generated
	 */
	void setNextStep(LogicalStep value);

	/**
	 * Returns the value of the '<em><b>Previous Step</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.inria.aoste.trace.LogicalStep#getNextStep <em>Next Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous Step</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous Step</em>' reference.
	 * @see #setPreviousStep(LogicalStep)
	 * @see fr.inria.aoste.trace.TracePackage#getLogicalStep_PreviousStep()
	 * @see fr.inria.aoste.trace.LogicalStep#getNextStep
	 * @model opposite="nextStep"
	 * @generated
	 */
	LogicalStep getPreviousStep();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.LogicalStep#getPreviousStep <em>Previous Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Previous Step</em>' reference.
	 * @see #getPreviousStep()
	 * @generated
	 */
	void setPreviousStep(LogicalStep value);

	/**
	 * Returns the value of the '<em><b>Step Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Number</em>' attribute.
	 * @see #setStepNumber(int)
	 * @see fr.inria.aoste.trace.TracePackage#getLogicalStep_StepNumber()
	 * @model required="true"
	 * @generated
	 */
	int getStepNumber();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.LogicalStep#getStepNumber <em>Step Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step Number</em>' attribute.
	 * @see #getStepNumber()
	 * @generated
	 */
	void setStepNumber(int value);

	/**
	 * Returns the value of the '<em><b>Assertion States</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.trace.AssertionState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion States</em>' containment reference list.
	 * @see fr.inria.aoste.trace.TracePackage#getLogicalStep_AssertionStates()
	 * @model containment="true"
	 * @generated
	 */
	EList<AssertionState> getAssertionStates();
	
	/**
	 * @generated NOT
	 * @return
	 */
	@Override
	public String toString();

} // LogicalStep
