/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace.impl;

import fr.inria.aoste.trace.PhysicalBase;
import fr.inria.aoste.trace.PhysicalSteps;
import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.TracePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Base</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.impl.PhysicalBaseImpl#getPhysicalSteps <em>Physical Steps</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.PhysicalBaseImpl#getRelatedDenseClock <em>Related Dense Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.PhysicalBaseImpl#getOffset <em>Offset</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PhysicalBaseImpl extends EObjectImpl implements PhysicalBase {
	/**
	 * The cached value of the '{@link #getPhysicalSteps() <em>Physical Steps</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalSteps()
	 * @generated
	 * @ordered
	 */
	protected EList<PhysicalSteps> physicalSteps;

	/**
	 * The cached value of the '{@link #getRelatedDenseClock() <em>Related Dense Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedDenseClock()
	 * @generated
	 * @ordered
	 */
	protected Reference relatedDenseClock;

	/**
	 * The default value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected static final double OFFSET_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected double offset = OFFSET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhysicalBaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TracePackage.Literals.PHYSICAL_BASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PhysicalSteps> getPhysicalSteps() {
		if (physicalSteps == null) {
			physicalSteps = new EObjectContainmentEList<PhysicalSteps>(PhysicalSteps.class, this, TracePackage.PHYSICAL_BASE__PHYSICAL_STEPS);
		}
		return physicalSteps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference getRelatedDenseClock() {
		return relatedDenseClock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelatedDenseClock(Reference newRelatedDenseClock, NotificationChain msgs) {
		Reference oldRelatedDenseClock = relatedDenseClock;
		relatedDenseClock = newRelatedDenseClock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK, oldRelatedDenseClock, newRelatedDenseClock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelatedDenseClock(Reference newRelatedDenseClock) {
		if (newRelatedDenseClock != relatedDenseClock) {
			NotificationChain msgs = null;
			if (relatedDenseClock != null)
				msgs = ((InternalEObject)relatedDenseClock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK, null, msgs);
			if (newRelatedDenseClock != null)
				msgs = ((InternalEObject)newRelatedDenseClock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK, null, msgs);
			msgs = basicSetRelatedDenseClock(newRelatedDenseClock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK, newRelatedDenseClock, newRelatedDenseClock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getOffset() {
		return offset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffset(double newOffset) {
		double oldOffset = offset;
		offset = newOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.PHYSICAL_BASE__OFFSET, oldOffset, offset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TracePackage.PHYSICAL_BASE__PHYSICAL_STEPS:
				return ((InternalEList<?>)getPhysicalSteps()).basicRemove(otherEnd, msgs);
			case TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK:
				return basicSetRelatedDenseClock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TracePackage.PHYSICAL_BASE__PHYSICAL_STEPS:
				return getPhysicalSteps();
			case TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK:
				return getRelatedDenseClock();
			case TracePackage.PHYSICAL_BASE__OFFSET:
				return getOffset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TracePackage.PHYSICAL_BASE__PHYSICAL_STEPS:
				getPhysicalSteps().clear();
				getPhysicalSteps().addAll((Collection<? extends PhysicalSteps>)newValue);
				return;
			case TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK:
				setRelatedDenseClock((Reference)newValue);
				return;
			case TracePackage.PHYSICAL_BASE__OFFSET:
				setOffset((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TracePackage.PHYSICAL_BASE__PHYSICAL_STEPS:
				getPhysicalSteps().clear();
				return;
			case TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK:
				setRelatedDenseClock((Reference)null);
				return;
			case TracePackage.PHYSICAL_BASE__OFFSET:
				setOffset(OFFSET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TracePackage.PHYSICAL_BASE__PHYSICAL_STEPS:
				return physicalSteps != null && !physicalSteps.isEmpty();
			case TracePackage.PHYSICAL_BASE__RELATED_DENSE_CLOCK:
				return relatedDenseClock != null;
			case TracePackage.PHYSICAL_BASE__OFFSET:
				return offset != OFFSET_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (offset: ");
		result.append(offset);
		result.append(')');
		return result.toString();
	}

} //PhysicalBaseImpl
