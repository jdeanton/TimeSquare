/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace.impl;

import fr.inria.aoste.trace.DiscretizedClockStep;
import fr.inria.aoste.trace.InterDiscretizedSteps;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.TracePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inter Discretized Steps</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.impl.InterDiscretizedStepsImpl#getPreviousFixStep <em>Previous Fix Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.InterDiscretizedStepsImpl#getNextFixStep <em>Next Fix Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.InterDiscretizedStepsImpl#getCorrespondingLogicalSteps <em>Corresponding Logical Steps</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InterDiscretizedStepsImpl extends PhysicalStepsImpl implements InterDiscretizedSteps {
	/**
	 * The cached value of the '{@link #getPreviousFixStep() <em>Previous Fix Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreviousFixStep()
	 * @generated
	 * @ordered
	 */
	protected DiscretizedClockStep previousFixStep;

	/**
	 * The cached value of the '{@link #getNextFixStep() <em>Next Fix Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextFixStep()
	 * @generated
	 * @ordered
	 */
	protected DiscretizedClockStep nextFixStep;

	/**
	 * The cached value of the '{@link #getCorrespondingLogicalSteps() <em>Corresponding Logical Steps</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondingLogicalSteps()
	 * @generated
	 * @ordered
	 */
	protected EList<LogicalStep> correspondingLogicalSteps;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterDiscretizedStepsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TracePackage.Literals.INTER_DISCRETIZED_STEPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscretizedClockStep getPreviousFixStep() {
		if (previousFixStep != null && previousFixStep.eIsProxy()) {
			InternalEObject oldPreviousFixStep = (InternalEObject)previousFixStep;
			previousFixStep = (DiscretizedClockStep)eResolveProxy(oldPreviousFixStep);
			if (previousFixStep != oldPreviousFixStep) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TracePackage.INTER_DISCRETIZED_STEPS__PREVIOUS_FIX_STEP, oldPreviousFixStep, previousFixStep));
			}
		}
		return previousFixStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscretizedClockStep basicGetPreviousFixStep() {
		return previousFixStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreviousFixStep(DiscretizedClockStep newPreviousFixStep) {
		DiscretizedClockStep oldPreviousFixStep = previousFixStep;
		previousFixStep = newPreviousFixStep;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.INTER_DISCRETIZED_STEPS__PREVIOUS_FIX_STEP, oldPreviousFixStep, previousFixStep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscretizedClockStep getNextFixStep() {
		if (nextFixStep != null && nextFixStep.eIsProxy()) {
			InternalEObject oldNextFixStep = (InternalEObject)nextFixStep;
			nextFixStep = (DiscretizedClockStep)eResolveProxy(oldNextFixStep);
			if (nextFixStep != oldNextFixStep) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TracePackage.INTER_DISCRETIZED_STEPS__NEXT_FIX_STEP, oldNextFixStep, nextFixStep));
			}
		}
		return nextFixStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscretizedClockStep basicGetNextFixStep() {
		return nextFixStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextFixStep(DiscretizedClockStep newNextFixStep) {
		DiscretizedClockStep oldNextFixStep = nextFixStep;
		nextFixStep = newNextFixStep;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.INTER_DISCRETIZED_STEPS__NEXT_FIX_STEP, oldNextFixStep, nextFixStep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LogicalStep> getCorrespondingLogicalSteps() {
		if (correspondingLogicalSteps == null) {
			correspondingLogicalSteps = new EObjectResolvingEList<LogicalStep>(LogicalStep.class, this, TracePackage.INTER_DISCRETIZED_STEPS__CORRESPONDING_LOGICAL_STEPS);
		}
		return correspondingLogicalSteps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TracePackage.INTER_DISCRETIZED_STEPS__PREVIOUS_FIX_STEP:
				if (resolve) return getPreviousFixStep();
				return basicGetPreviousFixStep();
			case TracePackage.INTER_DISCRETIZED_STEPS__NEXT_FIX_STEP:
				if (resolve) return getNextFixStep();
				return basicGetNextFixStep();
			case TracePackage.INTER_DISCRETIZED_STEPS__CORRESPONDING_LOGICAL_STEPS:
				return getCorrespondingLogicalSteps();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TracePackage.INTER_DISCRETIZED_STEPS__PREVIOUS_FIX_STEP:
				setPreviousFixStep((DiscretizedClockStep)newValue);
				return;
			case TracePackage.INTER_DISCRETIZED_STEPS__NEXT_FIX_STEP:
				setNextFixStep((DiscretizedClockStep)newValue);
				return;
			case TracePackage.INTER_DISCRETIZED_STEPS__CORRESPONDING_LOGICAL_STEPS:
				getCorrespondingLogicalSteps().clear();
				getCorrespondingLogicalSteps().addAll((Collection<? extends LogicalStep>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TracePackage.INTER_DISCRETIZED_STEPS__PREVIOUS_FIX_STEP:
				setPreviousFixStep((DiscretizedClockStep)null);
				return;
			case TracePackage.INTER_DISCRETIZED_STEPS__NEXT_FIX_STEP:
				setNextFixStep((DiscretizedClockStep)null);
				return;
			case TracePackage.INTER_DISCRETIZED_STEPS__CORRESPONDING_LOGICAL_STEPS:
				getCorrespondingLogicalSteps().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TracePackage.INTER_DISCRETIZED_STEPS__PREVIOUS_FIX_STEP:
				return previousFixStep != null;
			case TracePackage.INTER_DISCRETIZED_STEPS__NEXT_FIX_STEP:
				return nextFixStep != null;
			case TracePackage.INTER_DISCRETIZED_STEPS__CORRESPONDING_LOGICAL_STEPS:
				return correspondingLogicalSteps != null && !correspondingLogicalSteps.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InterDiscretizedStepsImpl
