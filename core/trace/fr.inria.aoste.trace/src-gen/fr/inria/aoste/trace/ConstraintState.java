/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.ConstraintState#getInternalValue <em>Internal Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.trace.TracePackage#getConstraintState()
 * @model
 * @generated
 */
public interface ConstraintState extends State {
	/**
	 * Returns the value of the '<em><b>Internal Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Value</em>' attribute.
	 * @see #setInternalValue(int)
	 * @see fr.inria.aoste.trace.TracePackage#getConstraintState_InternalValue()
	 * @model
	 * @generated
	 */
	int getInternalValue();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.ConstraintState#getInternalValue <em>Internal Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Value</em>' attribute.
	 * @see #getInternalValue()
	 * @generated
	 */
	void setInternalValue(int value);

} // ConstraintState
