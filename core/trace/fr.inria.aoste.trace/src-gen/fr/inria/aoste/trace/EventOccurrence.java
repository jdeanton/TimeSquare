/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Occurrence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.EventOccurrence#getContext <em>Context</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.EventOccurrence#isIsClockDead <em>Is Clock Dead</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.EventOccurrence#getCounter <em>Counter</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.EventOccurrence#getEState <em>EState</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.EventOccurrence#getFState <em>FState</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.EventOccurrence#isWasBorn <em>Was Born</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.trace.TracePackage#getEventOccurrence()
 * @model
 * @generated
 */
public interface EventOccurrence extends State {
	/**
	 * Returns the value of the '<em><b>Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' reference.
	 * @see #setContext(Reference)
	 * @see fr.inria.aoste.trace.TracePackage#getEventOccurrence_Context()
	 * @model
	 * @generated
	 */
	Reference getContext();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.EventOccurrence#getContext <em>Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' reference.
	 * @see #getContext()
	 * @generated
	 */
	void setContext(Reference value);

	/**
	 * Returns the value of the '<em><b>Is Clock Dead</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Clock Dead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Clock Dead</em>' attribute.
	 * @see #setIsClockDead(boolean)
	 * @see fr.inria.aoste.trace.TracePackage#getEventOccurrence_IsClockDead()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isIsClockDead();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.EventOccurrence#isIsClockDead <em>Is Clock Dead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Clock Dead</em>' attribute.
	 * @see #isIsClockDead()
	 * @generated
	 */
	void setIsClockDead(boolean value);

	/**
	 * Returns the value of the '<em><b>Counter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Counter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Counter</em>' attribute.
	 * @see #setCounter(int)
	 * @see fr.inria.aoste.trace.TracePackage#getEventOccurrence_Counter()
	 * @model required="true"
	 * @generated
	 */
	int getCounter();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.EventOccurrence#getCounter <em>Counter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Counter</em>' attribute.
	 * @see #getCounter()
	 * @generated
	 */
	void setCounter(int value);

	/**
	 * Returns the value of the '<em><b>EState</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.inria.aoste.trace.EnableStateKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EState</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EState</em>' attribute.
	 * @see fr.inria.aoste.trace.EnableStateKind
	 * @see #setEState(EnableStateKind)
	 * @see fr.inria.aoste.trace.TracePackage#getEventOccurrence_EState()
	 * @model
	 * @generated
	 */
	EnableStateKind getEState();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.EventOccurrence#getEState <em>EState</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EState</em>' attribute.
	 * @see fr.inria.aoste.trace.EnableStateKind
	 * @see #getEState()
	 * @generated
	 */
	void setEState(EnableStateKind value);

	/**
	 * Returns the value of the '<em><b>FState</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.inria.aoste.trace.FiredStateKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>FState</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FState</em>' attribute.
	 * @see fr.inria.aoste.trace.FiredStateKind
	 * @see #setFState(FiredStateKind)
	 * @see fr.inria.aoste.trace.TracePackage#getEventOccurrence_FState()
	 * @model required="true"
	 * @generated
	 */
	FiredStateKind getFState();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.EventOccurrence#getFState <em>FState</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FState</em>' attribute.
	 * @see fr.inria.aoste.trace.FiredStateKind
	 * @see #getFState()
	 * @generated
	 */
	void setFState(FiredStateKind value);

	/**
	 * Returns the value of the '<em><b>Was Born</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Was Born</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Was Born</em>' attribute.
	 * @see #setWasBorn(boolean)
	 * @see fr.inria.aoste.trace.TracePackage#getEventOccurrence_WasBorn()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isWasBorn();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.EventOccurrence#isWasBorn <em>Was Born</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Was Born</em>' attribute.
	 * @see #isWasBorn()
	 * @generated
	 */
	void setWasBorn(boolean value);

} // EventOccurrence
