/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inter Discretized Steps</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.InterDiscretizedSteps#getPreviousFixStep <em>Previous Fix Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.InterDiscretizedSteps#getNextFixStep <em>Next Fix Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.InterDiscretizedSteps#getCorrespondingLogicalSteps <em>Corresponding Logical Steps</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.trace.TracePackage#getInterDiscretizedSteps()
 * @model
 * @generated
 */
public interface InterDiscretizedSteps extends PhysicalSteps {
	/**
	 * Returns the value of the '<em><b>Previous Fix Step</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous Fix Step</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous Fix Step</em>' reference.
	 * @see #setPreviousFixStep(DiscretizedClockStep)
	 * @see fr.inria.aoste.trace.TracePackage#getInterDiscretizedSteps_PreviousFixStep()
	 * @model
	 * @generated
	 */
	DiscretizedClockStep getPreviousFixStep();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.InterDiscretizedSteps#getPreviousFixStep <em>Previous Fix Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Previous Fix Step</em>' reference.
	 * @see #getPreviousFixStep()
	 * @generated
	 */
	void setPreviousFixStep(DiscretizedClockStep value);

	/**
	 * Returns the value of the '<em><b>Next Fix Step</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next Fix Step</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Fix Step</em>' reference.
	 * @see #setNextFixStep(DiscretizedClockStep)
	 * @see fr.inria.aoste.trace.TracePackage#getInterDiscretizedSteps_NextFixStep()
	 * @model
	 * @generated
	 */
	DiscretizedClockStep getNextFixStep();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.InterDiscretizedSteps#getNextFixStep <em>Next Fix Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next Fix Step</em>' reference.
	 * @see #getNextFixStep()
	 * @generated
	 */
	void setNextFixStep(DiscretizedClockStep value);

	/**
	 * Returns the value of the '<em><b>Corresponding Logical Steps</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.aoste.trace.LogicalStep}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corresponding Logical Steps</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corresponding Logical Steps</em>' reference list.
	 * @see fr.inria.aoste.trace.TracePackage#getInterDiscretizedSteps_CorrespondingLogicalSteps()
	 * @model
	 * @generated
	 */
	EList<LogicalStep> getCorrespondingLogicalSteps();

} // InterDiscretizedSteps
