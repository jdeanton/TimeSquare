/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discretized Clock Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.DiscretizedClockStep#getCorrespondingLogicalStep <em>Corresponding Logical Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.DiscretizedClockStep#getTimestamp <em>Timestamp</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.trace.TracePackage#getDiscretizedClockStep()
 * @model
 * @generated
 */
public interface DiscretizedClockStep extends PhysicalSteps {
	/**
	 * Returns the value of the '<em><b>Corresponding Logical Step</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corresponding Logical Step</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corresponding Logical Step</em>' reference.
	 * @see #setCorrespondingLogicalStep(LogicalStep)
	 * @see fr.inria.aoste.trace.TracePackage#getDiscretizedClockStep_CorrespondingLogicalStep()
	 * @model required="true"
	 * @generated
	 */
	LogicalStep getCorrespondingLogicalStep();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.DiscretizedClockStep#getCorrespondingLogicalStep <em>Corresponding Logical Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Corresponding Logical Step</em>' reference.
	 * @see #getCorrespondingLogicalStep()
	 * @generated
	 */
	void setCorrespondingLogicalStep(LogicalStep value);

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(double)
	 * @see fr.inria.aoste.trace.TracePackage#getDiscretizedClockStep_Timestamp()
	 * @model
	 * @generated
	 */
	double getTimestamp();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.DiscretizedClockStep#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(double value);

} // DiscretizedClockStep
