/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace.impl;

import fr.inria.aoste.trace.DiscretizedClockStep;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.TracePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Discretized Clock Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.impl.DiscretizedClockStepImpl#getCorrespondingLogicalStep <em>Corresponding Logical Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.DiscretizedClockStepImpl#getTimestamp <em>Timestamp</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DiscretizedClockStepImpl extends PhysicalStepsImpl implements DiscretizedClockStep {
	/**
	 * The cached value of the '{@link #getCorrespondingLogicalStep() <em>Corresponding Logical Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondingLogicalStep()
	 * @generated
	 * @ordered
	 */
	protected LogicalStep correspondingLogicalStep;

	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final double TIMESTAMP_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected double timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiscretizedClockStepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TracePackage.Literals.DISCRETIZED_CLOCK_STEP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalStep getCorrespondingLogicalStep() {
		if (correspondingLogicalStep != null && correspondingLogicalStep.eIsProxy()) {
			InternalEObject oldCorrespondingLogicalStep = (InternalEObject)correspondingLogicalStep;
			correspondingLogicalStep = (LogicalStep)eResolveProxy(oldCorrespondingLogicalStep);
			if (correspondingLogicalStep != oldCorrespondingLogicalStep) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TracePackage.DISCRETIZED_CLOCK_STEP__CORRESPONDING_LOGICAL_STEP, oldCorrespondingLogicalStep, correspondingLogicalStep));
			}
		}
		return correspondingLogicalStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalStep basicGetCorrespondingLogicalStep() {
		return correspondingLogicalStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrespondingLogicalStep(LogicalStep newCorrespondingLogicalStep) {
		LogicalStep oldCorrespondingLogicalStep = correspondingLogicalStep;
		correspondingLogicalStep = newCorrespondingLogicalStep;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.DISCRETIZED_CLOCK_STEP__CORRESPONDING_LOGICAL_STEP, oldCorrespondingLogicalStep, correspondingLogicalStep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimestamp(double newTimestamp) {
		double oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.DISCRETIZED_CLOCK_STEP__TIMESTAMP, oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TracePackage.DISCRETIZED_CLOCK_STEP__CORRESPONDING_LOGICAL_STEP:
				if (resolve) return getCorrespondingLogicalStep();
				return basicGetCorrespondingLogicalStep();
			case TracePackage.DISCRETIZED_CLOCK_STEP__TIMESTAMP:
				return getTimestamp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TracePackage.DISCRETIZED_CLOCK_STEP__CORRESPONDING_LOGICAL_STEP:
				setCorrespondingLogicalStep((LogicalStep)newValue);
				return;
			case TracePackage.DISCRETIZED_CLOCK_STEP__TIMESTAMP:
				setTimestamp((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TracePackage.DISCRETIZED_CLOCK_STEP__CORRESPONDING_LOGICAL_STEP:
				setCorrespondingLogicalStep((LogicalStep)null);
				return;
			case TracePackage.DISCRETIZED_CLOCK_STEP__TIMESTAMP:
				setTimestamp(TIMESTAMP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TracePackage.DISCRETIZED_CLOCK_STEP__CORRESPONDING_LOGICAL_STEP:
				return correspondingLogicalStep != null;
			case TracePackage.DISCRETIZED_CLOCK_STEP__TIMESTAMP:
				return timestamp != TIMESTAMP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timestamp: ");
		result.append(timestamp);
		result.append(')');
		return result.toString();
	}

} //DiscretizedClockStepImpl
