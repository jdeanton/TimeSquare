/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace.impl;

import java.lang.reflect.Field;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.inria.aoste.trace.AssertionState;
import fr.inria.aoste.trace.ConstraintState;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.NamedReference;
import fr.inria.aoste.trace.TracePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Logical Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.impl.LogicalStepImpl#getConstraintStates <em>Constraint States</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.LogicalStepImpl#getEventOccurrences <em>Event Occurrences</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.LogicalStepImpl#getNextStep <em>Next Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.LogicalStepImpl#getPreviousStep <em>Previous Step</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.LogicalStepImpl#getStepNumber <em>Step Number</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.LogicalStepImpl#getAssertionStates <em>Assertion States</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LogicalStepImpl extends EObjectImpl implements LogicalStep {
	/**
	 * The cached value of the '{@link #getConstraintStates() <em>Constraint States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintStates()
	 * @generated
	 * @ordered
	 */
	protected EList<ConstraintState> constraintStates;

	/**
	 * The cached value of the '{@link #getEventOccurrences() <em>Event Occurrences</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventOccurrences()
	 * @generated
	 * @ordered
	 */
	protected EList<EventOccurrence> eventOccurrences;

	/**
	 * The cached value of the '{@link #getNextStep() <em>Next Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextStep()
	 * @generated
	 * @ordered
	 */
	protected LogicalStep nextStep;

	/**
	 * The cached value of the '{@link #getPreviousStep() <em>Previous Step</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreviousStep()
	 * @generated
	 * @ordered
	 */
	protected LogicalStep previousStep;

	/**
	 * The default value of the '{@link #getStepNumber() <em>Step Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int STEP_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStepNumber() <em>Step Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepNumber()
	 * @generated
	 * @ordered
	 */
	protected int stepNumber = STEP_NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAssertionStates() <em>Assertion States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertionStates()
	 * @generated
	 * @ordered
	 */
	protected EList<AssertionState> assertionStates;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogicalStepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TracePackage.Literals.LOGICAL_STEP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConstraintState> getConstraintStates() {
		if (constraintStates == null) {
			constraintStates = new EObjectContainmentEList<ConstraintState>(ConstraintState.class, this, TracePackage.LOGICAL_STEP__CONSTRAINT_STATES);
		}
		return constraintStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventOccurrence> getEventOccurrences() {
		if (eventOccurrences == null) {
			eventOccurrences = new EObjectContainmentEList<EventOccurrence>(EventOccurrence.class, this, TracePackage.LOGICAL_STEP__EVENT_OCCURRENCES);
		}
		return eventOccurrences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalStep getNextStep() {
		if (nextStep != null && nextStep.eIsProxy()) {
			InternalEObject oldNextStep = (InternalEObject)nextStep;
			nextStep = (LogicalStep)eResolveProxy(oldNextStep);
			if (nextStep != oldNextStep) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TracePackage.LOGICAL_STEP__NEXT_STEP, oldNextStep, nextStep));
			}
		}
		return nextStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalStep basicGetNextStep() {
		return nextStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNextStep(LogicalStep newNextStep, NotificationChain msgs) {
		LogicalStep oldNextStep = nextStep;
		nextStep = newNextStep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TracePackage.LOGICAL_STEP__NEXT_STEP, oldNextStep, newNextStep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextStep(LogicalStep newNextStep) {
		if (newNextStep != nextStep) {
			NotificationChain msgs = null;
			if (nextStep != null)
				msgs = ((InternalEObject)nextStep).eInverseRemove(this, TracePackage.LOGICAL_STEP__PREVIOUS_STEP, LogicalStep.class, msgs);
			if (newNextStep != null)
				msgs = ((InternalEObject)newNextStep).eInverseAdd(this, TracePackage.LOGICAL_STEP__PREVIOUS_STEP, LogicalStep.class, msgs);
			msgs = basicSetNextStep(newNextStep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.LOGICAL_STEP__NEXT_STEP, newNextStep, newNextStep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalStep getPreviousStep() {
		if (previousStep != null && previousStep.eIsProxy()) {
			InternalEObject oldPreviousStep = (InternalEObject)previousStep;
			previousStep = (LogicalStep)eResolveProxy(oldPreviousStep);
			if (previousStep != oldPreviousStep) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TracePackage.LOGICAL_STEP__PREVIOUS_STEP, oldPreviousStep, previousStep));
			}
		}
		return previousStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalStep basicGetPreviousStep() {
		return previousStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreviousStep(LogicalStep newPreviousStep, NotificationChain msgs) {
		LogicalStep oldPreviousStep = previousStep;
		previousStep = newPreviousStep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TracePackage.LOGICAL_STEP__PREVIOUS_STEP, oldPreviousStep, newPreviousStep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreviousStep(LogicalStep newPreviousStep) {
		if (newPreviousStep != previousStep) {
			NotificationChain msgs = null;
			if (previousStep != null)
				msgs = ((InternalEObject)previousStep).eInverseRemove(this, TracePackage.LOGICAL_STEP__NEXT_STEP, LogicalStep.class, msgs);
			if (newPreviousStep != null)
				msgs = ((InternalEObject)newPreviousStep).eInverseAdd(this, TracePackage.LOGICAL_STEP__NEXT_STEP, LogicalStep.class, msgs);
			msgs = basicSetPreviousStep(newPreviousStep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.LOGICAL_STEP__PREVIOUS_STEP, newPreviousStep, newPreviousStep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getStepNumber() {
		return stepNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStepNumber(int newStepNumber) {
		int oldStepNumber = stepNumber;
		stepNumber = newStepNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.LOGICAL_STEP__STEP_NUMBER, oldStepNumber, stepNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionState> getAssertionStates() {
		if (assertionStates == null) {
			assertionStates = new EObjectContainmentEList<AssertionState>(AssertionState.class, this, TracePackage.LOGICAL_STEP__ASSERTION_STATES);
		}
		return assertionStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TracePackage.LOGICAL_STEP__NEXT_STEP:
				if (nextStep != null)
					msgs = ((InternalEObject)nextStep).eInverseRemove(this, TracePackage.LOGICAL_STEP__PREVIOUS_STEP, LogicalStep.class, msgs);
				return basicSetNextStep((LogicalStep)otherEnd, msgs);
			case TracePackage.LOGICAL_STEP__PREVIOUS_STEP:
				if (previousStep != null)
					msgs = ((InternalEObject)previousStep).eInverseRemove(this, TracePackage.LOGICAL_STEP__NEXT_STEP, LogicalStep.class, msgs);
				return basicSetPreviousStep((LogicalStep)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TracePackage.LOGICAL_STEP__CONSTRAINT_STATES:
				return ((InternalEList<?>)getConstraintStates()).basicRemove(otherEnd, msgs);
			case TracePackage.LOGICAL_STEP__EVENT_OCCURRENCES:
				return ((InternalEList<?>)getEventOccurrences()).basicRemove(otherEnd, msgs);
			case TracePackage.LOGICAL_STEP__NEXT_STEP:
				return basicSetNextStep(null, msgs);
			case TracePackage.LOGICAL_STEP__PREVIOUS_STEP:
				return basicSetPreviousStep(null, msgs);
			case TracePackage.LOGICAL_STEP__ASSERTION_STATES:
				return ((InternalEList<?>)getAssertionStates()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TracePackage.LOGICAL_STEP__CONSTRAINT_STATES:
				return getConstraintStates();
			case TracePackage.LOGICAL_STEP__EVENT_OCCURRENCES:
				return getEventOccurrences();
			case TracePackage.LOGICAL_STEP__NEXT_STEP:
				if (resolve) return getNextStep();
				return basicGetNextStep();
			case TracePackage.LOGICAL_STEP__PREVIOUS_STEP:
				if (resolve) return getPreviousStep();
				return basicGetPreviousStep();
			case TracePackage.LOGICAL_STEP__STEP_NUMBER:
				return getStepNumber();
			case TracePackage.LOGICAL_STEP__ASSERTION_STATES:
				return getAssertionStates();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TracePackage.LOGICAL_STEP__CONSTRAINT_STATES:
				getConstraintStates().clear();
				getConstraintStates().addAll((Collection<? extends ConstraintState>)newValue);
				return;
			case TracePackage.LOGICAL_STEP__EVENT_OCCURRENCES:
				getEventOccurrences().clear();
				getEventOccurrences().addAll((Collection<? extends EventOccurrence>)newValue);
				return;
			case TracePackage.LOGICAL_STEP__NEXT_STEP:
				setNextStep((LogicalStep)newValue);
				return;
			case TracePackage.LOGICAL_STEP__PREVIOUS_STEP:
				setPreviousStep((LogicalStep)newValue);
				return;
			case TracePackage.LOGICAL_STEP__STEP_NUMBER:
				setStepNumber((Integer)newValue);
				return;
			case TracePackage.LOGICAL_STEP__ASSERTION_STATES:
				getAssertionStates().clear();
				getAssertionStates().addAll((Collection<? extends AssertionState>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TracePackage.LOGICAL_STEP__CONSTRAINT_STATES:
				getConstraintStates().clear();
				return;
			case TracePackage.LOGICAL_STEP__EVENT_OCCURRENCES:
				getEventOccurrences().clear();
				return;
			case TracePackage.LOGICAL_STEP__NEXT_STEP:
				setNextStep((LogicalStep)null);
				return;
			case TracePackage.LOGICAL_STEP__PREVIOUS_STEP:
				setPreviousStep((LogicalStep)null);
				return;
			case TracePackage.LOGICAL_STEP__STEP_NUMBER:
				setStepNumber(STEP_NUMBER_EDEFAULT);
				return;
			case TracePackage.LOGICAL_STEP__ASSERTION_STATES:
				getAssertionStates().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TracePackage.LOGICAL_STEP__CONSTRAINT_STATES:
				return constraintStates != null && !constraintStates.isEmpty();
			case TracePackage.LOGICAL_STEP__EVENT_OCCURRENCES:
				return eventOccurrences != null && !eventOccurrences.isEmpty();
			case TracePackage.LOGICAL_STEP__NEXT_STEP:
				return nextStep != null;
			case TracePackage.LOGICAL_STEP__PREVIOUS_STEP:
				return previousStep != null;
			case TracePackage.LOGICAL_STEP__STEP_NUMBER:
				return stepNumber != STEP_NUMBER_EDEFAULT;
			case TracePackage.LOGICAL_STEP__ASSERTION_STATES:
				return assertionStates != null && !assertionStates.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer("");//super.toString());
		//result.append(" (stepNumber: ");
		//result.append(stepNumber);
		//result.append(')');
			for(EventOccurrence occ : this.getEventOccurrences()){
				if(occ.getFState() == FiredStateKind.TICK){
				if(occ.getReferedElement() instanceof ModelElementReference){
					EList<EObject> instantiationPath = ((ModelElementReference)occ.getReferedElement()).getElementRef();
						EObject toPrint = instantiationPath.get(instantiationPath.size()-1);
						Field f = null;
						try {
							System.out.println(toPrint.getClass());
							for(Field ff : toPrint.getClass().getFields()){
							System.out.println(ff);
							}
							f = toPrint.getClass().getDeclaredField("name");
						} catch (NoSuchFieldException e1) {
							//e1.printStackTrace();
						} catch (SecurityException e1) {
							e1.printStackTrace();
						}
						
						try {
							if (f != null){
								result.append(f.get(toPrint)+"  ");
							}
						} catch (Throwable e) {
							e.printStackTrace();
						}
				}else{
					result.append(((NamedReference)occ.getReferedElement()).getValue());
				}
			}
			}
	
		return result.toString();
	}

} //LogicalStepImpl
