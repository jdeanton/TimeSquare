/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.trace.TraceFactory
 * @model kind="package"
 * @generated
 */
public interface TracePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "trace";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://team.inria.fr/kairos//trace/1.0.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "trace";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TracePackage eINSTANCE = fr.inria.aoste.trace.impl.TracePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.TraceImpl <em>Trace</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.TraceImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getTrace()
	 * @generated
	 */
	int TRACE = 0;

	/**
	 * The feature id for the '<em><b>Logical Steps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE__LOGICAL_STEPS = 0;

	/**
	 * The feature id for the '<em><b>Physical Bases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE__PHYSICAL_BASES = 1;

	/**
	 * The feature id for the '<em><b>References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE__REFERENCES = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE__NAME = 3;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE__DATE = 4;

	/**
	 * The feature id for the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE__AUTHOR = 5;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE__DOCUMENTATION = 6;

	/**
	 * The number of structural features of the '<em>Trace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.LogicalStepImpl <em>Logical Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.LogicalStepImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getLogicalStep()
	 * @generated
	 */
	int LOGICAL_STEP = 1;

	/**
	 * The feature id for the '<em><b>Constraint States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_STEP__CONSTRAINT_STATES = 0;

	/**
	 * The feature id for the '<em><b>Event Occurrences</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_STEP__EVENT_OCCURRENCES = 1;

	/**
	 * The feature id for the '<em><b>Next Step</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_STEP__NEXT_STEP = 2;

	/**
	 * The feature id for the '<em><b>Previous Step</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_STEP__PREVIOUS_STEP = 3;

	/**
	 * The feature id for the '<em><b>Step Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_STEP__STEP_NUMBER = 4;

	/**
	 * The feature id for the '<em><b>Assertion States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_STEP__ASSERTION_STATES = 5;

	/**
	 * The number of structural features of the '<em>Logical Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_STEP_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.StateImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getState()
	 * @generated
	 */
	int STATE = 4;

	/**
	 * The feature id for the '<em><b>Refered Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__REFERED_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.ConstraintStateImpl <em>Constraint State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.ConstraintStateImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getConstraintState()
	 * @generated
	 */
	int CONSTRAINT_STATE = 2;

	/**
	 * The feature id for the '<em><b>Refered Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_STATE__REFERED_ELEMENT = STATE__REFERED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Internal Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_STATE__INTERNAL_VALUE = STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constraint State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.EventOccurrenceImpl <em>Event Occurrence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.EventOccurrenceImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getEventOccurrence()
	 * @generated
	 */
	int EVENT_OCCURRENCE = 3;

	/**
	 * The feature id for the '<em><b>Refered Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE__REFERED_ELEMENT = STATE__REFERED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE__CONTEXT = STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Clock Dead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE__IS_CLOCK_DEAD = STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Counter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE__COUNTER = STATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>EState</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE__ESTATE = STATE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>FState</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE__FSTATE = STATE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Was Born</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE__WAS_BORN = STATE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Event Occurrence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE_FEATURE_COUNT = STATE_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.ReferenceImpl <em>Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.ReferenceImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getReference()
	 * @generated
	 */
	int REFERENCE = 5;

	/**
	 * The number of structural features of the '<em>Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.ModelElementReferenceImpl <em>Model Element Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.ModelElementReferenceImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getModelElementReference()
	 * @generated
	 */
	int MODEL_ELEMENT_REFERENCE = 6;

	/**
	 * The feature id for the '<em><b>Element Ref</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT_REFERENCE__ELEMENT_REF = REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Model Element Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_ELEMENT_REFERENCE_FEATURE_COUNT = REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.NamedReferenceImpl <em>Named Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.NamedReferenceImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getNamedReference()
	 * @generated
	 */
	int NAMED_REFERENCE = 7;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_REFERENCE__VALUE = REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Named Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_REFERENCE_FEATURE_COUNT = REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.PhysicalBaseImpl <em>Physical Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.PhysicalBaseImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getPhysicalBase()
	 * @generated
	 */
	int PHYSICAL_BASE = 8;

	/**
	 * The feature id for the '<em><b>Physical Steps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BASE__PHYSICAL_STEPS = 0;

	/**
	 * The feature id for the '<em><b>Related Dense Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BASE__RELATED_DENSE_CLOCK = 1;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BASE__OFFSET = 2;

	/**
	 * The number of structural features of the '<em>Physical Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_BASE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.PhysicalStepsImpl <em>Physical Steps</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.PhysicalStepsImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getPhysicalSteps()
	 * @generated
	 */
	int PHYSICAL_STEPS = 9;

	/**
	 * The number of structural features of the '<em>Physical Steps</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_STEPS_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.DiscretizedClockStepImpl <em>Discretized Clock Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.DiscretizedClockStepImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getDiscretizedClockStep()
	 * @generated
	 */
	int DISCRETIZED_CLOCK_STEP = 10;

	/**
	 * The feature id for the '<em><b>Corresponding Logical Step</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCRETIZED_CLOCK_STEP__CORRESPONDING_LOGICAL_STEP = PHYSICAL_STEPS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCRETIZED_CLOCK_STEP__TIMESTAMP = PHYSICAL_STEPS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Discretized Clock Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCRETIZED_CLOCK_STEP_FEATURE_COUNT = PHYSICAL_STEPS_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.InterDiscretizedStepsImpl <em>Inter Discretized Steps</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.InterDiscretizedStepsImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getInterDiscretizedSteps()
	 * @generated
	 */
	int INTER_DISCRETIZED_STEPS = 11;

	/**
	 * The feature id for the '<em><b>Previous Fix Step</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTER_DISCRETIZED_STEPS__PREVIOUS_FIX_STEP = PHYSICAL_STEPS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Next Fix Step</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTER_DISCRETIZED_STEPS__NEXT_FIX_STEP = PHYSICAL_STEPS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Corresponding Logical Steps</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTER_DISCRETIZED_STEPS__CORRESPONDING_LOGICAL_STEPS = PHYSICAL_STEPS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Inter Discretized Steps</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTER_DISCRETIZED_STEPS_FEATURE_COUNT = PHYSICAL_STEPS_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.impl.AssertionStateImpl <em>Assertion State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.impl.AssertionStateImpl
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getAssertionState()
	 * @generated
	 */
	int ASSERTION_STATE = 12;

	/**
	 * The feature id for the '<em><b>Refered Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATE__REFERED_ELEMENT = STATE__REFERED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Violated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATE__IS_VIOLATED = STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Assertion State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.EnableStateKind <em>Enable State Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.EnableStateKind
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getEnableStateKind()
	 * @generated
	 */
	int ENABLE_STATE_KIND = 13;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.trace.FiredStateKind <em>Fired State Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.trace.FiredStateKind
	 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getFiredStateKind()
	 * @generated
	 */
	int FIRED_STATE_KIND = 14;


	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.Trace <em>Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trace</em>'.
	 * @see fr.inria.aoste.trace.Trace
	 * @generated
	 */
	EClass getTrace();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.trace.Trace#getLogicalSteps <em>Logical Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Logical Steps</em>'.
	 * @see fr.inria.aoste.trace.Trace#getLogicalSteps()
	 * @see #getTrace()
	 * @generated
	 */
	EReference getTrace_LogicalSteps();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.trace.Trace#getPhysicalBases <em>Physical Bases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Bases</em>'.
	 * @see fr.inria.aoste.trace.Trace#getPhysicalBases()
	 * @see #getTrace()
	 * @generated
	 */
	EReference getTrace_PhysicalBases();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.trace.Trace#getReferences <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>References</em>'.
	 * @see fr.inria.aoste.trace.Trace#getReferences()
	 * @see #getTrace()
	 * @generated
	 */
	EReference getTrace_References();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.Trace#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.inria.aoste.trace.Trace#getName()
	 * @see #getTrace()
	 * @generated
	 */
	EAttribute getTrace_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.Trace#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see fr.inria.aoste.trace.Trace#getDate()
	 * @see #getTrace()
	 * @generated
	 */
	EAttribute getTrace_Date();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.Trace#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Author</em>'.
	 * @see fr.inria.aoste.trace.Trace#getAuthor()
	 * @see #getTrace()
	 * @generated
	 */
	EAttribute getTrace_Author();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.Trace#getDocumentation <em>Documentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Documentation</em>'.
	 * @see fr.inria.aoste.trace.Trace#getDocumentation()
	 * @see #getTrace()
	 * @generated
	 */
	EAttribute getTrace_Documentation();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.LogicalStep <em>Logical Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Step</em>'.
	 * @see fr.inria.aoste.trace.LogicalStep
	 * @generated
	 */
	EClass getLogicalStep();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.trace.LogicalStep#getConstraintStates <em>Constraint States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraint States</em>'.
	 * @see fr.inria.aoste.trace.LogicalStep#getConstraintStates()
	 * @see #getLogicalStep()
	 * @generated
	 */
	EReference getLogicalStep_ConstraintStates();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.trace.LogicalStep#getEventOccurrences <em>Event Occurrences</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Occurrences</em>'.
	 * @see fr.inria.aoste.trace.LogicalStep#getEventOccurrences()
	 * @see #getLogicalStep()
	 * @generated
	 */
	EReference getLogicalStep_EventOccurrences();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.trace.LogicalStep#getNextStep <em>Next Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next Step</em>'.
	 * @see fr.inria.aoste.trace.LogicalStep#getNextStep()
	 * @see #getLogicalStep()
	 * @generated
	 */
	EReference getLogicalStep_NextStep();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.trace.LogicalStep#getPreviousStep <em>Previous Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Previous Step</em>'.
	 * @see fr.inria.aoste.trace.LogicalStep#getPreviousStep()
	 * @see #getLogicalStep()
	 * @generated
	 */
	EReference getLogicalStep_PreviousStep();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.LogicalStep#getStepNumber <em>Step Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Step Number</em>'.
	 * @see fr.inria.aoste.trace.LogicalStep#getStepNumber()
	 * @see #getLogicalStep()
	 * @generated
	 */
	EAttribute getLogicalStep_StepNumber();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.trace.LogicalStep#getAssertionStates <em>Assertion States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assertion States</em>'.
	 * @see fr.inria.aoste.trace.LogicalStep#getAssertionStates()
	 * @see #getLogicalStep()
	 * @generated
	 */
	EReference getLogicalStep_AssertionStates();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.ConstraintState <em>Constraint State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint State</em>'.
	 * @see fr.inria.aoste.trace.ConstraintState
	 * @generated
	 */
	EClass getConstraintState();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.ConstraintState#getInternalValue <em>Internal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Internal Value</em>'.
	 * @see fr.inria.aoste.trace.ConstraintState#getInternalValue()
	 * @see #getConstraintState()
	 * @generated
	 */
	EAttribute getConstraintState_InternalValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.EventOccurrence <em>Event Occurrence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Occurrence</em>'.
	 * @see fr.inria.aoste.trace.EventOccurrence
	 * @generated
	 */
	EClass getEventOccurrence();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.trace.EventOccurrence#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Context</em>'.
	 * @see fr.inria.aoste.trace.EventOccurrence#getContext()
	 * @see #getEventOccurrence()
	 * @generated
	 */
	EReference getEventOccurrence_Context();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.EventOccurrence#isIsClockDead <em>Is Clock Dead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Clock Dead</em>'.
	 * @see fr.inria.aoste.trace.EventOccurrence#isIsClockDead()
	 * @see #getEventOccurrence()
	 * @generated
	 */
	EAttribute getEventOccurrence_IsClockDead();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.EventOccurrence#getCounter <em>Counter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Counter</em>'.
	 * @see fr.inria.aoste.trace.EventOccurrence#getCounter()
	 * @see #getEventOccurrence()
	 * @generated
	 */
	EAttribute getEventOccurrence_Counter();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.EventOccurrence#getEState <em>EState</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EState</em>'.
	 * @see fr.inria.aoste.trace.EventOccurrence#getEState()
	 * @see #getEventOccurrence()
	 * @generated
	 */
	EAttribute getEventOccurrence_EState();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.EventOccurrence#getFState <em>FState</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>FState</em>'.
	 * @see fr.inria.aoste.trace.EventOccurrence#getFState()
	 * @see #getEventOccurrence()
	 * @generated
	 */
	EAttribute getEventOccurrence_FState();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.EventOccurrence#isWasBorn <em>Was Born</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Was Born</em>'.
	 * @see fr.inria.aoste.trace.EventOccurrence#isWasBorn()
	 * @see #getEventOccurrence()
	 * @generated
	 */
	EAttribute getEventOccurrence_WasBorn();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see fr.inria.aoste.trace.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.trace.State#getReferedElement <em>Refered Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refered Element</em>'.
	 * @see fr.inria.aoste.trace.State#getReferedElement()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_ReferedElement();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.Reference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference</em>'.
	 * @see fr.inria.aoste.trace.Reference
	 * @generated
	 */
	EClass getReference();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.ModelElementReference <em>Model Element Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Element Reference</em>'.
	 * @see fr.inria.aoste.trace.ModelElementReference
	 * @generated
	 */
	EClass getModelElementReference();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.aoste.trace.ModelElementReference#getElementRef <em>Element Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Element Ref</em>'.
	 * @see fr.inria.aoste.trace.ModelElementReference#getElementRef()
	 * @see #getModelElementReference()
	 * @generated
	 */
	EReference getModelElementReference_ElementRef();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.NamedReference <em>Named Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Reference</em>'.
	 * @see fr.inria.aoste.trace.NamedReference
	 * @generated
	 */
	EClass getNamedReference();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.NamedReference#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.aoste.trace.NamedReference#getValue()
	 * @see #getNamedReference()
	 * @generated
	 */
	EAttribute getNamedReference_Value();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.PhysicalBase <em>Physical Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Base</em>'.
	 * @see fr.inria.aoste.trace.PhysicalBase
	 * @generated
	 */
	EClass getPhysicalBase();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.trace.PhysicalBase#getPhysicalSteps <em>Physical Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Physical Steps</em>'.
	 * @see fr.inria.aoste.trace.PhysicalBase#getPhysicalSteps()
	 * @see #getPhysicalBase()
	 * @generated
	 */
	EReference getPhysicalBase_PhysicalSteps();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.trace.PhysicalBase#getRelatedDenseClock <em>Related Dense Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Related Dense Clock</em>'.
	 * @see fr.inria.aoste.trace.PhysicalBase#getRelatedDenseClock()
	 * @see #getPhysicalBase()
	 * @generated
	 */
	EReference getPhysicalBase_RelatedDenseClock();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.PhysicalBase#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see fr.inria.aoste.trace.PhysicalBase#getOffset()
	 * @see #getPhysicalBase()
	 * @generated
	 */
	EAttribute getPhysicalBase_Offset();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.PhysicalSteps <em>Physical Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Steps</em>'.
	 * @see fr.inria.aoste.trace.PhysicalSteps
	 * @generated
	 */
	EClass getPhysicalSteps();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.DiscretizedClockStep <em>Discretized Clock Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Discretized Clock Step</em>'.
	 * @see fr.inria.aoste.trace.DiscretizedClockStep
	 * @generated
	 */
	EClass getDiscretizedClockStep();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.trace.DiscretizedClockStep#getCorrespondingLogicalStep <em>Corresponding Logical Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Corresponding Logical Step</em>'.
	 * @see fr.inria.aoste.trace.DiscretizedClockStep#getCorrespondingLogicalStep()
	 * @see #getDiscretizedClockStep()
	 * @generated
	 */
	EReference getDiscretizedClockStep_CorrespondingLogicalStep();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.DiscretizedClockStep#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see fr.inria.aoste.trace.DiscretizedClockStep#getTimestamp()
	 * @see #getDiscretizedClockStep()
	 * @generated
	 */
	EAttribute getDiscretizedClockStep_Timestamp();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.InterDiscretizedSteps <em>Inter Discretized Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inter Discretized Steps</em>'.
	 * @see fr.inria.aoste.trace.InterDiscretizedSteps
	 * @generated
	 */
	EClass getInterDiscretizedSteps();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.trace.InterDiscretizedSteps#getPreviousFixStep <em>Previous Fix Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Previous Fix Step</em>'.
	 * @see fr.inria.aoste.trace.InterDiscretizedSteps#getPreviousFixStep()
	 * @see #getInterDiscretizedSteps()
	 * @generated
	 */
	EReference getInterDiscretizedSteps_PreviousFixStep();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.trace.InterDiscretizedSteps#getNextFixStep <em>Next Fix Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next Fix Step</em>'.
	 * @see fr.inria.aoste.trace.InterDiscretizedSteps#getNextFixStep()
	 * @see #getInterDiscretizedSteps()
	 * @generated
	 */
	EReference getInterDiscretizedSteps_NextFixStep();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.aoste.trace.InterDiscretizedSteps#getCorrespondingLogicalSteps <em>Corresponding Logical Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Corresponding Logical Steps</em>'.
	 * @see fr.inria.aoste.trace.InterDiscretizedSteps#getCorrespondingLogicalSteps()
	 * @see #getInterDiscretizedSteps()
	 * @generated
	 */
	EReference getInterDiscretizedSteps_CorrespondingLogicalSteps();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.trace.AssertionState <em>Assertion State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion State</em>'.
	 * @see fr.inria.aoste.trace.AssertionState
	 * @generated
	 */
	EClass getAssertionState();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.trace.AssertionState#isIsViolated <em>Is Violated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Violated</em>'.
	 * @see fr.inria.aoste.trace.AssertionState#isIsViolated()
	 * @see #getAssertionState()
	 * @generated
	 */
	EAttribute getAssertionState_IsViolated();

	/**
	 * Returns the meta object for enum '{@link fr.inria.aoste.trace.EnableStateKind <em>Enable State Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Enable State Kind</em>'.
	 * @see fr.inria.aoste.trace.EnableStateKind
	 * @generated
	 */
	EEnum getEnableStateKind();

	/**
	 * Returns the meta object for enum '{@link fr.inria.aoste.trace.FiredStateKind <em>Fired State Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Fired State Kind</em>'.
	 * @see fr.inria.aoste.trace.FiredStateKind
	 * @generated
	 */
	EEnum getFiredStateKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TraceFactory getTraceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.TraceImpl <em>Trace</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.TraceImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getTrace()
		 * @generated
		 */
		EClass TRACE = eINSTANCE.getTrace();

		/**
		 * The meta object literal for the '<em><b>Logical Steps</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRACE__LOGICAL_STEPS = eINSTANCE.getTrace_LogicalSteps();

		/**
		 * The meta object literal for the '<em><b>Physical Bases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRACE__PHYSICAL_BASES = eINSTANCE.getTrace_PhysicalBases();

		/**
		 * The meta object literal for the '<em><b>References</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRACE__REFERENCES = eINSTANCE.getTrace_References();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRACE__NAME = eINSTANCE.getTrace_Name();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRACE__DATE = eINSTANCE.getTrace_Date();

		/**
		 * The meta object literal for the '<em><b>Author</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRACE__AUTHOR = eINSTANCE.getTrace_Author();

		/**
		 * The meta object literal for the '<em><b>Documentation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRACE__DOCUMENTATION = eINSTANCE.getTrace_Documentation();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.LogicalStepImpl <em>Logical Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.LogicalStepImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getLogicalStep()
		 * @generated
		 */
		EClass LOGICAL_STEP = eINSTANCE.getLogicalStep();

		/**
		 * The meta object literal for the '<em><b>Constraint States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_STEP__CONSTRAINT_STATES = eINSTANCE.getLogicalStep_ConstraintStates();

		/**
		 * The meta object literal for the '<em><b>Event Occurrences</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_STEP__EVENT_OCCURRENCES = eINSTANCE.getLogicalStep_EventOccurrences();

		/**
		 * The meta object literal for the '<em><b>Next Step</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_STEP__NEXT_STEP = eINSTANCE.getLogicalStep_NextStep();

		/**
		 * The meta object literal for the '<em><b>Previous Step</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_STEP__PREVIOUS_STEP = eINSTANCE.getLogicalStep_PreviousStep();

		/**
		 * The meta object literal for the '<em><b>Step Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGICAL_STEP__STEP_NUMBER = eINSTANCE.getLogicalStep_StepNumber();

		/**
		 * The meta object literal for the '<em><b>Assertion States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_STEP__ASSERTION_STATES = eINSTANCE.getLogicalStep_AssertionStates();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.ConstraintStateImpl <em>Constraint State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.ConstraintStateImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getConstraintState()
		 * @generated
		 */
		EClass CONSTRAINT_STATE = eINSTANCE.getConstraintState();

		/**
		 * The meta object literal for the '<em><b>Internal Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTRAINT_STATE__INTERNAL_VALUE = eINSTANCE.getConstraintState_InternalValue();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.EventOccurrenceImpl <em>Event Occurrence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.EventOccurrenceImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getEventOccurrence()
		 * @generated
		 */
		EClass EVENT_OCCURRENCE = eINSTANCE.getEventOccurrence();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_OCCURRENCE__CONTEXT = eINSTANCE.getEventOccurrence_Context();

		/**
		 * The meta object literal for the '<em><b>Is Clock Dead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_OCCURRENCE__IS_CLOCK_DEAD = eINSTANCE.getEventOccurrence_IsClockDead();

		/**
		 * The meta object literal for the '<em><b>Counter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_OCCURRENCE__COUNTER = eINSTANCE.getEventOccurrence_Counter();

		/**
		 * The meta object literal for the '<em><b>EState</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_OCCURRENCE__ESTATE = eINSTANCE.getEventOccurrence_EState();

		/**
		 * The meta object literal for the '<em><b>FState</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_OCCURRENCE__FSTATE = eINSTANCE.getEventOccurrence_FState();

		/**
		 * The meta object literal for the '<em><b>Was Born</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_OCCURRENCE__WAS_BORN = eINSTANCE.getEventOccurrence_WasBorn();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.StateImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Refered Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__REFERED_ELEMENT = eINSTANCE.getState_ReferedElement();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.ReferenceImpl <em>Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.ReferenceImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getReference()
		 * @generated
		 */
		EClass REFERENCE = eINSTANCE.getReference();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.ModelElementReferenceImpl <em>Model Element Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.ModelElementReferenceImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getModelElementReference()
		 * @generated
		 */
		EClass MODEL_ELEMENT_REFERENCE = eINSTANCE.getModelElementReference();

		/**
		 * The meta object literal for the '<em><b>Element Ref</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_ELEMENT_REFERENCE__ELEMENT_REF = eINSTANCE.getModelElementReference_ElementRef();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.NamedReferenceImpl <em>Named Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.NamedReferenceImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getNamedReference()
		 * @generated
		 */
		EClass NAMED_REFERENCE = eINSTANCE.getNamedReference();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_REFERENCE__VALUE = eINSTANCE.getNamedReference_Value();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.PhysicalBaseImpl <em>Physical Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.PhysicalBaseImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getPhysicalBase()
		 * @generated
		 */
		EClass PHYSICAL_BASE = eINSTANCE.getPhysicalBase();

		/**
		 * The meta object literal for the '<em><b>Physical Steps</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_BASE__PHYSICAL_STEPS = eINSTANCE.getPhysicalBase_PhysicalSteps();

		/**
		 * The meta object literal for the '<em><b>Related Dense Clock</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_BASE__RELATED_DENSE_CLOCK = eINSTANCE.getPhysicalBase_RelatedDenseClock();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_BASE__OFFSET = eINSTANCE.getPhysicalBase_Offset();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.PhysicalStepsImpl <em>Physical Steps</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.PhysicalStepsImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getPhysicalSteps()
		 * @generated
		 */
		EClass PHYSICAL_STEPS = eINSTANCE.getPhysicalSteps();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.DiscretizedClockStepImpl <em>Discretized Clock Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.DiscretizedClockStepImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getDiscretizedClockStep()
		 * @generated
		 */
		EClass DISCRETIZED_CLOCK_STEP = eINSTANCE.getDiscretizedClockStep();

		/**
		 * The meta object literal for the '<em><b>Corresponding Logical Step</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISCRETIZED_CLOCK_STEP__CORRESPONDING_LOGICAL_STEP = eINSTANCE.getDiscretizedClockStep_CorrespondingLogicalStep();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISCRETIZED_CLOCK_STEP__TIMESTAMP = eINSTANCE.getDiscretizedClockStep_Timestamp();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.InterDiscretizedStepsImpl <em>Inter Discretized Steps</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.InterDiscretizedStepsImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getInterDiscretizedSteps()
		 * @generated
		 */
		EClass INTER_DISCRETIZED_STEPS = eINSTANCE.getInterDiscretizedSteps();

		/**
		 * The meta object literal for the '<em><b>Previous Fix Step</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTER_DISCRETIZED_STEPS__PREVIOUS_FIX_STEP = eINSTANCE.getInterDiscretizedSteps_PreviousFixStep();

		/**
		 * The meta object literal for the '<em><b>Next Fix Step</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTER_DISCRETIZED_STEPS__NEXT_FIX_STEP = eINSTANCE.getInterDiscretizedSteps_NextFixStep();

		/**
		 * The meta object literal for the '<em><b>Corresponding Logical Steps</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTER_DISCRETIZED_STEPS__CORRESPONDING_LOGICAL_STEPS = eINSTANCE.getInterDiscretizedSteps_CorrespondingLogicalSteps();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.impl.AssertionStateImpl <em>Assertion State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.impl.AssertionStateImpl
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getAssertionState()
		 * @generated
		 */
		EClass ASSERTION_STATE = eINSTANCE.getAssertionState();

		/**
		 * The meta object literal for the '<em><b>Is Violated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTION_STATE__IS_VIOLATED = eINSTANCE.getAssertionState_IsViolated();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.EnableStateKind <em>Enable State Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.EnableStateKind
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getEnableStateKind()
		 * @generated
		 */
		EEnum ENABLE_STATE_KIND = eINSTANCE.getEnableStateKind();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.trace.FiredStateKind <em>Fired State Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.trace.FiredStateKind
		 * @see fr.inria.aoste.trace.impl.TracePackageImpl#getFiredStateKind()
		 * @generated
		 */
		EEnum FIRED_STATE_KIND = eINSTANCE.getFiredStateKind();

	}

} //TracePackage
