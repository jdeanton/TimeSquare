/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Base</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.PhysicalBase#getPhysicalSteps <em>Physical Steps</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.PhysicalBase#getRelatedDenseClock <em>Related Dense Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.PhysicalBase#getOffset <em>Offset</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.trace.TracePackage#getPhysicalBase()
 * @model
 * @generated
 */
public interface PhysicalBase extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Steps</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.trace.PhysicalSteps}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Steps</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Steps</em>' containment reference list.
	 * @see fr.inria.aoste.trace.TracePackage#getPhysicalBase_PhysicalSteps()
	 * @model containment="true"
	 * @generated
	 */
	EList<PhysicalSteps> getPhysicalSteps();

	/**
	 * Returns the value of the '<em><b>Related Dense Clock</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related Dense Clock</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related Dense Clock</em>' containment reference.
	 * @see #setRelatedDenseClock(Reference)
	 * @see fr.inria.aoste.trace.TracePackage#getPhysicalBase_RelatedDenseClock()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Reference getRelatedDenseClock();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.PhysicalBase#getRelatedDenseClock <em>Related Dense Clock</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Related Dense Clock</em>' containment reference.
	 * @see #getRelatedDenseClock()
	 * @generated
	 */
	void setRelatedDenseClock(Reference value);

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(double)
	 * @see fr.inria.aoste.trace.TracePackage#getPhysicalBase_Offset()
	 * @model
	 * @generated
	 */
	double getOffset();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.PhysicalBase#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(double value);

} // PhysicalBase
