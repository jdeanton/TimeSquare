/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.State#getReferedElement <em>Refered Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.trace.TracePackage#getState()
 * @model abstract="true"
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Refered Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refered Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refered Element</em>' reference.
	 * @see #setReferedElement(Reference)
	 * @see fr.inria.aoste.trace.TracePackage#getState_ReferedElement()
	 * @model required="true"
	 * @generated
	 */
	Reference getReferedElement();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.State#getReferedElement <em>Refered Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refered Element</em>' reference.
	 * @see #getReferedElement()
	 * @generated
	 */
	void setReferedElement(Reference value);

} // State
