/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assertion State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.AssertionState#isIsViolated <em>Is Violated</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.trace.TracePackage#getAssertionState()
 * @model
 * @generated
 */
public interface AssertionState extends State {
	/**
	 * Returns the value of the '<em><b>Is Violated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Violated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Violated</em>' attribute.
	 * @see #setIsViolated(boolean)
	 * @see fr.inria.aoste.trace.TracePackage#getAssertionState_IsViolated()
	 * @model
	 * @generated
	 */
	boolean isIsViolated();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.trace.AssertionState#isIsViolated <em>Is Violated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Violated</em>' attribute.
	 * @see #isIsViolated()
	 * @generated
	 */
	void setIsViolated(boolean value);

} // AssertionState
