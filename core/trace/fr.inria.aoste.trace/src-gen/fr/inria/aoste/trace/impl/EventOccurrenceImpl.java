/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.trace.impl;

import fr.inria.aoste.trace.EnableStateKind;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.TracePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Occurrence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.trace.impl.EventOccurrenceImpl#getContext <em>Context</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.EventOccurrenceImpl#isIsClockDead <em>Is Clock Dead</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.EventOccurrenceImpl#getCounter <em>Counter</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.EventOccurrenceImpl#getEState <em>EState</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.EventOccurrenceImpl#getFState <em>FState</em>}</li>
 *   <li>{@link fr.inria.aoste.trace.impl.EventOccurrenceImpl#isWasBorn <em>Was Born</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EventOccurrenceImpl extends StateImpl implements EventOccurrence {
	/**
	 * The cached value of the '{@link #getContext() <em>Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContext()
	 * @generated
	 * @ordered
	 */
	protected Reference context;

	/**
	 * The default value of the '{@link #isIsClockDead() <em>Is Clock Dead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsClockDead()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CLOCK_DEAD_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsClockDead() <em>Is Clock Dead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsClockDead()
	 * @generated
	 * @ordered
	 */
	protected boolean isClockDead = IS_CLOCK_DEAD_EDEFAULT;

	/**
	 * The default value of the '{@link #getCounter() <em>Counter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCounter()
	 * @generated
	 * @ordered
	 */
	protected static final int COUNTER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCounter() <em>Counter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCounter()
	 * @generated
	 * @ordered
	 */
	protected int counter = COUNTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getEState() <em>EState</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEState()
	 * @generated
	 * @ordered
	 */
	protected static final EnableStateKind ESTATE_EDEFAULT = EnableStateKind.TICK;

	/**
	 * The cached value of the '{@link #getEState() <em>EState</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEState()
	 * @generated
	 * @ordered
	 */
	protected EnableStateKind eState = ESTATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFState() <em>FState</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFState()
	 * @generated
	 * @ordered
	 */
	protected static final FiredStateKind FSTATE_EDEFAULT = FiredStateKind.TICK;

	/**
	 * The cached value of the '{@link #getFState() <em>FState</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFState()
	 * @generated
	 * @ordered
	 */
	protected FiredStateKind fState = FSTATE_EDEFAULT;

	/**
	 * The default value of the '{@link #isWasBorn() <em>Was Born</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWasBorn()
	 * @generated
	 * @ordered
	 */
	protected static final boolean WAS_BORN_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isWasBorn() <em>Was Born</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWasBorn()
	 * @generated
	 * @ordered
	 */
	protected boolean wasBorn = WAS_BORN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventOccurrenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TracePackage.Literals.EVENT_OCCURRENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference getContext() {
		if (context != null && context.eIsProxy()) {
			InternalEObject oldContext = (InternalEObject)context;
			context = (Reference)eResolveProxy(oldContext);
			if (context != oldContext) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TracePackage.EVENT_OCCURRENCE__CONTEXT, oldContext, context));
			}
		}
		return context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetContext() {
		return context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContext(Reference newContext) {
		Reference oldContext = context;
		context = newContext;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.EVENT_OCCURRENCE__CONTEXT, oldContext, context));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsClockDead() {
		return isClockDead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsClockDead(boolean newIsClockDead) {
		boolean oldIsClockDead = isClockDead;
		isClockDead = newIsClockDead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.EVENT_OCCURRENCE__IS_CLOCK_DEAD, oldIsClockDead, isClockDead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCounter() {
		return counter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCounter(int newCounter) {
		int oldCounter = counter;
		counter = newCounter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.EVENT_OCCURRENCE__COUNTER, oldCounter, counter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnableStateKind getEState() {
		return eState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEState(EnableStateKind newEState) {
		EnableStateKind oldEState = eState;
		eState = newEState == null ? ESTATE_EDEFAULT : newEState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.EVENT_OCCURRENCE__ESTATE, oldEState, eState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FiredStateKind getFState() {
		return fState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFState(FiredStateKind newFState) {
		FiredStateKind oldFState = fState;
		fState = newFState == null ? FSTATE_EDEFAULT : newFState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.EVENT_OCCURRENCE__FSTATE, oldFState, fState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isWasBorn() {
		return wasBorn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWasBorn(boolean newWasBorn) {
		boolean oldWasBorn = wasBorn;
		wasBorn = newWasBorn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TracePackage.EVENT_OCCURRENCE__WAS_BORN, oldWasBorn, wasBorn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TracePackage.EVENT_OCCURRENCE__CONTEXT:
				if (resolve) return getContext();
				return basicGetContext();
			case TracePackage.EVENT_OCCURRENCE__IS_CLOCK_DEAD:
				return isIsClockDead();
			case TracePackage.EVENT_OCCURRENCE__COUNTER:
				return getCounter();
			case TracePackage.EVENT_OCCURRENCE__ESTATE:
				return getEState();
			case TracePackage.EVENT_OCCURRENCE__FSTATE:
				return getFState();
			case TracePackage.EVENT_OCCURRENCE__WAS_BORN:
				return isWasBorn();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TracePackage.EVENT_OCCURRENCE__CONTEXT:
				setContext((Reference)newValue);
				return;
			case TracePackage.EVENT_OCCURRENCE__IS_CLOCK_DEAD:
				setIsClockDead((Boolean)newValue);
				return;
			case TracePackage.EVENT_OCCURRENCE__COUNTER:
				setCounter((Integer)newValue);
				return;
			case TracePackage.EVENT_OCCURRENCE__ESTATE:
				setEState((EnableStateKind)newValue);
				return;
			case TracePackage.EVENT_OCCURRENCE__FSTATE:
				setFState((FiredStateKind)newValue);
				return;
			case TracePackage.EVENT_OCCURRENCE__WAS_BORN:
				setWasBorn((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TracePackage.EVENT_OCCURRENCE__CONTEXT:
				setContext((Reference)null);
				return;
			case TracePackage.EVENT_OCCURRENCE__IS_CLOCK_DEAD:
				setIsClockDead(IS_CLOCK_DEAD_EDEFAULT);
				return;
			case TracePackage.EVENT_OCCURRENCE__COUNTER:
				setCounter(COUNTER_EDEFAULT);
				return;
			case TracePackage.EVENT_OCCURRENCE__ESTATE:
				setEState(ESTATE_EDEFAULT);
				return;
			case TracePackage.EVENT_OCCURRENCE__FSTATE:
				setFState(FSTATE_EDEFAULT);
				return;
			case TracePackage.EVENT_OCCURRENCE__WAS_BORN:
				setWasBorn(WAS_BORN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TracePackage.EVENT_OCCURRENCE__CONTEXT:
				return context != null;
			case TracePackage.EVENT_OCCURRENCE__IS_CLOCK_DEAD:
				return isClockDead != IS_CLOCK_DEAD_EDEFAULT;
			case TracePackage.EVENT_OCCURRENCE__COUNTER:
				return counter != COUNTER_EDEFAULT;
			case TracePackage.EVENT_OCCURRENCE__ESTATE:
				return eState != ESTATE_EDEFAULT;
			case TracePackage.EVENT_OCCURRENCE__FSTATE:
				return fState != FSTATE_EDEFAULT;
			case TracePackage.EVENT_OCCURRENCE__WAS_BORN:
				return wasBorn != WAS_BORN_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isClockDead: ");
		result.append(isClockDead);
		result.append(", counter: ");
		result.append(counter);
		result.append(", eState: ");
		result.append(eState);
		result.append(", fState: ");
		result.append(fState);
		result.append(", wasBorn: ");
		result.append(wasBorn);
		result.append(')');
		return result.toString();
	}

} //EventOccurrenceImpl
