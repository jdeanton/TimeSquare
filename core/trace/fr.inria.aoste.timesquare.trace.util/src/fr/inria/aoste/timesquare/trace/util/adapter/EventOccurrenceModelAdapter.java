/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util.adapter;

import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.timesquare.trace.util.TimeBase;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.Reference;

class EventOccurrenceModelAdapter implements IModelAdapter {
	static final IModelAdapter SINGLETON = new EventOccurrenceModelAdapter();
	
	private EventOccurrenceModelAdapter() {
		// SINGLETON
	}
	
	private static EObject getRef(EObject eo) {
		assert(eo instanceof EventOccurrence);
		return getRef((EventOccurrence)eo);
	}
	private static EObject getRef(EventOccurrence c) {
		Reference rf = c.getReferedElement();
		if (rf instanceof ModelElementReference)
			return  HelperFactory.getFirstReference((ModelElementReference) rf);
		return null;
	}

	@Override
	public boolean accept(EObject object) {
		return object instanceof EventOccurrence;
	}

	@Override
	public boolean isClock(EObject eo) {
		eo = getRef(eo);
		return AdapterRegistry.getAdapter(eo).isClock(eo);
	}

//	@Override
//	public boolean isHidden(EObject eo) {
//		eo = getRef(eo);
//		return AdapterRegistry.getAdapter(eo).isHidden(eo);
//	}
	
	@Override
	public String getName(EObject eo) {
		eo = getRef(eo);
		return AdapterRegistry.getAdapter(eo).getName(eo);
	}
	
	@Override
	public String getReferenceName(EObject clock) {
		clock = getRef(clock);
		return AdapterRegistry.getAdapter(clock).getReferenceName(clock);
	}

//	@Override
//	public String getAliasName(EObject clock) {
//		clock = getRef(clock);
//		return AdapterRegistry.getAdapter(clock).getAliasName(clock);
//	}

	@Override
	public String getUID(EObject clock) {
		clock = getRef(clock);
		return AdapterRegistry.getAdapter(clock).getUID(clock);
	}

	@Override
	public boolean isDiscrete(EObject clock) {
		clock = getRef(clock);
		return AdapterRegistry.getAdapter(clock).isDiscrete(clock);
	}

	@Override
	public List<TimeBase> getDiscretyzeByValue(EObject clock) {
		clock = getRef(clock);
		return AdapterRegistry.getAdapter(clock).getDiscretyzeByValue(clock);
	}

	@Override
	public HashMap<EObject, EObject> getDiscretyze(EObject clock) {
		clock = getRef(clock);
		return AdapterRegistry.getAdapter(clock).getDiscretyze(clock);
	}

	@Override
	public EventEnumerator getEventkind(EObject clock) {
		clock = getRef(clock);
		return AdapterRegistry.getAdapter(clock).getEventkind(clock);
	}

	@Override
	public List<EObject> fillWithReferencedElements(EObject reference,
			List<EObject> list) {
		return list;
	}

	@Override
	public String getQualifiedName(EObject eo) {
		//TODO ! I have no clue how to do this
		return  null;
	}

}
