/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter;
import fr.inria.aoste.trace.ModelElementReference;

public class QualifiedNameBuilder {

	/**
	 * Returns a textual representation of the object made by concatenating the names of all
	 * objects contained in the {@link ModelElementReference} mer. Names of each member of the
	 * ModelElementReference is obtained by calling the function {@link IModelAdapter#getName(EObject)}
	 * through the {@link AdapterRegistry} class.
	 */
	
	public static String buildQualifiedName(EObject eo, String separator) {
		if (eo == null) {
			return "__null_no_qualified_name__";
		}
		return AdapterRegistry.getAdapter(eo).getQualifiedName(eo);
	}
	
	public static String buildQualifiedName(ModelElementReference mer) {
		return buildQualifiedName(mer, "::");
	}

}
