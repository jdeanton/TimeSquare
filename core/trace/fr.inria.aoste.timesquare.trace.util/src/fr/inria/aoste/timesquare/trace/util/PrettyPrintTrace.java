/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util;

import java.util.ListIterator;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.NamedReference;
import fr.inria.aoste.trace.util.TraceSwitch;

/**
 * Idem to ReferenceNameBuilder except that it works with any Reference without knowing what the
 * concrete type actually is
 * 
 * @see ReferenceNameBuilder
 * @author fmallet
 *
 */
public class PrettyPrintTrace extends TraceSwitch<CharSequence> {
	private int nbLevel = 0;
	private String separator;
	/**
	 * By default the separator is :: and the number of Level is ALL
	 */
	public PrettyPrintTrace() {
		this("::", 1);
	}
	/**
	 * 
	 * @param separator character to be used between each level
	 * @param nbLevel hierarchy level at which we start (0: all the levels are shown, 1: all but the first level are shown, ...)
	 */
	public PrettyPrintTrace(String separator, int nbLevel) {
		this.separator = separator;
		this.nbLevel = nbLevel;
	}
	@Override
	public CharSequence caseNamedReference(NamedReference ref) {
		return ref.getValue();
	}
	/**
	 * Returns a textual representation of the object made by concatenating the names of all
	 * objects contained in the {@link ModelElementReference} mer. Names of each member of the
	 * ModelElementReference is obtained by calling the function {@link IModelAdapter#getName(EObject)}
	 * through the {@link AdapterRegistry} class.
	 */
	@Override
	public CharSequence caseModelElementReference(ModelElementReference mer) {
		StringBuilder sb = new StringBuilder();
		String sep="";
		ListIterator<EObject> iterator = mer.getElementRef().listIterator(nbLevel);
		while (iterator.hasNext()) {
			sb.append(sep);
			EObject o = iterator.next();
			sb.append(AdapterRegistry.getAdapter(o).getName(o));
			sep=separator;
		}
		return sb;
	}
	@Override
	public CharSequence doSwitch(EObject theEObject) {
		if (theEObject == null) {
			return "__null_no_qualified_name__";
		}
		
		return super.doSwitch(theEObject);
	}	
}
