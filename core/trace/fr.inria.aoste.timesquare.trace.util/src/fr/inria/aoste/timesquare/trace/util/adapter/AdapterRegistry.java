/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLHelperImpl;

import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;
import fr.inria.aoste.trace.ModelElementReference;

/***
 * 
 * @author fmallet, bferrero
 * 
 */
public class AdapterRegistry implements IExtensionManager {
	private static AdapterRegistry DEFAULT = null;

	private synchronized static AdapterRegistry getDefault() {
		if (DEFAULT == null)
			DEFAULT = new AdapterRegistry();
		return DEFAULT;
	}

	private AdapterRegistry() {
		ExtensionPointManager.findAllExtensions(this);
		// The ordering is essential
		adapters.add(ModelElementReferenceModelAdapter.SINGLETON); // accepts only ModelElementReference
		adapters.add(EventOccurrenceModelAdapter.SINGLETON); // accepts only EventOccurrences
		adapters.add(EObjectModelAdapter.SINGLETON); // accept only non-null references
		adapters.add(new DefaultModelAdapter());     // accept everything
	}

	@Override
	final public String getExtensionPointName() {
		return "fr.inria.aoste.timesquare.trace.util.adapter";
	}

	@Override
	final public String getPluginName() {
		return "fr.inria.aoste.timesquare.trace.util";
	}

	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable {
		Class<? extends IModelAdapter> tmp = ExtensionPointManager.getPointExtensionClass(ice, "adapter", IModelAdapter.class);
		if (tmp != null)
			addModelAdapter(tmp.newInstance());
	}

	static public IModelAdapter getAdapter(EObject r) {
		return getDefault().findAdapter(r);
	}

	private ArrayList<IModelAdapter> adapters = new ArrayList<IModelAdapter>();

	private void addModelAdapter(IModelAdapter adapter) {
		System.out.println("adding Adapter : " + adapter.getClass().getName());
		adapters.add(0, adapter);
	}

	private IModelAdapter findAdapter(EObject r) 
	{		
		for (IModelAdapter model : adapters) 
		{
			if (model.accept(r))
				return model;
		}
		throw new RuntimeException("Cannot happen since DefaultModelAdapter accept everyone");
	}

	
//	public String getReferenceName(EObject r) {
//		if (r instanceof NamedReference)
//		{
//			return ((NamedReference) r).getValue();
//		}
//		return findAdapter(r).getReferenceName(r);
//	}

//	public String getAliasName(EObject r) {
//		if (r instanceof NamedReference)
//		{
//			return "_"+((NamedReference) r).getValue();
//		}
//		return findAdapter(r).getAliasName(r);
//	}

	static public HashMap<EObject, EObject> getDiscrete(ModelElementReference mer) {
		EObject o = HelperFactory.getLastReference(mer);
		return getDefault().findAdapter(o).getDiscretyze(o);
	}

//	public final String getUID(EObject r) {
//		return findAdapter(r).getUID(r);
//	}

	public static  final String getHREF(EObject eo) {
		try {
			if (eo==null)
				return null;
			Resource rs = eo.eResource();
			// Resource rs2 = eo.eResource();

			XMLHelper xmlhelper = new XMLHelperImpl();
			HashMap<String, Object> map = new HashMap<String, Object>();
			URIHandlerImpl urih = new URIHandlerImpl();
			urih.setBaseURI(rs.getURI());
			map.put(XMLResource.OPTION_URI_HANDLER, urih);
			xmlhelper.setOptions(map);
			// System.out.println(eo.eResource());
			String s = xmlhelper.getHREF(eo);
			if (s != null) {
				int i = s.indexOf("#")+1;
				if (i == -1) {
					i = 0;
				}
				return s.substring(i);
			}

		} catch (Throwable e) {
			System.err.println(e.getStackTrace()[1] + " : " + e.getMessage());
		}
		return "";
	}
}
