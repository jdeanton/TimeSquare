/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.DiscretizedClockStep;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.InterDiscretizedSteps;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.PhysicalBase;
import fr.inria.aoste.trace.PhysicalSteps;

public class HelperPhysicalBase {

	PhysicalBase ph;

	public HelperPhysicalBase(PhysicalBase ph) {
		super();
		this.ph = ph;
	}
	
	public boolean isFixed(LogicalStep step)
	{
		return HelperFactory.isFixed(ph, step);
	}
	
	DiscretizedClockStep dcs=null;
	InterDiscretizedSteps ids=null;
	public PhysicalSteps createPhysicalSteps(LogicalStep steps)
	{
		//PhysicalSteps ps= null;
		EventOccurrence e=HelperFactory.getEventOccurence(steps,ph);
		if (e !=null)
		{
			if (e.getFState()== FiredStateKind.TICK)
			{
				DiscretizedClockStep ps= HelperFactory.createDiscretizedClockStep(steps);
				ph.getPhysicalSteps().add(ps);
				dcs =ps;
				if (ids!=null)
					ids.setNextFixStep(ps);
				ids=null;
				EObject  eo=((ModelElementReference)ph.getRelatedDenseClock()).getElementRef().get(0);
				List<TimeBase> listValues = AdapterRegistry.getAdapter(eo).getDiscretyzeByValue(eo); 
				double resolution = 0.0;
				if(listValues.size() != 0)
				{
				 	resolution =listValues.get(0).getBase();
				}
				if ( resolution!=0.0)
				{
					ps.setTimestamp( (1.0d*e.getCounter()*resolution));
				}
				else
				{
					ps.setTimestamp(e.getCounter());
				}			
				return ps;
			}
		}	
		
		if (ids==null)
		{
			ids= HelperFactory.createInterDiscretizedSteps(steps);
			ids.setPreviousFixStep(dcs);
			ph.getPhysicalSteps().add(ids);		
		}
		else
		{
			ids.getCorrespondingLogicalSteps().add(steps);
		}
		return ids;
	}
	
	
	public int getMaxInterval()
	{
		int n=0;
		for( PhysicalSteps ps : ph.getPhysicalSteps())
		{
			if (ps instanceof InterDiscretizedSteps)
			{
				int  m =((InterDiscretizedSteps) ps).getCorrespondingLogicalSteps().size();
				if (m> n)
					n=m;
			}
		}	
		return n;
	}
	
}
