/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.NamedReference;
import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.Trace;

public class SearchEventOccurence {

	
	public final Trace getTrace() {
		return trace;
	}

	protected final void setTrace(Trace trace) {
		this.trace = trace;
	}

	public SearchEventOccurence(Trace trace) {
		super();
		this.trace = trace;
	}

	public SearchEventOccurence() {
		super();
		// TODO Auto-generated constructor stub
	}

	private Trace trace;

	
	/****
	 * 
	 * @param o : identifier of clock ( String of NamedElement or Eobject of CCSLModelElementReference
	 * @param tickn : numero du tick recherche
	 * @return ClockState correspontant
	 */
	public EventOccurrence findClockState(Object o, int tickn)
	{
		if ( trace==null)
			return null;
		Reference r=null;
		if (o instanceof String )
		{
			r= findStringReference ((String) o);
			//return findStringClockState((String) o,tickn);
		}
		if (o instanceof EObject )
		{
			if (o instanceof Reference)
			{
				r=(Reference) o;
			}
			else
			{
			r= findEobjectReference((EObject) o  );
			}
		}
	
		if ( r!=null)
			return findClockState(r,tickn );
		return null;
	}
	
	private Reference  findStringReference(String o)
	{
		//Reference r=null;
		for (Reference r :trace.getReferences())
		{
			if ( r instanceof NamedReference)
			{
				NamedReference re= (NamedReference) r ;
				String str=re.getValue();
				if (str!=null && str.compareTo(o)==0)
				{
					return r;
				}
			}
		}
		return null;
	}
	
	
	private Reference  findEobjectReference(EObject  o)
	{
		//Reference r=null;
		for (Reference rf :trace.getReferences())
		{			
			if ( rf instanceof ModelElementReference)
			{
				ModelElementReference re= (ModelElementReference) rf ;					
				if (re.getElementRef().get(0)==o)
				{
					return rf ;
				}				
			}	
		}
		return null;
	}
	
	private EventOccurrence findClockState(Reference rf, int tickn)
	{
		for( LogicalStep st: trace.getLogicalSteps())
		{
			for( EventOccurrence cs: st.getEventOccurrences())
			{
				if ( rf== cs.getReferedElement())
				{
						if (cs.getCounter()==tickn )
						{
							if (cs.getFState() == FiredStateKind.TICK   )//instanceof Ticks)  
							{
								return cs;
							}
						}
					}
					
				}
			}	
		return null;
	}
	
	/*private EventOccurence findStringClockState(String o, int tickn)
	{
		for( LogicalStep st: trace.getLogicalSteps())
		{
			for( EventOccurence cs: st.getEventOccurences())
			{
				Reference rf= cs.getReferedElement();
				if ( rf instanceof NamedReference)
				{
					NamedReference re= (NamedReference) rf ;
					String str=re.getValue();
					if (str!=null && str.compareTo(o)==0)
					{
						if (cs.getCounter()==tickn )
						{
							if (cs.getFState() == FiredStateKind.TICK   )//instanceof Ticks)  
							{
								return cs;
							}
						}
					}
					
				}
			}
			
		}
		
		return null;
	}
	
	private EventOccurence findEObjectClockState(EObject o, int tickn)
	{
		for( LogicalStep st: trace.getLogicalSteps())
		{
			for( EventOccurence cs: st.getEventOccurences())
			{
				Reference rf= cs.getReferedElement();
				if ( rf instanceof ModelElementReference)
				{
					ModelElementReference re= (ModelElementReference) rf ;					
					if (re.getElementRef()==o)
					{
						if (cs.getCounter()==tickn )
							if (cs.getFState() ==  FiredStateKind.TICK) //instanceof Ticks)  
							{
								return cs;
							}
					}
					
				}
			}
			
		}
		
		return null;
	}*/
}
