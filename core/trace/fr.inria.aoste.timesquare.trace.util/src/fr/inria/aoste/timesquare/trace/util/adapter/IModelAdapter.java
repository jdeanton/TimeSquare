/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util.adapter;

import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.TimeBase;

/**
 * ModelAdapter offers a layer to access information of Ecore models
 * independent of the actual kind of EObject
 * Does not know whether there is ccslkernel.model or something else
 * The AdapterRegistry is a registry that deals with all the possible
 * IModelAdapters 
 *
 * @author bferrero
 * @see DefaultModelAdapter, EObjectModelAdapter for default realizations
 */
public interface IModelAdapter {
	public enum EventEnumerator
	{

		UNDEFINED(0, "undefined"),	
		START(1, "start" ), 		
		FINISH(2, "finish"),		
		SEND(3, "send"), 
		RECEIVE(4, "receive"), 
		CONSUME(6, "consume"), 
		PRODUCE(5, "produce");

		private final int value;

		private final String name;

		private EventEnumerator(int value, String name) {
			this.value = value;
			this.name = name;
		}

		public int getValue() {
			return value;
		}

		public String getName() {
			return name;
		}



	}

	/**
	 * This methods decides whether it can accept to deal with the given EObject.
	 * For a given EObject, the AdapterRegistry will ask each registered ModelAdapter if it is willing to deal with the object
	 * The first to accept (it depends on the registration ordering) is selected.
	 * 
	 * @see AdapterRegistry
	 * 
	 * @param object The Eobject on which the user wants to get information
	 * @return true iif the ModelAdapter accepts to give Model information on that kind of EObject
	 */
	boolean accept(EObject object);	

	/**
	 * 
	 * @param eo
	 * @return 
	 *  
	 *  isClock (x ) == false ==> isTimer(x) ==false 
	 */
	public boolean isClock(EObject eo);
	
	public String getName(EObject eo);
	
	public String getReferenceName(EObject eo);
	
	/**
	 * return a QualifiedNamed of the more meaningfull entity (either eo or its referencedElement)
	 * @param eo
	 * @return
	 */
	public String getQualifiedName(EObject eo);

	/**
	 * Fill the "list" with the set of elements referenced by "reference".
	 * The reference may itself reference other elements, the list is filled with "leaf elements": EObject in the model
	 * This is backend-dependent. See concrete implementations for details
	 * 
	 * @param reference The reference to parse
	 * @param list A list to fill, or null if the list should be created
	 * @return the list containing the referenced elements
	 */
	public List<EObject> fillWithReferencedElements(EObject reference, List<EObject> list);	

//	/***
//	 * Gives an Alias Name for a clock. This method is used in the VCD BackEnd generator to replace the 
//	 * internally generated name of clocks with a more readable name
//	 * The reason why the static type is not Clock is that this layer (plugin) does not have a direct dependency on
//	 * ccslkernel.model. Concrete classes choose the concrete type they want to represent clocks.
//	 * 
//	 * @param clock The EObject from which we want to get an alias, in most cases this should be a Clock
//	 * @return The Alias to be used instead of the generated UniqueName.
//	 */
//	public String getAliasName(EObject clock);

	public String getUID(EObject eo) ;
	/**** discretizedBy  ***/


	/***
	  indique si une clock donn�e est discretise<br>
	 * example :<BR>
	 * pour le system<BR>
	 *  {<BR>
		cc = idealclk discretizedBy 0.001;<BR>	 
	 	mc = idealclk discretizedBy 0.05;<BR>
	 	mc = idealclk2 discretizedBy 0.01;<BR>	 	
	 	c2 = idealclk2 discretizedBy 0.01;<BR>
 		c2b = idealclk2 discretizedBy 0.005;<BR> 		
 		a = b filteredBy 0b11(0101001);<BR>
 	}<BR>	
	 * INPUT :OUTPUT<BR> 	 	 
		cc,c2, c2b,mc : true;<BR>
		a, b , idealclk , idealclk2 : false;<BR>

	 */	
	public boolean isDiscrete(EObject clock);

	/**
	 * @return pour une clock donne  les valeurs (Model ) de disctretisation 
	 * 
	 *  * pour le system<BR>
	 *  {<BR>
		cc = idealclk discretizedBy 0.001;<BR>	 
	 	mc = idealclk discretizedBy 0.05;<BR>
	 	mc = idealclk2 discretizedBy 0.01;<BR>	 	
	 	c2 = idealclk2 discretizedBy 0.01;<BR>
 		c2b = idealclk2 discretizedBy 0.005;<BR> 		
 		a = b filteredBy 0b11(0101001);<BR>
 	}<BR>
	 * INPUT :OUTPUT<BR> 		 	
	 *  cc  :{ 0.0001}<BR>
	 *  mc : {0.05 , 0.01}<BR> 
	 *  c2 : {0.01}<BR>
	 *  c2b :{ 0.005}<BR>
	 *  a : {}<BR>
	 */
	public List<TimeBase> getDiscretyzeByValue(EObject clock);

	/**
	 * @param obejct du model
	 * @return les listes des couples present dans le modele
	  <BR>
	 *example :<BR>
	 * pour<BR>
	 *  {<BR>
		cc = idealclk discretizedBy 0.001;<BR>	 
	 	mc = idealclk discretizedBy 0.05;<BR>
	 	mc = idealclk2 discretizedBy 0.01;<BR>	 	
	 	c2 = idealclk2 discretizedBy 0.01;<BR>
 		c2b = idealclk2 discretizedBy 0.005;<BR> 		
 		a = b filteredBy 0b11(0101001);<BR>
 	}<BR>
 	return :<BR>
	 * 	 {{idealclk , 0.001},	 
	 	 {idealclk , 0.05},
	 	 {idealclk2 , 0.01},	 	
	 	 {idealclk2 , 0.01},
 		 {idealclk2 , 0.005} }
	 */
	public HashMap<EObject, EObject> getDiscretyze(EObject r);   

	public EventEnumerator getEventkind(EObject r);
}
