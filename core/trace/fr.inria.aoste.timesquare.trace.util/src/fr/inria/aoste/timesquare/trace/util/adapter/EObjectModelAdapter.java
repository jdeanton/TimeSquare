/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util.adapter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * ModelAdapter offers a layer to access information of Ecore models
 * independent of the actual kind of EObject
 * 
 * EObjectModelAdapter gives a default behavior for non-null EObject
 * 
 * @author fmallet
 *
 */
class EObjectModelAdapter extends DefaultModelAdapter {
	static final IModelAdapter SINGLETON = new EObjectModelAdapter();
	
	private EObjectModelAdapter() {
		// SINGLETON
	}
	@Override
	public boolean accept(EObject r) {
		return r!=null;
	}

	@Override
	public String getReferenceName(EObject r) {
		EStructuralFeature esf = r.eClass().getEStructuralFeature("name");
		if (esf != null) {
			//String name = r.eClass().getName();
			Object s = r.eGet(esf);
			if ((s instanceof String) && ((String) s).length() != 0) {
				return /*name+" "+*/(String) s;
			}
		}
		return r.toString();
	}
	
	@Override
	public String getQualifiedName(EObject eo) {
		String res=getReferenceName(eo);
		if(eo.eContainer()!=null){
			res = getQualifiedName(eo.eContainer()) +"::"+res;
		}
		return res;
	}

//	@Override
//	public String getAliasName(EObject r) {
//		return r.toString();
//	}
}
