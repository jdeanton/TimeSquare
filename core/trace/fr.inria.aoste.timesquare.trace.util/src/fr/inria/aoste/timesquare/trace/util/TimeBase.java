/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util;

public class TimeBase {

	public enum StandardUnit { seconde ;}
	
	
	public TimeBase() {
		super();
		this.base = 1.0d;	
		this.standardUnit=StandardUnit.seconde;
		this.unitname="";//this.standardUnit.toString();
	}

	public TimeBase(double base, String unitname) {
		super();
		this.base = base;
		this.unitname = unitname;
		this.standardUnit=null;
	}

	public TimeBase(double base) {
		super();
		this.base = base;	
		this.standardUnit=StandardUnit.seconde;
		this.unitname="";//this.standardUnit.toString();
	}

	
	public TimeBase(double base, StandardUnit standardUnit) {
		super();
		this.base = base;
		if ( standardUnit!=null)
			this.standardUnit = standardUnit;
		else
			this.standardUnit=StandardUnit.seconde;
		this.unitname="";//this.standardUnit.toString();
	}


	final private double base;
	final private String unitname;
	final private StandardUnit standardUnit;
	
	public double getBase() {
		return base;
	}

	public String getUnitname() {
		return unitname;
	}

	public StandardUnit getStandardUnit() {
		return standardUnit;
	}
	
	public boolean isStandardUnit() {
		return standardUnit!=null;
	}
	
	
}
