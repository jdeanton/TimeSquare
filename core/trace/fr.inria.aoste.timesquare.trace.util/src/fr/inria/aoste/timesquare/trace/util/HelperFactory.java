/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.trace.DiscretizedClockStep;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.InterDiscretizedSteps;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.NamedReference;
import fr.inria.aoste.trace.PhysicalBase;
import fr.inria.aoste.trace.PhysicalSteps;
import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.TraceFactory;

public class HelperFactory {

	
	
	static public ModelElementReference createModelElementReference(EObject eo) {
		ModelElementReference modelElementReference = TraceFactory.eINSTANCE.createModelElementReference();
		modelElementReference.getElementRef().add(eo);
		return modelElementReference;
	}

	


	static public NamedReference createNamedReference(String s) {
		NamedReference namedReference = TraceFactory.eINSTANCE.createNamedReference();
		namedReference.setValue(s);
		return namedReference;
	}	
	
	static public PhysicalBase createPhysicalBase(EObject eo) {
		PhysicalBase physicalBase = TraceFactory.eINSTANCE.createPhysicalBase();		
		physicalBase.setRelatedDenseClock(createModelElementReference(eo));
		return physicalBase;
		
	}

	
	static public DiscretizedClockStep createDiscretizedClockStep(LogicalStep step) {
		DiscretizedClockStep discretizedClockStep = TraceFactory.eINSTANCE.createDiscretizedClockStep();
		discretizedClockStep.setCorrespondingLogicalStep(step);
		return discretizedClockStep;
	}

	

	
	static public InterDiscretizedSteps createInterDiscretizedSteps(LogicalStep step) {
		InterDiscretizedSteps interDiscretizedSteps = TraceFactory.eINSTANCE.createInterDiscretizedSteps();
		interDiscretizedSteps.getCorrespondingLogicalSteps().add(step);
		return interDiscretizedSteps;
	}

	
	static public EventOccurrence getEventOccurence(LogicalStep ls,  PhysicalBase  pb)
	{
		Reference o = (Reference)((ModelElementReference) pb.getRelatedDenseClock()).getElementRef().get(0);
		if (o ==null)
			return null;
	/*	if (o instanceof NamedReference )
		{
			o =((NamedReference) o).getValue();
		}
		if ( o instanceof ModelElementReference)
		{
			o =((ModelElementReference) o).getElementRef();
		}*/
	
		for (EventOccurrence e :ls.getEventOccurrences())
		{
			Reference r=	e.getReferedElement();
			if (r==o)
			{
				return e;
			}			
		}		
		
		return null;
		
	}
	
	static public EventOccurrence getEventOccurence(LogicalStep ls,  Reference  o)
	{
		
		if (o ==null)
			return null;
	/*	if (o instanceof NamedReference )
		{
			o =((NamedReference) o).getValue();
		}
		if ( o instanceof ModelElementReference)
		{
			o =((ModelElementReference) o).getElementRef();
		}*/
	
		for (EventOccurrence e :ls.getEventOccurrences())
		{
			Reference r=	e.getReferedElement();
			if (r==o)
			{
				return e;
			}			
		}		
		
		return null;
		
	}
	
	static public int getMaxInterval(PhysicalBase ph)
	{
		int n=0;
		for( PhysicalSteps ps : ph.getPhysicalSteps())
		{
			if (ps instanceof InterDiscretizedSteps)
			{
				int  m =((InterDiscretizedSteps) ps).getCorrespondingLogicalSteps().size();
				if (m> n)
					n=m;
			}
		}	
		return n;
	}
	
	static public boolean isFixed(PhysicalBase pb,  LogicalStep step)
	{
		EventOccurrence e=HelperFactory.getEventOccurence(step,pb);
		
		//EventOccurrence e=HelperFactory.getEventOccurence(step, pb.getRelatedDenseClock());
		if (e !=null)
		{
			if (e.getFState()== FiredStateKind.TICK)
			{
				return true;
			}
		}
		return false;
	}
	
	
	static public EObject getFirstReference(ModelElementReference rf)
	{
		if (rf==null)
			return null;
		return rf.getElementRef().get(0);
	}
	
	static public EObject getLastReference(ModelElementReference rf)
	{
		if (rf == null)
			return null;
		return rf.getElementRef().get(rf.getElementRef().size() - 1);
	}
	
}
