/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util.adapter;

import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.timesquare.trace.util.TimeBase;
import fr.inria.aoste.trace.ModelElementReference;

/**
 * This ModelAdapter only deals with ModelElementReference.
 * Most of the time it browses through the elements referenced by the ModelElementReference and 
 * delegates the actual operation to other adapters dedicated to the concrete object the actual job.
 * 
 * @author fmallet
 *
 */
class ModelElementReferenceModelAdapter extends DefaultModelAdapter {
	static final ModelElementReferenceModelAdapter SINGLETON = new ModelElementReferenceModelAdapter();

	private ModelElementReferenceModelAdapter() {
		// SINGLETON
	}

	@Override
	public boolean accept(EObject r) {
		return r instanceof ModelElementReference;
	}

//	@Override
//	public String getAliasName(EObject r) {
//		return getAliasName((ModelElementReference)r);
//	}
//
//	private String getAliasName(ModelElementReference mer) {
//		if (mer.getElementRef().size() == 1) {
//			EObject reference = HelperFactory.getFirstReference(mer);
//			return AdapterRegistry.getAdapter(reference).getAliasName(reference);
//		} else {
//			StringBuilder sb = new StringBuilder("");
//			String prefix = "";
//			for (EObject er : mer.getElementRef()) {
//				sb.append(prefix);
//				sb.append(AdapterRegistry.getAdapter(er).getAliasName(er));
//				prefix = "::";
//			}
//			return sb.toString();
//		}
//	}

	@Override
	public String getUID(EObject eo) {
		return getUID((ModelElementReference)eo);
	}

	final private String getUID(ModelElementReference mer) {
		if (mer.getElementRef().size() == 1) {
			EObject reference = HelperFactory.getFirstReference(mer);
			return AdapterRegistry.getAdapter(reference).getUID(reference);
		} else {
			StringBuilder sb = new StringBuilder();
			String prefix = "{";
			for (EObject er : mer.getElementRef()) {
				sb.append(prefix);
				sb.append( AdapterRegistry.getAdapter(er).getUID(er));
				prefix=", ";
			}
			sb.append("}");
			return sb.toString();
		}
	}

	@Override
	public boolean isClock(EObject eo) {
		eo = HelperFactory.getLastReference((ModelElementReference) eo);
		return AdapterRegistry.getAdapter(eo).isClock(eo);
	}

//	@Override
//	public boolean isHidden(EObject eo) {
//		eo = HelperFactory.getLastReference((ModelElementReference) eo);
//		return AdapterRegistry.getAdapter(eo).isHidden(eo);
//	}

	@Override
	public String getReferenceName(EObject clock) {
		return getReferenceName((ModelElementReference)clock);
	}
	private String getReferenceName(ModelElementReference mer) {
		StringBuilder sb = new StringBuilder("");
		String prefix="";
		for (EObject er : mer.getElementRef()) {
			sb.append(prefix);
			sb.append(AdapterRegistry.getAdapter(er).getReferenceName(er));
			prefix = "::";
		}
		return sb.toString();
	}
	
	@Override
	public String getQualifiedName(EObject eo) {
		EObject ref = HelperFactory.getLastReference((ModelElementReference)eo);
		return AdapterRegistry.getAdapter(ref).getQualifiedName(ref);
	}

	@Override
	public List<EObject> fillWithReferencedElements(EObject reference,
			List<EObject> list) {
		return fillWithReferencedElements((ModelElementReference)reference, list);
	}
	private List<EObject> fillWithReferencedElements(ModelElementReference reference, List<EObject> list) {
		for(EObject o : reference.getElementRef()) {
			list = AdapterRegistry.getAdapter(o).fillWithReferencedElements(o, list);
		}
		return super.fillWithReferencedElements(reference, list);
	}

	@Override
	public boolean isDiscrete(EObject clock) {
		throw new RuntimeException("Operation not supported: isDiscrete not applicable to ModelElementReference:"+clock);
	}

	@Override
	public List<TimeBase> getDiscretyzeByValue(EObject clock) {
		clock = HelperFactory.getLastReference((ModelElementReference) clock);
		return AdapterRegistry.getAdapter(clock).getDiscretyzeByValue(clock);
	}

	@Override
	public HashMap<EObject, EObject> getDiscretyze(EObject r) {
		throw new RuntimeException("Operation not supported: getDiscretyze not applicable to ModelElementReference:"+r);
	}

	@Override
	public EventEnumerator getEventkind(EObject r) {
		throw new RuntimeException("Operation not supported: getEventKind not applicable to ModelElementReference:"+r);
	}
}
