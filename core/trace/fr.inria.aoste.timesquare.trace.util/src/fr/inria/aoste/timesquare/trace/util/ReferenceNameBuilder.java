/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter;
import fr.inria.aoste.trace.ModelElementReference;

public class ReferenceNameBuilder {

	/**
	 * Returns a textual representation of the object made by concatenating the names of all
	 * objects contained in the {@link ModelElementReference} mer. Names of each member of the
	 * ModelElementReference is obtained by calling the function {@link IModelAdapter#getName(EObject)}
	 * through the {@link AdapterRegistry} class.
	 */
	
	public static String buildQualifiedName(ModelElementReference mer, String separator) {
		if (mer == null) {
			return "__null_no_qualified_name__";
		}
		StringBuilder sb = new StringBuilder("");
		Iterator<EObject> it = mer.getElementRef().iterator();
		while (it.hasNext()) {
			EObject er = it.next();
			sb.append(AdapterRegistry.getAdapter(er).getName(er));
			if (it.hasNext()) {
				sb.append(separator);
			}
		}
		return sb.toString();
	}
	
	public static String buildQualifiedName(ModelElementReference mer) {
		return buildQualifiedName(mer, "::");
	}

}
