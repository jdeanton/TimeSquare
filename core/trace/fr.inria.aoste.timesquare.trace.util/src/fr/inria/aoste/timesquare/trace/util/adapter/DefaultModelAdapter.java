/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.trace.util.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.TimeBase;

/**
 * ModelAdapter offers a layer to access information of Ecore models
 * independent of the actual kind of EObject
 * 
 * DefaultModelAdapter gives a default behavior for all EObject including null references
 * 
 * @author fmallet
 */
public class DefaultModelAdapter implements IModelAdapter {
	@Override
	public boolean accept(EObject r) {
		return true;
	}
	
	@Override
	public String getName(EObject eo) {
		return eo.toString();
	}
	
	@Override
	public String getReferenceName(EObject r) {
		return "null__no__name";
	}

//	@Override
//	public String getAliasName(EObject r) {
//		return "null__no__name";
//	}
	
	@Override
	public String getUID(EObject eo) {
		return AdapterRegistry.getHREF(eo);
	}

//	@Override
//	public boolean isHidden(EObject r) {
//		return true;
//	}

	@Override
	public List<TimeBase> getDiscretyzeByValue(EObject r) {
		return new ArrayList<TimeBase>();
	}

	@Override
	public boolean isDiscrete(EObject r) {
		return false;
	}

	@Override
	public boolean isClock(EObject eo) {
		return false;
	}

	@Override
	public HashMap<EObject, EObject> getDiscretyze(EObject r) {
		return new HashMap<EObject, EObject>();
	}

	@Override
	public EventEnumerator getEventkind(EObject r) {
		return EventEnumerator.UNDEFINED;
	}

	@Override
	public List<EObject> fillWithReferencedElements(EObject reference,
			List<EObject> list) {
		if (list != null) return list;
		return new ArrayList<EObject>();
	}

	@Override
	public String getQualifiedName(EObject eo) {
		return "null__no__qualified__name";
	}
}
