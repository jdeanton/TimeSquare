/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.behaviors;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Permission;
import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IMethod;

import fr.inria.aoste.timesquare.backend.codeexecution.Activator;
import fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionHelper;
import fr.inria.aoste.timesquare.backend.codeexecution.ICodeExecutionAPI;
import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.Variable;
import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.trace.ModelElementReference;

public class CodeExecutionBehavior implements Behavior {

	/*
	 * When the behavior is built using the GUI, the field _method is used and
	 * set using the JDT object selected in the GUI, and the _methodName field
	 * is null. Conversely, when the behavior is built from a loaded
	 * .codeexecution model the field _methodName is used to hold the simplename
	 * of the method (non qualified name) and _method is null.
	 */
	protected IMethod _method;
	protected String _methodName;
	protected String _className;
	public ConfigurationHelper _confHelper = null;
	protected CodeExecutionHelper ce = null;
	protected ClassLoader newLoader = null;
	protected Object o = null;
	protected Method m;
	protected Variable variable = null;

	public final CodeExecutionHelper getCe() {
		return ce;
	}

	public final void setCe(CodeExecutionHelper ce) {
		this.ce = ce;

		if (variable != null) {
			variable.setCodeExecutionHelper(ce);
			o = variable.getObject();

		}
		applyo();
	}

	public String getClassName() {
		return _className;
	}

	public IStatus createStatusError(String message, Throwable throwable) {
		IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, message, throwable);
		Activator.getDefault().getLog().log(status);
		return status;
	}

	private void applyo() {
		if (o == null && ce != null) {
			try {
				if (variable != null) {
					variable.setCodeExecutionHelper(ce);
					o = variable.getObject();
				}
			} catch (Throwable e) {
				createStatusError("newInstance fail", e);
				e.printStackTrace();

			}
		}
		if (o instanceof ICodeExecutionAPI) {
			try {
				((ICodeExecutionAPI) o).setHelper(ce);
			} catch (Throwable e) {
				createStatusError("ICodeExecutionAPI.setHelper fail ", e);

			}
		}
	}

	public CodeExecutionBehavior(Variable v, IMethod method, ConfigurationHelper helper) {
		_confHelper = helper;
		try {
			setVariable(v);
			this._methodName = null;
			this._method = method;
			this._className = _method.getCompilationUnit().findPrimaryType()
					.getFullyQualifiedParameterizedName();
			applyo();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public CodeExecutionBehavior(Variable v, String methodName, ConfigurationHelper helper) {
		_confHelper = helper;
		try {
			setVariable(v);
			this._method = null;
			this._methodName = methodName;
			this._className = v.getTypeName();
			applyo();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static class ExitException extends SecurityException {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1460964248465204534L;
		public final int status;

		public ExitException(int status) {
			super("There is no escape!");
			this.status = status;
		}
	}

	public static class NoExitSecurityManager extends SecurityManager {

		public Thread t = null;

		public NoExitSecurityManager(Thread thread) {
			super();
			t = thread;

		}

		public void checkPermission(Permission perm, Object context) {

			// super.checkPermission(perm, context);
		}

		public void checkPermission(Permission perm) {

			// super.checkPermission(perm);
		}

		@Override
		public void checkExit(int status) {
			if (Thread.currentThread() == t) {
				super.checkExit(status);
				throw new ExitException(status);
			}
		}
	}

	public void execute(ArrayList<ModelElementReference> mersToForce) {

		SecurityManager security = System.getSecurityManager();

		System.setSecurityManager(new NoExitSecurityManager(Thread.currentThread()));
		try {
			if (_className == null) {
				return;
			}
			try {
				if (m == null) {
					m = o.getClass().getDeclaredMethod(
							(_method != null ? _method.getElementName() : _methodName));
				}
					// it is possible to get the result here !
					Object res = m.invoke(o);
					if (res instanceof Boolean){
						Boolean boolRes = (Boolean)res;
						if (boolRes.booleanValue()){
							for(ModelElementReference mer : mersToForce){
								ce.getSolver().forceClockPresence(mer);
							}
						}else{
							for(ModelElementReference mer : mersToForce){
								ce.getSolver().forceClockAbsence(mer);
							}
						}
					//	System.out.println("res of "+m+ " is "+res);
					}

			} catch (Throwable e) {
				int n = -1;
				if (e instanceof InvocationTargetException) {
					System.err.println("Code executor exception");
					if (e.getCause() != null) {
						 e.printStackTrace();
						n = e.getStackTrace().length;
						e = e.getCause();

					}
				}
				if (n != -1) {
					StackTraceElement[] st = e.getStackTrace();
					int x = st.length - n;
					if (x > 0) {
						StackTraceElement ste[] = new StackTraceElement[x];
						for (int i = 0; i < x; i++) {
							ste[i] = st[i];
						}
						e.setStackTrace(ste);
					}

				}
				if (e instanceof ExitException) {
					ce.printlnError(
							"==> Operation Illegal on method "
									+ (_method != null ? _method.getElementName() : _methodName), e);
				} else {
					ce.printlnError(
							"Error on method "
									+ (_method != null ? _method.getElementName() : _methodName), e);
				}
				createStatusError("Method fail ", e);
			}
		} catch (Throwable e) {
			System.err.println("Error : !! " + e);
			createStatusError("Error ", e);
		} finally {
			if (security != null)
				System.setSecurityManager(security);
		}
	}

	public boolean behaviorEquals(Behavior behavior) {
		if (behavior == this) {
			return true;
		}
		if (behavior == null) {
			return false;
		}
		if (behavior.getClass() != this.getClass()) {
			return false;
		}
		try {
			CodeExecutionBehavior ceb = (CodeExecutionBehavior) behavior;
			String itsMethodName = null;
			String itsClassName = null;
			if (ceb.getMethod() != null && this.getMethod() != null) {
				itsMethodName = ceb.getMethod().getElementName();
				itsClassName = ceb.getMethod().getCompilationUnit().findPrimaryType()
						.getFullyQualifiedParameterizedName();
			} else {
				itsMethodName = ceb.getMethodName();
				itsClassName = ceb.getClassName();
			}

			if (_method.getElementName().equals(itsMethodName) && _className.equals(itsClassName)) {
				if (variable == null && ceb.variable == null)
					return true;
				if (variable != null && variable.equals(ceb.variable))
					return true;
			}
		} catch (Throwable e) {
			e.printStackTrace();
			createStatusError("behaviorEquals fail ", e);
		}
		return false;
	}

	public IMethod getMethod() {
		return _method;
	}

	public void setMethod(IMethod method) {
		_method = method;
	}

	public String getMethodName() {
		return _methodName;
	}

	public void setMethodName(String _methodName) {
		this._methodName = _methodName;
	}

	public final String getDescription() {
		String name = getClass().getSimpleName();

		StringBuilder sb = new StringBuilder();
		for (char c : name.toCharArray()) {

			if (Character.isUpperCase(c))
				sb.append(" ");
			sb.append(c);
		}
		sb.append(" : ");
		sb.append(_className).append(" ");
		if (_method != null) {
			sb.append(_method.getElementName());
		} else {
			sb.append(_methodName);
		}
		return sb.toString();
	}

	public void finish() {
		if (o instanceof ICodeExecutionAPI) {
			try {
				((ICodeExecutionAPI) o).finish();
			} catch (Throwable e) {
				createStatusError("ICodeExecutionAPI.setHelper fail ", e);

			}
		}
	}

	public Variable getVariable() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}

}
