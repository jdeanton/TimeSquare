/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.gui;

import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import fr.inria.aoste.timesquare.backend.codeexecution.Activator;
import fr.inria.aoste.timesquare.backend.codeexecution.manager.CodeExecutionManager;
import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.Variable;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockActivationState;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.GUIHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.node.Node;
import fr.inria.aoste.timesquare.utils.ui.widgets.FileSelectionControl;

public class CodeExecutionGUI extends BehaviorManagerGUI {

	/** all String are public constant */
	public static final String WIDGET_CODEEXECUTOR_SELECT_RELATION = "codeexecutor.select.relation";
	public static final String WIDGET_CODEEXECUTOR_SELECT_CLOCK_ACTIVATION_STATE = "codeexecutor.select.clockActivationState";
	public static final String WIDGET_CODEEXECUTOR_SELECT_ASSERT = "codeexecutor.select.assert";
	public static final String WIDGET_CODEEXECUTOR_SELECT_CLOCK = "codeexecutor.select.clock";
	public static final String WIDGET_CODEEXECUTOR_SELECT_CLOCK_TO_FORCE = "codeexecutor.select.clocksToForce";
	
	private final class ComboTypeSelectionAdapter extends SelectionAdapter {
		public void widgetSelected(SelectionEvent arg0) {
			int s = c.getSelectionIndex();
			if (s == 0) {
				_compositeVariableMethod.setVisible(true);
				_compositeClock.setVisible(true);
				_compositeRelation.setVisible(false);
				_compositeAssert.setVisible(false);
				_compositeModel.setVisible(false);
			}
			else if (s == 1) {
				_compositeVariableMethod.setVisible(true);
				_compositeClock.setVisible(false);
				_compositeRelation.setVisible(true);
				_compositeAssert.setVisible(false);
				_compositeModel.setVisible(false);				
			}
			else if (s == 2) {
				_compositeVariableMethod.setVisible(true);
				_compositeClock.setVisible(false);
				_compositeRelation.setVisible(false);
				_compositeAssert.setVisible(true);
				_compositeModel.setVisible(false);
			}
			else if (s == 3) {
				_compositeVariableMethod.setVisible(false);
				_compositeClock.setVisible(false);
				_compositeRelation.setVisible(false);
				_compositeAssert.setVisible(false);
				_compositeModel.setVisible(true);				
			}
			updateOKStatus();
		}
	}

	Composite _composite = null;

	private IMethod _method = null;// IType
	private ClockActivationState _clockActivationState = null;

	private Tree _treeClock = null;
	private Tree _treeClockToForce = null;

	private boolean[] _assertState = null;
	private Combo _comboAssert = null;

	private Composite _compositeVariableMethod = null;
	private Composite _compositeClock = null;
	private Composite _compositeAssert = null;
	private Composite _compositeRelation = null;
	private Composite _compositeModel = null;
	
	private Tree _treeRelation = null;
	private Combo c = null;
	private IStatus fCurrStatus = Status.OK_STATUS;
	private Button ok;
	private Combo comboVariable;
	private Label label;
	private Combo comboMethod;

	private FileSelectionControl codeexecModelBrowser;

	public CodeExecutionGUI() {
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public Control createDialogArea(Composite composite) {
		if (composite == null) {
			return null;
		}

		_composite = new Composite(composite, SWT.FILL | SWT.RESIZE);
		composite.setLayout(new GridLayout());
		_composite.setLayoutData(new GridData(GridData.FILL_VERTICAL | GridData.FILL_HORIZONTAL));
		_composite.setLayout(new FormLayout());

		{
			c = new Combo(_composite, SWT.FILL_WINDING | SWT.READ_ONLY);
			{
				FormData layoutData = new FormData();
				layoutData.left = new FormAttachment(0, 6);
				layoutData.right = new FormAttachment(100, -6);
				layoutData.top = new FormAttachment(0, 6);
				c.setLayoutData(layoutData);
			}
			c.setLayout(new RowLayout());
			c.add("Clock Behavior");
			c.add("Relation Behavior");
			c.add("Assert Behavior");
			c.add("Load from Model");
			c.select(0);
		}

		displayCodeExecModelChoice(_composite);
		
		Group group2 = new Group(_composite, SWT.FILL_WINDING | SWT.FILL);
		group2.setLayout(new FormLayout());
		{
			{
				FormData layoutData = new FormData();
				layoutData.left = new FormAttachment(0, 6);
				layoutData.right = new FormAttachment(100, -6);
				layoutData.top = new FormAttachment(c, 6);
				group2.setLayoutData(layoutData);
			}
			label = new Label(group2, SWT.NONE);
			FormData fd_label = new FormData();
			fd_label.top = new FormAttachment(0, 3);
			fd_label.left = new FormAttachment(0, 3);
			label.setLayoutData(fd_label);
			label.setText("Variable:");
			label.pack();

			comboVariable = new Combo(group2, SWT.NONE);
			updateVariable();
			comboVariable.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					updateMethods();

				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					updateMethods();

				}
			});
			ok = new Button(group2, SWT.NONE);
			ok.setText("Add Var");
			ok.addSelectionListener(new CodeSelectionAdapter(this));
			Label lblNewLabel = new Label(group2, SWT.NONE);
			lblNewLabel.setText("Method:");
			comboMethod = new Combo(group2, SWT.NONE);
			updateMethods();
			comboMethod.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					selectMethod();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					selectMethod();

				}

			});
			{
				FormData fd_combo_1 = new FormData();
				fd_combo_1.top = new FormAttachment(0, 3);
				fd_combo_1.left = new FormAttachment(0, 70);
				fd_combo_1.right = new FormAttachment(ok, -6);
				comboVariable.setLayoutData(fd_combo_1);
			}

			{
				FormData fd_ok = new FormData();
				fd_ok.right = new FormAttachment(100, -10);
				fd_ok.width = 100;
				fd_ok.top = new FormAttachment(0, 3);

				ok.setLayoutData(fd_ok);
			}

			{
				FormData fd_lblNewLabel = new FormData();
				fd_lblNewLabel.bottom = new FormAttachment(100, -4);
				fd_lblNewLabel.top = new FormAttachment(label, 18);
				fd_lblNewLabel.left = new FormAttachment(label, 0, SWT.LEFT);

				lblNewLabel.setLayoutData(fd_lblNewLabel);
			}

			{
				FormData fd_combo = new FormData();
				fd_combo.right = new FormAttachment(ok, -6);
				fd_combo.left = new FormAttachment(comboVariable, 0, SWT.LEFT);
				fd_combo.top = new FormAttachment(label, 18);
				fd_combo.bottom = new FormAttachment(100, -4);
				comboMethod.setLayoutData(fd_combo);
			}
			_compositeVariableMethod = group2;
		}

		Group group3 = new Group(_composite, SWT.FILL_WINDING | SWT.FILL);
		{
			group3.setLayout(new FormLayout());
			{
				FormData layoutData = new FormData();
				layoutData.top = new FormAttachment(group2, 7);
				layoutData.bottom = new FormAttachment(100, -6);
				layoutData.left = new FormAttachment(0, 6);
				layoutData.right = new FormAttachment(100, -6);
				group3.setLayoutData(layoutData);
			}
			displayRelationChoice(group3);
			displayClockChoice(group3);
			displayAssertChoice(group3);
			group3.setSize(400, 350);
		}

		c.addSelectionListener(new ComboTypeSelectionAdapter());
		/* The default selection of the combo c is 0, set the visibility of the GUI elements
		 * according to this default choice.
		 */
		_compositeVariableMethod.setVisible(true);
		_compositeClock.setVisible(true);
		_compositeRelation.setVisible(false);
		_compositeAssert.setVisible(false);
		_compositeModel.setVisible(false);

		return composite;
	}

	public void okPressed() {
//		if (CodeExecutionManager.loadBehaviorsFromModel) {
//			addBehaviorsFromModel();
//			return;
//		}
		if (_compositeClock.isVisible()) {
			addClockBehavior();
			return;
		}
		if (_compositeRelation.isVisible()) {
			addRelationBehavior();
			return;
		}
		if (_compositeAssert.isVisible()) {
			addAssertBehavior();
		}
		if (_compositeModel.isVisible()) {
			addBehaviorsFromModel();
		}
	}

	@SuppressWarnings("unchecked")
	public void addClockBehavior() {
		ClockEntity ce = null;
		if (_treeClock != null) {
			ce = ((Node<ClockEntity>) _treeClock
					.getData(GUIHelper.COMBOSELECTION)).getValue();
		}
		TreeItem[] treeItemClocksToForce = null;
		ArrayList<ClockEntity> clocksToForce= new ArrayList<ClockEntity>();
		if (_treeClockToForce != null) {
			treeItemClocksToForce =  _treeClockToForce
					.getSelection();
		}
		for(int i=0; i<treeItemClocksToForce.length; i++){
			clocksToForce.add(((Node<ClockEntity>) (treeItemClocksToForce[i]).getData()).getValue());
		}
		
		if (_method != null && ce != null && _clockActivationState != null) {
			getBehaviorManager().addBehavior(var, _method, ce,
					_clockActivationState, clocksToForce);
		}
	}

	@SuppressWarnings("unchecked")
	public void addRelationBehavior() {
		RelationEntity ce = null;
		if (_treeRelation != null) {
			ce = ((Node<RelationEntity>) _treeRelation
					.getData(GUIHelper.COMBOSELECTION)).getValue();

		}
		if (_method != null && ce != null) {
			getBehaviorManager().addBehavior(var, _method, ce);
		}
	}

	public void addAssertBehavior() {
		ClockEntity ae = null;
		if (_comboAssert != null) {
			ae = (ClockEntity) _comboAssert.getData(GUIHelper.COMBOSELECTION);
		}
		if (_method != null && _assertState != null) {
			getBehaviorManager().addBehavior(var, _method, ae, _assertState);
		}
	}

	public void addBehaviorsFromModel() {
		if (codeexecModelBrowser != null) {
			getBehaviorManager().addBehaviorsFromModel(codeexecModelBrowser.getSelectedFile());
		}
	}

	@Override
	protected CodeExecutionManager getBehaviorManager() {

		return (CodeExecutionManager) super.getBehaviorManager();
	}

	@Override
	public void setBehaviorManager(BehaviorManager behaviorManager) {
		if (behaviorManager instanceof CodeExecutionManager)
			super.setBehaviorManager(behaviorManager);
	}

	public void cancelPressed() {
	}

	private Composite displayCodeExecModelChoice(Composite parent) {

		Group group1 = new Group(parent, SWT.FILL | SWT.FILL_WINDING);
		// parent has a FormLayout manager: associate a FormData to its son.
		{
			FormData layoutData = new FormData();
			layoutData.left = new FormAttachment(0, 6);
			layoutData.right = new FormAttachment(100, -6);
			layoutData.top = new FormAttachment(c, 6);
			group1.setLayoutData(layoutData);
		}
		// A File SelectionControl is (from the SWT point of view a group with a layoutData which is
		// a gridData, so lets set a GridLayout to be the manager for group1.
		group1.setLayout(new GridLayout(1, true));

		String[] extensions = { "codeexecution" };
		codeexecModelBrowser = new FileSelectionControl(group1, "Code Execution Model",
				extensions);

		String modelFile = this.getBehaviorManager().getCodeExecModelFilename();
		if (modelFile != null) {
			codeexecModelBrowser.setSelectedFile(modelFile);
		}
		_compositeModel = group1;
		return group1;
	}
	
	private void displayClockChoice(Composite composite) {
		_compositeClock = new Composite(composite, SWT.FILL_WINDING | SWT.RESIZE);
		_compositeClock.setLocation(10, 10);
		_compositeClock.setLayout(new FormLayout());
		_treeClock = getGUIHelper().displayTreeClock(_compositeClock,
				WIDGET_CODEEXECUTOR_SELECT_CLOCK, SWT.SINGLE).getTree();
		{
			FormData datatree = new FormData();
			datatree.top = new FormAttachment(0, 3);
			datatree.left = new FormAttachment(0, 3);
			datatree.right = new FormAttachment(100, -800);
			datatree.bottom = new FormAttachment(100, -6);
			_treeClock.setLayoutData(datatree);
		}
		
		_treeClockToForce = getGUIHelper().displayTreeClock(_compositeClock,
				WIDGET_CODEEXECUTOR_SELECT_CLOCK_TO_FORCE, SWT.MULTI).getTree();
		{
			FormData datatreeToForce = new FormData();
			datatreeToForce.top = new FormAttachment(0, 3);
			datatreeToForce.left = new FormAttachment(0, 550);
			datatreeToForce.right = new FormAttachment(100, -300);
			datatreeToForce.bottom = new FormAttachment(100, -6);
			_treeClockToForce.setLayoutData(datatreeToForce);
		}
		
		FormData data = new FormData();
		_clockActivationState = getGUIHelper().displayClockState(_compositeClock, data,
				WIDGET_CODEEXECUTOR_SELECT_CLOCK_ACTIVATION_STATE);
		{
			data.top = new FormAttachment(0, 3);
			data.right = new FormAttachment(100, -3);
			data.left = new FormAttachment(100, -290);
			data.bottom = new FormAttachment(100, -6);
			data.height = 250;
		}
		{
			FormData layoutData = new FormData();
			layoutData.bottom = new FormAttachment(100, -6);
			layoutData.left = new FormAttachment(0, 6);
			layoutData.right = new FormAttachment(100, -6);
			layoutData.top = new FormAttachment(0, 6);
			_compositeClock.setLayoutData(layoutData);
		}
		_compositeClock.pack();
	}

	private void displayAssertChoice(Composite composite) {
		_compositeAssert = new Composite(composite, SWT.FILL_WINDING | SWT.RESIZE);
		_compositeAssert.setLocation(10, 10);
		_compositeAssert.setLayout(new GridLayout(1, true));

		_comboAssert = getGUIHelper().displayList(_compositeAssert,
				getConfigurationHelper().getAssert(), WIDGET_CODEEXECUTOR_SELECT_ASSERT);
		_comboAssert.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		_assertState = getGUIHelper().displayAssertStateCheckBox(_compositeAssert,
				new GridData(SWT.FILL, SWT.NONE, true, false));

		{
			FormData layoutData = new FormData();
			layoutData.bottom = new FormAttachment(100, -6);
			layoutData.left = new FormAttachment(0, 6);
			layoutData.right = new FormAttachment(100, -6);
			layoutData.top = new FormAttachment(0, 6);
			_compositeAssert.setLayoutData(layoutData);
		}
		_compositeAssert.pack();
	}

	private void displayRelationChoice(Composite composite) {
		_compositeRelation = new Composite(composite, SWT.FILL_WINDING | SWT.RESIZE);
		_compositeRelation.setLocation(10, 10);
		_compositeRelation.setLayout(new FormLayout());
		_treeRelation = getGUIHelper().displayTreeRelation(_compositeRelation,
				WIDGET_CODEEXECUTOR_SELECT_RELATION).getTree();

		FormData _layoutData = new FormData();
		_layoutData.bottom = new FormAttachment(100, -6);
		_layoutData.left = new FormAttachment(0, 6);
		_layoutData.right = new FormAttachment(100, -6);
		_layoutData.top = new FormAttachment(0, 6);
		_layoutData.height = 250;
		_treeRelation.setLayoutData(_layoutData);
		_treeRelation.pack();

		{
			FormData layoutData = new FormData();
			layoutData.bottom = new FormAttachment(100, -6);
			layoutData.left = new FormAttachment(0, 6);
			layoutData.right = new FormAttachment(100, -6);
			layoutData.top = new FormAttachment(0, 6);
			_compositeRelation.setLayoutData(layoutData);
		}
		_compositeRelation.pack();

	}

	@Override
	public Point getMinimumSize() {
		return new Point(450, 350);
	}

	@Override
	public void updateOKStatus() {
//		if (CodeExecutionManager.loadBehaviorsFromModel) {
//			String filename = codeexecModelBrowser.getSelectedFile();
//			if (filename != null) {
//				fCurrStatus = Status.OK_STATUS;
//			}
//			return;
//		}
		try {
			fCurrStatus = Status.OK_STATUS;
			if (_compositeModel.isVisible()) {
//				String filename = codeexecModelBrowser.getSelectedFile();
//				if (filename == null || filename.isEmpty()) {
//					fCurrStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.ERROR,
//							"no model file", null);
//				}
				return;
			}
			if (_method == null) {
				fCurrStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.ERROR,
						"no code", null);
				return;
			}

			if (_compositeClock.isVisible()) {
				if (!(_treeClock.getData(GUIHelper.COMBOSELECTION) instanceof Node<?>)) {
					fCurrStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.ERROR,
							"no clock Selected", null);
					return;
				}
				if (((Node<?>) _treeClock.getData(GUIHelper.COMBOSELECTION))
						.getValue() == null)
					fCurrStatus = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID, IStatus.ERROR,
							"no clock Selected", null);
				return;
			}
			if (_compositeRelation.isVisible()) {
				if (!(_treeRelation.getData(GUIHelper.COMBOSELECTION) instanceof Node<?>)) {
					fCurrStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.ERROR,
							"no relation Selected", null);
					return;
				}

				if (((Node<?>) _treeRelation.getData(GUIHelper.COMBOSELECTION)).getValue() == null)
					fCurrStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.ERROR,
							"no relation selected", null);
				return;
			}
			if (_compositeAssert.isVisible()) {

				return;
			}

		} catch (Exception e) {
			System.err.println(e);
			fCurrStatus = new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.ERROR,
					"updateOKStatus() fail:" + e.getMessage(), null);
		} finally {
			if (dialog != null)
				dialog.updateStatus(fCurrStatus);
		}
	}

	public void updateVariable() {
		comboVariable.removeAll();
		for (Variable v : getBehaviorManager().getListVariable()) {
			comboVariable.add(v.name + " [" + v.getTypeName() + "]");
		}

	}

	private Variable var = null;

	public void updateMethods() {
		comboMethod.removeAll();
		int i = comboVariable.getSelectionIndex();
		if (i == -1)
			var = null;
		else
			var = getBehaviorManager().getListVariable().get(i);

		if (var != null) {
			for (IMethod v : var.getIMethods()) {
				try {
					String name = v.getElementName();
					// v.getParameters()[0].getTypeSignature()
					// + v.getNameRange().getLength();
					comboMethod.add(name);
				} catch (Throwable e) {
					// silently ignore the failure
					//TODO: from JD: WHY ?
				}
			}
		} else {
			_method = null;
		}
	}

	public void selectMethod() {
		try {
			int i = comboMethod.getSelectionIndex();
			if (i == -1 && var != null)
				_method = null;
			else
				_method = var.getIMethods().get(i);
		} catch (Exception e) {
			_method = null;
		}
	}

	public void selectVariable(Variable v) {
		int n = getBehaviorManager().getListVariable().indexOf(v);
		if (n != -1)
			comboVariable.select(n);

	}
}
