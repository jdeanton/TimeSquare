/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.gui;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.ui.model.WorkbenchContentProvider;

public class JavaWorkbenchContentProvider extends WorkbenchContentProvider {

	public Object[] getElements(Object element) {
		Object o[] = super.getElements(element).clone();
		try {
			int n = 0;
			ArrayList<IAdaptable> ijp = new ArrayList<IAdaptable>();
			for (n = 0; n < o.length; n++) {
				if (o[n] instanceof IProject) {
					IJavaProject no = JavaCore.create((IProject) o[n]);
					if (no != null) {
						try {
							ijp.add(no);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

			}
			return ijp.toArray();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return o;
	}

}