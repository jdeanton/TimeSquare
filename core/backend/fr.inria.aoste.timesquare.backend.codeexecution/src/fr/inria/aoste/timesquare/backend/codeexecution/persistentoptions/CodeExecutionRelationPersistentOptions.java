/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;

import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionRelationBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class CodeExecutionRelationPersistentOptions extends
		CodeExecutionPersistentOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6273371445044787973L;

	public CodeExecutionRelationPersistentOptions() {
		super();

	}

	public CodeExecutionRelationPersistentOptions(
			CodeExecutionBehavior codeExecutionBehavior) {
		super(codeExecutionBehavior);

	}

	public CodeExecutionRelationBehavior redoRelationBehavior(ConfigurationHelper confHelper) {
		IJavaElement javaElement = getJavaElementFromString(_serializedMethod);
		if (javaElement instanceof IMethod) {
			IMethod method = (IMethod) javaElement;
			return new CodeExecutionRelationBehavior(variable, method,confHelper);
		}
		return null;

	}

}
