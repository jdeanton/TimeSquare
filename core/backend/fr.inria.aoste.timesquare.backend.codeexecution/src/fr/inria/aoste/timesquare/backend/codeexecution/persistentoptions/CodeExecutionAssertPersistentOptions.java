/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;

import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class CodeExecutionAssertPersistentOptions extends
		CodeExecutionPersistentOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8037372232310765249L;

	public CodeExecutionAssertPersistentOptions() {
		super();

	}

	public CodeExecutionAssertPersistentOptions(
			CodeExecutionBehavior codeExecutionBehavior) {
		super(codeExecutionBehavior);
	}


	public CodeExecutionClockBehavior redoAssertBehavior(ConfigurationHelper confHelper) {
		if (_serializedMethod != null) {
			IJavaElement javaElement = getJavaElementFromString(_serializedMethod);
			if (javaElement instanceof IMethod) {
				IMethod method = (IMethod) javaElement;
				return new CodeExecutionClockBehavior(variable, method,null,confHelper);
			}

		}
		else if (_methodName != null) {
			return new CodeExecutionClockBehavior(variable, _methodName,null,confHelper);
		}
		return null;

	}

}
