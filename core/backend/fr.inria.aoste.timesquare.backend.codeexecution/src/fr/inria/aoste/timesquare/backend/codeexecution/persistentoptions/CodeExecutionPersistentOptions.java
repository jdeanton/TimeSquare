/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions;

import java.util.ArrayList;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaCore;

import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.trace.util.ReferenceNameBuilder;
import fr.inria.aoste.trace.ModelElementReference;

public abstract class CodeExecutionPersistentOptions implements
		PersistentOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8658086849972634369L;
	
	/* When the behavior is built using the GUI, the _serializedMethod field is initialised from the
	 * IJavaElement selected, and is a string that reflects the JDT hierarchy in the workspace.
	 * When the behavior is built from a Xtext .codeexecution model, the field _methodName is used
	 * with the simple name of the method (non qualified name) and _serializedMethod is null.
	 */
	protected String _serializedMethod;
	protected String _methodName;
	protected Variable variable;
	protected ArrayList<String> clocksQualifiedNameToForce;
	public CodeExecutionPersistentOptions() {
		super();

	}

	public CodeExecutionPersistentOptions(
			CodeExecutionBehavior codeExecutionBehavior) {
		if (codeExecutionBehavior != null) {
			if (codeExecutionBehavior.getMethod() != null) {
				_serializedMethod = getStringFromJavaElement(codeExecutionBehavior.getMethod());
				_methodName = null;
			}
			else {
				_methodName = codeExecutionBehavior.getMethodName();
				_serializedMethod = null;
			}
			variable = codeExecutionBehavior.getVariable();
			clocksQualifiedNameToForce = new ArrayList<String>();
			if (codeExecutionBehavior instanceof CodeExecutionClockBehavior){
				String separator = "::";
				for (ClockEntity ce : ((CodeExecutionClockBehavior) codeExecutionBehavior).getClocksToForce()) {
					ModelElementReference mer = ce.getModelElementReference();
					String qualifiedName = ReferenceNameBuilder.buildQualifiedName(mer, separator);
					clocksQualifiedNameToForce.add(qualifiedName);
				}
			}
		}
	}

	public String getDescription() {
		return null;
	}

	protected IJavaElement getJavaElementFromString(String s) {
		try {
			return JavaCore.create(s);
		} catch (Throwable t) {
			return null;
		}
	}

	protected String getStringFromJavaElement(IJavaElement javaElement) {
		if (javaElement != null) {
			String ret = ((javaElement).getHandleIdentifier());
			return ret;
		}
		return null;
	}

	public Variable getVariable() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}

}
