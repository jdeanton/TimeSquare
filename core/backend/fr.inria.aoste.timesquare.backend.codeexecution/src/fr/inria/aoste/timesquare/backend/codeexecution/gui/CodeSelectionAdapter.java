/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IType;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import fr.inria.aoste.timesquare.backend.codeexecution.Activator;
import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.Variable;

public final class CodeSelectionAdapter extends SelectionAdapter {

	private static final Status STATUS = new Status(IStatus.OK,
			Activator.PLUGIN_ID, IStatus.OK, "", null);

	private static final Status STATUSBadname = new Status(IStatus.ERROR,
			PlatformUI.PLUGIN_ID, IStatus.ERROR, "bad name", null);

	private static final Status STATUSEmpty = new Status(IStatus.ERROR,
			PlatformUI.PLUGIN_ID, IStatus.ERROR, "Class not found", null);

	private final class VariableTreeSelectionDialog extends
			SelectionStatusDialog {

		public final class TextSelectionListener implements SelectionListener,
				ModifyListener {
			@Override
			public void widgetSelected(SelectionEvent e) {
				name = text.getText();
				updateOKStatus();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				name = text.getText();
				updateOKStatus();
			}

			@Override
			public void modifyText(ModifyEvent e) {
				name = text.getText();
				updateOKStatus();

			}
		}

		public final class TreeSelectionChangedListener implements
				ISelectionChangedListener {
			public void selectionChanged(SelectionChangedEvent event) {
				accesssetResult(((IStructuredSelection) event.getSelection())
						.toList());
				updateOKStatus();
			}
		}

		public final class TreeDoubleClick implements IDoubleClickListener {
			public void doubleClick(DoubleClickEvent event) {
				updateOKStatus();

				ISelection selection = event.getSelection();
				if (selection instanceof IStructuredSelection) {
					Object item = ((IStructuredSelection) selection)
							.getFirstElement();
					if (fViewer.getExpandedState(item)) {
						fViewer.collapseToLevel(item, 1);
					} else {
						fViewer.expandToLevel(item, 1);
					}
				}

			}
		}

		private TreeViewer fViewer;

		private ILabelProvider fLabelProvider;

		private ITreeContentProvider fContentProvider;

		private ISelectionStatusValidator fValidator = null;

		private IStatus fCurrStatus = STATUS;

		private List<ViewerFilter> fFilters = new ArrayList<ViewerFilter>();

		private Object fInput;

		private boolean fIsEmpty;

		private String name = null;

		private Text text = null;

		public VariableTreeSelectionDialog(Shell parent) {
			super(parent);
			fLabelProvider = new WorkbenchLabelProvider();
			fContentProvider = new JavaWorkbenchContentProvider();
			setResult(Collections.emptyList());
			setStatusLineAboveButtons(true);
		}

		public void addFilter(ViewerFilter filter) {
			fFilters.add(filter);
		}

		public void setValidator(ISelectionStatusValidator validator) {
			fValidator = validator;
		}

		public void setInput(Object input) {
			fInput = input;
		}

		protected void updateOKStatus() {
			if (!fIsEmpty) {
				if (fValidator != null) {
					fCurrStatus = fValidator.validate(getResult());
					updateStatus(fCurrStatus);
				} else {
					if (name != null && name.length() != 0) {
						fCurrStatus = STATUS;
					} else {
						fCurrStatus = STATUSBadname;
					}

				}
			} else {
				fCurrStatus = STATUSEmpty;
			}
			updateStatus(fCurrStatus);
		}

		public int open() {
			fIsEmpty = evaluateIfTreeEmpty(fInput);
			super.open();
			return getReturnCode();
		}

		private void access$superCreate() {
			super.create();
		}

		protected void cancelPressed() {
			setResult(null);
			super.cancelPressed();
		}

		protected void computeResult() {
			setResult(((IStructuredSelection) fViewer.getSelection()).toList());
		}

		public void create() {
			BusyIndicator.showWhile(null, new Runnable() {
				public void run() {
					access$superCreate();
					fViewer.setSelection(new StructuredSelection(
							getInitialElementSelections()), true);
					updateOKStatus();
				}
			});
		}

		/*
		 * @see Dialog#createDialogArea(Composite)
		 */
		protected Control createDialogArea(Composite parent) {
			Composite composite = (Composite) super.createDialogArea(parent);

			composite.setLayout(new FormLayout());
			Label messageLabel = createMessageArea(composite);
			{
				FormData data = new FormData();

				data.left = new FormAttachment(0, 6);
				data.right = new FormAttachment(100, -6);
				data.top = new FormAttachment(0, 6);
				messageLabel.setLayoutData(data);
			}
			messageLabel.setText("Name :");
			text = new Text(composite, SWT.FILL | SWT.PUSH);
			text.setEditable(true);
			text.addModifyListener(new TextSelectionListener());
			{
				FormData data = new FormData();

				data.left = new FormAttachment(0, 6);
				data.right = new FormAttachment(100, -6);
				data.top = new FormAttachment(messageLabel, 6);
				text.setLayoutData(data);
			}
			TreeViewer treeViewer = createTreeViewer(composite);
			Tree treeWidget = treeViewer.getTree();

			{
				FormData data = new FormData();
				data.bottom = new FormAttachment(100, -6);
				data.left = new FormAttachment(0, 6);
				data.right = new FormAttachment(100, -6);
				data.top = new FormAttachment(text, 6);
				treeWidget.setLayoutData(data);
			}
			treeWidget.setFont(parent.getFont());

			if (fIsEmpty) {
				messageLabel.setEnabled(false);
				treeWidget.setEnabled(false);
			}

			return composite;
		}

		/**
		 * Creates and initializes the tree viewer.
		 * 
		 * @param parent
		 *            the parent composite
		 * @return the tree viewer
		 * @see #doCreateTreeViewer(Composite, int)
		 */
		protected TreeViewer createTreeViewer(Composite parent) {
			int style = SWT.BORDER | SWT.SINGLE;
			fViewer = new TreeViewer(new Tree(parent, style));
			fViewer.setContentProvider(fContentProvider);
			fViewer.setLabelProvider(fLabelProvider);
			fViewer.addSelectionChangedListener(new TreeSelectionChangedListener());
			for (int i = 0; i != fFilters.size(); i++) {
				fViewer.addFilter(fFilters.get(i));
			}
			fViewer.addDoubleClickListener(new TreeDoubleClick());
			fViewer.setInput(fInput);
			return fViewer;
		}

		private boolean evaluateIfTreeEmpty(Object input) {
			Object[] elements = fContentProvider.getElements(input);
			if (elements.length > 0) {
				if (fFilters != null) {
					for (int i = 0; i < fFilters.size(); i++) {
						ViewerFilter curr = fFilters.get(i);
						elements = curr.filter(fViewer, input, elements);
					}
				}
			}
			return elements.length == 0;
		}

		protected void accesssetResult(List<?> result) {
			super.setResult(result);
		}

		protected void handleShellCloseEvent() {
			setResult(null);
			super.handleShellCloseEvent();
		}

		public Variable getResultVariable() {
			Variable v = null;
			IType t = null;
			Object[] results = getResult();
			if (results != null) {
				if (results.length != 0) {
					if (results[0] instanceof IType) {
						t = (IType) results[0];
						v = codeExecutionGUI.getBehaviorManager()
								.createVariable(name, t);
					}
				}
			}
			return v;
		}

	}

	/**
	 * 
	 */
	private final CodeExecutionGUI codeExecutionGUI;

	/**
	 * @param codeExecutionGUI
	 */
	public CodeSelectionAdapter(CodeExecutionGUI codeExecutionGUI) {
		this.codeExecutionGUI = codeExecutionGUI;
	}

	public void widgetSelected(SelectionEvent arg0) {
		VariableTreeSelectionDialog dialog = new VariableTreeSelectionDialog(
				codeExecutionGUI._composite.getShell());

		dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
		dialog.setValidator(new FileValidator());
		dialog.addFilter(new DialogClassFilter());
		int r = dialog.open();

		if (r == Window.OK) {
			Variable v = dialog.getResultVariable();
			if (v != null) {
				codeExecutionGUI.updateVariable();
				codeExecutionGUI.selectVariable(v);
				codeExecutionGUI.updateMethods();
			}
		}
		this.codeExecutionGUI.updateOKStatus();
	}
}