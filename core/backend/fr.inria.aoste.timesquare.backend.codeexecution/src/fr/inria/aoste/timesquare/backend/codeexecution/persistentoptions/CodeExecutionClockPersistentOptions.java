/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions;

import java.util.ArrayList;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;

import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class CodeExecutionClockPersistentOptions extends
		CodeExecutionPersistentOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1316251036865805290L;
	
	public CodeExecutionClockPersistentOptions() {
		super();

	}

	public CodeExecutionClockPersistentOptions(
			CodeExecutionBehavior codeExecutionBehavior) {
		super(codeExecutionBehavior);

	}


	public CodeExecutionClockBehavior redoClockBehavior(ConfigurationHelper confHelper) {
		ArrayList<ClockEntity> clocksToForce = new ArrayList<ClockEntity>();
		
		if (_serializedMethod != null) {
			IJavaElement javaElement = getJavaElementFromString(_serializedMethod);
		  if (javaElement instanceof IMethod) {
			for(String clockQualifiedName: this.clocksQualifiedNameToForce){
				ClockEntity ce = getClock(confHelper, clockQualifiedName);
				clocksToForce.add(ce);
			}
			IMethod method = (IMethod) javaElement;
			return new CodeExecutionClockBehavior(variable, method,clocksToForce,confHelper);
		  }

		}
		else if (_methodName != null) {
			for(String clockQualifiedName: this.clocksQualifiedNameToForce){
				ClockEntity ce = getClock(confHelper, clockQualifiedName);
				clocksToForce.add(ce);
			}
			return new CodeExecutionClockBehavior(variable, _methodName,clocksToForce,confHelper);
		}
		return null;

	}

	/**
	 * getClock method<BR>
	 * This method returns the ClockEntity associated with the behavior passed
	 * by the user in parameter. The ClockEntity object and the behavior object
	 * must have the same name (as returned by the {@link ClockEntity#getName()} and
	 * {@link VCDGeneratorClockBehavior#getName()} calls on both objects)<BR>
	 * This is needed as the ClockEntity class may not be serialized as part of
	 * the VCDGeneratorPersistentOptions class.
	 * 
	 * @param helper
	 *            The ConfigurationHelper of the behavior manager
	 * @param behavior
	 *            The target behavior on which we require to get the clock.
	 * @return The ClockEntity associated with the <i>behavior</i> parameter.
	 */
	private static ClockEntity getClock(ConfigurationHelper helper, String clockName) {

		for (ClockEntity clock : helper.getClocks()) {
			if (clock.getName().compareTo(clockName) == 0)
				return clock;
		}
		return null;
	}
	
}
