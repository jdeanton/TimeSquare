/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.behaviors;

import org.eclipse.jdt.core.IMethod;

import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.Variable;
import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationHelper;

public class CodeExecutionRelationBehavior extends CodeExecutionBehavior
		implements RelationBehavior {

	public CodeExecutionRelationBehavior(Variable var, IMethod method,ConfigurationHelper helper) {
		super(var, method,helper);
	}

	public CodeExecutionRelationBehavior(Variable var, String methodName,ConfigurationHelper helper) {
		super(var, methodName,helper);
	}

	public void run(RelationHelper helper) {
		super.execute(null);
	}

	public boolean behaviorEquals(Behavior behavior) {
		return super.behaviorEquals(behavior);
	}

}
