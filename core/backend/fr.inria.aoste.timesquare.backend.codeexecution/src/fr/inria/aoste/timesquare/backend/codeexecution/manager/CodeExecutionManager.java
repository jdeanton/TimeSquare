/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.ui.resource.XtextResourceSetProvider;
import com.google.inject.Injector;

import fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionHelper;
import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionClockBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.behaviors.CodeExecutionRelationBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecRelationBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable;
import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.CodeExecutionAssertPersistentOptions;
import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.CodeExecutionClockPersistentOptions;
import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.CodeExecutionRelationPersistentOptions;
import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.Variable;
import fr.inria.aoste.timesquare.backend.codeexecution.xtext.ui.internal.XtextActivator;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockActivationState.State;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationActivationState;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.utils.CCSLKernelUtils;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.trace.util.ReferenceNameBuilder;
import fr.inria.aoste.trace.EnableStateKind;
import fr.inria.aoste.trace.FiredStateKind;
import fr.inria.aoste.trace.ModelElementReference;


public class CodeExecutionManager extends BehaviorManager {
	private ArrayList<CodeExecutionBehavior> _behaviorList;

	private ArrayList<Variable> _listvariable = new ArrayList<Variable>();

	private String codeExecModelFilename;

	public ArrayList<Variable> getListVariable() {
		return _listvariable;
	}

	public String getCodeExecModelFilename() {
		return codeExecModelFilename;
	}

	public void setCodeExecModelFilename(String codeExecModelFilename) {
		this.codeExecModelFilename = codeExecModelFilename;
	}

	@Override
	public void clear() {
		_listvariable.clear();
		_behaviorList.clear();
		super.clear();
	}

	protected CodeExecutionHelper ceh = null;

	public static boolean loadBehaviorsFromModel = true;

	public CodeExecutionManager() {
		ceh = new CodeExecutionHelper(null);
	}

	public Variable createVariable(String name, IType type) {
		Variable v = new Variable(name, type);
		addVariable(v);
		return v;
	}

	public Variable createVariable(String name, String typeName) {
		Variable v = new Variable(name, typeName);
		addVariable(v);
		return v;
	}

	private void addVariable(Variable v) {
		v.setCodeExecutionHelper(ceh);
		_listvariable.add(v);
	}

	private void addbehavior(CodeExecutionBehavior behavior) {
		_behaviorList.add(behavior);
		Variable v = behavior.getVariable();
		if (v != null) {
			int i = _listvariable.indexOf(v);
			if (i == -1) {
				v.setCodeExecutionHelper(ceh);
				_listvariable.add(v);
			} else {
				if (_listvariable.get(i) != v) {
					behavior.setVariable(v);
				}
			}
		}
		behavior.setCe(ceh);
	}

	@Override
	public void beforeExecution(ConfigurationHelper helper, IPath folderin,
			String namefilein, ISolverForBackend solver) {
		ceh.setConfigurationHelper(helper,solver);

	}

	@Override
	public void end(ConfigurationHelper helper) {
		for (CodeExecutionBehavior b : _behaviorList) {
			b.finish();
		}
		_behaviorList.clear();
		ceh.clear();
		ceh = null;
	}

	public String _getid() {
		return "codeexecution";
	}

	@Override
	public String getPluginName() {
		return "Code Execution";
	}

	@Override
	public void init(ConfigurationHelper helper) {
		super.init(helper);
		_behaviorList = new ArrayList<CodeExecutionBehavior>();
	}

	public void manageBehavior(ConfigurationHelper helper) {

	}

	@Override
	public ClockBehavior redoClockBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {
		if (persistentOptions != null
				&& persistentOptions instanceof CodeExecutionClockPersistentOptions) {
			CodeExecutionClockBehavior behavior = ((CodeExecutionClockPersistentOptions) persistentOptions)
					.redoClockBehavior(helper);

			addbehavior(behavior);

			return behavior;
		}
		return null;
	}

	@Override
	public RelationBehavior redoRelationBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {
		if (persistentOptions != null
				&& persistentOptions instanceof CodeExecutionRelationPersistentOptions) {
			CodeExecutionRelationBehavior behavior = ((CodeExecutionRelationPersistentOptions) persistentOptions)
					.redoRelationBehavior(helper);

			addbehavior(behavior);

			return behavior;
		}
		return null;
	}

	@Override
	public ClockBehavior redoAssertBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {

		if (persistentOptions != null
				&& persistentOptions instanceof CodeExecutionAssertPersistentOptions) {
			CodeExecutionClockBehavior behavior = ((CodeExecutionAssertPersistentOptions) persistentOptions)
					.redoAssertBehavior(helper);

			addbehavior(behavior);

			return behavior;
		}
		return null;
	}

	public void addBehavior(Variable v, IMethod method, ClockEntity ce,
			ClockActivationState clockState, ArrayList<ClockEntity> clockToForce) {

		if (_helper != null && method != null && ce != null && _behaviorList != null) {
			CodeExecutionClockBehavior behavior = new CodeExecutionClockBehavior(
					v, method,clockToForce,_helper);
			CodeExecutionClockPersistentOptions options = new CodeExecutionClockPersistentOptions(
					behavior);

			options.setVariable(v);

			addbehavior(behavior);
			
			_helper.addBehavior(ce, clockState, this.getPluginName(), behavior, options);
		}
	}

	public void addBehavior(Variable v, String methodName, ClockEntity ce,
			ClockActivationState clockState, ArrayList<ClockEntity> clockToForce) {

		if (_helper != null && methodName != null && ce != null && _behaviorList != null) {
			CodeExecutionClockBehavior behavior = new CodeExecutionClockBehavior(
					v, methodName,clockToForce,_helper);
			CodeExecutionClockPersistentOptions options = new CodeExecutionClockPersistentOptions(
					behavior);

			options.setVariable(v);

			addbehavior(behavior);
			
			_helper.addBehavior(ce, clockState, this.getPluginName(), behavior, options);
		}
	}


	public void addBehavior(Variable v, IMethod method, ClockEntity ce, boolean[] assertState) {
		if (_helper != null && method != null && ce != null
				&& _behaviorList != null) {
			CodeExecutionClockBehavior behavior = new CodeExecutionClockBehavior(
					v, method,null,_helper);
			CodeExecutionAssertPersistentOptions options = new CodeExecutionAssertPersistentOptions(
					behavior);

			options.setVariable(v);
			addbehavior(behavior);

			_helper.addBehavior(ce, new AssertActivationState(assertState), this.getPluginName(),
					behavior, options);
		}
	}

	public void addBehavior(Variable v, String methodName, ClockEntity ce, boolean[] assertState) {
		if (_helper != null && methodName != null && ce != null && _behaviorList != null) {
			CodeExecutionClockBehavior behavior = new CodeExecutionClockBehavior(v, methodName,null,_helper);
			CodeExecutionAssertPersistentOptions options = new CodeExecutionAssertPersistentOptions(
					behavior);

			options.setVariable(v);
			addbehavior(behavior);

			_helper.addBehavior(ce, new AssertActivationState(assertState), this.getPluginName(),
					behavior, options);
		}
	}

	public void addBehavior(Variable v, IMethod method, RelationEntity re) {
		if (_helper != null && method != null && _behaviorList != null) {
			CodeExecutionRelationBehavior behavior = new CodeExecutionRelationBehavior(
					v, method,_helper);


			CodeExecutionRelationPersistentOptions options = new CodeExecutionRelationPersistentOptions(
					behavior);

			options.setVariable(v);
			addbehavior(behavior);

			_helper.addBehavior(new RelationActivationState(re), this.getPluginName(), behavior,
					options);
		}
	}

	@Override
	public BehaviorConfigurator<?> getConfigurator(ConfigurationHelper configurationHelper) {

		return new CodeExecutorConfigurator(configurationHelper, this);
	}

	protected HashMap<String, Object> getData() {
		return ceh.getData();
	}

	public void addBehaviorsFromModel(String selectedFile) {
		CodeExecutionSpecification model = loadCodeExecutionSpecification(selectedFile);
		if (model != null) {

			for (CodeExecutionModelBehavior behavior : model.getBehaviors()) {
				if (behavior instanceof CodeExecClockBehavior) {
					buildClockBehavior((CodeExecClockBehavior) behavior);
				} else if (behavior instanceof CodeExecRelationBehavior) {
					buildRelationBehavior((CodeExecRelationBehavior) behavior);
				} else if (behavior instanceof CodeExecAssertionBehavior) {
					buildAssertionBehavior((CodeExecAssertionBehavior) behavior);
				}
			}
		}
	}

	private CodeExecutionSpecification loadCodeExecutionSpecification(String filename) {
		if (filename == null || filename.isEmpty()) {
			return null;
		}
		setCodeExecModelFilename(filename);

		String language = XtextActivator.FR_INRIA_AOSTE_TIMESQUARE_BACKEND_CODEEXECUTION_CODEEXECUTIONSPEC;
		Injector injector = XtextActivator.getInstance().getInjector(language);
		XtextResourceSetProvider provider = injector.getInstance(XtextResourceSetProvider.class);

		XtextResourceSet resourceSet = (XtextResourceSet) provider
				.get(findContainingProject(filename));
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);

		URI uri = URI.createPlatformResourceURI(filename, true);
		XtextResource resource = (XtextResource) resourceSet.getResource(uri, true);
		List<Diagnostic> errors = resource.getErrors();
		if (!errors.isEmpty()) {
			return null;
		}
		return (CodeExecutionSpecification) resource.getContents().get(0);
	}

	private IProject findContainingProject(String filename) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IPath path = root.getLocation();
		path = path.append(new Path(filename));
		
		//cleaner, other the other code commented after works... but sometimes fail later in the code executor to find the class...
		IFile file = (IFile) root.findMember(filename);
		
//		//is the path absolute to the workspace ?
//		//the file need absolute path but this does not work if the file is not actually in the absolute path composed of the workspace path + the file path
//		//this occurs when you import a project in a new workspace without copying it..
//		
//		IFile file = root.getFileForLocation(path);
//		if(file !=null){
//			return file.getProject();
//		}
//		//is it relative ?
//		URL fileUrl = null;
//		try {
//			fileUrl = FileLocator.toFileURL(new URL("platform:/resource/"+filename));
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//		file=root.getFileForLocation(new Path(fileUrl.getPath()));

//		path= new Path("platform:/resource/"+filename);
//		file = root.getFile(path);
		return (file != null ? file.getProject() : null);
	}

	private void buildClockBehavior(CodeExecClockBehavior behavior) {
		// Find the ClockEntity and build the activation state.
		ClockEntity ce = findClockEntity(behavior.getClock());
		ClockActivationState activationState = buildClockActivationState(behavior);
		// Build or find the variable
		Variable var = getOrCreateVariable(behavior.getObjectVariable());
		String methodName = behavior.getMethod().getSimpleName();
		ArrayList<ClockEntity> ceToForce = new ArrayList<ClockEntity>();
		for(Clock c : behavior.getClockToForce()){
			ceToForce.add(findClockEntity(c));
		}
		addBehavior(var, methodName, ce, activationState, ceToForce);
	}

	@SuppressWarnings("unused")
	private void buildRelationBehavior(CodeExecRelationBehavior behavior) {
		Relation relation = behavior.getRelation();
		System.out
				.println("** Creation of behavior for a Relation: (Temporarily) Unsupported operation !!");
	}

	private void buildAssertionBehavior(CodeExecAssertionBehavior behavior) {
		Relation relation = behavior.getAssertion();
		String assertQualifiedName = CCSLKernelUtils.getQualifiedName(relation);
		ClockEntity assertClockEntity = null;
		for (ClockEntity ce : _helper.getAssert()) {
			ModelElementReference mer = ce.getModelElementReference();
			String path = ReferenceNameBuilder.buildQualifiedName(mer, qualifiedNameSeparator);
			if (assertQualifiedName.compareTo(path) == 0) {
				assertClockEntity = ce;
				break;
			}
		}
		if (assertClockEntity != null) {
			Variable var = getOrCreateVariable(behavior.getObjectVariable());
			String methodName = behavior.getMethod().getSimpleName();
			boolean[] assertState = new boolean[] { behavior.isSatisfied(), behavior.isFailed() };
			addBehavior(var, methodName, assertClockEntity, assertState );
		}
	}

	private Variable getOrCreateVariable(ObjectVariable var) {
		String name = var.getName();
		for (Variable variable : _listvariable) {
			if (variable.name.compareTo(name) == 0)
				return variable;
		}
		JvmTypeReference typeRef = var.getType();
		JvmType jtype = typeRef.getType();
		String typeName = jtype.getIdentifier();
		Variable variable = createVariable(name, typeName);
		return variable;
	}

	private ClockActivationState buildClockActivationState(CodeExecClockBehavior behavior) {
		ClockActivationState activationState = new ClockActivationState();
		for (FiredStateKind e : behavior.getFireState()) {
			if (e == FiredStateKind.TICK)
				activationState.setState(State.ticks, true);
			else if (e == FiredStateKind.NO_TICK)
				activationState.setState(State.doesntTick, true);
		}
		for (EnableStateKind e : behavior.getEnableState()) {
			if (e == EnableStateKind.FREE)
				activationState.setState(State.isFree, true);
			else if (e == EnableStateKind.INDETERMINED)
				activationState.setState(State.isUndetermined, true);
			else if (e == EnableStateKind.NO_TICK)
				activationState.setState(State.cannotTick, true);
			else if (e == EnableStateKind.TICK)
				activationState.setState(State.mustTick, true);
		}

		for (LiveStateKind e : behavior.getLiveState()) {
			if (e == LiveStateKind.IS_ALIVE)
				activationState.setState(State.notIsDead, true);
			else if (e == LiveStateKind.IS_DEAD)
				activationState.setState(State.isDead, true);
		}
		return activationState;
	}

	private static String qualifiedNameSeparator = "::";

	private ClockEntity findClockEntity(ConcreteEntity clock) {
		String clockPath = CCSLKernelUtils.getQualifiedName(clock, qualifiedNameSeparator);

		for (ClockEntity ce : _helper.getClocks()) {
			ModelElementReference mer = ce.getModelElementReference();
			String path = ReferenceNameBuilder.buildQualifiedName(mer, qualifiedNameSeparator);
			if (clockPath.compareTo(path) == 0) {
				return ce;
			}
		}
		return null;
	}

}

/*************
 * 
 * old beforeExecution(ConfigurationHelper helper, IPath folderin, String
 * namefilein ) DO NOT REMOVE Ici, tests effectués en "dur" dans ce plugin. A
 * changer ...
 * 
 * On veut que les appel des fonctions par les comportements executent les
 * methodes d'un seul objet instancié à l'initialisation.
 * 
 * On invoque la méthode de la classe JavaProject2/src/jp2/Main.java :
 * 
 * public Map<String, Object> init()
 * 
 * 
 * Celle-ci nous rend une Map associant à chaque nom de classe, une réference
 * sur un Object correspondant. On donne ensuite ces Objects aux behaviors qui
 * executeront la méthode choisie par l'utilisateur sur ces objets.
 * 
 * Voici la méthode complete :
 * 
 * public Map<String, Object> init() { Map<String, Object> map = new
 * HashMap<String, Object>();
 * 
 * ClassB cb = new ClassB(); ClassA ca = new ClassA(cb);
 * 
 * map.put( cb.getClass().getName(), cb ); map.put( ca.getClass().getName(), ca
 * );
 * 
 * return map; }
 * 
 * 
 * 
 * 
 * IResource iresource = MyPluginManager.getWorkspaceRoot().findMember(
 * "JavaProject2/src/jp2/Main.java"); if (iresource == null) { return; }
 * IJavaElement javaElement = JavaCore.create(iresource);
 * 
 * ClassLoader newLoader =
 * ProjectClassLoader.getProjectClassLoader(javaElement);
 * 
 * try { if (javaElement instanceof ICompilationUnit) { ICompilationUnit comp =
 * (ICompilationUnit) javaElement; String s =
 * comp.findPrimaryType().getFullyQualifiedParameterizedName();
 * 
 * Object o = newLoader.loadClass(s).newInstance(); Method method =
 * o.getClass().getMethod("init");
 * 
 * Object ret = method.invoke(o);
 * 
 * if (ret instanceof Map<?, ?>) { Map<?, ?> map = (Map<?, ?>) ret; for
 * (Map.Entry<?, ?> e : map.entrySet()) { String className = (String)
 * e.getKey(); MyManager.println("-- " + className + "  " + e.getValue());
 * 
 * for (CodeExecutionBehavior behavior : _behaviorList) { if
 * (behavior.getClassName().equals(className)) {
 * behavior.setObject(e.getValue()); } }
 * 
 * } } } } catch (Exception e) { e.printStackTrace(); }
 */
