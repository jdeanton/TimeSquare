/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.manager;

import java.util.HashMap;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class CodeExecutorConfigurator extends
		BehaviorConfigurator<CodeExecutionManager> {

	public CodeExecutorConfigurator(ConfigurationHelper _ch,
			CodeExecutionManager _behaviorManager) {
		super(_ch, _behaviorManager);

	}

//	public void createBehaviorforClock(IMethod method, EObject clock)
//			throws Exception {
//		activate();
//		Variable var = new Variable(clock.toString(), method.getDeclaringType());
//		System.out.println("Configuration Behavior : createBehaviorforClock");
//
//		ClockEntity ce = getConfigurationHelper().getClock(clock);
//		getBehaviorManager().addBehavior(var, method, ce,
//				getConfigurationHelper().getTicksState());
//
//	}

//	public void createBehaviorforClock(Variable var, IMethod method,
//			EObject clock) throws Exception {
//		activate();
//		System.out.println("Configuration Behavior : createBehaviorforClock");
//
//		ClockEntity ce = getConfigurationHelper().getClock(clock);
//		getBehaviorManager().addBehavior(var, method, ce,
//				getConfigurationHelper().getTicksState());
//
//	}

	public HashMap<String, Object> getData() {
		return getBehaviorManager().getData();
	}
}
