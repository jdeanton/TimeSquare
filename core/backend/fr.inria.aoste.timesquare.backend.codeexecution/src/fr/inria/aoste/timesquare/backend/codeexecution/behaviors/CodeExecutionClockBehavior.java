/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.behaviors;

import java.util.ArrayList;

import org.eclipse.jdt.core.IMethod;

import fr.inria.aoste.timesquare.backend.codeexecution.persistentoptions.Variable;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;
import fr.inria.aoste.timesquare.trace.util.ReferenceNameBuilder;
import fr.inria.aoste.trace.ModelElementReference;

public class CodeExecutionClockBehavior extends CodeExecutionBehavior implements
		ClockBehavior {
	
      transient private ArrayList<ModelElementReference> _mersToForce=null;
      transient private ArrayList<ClockEntity> _clockToForce=null;
      private ArrayList<String> _clockQualifiedNameToForce= null;

	
	public CodeExecutionClockBehavior(Variable var, IMethod method,ArrayList<ClockEntity> clocksToForce,ConfigurationHelper helper) {
		super(var, method,helper);
		additionalInitiation(clocksToForce);
	}

	public CodeExecutionClockBehavior(Variable var, String methodName,ArrayList<ClockEntity> clocksToForce,ConfigurationHelper helper) {
		super(var, methodName,helper);
		additionalInitiation(clocksToForce);
	}

	private void additionalInitiation(ArrayList<ClockEntity> clocksToForce){
		_clockToForce = new ArrayList<ClockEntity>();
		_mersToForce = new ArrayList<ModelElementReference>();
		_clockQualifiedNameToForce = new ArrayList<String>();
		if(clocksToForce != null){
			_clockToForce = clocksToForce;
			for(ClockEntity ce: clocksToForce){
				ModelElementReference mer = ce.getModelElementReference();
				_mersToForce.add(mer);
				_clockQualifiedNameToForce.add(ReferenceNameBuilder.buildQualifiedName(mer, "::"));
			}
		}
	}

	public ArrayList<String> getClockQualifiedNameToForce(){
		return _clockQualifiedNameToForce;
	}
	
	public ArrayList<ClockEntity> getClocksToForce(){
		return _clockToForce;
	}

	public ArrayList<ModelElementReference> getMersTForce(){
		return _mersToForce;
	}

	public void run(TraceHelper helper) {
		super.execute(_mersToForce);
	}

	public void runWithWrongActivationState(TraceHelper helper) {
	}
}
