/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.gui;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public final class DialogClassFilter extends ViewerFilter {
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (element instanceof IType)
			return true;
		if (element instanceof IJavaProject) {
			try {
				if (((IJavaProject) element).getChildren().length != 0)
					return true;
			} catch (Exception e) {

			}
			return false;
		}
		if (element instanceof IPackageFragmentRoot) {
			if (((IPackageFragmentRoot) element).isArchive())
				return false;
			if (((IPackageFragmentRoot) element).isExternal())
				return false;

			return true;
		}
		if (element instanceof IPackageFragment) {
			return true;
		}
		if (element instanceof ITypeRoot) {
			return true;
		}
		// if (element instanceof I)
		// return true;
		// System.out.println( element.getClass().getName() +" "
		// +parentElement.getClass().getName());
		return false;
	}
}