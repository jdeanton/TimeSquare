/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.codeexecution.gui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IType;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

final class FileValidator implements ISelectionStatusValidator {

	public FileValidator() {
		super();
	}

	public IStatus validate(Object[] selection) {
		try {
			if (selection == null || selection.length <= 0
					|| !(selection[0] instanceof IType)) {
				return new Status(IStatus.ERROR, PlatformUI.PLUGIN_ID,
						IStatus.ERROR, "", null);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return new Status(IStatus.OK, PlatformUI.PLUGIN_ID, IStatus.OK, "",
				null);
	}
}
