/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions;

import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.VCDGeneratorClockBehavior;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;

/**
 * 
 * VCDGeneratorPersistentOptions class<BR>
 * This class implements PersistentOptions and defines the
 * options necessary for a VCD Generation to be persistent.
 * 
 * @author ybondue
 * @version 1.0.0
 * @since Stage DUT Yann Bondue 2011
 *
 */
public class VCDGeneratorPersistentOptions implements PersistentOptions {

	private static final long serialVersionUID = -2712572868182963625L;
	private boolean _pulses;
	private boolean _ghosts;
	private String	_name;
	private String _discretize="";
	
	/**
	 * Standard constructor
	 * 
	 * @param pulses A boolean indicating if the user has chosen to generate the pulses.
	 * @param ghosts A boolean indicating if the user has chosen to generate the ghosts.
	 * @param name The name of the clock.
	 * @since 1.0.0
	 */
	public VCDGeneratorPersistentOptions(boolean pulses, boolean ghosts, String name, ClockEntity discretize) {
		this._pulses = pulses;
		this._ghosts = ghosts;
		this._name	 = name;
		if ( discretize != null)
			this._discretize = AdapterRegistry.getAdapter(discretize.getModelElementReference()).getUID( discretize.getModelElementReference());
	}
	
	@Override
	public String getDescription() {
		
		String buffer = "Pulses status = " + _pulses;
		buffer += "\nGhosts status = " + _ghosts;
		buffer += "\nName representation = " + _name;
		return buffer;
	}

	/**
	 * buildClockBehavior method<BR>
	 * This method allows serialization by returning
	 * a behavior with all the data needed.
	 * 
	 * @return A VCDGeneratorBehavior serialized within these persistent options.
	 * @since 1.0.0
	 */
	public VCDGeneratorClockBehavior buildClockBehavior() {
		return new VCDGeneratorClockBehavior(_pulses, _ghosts, _name, _discretize);
	}
}
