/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors;

import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;
import fr.inria.aoste.timesquare.trace.util.ReferenceNameBuilder;
import fr.inria.aoste.timesquare.vcd.model.BinaryVectorValueChange;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.VarType;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;

/**
 * 
 * GenericDurationBehavior class<BR>
 * This class extends VCDDurationClockBehavior and defines how
 * a couple of clock behaves in order for it to be a
 * ComputationDuration or a CommunicationDuration.
 * 
 * @version 1.0.0
 * @since Stage DUT Yann Bondue 2011
 * @author ybondue
 *
 */
public class GenericDurationClockBehavior extends DurationClockBehavior {

	/*
	 * These constants specify the different modes
	 */
	public static final int MODE_START_FINISH = 0;
	public static final int MODE_SEND_RECEIVE = 1;

	
	private ClockEntity startClock;
	private ClockEntity endClock;
	private ClockEntity refClock;
	private int mode;
	private boolean displayedOverlap;
	
	/**
	 * isRef getter<BR>
	 * This getter allows outside classes to know
	 * if this Duration uses a reference clock or not.
	 * 
	 * @return A boolean indicating whether this Duration
	 * uses a reference clock or not.
	 * @since 1.0.0
	 */
	public boolean isRef() {
		return (refClock != null);
	}

	/**
	 * Standard constructor for a generic duration that uses a reference clock.
	 * 
	 * @param mode An integer specifying if this behavior is a Start/Finish or a Send/Receive.
	 * @param beginClock The beginning clock for this generic duration.
	 * @param endClock The ending clock for this generic duration.
	 * @param refClock The reference clock for this generic duration.
	 * @param displayedOverlap the boolean specifying the displayed overlap.
	 * @since 1.0.0
	 */
	public GenericDurationClockBehavior(int mode, ClockEntity beginClockEntity, ClockEntity endClockEntity,
			ClockEntity refClockEntity, boolean displayedOverlap) {
		this(mode, beginClockEntity, endClockEntity, displayedOverlap);
		this.refClock = refClockEntity;
	}
	
	/**
	 * Standard constructor for a generic duration that does not use a reference clock.
	 * 
	 * @param mode An integer specifying if this behavior is a Start/Finish or a Send/Receive.
	 * @param beginClock The beginning clock for this generic duration.
	 * @param endClock The ending clock for this generic duration.
	 * @param displayedOverlap the boolean specifying the displayed overlap.
	 * @since 1.0.0
	 */
	public GenericDurationClockBehavior(int mode, ClockEntity beginClockEntity, ClockEntity endClockEntity, boolean displayedOverlap) {
		if (mode != MODE_START_FINISH && mode != MODE_SEND_RECEIVE)
			return;
		this.startClock = beginClockEntity;
		this.endClock = endClockEntity;
		this.mode = mode;
		this.refClock = null;
		this.displayedOverlap = displayedOverlap;
	}

	@Override
	public void run(TraceHelper helper) {
		// TODO The Overlaps overlay has not yet been developed.
		
		EventOccurrence eventOccurrence = helper.getClockState();
		// TODO The following test is no sufficient. If one of the clock involved in the duration (either
		// as start, end or reference, is dead, it is necessary to terminate the duration. However, this
		// function (run()) is called for each of the three clocks, in any order, so it is possible that
		// if the starting clock ticks, we may start an "active" period of the duration, then, if one of
		// the other clocks id dead, terminating the period on the same step : the graphical aspect of this
		// is strange.
		if (eventOccurrence.isIsClockDead()) {
			_scoreBoard.add(new BinaryVectorValueChange("Z", _pCode));
			return;
		}
		// ClockEntity may not be the same objects : compare their names
		if (helper.getClockEntity().getName().compareTo(startClock.getName()) == 0) {
			if (eventOccurrence.getFState() == FiredStateKind.TICK)
				_scoreBoard.add(new BinaryVectorValueChange("X", _pCode));
			return;
		}
		else if (helper.getClockEntity().getName().compareTo(endClock.getName()) == 0) {
			if (eventOccurrence.getFState() == FiredStateKind.TICK)
				_scoreBoard.add(new BinaryVectorValueChange("Z", _pCode));
			return;
		}
		else if (refClock != null) {
			if (helper.getClockEntity().getName().compareTo(refClock.getName()) == 0) {
				return;
			}
		}
	}

	/**
	 * runWithWrongActivationState method<BR>
	 * <i>This method's semantics is irrelevant to the Generic Duration.</i>
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public void runWithWrongActivationState(TraceHelper helper) {}

	@Override
	public String getDescription() {
		String modeDesc = "";
		if (mode == GenericDurationClockBehavior.MODE_SEND_RECEIVE)
			modeDesc = "send/receive";
		else if (mode == MODE_START_FINISH)
			modeDesc = "start/finish";
		return startClock.getName() + " and " + endClock.getName() + " define a " + modeDesc + " relation.";
	}

	@Override
	public boolean equals(Object o) {
		
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (this.getClass() != o.getClass())
			return false;
		
		GenericDurationClockBehavior other = (GenericDurationClockBehavior)o;
		
		if (this.startClock == other.startClock && this.endClock == other.endClock && this.refClock == other.refClock
				&& this.displayedOverlap == other.displayedOverlap)
			return true;
		return false;
	}

	@Override
	public void initialize() {
		_scoreBoard.addVar("Duration" + _pCode, VarType.tri, _pCode);
		/*
		 * BeginGenericDuration alias generation (like : "Duration from clock1 to clock2").
		 */
		ICommentCommand icc = _scoreBoard.getVcdModel().createCommentClock("alias", "Duration" + _pCode);
		if (icc != null) {
			String startName = ReferenceNameBuilder.buildQualifiedName( startClock.getModelElementReference() );
			String endName = ReferenceNameBuilder.buildQualifiedName( endClock.getModelElementReference() );
			icc.setString(2, "\"" + startName + " to " + endName + "\"");
		}
		this.setInitialized(true);
	}

	public ClockEntity getStartClock() {
		return startClock;
	}

	public ClockEntity getEndClock() {
		return endClock;
	}

	public ClockEntity getRefClock() {
		return refClock;
	}

}
