/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.manager;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.backend.manager.utils.Filter;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class VCDBehaviorConfigurator extends BehaviorConfigurator<VCDGeneratorManager> {

	public VCDBehaviorConfigurator(ConfigurationHelper _ch, VCDGeneratorManager _behaviorManager) {
		super(_ch, _behaviorManager);
	}

	public ArrayList<ClockEntity> getDisctretizeClock() {
		return Filter.sublist(getConfigurationHelper().getClocks(), Filter.FILTER_CLOCK_ENTITY_DISCRETIZE);
	}

}
