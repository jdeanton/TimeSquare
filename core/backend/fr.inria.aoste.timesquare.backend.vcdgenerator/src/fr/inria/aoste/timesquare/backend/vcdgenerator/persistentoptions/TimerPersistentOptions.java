/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions;

/**
 * 
 * TimerDurationPersistentOptions class<BR>
 * This class implements PersistentOptions and defines the
 * options necessary for a Timer Duration to be persistent.
 * 
 * @author ybondue
 * @version 1.0.0
 * @since Stage DUT Yann Bondue 2011
 *
 */
public class TimerPersistentOptions extends DurationPersistentOptions {

	private static final long serialVersionUID = 5352441741435851035L;

	/**
	 * Standard constructor
	 * 
	 * @param ID The ID of the referenced element
	 * @since 1.0.0
	 */
	public TimerPersistentOptions(String durationModelFilename, String ID) {
		super(durationModelFilename);
		this._ID = ID;
	}
	
	@Override
	public String getDescription() {
		return "referenced element URI = " + _ID;
	}
}
