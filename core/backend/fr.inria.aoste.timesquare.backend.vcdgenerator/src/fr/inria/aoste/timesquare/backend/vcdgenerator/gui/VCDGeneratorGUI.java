/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import fr.inria.aoste.timesquare.backend.manager.utils.Filter;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.GUIHelper;
import fr.inria.aoste.timesquare.backend.vcdgenerator.manager.VCDGeneratorManager;
import fr.inria.aoste.timesquare.utils.ui.widgets.FileSelectionControl;

/**
 * 
 * VCDGeneratorGUI class<BR>
 * This class extends BehaviorManagerGUI and defines the specifications of the
 * graphical user interface used for the VCD Generation behavior.
 * 
 * @author ybondue
 * @version 1.0.0
 * @since Stage DUT Yann Bondue 2011
 * 
 */
/*
	TODO A listener that disables the OK button if both pulses and ghosts are unchecked would be nice. 
	TODO Or at least a listener to display a message if both are disabled and OKPressed
	TODO Don't allow the user to add multiple VCD Generation behavior to a single clock (Or at least not
	TODO with the same options) (I should check that). 
 */
public class VCDGeneratorGUI extends BehaviorManagerGUI {
	/** all String are public constant*/
	public static final String WIDGET_VCDGENERATOR_SELECT_TYPE = "vcdgenerator.select.type";	
	public static final String WIDGET_VCDGENERATOR_SELECT_TIMED_DURATION_COUNT_CLOCK = "vcdgenerator.select.timedDuration.countClock";
	public static final String WIDGET_VCDGENERATOR_SELECT_TIMED_DURATION_START = "vcdgenerator.select.timedDuration.start";
	public static final String WIDGET_VCDGENERATOR_SELECT_CLOCK2CLOCK_DURATION_REFCLOCK = "vcdgenerator.select.clock2clockDuration.refclock";
	public static final String WIDGET_VCDGENERATOR_SELECT_CLOCK2CLOCK_DURATION_ENDCLOCK = "vcdgenerator.select.clock2clockDuration.endclock";
	public static final String WIDGET_VCDGENERATOR_SELECT_CLOCK2CLOCK_DURATION_BEGINCLOCK = "vcdgenerator.select.clock2clockDuration.beginclock";
	public static final String WIDGET_VCDGENERATOR_SELECT_SPECIFIC_CLOCK = "vcdgenerator.select.specific_clock";
	public static final String WIDGET_VCDGENERATOR_SELECT_CLOCKDISCRETIZED = "vcdgenerator.select.clockdiscretized";
	/** all String are public constant*/
	public static final String NOTE_PULSES_AND_GHOSTS_ARE_GLOBAL_OPTION = "Note : Pulses and ghosts are global option";
	public static final String PULSES = "Pulses";
	public static final String LAUNCHVIEWER = "Launch VCD Viewer";
	public static final String GENERATE = "Generate :";
	public static final String DISCRETIZE = "Discretize :";
	public static final String GENERATE_VCD_FOR_A_SPECIFIC_CLOCK = "Generate VCD for a specific clock";
	public static final String GENERATE_VCD_FOR_ALL_CLOCKS = "Generate VCD for all clocks";
	public static final String VCD_GENERATION = "VCD Generation";
	public static final String DURATION_MODEL = "VCD Duration model :";
	
	//****/
	private Group _VCDGenerationComposite;
	//
	private Composite _compositeClocks;

	private Button _pulses;
	private Button _ghosts;
	private Button _launchVCDViewer;
	private Combo _discretize;
	private Button _allClocks;
	private Button _singleClock;
	private Combo _comboClock;
	private FileSelectionControl _durationModelBrowser;

	/**
	 * 
	 * ClockCheckBoxSelectionListener class<BR>
	 * This class is a listener which enables or disables the single clock
	 * selection list depending on the radio button selected.
	 * 
	 * @author ybondue
	 * @version 1.0.0
	 * @since Stage DUT Yann Bondue 2011
	 * 
	 */
	private static class ClockCheckBoxSelectionListener implements SelectionListener {

		private VCDGeneratorGUI gui;

		/**
		 * Standard constructor
		 * 
		 * @param gui
		 *            The GUI on which the listening is to be done.
		 * @since 1.0.0
		 */
		private ClockCheckBoxSelectionListener(VCDGeneratorGUI gui) {
			this.gui = gui;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			if (gui._allClocks.getSelection())
				gui._comboClock.setEnabled(false);
			else if (gui._singleClock.getSelection())
				gui._comboClock.setEnabled(true);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			if (gui._allClocks.getSelection())
				gui._comboClock.setEnabled(false);
			else if (gui._singleClock.getSelection())
				gui._comboClock.setEnabled(true);
		}
	}

	/**
	 * Standard constructor
	 * 
	 * @since 1.0.0
	 */
	public VCDGeneratorGUI() {
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public Control createDialogArea(Composite compositein) {
		if (compositein == null)
			return null;
		compositein.setLayout(new GridLayout());
		displayVCDGeneration(compositein);

		return _VCDGenerationComposite;
	}

	private void displayVCDGeneration(Composite parent) {

		_VCDGenerationComposite = new Group(parent, SWT.FILL_WINDING | SWT.RESIZE);
		_VCDGenerationComposite.setText(VCD_GENERATION);
		_VCDGenerationComposite.setLocation(5, 5);
		_VCDGenerationComposite.setLayout(new GridLayout(1, true));
		GridData layoutData = new GridData(SWT.FILL, SWT.NONE, true, false);
		layoutData.minimumWidth = 435;
		layoutData.minimumHeight = 213;
		_VCDGenerationComposite.setLayoutData(layoutData);
		{
			_allClocks = new Button(_VCDGenerationComposite, SWT.RADIO);
			_allClocks.setText(GENERATE_VCD_FOR_ALL_CLOCKS);
			_allClocks.setSelection(true);
			_allClocks.addSelectionListener(new ClockCheckBoxSelectionListener(this));

			_singleClock = new Button(_VCDGenerationComposite, SWT.RADIO);
			_singleClock.setText(GENERATE_VCD_FOR_A_SPECIFIC_CLOCK);
			_singleClock.setSelection(false);
			_singleClock.addSelectionListener(new ClockCheckBoxSelectionListener(this));

			_compositeClocks = new Composite(_VCDGenerationComposite, SWT.FILL);
			_compositeClocks.setLayout(new GridLayout(1, true));

			_comboClock = getGUIHelper().displayList(_compositeClocks, getConfigurationHelper().getClocks(),WIDGET_VCDGENERATOR_SELECT_SPECIFIC_CLOCK);
			_comboClock.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
			_comboClock.setEnabled(false);
		}

		{

			Label separator = new Label(_VCDGenerationComposite, SWT.SEPARATOR | SWT.HORIZONTAL);
			separator.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
			Label optionsLabel = new Label(_VCDGenerationComposite, SWT.NONE);
			optionsLabel.setText(DISCRETIZE);
			optionsLabel.pack();

			_discretize = getGUIHelper().displayList(_VCDGenerationComposite, getConfigurationHelper().getClocks(),	Filter.FILTER_CLOCK_ENTITY_DISCRETIZE,WIDGET_VCDGENERATOR_SELECT_CLOCKDISCRETIZED);
			_discretize.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
			_discretize.setEnabled(true);
			if (_discretize.getItemCount() != 0)			
				_discretize.select(0);
		}

		{

			Label separator = new Label(_VCDGenerationComposite, SWT.SEPARATOR | SWT.HORIZONTAL);
			separator.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
			Label optionsLabel = new Label(_VCDGenerationComposite, SWT.NONE);
			optionsLabel.setText(GENERATE);
			optionsLabel.pack();

			_pulses = new Button(_VCDGenerationComposite, SWT.CHECK);
			_pulses.setText(PULSES);
			_pulses.setSelection(true);

			_ghosts = new Button(_VCDGenerationComposite, SWT.CHECK);
			_ghosts.setText("Ghosts");
			_ghosts.setSelection(true);

			Label noteLabel = new Label(_VCDGenerationComposite, SWT.NONE);
			noteLabel.setText(NOTE_PULSES_AND_GHOSTS_ARE_GLOBAL_OPTION);
			noteLabel.pack();
		}
		
		{
			Label separator = new Label(_VCDGenerationComposite, SWT.SEPARATOR | SWT.HORIZONTAL);
			separator.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
			Label optionsLabel = new Label(_VCDGenerationComposite, SWT.NONE);
			optionsLabel.setText("");
			optionsLabel.pack();

			_launchVCDViewer = new Button(_VCDGenerationComposite, SWT.CHECK);
			_launchVCDViewer.setText(LAUNCHVIEWER);
			_launchVCDViewer.setSelection( this.getBehaviorManager().is_launchVCDViewer() );
			// TODO Currently the VCD file is created but not filled if the VCD viewer is not visible !
			// Disable the control to avoid changing the setting.
			_launchVCDViewer.setEnabled(false);
		}
		{
			Label separator = new Label(_VCDGenerationComposite, SWT.SEPARATOR | SWT.HORIZONTAL);
			separator.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
			Label optionsLabel = new Label(_VCDGenerationComposite, SWT.NONE);
			optionsLabel.setText("");
			optionsLabel.pack();
			
			String[] durationExtensions = {"duration"};
			_durationModelBrowser = new FileSelectionControl(_VCDGenerationComposite, DURATION_MODEL, durationExtensions);
			
			String durationFile = this.getBehaviorManager().get_durationModelFilename();
			if (durationFile != null) {
				_durationModelBrowser.setSelectedFile(durationFile);
			}
		}
		_VCDGenerationComposite.pack();
	}

	/*
	 * Potential new types of behavior must add a similar method
	 * as displayVCDGeneration, displayClockToClock or displayTimed
	 * here.
	 */

	@Override
	public void okPressed() {

		/*
		 * Potential new types of behavior must add code here.
		 */
		if (_VCDGenerationComposite.isVisible()) {
			createVCDGenerationBehavior();
		}
		this.getBehaviorManager().set_launchVCDViewer( _launchVCDViewer.getSelection() );
	}

	@Override
	public void cancelPressed() {
		/*
		 * Closes the window.
		 */
	}

	protected VCDGeneratorManager getBehaviorManager() {
		return (VCDGeneratorManager) super.getBehaviorManager();
	}

	/**
	 * createVCDGenerationBehavior method<BR>
	 * This method creates a VCD Generation behavior.
	 * 
	 * @since 1.0.0
	 */
	private void createVCDGenerationBehavior() {

		ClockEntity ced = null;
		if (_discretize.getData(GUIHelper.COMBOSELECTION) instanceof ClockEntity)
			ced = (ClockEntity) _discretize.getData(GUIHelper.COMBOSELECTION);
		if (_allClocks.getSelection()) {
			for (ClockEntity clock : this.getConfigurationHelper().getClocks()) {
				if (clock != null)
					this.getBehaviorManager().addVCDGeneratorBehavior(clock, _pulses.getSelection(),
							_ghosts.getSelection(),ced);
			}
		} else if (_singleClock.getSelection()) {
			if ((ClockEntity) _comboClock.getData(GUIHelper.COMBOSELECTION) != null) {
				ClockEntity ce = (ClockEntity) _comboClock.getData(GUIHelper.COMBOSELECTION);
				if (ce != null)
					this.getBehaviorManager().addVCDGeneratorBehavior(ce, _pulses.getSelection(),
							_ghosts.getSelection(),ced);
			}
		}
		this.getBehaviorManager().set_durationModelFilename(_durationModelBrowser.getSelectedFile());
		// tell the manager to setup and establish the duration behaviors
		this.getBehaviorManager().createDurationBehaviors();
	}
}
