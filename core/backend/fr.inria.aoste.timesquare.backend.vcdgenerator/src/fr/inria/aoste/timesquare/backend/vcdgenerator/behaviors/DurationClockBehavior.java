/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors;

import fr.inria.aoste.timesquare.vcd.model.BinaryVectorValueChange;

/**
 * 
 * VCDDurationClockBehavior class<BR>
 * This class extends AbstractVCDClockBehavior and 
 * defines an overlay to it, as it is this class that
 * will be extended by the actual behaviors, in a
 * very similar fashion to the one used in the Duration
 * model. The aim of this class is mainly to have a
 * superclass for the Duration behaviors.
 * 
 * @author ybondue
 * @version 1.0.0
 * @since Stage DUT Yann Bondue 2011
 *
 */
public abstract class DurationClockBehavior extends AbstractVCDClockBehavior {
	
	@Override
	public void end() {
		_scoreBoard.add(new BinaryVectorValueChange("Z", _pCode));
	}
	
	/**
	 * aNewStep method<BR>
	 * <i>Unnecessary in this context.</i>.
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public void aNewStep() {}
	
	/**
	 * aPostNewStep method<BR>
	 * <i>Unnecessary in this context.</i>.
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public void aPostNewStep() {}
}
