/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions;

import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

/**
 * 
 * PlugInPersistentOptions class<BR>
 * This class implements PersistentOptions and defines the
 * necessary options for the plug-in to compute.
 * More particularly, this means serializing the path to the
 * duration model.
 * 
 * @author ybondue
 * @version 1.0.0
 * @since Stage DUT Yann Bondue 2011
 *
 */
public class PlugInPersistentOptions implements PersistentOptions {

	private static final long serialVersionUID = -643451390276411565L;
	
	private String _pathToDurationModel;
	private boolean _launchVCDViewer;
	
	/**
	 * Standard constructor
	 * 
	 * @param path the path to the Duration model.
	 */
	public PlugInPersistentOptions(String path, boolean launchVCDViewer) {
		this._pathToDurationModel = path;
		this._launchVCDViewer = launchVCDViewer;
	}
	
	@Override
	public String getDescription() {
		return _pathToDurationModel + " " + _launchVCDViewer;
	}

	public String get_pathToDurationModel() {
		return _pathToDurationModel;
	}

	public boolean is_launchVCDViewer() {
		return _launchVCDViewer;
	}

}
