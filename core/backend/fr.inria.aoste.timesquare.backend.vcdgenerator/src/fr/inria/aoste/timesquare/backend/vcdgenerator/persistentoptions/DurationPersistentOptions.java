/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions;

import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

/**
 * 
 * DurationPersistentOptions class<BR>
 * This class implements PersistentOptions and defines the
 * options necessary for Durations to be persistent.
 * 
 * @author ybondue
 * @version 1.0.0
 * @since Stage DUT Yann Bondue 2011
 *
 */
@SuppressWarnings("serial")
public abstract class DurationPersistentOptions implements PersistentOptions {
	
	protected String _durationModelFilename;
	protected String _ID;
	
	/**
	 * Standard constructor
	 * 
	 * @since 1.0.0
	 */
	public DurationPersistentOptions(String durationModelFilename) {
		this._durationModelFilename = durationModelFilename;
	}
	
	public String getID() {
		return _ID;
	}

	public String getDurationModelFilename() {
		return _durationModelFilename;
	}
}
