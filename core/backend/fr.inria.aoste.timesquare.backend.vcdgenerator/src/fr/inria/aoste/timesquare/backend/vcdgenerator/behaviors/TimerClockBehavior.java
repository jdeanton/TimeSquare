/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors;

import java.util.HashMap;

import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;
import fr.inria.aoste.timesquare.trace.util.ReferenceNameBuilder;
import fr.inria.aoste.timesquare.vcd.model.BinaryVectorValueChange;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.VarType;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.FiredStateKind;

/**
 * 
 * TimerDurationBehavior class<BR>
 * This class implements DurationClockBehavior and defines how
 * a clock behaves in order for it to generate a Timer Duration.
 * 
 * @author ybondue
 * @version 1.0.0
 * @since Stage DUT Yann Bondue 2011
 *
 */
public class TimerClockBehavior extends DurationClockBehavior {

	/** This static field allows communication between the different
	 *  timer behaviors. */
	private static HashMap<TimerClockBehavior, Boolean> activeBehaviors = new HashMap<TimerClockBehavior, Boolean>();
	/** This static field allows the different behaviors to interact
	 *  on the same duration. */
	private static HashMap<TimerClockBehavior, Integer> remainingDurations = new HashMap<TimerClockBehavior, Integer>();
	
	private ClockEntity startClock;
	private ClockEntity refClock;
	private int delay;
	
	
	
	/**
	 * getDelay getter<br>
	 * 
	 * @return The delay of this behavior.
	 * @since 1.0.0
	 */
	public int getDelay() {
		return delay;
	}
	
	/**
	 * Standard constructor.
	 * 
	 * @param beginClock The beginning clock for this generic duration.
	 * @param refClock The reference clock for this generic duration.
	 * @param delay The duration in milliseconds of the action.
	 * @since 1.0.0
	 */	
	public TimerClockBehavior(ClockEntity beginClockEntity, ClockEntity refClockEntity, int delay) {
		this.startClock = beginClockEntity;
		this.refClock = refClockEntity;
		this.delay = delay;
	}

	@Override
	public void run(TraceHelper helper) {

		EventOccurrence eventOccurrence = helper.getClockState();
		
		if (eventOccurrence.isIsClockDead()) {
			_scoreBoard.add(new BinaryVectorValueChange("Z", _pCode));
			return;
		}
		
		if (helper.getClockEntity().getName().compareTo(refClock.getName()) == 0) {
			if (eventOccurrence.getFState() == FiredStateKind.TICK) {
				Integer currentCount = remainingDurations.get(this);
				if (currentCount != null) {
					remainingDurations.put(this, currentCount - 1);
					if (remainingDurations.get(this) <= 0) {
						_scoreBoard.add(new BinaryVectorValueChange("Z", _pCode));
						remainingDurations.put(this, getDelay());
						activeBehaviors.put(this, false);
					}
				}
			}
			return;
		}
		if (helper.getClockEntity().getName().compareTo(startClock.getName()) == 0) {
			if (eventOccurrence.getFState() == FiredStateKind.TICK) {
				if (activeBehaviors.get(this) == null)
					activeBehaviors.put(this, false);
				if (!activeBehaviors.get(this)) {
					_scoreBoard.add(new BinaryVectorValueChange("X", _pCode));
					remainingDurations.put(this, getDelay());
					activeBehaviors.put(this, true);
				}
			}
			return;
		}
	}

	/**
	 * runWithWrongActivationState method<BR>
	 * <i>This method's semantics is irrelevant to the Timer.</i>
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public void runWithWrongActivationState(TraceHelper helper) {}

	@Override
	public String getDescription() {
		return startClock.getName() + "'s ticks are the starting points of "+ getDelay() + " ticks durations on " + refClock.getName() + ".";
	}

	@Override
	public boolean equals(Object o) {
		
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (this.getClass() != o.getClass())
			return false;
		
		TimerClockBehavior other = (TimerClockBehavior)o;
	
		if (this.startClock == other.startClock && this.refClock == other.refClock && this.delay == other.delay)
			return true;
		return false;
	}

	@Override
	public void initialize() {
		
		_scoreBoard.addVar("Timer" + _pCode, VarType.tri, _pCode);
		/*
		 * Timer alias generation (like : "Timer on clock1").
		 */
		ICommentCommand icc = _scoreBoard.getVcdModel().createCommentClock("alias", "Timer" + _pCode);
		if (icc != null) {
			String startName = ReferenceNameBuilder.buildQualifiedName(startClock.getModelElementReference());
			icc.setString(2, "\"Timer started on " + startName + "\"");
		}
		this.setInitialized(true);
	}

	public ClockEntity getStartClock() {
		return startClock;
	}

	public ClockEntity getRefClock() {
		return refClock;
	}

}
