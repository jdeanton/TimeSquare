/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.vcdgenerator.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;

import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.DeleteHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.backend.vcdgenerator.ScoreBoard;
import fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.DurationClockBehavior;
import fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.GenericDurationClockBehavior;
import fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.TimerClockBehavior;
import fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.AbstractVCDClockBehavior;
import fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.VCDGeneratorClockBehavior;
import fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions.DurationPersistentOptions;
import fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions.GenericDurationPersistentOptions;
import fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions.PlugInPersistentOptions;
import fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions.TimerPersistentOptions;
import fr.inria.aoste.timesquare.backend.vcdgenerator.persistentoptions.VCDGeneratorPersistentOptions;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.NamedElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConcreteEntity;
import fr.inria.aoste.timesquare.duration.model.duration.CommunicationDuration;
import fr.inria.aoste.timesquare.duration.model.duration.Duration;
import fr.inria.aoste.timesquare.duration.model.duration.DurationModel;
import fr.inria.aoste.timesquare.duration.model.duration.GenericDuration;
import fr.inria.aoste.timesquare.duration.model.duration.Timer;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.trace.util.ReferenceNameBuilder;
import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.timesquare.trace.util.TimeBase;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;
import fr.inria.aoste.timesquare.vcd.antlr.editors.VcdMultiPageEditor;
import fr.inria.aoste.timesquare.vcd.antlr.view.ConstraintView;
import fr.inria.aoste.timesquare.vcd.menu.Mode;
import fr.inria.aoste.timesquare.vcd.model.ICommentCommand;
import fr.inria.aoste.timesquare.vcd.model.command.DateCommand;
import fr.inria.aoste.timesquare.vcd.model.command.SimulationCommand;
import fr.inria.aoste.timesquare.vcd.model.command.TimeScaleCommand;
import fr.inria.aoste.timesquare.vcd.model.command.VersionCommand;
import fr.inria.aoste.timesquare.vcd.model.comment.ScaleCommand;
import fr.inria.aoste.timesquare.vcd.model.keyword.TimeUnit;
import fr.inria.aoste.timesquare.vcd.preferences.VcdColorPreferences;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.PhysicalBase;
import fr.inria.aoste.trace.Reference;

/**
 * 
 * VCDGeneratorManager class<BR>
 * This class extends BehaviorManager and defines the specifications and basic
 * methods of the VCD Generation behavior.
 * 
 * @author ybondue
 * @version 2.0.0
 * @since Stage DUT Yann Bondue 2011
 * 
 */
public class VCDGeneratorManager extends BehaviorManager {

	private final static String PLUGIN_NAME = "VCD Generation";
	private final static String VERSION_NAME = "2.1.0";

	/**
	 * This field allows access to all the behaviors of the simulation from
	 * anywhere.
	 */
	private Vector<AbstractVCDClockBehavior> _behaviorList;
	/** This field allows the Manager to write in the VCD output. */
	private ScoreBoard _scoreBoard;
	/** This field gives the Manager the means to edit the VCD output. */
	private VcdMultiPageEditor _vcdEditor;
	/** This field specifies the path to the output file. */
	private IPath _outputFilePath;
	/** This field specifies the name of the output file. */
	private String _outputFileName;
	/** This field specifies the color options for the VCD Viewer plug-in. */
	private VcdColorPreferences _myColorAPI;

	/**
	 * This field specifies whether the pulses will be generated during this
	 * simulation.
	 */
	private boolean _pulses = false;
	/**
	 * This field specifies whether the ghosts will be generated during this
	 * simulation.
	 */
	private boolean _ghosts = false;
	/**
	 * This field is required for updating the VCD output, it remembers the size
	 * of the ScoreBoard since the last time the updating algorithm has been
	 * applied.
	 */
	private int previousSize = 0;

	/**
	 * 
	 */
	private boolean _launchVCDViewer = true;

	/**
	 * This field specifies the number of the step that is currently being
	 * computed.
	 */
	private int _currentStep = 0;

	private String _durationModelFilename;

	/** This field gives access to the set of resources used within the model. */
	private ResourceSet _resourceSet;
	/**
	 * This field gives access to the resource (supposedly unique) used within
	 * the model.
	 */
	private Resource _resource;
	/**
	 * This field gives access to the root of the elements represented within
	 * the model.
	 */
	private DurationModel _durationModelRoot;

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	public String get_durationModelFilename() {
		return _durationModelFilename;
	}

	public void set_durationModelFilename(String _durationModelFilename) {
		this._durationModelFilename = _durationModelFilename;
	}

	@Override
	public void init(ConfigurationHelper helper) {
		super.init(helper);
		_behaviorList = new Vector<AbstractVCDClockBehavior>();
		clear();
	}

	/**
	 * addVCDGeneratorBehavior method<BR>
	 * This methods adds a behavior to the list of behaviors that the simulation
	 * will have using the VCDGeneratorBehavior class constructor.<BR>
	 * For this class, it means adding a clock to the list of those which VCD
	 * will be generated. The behavior will have a name which is the same of the
	 * one of the ClockEntity (as returned by {@link ClockEntity#getName()}).
	 * This is necessary as this assumption of identical names is used both in
	 * this function and in
	 * {@link #getClock(ConfigurationHelper, VCDGeneratorClockBehavior)} when
	 * called either from
	 * {@link #beforeExecution(ConfigurationHelper, IPath, String)} or from
	 * {@link #redoClockBehavior(ConfigurationHelper, PersistentOptions)}.
	 * 
	 * @param clockEntity
	 *            The Clock chosen by the user in the GUI, see ClockEntity for
	 *            more details about the types that this includes.
	 * @param pulses
	 *            A boolean indicating if the user has chosen to generate the
	 *            pulses.
	 * @param ghosts
	 *            A boolean indicating if the user has chosen to generate the
	 *            ghosts.
	 * @param ced
	 * @see fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity
	 * @since 1.0.0
	 */
	public void addVCDGeneratorBehavior(ClockEntity clockEntity, boolean pulses, boolean ghosts,
			ClockEntity discretize) {
		for (AbstractVCDClockBehavior b : _behaviorList) {
			if (b instanceof VCDGeneratorClockBehavior) {
				if (((VCDGeneratorClockBehavior) b).getName().equals(clockEntity.getName())) {
					return;
				}
			}
		}
		addVCDGeneratorBehavior(
				new VCDGeneratorClockBehavior(pulses, ghosts, clockEntity.getName(),
						(discretize != null ? AdapterRegistry.getAdapter(
								discretize.getModelElementReference()).getUID(
								discretize.getModelElementReference()) : "")), clockEntity,
				discretize);
	}

	/**
	 * addVCDGeneratorBehavior method<BR>
	 * This methods adds a behavior to the list of behaviors that the simulation
	 * will have.<BR>
	 * For this class, it means adding a clock to the list of those which VCD
	 * will be generated.
	 * 
	 * @param behavior
	 *            The behavior that is going to be added.
	 * @param clockEntity
	 *            The Clock chosen by the user in the GUI, see ClockEntity for
	 *            more details about the types that this includes.
	 * @see fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.VCDGeneratorClockBehavior
	 * @since 1.0.0
	 */
	private void addVCDGeneratorBehavior(VCDGeneratorClockBehavior behavior,
			ClockEntity clockEntity, ClockEntity discretize) {

		addVCDGeneratorBehavior(behavior);

		VCDGeneratorPersistentOptions options = new VCDGeneratorPersistentOptions(
				behavior.isPulses(), behavior.isGhosts(), behavior.getName(), discretize);

		if (_helper != null)
			_helper.addBehavior(clockEntity, _helper.getAlwaysState(), this.getPluginName(),
					behavior, options);
	}

	private String discretize = null;

	/**
	 * addVCDGeneratorBehavior method<BR>
	 * This methods adds a behavior to the list of behaviors that the simulation
	 * will have.<BR>
	 * For this class, it means adding a clock to the list of those which VCD
	 * will be generated.<BR>
	 * This method has to set the _pulses and _ghosts fields for the Manager as
	 * these parameters will be used for every single behavior.
	 * 
	 * @param behavior
	 *            The behavior that is going to be added.
	 * @see fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.VCDGeneratorClockBehavior
	 * @since 1.0.0
	 */
	private void addVCDGeneratorBehavior(VCDGeneratorClockBehavior behavior) {

		this._behaviorList.add(behavior);
		if (behavior.isPulses())
			_pulses = true;
		if (behavior.isGhosts())
			_ghosts = true;
		discretize = behavior.getDiscretize();
	}

	/**
	 * Returns a ClockEntity associated with the current model clock. Used when
	 * building a duration related behaviour, where the duration model provides
	 * only the model clock, while the backend uses ClockEntity objects.
	 * 
	 * Assume that the clock for which we are looking the corresponding
	 * ClockEntity is a Clock which is "global" in the model, ie. it is directly
	 * contained in a Block. If this condition is satisfied, then the physical
	 * path in the model (hierarchy of containment) gives the same qualified
	 * name as the concatenation of names of the elements in the
	 * ModelElementReference stored in the ClockEntity.
	 */
	private ClockEntity findClockEntity(ConcreteEntity clock) {
		String separator = "::";
		String clockPath = buildPhysicalPath(clock, separator);
		for (ClockEntity ce : _helper.getClocks()) {
			ModelElementReference mer = ce.getModelElementReference();
			String path = ReferenceNameBuilder.buildQualifiedName(mer, separator);
			if (clockPath.compareTo(path) == 0) {
				return ce;
			}
		}
		return null;
	}

	private String buildPhysicalPath(ConcreteEntity clock, String separator) {
		StringBuilder sb = new StringBuilder(clock.getName());
		EObject container = clock.eContainer();
		while (container != null) {
			// Avoid the ClockConstraintSystem object at the top of the
			// containment hierarchy because
			// it is never part of the ModelElementReference
			if (container instanceof NamedElement && !(container instanceof ClockConstraintSystem)) {
				sb.insert(0, separator);
				sb.insert(0, ((NamedElement) container).getName());
			}
			container = container.eContainer();
		}
		return sb.toString();
	}

	/**
	 * Local cache of already created duration-related behavior. Duration
	 * related clock behavior are registered with more than one clock : usually
	 * two or three. Each such behavior must be represented by the same object
	 * instance, so that the logic behind a start-end duration or a timer (ie.
	 * clock counting) can be easily implemented. During consecutive calls of
	 * redoClockBehaviour(), some of them may actually concern the same duration
	 * object : on start-finish duration, there will be a call for the start
	 * clock, a second with the end clock, and possibly a third with the
	 * reference clock. The map is there to avoid creating different objects for
	 * the same duration behavior. The key in this map is a concatenation of the
	 * path to the duration model, plus the URIfragment of the duration object
	 * in the model : both of these components are part of the PersistentOptions
	 * object.
	 */
	HashMap<String, DurationClockBehavior> durationBehaviorsCache = new HashMap<String, DurationClockBehavior>();

	private DurationClockBehavior getDurationBehaviorFromCache(String key) {
		return durationBehaviorsCache.get(key);
	}

	private DurationClockBehavior getDurationBehaviorFromCache(Duration duration) {
		String URIFragment = duration.eResource().getURIFragment(duration);
		String key = _durationModelFilename + URIFragment;
		return getDurationBehaviorFromCache(key);
	}

	private void storeDurationBehaviorInCache(String key, DurationClockBehavior behavior) {
		durationBehaviorsCache.put(key, behavior);
	}

	private void storeDurationBehaviorInCache(Duration duration, DurationClockBehavior behavior) {
		String URIFragment = duration.eResource().getURIFragment(duration);
		String key = _durationModelFilename + URIFragment;
		storeDurationBehaviorInCache(key, behavior);
	}

	/**
	 * Called from the GUI when the OK button is pressed. Handles only the
	 * creation of duration related behaviors.
	 */
	public void createDurationBehaviors() {
		if (_durationModelFilename == null || _durationModelFilename.isEmpty()) {
			// Remove all information related to duration behaviors.
			deleteDurationModel();
			ArrayList<AbstractVCDClockBehavior> toRemove = new ArrayList<AbstractVCDClockBehavior>();
			for (AbstractVCDClockBehavior behavior : _behaviorList) {
				if (behavior instanceof DurationClockBehavior) {
					toRemove.add(behavior);
				}
			}
			_behaviorList.removeAll(toRemove);
			for (AbstractVCDClockBehavior behavior : toRemove) {
				removeDurationBehavior((DurationClockBehavior) behavior);
			}
		} else {
			if (_durationModelRoot == null) {
				loadDurationModel();
			}
		}
		if (_durationModelRoot == null) {
			return;
		}
		for (Duration duration : _durationModelRoot.getDurations()) {
			DurationClockBehavior existing = getDurationBehaviorFromCache(duration);
			if (existing == null) {
				if (duration instanceof GenericDuration) {
					GenericDurationClockBehavior behavior = buildClockToClockBehaviour((GenericDuration) duration);
					if (behavior != null) {
						addClockToClockBehavior(behavior,
								duration.eResource().getURIFragment(duration));
						storeDurationBehaviorInCache(duration, behavior);
					}
				} else if (duration instanceof Timer) {
					TimerClockBehavior behavior = buildTimerClockBehavior((Timer) duration);
					if (behavior != null) {
						addTimerBehavior(behavior, duration.eResource().getURIFragment(duration));
						storeDurationBehaviorInCache(duration, behavior);
					}
				}
			}
		}
	}

	private GenericDurationClockBehavior buildClockToClockBehaviour(GenericDuration duration) {
		ConcreteEntity start = ((GenericDuration) duration).getStart();
		ClockEntity startCE = findClockEntity(start);
		ConcreteEntity end = ((GenericDuration) duration).getEnd();
		ClockEntity endCE = findClockEntity(end);
		if (startCE == null || endCE == null) {
			return null;
		}
		ConcreteEntity reference = ((GenericDuration) duration).getRef();
		ClockEntity refCE = null;
		boolean useRef = (reference != null);
		if (useRef) {
			refCE = findClockEntity(reference);
			if (refCE == null) {
				return null;
			}
		}
		boolean displayedOverlap = ((GenericDuration) duration).isDisplayedOverlap();
		int mode;
		if (duration instanceof CommunicationDuration) {
			mode = GenericDurationClockBehavior.MODE_SEND_RECEIVE;
		} else {
			mode = GenericDurationClockBehavior.MODE_START_FINISH;
		}
		GenericDurationClockBehavior behavior = null;
		if (useRef)
			behavior = new GenericDurationClockBehavior(mode, startCE, endCE, refCE,
					displayedOverlap);
		else
			behavior = new GenericDurationClockBehavior(mode, startCE, endCE, displayedOverlap);
		return behavior;
	}

	private TimerClockBehavior buildTimerClockBehavior(Timer duration) {
		int delay = ((Timer) duration).getDelay();
		ConcreteEntity ref = ((Timer) duration).getRef();
		ConcreteEntity start = ((Timer) duration).getStart();
		ClockEntity startCE = findClockEntity(start);
		ClockEntity refCE = findClockEntity(ref);
		if (startCE == null || refCE == null) {
			return null;
		}
		TimerClockBehavior behavior = new TimerClockBehavior(startCE, refCE, delay);
		;
		return behavior;
	}

	public void addClockToClockBehavior(GenericDurationClockBehavior behavior, String fragmentURI) {

		GenericDurationPersistentOptions options = new GenericDurationPersistentOptions(
				_durationModelFilename, fragmentURI);

		addDurationBehavior(behavior, behavior.getStartClock(), options);
		addDurationBehavior(behavior, behavior.getEndClock(), options);
		if (behavior.getRefClock() != null) {
			addDurationBehavior(behavior, behavior.getRefClock(), options);
		}
	}

	public void addTimerBehavior(TimerClockBehavior behavior, String URIfragment) {
		TimerPersistentOptions options = new TimerPersistentOptions(_durationModelFilename,
				URIfragment);

		addDurationBehavior(behavior, behavior.getStartClock(), options);
		addDurationBehavior(behavior, behavior.getRefClock(), options);
	}

	/**
	 * addDurationBehavior method<BR>
	 * This methods adds a behavior to the list of behaviors that the simulation
	 * will have.<BR>
	 * This form is the 'main' one, it adds the behaviors to the
	 * configurationHelper.
	 * 
	 * @param behavior
	 *            The behavior that is going to be added.
	 * @param clockEntity
	 *            This clock.
	 * @param options
	 *            This behavior's persistent options.
	 * @see fr.inria.aoste.timesquare.backend.vcdgenerator.behaviors.DurationClockBehavior
	 * @since 2.0.0
	 */
	private void addDurationBehavior(DurationClockBehavior behavior, ClockEntity clockEntity,
			DurationPersistentOptions options) {

		addDurationBehavior(behavior);

		if (_helper != null)
			_helper.addBehavior(clockEntity, _helper.getAlwaysState(), this.getPluginName(),
					behavior, options);
	}

	private void removeDurationBehavior(DurationClockBehavior behavior) {
		if (_helper == null) {
			return;
		}
		if (behavior instanceof GenericDurationClockBehavior) {
			_helper.removeBehavior(((GenericDurationClockBehavior) behavior).getStartClock(),
					_helper.getAlwaysState(), behavior);
			_helper.removeBehavior(((GenericDurationClockBehavior) behavior).getEndClock(),
					_helper.getAlwaysState(), behavior);
			if (((GenericDurationClockBehavior) behavior).getRefClock() != null) {
				_helper.removeBehavior(((GenericDurationClockBehavior) behavior).getRefClock(),
						_helper.getAlwaysState(), behavior);
			}
		} else if (behavior instanceof TimerClockBehavior) {
			_helper.removeBehavior(((TimerClockBehavior) behavior).getStartClock(),
					_helper.getAlwaysState(), behavior);
			_helper.removeBehavior(((TimerClockBehavior) behavior).getRefClock(),
					_helper.getAlwaysState(), behavior);
		}
	}

	/**
	 * addDurationBehavior method<BR>
	 * This methods adds a behavior to the list of behaviors that the simulation
	 * will process. Due to the way behaviors are associated with ClockEntity
	 * objects, the same behavior is registered 2 or 3 times depending on the
	 * actual duration object specification : 2 or 3 times for GenericDuration
	 * (on the start and end clock, and possibly on the reference one) and 2
	 * times for the TimerDuration (on the start and reference clocks). We must
	 * take care to add the actual behavior object only one time to the behavior
	 * list : doing more will be both useless and will defeat the remove() call
	 * in the {@link #deleteEntity(Entity, DeleteHelper)} function that only
	 * removes one instance from the _behaviorList vector.
	 * 
	 * @param behavior
	 *            The behavior that is going to be added.
	 * @see DurationClockBehavior
	 * @since 2.0.0
	 */
	private void addDurationBehavior(DurationClockBehavior behavior) {
		if (!_behaviorList.contains(behavior))
			_behaviorList.add(behavior);
	}

	/**
	 * redoRelationBehavior method<BR>
	 * <i>Not required as the plug-in has a GUI.</i>.
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public void manageBehavior(ConfigurationHelper helper) {
	}

	@Override
	public ClockBehavior redoClockBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {

		/*
		 * TODO This function performs some quick sanity check on the data found
		 * in the options objects so that it does not build malformed behavior
		 * objects. However, there is no reporting of the results of these
		 * checks, because there is currently no way to do this. It should be
		 * desirable to find a way to report the problems found, otherwise
		 * everything seems Ok for the user, while previously existing behaviors
		 * do not work any more.
		 */
		if (persistentOptions instanceof VCDGeneratorPersistentOptions) {
			VCDGeneratorClockBehavior behavior = ((VCDGeneratorPersistentOptions) persistentOptions)
					.buildClockBehavior();
			ClockEntity clockEntity = getClock(helper, behavior);
			if (clockEntity == null) {
				return null;
			}
			behavior.setClock(clockEntity);
			addVCDGeneratorBehavior(behavior);

			return behavior;
		} // VCDGeneratorClockBehavior specific code

		if (persistentOptions instanceof GenericDurationPersistentOptions) {
			if (_durationModelRoot == null) {
				return null;
			}
			String modelFilename = ((GenericDurationPersistentOptions) persistentOptions)
					.getDurationModelFilename();
			String fragment = ((GenericDurationPersistentOptions) persistentOptions).getID();
			String key = modelFilename + fragment;
			/*
			 * First sanity check : the options contains the name of the
			 * duration model used to setup the behaviors. We check that this
			 * name is the same as the one of from which we just loaded the
			 * model, so that the URIfragment we got from the options may give
			 * us the correct duration object
			 */
			if (modelFilename == null || modelFilename.isEmpty() || _durationModelFilename == null
					|| modelFilename.compareTo(_durationModelFilename) != 0) {
				return null;
			}

			ClockBehavior cbehavior = getDurationBehaviorFromCache(key);
			if (cbehavior != null) {
				return cbehavior;
			}
			EObject dObject = _resource.getEObject(fragment);
			/*
			 * Second sanity check : if the duration model has changed since the
			 * last run of this config, the fragment may reference another
			 * object than the kind we expect.
			 */
			if (dObject == null || !(dObject instanceof GenericDuration)) {
				return null;
			}
			GenericDuration duration = (GenericDuration) dObject;
			GenericDurationClockBehavior behavior = buildClockToClockBehaviour(duration);
			if (behavior == null) {
				return null;
			}
			addDurationBehavior(behavior);
			storeDurationBehaviorInCache(key, behavior);
			return behavior;
		} // GenericDurationClockBehavior specific code

		if (persistentOptions instanceof TimerPersistentOptions) {
			if (_durationModelRoot == null) {
				return null;
			}
			String modelFilename = ((TimerPersistentOptions) persistentOptions)
					.getDurationModelFilename();
			String fragment = ((TimerPersistentOptions) persistentOptions).getID();
			String key = modelFilename + fragment;

			if (modelFilename == null || modelFilename.isEmpty() || _durationModelFilename == null
					|| modelFilename.compareTo(_durationModelFilename) != 0) {
				return null;
			}

			ClockBehavior cbehavior = getDurationBehaviorFromCache(key);
			if (cbehavior != null) {
				return cbehavior;
			}
			EObject dObject = _resource.getEObject(fragment);
			// Sanity check : if the duration model has changed since the last
			// run of this config, the fragment may
			// reference another object than the one we expect.
			if (dObject == null || !(dObject instanceof Timer)) {
				return null;
			}
			Timer duration = (Timer) dObject;
			TimerClockBehavior behavior = buildTimerClockBehavior(duration);
			if (behavior == null) {
				return null;
			}
			addDurationBehavior(behavior);
			storeDurationBehaviorInCache(key, behavior);
			return behavior;
		} // TimerClockBehavior specific code

		return null;
	}

	/**
	 * redoRelationBehavior method<BR>
	 * <i>Unnecessary, this behavior only manages clock behaviors.</i>.
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public RelationBehavior redoRelationBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {
		return null;
	}

	/**
	 * redoRelationBehavior method<BR>
	 * <i>Unnecessary, this behavior only manages clock behaviors.</i>
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public ClockBehavior redoAssertBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {
		return null;
	}

	@Override
	public PersistentOptions getPluginOptions() {
		PlugInPersistentOptions options = new PlugInPersistentOptions(_durationModelFilename,
				_launchVCDViewer);
		return options;
	}

	private void loadDurationModel() {
		if (_durationModelFilename == null || _durationModelFilename.isEmpty()) {
			_durationModelRoot = null;
			return;
		}
		if (_resource == null) {
			_resourceSet = new ResourceSetImpl();
			_resource = _resourceSet.createResource(URI.createPlatformResourceURI(
					_durationModelFilename, true));
		}
		try {
			_resource.load(Collections.EMPTY_MAP);
			EcoreUtil.resolveAll(_resource);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (_resource.getErrors().isEmpty()) {
			_durationModelRoot = (DurationModel) _resource.getContents().get(0);
		} else {
			// TODO: handle errors
		}
	}

	@Override
	public void receivePluginOptions(ConfigurationHelper helper, PersistentOptions persistentOptions)
			throws Throwable {
		if (!(persistentOptions instanceof PlugInPersistentOptions)) {
			return;
		}
		PlugInPersistentOptions options = (PlugInPersistentOptions) persistentOptions;
		String modelFilename = options.get_pathToDurationModel();
		if (modelFilename != null && (!modelFilename.isEmpty())) {
			set_durationModelFilename(modelFilename);
			loadDurationModel();
		}
		_launchVCDViewer = options.is_launchVCDViewer();
	}

	/**
	 * getClock method<BR>
	 * This method returns the ClockEntity associated with the behavior passed
	 * by the user in parameter. The ClockEntity object and the behavior object
	 * must have the same name (as returned by the {@link ClockEntity#getName()}
	 * and {@link VCDGeneratorClockBehavior#getName()} calls on both objects)<BR>
	 * This is needed as the ClockEntity class may not be serialized as part of
	 * the VCDGeneratorPersistentOptions class.
	 * 
	 * @param helper
	 *            The ConfigurationHelper of the behavior manager
	 * @param behavior
	 *            The target behavior on which we require to get the clock.
	 * @return The ClockEntity associated with the <i>behavior</i> parameter.
	 * @since 1.1.0
	 */
	private static ClockEntity getClock(ConfigurationHelper helper,
			VCDGeneratorClockBehavior behavior) {

		for (ClockEntity clock : helper.getClocks()) {
			if (clock.getName().compareTo(behavior.getName()) == 0)
				return clock;
		}
		return null;
	}

	/**
	 * Called directly when the DeleteAll button is pressed in the Launch
	 * configuration GUI. Also called from the
	 * {@link #init(ConfigurationHelper)} function for consistency of
	 * (re)initialization.
	 * 
	 * @see fr.inria.aoste.timesquare.backend.manager.view.View.DeleteAllButtonSelectionAdapter#widgetSelected(SelectionEvent)
	 */
	@Override
	public void clear() {
		_behaviorList.clear();
		deleteDurationModel();
		durationBehaviorsCache.clear();
	}

	private void deleteDurationModel() {
		if (_resourceSet != null) {
			for (Resource resource : _resourceSet.getResources()) {
				resource.unload();
			}
		}
		_resource = null;
		_resourceSet = null;
		_durationModelRoot = null;
		set_durationModelFilename(null);
	}

	/**
	 * deleteEntity method<br>
	 * This method enables the behavior manager to delete multiple behaviors
	 * when the delete button is pushed.
	 * 
	 * @since 1.0.0
	 * @override
	 */
	@Override
	public void deleteEntity(Entity entity, DeleteHelper deletehelper) {
		if (entity.getPersistentOptions() instanceof DurationPersistentOptions) {
			for (ClockBehaviorEntity cbe : deletehelper.getClockBehaviorEntity()) {
				if (cbe.getBehavior() == entity.getBehavior()) {
					deletehelper.deleteEntity(cbe);
				}
			}
		}
		_behaviorList.remove(entity.getBehavior());
	}

	private ScaleCommand scaleCommand;

	/**
	 * beforeExecution method<BR>
	 * This generates the VCD file header, which groups together :
	 * <ul>
	 * <li>The date of the simulation</li>
	 * <li>The version of the generator</li>
	 * <li>The current timescale</li>
	 * </ul>
	 * It then generates the VCD constraints :
	 * <ul>
	 * <li>The hidden constraint</li>
	 * <li>The xmi constraint</li>
	 * <li>The alias constraint</li>
	 * </ul>
	 * Lastly, it generates the relation constraint.<BR>
	 * It also sets the basic parameters options required for the generation.<BR>
	 * <BR>
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public void beforeExecution(ConfigurationHelper helper, IPath folderin, String namefilein,
			ISolverForBackend solver) {

		setOutputFile(folderin, namefilein);
		_myColorAPI = VcdColorPreferences.createColor();
		_scoreBoard = ScoreBoard.getScoreboard(_outputFileName, _myColorAPI);

		fileHeader();

		int i = 0;
		for (AbstractVCDClockBehavior behavior : _behaviorList) {
			/*
			 * Special treatments for behaviors begin here
			 */

			if (behavior instanceof VCDGeneratorClockBehavior) {
				((VCDGeneratorClockBehavior) behavior).setClock(getClock(helper,
						(VCDGeneratorClockBehavior) behavior));
				((VCDGeneratorClockBehavior) behavior).setPulses(_pulses);
				((VCDGeneratorClockBehavior) behavior).setGhosts(_ghosts);
			} // Special treatment for VCDGeneratorClockBehaviors

			/*
			 * Special treatments for behaviors end here
			 */

			// Generic treatment for VCDGenerationClockBehaviors. Also suitable
			// for DurationClockBehaviors.
			behavior.setScoreBoard(_scoreBoard);
			if (behavior.getPCode() == null)
				behavior.setPCode("!" + ++i);
			if (!behavior.isInitialized())
				behavior.initialize();

		}
		for (PhysicalBase lpb : helper.getPhysicalBases()) {
			Reference r = lpb.getRelatedDenseClock();
			if (r instanceof ModelElementReference) {
				EObject eo = ((ModelElementReference) r).getElementRef().get(0);
				String s = AdapterRegistry.getAdapter(eo).getUID(eo);
				if (s != null && s.equals(discretize)) {
					pb = lpb;
					try {
						TimeBase tb = AdapterRegistry.getAdapter(eo).getDiscretyzeByValue(eo)
								.get(0);
						physical = tb;
					} catch (Exception e) {
						physical = new TimeBase(1.0);
					}
				}
			}
		}
		if (pb == null) {
			scaleCommand = new ScaleCommand(0.10d);
		} else {
			scaleCommand = new ScaleCommand(physical.getBase(), physical.getUnitname());//
		}
		_scoreBoard.getVcdModel().addDefinition(scaleCommand);
		createFileCreationThread();
		createRelationConstraint(namefilein);
	}

	/**
	 * end method<BR>
	 * This generates the last part of the VCD : the step where all clocks are
	 * killed.<BR>
	 * This method also erases most Java references and frees memory.
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public void end(ConfigurationHelper helper) {
		_currentStep++;

		int instant = _currentStep * 10;
		// Representation of the end of the simulation
		SimulationCommand sc = _scoreBoard.tick(instant);
		for (AbstractVCDClockBehavior behavior : _behaviorList) {
			behavior.end();
		}
		StepManager sm = new StepManager();
		sm.simCommand = sc;
		sm.fixed = false;
		if (hmism != null)
			hmism.put(_currentStep, sm);
		lsstep.add(sm);
		if (pb != null) {
			updateDate(pb);
		}
		createEndThread();
		_resourceSet = null;
		_myColorAPI = null;
		_scoreBoard = null;
		for (AbstractVCDClockBehavior b : _behaviorList) {
			b.setScoreBoard(null);
		}
		_behaviorList.clear();
		_behaviorList = null;
		hmism.clear();
		lsstep.clear();
		hmism = null;
		lsstep = null;
		_vcdEditor = null;
	}

	/**
	 * aNewStep method<BR>
	 * This generates the steps numbers on the output and updates the VCD
	 * output.
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	public void aNewStep(int step) {
		if (_scoreBoard == null)
			return;
		int date = step;
		_currentStep = step;
		int instant = step * 10;

		StepManager sm = new StepManager();
		SimulationCommand sc = _scoreBoard.tick(instant);
		sm.simCommand = sc;
		sm.fixed = ((pb == null) ? false : HelperFactory.isFixed(pb, getCurrentStep()));
		hmism.put(date, sm);
		lsstep.add(sm);
	}

	@Override
	public void aPostNewStep(int step) {
		createVCDUpdateThread();
	}

	/**
	 * setOutputFile method<BR>
	 * This method sets the _outputFilePath and _outputFileName fields, these
	 * fields specify the location and the name of the file that will be
	 * generated.
	 * 
	 * @param path
	 *            The path to the folder containing the file.
	 * @param name
	 *            The name of the file that will be created (Without the
	 *            extension).
	 * @see IPath
	 * @since 1.0.0
	 */
	private void setOutputFile(IPath path, String name) {

		this._outputFilePath = path;
		this._outputFileName = name;
	}

	TimeScaleCommand tc;

	/**
	 * fileHeader method<BR>
	 * This method generates the header of the VCD file. It is comprised of the
	 * Date, Version and TimeScale of the current session.
	 * 
	 * @since 1.0.0
	 */
	private void fileHeader() {

		_scoreBoard.getVcdModel().addDefinition(new DateCommand(new Date().toString()));
		_scoreBoard.getVcdModel().addDefinition(new VersionCommand(PLUGIN_NAME, VERSION_NAME));
		tc = new TimeScaleCommand();
		tc.set(1, TimeUnit.tick);
		_scoreBoard.getVcdModel().addDefinition(tc);
	}

	/**
	 * createRelationConstraint method<BR>
	 * This method generates the relation constraint of the VCD file, which
	 * specifies the file which contains the relations between the clocks
	 * 
	 * @param namefilein
	 *            The name of the target .ccslrelationmodel file
	 * @since 1.0.0
	 */
	private void createRelationConstraint(String namefilein) {

		ICommentCommand icc = _scoreBoard.getVcdModel().createComment("relation");
		if (icc == null) {
			ErrorConsole
					.printlnError("Warning : Relation Model for Vcd requires : \"fr.inria.aoste.timesquare.vcd.instantrelation\" ");
			return;
		}
		icc.setString(1, namefilein + ".ccslrelationmodel");

		for (EObject relation : _helper.getRelationConstraint())
			icc.newData(relation);
		ConstraintView.getCurrentConstraintView().constraintViewRefresh();
	}

	/**
	 * createFileCreationThread method<BR>
	 * This method creates an instance of the FileCreationThread.
	 * 
	 * @see FileCreationThread
	 * @since 1.0.0
	 */
	private void createFileCreationThread() {

		try {
			Thread thread = new FileCreationThread();
			Display.getDefault().syncExec(thread);
			thread.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * createVCDUpdateThread method<BR>
	 * This method creates an instance of the VCD Updating Thread
	 * 
	 * @see UpdateThread
	 * @since 1.0.0
	 */
	private void createVCDUpdateThread() {

		if (_scoreBoard != null) {
			try {
				Thread thread = new UpdateThread();
				Display.getDefault().syncExec(thread);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * createEndThread method<BR>
	 * This method creates an instance of the End thread.
	 * 
	 * @since 1.0.0
	 * @see EndThread
	 */
	private void createEndThread() {

		try {
			Thread thread = new EndThread();
			Display.getDefault().syncExec(thread);
			thread.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * finalize method<BR>
	 * The aim of this override is to ensure that there won't be any memory
	 * leaks due to the scoreBoard.
	 * 
	 * @override
	 * @since 1.0.0
	 */
	@Override
	protected void finalize() throws Throwable {
		// System.out.println("Finalize VCDGeneratorManager");
		if (_scoreBoard != null)
			ScoreBoard.removeScoreboard(_scoreBoard);
		super.finalize();
	}

	/**
	 * 
	 * UpdateThread class<BR>
	 * This class allows the instantiation of threads that refresh the VCD
	 * Editor view using updating methods in order for the view to be up to date
	 * with the steps representation
	 * 
	 * @author ybondue
	 * @version 1.0.0
	 * @since Stage DUT Yann Bondue 2011
	 * @see UpdateThread#run()
	 * 
	 */
	private class UpdateThread extends Thread {

		/**
		 * Standard Thread constructor.
		 */
		protected UpdateThread() {
			super("Update VCD Thread");
		}

		/**
		 * run method<BR>
		 * This method operates updating processes to the VCD Editor.
		 * 
		 * @override
		 * @since 1.0.0
		 */
		@Override
		public void run() {
			if (_scoreBoard != null) {
				int currentSize = _scoreBoard.getSize();
				try {
					if (_vcdEditor != null)
						_vcdEditor.update2(previousSize, currentSize, false);
				} catch (Exception e) {
					e.printStackTrace();
				}
				previousSize = currentSize;
			}
		}
	}

	/**
	 * 
	 * FileCreationThread class<BR>
	 * This thread creates the file at the location specified within the
	 * _outputFilePath and _outputFileName fields, it also sets the basic
	 * options for the VCDEditor field.<BR>
	 * To set the fields used within this method, see the setOutputFile method.
	 * 
	 * @author ybondue
	 * @version 1.0.0
	 * @since Stage DUT Yann Bondue 2011
	 * @see VCDGeneratorManager#setOutputFile(IPath, String)
	 * @see FileCreationThread#run()
	 * 
	 */
	private class FileCreationThread extends Thread {

		/**
		 * Standard Thread constructor.
		 */
		protected FileCreationThread() {
			super("File Creation Thread");
		}

		/**
		 * run method<BR>
		 * This method creates the VCD file and sets the basic parameters of the
		 * VCD Editor.
		 * 
		 * @override
		 * @since 1.0.0
		 */
		@Override
		public void run() {
			/*
			 * TODO The creation of the VCD viewer should be optional and there
			 * should be a related settings in the GUI. On some long
			 * simulations, the VCD display consumes too much time and renders
			 * the whole application quite unusable.
			 */
			IFile file = PluginHelpers.touchFile(_outputFilePath, _outputFileName + ".vcd");
			_vcdEditor = VcdMultiPageEditor.createNewVcdMultiPageEditor(_scoreBoard.getVcdModel(),
					file, _myColorAPI);
			if (_vcdEditor != null) {
				_vcdEditor.setSimulation(true);
				_vcdEditor.getTraceCollector().setPartial(true);
				_vcdEditor.setGhostMode(Mode.bool2Mode(_ghosts));
				_vcdEditor.setSimulationProgress(2);
			}
		}
	}

	/**
	 * 
	 * EndThread class<BR>
	 * This class allows the instantiation of a thread which goal is to erase
	 * Java references and free allocated memory, it also sets the VCD Editor in
	 * its ending phase.
	 * 
	 * @author ybondue
	 * @version 1.0.0
	 * @since Stage DUT Yann Bondue 2011
	 * @see EndThread#run()
	 * 
	 */
	private class EndThread extends Thread {

		/**
		 * Standard Thread constructor.
		 */
		protected EndThread() {
			super("End VCD Thread");
		}

		/**
		 * run method<BR>
		 * This method operates memory freeing processes.
		 * 
		 * @override
		 * @since 1.0.0
		 */
		@Override
		public void run() {
			try {
				if (_vcdEditor != null) {
					_vcdEditor.setSimulationProgress(6);
					_vcdEditor.syncModel2Text();
					_vcdEditor.setFocus();
					int currentSize = _scoreBoard.getSize();
					_vcdEditor.update2(previousSize, currentSize, true);
					if (_vcdEditor.getTraceCollector() != null)
						_vcdEditor.getTraceCollector().setPartial(false);
					_vcdEditor.setSimulation(false);
					_vcdEditor.doSave(null);
					_vcdEditor.vcdMultiPageEditorRefresh();
				}
				ScoreBoard.removeScoreboard(_scoreBoard);
				_scoreBoard = null;
				_myColorAPI = null;
				if (_vcdEditor != null)
					_vcdEditor.setActiveVCDPage();
			} catch (Throwable t) {
				t.printStackTrace();
			}
			_scoreBoard = null;
			_myColorAPI = null;
		}
	}

	PhysicalBase pb = null;
	TimeBase physical = null;

	private static class StepManager {
		SimulationCommand simCommand;
		boolean fixed;
	}

	private HashMap<Integer, StepManager> hmism = new HashMap<Integer, StepManager>();
	private List<StepManager> lsstep = new ArrayList<StepManager>();

	public int updateDate(PhysicalBase pb) {
		try {
			scaleCommand.setBase(physical.getBase() / 100);
			Boolean firstSimulationFixed = lsstep.get(0).fixed;

			int intervalLenght = HelperFactory.getMaxInterval(pb) + 1;
			// System.err.println(" Linearize  ..." + (intervalLenght-1));
			int intervalLenghtRef = intervalLenght;
			if (intervalLenght % 10 != 0) {
				intervalLenght = intervalLenght / 10 + 10;
			}

			int scaleUnit = 0;
			if (intervalLenghtRef < 6) {
				scaleUnit = intervalLenght / 5;
			} else {
				scaleUnit = intervalLenght / 10;
			}

			int currentSimulationTime = -1;
			int currentIntervalUnit = 0;

			for (StepManager sm : lsstep) {
				if (sm == null) {
					continue;
				}
				if (firstSimulationFixed) {
					if (currentSimulationTime == -1) {
						currentSimulationTime += 1;
						sm.simCommand.setTime(0);
					} else {
						if (sm.fixed) {
							currentIntervalUnit += intervalLenght;
							currentSimulationTime = currentIntervalUnit;
							sm.simCommand.setTime(currentSimulationTime * 10);
						} else {
							currentSimulationTime += scaleUnit;
							sm.simCommand.setTime(currentSimulationTime * 10);
						}
					}
				} else {
					if (currentSimulationTime == -1) {
						currentSimulationTime += 1;
						sm.simCommand.setTime(0);

					} else {
						if (sm.fixed) {
							currentIntervalUnit += intervalLenght;
							currentSimulationTime = currentIntervalUnit;
							sm.simCommand.setTime(currentSimulationTime * 10);
						} else {
							currentSimulationTime += scaleUnit;
							sm.simCommand.setTime(currentSimulationTime * 10);
						}
					}

				}

			}

		} catch (Throwable e) {
			e.printStackTrace();
		}

		return 0;

		//
		//
		// try {
		// int n = HelperFactory.getMaxInterval(pb);
		// boolean firstfixed=true;
		// if( !lsstep.get(0).fixed)
		// {
		// int s=lsstep.get(0).ls.size();
		// if (s==n)
		// n++;
		// firstfixed=false;
		// }
		// int base = ((n + 1) * 3 + 1);
		// int count = 0;
		// int count2 = 0;
		//
		// BigDecimal physicalbiBigDecimal =
		// BigDecimal.valueOf(physical.getBase());
		//
		// //System.out.println(physicalbiBigDecimal.scale());
		// int scale = physicalbiBigDecimal.scale();
		// BigDecimal bg = BigDecimal.valueOf(1, scale);
		// // System.out.println(bg + " " + (physicaldouble / bg.doubleValue())
		// + " " + physicalbiBigDecimal.divide(bg));
		// int x = physicalbiBigDecimal.divide(bg).intValueExact();
		//
		// if (base > x)
		// {
		// while (base >x )
		// {
		// x= x*10;
		// scale++;
		// }
		// }
		//
		//
		// int delta = (x /base );
		// int delta2 = delta*2;
		// if (physical.isStandardUnit())
		// {
		// tc.set(scale);
		// }
		// else
		// {
		// scaleCommand.setBase(scaleCommand.getBase()/x);
		// }
		// System.err.println(" Linearize  ..." + n + ": " + x);
		// for (StepManager sm : lsstep) {
		// if (sm == null)
		// continue;
		// if (sm.fixed) {
		// if ((count!=0) || firstfixed)
		// {
		// count = count + x;
		// }
		// else
		// {
		// count = count2+delta2;
		// }
		// if (count < count2) {
		// System.err.println(" Error ...");
		// }
		// count2 = count + delta2;
		// if (sm.ls.size() >= 1)
		// sm.ls.get(0).setTime(count);
		// if (sm.ls.size() >= 2)
		// sm.ls.get(1).setTime(count + delta);
		//
		// } else {
		// if (sm.ls.size() >= 1)
		// sm.ls.get(0).setTime(count2);
		// if (sm.ls.size() >= 2)
		// sm.ls.get(1).setTime(count2 + delta);
		// count2 = count2 + delta2;
		// }
		//
		// }
		// } catch (Throwable e) {
		// e.printStackTrace();
		// }
		//
		// return 0;
	}

	@Override
	public VCDBehaviorConfigurator getConfigurator(ConfigurationHelper configurationHelper) {

		return new VCDBehaviorConfigurator(configurationHelper, this);

	}

	public boolean is_launchVCDViewer() {
		return _launchVCDViewer;
	}

	public void set_launchVCDViewer(boolean _launchVCDViewer) {
		this._launchVCDViewer = _launchVCDViewer;
	}

}
