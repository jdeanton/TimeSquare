/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.obeoviewpointanimator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.widgets.Display;

import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertActivationState;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.DeleteHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;
import org.eclipse.sirius.viewpoint.DRepresentation;
import org.eclipse.sirius.viewpoint.DRepresentationDescriptor;
//import org.eclipse.sirius.viewpoint.DRepresentationDescriptor;
import org.eclipse.sirius.viewpoint.DRepresentationElement;
import org.eclipse.sirius.viewpoint.DView;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;

public class ObeoViewpointAnimatorBehaviorManager extends BehaviorManager {

	private String _iardFilePath = null;
	private IFile _airdIFile = null;
	private Session _createdSession = null;
	final private List<AnimateTicksBehaviour> behaviorList = new ArrayList<AnimateTicksBehaviour>();
	final private List<AnimateAssertBehaviour> assertBehaviorList = new ArrayList<AnimateAssertBehaviour>();

	private CCSLInfo ccslhelper = null;

	@Override
	public boolean isActivable(ReportMessage rm, CCSLInfo _ccslhelper) {
//		ccslhelper = _ccslhelper;
//		if (ccslhelper.haveClockWithReferenceTo(UMLPackage.eINSTANCE.getElement())) {
//			cachevalidation.clear();
//			return true;
//		}
//		rm.setMessage("No reference to a UML model");
		return true;
	}

	/***
	 * Gestion d'erreur ==> pas de fichier aird definie
	 * @return Error Message if no valid .aird file, null otherwise
	 */
	@Override
	public String validate() {
		if (_airdIFile == null) {
			return "No *.aird File  ";
		}
		if (!_airdIFile.exists()) {
			return "File not found :"+_airdIFile.getName();
		}
		if ( !isValidAirdFile(_airdIFile))
		{
			return "File aird doesn't have any link with the specification";
		}
		return super.validate();
	}

	public String getDiFileString() {
		return _iardFilePath;
	}

	public IFile getAirdIFile() {
		return _airdIFile;
	}

	public void setAird(String s) {
		_iardFilePath = s;
		IWorkspaceRoot iWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = iWorkspaceRoot.findMember(_iardFilePath);
		if (resource instanceof IFile)
			_airdIFile = (IFile) resource;
		else
			_airdIFile = null;
	}

	public void setAird(IFile f) {
		_airdIFile = f;
		if (f == null) {
			_iardFilePath = null;
			return;
		}
		_iardFilePath = f.getFullPath().toString();
	}
	
	private HashMap<String, Boolean> cachevalidation=new HashMap<String, Boolean>();

	public boolean isValidAirdFile(String file) {
		try {
			if( cachevalidation.containsKey(file))
					return cachevalidation.get(file).booleanValue();
			ResourceSet rs = new ResourceSetImpl();
			URI uri = URI.createPlatformResourceURI(file, true);
			Resource r = rs.getResource(uri, true);
			r.load(Collections.EMPTY_MAP);
			try {
				EcoreUtil.resolveAll(rs);
			} catch (Exception e) {

			}
			for (Resource resource : rs.getResources()) {
				URI _uri = resource.getURI();
				if (ccslhelper.getMappingURIReferencedObject().keySet().contains(_uri))
				{
					cachevalidation.put(file, true);
					return true;
				}
			}
			cachevalidation.put(file, false);
			return false;
		} catch (Exception e) {
			cachevalidation.put(file, false);
			return false;
		}		
	}

	public boolean isValidAirdFile(IFile file) {
		if (file != null) {
			return true; // isValidAirdFile(file.getFullPath().toString());
		}
		return true;
	}

	public void end(ConfigurationHelper helper) {
		try {
			Display.getDefault().syncExec(new RunnableEnd());						
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEntity(Entity entity, DeleteHelper deletehelper) {
		behaviorList.remove(entity.getBehavior());
	}

	@Override
	public void clear() {
		behaviorList.clear();
		_iardFilePath = null;
		_airdIFile = null;
	}

	public String _getid() {
		return "obeoviewpointanimator";
	}

	public String getPluginName() {
		return "Obeo ViewPoint Animator";
	}

	public PersistentOptions getPluginOptions() {
		if (_iardFilePath != null)
			return new ObeoViewpointAnimatorPluginPersistentOptions(_iardFilePath);
		return null;

	}
	
	public void beforeExecution(ConfigurationHelper helper, IPath folderin, String namefilein, ISolverForBackend solver) {
		try {
			Display.getDefault().syncExec(new RunnableStart());						
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}	
	
	private class RunnableEnd implements Runnable {

		@Override
		public void run() {
			for (AnimateTicksBehaviour behavior : behaviorList) {
				behavior.finish();
			}
//			for (AnimateAssertBehaviour assertBehavior : assertBehaviorList) {
//				assertBehavior.finish();
//			}
		}
		
	}
	private final class RunnableStart implements Runnable {
		public void run() {	
			IWorkspaceRoot iWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			_airdIFile = (IFile) iWorkspaceRoot.findMember(_iardFilePath);
			if (_airdIFile == null)
				return;
			
			URI sessionResourceURI = URI.createPlatformResourceURI(_iardFilePath, true);
			_createdSession = SessionManager.INSTANCE.getExistingSession(sessionResourceURI);
			_createdSession.open(new NullProgressMonitor());
			
			for (AnimateTicksBehaviour behavior : behaviorList) {
				List<EObject> fromCCSL_semanticObjects = behavior._ce.getReferencedElement();
				ArrayList<DRepresentation> allRepresentations = new ArrayList<DRepresentation>();
				for(DView v : _createdSession.getOwnedViews()){
				
					for (DRepresentationDescriptor rd : v.getOwnedRepresentationDescriptors()){
						allRepresentations.add(rd.getRepresentation());
					}
				}
				ArrayList<DRepresentationElement> allRepresentationElements = new ArrayList<DRepresentationElement>();
				for (DRepresentation dRepresentation : allRepresentations) {
					allRepresentationElements.addAll(dRepresentation.getRepresentationElements());
				}
				
				ArrayList<DRepresentationElement> semanticObjectRepresentationElements = new ArrayList<DRepresentationElement>();

				for (EObject semanticObject : fromCCSL_semanticObjects) {
					if (semanticObject instanceof EOperation){
						continue;
					}
					for (DRepresentationElement dRepresentationElement : allRepresentationElements) {
						for (EObject reprObject : dRepresentationElement.getSemanticElements()) {
//TODO this is so ugly to compare if the represented object and the semantic object are the same ! !!!
//FIXME: how to compare properly two objects which are not the same in memory ?
							String reprObjectString = "";
							int indexOfSpace = reprObject.toString().indexOf(" ");
							if(indexOfSpace != -1){
								reprObjectString = (String) reprObject.toString().subSequence(indexOfSpace, reprObject.toString().length()-1);
							}
//							String reprObjectContainerString = "";//(String) reprObject.eContainer().toString().subSequence(reprObject.eContainer().toString().indexOf(" "), reprObject.eContainer().toString().length()-1);
							String semanticObjectString="";
							int indexOfSpace2 = semanticObject.toString().indexOf(" ");
							if(indexOfSpace2 != -1){
								semanticObjectString = (String) semanticObject.toString().subSequence(indexOfSpace2, semanticObject.toString().length()-1);
							}
//							String semanticObjectContainerString = (String) semanticObject.eContainer().toString().subSequence(semanticObject.eContainer().toString().indexOf(" "), semanticObject.eContainer().toString().length()-1);
//							if (reprObjectContainerString==null){
//								reprObjectContainerString="";
//							}
//							if (semanticObjectContainerString==null){
//								semanticObjectContainerString="";
//							}
							if (reprObjectString.compareTo(semanticObjectString) == 0 ){//&& reprObjectContainerString.compareTo(semanticObjectContainerString)==0 ){
								semanticObjectRepresentationElements.add(dRepresentationElement);
							}
						}
					}
				}
				behavior.setRepresentation(semanticObjectRepresentationElements);	 	
				behavior.start(_createdSession.getTransactionalEditingDomain());
			}
//			//TODO: assert
//			for (AnimateAssertBehaviour assertBehavior : assertBehaviorList) {
//				assertBehavior.setEditPart(_diagramEditPart);	 	
//				assertBehavior.start();
//			}
		}
	}

	

	public void manageBehavior(ConfigurationHelper helper) {
		try {
			loop1: for (ClockEntity ce : helper.getClocks()) {
				//TODO: fix this ugly comparison !!!
				if (ce.getModelElementReference().getElementRef().size() != 3) //not in main block
					continue loop1;
				for (AnimateTicksBehaviour b : behaviorList) { //already in the list
					if (b._ce == ce)
						continue loop1;
				}
				AnimateTicksBehaviour atb = new AnimateTicksBehaviour(ce);
				behaviorList.add(atb);
				helper.addBehavior(ce, helper.getTicksState(), getPluginName(), atb,
						new ObeoViewpointAnimatorPersistentOptions(ce.getID()));
			}
		
			for(ClockEntity ae: helper.getAssert()){
				AnimateAssertBehaviour aab = new AnimateAssertBehaviour(ae);
				assertBehaviorList.add(aab);
				boolean[] assertState={false,true};
				helper.addBehavior(ae, new AssertActivationState(assertState), getPluginName(), aab,
						new ObeoViewpointAnimatorPersistentOptions(ae.getID()));
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public BehaviorConfigurator<?> getConfigurator(ConfigurationHelper configurationHelper) {
	
		return new ObeoViewpointAnimatorBehaviorConfigurator(configurationHelper, this);
	}

	public void receivePluginOptions(ConfigurationHelper helper, PersistentOptions persistentOptions) throws Throwable {
		if (persistentOptions instanceof ObeoViewpointAnimatorPluginPersistentOptions) {
			setAird(((ObeoViewpointAnimatorPluginPersistentOptions) persistentOptions).get_di2FilePath());
		}
		return;
	}

	public AnimateTicksBehaviour redoClockBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		AnimateTicksBehaviour cl = null;
		if (persistentOptions instanceof ObeoViewpointAnimatorPersistentOptions) {
			ClockEntity ce = helper.getClock(((ObeoViewpointAnimatorPersistentOptions) persistentOptions).getID());
			cl = new AnimateTicksBehaviour(ce);
			behaviorList.add(cl);
		}
		return cl;

	}

	public ClockBehavior redoAssertBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		AnimateAssertBehaviour aab = null;
		if (persistentOptions instanceof ObeoViewpointAnimatorPersistentOptions) {
			ClockEntity ae = null;
			for(ClockEntity anAe : helper.getAssert()){
				if (anAe.getID().compareTo(((ObeoViewpointAnimatorPersistentOptions) persistentOptions).getID())==0){
					ae=anAe;
				}
			}
			aab = new AnimateAssertBehaviour(ae);
			assertBehaviorList.add(aab);
		}
		return aab;
	}

	public RelationBehavior redoRelationBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		return null;

	}
	
	
	/**
	 * 
	 * @return
	 */
	protected boolean [] test_ElementUMLFound()
	{
		boolean tb[]= new boolean[behaviorList.size()];
		for( int i=0;i<behaviorList.size();i++)
		{
			tb[i]=(behaviorList.get(i).get_ce().getReferencedElement().size()>0);
		}
		return tb;
	}

	
	

	
}
