/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.backend.obeoviewpointanimator;

import java.util.ArrayList;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.sirius.diagram.BorderedStyle; 
import org.eclipse.sirius.diagram.ContainerStyle;
import org.eclipse.sirius.diagram.DEdge;
import org.eclipse.sirius.diagram.DNode;
import org.eclipse.sirius.diagram.DNodeContainer;
import org.eclipse.sirius.diagram.DNodeList;
import org.eclipse.sirius.diagram.DiagramFactory;
import org.eclipse.sirius.diagram.EdgeStyle;
import org.eclipse.sirius.diagram.Ellipse;
import org.eclipse.sirius.diagram.FlatContainerStyle;
import org.eclipse.sirius.diagram.Lozenge;
import org.eclipse.sirius.diagram.NodeStyle;
import org.eclipse.sirius.diagram.ShapeContainerStyle;
import org.eclipse.sirius.diagram.Square;
import org.eclipse.sirius.viewpoint.DRepresentationElement;
import org.eclipse.sirius.viewpoint.RGBValues;
//import org.eclipse.sirius.viewpoint.BorderedStyle;
//import org.eclipse.sirius.viewpoint.ContainerStyle;
//import org.eclipse.sirius.viewpoint.DEdge;
//import org.eclipse.sirius.viewpoint.DNode;
//import org.eclipse.sirius.viewpoint.DNodeContainer;
//import org.eclipse.sirius.viewpoint.DNodeList;
//import org.eclipse.sirius.viewpoint.DRepresentationElement;
//import org.eclipse.sirius.viewpoint.EdgeStyle;
//import org.eclipse.sirius.viewpoint.Ellipse;
//import org.eclipse.sirius.viewpoint.FlatContainerStyle;
//import org.eclipse.sirius.viewpoint.Lozenge;
//import org.eclipse.sirius.viewpoint.NodeStyle;
//import org.eclipse.sirius.viewpoint.RGBValues;
//import org.eclipse.sirius.viewpoint.ShapeContainerStyle;
//import org.eclipse.sirius.viewpoint.Square;
import org.eclipse.sirius.viewpoint.Style;
import org.eclipse.sirius.viewpoint.ViewpointFactory;

import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehaviorAgain;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter.EventEnumerator;

public class AnimateTicksBehaviour implements ClockBehaviorAgain {

	private final static class ChangeColor extends RecordingCommand {
		private int red=0,green=0,blue=0;
		private RGBValues color;
		public ChangeColor(TransactionalEditingDomain domain,RGBValues c, int R, int G, int B ) {
			super(domain);
			red=R;
			green=G;
			blue=B;
			color= RGBValues.create(c.getRed(), c.getGreen(), c.getBlue());
		}

		@Override
		protected void doExecute() {
			color = RGBValues.create(red, green, blue);
			return;
		}
		
	}
	
	private final static class ChangeBorderCommand extends RecordingCommand {
		private int _borderSize=1;
		private ContainerStyle _Cstyle=null;
		private EdgeStyle _Estyle = null;
		private NodeStyle _Sstyle=null;
		private DRepresentationElement _repr=null;
		private RGBValues _newColor = null;
		private RGBValues _backGroundColor = null;
		public ChangeBorderCommand(TransactionalEditingDomain domain, DRepresentationElement repr, RGBValues newColor, int newBorderSize) {
			super(domain);
			Style originalStyle = repr.getStyle();
			if (originalStyle instanceof EdgeStyle){
				_borderSize = newBorderSize;
				_Estyle = DiagramFactory.eINSTANCE.createEdgeStyle();
				_newColor = newColor;
				_repr = repr;
				return;
			}
			if(originalStyle instanceof ShapeContainerStyle){
				_borderSize = newBorderSize;
				_Cstyle = DiagramFactory.eINSTANCE.createShapeContainerStyle();
				_newColor = newColor;
				_repr = repr;
				return;
			}
			if(originalStyle instanceof FlatContainerStyle){
				_borderSize = newBorderSize;
				_Cstyle = DiagramFactory.eINSTANCE.createFlatContainerStyle();
				_backGroundColor =((FlatContainerStyle) originalStyle).getBackgroundColor();
				_newColor = newColor;
				_repr = repr;
				return;
			}
			if(originalStyle instanceof ContainerStyle){
				_borderSize = newBorderSize;
				_Cstyle = DiagramFactory.eINSTANCE.createShapeContainerStyle();
				_newColor = newColor;
				_repr = repr;
				return;
			}
			if(originalStyle instanceof Square){
				_borderSize = newBorderSize;
				_Sstyle = DiagramFactory.eINSTANCE.createSquare();
				_backGroundColor = ((Square) originalStyle).getColor();
				_newColor = newColor;
				_repr = repr;
				return;
			}
			if(originalStyle instanceof Ellipse){
				_borderSize = newBorderSize;
				_Sstyle = DiagramFactory.eINSTANCE.createEllipse();
				_backGroundColor  =((Ellipse) originalStyle).getColor();
				_newColor = newColor;
				_repr = repr;
				return;
			}
			if(originalStyle instanceof Lozenge){
				_borderSize = newBorderSize;
				_Sstyle = DiagramFactory.eINSTANCE.createLozenge();
				_backGroundColor  =((Lozenge) originalStyle).getColor();
				_newColor = newColor;
				_repr = repr;
				return;
			}
			if(originalStyle instanceof DNode){
				_borderSize = newBorderSize;
				_Sstyle = DiagramFactory.eINSTANCE.createEllipse();
				_newColor = newColor;
				_repr = repr;
				return;
			}
			
			
		}

		@Override
		protected void doExecute() {
			if(_repr instanceof DNodeList){
				_Cstyle.setBorderSize(_borderSize);
				_Cstyle.setBorderColor(_newColor);
				((DNodeList)_repr).setOwnedStyle(_Cstyle);
				return;
			}
			if(_repr instanceof DNodeContainer){
				_Cstyle.setBorderSize(_borderSize);
				_Cstyle.setBorderColor(_newColor);
				if(_Cstyle instanceof ShapeContainerStyle){
					((ShapeContainerStyle)_Cstyle).setBackgroundColor(_backGroundColor);
				}else{
					((FlatContainerStyle)_Cstyle).setBackgroundColor(_backGroundColor);
				}
				((DNodeContainer)_repr).setOwnedStyle(_Cstyle);
				return;
			}

			if (_repr instanceof DNode ){
				_Sstyle.setBorderSize(_borderSize);
				_Sstyle.setBorderColor(_newColor);
				if(_Sstyle instanceof Square){
					((Square)_Sstyle).setColor(_backGroundColor);
				}else{
					if(_Sstyle instanceof Ellipse){
						((Ellipse)_Sstyle).setColor(_backGroundColor);
					}else{
						if(_Sstyle instanceof Lozenge){
							((Lozenge)_Sstyle).setColor(_backGroundColor);
						}
					}
				}
				((DNode)_repr).setOwnedStyle(_Sstyle);
				return;
			}
			if (_repr instanceof DEdge){
				_Estyle.setSize(_borderSize);
				_Estyle.setStrokeColor(_newColor);
				((DEdge)_repr).setOwnedStyle(_Estyle);
				return;
			}
		}
	};

	private final static class RestoreStyleCommand extends RecordingCommand {
		private Style _style=null;
		private DRepresentationElement _repr=null;
		public RestoreStyleCommand(TransactionalEditingDomain domain, DRepresentationElement repr, Style style) {
			super(domain);
			_style = style;
			_repr = repr;
		}

		@Override
		protected void doExecute() {
			if(_repr instanceof DNodeList){
				((DNodeList)_repr).setOwnedStyle((ContainerStyle) _style);
				return;
			}
			if(_repr instanceof DNodeContainer){
				
				if(_style instanceof ShapeContainerStyle){
					RGBValues color = ((ShapeContainerStyle)_repr.getStyle()).getBackgroundColor();
					((ShapeContainerStyle)_style).setBackgroundColor(color);
				}
				if(_style instanceof FlatContainerStyle){
					RGBValues color = ((FlatContainerStyle)_repr.getStyle()).getBackgroundColor();
					((FlatContainerStyle)_style).setBackgroundColor(color);
				}
				((DNodeContainer)_repr).setOwnedStyle((ContainerStyle) _style);
				return;
				
			}
			if(_repr instanceof DNode){
				if(_style instanceof Square){
					RGBValues color = ((Square)_repr.getStyle()).getColor();
					((Square)_style).setColor(color);
				}
				if(_style instanceof Ellipse){
					RGBValues color = ((Ellipse)_repr.getStyle()).getColor();
					((Ellipse)_style).setColor(color);
				}
				if(_style instanceof Lozenge){
					RGBValues color = ((Lozenge)_repr.getStyle()).getColor();
					((Lozenge)_style).setColor(color);
				}
				((DNode)_repr).setOwnedStyle((NodeStyle) _style);
				return;
			}
			if(_repr instanceof DEdge){
				((DEdge)_repr).setOwnedStyle((EdgeStyle) _style);
				return;
			}
		}
	};
	
	
	private int _clockCounter = 0;
	private ArrayList<DRepresentationElement> _representations = new ArrayList<DRepresentationElement>();
	private ArrayList<Style> _originalStyle = null;
	private TransactionalEditingDomain _ted = null; 
//	private Comment _comment = null;
//	private String _schedule = "";
	private TraceHelper _helper = null;
	final ClockEntity _ce;


	private EventEnumerator _ek = null;
	
	public AnimateTicksBehaviour(ClockEntity ce) {
		super();
		_ce = ce;		
		_ek=AdapterRegistry.getAdapter(_ce.getClock()).getEventkind(_ce.getClock());
		
	}

	public String getDescription() {	
		return "Obeo Viewpoint Animator";
	}


	public void setRepresentation(ArrayList<DRepresentationElement> representations) {
		this._representations = representations;
		_originalStyle = new ArrayList<Style>();
	}
	

	public void start(TransactionalEditingDomain ted) {
		_ted = ted;
		if (_representations == null)
			return;
		
		for (DRepresentationElement repr : _representations)
		{
			_originalStyle.add(repr.getStyle());
		}
	}
	
	private void changeBorderSize(DRepresentationElement repr, RGBValues newColor, int newVBorderSize){
		if (repr instanceof DNodeList || repr instanceof DNode || repr instanceof DNodeContainer || repr instanceof DEdge)
		{
			//TODO: avoid creation of object each time. Modify it instead
			RecordingCommand doChangeBorderSize = new ChangeBorderCommand(_ted, repr,newColor, newVBorderSize);
			_ted.getCommandStack().execute(doChangeBorderSize);
		}
	}

	boolean receiveDone =false;
	public void run(TraceHelper helper) {
		if (_representations.size()>0) {
			
			_helper = helper;
			_clockCounter = helper.getClockState().getCounter();
			RGBValues highlightedColor = ViewpointFactory.eINSTANCE.createRGBValues();
			RecordingCommand doChangeColor = new ChangeColor(_ted, highlightedColor,255,0,0);
			_ted.getCommandStack().execute(doChangeColor);
			if (_ce.getReferencedElement().size() > 0)
			{
				for (DRepresentationElement repr : _representations)
				{
					switch (_ek) {
					case START:
						if (_originalStyle.get(_representations.indexOf(repr)) != null && repr.getStyle().equals(_originalStyle.get(_representations.indexOf(repr)))){
							doChangeColor = new ChangeColor(_ted, highlightedColor,255,0,0);
							_ted.getCommandStack().execute(doChangeColor);
							changeBorderSize(repr, highlightedColor, 7);
						}else{
							RGBValues newColor = ViewpointFactory.eINSTANCE.createRGBValues();
							doChangeColor = new ChangeColor(_ted, highlightedColor,255,20,0);
							_ted.getCommandStack().execute(doChangeColor);
							changeBorderSize(repr, newColor, 7);
						}
						break;
	
					case SEND:
						if (_originalStyle.get(_representations.indexOf(repr)) != null && repr.getStyle().equals(_originalStyle.get(_representations.indexOf(repr)))){
							doChangeColor = new ChangeColor(_ted, highlightedColor,0,255,0);
							_ted.getCommandStack().execute(doChangeColor);
							changeBorderSize(repr, highlightedColor, 7);
						}else{
							RGBValues newColor = ViewpointFactory.eINSTANCE.createRGBValues();
							doChangeColor = new ChangeColor(_ted, highlightedColor,20,255,0);
							_ted.getCommandStack().execute(doChangeColor);
							changeBorderSize(repr, newColor, 7);
						}
						break;
	
					case PRODUCE:
						if (_originalStyle.get(_representations.indexOf(repr)) != null && repr.getStyle().equals(_originalStyle.get(_representations.indexOf(repr)))){
							doChangeColor = new ChangeColor(_ted, highlightedColor,127,0,0);
							_ted.getCommandStack().execute(doChangeColor);
							changeBorderSize(repr, highlightedColor, 7);
						}else{
							doChangeColor = new ChangeColor(_ted, highlightedColor,127,20,0);
							_ted.getCommandStack().execute(doChangeColor);
							changeBorderSize(repr, highlightedColor, 7);
						}
						break;
	
					case FINISH:
						Style s = _originalStyle.get(_representations.indexOf(repr));
						RecordingCommand doRestoreStyle = new RestoreStyleCommand(_ted, repr,s);
						_ted.getCommandStack().execute(doRestoreStyle);
						break;
	
					case CONSUME:
						s = _originalStyle.get(_representations.indexOf(repr));
						doRestoreStyle = new RestoreStyleCommand(_ted, repr,s);
						_ted.getCommandStack().execute(doRestoreStyle);
						break;
	
					case RECEIVE:
						
						
						if (_originalStyle.get(_representations.indexOf(repr)) != null && repr.getStyle().equals(_originalStyle.get(_representations.indexOf(repr)))){
							doChangeColor = new ChangeColor(_ted, highlightedColor,0,0,255);
							_ted.getCommandStack().execute(doChangeColor);
							changeBorderSize(repr, highlightedColor, 7);
						}else{
							RGBValues newColor = ViewpointFactory.eINSTANCE.createRGBValues();
							doChangeColor = new ChangeColor(_ted, highlightedColor,0,255,30);
							_ted.getCommandStack().execute(doChangeColor);
							changeBorderSize(repr, newColor, 7);
						}
						receiveDone = true;
						
						break;
					case UNDEFINED:
						Style style = null;
						try{
							style = _representations.get(_originalStyle.indexOf(repr)).getStyle();
						}catch(ArrayIndexOutOfBoundsException e){
							style =repr.getStyle();							
						}
						//changer pour faire, si il existe une méthode qui fait setColor(), alors.....
						if (style instanceof Square){
							doChangeColor = new ChangeColor(_ted, highlightedColor,
									Math.abs(255 -((Square)style).getColor().getGreen()),
									Math.abs(255 -((Square)style).getColor().getRed()),
									Math.abs(255 -((Square)style).getColor().getBlue())
									);
							_ted.getCommandStack().execute(doChangeColor);
						}
						if (style instanceof Ellipse){
							doChangeColor = new ChangeColor(_ted, highlightedColor,
									Math.abs(255 -((Ellipse)style).getColor().getGreen()),
									Math.abs(255 -((Ellipse)style).getColor().getRed()),
									Math.abs(255 -((Ellipse)style).getColor().getBlue())
									);
							_ted.getCommandStack().execute(doChangeColor);

						}
						if (style instanceof Lozenge){
							doChangeColor = new ChangeColor(_ted, highlightedColor,
									Math.abs(255 -((Lozenge)style).getColor().getGreen()),
									Math.abs(255 -((Lozenge)style).getColor().getRed()),
									Math.abs(255 -((Lozenge)style).getColor().getBlue())
									);
							_ted.getCommandStack().execute(doChangeColor);
						}
						if (style instanceof ShapeContainerStyle){
							doChangeColor = new ChangeColor(_ted, highlightedColor,
									Math.abs(255 - ((ShapeContainerStyle)style).getBackgroundColor().getGreen()),
									Math.abs(255 - ((ShapeContainerStyle)style).getBackgroundColor().getRed()),
									Math.abs(255 - ((ShapeContainerStyle)style).getBackgroundColor().getBlue())
									);
							_ted.getCommandStack().execute(doChangeColor);
						}
						if (style instanceof FlatContainerStyle){
							doChangeColor = new ChangeColor(_ted, highlightedColor,
									Math.abs(255 - ((FlatContainerStyle)style).getBackgroundColor().getGreen()),
									Math.abs(255 - ((FlatContainerStyle)style).getBackgroundColor().getRed()),
									Math.abs(255 - ((FlatContainerStyle)style).getBackgroundColor().getBlue())
									);
							_ted.getCommandStack().execute(doChangeColor);
						}
						
						//highlightedColor.setRed(255);

						int size = 0;
						if (style instanceof BorderedStyle){
							size = ((BorderedStyle)style).getBorderSize();
						}
						changeBorderSize(repr, highlightedColor, size+1);
	
					}
					
					
//							try {
//								cmd.execute(null, null);
//							} catch (ExecutionException e) {
//								e.printStackTrace();
//								_comment = UMLFactory.eINSTANCE.createComment();
//							}
//							if (_comment == null) {
//								_comment = UMLFactory.eINSTANCE.createComment();
//							}
				}
			}
		_helper = null;
		}
	}

	void finish() {
		_representations = null;
		_originalStyle = null;
		_ted = null; 
		_helper = null;
		_ek = null;
	}
	public void runWithWrongActivationState(TraceHelper helper) {
		if (_representations.size()>0) {
			_helper = helper;
			_clockCounter = helper.getClockState().getCounter();
			if (_ce.getReferencedElement().size() > 0)
			{
				if(_ek == EventEnumerator.UNDEFINED){
					for (DRepresentationElement repr : _representations)
					{
						Style s = _originalStyle.get(_representations.indexOf(repr));
						RecordingCommand doRestoreStyle = new RestoreStyleCommand(_ted, repr,s);
						_ted.getCommandStack().execute(doRestoreStyle);
					}
				}
				if(_ek == EventEnumerator.RECEIVE && receiveDone ==true){
					receiveDone = false;
					for (DRepresentationElement repr : _representations)
					{
						Style s = _originalStyle.get(_representations.indexOf(repr));
						RecordingCommand doRestoreStyle = new RestoreStyleCommand(_ted, repr,s);
						_ted.getCommandStack().execute(doRestoreStyle);
					}
				}
			}
		_helper = null;
		}
	}

	
	/**
	 * This method is called in debug mode when a step has already been computed
	 * but is visited again
	 */
	public void againRun(TraceHelper helper) {
		//TODO: complete with comment
		run(helper);
	}

	/**
	 * This method is called in debug mode when a step has already been computed
	 * but is visited again
	 */
	public void againRunWithWrongActivationState(TraceHelper helper) {
		//TODO: complete with comment
		runWithWrongActivationState(helper);
	}
	
	public boolean behaviorEquals(Behavior behavior) {
		if (behavior==this)
			return true;		
		return false;
	}
	
	
	public ClockEntity get_ce() {
		return _ce;
	}
}
