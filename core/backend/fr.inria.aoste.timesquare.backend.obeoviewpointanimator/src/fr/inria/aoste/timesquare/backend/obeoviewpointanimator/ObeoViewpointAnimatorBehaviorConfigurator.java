/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package fr.inria.aoste.timesquare.backend.obeoviewpointanimator;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class ObeoViewpointAnimatorBehaviorConfigurator extends BehaviorConfigurator<ObeoViewpointAnimatorBehaviorManager> {

	public ObeoViewpointAnimatorBehaviorConfigurator(ConfigurationHelper _ch, ObeoViewpointAnimatorBehaviorManager _behaviorManager) {
		super(_ch, _behaviorManager);
	}

	/***
	 * set a <i>aird</i> file for obeo viewpoint Animator
	 * @param file (String )
	 * @throws Exception
	 */
	public void setAird(String file) throws Exception {
		if (file != null) {
			if (!getBehaviorManager().isValidAirdFile(file)) {
				throw new IllegalArgumentException("File is bad (...)");
			}
			getBehaviorManager().setAird(file);
			getBehaviorManager().manageBehavior(getConfigurationHelper());
			return;
		}
		throw new NullPointerException("Argument is null");
	}

//	/**
//	 * set a <i>aird</i> file for obeo viewpoint Animator
//	 * @param file (Ifile)
//	 * @throws Exception
//	 */
//	public void setAird(IFile file) throws Exception {
//		if (file != null) {
//			if (!file.exists()) {
//				throw new IllegalArgumentException("File "+file.getName()+" does not exist!");
//			}
//			if (!getBehaviorManager().validDiFile(file)) {
//				throw new IllegalArgumentException("File "+file.getName()+" is not valid.");
//			}
//			getBehaviorManager().setDi(file);
//			getBehaviorManager().manageBehavior(getConfigurationHelper());
//			return;
//		}
//		throw new NullPointerException("Argument is null");
//	}
	
}
