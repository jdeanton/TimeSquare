/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;

import fr.inria.aoste.trace.EnableStateKind;
import fr.inria.aoste.trace.FiredStateKind;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Exec Clock Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getClock <em>Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getFireState <em>Fire State</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getEnableState <em>Enable State</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getLiveState <em>Live State</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getClockToForce <em>Clock To Force</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecClockBehavior()
 * @model
 * @generated
 */
public interface CodeExecClockBehavior extends CodeExecutionModelBehavior {
	/**
	 * Returns the value of the '<em><b>Clock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock</em>' reference.
	 * @see #setClock(Clock)
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecClockBehavior_Clock()
	 * @model required="true"
	 * @generated
	 */
	Clock getClock();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getClock <em>Clock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clock</em>' reference.
	 * @see #getClock()
	 * @generated
	 */
	void setClock(Clock value);

	/**
	 * Returns the value of the '<em><b>Fire State</b></em>' attribute list.
	 * The list contents are of type {@link fr.inria.aoste.trace.FiredStateKind}.
	 * The literals are from the enumeration {@link fr.inria.aoste.trace.FiredStateKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fire State</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fire State</em>' attribute list.
	 * @see fr.inria.aoste.trace.FiredStateKind
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecClockBehavior_FireState()
	 * @model
	 * @generated
	 */
	EList<FiredStateKind> getFireState();

	/**
	 * Returns the value of the '<em><b>Enable State</b></em>' attribute list.
	 * The list contents are of type {@link fr.inria.aoste.trace.EnableStateKind}.
	 * The literals are from the enumeration {@link fr.inria.aoste.trace.EnableStateKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enable State</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enable State</em>' attribute list.
	 * @see fr.inria.aoste.trace.EnableStateKind
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecClockBehavior_EnableState()
	 * @model
	 * @generated
	 */
	EList<EnableStateKind> getEnableState();

	/**
	 * Returns the value of the '<em><b>Live State</b></em>' attribute list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind}.
	 * The literals are from the enumeration {@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Live State</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Live State</em>' attribute list.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecClockBehavior_LiveState()
	 * @model
	 * @generated
	 */
	EList<LiveStateKind> getLiveState();

	/**
	 * Returns the value of the '<em><b>Clock To Force</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clock To Force</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clock To Force</em>' reference list.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecClockBehavior_ClockToForce()
	 * @model
	 * @generated
	 */
	EList<Clock> getClockToForce();

} // CodeExecClockBehavior
