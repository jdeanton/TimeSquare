/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage
 * @generated
 */
public interface CodeExecutionFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CodeExecutionFactory eINSTANCE = fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Code Exec Clock Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Exec Clock Behavior</em>'.
	 * @generated
	 */
	CodeExecClockBehavior createCodeExecClockBehavior();

	/**
	 * Returns a new object of class '<em>Object Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object Variable</em>'.
	 * @generated
	 */
	ObjectVariable createObjectVariable();

	/**
	 * Returns a new object of class '<em>Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Specification</em>'.
	 * @generated
	 */
	CodeExecutionSpecification createCodeExecutionSpecification();

	/**
	 * Returns a new object of class '<em>Code Exec Relation Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Exec Relation Behavior</em>'.
	 * @generated
	 */
	CodeExecRelationBehavior createCodeExecRelationBehavior();

	/**
	 * Returns a new object of class '<em>Model Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Behavior</em>'.
	 * @generated
	 */
	CodeExecutionModelBehavior createCodeExecutionModelBehavior();

	/**
	 * Returns a new object of class '<em>Code Exec Assertion Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Exec Assertion Behavior</em>'.
	 * @generated
	 */
	CodeExecAssertionBehavior createCodeExecAssertionBehavior();

	/**
	 * Returns a new object of class '<em>Import Java Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Import Java Statement</em>'.
	 * @generated
	 */
	ImportJavaStatement createImportJavaStatement();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CodeExecutionPackage getCodeExecutionPackage();

} //CodeExecutionFactory
