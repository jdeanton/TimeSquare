/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Exec Assertion Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#getAssertion <em>Assertion</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#isFailed <em>Failed</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#isSatisfied <em>Satisfied</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecAssertionBehavior()
 * @model
 * @generated
 */
public interface CodeExecAssertionBehavior extends CodeExecutionModelBehavior {
	/**
	 * Returns the value of the '<em><b>Assertion</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion</em>' reference.
	 * @see #setAssertion(Relation)
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecAssertionBehavior_Assertion()
	 * @model required="true"
	 * @generated
	 */
	Relation getAssertion();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#getAssertion <em>Assertion</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion</em>' reference.
	 * @see #getAssertion()
	 * @generated
	 */
	void setAssertion(Relation value);

	/**
	 * Returns the value of the '<em><b>Failed</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Failed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Failed</em>' attribute.
	 * @see #setFailed(boolean)
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecAssertionBehavior_Failed()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isFailed();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#isFailed <em>Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Failed</em>' attribute.
	 * @see #isFailed()
	 * @generated
	 */
	void setFailed(boolean value);

	/**
	 * Returns the value of the '<em><b>Satisfied</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Satisfied</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satisfied</em>' attribute.
	 * @see #setSatisfied(boolean)
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecAssertionBehavior_Satisfied()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isSatisfied();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#isSatisfied <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Satisfied</em>' attribute.
	 * @see #isSatisfied()
	 * @generated
	 */
	void setSatisfied(boolean value);

} // CodeExecAssertionBehavior
