/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl;

import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ImportJavaStatement;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionSpecificationImpl#getModelImports <em>Model Imports</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionSpecificationImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionSpecificationImpl#getBehaviors <em>Behaviors</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionSpecificationImpl#getClassImports <em>Class Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CodeExecutionSpecificationImpl extends EObjectImpl implements CodeExecutionSpecification {
	/**
	 * The cached value of the '{@link #getModelImports() <em>Model Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelImports()
	 * @generated
	 * @ordered
	 */
	protected EList<ImportStatement> modelImports;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<ObjectVariable> variables;

	/**
	 * The cached value of the '{@link #getBehaviors() <em>Behaviors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBehaviors()
	 * @generated
	 * @ordered
	 */
	protected EList<CodeExecutionModelBehavior> behaviors;

	/**
	 * The cached value of the '{@link #getClassImports() <em>Class Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassImports()
	 * @generated
	 * @ordered
	 */
	protected EList<ImportJavaStatement> classImports;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeExecutionSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CodeExecutionPackage.Literals.CODE_EXECUTION_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportStatement> getModelImports() {
		if (modelImports == null) {
			modelImports = new EObjectContainmentEList<ImportStatement>(ImportStatement.class, this, CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS);
		}
		return modelImports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectVariable> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList<ObjectVariable>(ObjectVariable.class, this, CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CodeExecutionModelBehavior> getBehaviors() {
		if (behaviors == null) {
			behaviors = new EObjectContainmentEList<CodeExecutionModelBehavior>(CodeExecutionModelBehavior.class, this, CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__BEHAVIORS);
		}
		return behaviors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportJavaStatement> getClassImports() {
		if (classImports == null) {
			classImports = new EObjectContainmentEList<ImportJavaStatement>(ImportJavaStatement.class, this, CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS);
		}
		return classImports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS:
				return ((InternalEList<?>)getModelImports()).basicRemove(otherEnd, msgs);
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__BEHAVIORS:
				return ((InternalEList<?>)getBehaviors()).basicRemove(otherEnd, msgs);
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS:
				return ((InternalEList<?>)getClassImports()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS:
				return getModelImports();
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__VARIABLES:
				return getVariables();
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__BEHAVIORS:
				return getBehaviors();
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS:
				return getClassImports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS:
				getModelImports().clear();
				getModelImports().addAll((Collection<? extends ImportStatement>)newValue);
				return;
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends ObjectVariable>)newValue);
				return;
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__BEHAVIORS:
				getBehaviors().clear();
				getBehaviors().addAll((Collection<? extends CodeExecutionModelBehavior>)newValue);
				return;
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS:
				getClassImports().clear();
				getClassImports().addAll((Collection<? extends ImportJavaStatement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS:
				getModelImports().clear();
				return;
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__VARIABLES:
				getVariables().clear();
				return;
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__BEHAVIORS:
				getBehaviors().clear();
				return;
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS:
				getClassImports().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS:
				return modelImports != null && !modelImports.isEmpty();
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__VARIABLES:
				return variables != null && !variables.isEmpty();
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__BEHAVIORS:
				return behaviors != null && !behaviors.isEmpty();
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS:
				return classImports != null && !classImports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CodeExecutionSpecificationImpl
