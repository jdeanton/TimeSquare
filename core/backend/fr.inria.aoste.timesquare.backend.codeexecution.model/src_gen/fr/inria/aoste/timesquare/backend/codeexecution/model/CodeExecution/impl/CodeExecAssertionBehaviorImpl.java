/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl;

import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Code Exec Assertion Behavior</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecAssertionBehaviorImpl#getAssertion <em>Assertion</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecAssertionBehaviorImpl#isFailed <em>Failed</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecAssertionBehaviorImpl#isSatisfied <em>Satisfied</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CodeExecAssertionBehaviorImpl extends CodeExecutionModelBehaviorImpl implements CodeExecAssertionBehavior {
	/**
	 * The cached value of the '{@link #getAssertion() <em>Assertion</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertion()
	 * @generated
	 * @ordered
	 */
	protected Relation assertion;

	/**
	 * The default value of the '{@link #isFailed() <em>Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFailed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FAILED_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isFailed() <em>Failed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFailed()
	 * @generated
	 * @ordered
	 */
	protected boolean failed = FAILED_EDEFAULT;
	/**
	 * The default value of the '{@link #isSatisfied() <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSatisfied()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SATISFIED_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isSatisfied() <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSatisfied()
	 * @generated
	 * @ordered
	 */
	protected boolean satisfied = SATISFIED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeExecAssertionBehaviorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CodeExecutionPackage.Literals.CODE_EXEC_ASSERTION_BEHAVIOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relation getAssertion() {
		if (assertion != null && assertion.eIsProxy()) {
			InternalEObject oldAssertion = (InternalEObject)assertion;
			assertion = (Relation)eResolveProxy(oldAssertion);
			if (assertion != oldAssertion) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION, oldAssertion, assertion));
			}
		}
		return assertion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relation basicGetAssertion() {
		return assertion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertion(Relation newAssertion) {
		Relation oldAssertion = assertion;
		assertion = newAssertion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION, oldAssertion, assertion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFailed() {
		return failed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFailed(boolean newFailed) {
		boolean oldFailed = failed;
		failed = newFailed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__FAILED, oldFailed, failed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSatisfied() {
		return satisfied;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSatisfied(boolean newSatisfied) {
		boolean oldSatisfied = satisfied;
		satisfied = newSatisfied;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__SATISFIED, oldSatisfied, satisfied));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION:
				if (resolve) return getAssertion();
				return basicGetAssertion();
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__FAILED:
				return isFailed();
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__SATISFIED:
				return isSatisfied();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION:
				setAssertion((Relation)newValue);
				return;
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__FAILED:
				setFailed((Boolean)newValue);
				return;
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__SATISFIED:
				setSatisfied((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION:
				setAssertion((Relation)null);
				return;
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__FAILED:
				setFailed(FAILED_EDEFAULT);
				return;
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__SATISFIED:
				setSatisfied(SATISFIED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION:
				return assertion != null;
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__FAILED:
				return failed != FAILED_EDEFAULT;
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR__SATISFIED:
				return satisfied != SATISFIED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (failed: ");
		result.append(failed);
		result.append(", satisfied: ");
		result.append(satisfied);
		result.append(')');
		return result.toString();
	}

} //CodeExecAssertionBehaviorImpl
