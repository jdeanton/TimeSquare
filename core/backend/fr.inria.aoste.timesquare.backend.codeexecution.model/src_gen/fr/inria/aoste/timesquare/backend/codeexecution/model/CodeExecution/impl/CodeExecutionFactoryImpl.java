/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl;

import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CodeExecutionFactoryImpl extends EFactoryImpl implements CodeExecutionFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CodeExecutionFactory init() {
		try {
			CodeExecutionFactory theCodeExecutionFactory = (CodeExecutionFactory)EPackage.Registry.INSTANCE.getEFactory("http://fr.inria.aoste.codeexecution/1.0"); 
			if (theCodeExecutionFactory != null) {
				return theCodeExecutionFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CodeExecutionFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecutionFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR: return createCodeExecClockBehavior();
			case CodeExecutionPackage.OBJECT_VARIABLE: return createObjectVariable();
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION: return createCodeExecutionSpecification();
			case CodeExecutionPackage.CODE_EXEC_RELATION_BEHAVIOR: return createCodeExecRelationBehavior();
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR: return createCodeExecutionModelBehavior();
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR: return createCodeExecAssertionBehavior();
			case CodeExecutionPackage.IMPORT_JAVA_STATEMENT: return createImportJavaStatement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case CodeExecutionPackage.LIVE_STATE_KIND:
				return createLiveStateKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case CodeExecutionPackage.LIVE_STATE_KIND:
				return convertLiveStateKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecClockBehavior createCodeExecClockBehavior() {
		CodeExecClockBehaviorImpl codeExecClockBehavior = new CodeExecClockBehaviorImpl();
		return codeExecClockBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectVariable createObjectVariable() {
		ObjectVariableImpl objectVariable = new ObjectVariableImpl();
		return objectVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecutionSpecification createCodeExecutionSpecification() {
		CodeExecutionSpecificationImpl codeExecutionSpecification = new CodeExecutionSpecificationImpl();
		return codeExecutionSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecRelationBehavior createCodeExecRelationBehavior() {
		CodeExecRelationBehaviorImpl codeExecRelationBehavior = new CodeExecRelationBehaviorImpl();
		return codeExecRelationBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecutionModelBehavior createCodeExecutionModelBehavior() {
		CodeExecutionModelBehaviorImpl codeExecutionModelBehavior = new CodeExecutionModelBehaviorImpl();
		return codeExecutionModelBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecAssertionBehavior createCodeExecAssertionBehavior() {
		CodeExecAssertionBehaviorImpl codeExecAssertionBehavior = new CodeExecAssertionBehaviorImpl();
		return codeExecAssertionBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportJavaStatement createImportJavaStatement() {
		ImportJavaStatementImpl importJavaStatement = new ImportJavaStatementImpl();
		return importJavaStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LiveStateKind createLiveStateKindFromString(EDataType eDataType, String initialValue) {
		LiveStateKind result = LiveStateKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLiveStateKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecutionPackage getCodeExecutionPackage() {
		return (CodeExecutionPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CodeExecutionPackage getPackage() {
		return CodeExecutionPackage.eINSTANCE;
	}

} //CodeExecutionFactoryImpl
