/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.xtext.common.types.JvmOperation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior#getObjectVariable <em>Object Variable</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior#getMethod <em>Method</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecutionModelBehavior()
 * @model
 * @generated
 */
public interface CodeExecutionModelBehavior extends EObject {
	/**
	 * Returns the value of the '<em><b>Object Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Variable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Variable</em>' reference.
	 * @see #setObjectVariable(ObjectVariable)
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecutionModelBehavior_ObjectVariable()
	 * @model required="true"
	 * @generated
	 */
	ObjectVariable getObjectVariable();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior#getObjectVariable <em>Object Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Variable</em>' reference.
	 * @see #getObjectVariable()
	 * @generated
	 */
	void setObjectVariable(ObjectVariable value);

	/**
	 * Returns the value of the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method</em>' reference.
	 * @see #setMethod(JvmOperation)
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecutionModelBehavior_Method()
	 * @model required="true"
	 * @generated
	 */
	JvmOperation getMethod();

	/**
	 * Sets the value of the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior#getMethod <em>Method</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method</em>' reference.
	 * @see #getMethod()
	 * @generated
	 */
	void setMethod(JvmOperation value);

} // CodeExecutionModelBehavior
