/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl;

import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;

import fr.inria.aoste.trace.EnableStateKind;
import fr.inria.aoste.trace.FiredStateKind;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Code Exec Clock Behavior</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl#getClock <em>Clock</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl#getFireState <em>Fire State</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl#getEnableState <em>Enable State</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl#getLiveState <em>Live State</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl#getClockToForce <em>Clock To Force</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CodeExecClockBehaviorImpl extends CodeExecutionModelBehaviorImpl implements CodeExecClockBehavior {
	/**
	 * The cached value of the '{@link #getClock() <em>Clock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClock()
	 * @generated
	 * @ordered
	 */
	protected Clock clock;

	/**
	 * The cached value of the '{@link #getFireState() <em>Fire State</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFireState()
	 * @generated
	 * @ordered
	 */
	protected EList<FiredStateKind> fireState;

	/**
	 * The cached value of the '{@link #getEnableState() <em>Enable State</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnableState()
	 * @generated
	 * @ordered
	 */
	protected EList<EnableStateKind> enableState;

	/**
	 * The cached value of the '{@link #getLiveState() <em>Live State</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiveState()
	 * @generated
	 * @ordered
	 */
	protected EList<LiveStateKind> liveState;

	/**
	 * The cached value of the '{@link #getClockToForce() <em>Clock To Force</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClockToForce()
	 * @generated
	 * @ordered
	 */
	protected EList<Clock> clockToForce;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeExecClockBehaviorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CodeExecutionPackage.Literals.CODE_EXEC_CLOCK_BEHAVIOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Clock getClock() {
		if (clock != null && clock.eIsProxy()) {
			InternalEObject oldClock = (InternalEObject)clock;
			clock = (Clock)eResolveProxy(oldClock);
			if (clock != oldClock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK, oldClock, clock));
			}
		}
		return clock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Clock basicGetClock() {
		return clock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClock(Clock newClock) {
		Clock oldClock = clock;
		clock = newClock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK, oldClock, clock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FiredStateKind> getFireState() {
		if (fireState == null) {
			fireState = new EDataTypeUniqueEList<FiredStateKind>(FiredStateKind.class, this, CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__FIRE_STATE);
		}
		return fireState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnableStateKind> getEnableState() {
		if (enableState == null) {
			enableState = new EDataTypeUniqueEList<EnableStateKind>(EnableStateKind.class, this, CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__ENABLE_STATE);
		}
		return enableState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LiveStateKind> getLiveState() {
		if (liveState == null) {
			liveState = new EDataTypeUniqueEList<LiveStateKind>(LiveStateKind.class, this, CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__LIVE_STATE);
		}
		return liveState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Clock> getClockToForce() {
		if (clockToForce == null) {
			clockToForce = new EObjectResolvingEList<Clock>(Clock.class, this, CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK_TO_FORCE);
		}
		return clockToForce;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK:
				if (resolve) return getClock();
				return basicGetClock();
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__FIRE_STATE:
				return getFireState();
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__ENABLE_STATE:
				return getEnableState();
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__LIVE_STATE:
				return getLiveState();
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK_TO_FORCE:
				return getClockToForce();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK:
				setClock((Clock)newValue);
				return;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__FIRE_STATE:
				getFireState().clear();
				getFireState().addAll((Collection<? extends FiredStateKind>)newValue);
				return;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__ENABLE_STATE:
				getEnableState().clear();
				getEnableState().addAll((Collection<? extends EnableStateKind>)newValue);
				return;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__LIVE_STATE:
				getLiveState().clear();
				getLiveState().addAll((Collection<? extends LiveStateKind>)newValue);
				return;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK_TO_FORCE:
				getClockToForce().clear();
				getClockToForce().addAll((Collection<? extends Clock>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK:
				setClock((Clock)null);
				return;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__FIRE_STATE:
				getFireState().clear();
				return;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__ENABLE_STATE:
				getEnableState().clear();
				return;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__LIVE_STATE:
				getLiveState().clear();
				return;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK_TO_FORCE:
				getClockToForce().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK:
				return clock != null;
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__FIRE_STATE:
				return fireState != null && !fireState.isEmpty();
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__ENABLE_STATE:
				return enableState != null && !enableState.isEmpty();
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__LIVE_STATE:
				return liveState != null && !liveState.isEmpty();
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR__CLOCK_TO_FORCE:
				return clockToForce != null && !clockToForce.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fireState: ");
		result.append(fireState);
		result.append(", enableState: ");
		result.append(enableState);
		result.append(", liveState: ");
		result.append(liveState);
		result.append(')');
		return result.toString();
	}

} //CodeExecClockBehaviorImpl
