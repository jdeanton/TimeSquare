/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getModelImports <em>Model Imports</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getVariables <em>Variables</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getBehaviors <em>Behaviors</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getClassImports <em>Class Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecutionSpecification()
 * @model
 * @generated
 */
public interface CodeExecutionSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Model Imports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Imports</em>' containment reference list.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecutionSpecification_ModelImports()
	 * @model containment="true"
	 * @generated
	 */
	EList<ImportStatement> getModelImports();

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecutionSpecification_Variables()
	 * @model containment="true"
	 * @generated
	 */
	EList<ObjectVariable> getVariables();

	/**
	 * Returns the value of the '<em><b>Behaviors</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Behaviors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behaviors</em>' containment reference list.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecutionSpecification_Behaviors()
	 * @model containment="true"
	 * @generated
	 */
	EList<CodeExecutionModelBehavior> getBehaviors();

	/**
	 * Returns the value of the '<em><b>Class Imports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ImportJavaStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Imports</em>' containment reference list.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#getCodeExecutionSpecification_ClassImports()
	 * @model containment="true"
	 * @generated
	 */
	EList<ImportJavaStatement> getClassImports();

} // CodeExecutionSpecification
