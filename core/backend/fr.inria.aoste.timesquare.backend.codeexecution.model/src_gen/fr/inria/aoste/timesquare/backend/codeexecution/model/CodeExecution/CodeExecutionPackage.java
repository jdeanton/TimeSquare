/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionFactory
 * @model kind="package"
 * @generated
 */
public interface CodeExecutionPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "CodeExecution";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fr.inria.aoste.codeexecution/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "codeexecution";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CodeExecutionPackage eINSTANCE = fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionModelBehaviorImpl <em>Model Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionModelBehaviorImpl
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecutionModelBehavior()
	 * @generated
	 */
	int CODE_EXECUTION_MODEL_BEHAVIOR = 4;

	/**
	 * The feature id for the '<em><b>Object Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE = 0;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXECUTION_MODEL_BEHAVIOR__METHOD = 1;

	/**
	 * The number of structural features of the '<em>Model Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl <em>Code Exec Clock Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecClockBehavior()
	 * @generated
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR = 0;

	/**
	 * The feature id for the '<em><b>Object Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR__OBJECT_VARIABLE = CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR__METHOD = CODE_EXECUTION_MODEL_BEHAVIOR__METHOD;

	/**
	 * The feature id for the '<em><b>Clock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR__CLOCK = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fire State</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR__FIRE_STATE = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Enable State</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR__ENABLE_STATE = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Live State</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR__LIVE_STATE = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Clock To Force</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR__CLOCK_TO_FORCE = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Code Exec Clock Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_CLOCK_BEHAVIOR_FEATURE_COUNT = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.ObjectVariableImpl <em>Object Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.ObjectVariableImpl
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getObjectVariable()
	 * @generated
	 */
	int OBJECT_VARIABLE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Object Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionSpecificationImpl <em>Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionSpecificationImpl
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecutionSpecification()
	 * @generated
	 */
	int CODE_EXECUTION_SPECIFICATION = 2;

	/**
	 * The feature id for the '<em><b>Model Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS = 0;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXECUTION_SPECIFICATION__VARIABLES = 1;

	/**
	 * The feature id for the '<em><b>Behaviors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXECUTION_SPECIFICATION__BEHAVIORS = 2;

	/**
	 * The feature id for the '<em><b>Class Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS = 3;

	/**
	 * The number of structural features of the '<em>Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXECUTION_SPECIFICATION_FEATURE_COUNT = 4;


	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecRelationBehaviorImpl <em>Code Exec Relation Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecRelationBehaviorImpl
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecRelationBehavior()
	 * @generated
	 */
	int CODE_EXEC_RELATION_BEHAVIOR = 3;

	/**
	 * The feature id for the '<em><b>Object Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_RELATION_BEHAVIOR__OBJECT_VARIABLE = CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_RELATION_BEHAVIOR__METHOD = CODE_EXECUTION_MODEL_BEHAVIOR__METHOD;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_RELATION_BEHAVIOR__RELATION = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Code Exec Relation Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_RELATION_BEHAVIOR_FEATURE_COUNT = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecAssertionBehaviorImpl <em>Code Exec Assertion Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecAssertionBehaviorImpl
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecAssertionBehavior()
	 * @generated
	 */
	int CODE_EXEC_ASSERTION_BEHAVIOR = 5;

	/**
	 * The feature id for the '<em><b>Object Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_ASSERTION_BEHAVIOR__OBJECT_VARIABLE = CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE;

	/**
	 * The feature id for the '<em><b>Method</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_ASSERTION_BEHAVIOR__METHOD = CODE_EXECUTION_MODEL_BEHAVIOR__METHOD;

	/**
	 * The feature id for the '<em><b>Assertion</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Failed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_ASSERTION_BEHAVIOR__FAILED = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_ASSERTION_BEHAVIOR__SATISFIED = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Code Exec Assertion Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_EXEC_ASSERTION_BEHAVIOR_FEATURE_COUNT = CODE_EXECUTION_MODEL_BEHAVIOR_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.ImportJavaStatementImpl <em>Import Java Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.ImportJavaStatementImpl
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getImportJavaStatement()
	 * @generated
	 */
	int IMPORT_JAVA_STATEMENT = 6;

	/**
	 * The feature id for the '<em><b>Imported Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_JAVA_STATEMENT__IMPORTED_NAMESPACE = 0;

	/**
	 * The number of structural features of the '<em>Import Java Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_JAVA_STATEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind <em>Live State Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getLiveStateKind()
	 * @generated
	 */
	int LIVE_STATE_KIND = 7;


	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior <em>Code Exec Clock Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Exec Clock Behavior</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior
	 * @generated
	 */
	EClass getCodeExecClockBehavior();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getClock <em>Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Clock</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getClock()
	 * @see #getCodeExecClockBehavior()
	 * @generated
	 */
	EReference getCodeExecClockBehavior_Clock();

	/**
	 * Returns the meta object for the attribute list '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getFireState <em>Fire State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Fire State</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getFireState()
	 * @see #getCodeExecClockBehavior()
	 * @generated
	 */
	EAttribute getCodeExecClockBehavior_FireState();

	/**
	 * Returns the meta object for the attribute list '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getEnableState <em>Enable State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Enable State</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getEnableState()
	 * @see #getCodeExecClockBehavior()
	 * @generated
	 */
	EAttribute getCodeExecClockBehavior_EnableState();

	/**
	 * Returns the meta object for the attribute list '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getLiveState <em>Live State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Live State</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getLiveState()
	 * @see #getCodeExecClockBehavior()
	 * @generated
	 */
	EAttribute getCodeExecClockBehavior_LiveState();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getClockToForce <em>Clock To Force</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Clock To Force</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior#getClockToForce()
	 * @see #getCodeExecClockBehavior()
	 * @generated
	 */
	EReference getCodeExecClockBehavior_ClockToForce();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable <em>Object Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Variable</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable
	 * @generated
	 */
	EClass getObjectVariable();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable#getName()
	 * @see #getObjectVariable()
	 * @generated
	 */
	EAttribute getObjectVariable_Name();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable#getType()
	 * @see #getObjectVariable()
	 * @generated
	 */
	EReference getObjectVariable_Type();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Specification</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification
	 * @generated
	 */
	EClass getCodeExecutionSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getModelImports <em>Model Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Model Imports</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getModelImports()
	 * @see #getCodeExecutionSpecification()
	 * @generated
	 */
	EReference getCodeExecutionSpecification_ModelImports();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getVariables()
	 * @see #getCodeExecutionSpecification()
	 * @generated
	 */
	EReference getCodeExecutionSpecification_Variables();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getBehaviors <em>Behaviors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Behaviors</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getBehaviors()
	 * @see #getCodeExecutionSpecification()
	 * @generated
	 */
	EReference getCodeExecutionSpecification_Behaviors();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getClassImports <em>Class Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class Imports</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification#getClassImports()
	 * @see #getCodeExecutionSpecification()
	 * @generated
	 */
	EReference getCodeExecutionSpecification_ClassImports();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecRelationBehavior <em>Code Exec Relation Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Exec Relation Behavior</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecRelationBehavior
	 * @generated
	 */
	EClass getCodeExecRelationBehavior();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecRelationBehavior#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Relation</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecRelationBehavior#getRelation()
	 * @see #getCodeExecRelationBehavior()
	 * @generated
	 */
	EReference getCodeExecRelationBehavior_Relation();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior <em>Model Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Behavior</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior
	 * @generated
	 */
	EClass getCodeExecutionModelBehavior();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior#getObjectVariable <em>Object Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object Variable</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior#getObjectVariable()
	 * @see #getCodeExecutionModelBehavior()
	 * @generated
	 */
	EReference getCodeExecutionModelBehavior_ObjectVariable();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Method</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior#getMethod()
	 * @see #getCodeExecutionModelBehavior()
	 * @generated
	 */
	EReference getCodeExecutionModelBehavior_Method();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior <em>Code Exec Assertion Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Exec Assertion Behavior</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior
	 * @generated
	 */
	EClass getCodeExecAssertionBehavior();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#getAssertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assertion</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#getAssertion()
	 * @see #getCodeExecAssertionBehavior()
	 * @generated
	 */
	EReference getCodeExecAssertionBehavior_Assertion();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#isFailed <em>Failed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Failed</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#isFailed()
	 * @see #getCodeExecAssertionBehavior()
	 * @generated
	 */
	EAttribute getCodeExecAssertionBehavior_Failed();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#isSatisfied <em>Satisfied</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Satisfied</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior#isSatisfied()
	 * @see #getCodeExecAssertionBehavior()
	 * @generated
	 */
	EAttribute getCodeExecAssertionBehavior_Satisfied();

	/**
	 * Returns the meta object for class '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ImportJavaStatement <em>Import Java Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import Java Statement</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ImportJavaStatement
	 * @generated
	 */
	EClass getImportJavaStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ImportJavaStatement#getImportedNamespace <em>Imported Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imported Namespace</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ImportJavaStatement#getImportedNamespace()
	 * @see #getImportJavaStatement()
	 * @generated
	 */
	EAttribute getImportJavaStatement_ImportedNamespace();

	/**
	 * Returns the meta object for enum '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind <em>Live State Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Live State Kind</em>'.
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind
	 * @generated
	 */
	EEnum getLiveStateKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CodeExecutionFactory getCodeExecutionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl <em>Code Exec Clock Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecClockBehaviorImpl
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecClockBehavior()
		 * @generated
		 */
		EClass CODE_EXEC_CLOCK_BEHAVIOR = eINSTANCE.getCodeExecClockBehavior();

		/**
		 * The meta object literal for the '<em><b>Clock</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXEC_CLOCK_BEHAVIOR__CLOCK = eINSTANCE.getCodeExecClockBehavior_Clock();

		/**
		 * The meta object literal for the '<em><b>Fire State</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_EXEC_CLOCK_BEHAVIOR__FIRE_STATE = eINSTANCE.getCodeExecClockBehavior_FireState();

		/**
		 * The meta object literal for the '<em><b>Enable State</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_EXEC_CLOCK_BEHAVIOR__ENABLE_STATE = eINSTANCE.getCodeExecClockBehavior_EnableState();

		/**
		 * The meta object literal for the '<em><b>Live State</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_EXEC_CLOCK_BEHAVIOR__LIVE_STATE = eINSTANCE.getCodeExecClockBehavior_LiveState();

		/**
		 * The meta object literal for the '<em><b>Clock To Force</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXEC_CLOCK_BEHAVIOR__CLOCK_TO_FORCE = eINSTANCE.getCodeExecClockBehavior_ClockToForce();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.ObjectVariableImpl <em>Object Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.ObjectVariableImpl
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getObjectVariable()
		 * @generated
		 */
		EClass OBJECT_VARIABLE = eINSTANCE.getObjectVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_VARIABLE__NAME = eINSTANCE.getObjectVariable_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_VARIABLE__TYPE = eINSTANCE.getObjectVariable_Type();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionSpecificationImpl <em>Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionSpecificationImpl
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecutionSpecification()
		 * @generated
		 */
		EClass CODE_EXECUTION_SPECIFICATION = eINSTANCE.getCodeExecutionSpecification();

		/**
		 * The meta object literal for the '<em><b>Model Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS = eINSTANCE.getCodeExecutionSpecification_ModelImports();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXECUTION_SPECIFICATION__VARIABLES = eINSTANCE.getCodeExecutionSpecification_Variables();

		/**
		 * The meta object literal for the '<em><b>Behaviors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXECUTION_SPECIFICATION__BEHAVIORS = eINSTANCE.getCodeExecutionSpecification_Behaviors();

		/**
		 * The meta object literal for the '<em><b>Class Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS = eINSTANCE.getCodeExecutionSpecification_ClassImports();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecRelationBehaviorImpl <em>Code Exec Relation Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecRelationBehaviorImpl
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecRelationBehavior()
		 * @generated
		 */
		EClass CODE_EXEC_RELATION_BEHAVIOR = eINSTANCE.getCodeExecRelationBehavior();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXEC_RELATION_BEHAVIOR__RELATION = eINSTANCE.getCodeExecRelationBehavior_Relation();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionModelBehaviorImpl <em>Model Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionModelBehaviorImpl
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecutionModelBehavior()
		 * @generated
		 */
		EClass CODE_EXECUTION_MODEL_BEHAVIOR = eINSTANCE.getCodeExecutionModelBehavior();

		/**
		 * The meta object literal for the '<em><b>Object Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE = eINSTANCE.getCodeExecutionModelBehavior_ObjectVariable();

		/**
		 * The meta object literal for the '<em><b>Method</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXECUTION_MODEL_BEHAVIOR__METHOD = eINSTANCE.getCodeExecutionModelBehavior_Method();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecAssertionBehaviorImpl <em>Code Exec Assertion Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecAssertionBehaviorImpl
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getCodeExecAssertionBehavior()
		 * @generated
		 */
		EClass CODE_EXEC_ASSERTION_BEHAVIOR = eINSTANCE.getCodeExecAssertionBehavior();

		/**
		 * The meta object literal for the '<em><b>Assertion</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION = eINSTANCE.getCodeExecAssertionBehavior_Assertion();

		/**
		 * The meta object literal for the '<em><b>Failed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_EXEC_ASSERTION_BEHAVIOR__FAILED = eINSTANCE.getCodeExecAssertionBehavior_Failed();

		/**
		 * The meta object literal for the '<em><b>Satisfied</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_EXEC_ASSERTION_BEHAVIOR__SATISFIED = eINSTANCE.getCodeExecAssertionBehavior_Satisfied();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.ImportJavaStatementImpl <em>Import Java Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.ImportJavaStatementImpl
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getImportJavaStatement()
		 * @generated
		 */
		EClass IMPORT_JAVA_STATEMENT = eINSTANCE.getImportJavaStatement();

		/**
		 * The meta object literal for the '<em><b>Imported Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT_JAVA_STATEMENT__IMPORTED_NAMESPACE = eINSTANCE.getImportJavaStatement_ImportedNamespace();

		/**
		 * The meta object literal for the '{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind <em>Live State Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind
		 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionPackageImpl#getLiveStateKind()
		 * @generated
		 */
		EEnum LIVE_STATE_KIND = eINSTANCE.getLiveStateKind();

	}

} //CodeExecutionPackage
