/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl;

import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecAssertionBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecClockBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecRelationBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionFactory;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionSpecification;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ImportJavaStatement;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.LiveStateKind;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ClockExpressionAndRelationPackage;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import fr.inria.aoste.trace.TracePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.xtext.common.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CodeExecutionPackageImpl extends EPackageImpl implements CodeExecutionPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeExecClockBehaviorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeExecutionSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeExecRelationBehaviorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeExecutionModelBehaviorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeExecAssertionBehaviorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importJavaStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum liveStateKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CodeExecutionPackageImpl() {
		super(eNS_URI, CodeExecutionFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CodeExecutionPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CodeExecutionPackage init() {
		if (isInited) return (CodeExecutionPackage)EPackage.Registry.INSTANCE.getEPackage(CodeExecutionPackage.eNS_URI);

		// Obtain or create and register package
		CodeExecutionPackageImpl theCodeExecutionPackage = (CodeExecutionPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CodeExecutionPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CodeExecutionPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TimeModelPackage.eINSTANCE.eClass();
		TracePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theCodeExecutionPackage.createPackageContents();

		// Initialize created meta-data
		theCodeExecutionPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCodeExecutionPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CodeExecutionPackage.eNS_URI, theCodeExecutionPackage);
		return theCodeExecutionPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeExecClockBehavior() {
		return codeExecClockBehaviorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecClockBehavior_Clock() {
		return (EReference)codeExecClockBehaviorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeExecClockBehavior_FireState() {
		return (EAttribute)codeExecClockBehaviorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeExecClockBehavior_EnableState() {
		return (EAttribute)codeExecClockBehaviorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeExecClockBehavior_LiveState() {
		return (EAttribute)codeExecClockBehaviorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecClockBehavior_ClockToForce() {
		return (EReference)codeExecClockBehaviorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectVariable() {
		return objectVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObjectVariable_Name() {
		return (EAttribute)objectVariableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectVariable_Type() {
		return (EReference)objectVariableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeExecutionSpecification() {
		return codeExecutionSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecutionSpecification_ModelImports() {
		return (EReference)codeExecutionSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecutionSpecification_Variables() {
		return (EReference)codeExecutionSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecutionSpecification_Behaviors() {
		return (EReference)codeExecutionSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecutionSpecification_ClassImports() {
		return (EReference)codeExecutionSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeExecRelationBehavior() {
		return codeExecRelationBehaviorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecRelationBehavior_Relation() {
		return (EReference)codeExecRelationBehaviorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeExecutionModelBehavior() {
		return codeExecutionModelBehaviorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecutionModelBehavior_ObjectVariable() {
		return (EReference)codeExecutionModelBehaviorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecutionModelBehavior_Method() {
		return (EReference)codeExecutionModelBehaviorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeExecAssertionBehavior() {
		return codeExecAssertionBehaviorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeExecAssertionBehavior_Assertion() {
		return (EReference)codeExecAssertionBehaviorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeExecAssertionBehavior_Failed() {
		return (EAttribute)codeExecAssertionBehaviorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeExecAssertionBehavior_Satisfied() {
		return (EAttribute)codeExecAssertionBehaviorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImportJavaStatement() {
		return importJavaStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImportJavaStatement_ImportedNamespace() {
		return (EAttribute)importJavaStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLiveStateKind() {
		return liveStateKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecutionFactory getCodeExecutionFactory() {
		return (CodeExecutionFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codeExecClockBehaviorEClass = createEClass(CODE_EXEC_CLOCK_BEHAVIOR);
		createEReference(codeExecClockBehaviorEClass, CODE_EXEC_CLOCK_BEHAVIOR__CLOCK);
		createEAttribute(codeExecClockBehaviorEClass, CODE_EXEC_CLOCK_BEHAVIOR__FIRE_STATE);
		createEAttribute(codeExecClockBehaviorEClass, CODE_EXEC_CLOCK_BEHAVIOR__ENABLE_STATE);
		createEAttribute(codeExecClockBehaviorEClass, CODE_EXEC_CLOCK_BEHAVIOR__LIVE_STATE);
		createEReference(codeExecClockBehaviorEClass, CODE_EXEC_CLOCK_BEHAVIOR__CLOCK_TO_FORCE);

		objectVariableEClass = createEClass(OBJECT_VARIABLE);
		createEAttribute(objectVariableEClass, OBJECT_VARIABLE__NAME);
		createEReference(objectVariableEClass, OBJECT_VARIABLE__TYPE);

		codeExecutionSpecificationEClass = createEClass(CODE_EXECUTION_SPECIFICATION);
		createEReference(codeExecutionSpecificationEClass, CODE_EXECUTION_SPECIFICATION__MODEL_IMPORTS);
		createEReference(codeExecutionSpecificationEClass, CODE_EXECUTION_SPECIFICATION__VARIABLES);
		createEReference(codeExecutionSpecificationEClass, CODE_EXECUTION_SPECIFICATION__BEHAVIORS);
		createEReference(codeExecutionSpecificationEClass, CODE_EXECUTION_SPECIFICATION__CLASS_IMPORTS);

		codeExecRelationBehaviorEClass = createEClass(CODE_EXEC_RELATION_BEHAVIOR);
		createEReference(codeExecRelationBehaviorEClass, CODE_EXEC_RELATION_BEHAVIOR__RELATION);

		codeExecutionModelBehaviorEClass = createEClass(CODE_EXECUTION_MODEL_BEHAVIOR);
		createEReference(codeExecutionModelBehaviorEClass, CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE);
		createEReference(codeExecutionModelBehaviorEClass, CODE_EXECUTION_MODEL_BEHAVIOR__METHOD);

		codeExecAssertionBehaviorEClass = createEClass(CODE_EXEC_ASSERTION_BEHAVIOR);
		createEReference(codeExecAssertionBehaviorEClass, CODE_EXEC_ASSERTION_BEHAVIOR__ASSERTION);
		createEAttribute(codeExecAssertionBehaviorEClass, CODE_EXEC_ASSERTION_BEHAVIOR__FAILED);
		createEAttribute(codeExecAssertionBehaviorEClass, CODE_EXEC_ASSERTION_BEHAVIOR__SATISFIED);

		importJavaStatementEClass = createEClass(IMPORT_JAVA_STATEMENT);
		createEAttribute(importJavaStatementEClass, IMPORT_JAVA_STATEMENT__IMPORTED_NAMESPACE);

		// Create enums
		liveStateKindEEnum = createEEnum(LIVE_STATE_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TimeModelPackage theTimeModelPackage = (TimeModelPackage)EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		TracePackage theTracePackage = (TracePackage)EPackage.Registry.INSTANCE.getEPackage(TracePackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		ClockExpressionAndRelationPackage theClockExpressionAndRelationPackage = (ClockExpressionAndRelationPackage)EPackage.Registry.INSTANCE.getEPackage(ClockExpressionAndRelationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		codeExecClockBehaviorEClass.getESuperTypes().add(this.getCodeExecutionModelBehavior());
		codeExecRelationBehaviorEClass.getESuperTypes().add(this.getCodeExecutionModelBehavior());
		codeExecAssertionBehaviorEClass.getESuperTypes().add(this.getCodeExecutionModelBehavior());

		// Initialize classes and features; add operations and parameters
		initEClass(codeExecClockBehaviorEClass, CodeExecClockBehavior.class, "CodeExecClockBehavior", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCodeExecClockBehavior_Clock(), theTimeModelPackage.getClock(), null, "clock", null, 1, 1, CodeExecClockBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodeExecClockBehavior_FireState(), theTracePackage.getFiredStateKind(), "fireState", null, 0, -1, CodeExecClockBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodeExecClockBehavior_EnableState(), theTracePackage.getEnableStateKind(), "enableState", null, 0, -1, CodeExecClockBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodeExecClockBehavior_LiveState(), this.getLiveStateKind(), "liveState", null, 0, -1, CodeExecClockBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodeExecClockBehavior_ClockToForce(), theTimeModelPackage.getClock(), null, "clockToForce", null, 0, -1, CodeExecClockBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(objectVariableEClass, ObjectVariable.class, "ObjectVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getObjectVariable_Name(), ecorePackage.getEString(), "name", null, 1, 1, ObjectVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectVariable_Type(), theTypesPackage.getJvmTypeReference(), null, "type", null, 1, 1, ObjectVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(codeExecutionSpecificationEClass, CodeExecutionSpecification.class, "CodeExecutionSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCodeExecutionSpecification_ModelImports(), theTimeModelPackage.getImportStatement(), null, "modelImports", null, 0, -1, CodeExecutionSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodeExecutionSpecification_Variables(), this.getObjectVariable(), null, "variables", null, 0, -1, CodeExecutionSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodeExecutionSpecification_Behaviors(), this.getCodeExecutionModelBehavior(), null, "behaviors", null, 0, -1, CodeExecutionSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodeExecutionSpecification_ClassImports(), this.getImportJavaStatement(), null, "classImports", null, 0, -1, CodeExecutionSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(codeExecRelationBehaviorEClass, CodeExecRelationBehavior.class, "CodeExecRelationBehavior", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCodeExecRelationBehavior_Relation(), theClockExpressionAndRelationPackage.getRelation(), null, "relation", null, 1, 1, CodeExecRelationBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(codeExecutionModelBehaviorEClass, CodeExecutionModelBehavior.class, "CodeExecutionModelBehavior", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCodeExecutionModelBehavior_ObjectVariable(), this.getObjectVariable(), null, "objectVariable", null, 1, 1, CodeExecutionModelBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodeExecutionModelBehavior_Method(), theTypesPackage.getJvmOperation(), null, "method", null, 1, 1, CodeExecutionModelBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(codeExecAssertionBehaviorEClass, CodeExecAssertionBehavior.class, "CodeExecAssertionBehavior", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCodeExecAssertionBehavior_Assertion(), theClockExpressionAndRelationPackage.getRelation(), null, "assertion", null, 1, 1, CodeExecAssertionBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodeExecAssertionBehavior_Failed(), ecorePackage.getEBoolean(), "failed", "false", 1, 1, CodeExecAssertionBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodeExecAssertionBehavior_Satisfied(), ecorePackage.getEBoolean(), "satisfied", "false", 1, 1, CodeExecAssertionBehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(importJavaStatementEClass, ImportJavaStatement.class, "ImportJavaStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImportJavaStatement_ImportedNamespace(), ecorePackage.getEString(), "importedNamespace", null, 1, 1, ImportJavaStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(liveStateKindEEnum, LiveStateKind.class, "LiveStateKind");
		addEEnumLiteral(liveStateKindEEnum, LiveStateKind.IS_ALIVE);
		addEEnumLiteral(liveStateKindEEnum, LiveStateKind.IS_DEAD);

		// Create resource
		createResource(eNS_URI);
	}

} //CodeExecutionPackageImpl
