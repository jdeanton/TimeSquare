/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl;

import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionModelBehavior;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage;
import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.ObjectVariable;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.xtext.common.types.JvmOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Behavior</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionModelBehaviorImpl#getObjectVariable <em>Object Variable</em>}</li>
 *   <li>{@link fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.impl.CodeExecutionModelBehaviorImpl#getMethod <em>Method</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CodeExecutionModelBehaviorImpl extends EObjectImpl implements CodeExecutionModelBehavior {
	/**
	 * The cached value of the '{@link #getObjectVariable() <em>Object Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectVariable()
	 * @generated
	 * @ordered
	 */
	protected ObjectVariable objectVariable;

	/**
	 * The cached value of the '{@link #getMethod() <em>Method</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethod()
	 * @generated
	 * @ordered
	 */
	protected JvmOperation method;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeExecutionModelBehaviorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CodeExecutionPackage.Literals.CODE_EXECUTION_MODEL_BEHAVIOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectVariable getObjectVariable() {
		if (objectVariable != null && objectVariable.eIsProxy()) {
			InternalEObject oldObjectVariable = (InternalEObject)objectVariable;
			objectVariable = (ObjectVariable)eResolveProxy(oldObjectVariable);
			if (objectVariable != oldObjectVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE, oldObjectVariable, objectVariable));
			}
		}
		return objectVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectVariable basicGetObjectVariable() {
		return objectVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectVariable(ObjectVariable newObjectVariable) {
		ObjectVariable oldObjectVariable = objectVariable;
		objectVariable = newObjectVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE, oldObjectVariable, objectVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JvmOperation getMethod() {
		if (method != null && method.eIsProxy()) {
			InternalEObject oldMethod = (InternalEObject)method;
			method = (JvmOperation)eResolveProxy(oldMethod);
			if (method != oldMethod) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__METHOD, oldMethod, method));
			}
		}
		return method;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JvmOperation basicGetMethod() {
		return method;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod(JvmOperation newMethod) {
		JvmOperation oldMethod = method;
		method = newMethod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__METHOD, oldMethod, method));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE:
				if (resolve) return getObjectVariable();
				return basicGetObjectVariable();
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__METHOD:
				if (resolve) return getMethod();
				return basicGetMethod();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE:
				setObjectVariable((ObjectVariable)newValue);
				return;
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__METHOD:
				setMethod((JvmOperation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE:
				setObjectVariable((ObjectVariable)null);
				return;
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__METHOD:
				setMethod((JvmOperation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__OBJECT_VARIABLE:
				return objectVariable != null;
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR__METHOD:
				return method != null;
		}
		return super.eIsSet(featureID);
	}

} //CodeExecutionModelBehaviorImpl
