/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 */
package fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.util;

import fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.inria.aoste.timesquare.backend.codeexecution.model.CodeExecution.CodeExecutionPackage
 * @generated
 */
public class CodeExecutionSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CodeExecutionPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeExecutionSwitch() {
		if (modelPackage == null) {
			modelPackage = CodeExecutionPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case CodeExecutionPackage.CODE_EXEC_CLOCK_BEHAVIOR: {
				CodeExecClockBehavior codeExecClockBehavior = (CodeExecClockBehavior)theEObject;
				T result = caseCodeExecClockBehavior(codeExecClockBehavior);
				if (result == null) result = caseCodeExecutionModelBehavior(codeExecClockBehavior);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CodeExecutionPackage.OBJECT_VARIABLE: {
				ObjectVariable objectVariable = (ObjectVariable)theEObject;
				T result = caseObjectVariable(objectVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CodeExecutionPackage.CODE_EXECUTION_SPECIFICATION: {
				CodeExecutionSpecification codeExecutionSpecification = (CodeExecutionSpecification)theEObject;
				T result = caseCodeExecutionSpecification(codeExecutionSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CodeExecutionPackage.CODE_EXEC_RELATION_BEHAVIOR: {
				CodeExecRelationBehavior codeExecRelationBehavior = (CodeExecRelationBehavior)theEObject;
				T result = caseCodeExecRelationBehavior(codeExecRelationBehavior);
				if (result == null) result = caseCodeExecutionModelBehavior(codeExecRelationBehavior);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CodeExecutionPackage.CODE_EXECUTION_MODEL_BEHAVIOR: {
				CodeExecutionModelBehavior codeExecutionModelBehavior = (CodeExecutionModelBehavior)theEObject;
				T result = caseCodeExecutionModelBehavior(codeExecutionModelBehavior);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CodeExecutionPackage.CODE_EXEC_ASSERTION_BEHAVIOR: {
				CodeExecAssertionBehavior codeExecAssertionBehavior = (CodeExecAssertionBehavior)theEObject;
				T result = caseCodeExecAssertionBehavior(codeExecAssertionBehavior);
				if (result == null) result = caseCodeExecutionModelBehavior(codeExecAssertionBehavior);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CodeExecutionPackage.IMPORT_JAVA_STATEMENT: {
				ImportJavaStatement importJavaStatement = (ImportJavaStatement)theEObject;
				T result = caseImportJavaStatement(importJavaStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Exec Clock Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Exec Clock Behavior</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeExecClockBehavior(CodeExecClockBehavior object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectVariable(ObjectVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeExecutionSpecification(CodeExecutionSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Exec Relation Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Exec Relation Behavior</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeExecRelationBehavior(CodeExecRelationBehavior object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Behavior</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeExecutionModelBehavior(CodeExecutionModelBehavior object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Exec Assertion Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Exec Assertion Behavior</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeExecAssertionBehavior(CodeExecAssertionBehavior object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import Java Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import Java Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImportJavaStatement(ImportJavaStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //CodeExecutionSwitch
