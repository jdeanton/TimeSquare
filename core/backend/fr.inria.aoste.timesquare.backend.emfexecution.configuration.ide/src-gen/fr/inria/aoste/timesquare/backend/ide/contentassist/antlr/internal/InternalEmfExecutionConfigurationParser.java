package fr.inria.aoste.timesquare.backend.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.inria.aoste.timesquare.backend.services.EmfExecutionConfigurationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalEmfExecutionConfigurationParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'When'", "'DSA'", "'returns'", "'true'", "'avoid'", "';'", "'import'", "'importJar'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalEmfExecutionConfigurationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalEmfExecutionConfigurationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalEmfExecutionConfigurationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalEmfExecutionConfiguration.g"; }


    	private EmfExecutionConfigurationGrammarAccess grammarAccess;

    	public void setGrammarAccess(EmfExecutionConfigurationGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleEMFExecutionConfiguration"
    // InternalEmfExecutionConfiguration.g:52:1: entryRuleEMFExecutionConfiguration : ruleEMFExecutionConfiguration EOF ;
    public final void entryRuleEMFExecutionConfiguration() throws RecognitionException {
        try {
            // InternalEmfExecutionConfiguration.g:53:1: ( ruleEMFExecutionConfiguration EOF )
            // InternalEmfExecutionConfiguration.g:54:1: ruleEMFExecutionConfiguration EOF
            {
             before(grammarAccess.getEMFExecutionConfigurationRule()); 
            pushFollow(FOLLOW_1);
            ruleEMFExecutionConfiguration();

            state._fsp--;

             after(grammarAccess.getEMFExecutionConfigurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEMFExecutionConfiguration"


    // $ANTLR start "ruleEMFExecutionConfiguration"
    // InternalEmfExecutionConfiguration.g:61:1: ruleEMFExecutionConfiguration : ( ( rule__EMFExecutionConfiguration__Group__0 ) ) ;
    public final void ruleEMFExecutionConfiguration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:65:2: ( ( ( rule__EMFExecutionConfiguration__Group__0 ) ) )
            // InternalEmfExecutionConfiguration.g:66:2: ( ( rule__EMFExecutionConfiguration__Group__0 ) )
            {
            // InternalEmfExecutionConfiguration.g:66:2: ( ( rule__EMFExecutionConfiguration__Group__0 ) )
            // InternalEmfExecutionConfiguration.g:67:3: ( rule__EMFExecutionConfiguration__Group__0 )
            {
             before(grammarAccess.getEMFExecutionConfigurationAccess().getGroup()); 
            // InternalEmfExecutionConfiguration.g:68:3: ( rule__EMFExecutionConfiguration__Group__0 )
            // InternalEmfExecutionConfiguration.g:68:4: rule__EMFExecutionConfiguration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EMFExecutionConfiguration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEMFExecutionConfigurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEMFExecutionConfiguration"


    // $ANTLR start "entryRuleForcedClockMapping"
    // InternalEmfExecutionConfiguration.g:77:1: entryRuleForcedClockMapping : ruleForcedClockMapping EOF ;
    public final void entryRuleForcedClockMapping() throws RecognitionException {
        try {
            // InternalEmfExecutionConfiguration.g:78:1: ( ruleForcedClockMapping EOF )
            // InternalEmfExecutionConfiguration.g:79:1: ruleForcedClockMapping EOF
            {
             before(grammarAccess.getForcedClockMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleForcedClockMapping();

            state._fsp--;

             after(grammarAccess.getForcedClockMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleForcedClockMapping"


    // $ANTLR start "ruleForcedClockMapping"
    // InternalEmfExecutionConfiguration.g:86:1: ruleForcedClockMapping : ( ( rule__ForcedClockMapping__Group__0 ) ) ;
    public final void ruleForcedClockMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:90:2: ( ( ( rule__ForcedClockMapping__Group__0 ) ) )
            // InternalEmfExecutionConfiguration.g:91:2: ( ( rule__ForcedClockMapping__Group__0 ) )
            {
            // InternalEmfExecutionConfiguration.g:91:2: ( ( rule__ForcedClockMapping__Group__0 ) )
            // InternalEmfExecutionConfiguration.g:92:3: ( rule__ForcedClockMapping__Group__0 )
            {
             before(grammarAccess.getForcedClockMappingAccess().getGroup()); 
            // InternalEmfExecutionConfiguration.g:93:3: ( rule__ForcedClockMapping__Group__0 )
            // InternalEmfExecutionConfiguration.g:93:4: rule__ForcedClockMapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getForcedClockMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleForcedClockMapping"


    // $ANTLR start "entryRuleImportStatement"
    // InternalEmfExecutionConfiguration.g:102:1: entryRuleImportStatement : ruleImportStatement EOF ;
    public final void entryRuleImportStatement() throws RecognitionException {
        try {
            // InternalEmfExecutionConfiguration.g:103:1: ( ruleImportStatement EOF )
            // InternalEmfExecutionConfiguration.g:104:1: ruleImportStatement EOF
            {
             before(grammarAccess.getImportStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleImportStatement();

            state._fsp--;

             after(grammarAccess.getImportStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalEmfExecutionConfiguration.g:111:1: ruleImportStatement : ( ( rule__ImportStatement__Group__0 ) ) ;
    public final void ruleImportStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:115:2: ( ( ( rule__ImportStatement__Group__0 ) ) )
            // InternalEmfExecutionConfiguration.g:116:2: ( ( rule__ImportStatement__Group__0 ) )
            {
            // InternalEmfExecutionConfiguration.g:116:2: ( ( rule__ImportStatement__Group__0 ) )
            // InternalEmfExecutionConfiguration.g:117:3: ( rule__ImportStatement__Group__0 )
            {
             before(grammarAccess.getImportStatementAccess().getGroup()); 
            // InternalEmfExecutionConfiguration.g:118:3: ( rule__ImportStatement__Group__0 )
            // InternalEmfExecutionConfiguration.g:118:4: rule__ImportStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleJarImportStatement"
    // InternalEmfExecutionConfiguration.g:127:1: entryRuleJarImportStatement : ruleJarImportStatement EOF ;
    public final void entryRuleJarImportStatement() throws RecognitionException {
        try {
            // InternalEmfExecutionConfiguration.g:128:1: ( ruleJarImportStatement EOF )
            // InternalEmfExecutionConfiguration.g:129:1: ruleJarImportStatement EOF
            {
             before(grammarAccess.getJarImportStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleJarImportStatement();

            state._fsp--;

             after(grammarAccess.getJarImportStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleJarImportStatement"


    // $ANTLR start "ruleJarImportStatement"
    // InternalEmfExecutionConfiguration.g:136:1: ruleJarImportStatement : ( ( rule__JarImportStatement__Group__0 ) ) ;
    public final void ruleJarImportStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:140:2: ( ( ( rule__JarImportStatement__Group__0 ) ) )
            // InternalEmfExecutionConfiguration.g:141:2: ( ( rule__JarImportStatement__Group__0 ) )
            {
            // InternalEmfExecutionConfiguration.g:141:2: ( ( rule__JarImportStatement__Group__0 ) )
            // InternalEmfExecutionConfiguration.g:142:3: ( rule__JarImportStatement__Group__0 )
            {
             before(grammarAccess.getJarImportStatementAccess().getGroup()); 
            // InternalEmfExecutionConfiguration.g:143:3: ( rule__JarImportStatement__Group__0 )
            // InternalEmfExecutionConfiguration.g:143:4: rule__JarImportStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__JarImportStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getJarImportStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleJarImportStatement"


    // $ANTLR start "rule__EMFExecutionConfiguration__Group__0"
    // InternalEmfExecutionConfiguration.g:151:1: rule__EMFExecutionConfiguration__Group__0 : rule__EMFExecutionConfiguration__Group__0__Impl rule__EMFExecutionConfiguration__Group__1 ;
    public final void rule__EMFExecutionConfiguration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:155:1: ( rule__EMFExecutionConfiguration__Group__0__Impl rule__EMFExecutionConfiguration__Group__1 )
            // InternalEmfExecutionConfiguration.g:156:2: rule__EMFExecutionConfiguration__Group__0__Impl rule__EMFExecutionConfiguration__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__EMFExecutionConfiguration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EMFExecutionConfiguration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__Group__0"


    // $ANTLR start "rule__EMFExecutionConfiguration__Group__0__Impl"
    // InternalEmfExecutionConfiguration.g:163:1: rule__EMFExecutionConfiguration__Group__0__Impl : ( ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 ) ) ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )* ) ) ;
    public final void rule__EMFExecutionConfiguration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:167:1: ( ( ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 ) ) ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )* ) ) )
            // InternalEmfExecutionConfiguration.g:168:1: ( ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 ) ) ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )* ) )
            {
            // InternalEmfExecutionConfiguration.g:168:1: ( ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 ) ) ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )* ) )
            // InternalEmfExecutionConfiguration.g:169:2: ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 ) ) ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )* )
            {
            // InternalEmfExecutionConfiguration.g:169:2: ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 ) )
            // InternalEmfExecutionConfiguration.g:170:3: ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )
            {
             before(grammarAccess.getEMFExecutionConfigurationAccess().getImportsAssignment_0()); 
            // InternalEmfExecutionConfiguration.g:171:3: ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )
            // InternalEmfExecutionConfiguration.g:171:4: rule__EMFExecutionConfiguration__ImportsAssignment_0
            {
            pushFollow(FOLLOW_4);
            rule__EMFExecutionConfiguration__ImportsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getEMFExecutionConfigurationAccess().getImportsAssignment_0()); 

            }

            // InternalEmfExecutionConfiguration.g:174:2: ( ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )* )
            // InternalEmfExecutionConfiguration.g:175:3: ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )*
            {
             before(grammarAccess.getEMFExecutionConfigurationAccess().getImportsAssignment_0()); 
            // InternalEmfExecutionConfiguration.g:176:3: ( rule__EMFExecutionConfiguration__ImportsAssignment_0 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==17) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalEmfExecutionConfiguration.g:176:4: rule__EMFExecutionConfiguration__ImportsAssignment_0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__EMFExecutionConfiguration__ImportsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getEMFExecutionConfigurationAccess().getImportsAssignment_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__Group__0__Impl"


    // $ANTLR start "rule__EMFExecutionConfiguration__Group__1"
    // InternalEmfExecutionConfiguration.g:185:1: rule__EMFExecutionConfiguration__Group__1 : rule__EMFExecutionConfiguration__Group__1__Impl rule__EMFExecutionConfiguration__Group__2 ;
    public final void rule__EMFExecutionConfiguration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:189:1: ( rule__EMFExecutionConfiguration__Group__1__Impl rule__EMFExecutionConfiguration__Group__2 )
            // InternalEmfExecutionConfiguration.g:190:2: rule__EMFExecutionConfiguration__Group__1__Impl rule__EMFExecutionConfiguration__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__EMFExecutionConfiguration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EMFExecutionConfiguration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__Group__1"


    // $ANTLR start "rule__EMFExecutionConfiguration__Group__1__Impl"
    // InternalEmfExecutionConfiguration.g:197:1: rule__EMFExecutionConfiguration__Group__1__Impl : ( ( rule__EMFExecutionConfiguration__JarImportStatementAssignment_1 ) ) ;
    public final void rule__EMFExecutionConfiguration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:201:1: ( ( ( rule__EMFExecutionConfiguration__JarImportStatementAssignment_1 ) ) )
            // InternalEmfExecutionConfiguration.g:202:1: ( ( rule__EMFExecutionConfiguration__JarImportStatementAssignment_1 ) )
            {
            // InternalEmfExecutionConfiguration.g:202:1: ( ( rule__EMFExecutionConfiguration__JarImportStatementAssignment_1 ) )
            // InternalEmfExecutionConfiguration.g:203:2: ( rule__EMFExecutionConfiguration__JarImportStatementAssignment_1 )
            {
             before(grammarAccess.getEMFExecutionConfigurationAccess().getJarImportStatementAssignment_1()); 
            // InternalEmfExecutionConfiguration.g:204:2: ( rule__EMFExecutionConfiguration__JarImportStatementAssignment_1 )
            // InternalEmfExecutionConfiguration.g:204:3: rule__EMFExecutionConfiguration__JarImportStatementAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__EMFExecutionConfiguration__JarImportStatementAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEMFExecutionConfigurationAccess().getJarImportStatementAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__Group__1__Impl"


    // $ANTLR start "rule__EMFExecutionConfiguration__Group__2"
    // InternalEmfExecutionConfiguration.g:212:1: rule__EMFExecutionConfiguration__Group__2 : rule__EMFExecutionConfiguration__Group__2__Impl ;
    public final void rule__EMFExecutionConfiguration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:216:1: ( rule__EMFExecutionConfiguration__Group__2__Impl )
            // InternalEmfExecutionConfiguration.g:217:2: rule__EMFExecutionConfiguration__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EMFExecutionConfiguration__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__Group__2"


    // $ANTLR start "rule__EMFExecutionConfiguration__Group__2__Impl"
    // InternalEmfExecutionConfiguration.g:223:1: rule__EMFExecutionConfiguration__Group__2__Impl : ( ( rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2 )* ) ;
    public final void rule__EMFExecutionConfiguration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:227:1: ( ( ( rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2 )* ) )
            // InternalEmfExecutionConfiguration.g:228:1: ( ( rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2 )* )
            {
            // InternalEmfExecutionConfiguration.g:228:1: ( ( rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2 )* )
            // InternalEmfExecutionConfiguration.g:229:2: ( rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2 )*
            {
             before(grammarAccess.getEMFExecutionConfigurationAccess().getForcedClockMappingsAssignment_2()); 
            // InternalEmfExecutionConfiguration.g:230:2: ( rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==11) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalEmfExecutionConfiguration.g:230:3: rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getEMFExecutionConfigurationAccess().getForcedClockMappingsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__Group__2__Impl"


    // $ANTLR start "rule__ForcedClockMapping__Group__0"
    // InternalEmfExecutionConfiguration.g:239:1: rule__ForcedClockMapping__Group__0 : rule__ForcedClockMapping__Group__0__Impl rule__ForcedClockMapping__Group__1 ;
    public final void rule__ForcedClockMapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:243:1: ( rule__ForcedClockMapping__Group__0__Impl rule__ForcedClockMapping__Group__1 )
            // InternalEmfExecutionConfiguration.g:244:2: rule__ForcedClockMapping__Group__0__Impl rule__ForcedClockMapping__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__ForcedClockMapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__0"


    // $ANTLR start "rule__ForcedClockMapping__Group__0__Impl"
    // InternalEmfExecutionConfiguration.g:251:1: rule__ForcedClockMapping__Group__0__Impl : ( 'When' ) ;
    public final void rule__ForcedClockMapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:255:1: ( ( 'When' ) )
            // InternalEmfExecutionConfiguration.g:256:1: ( 'When' )
            {
            // InternalEmfExecutionConfiguration.g:256:1: ( 'When' )
            // InternalEmfExecutionConfiguration.g:257:2: 'When'
            {
             before(grammarAccess.getForcedClockMappingAccess().getWhenKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getForcedClockMappingAccess().getWhenKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__0__Impl"


    // $ANTLR start "rule__ForcedClockMapping__Group__1"
    // InternalEmfExecutionConfiguration.g:266:1: rule__ForcedClockMapping__Group__1 : rule__ForcedClockMapping__Group__1__Impl rule__ForcedClockMapping__Group__2 ;
    public final void rule__ForcedClockMapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:270:1: ( rule__ForcedClockMapping__Group__1__Impl rule__ForcedClockMapping__Group__2 )
            // InternalEmfExecutionConfiguration.g:271:2: rule__ForcedClockMapping__Group__1__Impl rule__ForcedClockMapping__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__ForcedClockMapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__1"


    // $ANTLR start "rule__ForcedClockMapping__Group__1__Impl"
    // InternalEmfExecutionConfiguration.g:278:1: rule__ForcedClockMapping__Group__1__Impl : ( ( rule__ForcedClockMapping__ClockAssignment_1 ) ) ;
    public final void rule__ForcedClockMapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:282:1: ( ( ( rule__ForcedClockMapping__ClockAssignment_1 ) ) )
            // InternalEmfExecutionConfiguration.g:283:1: ( ( rule__ForcedClockMapping__ClockAssignment_1 ) )
            {
            // InternalEmfExecutionConfiguration.g:283:1: ( ( rule__ForcedClockMapping__ClockAssignment_1 ) )
            // InternalEmfExecutionConfiguration.g:284:2: ( rule__ForcedClockMapping__ClockAssignment_1 )
            {
             before(grammarAccess.getForcedClockMappingAccess().getClockAssignment_1()); 
            // InternalEmfExecutionConfiguration.g:285:2: ( rule__ForcedClockMapping__ClockAssignment_1 )
            // InternalEmfExecutionConfiguration.g:285:3: rule__ForcedClockMapping__ClockAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__ClockAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getForcedClockMappingAccess().getClockAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__1__Impl"


    // $ANTLR start "rule__ForcedClockMapping__Group__2"
    // InternalEmfExecutionConfiguration.g:293:1: rule__ForcedClockMapping__Group__2 : rule__ForcedClockMapping__Group__2__Impl rule__ForcedClockMapping__Group__3 ;
    public final void rule__ForcedClockMapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:297:1: ( rule__ForcedClockMapping__Group__2__Impl rule__ForcedClockMapping__Group__3 )
            // InternalEmfExecutionConfiguration.g:298:2: rule__ForcedClockMapping__Group__2__Impl rule__ForcedClockMapping__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__ForcedClockMapping__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__2"


    // $ANTLR start "rule__ForcedClockMapping__Group__2__Impl"
    // InternalEmfExecutionConfiguration.g:305:1: rule__ForcedClockMapping__Group__2__Impl : ( 'DSA' ) ;
    public final void rule__ForcedClockMapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:309:1: ( ( 'DSA' ) )
            // InternalEmfExecutionConfiguration.g:310:1: ( 'DSA' )
            {
            // InternalEmfExecutionConfiguration.g:310:1: ( 'DSA' )
            // InternalEmfExecutionConfiguration.g:311:2: 'DSA'
            {
             before(grammarAccess.getForcedClockMappingAccess().getDSAKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getForcedClockMappingAccess().getDSAKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__2__Impl"


    // $ANTLR start "rule__ForcedClockMapping__Group__3"
    // InternalEmfExecutionConfiguration.g:320:1: rule__ForcedClockMapping__Group__3 : rule__ForcedClockMapping__Group__3__Impl rule__ForcedClockMapping__Group__4 ;
    public final void rule__ForcedClockMapping__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:324:1: ( rule__ForcedClockMapping__Group__3__Impl rule__ForcedClockMapping__Group__4 )
            // InternalEmfExecutionConfiguration.g:325:2: rule__ForcedClockMapping__Group__3__Impl rule__ForcedClockMapping__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__ForcedClockMapping__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__3"


    // $ANTLR start "rule__ForcedClockMapping__Group__3__Impl"
    // InternalEmfExecutionConfiguration.g:332:1: rule__ForcedClockMapping__Group__3__Impl : ( 'returns' ) ;
    public final void rule__ForcedClockMapping__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:336:1: ( ( 'returns' ) )
            // InternalEmfExecutionConfiguration.g:337:1: ( 'returns' )
            {
            // InternalEmfExecutionConfiguration.g:337:1: ( 'returns' )
            // InternalEmfExecutionConfiguration.g:338:2: 'returns'
            {
             before(grammarAccess.getForcedClockMappingAccess().getReturnsKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getForcedClockMappingAccess().getReturnsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__3__Impl"


    // $ANTLR start "rule__ForcedClockMapping__Group__4"
    // InternalEmfExecutionConfiguration.g:347:1: rule__ForcedClockMapping__Group__4 : rule__ForcedClockMapping__Group__4__Impl rule__ForcedClockMapping__Group__5 ;
    public final void rule__ForcedClockMapping__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:351:1: ( rule__ForcedClockMapping__Group__4__Impl rule__ForcedClockMapping__Group__5 )
            // InternalEmfExecutionConfiguration.g:352:2: rule__ForcedClockMapping__Group__4__Impl rule__ForcedClockMapping__Group__5
            {
            pushFollow(FOLLOW_11);
            rule__ForcedClockMapping__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__4"


    // $ANTLR start "rule__ForcedClockMapping__Group__4__Impl"
    // InternalEmfExecutionConfiguration.g:359:1: rule__ForcedClockMapping__Group__4__Impl : ( 'true' ) ;
    public final void rule__ForcedClockMapping__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:363:1: ( ( 'true' ) )
            // InternalEmfExecutionConfiguration.g:364:1: ( 'true' )
            {
            // InternalEmfExecutionConfiguration.g:364:1: ( 'true' )
            // InternalEmfExecutionConfiguration.g:365:2: 'true'
            {
             before(grammarAccess.getForcedClockMappingAccess().getTrueKeyword_4()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getForcedClockMappingAccess().getTrueKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__4__Impl"


    // $ANTLR start "rule__ForcedClockMapping__Group__5"
    // InternalEmfExecutionConfiguration.g:374:1: rule__ForcedClockMapping__Group__5 : rule__ForcedClockMapping__Group__5__Impl rule__ForcedClockMapping__Group__6 ;
    public final void rule__ForcedClockMapping__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:378:1: ( rule__ForcedClockMapping__Group__5__Impl rule__ForcedClockMapping__Group__6 )
            // InternalEmfExecutionConfiguration.g:379:2: rule__ForcedClockMapping__Group__5__Impl rule__ForcedClockMapping__Group__6
            {
            pushFollow(FOLLOW_7);
            rule__ForcedClockMapping__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__5"


    // $ANTLR start "rule__ForcedClockMapping__Group__5__Impl"
    // InternalEmfExecutionConfiguration.g:386:1: rule__ForcedClockMapping__Group__5__Impl : ( 'avoid' ) ;
    public final void rule__ForcedClockMapping__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:390:1: ( ( 'avoid' ) )
            // InternalEmfExecutionConfiguration.g:391:1: ( 'avoid' )
            {
            // InternalEmfExecutionConfiguration.g:391:1: ( 'avoid' )
            // InternalEmfExecutionConfiguration.g:392:2: 'avoid'
            {
             before(grammarAccess.getForcedClockMappingAccess().getAvoidKeyword_5()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getForcedClockMappingAccess().getAvoidKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__5__Impl"


    // $ANTLR start "rule__ForcedClockMapping__Group__6"
    // InternalEmfExecutionConfiguration.g:401:1: rule__ForcedClockMapping__Group__6 : rule__ForcedClockMapping__Group__6__Impl rule__ForcedClockMapping__Group__7 ;
    public final void rule__ForcedClockMapping__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:405:1: ( rule__ForcedClockMapping__Group__6__Impl rule__ForcedClockMapping__Group__7 )
            // InternalEmfExecutionConfiguration.g:406:2: rule__ForcedClockMapping__Group__6__Impl rule__ForcedClockMapping__Group__7
            {
            pushFollow(FOLLOW_12);
            rule__ForcedClockMapping__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__6"


    // $ANTLR start "rule__ForcedClockMapping__Group__6__Impl"
    // InternalEmfExecutionConfiguration.g:413:1: rule__ForcedClockMapping__Group__6__Impl : ( ( rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6 ) ) ;
    public final void rule__ForcedClockMapping__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:417:1: ( ( ( rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6 ) ) )
            // InternalEmfExecutionConfiguration.g:418:1: ( ( rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6 ) )
            {
            // InternalEmfExecutionConfiguration.g:418:1: ( ( rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6 ) )
            // InternalEmfExecutionConfiguration.g:419:2: ( rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6 )
            {
             before(grammarAccess.getForcedClockMappingAccess().getClockToAvoidWhenTrueAssignment_6()); 
            // InternalEmfExecutionConfiguration.g:420:2: ( rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6 )
            // InternalEmfExecutionConfiguration.g:420:3: rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getForcedClockMappingAccess().getClockToAvoidWhenTrueAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__6__Impl"


    // $ANTLR start "rule__ForcedClockMapping__Group__7"
    // InternalEmfExecutionConfiguration.g:428:1: rule__ForcedClockMapping__Group__7 : rule__ForcedClockMapping__Group__7__Impl ;
    public final void rule__ForcedClockMapping__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:432:1: ( rule__ForcedClockMapping__Group__7__Impl )
            // InternalEmfExecutionConfiguration.g:433:2: rule__ForcedClockMapping__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ForcedClockMapping__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__7"


    // $ANTLR start "rule__ForcedClockMapping__Group__7__Impl"
    // InternalEmfExecutionConfiguration.g:439:1: rule__ForcedClockMapping__Group__7__Impl : ( ';' ) ;
    public final void rule__ForcedClockMapping__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:443:1: ( ( ';' ) )
            // InternalEmfExecutionConfiguration.g:444:1: ( ';' )
            {
            // InternalEmfExecutionConfiguration.g:444:1: ( ';' )
            // InternalEmfExecutionConfiguration.g:445:2: ';'
            {
             before(grammarAccess.getForcedClockMappingAccess().getSemicolonKeyword_7()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getForcedClockMappingAccess().getSemicolonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__Group__7__Impl"


    // $ANTLR start "rule__ImportStatement__Group__0"
    // InternalEmfExecutionConfiguration.g:455:1: rule__ImportStatement__Group__0 : rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1 ;
    public final void rule__ImportStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:459:1: ( rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1 )
            // InternalEmfExecutionConfiguration.g:460:2: rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__ImportStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__0"


    // $ANTLR start "rule__ImportStatement__Group__0__Impl"
    // InternalEmfExecutionConfiguration.g:467:1: rule__ImportStatement__Group__0__Impl : ( 'import' ) ;
    public final void rule__ImportStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:471:1: ( ( 'import' ) )
            // InternalEmfExecutionConfiguration.g:472:1: ( 'import' )
            {
            // InternalEmfExecutionConfiguration.g:472:1: ( 'import' )
            // InternalEmfExecutionConfiguration.g:473:2: 'import'
            {
             before(grammarAccess.getImportStatementAccess().getImportKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__0__Impl"


    // $ANTLR start "rule__ImportStatement__Group__1"
    // InternalEmfExecutionConfiguration.g:482:1: rule__ImportStatement__Group__1 : rule__ImportStatement__Group__1__Impl rule__ImportStatement__Group__2 ;
    public final void rule__ImportStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:486:1: ( rule__ImportStatement__Group__1__Impl rule__ImportStatement__Group__2 )
            // InternalEmfExecutionConfiguration.g:487:2: rule__ImportStatement__Group__1__Impl rule__ImportStatement__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__ImportStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__1"


    // $ANTLR start "rule__ImportStatement__Group__1__Impl"
    // InternalEmfExecutionConfiguration.g:494:1: rule__ImportStatement__Group__1__Impl : ( ( rule__ImportStatement__ImportURIAssignment_1 ) ) ;
    public final void rule__ImportStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:498:1: ( ( ( rule__ImportStatement__ImportURIAssignment_1 ) ) )
            // InternalEmfExecutionConfiguration.g:499:1: ( ( rule__ImportStatement__ImportURIAssignment_1 ) )
            {
            // InternalEmfExecutionConfiguration.g:499:1: ( ( rule__ImportStatement__ImportURIAssignment_1 ) )
            // InternalEmfExecutionConfiguration.g:500:2: ( rule__ImportStatement__ImportURIAssignment_1 )
            {
             before(grammarAccess.getImportStatementAccess().getImportURIAssignment_1()); 
            // InternalEmfExecutionConfiguration.g:501:2: ( rule__ImportStatement__ImportURIAssignment_1 )
            // InternalEmfExecutionConfiguration.g:501:3: rule__ImportStatement__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__1__Impl"


    // $ANTLR start "rule__ImportStatement__Group__2"
    // InternalEmfExecutionConfiguration.g:509:1: rule__ImportStatement__Group__2 : rule__ImportStatement__Group__2__Impl ;
    public final void rule__ImportStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:513:1: ( rule__ImportStatement__Group__2__Impl )
            // InternalEmfExecutionConfiguration.g:514:2: rule__ImportStatement__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__2"


    // $ANTLR start "rule__ImportStatement__Group__2__Impl"
    // InternalEmfExecutionConfiguration.g:520:1: rule__ImportStatement__Group__2__Impl : ( ';' ) ;
    public final void rule__ImportStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:524:1: ( ( ';' ) )
            // InternalEmfExecutionConfiguration.g:525:1: ( ';' )
            {
            // InternalEmfExecutionConfiguration.g:525:1: ( ';' )
            // InternalEmfExecutionConfiguration.g:526:2: ';'
            {
             before(grammarAccess.getImportStatementAccess().getSemicolonKeyword_2()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__2__Impl"


    // $ANTLR start "rule__JarImportStatement__Group__0"
    // InternalEmfExecutionConfiguration.g:536:1: rule__JarImportStatement__Group__0 : rule__JarImportStatement__Group__0__Impl rule__JarImportStatement__Group__1 ;
    public final void rule__JarImportStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:540:1: ( rule__JarImportStatement__Group__0__Impl rule__JarImportStatement__Group__1 )
            // InternalEmfExecutionConfiguration.g:541:2: rule__JarImportStatement__Group__0__Impl rule__JarImportStatement__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__JarImportStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JarImportStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JarImportStatement__Group__0"


    // $ANTLR start "rule__JarImportStatement__Group__0__Impl"
    // InternalEmfExecutionConfiguration.g:548:1: rule__JarImportStatement__Group__0__Impl : ( 'importJar' ) ;
    public final void rule__JarImportStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:552:1: ( ( 'importJar' ) )
            // InternalEmfExecutionConfiguration.g:553:1: ( 'importJar' )
            {
            // InternalEmfExecutionConfiguration.g:553:1: ( 'importJar' )
            // InternalEmfExecutionConfiguration.g:554:2: 'importJar'
            {
             before(grammarAccess.getJarImportStatementAccess().getImportJarKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getJarImportStatementAccess().getImportJarKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JarImportStatement__Group__0__Impl"


    // $ANTLR start "rule__JarImportStatement__Group__1"
    // InternalEmfExecutionConfiguration.g:563:1: rule__JarImportStatement__Group__1 : rule__JarImportStatement__Group__1__Impl rule__JarImportStatement__Group__2 ;
    public final void rule__JarImportStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:567:1: ( rule__JarImportStatement__Group__1__Impl rule__JarImportStatement__Group__2 )
            // InternalEmfExecutionConfiguration.g:568:2: rule__JarImportStatement__Group__1__Impl rule__JarImportStatement__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__JarImportStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JarImportStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JarImportStatement__Group__1"


    // $ANTLR start "rule__JarImportStatement__Group__1__Impl"
    // InternalEmfExecutionConfiguration.g:575:1: rule__JarImportStatement__Group__1__Impl : ( ( rule__JarImportStatement__PathToJarAssignment_1 ) ) ;
    public final void rule__JarImportStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:579:1: ( ( ( rule__JarImportStatement__PathToJarAssignment_1 ) ) )
            // InternalEmfExecutionConfiguration.g:580:1: ( ( rule__JarImportStatement__PathToJarAssignment_1 ) )
            {
            // InternalEmfExecutionConfiguration.g:580:1: ( ( rule__JarImportStatement__PathToJarAssignment_1 ) )
            // InternalEmfExecutionConfiguration.g:581:2: ( rule__JarImportStatement__PathToJarAssignment_1 )
            {
             before(grammarAccess.getJarImportStatementAccess().getPathToJarAssignment_1()); 
            // InternalEmfExecutionConfiguration.g:582:2: ( rule__JarImportStatement__PathToJarAssignment_1 )
            // InternalEmfExecutionConfiguration.g:582:3: rule__JarImportStatement__PathToJarAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__JarImportStatement__PathToJarAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getJarImportStatementAccess().getPathToJarAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JarImportStatement__Group__1__Impl"


    // $ANTLR start "rule__JarImportStatement__Group__2"
    // InternalEmfExecutionConfiguration.g:590:1: rule__JarImportStatement__Group__2 : rule__JarImportStatement__Group__2__Impl ;
    public final void rule__JarImportStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:594:1: ( rule__JarImportStatement__Group__2__Impl )
            // InternalEmfExecutionConfiguration.g:595:2: rule__JarImportStatement__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__JarImportStatement__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JarImportStatement__Group__2"


    // $ANTLR start "rule__JarImportStatement__Group__2__Impl"
    // InternalEmfExecutionConfiguration.g:601:1: rule__JarImportStatement__Group__2__Impl : ( ';' ) ;
    public final void rule__JarImportStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:605:1: ( ( ';' ) )
            // InternalEmfExecutionConfiguration.g:606:1: ( ';' )
            {
            // InternalEmfExecutionConfiguration.g:606:1: ( ';' )
            // InternalEmfExecutionConfiguration.g:607:2: ';'
            {
             before(grammarAccess.getJarImportStatementAccess().getSemicolonKeyword_2()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getJarImportStatementAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JarImportStatement__Group__2__Impl"


    // $ANTLR start "rule__EMFExecutionConfiguration__ImportsAssignment_0"
    // InternalEmfExecutionConfiguration.g:617:1: rule__EMFExecutionConfiguration__ImportsAssignment_0 : ( ruleImportStatement ) ;
    public final void rule__EMFExecutionConfiguration__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:621:1: ( ( ruleImportStatement ) )
            // InternalEmfExecutionConfiguration.g:622:2: ( ruleImportStatement )
            {
            // InternalEmfExecutionConfiguration.g:622:2: ( ruleImportStatement )
            // InternalEmfExecutionConfiguration.g:623:3: ruleImportStatement
            {
             before(grammarAccess.getEMFExecutionConfigurationAccess().getImportsImportStatementParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImportStatement();

            state._fsp--;

             after(grammarAccess.getEMFExecutionConfigurationAccess().getImportsImportStatementParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__ImportsAssignment_0"


    // $ANTLR start "rule__EMFExecutionConfiguration__JarImportStatementAssignment_1"
    // InternalEmfExecutionConfiguration.g:632:1: rule__EMFExecutionConfiguration__JarImportStatementAssignment_1 : ( ruleJarImportStatement ) ;
    public final void rule__EMFExecutionConfiguration__JarImportStatementAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:636:1: ( ( ruleJarImportStatement ) )
            // InternalEmfExecutionConfiguration.g:637:2: ( ruleJarImportStatement )
            {
            // InternalEmfExecutionConfiguration.g:637:2: ( ruleJarImportStatement )
            // InternalEmfExecutionConfiguration.g:638:3: ruleJarImportStatement
            {
             before(grammarAccess.getEMFExecutionConfigurationAccess().getJarImportStatementJarImportStatementParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleJarImportStatement();

            state._fsp--;

             after(grammarAccess.getEMFExecutionConfigurationAccess().getJarImportStatementJarImportStatementParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__JarImportStatementAssignment_1"


    // $ANTLR start "rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2"
    // InternalEmfExecutionConfiguration.g:647:1: rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2 : ( ruleForcedClockMapping ) ;
    public final void rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:651:1: ( ( ruleForcedClockMapping ) )
            // InternalEmfExecutionConfiguration.g:652:2: ( ruleForcedClockMapping )
            {
            // InternalEmfExecutionConfiguration.g:652:2: ( ruleForcedClockMapping )
            // InternalEmfExecutionConfiguration.g:653:3: ruleForcedClockMapping
            {
             before(grammarAccess.getEMFExecutionConfigurationAccess().getForcedClockMappingsForcedClockMappingParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleForcedClockMapping();

            state._fsp--;

             after(grammarAccess.getEMFExecutionConfigurationAccess().getForcedClockMappingsForcedClockMappingParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EMFExecutionConfiguration__ForcedClockMappingsAssignment_2"


    // $ANTLR start "rule__ForcedClockMapping__ClockAssignment_1"
    // InternalEmfExecutionConfiguration.g:662:1: rule__ForcedClockMapping__ClockAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ForcedClockMapping__ClockAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:666:1: ( ( ( RULE_ID ) ) )
            // InternalEmfExecutionConfiguration.g:667:2: ( ( RULE_ID ) )
            {
            // InternalEmfExecutionConfiguration.g:667:2: ( ( RULE_ID ) )
            // InternalEmfExecutionConfiguration.g:668:3: ( RULE_ID )
            {
             before(grammarAccess.getForcedClockMappingAccess().getClockClockCrossReference_1_0()); 
            // InternalEmfExecutionConfiguration.g:669:3: ( RULE_ID )
            // InternalEmfExecutionConfiguration.g:670:4: RULE_ID
            {
             before(grammarAccess.getForcedClockMappingAccess().getClockClockIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getForcedClockMappingAccess().getClockClockIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getForcedClockMappingAccess().getClockClockCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__ClockAssignment_1"


    // $ANTLR start "rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6"
    // InternalEmfExecutionConfiguration.g:681:1: rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6 : ( ( RULE_ID ) ) ;
    public final void rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:685:1: ( ( ( RULE_ID ) ) )
            // InternalEmfExecutionConfiguration.g:686:2: ( ( RULE_ID ) )
            {
            // InternalEmfExecutionConfiguration.g:686:2: ( ( RULE_ID ) )
            // InternalEmfExecutionConfiguration.g:687:3: ( RULE_ID )
            {
             before(grammarAccess.getForcedClockMappingAccess().getClockToAvoidWhenTrueClockCrossReference_6_0()); 
            // InternalEmfExecutionConfiguration.g:688:3: ( RULE_ID )
            // InternalEmfExecutionConfiguration.g:689:4: RULE_ID
            {
             before(grammarAccess.getForcedClockMappingAccess().getClockToAvoidWhenTrueClockIDTerminalRuleCall_6_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getForcedClockMappingAccess().getClockToAvoidWhenTrueClockIDTerminalRuleCall_6_0_1()); 

            }

             after(grammarAccess.getForcedClockMappingAccess().getClockToAvoidWhenTrueClockCrossReference_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForcedClockMapping__ClockToAvoidWhenTrueAssignment_6"


    // $ANTLR start "rule__ImportStatement__ImportURIAssignment_1"
    // InternalEmfExecutionConfiguration.g:700:1: rule__ImportStatement__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__ImportStatement__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:704:1: ( ( RULE_STRING ) )
            // InternalEmfExecutionConfiguration.g:705:2: ( RULE_STRING )
            {
            // InternalEmfExecutionConfiguration.g:705:2: ( RULE_STRING )
            // InternalEmfExecutionConfiguration.g:706:3: RULE_STRING
            {
             before(grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__ImportURIAssignment_1"


    // $ANTLR start "rule__JarImportStatement__PathToJarAssignment_1"
    // InternalEmfExecutionConfiguration.g:715:1: rule__JarImportStatement__PathToJarAssignment_1 : ( RULE_STRING ) ;
    public final void rule__JarImportStatement__PathToJarAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalEmfExecutionConfiguration.g:719:1: ( ( RULE_STRING ) )
            // InternalEmfExecutionConfiguration.g:720:2: ( RULE_STRING )
            {
            // InternalEmfExecutionConfiguration.g:720:2: ( RULE_STRING )
            // InternalEmfExecutionConfiguration.g:721:3: RULE_STRING
            {
             before(grammarAccess.getJarImportStatementAccess().getPathToJarSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getJarImportStatementAccess().getPathToJarSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JarImportStatement__PathToJarAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});

}