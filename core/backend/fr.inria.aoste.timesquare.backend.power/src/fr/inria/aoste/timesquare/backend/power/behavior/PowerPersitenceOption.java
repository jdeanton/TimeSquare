/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.behavior;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.ModelElementReference;

public class PowerPersitenceOption implements PersistentOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2299864251524550675L;

	@Override
	public String getDescription() {
	
		return "Power Persitence Option ";
	}

	public String startid, stopid, streo;

	transient public Couple c=null;
//	transient static AdapterRegistry adapter= AdapterRegistry.getDefault();
	public PowerPersitenceOption(ModelElementReference start,ModelElementReference stop, EObject eo) {
		super();
		this.startid = AdapterRegistry.getAdapter(start).getUID(start) ;
		this.stopid = AdapterRegistry.getAdapter(stop).getUID(stop) ;
		this.streo=AdapterRegistry.getAdapter(eo).getUID(eo);
	}
	
	
	public PowerPersitenceOption() {
		super();
		
	}


	public PowerPersitenceOption(Couple cbehavior) {
		this(cbehavior.getSource(),cbehavior.getDestination(),cbehavior.eo);
		c=cbehavior;
	}
	
	
	
	
}
