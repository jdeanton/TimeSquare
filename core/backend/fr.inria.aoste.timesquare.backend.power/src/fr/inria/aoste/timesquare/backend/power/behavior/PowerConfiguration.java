/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.behavior;

import java.util.List;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class PowerConfiguration extends BehaviorConfigurator<PowerBehaviorManager> {

	public PowerConfiguration(ConfigurationHelper _ch, PowerBehaviorManager _behaviorManager) {
		super(_ch, _behaviorManager);		
	}

// FM: REMOVED BECAUSE NEVER CALLED
	//	@Override
//	public void autoConfiguration() {
//		for (Couple c:getBehaviorManager().getLstCouples())
//		{
//			getBehaviorManager().addBehavior(c);
//		}
//	}

	public void addBehavior(Couple c) {
		getBehaviorManager().addBehavior(c);
	}
	
	public List<Couple> getLstCouples() {
		return getBehaviorManager().getLstCouples();
	}
	
	
}
