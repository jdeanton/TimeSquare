/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.behavior;

import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class PowerHelper {

	public PowerHelper() {
		super();
		
	}

	
	ConfigurationHelper helper=null;
	public void setConfigurationHelper(ConfigurationHelper _helper) {
		helper=_helper;		
	}
	
	
	public void print(String s) {
		if ( helper!=null)
		{
			helper.print(s);
		}
		
	}

	public void println(String s) {
		if ( helper!=null)
		{
			helper.println(s);
		}
		System.out.println("************************ "+s);
	}

	public void printlnError(String s) {
		if ( helper!=null)
		{
			helper.printlnError(s);
		}
		
	}
	
	public void printlnError(String s, Throwable e)
	{
		if ( helper!=null)
		{
			helper.printlnError(s,e);
		}
		
	
	}
	

	
}
