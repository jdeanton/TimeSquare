/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.view;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;

import fr.inria.aoste.timesquare.backend.power.view.PowerView.Dot;
import fr.inria.aoste.timesquare.backend.power.view.PowerView.IPowerViewFigure;

public class Ruler extends Polyline implements IPowerViewFigure {

	private Panel contents = null;

	public static final class LabelNumber extends Label implements IPowerViewFigure {
		public LabelNumber(int i) {
			super(Integer.toString(i));
		}
		
		
		@Override
		public void updateLocation(int size_y ) {
			y=size_y;
			super.setBounds(new Rectangle(rect.x,y-rect.y-rect.height,rect.width,rect.height));
			
		}
		int y=0;
		
		
		Rectangle rect=new Rectangle(0, 0, 0, 0);
		
		public void setBounds(Rectangle _rect) {
			rect=new Rectangle(_rect);
			updateLocation(y);		
		}
	}

	public enum HV {
		horizontal, vertical
	}

	public HV hv = null;
	private int lenght = 0;
	private final int x , y ;
	private final int scale, plot;

	public Ruler(Panel contents, HV hv, int bx, int by, int _scale, int _plot, int size_y) {
		super();
		this.setLineWidth(2);
		this.setLineStyle(SWT.LINE_SOLID);
		this.setForegroundColor(ColorConstants.black);
		this.setBackgroundColor(ColorConstants.yellow);
		this.setSize(1, 1);
		this.contents = contents;
		this.hv = hv;
		x = bx;
		y = by;
		scale = _scale;
		plot = _plot;
		updateLocation(size_y);
		updateLenght(100);
	}
	
	
	int sizey=0;
	@Override
	public void updateLocation(int y) {
		sizey=y;
		updateLenght(lenght);
	}



	protected int getLenght() {
		return lenght;
	}

	protected int updateLenght(int newlenght) {

		
			// newlenght+=200;
			PointList points = new PointList();
			points.addPoint(new Point(x, sizey-y));
			/*for(int i=0;i<numSegments;i++){
			    points.addPoint(labelPositions[i],(int)ySpan);
			}*/
			if (hv == HV.horizontal)
				points.addPoint(new Point(newlenght + x, sizey- y));
			if (hv == HV.vertical)
				points.addPoint(new Point(x,  sizey-y-newlenght));

			this.setPoints(points);

			if (newlenght > lenght)
			{
			for (int i = lenght; i < newlenght; i++) {
				if (i % scale == 0) {
					int tmp = i / scale;
					// if( i % )
					if (tmp % plot == 0) 
					{
						Label labels = new LabelNumber(tmp);
						labels.setForegroundColor(ColorConstants.black);

						int width = 30;
						int height = 15;
						int xPos = 0;
						int yPos = 0;
						if (hv == HV.horizontal) {
							xPos = x + i-width/2;
							yPos = y - height;
						}
						if (hv == HV.vertical) {
							xPos = x - width;
							yPos = y + i -height/2;
						}
						labels.setBounds(new Rectangle(xPos, yPos, width, height));
						contents.add(labels);
						Dot d= new Dot(ColorConstants.black,3);
						if (hv == HV.horizontal)
							d.setLocation(new Point(x+i, y));
						if (hv == HV.vertical)
							d.setLocation(new Point(x, y+i));
						contents.add(d);
						//contents.setConstraint(labels, new Rectangle(xPos, yPos, width, height));
					}
				}
			}
			if (hv == HV.horizontal)
				setSize(newlenght + 2 * x, 2 * y);
			if (hv == HV.vertical)
				setSize(2 * x, newlenght + 2 * y);
			repaint();
			contents.repaint();
			lenght = newlenght;
		}
		return lenght;
	}

}
