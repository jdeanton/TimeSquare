/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.view;

import java.util.ArrayList;

import org.eclipse.draw2d.IFigure; 
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.graphics.Color;

public class PowerData{
	
	Color color;
	String name;
	PowerView pv=null;
	ArrayList<Point> lst=new ArrayList<Point>();
	ArrayList<IFigure> lstf=new ArrayList<IFigure>();
	public PowerData(String namein, PowerView pvin, Color colorin) {		
		super();
		name = namein;
		pv=pvin;
		color=colorin;
		addPoint(0,0);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	IFigure old=null;
	public void addPoint(int x, int y)
	{
		IFigure newf=pv.createDot(x,y,color);
		lst.add(new Point(x,y));
		lstf.add(newf);
		if( old!=null)
			pv.createLine(newf,old,color );
		old=newf;
	}
	
	
}