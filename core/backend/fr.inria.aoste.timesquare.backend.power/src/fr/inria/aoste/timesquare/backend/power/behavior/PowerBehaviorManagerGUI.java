/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.behavior;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;

public class PowerBehaviorManagerGUI extends BehaviorManagerGUI {

	private Composite container;

	private Combo combo = null;
	private Text lblNewLabel = null;

	public PowerBehaviorManagerGUI() {
		super();

	}

	private final class ComboTypeSelectionAdapter extends SelectionAdapter {
		public void widgetSelected(SelectionEvent arg0) {
			try {
				int s = combo.getSelectionIndex();
				Couple e = getBehaviorManager().getLstCouples().get(s);
				lblNewLabel.setText(e.toInfoString());
			} catch (Exception e) {
				System.err.println(e);
			}

		}
	}

	/**
	 * 
	 * @wbp.parser.entryPoint
	 */	
	@Override
	public Control createDialogArea(Composite composite) {
		container =  new Composite(composite, SWT.FILL| SWT.RESIZE);
		composite.setLayout(new GridLayout());		
		container.setLayoutData(new GridData(GridData.FILL_VERTICAL | GridData.FILL_HORIZONTAL));
		container.setLayout(new FormLayout());

		combo = new Combo(container, SWT.NONE);
		getGUIHelper().registerID(combo, "selectCouple");
		FormData fd_combo = new FormData();
		fd_combo.left = new FormAttachment(0, 5);
		fd_combo.top = new FormAttachment(0, 5);
		fd_combo.right = new FormAttachment(100, -5);
		
		combo.setLayoutData(fd_combo);

		combo.setData(new Object());
		if (getBehaviorManager() != null) // for WindowsBuilder Editor
		{
			for (Couple e : getBehaviorManager().getLstCouples()) {
				combo.add(e.toString());
				combo.setData(e.toString(), e);
			}
			combo.select(0);
		}
		combo.pack();

		lblNewLabel = new Text(container, SWT.BORDER |    SWT.WRAP
		          | SWT.MULTI
		          | SWT.BORDER
		          | SWT.H_SCROLL
		          | SWT.V_SCROLL |SWT.READ_ONLY );
		FormData fd_lblNewLabel = new FormData();
		fd_lblNewLabel.left = new FormAttachment(combo, 0, SWT.LEFT);
		fd_lblNewLabel.bottom = new FormAttachment(100, -5);
		fd_lblNewLabel.top = new FormAttachment(combo, 2);
		fd_lblNewLabel.right = new FormAttachment(100, -5);
		lblNewLabel.setLayoutData(fd_lblNewLabel);
		if( getBehaviorManager()!=null)
		{
			Couple e = getBehaviorManager().getLstCouples().get(0);
			lblNewLabel.setText(e.toInfoString());
		}
		else
		{
			lblNewLabel.setText("test\nt1\nt2\nt3");
		}
		combo.addSelectionListener(new ComboTypeSelectionAdapter());
		combo.pack();
		lblNewLabel.pack();
		container.pack();
		// getBehaviorManager().g
		return container;
	}

	@Override
	public void okPressed() {
		Object o = combo.getData(combo.getText());
		if (o instanceof Couple) {
			getBehaviorManager().addBehavior((Couple) o);
		}
	}

	@Override
	public void cancelPressed() {

	}

	@Override
	protected PowerBehaviorManager getBehaviorManager() {
		// TODO Auto-generated method stub
		return (PowerBehaviorManager) super.getBehaviorManager();
	}
}
