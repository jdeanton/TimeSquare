/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.behavior;

import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.ValueSpecification;

import fr.inria.aoste.timesquare.backend.manager.utils.PairClockModelElementReference;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.ModelElementReference;

public class Couple extends PairClockModelElementReference{

	public Couple(ModelElementReference start, ModelElementReference stop, NamedElement eo) {
		super(start,stop);
		
		this.eo = eo;
		init();
	}


	final NamedElement eo;
	//private int countDependency = 0;
	private Dependency da = null;
	private Constraint c = null;
	private String str = "";
	public long value=-1;

	public void init() {
		//countDependency = eo.getClientDependencies().size();
		for (Dependency d : eo.getClientDependencies()) {
			Stereotype s = d.getAppliedStereotype("MARTE::MARTE_Foundations::Alloc::Allocate");
			if (s != null) {
				da = d;
				for (Element e : da.getRelatedElements()) {
					if (e instanceof Constraint) {
						Stereotype s2 = e.getAppliedStereotype("MARTE::MARTE_Foundations::NFPs::NfpConstraint");
						if (s2 != null) {
							c = (Constraint) e;
							try {
								ValueSpecification vs = c.getSpecification();
								if (vs instanceof OpaqueExpression) {
									OpaqueExpression oe = (OpaqueExpression) vs;
									String _str1 = oe.getLanguages().get(0);
									String _str2 = oe.getBodies().get(0);
									//System.out.println("------->" + _str1 + " " + _str2);
									if ("VSL".equals(_str1)) {
										str = _str2;
										String tb[] = str.split("==");
										for (int i = 0; i < tb.length; i++) {
											tb[i] = tb[i].trim();
											//System.out.println("@:" + tb[i]);
										}
										if( "cpu.power.value".equals(tb[0]))
										{
											value = Integer.parseInt(tb[1]);
										}
									}
								}
							} catch (Throwable t) {
								t.printStackTrace();
							}
						}
					}
				}
				// da.getC
				break;
			}
		}
	}

/*	public String toInfo1String() {
		String tmp = "Couple  (" + PowerBehaviorManager.adapter.getUID(start) + "\n >-> "
				+ PowerBehaviorManager.adapter.getUID(stop) + ")\n" + "on  { "
				+ PowerBehaviorManager.adapter.getReferenceName(eo) + "}\n";
		if (da != null) {
			tmp += "Allocation " + da.getName() + "\n";
			if (c != null) {
				tmp += "Constraint " + c.getName();
				tmp += "{" + str + "}\n";
			}
		}
		return tmp;
	}*/
	
	
	public String toInfoString() {
		String tmp = "Couple  (" + AdapterRegistry.getAdapter(getSource()).getUID(getSource()) + " >-> "
				+ AdapterRegistry.getAdapter(getDestination()).getUID(getDestination()) + ")\n" + "on  { "
				+ AdapterRegistry.getAdapter(eo).getReferenceName(eo) + "}\n";
		if (da != null) {
			tmp += "Allocation " + da.getName() + "\n";
			if (c != null) {
				tmp += "Constraint " + c.getName();
				tmp += "{" + str + "}\n";
			}
		}
		return tmp;
	}

	@Override
	public String toString() {

		return "Couple " + AdapterRegistry.getAdapter(getSource()).getUID(getSource()) + " >--> "
				+ AdapterRegistry.getAdapter(getDestination()).getUID(getDestination() ) + " :: "
				+ AdapterRegistry.getAdapter(eo).getUID(eo);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((eo == null) ? 0 : eo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Couple other = (Couple) obj;
		if (eo == null) {
			if (other.eo != null)
				return false;
		} else if (!eo.equals(other.eo))
			return false;
		return true;
	}
	
	

}