/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.behavior;

import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;
import fr.inria.aoste.timesquare.backend.power.view.PowerData;

public class ClockPowerBehavior implements ClockBehavior {

	
	int start=0;
	int stop=0;
	long value =0;
	PowerPersitenceOption options;
	public ClockPowerBehavior(PowerPersitenceOption _options) {
		options=_options ;
		value= options.c.value;
	}

	@Override
	public String getDescription() {
		try
		{
			return " ClockPowerBehavior " + options.c.toInfoString();
		}
		catch (Throwable  e) {
			return " ClockPowerBehavior ....." ;
		}
	}

	@Override
	public boolean behaviorEquals(Behavior behavior) {
		if ( behavior==this)
			return true;
		return false;
	}

	int integralpower=0;
	@Override
	public void run(TraceHelper helper) {
		//System.out.println("Running " +helper.getHRef());
		//helper.
		power.print("Ticking " + helper.getClockEntity().getName());
		

		if( helper.getClockEntity().getModelElementReference()== options.c.getSource())
		{
			power.print("  Starting :"+ start++);
			if( powerData!=null)
			{
				powerData.addPoint(s,integralpower);
			}
		}
		if( helper.getClockEntity().getModelElementReference()== options.c.getDestination())
		{
			power.print("  Stopping :"+ stop++);
			integralpower +=value ;
			if( powerData!=null)
			{
				powerData.addPoint(s,integralpower);
			}
		}
		power.println("");	
	}

	@Override
	public void runWithWrongActivationState(TraceHelper helper) {
		//System.out.println("WRunning " +helper.getHRef());

	}

	PowerHelper power=null;
	public void setPowerHelper(PowerHelper _power) {
		power=_power;
		
	}
	
	
	public void dispRapport()
	{
		power.println("***********************************");
		power.println("Couple :" + options.c.toInfoString());
		power.println("Starting :" + start +  "  Stopping " + stop);
		power.println("Full execution :"+stop);
		power.println("Partial execution :"+(start-stop));
	
		if( value!=-1)
		{
			power.print("Consommation : "+value +" * "+ stop+ " = " +(value* stop));
			power.println("");
		}
		power.println("");
	}

	PowerData powerData;
	public void setPowerData(PowerData createPowerData) {
		powerData=createPowerData;		
	}

	int s=0;
	public void step(int step) {
		s=step;
		
	}

}
