/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.behavior;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.UMLPackage;

import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.utils.EventEnumeratorPair;
import fr.inria.aoste.timesquare.backend.manager.utils.Filter;
import fr.inria.aoste.timesquare.backend.manager.utils.Filter.ICreatePairClockModelElementReference;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.DeleteHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.backend.power.view.IPowerView;
import fr.inria.aoste.timesquare.backend.power.view.PowerView;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.ModelElementReference;

public class PowerBehaviorManager extends BehaviorManager {
	private List<Couple> lstCouples = new ArrayList<Couple>();

	public List<Couple> getLstCouples() {
		return lstCouples;
	}

	private List<ClockPowerBehavior> behaviorList = new ArrayList<ClockPowerBehavior>();

	private PowerHelper power = new PowerHelper();

	private HashMap<PowerPersitenceOption, ClockPowerBehavior> mapoc = new HashMap<PowerPersitenceOption, ClockPowerBehavior>();
//	static AdapterRegistry adapter = AdapterRegistry.getDefault();

	@Override
	public String getPluginName() {
		return "Power Computation";
	}
	
	@Override
	public void clear() {
		behaviorList.clear();
	}

	@Override
	public void deleteEntity(Entity entity,DeleteHelper deletehelper) {
		if (entity!=null)
		{
			if ( behaviorList.remove(entity.getBehavior()))
			{
				//Couple cp=((ClockPowerBehavior) entity.getBehavior()).options.c;			
				for (ClockBehaviorEntity cbe: deletehelper.getClockBehaviorEntity())
				{
					if ( cbe.getBehavior()==entity.getBehavior())						
						deletehelper.deleteEntity(cbe);
				}				
			}
		}
	}
	
	public class  CoupleCreator implements ICreatePairClockModelElementReference<Couple>	
	{

		@Override
		public Couple create(ModelElementReference src, ModelElementReference dst) 
		{
			List<EObject> lSrc = AdapterRegistry.getAdapter(src).fillWithReferencedElements(src, null); 
			List<EObject> lDst = AdapterRegistry.getAdapter(dst).fillWithReferencedElements(dst, null);
			for (EObject eo : lSrc) {
				for (EObject eo2 : lDst) {
					{
						if (eo2 == eo && eo2 instanceof NamedElement) {
							Couple c = new Couple(src, dst, (NamedElement) eo);
							return c;
						//	System.out.println(c.toInfoString());
						}
					}
				}
			}
			return null;
		}
		
	}

	@Override
	public boolean isActivable(ReportMessage rm, CCSLInfo ccslhelper) {

		try {
			if (!ccslhelper.haveClockWithReferenceTo(UMLPackage.eINSTANCE.getElement())) {
				rm.setMessage("0 Clock have reference to UML ");
				return false;
			}
			ArrayList<ModelElementReference> lst =Filter.sublist((ccslhelper.getListofClock()),Filter.FILTER_REFERENCED_OBJECT);
			lstCouples = Filter.createPairClockModelElementReference(EventEnumeratorPair.StartFinish, lst, new CoupleCreator());
			if (lstCouples.isEmpty()) {
				rm.setMessage("0 couple start/stop find ");
				return false;
			}
		} catch (Throwable e) {
			e.printStackTrace();
			rm.setMessage("Throw :" + e.getMessage());
			return false;
		}
		return true;
	}


	
	//createPairClockModelElementReference
	
/*	public List<Couple> generateCoupleList(List<ModelElementReference> lstModelElementReferences) {
		// List<Couple>
		//System.out.println("Generate Couple List");
		lstCouples = new ArrayList<Couple>();
		List<ModelElementReference> startlst = Filter.sublist(lstModelElementReferences, Filter.FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_START);
				//getStartSublist(lstModelElementReferences);
		List<ModelElementReference> stoplst = Filter.sublist(lstModelElementReferences, Filter.FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_FINISH);
		//getStopSublist(lstModelElementReferences);
		for (ModelElementReference clock : startlst) {

			EList<EObject> l = (AdapterRegistry.getDefault().getReferencedObject(clock));

			for (EObject eo : l) {

				for (ModelElementReference clock2 : stoplst) {

					EList<EObject> l2 = (AdapterRegistry.getDefault().getReferencedObject(clock2));

					for (EObject eo2 : l2) {

						{
							if (eo2 == eo && eo2 instanceof NamedElement) {
								Couple c = new Couple(clock, clock2, (NamedElement) eo);
								lstCouples.add(c);
							//	System.out.println(c.toInfoString());
							}
						}
					}

				}

			}
		}
		//System.out.println("End Generate Couple List");
		return lstCouples;
	}*/

	@Override
	public void manageBehavior(ConfigurationHelper helper) {
		// generateCoupleList(helper);
		power.setConfigurationHelper(helper);
	}

	private void addbehavior(ClockPowerBehavior behavior) {
		behaviorList.add(behavior);
		behavior.setPowerHelper(power);
	}

	@Override
	public ClockPowerBehavior redoClockBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		if (persistentOptions instanceof PowerPersitenceOption) {
			PowerPersitenceOption po = (PowerPersitenceOption) persistentOptions;
			//System.out.println("Restore " + po.hashCode());
			ClockPowerBehavior behavior = mapoc.get(po);
			if (behavior == null) {
				Couple c = null;
				for (Couple cp : lstCouples) {
					if ((AdapterRegistry.getAdapter(cp.eo).getUID(cp.eo).equals(po.streo))
							&& (AdapterRegistry.getAdapter(cp.getSource()).getUID(cp.getSource()).equals(po.startid))
							&& (AdapterRegistry.getAdapter(cp.getDestination()).getUID(cp.getDestination()).equals(po.stopid))) {
						c = cp;
						break;
					}
				}
				if (c != null) {
					po.c = c;
					behavior = new ClockPowerBehavior(po);
					mapoc.put(po, behavior);
					addbehavior(behavior);
				}
			}
			return behavior;
		}
		return null;
	}
	

	public void addBehavior(Couple cbehavior) {
		if (_helper != null && cbehavior != null) {

			for ( ClockPowerBehavior b: behaviorList)
			{
				if (b.options.c.equals(cbehavior))
					return ;
			}
			PowerPersitenceOption options = new PowerPersitenceOption(cbehavior);
			ClockPowerBehavior behavior = new ClockPowerBehavior(options);
			addbehavior(behavior);

			ClockEntity cestart = _helper.getClock(cbehavior.getSource());
			ClockEntity cestop = _helper.getClock(cbehavior.getDestination());

			_helper.addBehavior(cestart, _helper.getTicksState(), this.getPluginName(), behavior, options);
			_helper.addBehavior(cestop, _helper.getTicksState(), this.getPluginName(), behavior, options);
		}
	}

	@Override
	public RelationBehavior redoRelationBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {

		return null;
	}

	@Override
	public ClockBehavior redoAssertBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {

		return null;
	}
	
	IPowerView powerView=null;
	@Override
	public void beforeExecution(ConfigurationHelper helper, IPath folderin, String namefilein, ISolverForBackend solver) {
		powerView=PowerView.getCourant();		
		power.setConfigurationHelper(helper);
		if( powerView!=null)
		{
			powerView.clear();
		
		for (ClockPowerBehavior cbp:behaviorList)
		{
			cbp.setPowerData(powerView.createPowerData(cbp.options.c.eo.getName()));
		}
	}
	}

	@Override
	public void end(ConfigurationHelper helper) {
		for (ClockPowerBehavior cpb : behaviorList) {
			cpb.dispRapport();
		}
		power=null;
		if (mapoc!=null)
			mapoc.clear();
		mapoc=null;
	}

	@Override
	public void aNewStep(int step) {	
		if( powerView!=null)
		{
			powerView.updatexRuler(step);
		}
		for (ClockPowerBehavior cpb : behaviorList) {
			cpb.step(step);  
		}
	}

	@Override
	public PowerConfiguration getConfigurator(ConfigurationHelper configurationHelper) {
		return new PowerConfiguration(configurationHelper, this);
	}
}
