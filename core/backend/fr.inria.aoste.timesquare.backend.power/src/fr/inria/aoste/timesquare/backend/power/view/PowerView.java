/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.power.view;

import java.util.ArrayList;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.EllipseAnchor;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.part.ViewPart;

import fr.inria.aoste.timesquare.backend.power.view.Ruler.HV;
import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;

public class PowerView extends ViewPart implements IPowerView {

	private static final String POWER_VIEW = "Power View";
	public static final String FR_INRIA_AOSTE_TIMESQUARE_BACKEND_POWER_POWERVIEW = "fr.inria.aoste.timesquare.backend.power.powerview";

	private final class CreateDotRunnable implements Runnable {
		private final Color color;
		private IFigure tmp;
		private final int x;
		private final int y;

		private CreateDotRunnable(int x, int y, Color color) {
			this.x = x;
			this.y = y;
			this.color = color;
		}

		protected IFigure getTmp() {
			return tmp;
		}

		@Override
		public void run() {
			tmp = createDot(contents, offsetx + x * stepscale, offsety + y, color);

		}

	}
	
	public interface IPowerViewFigure 
	{
		public void updateLocation(int y);
	}

	public static final class RectangleText extends RectangleFigure implements IPowerViewFigure {
		
		
		
		public RectangleText() {
			super();
			
		}
		@Override
		public void updateLocation(int size_y ) {
			y=size_y;
			super.setBounds(new Rectangle(rect.x,rect.y,rect.width,rect.height)); // TODO
			
		}
		int y=0;
		
		
		Rectangle rect=new Rectangle(0, 0, 0, 0);
		
		public void setBounds(Rectangle _rect) {
			rect=new Rectangle(_rect);
			updateLocation(y);		
		}
	}

	public static class Dot extends Ellipse implements IPowerViewFigure {

		/*public Dot() {
			this(ColorConstants.yellow,5);
			
		}*/

		public Dot(Color c) {
			this(c,2);
		}
		int size;
		public Dot(Color c, int rayon) {
			size=rayon;
			Dimension dim = new Dimension(size*2, size*2);
			this.setSize(dim);
			this.setBackgroundColor(c);
			this.setOpaque(true);
		}

		@Override
		public void updateLocation(int size_y ) {
			y=size_y;
			super.setLocation(new Point(location.x-size,size_y-location.y-size));
			
		}
		int y=0;
		Point location=new Point(0,0); 
		public void setLocation(Point p) {
			location =p;
			updateLocation(y);
		}
		

	}
	
	

	static private PowerView courant;
	static final public int offsetx = 160;
	static final public int offsety = 20;
	static final public int stepscale = 10;
	static final public int steppower = 1;

	public int sizex = 400;
	public int sizey = 300;

	public synchronized static IPowerView getCourant() {
		if (courant == null) {
			try {
				PluginHelpers.getShowView(FR_INRIA_AOSTE_TIMESQUARE_BACKEND_POWER_POWERVIEW);

				if (courant == null) {
					PluginHelpers.getCreateView(FR_INRIA_AOSTE_TIMESQUARE_BACKEND_POWER_POWERVIEW);
				}
				PluginHelpers.getShowView(FR_INRIA_AOSTE_TIMESQUARE_BACKEND_POWER_POWERVIEW);
				if (courant != null) {
					courant.setFocus();
				}

			} catch (Throwable ex) {
				ErrorConsole.printError(ex, POWER_VIEW);
			}
		}
		return courant;
	}

	static synchronized public void refreshCourant() {
		if (courant != null) {
			courant.refresh();
		}
	}

	private synchronized static final void setCourant(PowerView courant) {
		PowerView.courant = courant;
	}

	private FigureCanvas canvas = null;
	private Composite composite = null;
	private Panel contents = null;

	private int cptcolor = 0;

	public ArrayList<PowerData> listPowerDatas = new ArrayList<PowerData>();

	private Ruler xRuler = null;

	private Ruler yRuler = null;

	public PowerView() {
		setCourant(this);
	}

	@Override
	public void createPartControl(Composite parent) {
		composite = new Composite(parent, SWT.BORDER);
		composite.setLayout(new FillLayout());
		canvas = new FigureCanvas(composite,SWT.DOUBLE_BUFFERED);
		
		contents = new Panel();		
		canvas.setBackground(ColorConstants.white);
		canvas.setVerticalScrollBarVisibility(FigureCanvas.ALWAYS);
		canvas.setHorizontalScrollBarVisibility(FigureCanvas.ALWAYS);
		canvas.setContents(contents);			
	
		//clear();
	}

	@Override
	public void dispose() {

		super.dispose();
		setCourant(null);
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.backend.power.view.IPowerView#clear()
	 */
	@Override
	public void clear() {
		cptcolor = 0;
		listPowerDatas.clear();
		llabel.clear();
		contents.removeAll();
		xRuler = new Ruler(contents, HV.horizontal, offsetx, offsety, stepscale, 5,offsety+120);
		yRuler = new Ruler(contents, HV.vertical, offsetx, offsety, steppower, 20,offsety+120);

		contents.add(xRuler);
		contents.add(yRuler);
		

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				contentRepaint();
			}
		});

	}
	
	protected void contentRepaint()
	{
		//IPowerViewFigure
		for( Object f: contents.getChildren())
		{
			if( f instanceof IPowerViewFigure)
			{
				((IPowerViewFigure) f).updateLocation(yRuler.getLenght()+offsety);
			}
		}
		contents.repaint();
	}
	

	protected IFigure createDot(Panel contents, int x, int y, Color color) {
		Dot dotFigure = new Dot(color,2);
		contents.add(dotFigure);
		dotFigure.setLocation(new Point(x, y));
		contents.setConstraint(dotFigure, new Rectangle(x, y, -1, -1));
		
		xRuler.updateLenght(x);
		yRuler.updateLenght(y);
		Dimension d= contents.getMinimumSize();
		contents.setMinimumSize(new Dimension(Math.min(d.width,x) ,Math.min (d.height, y)));
		
		contentRepaint();
		canvas.layout();
		canvas.update();
		canvas.redraw();
		return dotFigure;
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.backend.power.view.IPowerView#createDot(int, int, org.eclipse.swt.graphics.Color)
	 */
	@Override
	public IFigure createDot(final int x, final int y, Color color) {
		CreateDotRunnable cdr = new CreateDotRunnable(x, y, color);
		Display.getDefault().syncExec(cdr);
		return cdr.getTmp();
	}

	public void createLine(final IFigure newf, final IFigure old, final Color color) {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {

				PolylineConnection wireFigure = new PolylineConnection();
				wireFigure.setForegroundColor(color);
				EllipseAnchor sourceAnchor = new EllipseAnchor(old);
				EllipseAnchor targetAnchor = new EllipseAnchor(newf);
				wireFigure.setSourceAnchor(sourceAnchor);
				wireFigure.setTargetAnchor(targetAnchor);
				contents.add(wireFigure);

			}
		});

	}

	public ArrayList<Label> llabel = new ArrayList<Label>();

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.backend.power.view.IPowerView#createPowerData(java.lang.String)
	 */
	@Override
	public PowerData createPowerData(String s) {
		Color c = getNewColour();
		PowerData powerData = new PowerData(s, this, c);
		listPowerDatas.add(powerData);
		int height = 30 + llabel.size() * 25;
		Label l = new Label(s);
		RectangleFigure rectangle = new RectangleText();
		int size = 100;
		rectangle.setOpaque(false);
		rectangle.setLineStyle(Graphics.LINE_DOT);
		rectangle.setForegroundColor(c);
		rectangle.setBounds(new Rectangle(1, height, size, 20));
		l.setBounds(new Rectangle(4, height + 1, size - 4, 20 - 2));
		l.setForegroundColor(ColorConstants.black);
		l.setOpaque(true);
		Font font = new Font(null, "Arial", 10, 1);
		l.setFont(font);
		// l.getFont().
		// l.setLocation(new Point(2,2));
		l.setForegroundColor(c);
		rectangle.add(l);
		llabel.add(l);
		contents.add(rectangle);
		return powerData;
	}

	public Color getNewColour() {
		if (cptcolor == 8)
			cptcolor = 0;
		else
			cptcolor++;

		switch (cptcolor) {
		case 0:
			return ColorConstants.red;
		case 1:
			return ColorConstants.blue;
		case 2:
			return ColorConstants.green;
		case 3:
			return ColorConstants.lightBlue;
		case 4:
			return ColorConstants.lightGreen;
		case 5:
			return ColorConstants.lightGray;
		case 6:
			return ColorConstants.darkBlue;
		case 7:
		default:
			return ColorConstants.darkGreen;
		}// end of switch
	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.backend.power.view.IPowerView#refresh()
	 */
	@Override
	public int refresh() {
		return 0;
	}

	@Override
	public void setFocus() {

	}

	/* (non-Javadoc)
	 * @see fr.inria.aoste.timesquare.backend.power.view.IPowerView#updatexRuler(int)
	 */
	@Override
	public void updatexRuler(final int x) {

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				if (xRuler != null) {
					xRuler.updateLenght(x * stepscale);
				}
				contents.repaint();
				canvas.update();
			}
		});

	}

}
