/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.papyrusmodelanimator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.editparts.AbstractEditPart;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.papyrus.infra.core.sasheditor.editor.ISashWindowsContainer;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.UMLPackage;

import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertActivationState;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.DeleteHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;

public class PapyrusAnimatorBehaviorManager extends BehaviorManager {

	private String _di2FilePath = null;
	private IFile _di2IFile = null;
	private DiagramEditPart _diagramEditPart = null;

	

	final private List<AnimateTicksBehaviour> behaviorList = new ArrayList<AnimateTicksBehaviour>();
	final private List<AnimateAssertBehaviour> assertBehaviorList = new ArrayList<AnimateAssertBehaviour>();

	private CCSLInfo ccslhelper = null;

	@Override
	public boolean isActivable(ReportMessage rm, CCSLInfo _ccslhelper) {
		ccslhelper = _ccslhelper;
		if (ccslhelper.haveClockWithReferenceTo(UMLPackage.eINSTANCE.getElement())) {
			cachevalidation.clear();
			return true;
		}
		rm.setMessage("No reference to a UML model");
		return false;
	}

	/***
	 * Gestion d'erreur ==> pas de fichier di definie
	 * @return Error Message if no valid .di file, null otherwise
	 */
	@Override
	public String validate() {
		if (_di2IFile == null) {
			return "No *.di File  ";
		}
		if (!_di2IFile.exists()) {
			return "File not found :"+_di2IFile.getName();
		}
		if ( !validDiFile(_di2IFile))
		{
			return "File DI2 don't have link with specification";
		}
		return super.validate();
	}

	public String getDiFileString() {
		return _di2FilePath;
	}

	public IFile getDiIFile() {
		return _di2IFile;
	}

	public void setDi(String s) {
		_di2FilePath = s;
		IWorkspaceRoot iWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = iWorkspaceRoot.findMember(_di2FilePath);
		if (resource instanceof IFile)
			_di2IFile = (IFile) resource;
		else
			_di2IFile = null;
	}

	public void setDi(IFile f) {
		_di2IFile = f;
		if (f == null) {
			_di2FilePath = null;
			return;
		}
		_di2FilePath = f.getFullPath().toString();
	}
	
	private HashMap<String, Boolean> cachevalidation=new HashMap<String, Boolean>();

	public boolean validDiFile(String file) {
		file = file.replace(".di", ".uml");
		try {
			if( cachevalidation.containsKey(file))
					return cachevalidation.get(file).booleanValue();
			ResourceSet rs = new ResourceSetImpl();
			URI uri = URI.createPlatformResourceURI(file, true);
			Resource r = rs.createResource(uri);
			r.load(Collections.EMPTY_MAP);
			try {
				EcoreUtil.resolveAll(rs);
			} catch (Exception e) {

			}
			for (Resource resource : rs.getResources()) {
				URI _uri = resource.getURI();
				if (ccslhelper.getMappingURIReferencedObject().keySet().contains(_uri))
				{
					cachevalidation.put(file, true);
					return true;
				}
			}
			cachevalidation.put(file, false);
			return false;
		} catch (Exception e) {
			cachevalidation.put(file, false);
			return false;
		}
	}

	public boolean validDiFile(IFile file) {
		if (file != null) {
			return validDiFile(file.getFullPath().toString());
		}
		return true;
	}

	public void end(ConfigurationHelper helper) {
		try {
			Display.getDefault().syncExec(new RunnableEnd());						
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEntity(Entity entity, DeleteHelper deletehelper) {
		behaviorList.remove(entity.getBehavior());
	}

	@Override
	public void clear() {
		behaviorList.clear();
		_di2FilePath = null;
		_di2IFile = null;
	}

	public String _getid() {
		return "papyrusanimator";
	}

	public String getPluginName() {
		return "Papyrus Diagram Animator";
	}

	public PersistentOptions getPluginOptions() {
		if (_di2FilePath != null)
			return new PapyrusAnimatorPluginPersistentOptions(_di2FilePath);
		return null;

	}

	private List<GraphicalEditPart> editPartGetAllChildren(AbstractEditPart dep, List<GraphicalEditPart> res) {
		@SuppressWarnings("unchecked")
		List<GraphicalEditPart> tmp = dep.getChildren();
		res.addAll(tmp);
		for (GraphicalEditPart ep : tmp) {
			editPartGetAllChildren(ep, res);
		}
		return res;
	}

	
	public void beforeExecution(ConfigurationHelper helper, IPath folderin, String namefilein, ISolverForBackend solver) {
		try {
			Display.getDefault().syncExec(new RunnableStart());						
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}	
	
	private class RunnableEnd implements Runnable {

		@Override
		public void run() {
			for (AnimateTicksBehaviour behavior : behaviorList) {
				behavior.finish();
			}
//			for (AnimateAssertBehaviour assertBehavior : assertBehaviorList) {
//				assertBehavior.finish();
//			}
		}
		
	}
	private final class RunnableStart implements Runnable {
		public void run() {			
			IWorkspaceRoot iWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			_di2IFile = (IFile) iWorkspaceRoot.findMember(_di2FilePath);
			if (_di2IFile == null)
				return;
			PapyrusModelBridge thePapyrusBridge = null;
			try {
				thePapyrusBridge = new PapyrusModelBridge(_di2IFile);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			org.eclipse.papyrus.uml.diagram.statemachine.UmlStateMachineDiagramForMultiEditor a = null;
			_diagramEditPart = ((DiagramEditor) thePapyrusBridge.papyrusEditor.getActiveEditor()).getDiagramEditPart();
			_diagramEditPart.setFocus(true);	
			
			while( !(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()instanceof IMultiDiagramEditor)){	
			}
			IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
			
			ISashWindowsContainer container = (ISashWindowsContainer )editorPart.getAdapter(ISashWindowsContainer.class);
			List<IEditorPart> parts = container.getVisibleIEditorParts();

// get graphical elements from all visible editors
			List<GraphicalEditPart> gmfElems = new ArrayList<GraphicalEditPart>();
			for(IEditorPart part: parts) {				
				if (part instanceof GraphicalEditor) {
					if (part instanceof DiagramEditor) {
						DiagramEditPart editPart = ((DiagramEditor) part).getDiagramEditPart();
						editPartGetAllChildren(editPart, gmfElems);
					}
				}
			}
// used to get only from the single one selected
//			List<GraphicalEditPart> gmfElems=editPartGetAllChildren(_diagramEditPart);				
			for (AnimateTicksBehaviour behavior : behaviorList) {
				behavior.setEditPart(_diagramEditPart,gmfElems);	 	
				behavior.start();
			}
			for (AnimateAssertBehaviour assertBehavior : assertBehaviorList) {
				assertBehavior.setEditPart(_diagramEditPart);	 	
				assertBehavior.start();
			}
		}
	}

	

	public void manageBehavior(ConfigurationHelper helper) {
		try {
			loop1: for (ClockEntity ce : helper.getClocks()) {
				//TODO: fix this ugly comparison !!!
				if (ce.getModelElementReference().getElementRef().size() != 3)
					continue loop1;
				for (AnimateTicksBehaviour b : behaviorList) {
					if (b._ce == ce)
						continue loop1;
				}
				AnimateTicksBehaviour atb = new AnimateTicksBehaviour(ce);
				behaviorList.add(atb);
				helper.addBehavior(ce, helper.getTicksState(), getPluginName(), atb,
						new PapyrusAnimatorPersistentOptions(ce.getID()));
			}
		
			for(ClockEntity ae: helper.getAssert()){
				AnimateAssertBehaviour aab = new AnimateAssertBehaviour(ae);
				assertBehaviorList.add(aab);
				boolean[] assertState={false,true};
				helper.addBehavior(ae, new AssertActivationState(assertState), getPluginName(), aab,
						new PapyrusAnimatorPersistentOptions(ae.getID()));
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public BehaviorConfigurator<?> getConfigurator(ConfigurationHelper configurationHelper) {
	
		return new PapyrusAnimatorBehaviorConfigurator(configurationHelper, this);
	}

	public void receivePluginOptions(ConfigurationHelper helper, PersistentOptions persistentOptions) throws Throwable {
		if (persistentOptions instanceof PapyrusAnimatorPluginPersistentOptions) {
			setDi(((PapyrusAnimatorPluginPersistentOptions) persistentOptions).get_di2FilePath());
		}
		return;
	}

	public AnimateTicksBehaviour redoClockBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		AnimateTicksBehaviour cl = null;
		if (persistentOptions instanceof PapyrusAnimatorPersistentOptions) {
			ClockEntity ce = helper.getClock(((PapyrusAnimatorPersistentOptions) persistentOptions).getID());
			cl = new AnimateTicksBehaviour(ce);
			behaviorList.add(cl);
		}
		return cl;

	}

	public ClockBehavior redoAssertBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		AnimateAssertBehaviour aab = null;
		if (persistentOptions instanceof PapyrusAnimatorPersistentOptions) {
			ClockEntity ae = null;
			for(ClockEntity anAe : helper.getAssert()){
				if (anAe.getID().compareTo(((PapyrusAnimatorPersistentOptions) persistentOptions).getID())==0){
					ae=anAe;
				}
			}
			aab = new AnimateAssertBehaviour(ae);
			assertBehaviorList.add(aab);
		}
		return aab;
	}

	public RelationBehavior redoRelationBehavior(ConfigurationHelper helper, PersistentOptions persistentOptions) {
		return null;

	}
	
	
	protected  DiagramEditPart getDiagramEditPart() {
		return _diagramEditPart;
	}

	/**
	 * for test only
	 * @return 
	 */
	protected int[] test_SizeofGetTableColor() {
		int ti[]= new int[behaviorList.size()];
		for( int i=0;i<behaviorList.size();i++)
		{
			ti[i]=behaviorList.get(i).getSizeofTableColor();
		}
		return ti;
	}
	/**
	 * 
	 * @return
	 */
	protected boolean [] test_ElementUMLFound()
	{
		boolean tb[]= new boolean[behaviorList.size()];
		for( int i=0;i<behaviorList.size();i++)
		{
			tb[i]=(behaviorList.get(i).getNamedElement()!=null);
		}
		return tb;
	}

	
	

	
}
