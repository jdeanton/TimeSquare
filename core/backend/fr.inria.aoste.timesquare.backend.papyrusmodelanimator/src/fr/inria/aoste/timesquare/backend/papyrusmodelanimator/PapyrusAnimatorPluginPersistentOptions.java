/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.papyrusmodelanimator;

import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

public class PapyrusAnimatorPluginPersistentOptions implements PersistentOptions
{
	private static final long serialVersionUID = -9002690199800237614L;
	private String _di2FilePath = null;

	public PapyrusAnimatorPluginPersistentOptions(String s)
    {
    	this._di2FilePath = s;
    }
    
    public String getDescription()
    {
        return "the only relevant action is the name of the di2 file and a way to retrieve the clock";
    }

	public String get_di2FilePath() {
		return _di2FilePath;
	}
}
