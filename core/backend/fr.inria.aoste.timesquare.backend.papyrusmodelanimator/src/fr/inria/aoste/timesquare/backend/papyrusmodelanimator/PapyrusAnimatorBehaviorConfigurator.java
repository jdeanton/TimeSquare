/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.papyrusmodelanimator;

import org.eclipse.core.resources.IFile;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class PapyrusAnimatorBehaviorConfigurator extends BehaviorConfigurator<PapyrusAnimatorBehaviorManager> {

	public PapyrusAnimatorBehaviorConfigurator(ConfigurationHelper _ch, PapyrusAnimatorBehaviorManager _behaviorManager) {
		super(_ch, _behaviorManager);
	}

	/***
	 * set a <i>di</i> file for Papyrus Animator
	 * @param file (String )
	 * @throws Exception
	 */
	public void setDi(String file) throws Exception {
		if (file != null) {
			if (!getBehaviorManager().validDiFile(file)) {
				throw new IllegalArgumentException("File is bad (...)");
			}
			getBehaviorManager().setDi(file);
			getBehaviorManager().manageBehavior(getConfigurationHelper());
			return;
		}
		throw new NullPointerException("Argument is null");
	}

	/**
	 * set a <i>di</i> file for Papyrus Animator
	 * @param file (Ifile)
	 * @throws Exception
	 */
	public void setDi(IFile file) throws Exception {
		if (file != null) {
			if (!file.exists()) {
				throw new IllegalArgumentException("File "+file.getName()+" does not exist!");
			}
			if (!getBehaviorManager().validDiFile(file)) {
				throw new IllegalArgumentException("File "+file.getName()+" is not valid.");
			}
			getBehaviorManager().setDi(file);
			getBehaviorManager().manageBehavior(getConfigurationHelper());
			return;
		}
		throw new NullPointerException("Argument is null");
	}
	
	/**
	 * for test only
	 * @return 
	 */
	public int [] test_SizeofGetTableColor()
	{
		return getBehaviorManager().test_SizeofGetTableColor();
	}
	public boolean [] test_ElementUMLFound()
	{
		return getBehaviorManager().test_ElementUMLFound();
	}
}
