/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.papyrusmodelanimator;

import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

public class PapyrusAnimatorPersistentOptions implements PersistentOptions
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -6900990210439069137L;



	public PapyrusAnimatorPersistentOptions() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String _ID = null;
   


    public PapyrusAnimatorPersistentOptions(String ID)
    {
    	_ID = ID;
    }
    
    
    public String getDescription()
    {
        return "the only relevant action is the name of the di2 file";
    }

	public String getID() {
		return _ID;
	}

    

}
