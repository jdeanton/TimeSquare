/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.papyrusmodelanimator;

import java.util.HashMap;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.workspace.AbstractEMFOperation;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.ShapeStyle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.UMLFactory;

import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehaviorAgain;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter.EventEnumerator;

public class AnimateTicksBehaviour implements ClockBehaviorAgain {

	
	private final class StartSetCommentOperation extends AbstractEMFOperation {
	
		public StartSetCommentOperation(GraphicalEditPart theEditPart) {
			super(theEditPart.getEditingDomain(), "SetComment");
		}
	
		@Override
		protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
			// TODO: fix that... the comment is created but not in the model
			// displayed by papyrus...
			if (_comment == null)
				_comment = (_elem).createOwnedComment();
			_comment.setBody("CCSL simulation results:\n   number of activation: " + _clockCounter + "\n   schedule: "
					+ _schedule);
			return Status.OK_STATUS;
		}
	}

	private static final class SetColorRun extends AbstractEMFOperation {
	
		private AnimateTicksBehaviour atb = null;
	
		public SetColorRun(AnimateTicksBehaviour atb) {
			super(atb.theEditPart.getEditingDomain(), "SetColor");
			this.atb = atb;
		}
	
		@Override
		protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
	
			if (atb._elemShape != null) {
	
				{
					//EventEnumerator ek =atb.ek; // AdapterRegistry.getDefault().getEventkind(atb._ce.getClock());
	
					switch (atb.ek) {
					case START:
						atb._elemShape.setFillColor(255);
						break;
	
					case SEND:
						atb._elemShape.setFillColor(127000000);
						break;
	
					case PRODUCE:
						atb._elemShape.setFillColor(127000000);
						break;
	
					case FINISH:
						atb._elemShape.setFillColor(atb._originalColor);
						break;
	
					case CONSUME:
						atb._elemShape.setFillColor(067000);
						break;
	
					case RECEIVE:
						atb._elemShape.setFillColor(067000);
						break;
					case UNDEFINED:
						atb._elemShape.setFillColor(255);
	
					}
					int newcolor = atb._elemShape.getFillColor();			
					atb.tablecolor.put( Integer.valueOf(atb._helper.getCurrentStepNumber()), Integer.valueOf(newcolor));
				}
			}
			//
			atb._comment.setBody("CCSL simulation results:\n   number of activation: " + atb._clockCounter
					+ "\n   schedule: " + atb._schedule);
	
			return Status.OK_STATUS;
		}
	}

	private static final class RestoreDefault extends AbstractEMFOperation implements Runnable {
		private AnimateTicksBehaviour atb = null;
	
		public RestoreDefault(AnimateTicksBehaviour atb) {
			super(atb.theEditPart.getEditingDomain(), "RestoreDefault");
			this.atb = atb;
		}
	
		@Override
		protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
			if (atb._elemShape != null) {
				atb._elemShape.setFillColor(atb._originalColor);
			}	
			return Status.OK_STATUS;
		}

		@Override
		public void run() {
			try {
				atb._diagramEditPart.activate();
				try {
					this.execute(null, null);
				} catch (ExecutionException e) {					
					e.printStackTrace();
				}	
				atb._diagramEditPart.refresh();
			} catch (Throwable e) {
				System.err.println(e);
			}
		}
	}
	
	private static final class SetColorRunWrongActivationState extends AbstractEMFOperation {
	
		private final AnimateTicksBehaviour atb;
	
		public SetColorRunWrongActivationState(AnimateTicksBehaviour atb) {
			super(atb.theEditPart.getEditingDomain(), "SetColor");
			this.atb = atb;
		}
	
		@Override
		protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
			if (atb._helper.getClockState().isIsClockDead()) {
				atb._elemShape.setFillColor(12632256);
			} else {
				{
					//EventEnumerator ek = atb.ek; //AdapterRegistry.getDefault().getEventkind(atb._ce.getClock());
					switch (atb.ek) {
	
					case SEND:
						atb._elemShape.setFillColor(atb._originalColor);
						break;
	
					case PRODUCE:
						atb._elemShape.setFillColor(atb._originalColor);
						break;
	
					case CONSUME:
						atb._elemShape.setFillColor(atb._originalColor);
						break;
	
					case RECEIVE:
						atb._elemShape.setFillColor(atb._originalColor);
						break;
	
					case FINISH:
						break;
					case START:
						break;
					case UNDEFINED:
						atb._elemShape.setFillColor(atb._originalColor);
						break;
	
					}
					int newcolor = atb._elemShape.getFillColor();			
					atb.tablecolor.put( Integer.valueOf(atb._helper.getCurrentStepNumber()), Integer.valueOf(newcolor));
	
				}
			}
			atb._comment.setBody("CCSL simulation results:\n   number of activation: " + atb._clockCounter	+ "\n   schedule: " + atb._schedule);
			return Status.OK_STATUS;
		}
	}

	private static final class SetColorAgain extends AbstractEMFOperation {
	
		private final AnimateTicksBehaviour atb;
		private final int color;
	
		public SetColorAgain(AnimateTicksBehaviour _atb, int _color) {
			super(_atb.theEditPart.getEditingDomain(), "SetColor");
			this.atb = _atb;
			Integer n= Integer.valueOf(atb._helper.getCurrentStepNumber());
			if ( atb.tablecolor.containsKey(n))
				this.color= atb.tablecolor.get(n).intValue();
			else				
				this.color = _color;
	
		}
	
		@Override
		protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
	
			atb._elemShape.setFillColor(color);
	
			return Status.OK_STATUS;
		}
	}

	private static final class RunnableRun implements Runnable {
	
		private final AnimateTicksBehaviour atb;
	
		public RunnableRun(AnimateTicksBehaviour _atb) {
			super();
			this.atb = _atb;
		}
	
		public void run() {
			try {
				atb._diagramEditPart.activate();
				AbstractEMFOperation cmd = new SetColorRun(atb);
	
			//	atb._clockCounter++;
				atb._schedule += '1';
	
				try {
					cmd.execute(new NullProgressMonitor(), null);
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
	
				atb._diagramEditPart.refresh();
			} catch (Throwable e) {
				System.err.println(e);
			}
		}
	
	}

	private static final class RunnnableWithWrongActivationState implements Runnable {
		private final AnimateTicksBehaviour atb;
	
		public RunnnableWithWrongActivationState(AnimateTicksBehaviour _atb) {
			super();
			this.atb = _atb;
		}
	
		public void run() {
			try {
				atb._diagramEditPart.activate();
				AbstractEMFOperation cmd = new SetColorRunWrongActivationState(atb);
	
				atb._schedule += '0';
	
				try {
					cmd.execute(null, null);
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
	
				atb._diagramEditPart.refresh();
			} catch (Throwable e) {
				System.err.println(e);
			}
		}
	}
	
	/**
	 * This method is called in debug mode when a step has already been computed
	 * but is visited again
	 */
	private static final class AgainRunnable implements Runnable {
	
		private final AnimateTicksBehaviour atb;
	
		private int color;
		
	
		public AgainRunnable(AnimateTicksBehaviour atb, int color) {
			super();
			this.atb = atb;
			this.color = color;
		}

		public void run() {
			try {
				atb._diagramEditPart.activate();
				AbstractEMFOperation cmd = new SetColorAgain(atb, color);
				try {
					cmd.execute(null, null);
				} catch (ExecutionException e) {					
					e.printStackTrace();
				}	
				atb._diagramEditPart.refresh();
			} catch (Throwable e) {
				System.err.println(e);
			}
		}
	}

	
	
	private int _clockCounter = 0;
	private int _originalColor;
	private GraphicalEditPart theEditPart = null;
	private ShapeStyle _elemShape = null;
	private DiagramEditPart _diagramEditPart = null;
	private Comment _comment = null;
	private String _schedule = "";
	private TraceHelper _helper = null;
	private NamedElement _elem = null;
	private List<GraphicalEditPart> gmfElems = null;
	private HashMap<Integer, Integer> tablecolor=new HashMap<Integer, Integer>();
	final ClockEntity _ce;
	private EventEnumerator ek = null;
	
	public AnimateTicksBehaviour(ClockEntity ce) {
		super();
		_ce = ce;		
		ek=AdapterRegistry.getAdapter(_ce.getClock()).getEventkind(_ce.getClock());
	}

	public String getDescription() {	
		return "Papyrus Animator";
	}


	public void setEditPart(DiagramEditPart _diagramEditPart, List<GraphicalEditPart> gmfElems) {
		this._diagramEditPart = _diagramEditPart;
		this.gmfElems = gmfElems;
	}

	public void start() {
		if (_diagramEditPart == null)
			return;
		if ((_ce.getReferencedElement().size() > 0)
				&& (_ce.getReferencedElement().get(0) instanceof org.eclipse.uml2.uml.NamedElement)) {
			// if (_diagramEditPart != null) {
			// gmfElems = EditPartGetAllChildren(_diagramEditPart);
			for (GraphicalEditPart o : gmfElems)
				if (o.resolveSemanticElement() instanceof NamedElement) {
					if (((NamedElement) _ce.getReferencedElement().get(0)).getQualifiedName().compareTo(
							((NamedElement) o.resolveSemanticElement()).getQualifiedName()) == 0) {
						theEditPart = o;
						AbstractEMFOperation cmd = new StartSetCommentOperation(theEditPart);
						_elem = ((NamedElement) o.resolveSemanticElement());					
						try {
							_elemShape = ((org.eclipse.gmf.runtime.notation.ShapeStyle)  theEditPart.getNotationView());
							_originalColor = _elemShape.getFillColor();
						} catch (Exception e) {
							// _elemDecoration
							// =((DecorationNode)((GraphicalEditPart)
							// theEditPart).getNotationView());
							// _originalColor =
							// _elemDecoration.getgetFillColor();
						}
						try {
							cmd.execute(null, null);
						} catch (ExecutionException e) {
							e.printStackTrace();
							_comment = UMLFactory.eINSTANCE.createComment();
						}
						if (_comment == null) {
							_comment = UMLFactory.eINSTANCE.createComment();
						}
					}
				}
			// }
		}

	}

	public void run(TraceHelper helper) {
		if (theEditPart != null && _diagramEditPart != null) {
			_helper = helper;
			_clockCounter = helper.getClockState().getCounter();
			Display.getDefault().syncExec(new RunnableRun(this));
			_helper = null;
		}
	}

	void finish() {
		if (theEditPart != null && _diagramEditPart != null) {
			Display.getDefault().syncExec(new RestoreDefault(this));
		}
	}
	public void runWithWrongActivationState(TraceHelper helper) {
		if (theEditPart != null && _diagramEditPart != null) {
			_helper = helper;
			Display.getDefault().syncExec(new RunnnableWithWrongActivationState(this));
			_helper = null;
		}
	}

	
	/**
	 * This method is called in debug mode when a step has already been computed
	 * but is visited again
	 */
	public void againRun(TraceHelper helper) {
		if (theEditPart != null && _diagramEditPart != null) {		
			_helper = helper;
			_clockCounter = helper.getClockState().getCounter();
			Display.getDefault().syncExec(new AgainRunnable(this,255));
			_helper = null;
		}
	}

	/**
	 * This method is called in debug mode when a step has already been computed
	 * but is visited again
	 */
	public void againRunWithWrongActivationState(TraceHelper helper) {
		if (theEditPart != null && _diagramEditPart != null) {
			_helper = helper;
			Display.getDefault().syncExec(new AgainRunnable(this,_originalColor));
			_helper = null;
		}

	}
	
	public boolean behaviorEquals(Behavior behavior) {
		if (behavior==this)
			return true;		
		return false;
	}
	
	public int getSizeofTableColor()
	{
		return tablecolor.size();
	}

	protected NamedElement getNamedElement() {
		return _elem;
	}
	

}
