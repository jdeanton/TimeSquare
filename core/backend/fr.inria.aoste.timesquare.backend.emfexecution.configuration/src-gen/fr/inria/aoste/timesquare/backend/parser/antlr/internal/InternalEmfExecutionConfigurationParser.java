package fr.inria.aoste.timesquare.backend.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.inria.aoste.timesquare.backend.services.EmfExecutionConfigurationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalEmfExecutionConfigurationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'When'", "'DSA'", "'returns'", "'true'", "'avoid'", "';'", "'import'", "'importJar'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalEmfExecutionConfigurationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalEmfExecutionConfigurationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalEmfExecutionConfigurationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalEmfExecutionConfiguration.g"; }



     	private EmfExecutionConfigurationGrammarAccess grammarAccess;

        public InternalEmfExecutionConfigurationParser(TokenStream input, EmfExecutionConfigurationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "EMFExecutionConfiguration";
       	}

       	@Override
       	protected EmfExecutionConfigurationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleEMFExecutionConfiguration"
    // InternalEmfExecutionConfiguration.g:63:1: entryRuleEMFExecutionConfiguration returns [EObject current=null] : iv_ruleEMFExecutionConfiguration= ruleEMFExecutionConfiguration EOF ;
    public final EObject entryRuleEMFExecutionConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEMFExecutionConfiguration = null;


        try {
            // InternalEmfExecutionConfiguration.g:63:66: (iv_ruleEMFExecutionConfiguration= ruleEMFExecutionConfiguration EOF )
            // InternalEmfExecutionConfiguration.g:64:2: iv_ruleEMFExecutionConfiguration= ruleEMFExecutionConfiguration EOF
            {
             newCompositeNode(grammarAccess.getEMFExecutionConfigurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEMFExecutionConfiguration=ruleEMFExecutionConfiguration();

            state._fsp--;

             current =iv_ruleEMFExecutionConfiguration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEMFExecutionConfiguration"


    // $ANTLR start "ruleEMFExecutionConfiguration"
    // InternalEmfExecutionConfiguration.g:70:1: ruleEMFExecutionConfiguration returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImportStatement ) )+ ( (lv_jarImportStatement_1_0= ruleJarImportStatement ) ) ( (lv_forcedClockMappings_2_0= ruleForcedClockMapping ) )* ) ;
    public final EObject ruleEMFExecutionConfiguration() throws RecognitionException {
        EObject current = null;

        EObject lv_imports_0_0 = null;

        EObject lv_jarImportStatement_1_0 = null;

        EObject lv_forcedClockMappings_2_0 = null;



        	enterRule();

        try {
            // InternalEmfExecutionConfiguration.g:76:2: ( ( ( (lv_imports_0_0= ruleImportStatement ) )+ ( (lv_jarImportStatement_1_0= ruleJarImportStatement ) ) ( (lv_forcedClockMappings_2_0= ruleForcedClockMapping ) )* ) )
            // InternalEmfExecutionConfiguration.g:77:2: ( ( (lv_imports_0_0= ruleImportStatement ) )+ ( (lv_jarImportStatement_1_0= ruleJarImportStatement ) ) ( (lv_forcedClockMappings_2_0= ruleForcedClockMapping ) )* )
            {
            // InternalEmfExecutionConfiguration.g:77:2: ( ( (lv_imports_0_0= ruleImportStatement ) )+ ( (lv_jarImportStatement_1_0= ruleJarImportStatement ) ) ( (lv_forcedClockMappings_2_0= ruleForcedClockMapping ) )* )
            // InternalEmfExecutionConfiguration.g:78:3: ( (lv_imports_0_0= ruleImportStatement ) )+ ( (lv_jarImportStatement_1_0= ruleJarImportStatement ) ) ( (lv_forcedClockMappings_2_0= ruleForcedClockMapping ) )*
            {
            // InternalEmfExecutionConfiguration.g:78:3: ( (lv_imports_0_0= ruleImportStatement ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==17) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalEmfExecutionConfiguration.g:79:4: (lv_imports_0_0= ruleImportStatement )
            	    {
            	    // InternalEmfExecutionConfiguration.g:79:4: (lv_imports_0_0= ruleImportStatement )
            	    // InternalEmfExecutionConfiguration.g:80:5: lv_imports_0_0= ruleImportStatement
            	    {

            	    					newCompositeNode(grammarAccess.getEMFExecutionConfigurationAccess().getImportsImportStatementParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_imports_0_0=ruleImportStatement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEMFExecutionConfigurationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_0_0,
            	    						"fr.inria.aoste.timesquare.backend.EmfExecutionConfiguration.ImportStatement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            // InternalEmfExecutionConfiguration.g:97:3: ( (lv_jarImportStatement_1_0= ruleJarImportStatement ) )
            // InternalEmfExecutionConfiguration.g:98:4: (lv_jarImportStatement_1_0= ruleJarImportStatement )
            {
            // InternalEmfExecutionConfiguration.g:98:4: (lv_jarImportStatement_1_0= ruleJarImportStatement )
            // InternalEmfExecutionConfiguration.g:99:5: lv_jarImportStatement_1_0= ruleJarImportStatement
            {

            					newCompositeNode(grammarAccess.getEMFExecutionConfigurationAccess().getJarImportStatementJarImportStatementParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_jarImportStatement_1_0=ruleJarImportStatement();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEMFExecutionConfigurationRule());
            					}
            					set(
            						current,
            						"jarImportStatement",
            						lv_jarImportStatement_1_0,
            						"fr.inria.aoste.timesquare.backend.EmfExecutionConfiguration.JarImportStatement");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalEmfExecutionConfiguration.g:116:3: ( (lv_forcedClockMappings_2_0= ruleForcedClockMapping ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==11) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalEmfExecutionConfiguration.g:117:4: (lv_forcedClockMappings_2_0= ruleForcedClockMapping )
            	    {
            	    // InternalEmfExecutionConfiguration.g:117:4: (lv_forcedClockMappings_2_0= ruleForcedClockMapping )
            	    // InternalEmfExecutionConfiguration.g:118:5: lv_forcedClockMappings_2_0= ruleForcedClockMapping
            	    {

            	    					newCompositeNode(grammarAccess.getEMFExecutionConfigurationAccess().getForcedClockMappingsForcedClockMappingParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_forcedClockMappings_2_0=ruleForcedClockMapping();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEMFExecutionConfigurationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"forcedClockMappings",
            	    						lv_forcedClockMappings_2_0,
            	    						"fr.inria.aoste.timesquare.backend.EmfExecutionConfiguration.ForcedClockMapping");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEMFExecutionConfiguration"


    // $ANTLR start "entryRuleForcedClockMapping"
    // InternalEmfExecutionConfiguration.g:139:1: entryRuleForcedClockMapping returns [EObject current=null] : iv_ruleForcedClockMapping= ruleForcedClockMapping EOF ;
    public final EObject entryRuleForcedClockMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForcedClockMapping = null;


        try {
            // InternalEmfExecutionConfiguration.g:139:59: (iv_ruleForcedClockMapping= ruleForcedClockMapping EOF )
            // InternalEmfExecutionConfiguration.g:140:2: iv_ruleForcedClockMapping= ruleForcedClockMapping EOF
            {
             newCompositeNode(grammarAccess.getForcedClockMappingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleForcedClockMapping=ruleForcedClockMapping();

            state._fsp--;

             current =iv_ruleForcedClockMapping; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForcedClockMapping"


    // $ANTLR start "ruleForcedClockMapping"
    // InternalEmfExecutionConfiguration.g:146:1: ruleForcedClockMapping returns [EObject current=null] : (otherlv_0= 'When' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'DSA' otherlv_3= 'returns' otherlv_4= 'true' otherlv_5= 'avoid' ( (otherlv_6= RULE_ID ) ) otherlv_7= ';' ) ;
    public final EObject ruleForcedClockMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;


        	enterRule();

        try {
            // InternalEmfExecutionConfiguration.g:152:2: ( (otherlv_0= 'When' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'DSA' otherlv_3= 'returns' otherlv_4= 'true' otherlv_5= 'avoid' ( (otherlv_6= RULE_ID ) ) otherlv_7= ';' ) )
            // InternalEmfExecutionConfiguration.g:153:2: (otherlv_0= 'When' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'DSA' otherlv_3= 'returns' otherlv_4= 'true' otherlv_5= 'avoid' ( (otherlv_6= RULE_ID ) ) otherlv_7= ';' )
            {
            // InternalEmfExecutionConfiguration.g:153:2: (otherlv_0= 'When' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'DSA' otherlv_3= 'returns' otherlv_4= 'true' otherlv_5= 'avoid' ( (otherlv_6= RULE_ID ) ) otherlv_7= ';' )
            // InternalEmfExecutionConfiguration.g:154:3: otherlv_0= 'When' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'DSA' otherlv_3= 'returns' otherlv_4= 'true' otherlv_5= 'avoid' ( (otherlv_6= RULE_ID ) ) otherlv_7= ';'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getForcedClockMappingAccess().getWhenKeyword_0());
            		
            // InternalEmfExecutionConfiguration.g:158:3: ( (otherlv_1= RULE_ID ) )
            // InternalEmfExecutionConfiguration.g:159:4: (otherlv_1= RULE_ID )
            {
            // InternalEmfExecutionConfiguration.g:159:4: (otherlv_1= RULE_ID )
            // InternalEmfExecutionConfiguration.g:160:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getForcedClockMappingRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(otherlv_1, grammarAccess.getForcedClockMappingAccess().getClockClockCrossReference_1_0());
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getForcedClockMappingAccess().getDSAKeyword_2());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_3, grammarAccess.getForcedClockMappingAccess().getReturnsKeyword_3());
            		
            otherlv_4=(Token)match(input,14,FOLLOW_9); 

            			newLeafNode(otherlv_4, grammarAccess.getForcedClockMappingAccess().getTrueKeyword_4());
            		
            otherlv_5=(Token)match(input,15,FOLLOW_5); 

            			newLeafNode(otherlv_5, grammarAccess.getForcedClockMappingAccess().getAvoidKeyword_5());
            		
            // InternalEmfExecutionConfiguration.g:187:3: ( (otherlv_6= RULE_ID ) )
            // InternalEmfExecutionConfiguration.g:188:4: (otherlv_6= RULE_ID )
            {
            // InternalEmfExecutionConfiguration.g:188:4: (otherlv_6= RULE_ID )
            // InternalEmfExecutionConfiguration.g:189:5: otherlv_6= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getForcedClockMappingRule());
            					}
            				
            otherlv_6=(Token)match(input,RULE_ID,FOLLOW_10); 

            					newLeafNode(otherlv_6, grammarAccess.getForcedClockMappingAccess().getClockToAvoidWhenTrueClockCrossReference_6_0());
            				

            }


            }

            otherlv_7=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getForcedClockMappingAccess().getSemicolonKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForcedClockMapping"


    // $ANTLR start "entryRuleImportStatement"
    // InternalEmfExecutionConfiguration.g:208:1: entryRuleImportStatement returns [EObject current=null] : iv_ruleImportStatement= ruleImportStatement EOF ;
    public final EObject entryRuleImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportStatement = null;


        try {
            // InternalEmfExecutionConfiguration.g:208:56: (iv_ruleImportStatement= ruleImportStatement EOF )
            // InternalEmfExecutionConfiguration.g:209:2: iv_ruleImportStatement= ruleImportStatement EOF
            {
             newCompositeNode(grammarAccess.getImportStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImportStatement=ruleImportStatement();

            state._fsp--;

             current =iv_ruleImportStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalEmfExecutionConfiguration.g:215:1: ruleImportStatement returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' ) ;
    public final EObject ruleImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalEmfExecutionConfiguration.g:221:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' ) )
            // InternalEmfExecutionConfiguration.g:222:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' )
            {
            // InternalEmfExecutionConfiguration.g:222:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';' )
            // InternalEmfExecutionConfiguration.g:223:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getImportStatementAccess().getImportKeyword_0());
            		
            // InternalEmfExecutionConfiguration.g:227:3: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalEmfExecutionConfiguration.g:228:4: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalEmfExecutionConfiguration.g:228:4: (lv_importURI_1_0= RULE_STRING )
            // InternalEmfExecutionConfiguration.g:229:5: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

            					newLeafNode(lv_importURI_1_0, grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImportStatementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"importURI",
            						lv_importURI_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getImportStatementAccess().getSemicolonKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleJarImportStatement"
    // InternalEmfExecutionConfiguration.g:253:1: entryRuleJarImportStatement returns [EObject current=null] : iv_ruleJarImportStatement= ruleJarImportStatement EOF ;
    public final EObject entryRuleJarImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJarImportStatement = null;


        try {
            // InternalEmfExecutionConfiguration.g:253:59: (iv_ruleJarImportStatement= ruleJarImportStatement EOF )
            // InternalEmfExecutionConfiguration.g:254:2: iv_ruleJarImportStatement= ruleJarImportStatement EOF
            {
             newCompositeNode(grammarAccess.getJarImportStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleJarImportStatement=ruleJarImportStatement();

            state._fsp--;

             current =iv_ruleJarImportStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJarImportStatement"


    // $ANTLR start "ruleJarImportStatement"
    // InternalEmfExecutionConfiguration.g:260:1: ruleJarImportStatement returns [EObject current=null] : (otherlv_0= 'importJar' ( (lv_pathToJar_1_0= RULE_STRING ) ) otherlv_2= ';' ) ;
    public final EObject ruleJarImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_pathToJar_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalEmfExecutionConfiguration.g:266:2: ( (otherlv_0= 'importJar' ( (lv_pathToJar_1_0= RULE_STRING ) ) otherlv_2= ';' ) )
            // InternalEmfExecutionConfiguration.g:267:2: (otherlv_0= 'importJar' ( (lv_pathToJar_1_0= RULE_STRING ) ) otherlv_2= ';' )
            {
            // InternalEmfExecutionConfiguration.g:267:2: (otherlv_0= 'importJar' ( (lv_pathToJar_1_0= RULE_STRING ) ) otherlv_2= ';' )
            // InternalEmfExecutionConfiguration.g:268:3: otherlv_0= 'importJar' ( (lv_pathToJar_1_0= RULE_STRING ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,18,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getJarImportStatementAccess().getImportJarKeyword_0());
            		
            // InternalEmfExecutionConfiguration.g:272:3: ( (lv_pathToJar_1_0= RULE_STRING ) )
            // InternalEmfExecutionConfiguration.g:273:4: (lv_pathToJar_1_0= RULE_STRING )
            {
            // InternalEmfExecutionConfiguration.g:273:4: (lv_pathToJar_1_0= RULE_STRING )
            // InternalEmfExecutionConfiguration.g:274:5: lv_pathToJar_1_0= RULE_STRING
            {
            lv_pathToJar_1_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

            					newLeafNode(lv_pathToJar_1_0, grammarAccess.getJarImportStatementAccess().getPathToJarSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getJarImportStatementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"pathToJar",
            						lv_pathToJar_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getJarImportStatementAccess().getSemicolonKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJarImportStatement"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000020L});

}