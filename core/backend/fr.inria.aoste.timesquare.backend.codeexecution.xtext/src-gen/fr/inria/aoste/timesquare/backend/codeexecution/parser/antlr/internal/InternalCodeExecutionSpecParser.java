package fr.inria.aoste.timesquare.backend.codeexecution.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.inria.aoste.timesquare.backend.codeexecution.services.CodeExecutionSpecGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalCodeExecutionSpecParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'CodeExecutionSpecification'", "'{'", "'}'", "'importModel'", "';'", "'as'", "'importClass'", "'importPackage'", "'Variable'", "':'", "'.'", "'*'", "'ClockBehavior'", "'execute'", "'call'", "'()'", "'when'", "'or'", "'->force'", "'('", "','", "')'", "'ticks'", "'doesnot_tick'", "'must_tick'", "'cannot_tick'", "'is_free'", "'is_undetermined'", "'isAlive'", "'isDead'", "'AssertionBehavior'", "'is_violated'", "'is_satisfied'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalCodeExecutionSpecParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCodeExecutionSpecParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCodeExecutionSpecParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCodeExecutionSpec.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */

     	private CodeExecutionSpecGrammarAccess grammarAccess;

        public InternalCodeExecutionSpecParser(TokenStream input, CodeExecutionSpecGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "CodeExecutionSpecification";
       	}

       	@Override
       	protected CodeExecutionSpecGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleCodeExecutionSpecification"
    // InternalCodeExecutionSpec.g:69:1: entryRuleCodeExecutionSpecification returns [EObject current=null] : iv_ruleCodeExecutionSpecification= ruleCodeExecutionSpecification EOF ;
    public final EObject entryRuleCodeExecutionSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCodeExecutionSpecification = null;


        try {
            // InternalCodeExecutionSpec.g:69:67: (iv_ruleCodeExecutionSpecification= ruleCodeExecutionSpecification EOF )
            // InternalCodeExecutionSpec.g:70:2: iv_ruleCodeExecutionSpecification= ruleCodeExecutionSpecification EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCodeExecutionSpecificationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCodeExecutionSpecification=ruleCodeExecutionSpecification();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCodeExecutionSpecification; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCodeExecutionSpecification"


    // $ANTLR start "ruleCodeExecutionSpecification"
    // InternalCodeExecutionSpec.g:76:1: ruleCodeExecutionSpecification returns [EObject current=null] : ( () otherlv_1= 'CodeExecutionSpecification' otherlv_2= '{' ( ( ( (lv_modelImports_3_0= ruleImportStatement ) ) | ( (lv_classImports_4_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_5_0= ruleImportStatement ) ) | ( (lv_classImports_6_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_7_0= ruleObjectVariable ) ) ( (lv_variables_8_0= ruleObjectVariable ) )* )? ( ( (lv_behaviors_9_0= ruleCodeExecutionBehavior ) ) ( (lv_behaviors_10_0= ruleCodeExecutionBehavior ) )* )? otherlv_11= '}' ) ;
    public final EObject ruleCodeExecutionSpecification() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_11=null;
        EObject lv_modelImports_3_0 = null;

        EObject lv_classImports_4_0 = null;

        EObject lv_modelImports_5_0 = null;

        EObject lv_classImports_6_0 = null;

        EObject lv_variables_7_0 = null;

        EObject lv_variables_8_0 = null;

        EObject lv_behaviors_9_0 = null;

        EObject lv_behaviors_10_0 = null;



        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:82:2: ( ( () otherlv_1= 'CodeExecutionSpecification' otherlv_2= '{' ( ( ( (lv_modelImports_3_0= ruleImportStatement ) ) | ( (lv_classImports_4_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_5_0= ruleImportStatement ) ) | ( (lv_classImports_6_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_7_0= ruleObjectVariable ) ) ( (lv_variables_8_0= ruleObjectVariable ) )* )? ( ( (lv_behaviors_9_0= ruleCodeExecutionBehavior ) ) ( (lv_behaviors_10_0= ruleCodeExecutionBehavior ) )* )? otherlv_11= '}' ) )
            // InternalCodeExecutionSpec.g:83:2: ( () otherlv_1= 'CodeExecutionSpecification' otherlv_2= '{' ( ( ( (lv_modelImports_3_0= ruleImportStatement ) ) | ( (lv_classImports_4_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_5_0= ruleImportStatement ) ) | ( (lv_classImports_6_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_7_0= ruleObjectVariable ) ) ( (lv_variables_8_0= ruleObjectVariable ) )* )? ( ( (lv_behaviors_9_0= ruleCodeExecutionBehavior ) ) ( (lv_behaviors_10_0= ruleCodeExecutionBehavior ) )* )? otherlv_11= '}' )
            {
            // InternalCodeExecutionSpec.g:83:2: ( () otherlv_1= 'CodeExecutionSpecification' otherlv_2= '{' ( ( ( (lv_modelImports_3_0= ruleImportStatement ) ) | ( (lv_classImports_4_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_5_0= ruleImportStatement ) ) | ( (lv_classImports_6_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_7_0= ruleObjectVariable ) ) ( (lv_variables_8_0= ruleObjectVariable ) )* )? ( ( (lv_behaviors_9_0= ruleCodeExecutionBehavior ) ) ( (lv_behaviors_10_0= ruleCodeExecutionBehavior ) )* )? otherlv_11= '}' )
            // InternalCodeExecutionSpec.g:84:3: () otherlv_1= 'CodeExecutionSpecification' otherlv_2= '{' ( ( ( (lv_modelImports_3_0= ruleImportStatement ) ) | ( (lv_classImports_4_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_5_0= ruleImportStatement ) ) | ( (lv_classImports_6_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_7_0= ruleObjectVariable ) ) ( (lv_variables_8_0= ruleObjectVariable ) )* )? ( ( (lv_behaviors_9_0= ruleCodeExecutionBehavior ) ) ( (lv_behaviors_10_0= ruleCodeExecutionBehavior ) )* )? otherlv_11= '}'
            {
            // InternalCodeExecutionSpec.g:84:3: ()
            // InternalCodeExecutionSpec.g:85:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getCodeExecutionSpecificationAccess().getCodeExecutionSpecificationAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,11,FollowSets000.FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getCodeExecutionSpecificationAccess().getCodeExecutionSpecificationKeyword_1());
              		
            }
            otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getCodeExecutionSpecificationAccess().getLeftCurlyBracketKeyword_2());
              		
            }
            // InternalCodeExecutionSpec.g:102:3: ( ( ( (lv_modelImports_3_0= ruleImportStatement ) ) | ( (lv_classImports_4_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_5_0= ruleImportStatement ) ) | ( (lv_classImports_6_0= ruleImportJavaStatement ) ) )* )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14||(LA3_0>=17 && LA3_0<=18)) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalCodeExecutionSpec.g:103:4: ( ( (lv_modelImports_3_0= ruleImportStatement ) ) | ( (lv_classImports_4_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_5_0= ruleImportStatement ) ) | ( (lv_classImports_6_0= ruleImportJavaStatement ) ) )*
                    {
                    // InternalCodeExecutionSpec.g:103:4: ( ( (lv_modelImports_3_0= ruleImportStatement ) ) | ( (lv_classImports_4_0= ruleImportJavaStatement ) ) )
                    int alt1=2;
                    int LA1_0 = input.LA(1);

                    if ( (LA1_0==14) ) {
                        alt1=1;
                    }
                    else if ( ((LA1_0>=17 && LA1_0<=18)) ) {
                        alt1=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 0, input);

                        throw nvae;
                    }
                    switch (alt1) {
                        case 1 :
                            // InternalCodeExecutionSpec.g:104:5: ( (lv_modelImports_3_0= ruleImportStatement ) )
                            {
                            // InternalCodeExecutionSpec.g:104:5: ( (lv_modelImports_3_0= ruleImportStatement ) )
                            // InternalCodeExecutionSpec.g:105:6: (lv_modelImports_3_0= ruleImportStatement )
                            {
                            // InternalCodeExecutionSpec.g:105:6: (lv_modelImports_3_0= ruleImportStatement )
                            // InternalCodeExecutionSpec.g:106:7: lv_modelImports_3_0= ruleImportStatement
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getCodeExecutionSpecificationAccess().getModelImportsImportStatementParserRuleCall_3_0_0_0());
                              						
                            }
                            pushFollow(FollowSets000.FOLLOW_4);
                            lv_modelImports_3_0=ruleImportStatement();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getCodeExecutionSpecificationRule());
                              							}
                              							add(
                              								current,
                              								"modelImports",
                              								lv_modelImports_3_0,
                              								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.ImportStatement");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalCodeExecutionSpec.g:124:5: ( (lv_classImports_4_0= ruleImportJavaStatement ) )
                            {
                            // InternalCodeExecutionSpec.g:124:5: ( (lv_classImports_4_0= ruleImportJavaStatement ) )
                            // InternalCodeExecutionSpec.g:125:6: (lv_classImports_4_0= ruleImportJavaStatement )
                            {
                            // InternalCodeExecutionSpec.g:125:6: (lv_classImports_4_0= ruleImportJavaStatement )
                            // InternalCodeExecutionSpec.g:126:7: lv_classImports_4_0= ruleImportJavaStatement
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getCodeExecutionSpecificationAccess().getClassImportsImportJavaStatementParserRuleCall_3_0_1_0());
                              						
                            }
                            pushFollow(FollowSets000.FOLLOW_4);
                            lv_classImports_4_0=ruleImportJavaStatement();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getCodeExecutionSpecificationRule());
                              							}
                              							add(
                              								current,
                              								"classImports",
                              								lv_classImports_4_0,
                              								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.ImportJavaStatement");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;

                    }

                    // InternalCodeExecutionSpec.g:144:4: ( ( (lv_modelImports_5_0= ruleImportStatement ) ) | ( (lv_classImports_6_0= ruleImportJavaStatement ) ) )*
                    loop2:
                    do {
                        int alt2=3;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==14) ) {
                            alt2=1;
                        }
                        else if ( ((LA2_0>=17 && LA2_0<=18)) ) {
                            alt2=2;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalCodeExecutionSpec.g:145:5: ( (lv_modelImports_5_0= ruleImportStatement ) )
                    	    {
                    	    // InternalCodeExecutionSpec.g:145:5: ( (lv_modelImports_5_0= ruleImportStatement ) )
                    	    // InternalCodeExecutionSpec.g:146:6: (lv_modelImports_5_0= ruleImportStatement )
                    	    {
                    	    // InternalCodeExecutionSpec.g:146:6: (lv_modelImports_5_0= ruleImportStatement )
                    	    // InternalCodeExecutionSpec.g:147:7: lv_modelImports_5_0= ruleImportStatement
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getCodeExecutionSpecificationAccess().getModelImportsImportStatementParserRuleCall_3_1_0_0());
                    	      						
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_4);
                    	    lv_modelImports_5_0=ruleImportStatement();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getCodeExecutionSpecificationRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"modelImports",
                    	      								lv_modelImports_5_0,
                    	      								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.ImportStatement");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalCodeExecutionSpec.g:165:5: ( (lv_classImports_6_0= ruleImportJavaStatement ) )
                    	    {
                    	    // InternalCodeExecutionSpec.g:165:5: ( (lv_classImports_6_0= ruleImportJavaStatement ) )
                    	    // InternalCodeExecutionSpec.g:166:6: (lv_classImports_6_0= ruleImportJavaStatement )
                    	    {
                    	    // InternalCodeExecutionSpec.g:166:6: (lv_classImports_6_0= ruleImportJavaStatement )
                    	    // InternalCodeExecutionSpec.g:167:7: lv_classImports_6_0= ruleImportJavaStatement
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getCodeExecutionSpecificationAccess().getClassImportsImportJavaStatementParserRuleCall_3_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_4);
                    	    lv_classImports_6_0=ruleImportJavaStatement();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getCodeExecutionSpecificationRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"classImports",
                    	      								lv_classImports_6_0,
                    	      								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.ImportJavaStatement");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalCodeExecutionSpec.g:186:3: ( ( (lv_variables_7_0= ruleObjectVariable ) ) ( (lv_variables_8_0= ruleObjectVariable ) )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==19) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalCodeExecutionSpec.g:187:4: ( (lv_variables_7_0= ruleObjectVariable ) ) ( (lv_variables_8_0= ruleObjectVariable ) )*
                    {
                    // InternalCodeExecutionSpec.g:187:4: ( (lv_variables_7_0= ruleObjectVariable ) )
                    // InternalCodeExecutionSpec.g:188:5: (lv_variables_7_0= ruleObjectVariable )
                    {
                    // InternalCodeExecutionSpec.g:188:5: (lv_variables_7_0= ruleObjectVariable )
                    // InternalCodeExecutionSpec.g:189:6: lv_variables_7_0= ruleObjectVariable
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCodeExecutionSpecificationAccess().getVariablesObjectVariableParserRuleCall_4_0_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_5);
                    lv_variables_7_0=ruleObjectVariable();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCodeExecutionSpecificationRule());
                      						}
                      						add(
                      							current,
                      							"variables",
                      							lv_variables_7_0,
                      							"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.ObjectVariable");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalCodeExecutionSpec.g:206:4: ( (lv_variables_8_0= ruleObjectVariable ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==19) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalCodeExecutionSpec.g:207:5: (lv_variables_8_0= ruleObjectVariable )
                    	    {
                    	    // InternalCodeExecutionSpec.g:207:5: (lv_variables_8_0= ruleObjectVariable )
                    	    // InternalCodeExecutionSpec.g:208:6: lv_variables_8_0= ruleObjectVariable
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getCodeExecutionSpecificationAccess().getVariablesObjectVariableParserRuleCall_4_1_0());
                    	      					
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_5);
                    	    lv_variables_8_0=ruleObjectVariable();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getCodeExecutionSpecificationRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"variables",
                    	      							lv_variables_8_0,
                    	      							"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.ObjectVariable");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalCodeExecutionSpec.g:226:3: ( ( (lv_behaviors_9_0= ruleCodeExecutionBehavior ) ) ( (lv_behaviors_10_0= ruleCodeExecutionBehavior ) )* )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==23||LA7_0==41) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalCodeExecutionSpec.g:227:4: ( (lv_behaviors_9_0= ruleCodeExecutionBehavior ) ) ( (lv_behaviors_10_0= ruleCodeExecutionBehavior ) )*
                    {
                    // InternalCodeExecutionSpec.g:227:4: ( (lv_behaviors_9_0= ruleCodeExecutionBehavior ) )
                    // InternalCodeExecutionSpec.g:228:5: (lv_behaviors_9_0= ruleCodeExecutionBehavior )
                    {
                    // InternalCodeExecutionSpec.g:228:5: (lv_behaviors_9_0= ruleCodeExecutionBehavior )
                    // InternalCodeExecutionSpec.g:229:6: lv_behaviors_9_0= ruleCodeExecutionBehavior
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCodeExecutionSpecificationAccess().getBehaviorsCodeExecutionBehaviorParserRuleCall_5_0_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_6);
                    lv_behaviors_9_0=ruleCodeExecutionBehavior();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCodeExecutionSpecificationRule());
                      						}
                      						add(
                      							current,
                      							"behaviors",
                      							lv_behaviors_9_0,
                      							"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.CodeExecutionBehavior");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalCodeExecutionSpec.g:246:4: ( (lv_behaviors_10_0= ruleCodeExecutionBehavior ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==23||LA6_0==41) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalCodeExecutionSpec.g:247:5: (lv_behaviors_10_0= ruleCodeExecutionBehavior )
                    	    {
                    	    // InternalCodeExecutionSpec.g:247:5: (lv_behaviors_10_0= ruleCodeExecutionBehavior )
                    	    // InternalCodeExecutionSpec.g:248:6: lv_behaviors_10_0= ruleCodeExecutionBehavior
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getCodeExecutionSpecificationAccess().getBehaviorsCodeExecutionBehaviorParserRuleCall_5_1_0());
                    	      					
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_6);
                    	    lv_behaviors_10_0=ruleCodeExecutionBehavior();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getCodeExecutionSpecificationRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"behaviors",
                    	      							lv_behaviors_10_0,
                    	      							"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.CodeExecutionBehavior");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,13,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_11, grammarAccess.getCodeExecutionSpecificationAccess().getRightCurlyBracketKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCodeExecutionSpecification"


    // $ANTLR start "entryRuleImportStatement"
    // InternalCodeExecutionSpec.g:274:1: entryRuleImportStatement returns [EObject current=null] : iv_ruleImportStatement= ruleImportStatement EOF ;
    public final EObject entryRuleImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportStatement = null;


        try {
            // InternalCodeExecutionSpec.g:274:56: (iv_ruleImportStatement= ruleImportStatement EOF )
            // InternalCodeExecutionSpec.g:275:2: iv_ruleImportStatement= ruleImportStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportStatementRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImportStatement=ruleImportStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImportStatement; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalCodeExecutionSpec.g:281:1: ruleImportStatement returns [EObject current=null] : (otherlv_0= 'importModel' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) ) ) ;
    public final EObject ruleImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_alias_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_importURI_1_0 = null;

        AntlrDatatypeRuleToken lv_alias_4_0 = null;



        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:287:2: ( (otherlv_0= 'importModel' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) ) ) )
            // InternalCodeExecutionSpec.g:288:2: (otherlv_0= 'importModel' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) ) )
            {
            // InternalCodeExecutionSpec.g:288:2: (otherlv_0= 'importModel' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) ) )
            // InternalCodeExecutionSpec.g:289:3: otherlv_0= 'importModel' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) )
            {
            otherlv_0=(Token)match(input,14,FollowSets000.FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getImportStatementAccess().getImportModelKeyword_0());
              		
            }
            // InternalCodeExecutionSpec.g:293:3: ( (lv_importURI_1_0= ruleEString ) )
            // InternalCodeExecutionSpec.g:294:4: (lv_importURI_1_0= ruleEString )
            {
            // InternalCodeExecutionSpec.g:294:4: (lv_importURI_1_0= ruleEString )
            // InternalCodeExecutionSpec.g:295:5: lv_importURI_1_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getImportStatementAccess().getImportURIEStringParserRuleCall_1_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_8);
            lv_importURI_1_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getImportStatementRule());
              					}
              					set(
              						current,
              						"importURI",
              						lv_importURI_1_0,
              						"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalCodeExecutionSpec.g:312:3: ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==15) ) {
                alt8=1;
            }
            else if ( (LA8_0==16) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalCodeExecutionSpec.g:313:4: ( (lv_alias_2_0= ';' ) )
                    {
                    // InternalCodeExecutionSpec.g:313:4: ( (lv_alias_2_0= ';' ) )
                    // InternalCodeExecutionSpec.g:314:5: (lv_alias_2_0= ';' )
                    {
                    // InternalCodeExecutionSpec.g:314:5: (lv_alias_2_0= ';' )
                    // InternalCodeExecutionSpec.g:315:6: lv_alias_2_0= ';'
                    {
                    lv_alias_2_0=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_alias_2_0, grammarAccess.getImportStatementAccess().getAliasSemicolonKeyword_2_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getImportStatementRule());
                      						}
                      						setWithLastConsumed(current, "alias", lv_alias_2_0, ";");
                      					
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:328:4: (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' )
                    {
                    // InternalCodeExecutionSpec.g:328:4: (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' )
                    // InternalCodeExecutionSpec.g:329:5: otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';'
                    {
                    otherlv_3=(Token)match(input,16,FollowSets000.FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_3, grammarAccess.getImportStatementAccess().getAsKeyword_2_1_0());
                      				
                    }
                    // InternalCodeExecutionSpec.g:333:5: ( (lv_alias_4_0= ruleEString ) )
                    // InternalCodeExecutionSpec.g:334:6: (lv_alias_4_0= ruleEString )
                    {
                    // InternalCodeExecutionSpec.g:334:6: (lv_alias_4_0= ruleEString )
                    // InternalCodeExecutionSpec.g:335:7: lv_alias_4_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getImportStatementAccess().getAliasEStringParserRuleCall_2_1_1_0());
                      						
                    }
                    pushFollow(FollowSets000.FOLLOW_9);
                    lv_alias_4_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getImportStatementRule());
                      							}
                      							set(
                      								current,
                      								"alias",
                      								lv_alias_4_0,
                      								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.EString");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    otherlv_5=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_5, grammarAccess.getImportStatementAccess().getSemicolonKeyword_2_1_2());
                      				
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleImportJavaStatement"
    // InternalCodeExecutionSpec.g:362:1: entryRuleImportJavaStatement returns [EObject current=null] : iv_ruleImportJavaStatement= ruleImportJavaStatement EOF ;
    public final EObject entryRuleImportJavaStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportJavaStatement = null;


        try {
            // InternalCodeExecutionSpec.g:362:60: (iv_ruleImportJavaStatement= ruleImportJavaStatement EOF )
            // InternalCodeExecutionSpec.g:363:2: iv_ruleImportJavaStatement= ruleImportJavaStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportJavaStatementRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImportJavaStatement=ruleImportJavaStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImportJavaStatement; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportJavaStatement"


    // $ANTLR start "ruleImportJavaStatement"
    // InternalCodeExecutionSpec.g:369:1: ruleImportJavaStatement returns [EObject current=null] : ( ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';' ) ;
    public final EObject ruleImportJavaStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;

        AntlrDatatypeRuleToken lv_importedNamespace_3_0 = null;



        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:375:2: ( ( ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';' ) )
            // InternalCodeExecutionSpec.g:376:2: ( ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';' )
            {
            // InternalCodeExecutionSpec.g:376:2: ( ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';' )
            // InternalCodeExecutionSpec.g:377:3: ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';'
            {
            // InternalCodeExecutionSpec.g:377:3: ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==17) ) {
                alt9=1;
            }
            else if ( (LA9_0==18) ) {
                alt9=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalCodeExecutionSpec.g:378:4: (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) )
                    {
                    // InternalCodeExecutionSpec.g:378:4: (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) )
                    // InternalCodeExecutionSpec.g:379:5: otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) )
                    {
                    otherlv_0=(Token)match(input,17,FollowSets000.FOLLOW_10); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_0, grammarAccess.getImportJavaStatementAccess().getImportClassKeyword_0_0_0());
                      				
                    }
                    // InternalCodeExecutionSpec.g:383:5: ( (lv_importedNamespace_1_0= ruleQualifiedName ) )
                    // InternalCodeExecutionSpec.g:384:6: (lv_importedNamespace_1_0= ruleQualifiedName )
                    {
                    // InternalCodeExecutionSpec.g:384:6: (lv_importedNamespace_1_0= ruleQualifiedName )
                    // InternalCodeExecutionSpec.g:385:7: lv_importedNamespace_1_0= ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getImportJavaStatementAccess().getImportedNamespaceQualifiedNameParserRuleCall_0_0_1_0());
                      						
                    }
                    pushFollow(FollowSets000.FOLLOW_9);
                    lv_importedNamespace_1_0=ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getImportJavaStatementRule());
                      							}
                      							set(
                      								current,
                      								"importedNamespace",
                      								lv_importedNamespace_1_0,
                      								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.QualifiedName");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:404:4: (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) )
                    {
                    // InternalCodeExecutionSpec.g:404:4: (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) )
                    // InternalCodeExecutionSpec.g:405:5: otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) )
                    {
                    otherlv_2=(Token)match(input,18,FollowSets000.FOLLOW_10); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_2, grammarAccess.getImportJavaStatementAccess().getImportPackageKeyword_0_1_0());
                      				
                    }
                    // InternalCodeExecutionSpec.g:409:5: ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) )
                    // InternalCodeExecutionSpec.g:410:6: (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard )
                    {
                    // InternalCodeExecutionSpec.g:410:6: (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard )
                    // InternalCodeExecutionSpec.g:411:7: lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getImportJavaStatementAccess().getImportedNamespaceQualifiedNameWithWildCardParserRuleCall_0_1_1_0());
                      						
                    }
                    pushFollow(FollowSets000.FOLLOW_9);
                    lv_importedNamespace_3_0=ruleQualifiedNameWithWildCard();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getImportJavaStatementRule());
                      							}
                      							set(
                      								current,
                      								"importedNamespace",
                      								lv_importedNamespace_3_0,
                      								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.QualifiedNameWithWildCard");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getImportJavaStatementAccess().getSemicolonKeyword_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportJavaStatement"


    // $ANTLR start "entryRuleObjectVariable"
    // InternalCodeExecutionSpec.g:438:1: entryRuleObjectVariable returns [EObject current=null] : iv_ruleObjectVariable= ruleObjectVariable EOF ;
    public final EObject entryRuleObjectVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectVariable = null;


        try {
            // InternalCodeExecutionSpec.g:438:55: (iv_ruleObjectVariable= ruleObjectVariable EOF )
            // InternalCodeExecutionSpec.g:439:2: iv_ruleObjectVariable= ruleObjectVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getObjectVariableRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleObjectVariable=ruleObjectVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleObjectVariable; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectVariable"


    // $ANTLR start "ruleObjectVariable"
    // InternalCodeExecutionSpec.g:445:1: ruleObjectVariable returns [EObject current=null] : (otherlv_0= 'Variable' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_type_3_0= ruleJvmTypeReference ) ) otherlv_4= ';' ) ;
    public final EObject ruleObjectVariable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_type_3_0 = null;



        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:451:2: ( (otherlv_0= 'Variable' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_type_3_0= ruleJvmTypeReference ) ) otherlv_4= ';' ) )
            // InternalCodeExecutionSpec.g:452:2: (otherlv_0= 'Variable' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_type_3_0= ruleJvmTypeReference ) ) otherlv_4= ';' )
            {
            // InternalCodeExecutionSpec.g:452:2: (otherlv_0= 'Variable' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_type_3_0= ruleJvmTypeReference ) ) otherlv_4= ';' )
            // InternalCodeExecutionSpec.g:453:3: otherlv_0= 'Variable' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' ( (lv_type_3_0= ruleJvmTypeReference ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,19,FollowSets000.FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getObjectVariableAccess().getVariableKeyword_0());
              		
            }
            // InternalCodeExecutionSpec.g:457:3: ( (lv_name_1_0= ruleEString ) )
            // InternalCodeExecutionSpec.g:458:4: (lv_name_1_0= ruleEString )
            {
            // InternalCodeExecutionSpec.g:458:4: (lv_name_1_0= ruleEString )
            // InternalCodeExecutionSpec.g:459:5: lv_name_1_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getObjectVariableAccess().getNameEStringParserRuleCall_1_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_11);
            lv_name_1_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getObjectVariableRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,20,FollowSets000.FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getObjectVariableAccess().getColonKeyword_2());
              		
            }
            // InternalCodeExecutionSpec.g:480:3: ( (lv_type_3_0= ruleJvmTypeReference ) )
            // InternalCodeExecutionSpec.g:481:4: (lv_type_3_0= ruleJvmTypeReference )
            {
            // InternalCodeExecutionSpec.g:481:4: (lv_type_3_0= ruleJvmTypeReference )
            // InternalCodeExecutionSpec.g:482:5: lv_type_3_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getObjectVariableAccess().getTypeJvmTypeReferenceParserRuleCall_3_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_9);
            lv_type_3_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getObjectVariableRule());
              					}
              					set(
              						current,
              						"type",
              						lv_type_3_0,
              						"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.JvmTypeReference");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getObjectVariableAccess().getSemicolonKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectVariable"


    // $ANTLR start "entryRuleQualifiedNameWithWildCard"
    // InternalCodeExecutionSpec.g:507:1: entryRuleQualifiedNameWithWildCard returns [String current=null] : iv_ruleQualifiedNameWithWildCard= ruleQualifiedNameWithWildCard EOF ;
    public final String entryRuleQualifiedNameWithWildCard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildCard = null;


        try {
            // InternalCodeExecutionSpec.g:507:65: (iv_ruleQualifiedNameWithWildCard= ruleQualifiedNameWithWildCard EOF )
            // InternalCodeExecutionSpec.g:508:2: iv_ruleQualifiedNameWithWildCard= ruleQualifiedNameWithWildCard EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameWithWildCardRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleQualifiedNameWithWildCard=ruleQualifiedNameWithWildCard();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedNameWithWildCard.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildCard"


    // $ANTLR start "ruleQualifiedNameWithWildCard"
    // InternalCodeExecutionSpec.g:514:1: ruleQualifiedNameWithWildCard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*' ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildCard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:520:2: ( (this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*' ) )
            // InternalCodeExecutionSpec.g:521:2: (this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*' )
            {
            // InternalCodeExecutionSpec.g:521:2: (this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*' )
            // InternalCodeExecutionSpec.g:522:3: this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*'
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getQualifiedNameWithWildCardAccess().getQualifiedNameParserRuleCall_0());
              		
            }
            pushFollow(FollowSets000.FOLLOW_12);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_QualifiedName_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            kw=(Token)match(input,21,FollowSets000.FOLLOW_13); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getQualifiedNameWithWildCardAccess().getFullStopKeyword_1());
              		
            }
            kw=(Token)match(input,22,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getQualifiedNameWithWildCardAccess().getAsteriskKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildCard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalCodeExecutionSpec.g:546:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalCodeExecutionSpec.g:546:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalCodeExecutionSpec.g:547:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedName.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalCodeExecutionSpec.g:553:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:559:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalCodeExecutionSpec.g:560:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalCodeExecutionSpec.g:560:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalCodeExecutionSpec.g:561:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ID_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
              		
            }
            // InternalCodeExecutionSpec.g:568:3: (kw= '.' this_ID_2= RULE_ID )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==21) ) {
                    int LA10_2 = input.LA(2);

                    if ( (LA10_2==RULE_ID) ) {
                        alt10=1;
                    }


                }


                switch (alt10) {
            	case 1 :
            	    // InternalCodeExecutionSpec.g:569:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,21,FollowSets000.FOLLOW_10); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(kw);
            	      				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	      			
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_14); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_ID_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleJvmTypeReference"
    // InternalCodeExecutionSpec.g:586:1: entryRuleJvmTypeReference returns [EObject current=null] : iv_ruleJvmTypeReference= ruleJvmTypeReference EOF ;
    public final EObject entryRuleJvmTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmTypeReference = null;


        try {
            // InternalCodeExecutionSpec.g:586:57: (iv_ruleJvmTypeReference= ruleJvmTypeReference EOF )
            // InternalCodeExecutionSpec.g:587:2: iv_ruleJvmTypeReference= ruleJvmTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmTypeReferenceRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleJvmTypeReference=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmTypeReference; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmTypeReference"


    // $ANTLR start "ruleJvmTypeReference"
    // InternalCodeExecutionSpec.g:593:1: ruleJvmTypeReference returns [EObject current=null] : ( () ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleJvmTypeReference() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:599:2: ( ( () ( ( ruleQualifiedName ) ) ) )
            // InternalCodeExecutionSpec.g:600:2: ( () ( ( ruleQualifiedName ) ) )
            {
            // InternalCodeExecutionSpec.g:600:2: ( () ( ( ruleQualifiedName ) ) )
            // InternalCodeExecutionSpec.g:601:3: () ( ( ruleQualifiedName ) )
            {
            // InternalCodeExecutionSpec.g:601:3: ()
            // InternalCodeExecutionSpec.g:602:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getJvmTypeReferenceAccess().getJvmAnyTypeReferenceAction_0(),
              					current);
              			
            }

            }

            // InternalCodeExecutionSpec.g:611:3: ( ( ruleQualifiedName ) )
            // InternalCodeExecutionSpec.g:612:4: ( ruleQualifiedName )
            {
            // InternalCodeExecutionSpec.g:612:4: ( ruleQualifiedName )
            // InternalCodeExecutionSpec.g:613:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getJvmTypeReferenceRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getJvmTypeReferenceAccess().getTypeJvmTypeCrossReference_1_0());
              				
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmTypeReference"


    // $ANTLR start "entryRuleCodeExecutionBehavior"
    // InternalCodeExecutionSpec.g:634:1: entryRuleCodeExecutionBehavior returns [EObject current=null] : iv_ruleCodeExecutionBehavior= ruleCodeExecutionBehavior EOF ;
    public final EObject entryRuleCodeExecutionBehavior() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCodeExecutionBehavior = null;


        try {
            // InternalCodeExecutionSpec.g:634:62: (iv_ruleCodeExecutionBehavior= ruleCodeExecutionBehavior EOF )
            // InternalCodeExecutionSpec.g:635:2: iv_ruleCodeExecutionBehavior= ruleCodeExecutionBehavior EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCodeExecutionBehaviorRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCodeExecutionBehavior=ruleCodeExecutionBehavior();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCodeExecutionBehavior; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCodeExecutionBehavior"


    // $ANTLR start "ruleCodeExecutionBehavior"
    // InternalCodeExecutionSpec.g:641:1: ruleCodeExecutionBehavior returns [EObject current=null] : (this_ClockBehavior_0= ruleClockBehavior | this_AssertionBehavior_1= ruleAssertionBehavior ) ;
    public final EObject ruleCodeExecutionBehavior() throws RecognitionException {
        EObject current = null;

        EObject this_ClockBehavior_0 = null;

        EObject this_AssertionBehavior_1 = null;



        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:647:2: ( (this_ClockBehavior_0= ruleClockBehavior | this_AssertionBehavior_1= ruleAssertionBehavior ) )
            // InternalCodeExecutionSpec.g:648:2: (this_ClockBehavior_0= ruleClockBehavior | this_AssertionBehavior_1= ruleAssertionBehavior )
            {
            // InternalCodeExecutionSpec.g:648:2: (this_ClockBehavior_0= ruleClockBehavior | this_AssertionBehavior_1= ruleAssertionBehavior )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            else if ( (LA11_0==41) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalCodeExecutionSpec.g:649:3: this_ClockBehavior_0= ruleClockBehavior
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getCodeExecutionBehaviorAccess().getClockBehaviorParserRuleCall_0());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ClockBehavior_0=ruleClockBehavior();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ClockBehavior_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:661:3: this_AssertionBehavior_1= ruleAssertionBehavior
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getCodeExecutionBehaviorAccess().getAssertionBehaviorParserRuleCall_1());
                      		
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_AssertionBehavior_1=ruleAssertionBehavior();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_AssertionBehavior_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCodeExecutionBehavior"


    // $ANTLR start "entryRuleClockBehavior"
    // InternalCodeExecutionSpec.g:676:1: entryRuleClockBehavior returns [EObject current=null] : iv_ruleClockBehavior= ruleClockBehavior EOF ;
    public final EObject entryRuleClockBehavior() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClockBehavior = null;


        try {
            // InternalCodeExecutionSpec.g:676:54: (iv_ruleClockBehavior= ruleClockBehavior EOF )
            // InternalCodeExecutionSpec.g:677:2: iv_ruleClockBehavior= ruleClockBehavior EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClockBehaviorRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleClockBehavior=ruleClockBehavior();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClockBehavior; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClockBehavior"


    // $ANTLR start "ruleClockBehavior"
    // InternalCodeExecutionSpec.g:683:1: ruleClockBehavior returns [EObject current=null] : (otherlv_0= 'ClockBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( (lv_fireState_10_0= ruleFiredStateKind ) ) | ( (lv_enableState_11_0= ruleEnableStateKind ) ) | ( (lv_liveState_12_0= ruleLiveStateKind ) ) ) (otherlv_13= 'or' ( ( (lv_fireState_14_0= ruleFiredStateKind ) ) | ( (lv_enableState_15_0= ruleEnableStateKind ) ) | ( (lv_liveState_16_0= ruleLiveStateKind ) ) ) )* (otherlv_17= '->force' otherlv_18= '(' ( (otherlv_19= RULE_ID ) ) (otherlv_20= ',' ( (otherlv_21= RULE_ID ) ) )* otherlv_22= ')' )? otherlv_23= ';' ) ;
    public final EObject ruleClockBehavior() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_13=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        AntlrDatatypeRuleToken lv_fireState_10_0 = null;

        AntlrDatatypeRuleToken lv_enableState_11_0 = null;

        AntlrDatatypeRuleToken lv_liveState_12_0 = null;

        AntlrDatatypeRuleToken lv_fireState_14_0 = null;

        AntlrDatatypeRuleToken lv_enableState_15_0 = null;

        AntlrDatatypeRuleToken lv_liveState_16_0 = null;



        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:689:2: ( (otherlv_0= 'ClockBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( (lv_fireState_10_0= ruleFiredStateKind ) ) | ( (lv_enableState_11_0= ruleEnableStateKind ) ) | ( (lv_liveState_12_0= ruleLiveStateKind ) ) ) (otherlv_13= 'or' ( ( (lv_fireState_14_0= ruleFiredStateKind ) ) | ( (lv_enableState_15_0= ruleEnableStateKind ) ) | ( (lv_liveState_16_0= ruleLiveStateKind ) ) ) )* (otherlv_17= '->force' otherlv_18= '(' ( (otherlv_19= RULE_ID ) ) (otherlv_20= ',' ( (otherlv_21= RULE_ID ) ) )* otherlv_22= ')' )? otherlv_23= ';' ) )
            // InternalCodeExecutionSpec.g:690:2: (otherlv_0= 'ClockBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( (lv_fireState_10_0= ruleFiredStateKind ) ) | ( (lv_enableState_11_0= ruleEnableStateKind ) ) | ( (lv_liveState_12_0= ruleLiveStateKind ) ) ) (otherlv_13= 'or' ( ( (lv_fireState_14_0= ruleFiredStateKind ) ) | ( (lv_enableState_15_0= ruleEnableStateKind ) ) | ( (lv_liveState_16_0= ruleLiveStateKind ) ) ) )* (otherlv_17= '->force' otherlv_18= '(' ( (otherlv_19= RULE_ID ) ) (otherlv_20= ',' ( (otherlv_21= RULE_ID ) ) )* otherlv_22= ')' )? otherlv_23= ';' )
            {
            // InternalCodeExecutionSpec.g:690:2: (otherlv_0= 'ClockBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( (lv_fireState_10_0= ruleFiredStateKind ) ) | ( (lv_enableState_11_0= ruleEnableStateKind ) ) | ( (lv_liveState_12_0= ruleLiveStateKind ) ) ) (otherlv_13= 'or' ( ( (lv_fireState_14_0= ruleFiredStateKind ) ) | ( (lv_enableState_15_0= ruleEnableStateKind ) ) | ( (lv_liveState_16_0= ruleLiveStateKind ) ) ) )* (otherlv_17= '->force' otherlv_18= '(' ( (otherlv_19= RULE_ID ) ) (otherlv_20= ',' ( (otherlv_21= RULE_ID ) ) )* otherlv_22= ')' )? otherlv_23= ';' )
            // InternalCodeExecutionSpec.g:691:3: otherlv_0= 'ClockBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( (lv_fireState_10_0= ruleFiredStateKind ) ) | ( (lv_enableState_11_0= ruleEnableStateKind ) ) | ( (lv_liveState_12_0= ruleLiveStateKind ) ) ) (otherlv_13= 'or' ( ( (lv_fireState_14_0= ruleFiredStateKind ) ) | ( (lv_enableState_15_0= ruleEnableStateKind ) ) | ( (lv_liveState_16_0= ruleLiveStateKind ) ) ) )* (otherlv_17= '->force' otherlv_18= '(' ( (otherlv_19= RULE_ID ) ) (otherlv_20= ',' ( (otherlv_21= RULE_ID ) ) )* otherlv_22= ')' )? otherlv_23= ';'
            {
            otherlv_0=(Token)match(input,23,FollowSets000.FOLLOW_15); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getClockBehaviorAccess().getClockBehaviorKeyword_0());
              		
            }
            // InternalCodeExecutionSpec.g:695:3: ()
            // InternalCodeExecutionSpec.g:696:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getClockBehaviorAccess().getCodeExecClockBehaviorAction_1(),
              					current);
              			
            }

            }

            // InternalCodeExecutionSpec.g:705:3: ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? )
            // InternalCodeExecutionSpec.g:706:4: (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )?
            {
            // InternalCodeExecutionSpec.g:706:4: (otherlv_2= 'execute' | otherlv_3= 'call' )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==24) ) {
                alt12=1;
            }
            else if ( (LA12_0==25) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalCodeExecutionSpec.g:707:5: otherlv_2= 'execute'
                    {
                    otherlv_2=(Token)match(input,24,FollowSets000.FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_2, grammarAccess.getClockBehaviorAccess().getExecuteKeyword_2_0_0());
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:712:5: otherlv_3= 'call'
                    {
                    otherlv_3=(Token)match(input,25,FollowSets000.FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_3, grammarAccess.getClockBehaviorAccess().getCallKeyword_2_0_1());
                      				
                    }

                    }
                    break;

            }

            // InternalCodeExecutionSpec.g:717:4: ( ( ruleEString ) )
            // InternalCodeExecutionSpec.g:718:5: ( ruleEString )
            {
            // InternalCodeExecutionSpec.g:718:5: ( ruleEString )
            // InternalCodeExecutionSpec.g:719:6: ruleEString
            {
            if ( state.backtracking==0 ) {

              						/* */
              					
            }
            if ( state.backtracking==0 ) {

              						if (current==null) {
              							current = createModelElement(grammarAccess.getClockBehaviorRule());
              						}
              					
            }
            if ( state.backtracking==0 ) {

              						newCompositeNode(grammarAccess.getClockBehaviorAccess().getObjectVariableObjectVariableCrossReference_2_1_0());
              					
            }
            pushFollow(FollowSets000.FOLLOW_12);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              						afterParserOrEnumRuleCall();
              					
            }

            }


            }

            otherlv_5=(Token)match(input,21,FollowSets000.FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_5, grammarAccess.getClockBehaviorAccess().getFullStopKeyword_2_2());
              			
            }
            // InternalCodeExecutionSpec.g:740:4: ( (otherlv_6= RULE_ID ) )
            // InternalCodeExecutionSpec.g:741:5: (otherlv_6= RULE_ID )
            {
            // InternalCodeExecutionSpec.g:741:5: (otherlv_6= RULE_ID )
            // InternalCodeExecutionSpec.g:742:6: otherlv_6= RULE_ID
            {
            if ( state.backtracking==0 ) {

              						/* */
              					
            }
            if ( state.backtracking==0 ) {

              						if (current==null) {
              							current = createModelElement(grammarAccess.getClockBehaviorRule());
              						}
              					
            }
            otherlv_6=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_16); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              						newLeafNode(otherlv_6, grammarAccess.getClockBehaviorAccess().getMethodJvmOperationCrossReference_2_3_0());
              					
            }

            }


            }

            // InternalCodeExecutionSpec.g:756:4: (otherlv_7= '()' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==26) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalCodeExecutionSpec.g:757:5: otherlv_7= '()'
                    {
                    otherlv_7=(Token)match(input,26,FollowSets000.FOLLOW_17); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_7, grammarAccess.getClockBehaviorAccess().getLeftParenthesisRightParenthesisKeyword_2_4());
                      				
                    }

                    }
                    break;

            }


            }

            otherlv_8=(Token)match(input,27,FollowSets000.FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getClockBehaviorAccess().getWhenKeyword_3());
              		
            }
            // InternalCodeExecutionSpec.g:767:3: ( (otherlv_9= RULE_ID ) )
            // InternalCodeExecutionSpec.g:768:4: (otherlv_9= RULE_ID )
            {
            // InternalCodeExecutionSpec.g:768:4: (otherlv_9= RULE_ID )
            // InternalCodeExecutionSpec.g:769:5: otherlv_9= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getClockBehaviorRule());
              					}
              				
            }
            otherlv_9=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_9, grammarAccess.getClockBehaviorAccess().getClockClockCrossReference_4_0());
              				
            }

            }


            }

            // InternalCodeExecutionSpec.g:783:3: ( ( (lv_fireState_10_0= ruleFiredStateKind ) ) | ( (lv_enableState_11_0= ruleEnableStateKind ) ) | ( (lv_liveState_12_0= ruleLiveStateKind ) ) )
            int alt14=3;
            switch ( input.LA(1) ) {
            case 33:
            case 34:
                {
                alt14=1;
                }
                break;
            case 35:
            case 36:
            case 37:
            case 38:
                {
                alt14=2;
                }
                break;
            case 39:
            case 40:
                {
                alt14=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalCodeExecutionSpec.g:784:4: ( (lv_fireState_10_0= ruleFiredStateKind ) )
                    {
                    // InternalCodeExecutionSpec.g:784:4: ( (lv_fireState_10_0= ruleFiredStateKind ) )
                    // InternalCodeExecutionSpec.g:785:5: (lv_fireState_10_0= ruleFiredStateKind )
                    {
                    // InternalCodeExecutionSpec.g:785:5: (lv_fireState_10_0= ruleFiredStateKind )
                    // InternalCodeExecutionSpec.g:786:6: lv_fireState_10_0= ruleFiredStateKind
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getClockBehaviorAccess().getFireStateFiredStateKindParserRuleCall_5_0_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_19);
                    lv_fireState_10_0=ruleFiredStateKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getClockBehaviorRule());
                      						}
                      						add(
                      							current,
                      							"fireState",
                      							lv_fireState_10_0,
                      							"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.FiredStateKind");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:804:4: ( (lv_enableState_11_0= ruleEnableStateKind ) )
                    {
                    // InternalCodeExecutionSpec.g:804:4: ( (lv_enableState_11_0= ruleEnableStateKind ) )
                    // InternalCodeExecutionSpec.g:805:5: (lv_enableState_11_0= ruleEnableStateKind )
                    {
                    // InternalCodeExecutionSpec.g:805:5: (lv_enableState_11_0= ruleEnableStateKind )
                    // InternalCodeExecutionSpec.g:806:6: lv_enableState_11_0= ruleEnableStateKind
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getClockBehaviorAccess().getEnableStateEnableStateKindParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_19);
                    lv_enableState_11_0=ruleEnableStateKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getClockBehaviorRule());
                      						}
                      						add(
                      							current,
                      							"enableState",
                      							lv_enableState_11_0,
                      							"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.EnableStateKind");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalCodeExecutionSpec.g:824:4: ( (lv_liveState_12_0= ruleLiveStateKind ) )
                    {
                    // InternalCodeExecutionSpec.g:824:4: ( (lv_liveState_12_0= ruleLiveStateKind ) )
                    // InternalCodeExecutionSpec.g:825:5: (lv_liveState_12_0= ruleLiveStateKind )
                    {
                    // InternalCodeExecutionSpec.g:825:5: (lv_liveState_12_0= ruleLiveStateKind )
                    // InternalCodeExecutionSpec.g:826:6: lv_liveState_12_0= ruleLiveStateKind
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getClockBehaviorAccess().getLiveStateLiveStateKindParserRuleCall_5_2_0());
                      					
                    }
                    pushFollow(FollowSets000.FOLLOW_19);
                    lv_liveState_12_0=ruleLiveStateKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getClockBehaviorRule());
                      						}
                      						add(
                      							current,
                      							"liveState",
                      							lv_liveState_12_0,
                      							"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.LiveStateKind");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalCodeExecutionSpec.g:844:3: (otherlv_13= 'or' ( ( (lv_fireState_14_0= ruleFiredStateKind ) ) | ( (lv_enableState_15_0= ruleEnableStateKind ) ) | ( (lv_liveState_16_0= ruleLiveStateKind ) ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==28) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalCodeExecutionSpec.g:845:4: otherlv_13= 'or' ( ( (lv_fireState_14_0= ruleFiredStateKind ) ) | ( (lv_enableState_15_0= ruleEnableStateKind ) ) | ( (lv_liveState_16_0= ruleLiveStateKind ) ) )
            	    {
            	    otherlv_13=(Token)match(input,28,FollowSets000.FOLLOW_18); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_13, grammarAccess.getClockBehaviorAccess().getOrKeyword_6_0());
            	      			
            	    }
            	    // InternalCodeExecutionSpec.g:849:4: ( ( (lv_fireState_14_0= ruleFiredStateKind ) ) | ( (lv_enableState_15_0= ruleEnableStateKind ) ) | ( (lv_liveState_16_0= ruleLiveStateKind ) ) )
            	    int alt15=3;
            	    switch ( input.LA(1) ) {
            	    case 33:
            	    case 34:
            	        {
            	        alt15=1;
            	        }
            	        break;
            	    case 35:
            	    case 36:
            	    case 37:
            	    case 38:
            	        {
            	        alt15=2;
            	        }
            	        break;
            	    case 39:
            	    case 40:
            	        {
            	        alt15=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 15, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt15) {
            	        case 1 :
            	            // InternalCodeExecutionSpec.g:850:5: ( (lv_fireState_14_0= ruleFiredStateKind ) )
            	            {
            	            // InternalCodeExecutionSpec.g:850:5: ( (lv_fireState_14_0= ruleFiredStateKind ) )
            	            // InternalCodeExecutionSpec.g:851:6: (lv_fireState_14_0= ruleFiredStateKind )
            	            {
            	            // InternalCodeExecutionSpec.g:851:6: (lv_fireState_14_0= ruleFiredStateKind )
            	            // InternalCodeExecutionSpec.g:852:7: lv_fireState_14_0= ruleFiredStateKind
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getClockBehaviorAccess().getFireStateFiredStateKindParserRuleCall_6_1_0_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_19);
            	            lv_fireState_14_0=ruleFiredStateKind();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getClockBehaviorRule());
            	              							}
            	              							add(
            	              								current,
            	              								"fireState",
            	              								lv_fireState_14_0,
            	              								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.FiredStateKind");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalCodeExecutionSpec.g:870:5: ( (lv_enableState_15_0= ruleEnableStateKind ) )
            	            {
            	            // InternalCodeExecutionSpec.g:870:5: ( (lv_enableState_15_0= ruleEnableStateKind ) )
            	            // InternalCodeExecutionSpec.g:871:6: (lv_enableState_15_0= ruleEnableStateKind )
            	            {
            	            // InternalCodeExecutionSpec.g:871:6: (lv_enableState_15_0= ruleEnableStateKind )
            	            // InternalCodeExecutionSpec.g:872:7: lv_enableState_15_0= ruleEnableStateKind
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getClockBehaviorAccess().getEnableStateEnableStateKindParserRuleCall_6_1_1_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_19);
            	            lv_enableState_15_0=ruleEnableStateKind();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getClockBehaviorRule());
            	              							}
            	              							add(
            	              								current,
            	              								"enableState",
            	              								lv_enableState_15_0,
            	              								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.EnableStateKind");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 3 :
            	            // InternalCodeExecutionSpec.g:890:5: ( (lv_liveState_16_0= ruleLiveStateKind ) )
            	            {
            	            // InternalCodeExecutionSpec.g:890:5: ( (lv_liveState_16_0= ruleLiveStateKind ) )
            	            // InternalCodeExecutionSpec.g:891:6: (lv_liveState_16_0= ruleLiveStateKind )
            	            {
            	            // InternalCodeExecutionSpec.g:891:6: (lv_liveState_16_0= ruleLiveStateKind )
            	            // InternalCodeExecutionSpec.g:892:7: lv_liveState_16_0= ruleLiveStateKind
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getClockBehaviorAccess().getLiveStateLiveStateKindParserRuleCall_6_1_2_0());
            	              						
            	            }
            	            pushFollow(FollowSets000.FOLLOW_19);
            	            lv_liveState_16_0=ruleLiveStateKind();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getClockBehaviorRule());
            	              							}
            	              							add(
            	              								current,
            	              								"liveState",
            	              								lv_liveState_16_0,
            	              								"fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionSpec.LiveStateKind");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            // InternalCodeExecutionSpec.g:911:3: (otherlv_17= '->force' otherlv_18= '(' ( (otherlv_19= RULE_ID ) ) (otherlv_20= ',' ( (otherlv_21= RULE_ID ) ) )* otherlv_22= ')' )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==29) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalCodeExecutionSpec.g:912:4: otherlv_17= '->force' otherlv_18= '(' ( (otherlv_19= RULE_ID ) ) (otherlv_20= ',' ( (otherlv_21= RULE_ID ) ) )* otherlv_22= ')'
                    {
                    otherlv_17=(Token)match(input,29,FollowSets000.FOLLOW_20); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_17, grammarAccess.getClockBehaviorAccess().getForceKeyword_7_0());
                      			
                    }
                    otherlv_18=(Token)match(input,30,FollowSets000.FOLLOW_10); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_18, grammarAccess.getClockBehaviorAccess().getLeftParenthesisKeyword_7_1());
                      			
                    }
                    // InternalCodeExecutionSpec.g:920:4: ( (otherlv_19= RULE_ID ) )
                    // InternalCodeExecutionSpec.g:921:5: (otherlv_19= RULE_ID )
                    {
                    // InternalCodeExecutionSpec.g:921:5: (otherlv_19= RULE_ID )
                    // InternalCodeExecutionSpec.g:922:6: otherlv_19= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getClockBehaviorRule());
                      						}
                      					
                    }
                    otherlv_19=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_19, grammarAccess.getClockBehaviorAccess().getClockToForceClockCrossReference_7_2_0());
                      					
                    }

                    }


                    }

                    // InternalCodeExecutionSpec.g:936:4: (otherlv_20= ',' ( (otherlv_21= RULE_ID ) ) )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==31) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // InternalCodeExecutionSpec.g:937:5: otherlv_20= ',' ( (otherlv_21= RULE_ID ) )
                    	    {
                    	    otherlv_20=(Token)match(input,31,FollowSets000.FOLLOW_10); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_20, grammarAccess.getClockBehaviorAccess().getCommaKeyword_7_3_0());
                    	      				
                    	    }
                    	    // InternalCodeExecutionSpec.g:941:5: ( (otherlv_21= RULE_ID ) )
                    	    // InternalCodeExecutionSpec.g:942:6: (otherlv_21= RULE_ID )
                    	    {
                    	    // InternalCodeExecutionSpec.g:942:6: (otherlv_21= RULE_ID )
                    	    // InternalCodeExecutionSpec.g:943:7: otherlv_21= RULE_ID
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							/* */
                    	      						
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElement(grammarAccess.getClockBehaviorRule());
                    	      							}
                    	      						
                    	    }
                    	    otherlv_21=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							newLeafNode(otherlv_21, grammarAccess.getClockBehaviorAccess().getClockToForceClockCrossReference_7_3_1_0());
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);

                    otherlv_22=(Token)match(input,32,FollowSets000.FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_22, grammarAccess.getClockBehaviorAccess().getRightParenthesisKeyword_7_4());
                      			
                    }

                    }
                    break;

            }

            otherlv_23=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_23, grammarAccess.getClockBehaviorAccess().getSemicolonKeyword_8());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClockBehavior"


    // $ANTLR start "entryRuleEString"
    // InternalCodeExecutionSpec.g:971:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalCodeExecutionSpec.g:971:47: (iv_ruleEString= ruleEString EOF )
            // InternalCodeExecutionSpec.g:972:2: iv_ruleEString= ruleEString EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEStringRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEString.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalCodeExecutionSpec.g:978:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:984:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalCodeExecutionSpec.g:985:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalCodeExecutionSpec.g:985:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==RULE_STRING) ) {
                alt19=1;
            }
            else if ( (LA19_0==RULE_ID) ) {
                alt19=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalCodeExecutionSpec.g:986:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_STRING_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:994:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ID_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleFiredStateKind"
    // InternalCodeExecutionSpec.g:1005:1: entryRuleFiredStateKind returns [String current=null] : iv_ruleFiredStateKind= ruleFiredStateKind EOF ;
    public final String entryRuleFiredStateKind() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFiredStateKind = null;


        try {
            // InternalCodeExecutionSpec.g:1005:54: (iv_ruleFiredStateKind= ruleFiredStateKind EOF )
            // InternalCodeExecutionSpec.g:1006:2: iv_ruleFiredStateKind= ruleFiredStateKind EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFiredStateKindRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFiredStateKind=ruleFiredStateKind();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFiredStateKind.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFiredStateKind"


    // $ANTLR start "ruleFiredStateKind"
    // InternalCodeExecutionSpec.g:1012:1: ruleFiredStateKind returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'ticks' | kw= 'doesnot_tick' ) ;
    public final AntlrDatatypeRuleToken ruleFiredStateKind() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:1018:2: ( (kw= 'ticks' | kw= 'doesnot_tick' ) )
            // InternalCodeExecutionSpec.g:1019:2: (kw= 'ticks' | kw= 'doesnot_tick' )
            {
            // InternalCodeExecutionSpec.g:1019:2: (kw= 'ticks' | kw= 'doesnot_tick' )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==33) ) {
                alt20=1;
            }
            else if ( (LA20_0==34) ) {
                alt20=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalCodeExecutionSpec.g:1020:3: kw= 'ticks'
                    {
                    kw=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getFiredStateKindAccess().getTicksKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:1026:3: kw= 'doesnot_tick'
                    {
                    kw=(Token)match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getFiredStateKindAccess().getDoesnot_tickKeyword_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFiredStateKind"


    // $ANTLR start "entryRuleEnableStateKind"
    // InternalCodeExecutionSpec.g:1035:1: entryRuleEnableStateKind returns [String current=null] : iv_ruleEnableStateKind= ruleEnableStateKind EOF ;
    public final String entryRuleEnableStateKind() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEnableStateKind = null;


        try {
            // InternalCodeExecutionSpec.g:1035:55: (iv_ruleEnableStateKind= ruleEnableStateKind EOF )
            // InternalCodeExecutionSpec.g:1036:2: iv_ruleEnableStateKind= ruleEnableStateKind EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnableStateKindRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnableStateKind=ruleEnableStateKind();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnableStateKind.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnableStateKind"


    // $ANTLR start "ruleEnableStateKind"
    // InternalCodeExecutionSpec.g:1042:1: ruleEnableStateKind returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'must_tick' | kw= 'cannot_tick' | kw= 'is_free' | kw= 'is_undetermined' ) ;
    public final AntlrDatatypeRuleToken ruleEnableStateKind() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:1048:2: ( (kw= 'must_tick' | kw= 'cannot_tick' | kw= 'is_free' | kw= 'is_undetermined' ) )
            // InternalCodeExecutionSpec.g:1049:2: (kw= 'must_tick' | kw= 'cannot_tick' | kw= 'is_free' | kw= 'is_undetermined' )
            {
            // InternalCodeExecutionSpec.g:1049:2: (kw= 'must_tick' | kw= 'cannot_tick' | kw= 'is_free' | kw= 'is_undetermined' )
            int alt21=4;
            switch ( input.LA(1) ) {
            case 35:
                {
                alt21=1;
                }
                break;
            case 36:
                {
                alt21=2;
                }
                break;
            case 37:
                {
                alt21=3;
                }
                break;
            case 38:
                {
                alt21=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }

            switch (alt21) {
                case 1 :
                    // InternalCodeExecutionSpec.g:1050:3: kw= 'must_tick'
                    {
                    kw=(Token)match(input,35,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getEnableStateKindAccess().getMust_tickKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:1056:3: kw= 'cannot_tick'
                    {
                    kw=(Token)match(input,36,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getEnableStateKindAccess().getCannot_tickKeyword_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalCodeExecutionSpec.g:1062:3: kw= 'is_free'
                    {
                    kw=(Token)match(input,37,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getEnableStateKindAccess().getIs_freeKeyword_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalCodeExecutionSpec.g:1068:3: kw= 'is_undetermined'
                    {
                    kw=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getEnableStateKindAccess().getIs_undeterminedKeyword_3());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnableStateKind"


    // $ANTLR start "entryRuleLiveStateKind"
    // InternalCodeExecutionSpec.g:1077:1: entryRuleLiveStateKind returns [String current=null] : iv_ruleLiveStateKind= ruleLiveStateKind EOF ;
    public final String entryRuleLiveStateKind() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleLiveStateKind = null;


        try {
            // InternalCodeExecutionSpec.g:1077:53: (iv_ruleLiveStateKind= ruleLiveStateKind EOF )
            // InternalCodeExecutionSpec.g:1078:2: iv_ruleLiveStateKind= ruleLiveStateKind EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiveStateKindRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLiveStateKind=ruleLiveStateKind();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiveStateKind.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiveStateKind"


    // $ANTLR start "ruleLiveStateKind"
    // InternalCodeExecutionSpec.g:1084:1: ruleLiveStateKind returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'isAlive' | kw= 'isDead' ) ;
    public final AntlrDatatypeRuleToken ruleLiveStateKind() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:1090:2: ( (kw= 'isAlive' | kw= 'isDead' ) )
            // InternalCodeExecutionSpec.g:1091:2: (kw= 'isAlive' | kw= 'isDead' )
            {
            // InternalCodeExecutionSpec.g:1091:2: (kw= 'isAlive' | kw= 'isDead' )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==39) ) {
                alt22=1;
            }
            else if ( (LA22_0==40) ) {
                alt22=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalCodeExecutionSpec.g:1092:3: kw= 'isAlive'
                    {
                    kw=(Token)match(input,39,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getLiveStateKindAccess().getIsAliveKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:1098:3: kw= 'isDead'
                    {
                    kw=(Token)match(input,40,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getLiveStateKindAccess().getIsDeadKeyword_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiveStateKind"


    // $ANTLR start "entryRuleAssertionBehavior"
    // InternalCodeExecutionSpec.g:1107:1: entryRuleAssertionBehavior returns [EObject current=null] : iv_ruleAssertionBehavior= ruleAssertionBehavior EOF ;
    public final EObject entryRuleAssertionBehavior() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssertionBehavior = null;


        try {
            // InternalCodeExecutionSpec.g:1107:58: (iv_ruleAssertionBehavior= ruleAssertionBehavior EOF )
            // InternalCodeExecutionSpec.g:1108:2: iv_ruleAssertionBehavior= ruleAssertionBehavior EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssertionBehaviorRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAssertionBehavior=ruleAssertionBehavior();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssertionBehavior; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssertionBehavior"


    // $ANTLR start "ruleAssertionBehavior"
    // InternalCodeExecutionSpec.g:1114:1: ruleAssertionBehavior returns [EObject current=null] : (otherlv_0= 'AssertionBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )? ) | ( ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )? ) ) otherlv_16= ';' ) ;
    public final EObject ruleAssertionBehavior() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token lv_failed_10_0=null;
        Token otherlv_11=null;
        Token lv_satisfied_12_0=null;
        Token lv_satisfied_13_0=null;
        Token otherlv_14=null;
        Token lv_failed_15_0=null;
        Token otherlv_16=null;


        	enterRule();

        try {
            // InternalCodeExecutionSpec.g:1120:2: ( (otherlv_0= 'AssertionBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )? ) | ( ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )? ) ) otherlv_16= ';' ) )
            // InternalCodeExecutionSpec.g:1121:2: (otherlv_0= 'AssertionBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )? ) | ( ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )? ) ) otherlv_16= ';' )
            {
            // InternalCodeExecutionSpec.g:1121:2: (otherlv_0= 'AssertionBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )? ) | ( ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )? ) ) otherlv_16= ';' )
            // InternalCodeExecutionSpec.g:1122:3: otherlv_0= 'AssertionBehavior' () ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? ) otherlv_8= 'when' ( (otherlv_9= RULE_ID ) ) ( ( ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )? ) | ( ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )? ) ) otherlv_16= ';'
            {
            otherlv_0=(Token)match(input,41,FollowSets000.FOLLOW_15); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getAssertionBehaviorAccess().getAssertionBehaviorKeyword_0());
              		
            }
            // InternalCodeExecutionSpec.g:1126:3: ()
            // InternalCodeExecutionSpec.g:1127:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getAssertionBehaviorAccess().getCodeExecAssertionBehaviorAction_1(),
              					current);
              			
            }

            }

            // InternalCodeExecutionSpec.g:1136:3: ( (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )? )
            // InternalCodeExecutionSpec.g:1137:4: (otherlv_2= 'execute' | otherlv_3= 'call' ) ( ( ruleEString ) ) otherlv_5= '.' ( (otherlv_6= RULE_ID ) ) (otherlv_7= '()' )?
            {
            // InternalCodeExecutionSpec.g:1137:4: (otherlv_2= 'execute' | otherlv_3= 'call' )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==24) ) {
                alt23=1;
            }
            else if ( (LA23_0==25) ) {
                alt23=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalCodeExecutionSpec.g:1138:5: otherlv_2= 'execute'
                    {
                    otherlv_2=(Token)match(input,24,FollowSets000.FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_2, grammarAccess.getAssertionBehaviorAccess().getExecuteKeyword_2_0_0());
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:1143:5: otherlv_3= 'call'
                    {
                    otherlv_3=(Token)match(input,25,FollowSets000.FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_3, grammarAccess.getAssertionBehaviorAccess().getCallKeyword_2_0_1());
                      				
                    }

                    }
                    break;

            }

            // InternalCodeExecutionSpec.g:1148:4: ( ( ruleEString ) )
            // InternalCodeExecutionSpec.g:1149:5: ( ruleEString )
            {
            // InternalCodeExecutionSpec.g:1149:5: ( ruleEString )
            // InternalCodeExecutionSpec.g:1150:6: ruleEString
            {
            if ( state.backtracking==0 ) {

              						/* */
              					
            }
            if ( state.backtracking==0 ) {

              						if (current==null) {
              							current = createModelElement(grammarAccess.getAssertionBehaviorRule());
              						}
              					
            }
            if ( state.backtracking==0 ) {

              						newCompositeNode(grammarAccess.getAssertionBehaviorAccess().getObjectVariableObjectVariableCrossReference_2_1_0());
              					
            }
            pushFollow(FollowSets000.FOLLOW_12);
            ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              						afterParserOrEnumRuleCall();
              					
            }

            }


            }

            otherlv_5=(Token)match(input,21,FollowSets000.FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_5, grammarAccess.getAssertionBehaviorAccess().getFullStopKeyword_2_2());
              			
            }
            // InternalCodeExecutionSpec.g:1171:4: ( (otherlv_6= RULE_ID ) )
            // InternalCodeExecutionSpec.g:1172:5: (otherlv_6= RULE_ID )
            {
            // InternalCodeExecutionSpec.g:1172:5: (otherlv_6= RULE_ID )
            // InternalCodeExecutionSpec.g:1173:6: otherlv_6= RULE_ID
            {
            if ( state.backtracking==0 ) {

              						/* */
              					
            }
            if ( state.backtracking==0 ) {

              						if (current==null) {
              							current = createModelElement(grammarAccess.getAssertionBehaviorRule());
              						}
              					
            }
            otherlv_6=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_16); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              						newLeafNode(otherlv_6, grammarAccess.getAssertionBehaviorAccess().getMethodJvmOperationCrossReference_2_3_0());
              					
            }

            }


            }

            // InternalCodeExecutionSpec.g:1187:4: (otherlv_7= '()' )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==26) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalCodeExecutionSpec.g:1188:5: otherlv_7= '()'
                    {
                    otherlv_7=(Token)match(input,26,FollowSets000.FOLLOW_17); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_7, grammarAccess.getAssertionBehaviorAccess().getLeftParenthesisRightParenthesisKeyword_2_4());
                      				
                    }

                    }
                    break;

            }


            }

            otherlv_8=(Token)match(input,27,FollowSets000.FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getAssertionBehaviorAccess().getWhenKeyword_3());
              		
            }
            // InternalCodeExecutionSpec.g:1198:3: ( (otherlv_9= RULE_ID ) )
            // InternalCodeExecutionSpec.g:1199:4: (otherlv_9= RULE_ID )
            {
            // InternalCodeExecutionSpec.g:1199:4: (otherlv_9= RULE_ID )
            // InternalCodeExecutionSpec.g:1200:5: otherlv_9= RULE_ID
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getAssertionBehaviorRule());
              					}
              				
            }
            otherlv_9=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(otherlv_9, grammarAccess.getAssertionBehaviorAccess().getAssertionRelationCrossReference_4_0());
              				
            }

            }


            }

            // InternalCodeExecutionSpec.g:1214:3: ( ( ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )? ) | ( ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )? ) )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==42) ) {
                alt27=1;
            }
            else if ( (LA27_0==43) ) {
                alt27=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // InternalCodeExecutionSpec.g:1215:4: ( ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )? )
                    {
                    // InternalCodeExecutionSpec.g:1215:4: ( ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )? )
                    // InternalCodeExecutionSpec.g:1216:5: ( (lv_failed_10_0= 'is_violated' ) ) (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )?
                    {
                    // InternalCodeExecutionSpec.g:1216:5: ( (lv_failed_10_0= 'is_violated' ) )
                    // InternalCodeExecutionSpec.g:1217:6: (lv_failed_10_0= 'is_violated' )
                    {
                    // InternalCodeExecutionSpec.g:1217:6: (lv_failed_10_0= 'is_violated' )
                    // InternalCodeExecutionSpec.g:1218:7: lv_failed_10_0= 'is_violated'
                    {
                    lv_failed_10_0=(Token)match(input,42,FollowSets000.FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							newLeafNode(lv_failed_10_0, grammarAccess.getAssertionBehaviorAccess().getFailedIs_violatedKeyword_5_0_0_0());
                      						
                    }
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElement(grammarAccess.getAssertionBehaviorRule());
                      							}
                      							setWithLastConsumed(current, "failed", lv_failed_10_0 != null, "is_violated");
                      						
                    }

                    }


                    }

                    // InternalCodeExecutionSpec.g:1230:5: (otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) ) )?
                    int alt25=2;
                    int LA25_0 = input.LA(1);

                    if ( (LA25_0==28) ) {
                        alt25=1;
                    }
                    switch (alt25) {
                        case 1 :
                            // InternalCodeExecutionSpec.g:1231:6: otherlv_11= 'or' ( (lv_satisfied_12_0= 'is_satisfied' ) )
                            {
                            otherlv_11=(Token)match(input,28,FollowSets000.FOLLOW_24); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						newLeafNode(otherlv_11, grammarAccess.getAssertionBehaviorAccess().getOrKeyword_5_0_1_0());
                              					
                            }
                            // InternalCodeExecutionSpec.g:1235:6: ( (lv_satisfied_12_0= 'is_satisfied' ) )
                            // InternalCodeExecutionSpec.g:1236:7: (lv_satisfied_12_0= 'is_satisfied' )
                            {
                            // InternalCodeExecutionSpec.g:1236:7: (lv_satisfied_12_0= 'is_satisfied' )
                            // InternalCodeExecutionSpec.g:1237:8: lv_satisfied_12_0= 'is_satisfied'
                            {
                            lv_satisfied_12_0=(Token)match(input,43,FollowSets000.FOLLOW_9); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								newLeafNode(lv_satisfied_12_0, grammarAccess.getAssertionBehaviorAccess().getSatisfiedIs_satisfiedKeyword_5_0_1_1_0());
                              							
                            }
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElement(grammarAccess.getAssertionBehaviorRule());
                              								}
                              								setWithLastConsumed(current, "satisfied", lv_satisfied_12_0 != null, "is_satisfied");
                              							
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCodeExecutionSpec.g:1252:4: ( ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )? )
                    {
                    // InternalCodeExecutionSpec.g:1252:4: ( ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )? )
                    // InternalCodeExecutionSpec.g:1253:5: ( (lv_satisfied_13_0= 'is_satisfied' ) ) (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )?
                    {
                    // InternalCodeExecutionSpec.g:1253:5: ( (lv_satisfied_13_0= 'is_satisfied' ) )
                    // InternalCodeExecutionSpec.g:1254:6: (lv_satisfied_13_0= 'is_satisfied' )
                    {
                    // InternalCodeExecutionSpec.g:1254:6: (lv_satisfied_13_0= 'is_satisfied' )
                    // InternalCodeExecutionSpec.g:1255:7: lv_satisfied_13_0= 'is_satisfied'
                    {
                    lv_satisfied_13_0=(Token)match(input,43,FollowSets000.FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							newLeafNode(lv_satisfied_13_0, grammarAccess.getAssertionBehaviorAccess().getSatisfiedIs_satisfiedKeyword_5_1_0_0());
                      						
                    }
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElement(grammarAccess.getAssertionBehaviorRule());
                      							}
                      							setWithLastConsumed(current, "satisfied", lv_satisfied_13_0 != null, "is_satisfied");
                      						
                    }

                    }


                    }

                    // InternalCodeExecutionSpec.g:1267:5: (otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) ) )?
                    int alt26=2;
                    int LA26_0 = input.LA(1);

                    if ( (LA26_0==28) ) {
                        alt26=1;
                    }
                    switch (alt26) {
                        case 1 :
                            // InternalCodeExecutionSpec.g:1268:6: otherlv_14= 'or' ( (lv_failed_15_0= 'is_violated' ) )
                            {
                            otherlv_14=(Token)match(input,28,FollowSets000.FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						newLeafNode(otherlv_14, grammarAccess.getAssertionBehaviorAccess().getOrKeyword_5_1_1_0());
                              					
                            }
                            // InternalCodeExecutionSpec.g:1272:6: ( (lv_failed_15_0= 'is_violated' ) )
                            // InternalCodeExecutionSpec.g:1273:7: (lv_failed_15_0= 'is_violated' )
                            {
                            // InternalCodeExecutionSpec.g:1273:7: (lv_failed_15_0= 'is_violated' )
                            // InternalCodeExecutionSpec.g:1274:8: lv_failed_15_0= 'is_violated'
                            {
                            lv_failed_15_0=(Token)match(input,42,FollowSets000.FOLLOW_9); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								newLeafNode(lv_failed_15_0, grammarAccess.getAssertionBehaviorAccess().getFailedIs_violatedKeyword_5_1_1_1_0());
                              							
                            }
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElement(grammarAccess.getAssertionBehaviorRule());
                              								}
                              								setWithLastConsumed(current, "failed", lv_failed_15_0 != null, "is_violated");
                              							
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            otherlv_16=(Token)match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_16, grammarAccess.getAssertionBehaviorAccess().getSemicolonKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssertionBehavior"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x00000200008E6000L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000020000882000L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000020000802000L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000018000L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000003000000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000000C000000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000008000000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000001FE00000000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000030008000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040000000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000180000000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x00000C0000000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000010008000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000080000000000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000040000000000L});
    }


}