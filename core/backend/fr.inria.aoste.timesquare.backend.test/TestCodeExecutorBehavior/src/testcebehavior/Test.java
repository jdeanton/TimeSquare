/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package testcebehavior;

import fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionHelper;
import fr.inria.aoste.timesquare.backend.codeexecution.ICodeExecutionAPI;
import fr.inria.aoste.timesquare.utils.console.ErrorConsole;

public class Test implements ICodeExecutionAPI {

	CodeExecutionHelper _ce=null;
	int n=0;
	public Test() {
		super();
		ErrorConsole.printWhere(8, "Create test");
		
	}

	public void run()
	{
		n++;
		System.out.println("Running :"+ n);
		ErrorConsole.printWhere( "Running Behavior :"+n);		
		if (_ce!=null)
		{
			_ce.println( "Running Behavior :"+n);
			_ce.putData("int", Integer.valueOf(n));
		}		
	}

	@Override
	public void setHelper(CodeExecutionHelper ce) {
		_ce=ce;		
	}
	
	
}
