/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.test.behavior;

import java.util.ArrayList;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.DeleteHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;

public class TestBehaviorManager extends BehaviorManager {
	private ArrayList<TestBehavior> _behaviorList;
		
	public void addbehavior(TestBehavior behavior) 
	{
		_behaviorList.add(behavior);		
	}
	
	@Override
	public String getPluginName() {
	
		return "testBehaviormanager";
	}

	@Override
	public void clear() {		
		_behaviorList.clear();
	}	

	@Override
	public void init(ConfigurationHelper helper) {
		super.init(helper);
		_behaviorList = new ArrayList<TestBehavior>();	
	}

	@Override
	public void deleteEntity(Entity entity,DeleteHelper deletehelper) {
		_behaviorList.remove(entity.getBehavior());		
	}

	/**
	 * @return Error Message is no behavior is defined, null otherwise
	 */
	@Override
	public String validate() {	
		if (_behaviorList.size()==0)
		{
			return "No Behavior";
		}
		return super.validate();
	}


	@Override
	public void manageBehavior(ConfigurationHelper helper) {
		 for (ClockEntity ce : helper.getClocks())
	        {
			 TestBehavior tb=  new TestBehavior(this);
			 _behaviorList.add(tb);
	            helper.addBehavior(ce, helper.getTicksState(),
	                    this.getPluginName(),tb,  new TestPersitenceOption());
	        }
	}

	public boolean createBehavior(EObject clock, ConfigurationHelper helper) {
		 for (ClockEntity ce : helper.getClocks())
	        {
			 if( ce.getModelElementReference()==clock)
			 {
				 TestBehavior tb=  new TestBehavior(this);
				 _behaviorList.add(tb);
	            helper.addBehavior(ce, helper.getTicksState(),
	                    this.getPluginName(), tb,  new TestPersitenceOption());
	            return true;
			 }
	        }
		return false;
	}

	@Override
	public ClockBehavior redoClockBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {
		
		if (persistentOptions instanceof TestPersitenceOption)
        {
			//TestPersitenceOption options = (TestPersitenceOption) persistentOptions;          
               
                TestBehavior tb=  new TestBehavior(this);
				 _behaviorList.add(tb);
				 return tb;
        }
		return null;
	}

	@Override
	public RelationBehavior redoRelationBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {
		
		return null;
	}

	@Override
	public ClockBehavior redoAssertBehavior(ConfigurationHelper helper,
			PersistentOptions persistentOptions) {
		
		return null;
	}

	@Override
	public void beforeExecution(ConfigurationHelper helper, IPath folderin,
			String namefilein, ISolverForBackend solver) {
		
	}

	@Override
	public void end(ConfigurationHelper helper) {
		
	}	

	@Override
	public void aPostNewStep(int step) {
		if( cptTickinstep>cptMaxTickByStep)
			cptMaxTickByStep=cptTickinstep;
		cptTickinstep=0;
	}


	@Override
	public BehaviorConfigurator<TestBehaviorManager> getConfigurator(
			ConfigurationHelper configurationHelper) {
	
		return new TestBehaviorConfigurator(configurationHelper, this);
	}

	int cptTick=0;
	int cptTickinstep=0;
	int cptMaxTickByStep=0;
	
	
	
	public int getMaxTickByStep() {
		return cptMaxTickByStep;
	}


	@Override
	public void repeatStep(int step) {
		if( cptTickinstep>cptMaxTickByStep)
			cptMaxTickByStep=cptTickinstep;
		cptTickinstep=0;
	}

	public int getTick() {
		return cptTick;
	}

	public void incTick() {
		cptTick++;	
		cptTickinstep++;
	}
}
