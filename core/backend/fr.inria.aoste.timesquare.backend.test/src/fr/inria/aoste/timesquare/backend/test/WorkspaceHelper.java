/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.test;

import static org.junit.Assert.fail;

import java.net.URL;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;

import fr.inria.aoste.timesquare.utils.console.ErrorConsole;
import fr.inria.aoste.timesquare.utils.pluginhelpers.PluginHelpers;

public class WorkspaceHelper {

	/**
	 * test if your running in eclipse and non UI thread
	 * @throws Exception
	 */
	public static void preConditionTest() throws Exception
	{
		PlatformUI.getWorkbench(); // throws IllegalStateException (if not plugin test)
		if (Display.getCurrent() != null) {
			fail("Runnning on UI Thread");
		}
		
	}
	
	public static final class CloseWelcomeView implements Runnable {
		@Override
		public void run() {
			try {
				IViewPart ivp = null;// PluginHelpers.getShowView("org.eclipse.ui.internal.introview");
				for (IViewReference ivr : PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.getViewReferences()) {
					if ("org.eclipse.ui.internal.introview".equals(ivr.getId())) {
						ivp = ivr.getView(false);

					}
				}
				if (ivp != null)
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage().hideView(ivp);
			} catch (Exception e) {

			}

		}
	}
	
	


	public final static String CodeExecutorProject = "TestCodeExecutorBehavior";
	
	public final static String testPapyrus="testPapyrus";
	
	/***
	 *  Import <B>TestCodeExecutorBehavior</B> Project into workspace 
	 */	
	public static void createTestCodeExecutorBehaviorProject() {
		try {
			IProject ip = getProjectHandle(CodeExecutorProject);
			ip.create(null);
			URL url = PluginHelpers.getInstallUrl(Activator.getDefault()
					.getBundle(), CodeExecutorProject);
			IPath ph = ResourcesPlugin.getWorkspace().getRoot().getLocation();
			IFileStore destination = PluginHelpers.getIFileStore(ph);
			destination = destination.getChild(CodeExecutorProject);
			destination.mkdir(EFS.OVERWRITE, null);
			EFS.getLocalFileSystem().getStore(url.toURI()).copy(destination, EFS.OVERWRITE, null);			
			ip.open(null);
			ip.refreshLocal(IResource.DEPTH_INFINITE, null);
			ip.build(IncrementalProjectBuilder.FULL_BUILD, null);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e);
		}
	}

	static public IProject getProjectHandle(String name) {
		return ResourcesPlugin.getWorkspace().getRoot().getProject(name);
	}
	
	
	static public void workspaceRefresh() throws CoreException {
		 ResourcesPlugin.getWorkspace().getRoot().refreshLocal(IResource.DEPTH_INFINITE, null);	
	}

	public static void closeWelcome() {
		Display.getDefault().syncExec(new CloseWelcomeView());
	}

	
	/****
	 *  create <B>"Test"</B> project with a file : <i>alternates.ccsl</i>
	 *  
	 */
	public static void createTestProject() {
		IProject project = null;
		IFile example = null;
		try {
			project = WorkspaceHelper.getProjectHandle("test");
			if (!project.exists())
				project.create(null);
			project.open(null);
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
			// copy example file
			copyfileCCSL(project, "alternates.ccsl");
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
			example = project.getFile("alternates.ccsl");
			example.refreshLocal(IResource.DEPTH_INFINITE, null);
			copyfileCCSL(project, "deadlock.ccsl");
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
			example = project.getFile("deadlock.ccsl");
			example.refreshLocal(IResource.DEPTH_INFINITE, null);
			URL url = PluginHelpers.getInstallUrl(Activator.getDefault().getBundle(), testPapyrus);
			
			IFileStore destination = PluginHelpers.getIFileStore(project.getLocation());
			destination = destination.getChild(testPapyrus);
			destination.mkdir(EFS.OVERWRITE, null);
			EFS.getLocalFileSystem().getStore(url.toURI()).copy(destination, EFS.OVERWRITE, null);			
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void copyfileCCSL(IProject ip, String s) throws Exception {

		ErrorConsole.println("" + s + " : ");
		URL url = PluginHelpers.getInstallUrl(Activator.getDefault().getBundle(), s);
		IPath ph = ip.getLocation().append(s);
		PluginHelpers.copy(PluginHelpers.getIFileStore(url),
				PluginHelpers.getIFileStore(ph), EFS.OVERWRITE, null);
		// generateModelCCSL
		ErrorConsole.printOKln("\tCopy " + s + " : [OK] ");
		ErrorConsole.printOKln("\tGenerate model from  " + s + " : [OK] ");

	}
	
	
	public static void copyfileCCSL(Bundle bundle,String source, IProject ip,  String dest) throws Exception {
		URL url = PluginHelpers.getInstallUrl(bundle, source);
		IPath ph = ip.getLocation().append(dest);
		PluginHelpers.copy(PluginHelpers.getIFileStore(url), PluginHelpers.getIFileStore(ph), EFS.OVERWRITE, null);
	}
	
	

}
