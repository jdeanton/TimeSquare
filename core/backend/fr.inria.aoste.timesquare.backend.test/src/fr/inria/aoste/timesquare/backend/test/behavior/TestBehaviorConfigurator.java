/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.test.behavior;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;

public class TestBehaviorConfigurator extends BehaviorConfigurator<TestBehaviorManager> {

	public TestBehaviorConfigurator(ConfigurationHelper _ch,
			TestBehaviorManager _behaviorManager) {
		super(_ch, _behaviorManager);
		
	}

//	REMOVED BECAUSE NEVER CALLED
	//	@Override
//	public void autoConfiguration() {
//		System.out.println("Configuration Behavior: autoConfiguration");
//		getBehaviorManager().manageBehavior(getConfigurationHelper());
//	}

	public void createBehaviorforClock(EObject clock) {
		System.out.println("Configuration Behavior : createBehaviorforClock");
		boolean b=getBehaviorManager().createBehavior(clock ,getConfigurationHelper());
		if( !b)
			throw new IllegalArgumentException( "Clock not found");
	}	
	
	
/*	public int getMaxTickByStep() {
		return getBehaviorManager().getMaxTickByStep();
	}


	

	

	public int getTick() {
		return getBehaviorManager().getTick();
	}*/

}
