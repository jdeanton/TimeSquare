/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.test.behavior;

import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

public class TestPersitenceOption implements PersistentOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1428190635458074451L;

	@Override
	public String getDescription() {	
		return "Description 2";
	}

}
