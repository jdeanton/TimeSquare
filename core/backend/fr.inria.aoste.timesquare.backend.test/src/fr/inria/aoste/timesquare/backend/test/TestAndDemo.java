/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
//package fr.inria.aoste.timesquare.backend.test;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//import java.util.HashMap;
//
//import org.eclipse.core.resources.IFile;
//import org.eclipse.core.resources.IProject;
//import org.eclipse.emf.ecore.EObject;
//import org.eclipse.jdt.core.IJavaElement;
//import org.eclipse.jdt.core.IMethod;
//import org.eclipse.jdt.core.JavaCore;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Ignore;
//import org.junit.Test;
//
//import fr.inria.aoste.timesquare.backend.codeexecution.manager.CodeExecutionManager;
//import fr.inria.aoste.timesquare.backend.codeexecution.manager.CodeExecutorConfigurator;
//import fr.inria.aoste.timesquare.backend.papyrusmodelanimator.PapyrusAnimatorBehaviorConfigurator;
//import fr.inria.aoste.timesquare.backend.papyrusmodelanimator.PapyrusAnimatorBehaviorManager;
//import fr.inria.aoste.timesquare.backend.power.behavior.PowerBehaviorManager;
//import fr.inria.aoste.timesquare.backend.power.behavior.PowerConfiguration;
//import fr.inria.aoste.timesquare.backend.test.behavior.TestBehaviorConfigurator;
//import fr.inria.aoste.timesquare.backend.test.behavior.TestBehaviorManager;
//import fr.inria.aoste.timesquare.backend.vcdgenerator.manager.VCDBehaviorConfigurator;
//import fr.inria.aoste.timesquare.backend.vcdgenerator.manager.VCDGeneratorManager;
//import fr.inria.aoste.timesquare.launcher.core.OutputManager;
//import fr.inria.aoste.timesquare.launcher.core.PESolverManager;
//import fr.inria.aoste.timesquare.launcher.debug.model.proxy.CCSLConstantProxyHelper;
//import fr.inria.aoste.timesquare.launcher.debug.model.proxy.Configurator;
//import fr.inria.aoste.timesquare.launcher.debug.model.proxy.ISimulatorControl;
//import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
//import fr.inria.aoste.trace.EventOccurrence;
//import fr.inria.aoste.trace.LogicalStep;
//import fr.inria.aoste.trace.ModelElementReference;
//import fr.inria.aoste.trace.Trace;
//
//public class TestAndDemo {
//
//	private IProject project;
//	private IFile example, example2, example3,example4;
//	private IProject project2;
//
//	/***
//	 * initialisation : verification que l'excution n'est pas dans un thread de
//	 * Display fermeture de la vue Welcome
//	 * 
//	 * 
//	 * @throws Throwable
//	 */
//	@BeforeClass
//	public static void initTest() throws Throwable {
//		WorkspaceHelper.preConditionTest();
//		// close welcome view
//		WorkspaceHelper.closeWelcome();
//		assertTrue("Solver ", PESolverManager.getDefault().getListExtension().length != 0);
//		assertTrue("Output ", OutputManager.getDefault().getListkey().size() != 0);
//		WorkspaceHelper.createTestProject();
//		WorkspaceHelper.createTestCodeExecutorBehaviorProject();
//		WorkspaceHelper.workspaceRefresh();
//
//	}
//
//	@AfterClass
//	public static void afterTest() throws Exception {
//		Thread.sleep(10000);
//	}
//
//	/*********
//	 * Creation du workspace : project test copies du fichier example:
//	 * alternates.ccsl
//	 * 
//	 */
//	@Before
//	public void setUp() throws Exception {
//		// create test project
//		project = WorkspaceHelper.getProjectHandle("test");
//		project.open(null);
////		example = project.getFile("alternates.ccsl");
////		example.refreshLocal(IResource.DEPTH_INFINITE, null);
////		example2 = project.getFile("deadlock.ccsl");
////		example2.refreshLocal(IResource.DEPTH_INFINITE, null);
//		example3 = project.getFile("testPapyrus/alternatesUML.extendedCCSL");
//		example4= project.getFile("bindingError.extendedCCSL");
//		project2 = WorkspaceHelper.getProjectHandle(WorkspaceHelper.CodeExecutorProject);
//		project2.open(null);
//		WorkspaceHelper.workspaceRefresh();
//		// org.eclipse.jdt.internal.ui.packageview.PackageExplorerPart
//
//	}
//
//	@After
//	public void tearDown() throws Exception {
//		System.gc();
//		WorkspaceHelper.workspaceRefresh();
//		Thread.sleep(1000);
//	}
//
//	/***
//	 * 
//	 * Simple launch test/alternets.ccsl
//	 */
//	@Ignore
//	@Test
//	public void testBasicRun() throws Throwable {		
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		// set 10 pas
//		launchconf.setStepNbr(10);
//		//
//		assertTrue("test Clock ", launchconf.getListofClock().size() != 0);
//		// launch simulation
//		ISimulatorControl simControl = launchconf.launch();
//		// run all
//		simControl.syncRunAllStep();
//		// test trace
//		Trace trace = simControl.getTrace();
//		assertFalse("Error During Simulation ", simControl.getStateEngine().isError());
//		assertEquals("Test Step", trace.getLogicalSteps().size(), 10);
//		assertEquals("Test ModelElementRef", trace.getReferences().size(), 3);
//		simControl.terminate();
//	}
//
//	
//	
//	
//	/***
//	 * 
//	 * Simple launch bindingError.extendedCCSL (bad Spec (bindind) ) ==> error
//	 */
//	@Test
//	public void testSpecWithError() throws Throwable {
//		try {
//			project.open(null);
//			@SuppressWarnings("unused")
//			CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example4);
//		} catch (Throwable e) {
//			// ==> error when load file 
//			System.err.println("expected error   :" + e);
//			return;
//
//		}
//		fail("Bad File : create launch");
//		project.open(null);
//
//	}
//	/***
//	 * 
//	 * Simple launch test/alternets.ccsl The project is close ==> error
//	 */
//	@Ignore
//	@Test
//	public void testProjectClose() throws Throwable {
//		try {
//			project.close(null);
//			@SuppressWarnings("unused")
//			CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		} catch (Throwable e) {
//			// ==> lauch is imposible because project is closed
//			System.err.println("expected error   :" + e);
//			return;
//
//		}
//		fail("Not Run :Error loading model ( Project is closed )");
//		project.open(null);
//
//	}
//
//	/***
//	 * 
//	 * launch test/alternets.ccsl with a TestBehavior
//	 * 
//	 */
//
//	@Test
//	public void testRunBehaviorTest() throws Throwable {
//		assertTrue("Behavior ID Found ", OutputManager.getDefault().getkey(TestBehaviorManager.class) != null);
//		Configurator o = null;	
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		launchconf.setStepNbr(10);
//		System.out.println("Clock found " + launchconf.getListofClock().size());
//
//		o = launchconf.getConfigurator(TestBehaviorManager.class);
//
//		if (o instanceof TestBehaviorConfigurator) {
//			TestBehaviorConfigurator tbc = (TestBehaviorConfigurator) o;
//			// active the behavior manager
//			tbc.activate();
//			ISimulatorControl simControl = launchconf.launch();
//			// run all
//			simControl.syncRunAllStep();
//			// check behavior manager execution
//			assertFalse("Error During Simulation ", simControl.getStateEngine().isError());
//			assertEquals("Test Run behavior ", tbc.getBehaviorManager().getTick(), 10);
//			assertEquals("Test Run behavior ", tbc.getBehaviorManager().getMaxTickByStep(), 1);
//			// assertTrue(simControl.getTrace().eResource()!=null);
//			simControl.terminate();
//
//		} else {
//			fail("Cast TestBehaviorConfigurator ");
//		}
//	}
//
//	/**
//	 * 
//	 * 
//	 * @throws Throwable
//	 */
//	@Ignore
//	@Test
//	public void testWithBadConfiguration() throws Throwable {
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		launchconf.setStepNbr(10);
//		System.out.println("Clock found " + launchconf.getListofClock().size());
//
//		Configurator o = launchconf.getConfigurator(TestBehaviorManager.class);
//		TestBehaviorConfigurator tbc = null;
//		if (o instanceof TestBehaviorConfigurator) {
//			tbc = (TestBehaviorConfigurator) o;
//			tbc.activate();
//			// Behavior is only active
//			// this behavior manager is invalid if don't have behavior
//			assertTrue(tbc.validate()!=null);
//			ISimulatorControl simControl = launchconf.launch();
//			// run all
//			simControl.syncRunAllStep();
//			assertFalse("Error During Simulation ", simControl.getStateEngine().isError());
//			assertEquals("Test Run behavior ", tbc.getBehaviorManager().getTick(), 0);
//			assertEquals("Test Run behavior ", tbc.getBehaviorManager().getMaxTickByStep(), 0);
//			simControl.terminate();
//		} else {
//			fail("Cast TestBehaviorConfigurator : ");
//		}
//	}
//
//	/***
//	 * 
//	 * launch test/alternets.ccsl with a TestBehavior configuration manual ( add
//	 * a behavior to clock A)
//	 */
//
//	@Ignore
//	@Test
//	public void testWith2Behavior() throws Throwable {
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		launchconf.setStepNbr(6);
//		System.out.println("Clock found " + launchconf.getListofClock().size());
//		EObject clock = null;
//		for (ModelElementReference eo : launchconf.getListofClock()) {
//			if ("a".equals(AdapterRegistry.getAdapter(eo).getReferenceName(eo)))
//				clock = eo;
//		}
//		assertTrue("clock a found ", clock != null);
//
//		Configurator o = launchconf.getConfigurator(TestBehaviorManager.class);
//		if (o instanceof TestBehaviorConfigurator) {
//			TestBehaviorConfigurator tbc = (TestBehaviorConfigurator) o;
//			tbc.activate();
//			tbc.createBehaviorforClock(clock);
//
//			o = launchconf.getConfigurator(VCDGeneratorManager.class);
//			if (o instanceof VCDBehaviorConfigurator) {
//				o.activate();
//			} else {
//				fail("Cast VCDBehaviorConfigurator");
//			}
//			ISimulatorControl simControl = launchconf.launch();
//			// run all
//			simControl.syncRunAllStep();
//			assertFalse("Error During Simulation ", simControl.getStateEngine().isError());
//			assertEquals("Test Run behavior ", tbc.getBehaviorManager().getTick(), 3); // 6/2
//			assertEquals("Test Run behavior ", tbc.getBehaviorManager().getMaxTickByStep(), 1);
//			simControl.terminate();
//		} else {
//			fail("Cast TestBehaviorConfigurator ");
//		}
//	}
//
//	@Ignore
//	@Test
//	public void testStepByStep() throws Throwable {
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		launchconf.setStepNbr(6);
//		System.out.println("Clock found " + launchconf.getListofClock().size());
//		EObject clock = null;
//		for (ModelElementReference eo : launchconf.getListofClock()) {
//			if ("a".equals(AdapterRegistry.getAdapter(eo).getReferenceName(eo)))
//				clock = eo;
//		}
//		assertNotNull("clock a found ", clock);
//		Configurator o = launchconf.getConfigurator(TestBehaviorManager.class);
//		TestBehaviorConfigurator tbc = null;
//		if (o instanceof TestBehaviorConfigurator) {
//			tbc = (TestBehaviorConfigurator) o;
//			tbc.activate();
//			tbc.createBehaviorforClock(clock);
//			ISimulatorControl simControl = launchconf.launch();
//			{
//				simControl.syncRunStep();
//				LogicalStep ls = simControl.getCurrentSolverStep();
//				assertEquals("Test Step ", ls.getStepNumber(), 0);
//				assertEquals("Test Run behavior ", tbc.getBehaviorManager().getTick(), 1);
//				assertEquals("Test Run behavior Max", tbc.getBehaviorManager().getMaxTickByStep(), 1);
//			}
//			{
//				simControl.syncRunStep();
//				LogicalStep ls = simControl.getCurrentSolverStep();
//				assertEquals("Test Step ", ls.getStepNumber(), 1);
//				assertEquals("Test Run behavior 2", tbc.getBehaviorManager().getTick(), 1);
//				assertEquals("Test Run behavior Max2", tbc.getBehaviorManager().getMaxTickByStep(), 1);
//			}
//			{
//				simControl.syncRunStep();
//				LogicalStep ls = simControl.getCurrentSolverStep();
//				assertEquals("Test Step ", ls.getStepNumber(), 2);
//				assertEquals("Test Run behavior 3", tbc.getBehaviorManager().getTick(), 2);
//			}
//			{
//				simControl.syncRunStep();
//				LogicalStep ls = simControl.getCurrentSolverStep();
//				assertEquals("Test Step ", ls.getStepNumber(), 3);
//				assertEquals("Test Run behavior 4", tbc.getBehaviorManager().getTick(), 2);
//			}
//			{
//				simControl.syncRunStep();
//				LogicalStep ls = simControl.getCurrentSolverStep();
//				assertEquals("Test Step ", ls.getStepNumber(), 4);
//				assertEquals("Test Run behavior 5", tbc.getBehaviorManager().getTick(), 3);
//			}
//			{
//				simControl.syncRunStep();
//				LogicalStep ls = simControl.getCurrentSolverStep();
//				assertEquals("Test Step ", ls.getStepNumber(), 5);
//				assertEquals("Test Run behavior 6", tbc.getBehaviorManager().getTick(), 3);
//			}
//			assertTrue(simControl.isTerminated());
//			assertFalse("Error During Simulation ", simControl.getStateEngine().isError());
//			simControl.terminate();
//		} else {
//
//			fail("Cast TestBehaviorConfigurator ");
//		}
//	}
//
//	@Ignore
//	@Test
//	public void testPartialRun() throws Throwable {
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		// launchconf.setErrorHandler(errorHandler)
//		launchconf.setStepNbr(20);
//		EObject clock = null;
//		for (ModelElementReference eo : launchconf.getListofClock()) {
//			if ("a".equals(AdapterRegistry.getAdapter(eo).getReferenceName(eo)))
//				clock = eo;
//		}
//		Configurator o = launchconf.getConfigurator(TestBehaviorManager.class);
//		TestBehaviorConfigurator tbc = null;
//		if (o instanceof TestBehaviorConfigurator) {
//			tbc = (TestBehaviorConfigurator) o;
//			tbc.activate();
//			tbc.createBehaviorforClock(clock);
//			ISimulatorControl simControl = launchconf.launch();
//			// running 10 step
//			for (int i = 0; i < 10; i++) {
//				simControl.syncRunStep();
//			}
//			// stop simulation
//			simControl.terminate();
//			assertTrue("Terminate ", simControl.isTerminated());
//			assertFalse("Error During Simulation ", simControl.getStateEngine().isError());
//
//			Trace trace = simControl.getTrace();
//			assertEquals("Test Step", trace.getLogicalSteps().size(), 10);
//			assertEquals("Test Run behavior ", tbc.getBehaviorManager().getTick(), 5);
//		} else {
//			fail("Cast TestBehaviorConfigurator : ");
//		}
//	}
//
//	@Ignore
//	@Test
//	public void testDeadlock() throws Throwable {
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example2);
//		launchconf.setStepNbr(20);
//		EObject clock = null;
//		for (ModelElementReference eo : launchconf.getListofClock()) {
//			if ("a".equals(AdapterRegistry.getAdapter(eo).getReferenceName(eo)))
//				clock = eo;
//		}
//		Configurator o = launchconf.getConfigurator(TestBehaviorManager.class);
//		TestBehaviorConfigurator tbc = null;
//		if (o instanceof TestBehaviorConfigurator) {
//			tbc = (TestBehaviorConfigurator) o;
//			tbc.activate();
//			tbc.createBehaviorforClock(clock);
//			ISimulatorControl simControl = launchconf.launch();
//			Exception exception = null;
//			try {
//				simControl.syncRunAllStep();
//			} catch (Exception e) {
//				exception = e;
//			}
//			assertNotNull("Exception Excepted", exception);
//			Trace trace = simControl.getTrace();
//			assertTrue("Error During Simulation ", simControl.getStateEngine().isError());
//			assertEquals("Test Step", trace.getLogicalSteps().size(), 6);
//			assertEquals("Test Run behavior ", tbc.getBehaviorManager().getTick(), 3);
//			simControl.terminate();
//		} else {
//			fail("Cast TestBehaviorConfigurator : ");
//		}
//	}
//
//	private static final String methodname = "=TestCodeExecutorBehavior/src<testcebehavior{Test.java[Test~run";
//
//	/***
//	 * CodeExecutor Example:
//	 * 
//	 * 
//	 * run methods Test.run() cf
//	 * /fr.inria.aoste.timesquare.backend.test/TestCodeExecutorBehavior
//	 * /src/testcebehavior/Test.java
//	 * 
//	 * @throws Throwable
//	 */
//
//	@Ignore
//	@Test
//	public void testCodeExecutor() throws Throwable {
//		assertNotNull("Behavior ID Found ", OutputManager.getDefault().getkey(CodeExecutionManager.class));
//		project2.open(null);
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		launchconf.setStepNbr(50);
//		System.out.println("Clock found " + launchconf.getListofClock().size());
//		ModelElementReference clock = null;
//		for (ModelElementReference eo : launchconf.getListofClock()) {
//			if ("a".equals(AdapterRegistry.getAdapter(eo).getReferenceName(eo)))
//				clock = eo;
//		}
//		assertTrue("clock a found ", clock != null);
//
//		Configurator o = launchconf.getConfigurator(CodeExecutionManager.class);
//		assertNotNull("Configurator ", o);
//		if (o instanceof CodeExecutorConfigurator) {
//			CodeExecutorConfigurator codeExecutorConfigurator = (CodeExecutorConfigurator) o;
//			codeExecutorConfigurator.activate();
//			IJavaElement ip = JavaCore.create(methodname);
//			assertNotNull(ip);
//			IMethod method = (IMethod) ip;
//			codeExecutorConfigurator.createBehaviorforClock(method, clock);
//			HashMap<String, Object> map = codeExecutorConfigurator.getData();
//			ISimulatorControl simControl = launchconf.launch();
//
//			while (!simControl.isTerminated()) {
//				simControl.syncRunStep();
//
//				EventOccurrence eo = null;
//				for (EventOccurrence cs : simControl.getCurrentSolverStep().getEventOccurrences()) {
//					if (clock == cs.getReferedElement()) {
//						eo = cs;
//						break;
//					}
//				}
//
//				assertNotNull("EventOccurrence ", eo);
//				int i = ((Integer) map.get("int")).intValue();
//				int counter = eo.getCounter();
//				assertEquals("Test Run behavior ", i, counter);
//
//			}
//
//			// simControl.syncRunAllStep();
//
//			int i = ((Integer) map.get("int")).intValue();
//			assertEquals("Test Run behavior ", i, 25);
//			assertFalse("Error During Simulation ", simControl.getStateEngine().isError());
//			simControl.terminate();
//		} else {
//			fail("Cast CodeExecutorConfigurator : ");
//		}
//	}
//
//	@Ignore
//	@Test
//	public void testPowerFail() throws Throwable {
//		assertNotNull("Behavior ID Found ", OutputManager.getDefault().getkey(PowerBehaviorManager.class));
//		Configurator o = null;	
//		project.open(null);
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example);
//		launchconf.setStepNbr(10);
//		System.out.println("Clock found " + launchconf.getListofClock().size());
//		o = launchconf.getConfigurator(PowerBehaviorManager.class);
//
//		if (o instanceof PowerConfiguration) {
//			PowerConfiguration tbc = (PowerConfiguration) o;
//			// active the behavior manager
//			try {
//				tbc.activate();
//			} catch (Exception e) {
//				// Power behavior require a clock with start/stop couple
//				return;
//			}
//			fail("activation");
//
//		} else {
//			fail("Cast PowerBehaviorManager ");
//		}
//
//	}
//
//	@Test
//	public void testPapyrus() throws Throwable {
//		assertNotNull("Behavior ID Found ", OutputManager.getDefault().getkey(PapyrusAnimatorBehaviorManager.class));
//		Configurator o = null;
//		project.open(null);
//		// example3 link clock to UML element in file
//		// test/testPapyrus/alternates.uml
//		CCSLConstantProxyHelper launchconf = CCSLConstantProxyHelper.createlaunch(example3);
//		launchconf.setStepNbr(20);
//		
//		
//		o = launchconf.getConfigurator(PapyrusAnimatorBehaviorManager.class);
//
//		if (o instanceof PapyrusAnimatorBehaviorConfigurator) {
//			PapyrusAnimatorBehaviorConfigurator papyrusAnimatorBehaviorConfigurator = (PapyrusAnimatorBehaviorConfigurator) o;
//
//			try {// active Papyrus
//				papyrusAnimatorBehaviorConfigurator.activate();
//			} catch (Exception e) {
//				fail("No link to UML element in spec");
//				return;
//			}
//
//			{
//				Exception exception = null;
//				try { // file not exist
//					papyrusAnimatorBehaviorConfigurator.setDi("test/model.di");
//				} catch (Exception e) {
//					exception = e;
//				}
//				assertNotNull("Exception Excepted", exception);
//			}
//
//			{
//				Exception exception = null;
//
//				try {
//					// file exist but dont UML element is link to the spec
//					//model.di ==> model.uml 
//					papyrusAnimatorBehaviorConfigurator.setDi("test/testPapyrus/model.di");
//				} catch (Exception e) {
//					exception = e;
//				}
//				assertNotNull("Exception Excepted", exception);
//			}
//			{
//				try {
//					// set good file
//					// alternates.di ==> alternates.uml
//					papyrusAnimatorBehaviorConfigurator.setDi("test/testPapyrus/alternates.di");
//				} catch (Exception e) {
//					fail("activation or Di File");
//					return;
//				}
//			}
//			// Start
//			ISimulatorControl simControl = launchconf.launch();
//			// Running
//			for (int i = 0; i < 20; i++) {
//				simControl.syncRunStep();
//				{
//					// Test Papyrus Running
//					int table[] = 		papyrusAnimatorBehaviorConfigurator.test_SizeofGetTableColor();
//					boolean element[] = papyrusAnimatorBehaviorConfigurator.test_ElementUMLFound();
//					assertEquals(table.length, element.length);
//					for (int j = 0; j < table.length; j++) {
//						if (element[j]) {
//							// Element UML find: ( change color )
//							assertEquals(table[j], i + 1);
//						} else {
//							// no element uml found
//							assertEquals(table[j], 0);
//						}
//					}
//				}
//				Thread.sleep(200);
//			}
//			System.out.println("Old Step");
//			for (int i = 0; i < 5; i++) {
//				simControl.syncPreviousStep();
//				{
//					// Test Papyrus Running
//					int table[] = 		papyrusAnimatorBehaviorConfigurator.test_SizeofGetTableColor();
//					boolean element[] = papyrusAnimatorBehaviorConfigurator.test_ElementUMLFound();
//					assertEquals(table.length, element.length);
//					for (int j = 0; j < table.length; j++) {
//						if (element[j]) {
//							// Element UML find: ( change color )
//							assertEquals(table[j], 20);
//						} else {
//							// no element uml found
//							assertEquals(table[j], 0);
//						}
//					}
//				}
//				Thread.sleep(500);
//			}
//			simControl.terminate();
//		} else {
//			fail("Cast PapyrusAnimatorBehaviorConfigurator");
//		}
//	}
//}
