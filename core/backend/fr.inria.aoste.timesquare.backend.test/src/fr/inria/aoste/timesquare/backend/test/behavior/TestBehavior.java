/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.test.behavior;

import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;

public class TestBehavior implements ClockBehavior {

	TestBehaviorManager testBehaviorManager;
	public TestBehavior(TestBehaviorManager testBehaviorManager) {
		this.testBehaviorManager=testBehaviorManager;
	}

	@Override
	public String getDescription() {
		
		return "description is here";
	}

	@Override
	public boolean behaviorEquals(Behavior behavior) {
		if( behavior==this)
			return true;
		return false;
	}

	@Override
	public void run(TraceHelper helper) {
		System.out.println("Run Behavior :" + helper );
		testBehaviorManager.incTick();
	}

	@Override
	public void runWithWrongActivationState(TraceHelper helper) {
		//System.out.println("runWithWrongActivationState Behavior :" + helper );
		
	}
	

}
