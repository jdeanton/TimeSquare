/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.utils;

import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.trace.ModelElementReference;

public class FilterClockEntity implements IFilter<ClockEntity> {

	final IFilter<ModelElementReference> filterModelElementReference;

	public FilterClockEntity(IFilter<ModelElementReference> filterModelElementReference) {
		this.filterModelElementReference = filterModelElementReference;
	}

	@Override
	public boolean accept(ClockEntity ce) {
		return filterModelElementReference.accept(ce.getModelElementReference());
	}
}