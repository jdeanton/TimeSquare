/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure;

import java.util.Collections;
import java.util.List;

import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertBehaviorList;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockBehaviorList;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationBehaviorList;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.trace.AssertionState;
import fr.inria.aoste.trace.EventOccurrence;

/**
 * 
 * This class acts as a big interface between the Controller and the
 * DataStructures used by clocks, relations and future items..
 * 
 * If you want to add a some DataStructures in this plugin, this class has to be
 * updated.
 * 
 * @author dlengell
 * 
 */
public class DataStructureManager {
	private ClockBehaviorList _clockBehaviorList = new ClockBehaviorList();
	private RelationBehaviorList _relationBehaviorList = new RelationBehaviorList();
	private AssertBehaviorList _assertBehaviorList = new AssertBehaviorList();

	/**
	 * Adds a ClockBehaviorEntity.
	 * 
	 * @param entity
	 */
	public void addEntity(ClockBehaviorEntity entity) {
		_clockBehaviorList.addEntity(entity);
	}

	/**
	 * Adds a ClockBehaviorEntity.
	 * 
	 * @param entity
	 */
	public void addEntity(AssertBehaviorEntity entity) {
		_assertBehaviorList.addEntity(entity);
	}

	/**
	 * Adds a RelationBehaviorEntity.
	 * 
	 * @param entity
	 */
	public void addEntity(RelationBehaviorEntity entity) {
		_relationBehaviorList.addEntity(entity);
	}

	/**
	 * Returns the ClockBehaviorEntity list.
	 * 
	 * @return The ClockBehaviorEntity list.
	 */
	public List<ClockBehaviorEntity> getClockBehaviorEntity() {
		return Collections.unmodifiableList(_clockBehaviorList.getData());
	}

	/**
	 * Returns the RelationBehaviorEntity list.
	 * 
	 * @return The RelationBehaviorEntity list.
	 */
	public List<RelationBehaviorEntity> getRelationBehaviorEntity() {
		return Collections.unmodifiableList(_relationBehaviorList.getData());
	}

	public List<AssertBehaviorEntity> getAssertBehaviorEntity() {
		return Collections.unmodifiableList(_assertBehaviorList.getData());
	}

	/**
	 * Clears the DataStrucures.
	 */
	public void clear() {
		_clockBehaviorList.clear();
		_relationBehaviorList.clear();
		_assertBehaviorList.clear();
	}

	public void deleteAssertBehaviorEntity(AssertBehaviorEntity assertBehaviorEntity) {
		_assertBehaviorList.deleteEntity(assertBehaviorEntity);
	}

	/**
	 * Deletes a ClockBehaviorEntity.
	 * 
	 * @param clockBehaviorEntity
	 */
	public void deleteClockBehaviorEntity(ClockBehaviorEntity clockBehaviorEntity) {

		_clockBehaviorList.deleteEntity(clockBehaviorEntity);

	}

	/**
	 * Deletes a RelationBehaviorEntity.
	 * 
	 * @param relationBehaviorEntity
	 */
	public void deleteRelationBehaviorEntity(RelationBehaviorEntity relationBehaviorEntity) {

		_relationBehaviorList.deleteEntity(relationBehaviorEntity);

	}

	/**
	 * Deletes all the entities which has a plugin name like which one passed in
	 * parameter.
	 * 
	 * @param pluginName
	 */
	public void deleteEntitiesByPluginName(String pluginName) {
		_clockBehaviorList.deleteEntitiesByPluginName(pluginName);
		_relationBehaviorList.deleteEntitiesByPluginName(pluginName);
		_assertBehaviorList.deleteEntitiesByPluginName(pluginName);
	}

	/**
	 * Executes ClockBehaviors.
	 * 
	 * @param clockEntity
	 * @param clockState
	 */
	public void executeNewBehaviors(ClockEntity clockEntity, EventOccurrence clockState) {
		_clockBehaviorList.executeNewBehaviors(clockEntity, clockState);
	}

	public void executeNewBehaviors(ClockEntity clockEntity, AssertionState clockState) {
		_assertBehaviorList.executeNewBehaviors(clockEntity, clockState);
	}

	/*****/
	/**
	 * Executes ClockBehaviors.
	 * 
	 * @param clockEntity
	 * @param clockState
	 */
	public void executeBehaviors(ClockEntity clockEntity, EventOccurrence clockState) {

		_clockBehaviorList.executeBehaviors(clockEntity, clockState);

	}

	/**
	 * Executes RelationBehaviors.
	 * 
	 * @param relation
	 */
	public void executeBehaviors(OccurrenceRelation relation) {

		_relationBehaviorList.executeBehaviors(relation);

	}

	/**
	 * Checks the consistency between the DataStructures and a new clock list.
	 * 
	 * @param clocks
	 */
	public void checkConsistency(List<ClockEntity> clocks, List<ClockEntity> _assert) {
		_clockBehaviorList.checkConsistency(clocks);
		_assertBehaviorList.checkConsistency(_assert);

	}

	/**
	 * Returns true if the plugin passed in parameter has declared some
	 * behaviors. Else returns false.
	 * 
	 * @param pluginName
	 * @return true if the plugin passed in parameter has declared some
	 *         behaviors else returns false.
	 */
	public boolean pluginPresent(String pluginName) {
		return (_clockBehaviorList.pluginPresent(pluginName) || _relationBehaviorList.pluginPresent(pluginName) || _assertBehaviorList
				.pluginPresent(pluginName));
	}

}
