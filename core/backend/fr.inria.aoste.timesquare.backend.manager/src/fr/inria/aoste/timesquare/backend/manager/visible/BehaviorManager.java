/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import org.eclipse.core.runtime.IPath;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;
import fr.inria.aoste.trace.LogicalStep;


/**
 * BehaviorManager Class  
 * 
 * IMPORTANT : Class which extends this class must have a constructor with no parameters.
 * 
 * @author dlengell,bferrero
 *
 */
public abstract class BehaviorManager
{
	/**
	 * Returns the name of your plugin. 
	 * This method is important because this name acts as an ID.
	 * It is the first method called by the plugin fr.inria.aoste.timesquare.backend.manager.
	 * 
	 * @return The plugin name which acts s an ID.
	 */
	public abstract  String getPluginName();

	//private String pluginName=null;
	private String id=null;

	protected String _getid()
	{
		String s= getPluginName().toLowerCase().replaceAll(" ", "");
		return s;
	}
	public  final String getId()
	{
		if ( id==null)
		{
			id=_getid();
		}
		return id;
	}
	
	protected  Controller controller=null;
	
	/**
	 * @param rm  
	 * @param ccslhelper 
	 */
	public boolean isActivable(ReportMessage rm , CCSLInfo ccslhelper)
	{
		return true;
	}
	
	
	/**
	 * Validate the options
	 * @return null if no Error, an error Message otherwise
	 */
	public String validate()
	{
		return null;
	}
	
	/**
	 * This method is called when an Entity is deleted using the "delete" button,
	 * it may or may not be implemented, it is only required when a special treatment
	 * is needed when a behavior is deleted.
	 * <br><br><i>For history purpose :</i><br>
	 * The need for this method was first felt for the Duration behavior, which required
	 * the behavior manager to delete entities from the duration model it created,
	 * it was also a good way to delete other behaviors than the one currently selected,
	 * as the Duration behavior creates several behaviors which rely on each other.
	 * 
	 * @param entity 
	 * <p>The ClockEntity/AssertEntity/RelationEntity which is to be deleted.
	 * 
	 * @param deletehelper 
	 * <p>for delete other Entity 
	 * 
	 * @author ybondue,bferrero
	 */
	public void deleteEntity( Entity entity ,DeleteHelper deletehelper) {}
	
	
		
	public final void setController( Controller c ) {
		this.controller = c;
	}
	
	protected ConfigurationHelper _helper = null;
	/**
	 * This method is the second method called by the plugin fr.inria.aoste.timesquare.backend.manager.
	 * It allows an initialization step to the plugin.
	 * Be careful, this method can be called several times, especially when a change is detected
	 * in the CCSL Model.
	 * 
	 * @param helper
	 */
	public void init( ConfigurationHelper helper ) {
		this._helper = helper;
	}
	
	/**
	 * This method is called when a user clicks on your plugin in the run cnfiguration and
	 * if the plugin doesn't have a BehaviorManagerGUI.
	 * Notice that if the plugin has a BehaviorManagerGUI, this method will never be called.
	 * 
	 * @param helper
	 */
	public abstract void manageBehavior( ConfigurationHelper helper );
	
	/**
	 * In a deserialization phase, the BehaviorManager will be asked to return a ClockBehavior
	 * thanks to the PersistentOptions passed in parameter.
	 * 
	 * @param helper
	 * @param persistentOptions
	 * @return A new ClockBehavior.
	 */
	public abstract ClockBehavior redoClockBehavior( ConfigurationHelper helper, PersistentOptions persistentOptions );
	
	/**
	 * In a deserialization phase, the BehaviorManager will be asked to return a RelationBehavior
	 * thanks to the PersistentOptions passed in parameter.
	 * 
	 * @param helper
	 * @param persistentOptions
	 * @return A new RelationBehavior.
	 */
	public abstract RelationBehavior redoRelationBehavior( ConfigurationHelper helper, PersistentOptions persistentOptions );
	
	public abstract ClockBehavior redoAssertBehavior( ConfigurationHelper helper, PersistentOptions persistentOptions );
	/**
	 * If the plugin has general options to serialized, the BehaviorManager must return
	 * these in this method in a PersistentOptions.
	 * 
	 * @return The plugin general options in a PersistentOptions. 
	 */
	public PersistentOptions getPluginOptions() {
		return null;
	}
	
	/**
	 * In a deserialization phase, the plugin options are returned to the BehaviorManager in
	 * this method.
	 * 
	 * @param helper
	 * @param persistentOptions
	 */
	public void receivePluginOptions( ConfigurationHelper helper, PersistentOptions persistentOptions ) throws Throwable {
		/* EMPTY BY DEFAULT */
	}
	
	/**
	 * This is an initialization method which is called at the beginning of the simulation.
	 * 
	 * @param helper
	 * @param folderin
	 * @param namefilein
	 * @param solver: the solver, used to force clock or set variable dynamically
	 */
	public abstract void beforeExecution( ConfigurationHelper helper, IPath folderin, String namefilein, ISolverForBackend solver );
	
	/**
	 * This method is called at the end of the simulation.
	 * 
	 * @param helper
	 */
	public abstract void end( ConfigurationHelper helper );
	
	/**
	 * This method is called during the simulation, when a new step is created.
	 * 
	 * @param step
	 */
	public void aNewStep( int step ) {
		/* EMPTY By DEFAULT */
	}
	
	/**
	 * This method is called during the simulation, when a step is repeated.
	 * 
	 * @param step
	 */
	public void repeatStep( int step ) {
		/* EMPTY By DEFAULT */
	}
	/**
	 * This method is called during the simulation, when a step is finished and before
	 * the next one begins.
	 * <br><br><i>For history purpose :</i><br>
	 * The need for this method was first felt for the VCDGeneration behavior, which required
	 * the behavior manager to operate once a step is finished and before another one begins.
	 * 
	 * @param step
	 * @author ybondue
	 */
	public void aPostNewStep( int step ) {}
	
	/**
	 * @param configurationHelper  
	 */
	public BehaviorConfigurator<?> getConfigurator(ConfigurationHelper configurationHelper) {
		
		return null;
	}
	
	public final  void activate()
	{
		controller.activate();
	}
	
	LogicalStep currentstep;

	public final LogicalStep getCurrentStep() {
		return currentstep;
	}
	public final void setCurrentStep(LogicalStep currentstep) {
		this.currentstep = currentstep;
	}
	
	public void clear() {
		
		
	}
	
	
	

}
