/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/**
 * The SpecialObjectInputStream is a Java ObjectInputStream that allows to load class
 * with our own ClassLoader.
 * 
 * This is useful during the deserialization step because we want to deserialize plugin's objects and
 * our ClassLoader can't deserialize them. 
 * 
 * @author dlengell
 *
 */
public class SpecialObjectInputStream extends ObjectInputStream
{
	private ClassLoader _classLoader;
	
	/**
	 * A constructor with two parameters.
	 * The ClassLoader passed in parameter will be use to resolve classes.
	 * 
	 * @param in
	 * @param classLoader
	 * @throws IOException
	 */
	public SpecialObjectInputStream( InputStream in, ClassLoader classLoader ) throws IOException
	{
		super( in );
		_classLoader = classLoader;
	}

	protected Class<?> resolveClass( ObjectStreamClass v ) throws IOException, ClassNotFoundException
	{
		try
		{
			Class<?> c = super.resolveClass( v );
			if ( c != null )
			{
				return c ;
			}
		}
		catch ( Throwable e )
		{
		    if ( _classLoader == null )
		    {
		        throw new ClassNotFoundException();
		    }
		        
		    return _classLoader.loadClass( v.getName() );
		}
		
		return null;
	}
	
}