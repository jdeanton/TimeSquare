/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import fr.inria.aoste.timesquare.backend.manager.serialization.BehaviorPersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.serialization.OptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.PersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.serialization.SpecialObjectInputStream;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;

/**
 * The ClockBehaviorOptionsSerializer manages the serialization/deserialization 
 * of the ClockBehaviorPersistentEntities.
 * 
 * Serialization step :
 * The ClockBehaviorPersistentEntities are added by the Controller into a private Map.
 * The serializer is used by the XMLStringMaker providing serialized ClockBehaviorPersistentEntities 
 * in a String for each plugin.
 * 
 * Deserialization step :
 * The XMLStringParser gives to the ClockBehaviorOptionsSerializer some strings.
 * The strings are deserialized, and the ClockBehaviorPersistentEntities are added to the private Map.
 * 
 * @author dlengell
 *
 */
public class BehaviorOptionsSerializer extends OptionsSerializer
{

	private Map< String, List< BehaviorPersistentEntity > > _data;
	
	/**
	 * A constructor with no parameter.
	 */
	public BehaviorOptionsSerializer()
	{
		super();

		_data = new HashMap< String, List< BehaviorPersistentEntity > >();
	}
	
	/**
	 * Returns the Map containing the ClockBehaviorPersistentEntities.
	 * 
	 * @return A List<AssertBehaviorPersistentEntity> linked to his plugin name.
	 */
	public Map< String, List< BehaviorPersistentEntity > > getData()
	{
		return _data;
	}
	
	/**
	 * Adds a AssertBehaviorPersistentEntity to the Map.
	 * 
	 * @param pluginName
	 * @param persistentEntity
	 */
	public void addPersistentEntity( String pluginName, BehaviorPersistentEntity persistentEntity )
	{
		if ( _data != null )
		{
			boolean find = false;

			//If the key already exists.
			for ( Map.Entry< String, List< BehaviorPersistentEntity > >  e : _data.entrySet() )
			{
				if ( e.getKey().equals( pluginName ) )
				{
					e.getValue().add( persistentEntity );
					find = true;
				}
			}
			
			//If the key is not found.
			if ( !find )
			{
				_data.put( pluginName, new ArrayList< BehaviorPersistentEntity >() );
				_data.get( pluginName ).add( persistentEntity );
			}
		}
	}
	
	/**
	 * Adds a list of AssertBehaviorPersistentEntity to the Map.
	 * Be careful, there is no test here about the key (if it's already exists or not).
	 * 
	 * @param pluginName
	 * @param persistentEntities
	 */
	public void addPersistentEntities( String pluginName, List< BehaviorPersistentEntity > persistentEntities )
	{
		if ( _data != null && !( _data.containsKey( pluginName ) ) )
		{
			_data.put( pluginName, persistentEntities );
		}
	}
	
	public boolean doesPluginNeedSerialization( String pluginName )
	{
	    if ( _data == null )
	    {
	        return false;
	    }
	    return _data.containsKey( pluginName );
	}
	
    public String getSerializedOptions( String pluginName )
    {
        String result = "";
        
        if ( _data == null )
        {
            return result;
        }
        
        List< BehaviorPersistentEntity > list = _data.get( pluginName );
        if ( list == null )
        {
            return result;
        }
        
        //We serialize a AssertBehaviorPersistentEntity list into a String.
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ObjectOutputStream oos= new ObjectOutputStream( baos );
            try
            {
                oos.writeObject( list ); 
                oos.flush();
            }
            finally
            {
                try
                {
                    oos.close();
                }
                finally
                {
                    result = transformByteInString( baos.toByteArray() ); //We transform the binaries into a String.
                    baos.close();
                }
            }
        }
        catch( Throwable ioe )
        {
            ioe.printStackTrace();
        }
        
        return result;
    }
	
    public void deserializeOptions( String options, BehaviorManager behaviorManager )
    {
        if ( options == null || (options.length()==0 )  || behaviorManager == null || _data == null ) 
        {
            return;
        }
    
        try
        {
            ByteArrayInputStream bais = new ByteArrayInputStream( transformStringInByte( options ) );
        
            SpecialObjectInputStream sois = new SpecialObjectInputStream( bais, behaviorManager.getClass().getClassLoader() );
        
            try
            {   
               List< ? > readObject = ( List< ? > )sois.readObject();

               if ( readObject != null )
               {
                   List< BehaviorPersistentEntity > list = new ArrayList< BehaviorPersistentEntity >();
                   ListIterator<?> li = readObject.listIterator();
                   while( li.hasNext() )
                   {
                       Object o = li.next();
                       if ( o instanceof PersistentEntity )
                       {
                           list.add( ( BehaviorPersistentEntity )o );
                       }
                   }
                   this.addPersistentEntities( behaviorManager.getPluginName(), list );
               }
            }
            finally
            {
                try
                {
                    sois.close();
                }
                finally
                {
                    bais.close();
                }
            }
            
        }
        catch( Throwable e )
        {
            e.printStackTrace();
        }
    
    }

}
