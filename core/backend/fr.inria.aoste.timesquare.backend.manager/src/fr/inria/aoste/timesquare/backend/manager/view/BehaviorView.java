/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.view;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.datastructure.DataStructureManager;
import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputSetup;

/**
 * 
 * This interface is manipulated by the View.
 * If you want to display some behaviors, you have to implement this interface and add it into the 
 * View's BehaviorView list.
 * 
 * @author dlengell
 *
 */
public interface BehaviorView
{
    /**
     * Creates content.
     * 
     * @param composite
     */
    public void createContent( Composite composite ,OutputSetup oSetup);
    
    /**
     * Displays the model (the entities) into the content created before.
     * 
     * @param dataStructureManager
     */
    public void displayModel( DataStructureManager dataStructureManager );
    
    /**
     * The BehaviorView has to delete the entity contained into the SelectionEvent parameter.
     * 
     * @param controller
     * @param selectionEvent
     */
    public void deleteBehavior( Controller controller, SelectionEvent selectionEvent );
    

    /**
     * The BehaviorView has to be cleared.
     */
    public void clear();
    
   
}
