/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import java.util.HashMap;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLHelperImpl;

import fr.inria.aoste.trace.AssertionState;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.NamedReference;
import fr.inria.aoste.trace.Reference;



/**
 * This class is instantiated and given to a plugin when a clock behavior is executed.
 * It offers the plugins common operations helping them manipulating the trace model. 
 * 
 * @author dlengell
 *
 */
public class TraceHelper
{
    private EventOccurrence _clockState;
    private ClockEntity _clockEntity;
    
    private AssertionState _assertState;
    
	/**
     * A constructor for this class.
     * 
     * @param clockState
     */
    public TraceHelper( EventOccurrence clockState, ClockEntity clockEntity )
    {
        _clockEntity = clockEntity;
        _clockState = clockState;
    }
    
    public TraceHelper(AssertionState assertState)
    {
        _assertState= assertState;
    }

    /**
     * Return the ClockState of the clock.
     * 
     * @return The ClockState of the clock linked to the behavior.
     */
    public EventOccurrence getClockState()
    {
        return _clockState;
    }
    
    /**
     * Return the ClockEntity.
     * 
     * @return The ClockEntity of the clock linked to the behavior.
     */
    public ClockEntity getClockEntity()
    {
        return _clockEntity;
    }
    
    /**
     * Return the current step number.
     * 
     * @return The current step number.
     */
    public int getCurrentStepNumber()
    {
        if ( _clockState != null )
        {
            LogicalStep step = ( LogicalStep ) _clockState.eContainer();
            return step.getStepNumber();
        }
        if ( _assertState != null )
        {
            LogicalStep step = ( LogicalStep ) _assertState.eContainer();
            return step.getStepNumber();
        }
        
        return -1;
    }
    
	public String getHRef() {
		if (_clockState != null)
			return getRef(_clockState.getReferedElement());
		if (_assertState != null)
			return getRef(_assertState.getReferedElement());
		return "";

	}
    
    static private final String getRef(Reference rf) {
		try {
			if (rf instanceof NamedReference) {
				return null;
			}
			if (rf instanceof ModelElementReference) {
				EObject eo = rf;// HelperFactory.getFirstReference((ModelElementReference) rf); 
					//((ModelElementReference) rf).getElementRef();
				Resource rs = rf.eResource();
				// Resource rs2 = eo.eResource();
				if (rs instanceof XMLResource) {
					XMLHelper xmlhelper = new XMLHelperImpl((XMLResource) rs);
					HashMap<String, Object> map = new HashMap<String, Object>();
					URIHandlerImpl urih = new URIHandlerImpl();
					urih.setBaseURI(rs.getURI());
					map.put(XMLResource.OPTION_URI_HANDLER, urih);
					xmlhelper.setOptions(map);
					//System.out.println(eo.eResource());
					String s = xmlhelper.getHREF(eo);
					if (s.charAt(0)=='#')
					{
						return rs.getURI().lastSegment()+s;
					}
					
					return s;
				}
			}
		} catch (Throwable e) {
			System.err.println(e.getStackTrace()[1] + " : " + e.getMessage());
		}
		return null;
	}
    
}
