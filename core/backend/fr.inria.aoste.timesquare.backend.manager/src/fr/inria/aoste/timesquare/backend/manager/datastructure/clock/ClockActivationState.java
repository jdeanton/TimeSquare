/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure.clock;

import fr.inria.aoste.trace.EventOccurrence;

/**
 * The ClockActivationState represents a activation state for a behavior on a
 * clock. It recapitulates the information in the Trace model, the EState and
 * FState.
 * 
 * If the Trace model is modified, this class has to be updated.
 * 
 * The structure is a simple boolean array. The order is defined in a String
 * array. For example, if you want to create an activation state when a clock
 * ticks, you have to select the EState ticks and the FState you wish, for all
 * FState :
 * 
 * [true] <- ticks [false] <- doesntTick [true] <- mustTick [false] <-
 * cannotTick [true] <- isFree [true] <- isUndetermined [true] <- isDead
 * 
 * If you associate this ClockActivationState to a ClockBehavior (association
 * done in the ClockBehaviorEntity), the behavior will be executed for all
 * possible tick state of the clock.
 * 
 * During the execution, the ClockState defined in the trace model is
 * transformed into a ClockActivationState. This ClockActivationState is
 * compared to the ClockActivationState defined by the plugins in the
 * ClockBehaviorEntity. The comparison is done with the method : <code> 
 * public boolean stateActivationOK(EventOccurrence clockState);
 * </code> If the return value is true, the behavior is executed.
 * 
 * 
 * @author dlengell,bferrero
 * 
 */
public class ClockActivationState {

	public enum StateGroup {
		fire, enable, dead;
	}

	public enum State {

		ticks("tick", 0, StateGroup.fire),
		/**/
		doesntTick("doesntTick", 1, StateGroup.fire),
		/**/
		mustTick("mustTick", 2, StateGroup.enable),
		/**/
		cannotTick("cannotTick", 3, StateGroup.enable),
		/**/
		isFree("isFree", 4, StateGroup.enable),
		/**/
		isUndetermined("isUndetermined", 5, StateGroup.enable),

		/**/
		isDead("isDead", 6, StateGroup.dead),
		/**/
		notIsDead("not IsDead", 7, StateGroup.dead);

		private State(String name, int i, StateGroup group) {
			this.i = i;
			this.name = name;
			this.group = group;
		}

		final public int i;
		final public String name;
		final public StateGroup group;

		public final static String[] string() {
			int n = State.values().length;
			String tb[] = new String[n];
			for (int i = 0; i < n; i++) {

				tb[i] = State.values()[i].getName();
			}
			return tb;
		}

		public int getIndice() {
			return i;
		}

		public String getName() {
			return name;
		}

		public StateGroup getGroup() {
			return group;
		}

		public final static State[] state(StateGroup g) {
			int n = State.values().length;
			int k = 0;
			for (int i = 0; i < n; i++) {
				if (State.values()[i].group == g)
					k++;
			}
			State tb[] = new State[k];
			k = 0;
			for (int i = 0; i < n; i++) {
				if (State.values()[i].group == g)
					tb[k++] = State.values()[i];
			}
			return tb;
		}

	}

	public boolean getState(State t) {
		if (_state == null || t == null || _state.length < t.i)
			return false;
		return _state[t.i];
	}

	public void setState(State t, boolean b) {
		if (_state == null || t == null || _state.length < t.i)
			return;
		_state[t.i] = b;
		check();
	}

	/**
	 * The number of possible state for a clock.
	 */
	public final static int stateNumber = State.values().length;

	/**
	 * The name of the possible state.
	 */
	public final static String[] _stateList = State.string();

	private boolean[] _state = null;
	
	
	

	public  ClockActivationState() {
		this(null);		
	}

	/**
	 * A constructor for this class. It takes an array of boolean in parameter.
	 * This array has to be in the good format.
	 * 
	 * @param state
	 */
	public ClockActivationState(boolean[] state) {
		_state = null;

		if (state == null) {
			_state = new boolean[stateNumber];
		} else {
			if (isValidClockState(state)) {
				_state = state;
			} else {
				if (state.length == 7) {
					_state = new boolean[8];
					for (int i = 0; i < 7; i++) {
						_state[i] = state[i];
					}
					// _state[6] = state[6];
					_state[7] = true; // state[6];
				}
			}
		}
		check();
	}

	boolean skipfire = false, skipenable = false, skipdead = false;

	/**
	 * 
	 */
	public void check() {
		skipfire = false;
		skipenable = false;
		skipdead = false;

		if (!_state[0] && !_state[1]) {
			skipfire = true;
		}

		if (!_state[2] && !_state[3] && !_state[4] && !_state[5]) {
			skipenable = true;
		}

		if (!_state[6] && !_state[7]) {
			skipdead = true;
		}

	}

	/**
	 * Returns the boolean array.
	 * 
	 * @return The boolean array.
	 */
	public boolean[] getState() {
		return _state.clone();
	}

	/**
	 * Checks if the boolean array in parameter is a valid ClockState.
	 * 
	 * @param state
	 * @return true if the boolean array passed in parameter has a good format
	 *         else returns false.
	 */
	public boolean isValidClockState(boolean[] state) {
		if (state == null) {
			return false;
		}

		return (state.length == ClockActivationState.stateNumber);
	}

	/**
	 * Returns the description of the ClockStateActivation.
	 * 
	 * @return The String description of the ClockStateActivation.
	 */
	public String getDescription() {
		try {
			StringBuilder tmp = new StringBuilder("");

			for (StateGroup sg : StateGroup.values()) {

				boolean b = false;
				for (State s : State.state(sg)) {

					if (_state[s.i]) {
						b = true;
						break;
					}
				}
				if (b) {
					if (tmp.length() != 0)
						tmp.append(",");
					tmp.append("{");
					b = false;
					for (State s : State.state(sg)) {

						if (_state[s.i]) {
							if (b) {

								tmp.append("|");
							}
							b = true;
							tmp.append(s.name);

						}
					}
					tmp.append("}");
				} else {
					tmp.append("("+sg.name() +":free )");
				}

			}

			return tmp.toString();
		} catch (Throwable e) {

		}
		return "...";
	}

	@Override
	public int hashCode() {
		int n = 0;
		int k = 1;
		for (int i = 0; i < _state.length; ++i) {
			if (_state[i]) {
				n = n + k;
			}
			k = k * 2;
			if (k <= 0) {
				k = 1;
			}
		}
		return n;
	}

	/**
	 * Redefines the equals method of Object. This method is very important when
	 * we delete/modify an Entity of the DataStructure (so a behavior).
	 */
	public boolean equals(Object o) {

		if (o == this) {
			return true;
		}

		if (o instanceof ClockActivationState) {
			ClockActivationState cs = (ClockActivationState) o;
			if (cs.getState() == null) {
				return (this._state == null);
			}
			// cs.getState()!=null
			if (this._state == null)
				return false;
			if (cs.getState().length == this._state.length) {

				for (int i = 0; i < _state.length; ++i) {
					if (cs.getState()[i] != _state[i]) {
						return false;
					}
				}
				return true;
			}

		}

		return false;
	}

	public boolean stateActivationOK(EventOccurrence clockState) {
		if (clockState == null) {
			return false;
		}
		// return stateActivationOK(this, new ClockActivationState(clockState));
		boolean result = true;
		boolean[] tmp = _state;
		if (!skipfire) {
			if (clockState.getFState() != null) {
				switch (clockState.getFState()) {
				case TICK:
					result &= tmp[0];
					break;
				case NO_TICK:
					result &= tmp[1];
					break;
				default:
					break;
				}
			} else {
				result = false; // TODO throws ?
			}
		}
		if (!skipenable) {
			if (clockState.getEState() != null) {
				switch (clockState.getEState()) {
				case TICK:
					result &= tmp[2];
					break;
				case NO_TICK:
					result &= tmp[3];
					break;
				case FREE:
					result &= tmp[4];
					break;
				case INDETERMINED:
					result &= tmp[5];
					break;

				}
			} else {
				result &= (tmp[2] && tmp[3] && tmp[4] && tmp[5]);//
			}
		}
		if (!skipdead) {
			if (clockState.isIsClockDead()) {
				result &= tmp[6];
			} else {
				result &= tmp[7];
			}
		}
		return result;
	}

}
