/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import fr.inria.aoste.timesquare.backend.manager.Activator;
import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputOption;
import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputSetup;
import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputTrace;
import fr.inria.aoste.timesquare.launcher.extensionpoint.OutputControler;

public final class BehaviorOutputControler extends OutputControler {	
	public BehaviorOutputControler(Class<? extends BehaviorManager> manager,
			Class<? extends BehaviorManagerGUI> gui,String id) {
		super(id, Activator.keyoutput+"@"+id);		
		this.manager = manager;
		this.gui = gui;
	}

	public void setKeybase(String keybase, String plugin) {
		setKey(keybase+"@"+plugin);
	}

	@Override
	public OutputTrace newOutputtrace() {
		return new OutputTrace(getKey(), getName());

	}

	@Override
	public OutputOption newOption() {
		return new OutputOption( this);
	}

	@Override
	public OutputSetup newUi() {
		return new OutputSetup(getKey(), getName());
	}

	private Class<? extends BehaviorManager> manager;
	private Class<? extends BehaviorManagerGUI> gui;

	@Override
	public boolean isCompatibleTo(Class<?> c) {	
		if (  OutputTrace.class.isAssignableFrom(c)) 
			return true;
		if ( OutputOption.class.isAssignableFrom(c))
			return true;
		if ( OutputSetup.class.isAssignableFrom(c))
			return true;
		if ( manager==c) // if ( manager.isAssignableFrom(c))
			return true;
		if ( gui!=null && gui.isAssignableFrom(c))
			return true;
		
		return false;
	}
	
	
	
	public BehaviorManager createManager() {
		if( manager!=null)
			try {
				return manager.newInstance();
			} catch (Throwable e) {				
				e.printStackTrace();
			} 
		return null;
	}
	
	public BehaviorManagerGUI createManagerGUI() {
		if( gui!=null)
			try {
				return gui.newInstance();
			} catch (Throwable e) {				
				e.printStackTrace();
			} 
		return null;
	}
}