/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization;

import java.io.Serializable;

import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

/**
 * The PersistentEntity is an serializable Entity.
 * An Entity can't be saved in byte array or string, we has to transform it into a
 * PersistentEntity.
 * 
 * @author dlengell
 *
 */
public abstract class PersistentEntity implements Serializable
{
	private static final long serialVersionUID = -6556677640671541617L;
	private String _pluginName;
	private PersistentOptions _persistentOptions;
	
	/**
	 * A constructor with no parameter.
	 */
	public PersistentEntity(String pluginName, PersistentOptions options)
	{ 
	    this._pluginName = pluginName;
	    this._persistentOptions = options;
	}
	
	/**
	 * Returns the plugin name.
	 * 
	 * @return The plugin name.
	 */
	public String getPluginName()
	{
		return _pluginName;
	}
	
	/**
	 * Returns the PersistentOptions.
	 * 
	 * @return The PersistentOptions.
	 */
	public PersistentOptions getPersistentOptions()
	{
		return _persistentOptions;
	}
	
	/**
	 * Sets a pluginName.
	 * 
	 * @param pluginName
	 */
	public void setPluginName( String pluginName )
	{
		_pluginName = pluginName;
	}
	
	/**
	 * Sets a PersitentOptions.
	 * 
	 * @param persistentOptions
	 */
	public void setPersistentOptions( PersistentOptions persistentOptions )
	{
		_persistentOptions = persistentOptions;
	}
	
	/**
	 * String representation of the PersistentEntity.
	 */
	public String toString()
	{
		return _pluginName + " " + _persistentOptions.getDescription();
	}

}
