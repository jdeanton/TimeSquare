/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure;

import java.util.ArrayList;
import java.util.List;

/**
 * A List of XXXBehaviorEntities 
 * 
 * @author fmallet
 *
 */
public abstract class ABehaviorList<T extends Entity> {
	private List< T > _data;

	protected ABehaviorList(List<T> data)
	{
		assert(data != null);
		_data = data;
	}

	/**
	 * Adds an Entity.
	 * 
	 * @param entity to be added
	 */
	final public void addEntity( T entity )
	{
		if (!( _data.contains( entity ) ) )
		{
			_data.add( entity );
		}        
	}

	/**
	 * Returns the RelationBehaviorEntity list.
	 * 
	 * @return The RelationBehaviorEntity list.
	 */
	final public List<T> getData()
	{
		return _data;
	}

	/**
	 * Deletes an entity.
	 * 
	 * @param entity to be removed
	 */
	final public void deleteEntity( T entity )
	{
		if ( entity != null)
		{   
			_data.remove( entity );
		}
	}

	/**
	 * Deletes all the entities (so the behaviors) created by a plugin.
	 * 
	 * @param pluginName
	 */
	final public void deleteEntitiesByPluginName( String pluginName )
	{
		List<Entity> toBeRemoved = new ArrayList<Entity>();
		for ( Entity e : _data )
		{
			if ( e.getPluginName().equals( pluginName ) )
			{
				toBeRemoved.add(e);
			}
		}
		for (Entity e : toBeRemoved)
			_data.remove(e);
	}

	/**
	 * Clears the RelationBehaviorEntity list.
	 */
	final public void clear()
	{
		try
		{
			_data.clear();
		}
		catch( Throwable e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Prints the list on console for debugging.
	 */
	final public void displayConsole()
	{
		System.out.println( "Data representation : " );

		for ( T e : _data )
		{
			System.out.println( "- " + e.getDescription() );
		}
	}


	/**
	 * Returns true if the plugin passed in parameter has declared some behaviors.
	 * Else returns false.
	 * 
	 * @param pluginName
	 * @return true if the plugin passed in parameter has declared some behaviors else returns false.
	 */
	final protected boolean pluginPresent( String pluginName )
	{
		for (T be : _data )
		{
			if ( be.getPluginName().equals( pluginName ) )
			{
				return true;
			}
		}

		return false;
	}
}
