/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.utils;

import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;
import fr.inria.aoste.trace.relation.IDescription;

public class FilterRelationEntity implements IFilter<RelationEntity> {

	final IFilter<IDescription> filterIDescription;

	public FilterRelationEntity(IFilter<IDescription> filterIDescription) {
		this.filterIDescription = filterIDescription;
	}

	@Override
	public boolean accept(RelationEntity ce) {
		return filterIDescription.accept(ce.getRelation());
	}
}