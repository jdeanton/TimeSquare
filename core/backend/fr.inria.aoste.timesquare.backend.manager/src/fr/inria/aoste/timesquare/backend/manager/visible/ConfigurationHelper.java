/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationBehaviorEntity;
import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.trace.PhysicalBase;


/**
 * This class is instantiated and given to a plugin when it has to do some operations.
 * It offers the plugins common methods helping them to manage these behaviors. 
 * 
 * @author dlengell
 *
 */
public class ConfigurationHelper
{
	private Controller _controller;

	/**
	 * A constructor for this class.
	 * 
	 * @param controller
	 */
	public ConfigurationHelper( Controller controller )
	{
		assert(controller!=null);
		_controller = controller;
	}

	/**
	 * Adds a ClockBehavior.
	 * 
	 * @param clockEntity
	 * @param clockActivationState
	 * @param pluginName
	 * @param behavior
	 * @param persistentOptions
	 */
	public void addBehavior( ClockEntity clockEntity, ClockActivationState clockActivationState, String pluginName, ClockBehavior behavior, PersistentOptions persistentOptions )
	{
		if ( clockEntity != null && clockActivationState != null && pluginName != null && behavior != null )
		{
			_controller.addEntity(new ClockBehaviorEntity( clockEntity, clockActivationState, pluginName, behavior, persistentOptions));
		}
	}

	public void removeBehavior(ClockEntity clockEntity, ClockActivationState activationState, ClockBehavior behavior) {
		/* It is necessary to find the ClockBehaviorEntity which was build at the time the
		 * behavior was registered (in addBehavior()). Otherwise, the delete operation will not
		 * work. */
		ClockBehaviorEntity foundCbe = null;
		for (ClockBehaviorEntity cbe : _controller.getDsm().getClockBehaviorEntity()) {
			if (cbe.getClockEntity().equals(clockEntity)
					&& cbe.getClockActivationState().equals(activationState)
					&& cbe.getBehavior().equals(behavior)) {
				foundCbe = cbe;
				break;
			}
		}
		if (foundCbe != null) {
			_controller.deleteEntity(foundCbe);
		}
	}

	/**
	 * Adds a RelationBehavior.
	 * 
	 * @param relation
	 * @param pluginName
	 * @param behavior
	 * @param persistentOptions
	 */
	public void addBehavior( RelationActivationState relation, String pluginName, RelationBehavior behavior, PersistentOptions persistentOptions )
	{
		if (pluginName != null)
		{
			_controller.addEntity(new RelationBehaviorEntity( relation, pluginName, behavior, persistentOptions));
		}
	}

	public void addBehavior( ClockEntity clockEntity, AssertActivationState clockActivationState, String pluginName, ClockBehavior behavior, PersistentOptions persistentOptions )
	{
		if ( clockEntity != null && clockActivationState != null && pluginName != null && behavior != null )
		{
			_controller.addEntity(new AssertBehaviorEntity( clockEntity, clockActivationState, pluginName, behavior, persistentOptions ));      
		}
	}
	public List< ClockEntity > getAssert()
	{    
		return _controller.getAssert();
	}


	public List<RelationEntity> getRelationEntities() {
		return _controller.getRelationEntities();
	}

	public List<EObject> getRelationConstraint()
	{
		return _controller.getRelationConstraint();
	}

	public List<EObject> getCcslConstraints()
	{
		return _controller.getCcslConstraints();
	}

	/**
	 * Returns the model's clocks.
	 * 
	 * @return A List<ClockEntity> which contains the model's clocks.
	 */
	public List< ClockEntity > getClocks()
	{    
		return _controller.getClocks();
	}

	/**
	 * Returns the ClockEntity whose clockID is given as the parameter
	 * 
	 * @param the ID of the clock associated to the desired ClockEntity
	 * @return A ClockEntity which whose clockID is given as the parameter
	 */
	public ClockEntity getClock(String ID)
	{    
		for(ClockEntity ce : _controller.getClocks()){
			if (ce.getID().compareTo(ID) == 0){
				return ce;
			}
		}
		return null; 
	}

	public ClockEntity getClock(EObject eo)
	{    
		for(ClockEntity ce : _controller.getClocks()){
			if (ce.getClock()==eo){
				return ce;
			}
			if (ce.getModelElementReference()==eo)
			{
				return ce;
			}
		}
		return null; 
	}


	/**
	 * Returns the number of possible states.
	 * 
	 * @return An int which is the number of possible states.
	 */
	public int getStateNumber()
	{
		return ClockActivationState.stateNumber;
	}

	/**
	 * Returns an always clock state.
	 * 
	 * @return An ClockActivationState for all state.
	 */
	public ClockActivationState getAlwaysState()
	{
		boolean[] tab = 
			{
				true, 	//ticks
				true, 	//doesn't tick
				true, 	//must tick
				true,	//cannot tick
				true,	//is free
				true, 	//is undetermined
				true,	//isDead
				true 	// isnotDead
			};

		return new ClockActivationState( tab );
	}

	/**
	 * Returns a ticks clock state.
	 * 
	 * @return An ClockActivationState tick state.
	 */
	public ClockActivationState getTicksState()
	{
		boolean[] tab = 
			{
				true, 	//ticks
				false, 	//doesn't tick

				true, 	//must tick
				false,	//cannot tick
				true,	//is free
				true, 	//is undetermined

				true,	//isDead
				true 	// isnotDead
			};

		return new ClockActivationState( tab );
	}

	/**
	 * Returns a doesn't tick clock state.
	 * 
	 * @return An ClockActivationState doesn't tick state.
	 */
	public ClockActivationState getDoesntTickState()
	{
		boolean[] tab = 
			{
				false, 	//ticks
				true, 	//doesn't tick
				false, 	//must tick
				true,	//cannot tick
				true,	//is free
				true, 	//is undetermined
				true,	//isDead
				true 	// isnotDead
			};

		return new ClockActivationState( tab );
	}

	/**
	 * Creates a ClockActivationState with a boolean array.
	 * 
	 * @param state
	 * @return The ClockActivationState created with the boolean array passed in parameter.
	 */
	public ClockActivationState createClockState( boolean[] state )
	{
		return new ClockActivationState( state );
	}

	public List<PhysicalBase> getPhysicalBases()
	{		
		return _controller.getPhysicalBases();		
	}

	/**
	 * Deletes all the Entities which was created by the plugin name passed in parameter.
	 * 
	 * @param pluginName
	 */
	public void deleteEntitiesByPluginName( String pluginName )
	{
		_controller.deleteEntitiesByPluginName( pluginName );
	}

	/**
	 * Returns the IPath of the CCSL model used.
	 * 
	 * @return The IPath of the CCSL model.
	 */
	public IPath getModelPath()
	{
		return _controller.getModelPath();
	}

	public void print(String s) {
		if ( cs!=null)
		{
			cs.printStdMessage(s);
		}
	}

	public void println(String s) {
		if ( cs!=null)
		{
			cs.printStdMessageln(s);
		}

	}

	public void printlnError(String s) {
		if ( cs!=null)
		{
			cs.printErrMessageln(s);
		}
	}

	public void printlnError(String s, Throwable e)
	{
		if ( cs!=null)
		{
			cs.printErrMessageln(s,e);
		}
	}

	private ConsoleSimulation cs;

	public synchronized final void setCs(ConsoleSimulation cs) {
		this.cs = cs;
	}
	
	public InputStream getInputStream(){
		return cs.getInput();
	}
} 
