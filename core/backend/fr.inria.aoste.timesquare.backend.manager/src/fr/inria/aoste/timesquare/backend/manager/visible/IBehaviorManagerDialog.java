/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import org.eclipse.core.runtime.IStatus;

public interface IBehaviorManagerDialog {

	/**
	 * Update the status of the ok button to reflect the given status. Subclasses
	 * may override this method to update additional buttons.
	 * @param status
	 */
	public abstract void updateStatus(IStatus status);
	
	
	public abstract BehaviorManagerGUI getBehaviorManagerGUI() ;

	
	/**
	 * Returns the name of your plugin. 
	 * This method is important because this name acts as an ID.
	 * It is the first method called by the plugin fr.inria.aoste.timesquare.backend.manager.
	 * 
	 * @return BehaviorManager.getPluginName()
	 */

	public abstract String getPluginName();

}