/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible.node;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import fr.inria.aoste.timesquare.backend.manager.Activator;


public final class NodeLabelProvider extends CellLabelProvider { 
	
	public NodeLabelProvider() {
		super();
		try
		{
		 image=AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID, "icons/tsq.gif").createImage();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {

		return false;
	}

	@Override
	public void dispose() {

	}

	public String getText(Object element) {
		if (element instanceof Node<?>) {
			return ((Node<?>) element).name;
		}
		if (element != null) {
			return element.toString();
		}
		return null;
	}

	Image image=null;
	public Image getImage(Object element) {
		if (element instanceof Node<?>) {
			if (((Node<?>) element).value != null)
				return image;
			else
				return null;
		}
		return null;
	}

	@Override
	public String getToolTipText(Object element) {
		return "Tooltip (" + getText(element) + ")";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ViewerLabelProvider#getTooltipShift(java
	 * .lang.Object)
	 */
	@Override
	public Point getToolTipShift(Object object) {
		return new Point(5, 5);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ViewerLabelProvider#getTooltipDisplayDelayTime
	 * (java.lang.Object)
	 */
	@Override
	public int getToolTipDisplayDelayTime(Object object) {
		return 1000;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ViewerLabelProvider#getTooltipTimeDisplayed
	 * (java.lang.Object)
	 */
	@Override
	public int getToolTipTimeDisplayed(Object object) {
		return 3000;
	}

	@Override
	public void update(ViewerCell cell) {
		cell.setText(getText(cell.getElement()));
		cell.setImage(getImage(cell.getElement()));
	}
}