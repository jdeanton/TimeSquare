/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorOutputControler;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.relation.IDescription;

/**
 * The OutputOption extends the fr.inria.aoste.ccslmodel.launcher.extensionpoint.IOutputOption defined in the launcher.
 * The options contains an unique String which is the serialized configuration of all behaviors defined by the
 * user.
 * 
 * @author dlengell
 *
 */
public class OutputOption extends IOutputOption
{

    private String _stringOptions;
    
    public OutputOption( BehaviorOutputControler behaviorOutputControler)
    {
    	super(behaviorOutputControler.getKey(), behaviorOutputControler.getName());
        _stringOptions = "";        
      //  activebydefault=false;
        setActive(false);
        _controller= new Controller(behaviorOutputControler,this);
    }
    
    /**
     * Returns the serialized configuration.
     * 
     * @return the configuration serialized in a XML String.
     */
    public String getStringOptions()
    {
        _stringOptions=getFlag( "option" );
        return _stringOptions;
    }
    
    /**
     * Sets a serialized configuration.
     * 
     * @param stringOptions
     */
    public void setStringOptions( String stringOptions )
    {
        _stringOptions = stringOptions;
        setFlag( "option", _stringOptions );
    }
    
    
    public void setDefault()
	{
		setActive(false);
		setStringOptions("");		
	}
	
   // final 
    private Controller _controller;

	public Controller getController() 
	{
		return _controller;
	}

	
	@Override
	public BehaviorConfigurator<?> getConfigurator() {		
		return _controller.getConfigurator() ;
	}	
	
	/*@Override
	protected boolean _isActivable(ReportMessage rm) {
		return _controller.isActivable( rm,getCcslhelper());
	}*/
	
	public static class CacheisActivable
	{
		boolean _isactivable;
		String message="";
		int ccslinfoevolution=-1; 
		CCSLInfo ccslInfo=null;
	}
			
	CacheisActivable cacheisActivable=null;	
		
	@Override
	public boolean _isActivable(ReportMessage rm) {
		CCSLInfo ccslhelper=getCcslhelper();
		if (cacheisActivable==null || cacheisActivable.ccslInfo!=ccslhelper || cacheisActivable.ccslinfoevolution!=ccslhelper.getCCSLInfoEvolution())
		{
			cacheisActivable= new CacheisActivable();
			cacheisActivable._isactivable=_controller.isActivable(rm, ccslhelper);
			cacheisActivable.ccslInfo=ccslhelper;
			cacheisActivable.message=rm.getMessage();
			cacheisActivable.ccslinfoevolution=ccslhelper.getCCSLInfoEvolution();
			return cacheisActivable._isactivable;
		}
		rm.setMessage(cacheisActivable.message);
		return  cacheisActivable._isactivable;
	}

	@Override
	public String validate() {		
		return _controller.validate();
	}
	
	private static class CacheOption
	{
		
		IFile model ;
		List<ModelElementReference> lsteo ;
		List<ModelElementReference> lsassert ;
	    List<IDescription> ldesc;
	    
		private CacheOption(IFile model, List<ModelElementReference> lsteo, List<ModelElementReference> lsassert,
				List<IDescription> ldesc) {
			super();
			this.model = model;
			this.lsteo = lsteo;
			this.lsassert = lsassert;
			this.ldesc = ldesc;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;			
			result = prime * result + ((ldesc == null) ? 0 : ldesc.hashCode());
			result = prime * result + ((lsassert == null) ? 0 : lsassert.hashCode());
			result = prime * result + ((lsteo == null) ? 0 : lsteo.hashCode());
			result = prime * result + ((model == null) ? 0 : model.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CacheOption other = (CacheOption) obj;
			
			if (ldesc == null) {
				if (other.ldesc != null)
					return false;
			} else if (!ldesc.equals(other.ldesc))
				return false;
			if (lsassert == null) {
				if (other.lsassert != null)
					return false;
			} else if (!lsassert.equals(other.lsassert))
				return false;
			if (lsteo == null) {
				if (other.lsteo != null)
					return false;
			} else if (!lsteo.equals(other.lsteo))
				return false;
			if (model == null) {
				if (other.model != null)
					return false;
			} else if (!model.equals(other.model))
				return false;
			return true;
		}
		
	    
	}
	CacheOption option=null;
	
	@Override
	public void updateModel() {
			IFile model = getModel();
			List<ModelElementReference> lsteo = getListofClock();
			List<ModelElementReference> lsassert = getListofAssert();
			List<IDescription> ldesc=getListofRelation();
			
			
			CacheOption _option = new CacheOption(model, lsteo, lsassert, ldesc);
			if( _option.equals(option))
			{
				return ;
			}			
			option=_option;
	        IPath modelPath = model.getLocation();
	        List< ClockEntity > clockEntities = new ArrayList< ClockEntity >();
	        if ( lsteo != null )
	        {
	            for ( ModelElementReference eo : lsteo )
	            {
	                clockEntities.add( new ClockEntity( eo ) );
	            }
	            Collections.sort(clockEntities);
	        }
	        List< ClockEntity > assertEntities = new ArrayList< ClockEntity >();
	        if ( lsassert != null )
	        {
	            for ( ModelElementReference eo : lsassert )
	            {
	            	assertEntities.add( new ClockEntity( eo ) );
	            }
	        }
	        List< RelationEntity > relationEntities = new ArrayList< RelationEntity >();
	        if ( ldesc != null )
	        {
	            for ( IDescription desc : ldesc )
	            {
	            	relationEntities.add( new RelationEntity( desc ) );
	            }
	        }
        	_controller.setModelPathAndClocks( modelPath, clockEntities ,assertEntities,relationEntities);  
        	
	    }
	
	@Override
	public void restore() {
		_controller.restoreConfigurations(this);
	}
}
