/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;


/**
 * This class is instantiated and given to a plugin when a relation behavior is executed.
 * It offers the plugins common operations helping them manipulating the relation model. 
 * 
 * @author dlengell
 *
 */
public class RelationHelper
{
    
    private OccurrenceRelation _relation;

    /**
     * A constructor which takes a Relation in parameter.
     * 
     * @param relation
     */
    public RelationHelper( OccurrenceRelation relation )
    {
        _relation = relation;
    }
    
    /**
     * Returns the Relation which activated the RelationBehavior.
     * 
     * @return The Relation which activated the RelationBehavior.
     */
    public OccurrenceRelation getRelation()
    {
        return _relation;
    }
}
