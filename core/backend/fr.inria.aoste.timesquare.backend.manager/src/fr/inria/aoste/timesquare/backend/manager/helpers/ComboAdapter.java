/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.helpers;

import java.util.List;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;
import fr.inria.aoste.timesquare.backend.manager.visible.GUIHelper;


/**
 * This class is an SelectionAdapter for the GUIHelper display list function.
 * 
 * @author dlengell
 *
 */
public class ComboAdapter extends SelectionAdapter
{
	private List<?> _listObject;
	private BehaviorManagerGUI _gui;
	
	public ComboAdapter(List<?> _listObject, BehaviorManagerGUI _gui) {
		super();
		this._listObject = _listObject;
		this._gui = _gui;
	}

	public void widgetSelected( SelectionEvent arg0 )
	{
		String s = ( ( Combo ) arg0.widget ).getText();
		
		for ( Object o : _listObject )
		{
			if ( o.toString().equals( s ) )
			{
			    ( ( Combo ) arg0.widget ).setData(GUIHelper.COMBOSELECTION, o );
			}
		}
		if ( _gui!=null)
			_gui.updateOKStatus();
		return;
	}
}
