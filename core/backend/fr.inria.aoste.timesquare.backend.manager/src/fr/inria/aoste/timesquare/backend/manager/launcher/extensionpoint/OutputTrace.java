/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.backend.manager.Activator;
import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.InitOutputData;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;
import fr.inria.aoste.timesquare.launcher.extensionpoint.OutputCommon;
import fr.inria.aoste.trace.AssertionState;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.Reference;
import fr.inria.aoste.trace.TracePackage;

/**
 * The OutputTrace listens to the launcher during the execution and get the
 * Trace model. It parses the Trace model and tells the controller information
 * used to execute behaviors.
 * 
 * @author dlengell
 * 
 */
public class OutputTrace extends OutputCommon // implements IOutputTrace
{
	private Controller _controller = null;// new Controller();
	private OutputOption options = null;

	String key = Activator.keyoutput;

	// static AdapterRegistry adapterRegistry=AdapterRegistry.getDefault();

	/**
	 * @author fmallet
	 * @param key BehavioursOuputControler's key
	 * @param name BehaviourOutputControler's id
	 */
	public OutputTrace(String key, String name) {
		super(name);
		this.key = key;
	}

	public void aFinalStep(int step) {
		if (_controller != null) {
			_controller.end();
		}
	}

	@Override
	protected void aNewStep(LogicalStep step, boolean timed) {
		if (step == null || _controller == null) {
			return;
		}
		_controller.aNewStep(step.getStepNumber(), timed, step);
		analyzeNewStep(step);
		_controller.aPostNewStep(step.getStepNumber(), timed, step);
	}

	public void aStep(LogicalStep step, boolean timed) {
		if (step == null || _controller == null) {
			return;
		}
		_controller.aStep(step.getStepNumber(), timed, step);
		analyzeStep(step);

	}

	public OutputOption getOption() {
		return options;
	}

	public String getkey() {
		return key;
	}

	public void initAndOutputFile(InitOutputData data,ISolverForBackend solver) throws Throwable {

		ConsoleSimulation cs = options.getCs();
		_controller.beforeExecution(data, cs,solver);

		// init(data.isRuntimein(), data.getLst());
	}

	/*  public void init( boolean runtimein ,List<PhysicalBase > lst)
	  {
	  	//TODO controler   _controller = Controller.getSingletonInstance();
	  }*/

	public void killTimer() {
	}

	public void setOption(IOutputOption o) throws Throwable {
		if (o == null || o.getIFile() == null || o.getListofClock() == null) {
			throw new Error("argument of setOption( IOutputOption o ) are null ");
			// return;
		}

		// OutputOption
		options = (OutputOption) o;
		// _controller = Controller.getSingletonInstance();
		_controller = options.getController();
		/*	IPath modelPath = o.getIFile().getFullPath();

			List<ClockEntity> clockEntities = new ArrayList<ClockEntity>();

			// if ( o.getListofClock() != null )
			{
				for (EObject eo : o.getListofClock()) {
					clockEntities.add(new ClockEntity(eo));
				}
			}
			List<AssertEntity> assertEntities = new ArrayList<AssertEntity>();
			if (o.getListofAssert() != null) {
				for (EObject eo : o.getListofAssert()) {
					assertEntities.add(new AssertEntity(eo));
				}
			}*/

		// _controller.setModelPathAndClocks(modelPath, clockEntities,
		// assertEntities);

		// _controller.restoreConfigurations(options);
	}

	public int terminate() {
		if (_controller != null) {
			// TODO controler finish _controller.end();
		}
		return 0;
	}

	/**
	 * Analyzes the current step passed in parameter. Tells to the Controller
	 * the information needed to execute behaviors.
	 * 
	 * @param step
	 */
	private void analyzeNewStep(LogicalStep step) {
		if (step == null) {
			return;
		}

		ClockEntity clockEntity;
		ClockEntity assertEntity;
		EList<EventOccurrence> list = step.getEventOccurrences();
		for (EventOccurrence cs : list) {

			Reference r = cs.getReferedElement();
			// TODO change
			if (r instanceof ModelElementReference) {
				// eo = ( ( ModelElementReference ) r).getElementRef().get(0);
				// if (adapterRegistry.accept(eo))
				{
					clockEntity = new ClockEntity((ModelElementReference) r);
					_controller.executeNewBehaviors(clockEntity, cs);
				}
			}
		}
		if (step.eIsSet(TracePackage.eINSTANCE.getLogicalStep_AssertionStates()))
			// test si step.getAssertionStates() !=0		
		{
			EList<AssertionState> list2 = step.getAssertionStates();
			for (AssertionState cs : list2) {

				Reference r = cs.getReferedElement();
				// TODO change
				if (r instanceof ModelElementReference) {
					assertEntity = new ClockEntity((ModelElementReference) r);
					_controller.executeNewBehaviors(assertEntity, cs);
				}
			}
		}
	}

	private void analyzeStep(LogicalStep step) {
		if (step == null) {
			return;
		}

		// EObject eo;
		ClockEntity clockEntity;

		EList<EventOccurrence> list = step.getEventOccurrences();
		for (EventOccurrence cs : list) {

			Reference r = cs.getReferedElement();
			// TODO change
			if (r instanceof ModelElementReference) {
				// eo = ( ( ModelElementReference ) r).getElementRef().get(0);
				// if (AdapterRegistry.getDefault().accept(r))
				{
					clockEntity = new ClockEntity((ModelElementReference) r);
					_controller.executeBehaviors(clockEntity, cs);
				}

			}
		}
	}

	@Override
	protected void aNewRelation(List<EObject> lrelation) {
		if (_controller != null) {
			if (lrelation != null) {
				for (EObject relation : lrelation) {
					if (relation instanceof OccurrenceRelation) {
						_controller.executeBehaviorsRelation((OccurrenceRelation) relation);
					}
				}
			}
		}
	}

}
