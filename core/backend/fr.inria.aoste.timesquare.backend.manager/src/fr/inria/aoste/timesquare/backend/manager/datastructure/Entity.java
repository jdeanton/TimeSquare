/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure;

import fr.inria.aoste.timesquare.backend.manager.visible.Behavior;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

/**
 * This abstract class re-groups common attributes of the different entities.
 * 
 * @author dlengell
 *
 */
public abstract class Entity
{

	protected String _pluginName;
	protected PersistentOptions _persistentOptions;
	
	/**
	 * A constructor for this class.
	 */
	public Entity()
	{
		_pluginName = null;
		_persistentOptions = null;
	}
	
	/**
	 * A constructor for this class with parameters.
	 * 
	 * @param pluginName
	 * @param persistentOptions
	 */
	public Entity( String pluginName, PersistentOptions persistentOptions )
	{
		_pluginName = pluginName;
		_persistentOptions = persistentOptions;
	}
	
	/**
	 * Returns the plugin name.
	 * 
	 * @return The plugin name.
	 */
	public String getPluginName()
	{
		return _pluginName;
	}

	/**
	 * Sets a plugin name.
	 * 
	 * @param pluginName
	 */
	public void setPluginName( String pluginName )
	{
		_pluginName = pluginName;
	}
	
	/**
	 * Returns the PersistentOptions.
	 * 
	 * @return The PersistentOptions.
	 */
	public PersistentOptions getPersistentOptions()
	{
		return _persistentOptions;
	}
	
	/**
	 * Sets a PersistentOptions
	 * 
	 * @param persistentOptions
	 */
	public void setPersistentOptions( PersistentOptions persistentOptions )
	{
		_persistentOptions = persistentOptions ;
	}
	
	/**
	 * Returns a description of the Entity.
	 * 
	 * @return A String description of the Entity.
	 */
	public String getDescription()
	{
		return _pluginName;
	}
	
	@Override
	public int hashCode() {
		
		return _pluginName.hashCode();
	}


	
	public abstract Behavior getBehavior();
	
	/**
     * Redefines the equals method of Object.
     * This method is very important when we delete/modify an Entity of the DataStructure (so a behavior).
	 */
	public boolean equals( Object o )
	{
        if ( o == this )
        {
            return true;
        }
        
        if ( o instanceof Entity )
        {
            Entity e = ( Entity ) o;
            
            if ( _pluginName.equals( e.getPluginName() ) )
            {
                return true;
            }
        }

        return false;
	}
}
