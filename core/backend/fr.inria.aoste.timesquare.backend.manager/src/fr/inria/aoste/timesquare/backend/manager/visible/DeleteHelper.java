/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationBehaviorEntity;

public class DeleteHelper
{		
	Controller controller=null;

	public  DeleteHelper(Controller controller) {
		super();
		this.controller = controller;
	}
	
	/**
	 * Delete entity
	 * @param entity
	 */
	public void deleteEntity(ClockBehaviorEntity entity)
	{
		controller.deleteEntity(entity);		
	}
	
	/**
	 * 
	 * @return copy of list of AssertBehaviorEntity
	 */
	public List<AssertBehaviorEntity> getAssertBehaviorEntity()
	{
		return new ArrayList<AssertBehaviorEntity>(controller.getDsm().getAssertBehaviorEntity());		
	}
	
	/**
	 * 
	 * @return copy of list of ClockBehaviorEntity
	 */
	public List<ClockBehaviorEntity> getClockBehaviorEntity()
	{
		return new ArrayList<ClockBehaviorEntity>(controller.getDsm().getClockBehaviorEntity());		
	}
	
	
	/**
	 * 
	 * @return copy of list of RelationBehaviorEntity
	 */
	public List<RelationBehaviorEntity> getRelationBehaviorEntity()
	{
		return new ArrayList<RelationBehaviorEntity>(controller.getDsm().getRelationBehaviorEntity());		
	}
	
	
}