/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure.relation;

import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.serialization.relation.RelationBehaviorPersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;

/**
 * 
 * This class is manipulated by the RelationBehaviorList.
 * It contains all the information about a relation behavior.
 *
 * @author dlengell
 *
 */
public class RelationBehaviorEntity extends Entity
{
    
    private RelationActivationState _relation;
    private RelationBehavior _behavior;
    
    /**
     * A constructor for this class. 
     */
    public RelationBehaviorEntity()
    {
        _relation = null;
        _behavior = null;
    }
    
    /**
     * A constructor for this class with parameters.
     * 
     * @param relation
     * @param pluginName
     * @param behavior
     * @param persistentOptions
     */
    public RelationBehaviorEntity( RelationActivationState relation, String pluginName, RelationBehavior behavior, PersistentOptions persistentOptions )
    {
        super( pluginName, persistentOptions );
        _behavior = behavior;
        _relation = relation;
    }

    /**
     * Returns the RelationActivationState.
     * 
     * @return The RelationActivationState.
     */
    public RelationActivationState getRelationActivationState()
    {
        return _relation;
    }
    
    /**
     * Sets a RelationActivationState.
     * @param relation
     */
    public void setRelationActivationState( RelationActivationState relation )
    {
        _relation = relation;
    }

    /**
     * Returns the RelationBehavior.
     * 
     * @return Returns the RelationBehavior.
     */
    public RelationBehavior getBehavior()
    {
        return _behavior;
    }
    
    /**
     * Sets a RelationBehavior.
     * 
     * @param behavior
     */
    public void setBehavior( RelationBehavior behavior )
    {
        _behavior = behavior;
    }
    
    /**
     * Transforms the RelationBehaviorEntity into an RelationBehaviorPersistentEntity which can be serialized.
     * 
     * @return The RelationBehaviorPersistentEntity obtained from this RelationBehaviorEntity.
     */
    public RelationBehaviorPersistentEntity transformEntityIntoPersistentEntity()
    {
        //Nothing to serialize.
        if ( _persistentOptions == null )
        {
            return null;
        }
        
        return new RelationBehaviorPersistentEntity(_pluginName, 
        		_persistentOptions, _relation.getID());
    }
    
    /**
     * Returns a description of the RelationBehaviorEntity.
     * 
     * @return The String description of the RelationBehaviorEntity.
     */
    public String getDescription()
    {
        return _relation.getDescription()+" "+_behavior.getDescription()+" "+super.getDescription();
    }

    /**
     * Redefines the equals method of Object.
     * This method is very important when we delete/modify an RelationBehaviorEntity.
     *
     * @param o
     * @return true if this equals to the Object passed in parameter.
     */
    public boolean equals( Object o )
    {
        if ( o == this )
        {
            return true;
        }
        
        if ( o instanceof RelationBehaviorEntity )
        {
            RelationBehaviorEntity e = ( RelationBehaviorEntity ) o;
            
            if ( _relation.equals( e.getRelationActivationState() )
            		&& _behavior.behaviorEquals( e.getBehavior() ) 
            		&& super.equals( o ) )
            {
                return true;
            }
        }

        return false;        
    }
    
    @Override
	public int hashCode() {
		
		return super.hashCode()+1;
	}
    
}
