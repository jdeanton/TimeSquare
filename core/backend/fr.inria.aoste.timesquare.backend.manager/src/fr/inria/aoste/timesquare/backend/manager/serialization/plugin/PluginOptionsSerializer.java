/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization.plugin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import fr.inria.aoste.timesquare.backend.manager.serialization.OptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.SpecialObjectInputStream;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

public class PluginOptionsSerializer extends OptionsSerializer
{
	private Map< String, PersistentOptions > _data = new HashMap< String, PersistentOptions >();

	public Map< String, PersistentOptions > getData()
	{
		return _data;
	}

	public void addPluginOptions( String pluginName, PersistentOptions persistentOptions )
	{
		_data.put( pluginName, persistentOptions );
	}

	public boolean doesPluginNeedSerialization( String pluginName )
	{
		return _data.containsKey( pluginName );
	}

	public String getSerializedOptions( String pluginName )
	{
		PersistentOptions po = _data.get( pluginName );
		if ( po == null )
		{
			return "";
		}

		String result = "";

		//On serialize po et on le retourne sous forme de string.
		try
		{
			//On ouvre un flux vers un ByteArrayOutputStream
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			// création d'un "flux objet" avec le flux fichier
			ObjectOutputStream oos= new ObjectOutputStream( baos );
			try
			{
				// sérialisation : écriture de l'objet dans le flux de sortie
				oos.writeObject( po ); 
				// on vide le tampon
				oos.flush();
			}
			finally
			{
				//fermeture des flux
				try
				{
					oos.close();
				}
				finally
				{
					result = transformByteInString( baos.toByteArray() ); //On transorme le binaire en string la string.
					baos.close();
				}
			}
		}
		catch( IOException ioe )
		{
			ioe.printStackTrace();
		}

		return result;
	}


	public void deserializeOptions( String options, BehaviorManager behaviorManager )
	{
		if ( options == null || (options.length()==0 ) || behaviorManager == null)
		{
			return;
		}

		try
		{
			// ouverture d'un flux d'entrée depuis le fichier "personne.serial"
			ByteArrayInputStream bais = new ByteArrayInputStream( transformStringInByte( options ) );

			// création d'un "flux objet" avec le flux fichier
			SpecialObjectInputStream sois = new SpecialObjectInputStream( bais, behaviorManager.getClass().getClassLoader() );

			try
			{   
				// désérialisation : lecture de l'objet depuis le flux d'entrée

				PersistentOptions readObject = ( PersistentOptions ) sois.readObject();
				this.addPluginOptions( behaviorManager.getPluginName(), readObject );

			}
			finally
			{
				// on ferme les flux
				try
				{
					sois.close();
				}
				finally
				{
					bais.close();
				}
			}

		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}
}
