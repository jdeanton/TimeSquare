/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import org.eclipse.core.runtime.Status;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * The BehaviorManagerGUI is a graphical interface for a plugin.
 * 
 * If a plugin declares a BehaviorManagerGUI in the extension point, when an user selects this plugin in 
 * the view's list, the BehaviorManagerGUI will be display in a JFace Dialog.
 * 
 * The BehaviorManagerGUI offers to its children two helpers : ConfigurationHelper and GUIHelper.
 * By the same way, the children can access to the plugin's BehaviorManager.
 *  *  
 * 
 * @author dlengell,bferrero
 *
 */
public abstract class BehaviorManagerGUI 
{
    protected ConfigurationHelper _configurationHelper = null;
    private GUIHelper _guiHelper = new GUIHelper( this);
    private BehaviorManager _behaviorManager = null;
    
	protected IBehaviorManagerDialog  dialog;
    
    public BehaviorManagerGUI() {
		super();
		setHelper(new ConfigurationHelper(null));
	}

	/**
     * Sets a BehaviorManager.
     * 
     * @param behaviorManager
     */
    public void setBehaviorManager( BehaviorManager behaviorManager )
    {
        _behaviorManager = behaviorManager;
    }
    
    /**
     * Sets the helpers.
     * 
     * @param configurationHelper
     * @param guiHelper
     */
    public void setHelper( ConfigurationHelper configurationHelper) //, GUIHelper guiHelper )
    {
        _configurationHelper = configurationHelper;
        _guiHelper.setConfigurationHelper(configurationHelper);
    }
    
    /**
     * Returns the BehaviorManager.
     * 
     * @return
     */
    protected BehaviorManager getBehaviorManager()
    {
        return _behaviorManager;
    }
    
    
    /**
     * 
     * @return BehaviorManager.getPluginName()
     */
    public String getPluginName() {		
		return _behaviorManager.getPluginName();
	}
    
    /**
     * Returns the ConfigurationHelper.
     * 
     * @return
     */
    protected ConfigurationHelper getConfigurationHelper()
    {
        return _configurationHelper;
    }
    
    /**
     * Returns the GUIHelper.
     * 
     * @return
     */
    public GUIHelper getGUIHelper()
    {
        return _guiHelper;
    }
    
    /**
     * Create the dialog area in the Composite passed in parameter.
     * 
     * @param composite
     * @return the Control to display in the composite passed in parameter.
     */
    public abstract Control createDialogArea( Composite composite );
    
    /**
     * The button OK is pressed.
     */
    public abstract void okPressed();
    
    /**
     * The button Cancel is pressed.
     */   
    public abstract void cancelPressed();

	public void setDialog(IBehaviorManagerDialog dialog) {
		this.dialog = dialog;
	}
	
	public void updateOKStatus() {
		dialog.updateStatus(Status.OK_STATUS);
	}

	public IBehaviorManagerDialog getDialog() {
		return dialog;
	}

	public Point getMinimumSize() {	
		return new Point(350, 200);
	}
}
