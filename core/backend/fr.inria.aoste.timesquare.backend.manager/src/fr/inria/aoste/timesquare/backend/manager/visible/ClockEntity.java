/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.ReferenceNameBuilder;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.ModelElementReference;


/**
 * This represents a Clock.
 * It encapsulates a CCSL model clock.
 * 
 * It can be a :
 * 
 * fr.inria.aoste.umlCCSL.CCSLUMLmodel.Clock
 * fr.inria.aoste.timesquare.ccslkernel.model.Clock
 * fr.inria.aoste.timesquare.ccslkernel.model.expressionandrelation.Expression
 * 
 * @author dlengell
 *
 */
public class ClockEntity implements Comparable<ClockEntity>
{
    private final ModelElementReference _modelElementReference;
    private final String _URIfragment;
    private final String name , id;
     
    /**
     * A constructor with an EObject in parameter.
     * 
     * @param clock
     */
    public ClockEntity( ModelElementReference clock )
    {
        this._modelElementReference = clock;
        this._URIfragment = AdapterRegistry.getAdapter(clock).getUID(clock); 
        this.name = ReferenceNameBuilder.buildQualifiedName(_modelElementReference);
        this.id =  name + _URIfragment;
    }
    
    private String getURIfragment()
    {
        return _URIfragment;
    }
    
    /**
     * Returns the CCSL Model clock as an EObject.
     * 
     * @return The CCSL Model clock.
     */
    public EObject getClock()
    {	
    	EList<EObject> refs = _modelElementReference.getElementRef();
    	for (EObject ref : refs) {
    		if (AdapterRegistry.getAdapter(ref).isClock(ref))
    		{
    			return ref;
    		}
		}
		return null;   
    }
    
    public ModelElementReference getModelElementReference() {
		return _modelElementReference;
	}

	/**
     * Returns the ID.
     * 
     * @return A String containing the ID.
     */
    final public String getID()
    {
        return id;// this.getClockName()+this.getURIfragment();
    }
    
    /**
     * Returns the name. All characters that are not usable in a VCD are skipped. The sets of allowed chars are
     * currently all letters and digits plus the underscore '_' and minus sign '-'.
     * 
     * @return A String containing the name.
     */
    public String getName()
    {    
    	return name;
    }
    
    /**
     * Returns a String representation of the clock.
     * 
     * @return A String representation of the clock.
     */
    public String getDescription()
    {
        return this.getName()+" "+this.getURIfragment();
    }
    
    public String toString()
    {
        return this.getName();
    }
    
    /**
     * Returns the elements referenced by the clock in the CCSL model.
     * 
     * @return A List<EObject> which are the elements referenced by the clock in the CCSL model.
     */
    public List< EObject > getReferencedElement()
    {
    	return AdapterRegistry.getAdapter(_modelElementReference).
    			fillWithReferencedElements(_modelElementReference, null);
    }
    
    public boolean equals( Object o )
    {
    	try {
    		if ( o == this ) {
    			return true;
    		}
    		if ( o instanceof ClockEntity ) {
    			ClockEntity ce = ( ClockEntity ) o;
    			if ( modelElementReferenceEquality(ce._modelElementReference, this._modelElementReference) ) {
    				return true;
    			} 
    		}
    	}
    	catch (Throwable e) {

    	}
    	return false;
    }

    private boolean modelElementReferenceEquality(ModelElementReference mer1, ModelElementReference mer2) {
    	if (mer1 == mer2) {
    		return true;
    	}
    	if (mer1.getElementRef().size() != mer2.getElementRef().size()) {
    		return false;
    	}
    	for (int i = 0; i < mer1.getElementRef().size(); i++) {
    		if (mer1.getElementRef().get(i) != mer2.getElementRef().get(i)) {
    			return false;
    		}
    	}
    	return true;
    }
    
	@Override
	public int hashCode() {
		int n= name!=null? name.hashCode() : 0;
			int c=id!=null? id.hashCode() : 0;
		return n+c;
	}
	
	@Override
	public int compareTo(ClockEntity arg0) {
		return name.compareTo(arg0.name);
	}
}
