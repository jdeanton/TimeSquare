/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible.node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.ModelElementReference;



public class Node<T> {
	
	public static <T> List<T> sortList(List<T> lst, Comparator<T> comp) {
		List<T> sort = new ArrayList<T>(lst);
		Collections.sort(sort, comp);
		return sort;
	}

	public static List<Node<ClockEntity>> buildListNodesClockEntity(
			List<ClockEntity> elements) {		
		return buildListNodesClockEntity(elements, 0);
	}

	public static List<Node<RelationEntity>> buildListNodesRelationEntity(
			List<RelationEntity> elements) {	
		return buildListNodesRelationEntity(elements, 0);
	}

	protected static List<Node<RelationEntity>> buildListNodesRelationEntity(
			List<RelationEntity> elements, int n) {
		List<Node<RelationEntity>> lst = new ArrayList<Node<RelationEntity>>();
		List<RelationEntity> tmp = new ArrayList<RelationEntity>();

		for (int i = 0; i < elements.size(); i++) {
			RelationEntity re = elements.get(i);
			ModelElementReference mer = (ModelElementReference) (re
					.getRelation().getCcslConstraint());

			if (mer != null && mer.getElementRef().size() > n) {
				EObject eo = mer.getElementRef().get(n);
				EObject eo1 = null;
				if (((i + 1) != elements.size())) {
					RelationEntity re2 = elements.get(i + 1);
					ModelElementReference mer2 = (ModelElementReference) (re2
							.getRelation().getCcslConstraint());
					if (mer2.getElementRef().size() > n)
						eo1 = mer2.getElementRef().get(n);
				}
				if (eo1 != eo || eo1 == null) {
					tmp.add(re);
					List<Node<RelationEntity>> _lst = buildListNodesRelationEntity(
							tmp, n + 1);
					Node<RelationEntity> node = new Node<RelationEntity>(
							AdapterRegistry.getAdapter(eo).getReferenceName(eo),
							null);
					boolean b = true;
					for (Node<RelationEntity> nd : lst) {
						if (nd.getValue() != null)
							b = false;
					}
					node.skip = b;

					node.getLnode().addAll(_lst);
					lst.add(node);
					tmp.clear();
				} else {
					tmp.add(re);
				}
			} else {
				Node<RelationEntity> node = new Node<RelationEntity>(re
						.getRelation().getSubInfo(), re);
				lst.add(node);
			}
		}

		return lst;
	}
	
	protected static List<Node<ClockEntity>> buildListNodesClockEntity(
			List<ClockEntity> elements, int n) {
		List<Node<ClockEntity>> lst = new ArrayList<Node<ClockEntity>>();
		List<ClockEntity> tmp = new ArrayList<ClockEntity>();
		ClockEntity link = null;
		for (int i = 0; i < elements.size(); i++) {
			ClockEntity re = elements.get(i);
			ModelElementReference mer = (re.getModelElementReference());
			// if ( link==null)
			// link=re;
			if (mer != null && mer.getElementRef().size() > n) {
				EObject eo = mer.getElementRef().get(n);
				EObject eo1 = null;
				if (((i + 1) != elements.size())) {
					ClockEntity re2 = elements.get(i + 1);
					ModelElementReference mer2 = re2.getModelElementReference();
					if (mer2.getElementRef().size() > n)
						eo1 = mer2.getElementRef().get(n);
				}
				if (mer.getElementRef().size() == n + 1)
					link = re;
				if (eo1 != eo || eo1 == null) {
					tmp.add(re);
					List<Node<ClockEntity>> _lst = buildListNodesClockEntity(
							tmp, n + 1);
					String s = AdapterRegistry.getAdapter(eo).getReferenceName(eo);
					Node<ClockEntity> node = null;
					if (_lst.size() == 0)
						node = new Node<ClockEntity>(s, re);
					else
						node = new Node<ClockEntity>(s, link);
					if (n != 0)
						node.skip = true;
					node.getLnode().addAll(_lst);
					lst.add(node);
					tmp.clear();
					link = null;
				} else {
					tmp.add(re);
				}
			} else {
			}
		}

		return lst;
	}

	public static final class ComparatorNode<T> implements
			Comparator<Node<T>> {
		@Override
		public int compare(Node<T> o1, Node<T> o2) {
			if ((o1.value == null) & (o2.value != null))
				return 1;
			if ((o1.value != null) & (o2.value == null))
				return -1;
			return o1.name.compareTo(o2.name);
		}
	}

	List<Node<T>> lnode = new ArrayList<Node<T>>();

	public boolean skip = false;

	public Node() {
		super();
	}

	public Node(String name, T value) {
		super();
		this.name = name;
		this.value = value;
	}

	String name;
	T value = null;
	
	String tooptips = "";

	public boolean isSkip() {
		return skip;
	}

	public String getTooptips() {
		return tooptips;
	}

	public List<Node<T>> getLnode() {
		return lnode;
	}

	public String getName() {
		return name;
	}

	public T getValue() {
		return value;
	}

	public boolean haveValue() {
		return value != null;
	}

	public void optimise() {
		if (lnode != null) {
			optimise(lnode);

			if (lnode.size() == 1) {
				Node<T> nd = lnode.get(0);
				if (nd.skip && nd.value == null) {
					if (nd.name != null) {
						lnode.remove(nd);
						lnode.addAll(nd.lnode);
						for (Node<T> n : nd.lnode) {
							n.name = nd.name + "::" + n.name;
						}
						optimise(lnode);
					}

				}
			}

		}

	}

	public static <T> void optimise(List<Node<T>> lst) {
		Collections.sort(lst, new Node.ComparatorNode<T>());
		for (Node<T> node : lst) {
			node.optimise();
		}
	}

}