/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure.clock;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.backend.manager.datastructure.ABehaviorList;
import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehaviorAgain;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;
import fr.inria.aoste.trace.EventOccurrence;


/**
 * The ClockBehaviorList is a data structure which manipulates ClockBehaviorEntities.
 * It contains all the ClockBehaviorEntity and manages them.
 * 
 * @author dlengell
 *
 */
public class ClockBehaviorList extends ABehaviorList<ClockBehaviorEntity>
{
	public ClockBehaviorList() {
		super(new ArrayList< ClockBehaviorEntity >());
	}

	/**
	 * During the simulation, the ClockBehaviorList has to execute the behaviors associated to a clock and its state.
	 * This is done in this method. The controller provides a ClockEntity and a ClockState. The ClockBehaviorList checks in
	 * the data list if a behavior needs to be execute or not and calls a method of the ClockBehavior class.
	 * 
	 * @param clockEntity
	 * @param clockState
	 */
	public void executeNewBehaviors( ClockEntity clockEntity, EventOccurrence clockState )
	{
		if ( clockEntity == null || clockState == null)
		{
			return;
		}

		//ClockState => ClockActivationState
		//    ClockActivationState clockActivationState = new ClockActivationState( clockState ); 

		for ( ClockBehaviorEntity e : getData() )
		{
			if ( e.getClockEntity().equals( clockEntity ) )
			{
				if ( e.getClockActivationState().stateActivationOK( clockState ) )
				{
					e.getBehavior().run( new TraceHelper( clockState, clockEntity ) );
				}
				else
				{
					e.getBehavior().runWithWrongActivationState( new TraceHelper( clockState, clockEntity ) );
				}
			}
		}
	}


	/**
	 * During the simulation, the ClockBehaviorList has to execute the behaviors associated to a clock and its state.
	 * This is done in this method. The controller provides a ClockEntity and a ClockState. The ClockBehaviorList checks in
	 * the data list if a behavior needs to be execute or not and calls a method of the ClockBehavior class.
	 * 
	 * @param clockEntity
	 * @param clockState
	 */
	public void executeBehaviors( ClockEntity clockEntity, EventOccurrence clockState )
	{
		if ( clockEntity == null || clockState == null)
		{
			return;
		}

		//ClockState => ClockActivationState
		//  ClockActivationState clockActivationState = new ClockActivationState( clockState ); 

		for ( ClockBehaviorEntity e : getData() )
		{
			ClockBehavior cb= e.getBehavior();
			if (cb instanceof ClockBehaviorAgain)
			{
				ClockBehaviorAgain cba = (ClockBehaviorAgain) cb;
				if ( e.getClockEntity().equals( clockEntity ) )
				{
					//if ( ClockActivationState.stateActivationOK( e.getClockActivationState(), clockActivationState ) )
					if ( e.getClockActivationState().stateActivationOK( clockState ) )
					{
						cba.againRun( new TraceHelper( clockState, clockEntity ) );
					}
					else
					{
						cba.againRunWithWrongActivationState( new TraceHelper( clockState, clockEntity ) );
					}
				}
			}
		}
	}

	/**
	 * Checks the consistency of the current data with the clocks in parameters.
	 * If a ClockBehaviorEntity deals with a ClockEntity which is not in the list, the ClockBehaviorEntity is delete.
	 * 
	 * @param clocks
	 */
	public void checkConsistency( List< ClockEntity > clocks )
	{
		if ( clocks == null )
		{
			this.clear();
			return;
		}

		List< Entity > toDelete = new ArrayList< Entity >();
		for ( ClockBehaviorEntity e : getData() )
		{
			if ( !( clocks.contains( e.getClockEntity() ) ) )
			{
				toDelete.add( e );
			}
		}

		for ( Entity e : toDelete )
		{
			getData().remove( e );
		}
	}    
}
