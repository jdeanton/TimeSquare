/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint;

import org.eclipse.swt.widgets.Composite;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.view.View;
import fr.inria.aoste.timesquare.launcher.debug.model.output.OutputSetupOption;
import fr.inria.aoste.timesquare.launcher.extensionpoint.IOutputOption;

/**
 * The OutputSetup extends the import
 * fr.inria.aoste.ccslmodel.debug.model.output.OutputSetupOption defined in the
 * launcher.
 * 
 * Its role is to interact with the launcher GUI. It uses a OutputOption to
 * get/set persistent serialized configurations of behaviors.
 * 
 * 
 * @author dlengell
 * 
 */
public class OutputSetup extends OutputSetupOption {
	private View _view = null;
	private Controller _controller = null; // new Controller();

	// _controller = Controller.getSingletonInstance();

	public OutputSetup(String key, String name) {
		super(key, name, OutputOption.class);
	}

	protected void _initializeFrom(IOutputOption configuration) {
		super._initializeFrom(configuration);
		OutputOption options = (OutputOption) configuration;
		_controller = options.getController();
		_controller.setOutputOption(this);
		_controller.setView(_view);
		
			_controller.restoreConfigurations(options);

	}

	protected void _performApply(IOutputOption configuration) {
		super._performApply(configuration);
		if (_controller != null) {
			((OutputOption) configuration).setStringOptions(_controller
					.saveConfigurations((OutputOption) configuration));
			_controller.saveConfigurations((OutputOption) configuration);
		}
	}

	protected void createControl(Composite parent) {
		_view = new View(_controller, parent, this);
		_view.display();
	}

	public void dispose() {
		_controller = null;
		// Controller.clearSingletonInstance();
	}

}
