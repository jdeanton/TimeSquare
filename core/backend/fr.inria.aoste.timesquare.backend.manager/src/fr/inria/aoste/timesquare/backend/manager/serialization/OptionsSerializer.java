/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization;

import org.apache.commons.codec.binary.Base64;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;

/**
 * This is an abstract class which has to be implemented by serializers.
 * It offers services like the transformation of a byte array in a string and the reverse with
 * base64 encoding. 
 * 
 * @author dlengell
 *
 */
public abstract class OptionsSerializer
{

    /**
     * Indicates if the plugin has something to serialize.
     * 
     * @param pluginName
     * @return true if the plugin has something to serialize else returns false.  
     */
    public abstract boolean doesPluginNeedSerialization( String pluginName );
    
    /**
     * Get the serialized options from the plugin in a string.
     * 
     * @param pluginName
     * @return The serialized options in a String.
     */
    public abstract String getSerializedOptions( String pluginName );
    
    /**
     * Deserializes the string option passed in parameter for the BehaviorManager passed in parameter.
     * 
     * @param options
     * @param behaviorManager
     */
    public abstract void deserializeOptions( String options, BehaviorManager behaviorManager );
    
    /**
     * Transforms a byte array in a String.
     * 
     * @param tab
     * @return The byte array transformed in a String.
     */
    public String transformByteInString( byte[] tab )
    {
        String result = "";
        
        byte[] encoded = Base64.encodeBase64( tab ); //We use base64 encoding.
        
        try
        {
            result = new String( encoded, "ASCII" );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
        
        return result; 
    }
    
    /**
     * Transforms a string in an byte array.
     * 
     * @param s
     * @return The String transformed in a byte array.
     */
    public byte[] transformStringInByte( String s )
    {
        return Base64.decodeBase64( s.getBytes() );
    }

}
