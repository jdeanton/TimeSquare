/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible.node;

import java.util.Comparator;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.ModelElementReference;

public final class CompatorModelElementReference implements
		Comparator<ModelElementReference> {

	public CompatorModelElementReference() {
		super();
	}

	public CompatorModelElementReference(boolean sizetest) {
		super();
		this.sizetest = sizetest;
	}

	boolean sizetest = false;

	@Override
	public int compare(ModelElementReference mer1,
			ModelElementReference mer2) {
		try {
			if (mer1 == mer2)
				return 0;
			int i = 0;
			int size1 = mer1.getElementRef().size();
			int size2 = mer2.getElementRef().size();
			if (sizetest) {
				int tmp = size1 - size2;
				if (tmp != 0)
					return tmp;
			}
			while (true) {
				if (size1 <= i) {
					if (size2 <= i) {
						return 0;
					}
					return -1;
				}
				if (mer2.getElementRef().size() <= i) {
					return 1;
				}
				EObject eo1 = mer1.getElementRef().get(i);
				EObject eo2 = mer2.getElementRef().get(i);
				if (eo1 != eo2) {
					int n = AdapterRegistry
							.getAdapter(eo1)
							.getReferenceName(eo1)
							.compareTo(
									AdapterRegistry.getAdapter(eo2)
											.getReferenceName(eo2));
					if (n != 0)
						return n;
				}
				i++;
			}
			// return 0;
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw e;
		}

	}
}