/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputOption;
import fr.inria.aoste.timesquare.backend.manager.serialization.plugin.PluginOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.relation.RelationBehaviorOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;

/**
 * The XMLStringParser parses an XML String built by a XMLStringMaker.
 * It uses Serializers to deserialize the persistent objects contained in the String.
 * The persistent objects are put into the Serializers data structure.
 * 
 * @author dlengell
 *
 */
public class XMLStringParser {
	PluginOptionsSerializer _pluginOS = null;
	BehaviorOptionsSerializer _clockBehaviorOS = null;
	BehaviorOptionsSerializer _assertBehaviorOS = null;
	RelationBehaviorOptionsSerializer _relationBehaviorOS = null;
	//ExtensionPointManager _behaviorManagers = null;

	/**
	 * The constructor. It takes an ExtensionPointManager to get the present BehaviorManagers. The persistent objects will be deserialized
	 * by serializers and will be put into these serializers.
	 * 
	 * @param behaviorManagers
	 * @param pos
	 * @param cbos
	 * @param rbos
	 */
	public XMLStringParser( PluginOptionsSerializer pos, BehaviorOptionsSerializer cbos,
			RelationBehaviorOptionsSerializer rbos, BehaviorOptionsSerializer abos) {
		//_behaviorManagers = behaviorManagers;

		_pluginOS = pos;
		_clockBehaviorOS = cbos;
		_relationBehaviorOS = rbos;
		_assertBehaviorOS = abos;
	}

	/**
	 * Parses the XML String and calls the serializers's deserialize method with the good options.
	 * 
	 * @param xmlOptions
	 */
	public void parse(String xmlOptions, OutputOption option,Controller epm ) {
		// System.out.println("RD\n"+xmlOptions);
		if ( _pluginOS == null || _clockBehaviorOS == null || _relationBehaviorOS == null ) {
			return;
		}

		try {
			//ExtensionPointManager 
			//epm = new ExtensionPointManager();
			//for (BehaviorManager behaviorManager : epm.getManagers()) 
			BehaviorManager behaviorManager =epm.getBehaviorManager();
			{

				String id = behaviorManager.getId();
				//System.out.println(id);
				//System.out.println(option.getKeyTable());				
				String text=null;
				
				text= option.getFlag(id + ".pluginOptions");
				deserializePluginOptions(text, behaviorManager);
				
				text = option.getFlag(id + ".clockBehaviorOptions");					
				deserializeClockBehaviorOptions(text, behaviorManager);
				

				text = option.getFlag(id + ".relationBehaviorOptions");
				deserializeRelationBehaviorOptions(text, behaviorManager);
				
				text = option.getFlag(id + ".assertBehaviorOptions");					
				deserializeAssertBehaviorOptions(text, behaviorManager);
				

			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return;
	}


	private void deserializePluginOptions(String options, BehaviorManager behaviorManager) {
		if (options != null && options.length() != 0)
		{
		if (_pluginOS != null) {
			_pluginOS.deserializeOptions(options, behaviorManager);
		}
		}
	}

	private void deserializeClockBehaviorOptions(String options, BehaviorManager behaviorManager) {
		if (options != null && options.length() != 0)
		{
		if (_clockBehaviorOS != null) {
			_clockBehaviorOS.deserializeOptions(options, behaviorManager);
		}
		}
	}

	private void deserializeRelationBehaviorOptions(String options, BehaviorManager behaviorManager) {
		if (options != null && options.length() != 0)
		{
		if (_relationBehaviorOS != null) {
			_relationBehaviorOS.deserializeOptions(options, behaviorManager);
		}
		}
	}

	private void deserializeAssertBehaviorOptions(String options, BehaviorManager behaviorManager) {
		if (options != null && options.length() != 0)
		{
		if (_assertBehaviorOS != null) {
			_assertBehaviorOS.deserializeOptions(options, behaviorManager);
		}
		}
	}

}

