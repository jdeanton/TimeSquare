/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.utils;

import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter.EventEnumerator;

/***
 * 
 * @author bferrero
 *
 */
public enum EventEnumeratorPair {
	
	/**
	 * 
	 */
	StartFinish(EventEnumerator.START, EventEnumerator.FINISH),
	/**
	 * 
	 */
	SendRecieve(EventEnumerator.SEND,	EventEnumerator.RECEIVE),
	/**
	 * 
	 */
	ProduceConsume(EventEnumerator.PRODUCE, EventEnumerator.CONSUME);
	
	private EventEnumeratorPair(EventEnumerator src, EventEnumerator dst) {
		this.src = src;
		this.dst = dst;
	}
	
	private final EventEnumerator src, dst;


	public EventEnumerator getSource() {
		return src;
	}

	public EventEnumerator getDestination() {
		return dst;
	}

	public static EventEnumeratorPair getPair(EventEnumerator ee) {
		if (ee != null) {
			switch (ee) {
			case START:
			case FINISH:
				return StartFinish;
			case CONSUME:
			case PRODUCE:
				return ProduceConsume;
			case RECEIVE:
			case SEND:
				return SendRecieve;
			case UNDEFINED:
				return null;
			}
		}
		return null;
	}
	
	//public 
	
	
}