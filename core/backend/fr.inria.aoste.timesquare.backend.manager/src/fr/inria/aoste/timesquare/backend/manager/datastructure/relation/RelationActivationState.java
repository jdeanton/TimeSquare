/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure.relation;

import java.util.List;

import fr.inria.aoste.timesquare.backend.manager.serialization.relation.RelationBehaviorPersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;


/**
 * The RelationActivationState represents a activation state for a behavior on a relation. 
 * It recapitulates the information in the Relation model.
 * 
 * If the Relation model is modified, this class has to be updated.
 * 
 * The structure is a simple boolean array. The order is defined in a String array.
 * For example, if you want to create an activation state on all precedences and a coincidence :
 * 
 * [true]   <- coincidence
 * [false]  <- disjoint
 * [false]   <- packet
 * [true]  <- strictPrecedence
 * [true]   <- precedence
 * 
 * If you associate this RelationActivationState to a RelationBehavior (association done in the 
 * RelationBehaviorEntity), the behavior will be executed when a Coincidence or a Precedence(strict and 
 * non strict) is detected.
 * 
 * During the execution, the Relation detected transformed into a RelationActivationState.
 * This RelationActivationState is compared to the RelationActivationState defined by the plugins in the 
 * RelationBehaviorEntity. The comparison is done with the method :
 * <code>
 * public static boolean relationActivationOK( RelationActivationState relation1, RelationActivationState relation2 );
 * </code>
 * If the return value is true, the behavior is executed.
 * 
 * 
 * 
 * @author dlengell
 *
 */
public class RelationActivationState
{
   
  /*  public final static int relationNumber = 5;
    
   
    public final static String[] relationList = 
    { 
        "coincidence", 
        "disjoint", 
        "packet", 
        "strictPrecedence", 
        "precedence"
    };
    
    private boolean[] _relation = new boolean[]{};*/
    
    RelationEntity relationEntity=null;

	/**
     * A constructor for this class.
     * It takes an array of boolean in parameter.
     * This array has to be in the good format.
     * 
     * @param relation
     */    
  /*  public RelationActivationState( boolean[] relation )
    {
        _relation = null;

        if ( isValidRelation( relation ) )
        {
            _relation = relation.clone();
        }
    }*/
    
    public RelationActivationState(RelationEntity re) {
		relationEntity=re;
	}

	/**
     * A constructor for this class.
     * It takes a fr.inria.aoste.umlrelationmodel.CCSLRelationModel.Relation in parameter,
     * the Relation is transformed into a RelationEntity in the aim to compare its boolean array 
     * with another RelationEntity.
     * 
     * @param relation
     */
   /* private RelationActivationState( OccurrenceRelation relation )
    {
        if ( relation == null )
        {
            _relation = null;
            return;
        }
        
        boolean[] tmp = new boolean[ RelationActivationState.relationNumber ];
        for ( int i=0; i<tmp.length; ++i )
        {
            tmp[i] = false;
        }
        
        if ( relation instanceof Coincidence )
            tmp[0] = true;
      //  if ( relation instanceof RelationDisJoint )
      //      tmp[1] = true;
        if ( relation instanceof Packet )
            tmp[2] = true;
        if ( relation instanceof Precedence )
        {
            if ( ( ( Precedence )relation ).isIsStrict() )
            {
                tmp[3] = true;
            }
            else
            {
                tmp[4] = true;
            }
        }
            
        if ( isValidRelation( tmp ) )
        {
            _relation = tmp;
        }
    }*/
    
  
    public RelationActivationState(	RelationBehaviorPersistentEntity persistentEntity, List<RelationEntity> tmp ) 
    {
    	//_relation = persistentEntity.getRelation();
    	String s=persistentEntity.getRelationid();
    	for ( RelationEntity ce : tmp )
		{
    		
		    if (ce.getRelation()!=null && s.equals( ce.getRelation().getID() ) )
		    {
		       
		       relationEntity=ce;
		    }
		}
    	   
	}

	/**
     * Checks if the boolean array in parameter is a valid relation.
     * 
     * @param relation
     * @return true if the boolean array passed in parameter has the good format.
     */
   /* public boolean isValidRelation( boolean[] relation )
    {
        if ( relation == null )
        {
            return false;
        }
        
        if ( relation.length == RelationActivationState.relationNumber )
        {
            return true;
        }
        else
        {
            return false;
        }
    }*/
    
    /**
     * Returns the boolean array.
     * 
     * @return The boolean array.
     */
   /* public boolean[] getRelation()
    {
    	if (_relation==null)
        		return new boolean[]{};
        return _relation.clone();
    }*/
    
    public String getID()
    {   
    	if(relationEntity.getRelation()!=null )
    	{
    		return relationEntity.getRelation().getID();
    	}
    	return null;
    }	
    
    /**
     * Returns the description of the RelationEntity.
     * 
     * @return The String description of the RelationEntity.
     */
    public String getDescription()
    {   
    	//if( relationEntity!=null)
    	{
    		return relationEntity.getDescription();
    	}
       
    }
    
    @Override
	public int hashCode() {
	/*	int n=0;
		int k=1;
		for ( int i=0; i<_relation.length; ++i )
		{
			if ( _relation[i] )
			{
				n = n+ k;
			}
			k = k*2;
			if (k<=0) { k=1; }
		}
		return n;*/
    	return relationEntity.getDescription().hashCode();
	}
    /**
     * Redefines the equals method of Object.
     * This method is very important when we delete/modify an Entity of the DataStructure (so a behavior).
     */
    public boolean equals( Object o )
    {
        
        if ( o == this )
        {
            return true;
        }
        
        if ( o instanceof RelationActivationState )
        {
            RelationActivationState re = ( RelationActivationState ) o;
            
            if (getDescription().equals(re.getDescription()) )
            {
               
                return true;
            }
        }

        return false;
    }
    
    /**
     * Compare two RelationActivationStates and returns true if the current relation (parameter relation1) 
     * detected during the execution is accepted by the RelationActivationState of a second relation 
     * (parameter relation2) defined in a RelationBehaviorEntity.
     * 
     * Returns true if : ( relation1 ) logical and ( relation2 ) equals to ( relation2 ).
     * 
     * @param relation1
     * @param relation2
     * @return Returns true if : ( relation1 ) logical and ( relation2 ) equals to ( relation2 ).
     */
    public  boolean relationActivationOK( OccurrenceRelation relation )
    {
    	
    		return relationEntity.getRelation().check(relation);    	
    }
    
   
}
