/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.controller.dealers;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;

/**
 * This is an abstract class. It's an option dealer and its role 
 * is to give the serialized options to the plugins and get the behaviors in return
 * in the aim to re-do all the behaviors configuration.
 * 
 * @author dlengell
 *
 */
public abstract class OptionsDealer
{
	
	protected Controller _controller;
	
	/**
	 * A constructor for this class.
	 * 
	 * @param behaviorManagers
	 * @param controler
	 */
	public OptionsDealer(  Controller controler  )
	{
	
		_controller = controler;
	}
	
	/**
	 * This method has to be implemented by the children. It restores the behaviors.
	 */
	public abstract void restoreOptions();
}
