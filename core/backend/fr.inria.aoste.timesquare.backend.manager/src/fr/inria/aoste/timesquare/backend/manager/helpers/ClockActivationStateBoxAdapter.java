/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.helpers;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;

import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockActivationState;

/**
 * This class is an SelectionAdapter for the GUIHelper display CheckBox function.
 * It updates the array given in parameter with the user's choice.
 * 
 * @author dlengell
 *
 */
public class ClockActivationStateBoxAdapter extends SelectionAdapter
{
	private ClockActivationState _tab;
	private ClockActivationState.State _index;
	
	/**
	 * A constructor with parameters.
	 * 
	 * @param tab
	 * @param i
	 */
	public ClockActivationStateBoxAdapter( ClockActivationState tab, ClockActivationState.State i )
	{
		_tab = tab;
		_index = i;
	}
	
	public void widgetSelected( SelectionEvent arg0 )
	{
		if ( ( ( Button ) arg0.widget ).getSelection() == true )
		{
			_tab.setState(_index, true);
		}
		else
		{
			_tab.setState(_index, false);
		}
	}
}
