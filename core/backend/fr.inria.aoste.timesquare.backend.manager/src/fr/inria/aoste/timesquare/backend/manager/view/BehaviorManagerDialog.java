/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.view;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceColors;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;
import fr.inria.aoste.timesquare.backend.manager.visible.IBehaviorManagerDialog;

/**
 * The BehaviorManagerDialog is a JFace Dialog. It will pop up when an user
 * selects a plugin in the view's list that has declared a BehaviorManagerGUI in
 * the extension point.
 * 
 * The methods contained in the BehaviorManagerGUI will be used to fill the
 * BehaviorManagerDialog.
 * 
 * @author dlengell
 * 
 */
public class BehaviorManagerDialog extends Dialog implements IBehaviorManagerDialog {

	private BehaviorManagerGUI _behaviorManagerGUI;
	private MessageLabeling label=null;

	private IStatus status=null;

	public static BehaviorManagerDialog create(BehaviorManagerGUI behaviorManagerGUI)
	{
		BehaviorManagerDialog behaviorManagerDialog = new BehaviorManagerDialog(new Shell());
		behaviorManagerDialog.setBehaviorManagerGUI(behaviorManagerGUI);
		behaviorManagerDialog.setBlockOnOpen(false);
		behaviorManagerDialog.open();

		return behaviorManagerDialog;
	}


	public BehaviorManagerDialog(Shell parentShell) {
		super(parentShell);		
		setShellStyle(SWT.RESIZE | getParentShell().getStyle());
		_behaviorManagerGUI = null;		
	}


	@Override
	public BehaviorManagerGUI getBehaviorManagerGUI() {
		return _behaviorManagerGUI;
	}

	public void setBehaviorManagerGUI(BehaviorManagerGUI behaviorManagerGUI) {
		_behaviorManagerGUI = behaviorManagerGUI;		
		_behaviorManagerGUI.setDialog(this);
	}

	public void cancelPressed() {
		if (_behaviorManagerGUI != null) {
			_behaviorManagerGUI.cancelPressed();
		}
		super.cancelPressed();
	}

	public void okPressed() {
		try {
			if (_behaviorManagerGUI != null) {
				_behaviorManagerGUI.okPressed();
			}
			super.okPressed();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public Control createDialogArea(Composite composite) {

		Composite composite2 = (Composite) super.createDialogArea(composite);
		if (_behaviorManagerGUI != null) {
			getShell().setText(_behaviorManagerGUI.getPluginName());
			Control control=  _behaviorManagerGUI.createDialogArea(composite2);
			label = new MessageLabeling(composite2, SWT.NONE);
			label.setAlignment(SWT.LEFT);
			GridData statusData = new GridData(GridData.FILL_HORIZONTAL);
			label.setErrorStatus(null);        
			label.setLayoutData(statusData);
			Point pt=_behaviorManagerGUI.getMinimumSize();	        		
			if (pt!=null )
				getShell().setMinimumSize(pt);	       
			return control;
		}

		return null;
	}



	@Override
	protected void createButtonsForButtonBar(Composite parent)
	{  
		super.createButtonsForButtonBar(parent);
		try
		{
			if (_behaviorManagerGUI!=null)
				_behaviorManagerGUI.updateOKStatus();
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
	}


	@Override
	public void updateStatus(IStatus updateStatus) {
		status=updateStatus;
		Button okButton = getOkButton();
		if (okButton != null && !okButton.isDisposed()) {
			okButton.setEnabled(!status.matches(IStatus.ERROR));
		}
		if (label!=null && !label.isDisposed())
		{
			label.setErrorStatus(status);
		}
	}
	public Button getOkButton() {
		return getButton(IDialogConstants.OK_ID);
	}

	public static class MessageLabeling extends CLabel {
		/**
		 * Creates a new message line as a child of the parent and with the given SWT stylebits.
		 */
		public MessageLabeling(Composite parent, int style) {
			super(parent, style);

		}

		private Image findImage(IStatus status) {
			if (status.isOK()) {
				return null;
			} else if (status.matches(IStatus.ERROR)) {
				return PlatformUI.getWorkbench().getSharedImages().getImage(
						ISharedImages.IMG_OBJS_ERROR_TSK);
			} else if (status.matches(IStatus.WARNING)) {
				return PlatformUI.getWorkbench().getSharedImages().getImage(
						ISharedImages.IMG_OBJS_WARN_TSK);
			} else if (status.matches(IStatus.INFO)) {
				return PlatformUI.getWorkbench().getSharedImages().getImage(
						ISharedImages.IMG_OBJS_INFO_TSK);
			}
			return null;
		}

		public void setErrorStatus(IStatus status) {
			if (status != null) {
				String message = status.getMessage();
				if (message != null && message.length() > 0) {
					setText(message);
					setImage(findImage(status));
					setBackground(JFaceColors.getErrorBackground(getDisplay()));
					return;
				}
			}
			setText(""); //$NON-NLS-1$	
			setImage(null);
			setBackground((Color)null);
		}

	}

	@Override
	public String getPluginName() {

		return getBehaviorManagerGUI().getPluginName();
	}
}
