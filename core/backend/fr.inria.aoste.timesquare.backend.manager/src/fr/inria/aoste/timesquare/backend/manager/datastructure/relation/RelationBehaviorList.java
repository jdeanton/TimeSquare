/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure.relation;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.backend.manager.datastructure.ABehaviorList;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationHelper;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;

/**
 * The RelationBehaviorList is a data structure which manipulates RelationBehaviorEntities.
 * It contains all the RelationBehaviorEntity and manages them.
 * 
 * 
 * @author dlengell
 *
 */
public class RelationBehaviorList extends ABehaviorList<RelationBehaviorEntity>
{
	public RelationBehaviorList() {
		super(new ArrayList< RelationBehaviorEntity >());
	}
    
    /**
     * During the simulation, the RelationBehaviorList has to execute the behaviors associated to a relation.
     * This is done in this method. The controller provides a Relation. The RelationBehaviorList checks in
     * the data list if a behavior needs to be execute or not and calls a method of the RelationBehavior class.
     * 
     * @param relation
     */
    public void executeBehaviors( OccurrenceRelation relation )
    {
        if ( relation == null)
        {
            return;
        }
        
      //  RelationActivationState relationActivationState = new RelationActivationState( relation );
        
        for ( RelationBehaviorEntity e : getData() )
        {
        	if( e.getRelationActivationState()!=null)
        	{
            if ( e.getRelationActivationState().relationActivationOK(  relation) )
            {
                e.getBehavior().run( new RelationHelper( relation ) );
            }
        	}
        	else
        	{
        		//System.err.println("..");
        	}
        }
    }    
}
