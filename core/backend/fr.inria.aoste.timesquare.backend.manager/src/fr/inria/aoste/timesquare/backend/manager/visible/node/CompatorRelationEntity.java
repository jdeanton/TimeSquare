/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible.node;

import java.util.Comparator;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;
import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.trace.ModelElementReference;
import fr.inria.aoste.trace.relation.IDescription;

public final class CompatorRelationEntity implements
		Comparator<RelationEntity> {
	@Override
	public int compare(RelationEntity o1, RelationEntity o2) {
		try {
			IDescription id1 = o1.getRelation();
			IDescription id2 = o2.getRelation();
			if (id1 == id2)
				return 0;
			int i = 0;
			ModelElementReference mer1 = (ModelElementReference) id1.getCcslConstraint();
			ModelElementReference mer2 = (ModelElementReference) id2.getCcslConstraint();
			if (mer2 == null)
				return 1;
			if (mer1 == null)
				return -1;

			while (true) {
				if (mer1.getElementRef().size() <= i) {
					if (mer2.getElementRef().size() <= i) {
						break;
					}
					return -1;
				}
				if (mer2.getElementRef().size() <= i) {
					return 1;
				}
				EObject eo1 = mer1.getElementRef().get(i);
				EObject eo2 = mer2.getElementRef().get(i);
				if (eo1 != eo2) {
					String st1=AdapterRegistry.getAdapter(eo1).getReferenceName(eo1);
					String st2=AdapterRegistry.getAdapter(eo2).getReferenceName(eo2);
					int n = st1.compareTo(st2) ; 
					if (n != 0)
						return n;
				}
				i++;
			}
			return id1.getSubInfo().compareTo(id2.getSubInfo());
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw e;
		}

	}
}