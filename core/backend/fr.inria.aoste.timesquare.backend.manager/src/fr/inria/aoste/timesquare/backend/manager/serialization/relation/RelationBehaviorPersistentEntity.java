/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization.relation;

import fr.inria.aoste.timesquare.backend.manager.serialization.PersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

/**
 * The RelationBehaviorPersistentEntity is a serializable RelationBehaviorEntity.
 * We can recreate a RelationBehaviorEntity with a RelationBehaviorPersistentEntity.
 * 
 * @author dlengell
 *
 */
public class RelationBehaviorPersistentEntity extends PersistentEntity
{
	private static final long serialVersionUID = -3585114610378849808L;
    private String relationid;
    
    public RelationBehaviorPersistentEntity(String pluginName, PersistentOptions options, String relId)
    {
        super(pluginName, options);
     
        relationid=relId;
    }
    
    /**
     * @return A relations Id
     */    
    public String getRelationid() {
		return relationid;
	}
}
