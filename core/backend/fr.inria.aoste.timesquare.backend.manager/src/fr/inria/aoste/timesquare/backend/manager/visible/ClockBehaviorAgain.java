/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

public interface ClockBehaviorAgain extends ClockBehavior {

	
	
	/**
     * This method is called when a clock in the Trace model fits with the clock activation state
     * associated to the behavior ( <b> for a replay step</b> ).
     * 
     * @param helper
     */
    public void againRun( TraceHelper helper );
    
    /**
     * This method is called when a clock in the Trace model don't fit with the clock activation state
     * associated to the behavior. ( <b> for a replay step</b> ).
     * 
     * @param helper
     */
    public void againRunWithWrongActivationState( TraceHelper helper );
}
