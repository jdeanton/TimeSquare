/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.view;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.datastructure.DataStructureManager;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputSetup;

/**
 * This class is a BehaviorView manipulated by the View.
 * It displays the ClockBehaviorEntities into a SWT Tree.
 * 
 * @author dlengell
 *
 */
public class ClockBehaviorView implements BehaviorView
{
    private Tree _tree;

 public String id;
    

    public ClockBehaviorView(String id) {
		super();
		this.id = id;
	}

    
    
 

	public void createContent( Composite composite ,OutputSetup oSetup)
    {
        _tree = new Tree( composite, SWT.SINGLE| SWT.FULL_SELECTION );
        oSetup.register(_tree, id);
        GridData layoutData = new GridData( SWT.FILL, SWT.FILL, true, true );
        layoutData.minimumHeight = 200;
        _tree.setLayoutData( layoutData );
                
        TreeColumn clockColumn = new TreeColumn( _tree, SWT.NONE );
        clockColumn.setText( "Clock" );
        clockColumn.setResizable( true );
        clockColumn.setMoveable( true );
        clockColumn.setWidth( 100 );

        TreeColumn stateColumn = new TreeColumn( _tree, SWT.NONE );
        stateColumn.setText( "State" );
        stateColumn.setResizable( true );
        stateColumn.setMoveable( true );
        stateColumn.setWidth( 300 );

       /* TreeColumn pluginNameColumn = new TreeColumn( _tree, SWT.NONE );
        pluginNameColumn.setText( "PluginName" );
        pluginNameColumn.setResizable( true );
        pluginNameColumn.setMoveable( true );
        pluginNameColumn.setWidth( 200 );*/

        TreeColumn behaviorColumn = new TreeColumn( _tree, SWT.NONE );
        behaviorColumn.setText( "Behavior" );
        behaviorColumn.setResizable( true );
        behaviorColumn.setMoveable( true );
        behaviorColumn.setWidth( 200 );

        _tree.setHeaderVisible( true );
        
       // _tree.pack();
    }



    public void displayModel( DataStructureManager ds )
    {
        if ( ds == null || _tree == null )
        {
            return;
        }
        
        List< ClockBehaviorEntity > tmp = ds.getClockBehaviorEntity();
        
        TreeItem clock;
        TreeItem subItem;

        TreeItem[] tab = _tree.getItems();
        
        boolean find = false;
        
        for ( ClockBehaviorEntity e : tmp )
        {
            find = false;
            
            for ( int i=0; i<tab.length; ++i )
            {
                if ( tab[i].getText().equals( e.getClockEntity().getName() ) )
                {
                    find = true;
                    subItem = new TreeItem( tab[i], SWT.NONE );
                    
                    subItem.setData( e );
                    
                    subItem.setText( 1, e.getClockActivationState().getDescription() );
                   // subItem.setText( 2, e.getPluginName() );
                    if ( e.getBehavior().getDescription() == null )
                    {
                        subItem.setText( 2, "" );
                    }
                    else
                    {
                        subItem.setText( 2, e.getBehavior().getDescription() );
                    }
                }
            }
            
            if ( !find )
            {
                clock = new TreeItem( _tree, SWT.NONE );
                clock.setText( e.getClockEntity().getName() );
                
                subItem = new TreeItem( clock, SWT.NONE );
                
                subItem.setData( e );
                
                subItem.setText( 1, e.getClockActivationState().getDescription() );
             //   subItem.setText( 2, e.getPluginName() );
                if ( e.getBehavior().getDescription() == null )
                {
                    subItem.setText( 2, "" );
                }
                else
                {
                    subItem.setText( 2, e.getBehavior().getDescription() );
                }
                
                tab = _tree.getItems();
            }
        }
        
       // _tree.pack();
    }
    
    public void deleteBehavior( Controller controller, SelectionEvent selectionEvent )
    {
        if ( controller == null || _tree == null )
        {
            return;
        }
        
        TreeItem[] tab = _tree.getSelection();
        if ( tab != null )
        {
            for ( int i = 0; i<tab.length; ++i )
            {
                if ( ( ClockBehaviorEntity )tab[i].getData() != null )
                {
                    controller.deleteEntity( ( ClockBehaviorEntity )tab[i].getData() );
                }
            }
        }
    }

    public void clear()
    {
        if ( _tree != null )
        {
            TreeItem[] tab = _tree.getItems();
            for ( int i=0; i<tab.length; ++i )
            {
                tab[i].dispose();
            }
        }        
    }

}
