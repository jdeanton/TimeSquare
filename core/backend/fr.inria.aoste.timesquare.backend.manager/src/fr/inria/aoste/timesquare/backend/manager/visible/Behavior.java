/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

/**
 * Behavior Interface.
 * 
 * @author dlengell
 *
 */
public interface Behavior
{
    /**
     * Returns a String representation of the Behavior.
     * 
     * @return The String representation of the Behavior.
     */
	public String getDescription();
	
	/**
	 * Returns true if the Behavior equals to the Behavior passed in parameter.
	 * Else returns false.
	 * 
	 * @param behavior
	 * @return true if this equals to the Behavior passed in parameter else returns false.
	 */
	public boolean behaviorEquals( Behavior behavior );

}
