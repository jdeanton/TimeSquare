/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.view;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.datastructure.DataStructureManager;
import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputSetup;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConstant;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;

/**
 * This is the view of this module.
 * 
 * It manipulates BehaviorView objects and displays into a SWT Composite given
 * in the constructor.
 * 
 * @author dlengell
 * 
 */
public class View   {

	private final class DeleteButtonSelectionAdapter extends SelectionAdapter {
		public void widgetSelected(SelectionEvent arg0) {
			if (_behaviorList == null || _controller == null) {
				return;
			}

			for (BehaviorView bv : _behaviorList) {
				bv.deleteBehavior(_controller, arg0);
			}
			_controller.refresh();
		}
	}

	private final class AddButtonSelectionAdapter extends SelectionAdapter {
		public void widgetSelected(SelectionEvent arg0) {
			managerSelected(_bm);
			_controller.refresh();
		}
	}

	private final class DeleteAllButtonSelectionAdapter extends SelectionAdapter {
		public void widgetSelected(SelectionEvent arg0) {
			_controller.deleteEntitiesByPluginName(_bm.getPluginName());
			_bm.clear();
			_controller.getDsm().clear();
			for (BehaviorView bv : _behaviorList) {
				bv.clear();
			}
			_controller.refresh();
		}
	}

	private Controller _controller;
	private DataStructureManager _ds;

	private Composite _rootComposite;
	// private Combo _combo;

	// private List< BehaviorManager > _behaviorManagerList;
	private BehaviorManager _bm;
	final private List<BehaviorView> _behaviorList;
	private OutputSetup outputSetup;
	/**
	 * A constructor for this class.
	 * 
	 * @param c
	 * @param ds
	 * @param composite
	 */
	public View(Controller c, Composite composite,OutputSetup oSetup) {
		outputSetup=oSetup;
		setController(c);
		_rootComposite = composite;
		_behaviorList = new ArrayList<BehaviorView>();
		_behaviorList.add(new ClockBehaviorView(BehaviorConstant.BEHAVIOR_CLOCK_TREE)); // ClockBehaviorView.
		_behaviorList.add(new AssertBehaviorView(BehaviorConstant.BEHAVIOR_ASSERT_TREE)); // AssertBehaviorView
		_behaviorList.add(new RelationBehaviorView(BehaviorConstant.BEHAVIOR_RELATION_TREE)); // RelationBehaviorView.
	}

	public void setController(Controller controller) {
		_controller = controller;
		if (controller == null)
			_ds = null;
		else
			_ds = _controller.getDsm();
	}

	/**
	 * Set the BehaviorManagers list.
	 * 
	 * @param behaviorManagers
	 */
	public void setBehaviorManagers(BehaviorManager bm) {
		_bm = bm;
	}

	/**
	 * Displays the view.
	 */
	public void display() {
		_rootComposite.setLayout(new GridLayout(1, true));

		// GridData layoutData = new GridData( SWT.FILL, SWT.FILL, true, true );
		// _rootComposite.setLayoutData( layoutData );

		// Group for the tree.
		Composite boutons = new Composite(_rootComposite, SWT.NONE);
		boutons.setLayout(new RowLayout());

		Group behaviorGroup = new Group(_rootComposite, SWT.FILL);

		GridLayout gridData = new GridLayout(1, true);
		behaviorGroup.setLayout(gridData);

		GridData layoutData2 = new GridData(SWT.FILL, SWT.FILL, true, true);
		behaviorGroup.setLayoutData(layoutData2);
		behaviorGroup.setText("");

		createBehaviorContents(behaviorGroup);

		
		Button addButton = new Button(boutons, SWT.PUSH);
		addButton.setText("Add");
		outputSetup.register(addButton, BehaviorConstant.BEHAVIOR_ADD);
		Button deleteButton = new Button(boutons, SWT.PUSH);
		deleteButton.setText("Delete");
		outputSetup.register(deleteButton, BehaviorConstant.BEHAVIOR_DELETE);
		Button deleteAllButton = new Button(boutons, SWT.PUSH);
		deleteAllButton.setText("Delete All");
		outputSetup.register(deleteAllButton, BehaviorConstant.BEHAVIOR_DELETE_ALL);
		boutons.pack();

		// We add behaviors to the buttons.
		addButton.addSelectionListener(new AddButtonSelectionAdapter());

		deleteButton.addSelectionListener(new DeleteButtonSelectionAdapter());

		deleteAllButton.addSelectionListener(new DeleteAllButtonSelectionAdapter());
		// _rootComposite.pack();
	}

	/**
	 * Creates the contents of the BehaviorView in the list.
	 * 
	 * @param composite
	 */
	private void createBehaviorContents(Composite composite) {
		if (composite == null || _behaviorList == null) {
			return;
		}

		for (BehaviorView bv : _behaviorList) {
			bv.createContent(composite,outputSetup);			
		}

	}
	
	/**
	 * Refreshes the view.
	 */
	public void refresh() {
		if (_behaviorList == null) {
			return;
		}

		for (BehaviorView bv : _behaviorList) {
			bv.clear();
		}

		if (_ds != null) {
			for (BehaviorView bv : _behaviorList) {
				bv.displayModel(_ds);
			}
		}
	}

	/**
	 * Tells to the controller that an user has selected a BehaviorManager.
	 * 
	 * @param behaviorManager
	 */
	public void managerSelected(BehaviorManager behaviorManager) {
		_controller.notifyBehaviorManager(behaviorManager);
	}

	/**
	 * Displays the BehaviorManagerGUI passed in parameter.
	 * 
	 * @param behaviorManagerGUI
	 */
	public void displayBehaviorManagerDialog(BehaviorManagerGUI behaviorManagerGUI) {
		if (behaviorManagerGUI == null) {
			return;
		}

		BehaviorManagerDialog behaviorManagerDialog = new BehaviorManagerDialog(_rootComposite.getShell());
		behaviorManagerDialog.setBehaviorManagerGUI(behaviorManagerGUI);
		behaviorManagerDialog.open();
	}

}
