/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure.clock;

import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.serialization.BehaviorPersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

/**
 * 
 * This class is manipulated by the ClockBehaviorList.
 * It contains all the information about a clock behavior.
 * 
 * @author dlengell
 *
 */
final public class ClockBehaviorEntity extends Entity
{
    private ClockEntity _clockEntity;
    private ClockActivationState _clockActivationState;
    private ClockBehavior _behavior;
    
    /**
     * A constructor for this class with parameters.
     * 
     * @param clockEntity
     * @param clockActivationState
     * @param pluginName
     * @param behavior
     * @param persistentOptions
     */
    public ClockBehaviorEntity( ClockEntity clockEntity, ClockActivationState clockActivationState, String pluginName, ClockBehavior behavior, PersistentOptions persistentOptions )
    {
        super( pluginName, persistentOptions );
        _clockEntity = clockEntity;
        _clockActivationState = clockActivationState;
        _behavior = behavior;
    }
    
    /**
     * Returns the ClockEntity.
     * 
     * @return The ClockEntity.
     */
    public ClockEntity getClockEntity()
    {
        return _clockEntity;
    }
    
    /**
     * Returns the ClockActivationState.
     * 
     * @return The ClockActivationState.
     */
    public ClockActivationState getClockActivationState()
    {
        return _clockActivationState;
    }
    
    /**
     * Returns the ClockBehavior.
     * 
     * @return The ClockBehavior.
     */
    public ClockBehavior getBehavior()
    {
        return _behavior;
    }
    
    /**
     * Transforms the ClockBehaviorEntity into a ClockBehaviorPersistentEntity which can be serialized.
     * 
     * @return The ClockBehaviorPersistentEntity obtained from this ClockBehaviorEntity.
     */
    public BehaviorPersistentEntity transformEntityIntoPersistentEntity()
    {
        //Nothing to serialize.
        if ( _persistentOptions == null )
        {
            return null;
        }
        
        return new BehaviorPersistentEntity(_pluginName, 
        		_persistentOptions, _clockEntity.getID(), 
        		_clockActivationState.getState());
    }
    
    
    /**
     * Returns a description of the ClockBehaviorEntity.
     * 
     * @return A String description of the ClockBehaviorEntity.
     */
    public String getDescription()
    {
        return _clockEntity.getName()+" "+_clockActivationState.getDescription()+" "+_behavior.getDescription()+" "+super.getDescription();
    }
    
    
    /**
     * Redefines the equals method of Object.
     * This method is very important when we delete/modify an ClockEntity.
     *
     * @param o
     * @return true if this equals to the Object passed in parameter.
     */
    public boolean equals( Object o )
    {
        if ( o == this )
        {
            return true;
        }
        
        if ( o instanceof ClockBehaviorEntity )
        {
            ClockBehaviorEntity e = ( ClockBehaviorEntity ) o;
            
            if ( _clockEntity.equals( e.getClockEntity() ) && 
            		_clockActivationState.equals( e.getClockActivationState() ) && 
            		_behavior.behaviorEquals( e.getBehavior() ) 
            		&& super.equals( o ) )
            {
                return true;
            }
        }

        return false;
    }

	@Override
	public int hashCode() {		
		return super.hashCode()+2;
	}

	@Override
	public String toString() {
		return "ClockBehaviorEntity["+_clockEntity+"]";
	}
}
