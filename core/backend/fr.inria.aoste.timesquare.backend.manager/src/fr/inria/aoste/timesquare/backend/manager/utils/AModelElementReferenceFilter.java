/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.utils;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.HelperFactory;
import fr.inria.aoste.trace.ModelElementReference;

abstract class AModelElementReferenceFilter implements IFilter<ModelElementReference> {
	private EObject getRef(ModelElementReference ref) {
		return HelperFactory.getLastReference(ref);		
	}
	
	abstract protected boolean accept(EObject o);
	
	@Override
	final public boolean accept(ModelElementReference o) {
		return accept(getRef(o));
	}

}
