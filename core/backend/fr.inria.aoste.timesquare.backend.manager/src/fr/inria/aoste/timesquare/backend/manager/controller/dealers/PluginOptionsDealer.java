/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.controller.dealers;

import java.util.Map;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.serialization.plugin.PluginOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

/**
 * The PluginOptionsDealer has to restore the general serialized options provided by the different plugins.
 * It uses a PluginOptionsSerializer in input to get the options.
 * It uses the Controller to add new entities with new behaviors.
 * 
 * 
 * @author dlengell
 *
 */
public class PluginOptionsDealer extends OptionsDealer
{
	private PluginOptionsSerializer _pluginOptionsSerializer;

	/**
	 * The constructor for this class.
	 * 
	 * @param behaviorManagers
	 * @param controler
	 * @param pluginOptionsSerializer
	 */
	public PluginOptionsDealer(  Controller controler, PluginOptionsSerializer pluginOptionsSerializer  )
	{
		super(  controler );
		_pluginOptionsSerializer = pluginOptionsSerializer;
	}

	/**
	 * This method distributes the general options to the different plugin managers.
	 */
	public void restoreOptions()// throws Throwable
	{
		if ( _pluginOptionsSerializer == null || _controller == null )
		{
			return;
		}
		
		for ( Map.Entry< String, PersistentOptions >  e : _pluginOptionsSerializer.getData().entrySet() )
		{
			try
			{
			BehaviorManager bm = _controller.getBehaviorManager( );
			if ( bm != null ) //Le plugin est present
			{
				
				bm.receivePluginOptions( new ConfigurationHelper( _controller ), e.getValue() );
			}
			}
			catch (Throwable te) {
				te.printStackTrace();
			}
		}	
	}
}
