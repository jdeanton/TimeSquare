/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure.assertion;

import java.util.ArrayList;
import java.util.List;

import fr.inria.aoste.timesquare.backend.manager.datastructure.ABehaviorList;
import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.TraceHelper;
import fr.inria.aoste.trace.AssertionState;

/**
 * The AssertBehaviorList is a data structure which manipulates AssertBehaviorEntities.
 * It contains all the AssertBehaviorEntity and manages them.
 * 
 * 
 * @author dlengell,bferrero
 *
 */
public class AssertBehaviorList extends ABehaviorList<AssertBehaviorEntity>
{
    /**
     * The constructor of this class with no parameters.
     */
    public AssertBehaviorList()
    {
        super(new ArrayList< AssertBehaviorEntity >());
    }
                        
    /**
     * During the simulation, the AssertBehaviorList has to execute the behaviors associated to a relation.
     * This is done in this method. The controller provides a Relation. The AssertBehaviorList checks in
     * the data list if a behavior needs to be execute or not and calls a method of the RelationBehavior class.
     * 
     * @param relation
     */
    public void executeNewBehaviors( ClockEntity clockEntity, AssertionState clockState )
    {
        if ( clockEntity == null || clockState == null)
        {
            return;
        }
        
        //ClockState => ClockActivationState
        AssertActivationState clockActivationState = new AssertActivationState( clockState ); 
        
        for ( AssertBehaviorEntity e : getData() )
        {
            if ( e.getClockEntity().equals( clockEntity ) )
            {
                if ( AssertActivationState.stateActivationOK( e.getClockActivationState(), clockActivationState ) )
                {
                    e.getBehavior().run( new TraceHelper( clockState) );
                }
                else
                {
                   // e.getBehavior().runWithWrongActivationState( new TraceHelper( clockState, clockEntity ) );
                }
            }
        }
    }
    
     public void checkConsistency( List< ClockEntity > _assert )
    {
        if ( _assert == null )
        {
            this.clear();
            return;
        }
        
        List< Entity > toDelete = new ArrayList< Entity >();
        for ( AssertBehaviorEntity e : getData() )
        {
            if ( !( _assert.contains( e.getClockEntity() ) ) )
            {
                toDelete.add( e );
            }
        }
        
        for ( Entity e : toDelete )
        {
            getData().remove(e);
        }
    }
     
}
