/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization;

import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputOption;
import fr.inria.aoste.timesquare.backend.manager.serialization.plugin.PluginOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.relation.RelationBehaviorOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;

/**
 * The XMLStringMaker builds a String representation of the actual behavior configuration.
 * This String contains XML formated data. 
 * 
 * @author dlengell
 *
 */
public class XMLStringMaker {
	PluginOptionsSerializer _pluginOS = null;
	BehaviorOptionsSerializer _clockBehaviorOS = null;
	RelationBehaviorOptionsSerializer _relationBehaviorOS = null;
	BehaviorOptionsSerializer _assertBehaviorOS = null;
	BehaviorManager bm = null;

	/**
	 * The constructor. It takes an ExtensionPointManager to get the present BehaviorManagers. It needs all the serializers which contain
	 * the persistent objects to serialize.
	 * 
	 * @param behaviorManagers
	 * @param pos
	 * @param cbos
	 * @param rbos
	 */
	public XMLStringMaker(BehaviorManager _bm, PluginOptionsSerializer pos, BehaviorOptionsSerializer cbos,
			RelationBehaviorOptionsSerializer rbos, BehaviorOptionsSerializer abos) {
		bm = _bm;
		_pluginOS = pos;
		_clockBehaviorOS = cbos;
		_relationBehaviorOS = rbos;
		_assertBehaviorOS = abos;
	}

	/**
	 * Creates and returns the XML String that contains the actual behavior configuration.
	 * 
	 * @return The XML String that contains the actual behavior configuration.
	 */
	public String createXMLString(OutputOption configuration) {
		String xmlOptions = "";

		if (bm == null || _pluginOS == null || _clockBehaviorOS == null || _relationBehaviorOS == null) {
			return xmlOptions;
		}

		try {

			/*  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			  DocumentBuilder parser = factory.newDocumentBuilder();
			  DOMImplementation impl = parser.getDOMImplementation();
			  
			  Document document = impl.createDocument( null, "root", null );
			  
			  Element root = document.getDocumentElement();
			  
			  Element pluginName;*/
			/*  Element pluginOptions;
			  Element clockBehaviorOptions;
			  Element relationBehaviorOptions;
			  Element assertBehaviorOptions;*/

		//	for (BehaviorManager bm : listBehavior)// _behaviorManagers.getManagers() ) //For each BehaviorManager.
			{
				String pluginname = bm.getPluginName();
				String id = bm.getId();
				// 
				if (!(_pluginOS.doesPluginNeedSerialization(pluginname) || _clockBehaviorOS.doesPluginNeedSerialization(pluginname) ||
						_relationBehaviorOS.doesPluginNeedSerialization(pluginname) || _assertBehaviorOS
						.doesPluginNeedSerialization(pluginname)))
					//continue;
				{
					configuration.setFlag(id + ".pluginOptions",null);
					configuration.setFlag(id + ".clockBehaviorOptions",null);
					configuration.setFlag(id + ".relationBehaviorOptions",null);
					configuration.setFlag(id + ".assertBehaviorOptions",null);					
					return xmlOptions;
				}
				// < pluginName name = "" >
				// pluginName = document.createElement( "pluginName" );
				// pluginName.setAttribute( "name",pluginname);

				// < pluginOptions >

				if (_pluginOS.doesPluginNeedSerialization(pluginname)) {
					// Element pluginOptions = document.createElement( "pluginOptions" );
					// pluginOptions.setTextContent( _pluginOS.getSerializedOptions( pluginname ) );
					// pluginName.appendChild( pluginOptions );
					configuration.setFlag(id + ".pluginOptions", _pluginOS.getSerializedOptions(pluginname));
				}
				else
				{
					configuration.setFlag(id + ".pluginOptions",null);
				}

				// < clockBehaviorOptions >

				if (_clockBehaviorOS.doesPluginNeedSerialization(pluginname)) {
					
					configuration.setFlag(id + ".clockBehaviorOptions", _clockBehaviorOS.getSerializedOptions(pluginname));
				}
				else
				{
					configuration.setFlag(id + ".clockBehaviorOptions",null);
				}

				// < relationBehaviorOptions >

				if (_relationBehaviorOS.doesPluginNeedSerialization(pluginname)) {
								configuration.setFlag(id + ".relationBehaviorOptions", _relationBehaviorOS.getSerializedOptions(pluginname));
				}
				else
				{
					configuration.setFlag(id + ".relationBehaviorOptions",null);
				}

				// < _assertBehaviorOptions >

				if (_assertBehaviorOS.doesPluginNeedSerialization(pluginname)) {
					configuration.setFlag(id + ".assertBehaviorOptions", _assertBehaviorOS.getSerializedOptions(pluginname));
				}
				else
				{
					configuration.setFlag(id + ".assertBehaviorOptions",null);
				}

				
				// root.appendChild( pluginName );
			}

			// We place the XML Document in the String.
			/*      StringWriter stringOut = new StringWriter(); 
			      TransformerFactory transFactory = TransformerFactory.newInstance(); 
			      Transformer trans = transFactory.newTransformer(); 
			      trans.setOutputProperty(OutputKeys.INDENT, "yes");
			  //    trans.transform( new DOMSource( document ), new StreamResult( stringOut ) );
			      xmlOptions =  stringOut.toString();*/
			// System.out.println("WR\n"+xmlOptions);
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return xmlOptions;
	}

}
