/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

public interface BehaviorConstant {

	public static final String BEHAVIOR_DELETE_ALL = "behavior.deleteAll";
	public static final String BEHAVIOR_DELETE = "behavior.delete";
	public static final String BEHAVIOR_ADD = "behavior.add";
	public static final String BEHAVIOR_RELATION_TREE = "behavior.relation.tree";
	public static final String BEHAVIOR_ASSERT_TREE = "behavior.assert.tree";
	public static final String BEHAVIOR_CLOCK_TREE = "behavior.clock.tree";


}