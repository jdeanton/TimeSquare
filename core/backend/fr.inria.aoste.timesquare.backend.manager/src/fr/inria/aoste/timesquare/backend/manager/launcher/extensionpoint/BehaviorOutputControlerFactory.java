/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint;

import java.util.ArrayList;

import fr.inria.aoste.timesquare.backend.manager.extensionpoints.BehaviorOption;
import fr.inria.aoste.timesquare.backend.manager.extensionpoints.PEBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorOutputControler;
import fr.inria.aoste.timesquare.launcher.extensionpoint.OutputControler;
import fr.inria.aoste.timesquare.launcher.extensionpoint.OutputControlerFactory;

public class BehaviorOutputControlerFactory implements OutputControlerFactory {

	@Override
	public OutputControler[] create(String key) {
		ArrayList<OutputControler> loc= new ArrayList<OutputControler>();
		for (BehaviorOption bo: PEBehavior.getDefault().getBehaviorOptionList())
		{
			String s=bo.getId();
			if ( s!=null)
			{
			BehaviorOutputControler bco=new BehaviorOutputControler(bo.getManager(),bo.getGui(), s);
			bco.setKeybase(key, bo.getPlugin()+"@"+bo.getManager().getName());
			loc.add(bco);
			}
		}
		return loc.toArray(new OutputControler[loc.size()]);
	}

}
