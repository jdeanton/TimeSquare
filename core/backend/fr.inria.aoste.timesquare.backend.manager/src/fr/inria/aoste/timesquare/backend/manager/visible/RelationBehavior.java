/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

/**
 * RelationBehavior interface.
 * 
 * @author dlengell
 *
 */
public interface RelationBehavior extends Behavior
{
    /**
     * 
     * This method is called when a relation is detected in the Trace model 
     * and this relation fits with the relation activation state of this behavior.
     * 
     * @param helper
     */
    public void run( RelationHelper helper );

}
