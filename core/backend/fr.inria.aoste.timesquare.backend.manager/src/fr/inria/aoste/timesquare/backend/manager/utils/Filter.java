/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.utils;

import java.util.ArrayList;


import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter.EventEnumerator;
import fr.inria.aoste.trace.ModelElementReference;

public class Filter {	
	
	public static interface ICreatePairClockModelElementReference<TOUT extends PairClockModelElementReference> {
		public TOUT create(ModelElementReference src, ModelElementReference dst);
	}
	

	public static <T, T2 extends T> boolean containt(Iterable<T2> lst, IFilter<T> filter) {
		for (T2 t : lst) {
			if (filter.accept(t)) {
				return true;
			}
		}
		return false;

	}

	public static <T, T2 extends T> int count(Iterable<T2> lst, IFilter<T> filter) {
		int n = 0;
		for (T2 t : lst) {
			if (filter.accept(t)) {
				n++;
			}
		}
		return n;
	}

	public static <T, T2 extends T> ArrayList<T2> sublist(Iterable<T2> lst, IFilter<T> filter) {
		ArrayList<T2> liste = new ArrayList<T2>();
		for (T2 t : lst) {
			if (filter.accept(t)) {
				liste.add(t);
			}
		}
		return liste;
	}
	
	
	public static <TOUT extends PairClockModelElementReference> ArrayList<TOUT> createPairClockModelElementReference(EventEnumeratorPair eep ,Iterable<ModelElementReference> lst, ICreatePairClockModelElementReference<TOUT> creator) 
	{
		ArrayList<TOUT> liste = new ArrayList<TOUT>();
		if (eep!=null)
		{
			ArrayList<ModelElementReference> listesrc=sublist(lst, getFilterEventEnumerator(eep.getSource()));
			ArrayList<ModelElementReference> listedst=sublist(lst, getFilterEventEnumerator(eep.getDestination()));
			for ( ModelElementReference mersrc: listesrc)
				for ( ModelElementReference merdst: listedst)
				{
					TOUT t=creator.create(mersrc, merdst);
					if ( t!=null)
					{
						liste.add(t);
					}
				}
		}		
		return liste;
	}


	

	
	
	/*public static class FilterAssertEntity implements IFilter<AssertEntity> {

		final IFilter<ModelElementReference> filterModelElementReference;

		public FilterAssertEntity(IFilter<ModelElementReference> filterModelElementReference) {

			this.filterModelElementReference = filterModelElementReference;
		}

		@Override
		public boolean accept(AssertEntity ae) {

			return filterModelElementReference.accept(ae.getModelElementReference());
		}
	}*/
	
	static final public FilterEventEnumerator FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_CONSUME = new FilterEventEnumerator(
			EventEnumerator.CONSUME);
	static final public FilterEventEnumerator FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_FINISH = new FilterEventEnumerator(
			EventEnumerator.FINISH);
	static final public FilterEventEnumerator FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_PRODUCE = new FilterEventEnumerator(
			EventEnumerator.PRODUCE);
	static final public FilterEventEnumerator FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_RECEIVE = new FilterEventEnumerator(
			EventEnumerator.RECEIVE);
	static final public FilterEventEnumerator FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_SEND = new FilterEventEnumerator(
			EventEnumerator.SEND);
	static final public FilterEventEnumerator FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_START = new FilterEventEnumerator(
			EventEnumerator.START);
	static final public FilterEventEnumerator FILTER_MODELELEMENTREFERENCE_EVENTENUMERATORUNDEFINED = new FilterEventEnumerator(
			EventEnumerator.UNDEFINED);

	static final public FilterReferencedObject FILTER_REFERENCED_OBJECT = new FilterReferencedObject();

	static final public FilterClockEntity FILTER_CLOCK_ENTITY_DISCRETIZE = new FilterClockEntity(new FilterDiscretize());
	
	static public FilterEventEnumerator getFilterEventEnumerator(EventEnumerator ee)
	{
		if (ee!=null)
		{
			switch (ee) {
			case CONSUME:
				return FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_CONSUME;
			case FINISH:
				return FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_FINISH; 
			case PRODUCE:
				return  FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_PRODUCE;
			case RECEIVE:
				return FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_RECEIVE;
			case SEND:
				return FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_SEND;
			case START:
				return FILTER_MODELELEMENTREFERENCE_EVENTENUMERATOR_START ;
			case UNDEFINED:
				return FILTER_MODELELEMENTREFERENCE_EVENTENUMERATORUNDEFINED;
						
			}
		}
		return null;
	}

}