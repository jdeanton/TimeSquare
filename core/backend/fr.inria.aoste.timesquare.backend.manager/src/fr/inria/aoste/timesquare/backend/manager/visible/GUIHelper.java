/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.Widget;

import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockActivationState.State;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockActivationState.StateGroup;
import fr.inria.aoste.timesquare.backend.manager.helpers.ClockStateCheckBoxAdapter;
import fr.inria.aoste.timesquare.backend.manager.helpers.ColorAdapter;
import fr.inria.aoste.timesquare.backend.manager.helpers.ComboAdapter;
import fr.inria.aoste.timesquare.backend.manager.helpers.TreeAdapter;
import fr.inria.aoste.timesquare.backend.manager.utils.IFilter;
import fr.inria.aoste.timesquare.backend.manager.visible.node.CompatorClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.node.CompatorModelElementReference;
import fr.inria.aoste.timesquare.backend.manager.visible.node.CompatorRelationEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.node.Node;
import fr.inria.aoste.timesquare.backend.manager.visible.node.NodeLabelProvider;
import fr.inria.aoste.timesquare.backend.manager.visible.node.NodeTreeContentProvider;
import fr.inria.aoste.timesquare.launcher.debug.model.output.OutputSetupOption;

/**
 * This helper offers common GUI operations.
 * 
 * @author dlengell,bferrero
 * 
 */
public class GUIHelper {

	public static String COMBOSELECTION = "combo.selection";

	private ConfigurationHelper controller;
	private BehaviorManagerGUI gui;

	public GUIHelper(BehaviorManagerGUI gui) {
		super();
		this.gui = gui;
	}

	public GUIHelper(ConfigurationHelper c, BehaviorManagerGUI gui) {
		super();
		this.controller = c;
		this.gui = gui;
	}

	public void setConfigurationHelper(ConfigurationHelper controller) {
		this.controller = controller;
	}

	private List<ClockEntity> getClocks() {
		if (controller == null)
			return Collections.emptyList();
		return controller.getClocks();
	}

	private List<ClockEntity> getAssert() {
		if (controller == null)
			return Collections.emptyList();
		return controller.getAssert();
	}

	private List<RelationEntity> getRelationEntities() {
		if (controller == null)
			return Collections.emptyList();
		return controller.getRelationEntities();
	}

	/**
	 * Displays a check box helping an user to choose an clock activation state
	 * for a behavior. The boolean array returned contains the values selected
	 * by the user.
	 * 
	 * 
	 * @param composite
	 *            :parent
	 * @return ClockActivationState which contains the states selected by the
	 *         user.
	 */
	public ClockActivationState displayClockState(Composite composite) {
		return displayClockState(composite, null);
	}

	/**
	 * Displays a check box helping an user to choose an clock activation state
	 * for a behavior. The boolean array returned contains the values selected
	 * by the user.
	 * 
	 * 
	 * @param composite
	 *            : parent
	 * @param layoutdata
	 * @return ClockActivationState which contains the states selected by the
	 *         user.
	 * 
	 * 
	 */
	public ClockActivationState displayClockState(Composite composite,
			Object layoutdata) {
		return displayClockState(composite, layoutdata, null);
	}

	/**
	 * Displays a check box helping an user to choose an clock activation state
	 * for a behavior. The boolean array returned contains the values selected
	 * by the user.
	 * 
	 * 
	 * @param composite
	 *            parent
	 * @param layoutdata
	 * @param id
	 *            : group id
	 * @return ClockActivationState which contains the states selected by the
	 *         user.
	 * 
	 * 
	 */
	public ClockActivationState displayClockState(Composite composite,
			Object layoutdata, String id) {
		if (composite == null) {
			return new ClockActivationState(null);
		}

		boolean[] tab = new boolean[ClockActivationState.stateNumber];
		ClockActivationState cas = new ClockActivationState(tab);
		Group group = new Group(composite, SWT.FILL);
		group.setLayout(new GridLayout(1, true));
		group.setText("Clock Activation State");
		if (layoutdata != null) {
			group.setLayoutData(layoutdata);
		}
		for (StateGroup g : StateGroup.values()) {
			Group group2 = new Group(group, SWT.FILL);
			group2.setText(g.name());
			group2.setLayout(new GridLayout(2, true));
			group2.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
			Button check;
			for (State s : State.state(g)) {
				check = new Button(group2, SWT.CHECK);
				check.setText(s.name);				
				check.addSelectionListener(new ClockStateCheckBoxAdapter(tab,
						s.i));
				check.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true,
						false));
				if ( s== State.ticks)
				{
					check.setSelection(true);
					tab[s.i]=true;
				}
				registerID(check, id + "." + s.name);
			}

		}
		group.pack();
		registerID(group, id);
		return cas;
	}

	public boolean[] displayAssertStateCheckBox(Composite composite) {
		if (composite == null) {
			return new boolean[] {};
		}
		boolean[] tab = new boolean[AssertActivationState.assertionNumber];
		Group group = new Group(composite, SWT.FILL);
		group.setLayout(new GridLayout(2, true));
		group.setText("Assert Activation State");
		String[] assertState = AssertActivationState.assertlist;
		for (int i = 0; i < assertState.length; i++) {
			Button check = new Button(group, SWT.CHECK);
			check.setText(assertState[i]);
			check.addSelectionListener(new ClockStateCheckBoxAdapter(tab, i));
			if (i == 0) {
				check.setEnabled(false);
			}
		}
		group.pack();
		return tab;
	}

	public boolean[] displayAssertStateCheckBox(Composite composite,
			Object layoutdata) {
		if (composite == null) {
			return new boolean[] {};
		}
		boolean[] tab = new boolean[AssertActivationState.assertionNumber];
		Group group = new Group(composite, SWT.FILL);
		group.setLayout(new GridLayout(2, true));
		group.setText("Assert Activation State");
		group.setLayoutData(layoutdata);
		String[] assertState = AssertActivationState.assertlist;
		for (int i = 0; i < assertState.length; i++) {
			Button check = new Button(group, SWT.CHECK);
			check.setText(assertState[i]);
			check.addSelectionListener(new ClockStateCheckBoxAdapter(tab, i));
			if (i == 0) {
				check.setEnabled(false);
			}
		}
		group.pack();
		return tab;
	}

	/**
	 * 
	 * @param composite
	 * @param id
	 *            (Combo identifier)(may be null) *
	 * 
	 * @return
	 * 
	 * @wbp.factory
	 */
	public Combo displayListClock(Composite composite, String id) {
		List<ClockEntity> elements = getClocks();
		return displayList(composite, elements, id);
	}

	/**
	 * 
	 * @param composite
	 * @param id
	 *            (may be null)
	 * @return clock TreeViewer
	 */
	public TreeViewer displayTreeClock(Composite composite, String id, int type) {
		if (composite == null) {
			return null;
		}
		List<ClockEntity> elements = new ArrayList<ClockEntity>(getClocks());
		Collections.sort(elements, new CompatorClockEntity(
				new CompatorModelElementReference(false)));
		List<Node<ClockEntity>> result = Node
				.buildListNodesClockEntity(elements);
		Node.optimise(result);
		return displayTree(composite, id, result, type);
	}

	/**
	 * 
	 * @param composite
	 * @param id
	 *            (Combo identifier)(may be null)
	 * @return
	 * 
	 * @wbp.factory
	 */
	public Combo displayListAssert(Composite composite, String id) {
		List<ClockEntity> elements = getAssert();
		return displayList(composite, elements, id);
	}

	/**
	 * 
	 * @param composite
	 * @param id
	 *            (may be null)
	 * @return
	 * 
	 * @wbp.factory
	 */
	public Combo displayListRelation(Composite composite, String id) {
		List<RelationEntity> elements = getRelationEntities();
		return displayList(composite, elements, id);
	}

	/**
	 * 
	 * @param composite
	 * @param id
	 *            (may be null)
	 * @return Rleation TreeViewer
	 */
	public TreeViewer displayTreeRelation(Composite composite, String id) {
		if (composite == null) {
			return null;
		}
		List<RelationEntity> elements = new ArrayList<RelationEntity>(
				getRelationEntities());
		Collections.sort(elements, new CompatorRelationEntity());
		List<Node<RelationEntity>> result = Node
				.buildListNodesRelationEntity(elements);
		Node.optimise(result);
		return displayTree(composite, id, result, SWT.SINGLE);
	}

	public <T> TreeViewer displayTree(Composite composite, String id,
			List<Node<T>> result, int type) {
		Tree tree = new Tree(composite, SWT.BORDER | SWT.READ_ONLY | type
				| SWT.RESIZE | SWT.V_SCROLL | SWT.H_SCROLL);
		TreeColumn column = null;
		column = new TreeColumn(tree, SWT.LEFT, 0);
		column.setMoveable(false);
		column.setWidth(450);
		tree.setData(COMBOSELECTION, new Object());
		tree.pack();
		tree.setHeaderVisible(true);
		TreeViewer tv = new TreeViewer(tree);
		tv.setContentProvider(new NodeTreeContentProvider());
		ColumnViewerToolTipSupport.enableFor(tv);
		tv.setLabelProvider(new NodeLabelProvider());
		tv.setInput(result);
		tree.addSelectionListener(new TreeAdapter(gui));
		tree.setSize(400, 400);
		registerID(tree, id);
		return tv;
	}

	/**
	 * Displays and returns a reference on a combo box wich contains the objects
	 * of the list elements. When an user selects a object in the list, this
	 * object is set in the Widget Combo.
	 * 
	 * To get the object selected :
	 * 
	 * <code>
	 * Combo combo = displayList( composite, elementList )
	 * Object o = combo.getData(); //You have to cast the result in your type.
	 * </code>
	 * 
	 * @param composite
	 * @param elements
	 * @param id
	 *            (Combo identifier) (may be null)
	 * @return A reference on a Combo which contains the object selected by the
	 *         user.
	 */
	public Combo displayList(Composite composite, List<?> elements, String id) {
		if (composite == null || elements == null) {
			return null;
		}

		Combo comboElements = new Combo(composite, SWT.READ_ONLY);
		comboElements.setData(COMBOSELECTION, new Object());
		for (Object e : elements) {
			comboElements.add(e.toString());
		}
		comboElements.addSelectionListener(new ComboAdapter(elements, gui));
		comboElements.pack();
		if (elements.size() != 0) {
			comboElements.setData(COMBOSELECTION, elements.get(0));
			comboElements.select(0);
		}
		return registerID(comboElements, id);
	}

	/**
	 * Displays and returns a reference on a combo box wich contains the objects
	 * of the list elements. When an user selects a object in the list, this
	 * object is set in the Widget Combo.
	 * 
	 * To get the object selected :
	 * 
	 * <code>
	 * Combo combo = displayList( composite, elementList )
	 * Object o = combo.getData(); //You have to cast the result in your type.
	 * </code>
	 * 
	 * @param composite
	 * @param elements
	 * @param filter
	 * @param id
	 *            (Combo identifier)
	 * @return A reference on a Combo which contains the object selected by the
	 *         user.
	 */
	public <T> Combo displayList(Composite composite,
			List<? extends T> elements, IFilter<T> filter, String id) {
		if (composite == null)
			return null;
		ArrayList<T> lst = new ArrayList<T>();
		Combo comboElements = new Combo(composite, SWT.READ_ONLY);
		comboElements.setData(COMBOSELECTION, new Object());
		if (elements != null) {
			for (T e : elements) {
				if (filter == null || filter.accept(e)) {
					comboElements.add(e.toString());
					lst.add(e);
				}
			}
		}
		comboElements.addSelectionListener(new ComboAdapter(lst, gui));
		comboElements.pack();
		if (lst.size() != 0) {
			comboElements.setData(COMBOSELECTION, lst.get(0));
			comboElements.select(0);
		}
		return registerID(comboElements, id);
	}

	/**
	 * Displays a Color dialog. The RGB returned contains the color selected by
	 * the user.
	 * 
	 * @param composite
	 * @return A reference on a RGB which contains the color selected by the
	 *         user.
	 */
	public RGB displayColorDialog(Composite composite) {
		if (composite == null) {
			return null;
		}
		RGB color = new RGB(255, 255, 255);
		Button button = new Button(composite, SWT.PUSH);
		button.setText("Colors");
		button.addSelectionListener(new ColorAdapter(color, composite, button));
		return color;
	}

	public Object getSelection(Combo c) {
		return c.getData(COMBOSELECTION);
	}

	public <T extends Widget> T registerID(T w, String id) {
		return OutputSetupOption.registerID(w, id);
	}
}
