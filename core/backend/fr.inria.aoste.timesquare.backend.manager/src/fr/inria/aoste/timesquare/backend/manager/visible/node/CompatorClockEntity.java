/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible.node;

import java.util.Comparator;

import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.trace.ModelElementReference;

public final class CompatorClockEntity implements
		Comparator<ClockEntity> {

	public CompatorClockEntity(Comparator<ModelElementReference> cmp) {
		super();
		this.cmp = cmp;
	}

	Comparator<ModelElementReference> cmp = null;

	@Override
	public int compare(ClockEntity o1, ClockEntity o2) {

		return cmp.compare(o1.getModelElementReference(),
				o2.getModelElementReference());
	}

}