/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.utils;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.trace.util.adapter.AdapterRegistry;
import fr.inria.aoste.timesquare.trace.util.adapter.IModelAdapter.EventEnumerator;

public class FilterEventEnumerator extends AModelElementReferenceFilter {
	final EventEnumerator eventEnumerator;

	public FilterEventEnumerator(EventEnumerator eventEnumerator) {
		super();
		this.eventEnumerator = eventEnumerator;
	}

	@Override
	protected boolean accept(EObject clock) {
		return AdapterRegistry.getAdapter(clock).getEventkind(clock) == eventEnumerator;
	}

	public EventEnumerator getEventEnumerator() {
		return eventEnumerator;
	}
	
	
	
}