/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.extensionpoints;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;

public class BehaviorOption {

	public BehaviorOption(Class<? extends BehaviorManager> manager, Class<? extends BehaviorManagerGUI> gui,
			String id, String plugin) {
		super();
		this.manager = manager;
		this.gui = gui;
		this.id = id;
		this.plugin = plugin;
	}

	public final Class<? extends BehaviorManager> getManager() {
		return manager;
	}

	public final String getId() {
		return id;
	}

	public final Class<? extends BehaviorManagerGUI> getGui() {
		return gui;
	}

	public String getPlugin() {
		return plugin;
	}

	private String id;
	private String plugin;
	private Class<? extends BehaviorManager> manager;
	private Class<? extends BehaviorManagerGUI> gui;
}