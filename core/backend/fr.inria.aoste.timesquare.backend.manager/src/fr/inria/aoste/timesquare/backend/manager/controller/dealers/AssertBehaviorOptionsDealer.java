/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.controller.dealers;

import java.util.List;
import java.util.Map;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.serialization.BehaviorOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.BehaviorPersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;

/**
 * The ClockBehaviorOptionsDealer has to restore the clock behavior serialized options provided by the different plugins.
 * It uses a ClockBehaviorOptionsSerializer in input to get the options.
 * It uses the Controller to add new entities with new behaviors.
 * 
 * @author dlengell
 *
 */
public class AssertBehaviorOptionsDealer extends OptionsDealer
{

	private BehaviorOptionsSerializer _clockBehaviorOptionsSerializer;
	
	/**
	 * The constructor for this class. 
	 * 
	 * @param behaviorManagers
	 * @param controller
	 * @param behaviorOptionsSerializer
	 */
	public AssertBehaviorOptionsDealer(  Controller controller, BehaviorOptionsSerializer behaviorOptionsSerializer )
	{
		super(  controller );
		_clockBehaviorOptionsSerializer = behaviorOptionsSerializer;
	}
	
	/**
	 * This method restores the clock behaviors.
	 */
	public void restoreOptions()
	{
		if ( _controller == null || _clockBehaviorOptionsSerializer == null  )
		{
			return;
		}
		
		for ( Map.Entry< String, List< BehaviorPersistentEntity > >  e : _clockBehaviorOptionsSerializer.getData().entrySet() )
		{
			List< BehaviorPersistentEntity > list = e.getValue();

			for ( BehaviorPersistentEntity pe : list )
			{
				AssertBehaviorEntity newEntity = transformPersistentEntityIntoEntity( pe );
				if ( newEntity != null )
				{
					_controller.addEntity( newEntity );
				}
			}
		}
	}
	
	
	/**
	 * Transforms an ClockBehaviorPersistentEntity in a ClockBehaviorEntity in the aim to re-do the behaviors.
	 * 
	 * @param persistentEntity
	 * @return The ClockBehaviorEntity obtained from the ClockBehaviorPersistentEntity passed in parameter.
	 */
	public AssertBehaviorEntity transformPersistentEntityIntoEntity( BehaviorPersistentEntity persistentEntity )
	{
		 if ( _controller == null )
    		 return null;
        BehaviorManager bm = _controller.getBehaviorManager(  );
        
        if (	bm == null )
        {
            return null;
        }

		
		ClockEntity _ce = null;
		
		//We re-find the clock if it's possible.
		List< ClockEntity > tmp = _controller.getAssert();
		boolean find = false;
		for ( ClockEntity ce : tmp )
		{
		    if ( persistentEntity.getID().equals( ce.getID() ) )
		    {
		        find = true;
		        _ce = ce;
		        break;
		    }
		}
		if ( !find ) return null;

		//We re-build the behavior.
		ClockBehavior b = bm.redoAssertBehavior( new ConfigurationHelper( _controller ), persistentEntity.getPersistentOptions() );
		if ( b == null ) //The plugin doesn't want or doesn't know how to deal with the PersistenOptions.
		    return null;
		
		AssertActivationState state = new AssertActivationState( persistentEntity.getState() );
		PersistentOptions options = persistentEntity.getPersistentOptions();
		String pluginName = persistentEntity.getPluginName();
		
		return new AssertBehaviorEntity(_ce, state, pluginName, b, options);
	}
}

