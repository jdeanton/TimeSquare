/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
/**
 * 
 */
package fr.inria.aoste.timesquare.backend.manager.extensionpoints;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;

public class BehaviorInstance{
	
	
	public BehaviorInstance(BehaviorManager manager, BehaviorManagerGUI gui, String id) {
		super();
		this.manager = manager;
		this.gui = gui;
		this.id=id;
	}

	
	protected final BehaviorManager getManager() {
		return manager;
	}
	protected final BehaviorManagerGUI getGui() {
		return gui;
	}

	
	protected final String getId() {
		return id;
	}


	String id;
	BehaviorManager manager;
	BehaviorManagerGUI gui;
}