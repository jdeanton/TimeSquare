/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.helpers;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;

/**
 * This class is an SelectionAdapter for the GUIHelper display color dialog function.
 * 
 * @author dlengell
 *
 */
public class ColorAdapter extends SelectionAdapter
{
    private RGB _color = null;
    private Composite _composite = null;
    private Button _button = null;
    
    /**
     * A constructor with parameters.
     * 
     * @param color
     * @param composite
     * @param button
     */
    public ColorAdapter( RGB color, Composite composite, Button button )
    {
        _color = color;
        _composite = composite;
        _button = button;
    }
    
    public void widgetSelected( SelectionEvent arg0 )
    {
        ColorDialog cd = new ColorDialog( _composite.getShell() );
        cd.setText( "Colors" );
        cd.setRGB( new RGB( 255, 255, 255 ) );
        RGB tmp_color = cd.open();
        if ( tmp_color != null ) 
        {
            _button.setForeground( new Color( null, tmp_color ) );
            
            _color.red = tmp_color.red;
            _color.blue = tmp_color.blue;
            _color.green = tmp_color.green;
            return;
        }
    }
}
