/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.utils;

import fr.inria.aoste.trace.ModelElementReference;

public class PairClockModelElementReference {

	
	
	public PairClockModelElementReference(ModelElementReference source, ModelElementReference destination) {
		super();
		this.source = source;
		this.destination = destination;
	}
	
	protected final ModelElementReference source;
	protected final ModelElementReference destination;
	
	public ModelElementReference getSource() {
		return source;
	}
	public ModelElementReference getDestination() {
		return destination;
	}
		
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PairClockModelElementReference other = (PairClockModelElementReference) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}
	
	
	
	
	
	
	
}
