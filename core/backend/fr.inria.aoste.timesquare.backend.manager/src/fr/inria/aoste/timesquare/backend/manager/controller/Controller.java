/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.widgets.Display;

import fr.inria.aoste.timesquare.backend.manager.controller.dealers.AssertBehaviorOptionsDealer;
import fr.inria.aoste.timesquare.backend.manager.controller.dealers.ClockBehaviorOptionsDealer;
import fr.inria.aoste.timesquare.backend.manager.controller.dealers.PluginOptionsDealer;
import fr.inria.aoste.timesquare.backend.manager.controller.dealers.RelationBehaviorOptionsDealer;
import fr.inria.aoste.timesquare.backend.manager.datastructure.DataStructureManager;
import fr.inria.aoste.timesquare.backend.manager.datastructure.Entity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.assertion.AssertBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.clock.ClockBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.launcher.extensionpoint.OutputOption;
import fr.inria.aoste.timesquare.backend.manager.serialization.BehaviorOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.BehaviorPersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.serialization.XMLStringMaker;
import fr.inria.aoste.timesquare.backend.manager.serialization.XMLStringParser;
import fr.inria.aoste.timesquare.backend.manager.serialization.plugin.PluginOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.relation.RelationBehaviorOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.relation.RelationBehaviorPersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.view.BehaviorManagerDialog;
import fr.inria.aoste.timesquare.backend.manager.view.View;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorConfigurator;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorOutputControler;
import fr.inria.aoste.timesquare.backend.manager.visible.ClockEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.DeleteHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.IBehaviorManagerDialog;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;
import fr.inria.aoste.timesquare.instantrelation.CCSLRelationModel.OccurrenceRelation;
import fr.inria.aoste.timesquare.launcher.core.console.ConsoleSimulation;
import fr.inria.aoste.timesquare.launcher.core.inter.CCSLInfo;
import fr.inria.aoste.timesquare.launcher.core.inter.ISolverForBackend;
import fr.inria.aoste.timesquare.launcher.debug.model.output.OutputSetupOption;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.InitOutputData;
import fr.inria.aoste.trace.AssertionState;
import fr.inria.aoste.trace.EventOccurrence;
import fr.inria.aoste.trace.LogicalStep;
import fr.inria.aoste.trace.PhysicalBase;

/**
 * This is the main class of this behavior module. It contains the main
 * algorithm and coordinates all the operations.
 * 
 * 
 * @author dlengell
 * 
 */

final public class Controller {

	private View _view = null;
	private DataStructureManager _dsm = new DataStructureManager();
	private List<ClockEntity> _clocks = null;
	private List<ClockEntity> _assert = null;
	private List<RelationEntity> _relationEntities = null;
	private IPath _modelPath = null;
	private OutputSetupOption _outputSetupOption = null;
	private BehaviorManager bm = null;
	private BehaviorManagerGUI bmgui = null;
	private OutputOption _outputOption;

	/**
	 * The constructor for this class.
	 * 
	 * @param behaviorOutputControler
	 * @param outputOption
	 */
	public Controller(BehaviorOutputControler behaviorOutputControler, OutputOption outputOption) {
		_outputOption = outputOption;
		// these options have to be initialized by setters.
		_clocks = Collections.emptyList();
		_assert = Collections.emptyList();
		_relationEntities = Collections.emptyList();
		_modelPath = null;
		_outputSetupOption = null;

		bm = behaviorOutputControler.createManager();
		bm.setController(this);
		bmgui = behaviorOutputControler.createManagerGUI();
	}

	public OutputOption getOutputOption() {
		return _outputOption;
	}

	public void activate() {
		_outputOption.setActive(true);
	}

	public boolean isActivable(ReportMessage rm, CCSLInfo ccslhelper) {
		return bm.isActivable(rm, ccslhelper);
	}

	/**
	 * Validate the options
	 * @return null if no Error, an error Message otherwise
	 */
	public String validate() {
		return bm.validate();
	}

	/**
	 * Returns the BehaviorManager which has the name passed in parameter.
	 * 
	 * @param pluginName
	 * @return The BehaviorManager which has the name passed in parameter.
	 */
	public BehaviorManager getBehaviorManager() {
		return bm;
	}

	/**
	 * Returns the PluginManagerGUI of the plugin which has the name passed in
	 * parameter.
	 * 
	 * @param pluginName
	 * @return The PluginManagerGUI of the plugin which has the name passed in
	 *         parameter.
	 */
	public BehaviorManagerGUI getBehaviorManagerGUI(String pluginName) {
		return bmgui;

	}

	public DataStructureManager getDsm() {
		return _dsm;
	}

	/**
	 * A setter for an OutputSetupOption for the launch configuration. This
	 * OutputSetupOption will be notify by the Controller when there is a change
	 * in the module.
	 * 
	 * @param outputSetupOption
	 */
	public void setOutputOption(OutputSetupOption outputSetupOption) {
		_outputSetupOption = outputSetupOption;
	}

	/**
	 * A setter for a modelPath and its list of clocks.
	 * 
	 * @param modelPath
	 * @param clocks
	 */
	public void setModelPathAndClocks(IPath modelPath, List<ClockEntity> clocks, List<ClockEntity> assert_,
			List<RelationEntity> relationEntities) {
		if (modelPath == null || clocks == null) {
			this.clearAndInit();
			return;
		}

		if (!(modelPath.equals(_modelPath))) // Different model.
		{
			this.clearAndInit();

			_modelPath = modelPath;
			_clocks = clocks;
			_assert = assert_;
			_relationEntities = relationEntities;
		} else // Same model, but the clocks have changed.
		{
			_clocks = clocks;
			_assert = assert_;
			_relationEntities = relationEntities;
			this.checkConsistency();
		}
		if (_clocks == null) {
			_clocks = Collections.emptyList();
		}
		if (_assert == null) {
			_assert = Collections.emptyList();
		}
		if (_relationEntities == null) {
			_relationEntities = Collections.emptyList();
		}

		if (_view != null) {
			_view.refresh();
		}
	}

	/**
	 * Displays the view in the Composite.
	 * 
	 * @param composite
	 */
	public void setView(View v) {
		if (v == null) {
			_view = null;
			return;
		}
		_view = v;
		_view.setController(this);
		// We give the BehaviorManagers list to the view
		_view.setBehaviorManagers(bm);
	}

		
	/**
	 * When an user clicks on the BehaviorManager button of the view, the
	 * controller calls the BehaviorManager.
	 * 
	 * If the BehaviorManager has declared a BehaviorManagerGUI in the extension
	 * point, it will be used and displayed in a JFace Dialog. Else the method :
	 * manageBehavior( ConfigurationHelper ) of the BehaviorManager will be
	 * called.
	 * 
	 */
	public void notifyBehaviorManager(BehaviorManager bm) {
		if (bm == null) {
			return;
		}

		BehaviorManagerGUI behaviorManagerGUI = getBehaviorManagerGUI(bm.getPluginName());
		if (behaviorManagerGUI != null) {
			behaviorManagerGUI.setBehaviorManager(bm);
			behaviorManagerGUI.setHelper(new ConfigurationHelper(this));//, new GUIHelper(this,behaviorManagerGUI));
			_view.displayBehaviorManagerDialog(behaviorManagerGUI);
		} else {
			bm.manageBehavior(new ConfigurationHelper(this));
		}
	}

	public IBehaviorManagerDialog createDialog(final BehaviorManager bm) {
		if (bm == null) {
			return null;
		}
		final BehaviorManagerGUI behaviorManagerGUI = getBehaviorManagerGUI(bm.getPluginName());
		if (behaviorManagerGUI != null) {
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {

					behaviorManagerGUI.setBehaviorManager(bm);
					behaviorManagerGUI.setHelper(new ConfigurationHelper(Controller.this));
					//, new GUIHelper(Controller.this,behaviorManagerGUI));
					// BehaviorManagerDialog bmd=
					BehaviorManagerDialog.create(behaviorManagerGUI);
				}
			});

			return behaviorManagerGUI.getDialog();
		}
		return null;

	}

	/**
	 * Tells the DataStructure to add a Entity.
	 * 
	 * @param entity
	 */
	public void addEntity(ClockBehaviorEntity entity) {
		_dsm.addEntity(entity);
	}

	/**
	 * Tells the DataStructure to add a Entity.
	 * 
	 * @param entity
	 */
	public void addEntity(AssertBehaviorEntity entity) {
		_dsm.addEntity(entity);
	}

	/**
	 * Tells the DataStructure to add a Entity.
	 * 
	 * @param entity
	 */
	public void addEntity(RelationBehaviorEntity entity) {
		_dsm.addEntity(entity);
	}

	/**
	 * Returns the clock list.
	 * 
	 * @return The clock list.
	 */
	public List<ClockEntity> getClocks() {
		return _clocks;
	}

	public List<ClockEntity> getAssert() {
		return _assert;
	}

	public List<RelationEntity> getRelationEntities() {
		return _relationEntities;
	}

	public List<EObject> getRelationConstraint() {
		List<EObject> l = new ArrayList<EObject>();
		for (RelationEntity e : getRelationEntities()) {
			EObject eo = e.getRelation().getRelationConstraint();
			if (l.indexOf(eo) == -1)
				l.add(eo);
		}
		return l;
	}

	public List<EObject> getCcslConstraints() {
		List<EObject> l = new ArrayList<EObject>();
		for (RelationEntity e : getRelationEntities()) {
			EObject eo = e.getRelation().getCcslConstraint();
			if (l.indexOf(eo) == -1)
				l.add(eo);
		}
		return l;
	}

	/**
	 * Saves the actual behavior configuration into a XML String.
	 * 
	 * @return The XML String which contains the actual behavior configuration.
	 */
	public String saveConfigurations(OutputOption configuration) {
		try {
		// We get all the plugin options and we give them to the
			// PluginOptionsSerializer.
			PluginOptionsSerializer pos = new PluginOptionsSerializer();
			// for (BehaviorManager bm : getManagers())
			{
				if (_dsm.pluginPresent(bm.getPluginName()) && bm.getPluginOptions() != null) {
					pos.addPluginOptions(bm.getPluginName(), bm.getPluginOptions());
				}
			}

			// We get all the clockbehavior options and we give them to the
			// AssertBehaviorOptionsSerializer.
			BehaviorOptionsSerializer cbos = new BehaviorOptionsSerializer();

			for (ClockBehaviorEntity e : _dsm.getClockBehaviorEntity()) {
				BehaviorPersistentEntity pe = e.transformEntityIntoPersistentEntity();
				if (pe != null) {
					cbos.addPersistentEntity(e.getPluginName(), pe);
				}
			}

			// We get all the relationbehavior options and we give them to the
			// RelationBehaviorOptionsSerializer.
			RelationBehaviorOptionsSerializer rbos = new RelationBehaviorOptionsSerializer();

			for (RelationBehaviorEntity e : _dsm.getRelationBehaviorEntity()) {
				RelationBehaviorPersistentEntity pe = e.transformEntityIntoPersistentEntity();
				if (pe != null) {
					rbos.addPersistentEntity(e.getPluginName(), pe);
				}
			}
			BehaviorOptionsSerializer abos = new BehaviorOptionsSerializer();

			for (AssertBehaviorEntity e : _dsm.getAssertBehaviorEntity()) {
				BehaviorPersistentEntity pe = e.transformEntityIntoPersistentEntity();
				if (pe != null) {
					abos.addPersistentEntity(e.getPluginName(), pe);
				}
			}

			// We serialize all the behaviors in a XML String.
			XMLStringMaker xmlStringMaker = new XMLStringMaker(bm, pos, cbos, rbos, abos);
			return xmlStringMaker.createXMLString(configuration);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Restores the behavior configuration contained in the String xmlOptions.
	 * 
	 * @param xmlOptions
	 */
	public void restoreConfigurations(OutputOption configuration) {
		String xmlOptions = configuration.getStringOptions();
		try
		{
			// We clear the model and init the behaviorManagers.
			this.clearAndInit();

			PluginOptionsSerializer pos = new PluginOptionsSerializer();
			BehaviorOptionsSerializer cbos = new BehaviorOptionsSerializer();
			RelationBehaviorOptionsSerializer rbos = new RelationBehaviorOptionsSerializer();
			BehaviorOptionsSerializer abos = new BehaviorOptionsSerializer();

			// We deserialize the XML String.
			XMLStringParser xmlStringParser = new XMLStringParser(pos, cbos, rbos, abos);
			xmlStringParser.parse(xmlOptions, configuration, this);

			// We give the plugin options to the plugin thanks to a
			// PluginOptionsDealer.
			PluginOptionsDealer pluginOptionsDealer = new PluginOptionsDealer(this, pos);
			pluginOptionsDealer.restoreOptions();

			// We re-do the ClockBehaviors thanks to a
			// ClockBehaviorOptionsDealer.
			ClockBehaviorOptionsDealer clockBehaviorOptionsDealer = new ClockBehaviorOptionsDealer(this, cbos);
			clockBehaviorOptionsDealer.restoreOptions();

			// We re-do the RelationBehaviors thanks to a
			// RelationBehaviorOptionsDealer
			RelationBehaviorOptionsDealer relationOptionsDealer = new RelationBehaviorOptionsDealer(this, rbos);
			relationOptionsDealer.restoreOptions();

			AssertBehaviorOptionsDealer assertOptionsDealer = new AssertBehaviorOptionsDealer(this, abos);
			assertOptionsDealer.restoreOptions();

			if (_view != null) {
				_view.refresh();
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}

	/* TODO work on this delete protocol. There are possible loops in the way behaviors are deleted.
	 * the deleteInProgress boolean is used to break such loops.
	 * Exemple of such loop (inner stack frame first) :
	 *             Controller.deleteEntityManager(Entity) line: 441	            if (delete)
	 *           Controller.deleteEntity(ClockBehaviorEntity) line: 467	        deleteEntityManager(e);
	 *         DeleteHelper.deleteEntity(ClockBehaviorEntity) line: 26	
	 *      VCDGeneratorManager.deleteEntity(Entity, DeleteHelper) line: 740	deletehelper.deleteEntity(cbe);
	 *    Controller.deleteEntityManager(Entity) line: 445	           bm.deleteEntity(e, new DeleteHelper(this));
	 *  Controller.deleteEntity(ClockBehaviorEntity) line: 467	       deleteEntityManager(e);
	 */
	private boolean deleteInProgress = false;

	synchronized private void deleteEntityManager(Entity e) {
		if (deleteInProgress)
			return;
		try {
			deleteInProgress = true;
			bm.deleteEntity(e, new DeleteHelper(this));
		} catch (Exception exception) {
			System.err.println(exception);
		} finally {
			deleteInProgress = false;
		}
	}

	public void deleteEntity(AssertBehaviorEntity e) {
		deleteEntityManager(e);
		if (e != null) {
			_dsm.deleteAssertBehaviorEntity(e);
			// this.refresh();
		}
	}

	/**
	 * Tells the DataStructure to delete an Entity.
	 * 
	 * @param e
	 */
	public void deleteEntity(ClockBehaviorEntity e) {
		deleteEntityManager(e);
		if (e != null) {
			_dsm.deleteClockBehaviorEntity(e);
			// this.refresh();
		}
	}

	/**
	 * Tells the DataStructure to delete an Entity.
	 * 
	 * @param e
	 */
	public void deleteEntity(RelationBehaviorEntity e) {
		deleteEntityManager(e);
		if (e != null) {
			_dsm.deleteRelationBehaviorEntity(e);
			// this.refresh();
		}
	}

	/**
	 * Tells the DataStructure to delete all the Entities (so behaviors)
	 * associated with the plugin name passed in parameter.
	 * 
	 * @param pluginName
	 */
	public void deleteEntitiesByPluginName(String pluginName) {
		_dsm.deleteEntitiesByPluginName(pluginName);
	}

	/**
	 * Tells the DataStructure that it has to execute the behaviors for the
	 * ClockEntity and the ClockState passed in parameter.
	 * 
	 * @param clockEntity
	 * @param clockState
	 */
	public void executeNewBehaviors(ClockEntity clockEntity, EventOccurrence clockState) {
		if (clockEntity == null || clockState == null) {
			return;
		}

		_dsm.executeNewBehaviors(clockEntity, clockState);
	}

	public void executeNewBehaviors(ClockEntity assertEntity, AssertionState assertState) {
		if (assertEntity == null || assertState == null) {
			return;
		}

		_dsm.executeNewBehaviors(assertEntity, assertState);
	}

	/**
	 * Tells the DataStructure that it has to execute the behaviors for the
	 * ClockEntity and the ClockState passed in parameter.
	 * 
	 * @param clockEntity
	 * @param clockState
	 */
	public void executeBehaviors(ClockEntity clockEntity, EventOccurrence clockState) {
		if (clockEntity == null || clockState == null) {
			return;
		}

		_dsm.executeBehaviors(clockEntity, clockState);
	}

	/**
	 * Tells the DataStructure that it has to execute the behaviors for the
	 * Relation passed in parameter.
	 * 
	 * @param relation
	 */
	public void executeBehaviorsRelation(OccurrenceRelation relation) {
		if (relation == null) {
			return;
		}

		_dsm.executeBehaviors(relation);
	}

	/**
	 * Checks the consistency of the DataStructure with the current clocks.
	 */
	private void checkConsistency() {
		if (_clocks == null || _assert == null) {
			return;
		}

		_dsm.checkConsistency(_clocks, _assert);
	}

	/**
	 * Initializes all the plugin managers ( BehaviorManager ).
	 */
	public void initBehaviorManager() {
		bm.init(new ConfigurationHelper(this));
	}

	/**
	 * Tells the plugins that it is the end of the simulation.
	 */
	public void end() {
		if (this.pluginPresent(bm.getPluginName())) {
			bm.end(new ConfigurationHelper(this));
		}
	}

	/**
	 * Tells the plugins that there is a new step.
	 * 
	 * @param stepNumber
	 *            the new step number
	 * @param timed
	 * @param step
	 *            <p>
	 *            the current new step
	 */
	public void aNewStep(int stepNumber, boolean timed, LogicalStep step) {
		if (pluginPresent(bm.getPluginName())) {
			bm.setCurrentStep(step);
			bm.aNewStep(stepNumber);
			bm.setCurrentStep(null);
		}
	}

	/**
	 * 
	 * @param stepNumber
	 *            the new step number
	 * @param timed
	 * @param step
	 *            the current new step
	 */
	public void aPostNewStep(int stepNumber, boolean timed, LogicalStep step) {
		if (pluginPresent(bm.getPluginName())) {
			bm.setCurrentStep(step);
			bm.aPostNewStep(stepNumber);
			bm.setCurrentStep(null);
		}
	}

	/**
	 * Tells the plugins that there is a new step.
	 * 
	 * @param stepNumber
	 *            the old step number
	 * @param timed
	 */
	public void aStep(int stepNumber, boolean timed, LogicalStep step) {
		if (pluginPresent(bm.getPluginName())) {
			bm.setCurrentStep(step);
			(bm).repeatStep(stepNumber);
			bm.setCurrentStep(null);
		}
	}

	public List<PhysicalBase> getPhysicalBases() {
		List<PhysicalBase> lst = null;
		if (_data != null)
			lst = _data.getLst();
		if (lst == null)
			lst = new ArrayList<PhysicalBase>();
		return Collections.unmodifiableList(lst);

	}

	InitOutputData _data = null;

	/**
	 * Tells the plugins that the execution will begin.
	 * 
	 * @param folderin
	 *            A folder for plugin's output files.
	 * @param namefilein
	 *            A file name for plugin's output files.
	 */
	public void beforeExecution(InitOutputData data, ConsoleSimulation cs,ISolverForBackend solver) {
		_data = data;
		IPath folderin = data.getFolder();
		String namefilein = data.getNamefile();
		// for (BehaviorManager bm : getManagers())
		{
			if (pluginPresent(bm.getPluginName())) {
				ConfigurationHelper ch = new ConfigurationHelper(this);
				ch.setCs(cs);
				bm.beforeExecution(ch, folderin, namefilein,solver);
			}
		}
	}

	/**
	 * Getter on the model's path. This method is called by the
	 * ConfigurationHelper.
	 * 
	 * @return an IPath of the model's path
	 */
	public IPath getModelPath() {
		return _modelPath;
	}

	/**
	 * This method is called when a changed is detected. It refreshes the View
	 * and notifies the OutputSetupOption.
	 * 
	 */
	public void refresh() {
		if (_view != null) {
			_view.refresh();
		}

		if (_outputSetupOption != null) {
			_outputSetupOption.notifyChange();
		}
	}

	/**
	 * Return true if the plugin passed in parameter has declared some
	 * behaviors. Else returns false.
	 * 
	 * @param pluginName
	 * @return
	 */
	private boolean pluginPresent(String pluginName) {
		return _dsm.pluginPresent(pluginName);
	}

	/**
	 * Clears the DataStructure and initializes the BehaviorManagers.
	 */
	private void clearAndInit() {
		_dsm.clear();
		this.initBehaviorManager();
	}

	BehaviorConfigurator<?> bc = null;

	public BehaviorConfigurator<?> getConfigurator() {
		if (bc == null) {
			bc = getBehaviorManager().getConfigurator(new ConfigurationHelper(this));
			bc.setController(this);
		}
		return bc;
	}
}
