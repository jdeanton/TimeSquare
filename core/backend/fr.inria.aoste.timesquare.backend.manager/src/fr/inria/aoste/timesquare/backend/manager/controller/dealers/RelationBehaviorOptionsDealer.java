/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.controller.dealers;

import java.util.List;
import java.util.Map;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationActivationState;
import fr.inria.aoste.timesquare.backend.manager.datastructure.relation.RelationBehaviorEntity;
import fr.inria.aoste.timesquare.backend.manager.serialization.relation.RelationBehaviorOptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.relation.RelationBehaviorPersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.ConfigurationHelper;
import fr.inria.aoste.timesquare.backend.manager.visible.PersistentOptions;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationBehavior;
import fr.inria.aoste.timesquare.backend.manager.visible.RelationEntity;

/**
 * The RelationBehaviorOptionsDealer has to restore the relation behavior serialized options provided by the different plugins.
 * It uses a RelationBehaviorOptionsSerializer in input to get the options.
 * It uses the Controller to add new entities with new behaviors.
 * 
 * @author dlengell
 *
 */
public class RelationBehaviorOptionsDealer extends OptionsDealer
{
    private RelationBehaviorOptionsSerializer _relationBehaviorOptionsSerializer;

    /**
     * A constructor for this class.
     * 
     * @param behaviorManagers
     * @param controller
     * @param behaviorOptionsSerializer
     */
    public RelationBehaviorOptionsDealer(  Controller controller, RelationBehaviorOptionsSerializer behaviorOptionsSerializer )
    {
        super( controller );
        _relationBehaviorOptionsSerializer = behaviorOptionsSerializer;
    }
    
    /**
     * This method restores the relation behaviors.
     */
    public void restoreOptions()
    {
        if (  _controller == null || _relationBehaviorOptionsSerializer == null  )
        {
            return;
        }
        
        for ( Map.Entry< String, List< RelationBehaviorPersistentEntity > >  e : _relationBehaviorOptionsSerializer.getData().entrySet() )
        {
            List< RelationBehaviorPersistentEntity > list = e.getValue();
            
            for ( RelationBehaviorPersistentEntity pe : list )
            {
                RelationBehaviorEntity newEntity = transformPersistentEntityIntoEntity( pe );
                if ( newEntity != null )
                {
                    _controller.addEntity( newEntity );
                }
            }
        }
    }
    
    /**
     * Transforms an RelationBehaviorPersistentEntity in a RelationBehaviorEntity in the aim to re-do the behaviors.
     * 
     * @param persistentEntity
     * @return  The RelationBehaviorEntity obtained from the RelationBehaviorPersistentEntity passed in parameter.
     */
    public RelationBehaviorEntity transformPersistentEntityIntoEntity( RelationBehaviorPersistentEntity persistentEntity )
    {
    	 if ( _controller == null )
    		 return null;
        BehaviorManager bm = _controller.getBehaviorManager( );
        
        if (	bm == null )
        {
            return null;
        }
        
        PersistentOptions po= persistentEntity.getPersistentOptions();
        RelationBehaviorEntity e = new RelationBehaviorEntity();
        List<RelationEntity> tmp = _controller.getRelationEntities();
        RelationEntity re=null; 
       
        
        	String s=persistentEntity.getRelationid();
       
	
		for ( RelationEntity ce : tmp )
		{
		    if ( s.equals( ce.getRelation().getID()) )
		    {
		       
		      re=ce;
		    }
		}
        if( re==null)
        	return null;
        //We re-build the behavior.
        RelationBehavior b = bm.redoRelationBehavior( new ConfigurationHelper( _controller ), po );
        if ( b == null ) //The plugin doesn't want or doesn't know how to deal with the PersistenOptions.
        {
            return null;
        }
        else
        {
            e.setBehavior( b );                
        }
        
        e.setRelationActivationState( new RelationActivationState( re) );
        e.setPersistentOptions( persistentEntity.getPersistentOptions() );
        e.setPluginName( persistentEntity.getPluginName() );
        
        return e;
    }
}
