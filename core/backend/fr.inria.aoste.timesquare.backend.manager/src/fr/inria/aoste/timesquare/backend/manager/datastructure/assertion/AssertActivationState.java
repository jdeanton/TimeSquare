/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.datastructure.assertion;

import fr.inria.aoste.trace.AssertionState;

/**
 * The AssertActivationState represents a activation state for a behavior on a relation. 
 * .
 * 
 * 
 * 
 * @author bferrero
 *
 */
public class AssertActivationState
{
    /**
     * The number of possible relation.
     */
    public final static int assertionNumber = 2;
    
    /**
     * The name of the possible relation.
     */
    public final static String[] assertlist = 
    { 
        "satisfied", 
        "failed"      
    };
    
    private boolean[] _relation;
    
    /**
     * A constructor for this class.
     * It takes an array of boolean in parameter.
     * This array has to be in the good format.
     * 
     * @param relation
     */    
    public AssertActivationState( boolean[] relation )
    {
        _relation = null;

        if ( isValidRelation( relation ) )
        {
            _relation = relation;
        }
    }
    
    /**
     * A constructor for this class.
     * 
     * 
     * @param assertion
     */
    public AssertActivationState( AssertionState assertion)
    {
        if ( assertion == null )
        {
            _relation = null;
            return;
        }
        
        boolean[] tmp = new boolean[ AssertActivationState.assertionNumber ];
        for ( int i=0; i<tmp.length; ++i )
        {
            tmp[i] = false;
        }
        
        if ( assertion.isIsViolated()==false)
            tmp[0] = true;
        if ( assertion.isIsViolated())        	
            tmp[1] = true;
       
            
        if ( isValidRelation( tmp ) )
        {
            _relation = tmp;
        }
    }
    
    /**
     * Checks if the boolean array in parameter is a valid relation.
     * 
     * @param relation
     * @return true if the boolean array passed in parameter has the good format.
     */
    public boolean isValidRelation( boolean[] relation )
    {
        if ( relation == null )
        {
            return false;
        }
        
        if ( relation.length == AssertActivationState.assertionNumber )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Returns the boolean array.
     * 
     * @return The boolean array.
     */
    public boolean[] getAssertState()
    {
        return _relation;
    }
    
    /**
     * Returns the description of the RelationEntity.
     * 
     * @return The String description of the RelationEntity.
     */
    public String getDescription()
    {   
        String tmp = "";
        if ( _relation ==null)
        	return "null";
        for ( int i = 0; i<_relation.length; ++i )
        {
            if ( _relation[i] == true )
            {
                if ( tmp.length()==0)// == "" )
                {
                    tmp = tmp.concat( AssertActivationState.assertlist[i] );
                }
                else
                {
                    tmp = tmp.concat( "|"+ AssertActivationState.assertlist[i] );
                }
            }
        }
        
        return tmp;
    }
    
    @Override
	public int hashCode() {
		int n=0;
		int k=1;
		for ( int i=0; i<_relation.length; ++i )
		{
			if ( _relation[i] )
			{
				n = n+ k;
			}
			k = k*2;
			if (k<=0) { k=1; }
		}
		return n;
	}
    /**
     * Redefines the equals method of Object.
     * This method is very important when we delete/modify an Entity of the DataStructure (so a behavior).
     */
    public boolean equals( Object o )
    {
        
        if ( o == this )
        {
            return true;
        }
        
        if ( o instanceof AssertActivationState )
        {
            AssertActivationState re = ( AssertActivationState ) o;
            
            if ( re.getAssertState().length == this._relation.length )
            {
                boolean b = true;
                for ( int i=0; i<_relation.length; ++i )
                {
                    if ( re.getAssertState()[i] != _relation[i] )
                    {
                        b = false;
                    }
                }
                return b;
            }
        }

        return false;
    }
        
    public static boolean stateActivationOK( AssertActivationState clock1, AssertActivationState clock2 )
	{
	    if ( clock1 == null || clock2 == null )
	    {
	        return false;
	    }
	    
	    for ( int i=0; i<clock1.getAssertState().length; ++i )
	    {
	        if ( !( ( clock1.getAssertState()[i] && clock2.getAssertState()[i] ) == clock2.getAssertState()[i] ) )
	        {
	            return false;
	        }
	    }
	    
	    return true;
	}
}
