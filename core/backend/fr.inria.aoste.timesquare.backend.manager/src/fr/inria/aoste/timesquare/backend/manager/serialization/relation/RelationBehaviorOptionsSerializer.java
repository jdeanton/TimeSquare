/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.serialization.relation;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import fr.inria.aoste.timesquare.backend.manager.serialization.OptionsSerializer;
import fr.inria.aoste.timesquare.backend.manager.serialization.PersistentEntity;
import fr.inria.aoste.timesquare.backend.manager.serialization.SpecialObjectInputStream;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;

/**
 * The RelationBehaviorOptionsSerializer manages the serialization/deserialization 
 * of the RelationBehaviorPersistentEntities.
 * 
 * Serialization step :
 * The RelationBehaviorPersistentEntities are added by the Controller into a private Map.
 * The serializer is used by the XMLStringMaker providing serialized RelationBehaviorPersistentEntities 
 * in a String for each plugin.
 * 
 * Deserialization step :
 * The XMLStringParser gives to the RelationBehaviorOptionsSerializer some strings.
 * The strings are deserialized, and the RelationBehaviorPersistentEntities are added to the private Map.
 * 
 * @author dlengell
 *
 */
public class RelationBehaviorOptionsSerializer extends OptionsSerializer
{

    private Map< String, List< RelationBehaviorPersistentEntity > > _data;
    
    /**
     * A constructor with no parameter.
     */
    public RelationBehaviorOptionsSerializer()
    {
        super();
        
        _data = new HashMap< String, List< RelationBehaviorPersistentEntity > >();
    }
    
    /**
     * Returns the Map containing the RelationBehaviorPersistentEntities.
     * 
     * @return A List<RelationBehaviorPersistentEntity> linked to his plugin name.
     */
    public Map< String, List< RelationBehaviorPersistentEntity > > getData()
    {
        return _data;
    }

    /**
     * Adds a RelationBehaviorPersistentEntity to the Map.
     * 
     * @param pluginName
     * @param persistentEntity
     */
    public void addPersistentEntity( String pluginName, RelationBehaviorPersistentEntity persistentEntity )
    {
        if ( _data != null )
        {
            boolean find = false;

            //If the key already exists.
            for ( Map.Entry< String, List< RelationBehaviorPersistentEntity > >  e : _data.entrySet() )
            {
                if ( e.getKey().equals( pluginName ) )
                {
                    e.getValue().add( persistentEntity );
                    find = true;
                }
            }
            
            //If the key is not found.
            if ( !find )
            {
                _data.put( pluginName, new ArrayList< RelationBehaviorPersistentEntity >() );
                _data.get( pluginName ).add( persistentEntity );
            }
        }
    }    

    /**
     * Adds a list of RelationBehaviorPersistentEntity to the Map.
     * Be careful, there is no test here about the key (if it's already exists or not).
     * 
     * @param pluginName
     * @param persistentEntities
     */
    public void addPersistentEntities( String pluginName, List< RelationBehaviorPersistentEntity > persistentEntities )
    {
        if ( _data != null && !( _data.containsKey( pluginName ) ) )
        {
            _data.put( pluginName, persistentEntities );
        }
    }
    
    
    public boolean doesPluginNeedSerialization( String pluginName )
    {
        if ( _data == null )
        {
            return false;
        }
        return _data.containsKey( pluginName );
    }
    
    
    public String getSerializedOptions( String pluginName )
    {
        String result = "";
        
        if ( _data == null )
        {
            return result;
        }
        
        List< RelationBehaviorPersistentEntity > list = _data.get( pluginName );
        if ( list == null )
        {
            return result;
        }
        
        //We serialize a RelationBehaviorPersistentEntity list into a String.
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ObjectOutputStream oos= new ObjectOutputStream( baos );
            try
            {
                oos.writeObject( list ); 
                oos.flush();
            }
            finally
            {
                try
                {
                    oos.close();
                }
                finally
                {
                    result = transformByteInString( baos.toByteArray() ); //We transform the binaries into a String.
                    baos.close();
                }
            }
        }
        catch( IOException ioe )
        {
            ioe.printStackTrace();
        }
        
        return result;
    }
    
    public void deserializeOptions( String options, BehaviorManager behaviorManager )
    {
        if ( options == null || (options.length()==0 )  || behaviorManager == null || _data == null ) 
        {
            return;
        }
    
        try
        {
            ByteArrayInputStream bais = new ByteArrayInputStream( transformStringInByte( options ) );
        
            SpecialObjectInputStream sois = new SpecialObjectInputStream( bais, behaviorManager.getClass().getClassLoader() );
        
            try
            {   
               List< ? > readObject = ( List< ? > )sois.readObject();

               if ( readObject != null )
               {
                   List< RelationBehaviorPersistentEntity > list = new ArrayList< RelationBehaviorPersistentEntity >();
                   ListIterator<?> li = readObject.listIterator();
                   while( li.hasNext() )
                   {
                       Object o = li.next();
                       if ( o instanceof PersistentEntity )
                       {
                           list.add( ( RelationBehaviorPersistentEntity )o );
                       }
                   }
                   this.addPersistentEntities( behaviorManager.getPluginName(), list );
               }
            }
            finally
            {
                try
                {
                    sois.close();
                }
                finally
                {
                    bais.close();
                }
            }
            
        }
        catch( Throwable e )
        {
            e.printStackTrace();
        }
            
    }

}
