/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import fr.inria.aoste.trace.relation.IDescription;


/**
 * This represents a Clock.
 * It encapsulates a CCSL model clock.
 * 
 * It can be a :
 * 
 * fr.inria.aoste.umlCCSL.CCSLUMLmodel.Clock
 * fr.inria.aoste.umlCCSL.CCSLUMLmodel.Clock
 * TimeModel.CCSLModel.GenericPattern_4_Expression_and_Relation.N_aryExpressionConcrete
 * 
 * @author bferrero
 *
 */
public class RelationEntity
{
    private final IDescription relation;
    private final String _URIfragment;
    private final String name , clkid;
     
    //static AdapterRegistry adapterRegistry=AdapterRegistry.getDefault();
    
    /**
     * A constructor with an EObject in parameter.
     * 
     * @param clock
     */
    public RelationEntity( IDescription _relation )
    {
        relation = _relation;
        _URIfragment = relation.getID();  
        	//assert_.eResource().getURIFragment( assert_ );
        name = relation.toString();
        clkid =  name+_URIfragment;
    }
    
  /*  private String getURIfragment()
    {
        return _URIfragment;
    }*/
    
    /**
     * Returns the CCSL Model clock as an EObject.
     * 
     * @return The CCSL Model clock.
     */
    public IDescription getRelation()
    {
        return relation;
    }
    
    /**
     * Returns the clock ID.
     * 
     * @return A String containing the clock ID.
     */
    public String getRelationID()
    {
        return clkid;// this.getClockName()+this.getURIfragment();
    }
    
    /**
     * Returns the clock name.
     * 
     * @return A String containing the clock name.
     */
    public String getRelationName()
    {
      	return name ;           
    }
    
    /**
     * Returns a String representation of the clock.
     * 
     * @return A String representation of the clock.
     */
    public String getDescription()
    {
        return relation.toString();
    }
    
    public String toString()
    {
        return this.getRelationName();
    }
    
   
    
    
    
    public boolean equals( Object o )
    {
    	try{
        if ( o == this )
        {
            return true;
        }
        if ( o instanceof RelationEntity )
        {
            RelationEntity ce = ( RelationEntity ) o;
            if ( ce.getRelationName().equals( name ) && ce.getRelationID().equals( clkid ) )
            {
                return true;
            }
        }
    	}
    	catch (Throwable e) {
			
		}
        return false;
    }

	@Override
	public int hashCode() {
		int n= name!=null? name.hashCode() : 0;
			int c=clkid!=null? clkid.hashCode() : 0;
		return n+c;
	}
}
