/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.helpers;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;
import fr.inria.aoste.timesquare.backend.manager.visible.GUIHelper;

/**
 * This class is an SelectionAdapter for the GUIHelper display list function.
 * 
 * @author bferrero
 * 
 */
public class TreeAdapter extends SelectionAdapter {

	private BehaviorManagerGUI _gui;
	
	public TreeAdapter(BehaviorManagerGUI gui) {
		_gui=gui;
	}

	public void widgetSelected(SelectionEvent arg0) {
		TreeItem[] s = ((Tree) arg0.widget).getSelection();
		if (s.length == 1) {
			((Tree) arg0.widget).setData(GUIHelper.COMBOSELECTION, s[0].getData());
		}
		if ( _gui!=null)
			_gui.updateOKStatus();
		return;
	}
}
