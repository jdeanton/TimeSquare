/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.visible;

import fr.inria.aoste.timesquare.backend.manager.controller.Controller;
import fr.inria.aoste.timesquare.launcher.debug.model.output.ReportMessage;
import fr.inria.aoste.timesquare.launcher.debug.model.proxy.Configurator;

public abstract class BehaviorConfigurator<T extends BehaviorManager> implements Configurator {

	protected ConfigurationHelper configurationHelper = null;
	protected T behaviorManager = null;

	public BehaviorConfigurator(ConfigurationHelper _ch, T _behaviorManager) {
		super();
		configurationHelper = _ch;
		behaviorManager = _behaviorManager;
	}

	public final boolean isActivable(ReportMessage rm) {
		return controller.isActivable(rm, controller.getOutputOption().getCcslhelper());
	}

	@Override
	public final void activate() throws Exception {
		ReportMessage rm = new ReportMessage();
		if (!isActivable(rm)) {
			throw new Exception(rm.getMessage());
		}
		behaviorManager.activate();
	}

	final public String validate() {
		return behaviorManager.validate();
	}

	final protected ConfigurationHelper getConfigurationHelper() {
		return configurationHelper;
	}

	final public T getBehaviorManager() {
		return behaviorManager;
	}

	private Controller controller = null;

	public final void setController(Controller controller) {
		this.controller = controller;
	}


	/**
	 *  Create a DialogBox with a BehaviorManagerGUI 
	 * @return
	 */
	public IBehaviorManagerDialog openDialog()
	{
		IBehaviorManagerDialog dialog=controller.createDialog(behaviorManager	);
		if( dialog!=null)
		{
			return dialog;
		}
		throw new RuntimeException("no dialog for "+behaviorManager.getPluginName());
	}
}
