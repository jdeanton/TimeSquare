/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.backend.manager.extensionpoints;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;

import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManager;
import fr.inria.aoste.timesquare.backend.manager.visible.BehaviorManagerGUI;
import fr.inria.aoste.timesquare.utils.extensionpoint.ExtensionPointManager;
import fr.inria.aoste.timesquare.utils.extensionpoint.IExtensionManager;

public class PEBehavior implements IExtensionManager {
	static PEBehavior _default = null;

	static synchronized public final PEBehavior getDefault() {
		if (_default == null) {
			_default = new PEBehavior();
		}
		return _default;
	}

	private PEBehavior() {
		ExtensionPointManager.findAllExtensions(this);
	}

	private static final String extensionPointID = "fr.inria.aoste.timesquare.backend.manager.behaviormanager";
	@Override
	final public String getExtensionPointName() {
		return extensionPointID;
	}
	
	@Override
	final public String getPluginName() {
		return "fr.inria.aoste.timesquare.backend.manager";
	}

	private HashMap<String, BehaviorOption> _mapBehavior;
	private List<BehaviorOption> lst;

	public int size() {
		return _mapBehavior.size();
	}

	@Override
	public void initExtension(IConfigurationElement ice) throws Throwable {
		// load BehaviorManager
		Class<? extends BehaviorManager> bm = ExtensionPointManager.getPointExtensionClass(ice, "BehaviorManager", BehaviorManager.class);
		// load BehaviorManagerGUI
		Class<? extends BehaviorManagerGUI> bmg = null;
		if (ice.getAttribute("BehaviorManagerGUI") != null) {
			bmg = ExtensionPointManager.getPointExtensionClass(ice, "BehaviorManagerGUI", BehaviorManagerGUI.class);
			// test BehavioManagerGUI
			bmg.newInstance();
		}

		// test BehaviorManager

		// record BehaviorManager

		String idplug = bm.newInstance().getPluginName();
		BehaviorOption bo = new BehaviorOption(bm, bmg, idplug, ExtensionPointManager.getPluginName(ice));

		if(_mapBehavior==null) _mapBehavior = new HashMap<String, BehaviorOption>();
		_mapBehavior.put(idplug, bo);

		if (lst == null) lst = new ArrayList<BehaviorOption>();			
		lst.add(bo);
		System.out.println("Behavior loading :" + idplug);
	}

	public List<BehaviorOption> getBehaviorOptionList() {
		return Collections.unmodifiableList(lst);
	}

	public HashMap<String, BehaviorInstance> createNewInstance() {
		System.out.println("Create Instance Behavior ");
		HashMap<String, BehaviorInstance> hmsbi = new HashMap<String, BehaviorInstance>();

		for (BehaviorOption bo : _mapBehavior.values()) {
			try {
				//	if (bo.activation != null)
				//	if (!bo.activation.isActivable("behavior"))
				//		continue;
				BehaviorManager bm = bo.getManager().newInstance();
				BehaviorManagerGUI bmg = null;
				if (bo.getGui() != null)
					bmg = bo.getGui().newInstance();
				hmsbi.put(bo.getId(), new BehaviorInstance(bm, bmg, bo.getId()));
			} catch (Throwable e) {
				e.printStackTrace();
				System.out.println("Error ");
			}
		}
		return hmsbi;
	}
}
