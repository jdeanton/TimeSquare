/*******************************************************************************
 * Copyright (c) 2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *
 * @author Frederic Mallet
 *******************************************************************************/
package fr.kairos.timesquare.xccsltolc;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;

/**
 * Abstract handler to simplify extension or type filtering of ClockConstraintSystem
 * see filter in plugins.xml
 * 
 * @author fmallet
 *
 */
abstract class AHandler extends AbstractHandler implements IHandler {
	private EObject getModelForResource(IFile file) {
		URI uri = URI.createURI("platform:/resource/"
				+ file.getProject().getName() + "/"
				+ file.getProjectRelativePath());
		ResourceSet set = new ResourceSetImpl();//resourceSetProvider.get(file.getProject());
		try {
			Resource res = set.getResource(uri, true);
			if (res != null) {
				return res.getContents().get(0);
			}
		} catch (Exception e) {
			ConsolePlugin.errorDialog(null, "Extracting Model from resource", 
					"AHandler.getModelForResource " + file.getName(), e);
			ConsolePlugin.log(e);
		}
		return null;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getSelection();
		if (!(selection instanceof StructuredSelection)) return null;

		for (Object o : ((StructuredSelection)selection).toList()) {
			if (o instanceof IFile) {
				treatCCS((ClockConstraintSystem)getModelForResource((IFile)o));
			} else if (o instanceof ClockConstraintSystem) {
				treatCCS((ClockConstraintSystem)o);
			} else {
				ConsolePlugin.log(new Status(Status.WARNING, Activator.PLUGIN_ID, 
						"Does not know what to do " + getClass().toString()));
			}
		}
		return null;
	}

	protected abstract void treatCCS(ClockConstraintSystem s);
}
