/*******************************************************************************
 * Copyright (c) 2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *
 * @author Frederic Mallet
 *******************************************************************************/
package fr.kairos.timesquare.xccsltolc;

import org.eclipse.emf.ecore.EObject;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.Clock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.DiscreteClockType;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.Element;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.Block;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Binding;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Expression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ExpressionDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.RelationDeclaration;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.impl.WaitImpl;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.impl.ExpressionImpl;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.util.CCSLModelSwitch;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.util.TimeModelSwitch;
import fr.kairos.timesquare.ccsl.ISpecification;
import fr.kairos.timesquare.ccsl.adapters.GenericToSimple;
import fr.kairos.timesquare.ccsl.simple.Definition;

/**
 * Switch to transform ClockConstraintSystem (EMF) into ISpecification (POJO)
 * 
 * @author fmallet
 *
 */
public class XCCSLtoLC extends CCSLModelSwitch<Object> {
	private InternalTimeModel internalTM = new InternalTimeModel();
	private InternalKernelExpression internalKernelE = new InternalKernelExpression();
	private InternalKerneRelation internalKernelR = new InternalKerneRelation();
	private ISpecification spec;
	
	public XCCSLtoLC(ISpecification spec) {
		this.spec = spec;
	}

	@Override
	public Boolean caseBlock(Block object) {
		for (Element e : object.getElements()) {
			internalTM.doSwitch(e);
		}
		for (Relation r : object.getRelations()) {
			caseRelation(r);
		}
		for (Expression e : object.getExpressions()) {
			caseExpression(e);
		}
//		System.out.println("Classical expressions");	
//		for (ClassicalExpression ce : object.getClassicalExpression()) {
//			System.out.println(ce);
//		}
		return Boolean.TRUE;
	}

	@Override
	public Object caseClockConstraintSystem(ClockConstraintSystem object) {
		return doSwitch(object.getSuperBlock());
	}
	
	@Override
	public Boolean defaultCase(EObject object) {
		System.out.println("Ignoring:" + object);
		return Boolean.TRUE;
	}

	private Expression currentExpression;
	private Boolean caseExpression(Expression expression) {
		ExpressionDeclaration decl = expression.getType();
		currentExpression = expression;
		Definition d = internalKernelE.doSwitch(decl);
		if (d != null) {
			spec.add(d);
		}
		return Boolean.TRUE;
	}

	private Relation currentRelation;
	private Boolean caseRelation(Relation relation) {
		RelationDeclaration decl = relation.getType();
		currentRelation = relation;
		fr.kairos.timesquare.ccsl.simple.Relation r = internalKernelR.doSwitch(decl);
		if (r != null) {
			spec.add(r);
		}
		return Boolean.TRUE;
	}
	
	class InternalTimeModel extends TimeModelSwitch<Boolean> {
		@Override
		public Boolean caseClock(Clock object) {
			spec.addClock(object.getName());
			return Boolean.TRUE;
		}
		
		@Override
		public Boolean defaultCase(EObject object) {
			System.out.println("Ignoring:" + object);
			return Boolean.TRUE;
		}
	}
	class InternalKerneRelation extends KernelRelationSwitch<fr.kairos.timesquare.ccsl.simple.Relation> {
		@Override
		public fr.kairos.timesquare.ccsl.simple.Relation caseCoincidence(Coincidence object) {
			// TODO Auto-generated method stub
			return super.caseCoincidence(object);
		}
		private String getLeft() {
			return currentRelation.getBindings().get(0).getBindable().getName();
		}
		private String getRight() {
			return currentRelation.getBindings().get(1).getBindable().getName();
		}
		@Override
		public fr.kairos.timesquare.ccsl.simple.Relation caseNonStrictPrecedence(NonStrictPrecedence object) {
			return new fr.kairos.timesquare.ccsl.simple.Relation(GenericToSimple.CAUSALITY, getLeft(), getRight());
		}
		@Override
		public fr.kairos.timesquare.ccsl.simple.Relation casePrecedence(Precedence object) {
			return new fr.kairos.timesquare.ccsl.simple.Relation(GenericToSimple.PRECEDENCE, getLeft(), getRight());
		}
		@Override
		public fr.kairos.timesquare.ccsl.simple.Relation caseSubClock(SubClock object) {
			return new fr.kairos.timesquare.ccsl.simple.Relation(GenericToSimple.SUBCLOCK, getLeft(), getRight());
		}
		@Override
		public fr.kairos.timesquare.ccsl.simple.Relation caseExclusion(Exclusion object) {
			return new fr.kairos.timesquare.ccsl.simple.Relation(GenericToSimple.EXCLUSION, getLeft(), getRight());
		}
		@Override
		public fr.kairos.timesquare.ccsl.simple.Relation caseRelationDeclaration(RelationDeclaration object) {
			if (object.getName().equals("Alternates")) {
				return new fr.kairos.timesquare.ccsl.simple.Relation(GenericToSimple.ALTERNATION, getLeft(), getRight());
			}
			return null;
		}
		@Override
		public fr.kairos.timesquare.ccsl.simple.Relation defaultCase(EObject object) {
			if (object instanceof RelationDeclaration) {
				fr.kairos.timesquare.ccsl.simple.Relation r = caseRelationDeclaration((RelationDeclaration)object);
				if (r != null) return r;
			}
			System.out.println("Ignoring relation " + object);
			return null;
		}
	}
	class InternalKernelExpression extends KernelExpressionSwitch<Definition> {
		private BindableEntity get(Expression e, String bindingName) {
			for (Binding b : e.getBindings()) {
				if (b.getAbstract().getName().equals(bindingName)) {
					return b.getBindable();
				}
			}
			return null;
		}
		private BindableEntity get(String bindingName) {
			return get(currentExpression, bindingName);
		}
		private String[] extractClocks() {
			String[] clocks = new String[currentExpression.getBindings().size()];
			int i = 0;
			for (Binding b : currentExpression.getBindings()) {
				if (b.getAbstract().getType() instanceof DiscreteClockType) {
					clocks[i] = b.getBindable().getName();
					i++;
				} else
					System.out.println(b.getAbstract().getType());
			}
			return clocks;
			
		}
		@Override
		public Definition caseInf(Inf object) {
			return new Definition(currentExpression.getName(), "Inf", extractClocks());
		}

		@Override
		public Definition caseIntersection(Intersection object) {
			return new Definition(currentExpression.getName(), "Intersection", extractClocks());
		}

		@Override
		public Definition caseSup(Sup object) {
			return new Definition(currentExpression.getName(), "Sup", extractClocks());
		}

		@Override
		public Definition caseUnion(Union object) {
			return new Definition(currentExpression.getName(), "Union", extractClocks());
		}

		@Override
		public Definition caseExpressionDeclaration(ExpressionDeclaration object) {
			if (object.getName().equals("Minus")) {
				return new Definition(currentExpression.getName(), "Minus", extractClocks());
			}
			if (object.getName().equals("SampledOn")) {
				return new Definition(currentExpression.getName(), "DelayFor", get("SampledOnSampledClock").getName(), 
						get("SampledOnTrigger").getName());
			}
			if (object.getName().equals("DelayFor")) {
				String clockToDelay = get("DelayForClockToDelay").getName();
				String clockForCounting = get("DelayForClockForCounting").getName();
				int value = ((IntegerElement)get("DelayForDelay")).getValue();
				if (clockToDelay.equals(clockForCounting)) {
					return new Definition(currentExpression.getName(), "DelayFor", clockToDelay).set("from", value);
				} else {
					return new Definition(currentExpression.getName(), "DelayFor", clockToDelay, clockForCounting).set("from", value);
					
				}
			}
			if (object.getName().equals("Periodic")) {
				int period = ((IntegerElement)get("PeriodicPeriod")).getValue();
				int from = ((IntegerElement)get("PeriodicOffset")).getValue();	
				return new Definition(currentExpression.getName(), "Periodic", get("PeriodicBaseClock").getName()).set("period", period).set("from", from); 
			}
			return null;
		}
		
		@Override
		public Definition caseUpTo(UpTo object) {
			String toFollow = get("ClockToFollow").getName();
			BindableEntity entity = get("KillerCLock");
			if (entity instanceof ExpressionImpl) {
				ExpressionImpl e = (ExpressionImpl)entity;
				ExpressionDeclaration decl = e.getType();
				if (decl instanceof WaitImpl) {
					int upto = ((IntegerElement)get(e, "WaitingValue")).getValue();
					return new Definition(currentExpression.getName(), "Periodic", toFollow).set("upto", upto);
				}
			}
			return super.caseUpTo(object);
		}

		@Override
		public Definition caseWait(Wait object) {
			int value = ((IntegerElement)get("WaitingValue")).getValue();
			return new Definition(currentExpression.getName(), "Periodic", get("WaitingClock").getName()).set("from", value).set("upto", value);
		}
		@Override
		public Definition defaultCase(EObject object) {
			if (object instanceof ExpressionDeclaration) {
				Definition d = caseExpressionDeclaration((ExpressionDeclaration)object);
				if (d != null) return d;
			}
			System.err.println("Ignoring " + object);
			return null;
		}
	}
}
