/*******************************************************************************
 * Copyright (c) 2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *
 * @author Frederic Mallet
 *******************************************************************************/
package fr.kairos.timesquare.xccsltolc;

import java.util.Set;

import javax.swing.JOptionPane;

import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.console.ConsolePlugin;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.kairos.timesquare.ccsl.safety.SafetyAnalyser;
import fr.kairos.timesquare.ccsl.simple.Specification;

/**
 * Check the safety of ClockConstraintSystem by transformation into LC
 * 
 * @author fmallet
 *
 */
public class CheckSafetyHandler extends AHandler implements IHandler {
	protected void treatCCS(ClockConstraintSystem s) {
		Specification spec = new Specification(s.getName());
		XCCSLtoLC convert = new XCCSLtoLC(spec);	
		convert.doSwitch(s);
		SafetyAnalyser analyzer = new SafetyAnalyser(spec);

		if (!analyzer.hasCounters()) {
			String message = "The specification has no counter => SAFE!";
			JOptionPane.showMessageDialog(null, "CCS " + s.getName(), message, JOptionPane.PLAIN_MESSAGE);
			ConsolePlugin.getDefault().getLog().log(new Status(Status.OK, 
					Activator.PLUGIN_ID, message));
			return;
		}
		if (analyzer.checkSafety()) {
			String message = "The specification is SAFE!";
			JOptionPane.showMessageDialog(null, "CCS " + s.getName(), message, JOptionPane.PLAIN_MESSAGE);
			ConsolePlugin.getDefault().getLog().log(new Status(Status.OK, 
					Activator.PLUGIN_ID, message));
		} else {
			Set<String> unsafeCounters = analyzer.findUnsafeCounters();
			
			StringBuilder sb = new StringBuilder("List of unsafe counters:\n");
			for(String counter : unsafeCounters) {
				sb.append(counter).append('\n');
			}

			JOptionPane.showMessageDialog(null, "CCS " + s.getName(), sb.toString(), JOptionPane.PLAIN_MESSAGE);			
			ConsolePlugin.getDefault().getLog().log(new Status(Status.INFO, 
					Activator.PLUGIN_ID, sb.toString()));
		}
	}
}
