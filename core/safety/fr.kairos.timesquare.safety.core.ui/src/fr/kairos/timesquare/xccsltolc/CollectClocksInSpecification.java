package fr.kairos.timesquare.xccsltolc;

import java.util.LinkedHashSet;

import fr.kairos.timesquare.ccsl.IDefinition;
import fr.kairos.timesquare.ccsl.IRelation;
import fr.kairos.timesquare.ccsl.ISpecification;

/**
 * Collect the clocks in a ISpecification
 * 
 * @author fmallet
 *
 */
public class CollectClocksInSpecification implements ISpecification {
	private LinkedHashSet<String> clocks = new LinkedHashSet<>();
	public Iterable<String> getClocks() {
		return clocks;
	}
	@Override
	public void addClock(String name) {
		clocks.add(name);
	}

	@Override
	public void add(IRelation relation) {
	}

	@Override
	public void add(IDefinition definition) {
	}

	@Override
	public boolean isConstraintSupported(String name) {
		return true;
	}
}
