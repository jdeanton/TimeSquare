/*******************************************************************************
 * Copyright (c) 2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *
 * @author Frederic Mallet
 *******************************************************************************/
package fr.kairos.timesquare.xccsltolc;

import java.io.File;
import java.io.FileWriter;

import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockConstraintSystem;
import fr.kairos.timesquare.ccsl.adapters.GenericToSimple;
import fr.kairos.timesquare.ccsl.simple.PrettyPrint;

/**
 * Handler to transform .extendedCCSL into .lc
 * 
 * @author fmallet
 */
public class XCCSLtoLCHandler extends AHandler implements IHandler {
	protected void treatCCS(ClockConstraintSystem s) {
		URI srcURI = s.eResource().getURI();
		URI tgtURI = srcURI.trimFileExtension().appendFileExtension("lc");

		try {
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			IProject project = root.getProject(srcURI.segment(1));
			IFile i = root.getFile(new Path(tgtURI.toPlatformString(true)));
//			IFile i = root.getFile(new Path(tgtURI.toPlatformString(true)));
			File f = i.getRawLocation().toFile();
			FileWriter fw = new FileWriter(f);
			
			// collect clocks beforehand to be sure they are before the specification
			CollectClocksInSpecification collect = new CollectClocksInSpecification();
			XCCSLtoLC firstPass = new XCCSLtoLC(collect);
			firstPass.doSwitch(s);
			
			PrettyPrint pp = new PrettyPrint(fw, s.getName(), collect.getClocks());
			XCCSLtoLC convert = new XCCSLtoLC(new GenericToSimple(pp));
			convert.doSwitch(s);
			pp.finish();
			
			fw.close();
			project.refreshLocal(IFolder.DEPTH_INFINITE, null);
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
	}

}
