/*******************************************************************************
 * Copyright (c) 2017-2021 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.common;

import java.io.File;
import java.util.HashMap;

/**
 * Utility Class to deal with parameters (not abstract)
 * 
 * @author fmallet
 *
 */
public class ParameterUtility {
	static final public String FOLDER = "folder";
	static final public String FOLDER_NAME = "folderName";
	
	private HashMap<String,Object> params = null;
	
	public void setParam(String name, Object value) {
		if (params == null) params = new HashMap<>();
		params.put(name, value);
	}

	/** Use the object folder (a File) as a first attempt, or the folderName as a second attempt
	 * @param defaut to be used as the name of the folder if parameters are not set.
	 * @return the file that was created
	 */
	public File createFolder(String defaut) {
		Object folder = getParam(FOLDER);
		File f;
		if (folder == null) {
			String folderName = getStringParam(FOLDER_NAME, defaut);
			f = new File(folderName);
		} else 
			f = (File)folder;
		
		if (!f.exists())
			f.mkdir();
		return f;
	}
	public boolean getBooleanParam(String name, boolean defaut) {
		if (params == null || !params.containsKey(name))
			return defaut;
		return (Boolean)params.get(name);
	}
	public int getIntParam(String name, int defaut) {
		if (params == null || !params.containsKey(name))
			return defaut;
		return (Integer)params.get(name);
	}
	public String getStringParam(String name, String defaut) {
		if (params == null || !params.containsKey(name))
			return defaut;
		return params.get(name).toString();
	}
	public Object getParam(String name) {
		if (params == null) return null;
		return params.get(name);
	}
}
