/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.common;

/**
 * Often needs to handle pairs of objects 
 * 
 * @author fmallet
 *
 * @param <T1,T2>
 */
public class Pair<T1,T2> {
	private T1 e1;
	private T2 e2;

	public Pair(T1 e1, T2 e2) {
		this.e1 = e1;
		this.e2 = e2;
	}
	public T1 getLeft() {
		return e1;
	}
	public T2 getRight() {
		return e2;
	}
	public String getName() {
		return e1.toString() + "_" + e2.toString();
	}
	@Override
	public int hashCode() {
		return e1.hashCode() + e2.hashCode();
	}
	public boolean equals(Pair<?,?> other) {
		return (e1.equals(other.e1) && e2.equals(other.e2)) ||
				(e1.equals(other.e2) && e2.equals(other.e1));
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Pair) {
			return equals((Pair<?,?>)obj);
		}
		return super.equals(obj);
	}
	@Override
	public String toString() {
		return "<" + e1 + ", " + e2 + ">";
	}
}
