package fr.kairos.common;

public interface IFactory<T> {
	public T build();
}
