/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.common.graph;

import java.util.Stack;

/**
 * Computes the StronglyConnectedComponent of a Directed Graph
 * using Tarjan's Algorithm
 * 
 * @author fmallet
 *
 */
public class StronglyConnectedComponents {
    private boolean[] marked;        // marked[v] = has v been visited?
    private int[] id;                // id[v] = id of strong component containing v
    private int[] low;				 // lowest id of the node in the strong component
    private int count;               // number of strongly-connected components
    private Stack<Integer> stack;

    private int pre = 0; // preorder
    
    private MyGraph graph;
    public StronglyConnectedComponents(MyGraph graph) {
        marked = new boolean[graph.getNodeNumber()];
        stack = new Stack<Integer>();
        id = new int[graph.getNodeNumber()]; 
        low = new int[graph.getNodeNumber()]; 

        this.graph = graph;
        
        for (int node = 0; node < graph.getNodeNumber(); node++) {
           	dfs(node);
        }
    }

    private void dfs(int current) { 
    	if (marked[current]) // already marked
    		return; 
    	
        marked[current] = true;
        int min = pre;
        low[current] = pre++;
        stack.push(current);
        for (int n : graph.neighbours(current)) {
            dfs(n);
            if (low[n] < min) 
            	min = low[n];
        }
        if (min < low[current]) { low[current] = min; return; }
        int w;
        do {
            w = stack.pop();
            id[w] = count;
            low[w] = graph.getNodeNumber();
        } while (w != current);
        count++;
    }


    /**
     * Returns the number of strong components.
     * @return the number of strong components
     */
    public int count() {
        return count;
    }

    /**
     * Are vertices <tt>v</tt> and <tt>w</tt> in the same strong component?
     */
    public boolean stronglyConnected(int v, int w) {
        return id[v] == id[w];
    }
}
