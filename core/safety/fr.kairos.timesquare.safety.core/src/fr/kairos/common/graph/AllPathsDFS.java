/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.common.graph;

/**
 * Computes all path from a node to other nodes in MyGraph using Depth-First Search
 *  
 *  This is an adaptation of SedgeWick's Algorithm to take names into account
 *  
 *  @author fmallet
 */
public class AllPathsDFS {
    private boolean[] marked;  // marked[v] = true if v is reachable from source
    private int[] edgeTo;      // edgeTo[v] = last edge on path from source to v

    private MyGraph graph;  // remove parameter in recursive call 
    /**
     */
    public AllPathsDFS(MyGraph graph, String source) {
        marked = new boolean[graph.getNodeNumber()];
        edgeTo = new int[graph.getNodeNumber()];
        this.graph = graph;
        dfs(graph.nameToIndex(source));
    }

    private void dfs(int current) { 
        marked[current] = true;
        for (int neighbour : graph.neighbours(current)) {
            if (!marked[neighbour]) {
                edgeTo[neighbour] = current;
                dfs(neighbour);
            }
        }
    }

    /**
     * Is there a directed path from <tt>source</tt> to <tt>node</tt>?
     */
    public boolean hasPathTo(String node) {
        return marked[graph.nameToIndex(node)];
    }
}
