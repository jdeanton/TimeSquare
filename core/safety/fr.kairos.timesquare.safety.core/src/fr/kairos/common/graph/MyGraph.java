/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.common.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import fr.kairos.timesquare.ccsl.graph.IGraph;

/**
 * A directed graph with String as node names.
 * @author fmallet
 */
public class MyGraph implements IGraph {
	private Set<Integer>[] edges;
	private String[] nodeNames;
	private HashMap<String,Integer> namesToIndex = new HashMap<>();
	private int nodeNumber = 0;
	
	@SuppressWarnings("unchecked")
	public MyGraph(int countNodes) {
        if (countNodes < 0) { 
        	throw new IllegalArgumentException("Le nombre de noeud doit être positif");
        }
        edges = (Set<Integer>[])new Set[countNodes];
        nodeNames = new String[countNodes];
	}

	@Override
	public void addNode(String name) {
		if (namesToIndex.containsKey(name)) {
			System.err.println("Already has a node with that name ");
			return;
		}
		node(name);
	}

	private int node(String name) {
		if (namesToIndex.containsKey(name)) {
			return namesToIndex.get(name);
		}
		if (nodeNumber == nodeNames.length) {
			throw new RuntimeException("cannot have more than " + nodeNames.length + " nodes");
		}		nodeNames[nodeNumber] = name;
		namesToIndex.put(name, nodeNumber);
        edges[nodeNumber] = new HashSet<Integer>();
		nodeNumber++;
		return nodeNumber - 1; 
	}
	
	@Override
	public void addEdge(String tail, String head, Object... param) {
		int tailNum = node(tail);
		int headNum = node(head);
		
		if (edges[tailNum].add(headNum)) {
//			edgeNumber++;
		}
	}
	
	public Iterable<Integer> neighbours(int node) {
		return edges[node];
	}
	public int getNodeNumber() {
		return nodeNumber;
	}
	
	public int nameToIndex(String name) {
		return namesToIndex.get(name);
	}
}
