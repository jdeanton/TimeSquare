/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * provides a naive way to store a directed graph, independently of any external library
 * 
 * @see IGraph
 * 
 * @author fmallet
 */
final public class DigraphBuilder {
	private Set<String> nodes = new HashSet<>();
	private Set<Arc> arcs = new HashSet<>();

	private String param = null;
	
	void setParam(String param) { this.param = param; }	

	public int getNodeNumber() {
		return nodes.size();
	}
	public int getEdgeNumber() {
		return arcs.size();
	}
	public Iterable<Arc> edges() {
		return arcs;
	}
	
	public static class Arc {
		private String head, tail;
		private String param;

		public String head() {
			return head;
		}
		public String tail() {
			return tail;
		}
		private Arc(String tail, String head, String param) {
			super();
			this.head = head;
			this.tail = tail;
			this.param = param;
		}
		
		protected void build(IGraph graph) {
			graph.addEdge(tail, head, param);
		} 	
		
		@Override
		public int hashCode() {
			return head.hashCode() + tail.hashCode() + param.hashCode();
		}
		public boolean equals(Arc arc) {
			return param == arc.param && head == arc.head && tail == arc.tail; 
		}
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Arc)
				return equals((Arc)obj);
			return super.equals(obj);
		}
		
		@Override
		public String toString() {
			return tail + " -> " + head + " : " + param;
		}
	}
	
	final protected void addArc(String tail, String head) {
		nodes.add(tail);
		nodes.add(head);
		arcs.add(new Arc(tail, head, param));
	}
	
	final public void buildGraph(IGraph graph) {
		for (Arc a : arcs) {
			graph.addNode(a.head);
			graph.addNode(a.tail);
		}
		for (Arc a : arcs) {
			a.build(graph);
		}
	}	
}
