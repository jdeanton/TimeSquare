/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

/**
 * Interface to hide some specific transformations from a SimpleSpecification to something else.
 * 
 * @author fmallet
 *
 */
public interface IUtility {
	void treat(String name, ISpecificationBuilder builder);
	void setParam(String name, Object value);
}
