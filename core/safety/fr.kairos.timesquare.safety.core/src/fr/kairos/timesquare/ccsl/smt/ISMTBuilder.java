/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.smt;

/**
 * Interface to hide an SMT Solver (like Z3)
 * 
 * @author fmallet
 *
 * @param <T> type used to store clocks (e.g String)
 * @param <B> type used to represent SMT boolean expressions
 * @param <F> type used to represent SMT functions
 */
public interface ISMTBuilder<T,B,F> {
	F tick(String clock);
	F counter(String clock);
	void defineClock(String clock, B body, String comment);

	void assert_FA(B e, String c);
	void assert_Ex(B e, String c);
	
	B existsNinInt(B e);
	B forAllNinInt(B e);

	B[] applyToNall(String ...clocks);

	T apply(F f, T e);
	B clockN(String clock);
	T applyToN(F f);
	B not(B e);
	T ite(B i, T t, T e);
	B implies(B cause, B cons);
	B eq(T left, T right);
	B gt(T left, T right);
	B ge(T left, T right);
	B or(Object ...e);
	B and(Object ...e);
	
	T mod(T left, T right);
	T constant(int v);

	void check(Object...params);

	T assertE(T existsNinInt);
	boolean addClock(String name);
	F addCounterFor(String ref);
	F addDiffCounter(String left, String right);
}
