/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.io.PrintWriter;
import java.io.Writer;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Display a simple specification with the syntax of .lc files
 * @author fmallet
 *
 */
public class PrettyPrint implements ISimpleSpecification {	
	protected PrintWriter pw;
	private boolean isFirstConstraint = true;
	public PrettyPrint(Writer wr, String name, Iterable<String> clocks) {
		pw = new PrintWriter(wr);
		pw.println("Specification " + name + " {");
		pw.print("\tClock");
		for (String clock : clocks) {
			pw.print(" " + clock);			
		}
	}
	@Override
	public void addClock(String name) {
	}

	public void finish() {
		pw.println();
		pw.println("\t]");
		pw.println("}");		
	}
	private void startConstraints() {
		pw.println("\n\t[");
	}
	protected void checkFirst() {
		if (isFirstConstraint)
			startConstraints();
		else 
			pw.println();
		isFirstConstraint = false;
		pw.append("\t\t");
	}
	@Override
	public void subclock(String left, String right) {
		checkFirst();
		pw.append("SubClocking ").append(left).append(" <- ").append(right);
	}
	@Override
	public void exclusion(String left, String right) {
		checkFirst();
		pw.append("Exclusion ").append(left).append(" # ").append(right);
	}
	@Override
	public void precedence(String left, String right) {
		checkFirst();
		pw.append("Precedence ");
		pw.append(left).append(" < ").append(right);
	}
	@Override
	public void precedence(String left, String right, int min, int max) {
		checkFirst();
		pw.append("Precedence ").append(left).append(" < ");

		boolean par = (max!=-1 || min > 0);
		
		if (par) pw.append('(');
		
		if (min > 0)
			pw.append("init:").print(min + " ");
		
		if (max != -1)
			pw.append("max:").print(max);
		
		if (par) pw.append(") ");
		
		pw.append(right);
	}
	@Override
	public void causality(String left, String right) {
		checkFirst();
		pw.append("Precedence ");
		pw.append(left).append(" <= ").append(right);
	}
	@Override
	public void causality(String left, String right, int min, int max) {
		checkFirst();
		pw.append("Precedence ").append(left).append(" <= ");
	
		boolean par = (max!=-1 || min > 0);
		
		if (par) pw.append('(');
		
		if (min > 0)
			pw.append("init:").print(min + " ");
		
		if (max != -1)
			pw.append("max:").print(max);
		
		if (par) pw.append(") ");
	
		pw.append(right);
	}
	private void prefix(String defClock, String op, String... clocks) {
		checkFirst();
		pw.append("Let ");
		pw.append(defClock).append(" be ").append(op).append('(');
		pw.append(String.join(", ", clocks));
		pw.append(")");
	}
	@Override
	public void inf(String defClock, String... clocks) {
		prefix(defClock, "inf", clocks);
	}
	@Override
	public void sup(String defClock, String... clocks) {
		prefix(defClock, "sup", clocks);
	}
	private void infix(String defClock, String op, String... clocks) {
		checkFirst();
		pw.append("Let ");
		pw.append(defClock).append(" be ");
		pw.append(String.join(op, clocks));
	}
	@Override
	public void union(String defClock, String... clocks) {
		infix(defClock, " + ", clocks);
	}
	@Override
	public void intersection(String defClock, String... clocks) {
		infix(defClock, " * ", clocks);
	}
	@Override
	public void minus(String defClock, String... clocks) {
		infix(defClock, " - ", clocks);
	}
	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		if (period == 1) {
			delayFor(defClock, ref, from, upto, null);
			return;
		}
		checkFirst();
		pw.append("repeat ");
		pw.append(defClock).append(" every ").print(period);
		pw.append(" ").print(ref);
		if (from != 0)
			pw.append(" from ").print(from);
		if (upto != -1)
			pw.append(" upTo ").print(upto);
	}
	@Override
	public void delayFor(String defClock, String ref, int from, int upto, String base) {
		checkFirst();
		pw.append(defClock).append(" = ").append(ref);
		if (base != null && from == 1) 
			pw.append(" sampledOn ").print(base);
		else if (upto == -1 ){
			pw.append(" $ ").print(from);
			if (base != null)
				pw.append(" on ").print(base);
		} else {
			pw.append("[").print(from);
			pw.append(", ").print(upto);
			pw.append("]");
		}
	}
}
