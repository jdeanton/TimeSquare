/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.io.Writer;

/**
 * Display a simple specification with the syntax of .lc files
 * @author fmallet
 *
 */
public class PrettyPrintV2 extends PrettyPrint {	
	public PrettyPrintV2(Writer wr, String name, Iterable<String> clocks) {
		super(wr, name, clocks);
	}
	@Override
	public void delayFor(String defClock, String ref, int from, int upto, String base) {
		checkFirst();
		pw.append("Let ").append(defClock).append(" be ").append(ref);
		if (base != null && from == 1) 
			pw.append(" sampledOn ").print(base);
		else if (upto == -1 ){
			pw.append(" $ ").print(from);
			if (base != null)
				pw.append(" on ").print(base);
		} else {
			pw.append("[").print(from);
			pw.append(", ").print(upto);
			pw.append("]");
		}
	}
}
