package fr.kairos.timesquare.ccsl.sat;

import java.util.Iterator;
import java.util.LinkedList;

public class ITEClause implements IClause {
	private int name, cond, thenVar, elseVar;
	
	public ITEClause(int name, int cond, int thenVar, int elseVar) {
		super();
		this.name = name;
		this.cond = cond;
		this.thenVar = thenVar;
		this.elseVar = elseVar;
	}

	@Override
	public Iterator<Clause> iterator() {
		LinkedList<Clause> res = new LinkedList<>();
		res.add(Clause.dualHornClause(elseVar, name, cond));
		res.add(Clause.dualHornClause(name, cond, elseVar));
		res.add(Clause.hornClause(name, cond, thenVar));
		res.add(Clause.hornClause(thenVar, name, cond));
		return res.iterator();
	}

	@Override
	public void accept(IClauseVisitor visitor) {
		visitor.visit(this);
	}
	public int getName() {
		return name;
	}
	public int getCond() {
		return cond;
	}
	public int getThenVar() {
		return thenVar;
	}
	public int getElseVar() {
		return elseVar;
	}
}
