package fr.kairos.timesquare.ccsl.reduce;

public interface IFilter<T> {
	T filter(T t);
}
