/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl;

/**
 * Extension with recent operators
 * 
 * @author fmallet
 */
public interface ISimpleSpecification2 extends ISimpleSpecification {
	public void nor(String defClock, String ...clocks);
	public void nand(String defClock, String ...clocks);
}
