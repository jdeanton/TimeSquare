/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.sat;

import java.util.ArrayList;
import java.util.List;

import fr.kairos.lightccsl.core.stepper.INameToIntegerMapper;

/**
 * Transform a IBooleanSpecification into a set of clauses
 * 
 * @author fmallet
 *
 */
public class BooleanSatisfactionProblem implements IBooleanSpecification {
	private List<IClause> clauses = new ArrayList<>();
	private INameToIntegerMapper nameMapper;

	public Iterable<IClause> clauses() { return clauses; }
	public int nbClauses() { return clauses.size(); }
	public int nbVars() { return this.nameMapper.size(); }

	@Override
	public void setNameMapper(INameToIntegerMapper nameMapper) {
		this.nameMapper = nameMapper;
	}

	@Override
	public int id(String name) {
		return nameMapper.getIdFromName(name);
	}

	@Override
	public void goalClause(String... vars) {
		clauses.add(Clause.goalClause(ids(vars)));
	}

	@Override
	public void dualHornClause(String var0, String ...vars) {
		clauses.add(Clause.dualHornClause(id(var0), ids(vars)));
	}	

	@Override
	public void hornClause(String var0, String... vars) {
		clauses.add(Clause.hornClause(id(var0), ids(vars)));
	}
	private int[] ids(String...vars) {
		int[] c = new int[vars.length];
		for (int i = 0; i < vars.length; i++) {
			c[i] = id(vars[i]);
		}
		return c;
	}
	@Override
	public void or(String name, String... vars) {
		clauses.add(new OrClause(id(name), ids(vars)));
	}
	@Override
	public void and(String name, String... vars) {
		clauses.add(new AndClause(id(name), ids(vars)));
	}
	@Override
	public void xor(String name, String... vars) {
		clauses.add(new XorClause(id(name), ids(vars)));
	}
	@Override
	public void ite(String name, String condVar, String thenVar, String elseVar) {
		clauses.add(new ITEClause(id(name), id(condVar), id(thenVar), id(elseVar)));		
	}
	
}
