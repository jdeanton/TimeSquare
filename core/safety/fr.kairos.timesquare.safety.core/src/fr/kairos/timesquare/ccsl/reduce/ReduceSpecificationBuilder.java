package fr.kairos.timesquare.ccsl.reduce;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;
import fr.kairos.timesquare.ccsl.simple.ISpecificationBuilder;

/**
 * Given a list of aliases, would produce the same spec with replacing all aliases with actual names
 * @author fmallet
 *
 */
public class ReduceSpecificationBuilder implements ISpecificationBuilder {
	private ISpecificationBuilder delegateBuilder;
	
	public ReduceSpecificationBuilder(ISpecificationBuilder delegateBuilder) {
		super();
		this.delegateBuilder = delegateBuilder;
	}
	
	private AliasRepository buildAlias() {
		AliasRepository repo = new AliasRepository();
		AliasBuilder aliasBuilder = new AliasBuilder(repo);
		delegateBuilder.build(aliasBuilder);
		repo.reduce();
		return repo;
	}
	
	private AliasRepository repo = null;
	@Override
	public void build(ISimpleSpecification spec) {	
		if (repo == null)
			repo = buildAlias();
		FilterSpecification filter = new FilterSpecification(spec, repo.filterClock(), repo.filterClocks());
		delegateBuilder.build(filter);
	}
}
