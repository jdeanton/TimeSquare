/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.util.ArrayList;

import fr.kairos.timesquare.ccsl.IDefinition;
import fr.kairos.timesquare.ccsl.IRelation;
import fr.kairos.timesquare.ccsl.ISpecification;

/**
 * Simple implementation of a generic CCSL specification
 * Most of the time we try to avoid having to store explicitly the constraints
 * 
 * @author fmallet
 *
 */
public class Specification implements ISpecification {
	private String name;
	
	public Specification(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	private ArrayList<String> clocks = new ArrayList<>();
	
	@Override
	public void addClock(String name) {
		clocks.add(name);
	}

	public int getClockNumber() {
		return clocks.size();
	}
	private ArrayList<IDefinition> definitions = new ArrayList<>();
	@Override
	public void add(IDefinition definition) {
		definitions.add(definition);
	}
	private ArrayList<IRelation> relations = new ArrayList<>();
	@Override
	public void add(IRelation relation) {
		relations.add(relation);
	}

	@Override
	public boolean isConstraintSupported(String name) {
		return true;
	}
	
	public void visit(ISpecification spec) {
		for(String clock : clocks) {
			spec.addClock(clock);
		}
		for (IDefinition def : definitions) {
			spec.add(def);
		}
		for (IRelation rel : relations) {
			spec.add(rel);
		}
	}
}
