/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.extended;

import fr.kairos.timesquare.ccsl.IConstraint;

/**
 * One possible naive implementation of simple expressions
 * Most of the time we try to avoid building explicit specifications to reduce memory footprint
 * 
 * @author fmallet
 */
class StringExpression implements IConstraint {
	private String constraintName, defClk;
	private String[] body;
	
	public StringExpression(String constraintName, String defClk, String...body) {
		super();
		this.constraintName = constraintName;
		this.defClk = defClk;
		this.body = body;
	}

	@Override
	public String getConstraintName() {
		return this.constraintName;
	}
	
	@Override
	public String toString() {
		return "Expression " + defClk + " = " + constraintName + "(" + String.join(",\n\t\t\t", body) + ")";
	}

}
