/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl;

/**
 * A "classical" Light-CCSL Specification
 * 
 * @author fmallet
 */
public interface ISimpleSpecification {
	/**
	 * Add global clock
	 * @param name name of the clock
	 */
	public void addClock(String name);
	
	public void subclock(String left, String right);
	public void exclusion(String left, String right);
	public void precedence(String left, String right);	
	public void precedence(String left, String right, int min, int max);
	public void causality(String left, String right);
	public void causality(String left, String right, int min, int max);
	
	public void inf(String defClock, String ...clocks);
	public void sup(String defClock, String ...clocks);
	public void union(String defClock, String ...clocks);
	public void intersection(String defClock, String ...clocks);
	public void minus(String defClock, String ...clocks);

	public void periodic(String defClock, String ref, int period, int from, int upTo);
	public void delayFor(String defClock, String ref, int from, int upTo, String base);
}
