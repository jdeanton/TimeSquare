/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Build a counter Graph from a Simple specification
 * Build an arc every time there is an unbounded integer counter in the specification (inf, precedes ...)
 * 
 * @author fmallet
 */
final public class BuildCounterGraph implements ISimpleSpecification {
	private DigraphBuilder builder;
	public BuildCounterGraph() {
		this(new DigraphBuilder());
	}
	public BuildCounterGraph(DigraphBuilder builder) {
		this.builder = builder;
		this.builder.setParam("counter");
	}
	
	private void addArc(String tail, String head) {
		builder.addArc(tail, head);
	}
	
	public DigraphBuilder getBuilder() {
		return builder;
	}
	@Override
	public void addClock(String name) {
//		builder.node(name);
	}
	@Override
	public void subclock(String left, String right) {
		// NO COUNTER
	}

	@Override
	public void exclusion(String left, String right) {
		// NO COUNTER
	}

	@Override
	public void precedence(String left, String right) {
		if (left.equals(right)) return;
		addArc(left, right);
	}

	@Override
	public void precedence(String left, String right, int min, int max) {
		if (left.equals(right)) return;

		if (max == -1)
			addArc(left, right);
		// else BOUNDED
	}

	@Override
	public void causality(String left, String right) {
		if (left.equals(right)) return;

		addArc(left, right);
	}

	@Override
	public void causality(String left, String right, int min, int max) {
		if (left.equals(right)) return;

		if (max == -1)
			addArc(left, right);
		// else BOUNDED
	}

	@Override
	public void inf(String defClock, String... clocks) {
		for (int i = 0; i < clocks.length - 1; i ++) {
			for (int j = i + 1; j < clocks.length; j++) {
				addArc(clocks[i], clocks[j]);
			}
		}
	}

	@Override
	public void sup(String defClock, String... clocks) {
		for (int i = 0; i < clocks.length - 1; i ++) {
			for (int j = i + 1; j < clocks.length; j++) {
				addArc(clocks[i], clocks[j]);
			}
		}
	}

	@Override
	public void union(String defClock, String... clocks) {
		// NO COUNTER
	}

	@Override
	public void intersection(String defClock, String... clocks) {
		// NO COUNTER
	}

	@Override
	public void minus(String defClock, String... clocks) {
		// NO COUNTER
	}

	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		// NO COUNTER
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upTo, String base) {
		// NO COUNTER
	}
}
