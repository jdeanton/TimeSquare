/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.extended;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import fr.kairos.timesquare.ccsl.IConstraint;
import fr.kairos.timesquare.ccsl.IDefinition;
import fr.kairos.timesquare.ccsl.IRelation;
import fr.kairos.timesquare.ccsl.ISpecification;

/**
 * Builds an extendedCCSL model based on a generic Light-CCSL specification
 * The light-CCSL specification is implicit
 * 
 * @author fmallet
 */
final public class XCCSLBuilder implements ISpecification {
	private Set<String> clocks = new HashSet<String>();
	private ArrayList<IConstraint> constraints = new ArrayList<>();
	@Override
	public void addClock(String name) {
		clocks.add(name);
	}

	private static String[] SUPPORTED_relations = { 
			"Subclock",
			"Exclusion",
			"Alternation",
			"Causality",
			"Precedence"
	};
	private static String[] SUPPORTED_expressions = { 
			"Union", "Intersection", "Minus",
			"Inf", "Sup",
			"Periodic"
	};

	@Override
	public void add(IRelation relation) {
		switch(relation.getConstraintName()) {
		case "Subclock":
			constraints.add(new Relation("SubClock", 
					"LeftClock -> " + relation.getLeft(),
					"RightClock -> " + relation.getRight()));
			break;
		case "Exclusion":
			constraints.add(new Relation("Exclusion", 
					"Clock1 -> " + relation.getLeft(),
					"Clock2 -> " + relation.getRight()));
			break;
		case "Causality":
		{
			int min = relation.getIntParam("min", 0);
			int max = relation.getIntParam("max", -1);
			String left = relation.getLeft();
			
			if (min > 0) {
				String left_min = "_" + left + "_" + min;
				delay(left_min, left, min, left);
				left = left_min;
			}
			if (max != -1)
				System.err.println("Ignore max:" + min + " " + max);

			constraints.add(new Relation("Causes", 
					"LeftClock -> " + left,
					"RightClock -> " + relation.getRight()));
		}
		break;
		case "Precedence":
		{
			int min = relation.getIntParam("min", 0);
			int max = relation.getIntParam("max", -1);

			if (min == 0 && max == 1) {
				alternates(relation);
				break;
			}
			String left = relation.getLeft();
			
			if (min > 0) {
				String left_min = "_" + left + "_" + min;
				delay(left_min, left, min, left);
				left = left_min;
			}
			if (max != -1)
				System.err.println("Ignore max:" + min + " " + max);

			constraints.add(new Relation("Precedes", 
					"LeftClock -> " + relation.getLeft(),
					"RightClock -> " + relation.getRight()));
		}
		break;
		case "Alternation":
			alternates(relation);
			break;
		default:
			throw new RuntimeException("Unsupported relation " + relation);
		}
	}

	private void alternates(IRelation relation) {
		constraints.add(new Relation("Alternates", 
				"AlternatesLeftClock -> " + relation.getLeft(),
				"AlternatesRightClock -> " + relation.getRight()));
	}

	@Override
	public void add(IDefinition definition) {
		switch(definition.getConstraintName()) {
		case "Union":
		case "Intersection":
		case "Inf":
		case "Sup":
			build(definition);
			break;
		case "Minus":
			build(definition, "MinusLeftClock", "MinusRightClock");
			break;
		case "Periodic":
		{int period = definition.getIntParam("period", 1);
		int from = definition.getIntParam("from", 0);
		int upto = definition.getIntParam("upto", -1);
		String defClk = definition.getDefinedClock();
		String base = definition.getRefClocks()[0];
		if (upto != -1) {
			constraints.add(new StringExpression("Wait", base + "_w",
					"WaitingClock -> " + base,
					"WaitingValue -> " + addInt(upto + period)));
			constraints.add(new StringExpression("UpTo", base + "_u",
					"ClockToFollow -> " + base,
					"KillerCLock -> " + base + "_w"));
			base = base + "_u";
		}
		String p = addInt(period);
		String o = addInt(from);
		constraints.add(new StringExpression("Periodic", defClk,
				"PeriodicBaseClock -> " + base,
				"PeriodicPeriod -> " + p,
				"PeriodicOffset -> " + o));
		}break;
		case "DelayFor":
		{int from = definition.getIntParam("from", 0);
		int upto = definition.getIntParam("upto", -1);
		String base = definition.getRefClocks()[0];
		String b  = base;
		String defClk = definition.getDefinedClock();
		if (upto != -1) {
			constraints.add(new StringExpression("Wait", defClk + "_w",
					"WaitingClock -> " + base,
					"WaitingValue -> " + addInt(upto)));
			constraints.add(new StringExpression("UpTo", defClk + "_u",
					"ClockToFollow -> " + base,
					"KillerCLock -> " + defClk + "_w"));
			b = defClk + "_u";
		}
		if (definition.getRefClocks().length > 1) {
			base = definition.getRefClocks()[1];
		}
		delay(defClk, b, from, base);
		}break;
		default:
			throw new RuntimeException("Unsupported definition " + definition);
		}
	}

	private void delay(String defClk, String ref, int delay, String base) {
		if (base != ref && delay == 0) {
			constraints.add(new StringExpression("SampledOn", defClk,
					"SampledOnSampledClock -> " + ref,
					"SampledOnTrigger -> " + base
					));

		} else {
			constraints.add(new StringExpression("DelayFor", defClk,
					"DelayForClockToDelay -> " + ref,
					"DelayForClockForCounting -> " + base,
					"DelayForDelay -> " + addInt(delay)
					));
		}
	}
	private String addInt(int v) {
		if (v < 11) return periods[v];
		String name = "_" + v;
		intDefs.add("Integer " + name + ":int = " + v);
		return name;
	}
	private Set<String> intDefs = new HashSet<>();

	private static String[] periods = { "zero", "one", "two", "three", "four", "five", "six", "seven",
			"eight", "nine", "ten"};

	private void build(IDefinition definition, String left, String right) {
		buildOther(definition.getConstraintName(), definition.getDefinedClock(), left, right, definition.getRefClocks());
	}
	private void buildOther(String cName, String def, String left, String right, String...clocks) {
		String r = clocks[1];
		if (clocks.length > 2) {
			String newDef = def + "_0";
			buildOther(cName, newDef, left, right, Arrays.copyOfRange(clocks, 1, clocks.length));
			r = newDef;
		} 
		constraints.add(new Definition(cName, def, left + " -> " + clocks[0], right + " -> " + r));
	}
	private void build(IDefinition definition) {
		build(definition, "Clock1", "Clock2");
	}

	@Override
	public boolean isConstraintSupported(String name) {
		for (String s : SUPPORTED_relations)
			if (s.equals(name)) return true;
		for (String s : SUPPORTED_expressions)
			if (s.equals(name)) return true;
		return false;
	}

	public void print(Writer wr, String name) {
		PrintWriter pw = new PrintWriter(wr);
		pw.println("ClockConstraintSystem " + name + " {");
		pw.println("    imports {");
		pw.println("		import \"platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib\" as lib0;"); 
		pw.println("		import \"platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib\" as lib1;");
		pw.println("    }");
		pw.println("    entryBlock main");
		pw.println();
		pw.println("    Block main {");

		for(String clock : clocks) {
			pw.println("		Clock " + clock);
		}
		pw.println();
		for(String def : intDefs) {
			pw.println("		" + def);
		}
		pw.println();
		for(IConstraint cons : constraints) {
			pw.println("        " + cons);
		}
		pw.println("	}");
		pw.println("}");
		pw.close();
	}

	public void printTo(String name, File path) throws IOException {
		if (!path.exists()) {
			throw new RuntimeException("Path " + path + " does not exist. Should create before calling");
		}
		File f = new File(path, name + ".extendedCCSL");
		FileWriter fw = new FileWriter(f);
		print(fw, name);
		fw.close();
	}
}
