package fr.kairos.timesquare.ccsl.sat;

import java.util.Collections;
import java.util.Iterator;

/**
 * Represents a clause in Boolean Satifiability Problem
 * Agnostique of the solver to be used
 * @see SAT4J or Choco
 * 
 * @author fmallet
 *
 */
public class Clause implements IClause {
	private SimpleClause positive, negative;

	protected Clause(int[] positive, int[] negative) {
		this(new SimpleClause(positive), new SimpleClause(negative));
	}
	protected Clause(SimpleClause positive, SimpleClause negative) {
		this.positive = positive;
		this.negative = negative;
	}
	public static Clause hornClause(int pos, int ...neg) {
		return new Clause(new int[] { pos }, neg);
	}
	public static Clause dualHornClause(int neg, int ...pos) {
		return new Clause(pos, new int[] { neg });
	}
	public static Clause goalClause(int ...neg) {
		return new Clause(new int[0], neg);
	}
	public SimpleClause getPositive() {
		return positive;
	}
	public SimpleClause getNegative() {
		return negative;
	}
	@Override
	public Iterator<Clause> iterator() {
		return Collections.singleton(this).iterator();
	}
	@Override
	public void accept(IClauseVisitor visitor) {
		visitor.visit(this);
	}
}
