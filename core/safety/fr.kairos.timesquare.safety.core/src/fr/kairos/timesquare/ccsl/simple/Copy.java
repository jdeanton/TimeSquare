/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;
import fr.kairos.timesquare.ccsl.adapters.SimpleToGeneric;

/**
 * Reproduce a SimpleSpecification and change its name without assuming any initial or intermediate
 * storing format. The result is a Specification
 * 
 * @author fmallet
 *
 */
public class Copy implements ISimpleSpecification {
	private Specification result;
	protected ISimpleSpecification simple;

	public Copy(String newName) {
		result = new Specification(newName);
		simple = new SimpleToGeneric(result);
	}

	@Override
	public void addClock(String name) {
		simple.addClock(name);
	}

	@Override
	public void subclock(String left, String right) {
		simple.subclock(left, right);
	}

	@Override
	public void exclusion(String left, String right) {
		simple.exclusion(left, right);
	}

	@Override
	public void precedence(String left, String right) {
		simple.precedence(left, right);
	}

	@Override
	public void precedence(String left, String right, int min, int max) {
		simple.precedence(left, right, min, max);
	}

	@Override
	public void causality(String left, String right) {
		simple.causality(left, right);
	}

	@Override
	public void causality(String left, String right, int min, int max) {
		simple.causality(left, right, min, max);
	}

	@Override
	public void inf(String defClock, String... clocks) {
		simple.inf(defClock, clocks);
	}

	@Override
	public void sup(String defClock, String... clocks) {
		simple.sup(defClock, clocks);
	}

	@Override
	public void union(String defClock, String... clocks) {
		simple.union(defClock, clocks);
	}

	@Override
	public void intersection(String defClock, String... clocks) {
		simple.intersection(defClock, clocks);
	}

	@Override
	public void minus(String defClock, String... clocks) {
		simple.minus(defClock, clocks);
	}

	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		simple.periodic(defClock, ref, period, from, upto);
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upTo, String base) {
		simple.delayFor(defClock, ref, from, upTo, base);
	}

	public Specification getResult() {
		return result;
	}
}
