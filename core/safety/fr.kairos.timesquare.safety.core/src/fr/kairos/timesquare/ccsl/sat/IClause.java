package fr.kairos.timesquare.ccsl.sat;

/**
 * Unification type for all clauses
 * @author fmallet
 *
 */
public interface IClause 
	extends Iterable<Clause> { // set of clauses (may be a singleton)
	void accept(IClauseVisitor visitor);
}
