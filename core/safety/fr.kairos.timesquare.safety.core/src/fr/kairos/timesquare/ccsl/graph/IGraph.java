/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

/**
 * A generic directed graph interface called by DigraphBuilder
 * The actual serialization depends on the concrete class.
 * 
 * @see DigraphBuilder
 * @see DotGraph
 * 
 * @author fmallet
 */
public interface IGraph {
	void addNode(String name);
	void addEdge(String tail, String head, Object...param);
}
