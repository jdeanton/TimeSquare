/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.sat;

import fr.kairos.lightccsl.core.stepper.IClockManager;
import fr.kairos.lightccsl.core.stepper.INameToIntegerMapper;
import fr.kairos.lightccsl.core.stepper.ISolutionSet;
import fr.kairos.lightccsl.core.stepper.ISpecificationToSolution;
import fr.kairos.timesquare.ccsl.simple.ISpecificationBuilder;

/**
 * Solver relying either on MyBDD or SAT4J
 * 
 * @author fmallet
 *
 */
public abstract class ASATSolutionFinder implements ISpecificationToSolution {
	protected abstract ISATSolverBuilder getSATSolverBuilder();

	@Override
	public ISolutionSet buildSolutionsFor(ISpecificationBuilder builder,
			INameToIntegerMapper mapper, IClockManager clockManager) {
		ISATSolverBuilder satBuilder = getSATSolverBuilder();
		
		// boolSpec will contain the SAT model of our CCSL specification
		IBooleanSpecification boolSpec = satBuilder.specification();
		boolSpec.setNameMapper(mapper);

		// The SemanticBuilder knows how to transform a ISimpleSpecification into a SAT problem
		SemanticBuilder simple = new SemanticBuilder(boolSpec, clockManager);

		// plays the specification on the semantic builder to build a semantic model
		builder.build(simple); 
		
		return satBuilder.solution();
	}
}
