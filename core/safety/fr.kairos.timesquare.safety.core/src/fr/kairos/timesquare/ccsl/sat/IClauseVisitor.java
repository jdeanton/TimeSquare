package fr.kairos.timesquare.ccsl.sat;

public interface IClauseVisitor {

	void visit(AndClause andClause);

	void visit(Clause clause);

	void visit(OrClause orClause);

	void visit(XorClause xorClause);
	
	void visit(ITEClause iteClause);
}
