package fr.kairos.timesquare.ccsl.reduce;

import java.util.HashMap;
import java.util.Map;

public class AliasRepository {
	private Map<String, String> aliases;
	
	public AliasRepository() { this(new HashMap<>()); }
	public AliasRepository(Map<String,String> aliases) {
		this.aliases = aliases;
	}
	public String resolve(String name) {
		String res = aliases.get(name);
		if (res == null) return name;
		return resolve(res);
	}
	public String[] resolve(String...names) {
		String[] res = new String[names.length];
		for (int i = 0; i < res.length; i++) {
			res[i] = resolve(names[i]);
		}
		return res;
	}
	public String declare(String alias, String realClock) {
		realClock = resolve(realClock); // to avoid making aliases of aliases
		alias = resolve(alias); 
		aliases.put(alias, realClock);
		return realClock;
	}
	
	public IFilter<String> filterClock() {
		return s -> resolve(s);
	}
	public IFilter<String[]> filterClocks() {
		return s -> resolve(s);
	}
	public void reduce() {
		for (String key : aliases.keySet()) {
			String localKey = key;
			do {
				String other = resolve(localKey);
				if (other == localKey) break;
				localKey = other;
			} while (true);
			if (key != localKey)
				aliases.put(key, localKey);
		}
	}
}
