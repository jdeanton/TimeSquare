/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl;

/**
 * A generic CCSL specification, not assuming any library of "primitive" constraints
 *  
 * @author fmallet
 */
public interface ISpecification {
	public void addClock(String name);
	
	public void add(IRelation relation);
	public void add(IDefinition definition);
	public boolean isConstraintSupported(String name);
}
