/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.sat;

import fr.kairos.lightccsl.core.stepper.IClock;
import fr.kairos.lightccsl.core.stepper.IClockManager;
import fr.kairos.timesquare.ccsl.ISimpleSpecification;
import fr.kairos.timesquare.ccsl.simple.IUnfoldHelper;

/**
 * implements the semantics of CCSL constraints based on the given history
 * Someone needs to make the decision and make the history evolve (this is the role of the IClockManager)
 * 
 * @see fr.unice.lightccsl.sat
 * 
 * @author fmallet
 */
public class SemanticBuilder implements ISimpleSpecification {
	// build a boolean spec
	private IBooleanSpecification boolSpec;
	// the spec might depend on the history
	private IClockManager clockManager;
	
	public SemanticBuilder(IBooleanSpecification boolSpec, IClockManager clockManager) {
		super();
		this.boolSpec = boolSpec;
		this.clockManager = clockManager;
	}

	@Override
	public void addClock(String name) {
		boolSpec.id(name);
	}
	
	private void not(String c) {
		boolSpec.goalClause(c);
	}
	private void forbids(String c1, String c2) {
		boolSpec.goalClause(c1, c2);
	}
	private void implies(String c1, String ...clocks) {
		boolSpec.dualHornClause(c1, clocks);
	}
	@Override
	public void subclock(String left, String right) {
		implies(left, right); 
	}

	@Override
	public void exclusion(String left, String right) {
		boolSpec.goalClause(left, right); // forbids
	}

	@Override
	public void precedence(String left, String right) {
		// h_left >= h_right
		// h_l[0] = h_r[0] => !right, so when 0, only left can increase, 
		// otherwise goes back to 0 at max
		if (history(left) == history(right)) {
			not(right);
		}
	}

	@Override
	public void precedence(String left, String right, int init, int max) {
		int delta = history(left) - history(right) + init;
		if (delta == 0) {
			not(right); 
		} 
		if (max != -1) {
			if (delta == max) {
				not(left); 
			}
		}
	}

	@Override
	public void causality(String left, String right) {
		int delta = history(left) - history(right); // \delta >= 0
		if (delta == 0) {
			implies(right, left);
		}
	}

	@Override
	public void causality(String left, String right, int init, int max) {
		int delta = history(left) - history(right) + init;
		if (delta == 0) {
			implies(right, left);	
		} 
		if (max != -1) {
			if (delta == max) {
				implies(left, right);	
			}
		}
	}

	@Override
	public void inf(String defClock, String... clocks) {
		new IUnfoldHelper() {
			@Override
			public void binary(String def, String clock1, String clock2) {
				boolSpec.id(def);
				int chi = history(clock1) - history(clock2);
				if (chi > 0) {
					implies(clock1, def); 
					implies(def, clock1); 
				} else if (chi < 0) {
					implies(clock2, def); 
					implies(def, clock2); 
				} else { // chi == 0
					implies(clock1, def); 
					implies(clock2, def); 
					implies(def, clock1, clock2);
				}
			}
		}.unfold(defClock, clocks);
	}

	@Override
	public void sup(String defClock, String... clocks) {
		new IUnfoldHelper() {
			@Override
			public void binary(String def, String clock1, String clock2) {
				int chi = history(clock1) - history(clock2);
				if (chi > 0) {
					implies(clock2, def); 
					implies(def, clock2); 
				} else if (chi < 0) {
					implies(clock1, def);
					implies(def, clock1);
				} else {// chi == 0
					implies(def, clock1);
					implies(def, clock2);
					boolSpec.hornClause(def, clock1, clock2);
				}				
			}
		}.unfold(defClock, clocks);
	}

	@Override
	public void union(String defClock, String... clocks) {
// the or as below does not alter the spec ???
//		boolSpec.or(defClock, clocks);
		
		for (String clock : clocks) {
			implies(clock, defClock);
		}
		implies(defClock, clocks);
	}

	@Override
	public void intersection(String defClock, String... clocks) {
		// the and as below does not alter the spec ???
//		boolSpec.and(defClock, clocks);
		
		// AND(clocks) => defClock --> !AND(clocks) or defClock
		//                         -->  OR(!clocks) or defClock
		boolSpec.hornClause(defClock, clocks);
		
		// def => And(clocks) --> !def or and(clocks)
		//                    --> (!def or clocks[0]) and (!def or clocks[1])...
		//                    --> def => clocks[0] and def => clocks[1] ...
		for (String clock : clocks) {
			implies(defClock, clock);
		}
	}

	@Override
	public void minus(String defClock, String... clocks) {
		// defclock <=> clocks[0] And (!clocks[1:])
		// (def => c0 and !c1 and !c2...)  ->  !def or (c0 and !c1 and !c2...)
		implies(defClock, clocks[0]);
		for (int i = 1; i < clocks.length; i++) {
			forbids(defClock, clocks[i]);
		}
		// !(c0 and !c1 and !c2) or def -> (!c0 or c1 or c2) or def
		String c0 = clocks[0];
		clocks[0] = defClock;
		implies(c0, clocks);
		clocks[0] = c0; // restore in case used outside (not possible ?)
	}

	@Override
	public void periodic(String p, String ref, int period, int from, int upto) {
		implies(p, ref); // p is a subclock of ref
		
		int h = history(ref);
		
		if (h < from) {
			not(p);
			return;
		}
		if (upto != -1 && h > upto) {
			not(p);
			return;
		}
		
		h = ( h - from ) % period;
		if (h == 0) implies(ref, p);
		else not(p);
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upTo, String base) {
		if (base == null || from == 0) 
			delay(defClock, ref, from, upTo);
		else
			delay(defClock, ref, from, upTo, base);
	}
	
	private void delay(String d, String ref, int delay, int upTo) {
		implies(d, ref); // if no base, defClock is subclock of ref		

		int h = history(ref);
		
		if (h < delay) {
			not(d);
			return;
		}
		if (upTo != -1 && h > upTo) {
			not(d);
			return;
		}
		implies(ref, d);
	}
	
	private void delay(String d, String ref, int delay, int upTo, String base) {
		assert(base != null && delay != 0); // see delayFor
		IClock baseClock = clock(base);
		if (baseClock.getHistory() < delay) {
			not(d);
			return;
		}
		implies(d, base); // defClock is subclock of base

		int lastBase = baseClock.previous(delay - 1);
		int lastBaseBefore = baseClock.previous(delay);
		
		IClock refClock = clock(ref);
		int back = 0;
		do {
			int step = refClock.previous(back++);
			if (step < lastBaseBefore) break;
			if (step <= lastBase) {
				implies(base, d);
				return;				
			}
		} while (true);
		not(d);
		implies(d, ref); // just so 'ref' appears in the solutions
	}

	private IClock clock(String clock) {
		return clockManager.clock(clock);
	}
	private int history(String clock) {
		IClock c = clockManager.clock(clock);
		if (c == null) return 0;
		return c.getHistory();
	}	
}
