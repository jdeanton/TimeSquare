/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

import fr.kairos.common.graph.MyGraph;
import fr.kairos.timesquare.ccsl.adapters.GenericToSimple;
import fr.kairos.timesquare.ccsl.simple.Specification;

/**
 * Help building a MyGraph that represents the causality relations of a simple specification
 * 
 * @see MyGraph
 * 
 * @author fmallet
 */
public class Helper {
	static public MyGraph buildCausalityGraph(Specification spec) {
		BuildCausalityGraph bcg = new BuildCausalityGraph(true);
		spec.visit(new GenericToSimple(bcg));
		MyGraph graph = new MyGraph(bcg.getBuilder().getNodeNumber());
		bcg.getBuilder().buildGraph(graph);
		
		return graph;
	}
}
