/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Utility to pretty print a ISimpleSpecification
 * 
 * One parameter supported
 * target: Writer => Writer in which the spec is printed
 * target: PrintStream => PrintStream in which the spec is printed
 * by default: target=System.out
 * 
 * @author fmallet
 *
 */
public class PrettyPrintUtility extends AUtility implements IUtility {
	static public String REDUCE = "reduce";
	static public String OUT = "out";
	static public String V1 = "v1";
	@Override
	public void treat(String name, ISpecificationBuilder builder) {
// DO NOT apply again, as applied by default by the xtendGenerator.
//		if (this.getBooleanParam(REDUCE, true)) {
//			builder = new ReduceSpecificationBuilder(builder);
//		}
		StringWriter wr = new StringWriter();
		
		CollectClocks collect = new CollectClocks(true);
		builder.build(collect);
	
		boolean v1 = getBooleanParam(V1, false);
		PrettyPrint pp;
		if (v1) pp = new PrettyPrint(wr, name, collect.clocks());
		else pp = new PrettyPrintV2(wr, name, collect.clocks());
		
		builder.build(pp);
		
		pp.finish();
		try {
			Object target = getParam(OUT);
			if (target != null) {
				if (target instanceof Writer) {
					((Writer)target).write(wr.toString());
					return;
				} else if (target instanceof PrintStream) {
					((PrintStream)target).println(wr);
					return;
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		System.out.println(wr);
	}
}
