/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.util.HashMap;
import java.util.Map;

import fr.kairos.timesquare.ccsl.IDefinition;
import fr.kairos.timesquare.ccsl.IRelation;
import fr.kairos.timesquare.ccsl.ISpecification;

/**
 * Same as Copy but allow clock aliasing (replace one name by another one)
 * 
 * @author fmallet
 *
 */
public class CopyWithReplace implements ISpecification {
	private Specification result;
	private Map<String,String> replaceClocks = new HashMap<>();

	public CopyWithReplace(String newName) {
		this(newName, new HashMap<>());
	}
	public CopyWithReplace(String newName, Map<String,String> map) {
		result = new Specification(newName);
		this.replaceClocks = map;
	}

	public CopyWithReplace replace(String oldName, String newName) {
		replaceClocks.put(oldName, newName);
		return this;
	}
	public String replace(String name) {
		if (replaceClocks.containsKey(name))
			return replaceClocks.get(name);
		return name;
	}
	public String[] replace(String []clocks) {
		String []res = new String[clocks.length];
		for (int i = 0; i < clocks.length; i++) {
			res[i] = replace(clocks[i]);
		}
		return res;
	}
	@Override
	public void addClock(String name) {
		result.addClock(replace(name));
	}

	public Specification getResult() {
		return result;
	}
	
	@Override
	public void add(IDefinition def) {
		Definition res = new Definition(def.getDefinedClock(), def.getConstraintName(), replace(def.getRefClocks()));
		res.copyParamFrom(def);
		result.add(res);
	}
	@Override
	public void add(IRelation rel) {
		Relation res = new Relation(rel.getConstraintName(), replace(rel.getLeft()), replace(rel.getRight()));
		res.copyParamFrom(rel);
		result.add(res);
	}
	@Override
	public boolean isConstraintSupported(String name) {
		return true;
	}
}
