package fr.kairos.timesquare.ccsl.sat;

import java.util.Iterator;
import java.util.LinkedList;

public class AndClause extends SimpleClause implements IClause {
	private int res;
	AndClause(int res, int[] terms) {
		super(terms);
		this.res = res;		
	}
	@Override
	public Iterator<Clause> iterator() {
		LinkedList<Clause> res = new LinkedList<>();
		for (int v : values()) {
			res.add(Clause.dualHornClause(this.res, v));
		}
		res.add(Clause.hornClause(this.res, values()));
		return res.iterator();
	}
	@Override
	public void accept(IClauseVisitor visitor) {
		visitor.visit(this);
	}	
	public int getRes() {
		return res;
	}
}
