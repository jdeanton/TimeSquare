/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.extended;

import fr.kairos.timesquare.ccsl.IConstraint;

/**
 * One possible naive implementation of a Relation
 * Most of the time we try to avoid building explicit specifications to reduce memory footprint
 * 
 * @author fmallet
 */
class Relation implements IConstraint {
	static int count = 0;
	private String id, constraintName, left, right;
	Relation(String relation, String left, String right) {
		id = "r_" + count;
		count++;
		this.constraintName = relation;
		this.left = left;
		this.right = right;
	}
	public String toString() {
		return "Relation " + id + "[" + constraintName + "] (" + left + ", " + right + ")";
	}
	@Override
	public String getConstraintName() {
		return this.constraintName;
	}
}
