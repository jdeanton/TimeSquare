package fr.kairos.timesquare.ccsl.sat;

import java.util.Iterator;
import java.util.LinkedList;

public class XorClause extends SimpleClause implements IClause {
	private int res;
	XorClause(int res, int[] terms) {
		super(terms);
		this.res = res;		
	}
	@Override
	public Iterator<Clause> iterator() {
		LinkedList<Clause> res = new LinkedList<>();
		return res.iterator();
	}
	@Override
	public void accept(IClauseVisitor visitor) {
		visitor.visit(this);
	}	
	public int getRes() {
		return res;
	}
}
