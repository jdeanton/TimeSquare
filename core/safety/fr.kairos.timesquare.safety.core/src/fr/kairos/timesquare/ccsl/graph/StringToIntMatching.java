/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

import java.util.HashMap;

/**
 * Often needs a dictionary to map clocks into integers
 * 
 * @author fmallet
 */

public class StringToIntMatching {
	private HashMap<String,Integer> clockToInt = new HashMap<>();
	private int count = 0;
	private String[] names = null;
	private void buildNames() {
		if (names != null) return;
		names = new String[count];
		for (String name : clockToInt.keySet()) {
			names[clockToInt.get(name)] = name;
		}
	}
	final public int countNodes() {
		return count;
	}
	
	final public CharSequence intToNames(Iterable<Integer> it, String sep) {
		String s = "";
		StringBuilder builder = new StringBuilder();
		buildNames();
		for (int  i : it) {
			builder.append(s).append(names[i]);
			s = sep;
		}
		return builder;
	}
	final public CharSequence nodeNames() {
		buildNames();
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < names.length; i++) {
			builder.append(i).append(" -> ").append(names[i]).append('\n');
		}
		return builder;
	}
	final public String nodeName(int node) {
		buildNames();
		return names[node];
	}
	final public int node(String name) {
		if (clockToInt.containsKey(name)) return clockToInt.get(name);
		names = null;
		clockToInt.put(name, count);
		return count++;
	}

}
