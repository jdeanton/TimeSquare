/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.adapters;

import fr.kairos.timesquare.ccsl.IDefinition;
import fr.kairos.timesquare.ccsl.IRelation;
import fr.kairos.timesquare.ccsl.ISimpleSpecification;
import fr.kairos.timesquare.ccsl.ISpecification;

/**
 * Transform a generic specification into a simple one.
 * Assuming that no "non-simple" constraints are used.
 * 
 * @author fmallet
 */
final public class GenericToSimple implements ISpecification {
	public static String SUBCLOCK = "Subclock";
	public static String EXCLUSION = "Exclusion";
	public static String ALTERNATION = "Alternation";
	public static String CAUSALITY = "Causality";
	public static String PRECEDENCE = "Precedence";
	
	private ISimpleSpecification spec;

	public GenericToSimple(ISimpleSpecification spec) {
		super();
		this.spec = spec;
	}

	@Override
	public void addClock(String name) {
		spec.addClock(name);
	}

	private static String[] SUPPORTED_relations = { 
			SUBCLOCK,
			EXCLUSION,
			ALTERNATION,
			CAUSALITY,
			PRECEDENCE
	};
	private static String[] SUPPORTED_expressions = { 
			"Union", "Intersection", "Minus",
			"Inf", "Sup",
			"Periodic"
	};

	@Override
	public void add(IRelation relation) {
		switch(relation.getConstraintName()) {
		case "Subclock":
			spec.subclock(relation.getLeft(), relation.getRight()); break;
		case "Exclusion":
			spec.exclusion(relation.getLeft(), relation.getRight()); break;
		case "Causality":
		{
			int min = relation.getIntParam("min", 0);
			int max = relation.getIntParam("max", -1);
			if (min != 0 || max != -1)
				spec.causality(relation.getLeft(), relation.getRight(), min, max);
			else
				spec.causality(relation.getLeft(), relation.getRight());
		}
		break;
		case "Precedence":
		{
			int min = relation.getIntParam("min", 0);
			int max = relation.getIntParam("max", -1);
			if (min != 0 || max != -1)
				spec.precedence(relation.getLeft(), relation.getRight(), min, max);
			else
				spec.precedence(relation.getLeft(), relation.getRight());
		}
		break;
		case "Alternation":
			spec.causality(relation.getLeft(), relation.getRight(), 0, 1);
			break;
		default:
			throw new RuntimeException("Unsupported relation " + relation);
		}
	}
	
	@Override
	public void add(IDefinition definition) {
		switch(definition.getConstraintName()) {
		case "Union":
			spec.union(definition.getDefinedClock(), definition.getRefClocks()); 
			break;
		case "Intersection":
			spec.intersection(definition.getDefinedClock(), definition.getRefClocks());
			break;
		case "Inf":
			spec.inf(definition.getDefinedClock(), definition.getRefClocks());
			break;
		case "Sup":
			spec.sup(definition.getDefinedClock(), definition.getRefClocks());
			break;
		case "Minus":
			spec.minus(definition.getDefinedClock(), definition.getRefClocks());
			break;
		case "Periodic":
			{int period = definition.getIntParam("period", 1);
			int from = definition.getIntParam("from", 0);
			int upto = definition.getIntParam("upto", -1);
			spec.periodic(definition.getDefinedClock(), definition.getRefClocks()[0], period, from, upto);
			}break;
		case "DelayFor":
			{int from = definition.getIntParam("from", 0);
			int upto = definition.getIntParam("upto", -1);
			String base = null;
			if (definition.getRefClocks().length > 1) {
				base = definition.getRefClocks()[1];
			}
			spec.delayFor(definition.getDefinedClock(), definition.getRefClocks()[0], from, upto, base);
			}break;
		default:
			throw new RuntimeException("Unsupported definition " + definition);
		}
	}

	@Override
	public boolean isConstraintSupported(String name) {
		for (String s : SUPPORTED_relations)
			if (s.equals(name)) return true;
		for (String s : SUPPORTED_expressions)
			if (s.equals(name)) return true;
		return false;
	}
}
