/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.safety;

import java.util.HashSet;
import java.util.Set;

import fr.kairos.common.graph.MyGraph;
import fr.kairos.common.graph.StronglyConnectedComponents;
import fr.kairos.timesquare.ccsl.adapters.GenericToSimple;
import fr.kairos.timesquare.ccsl.graph.BuildCausalityGraph;
import fr.kairos.timesquare.ccsl.graph.BuildCounterGraph;
import fr.kairos.timesquare.ccsl.graph.BuildDelayedCausalityGraph;
import fr.kairos.timesquare.ccsl.graph.DigraphBuilder;
import fr.kairos.timesquare.ccsl.graph.DigraphBuilder.Arc;
import fr.kairos.timesquare.ccsl.simple.Specification;

/**
 * Implements MEMOCODE 2013.
 * 
 * Check whether a CCSL Specification is safe using counter graphs
 * 
 * @author fmallet
 */
public class SafetyAnalyser {
	private StronglyConnectedComponents scc;
	private MyGraph causalityGraph;
	private BuildCounterGraph bcog;
	
	public SafetyAnalyser(Specification spec) {
		// build a separate graph for counters
		bcog = new BuildCounterGraph();
		spec.visit(new GenericToSimple(bcog));

		if (! hasCounters()) {
			// there is no counter -> safe;
			return;
		}
		// build causality graph with delays
		BuildCausalityGraph bcg = new BuildCausalityGraph(true);
		DigraphBuilder builder = bcg.getBuilder();

		spec.visit(new GenericToSimple(bcg));

		BuildDelayedCausalityGraph bdcg = new BuildDelayedCausalityGraph(builder);
		spec.visit(new GenericToSimple(bdcg));

		this.causalityGraph = new MyGraph(builder.getNodeNumber());
		
		builder.buildGraph(this.causalityGraph);
		
		// find SCCs on causality graph
		this.scc = new StronglyConnectedComponents(this.causalityGraph);		

	}

	public boolean checkSafety() {
		if (! hasCounters()) return true; // no counter => safe

		for(Arc edge : bcog.getBuilder().edges()) { // iterate over adjacent edges
			if (!scc.stronglyConnected(node(edge.tail()), node(edge.head())))
				return false; // need only one
		}
		return true;
	}

	public boolean hasCounters() {
		return bcog.getBuilder().getEdgeNumber() > 0;
	}
	public Set<String> findUnsafeCounters() {
		Set<String> unsafeCounters = new HashSet<>();

		for(Arc edge : bcog.getBuilder().edges()) {
			if (!scc.stronglyConnected(node(edge.tail()), 
					node(edge.head()))) {
				unsafeCounters.add(edge.toString());
			}
		}

		return unsafeCounters;
	}
	
	private int node(String name) {
		return this.causalityGraph.nameToIndex(name);
	}
}
