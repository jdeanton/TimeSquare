package fr.kairos.timesquare.ccsl.reduce;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Given a list of aliases, would produce the same spec with replacing all aliases with actual names
 * @author fmallet
 *
 */
public class AliasBuilder implements ISimpleSpecification {
	private AliasRepository repository;
		
	public AliasBuilder(AliasRepository repo) {
		this.repository = repo;
	}
	@Override
	public void addClock(String name) {
	}

	@Override
	public void subclock(String left, String right) {
	}

	@Override
	public void exclusion(String left, String right) {
	}

	@Override
	public void precedence(String left, String right) {
	}

	@Override
	public void precedence(String left, String right, int min, int max) {
	}

	@Override
	public void causality(String left, String right) {
	}

	@Override
	public void causality(String left, String right, int min, int max) {
		if (max == 0) repository.declare(right, left);
	}

	@Override
	public void inf(String defClock, String... clocks) {
		if (clocks.length == 1)
			repository.declare(defClock, clocks[0]);
	}

	@Override
	public void sup(String defClock, String... clocks) {
		if (clocks.length == 1)
			repository.declare(defClock, clocks[0]);
	}

	@Override
	public void union(String defClock, String... clocks) {
		if (clocks.length == 1)
			repository.declare(defClock, clocks[0]);
	}

	@Override
	public void intersection(String defClock, String... clocks) {
		if (clocks.length == 1)
			repository.declare(defClock, clocks[0]);
	}

	@Override
	public void minus(String defClock, String... clocks) {
		if (clocks.length == 1)
			repository.declare(defClock, clocks[0]);
	}

	@Override
	public void periodic(String defClock, String ref, int period, int from, int upTo) {
		if (period == 1 && from == 0 && upTo == -1)
			repository.declare(defClock, ref);
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upTo, String base) {
		if (from == 0 && upTo == -1)
			repository.declare(defClock, ref);
	}
}
