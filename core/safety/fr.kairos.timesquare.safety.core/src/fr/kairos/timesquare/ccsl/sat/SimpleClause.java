package fr.kairos.timesquare.ccsl.sat;

/**
 * Represents an abstract clause in Boolean Satifiability Problem
 * Do not know if terms are positive or negative
 * Agnostique of the solver to be used
 * @see SAT4J or Choco
 * 
 * @author fmallet
 *
 */
public class SimpleClause {
	private int[] terms;
	protected SimpleClause (int[] terms) {
		this.terms = terms;
	}
	public int nbValues() { return terms==null?0:terms.length; }	
	public int[] values() {
		return terms;
	}
}
