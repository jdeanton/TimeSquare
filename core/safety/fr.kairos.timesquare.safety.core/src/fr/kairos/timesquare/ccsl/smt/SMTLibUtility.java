/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.smt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import fr.kairos.timesquare.ccsl.simple.AUtility;
import fr.kairos.timesquare.ccsl.simple.ISpecificationBuilder;
import fr.kairos.timesquare.ccsl.simple.IUtility;

/**
 * Utility to produce <<specName>>.smt-lib file from a ISimpleSpecification
 * 
 * Three parameters supported
 * folder: File => Gives the directory in which the file is produced
 * folderName: String => Gives the name of the directory in which the file is produced; def = smt-lib
 * len : int => Gives the parameter to the builder (number of values to print): def = 6
 * 
 * @author fmallet
 *
 */
public class SMTLibUtility extends AUtility implements IUtility {
	@Override
	public void treat(String name, ISpecificationBuilder specBuilder) {
		SMTLibBuilder builder = new SMTLibBuilder();
		SMTSpecification<String,String,String> smtlibs = new SMTSpecification<>(builder);		
		
		specBuilder.build(smtlibs);
		
		try {
			File dir = createFolder("smt-lib");
			File f = new File(dir, name + ".smt-lib");
			
			FileWriter fw =new FileWriter(f);
			int len = getIntParam("len", 6);
			builder.check(fw, len);
			fw.close();
			System.out.println("File generated " + f.getAbsolutePath());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
