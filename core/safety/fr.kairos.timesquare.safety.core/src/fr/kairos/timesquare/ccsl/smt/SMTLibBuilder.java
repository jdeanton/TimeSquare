/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.smt;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import fr.kairos.common.Pair;

/**
 * Concrete implements of ISMTBuilder that produces a .smtlib file
 * 
 * @author fmallet
 *
 */
public class SMTLibBuilder implements ISMTBuilder<String, String, String> {
	private ArrayList<String[]> constraints = new ArrayList<>();
	private ArrayList<String> clocks = new ArrayList<>();
	private ArrayList<String> clocksDecl = new ArrayList<>();	
	private Set<String> counters = new HashSet<>();
	private Set<Pair<String,String>> diffCounters = new HashSet<>();

	public SMTLibBuilder() {
		super();
	}
	
	@Override
	final public void assert_Ex(String e, String c) {
		addConstraint(assertE(existsNinInt(e)), c);
	}
	@Override
	final public void assert_FA(String e, String c) {
		addConstraint(assertE(forAllNinInt(e)), c);
	}	
	@Override
	final public String clockN(String clock) {
		return applyToN(tick(clock));
	}
	static String op(String op, String ...e) {
		return "(" + op + " " + String.join(" ", e)+ ")";
	}

	private void addConstraint(String cons, String com) {
		constraints.add(new String[] {cons, com});
	}
	@Override
	public String tick(String clock) {
		return "t_" + clock;
	}
	@Override
	public String counter(String clock) {
		return "c_" + clock;
	}
	@Override
	public void defineClock(String clock, String body, String comment) {
		clocks.add(clock);
		addConstraint(op("define-fun", tick(clock), "((n Int))", "Bool", body), comment); 
	}
	@Override
	public String assertE(String existsNinInt) {
		return "(assert " + existsNinInt + ")";
	}
	@Override
	public String existsNinInt(String e) {
		return "(exists ( (n Int) ) " + e + ")";
	}
	@Override
	public String forAllNinInt(String e) {
		return "(forall ( (n Int) ) " + e + ")";
	}
	@Override
	public String[] applyToNall(String ...clocks) {
		String [] tab = new String[clocks.length];
		for(int i = 0; i<clocks.length; i++) {
			tab[i] = clockN(clocks[i]);
		}
		return tab;
	}
	@Override
	public String apply(String f, String e) {
		return "(" + f + " " + e + ")";
	}
	@Override
	public String applyToN(String f) {
		return apply(f, "n");
	}
	@Override
	public String not(String e) {
		return "(not " + e + ")";
	}
	@Override
	public String ite(String i, String t, String e) {
		return "(ite " + i + " " + t + " " + e + ")";
	}
	@Override
	public String implies(String cause, String cons) {
		return op("=>", cause, cons);
	}
	@Override
	public String eq(String left, String right) {
		return op("=", left, right);
	}
	@Override
	public String gt(String left, String right) {
		return op(">", left, right);
	}
	@Override
	public String ge(String left, String right) {
		return op(">=", left, right);
	}
	@Override
	public String or(Object ...e) {
		return op("or", obj2str(e));
	}
	private String[] obj2str(Object[] t) {
		String[] res = new String[t.length];
		for (int i = 0; i < t.length; i++) {
			res[i] = t[i].toString();
		}
		return res;
	}
	@Override
	public String and(Object ...e) {
		return op("and", obj2str(e));
	}

	@Override
	public String mod(String left, String right) {
		return op("mod", left, right);
	}
	@Override
	public String constant(int v) {
		return v + "";
	}

	public void check(Object...params) {
		PrintWriter pw = new PrintWriter((Writer)params[0]);
		pw.println("(set-logic UFLIA)");
		int longueur = 6;
		if (params.length >1) {
			longueur = (Integer)params[1];
		}		
		
		for (String clock : clocksDecl) {
			pw.println();
			pw.println("; clock " + clock);
			pw.println("(declare-fun t_" + clock + " (Int) Bool)");			
		}
		String [] clockTicks = applyToNall(clocks.toArray(new String[clocks.size()]));
		assert_FA(or((Object[])clockTicks), "avoid empty steps");

		for (String c : counters) {
			buildCounter(pw, c);
		}
		for (Pair<String,String> p : diffCounters) {
			buildDiffCounter(pw, p);
		}
		for (String[] c : constraints) {
			pw.println();
			pw.println("; " + c[1]);
			pw.println(c[0]);
		}
		pw.println();
		pw.println("(check-sat)");
		pw.println("(get-model)");

		for (String clock : clocks) {
			pw.append("(get-value (");
			for (int i = 0; i < longueur; i++) {
				pw.append(op(tick(clock), i+""));
			}
			pw.println("))");
		}
		for (String clock : counters) {
			pw.append("(get-value (");
			for (int i = 0; i < longueur; i++) {
				pw.append(op(counter(clock), i+""));
			}
			pw.println("))");
		}
		for (Pair<String,String> clock : diffCounters) {
			pw.append("(get-value (");
			for (int i = 0; i < longueur; i++) {
				pw.append(op(counter(clock.getName()), i+""));
			}
			pw.println("))");
		}
	}

	private void buildDiffCounter(PrintWriter pw, Pair<String,String> p) {
		String n_1 = "(- n 1)"; 
		pw.println("\n(define-fun-rec c_" + p.getName() +" ((n Int)) Int");
		pw.println("    (ite (= 0 n)");
		pw.println("         0");
		pw.println("         " + op ("+", op ("c_" + p.getName(), n_1), 
				ite(op(tick(p.getLeft()), n_1), "1", "0"),
				ite(op(tick(p.getRight()), n_1), "-1", "0")));
		pw.println("    ))");
		pw.println("(define-fun-rec c_" + p.getRight() + "_" + p.getLeft() +" ((n Int)) Int");
		pw.println("    (- (c_" + p.getName() + " n)))");
	}
	private void buildCounter(PrintWriter pw, String clock) {
		String n_1 = "(- n 1)"; 
		pw.println("\n(define-fun-rec c_" + clock +" ((n Int)) Int");
		pw.println("    (ite (= 0 n)");
		pw.println("         0");
		pw.println("         " + op ("+", op ("c_" + clock, n_1), ite(op(tick(clock), n_1), "1", "0")));
		pw.println("    ))");
	}

	@Override
	public boolean addClock(String name) {
		if (clocks.contains(name)) return true;
		clocks.add(name);
		clocksDecl.add(name);
		return false;
	}

	@Override
	public String addCounterFor(String clock) {
		counters.add(clock);
		return "c_" + clock;
	}

	@Override
	public String addDiffCounter(String left, String right) {
		Pair<String,String> p = new Pair<>(left,right);
		diffCounters.add(p);
		return "c_" + p.getName();
	}
}
