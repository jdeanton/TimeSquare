/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.sat;

import fr.kairos.lightccsl.core.stepper.INameToIntegerMapper;

/**
 * Minimal interface to hide a library-specific SAT solver
 * 
 * @author fmallet
 */
public interface IBooleanSpecification {
	// name = And(vars);
	void and(String name, String...vars);
	// name = Or(vars);
	void or(String name, String...vars);
	// name = Or(vars);
	void xor(String name, String...vars);
	// name = IF cond THEN then ELSE else;
	void ite(String name, String condVar, String thenVar, String elseVar);
	
	// var0 Or  Or(!vars)
	// forces is a particular hornClause with no negative variable
	void hornClause(String var0, String ...vars);

	// !var0 or (Or(vars))
	// implies is a dualHornClause a=>b <=> !a v b
	void dualHornClause(String var0, String ...vars);

	// Or(!vars)
	// not is a particular goalClause with no positive variable
	// var1 forbids var2: var1 => ! var2   <=? !var1 v !var2
	void goalClause(String ...vars);
	
	int id(String var);
	
	void setNameMapper(INameToIntegerMapper nameMapper);

}
