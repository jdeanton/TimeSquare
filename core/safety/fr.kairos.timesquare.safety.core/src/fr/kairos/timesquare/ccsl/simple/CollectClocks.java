/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.util.LinkedHashSet;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Collect all the clocks within a specification
 * @author fmallet
 *
 */
public class CollectClocks implements ISimpleSpecification {	
	private LinkedHashSet<String> clocks = new LinkedHashSet<>();
	// true : collect only the ones expressed in addClock
	// false : collect also the ones used in constraints but not declared;
	private boolean onlyDeclared = true;
	public CollectClocks(boolean onlyDeclaredClocks) {
		this.onlyDeclared = onlyDeclaredClocks;
	}
	@Override
	public void addClock(String name) {
		clocks.add(name);
	}

	public Iterable<String> clocks() {
		return clocks;
	}
	@Override
	public void subclock(String left, String right) {
		if (!onlyDeclared) {
			addClock(left);
			addClock(right);
		}
	}
	@Override
	public void exclusion(String left, String right) {
		if (!onlyDeclared) {
			addClock(left);
			addClock(right);
		}
	}
	@Override
	public void precedence(String left, String right) {
		if (!onlyDeclared) {
			addClock(left);
			addClock(right);
		}
	}
	@Override
	public void precedence(String left, String right, int min, int max) {
		if (!onlyDeclared) {
			addClock(left);
			addClock(right);
		}
	}
	@Override
	public void causality(String left, String right) {
		if (!onlyDeclared) {
			addClock(left);
			addClock(right);
		}
	}
	@Override
	public void causality(String left, String right, int min, int max) {
		if (!onlyDeclared) {
			addClock(left);
			addClock(right);
		}
	}
	private void expression(String defClock, String... clocks) {
		if (!onlyDeclared) {
			addClock(defClock);
			for (String clock : clocks) {
				addClock(clock);
			}
		}
	}
	@Override
	public void inf(String defClock, String... clocks) {
		expression(defClock, clocks);
	}
	@Override
	public void sup(String defClock, String... clocks) {
		expression(defClock, clocks);
	}
	@Override
	public void union(String defClock, String... clocks) {
		expression(defClock, clocks);
	}
	@Override
	public void intersection(String defClock, String... clocks) {
		expression(defClock, clocks);
	}
	@Override
	public void minus(String defClock, String... clocks) {
		expression(defClock, clocks);
	}
	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		if (!onlyDeclared) {
			addClock(defClock);
			addClock(ref);
		}
	}
	@Override
	public void delayFor(String defClock, String ref, int from, int upto, String base) {
		if (!onlyDeclared) {
			addClock(defClock);
			addClock(ref);
			if (base != null && !base.equals(ref)) {
				addClock(base);
			}
		}
	}
}
