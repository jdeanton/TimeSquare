/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.util.LinkedList;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Use several specifications as one
 * @author fmallet
 *
 */
public class SimpleSpecifications implements ISimpleSpecification {	
	private LinkedList<ISimpleSpecification> specifications = new LinkedList<>();
	public SimpleSpecifications(ISimpleSpecification ...specifications) {
		add(specifications);
	}
	public void add(ISimpleSpecification ...specifications) {
		for (ISimpleSpecification spec : specifications) {
			this.specifications.add(spec);
		}
	}
	@Override
	public void addClock(String name) {
		for (ISimpleSpecification spec : specifications)
			spec.addClock(name);
	}

	@Override
	public void subclock(String left, String right) {
		for (ISimpleSpecification spec : specifications)
			spec.subclock(left, right);
	}
	@Override
	public void exclusion(String left, String right) {
		for (ISimpleSpecification spec : specifications)
			spec.exclusion(left, right);
	}
	@Override
	public void precedence(String left, String right) {
		for (ISimpleSpecification spec : specifications)
			spec.precedence(left, right);
	}
	@Override
	public void precedence(String left, String right, int min, int max) {
		for (ISimpleSpecification spec : specifications)
			spec.precedence(left, right, min, max);
	}
	@Override
	public void causality(String left, String right) {
		for (ISimpleSpecification spec : specifications)
			spec.causality(left, right);
	}
	@Override
	public void causality(String left, String right, int min, int max) {
		for (ISimpleSpecification spec : specifications)
			spec.causality(left, right, min, max);
	}
	@Override
	public void inf(String defClock, String... clocks) {
		for (ISimpleSpecification spec : specifications)
			spec.inf(defClock, clocks);
	}
	@Override
	public void sup(String defClock, String... clocks) {
		for (ISimpleSpecification spec : specifications)
			spec.sup(defClock, clocks);
	}
	@Override
	public void union(String defClock, String... clocks) {
		for (ISimpleSpecification spec : specifications)
			spec.union(defClock, clocks);
	}
	@Override
	public void intersection(String defClock, String... clocks) {
		for (ISimpleSpecification spec : specifications)
			spec.intersection(defClock, clocks);
	}
	@Override
	public void minus(String defClock, String... clocks) {
		for (ISimpleSpecification spec : specifications)
			spec.minus(defClock, clocks);
	}
	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		for (ISimpleSpecification spec : specifications)
			spec.periodic(defClock, ref, period, from, upto);
	}
	@Override
	public void delayFor(String defClock, String ref, int from, int upto, String base) {
		for (ISimpleSpecification spec : specifications)
			spec.delayFor(defClock, ref, from, upto, base);
	}
}
