/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;

/**
 * Serialize a IGraph as a .DOT format.
 * @see IGraph
 * 
 * @author fmallet
 */
public class DotGraph implements IGraph {
	private StringBuilder edges = new StringBuilder();
	private Set<String> clocks = new HashSet<>();


	private void addStyle(String type, StringBuilder sb) {
		switch(type){
		case "delayed":
			sb.append("[\"style\"=\"dashed\"]");
			break;
		case "subclock":
			sb.append("[\"color\"=\"blue\", \"arrowhead\"=\"onormal\"]"); //replace empty (deprecated by onormal)
			break;
		case "counter":
			sb.append("[\"color\"=\"red\", \"dir\"=\"both\", \"arrowtail\"=\"dot\", \"arrowhead\"=\"dot\"]");
			break;
		}

	}
	@Override
	public void addEdge(String tail, String head, Object... param) {
		edges.append(tail).append(" -> ").append(head);
		if (param != null && param.length > 0 && param[0] != null)
			addStyle(param[0].toString(), edges);
		edges.append("\n");
	}

	public void toDotFile(Writer w) {
		PrintWriter pw = new PrintWriter(w);
		pw.println("digraph {");
		for (String c : clocks) {
			pw.println("  \"" + c + "\";");
		}
		pw.println(edges);
		pw.println("}");
	}

	@Override
	public void addNode(String name) {
		clocks.add(name);		
	}

}
