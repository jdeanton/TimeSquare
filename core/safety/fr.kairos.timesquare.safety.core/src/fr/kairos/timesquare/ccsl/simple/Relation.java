/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.util.HashMap;

import fr.kairos.timesquare.ccsl.IRelation;

/**
 * Simple implementation of a generic CCSL Relation
 * 
 * @author fmallet
 *
 */
final public class Relation implements IRelation {
	private String relationName;
	private HashMap<String,Integer> params;
	private String left, right;
	
	public Relation(String relationName, String left, String right) {
		this.relationName = relationName;
		this.left = left;
		this.right = right;
	}
	public Relation set(String name, int value) {
		if (params==null) params = new HashMap<String, Integer>();		
		params.put(name, value);
		return this;
	}
	public int getIntParam(String name, int defaut) {
		if (params!=null && params.containsKey(name))
			return params.get(name);
		return defaut;
	}
	@Override
	public String toString() {
		return relationName + " " + left + " " + right;
	}
	@Override
	public String getLeft() {
		return this.left;
	}
	@Override
	public String getRight() {
		return this.right;
	}
	@Override
	public String getConstraintName() {
		return relationName;
	}
	
	public void copyParamFrom(IRelation rel) {
		if (rel instanceof Relation) {
			Relation r = (Relation)rel;
			if (r.params != null) {
				for (String k : r.params.keySet()) {
					set(k, r.params.get(k));
				}
			}
		} else 
			throw new RuntimeException("Do not know how to copy params");
	}
}

