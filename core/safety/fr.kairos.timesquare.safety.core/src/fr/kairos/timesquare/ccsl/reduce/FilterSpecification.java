package fr.kairos.timesquare.ccsl.reduce;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Apply an external filter to all clocks in a specification
 * @author fmallet
 *
 */
public class FilterSpecification implements ISimpleSpecification {
	private ISimpleSpecification delegate;
	private IFilter<String> filterClock;
	private IFilter<String[]> filterClocks;
	
	public FilterSpecification(ISimpleSpecification spec, IFilter<String> filterClock, IFilter<String[]> filterClocks) {
		this.delegate = spec;
		this.filterClock = filterClock;
		this.filterClocks = filterClocks;
	}
	
	private String resolve(String clock) { return filterClock.filter(clock); }
	private String[] resolve(String[] clocks) { return filterClocks.filter(clocks); }
	
	@Override
	public void addClock(String name) {
		delegate.addClock(resolve(name));
	}

	@Override
	public void subclock(String left, String right) {
		delegate.subclock(resolve(left), resolve(right));
	}

	@Override
	public void exclusion(String left, String right) {
		delegate.exclusion(resolve(left), resolve(right));
	}

	@Override
	public void precedence(String left, String right) {
		delegate.precedence(resolve(left), resolve(right));
	}

	@Override
	public void precedence(String left, String right, int min, int max) {
		delegate.precedence(resolve(left), resolve(right), min, max);
	}

	@Override
	public void causality(String left, String right) {
		delegate.causality(resolve(left), resolve(right));
	}

	@Override
	public void causality(String left, String right, int min, int max) {
		if (max == 0) return;
		
		delegate.causality(resolve(left), resolve(right), min, max);
	}

	@Override
	public void inf(String defClock, String... clocks) {
		if (clocks.length == 1)
			return;
		delegate.inf(resolve(defClock), resolve(clocks));
	}

	@Override
	public void sup(String defClock, String... clocks) {
		if (clocks.length == 1)
			return;
		delegate.sup(resolve(defClock), resolve(clocks));
	}

	@Override
	public void union(String defClock, String... clocks) {
		if (clocks.length == 1)
			return;
		delegate.union(resolve(defClock), resolve(clocks));
	}

	@Override
	public void intersection(String defClock, String... clocks) {
		if (clocks.length == 1)
			return;
		delegate.intersection(resolve(defClock), resolve(clocks));
	}

	@Override
	public void minus(String defClock, String... clocks) {
		if (clocks.length == 1)
			return;
		delegate.minus(resolve(defClock), resolve(clocks));
	}

	@Override
	public void periodic(String defClock, String ref, int period, int from, int upTo) {
		if (period == 1 && from == 0 && upTo == -1)
			return;
		delegate.periodic(resolve(defClock), resolve(ref), period, from, upTo);
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upTo, String base) {
		if (from == 0 && upTo == -1) {
			//delegate.delayFor(defClock, resolve(ref), from, upTo, resolve(base)); // otherwise loose the trace of the existence of the equivalent clocks
		} else
			delegate.delayFor(resolve(defClock), resolve(ref), from, upTo, resolve(base));
	}
}
