/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.extended;

import java.io.File;
import java.io.IOException;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;
import fr.kairos.timesquare.ccsl.adapters.SimpleToGeneric;
import fr.kairos.timesquare.ccsl.simple.AUtility;
import fr.kairos.timesquare.ccsl.simple.ISpecificationBuilder;
import fr.kairos.timesquare.ccsl.simple.IUtility;

/**
 * Utility to produce <<specName>>.extendedCCSL file from a ISimpleSpecification
 * 
 * Two params supported
 * folder: File => Gives the directory in which the file is produced
 * folderName: String => Gives the name of the directory in which the file is produced
 * 
 * @author fmallet
 *
 */
public class XCCSLUtility extends AUtility implements IUtility {
	@Override
	public void treat(String name, ISpecificationBuilder specBuilder) {
		XCCSLBuilder builder = new XCCSLBuilder();
		ISimpleSpecification spec = new SimpleToGeneric(builder);
		specBuilder.build(spec);

		File folder = createFolder("xccsl");
		try {
			builder.printTo(name, folder);
			System.out.println("Printed to " + folder.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
