/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.extended;

import fr.kairos.timesquare.ccsl.IConstraint;

/**
 * One possible naive implementation of a Definition
 * Most of the time we try to avoid building explicit specifications to reduce memory footprint
 * 
 * @author fmallet
 */
class Definition implements IConstraint {
	private String constraintName;
	private String defClock, c1, c2;
	
	Definition(String constraintName, String defClock, String c1, String c2) {
		super();
		this.constraintName = constraintName;
		this.defClock = defClock;
		this.c1 = c1;
		this.c2 = c2;
	}

	@Override
	public String toString() {
		return "Expression " + defClock + "=" + constraintName + "("
				+ c1 + ", " + c2 + ")";
	}
	
	@Override
	public String getConstraintName() {
		return this.constraintName;
	}
}
