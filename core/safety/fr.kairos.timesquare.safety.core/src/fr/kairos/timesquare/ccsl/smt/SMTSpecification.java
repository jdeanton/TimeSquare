/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.smt;

import java.util.Arrays;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Provides the semantics of CCSL Specification as a SMT model
 * The build adapts the semantics to the back-end
 * @see SMTLibBuilder backend towards smt-lib files
 * @see Z3Builder backend towards z3 Java API
 *
 * @author fmallet
 *
 * @param <T> type for encoding expressions
 * @param <F> type for Encoding function declarations
 */
final public class SMTSpecification<T, B extends T, F> implements ISimpleSpecification {
	private ISMTBuilder<T, B, F> b;
	
	public SMTSpecification(ISMTBuilder<T, B, F> builder) {
		super();
		this.b = builder;
	}
	
	@Override
	public void addClock(String name) {
		boolean res = b.addClock(name);
		if (!res)
			b.assert_Ex(b.clockN(name), "avoid empty clock " + name);		
	}
	
	@Override
	final public void subclock(String left, String right) {
		b.assert_FA(b.implies(b.clockN(left), b.clockN(right)), left + " isSubclockOf " + right);
	}

	@Override
	final public void exclusion(String left, String right) {
		b.assert_FA(b.or(b.not(b.clockN(left)), b.not(b.clockN(right))), 
				left + " excludes " + right);		
	}

	private void prec(String left, String right, int init) {
		F counter= b.addDiffCounter(left, right);
		b.assert_FA(b.implies(b.eq(b.applyToN(counter), b.constant(init)), b.not(b.clockN(right))), 
				left + " < " + right);
		
	}
	@Override
	final public void precedence(String left, String right) {
		prec(left, right, 0);
	}

	@Override
	final public void precedence(String left, String right, int min, int max) {
		prec(left, right, min);
		if (max != -1)
			prec(right, left, max);
	}

	private void caus(String left, String right, int init) {
		F counter= b.addDiffCounter(left, right);
		b.assert_FA(b.ge(b.applyToN(counter), b.constant(-init)), left + " <= " + right); 
		
	}
	@Override
	final public void causality(String left, String right) {
		caus(left, right, 0);
	}

	@Override
	final public void causality(String left, String right, int min, int max) {
		caus(left, right, min);
		if (max != -1)
			caus(right, left, max);
	}

	@Override
	final public void inf(String defClock, String... clocks) {
		String right = clocks[1];
		if (clocks.length > 2) {
			right = defClock + "_i";
			String[] newClocks = Arrays.copyOfRange(clocks, 1, clocks.length);
			inf(right, newClocks);
		}
		inf(defClock, clocks[0], right);
	}
	
	private void inf(String defClock, String left, String right) {
		F counter= b.addDiffCounter(left, right);
		b.assert_FA(b.eq(b.clockN(defClock), 
						b.ite(b.ge(b.applyToN(counter), b.constant(0)), 
							  b.clockN(right), b.clockN(left))),
				"inf(" + left + ", " + right + ")");
	}
	private void sup(String defClock, String left, String right) {
		F counter= b.addDiffCounter(left, right);
		b.assert_FA(b.eq(b.clockN(defClock), 
						b.ite(b.ge(b.applyToN(counter), b.constant(0)), 
							  b.clockN(left), b.clockN(right))),
				"sup(" + left + ", " + right + ")");
	}

	@Override
	final public void sup(String defClock, String... clocks) {
		String right = clocks[1];
		if (clocks.length > 2) {
			right = defClock + "_i";
			String[] newClocks = Arrays.copyOfRange(clocks, 1, clocks.length);
			sup(right, newClocks);
		}
		sup(defClock, clocks[0], right);
	}

	@Override
	final public void union(String defClock, String... clocks) {
		b.defineClock(defClock, 
				b.or(b.applyToNall(clocks)), 
				defClock + " = " + String.join(" + ", clocks));
	}
	@Override
	final public void intersection(String defClock, String... clocks) {
		b.defineClock(defClock, 
				b.and(b.applyToNall(clocks)), 
				defClock + " = " + String.join(" * ", clocks));
	}

	@Override
	final public void minus(String defClock, String... clocks) {
		B [] app = b.applyToNall(clocks);
		for (int i = 1; i < app.length; i++) {
			app[i] = b.not(app[i]);
		}
		b.defineClock(defClock, 
				b.and(app), 
				defClock + " = " + String.join(" - ", clocks));
	}

	@Override
	final public void periodic(String defClock, String ref, int period, int from, int upto) {
		F counter= b.addCounterFor(ref);
		b.defineClock(defClock, b.and(b.clockN(ref), 
				                      b.eq(b.constant(period - 1), 
								           b.mod(b.applyToN(counter), 
								                 b.constant(period)))),
				"repeat " + defClock + " every " + period + " " + ref + " from " + from + " upto " + upto);
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upto, String base) {
		String s = 	defClock + 
				" = " + ref;
		if (upto == -1)
			s += " $ " + from;
		else
			s += "[" + from + ", " + upto + "]";
		if (base == null) {
			F counter= b.addCounterFor(ref);
			if (upto == -1)
				b.defineClock(defClock, b.and(b.clockN(ref), 
					                      b.gt(b.applyToN(counter), b.constant(from - 1))), s);
			else
				b.defineClock(defClock, b.and(b.clockN(ref), 
	                      b.gt(b.applyToN(counter), b.constant(from - 1)),
	                      b.gt(b.constant(upto), b.applyToN(counter))), s);
			
		} else {
			s += " on " + base; 
			System.err.println("does not support " + s);
		}
	}

}
