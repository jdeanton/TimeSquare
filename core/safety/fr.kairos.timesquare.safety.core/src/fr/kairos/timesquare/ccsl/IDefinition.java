/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl;

/**
 * A CCSL Definition
 * definedClock is defined based on refClocks
 * 
 * Some integer or String parameters are allowed, with a dictionary-based naming convention, name=>value
 * 
 * @author fmallet
 *
 */
public interface IDefinition extends IConstraint {
	String getDefinedClock();
	int getIntParam(String name, int defaut);
	String getStringParam(String name);
	String[] getRefClocks();
}
