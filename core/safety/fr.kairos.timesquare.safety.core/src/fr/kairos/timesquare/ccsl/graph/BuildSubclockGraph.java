/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Build a subclock Graph from a Simple specification
 * Make an arc every time there is a subclocking relation.
 * 
 * @author fmallet
 */
final public class BuildSubclockGraph implements ISimpleSpecification {
	private DigraphBuilder builder;
	public BuildSubclockGraph() {
		this(new DigraphBuilder());
	}
	public BuildSubclockGraph(DigraphBuilder builder) {
		this.builder = builder;
		this.builder.setParam("subclock");
	}

	private void addArc(String tail, String head) {
		builder.addArc(tail, head);
	}

	public DigraphBuilder getBuilder() {
		return builder;
	}
	@Override
	public void addClock(String name) {
//		builder.node(name);
	}

	@Override
	public void subclock(String left, String right) {
		addArc(left, right);
	}

	@Override
	public void exclusion(String left, String right) {
//		union(left + "_" + right, left, right);
	}

	@Override
	public void precedence(String left, String right) {
		causality(left, right, 1, -1); // ?
	}

	@Override
	public void precedence(String left, String right, int min, int max) {
		causality(left, right, min + 1, max); // ?
	}

	@Override
	public void causality(String left, String right) {
	}

	@Override
	public void causality(String left, String right, int min, int max) {
//		if (min > 0)
//			union(left + "_" + right, left, right);
	}

	@Override
	public void inf(String defClock, String... clocks) {
//		builder.node(defClock);
		for (String c : clocks) {
			causality(defClock, c);
		}
	}

	@Override
	public void sup(String defClock, String... clocks) {
//		builder.node(defClock);
		for (String c : clocks) {
			causality(c, defClock);
		}
	}

	@Override
	public void union(String defClock, String... clocks) {
		for (String c : clocks) {
			subclock(c, defClock);
		}
	}

	@Override
	public void intersection(String defClock, String... clocks) {
		for (String c : clocks) {
			subclock(defClock, c);
		}
	}

	@Override
	public void minus(String defClock, String... clocks) {
		subclock(defClock, clocks[0]);
	}

	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		subclock(defClock, ref);
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upTo, String base) {
		if (base == null)
			subclock (defClock, ref);
		else 
			subclock (defClock, base);
		if (from == 0) 
			subclock(ref, defClock);
	}
}
