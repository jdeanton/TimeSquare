/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.util.Arrays;
import java.util.HashMap;

import fr.kairos.timesquare.ccsl.IDefinition;

/**
 * Simple implementation of a generic CCSL Definition
 * 
 * @author fmallet
 *
 */
final public class Definition implements IDefinition {
	private String expressionName;
	private HashMap<String,Integer> intParams;
	private HashMap<String,String> stringParams;
	private String[] refClocks;
	private String definedClock;
	
	public Definition(String definedClockName, String expressionName, String ...clocks) {
		this.expressionName = expressionName;
		this.refClocks = clocks;
		this.definedClock = definedClockName;
	}
	public Definition set(String name, int value) {
		if (intParams==null) intParams = new HashMap<String, Integer>();
		intParams.put(name, value);
		return this;
	}
	public Definition set(String name, String value) {
		if (stringParams==null) stringParams = new HashMap<String, String>();
		stringParams.put(name, value);
		return this;
	}
	@Override
	public int getIntParam(String name, int defaut) {
		if (intParams != null && intParams.containsKey(name))
			return intParams.get(name);
		return defaut;
	}

	@Override
	public String getStringParam(String name) {
		if (stringParams != null && stringParams.containsKey(name))
			return stringParams.get(name);
		return null;
	}
	
	@Override
	public String toString() {
		return definedClock + " = " + expressionName + "(" + Arrays.deepToString(refClocks) + ")";
	}
	@Override
	public String getDefinedClock() {
		return this.definedClock;
	}
	
	@Override
	public String[] getRefClocks() {
		return refClocks;
	}
	@Override
	public String getConstraintName() {
		return this.expressionName;
	}
	public void copyParamFrom(IDefinition def) {
		if (def instanceof Definition) {
			Definition d = (Definition)def;
			if (d.stringParams != null) {
				for (String k : d.stringParams.keySet()) {
					set(k, d.stringParams.get(k));
				}
			}
			if (d.intParams != null) {
				for (String k : d.intParams.keySet()) {
					set(k, d.intParams.get(k));
				}
			}
		} else 
			throw new RuntimeException("Do not know how to copy params");
	}
}
