/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.sat;

import fr.kairos.lightccsl.core.stepper.ISolutionSet;

/**
 * Abstract Factory for SATSolver (Hide whether it is based on MyBDD or SAT4J)
 * 
 * @author fmallet
 */
public interface ISATSolverBuilder {
	IBooleanSpecification specification();
	ISolutionSet solution();
}
