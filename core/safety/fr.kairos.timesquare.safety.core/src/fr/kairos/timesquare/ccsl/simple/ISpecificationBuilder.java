/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Interface to apply a spec without assuming a particular format
 * 
 * @author fmallet
 *
 */
public interface ISpecificationBuilder {
	void build(ISimpleSpecification spec);
}
