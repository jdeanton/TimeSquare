/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Build a Delayed Causality Graph from a Simple specification
 * Convert delays into causality
 * 
 * @author fmallet
 */
final public class BuildDelayedCausalityGraph implements ISimpleSpecification {
	private DigraphBuilder builder;
	public BuildDelayedCausalityGraph() {
		this(new DigraphBuilder());
	}
	public BuildDelayedCausalityGraph(DigraphBuilder builder) {
		this.builder = builder;
		this.builder.setParam("delayed");
	}

	private void addArc(String tail, String head) {
		builder.addArc(tail, head);
	}
	
	public DigraphBuilder getBuilder() {
		return builder;
	}
	@Override
	public void addClock(String name) {
//		builder.node(name);
	}
	@Override
	public void subclock(String left, String right) {
	}

	@Override
	public void exclusion(String left, String right) {
	}

	@Override
	public void precedence(String left, String right) {
	}

	@Override
	public void precedence(String left, String right, int min, int max) {
		if (max != -1) {
			addArc(right, left);
		}
	}

	@Override
	public void causality(String left, String right) {
	}

	@Override
	public void causality(String left, String right, int min, int max) {
		if (max != -1) {
			addArc(right, left);
		}
	}

	@Override
	public void inf(String defClock, String... clocks) {
	}

	@Override
	public void sup(String defClock, String... clocks) {
	}

	@Override
	public void union(String defClock, String... clocks) {
	}

	@Override
	public void intersection(String defClock, String... clocks) {
	}

	@Override
	public void minus(String defClock, String... clocks) {
	}

	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		if (period == 1 && upto == -1) {
			addArc(defClock, ref);
		}
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upTo, String base) {
		if (from == 0) return;
		if (base == null || base == ref) {
			addArc(defClock, ref);			
		}
	}
}
