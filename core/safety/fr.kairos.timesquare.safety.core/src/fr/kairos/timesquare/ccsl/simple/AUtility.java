/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import fr.kairos.common.ParameterUtility;

/**
 * Utility Class : relies now on common class but keep the API.
 * Just given to keep the API, should use ParameterUtility instead
 * 
 * @author fmallet
 *
 */
public abstract class AUtility  extends ParameterUtility implements IUtility {
}
