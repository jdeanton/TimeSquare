/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.adapters;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;
import fr.kairos.timesquare.ccsl.ISpecification;
import fr.kairos.timesquare.ccsl.simple.Definition;
import fr.kairos.timesquare.ccsl.simple.Relation;

/**
 * Transform a simple specification into a generic one.
 * Useful to store a specification in a general way.
 * 
 * @author fmallet
 */
final public class SimpleToGeneric implements ISimpleSpecification {
	private ISpecification spec;

	public SimpleToGeneric(ISpecification spec) {
		super();
		this.spec = spec;
	}

	@Override
	public void addClock(String name) {
		spec.addClock(name);
	}

	@Override
	public void subclock(String left, String right) {
		spec.add(new Relation("Subclock", left, right));
	}

	@Override
	public void exclusion(String left, String right) {
		spec.add(new Relation("Exclusion", left, right));
	}

	@Override
	public void precedence(String left, String right) {
		spec.add(new Relation("Precedence", left, right));
	}

	@Override
	public void precedence(String left, String right, int min, int max) {
		spec.add(new Relation("Precedence", left, right).set("min", min).set("max", max));
	}

	@Override
	public void causality(String left, String right) {
		spec.add(new Relation("Causality", left, right));
	}

	@Override
	public void causality(String left, String right, int min, int max) {
		spec.add(new Relation("Causality", left, right).set("min", min).set("max", max));
	}

	@Override
	public void inf(String defClock, String... clocks) {
		spec.add(new Definition(defClock, "Inf", clocks));
	}

	@Override
	public void sup(String defClock, String... clocks) {
		spec.add(new Definition(defClock, "Sup", clocks));
	}

	@Override
	public void union(String defClock, String... clocks) {
		spec.add(new Definition(defClock, "Union", clocks));
	}

	@Override
	public void intersection(String defClock, String... clocks) {
		spec.add(new Definition(defClock, "Intersection", clocks));
	}

	@Override
	public void minus(String defClock, String... clocks) {
		spec.add(new Definition(defClock, "Minus", clocks));
	}

	private Definition p(String defClock, String ref, int period, int from, int upto) {
		return new Definition(defClock, "Periodic", ref).set("period", period).set("from", from).set("upto", upto);
	}
	
	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		spec.add(this.p(defClock, ref, period, from, upto));
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upto, String base) {
		if (base != null)
			spec.add(new Definition(defClock, "DelayFor", ref, base).set("from", from).set("upto", upto));
		else
			spec.add(new Definition(defClock, "DelayFor", ref).set("from", from).set("upto", upto));
	}
}
