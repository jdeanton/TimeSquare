/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.graph;

import fr.kairos.timesquare.ccsl.ISimpleSpecification;

/**
 * Build a causality Graph from a Simple specification
 * May build direct causalities (a <= b) or derived (e.g. from subclocking relations)
 * 
 * @author fmallet
 */
final public class BuildCausalityGraph implements ISimpleSpecification {
	private DigraphBuilder builder;
	private boolean showDerived = false;
	
	public BuildCausalityGraph(boolean showDerived) {
		this(showDerived, new DigraphBuilder());
	}
	public BuildCausalityGraph(boolean showDerived, DigraphBuilder builder) {
		this.builder = builder;
		this.showDerived = showDerived;
		this.builder.setParam("causality");
	}
	
	private void addArc(String tail, String head) {
		builder.addArc(tail, head);
	}
	
	public DigraphBuilder getBuilder() {
		return builder;
	}
	@Override
	public void addClock(String name) {
//		builder.node(name);
	}
	@Override
	public void subclock(String left, String right) {
		if (left.equals(right)) return;

		if (showDerived)
			addArc(right, left);
	}

	@Override
	public void exclusion(String left, String right) {
	}

	@Override
	public void precedence(String left, String right) {
		addArc(left, right);
	}

	@Override
	public void precedence(String left, String right, int min, int max) {
		addArc(left, right);
	}

	@Override
	public void causality(String left, String right) {
		if (left.equals(right)) return;

		addArc(left, right);
	}

	@Override
	public void causality(String left, String right, int min, int max) {
		if (left.equals(right)) return;

		addArc(left, right);
	}

	@Override
	public void inf(String defClock, String... clocks) {
		for (String c : clocks) {
			causality(defClock, c);
		}
	}

	@Override
	public void sup(String defClock, String... clocks) {
		for (String c : clocks) {
			causality(c, defClock);
		}
	}

	@Override
	public void union(String defClock, String... clocks) {
		if (!showDerived) return;
			
		for (String c : clocks) {
			addArc(defClock, c);
		}
	}

	@Override
	public void intersection(String defClock, String... clocks) {
		if (!showDerived) return;

		for (String c : clocks) {
			addArc(c, defClock);
		}
	}

	@Override
	public void minus(String defClock, String... clocks) {
		if (!showDerived) return;

		addArc(clocks[0], defClock);
	}

	@Override
	public void periodic(String defClock, String ref, int period, int from, int upto) {
		if (!showDerived) return;

		addArc(ref, defClock);
	}

	@Override
	public void delayFor(String defClock, String ref, int from, int upTo, String base) {
		if (!showDerived) return;

		addArc(ref, defClock);
		if (from == 0) {
			addArc(defClock, ref);
		}
		if (base != null && !base.equals(ref)) 
			addArc(base, defClock);
	}
}
