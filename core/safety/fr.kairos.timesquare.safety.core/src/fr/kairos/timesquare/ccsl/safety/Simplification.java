/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.safety;

import fr.kairos.common.graph.AllPathsDFS;
import fr.kairos.common.graph.MyGraph;
import fr.kairos.timesquare.ccsl.IDefinition;
import fr.kairos.timesquare.ccsl.IRelation;
import fr.kairos.timesquare.ccsl.ISpecification;
import fr.kairos.timesquare.ccsl.graph.Helper;
import fr.kairos.timesquare.ccsl.simple.CopyWithReplace;
import fr.kairos.timesquare.ccsl.simple.Definition;
import fr.kairos.timesquare.ccsl.simple.Specification;

/**
 * Simplify Simple specification, mostly remove redundant inf, sup or precedences
 * 
 * @author fmallet
 *
 */
public class Simplification implements ISpecification {
	private MyGraph graph;
	private CopyWithReplace copy;
	
	private Simplification(String name, MyGraph graph) {
		copy = new CopyWithReplace(name + "_s");
		this.graph = graph;
	}

	public static Specification simplify(ISpecification spec) {
		if (spec instanceof Specification)
			return simplify((Specification)spec);
		throw new RuntimeException("Do not know how to copy a generic ISpecification");
	}
	
	public static Specification simplify(Specification spec) {
		Simplification simple = new Simplification(spec.getName(), Helper.buildCausalityGraph(spec));
		spec.visit(simple);
		
		return simple.copy.getResult();
	}
	
	private boolean hasPathTo(String source, String target) {
		AllPathsDFS dfs = new AllPathsDFS(graph, source);
		return dfs.hasPathTo(target);
	}
	/**
	 * The result may be shorter than clocks.length, if length=1, then can remove inf relation
	 * @param defClock 
	 * @param clocks
	 * @return clocks where all the names have been replaced and causality removed.
	 */
	private String[] inf(String defClock, String... clocks) {
		// start by replacing all the clocks in the input clocks
		clocks = copy.replace(clocks);
		
		int iMin = 0;
		int toBeRemoved = 0;
		for (int i = 1; i < clocks.length; i++) {
			if (hasPathTo(clocks[i], clocks[iMin])) {
				clocks[iMin] = null;
				iMin = i;
				toBeRemoved ++;
			} else if (hasPathTo(clocks[iMin], clocks[i])) {
				clocks[i] = null;
				toBeRemoved ++;
			} 
		}
		// ask to replace all instances of defClock by min in future constraints
		if (toBeRemoved == clocks.length-1)
			copy.replace(defClock, clocks[iMin]);
		return removeNull(clocks, toBeRemoved);
	}
	private String[] removeNull(String[] init, int toBeRemoved) {
		if (toBeRemoved == 0) return init;
		String[] res = new String[init.length - toBeRemoved];
		int i = 0;
		for (String s : init) {
			if (s != null) {
				res[i++] = s;
			}
		}
		return res;
	}

	private String[] sup(String defClock, String... clocks) {
		clocks = copy.replace(clocks);
		
		int iMax = 0;
		int toBeRemoved = 0;
		for (int i = 1; i < clocks.length; i++) {
			if (hasPathTo(clocks[iMax], clocks[i])) {
				clocks[iMax] = null;
				iMax = i;
				toBeRemoved++;
			} else if (hasPathTo(clocks[i], clocks[iMax])) {
				clocks[i] = null;
				toBeRemoved++;
			} 
		}
		if (toBeRemoved == clocks.length-1)
			copy.replace(defClock, clocks[iMax]);
		return removeNull(clocks, toBeRemoved);
	}

	@Override
	public void addClock(String name) {
		copy.addClock(name);
	}

	@Override
	public void add(IDefinition def) {
		if (def.getConstraintName().equals("Inf")) {
			String[] clocks = inf(def.getDefinedClock(), def.getRefClocks());
			if (clocks.length == 1) return; // no need to add a relation if only one clock
			def = new Definition(def.getDefinedClock(), "Inf", clocks);
		} else if (def.getConstraintName().equals("Sup")) {
			String[] clocks = sup(def.getDefinedClock(), def.getRefClocks());
			if (clocks.length == 1) return; // no need to add a relation if only one clock
			def = new Definition(def.getDefinedClock(), "Sup", clocks);			
		}
		
		copy.add(def);
	}
	@Override
	public void add(IRelation relation) {
		copy.add(relation);
	}

	@Override
	public boolean isConstraintSupported(String name) {
		return true;
	}
}
