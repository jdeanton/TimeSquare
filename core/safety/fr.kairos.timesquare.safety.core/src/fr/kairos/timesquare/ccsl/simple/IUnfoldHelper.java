/*******************************************************************************
 * Copyright (c) 2017,2018 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * I3S laboratory and INRIA Kairos – initial API and implementation
 *******************************************************************************/
package fr.kairos.timesquare.ccsl.simple;

import java.util.Arrays;

/**
 * Helper to expand n-ary expression as a conjunction of binary ones.
 * 
 * @author fmallet
 *
 */
public interface IUnfoldHelper {
	
	void binary(String def, String clock1, String clock2);

	default void unfold(String def, String...clocks) {
		String right = clocks[1];
		if (clocks.length > 2) {
			right = def + "_i";
			String[] newClocks = Arrays.copyOfRange(clocks, 1, clocks.length);
			unfold(right, newClocks);
		}
		binary(def, clocks[0], right);
	}
}
