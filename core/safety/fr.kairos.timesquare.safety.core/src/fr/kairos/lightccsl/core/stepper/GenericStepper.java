package fr.kairos.lightccsl.core.stepper;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import fr.kairos.common.IFactory;

/**
 * Tries to hava a generic implementation of a stepper
 * @author fmallet
 *
 */
public class GenericStepper implements IStepper, IClockManager, INameToIntegerMapper {
	private int current = 0;
	// someone who knows all the clock and put them in order
	private ClockCollector collector;
	private IFactory<ISolutionSet> solutionBuilder;

	/**
	 * Needs at least someone that knows how to build a solution
	 * May also provide an external clock builder
	 * 
	 * @param solutionBuilder
	 * @param externalClockBuilder
	 */
	public GenericStepper(IFactory<ISolutionSet> solutionBuilder, IClockBuilder externalClockBuilder) {
		this.collector = new ClockCollector(externalClockBuilder);
		this.solutionBuilder = solutionBuilder;
	}

	@Override
	public IClock clock(String clock) {
		return collector.nameToClock(clock);
	}

	@Override
	public int getIdFromName(String name) {
		return collector.getIdFromName(name);
	}
	@Override
	public String getNameFromId(int id) {
		return collector.getNameFromId(id);
	}
	@Override
	public int size() {
		return collector.size();
	}
	private void tick(Scanner sc, IStep step) throws IOException {
		step.tick();

		boolean[] ticks = new boolean[size()];

		int nbMust = 0;

		// force tick on must clock
		for (int clockId : new StepIterableFilter(ClockStatus.Must, step)) {
			idToClock(clockId).tick(current);
			ticks[clockId] = true;
			nbMust++;
		}
		// force tick on may clock if user say 1
		for (int clockId : new StepIterableFilter(ClockStatus.May, step)) {
			if (sc != null && sc.hasNextInt()) { // sc != null only in interactive mode
				if (sc.nextInt() == 1) {
					idToClock(clockId).tick(current);
					ticks[clockId] = true;
				} else {
					idToClock(clockId).ghost(current);
				}
			} else if (nbMust == 0) { // if not interactive and no MUST, then force all MAY
				idToClock(clockId).tick(current);
				ticks[clockId] = true;
			} else {
				idToClock(clockId).ghost(current);
				break;
			}
		}

		for (int clockId : new StepIterableFilter(ClockStatus.Undetermined, step)) {
			ClockStatus stat = step.status(clockId, ticks);
			if (stat == ClockStatus.Must) {
				idToClock(clockId).tick(current);
				ticks[clockId] = true;
			}
		}
		current++;
	}
	private IClock idToClock(int id) {
		return collector.idToClock(id);
	}
	private int getMaxLength() {
		int max = 10;
		for (int id = 0; id < collector.size(); id++) {
			String name = collector.getNameFromId(id);
			if (name.length() > max) max = name.length();
		}
		return max;
	}
	private static String align(String s, int length) {
		while (s.length() < length) s = " " + s;
		return s;
	}
	void printTrace(int nbStep) {
		int maxLength = getMaxLength();
		System.out.printf("%s | ", align("clock", maxLength));
		for (int i = 0; i < nbStep; i ++) {
			System.out.printf("%3d", i);
		}
		System.out.println();
		int id = 0;
		for (IClock c : collector) {
			System.out.printf("%s | ", align(collector.getNameFromId(id), maxLength));
			System.out.println(c.toString());
			id++;
		}
	}

	private List<? extends IStep> printAllSolutions(ISolutionSet boolSpec) throws Exception {
		List<? extends IStep> solutions = boolSpec.allSolutions();

		boolean unsat = true;
		int nb = 1;
		for (IStep solution : solutions) {
			unsat = false;

			System.out.printf("%2d:", nb++);

			int i = 0;
			for (@SuppressWarnings("unused") IClock clock : collector) {
				switch(solution.status(i)) {
				case Cannot:
					System.out.print(" !");
					break;
				case Must:
					System.out.print("  ");
					break;
				case May:
					System.out.print(" ?");
					break;
				case Undetermined:
					i++;
					continue; // this is not a choice to be done					
				}
				System.out.print(collector.getNameFromId(i));
				i++;
			}
			System.out.println();
		} 
		if (unsat) {
			System.out.println("UNSAT");			
		}

		return solutions;
	}

	/**
	 * Picks randomly one solution using OneSAT
	 * @param debug if true, prints all solutions at each step
	 */
	void step(boolean debug) throws Exception {
		ISolutionSet solutions = solutionBuilder.build();

		// needs to pick one solution before the iteration on all solutions
		// since the iteration is destructive
		IStep sol = solutions.pickOneSolution();

		if (debug) {
			System.out.println(current+": ");
			printAllSolutions(solutions);
		}

		// propagates the solution on the history
		tick(null, sol);
	}

	/**
	 * step nbStep by picking randomly one solution at each step (OneSAT)
	 * @param nbStep
	 * @param debug if true, displays all the possible solutions at each step
	 */
	public void stepAndPrint(int nbStep, boolean debug) throws Exception {
		for (int i = 0; i < nbStep; i++) {
			step(debug);
		}
		printTrace(nbStep);
	}

	/**
	 * Ask user to pick one step
	 * @param nbStep
	 * @param debug if true, displays all the possible solutions at each step
	 */
	public int interactiveStep() throws Exception {
		int nbStep = 0;

		Scanner sc = new Scanner(System.in);
		sc.useDelimiter("\\s*");
		do {
			ISolutionSet solver = solutionBuilder.build();

			System.out.println(" 0: stop");

			List<? extends IStep> solutions = printAllSolutions(solver);

			System.out.println(solutions.size() + " solutions. pick one ?");

			try {
				String line = sc.nextLine();
				Scanner sLine = new Scanner(line);
				int v = sLine.nextInt();
				if (v<1 || v > solutions.size()) break;

				IStep solution = solutions.get(v - 1);

				nbStep++;

				tick(sLine, solution);
			} catch(NoSuchElementException nse) {
				System.err.println("No Such Element => exit");
				break;
			}
		} while (true);

		sc.close();

		printTrace(nbStep);

		return nbStep;
	}

	@Override
	public Iterable<String> getClockNames() {
		return this.collector.getClockNames();
	}
}
