package fr.kairos.lightccsl.core.stepper;

/**
 * 
 * @author fmallet
 * 
 */
public interface IStep {
	/**
	 * @param pos number of clock in the system
	 * @param vals sometimes the status may depend on the value of other clocks, which are given in val
	 * @return status (Must, May, Cannot) of clock number 'pos'
	 */
	ClockStatus status(int pos, boolean...vals);
	
	/**
	 * @return number of clocks in this step
	 */
	int size();

	/**
	 * Force the all the must clock to tick
	 */
	void tick();
}