package fr.kairos.lightccsl.core.stepper;

import fr.kairos.timesquare.ccsl.simple.ISpecificationBuilder;

public interface ISpecificationToSolution {
	/**
	 * Needs to acces the specification and needs to know the clock builder if required 
	 * intermediate clock creation
	 * 
	 * @param clockbuilder
	 * @return
	 */
	ISolutionSet buildSolutionsFor(ISpecificationBuilder builder, 
			INameToIntegerMapper mapper,
			IClockManager clockManager);
}
