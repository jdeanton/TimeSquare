package fr.kairos.lightccsl.core.stepper;

import fr.kairos.common.IFactory;
import fr.kairos.timesquare.ccsl.simple.AUtility;
import fr.kairos.timesquare.ccsl.simple.ISpecificationBuilder;
import fr.kairos.timesquare.ccsl.simple.IUtility;
/**
 * An abstract utility that allows interactive or nbStep execution, with or without debug, and with a backend
 * 
 * @author fmallet
 *
 */
public class StepperUtility extends AUtility implements IUtility {
	public static String NB_STEPS = "NbSteps";
	public static String DEBUG = "Debug";
	public static String INTERACTIVE = "Interactive";
	
	private ISpecificationToSolution spec2sol;
	private GenericStepper stepper;
	
	// needs some who can build a solution from a specification
	public StepperUtility(ISpecificationToSolution spec2sol) {
		this.spec2sol = spec2sol;
	}
	
	class SolutionBuilder implements IFactory<ISolutionSet> {
		private ISpecificationBuilder consumer;
		public SolutionBuilder(ISpecificationBuilder consumer) {
			this.consumer = consumer;
		}

		@Override
		public ISolutionSet build() {
			return spec2sol.buildSolutionsFor(consumer, stepper, stepper);
		}		
	}
	
	private IBackend backend;
	
	public void setBackend(IBackend backend) {
		this.backend = backend;
	}
	
	@Override
	final public void treat(String name, ISpecificationBuilder consumer) {
		try {
			IClockBuilder clockBuilder = null;
			if (backend != null) clockBuilder = backend.init(this);
			
			stepper = new GenericStepper(new SolutionBuilder(consumer), clockBuilder);
			if (getBooleanParam(INTERACTIVE, true)) {
				stepper.interactiveStep();
			} else {
				stepper.stepAndPrint(getIntParam(NB_STEPS, 10), getBooleanParam(DEBUG, false));
			}
			
			if (backend != null) backend.finish(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}