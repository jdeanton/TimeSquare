package fr.kairos.lightccsl.core.stepper;

/**
 * clocks are numbered by integers starting at 1
 * 
 * @author fmallet
 */
public interface IClockManager {
	IClock clock(String clock);
}
