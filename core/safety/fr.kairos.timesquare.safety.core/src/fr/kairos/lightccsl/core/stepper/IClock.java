package fr.kairos.lightccsl.core.stepper;

/**
 * A CCSL Clock with runtime information about its ticking
 * @author fmallet
 *
 */
public interface IClock {
	int getHistory();
	int getHistory(int step);
	boolean isTicking();
	boolean wasTicking(int step);
	
	/**
	 * @param nb if 0, the previous step, if nb=1, the one before the previous ...
	 * @return the nb^th previous step at which the clock ticked.
	 */
	int previous(int nb);
	
	void tick(int current);
	void ghost(int current);
	IClock cloneClock();
}
