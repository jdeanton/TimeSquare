package fr.kairos.lightccsl.core.stepper;

import java.util.Iterator;

final class StepIterableFilter implements Iterable<Integer> {
	private IStep step;
	private ClockStatus expected;
	
	StepIterableFilter(ClockStatus expected, IStep step) {
		this.expected = expected;
		this.step = step;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new MyIterator();
	}
	
	class MyIterator implements Iterator<Integer> {
		private int pos = -1;

		MyIterator() {
			findNext();
		}
		private void findNext() {
			do {
				pos ++;
			} while (pos < step.size() && step.status(pos) != expected);			
		}
		@Override
		public boolean hasNext() {
			return pos < step.size();
		}

		@Override
		public Integer next() {
			int res = pos;
			findNext();
			return res;
		}		
	}
}
