package fr.kairos.lightccsl.core.stepper;

import java.util.List;

/**
 * Represent a set of possible solutions (represented as steps)
 * 
 * @author fmallet
 */
public interface ISolutionSet  {
	List<? extends IStep> allSolutions() throws Exception;
	IStep pickOneSolution() throws Exception;
}
