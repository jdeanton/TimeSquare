package fr.kairos.lightccsl.core.stepper;

public interface IStepper {
	int interactiveStep() throws Exception;

	void stepAndPrint(int intParam, boolean booleanParam) throws Exception;
}
