package fr.kairos.lightccsl.core.stepper;

public enum ClockStatus {
	May('?'), Must('x'), Cannot(' '),
	// undetermined is when the variable does not impact the boolean formula
	Undetermined('-'); 
	
	private char c;
	ClockStatus(char c) { this.c = c; }
	public char oneChar() { return c; }
}