package fr.kairos.lightccsl.core.stepper;

/**
 * Maps clock names to unique integer id
 * @author fmallet
 *
 */
public interface INameToIntegerMapper {
	/**
	 * May create a name if does not exist
	 * @param name
	 * @return unique id for clock "name" or -1 if not known
	 */
	int getIdFromName(String name);

	/**
	 * @return Number of Unique names known
	 */
	int size();

	/**
	 * @param id
	 * @return the name associated with the id
	 */
	String getNameFromId(int id);
	
	
	/***
	 * @return the names of all known clocks
	 */
	Iterable<String> getClockNames();
}
