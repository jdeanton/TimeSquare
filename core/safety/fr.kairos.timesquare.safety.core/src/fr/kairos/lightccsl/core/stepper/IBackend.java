package fr.kairos.lightccsl.core.stepper;

import java.io.IOException;

import fr.kairos.common.ParameterUtility;

public interface IBackend {
	/** finish what the backend if supposed to do 
	 * @throws IOException */
	void finish(String name) throws IOException;

	/** initialize backend with parameters
	 * @return clock builder*/
	IClockBuilder init(ParameterUtility utility);
}
