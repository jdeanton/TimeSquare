package fr.kairos.lightccsl.core.stepper;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Clock implementation with history for displaying a trace
 * 
 * @author fmallet
 *
 */
public class Clock implements IClock {
	private int history = 0;
	private LinkedList<Integer> trace = new LinkedList<>();
	private LinkedList<Integer> ghosts = new LinkedList<>();
	private int lastCurrent = 0;

	@Override
	public Clock cloneClock() {
		Clock res = new Clock();
		res.history = history;
		res.lastCurrent = lastCurrent;
		for (int i : trace) res.trace.add(i);
		for (int i : ghosts) res.ghosts.add(i);
		return res;
	}
	
	/**
	 * @param step should be >= 0
	 * @return number of ticks upto step (not included)
	 */
	@Override
	public int getHistory(int step) {
		int tick = 0;
		for (int i : trace) {
			if (i < step) tick++;
			else return tick;
		}
		return tick;
	}
	@Override
	public int getHistory() {
		return history;
	}
	@Override
	public boolean isTicking() {
		return !trace.isEmpty() && trace.getLast() == lastCurrent;
	}
	@Override
	public boolean wasTicking(int step) {
		return trace.contains(step);
	}
	@Override
	public int previous(int nb) {
		ListIterator<Integer> it  = trace.listIterator(trace.size());
		for (int i = 0; i < nb; i++) {
			if (!it.hasPrevious()) {
				return -1;
			}
			it.previous();
		}
		if (it.hasPrevious())
			return it.previous();
		return -1;
	}

	@Override
	public void tick(int current) {
		history++;
		trace.add(current);
		lastCurrent = current;
	}

	public CharSequence toString(int nbStep) {
		StringBuilder sb = new StringBuilder();
		int last = -1;
		for (int i : trace) {
			if (i > nbStep) break;
			for (int j = last + 1; j < i; j++) 
				sb.append("   ");
			sb.append("  x");
			last = i;
		}
		for (int j = last + 1; j < nbStep; j++) 
			sb.append("   ");
		return sb;
	}
	private CharSequence stringWithGhosts(int nbStep) {
		StringBuilder sb = new StringBuilder();
		int current = 0;
		Iterator<Integer> itTrace = trace.iterator();
		Iterator<Integer> itGhost = ghosts.iterator();
		int nextTrace = itTrace.hasNext()?itTrace.next():(nbStep+1);
		int nextGhost = itGhost.hasNext()?itGhost.next():(nbStep+1);
		do {
			String str;
			int next;
			if (nextTrace < nextGhost) {
				str = "  x";
				next = nextTrace;
				nextTrace = itTrace.hasNext()?itTrace.next():(nbStep+1);
			} else {
				str = "  -";
				next = nextGhost;
				nextGhost = itGhost.hasNext()?itGhost.next():(nbStep+1);
			}
			while(current < next) {
				sb.append("   ");
				current++;
			}
			sb.append(str);
			current++;
			if (current > nbStep) break;
		} while (true);

		return sb;
	}
	@Override
	public String toString() {
		return stringWithGhosts(lastCurrent).toString();
	}
	@Override
	public void ghost(int current) {
		ghosts.add(current);
		lastCurrent = current;
	}
}
