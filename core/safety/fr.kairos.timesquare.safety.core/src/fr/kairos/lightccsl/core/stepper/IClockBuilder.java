package fr.kairos.lightccsl.core.stepper;

public interface IClockBuilder {
	IClock buildClock(String name);
}
