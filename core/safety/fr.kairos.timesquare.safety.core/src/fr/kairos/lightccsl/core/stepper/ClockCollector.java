package fr.kairos.lightccsl.core.stepper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

final public class ClockCollector implements Iterable<IClock>, INameToIntegerMapper, IClockManager {
	private HashMap<String, Integer> namesToId;
	private ArrayList<IClock> clocks = new ArrayList<>();
	private ArrayList<String> names;
	
	// needs to delegate the creation of clocks 
	private IClockBuilder clockBuilder;
	
	public ClockCollector(IClockBuilder clockBuilder) {
		this(new HashMap<>(), new ArrayList<>(), clockBuilder);
	}
	private ClockCollector(HashMap<String,Integer> namesToId, ArrayList<String> names, IClockBuilder clockBuilder) {
		super();
		this.namesToId = namesToId;
		this.names = names;
		this.clockBuilder = clockBuilder;
	}
	public ClockCollector cloneClock() {
		ClockCollector res = new ClockCollector(namesToId, names, clockBuilder);
		for(IClock clock : clocks) {
			res.clocks.add(clock.cloneClock());
		}
		return res;
	}
	private int addClock(String name) {
		Integer i = namesToId.get(name);
		if (i == null) {
			namesToId.put(name, i = names.size());
			if (this.clockBuilder != null) {
				clocks.add(this.clockBuilder.buildClock(name));
			} else clocks.add(new Clock()); // use a dummy clock
			names.add(name);
		}  
		return i;
	}

	@Override
	public Iterator<IClock> iterator() {
		return clocks.iterator();
	}

	@Override
	public int getIdFromName(String name) {
		return addClock(name); // builds the clock if does not exist
//		Integer res = namesToId.get(name);
//		if (res == null) return -1;
//		return res;
	}
	
	IClock nameToClock(String name) {
		return idToClock(addClock(name)); // will build the clock if it does not exist
//		if (getIdFromName(name) == -1) return null;
//		return idToClock(namesToId.get(name));
	}

	IClock idToClock(int id) {
		return clocks.get(id);
	}

	@Override
	public String getNameFromId(int id) {
		return names.get(id);
	}
	
	@Override
	public int size() {
		return names.size();
	}

	@Override
	public Iterable<String> getClockNames() {
		return names;
	}

	@Override
	public IClock clock(String clockName) {
		return nameToClock(clockName);
	}
}
