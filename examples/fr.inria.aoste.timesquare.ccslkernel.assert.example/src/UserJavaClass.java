/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/


import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.eclipse.core.runtime.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

/**
 * This class demonstrates JFace's ErrorDialog class
 */
public class UserJavaClass {
  

  /**
   * Runs the application
   */
  public void run() {
   String message = "\"the assertion is violated !";
	    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
	        JOptionPane.ERROR_MESSAGE);
  }



  /**
   * The application entry point
   * 
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    new UserJavaClass().run();
  }
}
