/*
 * CCSL specification with two task. 
 * 		task2 is periodic, period 11 offset 0 and the execution time is 5 CPU ticks
 * 		task1 is periodic, period 4, the execution time is 2 CPU ticks and the offset is given by a clock. Here this clock is the end of the first task + 22ms
 * 		the tasks are scheduled on one CPU and can be preempted. The CPU frequence is 1/10.10e-3= 100hertz
 * 		Note: this specification is synchronous but the CPU is choose arbitrarly. It can be left free. Moreover, here a chronometric clock is used (ms) but the scheduling can be done with pure logical time
 * @author:  jdeanton
 * date :  Sun August 28 2011  08:40:12 CEST 
 */
ClockConstraintSystem MySpec {
    imports {
       //import
		import "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib" as lib0; 
		import "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib" as lib1; 
		import "CCSL4RT.ccslLib" as lib2; 

    }
    entryBlock main 
     
        Block main {
        	//task1
        	Clock ms
        	Clock task1Start ->eventStartT1():start
        	Clock task1Stop ->eventStopT1():stop
        	Clock TimeRefTask1
        	Integer et1=2
         	Integer period1=4
        	Relation Task1[PeriodicTaskOffsetClock] ( PeriodicTaskOffsetClock_start-> task1Start,
        										PeriodicTaskOffsetClock_stop-> task1Stop,
        										PeriodicTaskOffsetClock_ref4Period -> CPU,
        										PeriodicTaskOffsetClock_ref4et-> TimeRefTask1,
        										PeriodicTaskOffsetClock_et-> et1,
        										PeriodicTaskOffsetClock_period-> period1,
        										PeriodicTaskOffsetClock_offsetClock-> theOffsetClock
        	)
        	
        	
        	
        	Integer offset1=22
			Expression theOffsetClock=DelayFor( DelayForClockToDelay-> task2Stop 	,DelayForClockForCounting-> ms 	,DelayForDelay-> offset1  )


			//	task2
        	Clock task2Start ->eventStartT2():start
        	Clock task2Stop ->eventStopT2():stop
        	Clock TimeRefTask2

        	Integer et2=5
        	Integer period2=11
        	Integer offset2=0
        	Relation Task2[PeriodicTask](  PeriodicTask_start -> task2Start,
        										PeriodicTask_stop -> task2Stop,
        										PeriodicTask_ref4Period -> CPU,
        										PeriodicTask_ref4et -> TimeRefTask2,
        										PeriodicTask_et -> et2,
        										PeriodicTask_period -> period2,
        										PeriodicTask_offset -> offset2
        	)
        	Integer deadline=12
        	Relation enforceNoDeadlineT2[Deadline](DeadlineStart-> task2Start ,	DeadlineEnd-> task2Stop,	DeadlineBase-> CPU ,	DeadlineValue-> deadline)


			// CPU
	 		
			Expression Task1and2=Union( Clock1-> TimeRefTask1,Clock2-> TimeRefTask2 )
			Clock CPUSleep
			Expression CPU=Union( Clock1-> CPUSleep 	,Clock2-> Task1and2  )
			Integer CPUspeed=10
			Expression TenMs=Periodic( PeriodicBaseClock-> ms 	,PeriodicPeriod->CPUspeed  	,PeriodicOffset->zero)
			Relation relation_0[Coincides](   Clock2-> CPU ,	Clock1-> TenMs  )

			Relation Task1ExcludesTask2[Exclusion](Clock2-> TimeRefTask1 ,	Clock1-> TimeRefTask2 )
			Relation CPUsleepsOrWorks[Exclusion](Clock2-> CPUSleep, Clock1-> Task1and2)
			
	}
}
