/*
 * CCSL kernel Library
 * @author: Julien Deantoni
 * date : Sun Jan 31 2011
 */
 
 
Library CCSL{ 

	imports{ 
		import "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib" as kernel;
	}
	
 
		ExpressionLibrary CCSL_Expressions{
			//for general use
			Integer minusOne:int=-1
			Integer one:int=1
			Integer two:int=2
			Integer three:int=3
			Integer four:int=4
			Integer five:int=5
			Integer six:int=6
			Integer seven:int=7
			Integer eight:int=8
			Integer nine:int=9
			Integer ten:int=10
			
			//used by Periodic --> 1(Period)
			Sequence seqForPeriodic:IntegerSequence = 
					uno=1
					( IntegerVariableRef ref2ByFirstSize [ PeriodicPeriod ] )
			
			//used by alternates --> (1)
			Sequence seqOneInfinite:IntegerSequence =
				(one=1) 
				
			//used by DelayFor --> (Delay)
			Sequence seqForDelayFor:IntegerSequence = 
					( IntegerVariableRef ref2DelayForDelay [ DelayForDelay ] )
			
				ExpressionDeclaration PeriodicOffsetP(PeriodicOffsetPBaseClock:clock, PeriodicOffsetPPeriod:int):clock
				ExpressionDeclaration Periodic(PeriodicBaseClock:clock, PeriodicPeriod:int, PeriodicOffset:int):clock
				ExpressionDeclaration FilterBy(FilterByClock:clock, FilterBySeq:IntegerSequence):clock
				ExpressionDeclaration DelayFor(DelayForClockToDelay:clock, DelayForClockForCounting:clock, DelayForDelay:int):clock
				ExpressionDeclaration OneTickAndDie(OneTickAndDieClock:clock):clock
				
				
				ExpressionDefinition OneTickAndDieDef [OneTickAndDie]{
					root=OneTickAndDie_waitOne 
					Expression OneTickAndDie_waitOne = Wait(
									WaitingClock -> OneTickAndDieClock,
									WaitingValue -> one
								)
				}
				
				ExpressionDefinition DelayForDef [DelayFor]{
					root = DelayFor_defer
					Expression DelayFor_defer = Defer
						(BaseClock -> DelayForClockToDelay,
						 DelayClock -> DelayForClockForCounting,
						 DelayPatternExpression -> seqForDelayFor)  
				}

		
				ExpressionDefinition PeriodicOffsetPDef [PeriodicOffsetP]{
					root=PeriodicOffsetPDef_concatOffsetPeriod
					Expression PeriodicOffsetPDef_concatOffsetPeriod = Concatenation
						( LeftClock -> PeriodicOffsetPDef_waitPeriod, 
						 RightClock-> PeriodicOffsetPDef_concatOffsetPeriod )  

						
					Expression PeriodicOffsetPDef_waitPeriod = Wait 
						(WaitingClock -> PeriodicOffsetPBaseClock,
						 WaitingValue -> PeriodicOffsetPPeriod)  
						
				}
				
				ExpressionDefinition PeriodicDef[Periodic]{
					root = PeriodicDef_concatOffsetPeriod						
						Expression PeriodicDef_concatOffsetPeriod =Concatenation
							(LeftClock -> PeriodicDef_waitOffset,
							RightClock -> PeriodicDef_periodicOffsetP)
						
						IntPlus plusOne(IntegerVariableRef[PeriodicOffset], IntegerRef[one])
						
						Expression PeriodicDef_waitOffset =Wait
							(WaitingClock -> PeriodicBaseClock,
							WaitingValue -> plusOne)
							
						
						
						Expression PeriodicDef_periodicOffsetP =PeriodicOffsetP
							(PeriodicOffsetPBaseClock -> PeriodicBaseClock,
							PeriodicOffsetPPeriod -> PeriodicPeriod)
	
				}
				
				//FilterBy
				ConditionalExpressionDefinition FilterByDef[FilterBy]{
				
						Expression FilterBy_waitHead = Wait
							(WaitingClock -> FilterByClock,
							WaitingValue -> SeqHead)
							 
						
						Expression FilterBy_FilterBySeqTail = FilterBy
							(FilterByClock -> FilterByClock,
							FilterBySeq -> SeqTail)
							
						
					
					
						SeqGetHead SeqHead (SeqVarRef[FilterBySeq])
						SeqGetTail SeqTail (SeqVarRef[FilterBySeq])
				

					switch{
						case SeqIsEmpty(SeqVarRef[FilterBySeq])
							:	Expression FilterBy_death = Death()
						case Not(SeqIsEmpty(SeqVarRef[FilterBySeq]))
							:Expression FilterBy_concatWait = Concatenation
									(LeftClock -> FilterBy_waitHead,
									RightClock -> FilterBy_FilterBySeqTail)
								
						}
						default Expression killIt = Death()

					}
				}
			
			


	RelationLibrary CCSL_Relations{
		
		
			RelationDeclaration Alternates(AlternatesLeftClock:clock, AlternatesRightClock:clock)
			RelationDeclaration Synchronizes(SynchronizesLeftClock:clock, SynchronizesRightClock:clock)
			
			RelationDeclaration testConditional(testConditionalLeftClock:clock, testConditionalRightClock:clock, testConditionalParam:int)
			
			ConditionalRelationDefinition testConditionalDef[testConditional]{
				switch{
					case IntSup{name testIfSupThanOne 
						leftValue IntegerVariableRef[testConditionalParam]
						rightValue IntegerRef[one]
					}: Relation relation_1[Coincides](   Clock2-> testConditionalLeftClock , Clock1-> testConditionalRightClock  )

				}
				default Relation relation_0[Exclusion](   Clock2-> testConditionalLeftClock , Clock1-> testConditionalRightClock  )

			}
			
			RelationDefinition AlternatesDef[Alternates]{
				//c1 < c2
				Relation Alt_c1PrecedesC2[Precedes]
					(LeftClock -> AlternatesLeftClock
					,RightClock -> AlternatesRightClock)
				
				
				// c1 delayedBy (1)
				Expression Alt_c1DelayedByOne=Defer
					(BaseClock -> AlternatesLeftClock,
					DelayClock -> AlternatesLeftClock,
					DelayPatternExpression -> seqOneInfinite)
				
				
				//c2 < (c1 delayedBy (1))
				Relation Alt_c2precedesC1DelayedByOne[Precedes]
					(LeftClock -> AlternatesRightClock,
					RightClock -> Alt_c1DelayedByOne)
				
										
			}
			
			RelationDefinition SynchronizesDef[Synchronizes]{
				//c1 < c2delayedBy1
				Relation Synch_c1PrecedesC2delayedFor1[Precedes]
					(LeftClock -> SynchronizesLeftClock
					,RightClock -> Synch_c2DelayedByOne)
				
				
				// c1 delayedBy (1)
				Expression Synch_c1DelayedByOne=Defer
					(BaseClock -> SynchronizesLeftClock,
					DelayClock -> SynchronizesLeftClock,
					DelayPatternExpression -> seqOneInfinite)
				
				// 21 delayedBy (1)
				Expression Synch_c2DelayedByOne=Defer
					(BaseClock -> SynchronizesRightClock,
					DelayClock -> SynchronizesRightClock,
					DelayPatternExpression -> seqOneInfinite)

				
				//c2 < (c1 delayedBy (1))
				Relation Synch_c2precedesC1DelayedByOne[Precedes]
					(LeftClock -> SynchronizesRightClock,
					RightClock -> Synch_c1DelayedByOne)
				
										
			}
		
				

	
	}

}