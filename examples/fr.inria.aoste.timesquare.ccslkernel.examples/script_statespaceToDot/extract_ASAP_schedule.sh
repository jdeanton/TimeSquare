#!/bin/sh
#*******************************************************************************
# Copyright (c) 2017 I3S laboratory, INRIA and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     I3S laboratory and INRIA Kairos - initial API and implementation
#*******************************************************************************


keepLonger(){
  longest=""
  maxSize=0
  while read line
  do
    lineSize=$(echo $line | sed -e 's/[^,]*,*/x/g' | wc -c)
    if test $lineSize -gt $maxSize
    then
      maxSize=$lineSize
      longest=$line
    fi
  done
  echo $longest
}
  

extract(){
  
  nextSource=$(cat $1 | read line | cut -d ' ' -f1) 
  while read line
  do
      longest=$(grep -e "^$nextSource " $1 | keepLonger)
      nextSource=$(echo $longest | cut -d ' ' -f2)
      
  done < $1
 }


if test $# -ne 2
then
  echo "usage: ./extractASAP_schedule sourceCipFile resCipFile"
else
  extract $1 > $2
fi