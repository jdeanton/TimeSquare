#!/bin/sh
#*******************************************************************************
# Copyright (c) 2017 I3S laboratory, INRIA and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     I3S laboratory and INRIA Kairos - initial API and implementation
#*******************************************************************************


if test $# -ne 1
then
  echo "usage: ./processDotfile.sh sourceDotFile"
else
  ./ciprianize.sh $1 $1.cip
  ./extract_ASAP_schedule.sh $1.cip $1.asap.cip
  ./cleanUpDot.sh $1 $1.clean.dot 
  sfdp -Tpng $1.clean.dot > $1.clean.png
fi