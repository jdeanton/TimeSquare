#!/bin/sh
#*******************************************************************************
# Copyright (c) 2017 I3S laboratory, INRIA and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     I3S laboratory and INRIA Kairos - initial API and implementation
#*******************************************************************************

getLonger(){
  longest=""
  maxSize=0

  while read line
  do
    lineSize=$(echo $line | sed -e 's/[^,]*,*/x/g' | wc -c)
    if test $lineSize -gt $maxSize
    then
      maxSize=$lineSize
      longest=$line
    fi
  done
  echo $longest
}

extract(){

  nextSource=0
  max_State=$(cat $1 | read line | cut -d ' ' -f1) 
  while read line
  do
      if echo $line | grep -q -e "^$nextSource ->" 
      then
	longest=$(grep -e "^	$nextSource ->" $1 |getLonger)
	echo " edge [color=red];"
	echo "$longest" | sed -e 's/color="#aaaaaa", size="1"/color="#ff0000",style=bold, arrowsize=0.1, size="4"/'
	nextSource=$(echo $longest | cut -d ' ' -f3)
      fi

  echo $line
      
  done <$1
 }


cleanUp(){
  while read line
  do
      echo $line | sed -e 's/shape="circle"/shape="point"/'| sed -e 's/size="10",//'| sed -e 's/, label=".*"//' | sed -e 's/color="#aaaaaa"/color="#cccccc",arrowsize=0.1,  size="0.1"/'
  done 
}

removeDouble(){
  deb="impossibleInaDotFile"
  while read line
  do
    if echo $line | grep -q -e "^$deb" 
    then
      echo ""
    else
      echo $line
      if echo $line | grep -q -e "->.*ff0000"
      then
	deb=$(echo $line |cut -d ' ' -f1,2,3)
      fi
    fi
  done 
  echo "}"
}

if test $# -ne 2
then
  echo "usage: ./cleanUpDot.sh sourceDotFile targetDotFile"
else
  extract $1  |cleanUp | removeDouble > $2
fi