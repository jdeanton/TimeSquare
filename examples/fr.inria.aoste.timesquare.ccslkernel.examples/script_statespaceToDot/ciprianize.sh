#!/bin/sh
#*******************************************************************************
# Copyright (c) 2017 I3S laboratory, INRIA and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     I3S laboratory and INRIA Kairos - initial API and implementation
#*******************************************************************************


remove_label(){
  while read line
  do
    echo $line | grep -e '->' |  sed -e 's/\([0-9]*\) -> \([0-9]*\) .*/\1 \2/'
  done < $1
}
  

add_nb_state(){
  max=0
  nb_trans=0
  while read line
  do
    nb_trans=$(expr $nb_trans + 1)
    un=$(echo $line | cut -d ' ' -f1)
    deux=$(echo $line | cut -d ' ' -f2)
    if test $un -ge $deux
    then
      if test $un -ge $max
      then
	max=$un
      fi
    else
      if test $deux -ge $max
      then
	max=$deux
      fi
    fi 
  done < $1
  echo $max $nb_trans
}

createRes(){
  echo $2 > $3
  cat $1 >> $3
  rm $1
}






if test $# -ne 2
then
  echo "usage: ./ciprianize sourceDotFile targetCipFile"
else
  remove_label $1 > file.tmp
  nbState_nbTrans=$(add_nb_state file.tmp)
  createRes file.tmp "$nbState_nbTrans" $2
fi