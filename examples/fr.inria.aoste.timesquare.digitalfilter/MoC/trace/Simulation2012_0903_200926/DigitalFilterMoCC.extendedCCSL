/*
 * CCSL specification for a digital filter
 * @author: Julien Deantoni
 * date : Friday 26th of august 2011
 */
ClockConstraintSystem  MySpec4EastADL_like {
	imports {
		import "ccsl:kernel" as kernelLib ; 
		import "ccsl:lib" as CCSL;
		import "model.uml" as theModel;
	} 
	entryBlock main
	
	
		Block main {
		Expression firstStart=FilterBy( FilterByClock-> filterStart, FilterBySeq-> s1)
		//the first ready is caused by the first start
			Expression firstReady=FilterBy( FilterByClock-> filterReady, FilterBySeq-> s1)
		Expression filterDataOutBy4Begin=FilterBy(FilterByClock-> filterDataOut, FilterBySeq-> by4begin)
		Expression sixteenDataOut=Periodic(PeriodicBaseClock-> filterDataOut ,PeriodicPeriod-> lineSize ,PeriodicOffset-> lineSize)
		Expression filterDataOutBy4end=FilterBy(FilterByClock-> filterDataOut, FilterBySeq-> by4)
		//wait ready before to receive data (answer response protocol)
			Relation answerResponseCom[Alternates](AlternatesLeftClock-> filterReady, AlternatesRightClock-> filterDataIn)
		Relation filterStartIsUnic[Coincides](Clock2-> filterStart, Clock1-> firstStart)
		Relation filterStartCausesFilterReady[Causes](LeftClock-> firstStart,RightClock-> firstReady)
		Relation dataInCausesAsequenceOf4dataOut[Causes](LeftClock-> filterDataIn ,RightClock-> filterDataOutBy4Begin)
			//add temporal information on causality
			Relation relation_0[Precedes](LeftClock->filterDataIn ,	RightClock-> filterDataOutBy4Begin)
		Relation eolInBetweenSixteenDataOut[Alternates](AlternatesLeftClock-> sixteenDataOut,AlternatesRightClock-> filterEol)
		// it is a non reentrant memory
			Relation NonRentrantMemory[Alternates](AlternatesLeftClock-> memoryGet, AlternatesRightClock-> memoryData)
			// the memory starts as soon as a get is consummed and stops when a data is produced
			Relation GetEquStart[Coincides](Clock2-> memoryGet ,	Clock1-> memoryStart)
			Relation DataEquStop[Coincides](Clock2-> memoryStop , Clock1-> memoryData)
		Relation videoStartsAsSoonAsData[Coincides](Clock2-> videoDeviceData, Clock1-> videoDeviceStart)
			//finish the printing before the new dataIn and no data squeezed
			Relation videoFinishBeforeNewData[Alternates](AlternatesLeftClock-> videoDeviceStart, AlternatesRightClock-> videoDeviceStop)

			 
		//connectors
			//all communications are here considered as instantaneous
			Relation connector1[Coincides](Clock2-> filterReady, Clock1-> memoryGet)
			Relation connector2[Alternates](AlternatesRightClock-> filterDataIn, AlternatesLeftClock-> memoryData)
			Relation connector3[Coincides](Clock2-> filterEol,	Clock1-> videoDeviceCR)
			Relation connector4[Alternates](AlternatesLeftClock-> filterDataOut, AlternatesRightClock-> videoDeviceData)
		Relation AvoidLostInInputOfVideoDevice[Alternates](AlternatesLeftClock-> filterDataIn, AlternatesRightClock-> filterDataOutBy4end)

		
		
		//wrong assertions as a demo !
			assert Relation assertConnector1[Precedes](LeftClock-> filterReady, RightClock-> memoryGet)
		//part concerning the filter
			Clock filterStart -> filterS("theModel->Model::MyToyDigitalSystem::itsFilter"):start
			Clock filterDataOut -> filterDataOutP("theModel->Model::Filter::data_out"):produce
			Clock filterEol -> filterEolP("theModel->Model::Filter::eol"):produce
			Clock filterReady -> filterReadyP("theModel->Model::Filter::ready"):produce
			Clock filterDataIn -> filterDataInC("theModel->Model::Filter::data_in"):consume
		// OnlyOneStart 
			Sequence s1=1(0)
		//4 output data are produced for each input data and no output before the first data
			Sequence by4begin=1(4)
		//the eol is produced each 16 data out
			Integer lineSize=16
		//part concerning the Memory
			Clock memoryStart -> memoryS("theModel->Model::MyToyDigitalSystem::itsMemory"):start
			Clock memoryStop -> memoryF("theModel->Model::MyToyDigitalSystem::itsMemory"):finish
			Clock memoryGet -> memoryGetC("theModel->Model::Memory::get"):consume
			Clock memoryData -> memoryDataP("theModel->Model::Memory::data"):produce
		//part concerning the video device
			Clock videoDeviceStart -> videoDeviceS("theModel->Model::MyToyDigitalSystem::itsVideoDevice"):start
			Clock videoDeviceStop -> videoDeviceF("theModel->Model::MyToyDigitalSystem::itsVideoDevice"):finish
			Clock videoDeviceData -> videoDeviceDataC("theModel->Model::VideoDevice::data"):consume
			Clock videoDeviceCR -> videoDeviceCRC("theModel->Model::VideoDevice::CR"):consume
		//restrict the number of "data in" in the Filter to be one (an alternates is a precedes bounded by 1...)
			Sequence by4=(4)
	}
		
		
	
}