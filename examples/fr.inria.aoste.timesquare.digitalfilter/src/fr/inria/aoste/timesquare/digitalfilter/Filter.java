/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.digitalfilter;
import java.util.ArrayList;

import org.eclipse.swt.graphics.Color;

import fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionHelper;
import fr.inria.aoste.timesquare.backend.codeexecution.ICodeExecutionAPI;


public class Filter  implements ICodeExecutionAPI { 
	
	
	private CodeExecutionHelper _ce=null;
	static ArrayList<Color> dataIn=new ArrayList<Color>();
	static int nb=0;
	@Override
	 public void setHelper(CodeExecutionHelper ce) {
		_ce=ce;
	 }
	
	 public void dataIn(){
		 Color lastColor = (Color) _ce.getData("memoryData");
		 dataIn.add(lastColor);
	 }
	 public void dataOut(){
		 int red = dataIn.get(dataIn.size()-1).getRed();
		 int green = dataIn.get(dataIn.size()-1).getGreen();
		 int blue = dataIn.get(dataIn.size()-1).getBlue();
		 Color newColor=null;
		 switch (nb) {
			case 0:
				newColor=new Color(null, Math.abs(Math.min(red-30, 255)), Math.abs(Math.min(green-30,255)), Math.abs(Math.min(blue-30,255)));
				break;
			case 1:
				newColor=new Color(null, Math.abs(Math.min(red-10, 255)), Math.abs(Math.min(green-10,255)), Math.abs(Math.min(blue-10,255)));
				break;
			case 2:
				newColor=new Color(null, Math.abs(Math.min(red+10, 255)), Math.abs(Math.min(green+10,255)), Math.abs(Math.min(blue+10,255)));
				break;
			case 3:
				newColor=new Color(null, Math.abs(Math.min(red+30, 255)), Math.abs(Math.min(green+30,255)), Math.abs(Math.min(blue+30,255)));
				break;
			default:
				break;
		 }
		 nb=(nb+1)%4;
		 _ce.putData("Color", newColor);
	 }

	@Override
	public void finish() {
		if ( dataIn!=null)
			dataIn.clear();
		dataIn=null;
		
	}
	
}
