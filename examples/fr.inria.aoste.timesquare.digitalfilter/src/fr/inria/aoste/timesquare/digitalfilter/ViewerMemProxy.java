/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.digitalfilter;
import org.eclipse.swt.widgets.Display;

import fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionHelper;
import fr.inria.aoste.timesquare.backend.codeexecution.ICodeExecutionAPI;

public class ViewerMemProxy implements ICodeExecutionAPI {
	static private ViewerMem _v = null;

	@Override
	public void setHelper(final CodeExecutionHelper ce) {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				if (_v == null)
					_v = new ViewerMem(4);
				_v.setHelper(ce);
			}
		});
	}

	public void start() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				_v.start();
			}
		});
	}

	public void dataIn() {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				_v.dataIn();
			}
		});
	}

	@Override
	public void finish() {
		_v.disposePalette();
		_v = null;
	}
}
