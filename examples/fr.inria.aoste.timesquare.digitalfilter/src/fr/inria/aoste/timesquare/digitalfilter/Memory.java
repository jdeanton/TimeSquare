/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.digitalfilter;
import java.util.Random;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionHelper;
import fr.inria.aoste.timesquare.backend.codeexecution.ICodeExecutionAPI;


public class Memory implements ICodeExecutionAPI{
	private CodeExecutionHelper _ce=null;
	private Color[] data;
	private int currentPlace=0;
	
	public Memory() { this(128); }
	
	Memory(int dataSize){
		Random randomGenerator = new Random();
		data = new Color[dataSize];
		for (int i=0; i <dataSize; i++){
			int randomRed = randomGenerator.nextInt(256);
			int randomGreen = randomGenerator.nextInt(256);
			int randomBlue = randomGenerator.nextInt(256);
			data[i] = new Color(Display.getDefault(), randomRed, randomGreen, randomBlue);
		}
	}

	@Override
	public void setHelper(CodeExecutionHelper ce) {
		_ce=ce;
	}

	public void data(){
		_ce.putData("memoryData", data[currentPlace]);
		currentPlace=(currentPlace+1)%data.length;
	}

	@Override
	public void finish() {
		if ( data!=null)
			for(Color c : data) c.dispose();
		data=null;
	}
}
