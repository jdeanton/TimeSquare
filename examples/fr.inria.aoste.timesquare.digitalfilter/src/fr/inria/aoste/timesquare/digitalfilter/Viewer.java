/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.digitalfilter;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionHelper;

class Viewer extends ApplicationWindow {
	protected int currentIndex=0;
	private Label[] labels;
	private Color[] colors;
	private boolean _eol=false;

	Viewer(int size) {
		super(new Shell(SWT.NO_TRIM | SWT.ON_TOP));
		Shell _shell= this.getParentShell();
		if (_shell != null){
			_shell.setText("itVideoDevice");
		}
		labels = new Label[size];
		colors = new Color[size];
	}

	private CodeExecutionHelper _ce=null;
	void setHelper(CodeExecutionHelper ce) {
		_ce = ce;
	}

	Display _d=null;

	void dataIn(){
		colors[currentIndex].dispose();
		colors[currentIndex] = (Color)_ce.getData("Color");	  
		labels[currentIndex].setBackground(colors[currentIndex]);
	}

	void disposePalette() {
		for(Color c : colors) c.dispose();
		colors = null;
		for(Label l : labels) l.dispose();
		labels = null;
	}

	public void CR(){
		_eol=true;
	}

	public void propagate(){
		if (_eol){
			currentIndex=0;
			_eol=false;
		}else{
			currentIndex=currentIndex+1;
		}
	}

	private Composite composite = null;
	@Override
	protected Control createContents(Composite parent) {
		if (composite != null) return composite;
		Composite composite = new Composite(parent, SWT.NULL);
		composite.setLayout(new RowLayout());
		int step = 256/labels.length;
		for (int i=0; i<labels.length; i++){
			final Label label = new Label(composite, SWT.PUSH);
			label.setText("__");
			colors[i] = new Color(Display.getDefault(), i*step, i*step, i*step);
			label.setBackground(colors[i]);
			labels[i] = label;
		}
		return composite;
	}


	void start() {
		setBlockOnOpen(false);
		open();
	}	
}
