/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.demo;

import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import simbad.sim.Agent;
import simbad.sim.Box;


/** This the simplest Demo. Robot has no sensors.
 * The robot progress until it reaches a box.
*
 */ 
public class SimplestDemo extends Demo {
    
    public class Robot extends Agent {
        
        public Robot(Vector3d position, String name) {
            super(position, name);
        }
        
        /** Initialize Agent's Behavior */
        public void initBehavior() {
            setTranslationalVelocity(1f);
        }
        
        /** Perform one step of Agent's Behavior */
        public void performBehavior() {
 
        }
    }    
        public SimplestDemo() {
            add(new Box(new Vector3d(10, 0, 0), new Vector3f(3f, 3f, 3f), this));
            add(new Robot(new Vector3d(0, 0, 0), "robot"));
        }
    }