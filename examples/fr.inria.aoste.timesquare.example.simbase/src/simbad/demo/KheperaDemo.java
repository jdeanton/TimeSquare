/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.demo;

import javax.vecmath.Vector3d;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3f;
import simbad.sim.*;
/** A Khepera Robot demo.
 * 
 **/
public class KheperaDemo extends Demo {

    // derive from class KheperaRobot instead of class Agent 
    public class Robot extends KheperaRobot {
        public Robot(Vector3d position, String name) {
            super(position, name);
        }

        /** Initialize Agent's Behavior */
        public void initBehavior() {
        }

        /** Perform one step of Agent's Behavior */
        public void performBehavior() {
           setWheelsVelocity(0.01,0.01);
            if (collisionDetected()) moveToStartPosition();
      }

    }

    public KheperaDemo() {
        setWorldSize(1f);
        boxColor = new Color3f(0.6f,0.5f,.3f);

			
        add(new Box(new Vector3d(0,0,-0.07),new Vector3f(0.10f,0.03f,0.055f),this));
		
		add(new Box(new Vector3d(0.2,0,0),new Vector3f(0.055f,0.1f,0.055f),this));
			

		// Add a robot.
        add(new Robot(new Vector3d(0, 0, 0),"khepera"));
        
    
    }
}