/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.demo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *  Manages (simply) a list of demonstrations and is used by Simbad main class.
 *  This class is used statically only.
 */
public class DemoManager {

    // available demos classes
    final static String classNames[] = { "simbad.demo.BaseDemo","simbad.demo.BumpersDemo",
            "simbad.demo.SingleAvoiderDemo", "simbad.demo.AvoidersDemo",
            "simbad.demo.ImagerDemo", "simbad.demo.LightSearchDemo",
            "simbad.demo.SimplestDemo", "simbad.demo.BlinkingLampDemo",
            "simbad.demo.DifferentialKinematicDemo","simbad.demo.KheperaDemo","simbad.demo.PickCherriesDemo",
            "simbad.demo.PushBallsDemo"};

    /** Creates a menu corresponding to the demo set. */
    public final static JMenu createMenu(ActionListener actionListener) {
        JMenu menu = new JMenu("Examples");
        for (int i = 0; i < classNames.length; i++) {
            JMenuItem item = new JMenuItem(classNames[i]);
            item.setActionCommand("demo");
            item.addActionListener(actionListener);
            menu.add(item);
        }
        return menu;
    }

    /** Creates a demo corresponding to the menu item. */
       public static Demo getDemoFromActionEvent(ActionEvent event) {
        Class cl = null;
        Demo demo=null;
        String demoName = ((JMenuItem) event.getSource()).getText();
        try {
            cl = Class.forName(demoName);
            demo = (Demo)cl.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return demo;
    }
}