/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.sim;


/**
 * This class represents a CameraSensor.
 * See Eye class api .
 */
public class CameraSensor extends Eye {

    /**
     * Constructs
     * @param radius
     * @param imageWidth
     * @param imageHeight
     */
    CameraSensor(float radius, int imageWidth, int imageHeight) {
        super(radius, imageWidth, imageHeight);
      
    }}
