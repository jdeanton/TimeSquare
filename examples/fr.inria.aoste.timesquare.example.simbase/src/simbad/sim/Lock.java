/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.sim;

/**
 * A Simple lock for mutual exclusion.
 */
public class Lock  {
    boolean locked = false;;
    
    
    Lock(){    }
 
    /** Obtain the lock. Only one thread at each time.*/ 
    public synchronized void lock(){
        while(locked){
            try {
                wait();
            } catch (InterruptedException e) {             
            }
            
        }
        locked = true;
    }
    /** Release the lock.*/
    public synchronized void unlock(){
        locked = false;
        notifyAll();
    }
}
