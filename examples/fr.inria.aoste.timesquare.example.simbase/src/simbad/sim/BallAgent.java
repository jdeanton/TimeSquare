/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package simbad.sim;

import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.Bounds;
import javax.media.j3d.Node;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.geometry.Sphere;

/**
 * @author louis
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class BallAgent extends SimpleAgent {

    Color3f color;
    /**
     * @param pos
     * @param name
     */
    public BallAgent(Vector3d pos, String name, Color3f color,float  radius,float mass) {
        super(pos, name);
        this.radius = radius;
        this.height = 2*radius;
        this.mass =mass;
        staticFrictionCoefficient = 0.1f;
        this.color = color;
        
        
    }
    public BallAgent(Vector3d pos, String name,float  radius) {
        this(pos, name, new Color3f(1f,0.2f,0.05f),radius,0.25f);
        
    }

   
    /** Create 3D geometry. */
    protected void create3D(){
         Appearance  appear = new Appearance();
        
        material.setDiffuseColor(color);
        material.setSpecularColor(black);
        appear.setMaterial(material);
        int flags = Primitive.GEOMETRY_NOT_SHARED | Primitive.ENABLE_GEOMETRY_PICKING | Primitive.GENERATE_NORMALS;
        
      
        body = new Sphere(radius,flags,appear);
        
        // we must be able to change the pick flag of the agent 
        body.setCapability(Node.ALLOW_PICKABLE_READ);
        body.setCapability(Node.ALLOW_PICKABLE_WRITE);
        body.setPickable(true);
        body.setCollidable(true);
        addChild(body);
     	    
	    // Add bounds for interactions
	    Bounds bounds = new BoundingSphere(new Point3d(0,0,0),radius);
	    setBounds(bounds);
   
    }

}
