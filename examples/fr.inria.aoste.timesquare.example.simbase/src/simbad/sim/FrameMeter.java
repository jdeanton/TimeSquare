/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package simbad.sim;

/* A helper class to measure the frame per second  at a given point. */
 final class FrameMeter {

    private long lastTime;
    private long startTime;
    private long  count;
    private long totalCount;
    /** Instant fps */
    public float fps;
    public float fpsSinceStart;
    private int updateRate;
    
    public FrameMeter() {
        updateRate = 200;
        reset();
    }

    
    /*
     *  Method to be called at the measure point 
     *  
     */
    public void measurePoint(int inc) {
        count += inc;
        totalCount += inc;
        if (count >= updateRate) {
            long currTime = System.currentTimeMillis();
            long dt = (currTime - lastTime);
            if (dt != 0) fps = (1000f * count) / (float) dt;
            fpsSinceStart =(1000f *totalCount)/(float)(currTime - startTime);
            lastTime = currTime;
            count = 0;
        }
    }

    public int getUpdateRate(){
        return updateRate;
    }
    public void setUpdateRate(int rate){
        updateRate = rate;
        reset();
    }
    /*  Resets the measure */
    public void reset() {
        startTime = lastTime = System.currentTimeMillis();
        totalCount =0;
        count= 0;
        fps=0;fpsSinceStart=0;
    }
}