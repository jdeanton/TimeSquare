/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.sim;


import javax.vecmath.Vector3d;

public class Robot extends Agent {

    
    Plan plan;

    public Robot(Vector3d position, String name) {
        super(position, name);
     // a square movement plan
        double deg90=Math.PI/2;
        plan = new Plan(this);
        plan.forward(3, 2);
        plan.turn(-deg90, 2);
        plan.forward(3, 2);
        plan.turn(-deg90, 2);
        plan.forward(3, 2);
        plan.turn(-deg90, 2);
        plan.forward(3, 2);
        plan.turn(-deg90, 2);
        plan.loop();
        
    }

    public void initBehavior() {
    //    plan.reset();
    }

    public void performBehavior() {
       if (collisionDetected()){
            // Change plan
        }
        plan.doStep();
       
    }
}