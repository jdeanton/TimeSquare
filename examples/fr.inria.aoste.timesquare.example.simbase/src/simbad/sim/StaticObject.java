/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package simbad.sim;

import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.Bounds;
import javax.media.j3d.Transform3D;

/**
 * Base Object for all block world objects (box,wall,arch ...).
 * All object which doesnt move. 
 */
public class StaticObject extends BaseObject {
    /** Transformed bounds is the bound object in global coordinates.   */
    protected Bounds transformedBounds;
    /** The local to global transform.*/
    protected Transform3D localToVworld; 
    
    /** Appearance of the object. */
    protected Appearance appearance;

    /** Create object's geometry. */
    void create3D(){
        super.create3D(false);
        appearance = new Appearance();
        compilable = true;
        localToVworld = new Transform3D();
    }
    
    /** Create and pre Compute the transformed bound of the objects.
     * Needs localtoVWorld. */
    protected void createTransformedBounds(){
        
        transformedBounds = (Bounds)(localBounds.clone());
        transformedBounds.transform(localBounds,localToVworld);
         
    }
    
    
    /** Gets the bound of the object taking account of its current position.*/
    protected Bounds getTransformedBounds(){
        return transformedBounds;
    }
    
    /** Create the definitive local to global transform .*/
    protected void createLocalToVworld(){
        group.getLocalToVworld(localToVworld);
    }
    
    /**  Returns true if the object intersect with the given bounding sphere. 
     * This can be overriden.
     * @param bs the boundingsphere to intersect with.*/
    protected boolean intersect(BoundingSphere bs){
        return transformedBounds.intersect(bs);
    }
}
