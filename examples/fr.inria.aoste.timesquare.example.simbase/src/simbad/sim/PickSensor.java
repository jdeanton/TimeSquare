/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.sim;

import javax.media.j3d.BranchGroup;


/**
 * Base class for all sensors using picking.
 */
public abstract class PickSensor extends SensorDevice {
    protected BranchGroup pickableSceneBranch;
    
    /**
     * It should be called before update.
     * @param pickableSceneBranch
     */
   protected  void setPickableSceneBranch(BranchGroup pickableSceneBranch)
    { this.pickableSceneBranch = pickableSceneBranch;}

}
