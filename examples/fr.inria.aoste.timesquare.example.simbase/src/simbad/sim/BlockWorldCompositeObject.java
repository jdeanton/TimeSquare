/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package simbad.sim;

import java.util.ArrayList;


import javax.media.j3d.BoundingSphere;


/**
 * Base classt for all composite  block world objects (arch...).
 */
public class BlockWorldCompositeObject extends BlockWorldObject {
    /** Keep list of all simple components .*/
    ArrayList components;
   
    BlockWorldCompositeObject(){
        components = new ArrayList();
    }
    
    
    /** Create and pre Compute the transformed bound of the objects. */
    protected void createTransformedBounds(){
        for (int i = 0; i< components.size();i++){
            BlockWorldObject bo = (BlockWorldObject) components.get(i);
            bo.createLocalToVworld();
            bo.createTransformedBounds();
        }
         
    }

    /**  Returns true if the object intersect with the given bounding sphere. 
     * This can be overriden.
     * @param bs the boundingsphere to intersect with.*/
    protected boolean intersect(BoundingSphere bs){
        // If any component intersect
        for (int i = 0; i< components.size();i++){
            if (((BlockWorldObject) components.get(i)).intersect(bs))
                return true;
        }
        return false ;
    }
    
    protected void addComponent(BlockWorldObject o){
        components.add(o);
        addChild(o);
    }
}
