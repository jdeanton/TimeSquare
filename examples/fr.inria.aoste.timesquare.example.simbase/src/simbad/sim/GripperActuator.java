/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/

package simbad.sim;



import javax.media.j3d.Appearance;
import javax.media.j3d.Material;
import javax.media.j3d.Node;
import javax.vecmath.Color3f;

import com.sun.j3d.utils.geometry.Sphere;

/**
 *experimental.
 */
public class GripperActuator extends ActuatorDevice {
    
    GripperActuator(float radius  ) {
        create3D(radius);
    }

    void create3D(float radius) {
        super.create3D(true);
        // body
        if (radius > 0) {
            Color3f color = new Color3f(0.8f, 0.8f, 0.0f);
            Appearance appear = new Appearance();
            appear
                    .setMaterial(new Material(color, black, color, white,
                            100.0f));
            Node node = new Sphere(radius, appear);
            node.setCollidable(false);
            node.setPickable(false);
            addChild(node);
        }
    }
 
}
