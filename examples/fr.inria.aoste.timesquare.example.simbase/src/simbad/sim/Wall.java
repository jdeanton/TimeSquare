/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.sim;


import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;


/**
 * A wall which can be put in the environnement. 
 */
public class Wall extends Box{
    final static  float thickness = 0.3f;
 
    public Wall(Vector3d pos ,float length,float height ,EnvironmentDescription wd) {
        super(pos,new Vector3f(length,height,thickness),wd);
        setColor(wd.wallColor);
    }
    

    public Wall(Vector3d pos ,float length,float width, float height, EnvironmentDescription wd) {
        super(pos,new Vector3f(length,height,width),wd);
        setColor(wd.wallColor);
    }
  }
