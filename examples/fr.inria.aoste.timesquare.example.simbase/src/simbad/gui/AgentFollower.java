/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.gui;

import simbad.sim.*;

/**
 * The AgentFollower class is a helper class for the AgentInspector. It updates cyclically the world view point.
 * in order to follow agent movements.  
 *  
 */
class AgentFollower implements Runnable {

    World world;
    SimpleAgent agent;
    Thread thread;
    boolean stopped;
    boolean changed;
    int viewPointType;

    AgentFollower(World world, SimpleAgent agent) {
        this.agent = agent;
        this.world = world;
        viewPointType = World.VIEW_ABOVE_AGENT;
        stopped = true;
        thread = new Thread(this);
        thread.start();
    }

    public void run() {
        while (true) {
            changed= false;
            if (!stopped)
            			world.changeViewPoint(viewPointType, agent);
            try {
                // don't need precise time tick
                // do multiple sleep to diminish ui latency
                for (int i=0;i<30;i++){
                    Thread.sleep(100);
                    if (changed) break;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected void setViewPointType(int type){
        viewPointType = type;
    }
    protected void suspend() {
        stopped = true;
        changed= true;
    }

    protected void resume() {
        stopped = false;
        changed= true;
    }
}