/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.gui;

import java.awt.BorderLayout;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import simbad.sim.World;

/**
 * This window is used to visualise the 3D world.
 */
public final class WorldWindow extends JInternalFrame {
  
	private static final long serialVersionUID = 1L;
	World world;
 
    public WorldWindow(World world) {
        super("world");
        this.world = world;
        initialize();
    }

    private void initialize() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add("Center",world.getCanvas3D());
         setContentPane(panel);
        setSize(400, 400);
        setResizable(true);
    }
}