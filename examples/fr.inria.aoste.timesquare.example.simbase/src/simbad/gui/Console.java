/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.gui;
import java.awt.Font;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 * <p> Redirects all System.out.print in a window 
 * <p>
 */
public class Console extends JInternalFrame implements Runnable {


	private static final long serialVersionUID = 1L;
	Thread thread;
    PipedOutputStream out;
    PipedInputStream in;
    JTextArea jtextArea;
    JScrollPane jscrollPane;
    static int MAXLENGHT = 10*1024;

    public Console(int sizex, int sizey) {
        super("System.out");
        initialize();
        setSize(sizex, sizey);
    }

    private void initialize() {
        // redirige la System.out sur un pipe
        try {
            out = new PipedOutputStream();
            in = new PipedInputStream(out);
        } catch (IOException e) {
            System.err.println(" IO Exception");
        }
        System.setOut(new PrintStream(out));
        jtextArea = new JTextArea(30, 30);
        jtextArea.setEditable(false);
        jtextArea.setAutoscrolls(true);
        jtextArea.setFont(new Font("Courier", Font.PLAIN, 10));
             jscrollPane = new JScrollPane(jtextArea);
        jscrollPane
                .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setContentPane(jscrollPane);
        thread = new Thread(this);
        thread.start();
        setResizable(true);
    }

     public void run() {
        while (true) {
            try {
                Thread.sleep(100);
                if (in.available()> 0)
                {
                    byte buf[] = new byte[in.available()];
                    in.read(buf, 0, buf.length);
                    // devrait �tre bloquant ?
                    String text = jtextArea.getText();
                    text += new String(buf);
                    int l = text.length();
                    if (l > MAXLENGHT)
                            text = text.substring(l - MAXLENGHT, l - 1);
                    jtextArea.setText(text);
                    
                    jtextArea.setCaretPosition(jtextArea.getDocument().getLength());

                }
            } catch (IOException e) {
                System.err.println("Console IO Exception");
            } catch (InterruptedException e) {}
        }
    }
}