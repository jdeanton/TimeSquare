/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package simbad.gui;

import javax.swing.JInternalFrame;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import simbad.sim.Simulator;
import simbad.sim.World;
/*
 * A Container for all the control panels (world, simulator).
 * @author Louis Hugues
 */
public class ControlWindow extends JInternalFrame {

	private static final long serialVersionUID = 1L;

	public ControlWindow(World world,  Simulator simulator){
    	    super("Control");
    	    createGui(world,simulator);
    	}
    	
    	private void createGui(World world,Simulator simulator){
    	    
    	    JPanel panel = new JPanel();
    	    setContentPane(panel);
    	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    	    panel.add(new WorldControlGUI(world,simulator));
       	panel.add(new SimulatorControlGUI((JFrame)getParent(),simulator));
    	    pack();
    	}
}
