/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.example.simbase.control;

import java.util.ArrayList;

import javax.vecmath.Vector3d;

import simbad.sim.Agent;
import simbad.sim.CameraSensor;
import simbad.sim.RangeSensorBelt;
import simbad.sim.RobotFactory;

public class Robot extends Agent {

    RangeSensorBelt sonars;
    CameraSensor camera;
    
    public ArrayList<Double> readSonars(){
    	ArrayList<Double> res = new ArrayList<>();
    	for (int i = 0; i < sonars.getNumSensors(); i++) {
			res.add(sonars.getMeasurement(i));
		}
    	return res;
    }
    public double requiredSpeed=0.0;
    public double requiredRotationalVelocity=0.0;
    
	public Robot(Vector3d position, String name) {
	    super(position, name);
	    // Add camera
	    camera = RobotFactory.addCameraSensor(this);
	    // Add sonars
	    sonars = RobotFactory.addSonarBeltSensor(this);
	    
	    
	}
	
	/** This method is called by the simulator engine on reset. */
	public void initBehavior() {
	    // nothing particular in this case
	}
	
	/** This method is call cyclically (20 times per second)  by the simulator engine. */
	public void performBehavior() {
//	    // adjust speed
//		this.setTranslationalVelocity(1);
//	    //adjustRotation
//	    this.setRotationalVelocity(requiredRotationalVelocity);
	}
}