/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.example.simbase.control;


import java.util.ArrayList;

import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;

import simbad.gui.Simbad;
import simbad.sim.Simulator;
import fr.inria.aoste.timesquare.backend.codeexecution.CodeExecutionHelper;
import fr.inria.aoste.timesquare.backend.codeexecution.ICodeExecutionAPI;


/**
  Derivate your own code from this example.
 */


public class FishBrainRobot implements ICodeExecutionAPI{
	private Simulator _sim;
	private Robot _robot;
	Simbad frame;
	
	private ArrayList<Double> _sonars;
	private double _obstacleDistance;
	private int _criticSonar;
	
	private double _speedCommand;
	private double _rotationCommand;
	
	private CodeExecutionHelper _ce=null;

    public FishBrainRobot(){
        System.out.println("################################\n      create fishbot\n########################");
        initSimulator();
    }
    
    public void initSimulator(){
    	// request antialising
        System.setProperty("j3d.implicitAntialiasing", "true");
        // create Simbad instance with given environment
        _robot = new Robot(new Vector3d(0, 0, 0), "fishBot");
        frame = new Simbad(new Environnement(_robot), false);
        _sim= frame.getSimulator();
        _sonars = new ArrayList<Double>();
        _sim.setVirtualTimeFactor(5);
    }
    
	@Override
	public void setHelper(CodeExecutionHelper ce) {
		_ce=ce;
	}

	@Override
	public void finish() {
		_sim.stopSimulation();
		_sim.dispose();
		frame.dispose();
		
	}
   
    public void stopRobot(){
    	_robot.setTranslationalVelocity(0);
    }
    
    public void switchOffRobotAlertColor(){
    	_robot.setColor(new Color3f(255, 255, 255));
    }
    
    public void switchOnRobotAlertColor(){
    	_robot.setColor(new Color3f(255, 0, 0));
    }
    
    public int getCloserSonarNumber(ArrayList<Double> sonars){
    	double min=10;
    	int sonarNumber=-1; //not close enough to measure 
    	System.out.println(sonars.size());
    	for (int i=0; i<sonars.size(); i++) {
    		if (i <=2 || i >=sonars.size()-2){ //only front obstacles matter
	    		Double current = sonars.get(i);
				if (current < min){
					min=current;
					sonarNumber=i;
				}
    		}
		}
    	return sonarNumber;
    }
    
    public void readSonars(){
    	_sonars = _robot.readSonars();
    }
    
    public Boolean correlateAndCheckIfAlarm(){
    	_criticSonar = getCloserSonarNumber(_sonars);
    	
    	if (_criticSonar != -1){ //an obstacle detected
    		System.out.println("alarm requested");
    		_obstacleDistance=_sonars.get(_criticSonar);
    		return true; 
    	}else{
    		_obstacleDistance=-1; //no obstacle
    		return false; //no alarm
    	}
    	
    }
    
     
    public void fishBrainControl(){
    	//switchOffRobotAlertColor(); //should not be here
    	_speedCommand = 9*Math.random();
    	_rotationCommand = Math.random();
    }
    
    public void computeRobotSafeCommand(){
    	if (_obstacleDistance < 1){
    		_speedCommand = 0.3;
    	}else{
    		_speedCommand =0.6;
    	}
    	
    	if (_criticSonar < 2){ //right obstacle -> turn left
    		_rotationCommand = -Math.PI/4;
    	}
    	if (_criticSonar > _sonars.size()-2){ //left obstacle -> turn right
    		_rotationCommand = Math.PI/4;
    	}
    }
    
    
    public void servoMotorControl(){
    	_robot.setTranslationalVelocity(_speedCommand);
    	_robot.rotateY(_rotationCommand);
    }
    
    
    public void increaseTime(){
    	_sim.simulateOneStep();
    }
    
    public static void main(String[] args) {
        FishBrainRobot fishBot = new FishBrainRobot();
        fishBot._robot.setTranslationalVelocity(2);
        for (int i = 0; i < 10000; i++) {
			if(i%20==0){
				fishBot.readSonars();
				if(fishBot.correlateAndCheckIfAlarm()){
					fishBot.computeRobotSafeCommand();
					fishBot.switchOnRobotAlertColor();
				}else{
					fishBot.fishBrainControl();
					fishBot.switchOffRobotAlertColor();
				}
				fishBot.servoMotorControl();
				
			}
			
			try {
				Thread.sleep(50, 0);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			fishBot._sim.simulateOneStep();
		}
        
    }

	

} 