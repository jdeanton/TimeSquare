import 'http://www.eclipse.org/uml2/4.0.0/UML'
 
ECLimport  "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib"
ECLimport  "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib"
 
package uml 
	 
	context InputPin
		def : dataRead : Event(self) = ConsumeEvent
	
	context OutputPin
		def : dataWrite : Event(self) = produceEvent
	
	context ControlNode
		def : startIt : Event(self) =StartEvent
		def : finishIt : Event(self) =StartEvent
		
	context Activity  
		def: startActivity : Event(self) = StartEvent
		def: finishActivity: Event(self) = StopEvent
		
	context Action
		def : startIt : Event(self) = StartEvent
		def : finishIt : Event(self) = StopEvent
	
	context ObjectFlow
		inv consumeonlyIfproduced:
			(self.target.oclIsKindOf(InputPin)) implies
			(Relation Precedes(self.source.oclAsType(OutputPin).dataWrite, self.target.oclAsType(InputPin).dataRead))	
		inv consumeMeansStart:
			(self.target.oclIsKindOf(Action)) implies
			(Relation Precedes(self.source.oclAsType(OutputPin).dataWrite, self.target.oclAsType(Action).startIt))	

		inv consumeMeansControl:
			(self.target.oclIsKindOf(ControlNode)) implies
			(Relation Precedes(self.source.oclAsType(OutputPin).dataWrite, self.target.oclAsType(ControlNode).startIt))	
			
	context Action	
		inv waitControlToExecute:
		(self.incoming->size() > 0) implies
			let incomingFinished : Event = self.incoming->asSequence()->first().source.finishIt in
			(Relation Causes(incomingFinished, self.startIt))
		
		inv StartAlternatesWithFinish:
			Relation Alternates(self.startIt,self.finishIt)
	
		inv startWhenDataReadable:
		(self.input->size() > 0) implies
			let allInputDataRead : Event = Expression Inf(self.input.dataRead) in
			Relation Coincides(self.startIt,allInputDataRead)
		
		inv lastDataWroteCauseStop:
		(self.output->size() > 0) implies
			let allDataWrote : Event = Expression Sup(self.output.dataWrite) in
			Relation Alternates(allDataWrote,self.finishIt)

		inv startBeforeWriting:
		(self.output->size() > 0) implies
			let firstDataWrote : Event = Expression Inf(self.output.dataWrite) in
			Relation Alternates(self.startIt,firstDataWrote)

	context Activity  
		
		inv startCausesInit:
			Relation Causes(self.startActivity, self.node->select(n|n.oclIsKindOf(InitialNode))->asSequence()->first().oclAsType(InitialNode).startIt)
	
		inv lastFinalNodeStopActivity:
			let lastFinalNode = Expression Union(self.node->select(n|n.oclIsKindOf(FinalNode)).oclAsType(FinalNode).finishIt) in
			Relation Causes(lastFinalNode,self.finishActivity)
			
		inv noReEntrance:
			Relation Alternates(self.startActivity, self.finishActivity)
	
	context DecisionNode 
		inv DecisionCausalities: 
			let startChoice1 : Event = self.outgoing->asSequence()->first().target.oclAsType(Action).startIt in
			let startChoice2 : Event = self.outgoing->asSequence()->last().target.oclAsType(Action).startIt in
			--let unionOfChoices : Event = Expression[Union](startChoice1, startChoice2) in
			--(Relation Precedes(startIt, unionOfChoices)) and 
			(Relation Exclusion(startChoice1, startChoice2)) 
			
			inv DecisionCausalities: 
			let allStartChoices : Event = Expression Union(self.outgoing->asSequence().target.oclAsType(Action).startIt) in
			Relation Alternates(self.startIt, allStartChoices)
	
	context MergeNode
		inv MergeCausalities: 
			let unionOfIncomes : Event = Expression Union(self.incoming.source.oclAsType(Action).finishIt) in
	 		(Relation Precedes(unionOfIncomes, self.startIt))
	 
	 context ControlNode
	 	inv instantaneousExecution:
			Relation Coincides(self.startIt,self.finishIt)
			
		inv waitInputToControl:
		((self.incoming->size() > 0) and self.incoming->asSequence()->first().source.oclIsKindOf(Action)) implies
			let incomingFinished : Event = self.incoming->asSequence()->first().source.finishIt in
			(Relation Causes(incomingFinished, self.startIt))
--	--just to see more easily the causalities		
--	context InitialNodend 
--		inv runOnlyOnce:
--			let firstStart : Event = Expression OneTickAndDie(self.startIt) in 
--			Relation Coincides(self.startIt, firstStart)
--	
endpackage