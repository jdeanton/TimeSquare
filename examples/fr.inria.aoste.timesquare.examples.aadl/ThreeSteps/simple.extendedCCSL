/*
 * CCSL specification : AADL example three steps
 * Just the application part with causalities
 * 
 * @author:  Frederic Mallet
 * date :  March 6 2012  10:56:56 CET 
 */
ClockConstraintSystem MySpec {
    imports {
        // import statements
		import "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib" as lib0; 
		import "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib" as lib1; 
    }
    entryBlock main
     
	dataTypes {
		DenseClockType Dense{baseUnit second}
	}
	Block main {
    	// -----------------------------------------
		// ------------- APPLICATION ---------------	
		Clock sense
		Clock t1
		Clock t2
		Clock t3
		Clock act
		
		// data dependency sense <- t1 
		Relation com1[Causes](  LeftClock-> sense,	RightClock-> t1  )
					
		// data dependency t1 <- t2 
		Relation com2[Causes](  LeftClock-> t1,	RightClock-> t2  )
					
		// data dependency t2 <- t3 
		Relation com3[Causes](  LeftClock-> t2,	RightClock-> t3  )

		// data dependency t3 <- act 
		Relation com4[Causes](  LeftClock-> t3,	RightClock-> act  )
		
		
    	// --------------------------------
		// ------------- OS ---------------
    	Clock physicalTime:Dense
    	Real factor=0.01
     	Expression _100hz=Discretize( DenseClock->  physicalTime	,DiscretizationFactor-> factor  )
     	
     	Relation acq[Coincides](   Clock2-> _100hz ,	Clock1-> sense  )

		// t1 periodic 50hz
		Integer two=2
		Expression _50hz=PeriodicOffsetP(PeriodicOffsetPBaseClock->_100hz, PeriodicOffsetPPeriod->two)
		Expression t1_dispatch=SampledOn( SampledOnSampledClock-> sense, SampledOnTrigger-> _50hz  )

		Relation period1[Causes](  LeftClock-> t1_dispatch,	RightClock-> t1  )
     	
		// t3 periodic 25hz
		Integer two=2
		Expression t3_dispatch=PeriodicOffsetP(PeriodicOffsetPBaseClock->_50hz, PeriodicOffsetPPeriod->two)
		Relation period3[Causes](  LeftClock-> t3_dispatch,	RightClock-> t3  )
	}
}
