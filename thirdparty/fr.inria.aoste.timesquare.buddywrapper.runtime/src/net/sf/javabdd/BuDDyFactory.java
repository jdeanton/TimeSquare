package net.sf.javabdd;
/**
 * freely inspired from javaBDD
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.PrimitiveIterator.OfInt;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

import fr.inria.aoste.timesquare.buddywrapper.Activator;
import fr.inria.aoste.timesquare.buddywrapper.LogicalOperator;
import net.sf.javabdd.BDDFactory.GCStats;
import net.sf.javabdd.BuDDyFactory.BuDDyBDD.ReorderStats;


public class BuDDyFactory extends BDDFactory{
	
	
	//called when the class is loaded
	static {
		if ( ! Activator.loadNativeLibrary("buddy") ) {
			if (Activator.arch.equalsIgnoreCase("x86_64")) {
				Activator.loadLibrary("buddy" + "64");
			}
			else {
				Activator.loadLibrary("buddy");
			}
		}
		registerNatives();
	}
	synchronized private static native void registerNatives();
	

	protected int fdvarnum;
	protected int firstbddvar;

	public synchronized static BuDDyFactory init(int nodenum, int cachesize) {
		if (INSTANCE != null){
			throw new InternalError("Error: BuDDyFactory already initialized.");
		}
		INSTANCE = new BuDDyFactory();
		initialize0(nodenum, cachesize);
		return INSTANCE;
	}
	synchronized private static native void initialize0(int nodenum, int cachesize);

	/**
	 * Singleton factory instance. Only one factory object is enabled at a time.
	 */
	private static BuDDyFactory INSTANCE;



	private static BuDDyBDD makeBDD(int id) {
		BuDDyBDD b;
		b = new BuDDyBDD(id);
		return b;
	}

	public BuDDyBDD zero() {
		return makeBDD(0);
	}
	
	public BuDDyBDD one() {
		return makeBDD(1);
	}

	/**
	 * Converts collection of BuDDyBDD's into an int array, for passing to native code.
	 * 
	 * @param c
	 *            collection of BuDDyBDD's
	 * @return int array of indices
	 */
	private static int[] toBuDDyArray(Collection<BuDDyBDD> c) {
		int[] a = new int[c.size()];
		int k = 0;
		for (BuDDyBDD b : c) {
			a[k] = b._id;
		}
		return a;
	}

	public boolean isInitialized() {
		return isInitialized0();
	}

	synchronized private static native boolean isInitialized0();

	public void done() {
		System.gc();
		System.runFinalization();
		INSTANCE = null;
		done0();
	}

	synchronized private static native void done0();


	public void reset() {
		System.gc();
		System.runFinalization();
		try {
			int nodes = 100000;
			int cache = 100000;
			try {
				nodes = getNodeTableSize();
				cache = getCacheSize();
			} catch (Throwable e) {
				// System.err.println("Reset "+ e);
			}
			fdvarnum = 0;
			firstbddvar = 0;
			done();
			init(nodes, cache);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setError(int code) {
		setError0(code);
	}

	synchronized private static native void setError0(int code);

	public void clearError() {
		clearError0();
	}

	synchronized private static native void clearError0();

	public int setMaxNodeNum(int size) {
		return setMaxNodeNum0(size);
	}

	synchronized private static native int setMaxNodeNum0(int size);

	public double setMinFreeNodes(double x) {
		return setMinFreeNodes0((int) (x * 100.)) / 100.;
	}

	synchronized private static native int setMinFreeNodes0(int x);

	public int setMaxIncrease(int x) {
		return setMaxIncrease0(x);
	}

	synchronized private static native int setMaxIncrease0(int x);
	
	public double setIncreaseFactor(double x) {
		return setIncreaseFactor0(x);
	}

	synchronized private static native double setIncreaseFactor0(double x);

	public double setCacheRatio(double x) {
		return setCacheRatio0((int) (x * 100.)) / 100.;
	}

	synchronized private static native int setCacheRatio0(int x);

	public int setNodeTableSize(int x) {
		return setNodeTableSize0(x);
	}

	synchronized private static native int setNodeTableSize0(int x);

	public int setCacheSize(int x) {
		return setCacheSize0(x);
	}

	synchronized private static native int setCacheSize0(int x);

	public int varNum() {
		return varNum0();
	}

	synchronized private static native int varNum0();

	public int setVarNum(int num) {
		return setVarNum0(num);
	}

	synchronized private static native int setVarNum0(int num);

	public int duplicateVar(int var) {
		return duplicateVar0(var);
	}

	synchronized private static native int duplicateVar0(int var);

	public int extVarNum(int num) {
		return extVarNum0(num);
	}

	synchronized private static native int extVarNum0(int num);

	public BuDDyBDD ithVar(int var) {
		int id = ithVar0(var);
		return makeBDD(id);
	}

	synchronized private static native int ithVar0(int var);

	public BuDDyBDD nithVar(int var) {
		int id = nithVar0(var);
		return makeBDD(id);
	}

	synchronized private static native int nithVar0(int var);

	public void swapVar(int v1, int v2) {
		swapVar0(v1, v2);
	}

	synchronized private static native void swapVar0(int v1, int v2);

	public void printAll() {
		printAll0();
	}

	synchronized private static native void printAll0();

	public void printTable(BuDDyBDD bdd) {
		printTable0(bdd._id);
	}

	synchronized private static native void printTable0(int b);

	public BuDDyBDD load(String filename) {
		int id = load0(filename);
		return makeBDD(id);
	}

	synchronized private static native int load0(String filename);
	
	public BuDDyBDD load(BufferedReader ifile) throws IOException {

		tokenizer = null;

		int lh_nodenum = Integer.parseInt(readNext(ifile));
		int vnum = Integer.parseInt(readNext(ifile));

		// Check for constant true / false
		if (lh_nodenum == 0 && vnum == 0) {
			int r = Integer.parseInt(readNext(ifile));
			return r == 0 ? zero() : one();
		}

//		// Not actually used.
//		int[] loadvar2level = new int[vnum];
//		for (int n = 0; n < vnum; n++) {
//			loadvar2level[n] = Integer.parseInt(readNext(ifile));
//		}

		for (int n = 0; n < vnum; n++) {
			Integer.parseInt(readNext(ifile));
		}
		
		if (vnum > varNum())
			setVarNum(vnum);

		LoadHash[] lh_table = new LoadHash[lh_nodenum];
		for (int n = 0; n < lh_nodenum; n++) {
			lh_table[n] = new LoadHash();
			lh_table[n].first = -1;
			lh_table[n].next = n + 1;
		}
		lh_table[lh_nodenum - 1].next = -1;
		int lh_freepos = 0;

		BuDDyBDD root = null;
		OfInt it = IntStream.range(0, lh_nodenum).iterator();
		while(it.hasNext()){
			int key = Integer.parseInt(readNext(ifile));
			int var = Integer.parseInt(readNext(ifile));
			int lowi = Integer.parseInt(readNext(ifile));
			int highi = Integer.parseInt(readNext(ifile));

			BuDDyBDD low, high;
			low = loadhash_get(lh_table, lh_nodenum, lowi);
			high = loadhash_get(lh_table, lh_nodenum, highi);

			if (low == null || high == null || var < 0){
				throw new UnsupportedOperationException("BuDDyWrapper, Incorrect file format");
			}

			BuDDyBDD b = ithVar(var);
			root = b.ite(high, low);
			b.free();

			int hash = key % lh_nodenum;
			int pos = lh_freepos;

			lh_freepos = lh_table[pos].next;
			lh_table[pos].next = lh_table[hash].first;
			lh_table[hash].first = pos;

			lh_table[pos].key = key;
			lh_table[pos].data = root;
		}
		
		BuDDyBDD tmproot = root.id();
		
		for (int n : IntStream.range(0, lh_nodenum).toArray()){
			lh_table[n].data.free();
		}
		lh_table = null;
		return tmproot;
	}

	protected StringTokenizer tokenizer;

	protected String readNext(BufferedReader ifile) throws IOException {
		while (tokenizer == null || !tokenizer.hasMoreTokens()) {
			String s = ifile.readLine();
			if (s == null)
				throw new UnsupportedOperationException("Incorrect file format");
			tokenizer = new StringTokenizer(s);
		}
		return tokenizer.nextToken();
	}

	/**
	 * LoadHash is used to hash during loading.
	 */
	protected static class LoadHash {
		BuDDyBDD data;
		int key;
		int first;
		int next;
	}

	protected BuDDyBDD loadhash_get(LoadHash[] lh_table, int lh_nodenum, int key) {
		if (key < 0)
			return null;
		if (key == 0)
			return zero();
		if (key == 1)
			return one();

		int hash = lh_table[key % lh_nodenum].first;

		while (hash != -1 && lh_table[hash].key != key)
			hash = lh_table[hash].next;

		if (hash == -1)
			return null;
		return lh_table[hash].data;
	}

	
	public void save(String filename, BuDDyBDD bdd) {
		save0(filename, bdd._id);
	}

	synchronized private static native void save0(String filename, int b);

	public int level2Var(int level) {
		return level2Var0(level);
	}

	synchronized private static native int level2Var0(int level);

	public int var2Level(int var) {
		return var2Level0(var);
	}

	synchronized private static native int var2Level0(int var);


	public void addVarBlock(int first, int last, boolean fixed) {
		addVarBlock1(first, last, fixed);
	}

	synchronized private static native void addVarBlock1(int first, int last, boolean fixed);

	public void varBlockAll() {
		varBlockAll0();
	}

	synchronized private static native void varBlockAll0();

	public void clearVarBlocks() {
		clearVarBlocks0();
	}

	synchronized private static native void clearVarBlocks0();


	public int nodeCount(Collection<BuDDyBDD> r) {
		int[] a = toBuDDyArray(r);
		return nodeCount0(a);
	}

	synchronized private static native int nodeCount0(int[] a);

	public int getNodeTableSize() {
		return getAllocNum0();
	}

	synchronized private static native int getAllocNum0();

	public int getCacheSize() {
		return getCacheSize0();
	}

	synchronized private static native int getCacheSize0();

	public int getNodeNum() {
		return getNodeNum0();
	}

	synchronized private static native int getNodeNum0();

	//used by native code
	 private static void gc_callback(int i) {
	         INSTANCE.gbc_handler(i!=0, INSTANCE.gcstats);
	 }
	
	public static class BuDDyBDD{
		
		


		/** The value used by the BuDDyBDD library. */
		protected int _id;

		/** An invalid id, for use in invalidating BDDs. */
		static final int INVALID_BDD = -1;

		/**
		 * <p>
		 * Gets the level of this BuDDyBDD.
		 * </p>
		 * 
		 * <p>
		 * Compare to LEVEL() macro.
		 * </p>
		 * 
		 * @return the level of this BuDDyBDD
		 */
		public int level() {
			return getBuddyFactory().var2Level(var());
		}

		/**
		 * <p>
		 * Returns the logical 'and' of two BDDs. This is a shortcut for calling "apply" with the "and" operator.
		 * </p>
		 * 
		 * <p>
		 * Compare to bdd_and.
		 * </p>
		 * 
		 * @param that
		 *            BuDDyBDD to 'and' with
		 * @return the logical 'and' of two BDDs
		 */
		public BuDDyBDD and(BuDDyBDD that) {
			return this.apply(that, and);
		}

		/**
		 * <p>
		 * Makes this BuDDyBDD be the logical 'and' of two BDDs. The "that" BuDDyBDD is consumed, and can no longer be used. This is a shortcut for
		 * calling "applyWith" with the "and" operator.
		 * </p>
		 * 
		 * <p>
		 * Compare to bdd_and and bdd_delref.
		 * </p>
		 * 
		 * @param that
		 *            the BuDDyBDD to 'and' with
		 */
		public BuDDyBDD andWith(BuDDyBDD that) {
			return this.applyWith(that, and);
		}

		/**
		 * <p>
		 * Returns the logical 'or' of two BDDs. This is a shortcut for calling "apply" with the "or" operator.
		 * </p>
		 * 
		 * <p>
		 * Compare to bdd_or.
		 * </p>
		 * 
		 * @param that
		 *            the BuDDyBDD to 'or' with
		 * @return the logical 'or' of two BDDs
		 */
		public BuDDyBDD or(BuDDyBDD that) {
			return this.apply(that, or);
		}

		/**
		 * <p>
		 * Makes this BuDDyBDD be the logical 'or' of two BDDs. The "that" BuDDyBDD is consumed, and can no longer be used. This is a shortcut for calling
		 * "applyWith" with the "or" operator.
		 * </p>
		 * 
		 * <p>
		 * Compare to bdd_or and bdd_delref.
		 * </p>
		 * 
		 * @param that
		 *            the BuDDyBDD to 'or' with
		 */
		public BuDDyBDD orWith(BuDDyBDD that) {
			return this.applyWith(that, or);
		}

		/**
		 * <p>
		 * Returns the logical 'xor' of two BDDs. This is a shortcut for calling "apply" with the "xor" operator.
		 * </p>
		 * 
		 * <p>
		 * Compare to bdd_xor.
		 * </p>
		 * 
		 * @param that
		 *            the BuDDyBDD to 'xor' with
		 * @return the logical 'xor' of two BDDs
		 */
		public BuDDyBDD xor(BuDDyBDD that) {
			return this.apply(that, xor);
		}


		/**
		 * <p>
		 * Returns the logical 'implication' of two BDDs. This is a shortcut for calling "apply" with the "imp" operator.
		 * </p>
		 * 
		 * <p>
		 * Compare to bdd_imp.
		 * </p>
		 * 
		 * @param that
		 *            the BuDDyBDD to 'implication' with
		 * @return the logical 'implication' of two BDDs
		 */
		public BuDDyBDD imp(BuDDyBDD that) {
			return this.apply(that, imp);
		}

		/**
		 * Utility function to convert from a BuDDyBDD varset to an array of levels.
		 * 
		 * @param r
		 *            BuDDyBDD varset
		 * @return array of levels
		 */
		private static int[] varset2levels(BuDDyBDD r) {
			int size = 0;
			BuDDyBDD p = r.id();
			while (!p.isOne() && !p.isZero()) {
				++size;
				BuDDyBDD p2 = p.high();
				p.free();
				p = p2;
			}
			p.free();
			int[] result = new int[size];
			size = -1;
			p = r.id();
			while (!p.isOne() && !p.isZero()) {
				result[++size] = p.level();
				BuDDyBDD p2 = p.high();
				p.free();
				p = p2;
			}
			p.free();
			return result;
		}

		/**
		 * <p>
		 * Returns an iteration of the satisfying assignments of this BuDDyBDD. Returns an iteration of minterms. The <tt>var</tt> argument is the
		 * set of variables that will be mentioned in the result.
		 * </p>
		 * 
		 * @param var
		 *            set of variables to mention in result
		 * @return an iteration of minterms
		 */
		public BDDIterator iterator(final BuDDyBDD var) {
			return new BDDIterator(this, var);
		}

		/**
		 * <p>
		 * BDDIterator is used to iterate through the satisfying assignments of a BuDDyBDD. It includes the ability to check if bits are dont-cares
		 * and skip them.
		 * </p>
		 * 
		 * @author john whaley
		 * @version $Id: BuDDyBDD.java,v 1.4 2008/11/21 13:37:41 bferrero Exp $
		 */
		public static class BDDIterator implements Iterator<Object> {
			protected BuDDyFactory factory;
			protected int[] levels;
			protected boolean[] values;
			protected BuDDyBDD[] nodes = null;
			protected boolean more = false;

			/**
			 * <p>
			 * Construct a new BDDIterator on the given BuDDyBDD. The <tt>var</tt> argument is the set of variables that will be mentioned in the
			 * result.
			 * </p>
			 * 
			 * @param dis
			 *            BuDDyBDD to iterate over
			 * @param var
			 *            variable set to mention in result
			 */
			public BDDIterator(BuDDyBDD dis, BuDDyBDD var) {
				factory = dis.getBuddyFactory();
				if (!dis.isZero()) {
					levels = varset2levels(var);
					values = new boolean[levels.length];
					nodes = new BuDDyBDD[levels.length];
					fillInSatisfyingAssignment(dis.id(), 0);
					more = true;
				}
			}

			protected void fillInSatisfyingAssignment(BuDDyBDD node, int i) {
				while (!node.isOne() && !node.isZero()) {
					int v = node.level();

					// Mark skipped variables as dont-cares.
					int j = i;
					for(j=i; (j < levels.length && levels[j] != v); j++) {
						if (nodes[j] != null)
							throw new InternalError("nodes[" + j + "] should be null");
					}
					if (j == levels.length) {
						throw new UnsupportedOperationException("Exception in fillInSatisfyingAssignment: j == levels.length");
					}
					i = j;

					// Put node in table.
					nodes[i] = node;

					// Choose zero edge.
					BuDDyBDD node2 = node.low();
					if (node2.isZero()) {
						// Zero edge is F. Choose one edge instead.
						node2.free();
						values[i] = true;
						node2 = node.high();
					}
					node = node2;
					++i;
				}
			}

			protected boolean findNextSatisfyingAssignment() {
				for(int i = nodes.length - 1; i >= 0; i--){
					if (nodes[i] != null) {
						if (!values[i]) {
							// We already tried zero, try a one here now.
							BuDDyBDD hi = nodes[i].high();
							if (!hi.isZero()) {
								values[i] = true;
								fillInSatisfyingAssignment(hi, i + 1);
								return true;
							}
						}
						// We already tried both zero and one.
						nodes[i].free();
						nodes[i] = null;
						values[i] = false;
						// Fallthrough: Go to next bit.
					} else {
						// This is a dont-care bit, skip it.
					}
				}
				return false;
			}


			protected BuDDyBDD buildAndIncrement() {
				more = false;
				BuDDyBDD b = factory.one();
				boolean carry = true;
				for (int i = levels.length - 1; i >= 0; --i) {
					int level = levels[i];
					int var = factory.level2Var(level);
					boolean val = values[i];
					if (nodes[i] == null) {
						if (carry) {
							values[i] = !val;
							more |= !val;
							carry = val;
						}
					}
					BuDDyBDD v = val ? factory.ithVar(var) : factory.nithVar(var);
					b.andWith(v);
				}
				return b;
			}

			protected void free() {
				for (int i = levels.length - 1; i >= 0; --i) {
					if (nodes[i] != null) {
						nodes[i].free();
						nodes[i] = null;
					}
				}
				nodes = null;
			}

			public Object next() {
				BuDDyBDD b;
				if (more) {
					b = buildAndIncrement();
				} else {
					throw new NoSuchElementException();
				}
				if (!more) {
					more = findNextSatisfyingAssignment();
					if (!more) {
						free();
					}
				}
				return b;
			}

			public boolean hasNext() {
				return nodes != null;
			}

			public void remove() {
				throw new UnsupportedOperationException();
			}


			protected void fastForward0(int var) {
				if (levels == null)
					throw new UnsupportedOperationException();
				int level = factory.var2Level(var);
				int i = Arrays.binarySearch(levels, level);
				if (i < 0)
					throw new UnsupportedOperationException();
				if (nodes[i] != null)
					throw new UnsupportedOperationException();
				values[i] = true;
			}

			/**
			 * Fast-forward the iteration such that the given variable number is true.
			 * 
			 * @param var
			 *            number of variable
			 */
			public void fastForward(int var) {
				fastForward0(var);
			}



		}


		@Override
		public boolean equals(Object o) {
			if (!(o instanceof BuDDyBDD))
				return false;
			return this.equals((BuDDyBDD) o);
		}


		/**
		 * <p>
		 * Protected constructor.
		 * </p>
		 */
		protected BuDDyBDD() {
		}
		
		
		protected BuDDyBDD(int id) {
			_id = id;
			addRef(_id);
		}

		public BuDDyFactory getBuddyFactory() {
			return INSTANCE;
		}


		public boolean isZero() {
			return _id == 0;
		}

		public boolean isOne() {
			return _id == 1;
		}

		public int var() {
			return var0(_id);
		}

		synchronized private static native int var0(int b);


		public BuDDyBDD high() {
			int b = high0(_id);
			return makeBDD(b);
		}

		synchronized private static native int high0(int b);

		public BuDDyBDD low() {
			int b = low0(_id);
			return makeBDD(b);
		}

		synchronized private static native int low0(int b);

		public BuDDyBDD id() {
			return makeBDD(_id);
		}

		public BuDDyBDD not() {
			int b = not0(_id);
			return makeBDD(b);
		}

		synchronized private static native int not0(int b);

		public BuDDyBDD ite(BuDDyBDD thenBDD, BuDDyBDD elseBDD) {
			int b = ite0(_id, thenBDD._id, elseBDD._id);
			return makeBDD(b);
		}

		synchronized private static native int ite0(int b, int c, int d);

		public BuDDyBDD relprod(BuDDyBDD that, BuDDyBDD var) {
			int b = relprod0(_id, that._id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int relprod0(int b, int c, int d);

		public BuDDyBDD compose(BuDDyBDD that, int var) {
			int b = compose0(_id, that._id, var);
			return makeBDD(b);
		}

		synchronized private static native int compose0(int b, int c, int var);

		public BuDDyBDD constrain(BuDDyBDD var) {
			int b = constrain0(_id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int constrain0(int b, int c);

		public BuDDyBDD exist(BuDDyBDD var) {
			int b = exist0(_id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int exist0(int b, int var);

		public BuDDyBDD forAll(BuDDyBDD var) {
			int b = forAll0(_id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int forAll0(int b, int var);

		public BuDDyBDD unique(BuDDyBDD var) {
			int b = unique0(_id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int unique0(int b, int var);

		public BuDDyBDD restrict(BuDDyBDD var) {
			int b = restrict0(_id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int restrict0(int b, int var);

		public BuDDyBDD restrictWith(BuDDyBDD var) {
			int b = restrict0(_id, var._id);
			addRef(b);
			delRef(_id);
			if (this != var) {
				delRef(var._id);
				var._id = INVALID_BDD;
			}
			_id = b;
			return this;
		}

		public BuDDyBDD simplify(BuDDyBDD that) {
			int b = simplify0(_id, that._id);
			return makeBDD(b);
		}

		synchronized private static native int simplify0(int b, int d);

		public BuDDyBDD support() {
			int b = support0(_id);
			return makeBDD(b);
		}

		synchronized private static native int support0(int b);

		public BuDDyBDD apply(BuDDyBDD that, LogicalOperator opr) {
			if (that == null)
				throw new RuntimeException("Apply null");
			int b = apply0(_id, that._id, opr.id);
			return makeBDD(b);
		}

		synchronized private static native int apply0(int b, int c, int opr);

		public BuDDyBDD applyWith(BuDDyBDD var, LogicalOperator opr) {
			int b = apply0(_id, var._id, opr.id);
			addRef(b);
			delRef(_id);
			if (this != var) {
				delRef(var._id);
				var._id = INVALID_BDD;
			}
			_id = b;
			return this;
		}

		
		public BuDDyBDD applyAll(BuDDyBDD that, LogicalOperator opr, BuDDyBDD var) {
			int b = applyAll0(_id, that._id, opr.id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int applyAll0(int b, int c, int opr, int d);

		public BuDDyBDD applyEx(BuDDyBDD that, LogicalOperator opr, BuDDyBDD var) {
			int b = applyEx0(_id, that._id, opr.id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int applyEx0(int b, int c, int opr, int d);

		public BuDDyBDD applyUni(BuDDyBDD that, LogicalOperator opr, BuDDyBDD var) {
			int b = applyUni0(_id, that._id, opr.id, var._id);
			return makeBDD(b);
		}

		synchronized private static native int applyUni0(int b, int c, int opr, int d);

		public BuDDyBDD satOne() {
			int b = satOne0(_id);
			return makeBDD(b);
		}

		synchronized private static native int satOne0(int b);

		public BuDDyBDD satOne(BuDDyBDD var, boolean pol) {
			BuDDyBDD c = (BuDDyBDD) var;
			int b = satOne1(_id, c._id, pol ? 1 : 0);
			return makeBDD(b);
		}

		synchronized private static native int satOne1(int b, int c, int d);

		public List<byte[]> allsat() {
			return Arrays.asList(allsat0(_id));
		}

		synchronized private static native byte[][] allsat0(int b);


		public int nodeCount() {
			return nodeCount0(_id);
		}

		synchronized private static native int nodeCount0(int b);

		public double satCount() {
			return satCount0(_id);
		}

		synchronized private static native double satCount0(int b);

		public double satCount(BuDDyBDD varset) {
			BuDDyBDD c = (BuDDyBDD) varset;
			return satCount1(_id, c._id);
		}

		synchronized private static native double satCount1(int b, int c);

		synchronized private static native void addRef(int b);

		synchronized private static native void delRef(int b);

		public void free() {
			if ((_id == 0) || (_id == 1))
				return;
			if (INSTANCE != null) {
				delRef(_id);
			}
			_id = INVALID_BDD;
		}

		public boolean equals(BuDDyBDD that) {
			return this._id == ((BuDDyBDD) that)._id;
		}

		public int hashCode() {
			return this._id;
		}
		
		
		
		   /**
	     * Stores statistics about reordering.
	     * 
	     * @author jwhaley
	     * @version $Id: BDDFactory.java,v 1.11 2005/05/04 22:31:35 joewhaley Exp $
	     */
	    public static class ReorderStats {
	        
	        public long time;
	        public int usednum_before, usednum_after;
	        
	        protected ReorderStats() { }
	        
	        public int gain() {
	            if (usednum_before == 0)
	                return 0;

	            return (100 * (usednum_before - usednum_after)) / usednum_before;
	        }
	        
	        public String toString() {
	            StringBuffer sb = new StringBuffer();
	            sb.append("Went from ");
	            sb.append(usednum_before);
	            sb.append(" to ");
	            sb.append(usednum_after);
	            sb.append(" nodes, gain = ");
	            sb.append(gain());
	            sb.append("% (");
	            sb.append((float) time / 1000f);
	            sb.append(" sec)");
	            return sb.toString();
	        }
	    }
	    
	    /**
	     * Singleton object for reorder statistics.
	     */
	    protected ReorderStats reorderstats = new ReorderStats();
	    
		

	}
	

	public static LogicalOperator and = new LogicalOperator(0, "and");
	public static LogicalOperator xor = new LogicalOperator(1, "xor");
	public static LogicalOperator or = new LogicalOperator(2, "or");
	public static LogicalOperator nand = new LogicalOperator(3, "nand");
	public static LogicalOperator nor = new LogicalOperator(4, "nor");
	public static LogicalOperator imp = new LogicalOperator(5, "imp");
	public static LogicalOperator biimp = new LogicalOperator(6, "biimp");
	public static LogicalOperator diff = new LogicalOperator(7, "diff");
	public static LogicalOperator less = new LogicalOperator(8, "less");
	public static LogicalOperator invimp = new LogicalOperator(9, "invimp");
	
	
	
  /**** CALLBACKS ****/
    
    protected List gc_callbacks, reorder_callbacks, resize_callbacks;
    
    /**
     * <p>Register a callback that is called when garbage collection is about
     * to occur.</p>
     * 
     * @param o  base object
     * @param m  method
     */
    public void registerGCCallback(Object o, Method m) {
        if (gc_callbacks == null) gc_callbacks = new LinkedList();
        registerCallback(gc_callbacks, o, m);
    }
    
    /**
     * <p>Unregister a garbage collection callback that was previously
     * registered.</p>
     * 
     * @param o  base object
     * @param m  method
     */
    public void unregisterGCCallback(Object o, Method m) {
        if (gc_callbacks == null) throw new RuntimeException();
        if (!unregisterCallback(gc_callbacks, o, m))
            throw new RuntimeException();
    }
    
    /**
     * <p>Register a callback that is called when reordering is about
     * to occur.</p>
     * 
     * @param o  base object
     * @param m  method
     */
    public void registerReorderCallback(Object o, Method m) {
        if (reorder_callbacks == null) reorder_callbacks = new LinkedList();
        registerCallback(reorder_callbacks, o, m);
    }
    
    /**
     * <p>Unregister a reorder callback that was previously
     * registered.</p>
     * 
     * @param o  base object
     * @param m  method
     */
    public void unregisterReorderCallback(Object o, Method m) {
        if (reorder_callbacks == null) throw new RuntimeException();
        if (!unregisterCallback(reorder_callbacks, o, m))
            throw new RuntimeException();
    }
    
    /**
     * <p>Register a callback that is called when node table resizing is about
     * to occur.</p>
     * 
     * @param o  base object
     * @param m  method
     */
    public void registerResizeCallback(Object o, Method m) {
        if (resize_callbacks == null) resize_callbacks = new LinkedList();
        registerCallback(resize_callbacks, o, m);
    }
    
    /**
     * <p>Unregister a reorder callback that was previously
     * registered.</p>
     * 
     * @param o  base object
     * @param m  method
     */
    public void unregisterResizeCallback(Object o, Method m) {
        if (resize_callbacks == null) throw new RuntimeException();
        if (!unregisterCallback(resize_callbacks, o, m))
            throw new RuntimeException();
    }
    
    protected void gbc_handler(boolean pre, GCStats s) {
        if (gc_callbacks == null) {
            bdd_default_gbchandler(pre, s);
        } else {
            doCallbacks(gc_callbacks, new Integer(pre?1:0), s);
        }
    }
    
    protected static void bdd_default_gbchandler(boolean pre, GCStats s) {
        if (!pre) {
            System.err.println(s.toString());
        }
    }
    
    void reorder_handler(boolean b, ReorderStats s) {
        if (b) {
            s.usednum_before = getNodeNum();
            s.time = System.currentTimeMillis();
        } else {
            s.time = System.currentTimeMillis() - s.time;
            s.usednum_after = getNodeNum();
        }
        if (reorder_callbacks == null) {
            bdd_default_reohandler(b, s);
        } else {
            doCallbacks(reorder_callbacks, new Integer(b?1:0), s);
        }
    }

    protected void bdd_default_reohandler(boolean prestate, ReorderStats s) {
        int verbose = 1;
        if (verbose > 0) {
            if (prestate) {
                System.out.println("Start reordering");
                s.usednum_before = getNodeNum();
                s.time = System.currentTimeMillis();
            } else {
                s.time = System.currentTimeMillis() - s.time;
                s.usednum_after = getNodeNum();
                System.out.println("End reordering. "+s);
            }
        }
    }

    protected void resize_handler(int oldsize, int newsize) {
        if (resize_callbacks == null) {
            bdd_default_reshandler(oldsize, newsize);
        } else {
            doCallbacks(resize_callbacks, new Integer(oldsize), new Integer(newsize));
        }
    }

    protected static void bdd_default_reshandler(int oldsize, int newsize) {
        int verbose = 1;
        if (verbose > 0) {
            System.out.println("Resizing node table from "+oldsize+" to "+newsize);
        }
    }
    
    protected void registerCallback(List callbacks, Object o, Method m) {
        if (!Modifier.isPublic(m.getModifiers()) && !m.isAccessible()) {
            throw new RuntimeException("Callback method not accessible");
        }
        if (!Modifier.isStatic(m.getModifiers())) {
            if (o == null) {
                throw new RuntimeException("Base object for callback method is null");
            }
            if (!m.getDeclaringClass().isAssignableFrom(o.getClass())) {
                throw new RuntimeException("Base object for callback method is the wrong type");
            }
        }
        if (false) {
            Class[] params = m.getParameterTypes();
            if (params.length != 1 || params[0] != int.class) {
                throw new RuntimeException("Wrong signature for callback");
            }
        }
        callbacks.add(new Object[] { o, m });
    }
    
    protected boolean unregisterCallback(List callbacks, Object o, Method m) {
        if (callbacks != null) {
            for (Iterator i = callbacks.iterator(); i.hasNext(); ) {
                Object[] cb = (Object[]) i.next();
                if (o == cb[0] && m.equals(cb[1])) {
                    i.remove();
                    return true;
                }
            }
        }
        return false;
    }
    
    protected void doCallbacks(List callbacks, Object arg1, Object arg2) {
        if (callbacks != null) {
            for (Iterator i = callbacks.iterator(); i.hasNext(); ) {
                Object[] cb = (Object[]) i.next();
                Object o = cb[0];
                Method m = (Method) cb[1];
                try {
                    switch (m.getParameterTypes().length) {
                    case 0:
                        m.invoke(o, new Object[] { } );
                        break;
                    case 1:
                        m.invoke(o, new Object[] { arg1 } );
                        break;
                    case 2:
                        m.invoke(o, new Object[] { arg1, arg2 } );
                        break;
                    default:
                        throw new RuntimeException("Wrong number of arguments for "+m);
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    if (e.getTargetException() instanceof RuntimeException)
                        throw (RuntimeException) e.getTargetException();
                    if (e.getTargetException() instanceof Error)
                        throw (Error) e.getTargetException();
                    e.printStackTrace();
                }
            }
        }
    }

    
    private static void resize_callback(int i, int j) {
        INSTANCE.resize_handler(i, j);
    }
	

	
}
