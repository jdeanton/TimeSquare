package net.sf.javabdd;

public class BDDException extends RuntimeException {

	public BDDException(String msg) {
		super(msg);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -7238785517012324851L;

}
