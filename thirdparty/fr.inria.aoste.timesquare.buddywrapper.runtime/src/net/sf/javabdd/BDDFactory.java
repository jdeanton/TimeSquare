package net.sf.javabdd;

public class BDDFactory {

	public BDDFactory() {
	}
		
    public static class GCStats {
        public int nodes;
        public int freenodes;
        public long time;
        public long sumtime;
        public int num;
        
        protected GCStats() { }
        
        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        public String toString() {
//             StringBuffer sb = new StringBuffer();
//             sb.append("Garbage collection #");
//             sb.append(num);
//             sb.append(": ");
//             sb.append(nodes);
//             sb.append(" nodes / ");
//             sb.append(freenodes);
//             sb.append(" free");
//             
//             sb.append(" / ");
//             sb.append((float) time / (float) 1000);
//             sb.append("s / ");
//             sb.append((float) sumtime / (float) 1000);
//             sb.append("s total");
            return "";//sb.toString();
        }
    }
    
  
	    
	    /**
	     * Singleton object for GC statistics.
	     */
	    protected GCStats gcstats = new GCStats();
	    
	    /**
	     * <p>Return the current GC statistics for this BDD factory.</p>
	     * 
	     * @return  GC statistics
	     */
	    public GCStats getGCStats() {
	        return gcstats;
	    }

}
