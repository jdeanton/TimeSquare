package fr.inria.aoste.timesquare.buddywrapper;

public class LogicalOperator {
	public final int id;
	public String name;

	public LogicalOperator(int id, String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
