package fr.inria.aoste.timesquare.buddywrapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.AccessControlException;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.osgi.internal.framework.EquinoxBundle;
import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.eclipse.osgi.storage.BundleInfo.Generation;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * The activator class controls the plug-in life cycle
 */

@SuppressWarnings("restriction")
public class Activator extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "fr.inria.aoste.timesquare.buddywrapper.runtime";

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		ServiceReference<EnvironmentInfo> sr = context.getServiceReference(EnvironmentInfo.class);
		EnvironmentInfo ei = context.getService(sr);
		os = ei.getOS();
		arch = ei.getOSArch();
		context.ungetService(sr);
	}
	
	public static String os = "linux";
	public static String arch = "x86_64";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	/**
	 * Loads a native library by looking for library name composed with the base name given in the
	 * libname parameter followed by the os identifier then the architecture. eg. XXX-linux-x86_64.
	 * @param libname
	 * @return true if the load succeeds
	 */
	public static boolean loadNativeLibrary(String libname) {
		return loadLibrary("buddy"  + "-" + os + "-" + arch);
	}

	public static boolean loadLibrary(String libvname) {
		String libname = null;
		String filename = null;
		// Load Method 1
		try {
			System.loadLibrary(libvname);
			return true;
		} catch (Throwable x) {
			System.out.println("M1 " + x);
		}

		try {
			
//		 	AbstractBundle bundleImpl = (AbstractBundle) Activator.getDefault().getBundle();
//			BaseData bundleData = (BaseData) bundleImpl.getBundleData();
//			//EquinoxBundle bundleImpl = ((EquinoxBundle) Activator.getDefault().getBundle());
//			//Generation generation = (Generation) bundleImpl.getModule().getCurrentRevision().getRevisionInfo();
//			String s = bundleData.findLibrary(libvname);//getModule().getCurrentRevision().getRevisionInfo().findLibrary(libvname);
//			System.load(new File(s).toString());
			
			
			
			EquinoxBundle bundleImpl = ((EquinoxBundle) Activator.getDefault().getBundle());
			Generation generation = (Generation) bundleImpl.getModule().getCurrentRevision().getRevisionInfo();
			String s = generation.findLibrary(libvname);
			System.load(new File(s).toString());
			return true;
		} catch (Throwable x) {
			System.out.println("M2 " + x);
		}
		// Load Method 3
		try {
			libname = System.mapLibraryName(libvname);
			System.loadLibrary(libname);
			return true;
		} catch (Throwable e) {
			System.out.println("M3 " + e);
		}
		try {

			InputStream fis = FileLocator.openStream(Activator.getDefault()
					.getBundle(), new Path("/" + libname), false);

			File f2 = File.createTempFile(libvname, ".dll");
			if (!f2.exists()) {
				copyFile(fis, f2);
			}
			// f2.deleteOnExit();
			System.load(f2.getAbsolutePath());
			return true;

		} catch (Throwable y) {
			System.out.println("M4 " + y);
		}
		File f = null;
		try {
			libname = System.mapLibraryName(libvname);

			String currentdir = getProperty("user.dir", ".");
			String sep = getProperty("file.separator", "/");
			filename = currentdir + sep + libname;
			f = new File(filename);
			System.load(f.toString());
			return true;
		} catch (Throwable e) {
			System.out.println("M5 " + e);
		}

		try {
			File f2 = File.createTempFile(libvname, ".dll");
			copyFile(f, f2);
			f2.deleteOnExit();
			System.load(f2.toString());
			return true;
		} catch (Throwable z) {
			System.out.println("M6 " + z);
		}
		return false;
	}

	private static final String getProperty(String key, String def) {
		try {
			return System.getProperty(key, def);
		} catch (AccessControlException ace) {
			return def;
		}
	}

	private static void copyFile(InputStream fis, File out) throws IOException {
		// FileInputStream fis = new FileInputStream(in);
		FileOutputStream fos = new FileOutputStream(out);
		byte[] buf = new byte[1024];
		int i = 0;
		while ((i = fis.read(buf)) != -1) {
			fos.write(buf, 0, i);
		}
		fis.close();
		fos.close();
	}

	private static void copyFile(File in, File out) throws IOException {
		FileInputStream fis = new FileInputStream(in);
		FileOutputStream fos = new FileOutputStream(out);
		byte[] buf = new byte[1024];
		int i = 0;
		while ((i = fis.read(buf)) != -1) {
			fos.write(buf, 0, i);
		}
		fis.close();
		fos.close();
	}

	public final static String bdd = "bdd";

}
