/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
//package tests;
//
//import fr.inria.aoste.utils.grph.FindAllCycles;
//import grph.Grph;
//import grph.algo.topology.ClassicalGraphs;
//import grph.path.ArrayPath;
//import toools.thread.Threads;
//
//import com.carrotsearch.hppc.IntArrayList;
//
//public class TestFindCycles {
//
//	
//	
//	public static void main(String[] args) {
//		 int n = 6;
//
//		 Grph g = ClassicalGraphs.grid(n, n);
//
//		 g.display();
//
//		 FindAllCycles algo = new FindAllCycles(g);
//
//		 for (IntArrayList v : algo)
//		 {
//	     
////		 ArrayPath p = new ArrayPath(v.toArray());
////
////		 p.setColor(g, 3);
////
////		 Threads.sleepMs(1000);
////
//	 System.out.println(v);
////		 
////		 p.setColor(g,0);
//		 }
//	}
//	
//}
